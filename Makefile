dev-start:
	docker-compose up -d
dev-restart:
	docker-compose down && docker-compose up -d
dev-php:
	docker exec -it s-routes-php-fpm /bin/bash

