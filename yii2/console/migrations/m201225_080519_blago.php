<?php

use yii\db\Migration;

/**
 * Class m201225_080519_blago
 */
class m201225_080519_blago extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('school_koop_project', 'type_id', 'tinyint null');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m201225_080519_blago cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m201225_080519_blago cannot be reverted.\n";

        return false;
    }
    */
}
