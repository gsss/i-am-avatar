
<?php

use common\models\avatar\Currency;
use yii\db\Migration;

class m171111_100905_token extends Migration
{


    public function safeUp()
    {
        $this->addColumn('school_settings', 'guest_frontend', 'text default null');
        $this->addColumn('school_settings', 'auth_frontend', 'text default null');

        $this->execute('CREATE TABLE `school_settings_main` (
      `id` int(11) NOT NULL AUTO_INCREMENT,
	school_id int null,
	guest_frontend text null,
	auth_backend text null,
	auth_frontend text null,
    PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8');
    }

    public function safeDown()
    {
        echo "m171111_100717_token cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m171111_100717_token cannot be reverted.\n";

        return false;
    }
    */
}
