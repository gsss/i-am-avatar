<?php

use yii\db\Migration;

/**
 * Class m200624_123257_dns
 */
class m200624_123257_dns extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        foreach (\common\models\school\School::find()->all() as $s) {
            if (!\cs\Application::isEmpty($s->dns)) {
                if (!\yii\helpers\StringHelper::startsWith($s->dns, 'http')) {
                    $s->dns = 'http://' . $s->dns . '.' . Yii::$app->params['root_url'];
                    $s->save();
                    echo $s->dns .  "\n";
                }
            }
        }
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m200624_123257_dns cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200624_123257_dns cannot be reverted.\n";

        return false;
    }
    */
}
