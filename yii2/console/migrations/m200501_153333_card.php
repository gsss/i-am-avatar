<?php

use yii\db\Migration;

/**
 * Class m200501_153333_card
 */
class m200501_153333_card extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->execute('CREATE TABLE `card_school_link` (
      `id` int(11) NOT NULL AUTO_INCREMENT,
    school_id          int                  null,
    card_id          int                  null,
    PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m200501_153333_card cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200501_153333_card cannot be reverted.\n";

        return false;
    }
    */
}
