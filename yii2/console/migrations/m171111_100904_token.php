
<?php

use common\models\avatar\Currency;
use yii\db\Migration;

class m171111_100904_token extends Migration
{


    public function safeUp()
    {
        $this->addColumn('school_settings', 'news_image_cut_id', 'tinyint default null');
        $this->addColumn('school_settings', 'news_image_width', 'int default null');
        $this->addColumn('school_settings', 'news_image_height', 'int default null');
    }

    public function safeDown()
    {
        echo "m171111_100717_token cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m171111_100717_token cannot be reverted.\n";

        return false;
    }
    */
}
