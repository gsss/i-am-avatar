<?php

use yii\db\Migration;

/**
 * Class m200510_171541_task
 */
class m200510_171541_task extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createIndex('school_task_school_id', 'school_task','school_id' );
        $this->createIndex('school_task_user_id', 'school_task','user_id' );
        $this->createIndex('school_task_created_at', 'school_task','created_at' );
        $this->createIndex('school_task_status', 'school_task','status' );
        $this->createIndex('school_task_executer_id', 'school_task','executer_id' );
        $this->createIndex('school_task_category_id', 'school_task','category_id' );
        $this->createIndex('school_task_parent_id', 'school_task','parent_id' );
        $this->createIndex('school_task_is_hide', 'school_task','is_hide' );

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m200510_171541_task cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200510_171541_task cannot be reverted.\n";

        return false;
    }
    */
}
