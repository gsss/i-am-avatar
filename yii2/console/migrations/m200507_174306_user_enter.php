<?php

use yii\db\Migration;

/**
 * Class m200507_174306_user_enter
 */
class m200507_174306_user_enter extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->dropColumn('user_enter', 'email');
        $this->dropColumn('user_enter', 'iof');
        $this->dropColumn('user_enter', 'place');
        $this->dropColumn('user_enter', 'address');
        $this->dropColumn('user_enter', 'whatsapp');
        $this->dropColumn('user_enter', 'place_type_id');
        $this->dropColumn('user_enter', 'address_gruz');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m200507_174306_user_enter cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200507_174306_user_enter cannot be reverted.\n";

        return false;
    }
    */
}
