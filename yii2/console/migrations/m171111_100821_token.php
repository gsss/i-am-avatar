<?php

use common\models\avatar\Currency;
use yii\db\Migration;

class m171111_100821_token extends Migration
{


    public function safeUp()
    {
        $this->addColumn('school_kurs', 'for_who', 'text default null');
        $this->addColumn('school_kurs', 'daet', 'text default null');
        $this->addColumn('school_kurs', 'is_show', 'tinyint default 0');
    }

    public function safeDown()
    {
        echo "m171111_100717_token cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m171111_100717_token cannot be reverted.\n";

        return false;
    }
    */
}
