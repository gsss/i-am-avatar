<?php

use yii\db\Migration;

/**
 * Class m210412_200932_invest
 */
class m210412_200932_invest extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('school_invest_program', 'project_id', 'int default null');
        $this->createIndex('school_invest_program_project_id', 'school_invest_program', 'project_id');

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m210412_200932_invest cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210412_200932_invest cannot be reverted.\n";

        return false;
    }
    */
}
