<?php

use yii\db\Migration;

/**
 * Class m200423_220050_table
 */
class m200423_220050_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->execute('CREATE TABLE `school_task_table` (
      `id` int(11) NOT NULL AUTO_INCREMENT,
	school_id int null,
	name varchar(100) null,
    PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m200423_220050_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200423_220050_table cannot be reverted.\n";

        return false;
    }
    */
}
