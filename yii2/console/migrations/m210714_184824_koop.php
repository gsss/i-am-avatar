<?php

use yii\db\Migration;

/**
 * Class m210714_184824_koop
 */
class m210714_184824_koop extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('koop_podryad', 'agent', 'varchar(200) null');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m210714_184824_koop cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210714_184824_koop cannot be reverted.\n";

        return false;
    }
    */
}
