<?php

use common\models\avatar\Currency;
use yii\db\Migration;

class m171111_100851_token extends Migration
{

    public function safeUp()
    {
        $this->execute('CREATE TABLE `yandex_transaction` (
      `id` int(11) NOT NULL AUTO_INCREMENT,
      notification_type varchar(255) null,
	amount float null,
	datetime varchar(255) null,
	created_at int null,
	codepro varchar(255) null,
	withdraw_amount float null,
	sender varchar(255) null,
	sha1_hash varchar(255) null,
	unaccepted varchar(255) null,
	operation_label varchar(255) null,
	operation_id varchar(255) null,
	currency int null,
	label varchar(255) null,
	is_valid tinyint(1) null,
	zip varchar(100) null,
	firstname varchar(100) null,
	city varchar(100) null,
	building varchar(100) null,
	lastname varchar(100) null,
	suite varchar(100) null,
	phone varchar(100) null,
	street varchar(100) null,
	flat varchar(100) null,
	email varchar(100) null,
	fathersname varchar(100) null,
      PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8');

    }

    public function safeDown()
    {
        echo "m171111_100717_token cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m171111_100717_token cannot be reverted.\n";

        return false;
    }
    */
}
