<?php

use common\models\avatar\Currency;
use yii\db\Migration;

class m171111_100823_token extends Migration
{


    public function safeUp()
    {
        $this->addColumn('school_kurs', 'social_net_image', 'varchar(255) default null');
        $this->addColumn('school_kurs', 'social_net_title', 'varchar(255) default null');
        $this->addColumn('school_kurs', 'social_net_description', 'varchar(2000) default null');
    }

    public function safeDown()
    {
        echo "m171111_100717_token cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m171111_100717_token cannot be reverted.\n";

        return false;
    }
    */
}
