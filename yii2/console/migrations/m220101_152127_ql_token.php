<?php

use yii\db\Migration;

/**
 * Class m220101_152127_ql_token
 */
class m220101_152127_ql_token extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('school_blago_input', 'user_id', 'int null');
        $this->createIndex('school_blago_input_user_id', 'school_blago_input', 'user_id');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m220101_152127_ql_token cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m220101_152127_ql_token cannot be reverted.\n";

        return false;
    }
    */
}
