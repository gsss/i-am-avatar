<?php

use yii\db\Migration;

/**
 * Class m211102_121559_snt
 */
class m211102_121559_snt extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->execute('CREATE TABLE `school_snt_out` (
    `id` int(11) NOT NULL AUTO_INCREMENT,
    created_at  int          null,
	school_id   int          null,
    currency_id int          null,
    amount      int          null,
    user_id     int          null,
    PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8');

        $this->createIndex('school_snt_out_school_id', 'school_snt_out', 'school_id');

        $this->execute('CREATE TABLE `school_snt_pohod` (
    `id` int(11) NOT NULL AUTO_INCREMENT,
    created_at  int          null,
	school_id   int          null,
    date        date         null,
    description  text         null,
    PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8');

        $this->createIndex('school_snt_pohod_school_id', 'school_snt_pohod', 'school_id');

        $this->execute('CREATE TABLE `school_snt_pohod_file` (
    `id` int(11) NOT NULL AUTO_INCREMENT,
    created_at  int          null,
	pohod_id    int          null,
    file        text         null,
    PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8');

        $this->createIndex('school_snt_pohod_file_pohod_id', 'school_snt_pohod_file', 'pohod_id');

        $this->execute('CREATE TABLE `school_snt_pohod_user` (
    `id` int(11) NOT NULL AUTO_INCREMENT,
    created_at  int          null,
	pohod_id    int          null,
	user_id     int          null,
	`start`     int          null,
	`end`       int          null,
	`tarif`     int          null,
    PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8');

        $this->createIndex('school_snt_pohod_user_pohod_id', 'school_snt_pohod_user', 'pohod_id');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m211102_121559_snt cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m211102_121559_snt cannot be reverted.\n";

        return false;
    }
    */
}
