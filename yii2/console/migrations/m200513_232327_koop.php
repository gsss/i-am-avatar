<?php

use yii\db\Migration;

/**
 * Class m200513_232327_koop
 */
class m200513_232327_koop extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {

         $this->execute('CREATE TABLE `school_role_link` (
    `id` int(11) NOT NULL AUTO_INCREMENT,
    name    varchar(255) null,
    school_id int        null,
    role_id int          null,
    user_id int          null,
    PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8');

         $this->execute('CREATE TABLE `school_role` (
    `id` int(11) NOT NULL AUTO_INCREMENT,
    name    varchar(255) null,
    school_id int null,
    PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m200513_232327_koop cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200513_232327_koop cannot be reverted.\n";

        return false;
    }
    */
}
