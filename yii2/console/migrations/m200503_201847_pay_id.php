<?php

use yii\db\Migration;

/**
 * Class m200503_201847_pay_id
 */
class m200503_201847_pay_id extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('user_enter', 'pay_id', 'varchar(64) null');
        $this->addColumn('user_enter', 'is_payd', 'tinyint default 0');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m200503_201847_pay_id cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200503_201847_pay_id cannot be reverted.\n";

        return false;
    }
    */
}
