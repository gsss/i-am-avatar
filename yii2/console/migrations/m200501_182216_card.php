<?php

use yii\db\Migration;

/**
 * Class m200501_182216_card
 */
class m200501_182216_card extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('card', 'pin', 'varchar(100) null');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m200501_182216_card cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200501_182216_card cannot be reverted.\n";

        return false;
    }
    */
}
