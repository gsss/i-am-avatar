<?php

use common\models\avatar\Currency;
use yii\db\Migration;

class m171111_100831_token extends Migration
{

    public function safeUp()
    {
        $this->execute('CREATE TABLE `school_potok_fields_list` (
      `id` int(11) NOT NULL AUTO_INCREMENT,
      `field_id` int(11) DEFAULT NULL,
      `name` VARCHAR(100) DEFAULT NULL,
      PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8');
        $this->execute('CREATE TABLE `school_potok_fields` (
      `id` int(11) NOT NULL AUTO_INCREMENT,
      `potok_id` int(11) DEFAULT NULL,
      `type_id` int(11) DEFAULT NULL,
      `is_required` int(11) DEFAULT NULL,
      `created_at` int(11) DEFAULT NULL,
      `name` VARCHAR(100) DEFAULT NULL,
      `description` VARCHAR(1000) DEFAULT NULL,
      PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8');
    }

    public function safeDown()
    {
        echo "m171111_100717_token cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m171111_100717_token cannot be reverted.\n";

        return false;
    }
    */
}
