<?php

use yii\db\Migration;

/**
 * Class m211231_184458_reg
 */
class m211231_184458_reg extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->execute('CREATE TABLE `user_register_code` (
    `id` int(11) NOT NULL AUTO_INCREMENT,
	parent_id     int          null,
    code          VARCHAR(6)   null,
    created_at    int          null,
    date_finish   int          null,
    PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8');

        $this->createIndex('user_register_code_parent_id', 'user_register_code', 'parent_id');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m211231_184458_reg cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m211231_184458_reg cannot be reverted.\n";

        return false;
    }
    */
}
