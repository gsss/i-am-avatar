<?php

use yii\db\Migration;

/**
 * Class m211221_190343_vote
 */
class m211221_190343_vote extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('vote_item', 'hash', 'varchar (64) null');
        $this->addColumn('vote_item', 'sign', 'varchar (128) null');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m211221_190343_vote cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m211221_190343_vote cannot be reverted.\n";

        return false;
    }
    */
}
