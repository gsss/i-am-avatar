<?php

use yii\db\Migration;

/**
 * Class m200513_205236_koop
 */
class m200513_205236_koop extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('school_koop', 'okved', 'varchar(10) null');
        $this->addColumn('school_koop', 'okved_dop', 'varchar(1000) null');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m200513_205236_koop cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200513_205236_koop cannot be reverted.\n";

        return false;
    }
    */
}
