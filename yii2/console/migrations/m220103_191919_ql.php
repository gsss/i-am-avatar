<?php

use yii\db\Migration;

/**
 * Class m220103_191919_ql
 */
class m220103_191919_ql extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->execute('CREATE TABLE `company_blago_input_transaction` (
    `id` int(11) NOT NULL AUTO_INCREMENT,
	proram_id     int          null,
    created_at    int          null,
    oid           int          null,
    input_id      int          null,
    PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8');

        $this->createIndex('company_blago_input_transaction_proram_id', 'company_blago_input_transaction', 'proram_id');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m220103_191919_ql cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m220103_191919_ql cannot be reverted.\n";

        return false;
    }
    */
}
