<?php

use yii\db\Migration;

/**
 * Class m210205_140416_p4p
 */
class m210205_140416_p4p extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        \common\models\avatar\CurrencyLink::add([
            'currency_int_id' => 15,
            'currency_ext_id' => 16,
        ]);
        $s = \common\models\school\School::findOne(84);
        $s->wallet_id = 15;
        $s->save();
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m210205_140416_p4p cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210205_140416_p4p cannot be reverted.\n";

        return false;
    }
    */
}
