<?php

use yii\db\Migration;

/**
 * Class m200603_202403_api
 */
class m200603_202403_api extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('school_settings', 'onec_api_url', 'varchar(255) null');
        $this->addColumn('school_settings', 'onec_api_key', 'varchar(255) null');
        $this->addColumn('school_settings', 'sms_login', 'varchar(255) null');
        $this->addColumn('school_settings', 'sms_password', 'varchar(255) null');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m200603_202403_api cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200603_202403_api cannot be reverted.\n";

        return false;
    }
    */
}
