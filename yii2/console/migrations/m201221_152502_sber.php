<?php

use yii\db\Migration;

/**
 * Class m201221_152502_sber
 */
class m201221_152502_sber extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->execute('CREATE TABLE `payments_sber_kassa` (
    `id` int(11) NOT NULL AUTO_INCREMENT,
    billing_id int null,
	payment_id varchar(100) null,
	currency_id int null,
	amount int null,
	created_at int null,
    PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8');

        $this->createIndex('payments_sber_kassa_billing_id', 'payments_sber_kassa', 'billing_id');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m201221_152502_sber cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m201221_152502_sber cannot be reverted.\n";

        return false;
    }
    */
}
