<?php

use yii\db\Migration;

/**
 * Class m200520_153028_vote
 */
class m200520_153028_vote extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('vote_list', 'price', 'varchar(30) null');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m200520_153028_vote cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200520_153028_vote cannot be reverted.\n";

        return false;
    }
    */
}
