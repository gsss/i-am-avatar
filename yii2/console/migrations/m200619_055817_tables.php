<?php

use yii\db\Migration;

/**
 * Class m200619_055817_tables
 */
class m200619_055817_tables extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->execute('CREATE TABLE `table` (
    `id` int(11) NOT NULL AUTO_INCREMENT,
    column_id  int          null,
    comment       varchar(100) null,
    PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8');

        $this->createIndex('table_column_id', 'table', 'column_id');

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m200619_055817_tables cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200619_055817_tables cannot be reverted.\n";

        return false;
    }
    */
}
