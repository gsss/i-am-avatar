<?php

use yii\db\Migration;

/**
 * Class m200529_002053_chat
 */
class m200529_002053_chat extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {


$this->execute('CREATE TABLE `chat_group` (
    `id` int(11) NOT NULL AUTO_INCREMENT,
    room_id    int          null,
    name       varchar(100) null,
    image      varchar(100) null,
    author_id  int          null,
    created_at int          null,
    PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8');

        $this->createIndex('chat_group_author_id', 'chat_group', 'author_id');
        $this->createIndex('chat_group_room_id', 'chat_group', 'room_id');

$this->execute('CREATE TABLE `chat_group_user` (
    `id` int(11) NOT NULL AUTO_INCREMENT,
    group_id int null,
    user_id  int null,
    PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8');

        $this->createIndex('chat_group_user_group_id', 'chat_group_user', 'group_id');

$this->execute('CREATE TABLE `chat_list` (
    `id` int(11) NOT NULL AUTO_INCREMENT,
    room_id int null,
    user_id int null,
    PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8');

        $this->createIndex('chat_list_user_id', 'chat_list', 'user_id');

$this->execute('CREATE TABLE `chat_message` (
    `id` int(11) NOT NULL AUTO_INCREMENT,
    room_id    int  null,
    user_id    int  null,
    text       text null,
    created_at int  null,
    PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8');

        $this->createIndex('chat_message_room_id', 'chat_message', 'room_id');

$this->execute('CREATE TABLE `chat_room` (
    `id` int(11) NOT NULL AUTO_INCREMENT,
    type_id      int null,
    last_message int null,
    PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8');

        $this->createIndex('chat_room_last_message', 'chat_room', 'last_message');

$this->execute('CREATE TABLE `chat_tet` (
    `id` int(11) NOT NULL AUTO_INCREMENT,
    user1_id int null,
    user2_id int null,
    room_id  int null,
    PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8');

        $this->createIndex('chat_tet_room_id', 'chat_tet', 'room_id');


    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m200529_002053_chat cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200529_002053_chat cannot be reverted.\n";

        return false;
    }
    */
}
