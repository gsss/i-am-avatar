<?php

use yii\db\Migration;

/**
 * Class m220224_235123_task
 */
class m220224_235123_task extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->execute('ALTER TABLE school_task MODIFY price BIGINT;');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m220224_235123_task cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m220224_235123_task cannot be reverted.\n";

        return false;
    }
    */
}
