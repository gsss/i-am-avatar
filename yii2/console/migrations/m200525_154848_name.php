<?php

use yii\db\Migration;

/**
 * Class m200525_154848_name
 */
class m200525_154848_name extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m200525_154848_name cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200525_154848_name cannot be reverted.\n";

        return false;
    }
    */
}
