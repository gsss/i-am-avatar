<?php

use yii\db\Migration;

/**
 * Class m210715_100322_tz
 */
class m210715_100322_tz extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('koop_podryad_command_user', 'tz_address', 'varchar(100) null');
        $this->addColumn('koop_podryad_command_user', 'tz_sign', 'varchar(100) null');
        $this->addColumn('koop_podryad_command_user', 'tz_created_at', 'int null');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m210715_100322_tz cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210715_100322_tz cannot be reverted.\n";

        return false;
    }
    */
}
