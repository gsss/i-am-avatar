<?php

use yii\db\Migration;

/**
 * Class m210813_153635_ruse
 */
class m210813_153635_ruse extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('user_enter', 'inn', 'string(20) null');
        $this->addColumn('user_enter', 'snils', 'string(20) null');
        $this->addColumn('user_enter', 'passport_reg_address', 'string(200) null');
        $this->addColumn('user_enter', 'grajd', 'string(200) null');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m210813_153635_ruse cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210813_153635_ruse cannot be reverted.\n";

        return false;
    }
    */
}
