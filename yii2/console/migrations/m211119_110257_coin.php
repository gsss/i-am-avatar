<?php

use yii\db\Migration;

/**
 * Class m211119_110257_coin
 */
class m211119_110257_coin extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('school_coin', 'wallet_id', 'int null');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m211119_110257_coin cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m211119_110257_coin cannot be reverted.\n";

        return false;
    }
    */
}
