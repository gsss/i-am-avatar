<?php

use yii\db\Migration;

/**
 * Class m201218_045521_aim
 */
class m201218_045521_aim extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('school_blago_program' , 'project_id', 'int null');

        $this->execute('CREATE TABLE `school_koop_program` (
    `id` int(11) NOT NULL AUTO_INCREMENT,
    school_id  int    null,
    name  varchar(200)    null,
    document  varchar(200)    null,
    link  varchar(200)    null,
    created_at  int null,
    PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8');

        $this->execute('CREATE TABLE `school_koop_project` (
    `id` int(11) NOT NULL AUTO_INCREMENT,
    school_id  int    null,
    program_id  int    null,
    name  varchar(200)    null,
    document  varchar(200)    null,
    link  varchar(200)    null,
    author_id  int null,
    created_at  int null,
    PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8');

        $this->createIndex('school_koop_program_school_id','school_koop_program', 'school_id');
        $this->createIndex('school_koop_project_school_id','school_koop_project', 'school_id');
        $this->createIndex('school_koop_project_program_id','school_koop_project', 'program_id');
        $this->createIndex('school_koop_project_author_id','school_koop_project', 'author_id');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m201218_045521_aim cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m201218_045521_aim cannot be reverted.\n";

        return false;
    }
    */
}
