
<?php

use common\models\avatar\Currency;
use yii\db\Migration;

class m171111_100859_token extends Migration
{
    public function init()
    {
        parent::init();
        $module = \Yii::$app->getModule('sroutes');
        $this->db = $module->db;
    }

    public function safeUp()
    {
        $this->execute('CREATE TABLE `product_ext_info` (
      `id` int(11) NOT NULL AUTO_INCREMENT,
	product_id int null,
	oferta_id int null,
    PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8');
    }

    public function safeDown()
    {
        echo "m171111_100717_token cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m171111_100717_token cannot be reverted.\n";

        return false;
    }
    */
}
