
<?php

use common\models\avatar\Currency;
use yii\db\Migration;

class m171111_100862_token extends Migration
{


    public function safeUp()
    {
        $this->execute('CREATE TABLE `card_id` (
      `id` int(11) NOT NULL AUTO_INCREMENT,
	name_first VARCHAR(100) null,
	name_patronymic VARCHAR(100) null,
	name_last VARCHAR(100) null,
	date_born date null,
	date_get date null,
	place_born VARCHAR(100) null,
	image_top VARCHAR(100) null,
	image_back VARCHAR(100) null,
	created_at int null,
    PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8');
    }

    public function safeDown()
    {
        echo "m171111_100717_token cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m171111_100717_token cannot be reverted.\n";

        return false;
    }
    */
}
