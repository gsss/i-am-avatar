<?php

use yii\db\Migration;

/**
 * Class m210405_185639_invest
 */
class m210405_185639_invest extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        \common\models\BillingMainClass::add(['name' => '\common\models\SchoolInvestEvent']);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m210405_185639_invest cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210405_185639_invest cannot be reverted.\n";

        return false;
    }
    */
}
