<?php

use common\models\avatar\Currency;
use yii\db\Migration;

class m171111_100829_token extends Migration
{

    public function safeUp()
    {
        $this->addColumn('gs_users_shop_requests', 'is_paid_client', 'tinyint default 0');
    }

    public function safeDown()
    {
        echo "m171111_100717_token cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m171111_100717_token cannot be reverted.\n";

        return false;
    }
    */
}
