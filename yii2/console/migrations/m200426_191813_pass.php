<?php

use yii\db\Migration;

/**
 * Class m200426_191813_pass
 */
class m200426_191813_pass extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('user_enter', 'passport_scan1', 'varchar(255) null');
        $this->addColumn('user_enter', 'passport_scan2', 'varchar(255) null');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m200426_191813_pass cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200426_191813_pass cannot be reverted.\n";

        return false;
    }
    */
}
