<?php

use yii\db\Migration;

/**
 * Class m210412_194420_invest
 */
class m210412_194420_invest extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('school_invest_program', 'success_url', 'varchar(255) default null');
        $this->addColumn('school_invest_program', 'error_url', 'varchar(255) default null');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m210412_194420_invest cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210412_194420_invest cannot be reverted.\n";

        return false;
    }
    */
}
