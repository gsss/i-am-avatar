<?php

use yii\db\Migration;

/**
 * Class m210629_195338_coin
 */
class m210629_195338_coin extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $cExt = \common\models\avatar\Currency::add([
            'code'     => 'QLWB',
            'title'    => 'Качество жизни. Бюджет на разработку',
            'decimals' => 2,
        ]);
        $cInt = \common\models\piramida\Currency::add([
            'code'     => 'QLWB',
            'name'     => 'Качество жизни. Бюджет на разработку',
            'decimals' => 2,
        ]);
        $link = \common\models\avatar\CurrencyLink::add([
            'currency_int_id' => $cInt->id,
            'currency_ext_id' => $cExt->id,
        ]);
        $n = \common\models\SchoolCoin::findOne(1);
        $n->budjet_currency_id = $cInt->id;
        $n->save();
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m210629_195338_coin cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210629_195338_coin cannot be reverted.\n";

        return false;
    }
    */
}
