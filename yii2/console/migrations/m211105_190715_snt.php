<?php

use yii\db\Migration;

/**
 * Class m211105_190715_snt
 */
class m211105_190715_snt extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('school_snt_out', 'pohod_id', 'int null');

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m211105_190715_snt cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m211105_190715_snt cannot be reverted.\n";

        return false;
    }
    */
}
