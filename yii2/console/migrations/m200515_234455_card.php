<?php

use yii\db\Migration;

/**
 * Class m200515_234455_card
 */
class m200515_234455_card extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('school_design', 'card_design_id', 'int null');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m200515_234455_card cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200515_234455_card cannot be reverted.\n";

        return false;
    }
    */
}
