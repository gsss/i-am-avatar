
<?php

use common\models\avatar\Currency;
use yii\db\Migration;

class m171111_100888_token extends Migration
{


    public function safeUp()
    {
        $this->execute('CREATE TABLE `school_referal_out_request` (
      `id` int(11) NOT NULL AUTO_INCREMENT,
	school_id int null,
	user_id int null,
	currency_id int null,
	`to` varchar(200) null,
	amount int null,
	`status` int null,
	transaction_id int null,
	created_at int null,
    PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8');

$this->createIndex('school_referal_out_request_school_id', 'school_referal_out_request', 'school_id');
$this->createIndex('school_referal_out_request_user_id', 'school_referal_out_request', 'user_id');
    }

    public function safeDown()
    {
        echo "m171111_100717_token cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m171111_100717_token cannot be reverted.\n";

        return false;
    }
    */
}
