<?php

use yii\db\Migration;

/**
 * Class m210602_113653_coin
 */
class m210602_113653_coin extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m210602_113653_coin cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210602_113653_coin cannot be reverted.\n";

        return false;
    }
    */
}
