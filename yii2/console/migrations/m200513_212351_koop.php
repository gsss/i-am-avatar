<?php

use yii\db\Migration;

/**
 * Class m200513_212351_koop
 */
class m200513_212351_koop extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('vote_list', 'school_id', 'int null');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m200513_212351_koop cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200513_212351_koop cannot be reverted.\n";

        return false;
    }
    */
}
