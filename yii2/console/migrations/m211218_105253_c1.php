<?php

use yii\db\Migration;

/**
 * Class m211218_105253_c1
 */
class m211218_105253_c1 extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->execute('CREATE TABLE `cov_cert` (
    `id` int(11) NOT NULL AUTO_INCREMENT,
	user_id       int          null,
    uid           VARCHAR(64)  null,
    passport_num  VARCHAR(64)  null,
    end_date      date         null,
    born_date     date         null,
    created_at    int          null,
    company_id    int          null,
    PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8');

        $this->createIndex('cov_cert_company_id', 'cov_cert', 'company_id');

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m211218_105253_c1 cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m211218_105253_c1 cannot be reverted.\n";

        return false;
    }
    */
}
