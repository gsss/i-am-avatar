<?php

use yii\db\Migration;

/**
 * Class m210507_052942_sber
 */
class m210507_052942_sber extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('payments_sber_kassa', 'form_url', 'varchar(255) default null');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m210507_052942_sber cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210507_052942_sber cannot be reverted.\n";

        return false;
    }
    */
}
