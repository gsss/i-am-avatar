<?php

use yii\db\Migration;

/**
 * Class m210714_100922_money_income
 */
class m210714_100922_money_income extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->execute('alter table school_money_income
	add school_id int null;');
        $this->createIndex('school_money_income_school_id', 'school_money_income', 'school_id');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m210714_100922_money_income cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210714_100922_money_income cannot be reverted.\n";

        return false;
    }
    */
}
