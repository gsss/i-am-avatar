<?php

use yii\db\Migration;

/**
 * Class m220108_164209_task
 */
class m220108_164209_task extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->execute('CREATE TABLE `company_task_transaction` (
    `id` int(11) NOT NULL AUTO_INCREMENT,
	task_id       int          null,
    created_at    int          null,
    emission_oid  int          null,
    burn_oid      int          null,
    PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8');

        $this->createIndex('company_task_transaction_task_id', 'company_task_transaction', 'task_id');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m220108_164209_task cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m220108_164209_task cannot be reverted.\n";

        return false;
    }
    */
}
