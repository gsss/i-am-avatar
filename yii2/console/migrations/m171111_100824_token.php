<?php

use common\models\avatar\Currency;
use yii\db\Migration;

class m171111_100824_token extends Migration
{
    public function init()
    {
        $this->db = 'dbWallet';
        parent::init();
    }

    public function safeUp()
    {
        $this->execute('CREATE TABLE `login` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `login` VARCHAR(64) DEFAULT NULL,
  `secret` VARCHAR(64) DEFAULT NULL,
  `created_at` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8');
    }

    public function safeDown()
    {
        echo "m171111_100717_token cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m171111_100717_token cannot be reverted.\n";

        return false;
    }
    */
}
