<?php

use yii\db\Migration;

/**
 * Class m211102_123830_snt
 */
class m211102_123830_snt extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $userList = [
            [
                'name_first' => 'Алексей',
            ],
            [
                'name_first' => 'Сергей',
                'name_last'  => 'Паленов',
            ],
            [
                'name_first' => 'Алексей (солохаул)',
                'name_last'  => 'Барановский',
            ],
            [
                'name_first' => 'Сергей',
                'name_last'  => 'Ефимов',
            ],
            [
                'name_first' => 'Виктор',
                'name_last'  => 'Жеребцов',
            ],
            [
                'name_first' => 'Евгений',
                'name_last'  => 'Андрющенко',
            ],
            [
                'name_first' => 'Алкесандр',
                'name_last'  => 'Игнатенко',
            ],
            [
                'name_first' => 'Костя',
            ],
            [
                'name_first' => 'Дмитрий',
            ],
        ];

        $c = 1;

        foreach ($userList as $u) {
            $userRoot = \common\models\UserRoot::add([
                'email'         => 'user_' . $c . '@st_severnoe.ru',
                'avatar_status' => 2,
            ]);

            $user = \common\models\UserAvatar::add([
                'email'              => $userRoot->email,
                'password'           => \common\models\UserAvatar::hashPassword('123456'),
                'user_root_id'       => $userRoot->id,
                'registered_ad'      => time(),
                'auth_key'           => \common\services\Security::generateRandomString(60),
                'password_save_type' => \common\models\avatar\UserBill::PASSWORD_TYPE_OPEN_CRYPT,
                'name_first'         => $u['name_first'],
                'name_last'          => \yii\helpers\ArrayHelper::getValue($u, 'name_last'),
            ]);

            $c++;

            echo 'user id=' . $user->id . "\n";
        }
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m211102_123830_snt cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m211102_123830_snt cannot be reverted.\n";

        return false;
    }
    */
}
