<?php

use yii\db\Migration;

/**
 * Class m210629_193817_coin
 */
class m210629_193817_coin extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('school_coin', 'budjet_currency_id', 'int default null');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m210629_193817_coin cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210629_193817_coin cannot be reverted.\n";

        return false;
    }
    */
}
