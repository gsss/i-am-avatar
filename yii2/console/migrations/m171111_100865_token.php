
<?php

use common\models\avatar\Currency;
use yii\db\Migration;

class m171111_100865_token extends Migration
{


    public function safeUp()
    {
        $this->createIndex('gs_unions_shop_product_tree_node_id', 'gs_unions_shop_product', 'tree_node_id');
        $this->createIndex('gs_unions_shop_product_school_id', 'gs_unions_shop_product', 'school_id');
        $this->createIndex('gs_unions_shop_tree_school_id', 'gs_unions_shop_tree', 'school_id');
        $this->createIndex('gs_unions_shop_tree_parent_id', 'gs_unions_shop_tree', 'parent_id');
    }

    public function safeDown()
    {
        echo "m171111_100717_token cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m171111_100717_token cannot be reverted.\n";

        return false;
    }
    */
}
