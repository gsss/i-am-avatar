
<?php

use common\models\avatar\Currency;
use yii\db\Migration;

class m171111_100887_token extends Migration
{


    public function safeUp()
    {

        $this->createIndex('school_sms_subscribe_result_subscribe_id', 'school_sms_subscribe_result', 'subscribe_id');
        $this->createIndex('school_sms_subscribe_school_id', 'school_sms_subscribe', 'school_id');
        $this->createIndex('school_sms_subscribe_potok_id', 'school_sms_subscribe', 'potok_id');
    }

    public function safeDown()
    {
        echo "m171111_100717_token cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m171111_100717_token cannot be reverted.\n";

        return false;
    }
    */
}
