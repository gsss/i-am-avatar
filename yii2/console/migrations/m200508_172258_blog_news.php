<?php

use yii\db\Migration;

/**
 * Class m200508_172258_blog_news
 */
class m200508_172258_blog_news extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('school_blog_article', 'is_subscribe', 'tinyint default 0');
        $this->addColumn('school_news_article', 'is_subscribe', 'tinyint default 0');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m200508_172258_blog_news cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200508_172258_blog_news cannot be reverted.\n";

        return false;
    }
    */
}
