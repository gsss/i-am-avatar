
<?php

use common\models\avatar\Currency;
use yii\db\Migration;

class m171111_100880_token extends Migration
{


    public function safeUp()
    {
        $this->execute('alter table user_map modify lat bigint null;');
        $this->execute('alter table user_map modify lng bigint null;');
        $this->createIndex('user_map_user_id', 'user_map', 'user_id');
    }

    public function safeDown()
    {
        echo "m171111_100717_token cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m171111_100717_token cannot be reverted.\n";

        return false;
    }
    */
}
