<?php

use yii\db\Migration;

/**
 * Class m210602_120557_coin
 */
class m210602_120557_coin extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('school_task', 'wallet_type', 'tinyint default null');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m210602_120557_coin cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210602_120557_coin cannot be reverted.\n";

        return false;
    }
    */
}
