
<?php

use common\models\avatar\Currency;
use yii\db\Migration;

class m171111_100861_token extends Migration
{
    public function init()
    {
        parent::init();
        $module = \Yii::$app->getModule('sroutes');
        $this->db = $module->db;
    }

    public function safeUp()
    {
        $this->addColumn('product_ext_info','is_send_vvb', 'int default 0');
    }

    public function safeDown()
    {
        echo "m171111_100717_token cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m171111_100717_token cannot be reverted.\n";

        return false;
    }
    */
}
