<?php

use yii\db\Migration;

/**
 * Class m200509_143546_comment
 */
class m200509_143546_comment extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('comments', 'file', 'varchar(100) null');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m200509_143546_comment cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200509_143546_comment cannot be reverted.\n";

        return false;
    }
    */
}
