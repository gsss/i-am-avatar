<?php

use yii\db\Migration;

/**
 * Class m200509_190241_cloud
 */
class m200509_190241_cloud extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->execute('CREATE TABLE `school_cloud` (
    `id` int(11) NOT NULL AUTO_INCREMENT,
    school_id int null,
    url varchar(255) null,
    `key` varchar(32) null,
    is_active tinyint null,
    PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8');
        $this->createIndex('school_cloud_school_id', 'school_cloud', 'school_id');
        $this->createIndex('school_cloud_is_active', 'school_cloud', 'is_active');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m200509_190241_cloud cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200509_190241_cloud cannot be reverted.\n";

        return false;
    }
    */
}
