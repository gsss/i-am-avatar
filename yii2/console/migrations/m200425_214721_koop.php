<?php

use yii\db\Migration;

/**
 * Class m200425_214721_koop
 */
class m200425_214721_koop extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->execute('CREATE TABLE `user_enter` (
      `id` int(11) NOT NULL AUTO_INCREMENT,
    email               varchar(100)         null,
    iof                 varchar(150)         null,
    place               varchar(2000)        null,
    address             varchar(100)         null,
    phone               varchar(24)          null,
    whatsapp            varchar(24)          null,
    created_at          int                  null,
    code                varchar(32)          null,
    status              tinyint(3) default 0 null,
    user_id             int                  null,
    passport_ser        varchar(4)           null,
    passport_number     int                  null,
    passport_vidan_kem  varchar(100)         null,
    passport_vidan_date date                 null,
    passport_vidan_num  varchar(7)           null,
    passport_born_date  date                 null,
    passport_born_place varchar(20)          null,
    place_type_id       tinyint(3)           null,
    name_first          varchar(100)         null,
    name_last           varchar(100)         null,
    name_middle         varchar(100)         null,
    address_pochta      varchar(255)         null,
    address_gruz        varchar(255)         null,
    PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m200425_214721_koop cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200425_214721_koop cannot be reverted.\n";

        return false;
    }
    */
}
