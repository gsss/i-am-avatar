<?php

use yii\db\Migration;

/**
 * Class m200525_235604_doc
 */
class m200525_235604_doc extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('user_documents_signatures', 'signature', 'varchar(160) null');
        $this->addColumn('user_documents_signatures', 'user_id', 'int null');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m200525_235604_doc cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200525_235604_doc cannot be reverted.\n";

        return false;
    }
    */
}
