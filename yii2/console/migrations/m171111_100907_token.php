
<?php

use common\models\avatar\Currency;
use cs\services\Str;
use yii\db\Migration;

class m171111_100907_token extends Migration
{


    public function safeUp()
    {

        foreach (\common\models\BlogItem::find()->all() as $b) {
            $i = new \common\models\blog\Article([
                'school_id'     => 2,
                'created_at'    => $b['created_at'],
                'date'          => date('Y-m-d', $b['created_at']),
                'id_string'     => Str::rus2translit($b['name']),
                'content'       => $b['content'],
                'name'          => $b['name'],
                'image'         => $b['image'],
                'description'   => $b['description'],
                'views_counter' => $b['views_counter'],
                'link'          => $b['link'],
            ]);
            $i->save();
            echo 'id='.$b['id']. "\n";
        }
        foreach (\common\models\NewsItem::find()->all() as $b) {
            $i = new \common\models\school\NewsItem([
                'school_id'     => 2,
                'created_at'    => $b['created_at'],
                'date'          => date('Y-m-d', $b['created_at']),
                'id_string'     => Str::rus2translit($b['name']),
                'content'       => $b['content'],
                'name'          => $b['name'],
                'image'         => $b['image'],
                'views_counter' => $b['views_counter'],
            ]);
            $i->save();
            echo 'id='.$b['id']. "\n";
        }
    }

    public function safeDown()
    {
        echo "m171111_100717_token cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m171111_100717_token cannot be reverted.\n";

        return false;
    }
    */
}
