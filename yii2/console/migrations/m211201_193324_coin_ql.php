<?php

use yii\db\Migration;

/**
 * Class m211201_193324_coin_ql
 */
class m211201_193324_coin_ql extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $c = \common\models\piramida\Currency::add([
            'name'     => 'Счетчик оплаты сообщества',
            'code'     => 'QL',
            'decimals' => 2,
        ]);
        $this->addColumn('school', 'pay_wallet_id', 'int null');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m211201_193324_coin_ql cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m211201_193324_coin_ql cannot be reverted.\n";

        return false;
    }
    */
}
