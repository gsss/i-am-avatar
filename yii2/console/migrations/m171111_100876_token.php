
<?php

use common\models\avatar\Currency;
use yii\db\Migration;

class m171111_100876_token extends Migration
{


    public function safeUp()
    {
        $this->dropColumn('user_documents', 'document_id');
        $this->addColumn('school_blog_article', 'document_id', 'int default null');
    }

    public function safeDown()
    {
        echo "m171111_100717_token cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m171111_100717_token cannot be reverted.\n";

        return false;
    }
    */
}
