<?php

use yii\db\Migration;

/**
 * Class m210709_125605_vote
 */
class m210709_125605_vote extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {

        $this->execute('CREATE TABLE `user_digital_sign_personal_data` (
    `id` int(11) NOT NULL AUTO_INCREMENT,
	user_id         int null,
	created_at      int null,
	address         varchar(100) null,
	sign            varchar(200) null,
	name_first      varchar(100) null,
	name_last       varchar(100) null,
	avatar          varchar(200) null,
	hash            varchar(100) null,
    PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8');

        $this->createIndex('user_digital_sign_personal_data_user_id', 'user_digital_sign_personal_data', 'user_id');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m210709_125605_vote cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210709_125605_vote cannot be reverted.\n";

        return false;
    }
    */
}
