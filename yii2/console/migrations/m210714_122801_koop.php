<?php

use yii\db\Migration;

/**
 * Class m210714_122801_koop
 */
class m210714_122801_koop extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->execute('CREATE TABLE `koop_podryad` (
    `id` int(11) NOT NULL AUTO_INCREMENT,
	school_id         int null,
	created_at        int null,
	status            int null,
	dogovor_podryada  varchar(200) null,
	akt_pp_sirya      varchar(200) null,
	akt_pp_prod       varchar(200) null,
	report            varchar(200) null,
	tz                varchar(200) null,
	protokol          varchar(200) null,
    PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8');

        $this->execute('CREATE TABLE `koop_podryad_command_user` (
    `id` int(11) NOT NULL AUTO_INCREMENT,
	podryad_id      int null,
	user_id         int null,
    PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8');

        $this->createIndex('koop_podryad_school_id', 'koop_podryad', 'school_id');
        $this->createIndex('koop_podryad_command_user_podryad_id', 'koop_podryad_command_user', 'podryad_id');

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m210714_122801_koop cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210714_122801_koop cannot be reverted.\n";

        return false;
    }
    */
}
