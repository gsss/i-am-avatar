<?php

use yii\db\Migration;

/**
 * Class m210405_152944_invest
 */
class m210405_152944_invest extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->execute('CREATE TABLE `school_invest_program` (
    `id` int(11) NOT NULL AUTO_INCREMENT,
	name varchar(100) null,
	document varchar(200) null,
	created_at int null,
    PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8');

        $this->execute('CREATE TABLE `school_invest_event` (
    `id` int(11) NOT NULL AUTO_INCREMENT,
	document varchar(200) null,
	program_id int null,
	amount int null,
	currency_id int null,
	created_at int null,
	user_id int null,
	billing_id int null,
    PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8');

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m210405_152944_invest cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210405_152944_invest cannot be reverted.\n";

        return false;
    }
    */
}
