<?php

use yii\db\Migration;

/**
 * Class m200515_234143_card
 */
class m200515_234143_card extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m200515_234143_card cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200515_234143_card cannot be reverted.\n";

        return false;
    }
    */
}
