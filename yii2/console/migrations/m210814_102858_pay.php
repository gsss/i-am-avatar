<?php

use yii\db\Migration;

/**
 * Class m210814_102858_pay
 */
class m210814_102858_pay extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        // \common\models\PaymentPayKeeper
        $this->execute('CREATE TABLE `payments_pay_keeper` (
    `id` int(11) NOT NULL AUTO_INCREMENT,
	billing_id  int          null,
    payment_id  varchar(100) null,
    currency_id int          null,
    amount      int          null,
    created_at  int          null,
    form_url    varchar(255) null,
    PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8');

        $this->createIndex('payments_pay_keeper_billing_id', 'payments_pay_keeper', 'billing_id');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m210814_102858_pay cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210814_102858_pay cannot be reverted.\n";

        return false;
    }
    */
}
