<?php

use common\models\avatar\Currency;
use yii\db\Migration;

class m171111_100843_token extends Migration
{

    public function init()
    {
        $this->db = 'dbWallet';
        parent::init();
    }

    public function safeUp()
    {
        $this->addColumn('operations', 'hash', 'varchar(64) default null');
    }

    public function safeDown()
    {
        echo "m171111_100717_token cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m171111_100717_token cannot be reverted.\n";

        return false;
    }
    */
}
