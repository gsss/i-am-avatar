<?php

use yii\db\Migration;

/**
 * Class m200814_175613_shop
 */
class m200814_175613_shop extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('school_settings', 'shop_currency_id', 'int null');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m200814_175613_shop cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200814_175613_shop cannot be reverted.\n";

        return false;
    }
    */
}
