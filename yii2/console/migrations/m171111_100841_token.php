<?php

use common\models\avatar\Currency;
use yii\db\Migration;

class m171111_100841_token extends Migration
{

    public function safeUp()
    {
        $this->execute('CREATE TABLE `school_design` (
      `id` int(11) NOT NULL AUTO_INCREMENT,
      `school_id` int(11) DEFAULT NULL,
      `logo_top_menu` VARCHAR(255) DEFAULT NULL,
      `video_intro` VARCHAR(255) DEFAULT NULL,
      PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8');
    }

    public function safeDown()
    {
        echo "m171111_100717_token cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m171111_100717_token cannot be reverted.\n";

        return false;
    }
    */
}
