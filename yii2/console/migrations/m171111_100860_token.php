
<?php

use common\models\avatar\Currency;
use yii\db\Migration;

class m171111_100860_token extends Migration
{
    public function init()
    {
        parent::init();
        $module = \Yii::$app->getModule('sroutes');
        $this->db = $module->db;
    }

    public function safeUp()
    {
        $this->execute('CREATE TABLE `oferta` (
      `id` int(11) NOT NULL AUTO_INCREMENT,
	name VARCHAR(255) null,
	content text null,
	created_at int null,
    PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8');
    }

    public function safeDown()
    {
        echo "m171111_100717_token cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m171111_100717_token cannot be reverted.\n";

        return false;
    }
    */
}
