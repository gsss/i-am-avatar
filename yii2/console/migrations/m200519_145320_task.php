<?php

use yii\db\Migration;

/**
 * Class m200519_145320_task
 */
class m200519_145320_task extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('school_task_status', 'type_id', 'tinyint null');
        $this->execute('UPDATE school_task_status set type_id=sort_index');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m200519_145320_task cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200519_145320_task cannot be reverted.\n";

        return false;
    }
    */
}
