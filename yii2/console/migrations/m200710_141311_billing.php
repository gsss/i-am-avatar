<?php

use yii\db\Migration;

/**
 * Class m200710_141311_billing
 */
class m200710_141311_billing extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $i = \common\models\BillingMainClass::findOne(['name' => '\\avatar\\models\\UserEnter']);
        if (is_null($i)) {
            \common\models\BillingMainClass::add(['name' => '\\avatar\\models\\UserEnter']);
            echo 'added'. "\n";
        }
        $this->addColumn('billing', 'description', 'varchar(255) null');
        $this->addColumn('billing', 'destination', 'varchar(255) null');
        $this->addColumn('billing', 'successUrl', 'varchar(255) null');
        $this->addColumn('billing', 'failUrl', 'varchar(255) null');
        $this->addColumn('billing', 'action', 'varchar(64) null');
        $this->addColumn('user_enter', 'is_paid_client', 'tinyint default 0');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m200710_141311_billing cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200710_141311_billing cannot be reverted.\n";

        return false;
    }
    */
}
