<?php

use yii\db\Migration;

/**
 * Class m201127_060550_wallets
 */
class m201127_060550_wallets extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->execute('CREATE TABLE `school_koop_wallet` (
    `id` int(11) NOT NULL AUTO_INCREMENT,
    school_id       int    null,
    currency_id     int    null,
    name            varchar(100) null,
    wallet_id       varchar(100) null,
    type_id         int    null,
    PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8');

        $this->createIndex('school_koop_wallet_school_id', 'school_koop_wallet', 'school_id');
        $this->createIndex('school_koop_wallet_currency_id', 'school_koop_wallet', 'currency_id');

        $data = [
            'ПФ'   => 1,
            'РОХД' => 2,
            'СФ'   => 3,
            'РФ'   => 4,
        ];
        $school_id = 50;
        $currency_id = \common\models\piramida\Currency::RUB;
        foreach ($data as $name => $type_id) {
            $wallet =\common\models\piramida\Wallet::addNew(['currency_id' => $currency_id]);
            \common\models\SchoolKoopWallet::add([
                'wallet_id'   => $wallet->id,
                'type_id'     => $type_id,
                'school_id'   => $school_id,
                'currency_id' => $currency_id,
                'name'        => $name,
            ]);
        }

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m201127_060550_wallets cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m201127_060550_wallets cannot be reverted.\n";

        return false;
    }
    */
}
