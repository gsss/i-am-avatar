<?php

use yii\db\Migration;

/**
 * Class m200429_004902_verify
 */
class m200429_004902_verify extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->execute('CREATE TABLE `koop_user_verification` (
      `id` int(11) NOT NULL AUTO_INCREMENT,
    school_id          int                  null,
    created_at          int                  null,
    user1_id          int                  null,
    user2_id          int                  null,
       public_key1               varchar(100)         null,
       public_key2               varchar(100)         null,
       hash               varchar(100)         null,
       signature1               varchar(100)         null,
       signature2               varchar(100)         null,
       file               varchar(1000)         null,

    PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8');
        $this->createIndex('koop_user_verification_school_id', 'koop_user_verification', 'school_id');
        $this->createIndex('koop_user_verification_created_at', 'koop_user_verification', 'created_at');
        $this->createIndex('koop_user_verification_user1_id', 'koop_user_verification', 'user1_id');
        $this->createIndex('koop_user_verification_user2_id', 'koop_user_verification', 'user2_id');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m200429_004902_verify cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200429_004902_verify cannot be reverted.\n";

        return false;
    }
    */
}
