<?php

use yii\db\Migration;

/**
 * Class m210630_134023_currency_int
 */
class m210630_134023_currency_int extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('school_task', 'operation_sub', 'int null');
        $this->addColumn('school_task', 'operation_add', 'int null');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m210630_134023_currency_int cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210630_134023_currency_int cannot be reverted.\n";

        return false;
    }
    */
}
