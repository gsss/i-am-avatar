
<?php

use common\models\avatar\Currency;
use yii\db\Migration;

class m171111_100871_token extends Migration
{


    public function safeUp()
    {
        $this->execute('CREATE TABLE `school_event` (
      `id` int(11) NOT NULL AUTO_INCREMENT,
	school_id int null,
	potok_id int null,
	created_at int null,
	config text null,
    PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8');
    }

    public function safeDown()
    {
        echo "m171111_100717_token cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m171111_100717_token cannot be reverted.\n";

        return false;
    }
    */
}
