<?php

use yii\db\Migration;

/**
 * Class m211102_131630_snt
 */
class m211102_131630_snt extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('school_snt_out', 'name', 'varchar(255) null');
        $this->addColumn('school_snt_out', 'date', 'date null');

        $rows = [
            ['ПНД труба 25*2 SDR 13.6 * 400 м','Саша',7,7,2017,11520],
            ['ПНД труба 25*2 SDR 13.6 * 600 м','Саша',26,7,2017,18640],
            ['Андрей ЗП 200 м','Саша',1,8,2017,500],
            ['Андрей ЗП 200 м','Саша',1,8,2017,500],
            ['Бензин Солох Аул','Сергей Паленов',8,6,2018,459],
            ['Бензин Солох Аул','Саша',8,6,2018,459],
            ['Бензин Газ 66','Саша',16,6,2018,425],
            ['Труба пп32 + муфта с резьбой','Саша',22,6,2018,425],
            ['NIVA бензин','Саша',11,7,2018,1210],
            ['Магнит','Саша',11,7,2018,184],
            ['цемент 1 мешок','Сергей Паленов',11,7,2018,320],
            ['гпс 6 мешков','Сергей Паленов',11,7,2018,720],
            ['Бензин Солох Аул','Сергей Паленов',21,7,2018,459],
            ['вернерусское на перевозку труб лоо краснодар сочи','АЛЕКСЕЙ Барановский (солохаул)',21,7,2018,10000],
            ['РЕНО БЕНЗИН сочи краснодар','Саша',23,7,2018,1510],
            ['РЕНО БЕНЗИН сочи краснодар','Саша',23,8,2018,1210],
            ['еда краснодар','Саша',23,7,2018,640],
            ['еда краснодар','Саша',23,7,2018,176],
            ['труба из краснодара','Саша',23,7,2018,224730],
            ['переозка кр-сочи','Саша',23,7,2018,16000],
            ['Бензин Солох Аул рено','Саша',28,7,2018,500],
            ['Бензин Газ 66 Солох Аул','Саша',28,7,2018,850],
            ['Магнит еда Солох Аул','Саша',28,7,2018,245],
            ['бензин газ 66 + нива, Сергей Саша Борис','Саша',8,8,2018,1510],
            ['Цемент пол мешка 320:2','Сергей Паленов',12,8,2018,160],
            ['гпс 3 мешка 120*3','Сергей Паленов',12,8,2018,360],
            ['переходник 3/4 на 25пнд','Саша',13,8,2018,40],
            ['бензин нива + газ 66 Сергей Саша','Саша',15,8,2018,1510],
            ['Обед магнит','Саша',15,8,2018,323],
            ['гпс 2 мешка','Сергей Паленов',15,8,2018,240],
            ['бензин нива + газ 66','Саша',17,8,2018,1510],
            ['Обед магнит','Саша',17,8,2018,461],
            ['гпс 4 мешка ','Сергей Паленов',17,8,2018,480],
            ['бензин газ66','Саша',18,8,2018,808],
            ['бензин рено','Сергей Паленов',18,8,2018,500],
            ['Обед магнит','Саша',18,8,2018,174],
            ['Обед магнит','Сергей Паленов',18,8,2018,49],
            ['бензин газ66','Саша',21,8,2018,850],
            ['бензин рено','Сергей Паленов',21,8,2018,500],
            ['Обед магнит','Саша',21,8,2018,251],
            ['бензин нива + газ 66 Руслан, Сергей Саша','Саша',23,8,2018,1510],
            ['бензин нива + газ 66 Руслан, Сергей Саша','Саша',26,8,2018,1510],
            ['переходник с Ж на ПНД','Саша',26,8,2018,40],
            ['Сгони железа','Саша',26,8,2018,100],
            ['Обед магнит','Саша',26,8,2018,166],
            ['шайбы и прокладки','Саша',26,8,2018,88],
            ['бензин нива + газ 66 Руслан, Сергей Саша','Саша',28,8,2018,1510],
            ['ПГС 5 мешков','Сергей Паленов',29,8,2018,600],
            ['бензин нива','Саша',31,8,2018,1010],
            ['ПГС 5 мешков','Сергей Паленов',31,8,2018,600],
            ['бензин рено','Саша',28,9,2018,459],
            ['бензин нива + газ66','Саша',6,10,2018,1710],
            ['бензин нива + газ66','Саша',7,10,2018,1275],
            ['Фирма конси','Саша',7,10,2018,1015],
            ['бензин нива +газ 66','Саша',13,10,2018,1200],
            ['Трубка + Сгои','Саша',13,10,2018,100],
            ['бензин газ66','Саша',14,10,2018,815],
            ['бензин рено','Сергей Паленов',14,10,2018,459],
            ['герметик+пакля','Саша',14,10,2018,565],
            ['бензин газ66','Саша',17,10,2018,815],
            ['бензин рено','Сергей Паленов',17,10,2018,459],
            ['бензин рено','Саша',20,10,2018,500],
            ['обезжириватель','Саша',20,10,2018,42],
            ['кофе+рулет','Саша',20,10,2018,228],
            ['бензин газ66+нива','Саша',4,12,2018,2135],
            ['муфта пнд32','Саша',17,5,2019,240],
            ['цемент 1 мешок','Сергей Паленов',15,7,2019,320],
            ['муфта американка','Сергей Паленов',15,7,2019,225],
            ['муфта разьемная сетка','Сергей Паленов',15,7,2019,40],
            ['песок 2 мешка','Сергей Паленов',15,7,2019,200],
            ['муфта пнф','Сергей Паленов',15,7,2019,40],
            ['подьем 2 раза','Сергей Паленов',15,7,2019,2000],
            ['бензин газ66','Сергей Паленов',15,7,2019,216],
            ['бензин газ66','Сергей Паленов',15,7,2019,433],
            ['бензин газ66','Сергей Паленов',15,7,2019,867],
            ['бензин рено','Сергей Паленов',15,7,2019,500],
            ['бензин ШНива','Саша',15,7,2019,1000],
            ['подьем','Ефимов Сергей',23,7,2019,1000],
            ['Бензин газ66','Ефимов Сергей',23,7,2019,1000],
            ['Петля, саморез','Сергей Паленов',30,7,2019,111],
            ['подъем+газ66','Ефимов Сергей',30,7,2019,1500],
            ['бензин газ66','Сергей Паленов',30,7,2019,500],
            ['бензин рено','Сергей Паленов',30,7,2019,500],
            ['Цемент','Ефимов Сергей',30,7,2019,320],
            ['песок','Ефимов Сергей',30,7,2019,350],
            ['бензин газ66','Ефимов Сергей',6,8,2019,2000],
            ['электроды саморезы','Ефимов Сергей',7,8,2019,530],
            ['песок 250кг','Ефимов Сергей',7,8,2019,300],
            ['цемент 2 мешка','Ефимов Сергей',7,8,2019,660],
            ['бензин газ66','Ефимов Сергей',7,8,2019,870],
            ['бензин рено','Сергей Паленов',7,8,2019,500],
            ['бензин ШНива','Саша',7,8,2019,500],
            ['бензин ШНива','Саша',4,10,2019,500],
            ['Оплата машина газ66 охотники','Саша',7,10,2019,15000],
            ['Муфта','Саша',13,10,2019,460],
            ['','Саша',13,10,2019,320],
            ['бензин ШНива','Саша',13,10,2019,500],
            ['за протяжку 10 бухт охотники','Саша',16,10,2019,30000],
            ['Муфты охотники','Саша',21,10,2019,160],
            ['За протяжку 3 бухт + бенз охотники','Саша',9,12,2019,11000],
            ['За протяжку бухты + бенз охотники','Саша',22,10,2019,12000],
            ['Труба','Ефимов Сергей',6,7,2020,6000],
            ['муфта','Саша',6,7,2020,8616],
            ['Труба','Саша',31,7,2020,2880],
            ['Труба','Виктор Жеребцов',2,8,2020,3000],
            ['Муфта','Саша',2,8,2020,2847],
            ['Труба','Саша',9,8,2020,3010],
            ['Муфта','Саша',9,8,2020,2967],
            ['Бензин','Саша',7,8,2020,1000],
            ['Муфта 5шт + труба 500 м','Ефимов Сергей',29,8,2020,10000],
            ['Муфта 5шт + труба 500 м','Виктор Жеребцов',29,8,2020,3000],
            ['Муфта 5шт + труба 500 м','Саша',29,8,2020,1616],
            ['бензин','Саша',29,8,2020,500],
            ['труба + кран','Виктор Жеребцов',4,9,2020,3137],
            ['кран','Саша',4,9,2020,257],
            ['бензин','Сергей Паленов',4,9,2020,500],
            ['труба','Святослав',8,10,2020,3000],
            ['бенз','Саша',24,10,2020,500],
            ['трубы 500 м + фитинги 5 шт','Алексей',28,10,2020,11592],
            ['трубы 500 м + фитинги 5 шт','Алексей',28,10,2020,8408],
            ['трубы 500 м + фитинги 5 шт','Алексей',9,11,2020,30000],
            ['За прокладку','Святослав',13,11,2020,20000],
            ['на бак','Виктор Жеребцов',16,11,2020,4000],
            ['бак и 2 бухты','Ефимов Сергей',14,11,2020,2870],
            ['Муфты','Сергей Паленов',29,6,2021,100],
            ['Машина Рено бензин','Сергей Паленов',29,6,2021,500],
            ['Машина в Тихую Заводь','Алексей',2,7,2021,500],
            ['трубы','Святослав',1,11,2020,30000],
            ['Покупка труб','Евгений Андрющенко',30,12,1899,4000],
            ['Машина Daihatsu','Дмитрий',12,10,2021,500],
            ['Вклад','Ефимов Сергей',30,12,1899,22600],
            ['Муфты','Костя',24,10,2021,585],
            ['Машина в Тихую Заводь','Алексей',24,10,2021,500],
        ];
        $school_id = 98;

        $users = [
            9    => 'Святослав',
            4620 => 'Алексей',
            4621 => 'Сергей Паленов',
            4622 => 'АЛЕКСЕЙ Барановский (солохаул)',
            4623 => 'Ефимов Сергей',
            4624 => 'Виктор Жеребцов',
            4625 => 'Евгений Андрющенко',
            4626 => 'Саша',
            4627 => 'Костя',
            4628 => 'Дмитрий',
            4629 => 'Скоробогатов',
        ];

        foreach ($rows as $row) {
            $i = \common\models\SchoolSntOut::add([
                'school_id'   => $school_id,
                'amount'      => $row[5] * 100,
                'user_id'     => $this->get($users, $row[1]),
                'currency_id' => 7,
                'date'        => $this->getDate($row),
                'name'        => $row[0],
            ]);
            echo 'id=' . $i->id . "\n";
        }
    }

    private function get($userList, $name)
    {
        foreach ($userList as $k => $n)
        {
            if ($n == $name) return $k;
        }

        return null;
    }

    private function getDate($row)
    {
        if ($row[4] == 1899) return null;
        $d =    $row[2];
        $m =    $row[3];
        $y =    $row[4];
        if ($d < 10) $d = '0' . $d;
        if ($m < 10) $m = '0' . $m;

        return $y . '-' . $m . '-' . $d ;
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m211102_131630_snt cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m211102_131630_snt cannot be reverted.\n";

        return false;
    }
    */
}
