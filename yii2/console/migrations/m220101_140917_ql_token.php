<?php

use yii\db\Migration;

/**
 * Class m220101_140917_ql_token
 */
class m220101_140917_ql_token extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $c = \common\models\avatar\Currency::add([
            'code'     => 'QL',
            'title'    => 'QualityLive',
            'decimals' => 2,
        ]);
        $Int = \common\models\piramida\Currency::findOne(['code' => 'QL']);
        \common\models\avatar\CurrencyLink::add([
            'currency_ext_id' => $c->id,
            'currency_int_id' => $Int->id,
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m220101_140917_ql_token cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m220101_140917_ql_token cannot be reverted.\n";

        return false;
    }
    */
}
