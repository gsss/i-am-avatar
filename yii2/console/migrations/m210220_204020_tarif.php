<?php

use yii\db\Migration;

/**
 * Class m210220_204020_tarif
 */
class m210220_204020_tarif extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->execute('CREATE TABLE `tarif` (
    `id` int(11) NOT NULL AUTO_INCREMENT,
	name varchar(100) null,
	description text null,
	price bigint null,
	currency_id int null,
    PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8');

        $this->execute('CREATE TABLE `tarif_service` (
    `id` int(11) NOT NULL AUTO_INCREMENT,
	tarif_id int null,
	company_id int null,
	paid_till date null,
    PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8');

        $this->execute('CREATE TABLE `tarif_request` (
    `id` int(11) NOT NULL AUTO_INCREMENT,
	tarif_id int null,
	company_id int null,
	count int null,
	price bigint null,
	currency_id int null,
	billing_id int null,
	created_at int null,
    PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8');

        $this->createIndex('tarif_service_company_id', 'tarif_service', 'company_id');
        $this->createIndex('tarif_service_tarif_id', 'tarif_service', 'tarif_id');

        $this->createIndex('tarif_request_tarif_id', 'tarif_request', 'tarif_id');
        $this->createIndex('tarif_request_company_id', 'tarif_request', 'company_id');
        $this->createIndex('tarif_request_billing_id', 'tarif_request', 'billing_id');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m210220_204020_tarif cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210220_204020_tarif cannot be reverted.\n";

        return false;
    }
    */
}
