
<?php

use common\models\avatar\Currency;
use yii\db\Migration;

class m171111_100864_token extends Migration
{


    public function safeUp()
    {
        $this->execute('ALTER TABLE gs_unions_shop_tree CHANGE union_id school_id INT(11);');
        $this->execute('ALTER TABLE gs_unions_shop_tree CHANGE date_insert created_at INT(11);');
        $this->dropColumn('gs_unions_shop_tree', 'shop_id');
    }

    public function safeDown()
    {
        echo "m171111_100717_token cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m171111_100717_token cannot be reverted.\n";

        return false;
    }
    */
}
