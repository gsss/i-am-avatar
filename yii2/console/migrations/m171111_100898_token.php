
<?php

use common\models\avatar\Currency;
use yii\db\Migration;

class m171111_100898_token extends Migration
{


    public function safeUp()
    {
        $this->createIndex('school_urok_kurs_id', 'school_urok', 'kurs_id');
        $this->dropColumn('school_urok_dz_request', 'is_stop');
        $this->addColumn('school_urok', 'is_stop', 'tinyint default 0');
        $this->createIndex('school_urok_dz_response_lesson_state_id', 'school_urok_dz_response', 'lesson_state_id');
        $this->createIndex('school_urok_dz_rate_lesson_state_id', 'school_urok_dz_rate', 'lesson_state_id');
        $this->createIndex('school_kurs_lesson_kurs_id', 'school_kurs_lesson', 'kurs_id');
    }

    public function safeDown()
    {
        echo "m171111_100717_token cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m171111_100717_token cannot be reverted.\n";

        return false;
    }
    */
}
