<?php

use yii\db\Migration;

/**
 * Class m210820_115344_pay
 */
class m210820_115344_pay extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->execute('CREATE TABLE `payments_cash` (
    `id` int(11) NOT NULL AUTO_INCREMENT,
	billing_id  int          null,
    currency_id int          null,
    amount      int          null,
    created_at  int          null,
    data        text         null,
    PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8');

        $this->createIndex('payments_cash_billing_id', 'payments_cash', 'billing_id');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m210820_115344_pay cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210820_115344_pay cannot be reverted.\n";

        return false;
    }
    */
}
