<?php

use yii\db\Migration;

/**
 * Class m210714_093547_money_income
 */
class m210714_093547_money_income extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->execute('CREATE TABLE `school_money_income` (
    `id` int(11) NOT NULL AUTO_INCREMENT,
	user_id         int null,
	created_at      int null,
	amount          int null,
	currency_id     int null,
	date            date null,
	description     varchar(2000) null,
	image           varchar(200) null,
    PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8');

        $this->createIndex('school_money_income_user_id', 'school_money_income', 'user_id');
        $this->createIndex('school_money_income_created_at', 'school_money_income', 'created_at');

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m210714_093547_money_income cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210714_093547_money_income cannot be reverted.\n";

        return false;
    }
    */
}
