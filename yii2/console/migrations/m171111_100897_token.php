
<?php

use common\models\avatar\Currency;
use yii\db\Migration;

class m171111_100897_token extends Migration
{


    public function safeUp()
    {
        $this->execute('CREATE TABLE `school_urok_potok_state` (
      `id` int(11) NOT NULL AUTO_INCREMENT,
	lesson_id int null,
	potok_id int null,
	status int null,
	is_hide int null,
    PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8');
        $this->createIndex('school_urok_potok_state_lesson_id', 'school_urok_potok_state', 'lesson_id');
        $this->createIndex('school_urok_potok_state_potok_id', 'school_urok_potok_state', 'potok_id');

//        $this->execute('CREATE TABLE `school_urok_dz_request` (
//      `id` int(11) NOT NULL AUTO_INCREMENT,
//	lesson_id int null,
//	content text null,
//    PRIMARY KEY (`id`)
//) ENGINE=InnoDB DEFAULT CHARSET=utf8');

        $this->execute('CREATE TABLE `school_urok_dz_rate` (
      `id` int(11) NOT NULL AUTO_INCREMENT,
	lesson_state_id int null,
	user_id int null,
	ocenka int null,
	created_at int null,
    PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8');

        $this->execute('CREATE TABLE `school_urok_dz_response` (
      `id` int(11) NOT NULL AUTO_INCREMENT,
	lesson_state_id int null,
	user_id int null,
	content text null,
	created_at int null,
    PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8');
    }

    public function safeDown()
    {
        echo "m171111_100717_token cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m171111_100717_token cannot be reverted.\n";

        return false;
    }
    */
}
