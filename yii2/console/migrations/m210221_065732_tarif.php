<?php

use yii\db\Migration;

/**
 * Class m210221_065732_tarif
 */
class m210221_065732_tarif extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $c = \common\models\avatar\Currency::findOne(['code' => 'RUB']);

        \common\models\Tarif::add([
            'name'        => 'Задачник',
            'description' => 'Задачник',
            'price'       => 100,
            'currency_id' => $c->id,
        ]);

        \common\models\Tarif::add([
            'name'        => 'Краудфандинг',
            'description' => 'Сервис по сбору средств на проект',
            'price'       => 100,
            'currency_id' => $c->id,
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m210221_065732_tarif cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210221_065732_tarif cannot be reverted.\n";

        return false;
    }
    */
}
