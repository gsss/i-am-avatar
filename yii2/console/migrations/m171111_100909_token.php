
<?php

use common\models\avatar\Currency;
use yii\db\Migration;

class m171111_100909_token extends Migration
{


    public function safeUp()
    {
        $ps = new \common\models\PaySystem([
            'image'      => 'https://cloud1.cloud999.ru/upload/cloud/15870/54662_Cm4ycUZiED.png',
            'code'       => 'guslitsa-pay',
            'title'      => 'ГусликиПей',
            'class_name' => 'GuslitsaPay',
            'currency'   => 'GUS',
        ]);
        $ps->save();

        echo('$ps->id = '. $ps::getDb()->lastInsertID);
    }

    public function safeDown()
    {
        echo "m171111_100717_token cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m171111_100717_token cannot be reverted.\n";

        return false;
    }
    */
}
