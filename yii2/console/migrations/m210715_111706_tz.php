<?php

use yii\db\Migration;

/**
 * Class m210715_111706_tz
 */
class m210715_111706_tz extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('koop_podryad', 'tz_hash', 'varchar(100) null');
        $this->addColumn('koop_podryad', 'protokol_hash', 'varchar(100) null');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m210715_111706_tz cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210715_111706_tz cannot be reverted.\n";

        return false;
    }
    */
}
