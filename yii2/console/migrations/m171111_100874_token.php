
<?php

use common\models\avatar\Currency;
use yii\db\Migration;

class m171111_100874_token extends Migration
{


    public function safeUp()
    {
        $this->execute('CREATE TABLE `school_potok_hash` (
      `id` int(11) NOT NULL AUTO_INCREMENT,
	potok_id int null,
	hash VARCHAR(100) null,
    PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8');
    }

    public function safeDown()
    {
        echo "m171111_100717_token cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m171111_100717_token cannot be reverted.\n";

        return false;
    }
    */
}
