<?php

use yii\db\Migration;

/**
 * Class m210602_111450_coins
 */
class m210602_111450_coins extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->execute('CREATE TABLE `school_coin` (
    `id` int(11) NOT NULL AUTO_INCREMENT,
	currency_id int null,
	school_id int null,
    PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8');

        $this->createIndex('school_coin_school_id', 'school_coin', 'school_id');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m210602_111450_coins cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210602_111450_coins cannot be reverted.\n";

        return false;
    }
    */
}
