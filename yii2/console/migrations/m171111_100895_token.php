
<?php

use common\models\avatar\Currency;
use yii\db\Migration;

class m171111_100895_token extends Migration
{


    public function safeUp()
    {
        $this->execute('CREATE TABLE `school_urok_dz_request` (
      `id` int(11) NOT NULL AUTO_INCREMENT,
	lesson_id int null,
	`content` text null,
	is_stop int default 0,
    PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8');
        $this->createIndex('school_urok_dz_request_lesson_id', 'school_urok_dz_request', 'lesson_id');

    }

    public function safeDown()
    {
        echo "m171111_100717_token cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m171111_100717_token cannot be reverted.\n";

        return false;
    }
    */
}
