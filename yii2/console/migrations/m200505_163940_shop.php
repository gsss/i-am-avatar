<?php

use yii\db\Migration;

/**
 * Class m200505_163940_shop
 */
class m200505_163940_shop extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('gs_unions_shop_tree', 'pay_config_id', 'int null');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m200505_163940_shop cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200505_163940_shop cannot be reverted.\n";

        return false;
    }
    */
}
