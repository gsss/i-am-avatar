<?php

use yii\db\Migration;

/**
 * Class m210719_095819_protokol
 */
class m210719_095819_protokol extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('koop_podryad_command_user', 'protokol_address', 'varchar(100) null');
        $this->addColumn('koop_podryad_command_user', 'protokol_sign', 'varchar(100) null');
        $this->addColumn('koop_podryad_command_user', 'protokol_created_at', 'int null');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m210719_095819_protokol cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210719_095819_protokol cannot be reverted.\n";

        return false;
    }
    */
}
