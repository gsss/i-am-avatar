<?php

use yii\db\Migration;

/**
 * Class m201203_051241_blago
 */
class m201203_051241_blago extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->execute('CREATE TABLE `school_blago_input` (
    `id` int(11) NOT NULL AUTO_INCREMENT,
    program_id  int    null,
    amount      int null,
    currency_id  int null,
    billing_id  int null,
    created_at  int null,
    PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8');
        $this->createIndex('school_blago_input_program_id', 'school_blago_input', 'program_id');

        \common\models\BillingMainClass::add(['name' => '\common\models\SchoolBlagoInput']);

        $this->addColumn('school_blago_program', 'success_url', 'varchar(200) null');
        $this->addColumn('school_blago_program', 'error_url', 'varchar(200) null');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m201203_051241_blago cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m201203_051241_blago cannot be reverted.\n";

        return false;
    }
    */
}
