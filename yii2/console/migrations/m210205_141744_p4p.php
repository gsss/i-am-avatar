<?php

use yii\db\Migration;

/**
 * Class m210205_141744_p4p
 */
class m210205_141744_p4p extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $w = \common\models\piramida\Wallet::addNew(['currency_id' => 15]);
        $w->in(1000000000, 'Первая эмиссия (тест)');
        $s = \common\models\school\School::findOne(84);
        $s->wallet_id = $w->id;
        $s->currency_id = 15;
        $s->save();
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m210205_141744_p4p cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210205_141744_p4p cannot be reverted.\n";

        return false;
    }
    */
}
