
<?php

use common\models\avatar\Currency;
use yii\db\Migration;

class m171111_100878_token extends Migration
{


    public function safeUp()
    {
        $this->addColumn('school_kurs_sale_request', 'is_paid_client', 'int default null');
        $this->addColumn('school_kurs_sale_request', 'billing_id', 'int default null');
        $this->createIndex('school_kurs_sale_request_billing_id', 'school_kurs_sale_request', 'billing_id');
        $this->createIndex('billing_class_name', 'billing_class', 'name');
    }

    public function safeDown()
    {
        echo "m171111_100717_token cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m171111_100717_token cannot be reverted.\n";

        return false;
    }
    */
}
