
<?php

use common\models\avatar\Currency;
use yii\db\Migration;

class m171111_100886_token extends Migration
{


    public function safeUp()
    {
        $this->execute('CREATE TABLE `school_sms_subscribe` (
      `id` int(11) NOT NULL AUTO_INCREMENT,
	school_id int null,
	potok_id int null,
	text varchar(2000) null,
	created_at int null,
    PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8');

        $this->execute('CREATE TABLE `school_sms_subscribe_result` (
      `id` int(11) NOT NULL AUTO_INCREMENT,
	subscribe_id int null,
	phone varchar(20) null,
	result varchar(30) null,
	created_at int null,
    PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8');

    }

    public function safeDown()
    {
        echo "m171111_100717_token cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m171111_100717_token cannot be reverted.\n";

        return false;
    }
    */
}
