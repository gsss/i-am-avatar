<?php

use yii\db\Migration;

/**
 * Class m211102_130707_snt
 */
class m211102_130707_snt extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $userList = [
            [
                'name_first' => 'Сергей',
                'name_last'  => 'Скоробогатов',
            ],
        ];

        $c = 10;

        foreach ($userList as $u) {
            $userRoot = \common\models\UserRoot::add([
                'email'         => 'user_' . $c . '@st_severnoe.ru',
                'avatar_status' => 2,
            ]);

            $user = \common\models\UserAvatar::add([
                'email'              => $userRoot->email,
                'password'           => \common\models\UserAvatar::hashPassword('123456'),
                'user_root_id'       => $userRoot->id,
                'registered_ad'      => time(),
                'auth_key'           => \common\services\Security::generateRandomString(60),
                'password_save_type' => \common\models\avatar\UserBill::PASSWORD_TYPE_OPEN_CRYPT,
                'name_first'         => $u['name_first'],
                'name_last'          => \yii\helpers\ArrayHelper::getValue($u, 'name_last'),
            ]);

            $c++;

            echo 'user id=' . $user->id . "\n";
        }
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m211102_130707_snt cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m211102_130707_snt cannot be reverted.\n";

        return false;
    }
    */
}
