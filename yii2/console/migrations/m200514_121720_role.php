<?php

use yii\db\Migration;

/**
 * Class m200514_121720_role
 */
class m200514_121720_role extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        \common\models\school\Role::add([
            'name' => 'Администратор',
        ]);
        \common\models\school\Role::add([
            'name' => 'Учитель',
        ]);
        \common\models\school\Role::add([
            'name' => 'Команда',
        ]);
        \common\models\school\Role::add([
            'name' => 'Председатель Правления',
        ]);
        \common\models\school\Role::add([
            'name' => 'Член Совета',
        ]);
        \common\models\school\Role::add([
            'name' => 'Член Правления',
        ]);
        \common\models\school\Role::add([
            'name' => 'Ревизор',
        ]);
        \common\models\school\Role::add([
            'name' => 'Участник кооператива',
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m200514_121720_role cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200514_121720_role cannot be reverted.\n";

        return false;
    }
    */
}
