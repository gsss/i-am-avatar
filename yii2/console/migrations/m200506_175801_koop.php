<?php

use yii\db\Migration;

/**
 * Class m200506_175801_koop
 */
class m200506_175801_koop extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('user_enter', 'user1_id', 'int null');
        $this->addColumn('user_enter', 'user2_id', 'int null');
        $this->addColumn('user_enter', 'public_key1', 'varchar(100) null');
        $this->addColumn('user_enter', 'public_key2', 'varchar(100) null');
        $this->addColumn('user_enter', 'hash', 'varchar(100) null');
        $this->addColumn('user_enter', 'signature1', 'varchar(100) null');
        $this->addColumn('user_enter', 'signature2', 'varchar(100) null');
        $this->addColumn('user_enter', 'file', 'varchar(100) null');

        $this->createIndex('user_enter_user1_id', 'user_enter', 'user1_id');
        $this->createIndex('user_enter_user2_id', 'user_enter', 'user2_id');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m200506_175801_koop cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200506_175801_koop cannot be reverted.\n";

        return false;
    }
    */
}
