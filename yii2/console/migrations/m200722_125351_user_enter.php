<?php

use yii\db\Migration;

/**
 * Class m200722_125351_user_enter
 */
class m200722_125351_user_enter extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->execute('alter table user_enter change is_payd is_paid tinyint default 0 null;');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m200722_125351_user_enter cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200722_125351_user_enter cannot be reverted.\n";

        return false;
    }
    */
}
