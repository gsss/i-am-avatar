<?php

use yii\db\Migration;

/**
 * Class m220109_165713_processing
 */
class m220109_165713_processing extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        /** @var \iAvatar777\services\Processing\Transaction $t */
        foreach (\iAvatar777\services\Processing\Transaction::find()->all() as $t) {

            if ($t->datetime < 164128987049) {
                $t->datetime = (int) ($t->datetime * 1000);
                $t->hash = $t->hash();
                $t->save();
            }

            echo 'tid=' . $t->id . ' updated' . "\n";
        }

        /** @var \iAvatar777\services\Processing\Operation $t */
        foreach (\iAvatar777\services\Processing\Operation::find()->all() as $t) {

            if ($t->datetime < 164128987049) {
                $t->datetime = (int) ($t->datetime * 1000);
                $t->hash = $t->hash();
                $t->save();

                echo 'oid=' . $t->id . ' updated' . "\n";
            }
        }
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m220109_165713_processing cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m220109_165713_processing cannot be reverted.\n";

        return false;
    }
    */
}
