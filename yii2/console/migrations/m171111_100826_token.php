<?php

use common\models\avatar\Currency;
use yii\db\Migration;

class m171111_100826_token extends Migration
{

    public function safeUp()
    {
        $this->execute('CREATE TABLE `school_referal_level` (
      `id` int(11) NOT NULL AUTO_INCREMENT,
      `school_id` int(11) DEFAULT NULL,
      `level` int(11) DEFAULT NULL,
      `percent` int(11) DEFAULT NULL,
      PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8');
        $this->execute('CREATE TABLE `school_referal_transaction` (
      `id` int(11) NOT NULL AUTO_INCREMENT,
      `school_id` int(11) DEFAULT NULL,
      `request_id` int(11) DEFAULT NULL,
      `level` int(11) DEFAULT NULL,
      `from_uid` int(11) DEFAULT NULL,
      `to_uid` int(11) DEFAULT NULL,
      `to_wid` int(11) DEFAULT NULL,
      `amount` int(11) DEFAULT NULL,
      `transaction_id` bigint DEFAULT NULL,
      PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8');
    }

    public function safeDown()
    {
        echo "m171111_100717_token cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m171111_100717_token cannot be reverted.\n";

        return false;
    }
    */
}
