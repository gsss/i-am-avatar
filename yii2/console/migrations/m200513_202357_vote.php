<?php

use yii\db\Migration;

/**
 * Class m200513_202357_vote
 */
class m200513_202357_vote extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->execute('CREATE TABLE `vote_answer` (
    `id` int(11) NOT NULL AUTO_INCREMENT,
    name    varchar(255) null,
    list_id int          null,
    PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8');
        $this->execute('CREATE TABLE `vote_item` (
    `id` int(11) NOT NULL AUTO_INCREMENT,
    comment    text null,
    user_id    int  null,
    answer_id  int  null,
    created_at int  null,
    list_id    int  null,
    PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8');
        $this->execute('CREATE TABLE `vote_list` (
    `id` int(11) NOT NULL AUTO_INCREMENT,
    name       varchar(255) null,
    content    text         null,
    created_at int          null,
    user_id    int          null,
    PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8');

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m200513_202357_vote cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200513_202357_vote cannot be reverted.\n";

        return false;
    }
    */
}
