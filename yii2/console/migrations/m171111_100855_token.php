
<?php

use common\models\avatar\Currency;
use yii\db\Migration;

class m171111_100855_token extends Migration
{

    public function safeUp()
    {
        $design = \common\models\school\SchoolDesign::findOne(['school_id' => 20]);
        if (is_null($design)) return;
        $design->cabinet_menu = \yii\helpers\Json::encode([
            [
                'label' => 'О нас',
                'url' => '/about',
            ],
            [
                'label' => 'Все проекты',
                'url' => '/projects',
            ],
            [
                'label' => 'Задачи',
                'route' => 'cabinet-task-list/index',
            ],
        ]);
        $design->save();
    }

    public function safeDown()
    {
        echo "m171111_100717_token cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m171111_100717_token cannot be reverted.\n";

        return false;
    }
    */
}
