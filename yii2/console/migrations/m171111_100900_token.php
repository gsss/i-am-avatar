
<?php

use common\models\avatar\Currency;
use yii\db\Migration;

class m171111_100900_token extends Migration
{


    public function safeUp()
    {
        /** @var \common\models\comment\CommentListType $list */
        $list = \common\models\comment\CommentListType::add([
            'name' => 'Урок в потоке',
        ]);

        /** @var \common\models\comment\CommentList $l */
        foreach (\common\models\comment\CommentList::find()->where(['type_id' => 2])->all() as $l) {

            /** @var \common\models\comment\Comment $c */
            foreach (\common\models\comment\Comment::find()->where(['list_id' => $l->id])->all() as $c) {

                $listNew = \common\models\comment\CommentList::get($list->id, $l->object_id);
                $cNew = \common\models\comment\Comment2::add([
                    'text'       => $c->text,
                    'list_id'    => $listNew->id,
                    'user_id'    => $c->user_id,
                    'created_at' => $c->created_at,
                ]);
                echo('new comment id='.$cNew->id);
                echo("\n");
            }
        }
    }

    public function safeDown()
    {
        echo "m171111_100717_token cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m171111_100717_token cannot be reverted.\n";

        return false;
    }
    */
}
