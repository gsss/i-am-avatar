<?php

use yii\db\Migration;

/**
 * Class m200429_180649_verify
 */
class m200429_180649_verify extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('koop_user_verification', 'user_enter_id', 'int null');
        $this->createIndex('koop_user_verification_user_enter_id', 'koop_user_verification', 'user_enter_id');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m200429_180649_verify cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200429_180649_verify cannot be reverted.\n";

        return false;
    }
    */
}
