<?php

use yii\db\Migration;

/**
 * Class m201130_065655_blago
 */
class m201130_065655_blago extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->execute('CREATE TABLE `school_blago_program` (
    `id` int(11) NOT NULL AUTO_INCREMENT,
    school_id  int    null,
    name       varchar(100) null,
    PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8');

        $this->createIndex('school_blago_program_school_id', 'school_blago_program', 'school_id');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m201130_065655_blago cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m201130_065655_blago cannot be reverted.\n";

        return false;
    }
    */
}
