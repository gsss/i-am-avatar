
<?php

use common\models\avatar\Currency;
use yii\db\Migration;

class m171111_100890_token extends Migration
{


    public function safeUp()
    {
        $this->execute('CREATE TABLE `payments_yandex_kassa` (
      `id` int(11) NOT NULL AUTO_INCREMENT,
	billing_id int null,
	payment_id varchar(100) null,
	currency_id int null,
	amount int null,
	created_at int null,
    PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8');

    }

    public function safeDown()
    {
        echo "m171111_100717_token cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m171111_100717_token cannot be reverted.\n";

        return false;
    }
    */
}
