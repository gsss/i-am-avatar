<?php

use yii\db\Migration;

/**
 * Class m210212_205415_koop
 */
class m210212_205415_koop extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->execute('ALTER TABLE i_am_avatar_prod_main.school_koop MODIFY phone VARCHAR(20);');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m210212_205415_koop cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210212_205415_koop cannot be reverted.\n";

        return false;
    }
    */
}
