<?php

use yii\db\Migration;

/**
 * Class m211103_071225_snt
 */
class m211103_071225_snt extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('school_snt_pohod_user', 'school_id', 'int null');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m211103_071225_snt cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m211103_071225_snt cannot be reverted.\n";

        return false;
    }
    */
}
