<?php

use yii\db\Migration;

/**
 * Class m200529_171614_docs
 */
class m200529_171614_docs extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('user_documents', 'school_id', 'int null');
        $this->createIndex('user_documents_school_id', 'user_documents', 'school_id');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m200529_171614_docs cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200529_171614_docs cannot be reverted.\n";

        return false;
    }
    */
}
