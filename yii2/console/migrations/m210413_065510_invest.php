<?php

use yii\db\Migration;

/**
 * Class m210413_065510_invest
 */
class m210413_065510_invest extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m210413_065510_invest cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210413_065510_invest cannot be reverted.\n";

        return false;
    }
    */
}
