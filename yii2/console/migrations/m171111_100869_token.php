
<?php

use common\models\avatar\Currency;
use yii\db\Migration;

class m171111_100869_token extends Migration
{


    public function safeUp()
    {
        $this->createIndex('school_task_session_task_id','school_task_session','task_id');
        $this->createIndex('school_task_session_is_finish','school_task_session','is_finish');
    }

    public function safeDown()
    {
        echo "m171111_100717_token cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m171111_100717_token cannot be reverted.\n";

        return false;
    }
    */
}
