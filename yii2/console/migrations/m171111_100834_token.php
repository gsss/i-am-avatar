<?php

use common\models\avatar\Currency;
use yii\db\Migration;

class m171111_100834_token extends Migration
{

    public function safeUp()
    {
        $this->execute('CREATE TABLE `school_shop_anketa_link` (
      `id` int(11) NOT NULL AUTO_INCREMENT,
      `anketa_id` int(11) DEFAULT NULL,
      `product_id` int(11) DEFAULT NULL,
      PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8');

        $this->execute('CREATE TABLE `school_potok_fields_list_data_lid` (
      `id` int(11) NOT NULL AUTO_INCREMENT,
      `lid_id` int(11) DEFAULT NULL,
      `potok_id` int(11) DEFAULT NULL,
      `created_at` int(11) DEFAULT NULL,
      `data` text DEFAULT NULL,
      PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8');

    }

    public function safeDown()
    {
        echo "m171111_100717_token cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m171111_100717_token cannot be reverted.\n";

        return false;
    }
    */
}
