<?php

use yii\db\Migration;

/**
 * Class m200902_172816_koop
 */
class m200902_172816_koop extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('school_koop', 'input_amount', 'int null');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m200902_172816_koop cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200902_172816_koop cannot be reverted.\n";

        return false;
    }
    */
}
