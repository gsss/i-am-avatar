<?php

use yii\db\Migration;

/**
 * Class m211231_185902_reg
 */
class m211231_185902_reg extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('user_register_code', 'password_hash', 'varchar(128) null');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m211231_185902_reg cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m211231_185902_reg cannot be reverted.\n";

        return false;
    }
    */
}
