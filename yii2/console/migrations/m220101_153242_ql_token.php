<?php

use yii\db\Migration;

/**
 * Class m220101_153242_ql_token
 */
class m220101_153242_ql_token extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('school_blago_program', 'currency_id', 'int null');
        $this->addColumn('school_blago_program', 'currency_wallet_id', 'int null');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m220101_153242_ql_token cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m220101_153242_ql_token cannot be reverted.\n";

        return false;
    }
    */
}
