<?php

use yii\db\Migration;

/**
 * Class m200426_205127_koop
 */
class m200426_205127_koop extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('user_enter', 'school_id', 'int null');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m200426_205127_koop cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200426_205127_koop cannot be reverted.\n";

        return false;
    }
    */
}
