<?php

use yii\db\Migration;

/**
 * Class m200525_154932_name
 */
class m200525_154932_name extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('school_design', 'is_add_user_in_command_after_register', 'tinyint default 0');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m200525_154932_name cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200525_154932_name cannot be reverted.\n";

        return false;
    }
    */
}
