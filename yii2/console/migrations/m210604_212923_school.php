<?php

use yii\db\Migration;

/**
 * Class m210604_212923_school
 */
class m210604_212923_school extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('school', 'is_super_company', 'tinyint default 0');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m210604_212923_school cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210604_212923_school cannot be reverted.\n";

        return false;
    }
    */
}
