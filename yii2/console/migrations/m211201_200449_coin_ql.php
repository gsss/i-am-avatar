<?php

use yii\db\Migration;

/**
 * Class m211201_200449_coin_ql
 */
class m211201_200449_coin_ql extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $c = \common\models\piramida\Currency::findOne(['code' => 'QL']);
        $rows = \common\models\school\School::find()->all();
        /** @var \common\models\school\School $s */
        foreach ($rows as $s) {
            if (\cs\Application::isEmpty($s->pay_wallet_id)) {
                $w = \common\models\piramida\Wallet::addNew(['currency_id' => $c->id]);
                $s->pay_wallet_id = $w->id;
                $s->save();
                echo 'sid='.$s->id . ' wid='. $s->pay_wallet_id .' update' . "\n";
            }
        }
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m211201_200449_coin_ql cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m211201_200449_coin_ql cannot be reverted.\n";

        return false;
    }
    */
}
