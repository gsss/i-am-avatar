
<?php

use common\models\avatar\Currency;
use yii\db\Migration;

class m171111_100896_token extends Migration
{


    public function safeUp()
    {
        $this->execute('CREATE TABLE `user_digital_sign` (
      `id` int(11) NOT NULL AUTO_INCREMENT,
	user_id int null,
	created_at int null,
	`address` varchar(160) null,
	`address_uncompressed` varchar(160) null,
	`private_key` varchar(160) null,
	`public_key_uncompressed` varchar(160) null,
	type_id int default 0,
    PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8');
        $this->createIndex('user_digital_sign_user_id', 'user_digital_sign', 'user_id');

    }

    public function safeDown()
    {
        echo "m171111_100717_token cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m171111_100717_token cannot be reverted.\n";

        return false;
    }
    */
}
