<?php

use yii\db\Migration;

/**
 * Class m210720_074643_command
 */
class m210720_074643_command extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('school_command_link', 'hour_price', 'int null');
        $this->addColumn('school_command_link', 'hour_price_currency_id', 'int null');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m210720_074643_command cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210720_074643_command cannot be reverted.\n";

        return false;
    }
    */
}
