<?php

use yii\db\Migration;

/**
 * Class m201123_153844_koop_aim
 */
class m201123_153844_koop_aim extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->execute('CREATE TABLE `school_koop_aim` (
    `id` int(11) NOT NULL AUTO_INCREMENT,
    koop_id  int    null,
    name            varchar(100) null,
    PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8');

        $this->createIndex('school_koop_aim_koop_id', 'school_koop_aim', 'koop_id');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m201123_153844_koop_aim cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m201123_153844_koop_aim cannot be reverted.\n";

        return false;
    }
    */
}
