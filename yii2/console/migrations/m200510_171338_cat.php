<?php

use yii\db\Migration;

/**
 * Class m200510_171338_cat
 */
class m200510_171338_cat extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createIndex('school_task_category_school_id', 'school_task_category','school_id' );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m200510_171338_cat cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200510_171338_cat cannot be reverted.\n";

        return false;
    }
    */
}
