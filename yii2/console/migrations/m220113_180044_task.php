<?php

use yii\db\Migration;

/**
 * Class m220113_180044_task
 */
class m220113_180044_task extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('school_task', 'wallet_type3_user_id', 'int null');

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m220113_180044_task cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m220113_180044_task cannot be reverted.\n";

        return false;
    }
    */
}
