<?php

use yii\db\Migration;

/**
 * Class m210101_191153_task
 */
class m210101_191153_task extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('school_task', 'sort_index', 'int null');
        $this->createIndex('school_task_sort_index', 'school_task', 'sort_index');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m210101_191153_task cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210101_191153_task cannot be reverted.\n";

        return false;
    }
    */
}
