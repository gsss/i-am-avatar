<?php

namespace console\controllers;

use avatar\models\forms\school\Certificate;
use avatar\models\forms\school\Lesson;
use common\models\Anketa;
use common\models\avatar\UserBill;
use common\models\blog\Article;
use common\models\BlogItem;
use common\models\CardDesign;
use common\models\comment\Comment;
use common\models\comment\Type;
use common\models\Config;
use common\models\Language;
use common\models\language\Category;
use common\models\language\Message;
use common\models\language\SourceMessage;
use common\models\NewsItem;
use common\models\PaymentBitCoin;
use common\models\PaySystem;
use common\models\piramida\Transaction;
use common\models\piramida\Wallet;
use common\models\rbac\AuthAssignment;
use common\models\rbac\AuthItemChild;
use common\models\school\Kurs;
use common\models\school\LessonPotokState;
use common\models\school\Potok;
use common\models\school\PotokUser3Link;
use common\models\school\PotokUserExt;
use common\models\school\PotokUserLink;
use common\models\school\School;
use common\models\school\Sertificate;
use common\models\school\UserLink;
use common\models\shop\Product;
use common\models\User2;
use common\models\UserAvatar;
use common\models\UserRoot;
use common\models\UserWallet;
use cs\Application;
use cs\services\File;
use cs\Widget\FileUpload3\FileUpload;
use Yii;
use yii\base\Exception;
use yii\db\Connection;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;
use yii\helpers\StringHelper;
use yii\helpers\Url;
use yii\helpers\VarDumper;
use yii\httpclient\Client;
use yii\web\HttpException;
use yii\web\Request;


/**
 *
 * @package console\controllers
 */
class CloudController extends \console\base\Controller
{

    /**
     */
    public function actionUpdate()
    {
        /** @var \common\services\AvatarCloud $cloud */
        $cloud = Yii::$app->AvatarCloud;

        if (count(Yii::$app->requestedParams) < 3) {
            echo 'Список параметров для запуска: php yii cloud/update <tid> <class> <attribute> [school_id]';
            exit();
        }

        $tid = Yii::$app->requestedParams[0];
        $class = Yii::$app->requestedParams[1];
        $attribute = Yii::$app->requestedParams[2];

        // если пусто то $class->school_id
        $school_id = 2;
        if (count(Yii::$app->requestedParams) == 4) {
            $school_id = Yii::$app->requestedParams[3];
        }
        if (!Application::isInteger($school_id)) {
            echo 'Параметр [school_id] должен быть целым числом';
            exit();
        }
        if (!Application::isInteger($tid)) {
            echo 'Параметр [tid] должен быть целым числом';
            exit();
        }
        if (!class_exists($class)) {
            echo 'Класс ['.$class.'] не существует';
            exit();
        }
        $o = $class::find()->one();
        if (!$o->hasAttribute($attribute)) {
            echo 'Класс ['.$class.'] не содержит атрибута ['.$attribute.']';
            exit();
        }

        foreach ($class::find()->all() as $item) {
            self::log($item->id);

            if (Application::isEmpty($school_id)) {
                $school_id = $item->school_id;
            }

            if (!Application::isEmpty($item->$attribute)) {
                if (StringHelper::startsWith($item->$attribute, '/')) {
                    // загружаю файл
                    $path = Yii::getAlias('@web' . FileUpload::getOriginal($item->$attribute));
                    if (file_exists($path)) {
                        $file = File::path($path);
                        $data = $cloud->post(null, 'upload/image', [
                            'update' => Json::encode([
                                [
                                    'function' => 'crop',
                                    'index'    => 'crop',
                                    'options'  => [
                                        'width'  => '300',
                                        'height' => '300',
                                        'mode'   => 'MODE_THUMBNAIL_CUT',
                                    ],
                                ],
                            ]),
                        ], ['files' => [$file]]);
                        if ($data['success']) {
                            $fileList = $data['data']['fileList'];
                            /**
                             * 'file' => '58292_q217BWHG6d.png'
                             * 'url' => 'https://cloud1.i-am-avatar.com/upload/cloud/15585/58292_q217BWHG6d.png'
                             * 'size' => 771754
                             * 'update' => [
                             *      'crop' => [
                             *          'url' => 'https://cloud1.i-am-avatar.com/upload/cloud/15585/58292_q217BWHG6d_crop.png'
                             *          'size' => 12546
                             *      ]
                             * ]
                             */
                            $file = $fileList[0];
                            self::log($file['url']);

                            // Добавляю файл
                            \common\models\school\File::add([
                                'school_id' => $school_id,
                                'type_id'   => $tid,
                                'size'      => $file['size'],
                                'file'      => $file['url'],
                            ]);

                            // Добавляю файл
                            \common\models\school\File::add([
                                'school_id' => $school_id,
                                'type_id'   => $tid,
                                'size'      => $file['update']['crop']['size'],
                                'file'      => $file['update']['crop']['url'],
                            ]);

                            // Удаляю старые
                            $path = Yii::getAlias('@web' . FileUpload::getOriginal($item->$attribute));
                            if (file_exists($path)) {
                                try {
                                    //unlink($path);
                                } catch (\Exception $e) {

                                }
                            }
                            $path = Yii::getAlias('@web' . $item->$attribute);
                            if (file_exists($path)) {
                                try {
                                    //unlink($path);
                                } catch (\Exception $e) {

                                }
                            }

                            // прописываю новый файл
                            $item->$attribute = $file['url'];
                            $item->save();
                        }
                    } else {
                        self::log('File not exist');
                    }

                } else {
                    self::log('Allready loaded');
                }

            } else {
                self::log('No image');
            }
        }

    }

}
