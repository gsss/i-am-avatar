<?php

namespace console\controllers;

use avatar\models\forms\school\Certificate;
use avatar\models\forms\school\Lesson;
use common\models\Anketa;
use common\models\avatar\UserBill;
use common\models\blog\Article;
use common\models\BlogItem;
use common\models\comment\Comment;
use common\models\comment\Type;
use common\models\Config;
use common\models\Language;
use common\models\language\Category;
use common\models\language\Message;
use common\models\language\SourceMessage;
use common\models\NewsItem;
use common\models\PaymentBitCoin;
use common\models\PaySystem;
use common\models\piramida\Transaction;
use common\models\piramida\Wallet;
use common\models\rbac\AuthAssignment;
use common\models\rbac\AuthItemChild;
use common\models\school\Kurs;
use common\models\school\LessonPotokState;
use common\models\school\Potok;
use common\models\school\PotokUser3Link;
use common\models\school\PotokUserExt;
use common\models\school\PotokUserLink;
use common\models\school\School;
use common\models\school\Sertificate;
use common\models\school\UserLink;
use common\models\shop\Product;
use common\models\User2;
use common\models\UserAvatar;
use common\models\UserRoot;
use common\models\UserWallet;
use cs\Application;
use cs\services\File;
use cs\Widget\FileUpload3\FileUpload;
use Yii;
use yii\base\Exception;
use yii\db\Connection;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;
use yii\helpers\StringHelper;
use yii\helpers\Url;
use yii\helpers\VarDumper;
use yii\httpclient\Client;
use yii\web\HttpException;
use yii\web\Request;


/**
 * Выполняет воссоздание базы данных с оригинала
 *
 * Class SyncController
 *
 * @package console\controllers
 */
class TestController extends \console\base\Controller
{
    public function actionIndex()
    {
        $link1 = Yii::$app->db->beginTransaction();
        try {
            $rows = PotokUser3Link::find()->where(['potok_id' => 2])->all();
            $potokList = [1, 3, 4, 5];
            /** @var \common\models\school\PotokUser3Link $row */
            foreach ($rows as $row) {
                $c = PotokUser3Link::find()
                    ->where(['user_root_id' => $row->user_root_id])
                    ->andWhere(['potok_id' => $potokList])
                    ->count();
                if ($c == 0) {
                    echo 'Пользователь = ' . $row->user_root_id . ' не существует в других потоках.';
                    /** @var \common\models\school\UserLink $link */
                    $link = UserLink::find()->where([
                        'user_root_id' => $row->user_root_id,
                        'school_id'    => 1,
                    ])->one();
                    if (is_null($link)) {
                        echo ' ' . 'ссылка отсутствует.';
                    } else {
                        echo ' ' . 'ссылка присутствует.';
                        $link->school_id = 2;
                        $ret = $link->save();
                        if ($ret) {
                            echo ' ' . 'сохранено успешно.';
                        } else {
                            echo ' ' . 'сохранено не успешно.';
                        }
                    }
                } else {
                    echo 'Пользователь = ' . $row->user_root_id . ' существует в других потоках';
                    UserLink::add([
                        'user_root_id' => $row->user_root_id,
                        'school_id'    => 2,
                    ]);
                }
                echo "\n";
            }
            $kurs = Kurs::findOne(2);
            $kurs->school_id = 2;
            $kurs->save();

            $link1->commit();
        } catch (\Exception $e) {
            $link1->rollBack();

            echo 'error';
        }
    }

    public function actionIndex2()
    {
        $data = [
            [3, 41102,],
            [2, 41182,],
            [2, 41212,],
            [2, 42082,],
            [2, 43182,],
            [2, 43302,],
            [2, 43582,],
            [2, 44242,],
        ];
        foreach ($data as $row) {
            $count = $row[0];
            $d = $row[1];
            $user_root_id = substr($d, 0, 4);
            $school_id = substr($d, 4);
            for ($i = 0; $i < $count - 1; $i++) {
                $link = UserLink::findOne([
                    'user_root_id' => $user_root_id,
                    'school_id'    => $school_id,
                ]);
                $link->delete();
                echo $d;
                echo "\n";

            }
        }
    }

    public function actionIndex3()
    {
        $data = \common\models\piramida\Currency::find()->all();


        echo '\common\models\piramida\Currency' . "\n";
        /** @var \common\models\piramida\Currency $row */
        foreach ($data as $row) {
            $id = $row->id;
            $address = 'C_0x' . hash('sha256', $id);
            $row->address = $address;
            $row->save();
            echo 'id = ' . $row->id . ' address = ' . $address . "\n";
        }

        $data = \common\models\piramida\Operation::find()->all();
        echo '' . "\n";
        echo '\common\models\piramida\Operation' . "\n";
        /** @var \common\models\piramida\Operation $row */
        foreach ($data as $row) {
            $id = $row->id;
            $address = 'O_0x' . hash('sha256', $id);
            $row->address = $address;
            $row->save();
            echo 'id = ' . $row->id . ' address = ' . $address . "\n";
        }

        $data = \common\models\piramida\Transaction::find()->all();
        echo '' . "\n";
        echo '\common\models\piramida\Transaction' . "\n";
        /** @var \common\models\piramida\Transaction $row */
        foreach ($data as $row) {
            $id = $row->id;
            $address = 'T_0x' . hash('sha256', $id);
            $row->address = $address;
            $row->save();
            echo 'id = ' . $row->id . ' address = ' . $address . "\n";
        }

        $data = \common\models\piramida\Wallet::find()->all();
        echo '' . "\n";
        echo '\common\models\piramida\Wallet' . "\n";
        /** @var \common\models\piramida\Wallet $row */
        foreach ($data as $row) {
            $id = $row->id;
            $address = 'W_0x' . hash('sha256', $id);
            $row->address = $address;
            $row->save();
            echo 'id = ' . $row->id . ' address = ' . $address . "\n";
        }
    }

    public function actionIndex4()
    {
        $data = UserWallet::find()->all();

        /** @var UserWallet $row */
        foreach ($data as $row) {
            try {
                $b = UserBill::findOne(['address' => $row->wallet_id]);
                echo 'id = ' . $b->id . ' exist' . "\n";
            } catch (\Exception $e) {
                $b = UserBill::add([
                    'user_id'  => $row->user_id,
                    'address'  => (string)$row->wallet_id,
                    'currency' => in_array($row->currency_id, [1, 2]) ? $row->currency_id : 4,
                    'name'     => ($row->currency_id == 1) ? 'Золотой Эликсир' : (($row->currency_id == 2) ? 'Небесный Эликсир' : 'Изумруд'),
                ]);
                echo 'id = ' . $b->id . ' new' . "\n";
            }
        }
    }

    public function actionIndex5()
    {
        $data = UserBill::find()->all();

        /** @var UserBill $row */
        foreach ($data as $row) {

            try {
                $b = UserAvatar::findOne($row->user_id);
//                echo 'id = ' . $row->id . ' exist' . "\n";
            } catch (\Exception $e) {
                //$row->delete();
                $w = Wallet::findOne($row->address);
                if (is_null($w)) {
                    echo 'wid = ' . $row->address . ' not found' . "\n";
                }
                echo 'id = ' . $row->id . ' not found' . "\n";
            }
        }
    }

    public function actionTest6()
    {
        $a = [
            4108,
            4318,
            4115,
            4194,
            4335,
            4336,
            4341,
            4121,
            4105,
            4352,
            4165,
            4363,
            4420,
            4421,
            4424,
            4425,
            4429,
            4430,
            4113,
            4436,
            4440,
            4442,
            4403,
            4444,
            4443,
            4445,
            4446,
            4452,
            4453,
            4454,
            4377,
            4383,
            4402,
            4479,
            4486,
            4485,
            4494,
            4497,
            4493,
            4478,
            4541,
            4533,
            4549,
            4550,
            4551,
            4459,
            4502,
            4111,
            4555,
            4556,
            4569,
            4358,
            4584,
            4585,
            4586,
            4587,
            4547,
            4282,
            4589,
            4593,
            4595,
            4597,
            4598,
            4599,
            4601,
            4602,
            4603,
        ];

        $c = 0;
        foreach ($a as $id) {
            if (!PotokUser3Link::find()->where([
                'user_root_id' => $id,
                'potok_id'     => 2,
            ])->exists()
            ) {
                PotokUser3Link::add([
                    'user_root_id' => $id,
                    'is_avatar'    => 1,
                    'potok_id'     => 2,
                ]);
                self::log('add id = ' . $id);
                $c++;
            }
        }
        self::log('added ' . $c . ' items');
    }

    public function actionTest7()
    {
        // Проанализировать все счета которые принадлежат пользователям
        $userList = UserBill::find()->where(['not', ['user_id' => null]])->groupBy(['user_id'])->select('user_id')->column();
        foreach ($userList as $uid) {
            // Проанализировать Каждую валюту
            $currencyList = UserBill::find()->where(['user_id' => $uid])->groupBy('currency')->select('currency')->column();
            foreach ($currencyList as $cid) {
                // Проставляю на каждый первый счет флаг счет по умолчанию
                $list = UserBill::find()->where(['user_id' => $uid, 'currency' => $cid])->all();
                if (count($list) == 1) {
                    /** @var \common\models\avatar\UserBill $bill */
                    $bill = $list[0];
                    $bill->is_default = 1;
                    $bill->save();
                    self::log('$bill id = ' . $bill->id . ' set is_default=1');
                } else {
                    /** @var \common\models\avatar\UserBill $bill */
                    $bill = $list[0];
                    $bill->is_default = 1;
                    $bill->save();
                    self::log('$bill id = ' . $bill->id . ' set is_default=1 is many');
                }
            }
        }

        // Проанализировать все счета которые не принадлежат пользователям
        // А вот здесь непонятно что делать, хотя вообщем то это же карты, потому их можно сделать по умолчанию
        // А вдруг эту карту присоединит кто то, у кого уже есть счета? Да значит эти счета уже будут не по умолчанию
        // Значин надо при присоединении карты выставлять этот флаг отдельно
    }

    /**
     */
    public function actionTest9()
    {
        /** @var \common\services\AvatarCloud $cloud */
        $cloud = Yii::$app->AvatarCloud;

        $tid = 18;
        $attribute = 'avatar';
        $class = '\common\models\UserAvatar';

        foreach ($class::find()->all() as $item) {
            self::log($item->id);

            $school_id = 2;

            if (!Application::isEmpty($item->$attribute)) {
                if (StringHelper::startsWith($item->$attribute, '/')) {
                    // загружаю файл
                    $path = Yii::getAlias('@web' . FileUpload::getOriginal($item->$attribute));
                    if (file_exists($path)) {
                        $file = File::path($path);
                        $data = $cloud->post(null, 'upload/image', [
                            'update' => Json::encode([
                                [
                                    'function' => 'crop',
                                    'index'    => 'crop',
                                    'options'  => [
                                        'width'  => '300',
                                        'height' => '300',
                                        'mode'   => 'MODE_THUMBNAIL_CUT',
                                    ],
                                ],
                            ]),
                        ], ['files' => [$file]]);
                        if ($data['success']) {
                            $fileList = $data['data']['fileList'];
                            /**
                             * 'file' => '58292_q217BWHG6d.png'
                             * 'url' => 'https://cloud1.i-am-avatar.com/upload/cloud/15585/58292_q217BWHG6d.png'
                             * 'size' => 771754
                             * 'update' => [
                             *      'crop' => [
                             *          'url' => 'https://cloud1.i-am-avatar.com/upload/cloud/15585/58292_q217BWHG6d_crop.png'
                             *          'size' => 12546
                             *      ]
                             * ]
                             */
                            $file = $fileList[0];
                            self::log($file['url']);


                            // Добавляю файл
                            \common\models\school\File::add([
                                'school_id' => $school_id,
                                'type_id'   => $tid,
                                'size'      => $file['size'],
                                'file'      => $file['url'],
                            ]);

                            // Добавляю файл
                            \common\models\school\File::add([
                                'school_id' => $school_id,
                                'type_id'   => $tid,
                                'size'      => $file['update']['crop']['size'],
                                'file'      => $file['update']['crop']['url'],
                            ]);

                            // Удаляю старые
                            $path = Yii::getAlias('@web' . FileUpload::getOriginal($item->$attribute));
                            if (file_exists($path)) {
                                try {
                                    //unlink($path);
                                } catch (\Exception $e) {

                                }
                            }
                            $path = Yii::getAlias('@web' . $item->$attribute);
                            if (file_exists($path)) {
                                try {
                                    //unlink($path);
                                } catch (\Exception $e) {

                                }
                            }

                            // прописываю новый файл
                            $item->$attribute = $file['url'];
                            $item->save();
                        }
                    } else {
                        self::log('File not exist');
                    }

                } else {
                    self::log('Allready loaded');
                }

            } else {
                self::log('No image');
            }
        }

    }

    /**
     */
    public function actionTest10()
    {

/** @var \common\models\piramida\Operation $item */
        foreach (\common\models\piramida\Operation::find()->all() as $item) {
            $item->hash = $item->hash();
            $item->save();
            $this->log($item->id . ' done.');

        }

    }

    /**
     */
    public function actionTest11()
    {
        /** @var \common\models\school\Kurs $k */
        foreach (Kurs::find()->all() as $k) {
            /** @var \common\models\school\Lesson $l */
            foreach (\common\models\school\Lesson::find()->where(['kurs_id' => $k->id])->all() as $l) {
                /** @var \common\models\school\Potok $p */
                foreach (\common\models\school\Potok::find()->where(['kurs_id' => $k->id])->all() as $p) {
                    $i = LessonPotokState::add([
                        'lesson_id' => $l->id,
                        'potok_id'  => $p->id,
                        'status'    => 0,
                        'is_hide'   => 0,
                    ]);
                    self::log('LessonPotokState add Lesson='.$l->id.' potok='.$p->id);
                }
            }
        }
    }
}
