<?php

namespace console\controllers;

use common\models\avatar\Currency;
use common\models\Config;
use common\models\CryptoCap;
use common\models\task\Session;
use common\models\task\Task;
use common\models\UserAvatar;
use common\services\Subscribe;
use cs\services\VarDumper;
use yii\console\Controller;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;
use yii\httpclient\Client;

/**
 */
class TaskController extends \console\base\Controller
{

    /**
     * скрипт остановки задачи если она уже более 24 часов висит
     */
    public function actionCloseOpenLong()
    {
        $all = Session::find()->where(['is_finish' => 0])->all();
        $time = 60*60*24;

        /** @var \common\models\task\Session $item */
        foreach ($all as $item) {
            if ($item->start + $time < time()) {
                $item->finish = time();
                $item->is_finish = 1;
                $item->save();
                $task = Task::findOne($item->task_id);
                if ($task->executer_id) {
                    Subscribe::sendArraySchool($task->getSchool(), [UserAvatar::findOne($task->executer_id)->email], 'Задача остановлена', 'task/finish-console', [
                        'task' => $task,
                    ]);
                    self::log('task closed id='.$item->id .' send mail');

                } else {
                    self::log('task closed id='.$item->id);

                }
            }
        }
    }

}