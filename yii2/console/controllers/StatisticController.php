<?php
/**
 * Created by PhpStorm.
 * User: s.arhangelskiy
 * Date: 29.03.2017
 * Time: 17:59
 */

namespace console\controllers;


use common\components\Monitoring;
use common\components\OnlineManager;
use common\components\providers\ETH;
use common\models\avatar\UserBinance;
use common\models\LogRequest;
use common\models\Server;
use common\models\statistic\PingProd;
use common\models\statistic\PingStatus;
use common\models\statistic\PingTest;
use common\models\statistic\ServerRequest;
use common\models\statistic\StatisticDb;
use common\models\statistic\StatisticOnline;
use common\models\statistic\StatisticRoute;
use cs\services\VarDumper;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;
use yii\httpclient\Client;

class StatisticController extends \console\base\Controller
{
    public function init()
    {
        $this->setIsLog();
    }

    /**
     * Записывает кол-во пользователей online каждую минуту в таблицу statistic_online
     */
    public function actionCalculate()
    {
        try {
            $o = new OnlineManager();
            $m = new Monitoring();
            $fields = [
                'time'   => time(),
                'online' => $o->count(),
            ];
            $new = $m->calculate();
            $new['request_per_second'] = round($new['request_per_second'] * 60, 2);
            $fields = ArrayHelper::merge($fields, $new);
            (new StatisticOnline($fields))->save();
            $m->reset();

//            $dbStatisticValues = \Yii::$app->monitoringDb->calculate();
//            $fields = [
//                'time'               => time(),
//                'main_master_select' => $dbStatisticValues['master']['select'],
//                'main_master_insert' => $dbStatisticValues['master']['insert'],
//                'main_master_update' => $dbStatisticValues['master']['update'],
//                'main_master_delete' => $dbStatisticValues['master']['delete'],
//                'main_slave_select'  => (count($dbStatisticValues['slaveArray']) > 0) ? $dbStatisticValues['slaveArray'][0]['select'] : 0,
//                'statistic_select'   => $dbStatisticValues['statistic']['select'],
//                'statistic_insert'   => $dbStatisticValues['statistic']['insert'],
//                'statistic_update'   => $dbStatisticValues['statistic']['update'],
//                'statistic_delete'   => $dbStatisticValues['statistic']['delete'],
//            ];
//            $result = (new StatisticDb($fields))->save();
//            if ($result) {
//                \Yii::$app->monitoringDb->reset();
//            } else {
//                $this->logDanger('Coudnt save to StatisticDb');
//            }
        } catch (\Exception $e) {
            VarDumper::dump($e->getMessage());
        }
    }

    /**
     * Записывает доступность PROD и TEST серверов раз в 10 минут и записывает в таблицу statistic_ping_server_prod
     */
    public function actionPingServers()
    {
        /** @var \common\models\Server $server */
        foreach (Server::find()->all() as $server) {
            $data = $this->ping2($server->api);
            PingStatus::add($server->id, $data['status'], $data);
        }
    }

    /**
     * Пингует $url
     *
     *
     * @param $url
     * @return int 1 - 200,401; Иначе - 0
     */
    private function ping($url)
    {
        $client = new Client();
        try {
            $response = $client->get($url)->send();
            $status = $response->headers['http-code'];
            if (in_array($status, [200, 401])) {
                self::logSuccess('status: ' . $url . ' ' . $status);
                return 1;
            } else {
                self::logDanger('status: ' . $url . ' ' . $status);
            }
        } catch (\Exception $e) {
            self::logDanger($e->getMessage());
        }

        return 0;
    }

    /**
     * Пингует $url
     *
     *
     * @param string $url
     * @return array
     * [
     *  'status'    => 1,
     *  'disk'      => 1,
     *  'memory'    => 1,
     *  'processor' => 1,
     * ] - 200,401;
     * Иначе -
     * [
     *  'status'    => 0,
     * ]
     */
    private function ping2($url)
    {
        if (is_null($url)) {
            return ['status' => 0];
        }

        // убираю слеш вконце если есть
        $url = rtrim($url, '/');

        // Формирую url
        $url .= '/site/all';

        $client = new Client();
        try {
            $response = $client->get($url)->send();
            $status = $response->headers['http-code'];
            if (in_array($status, [200, 401])) {
                self::logSuccess('status: ' . $url . ' ' . $status);
                try {
                    $data = Json::decode($response->content);
                } catch (\Exception $e) {
                    return [
                        'status' => 1,
                    ];
                }

                return ArrayHelper::merge([
                    'status' => 1,
                ], $this->convertAll($data));
            } else {
                self::logDanger('status: ' . $url . ' ' . $status);
            }
        } catch (\Exception $e) {
            self::logDanger($e->getMessage());
        }

        return [
            'status' => 0,
        ];
    }

    /**
     * @param array $data
     * [
     * 'success'
     * 'data'
     * ]
     *
     * @return array
     * [
     *  'disk'
     *  'memory'
     *  ?'processor'
     * ]
     */
    private function convertAll($data)
    {
        $disk = $data['data']['disk']['all'];
        $disk = $disk['Avail'] / $disk['Size'];
        $disk = 100 - ($disk * 100);

        $mem = $data['data']['mem']['mem'];
        $mem = $mem['available'] / $mem['total'];
        $mem = 100 - ($mem * 100);

        if (!isset($data['data']['processor'])) {
            return [
                'disk'      => (float)$disk,
                'memory'    => (float)$mem,
            ];
        }

        $processor = $data['data']['processor']['all'];
        $processor = 100 - $processor['idle'];

        return [
            'disk'      => (float)$disk,
            'memory'    => (float)$mem,
            'processor' => (float)$processor,
        ];
    }

    /**
     * Удаляет старые записи старше чем 1 день
     * Желательно запускать 1 раз в день
     */
    public function actionClear()
    {
        $day = 60 * 60 * 24;
        StatisticOnline::deleteAll(['<', 'time', time() - ($day * 30)]);
        StatisticRoute::deleteAll(['<', 'time', time() - $day]);
        StatisticDb::deleteAll(['<', 'time', time() - $day]);
        LogRequest::deleteAll(['<', 'time', time() - ($day * 30)]);
        ServerRequest::deleteAll(['<', 'log_time', time() - ($day * 2)]);
    }

    /**
     * Удаляет старые записи старше чем неделя из таблицы log_requests
     * пример запуска `php yii statistic/log-requests-clear`
     */
    public function actionLogRequestsClear()
    {
        $period = 60 * 60 * 24 * 7;
        LogRequest::deleteAll(['<', 'time', time() - $period]);
    }

    /**
     * Пишет статистику портфелей для всех пользователей
     */
    public function actionBinance()
    {
        /** @var \common\models\avatar\UserBinance $binance */
        foreach (UserBinance::find()->all() as $binance) {
            $sum = $this->getUser($binance);
            \common\models\statistic\UserBinanceStatisticItem::add([
                'user_id' => $binance->id,
                'usd'     => $sum,
            ]);
            self::log('user='.$binance->id.' sum='.$sum);
        }
    }

    /**
     * Возвращает сумму портфеля
     *
     * @param \common\models\avatar\UserBinance $userBinance
     *
     * @return float
     * @throws
     */
    private function getUser($userBinance)
    {
        $binanceApi = new \avatar\modules\Binance\UserBinanceApi([
            '_user'     => $this,
            'apiKey'    => $userBinance->api_key,
            'apiSecret' => $userBinance->api_secret,
        ]);

        $accountData = $binanceApi->account();
        $balances = $accountData['balances'];
        $sum = 0;
        foreach ($balances as $item) {
            if ($item['asset'] == 'ETF') continue;
            if ($item['free'] != 0 || $item['locked'] != 0) {
                $amount = $item['locked'] + $item['free'];
                $item['all'] = $amount;

                try {
                    $item['usd'] = \common\models\avatar\Currency::convert($amount, $item['asset'], 'USD');
                } catch (\Exception $e) {
                    $item['usd'] = 0;
                }
                $sum += $item['usd'];
            }
        }

        return $sum;
    }
}