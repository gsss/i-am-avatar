<?php

namespace console\controllers;

use common\models\avatar\Currency;
use common\models\Config;
use common\models\CryptoCap;
use cs\services\VarDumper;
use yii\console\Controller;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;
use yii\httpclient\Client;

/**
 * Test controller
 */
class MoneyRateController extends \console\base\Controller
{
    const RATE_BTC_USD = 'rate.BTC/USD';
    const RATE_BTC_RUB = 'rate.BTC/RUB';

    /**
     * Обновляет графики курс биткойна, AllMarket
     */
    public function actionUpdate()
    {
        // Сохраняю курс AllMarket
        $this->saveCryptoCap();
    }

    public function actionUpdate3()
    {
        $this->CoinMarketCapUpdateFirstPage();
    }

    public function actionFlush()
    {
        \Yii::$app->cache->flush();
    }

    /**
     * Обновляет все основыне валюты
     */
    public function actionUpdate2()
    {
        try {
            // Обновляю курсы для USD EUR
            $url = 'https://min-api.cryptocompare.com';

            $client = new \yii\httpclient\Client(['baseUrl' => $url]);
            $response = $client->get('data/price', [
                'fsym'  => 'USD',
                'tsyms' => 'RUB,EUR',
            ])->send();
            /** @var array $data
             * {
             * "RUB": 58.69,
             * "EUR": 0.8333
             * }
             */
            $data = \yii\helpers\Json::decode($response->content);

            $b = Currency::findOne(['code' => 'USD']);
            $b->kurs = $data['RUB'];
            $b->save();
            $b = Currency::findOne(['code' => 'EUR']);
            $b->kurs = $data['RUB'] / $data['EUR'];
            $b->save();

            // Сохраняю Крипто маркет
            $this->CoinMarketCapUpdate();

            // очищаю кеш
            Currency::clearCache();

            // Сохраняю в память
            $btc = Currency::getRate('BTC');
            $usd = Currency::getRate('USD');
            Config::set(self::RATE_BTC_RUB, $btc);
            Config::set(self::RATE_BTC_USD, $btc / $usd);

            Config::set('kurs-update-time', time());

        }catch (\Exception $e) {
            \Yii::error($e->getMessage(), 'avatar\console\controllers\MoneyRateController::actionUpdate2');
            \Yii::error($e->getTrace(), 'avatar\console\controllers\MoneyRateController::actionUpdate2');
            echo $e->getMessage();
        }

    }

    /**
     * Выдает капитализацию криптовалют в млрд
     * @return float
     */
    private function getCryptoCap()
    {
        $url = "http://socket.coincap.io/front";
        $client = new Client();
        $response = $client->get($url, null)->send();
        $sum = 0;

        foreach (Json::decode($response->content) as $currency) {
            $s  = ArrayHelper::getValue($currency, 'mktcap');
            if ($s != 'NaN') $sum += $s;
        }

        return $sum / (1000 * 1000 * 1000);
    }

    /**
     * Обновляет курсы всех валют кроме 'RUB', 'USD', 'EUR'
     * @return bool
     */
    private function CoinMarketCapUpdate()
    {
        // Кол-во перебираемых страниц
        $max = 15;

        for ($page = 1; $page <= $max; $page++) {
            self::log('coinmarketcap page=' . $page);
            $this->CoinMarketCapUpdatePage($page);
        }

        return true;
    }

    /**
     * Обновляет курсы всех валют кроме 'RUB', 'USD', 'EUR'
     * @return bool
     */
    private function CoinMarketCapUpdatePage($page)
    {
        $client = new Client();
        $response = $client->get('https://api.coinmarketcap.com/v1/ticker', ['convert' => 'RUB', 'start' => 100 * ($page - 1)])->send();
        if (!in_array($response->headers->get('http-code'), [200, 301])) {
            return false;
        }
        $data = Json::decode($response->content);

        $currencyList = Currency::find()
            ->where(['not in', 'code', ['RUB', 'USD', 'EUR']])
            ->andWhere(['not', ['code_coinmarketcap' => '']])
            ->all();

        /** @var \common\models\avatar\Currency $currency */
        foreach ($currencyList as $currency) {
            foreach ($data as $CoinCapCurrency) {
                if ($currency->id_coinmarketcap) {
                    if ($CoinCapCurrency['id'] == $currency->id_coinmarketcap) {
                        $this->save($currency, $CoinCapCurrency);
                        continue;
                    }
                } else {
                    if (isset($CoinCapCurrency['symbol'])) {
                        if ($currency->code_coinmarketcap == $CoinCapCurrency['symbol']) {
                            $this->save($currency, $CoinCapCurrency);
                            continue;
                        }
                    } else {
                        \Yii::warning('No item SYMBOL '. \yii\helpers\VarDumper::dumpAsString([$data,$response]), 'avatar\console\controllers\MoneyRateController::CoinMarketCapUpdatePage');
                        self::log(\yii\helpers\VarDumper::dumpAsString([$data,$response]));
                    }
                }
            }
        }

        return true;
    }

    /**
     * @param \common\models\avatar\Currency    $currencyAv
     * @param array                             $currencyCmc
     */
    private function save($currencyAv, $currencyCmc)
    {
        $currency = $currencyAv;
        $CoinCapCurrency = $currencyCmc;
        $currency->kurs = $CoinCapCurrency['price_rub'];
        $currency->chanche_1h = $CoinCapCurrency['percent_change_1h'];
        $currency->chanche_1d = $CoinCapCurrency['percent_change_24h'];
        $currency->chanche_1w = $CoinCapCurrency['percent_change_7d'];
        $currency->day_volume_usd = $CoinCapCurrency['24h_volume_usd'];
        $currency->market_cap_usd = $CoinCapCurrency['market_cap_usd'];
        $currency->available_supply = $CoinCapCurrency['available_supply'];
        $currency->total_supply = $CoinCapCurrency['total_supply'];
        $currency->max_supply = $CoinCapCurrency['max_supply'];
        $currency->rank = $CoinCapCurrency['rank'];
        $currency->price_rub = $CoinCapCurrency['price_rub'];
        $currency->price_usd = $CoinCapCurrency['price_usd'];
        $currency->price_btc = $CoinCapCurrency['price_btc'];
        $currency->id_coinmarketcap = $CoinCapCurrency['id'];
        $ret = $currency->save();
        if (!$ret) \Yii::warning('not save c='.$currency->code , 'avatar\console\controllers\MoneyRateController::CoinMarketCapUpdatePage');

        // Сохраняю график
        $ret = \common\models\ChartPoint::add([
            'currency_id' => $currency->id,
            'rub' => $CoinCapCurrency['price_rub'],
            'usd' => $CoinCapCurrency['price_usd'],
            'btc' => $CoinCapCurrency['price_btc'],
        ]);
    }

    /**
     * Обновляет курсы всех валют кроме 'RUB', 'USD', 'EUR'
     * @return bool
     */
    private function CoinMarketCapUpdateFirstPage()
    {
        $client = new Client();
        $response = $client->get('https://api.coinmarketcap.com/v1/ticker', ['convert' => 'RUB'])->send();
        if (!in_array($response->headers->get('http-code'), [200, 301])) {
            return false;
        }
        $data = Json::decode($response->content);

        $currencyList = Currency::find()
            ->where(['is_crypto' => 1])
            ->andWhere(['not', ['code_coinmarketcap' => '']])
            ->all();

        foreach ($data as $CoinCapCurrency) {
            $isFind = false;
            /** @var \common\models\avatar\Currency $currency */
            foreach ($currencyList as $currency) {
                if (isset($CoinCapCurrency['symbol'])) {
                    if ($currency->code_coinmarketcap == $CoinCapCurrency['symbol']) {
                        $isFind = true;
                        break;
                    }
                }
            }

            if (!$isFind) {
                $currency = new Currency([
                    'code'               => $CoinCapCurrency['symbol'],
                    'title'              => $CoinCapCurrency['name'],
                    'kurs'               => $CoinCapCurrency['price_rub'],
                    'code_coinmarketcap' => $CoinCapCurrency['symbol'],
                    'is_crypto'          => 1,
                    'chanche_1h'         => $CoinCapCurrency['percent_change_1h'],
                    'chanche_1d'         => $CoinCapCurrency['percent_change_24h'],
                    'chanche_1w'         => $CoinCapCurrency['percent_change_7d'],
                    'day_volume_usd'     => $CoinCapCurrency['24h_volume_usd'],
                    'market_cap_usd'     => $CoinCapCurrency['market_cap_usd'],
                    'available_supply'   => $CoinCapCurrency['available_supply'],
                    'total_supply'       => $CoinCapCurrency['total_supply'],
                    'max_supply'         => $CoinCapCurrency['max_supply'],
                    'rank'               => $CoinCapCurrency['rank'],
                    'price_rub'          => $CoinCapCurrency['price_rub'],
                    'price_usd'          => $CoinCapCurrency['price_usd'],
                    'price_btc'          => $CoinCapCurrency['price_btc'],
                ]);
                $ret = $currency->save();

                if (!$ret) \Yii::warning('not save c='.$currency->code , 'avatar\console\controllers\MoneyRateController::CoinMarketCapUpdateFirstPage');

                // Сохраняю график
                $ret = \common\models\ChartPoint::add([
                    'currency_id' => $currency::getDb()->lastInsertID,
                    'rub'         => $CoinCapCurrency['price_rub'],
                    'usd'         => $CoinCapCurrency['price_usd'],
                    'btc'         => $CoinCapCurrency['price_btc'],
                ]);
                self::log('added ' . $CoinCapCurrency['symbol']);
            }

        }



        return true;
    }

    /**
     * Сохраняет значение криптомаркета в таблицу
     */
    private function saveCryptoCap()
    {
        $o = new CryptoCap([
            'value' => \Yii::$app->formatter->asDecimal($this->getCryptoCap(), 2),
            'time'  => time(),
        ]);
        $o->save();
    }

    private function saveCharts()
    {
    }


}