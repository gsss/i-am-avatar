<?php
/**
 * Created by PhpStorm.
 * User: s.arhangelskiy
 * Date: 29.03.2017
 * Time: 17:59
 */

namespace console\controllers;


use common\components\Monitoring;
use common\components\OnlineManager;
use common\components\providers\ETH;
use common\models\avatar\UserBinance;
use common\models\LogRequest;
use common\models\Server;
use common\models\statistic\PingProd;
use common\models\statistic\PingStatus;
use common\models\statistic\PingTest;
use common\models\statistic\ServerRequest;
use common\models\statistic\StatisticDb;
use common\models\statistic\StatisticOnline;
use common\models\statistic\StatisticRoute;
use cs\services\VarDumper;
use yii\helpers\ArrayHelper;
use yii\helpers\FileHelper;
use yii\helpers\Json;
use yii\httpclient\Client;

class MigrationsController extends \console\base\Controller
{

    /**
     * Записывает кол-во пользователей online каждую минуту в таблицу statistic_online
     */
    public function actionLast()
    {
        $path = \Yii::getAlias('@console/migrations');
        $d =  FileHelper::findFiles($path);
        $rows = [];
        foreach ($d as $i) {
            $rows[] = substr($i, strlen($path) + 1);
        }
        sort($rows);
//        \Yii::$app->request->params[1];

        echo $rows[count($rows) - 1]; exit();
    }

}