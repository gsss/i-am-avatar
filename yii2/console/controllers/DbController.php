<?php

namespace console\controllers;

use avatar\models\forms\school\Certificate;
use avatar\models\forms\school\Lesson;
use common\models\Anketa;
use common\models\avatar\UserBill;
use common\models\blog\Article;
use common\models\BlogItem;
use common\models\CardDesign;
use common\models\comment\Comment;
use common\models\comment\Type;
use common\models\Config;
use common\models\Language;
use common\models\language\Category;
use common\models\language\Message;
use common\models\language\SourceMessage;
use common\models\NewsItem;
use common\models\PaymentBitCoin;
use common\models\PaySystem;
use common\models\piramida\Transaction;
use common\models\piramida\Wallet;
use common\models\rbac\AuthAssignment;
use common\models\rbac\AuthItemChild;
use common\models\school\Kurs;
use common\models\school\LessonPotokState;
use common\models\school\Potok;
use common\models\school\PotokUser3Link;
use common\models\school\PotokUserExt;
use common\models\school\PotokUserLink;
use common\models\school\School;
use common\models\school\Sertificate;
use common\models\school\UserLink;
use common\models\SchoolCoin;
use common\models\shop\Product;
use common\models\User2;
use common\models\UserAvatar;
use common\models\UserRoot;
use common\models\UserWallet;
use cs\Application;
use cs\services\File;
use cs\Widget\FileUpload3\FileUpload;
use Yii;
use yii\base\Exception;
use yii\db\Connection;
use yii\db\mssql\PDO;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;
use yii\helpers\StringHelper;
use yii\helpers\Url;
use yii\helpers\VarDumper;
use yii\httpclient\Client;
use yii\web\HttpException;
use yii\web\Request;


/**
 *
 * @package console\controllers
 */
class DbController extends \console\base\Controller
{

    public function actionCoinTest()
    {
        /** @var \common\models\school\School $s */
        foreach (School::find()->all() as $s) {
            if (!Application::isEmpty($s->wallet_id)) {
                $w = Wallet::findOne($s->wallet_id);
                $sc = SchoolCoin::findOne(['school_id' => $s->id, 'currency_id' => $w->currency_id]);
                if (is_null($sc)) {
                    $sc = new SchoolCoin([
                        'school_id'   => $s->id,
                        'currency_id' => $w->currency_id,
                        'wallet_id'   => $s->wallet_id,
                    ]);
                    $sc->save();
                    echo 's=' . $s->id . ' new' . "\n";
                } else {
                    if (Application::isEmpty($sc->wallet_id)) {
                        $sc->wallet_id = $s->wallet_id;
                        $sc->save();
                        echo 's=' . $s->id . ' update' . "\n";
                    } else {
                        echo 's=' . $s->id . ' good' . "\n";
                    }
                }
            }
        }
    }

    /**
     */
    public function actionInstall()
    {
        $content = [];

        $data = file_get_contents(Yii::getAlias('@common/config/db.json'));
        $data = Json::decode($data);
        foreach ($data as $db) {
            $dbComponentName = $db['component'];

            Yii::$app->dbInfo->createCommand('create database ' . $db['name'] . ';')->execute();

            foreach ($db['tables'] as $table) {
                $t1 = microtime(true);
                $columns = [];
                foreach ($table['columns'] as $c) {
                    if ($c[1] == 'id') continue;
                    $item = [];
                    $item[] = '`' . $c[1] . '`';
                    $item[] = $c[2];
                    // COLUMN_DEFAULT
                    $default = $c[4] == null ? 'not null' : 'default ' . $c[4];
                    // IS_NULLABLE
                    $item[] = $c[3] == 'YES' ? 'null' : $default;
                    $item[] = ',';
                    $columns[] = join(' ', $item);
                }
                $sql = 'CREATE TABLE `' . $table['table'] . '` (
    `id` int(11) NOT NULL AUTO_INCREMENT,
' .

                    join("\n", $columns) .

                    '
    PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;';
                $content[] = $sql;
                Yii::$app->$dbComponentName->createCommand($sql)->execute();
                echo 'created ' . $table['table'] . ' t=' . Yii::$app->formatter->asDecimal(microtime(true) - $t1, 3) . "\n";
            }
        }
    }

    /**
     */
    public function actionSaveSql()
    {
        $content = [];

        $data = file_get_contents(Yii::getAlias('@common/config/db.json'));
        $data = Json::decode($data);
        foreach ($data as $db) {
            $content[] = 'use ' . $db['name'] . ';';
            foreach ($db['tables'] as $table) {
                $columns = [];
                foreach ($table['columns'] as $c) {
                    if ($c[1] == 'id') continue;
                    $item = [];
                    $item[] = '`' . $c[1] . '`';
                    $item[] = $c[2];
                    // COLUMN_DEFAULT
                    $default = $c[4] == null ? 'not null' : 'default ' . $c[4];
                    // IS_NULLABLE
                    $item[] = $c[3] == 'YES' ? 'null' : $default;
                    $item[] = ',';
                    $columns[] = join(' ', $item);
                }
                $sql = 'CREATE TABLE `' . $table['table'] . '` (
`id` int(11) NOT NULL AUTO_INCREMENT,
' .

                    join("\n", $columns) .

                    '
PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;';
                $content[] = $sql;
            }
        }
        $f = Yii::getAlias('@common/config/db.sql');
        if (file_exists($f)) {
            unlink($f);
        }
        file_put_contents($f, join("\n", $content));

        echo 'saved @common/config/db.sql' . "\n";
    }

    public function actionSave()
    {
        $db = [
            'i_am_avatar_prod_main'   => 'db',
            'i_am_avatar_prod_stat'   => 'dbStatistic',
            'i_am_avatar_prod_wallet' => 'dbWallet',
        ];
        $dbList = [];

        $rows1 = \common\models\information_schema\Tables::find()
            ->select([
                'TABLE_SCHEMA',
            ])
            ->groupBy(['TABLE_SCHEMA'])
            ->asArray()
            ->all();

        foreach ($rows1 as $item) {
            $dbName = $item['TABLE_SCHEMA'];

            if (isset($db[$dbName])) {
                $dbItem = [
                    'name'      => $dbName,
                    'component' => $db[$dbName],
                ];
                $rows2 = \common\models\information_schema\Tables::find()
                    ->select([
                        'TABLE_NAME',
                    ])
                    ->where(['TABLE_SCHEMA' => $dbName])
                    ->groupBy(['TABLE_NAME'])
                    ->asArray()
                    ->all();
                $rows3 = [];
                foreach ($rows2 as $e) {
                    $rows3[] = [
                        'table'   => $e['TABLE_NAME'],
                        'columns' => ArrayHelper::map(\common\models\information_schema\Column::find()
                            ->where([
                                'TABLE_SCHEMA' => $dbName,
                                'TABLE_NAME'   => $e['TABLE_NAME'],
                            ])
                            ->select([
                                'ORDINAL_POSITION',
                                'COLUMN_NAME',
                                'COLUMN_TYPE',
                                'IS_NULLABLE',
                                'COLUMN_DEFAULT',
                            ])
                            ->asArray()
                            ->all(),
                            'ORDINAL_POSITION',
                            function ($i) {
                                return array_values($i);
                            }
                        )
                    ];
                }
                $dbItem['tables'] = $rows3;
                $dbList[] = $dbItem;
            }
        }
        $f = Yii::getAlias('@common/config/db.json');
        if (file_exists($f)) {
            unlink($f);
        }
        file_put_contents($f, Json::encode($dbList));

        echo 'saved @common/config/db.json' . "\n";
    }

    /**
     * Загружает в БД первичную минимально необходимую информацию
     */
    public function actionImportData()
    {
        $data = file_get_contents(Yii::getAlias('@common/config/db-data.json'));
        $data = Json::decode($data);
        $dbList = [
            [
                'name'      => 'i_am_avatar_prod_main',
                'component' => 'db',
                'tables'    => [
                    'school',
                    'school_page',
                    'school_page_block',
                    'school_page_block_content',
                    'languages',
                ],
            ]
        ];
        $dbList = [];
        foreach ($data as $db) {
            $component = $db['component'];
            foreach ($db['tables'] as $table) {

                $tableName = $table['name'];
                $tableRows = $table['rows'];

                $sql = 'delete from `' . $tableName;
                Yii::$app->$component->createCommand($sql);

                foreach ($tableRows as $row) {
                    $columns = array_keys($row);
                    $columns2 = [];
                    foreach ($columns as $c) {
                        $columns2[] = '`' . $c . '`';
                    }

                    $values = array_values($row);
                    $values2 = [];
                    foreach ($values as $v1) {
                        if (is_null($v1)) {
                            $v = 'null';
                        }
                        if (is_numeric($v1)) {
                            $v = $v1;
                        }
                        if (is_string($v1)) {
                            $v = \Yii::$app->db->quoteValue($v1);
                        }
                        $values2[] = $v;
                    }
                    $sql = 'insert into `' . $tableName . '` (' . join(',', $columns2) . ') values (' . join(',', $values2) . ');';
                    echo substr($sql, 0, 100) . "\n";
                    Yii::$app->$component->createCommand($sql)->execute();
                }
            }
        }

        //echo 'loaded @common/config/db-data.json'."\n";
    }

    /**
     * Загружает в БД первичную минимально необходимую информацию
     */
    public function actionExportData()
    {
        $dbList = [
            [
                'name'      => 'i_am_avatar_prod_main',
                'component' => 'db',
                'tables'    => [
                    'school',
                    'school_page',
                    'school_page_block',
                    'school_page_block_content',
                    'languages',
                ],
            ]
        ];

        $dbListNew = [];
        foreach ($dbList as $db) {
            $tables = [];
            $component = $db['component'];
            foreach ($db['tables'] as $table) {
                $rows = Yii::$app->$component->createCommand('select * from ' . $table)->queryAll();
                $item = [
                    'name' => $table,
                    'rows' => $rows,
                ];
                $tables[] = $item;
            }
            $dbNew = [
                'name'      => $db['name'],
                'component' => $db['component'],
                'tables'    => $tables,
            ];
            $dbListNew[] = $dbNew;
        }

        $f = Yii::getAlias('@common/config/db-data.json');
        if (file_exists($f)) {
            unlink($f);
        }
        file_put_contents($f, Json::encode($dbListNew));

        echo 'loaded @common/config/db-data.json' . "\n";
    }

}
