<?php

namespace console\controllers;

use app\models\ActionsRange;
use app\models\Certificates;
use app\models\CertificatesDocuments;
use backend\components\Controller;
use backend\models\PaymentSystemsForm;
use common\extensions\KendoAdapter\KendoDataProvider;
use common\models\Anketa;
use common\models\avatar\UserBill;
use common\models\comment\Comment;
use common\models\comment\Type;
use common\models\Config;
use common\models\Currency;
use common\models\CurrencyLog;
use common\models\Language;
use common\models\language\Category;
use common\models\language\Message;
use common\models\language\SourceMessage;
use common\models\Orders;
use common\models\PaymentBitCoin;
use common\models\PaymentSystem;
use common\models\piramida\Wallet;
use common\models\rbac\AuthAssignment;
use common\models\rbac\AuthItemChild;
use common\models\school\Kurs;
use common\models\school\Potok;
use common\models\school\PotokUser3Link;
use common\models\school\PotokUserExt;
use common\models\school\PotokUserLink;
use common\models\school\UserLink;
use common\models\User2;
use common\models\UserAvatar;
use common\models\UserRoot;
use common\models\UserWallet;
use common\services\Debugger;
use frontend\base\JsonController;
use GuzzleHttp\Query;
use Yii;
use yii\base\Exception;
use yii\db\Connection;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\helpers\VarDumper;
use yii\httpclient\Client;
use yii\web\HttpException;
use common\models\User;
use common\extensions\KendoAdapter\KendoFiltersCollection;
use app\models\CrmLog;
use common\models\Billing;
use common\models\Banners;
use app\models\UserPaymentAccountsTypes;
use common\models\SmartresponderHistory;
use yii\web\Request;


/**
 *
 * @package console\controllers
 */
class WalletsController extends \console\base\Controller
{


    public function actionFindEmptyBills()
    {
        $data = UserBill::find()->all();

        /** @var UserBill $row */
        foreach ($data as $row) {

            try {
                $b = UserAvatar::findOne($row->user_id);
//                echo 'id = ' . $row->id . ' exist' . "\n";
            } catch (\Exception $e) {
                //$row->delete();
                $w = Wallet::findOne($row->address);
                if (is_null($w)) {
                    echo 'wid = ' . $row->address . ' not found' . "\n";
                }
                echo 'id = ' . $row->id . ' not found' . "\n";
            }
        }
    }
}
