<?php

namespace console\controllers;

use app\models\ActionsRange;
use app\models\Certificates;
use app\models\CertificatesDocuments;
use backend\components\Controller;
use backend\models\PaymentSystemsForm;
use common\extensions\KendoAdapter\KendoDataProvider;
use common\models\Config;
use common\models\Currency;
use common\models\CurrencyLog;
use common\models\Language;
use common\models\language\Category;
use common\models\language\Message;
use common\models\language\SourceMessage;
use common\models\Orders;
use common\models\PaymentBitCoin;
use common\models\PaymentSystem;
use common\models\rbac\AuthAssignment;
use common\models\rbac\AuthItemChild;
use common\services\Debugger;
use frontend\base\JsonController;
use GuzzleHttp\Query;
use Yii;
use yii\base\Exception;
use yii\db\Connection;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\helpers\VarDumper;
use yii\httpclient\Client;
use yii\web\HttpException;
use common\models\User;
use common\extensions\KendoAdapter\KendoFiltersCollection;
use app\models\CrmLog;
use common\models\Billing;
use common\models\Banners;
use app\models\UserPaymentAccountsTypes;
use common\models\SmartresponderHistory;
use yii\web\Request;


/**
 * Выполняет воссоздание базы данных с оригинала
 *
 * Class SyncController
 * @package console\controllers
 */
class SyncController extends \console\base\Controller
{
    public $origin = 'https://www.avatar-bank.com';

    public $key = 'YIslvjZCMicGwFiN7ClX762lusQ0451M';

    public $updateData = [
        'bigData' => [
//            'bill_codes',
            'user_bill',
//            'user',
        ],
        'other' => [
            'bill_codes',
//            'bill_merchant',
//            'config',
//            'currency',
//            'log',
//            'migration',
//            'nw_account',
//            'nw_billing',
//            'nw_bit_coin_account',
//            'nw_bit_coin_account_addresses',
//            'nw_bit_coin_kurs',
//            'nw_currency',
//            'nw_in_requests',
//            'nw_move',
//            'nw_operations',
//            'nw_out_requests',
//            'nw_out_requests_messages',
//            'nw_referal_bonus',
//            'nw_transactions',
//            'paysystems',
//            'paysystems_config',
//            'paysystems_config_list',
//            'piramida_in_requests',
//            'piramida_in_requests_messages',
//            'send_letter',
//            'user',
//            'user_bill',
//            'user_bill_address',
//            'user_bill_config',
//            'user_bill_operation',
//            'user_bill_transaction',
//            'user_login',
//            'user_recover',
//            'user_register',
        ]
    ];

    public function actionUpdate()
    {
        $t1 = microtime(true);

        $options = [
            'isReCreateTable' => true,
            'itemsPerPage'    => 100,
        ];
        $options['key'] = $this->key;
        $except = [
        ];
        $tables = $this->updateData['bigData'];
        $client = new Client(['baseUrl' => $this->origin]);
        if (count($tables) > 0) {
            foreach ($tables as $table) {
                if (!in_array($table, $except)) {
                    $t = microtime(true);
                    $this->clearTable($table);
                    $this->updateTable($client, $table, $options);
                    self::logTime($table . ' ' . 'success. t = ', $t);
                }
            }
        } else {
            $response = $client->get('sync/tables')->send();
            $ret = $response->data;
            if ($ret['status']) {
                $data = $ret['data'];
                foreach ($data as $table) {
                    if (!in_array($table, $except)) {
                        $t = microtime(true);
                        $this->clearTable($table);
                        $this->updateTable($client, $table, $options);
                        self::logTime($table . ' ' . 'success. t = ', $t);
                    }
                }
            } else {
                throw new Exception('нет данных');
            }
        }

        self::successTime('All done. t = ', $t1);
    }

    /**
     * Загружает все даннные в таблицу
     *
     * @param \yii\httpclient\Client $client
     * @param string $name название таблицы
     */
    private function updateTable($client, $name, $options)
    {
        $page = 1;
        $c = 0;
        do {
            try {
                $t = microtime(true);
                // получаю порцию данных
                $response = $client->get('sync/table', [
                    'name'         => $name,
                    'itemsPerPage' => $options['itemsPerPage'],
                    'page'         => $page,
                ])->send();
                $ret = $response->data;
                if ($ret['success']) {
                    $data = $ret['data'];
                    $columns = array_keys($data['columns']);
                    if (count($data['rows']) > 0) {
                        (new \yii\db\Query())->createCommand()->batchInsert($name, $columns, $data['rows'])->execute();
                    }
                    $page++;
                    if (count($data['rows']) == $options['itemsPerPage']) {
                        $c += $options['itemsPerPage'];
                        self::logTime($c . ' t = ', $t);
                    } else {
                        $c += count($data['rows']);
                        self::logTime($c . ' t = ', $t);
                    }
                } else {
                    self::logDanger('Error while import table = ' . $name);
                    break;
                }
            } catch (\Exception $e) {
                $c += $options['itemsPerPage'];
                self::logDanger('Error '. $e->getMessage());
                break;
            }
        } while (count($data['rows']) == $options['itemsPerPage']);
    }

    /**
     * Очищает таблицу
     *
     * @param string $name название таблицы
     *
     * @return int кол-во удаленных строк
     */
    private function clearTable($name)
    {
        $n = (new \yii\db\Query())->createCommand()->delete($name)->execute();
        self::log('Clear complete. Table = ' . $name);

        return $n;
    }

    /**
     * Удаляет таблицу если существует
     * Создает таблицу
     * @param \yii\db\Connection $db
     * @param array $data
     * [
     *
     * ]
     *
     * @return bool
     */
    public function createTable($db, $data)
    {
        $tables = $db->schema->getTableNames();

        if (true) {
            // очищаю таблицу
            (new \yii\db\Query())->createCommand($db)->delete($data['name'], null)->execute();
        } else {
            // удаляю таблицу
            if (in_array($data['name'], $tables)) {
                (new \yii\db\Query())->createCommand($db)->dropTable($data['name'])->execute();
            }
            // создаю таблицу
        }

        return true;
    }

    /**
     * Удаляет не нужные колонки
     * Создает недостающие
     *
     * @param \yii\db\Connection $db
     * @param array $data
     * [
     *
     * ]
     *
     * @return bool
     */
    private function syncColumns($db, $data)
    {

        return true;
    }

    public function getTables()
    {

    }

    public function actionUpdateFolders()
    {
        $folders = [
            '/verificate_scans',
            '/cabinet/web/uploads/user_images',
            '/cabinet/web/uploads/banners',
        ];

        foreach($folders as $folder) {
            $this->updateFolder($folder);
        }
    }

    /**
     * Затягивает папку себе
     *
     * @param string $folder путь к папке от корня проекта, например `/cabinet/web/uploads/banners`
     */
    private function updateFolder($folder)
    {
        $client = new Client(['baseUrl' => $this->origin]);

        $page = 1;
        $c = 0;
        $t = microtime(true);
        // получаю порцию данных
        $response = $this->send('sync/folder', [
            'path'         => $folder,
        ]);
        $ret = $response->data;
        if ($ret['status']) {
            $data = $ret['data'];
            $files = $data;
            $c = count($files);
            for ($i = 0; $i < $c; $i++) {
                $this->getFile($folder, $i);
            }
        } else {
            self::logDanger('Error. Coudnt import folder = ' . $folder);
            return;
        }
    }

    /**
     * Получает файл и сохранаяет в файловую систему
     *
     * @param $folder
     * @param $index
     */
    private function getFile($folder, $index)
    {
        $response = $this->send('sync/file', [
            'path'   => $folder,
            'offset' => $index,
        ]);
        $ret = $response->data;
        if ($ret['status']) {
            /**
             * {
             *      'name':     string - только имя
             *      'content':  array (base64) - закодированный в Base64 данные файла разбитые на строки по 1000 символов
             * }
             */
            $data = $ret['data'];
            $name = $data['name'];
            $contentArray = $data['content'];
            $contentString = '';
            foreach($contentArray as $row) {
                $contentString .= $row;
            }
            $content = base64_decode($contentString);
            $path = $folder . '/' . $name;
            $fullPath = Yii::getAlias('@cabinet/..'.$path);
            if (file_exists($fullPath)) {
                unlink($fullPath);
            }
            file_put_contents($fullPath, $content);
            $this->log('file created = ' . $path);
        } else {
            self::logDanger('Error. Coudnt get fileIndex = ' . $index);
            return;
        }
    }

    private function send($action, $data)
    {
        $client = new Client(['baseUrl' => $this->origin]);
        $data['key'] = $this->key;

        return $client->get($action, $data)->send();
    }
}
