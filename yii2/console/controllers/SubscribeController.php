<?php

namespace console\controllers;

use common\models\subscribe\Subscribe;
use common\models\subscribe\SubscribeMail;
use common\models\subscribe\SubscribeStat;
use common\models\UserAvatar;
use cs\Application;
use kartik\base\Config;
use yii\console\Controller;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\helpers\VarDumper;

/**
 * Занимается рассылкой писем
 */
class SubscribeController extends Controller
{
    public function dump($v)
    {
        VarDumper::dump($v,3);
        echo("\n");
        exit;
    }

    /**
     * Делает рассылку писем из списка рассылки
     */
    public function actionSend()
    {
        $limit = 8; // писем в минуту
        $limitPerDay = 1900; // писем в день

        $counterDay = \common\models\Config::get('mailCounterPerDay');
        if ($counterDay + $limit > $limitPerDay) {
            echo 'limit end $counterDay=' . $counterDay;
            echo "\n";
            return;
        }


        $time = microtime(true);
        $idsSuccess = [];
        $rows = SubscribeMail::find()
            ->limit($limit)
            ->select(['id', 'subscribe_id', 'mail'])
            ->all();
        if (count($rows) > 0) {
            $subscribeIds = ArrayHelper::map($rows, 'id', 'mail', 'subscribe_id');
            foreach ($subscribeIds as $subscribeId => $mailList) {
                /** @var Subscribe $s */
                $s = Subscribe::findOne($subscribeId);
                if ($s) {
                    foreach ($mailList as $id => $mail) {
                        $html = $s->html;
                        $pos = strpos($html, '{{link}}');
                        if ($pos !== false) {
                            $pos2 = strpos($html, '?', $pos);
                            $query1 = substr($html, $pos + 8, $pos2-$pos - 8);
                            $url = new \cs\services\Url('http://www.test.ru/?' . $query1);
                            $potok_id = $url->getParam('potok_id');
                            $sid = $url->getParam('sid');
                            $pass = \Yii::$app->params['subscribe_unsubscribe_pass'];
                            $query = 'sid' . '=' . $sid . '&' . 'email' . '=' . urlencode($mail) . '&' . 'password' . '=' . urlencode($pass);
                            $hash = md5($query);
                            $url2 = Url::to(['subscribe/unsubscribe3', 'sid' => $sid, 'email' => $mail, 'hash' => $hash], true);
                            $html = str_replace('{{link}}' . $query1 . '?', $url2, $html);
                        }

                        try {
                            if (!empty($s->from_email) && !empty($s->from_name)) {
                                $from = [$s->from_email => $s->from_name];
                            } else {
                                $from = \Yii::$app->params['mailer']['from'];
                            }

                            $mailO = \Yii::$app->mailer
                                ->compose()
                                ->setFrom($from)
                                ->setTo($mail)
                                ->setSubject($s->subject)
                                ->setHtmlBody($html);
                        } catch (\Exception $e) {
                            \Yii::info($e->getMessage(), 'avatar\\app\\commands\\SubscribeController::actionSend2');
                        }

                        echo $mail;
                        echo ' ';
                        try {
                            $result = $mailO->send();
                            if ($result == false) {
                                \Yii::info('Не удалось доствить: ' . $mail, 'avatar\\app\\commands\\SubscribeController::actionSend1');
                                echo 'Не удалось доствить: ' . $mail;
                            }
                        } catch (\Exception $e) {
                            \Yii::info('Не удалось доствить: ' . $mail . ' ' . $e->getMessage(), 'avatar\\app\\commands\\SubscribeController::actionSend2');
                            //echo $e->getMessage();

                            // тут обработчик
                            $this->sendError('письмо to:' . $mail . "\n" . $e->getMessage());
                        }
                        echo "\n";
                        $idsSuccess[] = $id;
                    }
                } else {
                    SubscribeMail::deleteAll(['subscribe_id' => $subscribeId]);
                }
            }
            (new SubscribeStat([
                'time'    => time(),
                'counter' => count($idsSuccess),
                'delay'   => microtime(true) - $time,
            ]))->save();

            // Удаляю все старые письма
            SubscribeMail::deleteAll(['in', 'id', $idsSuccess]);

            // Устанавливаю счетчик отправленных писем
            \common\models\Config::set('mailCounterPerDay', $counterDay + count($idsSuccess));

        } else {
            Subscribe::deleteAll();
        }

    }

    private function sendError($text)
    {
        /** @var \aki\telegram\Telegram $telegram */
        $telegram = \Yii::$app->telegram;

        $users = \Yii::$app->authManager->getUserIdsByRole('role_admin');
        foreach ($users as $uid) {
            $u = UserAvatar::findOne($uid);
            if (!Application::isEmpty($u->telegram_chat_id)) {
                $telegram->sendMessage([
                    'chat_id' => $u->telegram_chat_id,
                    'text'    => $text
                ]);
            }
        }
    }

    public function actionPhone()
    {
        $data = [
            '79814501189',
            '380501818024',
            '79139499224',
            '87055417700',
            '89194516771',
            '79283510784',
            '375447375042',
            '79296042166',
            '89055539641',
            '89247458555',
            '79037244222',
            '375339039116',
            '380984987037',
            '380713245479',
            '79175258391',
            '79098447160',
            '79996088055',
            '79113110032',
            '79113190641',
            '77018090111',
            '79292222257',
            '380685131769',
            '79036542023',
            '89819896103',
            '79183309202',
            '89851260567',
            '89883450998',
            '79501077223',
            '375291010376',
            '89278333053',
            '89852802480',
            '89130815528',
            '79524469490',
            '79500104811',
            '79500104811',
            '89150643786',
            '89017049022',
            '79174998330',
            '89641190156',
            '79256178667',
            '89109581734',
            '79229513921',
            '89069217733',
            '79882373267',
            '89227774844',
            '37368680505',
            '79163545623',
            '79687144603',
            '79687144603',
            '359878966690',
            '380936743894',
            '79787339374',
            '77073641391',
            '380997377703',
            '89260375917',
            '89136020390',
            '79166145076',
            '79312507268',
            '87777648634',
            '89636471771',
            '89500188868',
            '79645092242',
        ];
        /** @var \common\components\sms\IqSms $sms */
        $sms = \Yii::$app->sms;
        foreach ($data as $item) {
            if (substr($item,0,1) ==  '8') {
                $item = '7' . substr( $item,1);
            } else {
                echo $item;
                $response = $sms->send($item, 'Начало вебинара в 21:21 МСК по ссылке https://www.youtube.com/watch?v=bsScJeQdj4E', 'Avatar');
                echo ' ';
                echo $response;
                echo "\n";
            }
        }
    }

    public function actionPhone2()
    {
        $data = [
            '89252374501',
        ];
        /** @var \common\components\sms\IqSms $sms */
        $sms = \Yii::$app->sms;
        foreach ($data as $item) {
            if (substr($item,0,1) ==  '8') {
                $item = '7' . substr( $item,1);
                echo $item;
                $response = $sms->send($item, 'Начало вебинара в 21:21 МСК по ссылке https://www.youtube.com/watch?v=bsScJeQdj4E', 'Avatar');
                echo ' ';
                echo $response;
                echo "\n";
            }
        }
    }

    public function actionGet()
    {
        echo \common\models\Config::get('mailCounterPerDay');
        echo "\n";
    }

    public function actionSetMax()
    {
        \common\models\Config::set('mailCounterPerDay', 1900);
        echo "success\n";
    }

    public function actionResetCounter()
    {
        \common\models\Config::set('mailCounterPerDay', 0);
        echo "success\n";
    }
}