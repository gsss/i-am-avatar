<?php
$params = array_merge(
    require(__DIR__ . '/../../common/config/params.php'),
    require(__DIR__ . '/params.php')
);

return [
    'id'                  => 'app-console',
    'basePath'            => dirname(__DIR__),
    'bootstrap'           => ['log'],
    'controllerNamespace' => 'console\controllers',
    'components'          => [
        'urlManager' => [
            'baseUrl' => env('URL_MANAGER_HOST_INFO'),
        ],
        'log'              => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets'    => [
                [
                    'class'       => 'common\services\DbTarget',
                    'levels'      => [
                        'warning',
                        'error',
                    ],
                    'except'      => [
                        'yii\web\HttpException:404',
                        'yii\web\HttpException:403',
                        'cs\web\Exception',
                    ],

                    'db'         => 'dbStatistic',
                    'logTable'   => 'log2_console',
                ],
                [
                    'class'      => 'common\services\DbTarget',
                    'categories' => ['avatar\\*'],
                    'levels'     => [
                        'info',
                        'trace',
                    ],

                    'db'         => 'dbStatistic',
                    'logTable'   => 'log2_console',
                ],
            ],
        ],
    ],
    'modules'                                         => [
        'sroutes'  => ['class' => '\school\modules\sroutes\Module'],
    ],
    'params'              => $params,
];
