<?php
/**
 * Created by PhpStorm.
 * User: Ra-m-ha
 * Date: 25.07.2019
 * Time: 0:41
 */
namespace avatar\base;


class Model extends \yii\base\Model
{
    public $attrList = [];

    public function __get($name)
    {
        if (!isset($this->attrList[$name])) return null;

        return $this->attrList[$name];
    }

    public function __set($name, $value)
    {
        $this->attrList[$name] = $value;
    }
}