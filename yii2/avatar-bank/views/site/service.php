<?php
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
$this->title = 'Сервис';

?>

<div class="container" style="padding-bottom: 70px;">
    <div class="col-lg-12">
        <h1 class="page-header text-center"><?= $this->title ?></h1>
    </div>
    <div class="col-lg-6 col-lg-offset-3">

        <p><a href="https://www.bhost.net/">https://www.bhost.net/</a> Хостинг</p>
        <p><a href="http://silviomoreto.github.io/bootstrap-select/">http://silviomoreto.github.io/bootstrap-select/</a> bootstrap-select</p>
    </div>
</div>


