<?php
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
$this->title = 'Avatar Network';

?>

<style>
    .download-section {
        width: 100%;
        padding: 50px 0;
        background: url('<?= \Yii::t('c.FzjNa3kr9t', 'Главная картинка') ?>') no-repeat center center scroll;
        background-color: #fff;
        -webkit-background-size: cover;
        -moz-background-size: cover;
        background-size: cover;
        -o-background-size: cover;
    }
</style>

<section id="contact" class="content-section text-center" style="">
    <div class="download-section" style="
    border-bottom: 1px solid #87aad0;
    border-top: 1px solid #87aad0;
    height: 900px;
    ">
        <div class="container">
            <div class="col-lg-8 col-lg-offset-2" style="color: #000000; margin-top: 100px;">
            </div>
        </div>
    </div>
</section>


<div class="container" style="padding-bottom: 70px;">
    <div class="col-lg-12">
        <p class="text-center" style="margin-top: 50px;"><img src="/images/controller/site/index/logobig.png"
                                                              style="" width="200"
                                                              title="<?= \Yii::t('c.FzjNa3kr9t', 'Аватар Банк') ?>"
                                                              data-toggle="tooltip"></p>

        <h1 class="page-header text-center"><?= \Yii::t('c.FzjNa3kr9t', 'Миссия') ?></h1>

    </div>

    <div class="row">
        <div class="col-lg-8 col-lg-offset-2">
            <p class="lead"><?= \Yii::t('c.FzjNa3kr9t', 'Способствовать позитивным переменам') ?></p>
            <p>
                <iframe width="100%" height="315" src="https://www.youtube.com/embed/<?= \Yii::t('c.FzjNa3kr9t', 'Видео') ?>" frameborder="0"
                        allowfullscreen></iframe>
            </p>
            <?php if (Yii::$app->user->isGuest) { ?>
                <p>
                    <a href="/auth/registration" class="btn btn-success btn-lg"
                       style="width: 100%;margin: 80px 0px 80px 0px;">Регистрация</a>
                </p>
            <?php } ?>
            <p class="lead"><?= \Yii::t('c.FzjNa3kr9t', 'Аватар Банк — возможность') ?></p>

        </div>
    </div>
    <div class="row" style="margin: 100px 0px 70px 0px;">
        <div class="col-lg-4">
            <a href="https://bitcoin.org/" target="_blank">
                <img src="/images/controller/site/index/bitcoin2.jpg" width="100%" class="thumbnail">
            </a>

            <p>Наша основа BlockChain. Это основа наших договоров!</p>
        </div>
        <div class="col-lg-4">

        </div>
        <div class="col-lg-4">
            <a href="https://ethereum.org/" target="_blank">
                <img src="/images/controller/site/index/ethereum_thumb800.png" width="100%" class="thumbnail">
            </a>

            <p>Умные контракты позволяют реализовать все возможности автономной цивилизации!</p>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12">
            <p class="text-center" style="margin: 30px 0px 30px 0px;">
                <a href="http://www.galaxysss.ru/newEarth/game" target="_blank">
                    <img alt=""
                         src="/images/controller/site/index/strategy.png"
                         style="width: 100%; max-width: 1200px; border-radius: 30px;"
                         title="Наша структура организации"
                         data-toggle="tooltip"
                    >
                </a>
            </p>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-8 col-lg-offset-2">
            <p>
                <b>Кошелек Аватара</b> – является счетом в банке, по которому ведется учет денежных единиц. Построен
                на системе Блокчейн Биткойна, Ethereum и пополняется с банкомата
                биткойн.
            </p>
            <p>
                <b>Платежные терминалы</b> – приложение для Android и iPad для создания кассы для магазина при
                помощи которой можно сканировать QR коды товаров. Формировать чек и проводить оплату с карты
                Аватара.
            </p>
            <p>
                <b>Банкоматы</b> – стандартные платежные терминалы приема денег с установленными сканерами QR
                кодов и своим программным обеспечением.
            </p>
            <p>
                <b>Карта Аватара</b> – пластиковая карта с QR-кодом, который является мульти-паспортом и предоставляет доступ к
                счетам разных валют.
            </p>
            <p>
                <b>Биржа</b> – предназначена как прослойка между старой финансовой системой для обмена фиатных
                валют в криптовалюты и обратно.
            </p>
            <p>
                <b>Мерчант</b> – платежные шлюзы интернет магазинов оплаты заказов со встроенным кошельком и системой безопасности.
            </p>
            <p>
                <b>Мультипасспорт</b> – Карта Аватара для идентификации в различных контрактах (SMART CONTRACT) и
                подписывания своих юридически значимых документов и договоров.
            </p>
            <p>
                <b>Готовые решения для бизнеса</b> – пакетные решения для большого бизнеса с аналитикой,
                бухгалтерии любых масштабов.
            </p>
            <p>
                Банк Аватар инcтрумент для построения Нового Общества. Это общество Свободных Творческих
                Людей живущих во имя Процветания Планеты Земля.
            </p>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <h2 class="page-header text-center">Наши услуги</h2>
        </div>
        <div class="col-lg-6">
            <h2 class="page-header text-center">Для частных лиц</h2>
            <p><a href="http://www.galaxysss.ru/shop/product/51" target="_blank">Получить карту</a> </p>
            <p><a href="/auth/registration">Открыть счет</a></p>
            <p><a href="/site/merchant">Принимать оплату за услуги и товары</a></p>
        </div>
        <div class="col-lg-6">
            <h2 class="page-header text-center">Для предпринимателей и организаций</h2>
            <p><a href="/site/merchant">Открыть мерчант  или платежный терминал</a> </p>
            <p>Получить консалтинговые услуги</p>
            <p>Провести ICO</p>
            <p>Перевести бизнес на умные контракты</p>
            <p>Открыть свободное децентрализованное сообщество-государство</p>
        </div>
    </div>


    <div class="row" style="margin: 100px 0px 70px 0px;">
        <h1 class="page-header text-center">Курс Биткойна к рублю</h1>
        <?php
        $period = 24 * 2;

        $rows = \common\models\piramida\BitCoinKurs::find()
            ->select([
                'rub',
                'time',
            ])
            ->where(['>', 'time', time() - (60 * 60 * 24 * 30)])
            ->all()
        ;
        $rowsJson = \yii\helpers\Json::encode($rows);
        $this->registerJs(<<<JS
        Highcharts.setOptions({
            global: {
                timezoneOffset: -3 * 60
            }
        });
        var rows = {$rowsJson};
        var newRows = [];
        for(i = 0; i < rows.length; i++)
        {
            var item = rows[i];
            newRows.push({
                x: new Date(item.time * 1000),
                y: item.rub
            });
        }
JS
        );
        ?>
        <?= \common\widgets\HighCharts\HighCharts::widget([
            'chartOptions' => [
                'chart'       => [
                    'zoomType' => 'x',
                    'type'     => 'spline',
                ],
                'title'       => [
                    'text' => 'График',
                ],
                'subtitle'    => [
                    'text' => 'Выделите область для изменения масштаба',
                ],
                'xAxis'       => [
                    'type' => 'datetime',
                ],
                'yAxis'       => [
                    [
                        'title' => [
                            'text' => 'Количество',
                        ],
                    ],
                ],
                'legend'      => [
                    'enabled' => true,
                ],
                'tooltip'     => [
                    'crosshairs' => true,
                    'shared'     => true,
                ],
                'plotOptions' => [
                    'series' => [
                        'turboThreshold' => 0,
                    ],
                ],
                'series'      => [
                    [
                        'type' => 'spline',
                        'name' => 'RUB',
                        'data' => new \yii\web\JsExpression('newRows'),
                    ],
                ],
            ],
        ]);
        ?>
    </div>

    <div class="row">
        <div class="col-lg-6 col-lg-offset-3">

            <?php if (Yii::$app->user->isGuest) { ?>
                <p><a
                            href="/auth/registration"
                            class="btn btn-success btn-lg"
                            style="width: 100%;margin: 80px 0px 80px 0px;">Регистрация</a></p>
            <?php } ?>

            <p class="text-center" style="margin: 30px 0px 30px 0px;">
                <a href="/card/index">
                    <img alt=""
                         src="/images/controller/landing/index/card/credit-card-avatar-7.jpg"
                         style="width: 100%; max-width: 400px; border-radius: 30px;"
                         title=""
                         data-toggle="tooltip"
                    >
                </a>
            </p>

            <p>
                <a href="http://www.galaxysss.ru/shop/product/51"
                  target="_blank"
                  class="btn btn-success"
                  style="width: 100%;">Купить</a>
            </p>
        </div>
    </div>


    <div class="row">
        <div class="col-lg-8 col-lg-offset-2">
            <center>
                <?= $this->render('../blocks/share', [
                    'image'       => Url::to('/images/share/image.jpg', true),
                    'title'       => 'AvatarNetwork',
                    'url'         => Url::current([], true),
                    'description' => 'Банк в блокчейн сети для криптовалют — это возможность построить более экономически справедливый и более технически совершенный мир. Безусловно, для существования подобного мира недостаточно одной лишь технологии: необходимо и достаточное количество людей, разделяющих трансперсональные ценности — не ради только себя, но и ради человечества как единого организма.',
                ]) ?>
            </center>
        </div>
    </div>
</div>


