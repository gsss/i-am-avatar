<?php
/**
 * Created by PhpStorm.
 * User: Metatron
 * Date: 22.12.2018
 * Time: 5:44
 */

/** @var \yii\web\View $this */
/** @var  $page \common\models\school\Page  */


?>

<?= $this->renderFile('@school/views/page/page.php', ['page' => $page]); ?>