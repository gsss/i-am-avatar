<?php
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
$this->title = 'Проект «Аватар»';

$images = [
    '15350558_1202189576533521_3548613474051056211_n.jpg',
    '15356486_1202189609866851_4049822702915408992_n.jpg',
    '15356544_1202189746533504_8129153023486549843_n.jpg',
    '15356758_1202189829866829_4074769233110517723_n.jpg',
    '15380464_1202189616533517_259634258170636560_n.jpg',
    '15390795_1202189573200188_318587594699491255_n.jpg',
    '15390968_1202189613200184_3540437716019904541_n.jpg',
    '15391020_1202189923200153_3467294403039526726_n.jpg',
    '15400506_1202189719866840_6682182747854654793_n.jpg',
    '15400939_1202189656533513_27877885749638836_n.jpg',
    '15492151_1202189893200156_5319517002346581692_n.jpg',
    '15492218_1202189653200180_7323576407327768082_n.jpg',
    '15541667_1202189899866822_2136885950258805506_n.jpg',
    '1.jpg',
    '2.jpg',
    '14.jpg',
    '1444274792248.png',
    'avatar.jpg',
    'avatar2.jpg',
    'avatar33.jpg',
    'DDm0Hhbe35w.jpg',
    '15384374_1197193457033133_5343240816671343108_o.jpg',
    '16179721_1246587668760378_7781595111301726776_o.jpg',
];

?>

<div class="container" style="padding-bottom: 70px;">
    <div class="row">
        <div class="col-lg-8 col-lg-offset-2">
            <h1 class="page-header text-center"><?= $this->title ?></h1>
            <h2 class="text-center page-header">
                <a href="http://www.i-am-avatar.com/" target="_blank">Школа Богов</a>. Проект Аватар. Я Есмь Творец
            </h2>
            <p class="text-center">
                <iframe width="100%" height="315" src="https://www.youtube.com/embed/AGvx9vhHbDk" frameborder="0" allowfullscreen></iframe>
            </p>
            <h2 class="text-center page-header">
                <a href="http://www.bog-dan.com" target="_blank">Авиалинии БогДан</a>. Проект Крылья Ангела
            </h2>
            <p class="text-center">
                <iframe src="//vk.com/video_ext.php?oid=168090163&id=456239051&hash=5c3edf856e69224c&hd=2" width="100%" height="320" frameborder="0" allowfullscreen></iframe>
            </p>
            <h2 class="text-center page-header">
                Задача Аватара Свободы
            </h2>
            <p class="text-center">
                <iframe width="100%" height="315" src="https://www.youtube.com/embed/u-b_I_UWSIM" frameborder="0" allowfullscreen></iframe>
            </p>
            <h2 class="text-center page-header">
                Кристаллическая Сапфировая Энергия Луча Аватара Свободы Альфа 16
            </h2>
            <p class="text-center">
                <iframe width="100%" height="315" src="https://www.youtube.com/embed/06xt9JjeI-Q" frameborder="0" allowfullscreen></iframe>
            </p>
            <h2 class="text-center page-header">
                Вход в нулевую Точку или Формула Творца
            </h2>
            <p class="text-center">
                <iframe width="100%" height="315" src="https://www.youtube.com/embed/M0IjVogEvp4" frameborder="0" allowfullscreen></iframe>
            </p>
            <h2 class="text-center page-header">
                Проект Новая Земля
            </h2>
            <p class="text-center">
                <iframe width="100%" height="315" src="https://www.youtube.com/embed/PNvcyTCC6Qs" frameborder="0" allowfullscreen></iframe>
            </p>
            <h2 class="text-center page-header">
                Древнейший язык планеты Земля
            </h2>
            <p class="text-center lead">
                МОДУЛЬ ИНОПЛАНЕТНЫХ ИМПЛАНТАТОВ<br>
                <a href="/images/controller/site/avatar/HGSAlian.pdf"><img src="/images/controller/site/avatar/pdf.gif" width="100"></a>
            </p>
            <p class="text-center">
                <a href="/images/controller/site/avatar/HGSAlian.pdf">HGS-Avatar.pdf</a>
            </p>
            <p class="text-center">
                <iframe width="100%" height="315" src="https://www.youtube.com/embed/I9ROIi4InfU" frameborder="0" allowfullscreen></iframe>
            </p>
            <div class="text-center" style="margin-bottom: 40px;margin-top: 40px;">
                <a href="http://www.galaxysss.ru/category/semya/article/2016/01/20/soznanie_avatara"
                   class="btn btn-info" style="margin-bottom: 10px;">Сознание
                    Аватара</a>
                <a href="http://www.galaxysss.ru/chenneling/2016/02/22/uroven_alfa16_buduschee" class="btn btn-info"
                   style="margin-bottom: 10px;">Уровень
                    Альфа-16. Будущее</a>
                <a href="http://www.galaxysss.ru/chenneling/2016/02/22/sefera_planeta_avatar" class="btn btn-info"
                   style="margin-bottom: 10px;">Планета
                    Аватар</a><br>
                <a href="http://www.galaxysss.ru/category/study/324" class="btn btn-info" style="margin-bottom: 10px;">Дизайн
                    Человека</a>
                <a href="http://www.galaxysss.ru/blog/2016/02/22/raskrytie_vysshego_prednaznach" class="btn btn-info"
                   style="margin-bottom: 10px;">Раскрытие высшего предназначения,
                    таящегося в вашей ДНК</a>
            </div>
            <p>
                <iframe width="100%" height="315" src="https://www.youtube.com/embed/I4HseTGA4vc" frameborder="0"
                        allowfullscreen></iframe>
            </p>
            <p>О Белых Богах. Александр Колтыпин</p>
            <p>
                <iframe width="100%" height="315" src="https://www.youtube.com/embed/-TjwHWHxVzA" frameborder="0"
                        allowfullscreen></iframe>
            </p>

            <?php foreach ($images as $i) { ?>
                <p class="text-center">
                    <a href="/images/controller/site/avatar/<?= $i ?>" target="_blank">
                        <img src="/images/controller/site/avatar/<?= $i ?>" width="300">
                    </a>
                </p>
            <?php } ?>

        </div>
    </div>
</div>


