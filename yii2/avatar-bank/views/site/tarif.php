<?php
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
$this->title = 'Тарифы платформы ЯАватар';


?>
<style>
    .article p {
        font-size: 150%;
    }
</style>
<div class="container" style="padding-bottom: 70px;">
    <div class="col-lg-12">
        <h1 class="page-header text-center"><?= $this->title ?></h1>
    </div>
    <div class="row">
        <div class="col-lg-8 col-lg-offset-2">

            <?php
            $rows = [
                [
                    'name' => 'Утренняя заря',
                    'm1'   => 1,
                    'm2'   => 1,
                ],
                [
                    'name' => 'Луч солнца',
                    'm1'   => 2.5,
                ],
                [
                    'name' => 'Золотая заря',
                    'm1'   => 5,
                ],
                [
                    'name' => 'Планета',
                    'm1'   => 10,
                ],
                [
                    'name' => 'Солнце',
                    'm1'   => 25,
                ],
                [
                    'name' => 'Солнечная система',
                    'm1'   => 50,
                ],
                [
                    'name' => 'Звезда',
                    'm1'   => 100,
                ],
                [
                    'name' => 'Созвездие',
                    'm1'   => 250,
                ],
                [
                    'name' => 'Галактика',
                    'm1'   => 500,
                    'm2'   => 0.12,
                ],
            ];
            $users = 1000;
            $price = 3000;
            for ($i = 0; $i < count($rows); $i++) {
                $rows[$i]['users'] = $users * $rows[$i]['m1'];
                if ($i > 0) {
                    if ($i < count($rows) - 1) {
                        $diff = $rows[0]['m2'] - $rows[count($rows) - 1]['m2'];
                        $one = $diff / (count($rows) - 1);
                        $rows[$i]['m2'] = $rows[0]['m2'] - ($one * $i);
                    }
                }
                $rows[$i]['price'] = $price * $rows[$i]['m1'] * $rows[$i]['m2'];
                $rows[$i]['discont3'] = 30;
                $rows[$i]['discont12'] = 50;
            }
            ?>
            <?= \yii\grid\GridView::widget([
                'dataProvider' => new \yii\data\ArrayDataProvider([
                    'allModels' => $rows,
                ]),
                'tableOptions' => [
                    'class' => 'table table-striped table-hover',
                    'style' => 'width: auto',
                ],
                'summary'      => '',
                'columns'      => [
                    [
                        'header'    => 'Тариф',
                        'attribute' => 'name',
                    ],
                    [
                        'header'         => 'Пользователей',
                        'attribute'      => 'users',
                        'format'         => ['decimal', 0],
                        'contentOptions' => [
                            'style' => 'text-align:right;',
                        ],
                        'headerOptions'  => [
                            'style' => 'text-align:right;',
                        ],
                    ],
                    [
                        'header'         => 'Места',
                        'attribute'      => 'space',
                        'contentOptions' => [
                            'style'  => 'text-align:right;',
                            'nowrap' => 'nowrap',
                        ],
                        'headerOptions'  => [
                            'style' => 'text-align:right;',
                        ],
                        'content'        => function ($item) {
                            return Yii::$app->formatter->asDecimal($item['users'] / 50) . ' Гб';
                        },
                    ],
                    [
                        'header'         => 'Стоимость в мес',
                        'attribute'      => 'price',
                        'format'         => ['decimal', 0],
                        'contentOptions' => [
                            'style' => 'text-align:right;',
                        ],
                        'headerOptions'  => [
                            'style' => 'text-align:right;',
                        ],
                    ],
                    [
                        'header'         => 'Скидка при оплате за 3 мес',
                        'attribute'      => 'discont3',
                        'contentOptions' => [
                            'style' => 'text-align:right;',
                        ],
                        'headerOptions'  => [
                            'style' => 'text-align:right;',
                        ],
                        'content'        => function ($item) {
                            $v = $item['price'] * 3;
                            return Yii::$app->formatter->asDecimal($v * ((100 - $item['discont3']) / 100), 0);
                        },
                    ],
                    [
                        'header'         => 'Скидка при оплате за 12 мес',
                        'attribute'      => 'discont12',
                        'contentOptions' => [
                            'style' => 'text-align:right;',
                        ],
                        'headerOptions'  => [
                            'style' => 'text-align:right;',
                        ],
                        'content'        => function ($item) {
                            return Yii::$app->formatter->asDecimal($item['price'] * 12 * ((100 - $item['discont12']) / 100), 0);
                        },
                    ],
                ],
            ]) ?>
            <p>Оплата производится в <a href="/site/elixir">Золотом Эликсире</a> (ELXGOLD). 1 ELXGOLD = 1 RUB.</p>
            <p>Скидка при оплате за три мес = 30%.</p>
            <p>Скидка при оплате за год = 50%.</p>
            <p>Сейчас платформа работает в тестовом режиме. Стоимость одного месяца равна 100 ELXGOLD.</p>
            <p>Цены ориентировочные и возможно могут корректироваться, но не сильно.</p>


            <a name="service"></a>
            <h2 class="page-header text-center">Услуги</h2>
            <p>Если вам сложно разобраться самостоятельно как работать с платформой вы можете заказать набор
                дополнительных услуг и мы вам поможем сделать все быстро и качественно.</p>
            <p>Для запуска проекта мы предлагаем три формата сотрудничества:<br>
                1 Наставничство - консультации экспертов (25% от прайс листа),<br>
                2 Обучение - покупка видеокурса + консультация (50%),<br>
                3 Реализация под ключ (100% от прайс листа)</p>

            <?php
            $rows = [
                [
                    'name'    => 'Подключить свой домен',
                    'rub'     => 2000,
                    'elxgold' => 2000,
                    'result'  => 'ваш домен будет открываться и вы можете на нем создавать страницы при помощи платформы ЯАватар',
                    'prepare' => 'Логин и пароль для доступа к панели управления доменом у регистратора',
                ],
                [
                    'name'    => 'Подготовка контекнта (для одностраничного сайта)',
                    'rub'     => 5000,
                    'elxgold' => 5000,
                    'result'  => '',
                    'prepare' => '',
                ],
                [
                    'name'    => 'Создание лендинга визитной карточки Личности, Бренда, Услуги',
                    'rub'     => 5000,
                    'elxgold' => 5000,
                    'result'  => 'Лендинг минимально состоящий из Представительского экрана, описания, формы заявки',
                    'prepare' => 'Тексты о представляемом объекте и сопутствующие картинки',
                ],
                [
                    'name'    => 'Разработка макета клубной карты',
                    'rub'     => 2000,
                    'elxgold' => 2000,
                    'result'  => '',
                    'prepare' => '',
                ],
                [
                    'name'    => 'Покупка доменного имени',
                    'rub'     => 2000,
                    'elxgold' => 2000,
                    'result'  => '',
                    'prepare' => '',
                ],
                [
                    'name'    => 'Концептуальная упаковка проекта (Мастера) - Легенда мастера',
                    'rub'     => 5000,
                    'elxgold' => 5000,
                    'result'  => '',
                    'prepare' => '',
                ],
                [
                    'name'    => 'Разработка логотипа и фирменного стиля',
                    'rub'     => 10000,
                    'elxgold' => 10000,
                    'result'  => '',
                    'prepare' => '',
                ],
                [
                    'name'    => 'Бизнес финансовое моделирование',
                    'rub'     => 20000,
                    'elxgold' => 20000,
                    'result'  => 'Продуктовая линейка, прасл лист на продукты, бюджет, финансовый план',
                    'prepare' => '',
                ],
                [
                    'name'    => 'Маркетинговая стратегия',
                    'rub'     => 50000,
                    'elxgold' => 50000,
                    'result'  => 'Маркетинг кит, настроены соц каналы, описана целевая аудитория, разработка таргетинга, CRM - воронка продаж, Скрипты продаж (офферы), стартовый контент план',
                    'prepare' => '',
                ],
                [
                    'name'    => 'Настройка рекламной компании',
                    'rub'     => 30000,
                    'elxgold' => 30000,
                    'result'  => 'разработка и размещение рекламных материалов, закупка трафика, настройка таргетинга, печать клубных карт',
                    'prepare' => '',
                ],
                [
                    'name'    => 'Разработка и продюсирование образовательного курса',
                    'rub'     => 50000,
                    'elxgold' => 50000,
                    'result'  => 'Семь видеоуроков, методичка, домашние задания, заполнение на платформу',
                    'prepare' => '',
                ],
            ];
            ?>
            <?= \yii\grid\GridView::widget([
                'dataProvider' => new \yii\data\ArrayDataProvider([
                    'allModels' => $rows,
                ]),
                'tableOptions' => [
                    'class' => 'table table-striped table-hover',
                    'style' => 'width: auto',
                ],
                'summary'      => '',
                'columns'      => [
                    [
                        'header'    => 'Наименование',
                        'attribute' => 'name',
                    ],
                    [
                        'header'    => 'Что нужно подготовить и передать',
                        'attribute' => 'prepare',
                    ],
                    [
                        'header'    => 'Результат',
                        'attribute' => 'result',
                    ],
                    [
                        'header'         => 'Стандарт, Стоимость в рублях',
                        'attribute'      => 'rub',
                        'contentOptions' => [
                            'style' => 'text-align:right;',
                        ],
                        'headerOptions'  => [
                            'style' => 'text-align:right;',
                        ],
                        'content' => function($item) {
                            return Yii::$app->formatter->asDecimal($item['rub'] , 0);
                        }
                    ],
                    [
                        'header'         => 'Бизнес, Стоимость в рублях',
                        'attribute'      => 'rub',
                        'contentOptions' => [
                            'style' => 'text-align:right;',
                        ],
                        'headerOptions'  => [
                            'style' => 'text-align:right;',
                        ],
                        'content' => function($item) {
                            return Yii::$app->formatter->asDecimal($item['rub'] * 2, 0);
                        }
                    ],
                    [
                        'header'         => 'VIP, Стоимость в рублях',
                        'attribute'      => 'rub',
                        'contentOptions' => [
                            'style' => 'text-align:right;',
                        ],
                        'headerOptions'  => [
                            'style' => 'text-align:right;',
                        ],
                        'content' => function($item) {
                            return Yii::$app->formatter->asDecimal($item['rub'] * 5, 0);
                        }
                    ],
//                    [
//                        'header'         => 'Стоимость в эликсире (ELXGOLD)',
//                        'attribute'      => 'elxgold',
//                        'format'         => ['decimal', 0],
//                        'contentOptions' => [
//                            'style' => 'text-align:right;',
//                        ],
//                        'headerOptions'  => [
//                            'style' => 'text-align:right;',
//                        ],
//                    ],

                ],
            ]) ?>

        </div>
    </div>
    <hr>
    <center>
        <?= $this->render('../blocks/share', [
            'image'       => Url::to(\avatar\modules\Share\Share::image("Тарифы\nплатформы\nЯАватар", __FILE__)->getPath(), true),
            'title'       => $this->title,
            'url'         => Url::current([], true),
            'description' => 'Тарифы платформы ЯАватар',
        ]) ?>
    </center>

</div>


