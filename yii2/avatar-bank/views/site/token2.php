<?php
/**
 * Created by PhpStorm.
 * User: Ra-m-ha
 * Date: 06.04.2019
 * Time: 2:38
 */

// App ID и App Secret из настроек приложения
$app_id = "855000061506051";
$app_secret = "ee0667a77ab280519ffb6df8f758a6d2";

// ссылка на страницу возврата после авторизации
// домен должен совпадать с указанным в настройках приложения
$callback = "https://www.i-am-avatar.com/site/callback";

$fb = new \Facebook\Facebook([
    'app_id'                => $app_id,
    'app_secret'            => $app_secret,
    'default_graph_version' => 'v2.4',
]);

$helper = $fb->getRedirectLoginHelper();

// для публикации в группах достаточно разрешения publish_actions
// для публикации на страницах нужны все 3 элемента
$permissions = ['publish_actions', 'manage_pages', 'publish_pages'];
$loginUrl = $helper->getLoginUrl($callback, $permissions);



?>

<div class="container" style="padding-bottom: 70px;">
    <div class="col-lg-12">
        <h1 class="page-header text-center">Монета платформы ЯАватар</h1>
        <p><?php echo '<a href="' . htmlspecialchars($loginUrl) . '">Log in with Facebook!</a>'; ?></p>
    </div>
</div>



