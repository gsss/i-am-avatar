<?php
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */


$this->title = Yii::t('c.B5VVWNSe5V', 'Контакты');

?>

<div class="container" style="padding-bottom: 70px;">
    <div class="col-lg-12">
        <h1 class="page-header text-center"><?= $this->title ?></h1>
    </div>
    <div class="col-lg-6 col-lg-offset-3">

        <?php if (Yii::$app->session->hasFlash('contactFormSubmitted')): ?>

            <p class="alert alert-success">Успешно отправлено.</p>

        <?php else: ?>
            <?php $form = \yii\bootstrap\ActiveForm::begin(['id' => 'contact-form']); ?>
            <?= $form->field($model, 'name', ['inputOptions' => ['class' => 'form-control', 'placeholder' => Yii::t('c.B5VVWNSe5V', 'Ваше имя')]])->label(Yii::t('c.B5VVWNSe5V', 'Ваше имя'), ['class' => 'hide']) ?>
            <?= $form->field($model, 'email', ['inputOptions' => ['class' => 'form-control', 'placeholder' => 'Email']])->label('Email', ['class' => 'hide']) ?>
            <?= $form->field($model, 'subject', ['inputOptions' => ['class' => 'form-control', 'placeholder' => Yii::t('c.B5VVWNSe5V', 'Тема')]])->label(Yii::t('c.B5VVWNSe5V', 'Тема'), ['class' => 'hide']) ?>
            <?= $form->field($model, 'body')->textArea(['rows' => 6])->label(Yii::t('c.B5VVWNSe5V', 'Сообщение')) ?>
            <?= $form->field($model, 'verifyCode')->widget(\yii\captcha\Captcha::className(), [
                'options' => ['class' => 'form-control', 'placeholder' => Yii::t('c.B5VVWNSe5V', 'Введите код указанный на картинке')]
            ]) ?>
            <hr>
            <div class="form-group">
                <?= Html::submitButton(Yii::t('c.B5VVWNSe5V', 'Отправить'), [
                    'class' => 'btn btn-primary',
                    'name' => 'contact-button',
                    'style' => 'width: 100%',
                ]) ?>
            </div>
            <?php \yii\bootstrap\ActiveForm::end(); ?>

        <?php endif; ?>
    </div>
</div>


