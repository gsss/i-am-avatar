<?php

use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
$this->title = 'Записаться на курс Святослава';


?>

<div class="container" style="padding-bottom: 70px;">
    <div class="col-lg-12">
        <h1 class="page-header text-center"><?= $this->title ?></h1>
    </div>
    <div class="row">
        <div class="col-lg-6 col-lg-offset-3">

            <?php if (!is_null($id = Yii::$app->session->getFlash('form'))) : ?>

                <div class="alert alert-success">
                    Успешно добавлено.
                </div>

            <?php else: ?>

                <?php $form = \yii\bootstrap\ActiveForm::begin([
                    'options' => ['enctype' => 'multipart/form-data'],
                ]); ?>
                <?= $form->field($model, 'email')->label('Почта') ?>
                <?= $form->field($model, 'name')->label('Имя') ?>
                <?= $form->field($model, 'phone')->label('Телефон') ?>

                <hr>
                <div class="form-group">
                    <?= Html::submitButton('Добавить', [
                        'class' => 'btn btn-default',
                        'name'  => 'contact-button',
                        'style' => 'width:100%',
                    ]) ?>
                </div>
                <?php \yii\bootstrap\ActiveForm::end(); ?>

            <?php endif; ?>
        </div>


    </div>
</div>


