<?php

use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
$this->title = 'Записаться в команду iAvatar';


?>

<div class="container" style="padding-bottom: 70px;">
    <div class="col-lg-12">
        <h1 class="page-header text-center"><?= $this->title ?></h1>
    </div>
    <div class="row">
        <div class="col-lg-6 col-lg-offset-3">
            <?php if (!is_null($id = Yii::$app->session->getFlash('form'))) : ?>
                <div class="alert alert-success">
                    Успешно добавлено.
                </div>
            <?php else: ?>
                <?php $form = \yii\bootstrap\ActiveForm::begin([
                    'options' => ['enctype' => 'multipart/form-data'],
                ]); ?>
                <?= $form->field($model, 'email')->label('Почта') ?>
                <?= $form->field($model, 'name')->label('Имя') ?>
                <?= $form->field($model, 'phone')->label('Телефон') ?>
                <hr>
                <div class="form-group">
                    <?= Html::submitButton('Добавить', [
                        'class' => 'btn btn-default',
                        'name'  => 'contact-button',
                        'style' => 'width:100%',
                    ]) ?>
                </div>
                <?php \yii\bootstrap\ActiveForm::end(); ?>
            <?php endif; ?>
            <hr>
            <h2 class="page-header text-center">Список задач</h2>
            <p>Составить курсы для детей на полный курс обучения.<br>
                <br>
                Подготовить макеты на мобильное приложение.<br>
                <br>
                Редактор документации и обучающего курса по платформе iAvatar.<br>
                <br>
                Организовать представительство платформы на других языках и других странах.<br>
                <br>
                Переводы строк дизайна и текстов в платформе.<br>
                <br>
                Реклама: писать офферы (приглашения) для рекламной компании для платформы<br>
                <br>
                Провести исследование рынка онлайн школ и офлайн образования.<br>
                <br>
                Написать бизнес план на проект.<br>
                <br>
                Составить финансовый план.<br>
                <br>
                Юридическое сопровождение компании.<br>
                <br>
                Нарисовать брендбук проекта.<br>
                <br>
                Сформировать компоновку личного кабинета школы (UI/UX дизайн). </p>
        </div>
    </div>
</div>


