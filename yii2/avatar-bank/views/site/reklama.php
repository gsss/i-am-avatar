<?php

use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
$this->title = 'Рекламные матриалы';

\avatar\assets\Paralax::register($this);
?>
<div class="parallax-window" data-parallax="scroll"
     data-image-src="/images/controller/site/about/2.png"
     style="min-height: 650px; background: transparent;">
</div>

<div class="container" style="padding-bottom: 70px;">
    <div class="col-lg-12">
        <h1 class="page-header text-center"><?= $this->title ?></h1>
    </div>
    <div class="row">
        <div class="col-lg-8 col-lg-offset-2">
            <style>
                .article p {
                    font-size: 150%;
                }
            </style>
            <div class="article">
                <h2 class="page-header">Логотип</h2>
                <??>
                <p>Логотип</p>
                <p>логотип</p>

            </div>
        </div>
    </div>
    <hr>
    <center>
        <?= $this->render('../blocks/share', [
            'image'       => Url::to(\avatar\modules\Share\Share::image($this->title, __FILE__)->getPath(), true),
            'title'       => $this->title,
            'url'         => Url::current([], true),
            'description' => 'Проект iAvatar - платформа для ускоренного обучения человечества методам перехода Золотой уровень Сознания.',
        ]) ?>
    </center>

</div>


