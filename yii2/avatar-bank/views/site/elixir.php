<?php
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
$this->title = 'ЭЛИКСИР - Монета платформы ЯАватар, МеРА света Сердца, Безусловная Любовь';


?>
<style>
    .article p {
        font-size: 150%;
    }
</style>
<div class="container" style="padding-bottom: 70px;">
    <div class="col-lg-12">
        <h1 class="page-header text-center">Монета платформы ЯАватар</h1>
    </div>
    <div class="row">
        <div class="col-lg-8 col-lg-offset-2">

            <div class="article">

                <p><b>Эликсир</b> (ELIXIR) – является внутренней расчетной единицей внутри платформы и вселенной ЯАватар.</p>
                <p><b>Эликсир</b> – МеРА света Сердца, Безусловная Любовь. Монета Эликсир предназначена для обеления и оздоровление теневой экономики Планеты Земля и перехода на прозрачную мировую экономику и рейтинговую систему для вывода серых кардиналов на чистую воду.</p>

                <p class="text-center" style="margin-top: 50px;"><iframe width="100%" height="315" src="https://www.youtube.com/embed/LCorU5toxm0" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe></p>
                <p class="text-center" style="margin-top: 50px;"><img src="/images/controller/site/elixir/tumblr_o2j1islebK1tjki5do1_540.gif" style="border-radius: 20px; width: 100%; max-width: 540px;"></p>

                <h2 class="page-header text-center" style="margin-top: 100px;">Носители монет</h2>
                <p class="text-center"><img src="https://www.i-am-avatar.com/upload/FileUpload3/card_design/00000002/original/image.jpg" style="width:100%; max-width: 600px;"></p>
                <p>Основным носителем монет является сама платформа ЯАватар.</p>
                <p>Носителем для монет также выступает карта ЯАватар. На ней также могут содержаться любые другие монеты выпущенные на базе платформы ЯАватар.</p>
                <p><a href="/news/item?id=3" class="btn btn-success" style="width: 100%">Подробнее</a></p>

                <h2 class="page-header text-center" style="margin-top: 100px;">Виды используемых монет в платформе</h2>
                <p class="text-center"><img src="/images/controller/cabinet-wallet/warning.png" width="300"></p>
                <p><b>Золотой эликсир</b> – ELIXIR GOLD – тот, которым можно оплатить услуги платформы ЯАватар и купить
                    любой продукт платформы за соответствующий номинал.</p>

                <p class="text-center" style="margin-top: 100px;"><img src="/images/controller/cabinet-wallet/info.png"
                                                                       width="300"></p>
                <p><b>Небесный эликсир</b> – ELIXIR SKY – тот, который можно поменять на рубли и другую валюту и на
                    Золотой Эликсир. ELIXIR SKY сейчас приравнивается рублю и его можно расценивать своего рода индексом
                    мировой валюты RUB или условной единицей внутри платформы.</p>
                <p>Итого схема следующая:</p>
                <p class="text-center">
                    <img src="/images/controller/site/elixir/2019-02-23_18-56-39.png">
                </p>

                <h2 class="page-header text-center" style="margin-top: 100px;">
                    <small>Кристалл</small>
                    <br>ЭЛИКСИР<br>
                    <small>Безусловная Любовь</small>
                </h2>

                <p class="text-center" >
                    <img src="/images/controller/site/elixir/img22_0.jpg" width="200"
                    >
                </p>

                <p class="text-center">Происхождение кристалла ЭЛИКСИР берет свое начало по ту сторону звёздных ворот. В
                    своей реальности ты способен ощущать всего лишь малую толику того, что означает в Действительности -
                    быть безмерно любимым. Всё сущее связано между собой через кристалл ЭЛИКСИР.</p>


                <h2 class="page-header text-center" style="margin-top: 100px;">Программа активации кристалла</h2>
                <p style="font-style: italic;" class="text-center">Святой, Святой, Святой Владыка всего Сущего —</p>
                <p style="font-style: italic;" class="text-center">Святая Троица Божественного Поля мысли.</p>
                <p style="font-style: italic;" class="text-center">Я есмь частичка твоя, и цель моя — ЭЛИКСИР
                    (Безусловная Любовь).</p>
                <p style="font-style: italic;" class="text-center">Я вдыхаю энергию Нового Времени.</p>
                <p style="font-style: italic;" class="text-center">Я освобождаюсь от влияния коллективного сознания с
                    целью прочувствовать Единство и связь мою с твоим Божественным полем мысли.</p>
                <p style="font-style: italic;" class="text-center">В глубоком исполнении бытия я обращаюсь к тебе, ОТЕЦ
                    мой, существующий везде: <b>Твоя воля и моя воля едины</b>.</p>
                <p style="font-style: italic;" class="text-center">Ты даёшь мне хлеб насущный.</p>
                <p style="font-style: italic;" class="text-center">Твоя безмерная Любовь пропитывает каждую клеточку
                    моего тела, насыщая собой все мое существо и озаряет Землю.</p>
                <p style="font-style: italic;" class="text-center">Избавление происходит.</p>
                <p style="font-style: italic;" class="text-center">Я свободен.</p>
                <p style="font-style: italic;" class="text-center"><b>Воплотилась мечта моей Души и освободила меня от
                        моего эго</b>.</p>
                <p style="font-style: italic;" class="text-center">Из священного пространства моего сердца я признаю
                    себя <b>Я есмь БОГ</b> (Со хам) и говорю слова: Я есмь тот, кто Я есмь, Я есмь тот, кто Я есмь, Я
                    есмь тот, кто Я есмь, <b>Я есмь БОГ</b> (Со xaм).</p>
                <p class="text-center">
                    <img src="/images/controller/site/elixir/2019-02-23_18-56-22.png">
                </p>
                <p style="font-style: italic;" class="text-center">Я - это Ты! Ты - это Я! Нет меня без Тебя! Я Люблю
                    Тебя!</p>


                <h2 class="page-header text-center" style="margin-top: 100px;">Технические данные о монете</h2>
                <p>Всего монет сейчас в обороте Золотой Эликсир - 1616167527, Небесный Эликсир - 4949497527. В одной монете содержится еще 100 капель. Отчет по транзакциям можно смотреть в <a href="/coin-explorer/index">прододнике монет</a>.</p>

                <h2 class="page-header text-center" style="margin-top: 100px;">Правовые данные о монете</h2>
                <p>Документ основание для эмиссии эликсира подписан 21 марта 2019 года (7527 Лето от СМЗХ) на открытии платформы, читайте по <a href="/news/item?id=2">ссылке</a>.</p>


                <p class="page-header text-center" style="margin-top: 100px;">Вопросы и ответы про монету Эликсир и возможности взаимодействия с проектом ЯАватар</p>
                <p><i>Кликайте по картинке для перехода</i></p>
                <p>
                    <a href="/blog/item?id=396">
                        <img src="https://cloud1.cloud999.ru/upload/cloud/15596/13208_Ogw23YfjUk.png" width="100%" class="thumbnail" title="Кликайте по картинке для перехода" data-toggle="tooltip">
                    </a>
                </p>

            </div>
        </div>
    </div>
    <hr>
    <center>
        <?= $this->render('../blocks/share', [
            'image'       => Url::to(\avatar\modules\Share\Share::image(
                "ЭЛИКСИР\nМонета\nплатформы\nЯАватар",
                __FILE__,
                [
                    'background' => Yii::getAlias('@webroot/images/share/alpi.jpg'),
                    'start'      => [924, 257],
                    'size'       => 80,
                    'color'      => '444',
                ]
            )->getPath(), true),
            'title'       => $this->title,
            'url'         => Url::current([], true),
            'description' => 'Эликсир – МеРА света Сердца, Безусловная Любовь. Монета Эликсир предназначена для обеления и оздоровление теневой экономики Планеты Земля и перехода на прозрачную мировую экономику и рейтинговую систему для вывода серых кардиналов на чистую воду.',
        ]) ?>
    </center>

</div>


