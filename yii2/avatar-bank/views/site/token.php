<?php
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
$this->title = 'AvatarCoin';


?>

<div class="container" style="padding-bottom: 70px;">
    <div class="col-lg-12">
        <h1 class="page-header text-center"><?= $this->title ?></h1>

        <p>Эмиссия токенов</p>
        <p>Кол-во эмиссии 100,000,000 AVR</p>
        <p>Кол-во знаков после запятой 8</p>

        <p>Токены распределяются в три этапа в соответствии с <a href="/images/controller/development-contracts/avr-token/ico-avatar-network.xlsx" target="_blank">таблицей</a></p>
        <p>Распродаются токены из фондов "Инвесторы" и "Инвестиционный фонд".</p>

        <p>При инициализации контракта можно задать параметр продаж токена, включена или выключена и установить цену токена.</p>
        <p>Продажи токена производятся через безымянную функцию, которая срабатывает на прием эфира.</p>

        <p>Все нераспроданные токены на предыдущем этапе переходят на следущий.</p>
        <p>Если выделенное кол-во токенов на этап продажи заканчивается то продажи останавливаются и доп токены не выделяются.</p>
        <p>После завершения ICO все токены выводятся на биржу.</p>

    </div>

    <div class="row" style="margin: 100px 0px 70px 0px;">
        <h1 class="page-header text-center">Распределение токенов по фондам</h1>
        <?php

        $this->registerJs(<<<JS
Highcharts.getOptions().plotOptions.pie.colors = (function () {
    var colors = [],
        base = Highcharts.getOptions().colors[0],
        i;

    for (i = 0; i < 10; i += 1) {
        // Start out with a darkened base color (negative brighten), and end
        // up with a much brighter color
        colors.push(Highcharts.Color(base).brighten((i - 3) / 7).get());
    }
    return colors;
}());
JS
        );
        ?>
        <?= \common\widgets\HighCharts\HighCharts::widget([
            'chartOptions' => [
                'chart'     => [
                    'plotBackgroundColor' => null,
                    'plotBorderWidth' => null,
                    'plotShadow' => false,
                    'type' => 'pie',
                ],
                'title'     => [
                    'text' => '',
                ],
                'tooltip' => [
                    'pointFormat' => '{series.name}: <b>{point.percentage:.1f}%</b>'
                ],
                'plotOptions' => [
                    'pie' => [
                        'allowPointSelect' => true,
                        'cursor' => 'pointer',
                        'dataLabels' => [
                            'enabled' => true,
                            'format' => '<b>{point.name}</b>: {point.percentage:.1f} %',
                            'style' => [
                                'color' => new \yii\web\JsExpression('(Highcharts.theme && Highcharts.theme.contrastTextColor) || \'black\''),
                            ]
                        ]
                    ]
                ],
                'series' => [
                    [
                        'name' => 'Токенов',
                        'colorByPoint' => true,
                        'data' => [
                            [
                                'name' => 'Инвесторы',
                                'y'    => 65,
                            ],
                            [
                                'name' => 'Адвайзеры',
                                'y'    => 5,
                            ],
                            [
                                'name' => 'Баунти программа',
                                'y'    => 10,
                            ],
                            [
                                'name' => 'Инвестиционный фонд',
                                'y'    => 10,
                            ],
                            [
                                'name' => 'Основатели',
                                'y'    => 10,
                            ],
                        ]
                    ]
                ]
            ],
        ]);
        ?>
    </div>
    <div class="row" style="margin: 100px 0px 70px 0px;">
        <h1 class="page-header text-center">Распределение токенов по этапам продаж</h1>
        <?= \common\widgets\HighCharts\HighCharts::widget([
            'chartOptions' => [
                'chart'     => [
                    'type' => 'column',
                ],
                'title'     => [
                    'text' => '',
                ],
                'colors' => ['#7cb5ec', '#5891c8', '#346da4', '#0f487f'],
                'xAxis' => [
                    'categories' => [
                        'preSale',
                        'preICO',
                        'ICO',
                    ],
                    'crosshair' => true
                ],
                'yAxis' => [
                    'min' => 0,
                    'title' => [
                        'text' => 'AVR',
                    ]
                ],
                'tooltip' => [
                    'valueSuffix' => ' AVR',
                ],
                'plotOptions' => [
                    'column' => [
                        'pointPadding' => 0.2,
                        'borderWidth' => 0,
                    ]
                ],
                'series' => [
                    [
                        'name' => 'Токены',
                        'data' => [1000000, 9000000, 90000000],
                    ],
                ]
            ],
        ]);
        ?>
    </div>
    <div class="row" style="margin: 100px 0px 70px 0px;">
        <h1 class="page-header text-center">Цена токена на этапах продаж</h1>
        <?= \common\widgets\HighCharts\HighCharts::widget([
            'chartOptions' => [
                'chart'     => [
                    'type' => 'column',
                ],
                'title'     => [
                    'text' => '',
                ],
                'xAxis' => [
                    'categories' => [
                        'preSale',
                        'preICO',
                        'ICO',
                    ],
                    'crosshair' => true
                ],
                'yAxis' => [
                    'min' => 0,
                    'title' => [
                        'text' => 'USD',
                    ]
                ],
                'tooltip' => [
                    'valueSuffix' => ' USD',
                ],
                'plotOptions' => [
                    'column' => [
                        'pointPadding' => 0.2,
                        'borderWidth'  => 0,
                    ],
                ],
                'colors' => ['#7cb5ec', '#5891c8', '#346da4', '#0f487f'],
                'series' => [
                    [
                        'name' => '1 неделя',
                        'data' => [0.1, 0.4, 0.85],
                    ],
                    [
                        'name' => '2 неделя',
                        'data' => [0.15, 0.45, 0.90],
                    ],
                    [
                        'name' => '3 неделя',
                        'data' => [0.20, 0.50, 0.95],
                    ],
                    [
                        'name' => '4 неделя',
                        'data' => [0.25, 0.55, 1.00],
                    ],
                ],
            ],
        ]);
        ?>
    </div>
    <hr>
    <center>
        <?= $this->render('../blocks/share', [
            'image'       => Url::to('/images/controller/site/avatar-pay/logo.png', true),
            'title'       => $this->title,
            'url'         => Url::current([], true),
            'description' => 'AvatarCoin',

        ]) ?>
    </center>

</div>


