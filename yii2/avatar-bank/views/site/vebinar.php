<p align="center"><img alt="" src="https://i-am-avatar.com/upload/HtmlContent/school_potok_subscribe/content/00000001/1545255495_urnF1Petga.png" style="width: 100%;" /></p>

<p align="center">Я с радостью приветствую твою великую Душу, что пришла на эту планету не просто так в это Время!</p>

<p align="center">Ночь с 21 на 22 декабря является особенной. Во-первых, это будет момент Зимнего Солнцестояния (1:23 ночи 22 декабря по московскому времени). Во-вторых, Луна будет в фазе полнолуния, своего максимального света, а также в Тельце &mdash; знаке своей экзальтации (ведический гороскоп).</p>

<p align="center">Естественно дата и время выбраны не просто так, а намеренно, чтобы заложить ядро старта проекта.</p>

<p align="center">Напоминаю презентация состоится 21 декабря в 21:12 по Московскому времени по <a href="https://www.youtube.com/watch?v=SH1EHCGrlGw">ссылке</a> либо на самой <a href="http://i-avatar.tilda.ws/vebinar21dec2018">странице анонса</a>.</p>

<p align="center" style="color:red">Внимание! В конце презентации мы проведем активацию и поэтому подготовь&nbsp;пожалуйста заранее фрукт любой, сосуд с водой, свечка и благовония.</p>

<p align="center">Жду тебя с нетерпением!</p>

<p align="center">Я это Ты! Ты это Я! Нет меня без тебя! Я Люблю Тебя!</p>

<p align="center" style="color:red">❤❤❤</p>
