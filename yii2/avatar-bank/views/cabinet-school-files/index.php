<?php

/** $this \yii\web\View  */
/** @var $school \common\models\school\School */

use yii\helpers\Url;
use app\services\GsssHtml;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use app\services\GetArticle\Collection;

$this->title = 'Облако';


?>

<div class="container">



    <h1 class="page-header"><?= Html::encode($this->title) ?></h1>

    <div class="row">
        <div class="col-lg-6">
            <?php
            $files_max_size = $school->files_max_size;
            $use = \common\models\school\File::find()->where(['school_id' => $school->id])->select('sum(size)')->scalar() / (1024 * 1024);
            $avial = $files_max_size - $use;
            ?>
            <p>Занято: <?= Yii::$app->formatter->asDecimal($use, 3) ?> Мб
                из <?= Yii::$app->formatter->asDecimal($files_max_size, 3) ?> Мб</p>

            <?php
            $this->registerJs(<<<JS
Highcharts.setOptions({
    colors: Highcharts.map(Highcharts.getOptions().colors, function (color) {
        return {
            radialGradient: {
                cx: 0.5,
                cy: 0.3,
                r: 0.7
            },
            stops: [
                [0, color],
                [1, Highcharts.Color(color).brighten(-0.3).get('rgb')] // darken
            ]
        };
    })
});

JS
            );

            ?>
            <?= \cs\Widget\HighCharts\HighCharts::widget([
                'chartOptions' => [
                    'chart'       => [
                        'plotBackgroundColor' => null,
                        'plotBorderWidth'     => null,
                        'plotShadow'          => false,
                        'type'                => 'pie',
                        'colors'              => ['#990000', '#009900'],
                    ],
                    'title'       => [
                        'text' => 'График',
                    ],
                    'tooltip'     => [
                        'pointFormat' => '{series.name}: <b>{point.percentage:.1f}%</b>',
                    ],
                    'plotOptions' => [
                        'pie' => [
                            'allowPointSelect' => true,
                            'cursor'           => 'pointer',
                            'colors'              => ['#990000', '#009900'],
                            'dataLabels'       => [
                                'enabled'        => true,
                                'format'         => '<b>{point.name}</b>: {point.percentage:.1f} %',
                                'connectorColor' => 'silver',
                            ],
                        ],
                    ],


                    'series' => [
                        [
                            'name' => 'Share',
                            'data' => [
                                [
                                    'name' => 'Занято',
                                    'y'    => $use,
                                ],
                                [
                                    'name' => 'Доступно',
                                    'y'    => $avial,
                                ],
                            ],
                        ],

                    ],
                ],
            ]);

            ?>
        </div>
        <div class="col-lg-6">
            <?php $model = \common\models\school\Cloud::find()->where(['school_id' => $school->id, 'is_active' => 1])->one(); ?>
            <?php $model = new \avatar\models\validate\CabinetSchoolFilesCloudSave([
                'id'  => $school->id,
                'url' => is_null($model)? '' : $model->url,
                'key' => is_null($model)? '' : $model->key,
            ]); ?>
            <?php $form = \iAvatar777\services\FormAjax\ActiveForm::begin([
                'model'   => $model,
                'formUrl' => '/cabinet-school-files/cloud-save',
                'success' => <<<JS
function (ret) {
    $('#modalInfo').on('hidden.bs.modal', function() {
        
    }).modal();
}
JS

            ]) ?>
            <?= Html::activeHiddenInput($model, 'id') ?>
            <?= $form->field($model, 'url') ?>
            <?= $form->field($model, 'key')->passwordInput() ?>
            <?php \iAvatar777\services\FormAjax\ActiveForm::end(['label' => 'Сохранить']) ?>
        </div>
    </div>



    <p>
        <?php
        \avatar\assets\Notify::register($this);

        $this->registerJS(<<<JS
$('.buttonDeleteAll').click(function (e) {
    e.preventDefault();
    e.stopPropagation();

    if (confirm('Подтвердите удаление')) {
        var button = $(this);
        var id = $(this).data('id');
        var all = '1,2';
        var allArray = [];

        $('input[name="selection[]"]').each(function(i,v) {
            var vObject = $(v);
            if (vObject.is(':checked')) {
                allArray.push(vObject.attr('value'));
            }
        });
        if (allArray.length == 0) {
            new Noty({
                timeout: 1000,
                theme: 'sunset',
                type: 'error',
                layout: 'bottomLeft',
                text: 'Нужно выбрать хотя бы один элемент'
            }).show();
        }
        if (allArray.length > 0) {
            ajaxJson({
                url: '/cabinet-school-files/delete-all',
                data: {ids: allArray.join(',')},
                success: function (ret) {
                    $('#modalInfo').on('hidden.bs.modal', function() {
                        window.location.reload();
                    }).modal();
                }
            });
        } 
    }
});


JS
        );
        ?>
        <button
          class="btn btn-default buttonDeleteAll">Удалить</button>
    </p>
    <?php \yii\widgets\Pjax::begin(); ?>
    <?php
    $this->registerJS(<<<JS
$('[data-toggle="tooltip"]').tooltip();
$('.buttonDelete2').click(function (e) {
    e.preventDefault();
    e.stopPropagation();
    if (confirm('Подтвердите удаление')) {
        var button = $(this);
        var id = $(this).data('id');
        ajaxJson({
            url: '/cabinet-school-files/delete' + '?' + 'id' + '=' + id,
            success: function (ret) {
                $('#modalInfo').on('hidden.bs.modal', function() {
                    button.parent().parent().remove();
                }).modal();
            }
        });
    }
});


JS
    );
    ?>
    <?= \yii\grid\GridView::widget([
        'dataProvider' => new \yii\data\ActiveDataProvider([
            'query'      => \common\models\school\File::find()
                ->where(['school_id' => $school->id])
                ->orderBy(['created_at' => SORT_DESC])
            ,
            'pagination' => [
                'pageSize' => 20,
            ],
        ]),
        'tableOptions' => [
            'class' => 'table table-striped table-hover',
        ],
        'rowOptions'   => function ($item) {
            $data = [
                'data'  => ['id' => $item['id']],
                'role'  => 'button',
                'class' => 'rowTable',
            ];
            return $data;
        },
        'columns'      => [
            ['class' => 'yii\grid\CheckboxColumn'],
            'id',
            [
                'header'    => 'file',
                'attribute' => 'file',
                'content'   => function ($item) {
                    $i = ArrayHelper::getValue($item, 'file', '');
                    if ($i == '') return '';

                    return Html::a($i, $i, ['target' => '_blank', 'data' => ['pjax' => 0]]);
                },
            ],
            [
                'header'         => 'Размер, Мб',
                'attribute'      => 'size',
                'contentOptions' => ['class' => 'text-right'],
                'headerOptions'  => ['class' => 'text-right'],
                'content'        => function ($item) {
                    return Yii::$app->formatter->asDecimal($item['size'] / (1024 * 1024), 3);
                },
            ],
            [
                'header'    => 'Тип',
                'attribute' => 'type_id',
                'content'   => function ($item) {
                    $v = \yii\helpers\ArrayHelper::getValue($item, 'type_id', 0);
                    if ($v == 0) return '';

                    return $v;
                },
            ],
            [
                'header'  => 'Создано',
                'content' => function ($item) {
                    $v = \yii\helpers\ArrayHelper::getValue($item, 'created_at', 0);
                    if ($v == 0) return '';

                    return Html::tag('abbr', \cs\services\DatePeriod::back($v, ['isShort' => true]), ['data' => ['toggle' => 'tooltip'], 'title' => Yii::$app->formatter->asDatetime($v)]);
                },
            ],
            [
                'header'  => 'Удалить',
                'content' => function ($item) {
                    return Html::button('Удалить', [
                        'class' => 'btn btn-danger btn-xs buttonDelete',
                        'data'  => [
                            'id' => $item['id'],
                        ],
                    ]);
                },
            ],
        ],
    ]) ?>
    <?php \yii\widgets\Pjax::end(); ?>


</div>

<div class="modal fade" id="modalInfo" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="myModalLabel">Информация</h4>
            </div>
            <div class="modal-body">
                Успешно!
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
            </div>
        </div>
    </div>
</div>