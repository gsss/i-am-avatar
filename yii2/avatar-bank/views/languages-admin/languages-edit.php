<?php

/** @var \yii\web\View $this */
/** @var \common\models\Language $model */

$this->title = $model->name;

?>


<h2><?= $this->title ?></h2>

<?php if (Yii::$app->session->hasFlash('form')) { ?>
    <p>Успешно сохранено</p>
<?php } else { ?>
    <div class="row">
    <div class="col-lg-6">
    <?php $form = \yii\bootstrap\ActiveForm::begin([
    ]) ?>
    <?= $form->field($model, 'name')->hint('Название на русском') ?>
    <?= $form->field($model, 'code')->hint('Двухсимвольный код на английском по стандарту ISO 639-1') ?>
    <?= $form->field($model, 'status')->dropDownList([
        1 => 'режим редактирования',
        2 => 'режим рабочий',
    ])->hint('редактирование - видит только редактор, рабочий - видят все') ?>
    <hr>
    <?= \yii\helpers\Html::submitButton('Обновить', [
        'class' => 'btn btn-success',
        'style' => 'width: 100%;',
    ]) ?>
    <?php \yii\bootstrap\ActiveForm::end(); ?>
    </div>
    </div>
<?php } ?>