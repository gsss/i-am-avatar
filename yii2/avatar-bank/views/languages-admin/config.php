<?php


/** @var \yii\web\View $this */
use common\components\DbMessageSourceSky;

/** @var \backend\models\form\LanguageSettings $model */

$this->title = 'Настройки';


?>

<div class="container" style="padding-bottom: 70px;">
    <div class="col-lg-12">
        <h1 class="page-header">Настройки</h1>
        <?php if (Yii::$app->session->hasFlash('form')) { ?>
            <p class="alert alert-success">Настройки успешно сохранены.</p>
        <?php } else { ?>
            <?php $form = \yii\bootstrap\ActiveForm::begin() ?>
            <?= $form->field($model, 'strategy')->dropDownList([
                '- ничего не выбрано -',
                DbMessageSourceSky::STRATEGY_TRANSLATE_KEY        => 'выдается ключ для перевода',
                DbMessageSourceSky::STRATEGY_TRANSLATE_RUS        => 'выдается русский, иначе ОШИБКА',
                DbMessageSourceSky::STRATEGY_TRANSLATE_RUS_KEY    => 'выдается русский, иначе ключ',
                DbMessageSourceSky::STRATEGY_TRANSLATE_ENG_KEY    => 'выдается английский, иначе ключ',
                DbMessageSourceSky::STRATEGY_TRANSLATE_ERROR      => 'выдается ОШИБКА',
                DbMessageSourceSky::STRATEGY_TRANSLATE_ERROR_PLUS => 'выдается +++',
            ]) ?>
            <?= \yii\helpers\Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
            <?php \yii\bootstrap\ActiveForm::end() ?>
        <?php } ?>
    </div>
</div>
