<?php


/** @var \yii\web\View $this */
/** @var integer $id */

$this->title = 'Группа переводчиков';

$columns = [];
$columns[] = 'id:text:ID';    // идентификатор пользователя
$columns[] = 'email:text:email'; // email пользователя
$columns[] = 'name:text:Имя'; // имя пользователя
$languages = \common\models\Language::find()->all();
$languagesJson = json_encode(\yii\helpers\ArrayHelper::getColumn($languages, 'code'));
$this->registerJs(<<<JS
var languages = {$languagesJson};
JS
);
$languageInfo = [
    'languages' => $languages,
    'default'   => 0,
    'count'     => count($languages),
];
Yii::$app->cache->set('languages.cache', $languageInfo);

/** @var \common\models\Language $lang */
foreach($languages as $lang) {
    $columns[] = [
        'header'  => strtoupper($lang->code),
        'content' => function($item) {
            $languageInfo = Yii::$app->cache->get('languages.cache');
            $default = $languageInfo['default'];

            /** @var \common\models\Language $lang */
            $lang = $languageInfo['languages'][$default];
            $checked = \common\models\language\Access::find()->where([
                'language_id' => $lang->id,
                'user_id'     => $item['id'],
            ])->exists();
            $name = 'LanguageAccess[name][' . $item['id'] . ']' . '[' . $lang->code . ']';

            $default++;
            if ($default >= $languageInfo['count']) {
                $default = 0;
            }
            $languageInfo['default'] = $default;
            Yii::$app->cache->set('languages.cache', $languageInfo);

            return \yii\helpers\Html::checkbox($name, $checked);
        },
    ];
}
$columns[] = [
    'header'  => 'Удалить',
    'content' => function($item) {
        return \yii\helpers\Html::button('Удалить', ['class' => 'btn btn-danger btn-xs buttonDelete', 'data' => ['id' => $item['id']]]);
    },
];
$this->registerJs(<<<JS
$('.buttonDelete').click(function(e){
    if (confirm('Вы уверены?')) {
        var b = $(this);
        ajaxJson({
            url: '/languages-admin/group-delete',
            data: {
                id: b.data('id')
            },
            success: function(ret) {
                b.parent().parent().remove();
            }
        });
    }
});
JS
);
$users = Yii::$app->authManager->getUserIdsByRole('languages');

$users = \common\models\UserAvatar::find()
    ->where(['in', 'id', $users])
    ->select(['id','name_first as name','email'])
    ->asArray()
    ->all();
?>

<style>
    a.asc:after {
        content: ' ↓';
        display: inline;
    }

    a.desc:after {
        content: ' ↑';
        display: inline;
    }
</style>

<div class="modal fade" id="modalAdd" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                        aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Добавление</h4>
            </div>
            <div class="modal-body">
                <div class="col-lg-12" style="margin: 40px 0px 40px 0px;">
                    <input type="text" class="form-control" id="email">
                    <p style="color: red;display: none;" id="error"></p>
                </div>
            </div>
            <div class="modal-footer">
                <?php
                $this->registerJs(<<<JS
var buttonDeleteFunction = function(e){};
$('#email').on('focus', function(){
    $('#error').html('').hide();
});
$('.buttonAdd1').click(function(e){
    ajaxJson({
        url: '/languages-admin/group-add',
        data: {
            mail: $('#email').val()
        },
        success: function(ret) {
            $('#modalAdd').modal('hide');
            console.log(ret);
            var tr = $('<tr>')
                .append(
                    $('<td>').html(ret.id)
                )
                .append(
                    $('<td>').html(ret.email)
                )
                .append(
                    $('<td>').html(ret.name)
                );
                var i;
                var item;
            for(i = 0; i < languages.length; i++) {
                item = languages[i];
                tr.append(
                    $('<td>').append(
                        $('<input>',{'type':'checkbox','name': 'LanguageAccess[name]['+ret.id+']['+item+']', 'value': 1})
                    )
                )
            }
            tr.append(
                    $('<td>').append(
                        $('<button>', {class:'btn btn-danger btn-xs buttonDelete', type: 'button', 'data-id': ret.id}).click(buttonDeleteFunction)
                        .html('Удалить')
                    )
                );
            $('.table-rows')
            .append(tr);
            $('#email').val('');
        },
        errorScript: function(ret) {
            console.log(ret);
            $('#error').html(ret.data).show();
        }
    });
});
JS
                );
                ?>
                <button type="button" class="btn btn-default buttonAdd1">Добавить</button>
            </div>
        </div>
    </div>
</div>


<div class="container" style="padding-bottom: 70px;">
    <div class="row">
        <div class="col-lg-12">
<h2 class="page-header">Группа переводчиков</h2>
<?php if (Yii::$app->session->hasFlash('form')) {?>
    <p class="alert alert-success">Удачно</p>
<?php } else {?>
    <?php $form = \yii\bootstrap\ActiveForm::begin() ?>
    <?= \yii\grid\GridView::widget([
        'dataProvider' => new \yii\data\ArrayDataProvider([
            'allModels'  => $users,
            'pagination' => [
                'pageSize' => 50,
            ],
        ]),
        'tableOptions' => [
            'class' => 'table table-striped table-bordered table-rows',
        ],
        'columns' => $columns,
    ]) ?>
    <?php
    $this->registerJs(<<<JS

$('.buttonAdd').click(function(e){
    $('#modalAdd').modal();
});
JS
);
    ?>
    <?= \yii\helpers\Html::button('Добавить', ['class' => 'btn btn-success buttonAdd', 'style' => 'margin-top: 20px;']) ?>
    <?= \yii\helpers\Html::submitButton('Обновить', ['class' => 'btn btn-success', 'style' => 'margin-top: 20px;']) ?>

    <?php \yii\bootstrap\ActiveForm::end() ?>
<?php }?>



</div>
</div>
</div>