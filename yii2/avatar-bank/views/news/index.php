<?php
use common\widgets\FileUpload3\FileUpload;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */

$this->title = 'Новости';



$school = \common\models\school\School::get();

$query = \common\models\school\NewsItem::find()->where(['school_id' => $school->id]);
$recordsCount = $query->count();

?>
<style>
    .header {
        height: 70px;
    }
    .description {
        height: 150px;
    }

</style>
<div class="container" style="padding-bottom: 70px;">
    <div class="col-lg-12">
        <h1 class="page-header text-center"><?= $this->title ?></h1>
    </div>
    <?php /** @var \common\models\NewsItem $item */ ?>
    <?php foreach ($query->orderBy(['created_at' => SORT_DESC])->all() as $item) { ?>
        <div class="col-lg-3">
            <p style="height: 100px;"><b><?= $item->name ?></b></p>
            <p>
                <a href="<?= Url::to(['news/item', 'id' => $item->id ])?>">
                    <img src="<?= \common\widgets\FileUpload7\FileUpload::getFile($item->image, 'crop') ?>" width="100%" class="thumbnail"/>
                </a>
            </p>
            <p style="color: #ccc;"><?= Yii::$app->formatter->asDate($item->created_at) ?></p>
        </div>
    <?php } ?>
</div>


