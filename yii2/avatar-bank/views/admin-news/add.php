<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;
use app\models\UnionCategory;
use yii\db\Query;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \avatar\models\forms\NewsItem */

$this->title = 'Добавить новость';

?>
<div class="container">
    <div class="col-lg-12">
        <h1 class="page-header"><?= Html::encode($this->title) ?></h1>

        <?php if (Yii::$app->session->hasFlash('form')): ?>

            <div class="alert alert-success">
                Успешно добавлено.
            </div>

            <?php $id = Yii::$app->session->getFlash('form'); ?>
            <p>
                <a href="<?= \yii\helpers\Url::to(['admin-news/edit', 'id' => $id]) ?>" class="btn btn-success">Редактировать</a>
                <a href="<?= \yii\helpers\Url::to(['news-blog/index']) ?>" class="btn btn-default">Новости в админке</a>
                <a href="<?= \yii\helpers\Url::to(['news/index']) ?>" class="btn btn-default">Новости на сайте</a>
                <a href="<?= \yii\helpers\Url::to(['news/item', 'id' => $id]) ?>" class="btn btn-success">Посмотреть на сайте</a>
            </p>

        <?php else: ?>


            <div class="row">
                <div class="col-lg-5">
                    <?php $form = ActiveForm::begin([
                        'options' => ['enctype' => 'multipart/form-data']
                    ]); ?>
                    <?= $model->field($form, 'name') ?>
                    <?= $model->field($form, 'description')->textarea(['rows' => 5]) ?>
                    <?= $model->field($form, 'content') ?>
                    <?= $model->field($form, 'image') ?>


                    <hr>
                    <div class="form-group">
                        <?= Html::submitButton('Добавить', [
                            'class' => 'btn btn-default',
                            'name'  => 'contact-button',
                            'style' => 'width:100%',
                        ]) ?>
                    </div>
                    <?php ActiveForm::end(); ?>
                </div>
            </div>

        <?php endif; ?>
    </div>
</div>
