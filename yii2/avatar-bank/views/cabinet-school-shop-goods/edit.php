<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;
use app\models\UnionCategory;
use yii\db\Query;
use yii\helpers\ArrayHelper;

/** @var $this yii\web\View */
/** @var $form yii\bootstrap\ActiveForm */
/** @var $school \common\models\school\School */
/** @var $model \avatar\models\forms\shop\Product */

$this->title = $model->name;

?>
<div class="container">

    <?php \avatar\widgets\SchoolMenu::begin(['school' => $school]) ?>
    <h1 class="page-header"><?= Html::encode($this->title) ?></h1>

    <p>
        <a href="<?= \yii\helpers\Url::to(['cabinet-school-shop-goods-images/index', 'id' => $model->id]) ?>" class="btn btn-default">Изображения</a>
    </p>

    <?php if (Yii::$app->session->hasFlash('form')) { ?>
        <div class="alert alert-success">
            Успешно обновлено.
        </div>
        <p>
            <a href="<?= \yii\helpers\Url::to(['cabinet-school-shop-goods/index', 'id' => $school->id]) ?>" class="btn btn-success">Товары</a>
            <a href="<?= \yii\helpers\Url::to(['cabinet-school-shop/index', 'id' => $school->id]) ?>" class="btn btn-default">Интернет магазин</a>
        </p>
    <?php } else { ?>

        <div class="row">
            <div class="col-lg-8">
                <?php $form = ActiveForm::begin([
                    'id'      => 'contact-form',
                    'options' => ['enctype' => 'multipart/form-data'],
                ]); ?>
                <?= $model->field($form, 'name') ?>
                <?= $model->field($form, 'price') ?>
                <?= $model->field($form, 'currency_id')->dropDownList(
                    ArrayHelper::map(
                        \common\models\avatar\Currency::find()->all(),
                        'id',
                        'code'
                    ), ['prompt' => '- Ничего не выбрано -']
                )
                ?>
                <?= $model->field($form, 'image') ?>
                <?= $model->field($form, 'content') ?>
                <?= $model->field($form, 'tree_node_id')->dropDownList(
                        ArrayHelper::map(
                                \common\models\shop\CatalogItem::find()->where(['school_id' => $school->id])->all(),
                                'id',
                                'name'
                        ), ['prompt' => '- Ничего не выбрано -']
                ) ?>
                <?= $model->field($form, 'is_electron') ?>

                <hr>
                <div class="form-group">
                    <?= Html::submitButton('Обновить', [
                        'class' => 'btn btn-default',
                        'name'  => 'contact-button',
                        'style' => 'width:100%',
                    ]) ?>
                </div>
                <?php ActiveForm::end(); ?>
            </div>
        </div>
    <?php } ?>
    <?php \avatar\widgets\SchoolMenu::end() ?>


</div>
