<?php

/** @var $this \yii\web\View  */

/** @var $item \avatar\models\UserEnter */

use yii\helpers\Url;
use app\services\GsssHtml;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use app\services\GetArticle\Collection;

$this->title = 'Заявка #' . $item->id;
$school = \common\models\school\School::findOne($item->school_id);
\avatar\assets\Notify::register($this);

?>

<div class="container">

    <?php \avatar\widgets\SchoolMenu::begin(['school' => $school]) ?>
    <h1 class="page-header"><?= Html::encode($this->title) ?></h1>

    <div class="row">
        <div class="col-lg-6">
            <h2 class="page-header">Человек</h2>
            <?= \yii\widgets\DetailView::widget([
                'model'      => $item,
                'attributes' => [
                    [
                        'label'          => 'ID',
                        'value'          => $item->id,
                        'captionOptions' => ['style' => 'width: 40%'],
                    ],
                    [
                        'label' => 'Имя',
                        'value' => $item->name_first,
                    ],
                    [
                        'label' => 'Фамилия',
                        'value' => $item->name_last,
                    ],
                    [
                        'label' => 'Отчество',
                        'value' => $item->name_middle,
                    ],
                    [
                        'label' => 'Телефон',
                        'value' => $item->phone,
                    ],
                    [
                        'label' => 'Почтовый адрес',
                        'value' => $item->address_pochta,
                    ],
                ],
            ]) ?>
            <p><a class="btn btn-primary" href="<?= \yii\helpers\Json::decode($item->file)['data'] ?>">Ссылка на видео</a></p>
        </div>
        <div class="col-lg-6">
            <h2 class="page-header">Паспорт</h2>
            <?= \yii\widgets\DetailView::widget([
                'model'      => $item,
                'attributes' => [
                    [
                        'label'          => 'Серия',
                        'value'          => $item->passport_ser,
                        'captionOptions' => ['style' => 'width: 40%'],
                    ],
                    [
                        'label' => 'Номер',
                        'value' => $item->passport_number,
                    ],
                    [
                        'label' => 'Выдан кем',
                        'value' => $item->passport_vidan_kem,
                    ],
                    [
                        'label'  => 'Выдан когда',
                        'format' => 'date',
                        'value'  => $item->passport_vidan_date,
                    ],
                    [
                        'label' => 'Выдан подразделение',
                        'value' => $item->passport_vidan_num,
                    ],
                    [
                        'label'  => 'Родился когда',
                        'format' => 'date',
                        'value'  => $item->passport_born_date,
                    ],
                    [
                        'label' => 'Родился где',
                        'value' => $item->passport_born_place,
                    ],
                ],
            ]) ?>
            <div class="row">
                <div class="col-lg-3">
                    <a href="<?= $item->passport_scan1 ?>" target="_blank">
                        <img src="<?= \iAvatar777\widgets\FileUpload7\FileUpload::getFile($item->passport_scan1, 'crop') ?>"
                             style="width: 100%;">
                    </a>
                </div>
                <div class="col-lg-3">
                    <a href="<?= $item->passport_scan2 ?>" target="_blank">
                        <img src="<?= \iAvatar777\widgets\FileUpload7\FileUpload::getFile($item->passport_scan2, 'crop') ?>"
                             style="width: 100%;">
                    </a>
                </div>
            </div>
        </div>
    </div>

    <hr>
    <?php
    \avatar\assets\Notify::register($this);
    $uDigSig = \common\models\UserDigitalSign::findOne(['user_id' => Yii::$app->user->id]);
    if ($uDigSig->hasPK()) {
        $this->registerJs(<<<JS
$('.buttonSign').click(function(e) {
    if (confirm('Подтвердите подпись')) {
        ajaxJson({
            url: '/cabinet-school-koop/sign-ajax',
            data: {id: {$item->id}},
            success: function(ret) {
                new Noty({
                    timeout: 1000,
                    theme: 'sunset',
                    type: 'success',
                    layout: 'bottomLeft',
                    text: ret.sign
                }).show();
                
                $('#modalInfo').on('hidden.bs.modal', function(e) {
                    window.location = '/cabinet-school-koop/index?id=' + {$school->id};
                }).modal();
            }
        });
    }
});
JS
        );

    } else {
        $this->registerJs(<<<JS
$('.buttonSign').click(function(e) {
    $('#modalForm').modal();
});
JS
        );

    }
    ?>
    <p><button class="btn btn-success buttonSign">Верифицировать</button>

        <?php \avatar\widgets\SchoolMenu::end() ?>
</div>

<div class="modal fade" id="modalInfo" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="myModalLabel">Подпись информации</h4>
            </div>
            <div class="modal-body">
                Успешно!
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modalForm" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="myModalLabel">Подпись информации</h4>
            </div>
            <div class="modal-body">
                <?php $model = new \avatar\models\validate\CabinetSchoolKoopSign(['id' => $item->id]); ?>
                <?php $form = \iAvatar777\services\FormAjax\ActiveForm::begin([
                    'model'        => $model,
                    'formUrl'      => '/cabinet-school-koop/view-sign',
                    'success' => <<<JS
function (ret) {
    $('#modalForm').on('hidden.bs.modal', function(e) {
        $('#modalInfo').on('hidden.bs.modal', function(e) {
            window.location = '/cabinet-school-koop/index?id=' + {$school->id};
        }).modal();
    }).modal('hide');
}
JS

                ]) ?>
                    <?= Html::activeHiddenInput($model, 'id') ?>
                    <?= $form->field($model, 'secret')->passwordInput()->label('Приватный ключ') ?>
                <?php \iAvatar777\services\FormAjax\ActiveForm::end() ?>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
            </div>
        </div>
    </div>
</div>