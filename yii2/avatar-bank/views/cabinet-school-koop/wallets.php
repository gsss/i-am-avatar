<?php

/** $this \yii\web\View  */
/** @var $school \common\models\school\School */

use yii\helpers\Url;
use app\services\GsssHtml;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use app\services\GetArticle\Collection;

$this->title = 'Фонды';

\avatar\assets\Notify::register($this);


?>

<div class="container">

    <?php \avatar\widgets\SchoolMenu::begin(['school' => $school]) ?>
    <h1 class="page-header"><?= Html::encode($this->title) ?></h1>


    <?php \yii\widgets\Pjax::begin(); ?>
    <?php
    $this->registerJS(<<<JS
$('[data-toggle="tooltip"]').tooltip();

$('.rowTable').click(function(e) {
    window.location = '/cabinet-school-koop/wallets-item' + '?' + 'id' + '=' + $(this).data('id');
});

JS
    );


    ?>
    <?= \yii\grid\GridView::widget([
        'dataProvider' => new \yii\data\ActiveDataProvider([
            'query'      => \common\models\SchoolKoopWallet::find()
                ->where(['school_id' => $school->id])
            ,
            'pagination' => [
                'pageSize' => 20,
            ],
        ]),
        'summary' => '',
        'tableOptions' => [
            'class' => 'table table-striped table-hover',
        ],
        'rowOptions'   => function ($item) {
            $data = [
                'data'  => ['id' => $item['id']],
                'role'  => 'button',
                'class' => 'rowTable',
            ];
            return $data;
        },
        'columns'      => [
            [
                'header'    => 'WID',
                'content'   => function ($item) {
                    $w = new \common\models\piramida\Wallet(['id' => $item->wallet_id]);
                    $addressShort = $w->getAddressShort();

                    return Html::tag('code', $addressShort);
                },
            ],
            [
                'header'  => 'Валюта',
                'content' => function ($item) {
                    $link = \common\models\avatar\CurrencyLink::findOne(['currency_int_id' => $item->currency_id]);
                    $currency = \common\models\avatar\Currency::findOne($link->currency_ext_id);
                    if (is_null($currency)) return '';

                    return Html::tag('span', $currency->code, ['class' => 'label label-info']);
                },
            ],
            [
                'header'  => '',
                'content' => function ($item) {
                    $link = \common\models\avatar\CurrencyLink::findOne(['currency_int_id' => $item->currency_id]);
                    $currency = \common\models\avatar\Currency::findOne($link->currency_ext_id);
                    if (is_null($currency)) return '';

                    return Html::img(\common\widgets\FileUpload7\FileUpload::getFile($currency->image, 'crop'), [
                        'class'  => "img-circle",
                        'width'  => 60,
                        'height' => 60,
                        'style'  => 'margin-bottom: 0px;',
                    ]);
                },
            ],
            [
                'header'  => 'Название',
                'content' => function ($item) {
                    return $item['name'];
                },
            ],

            [
                'header'         => 'Монет',
                'headerOptions'  => ['class' => 'text-right'],
                'contentOptions' => ['class' => 'text-right'],
                'content'        => function ($item) {
                    $wallet =\common\models\piramida\Wallet::findOne($item->wallet_id);
                    if (is_null($wallet)) return '';

                    return Yii::$app->formatter->asDecimal($wallet->getAmountWithDecimals(), 2);
                },
            ],
        ],
    ]) ?>
    <?php \yii\widgets\Pjax::end(); ?>

</div>
