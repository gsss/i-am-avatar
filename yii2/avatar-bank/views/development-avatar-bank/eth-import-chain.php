
<?php
use common\services\UsersInCache;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;

/* @var $this yii\web\View */

$this->title = 'ETH. Импортирование цепи';

?>
<div class="container">
    <div class="col-lg-12">
        <h1 class="page-header text-center"><?= Html::encode($this->title) ?></h1>
        <p class="alert alert-danger">В разработке</>
        <p>Потребность: так как нет нормального API который предоставляет данные о транзакциях токенов на кошельках, то его надо сделать самому</p>
        <p>Решение как предположение: создать свою таблицу движения токенов, и сканировать цепь эфира на предмет передвижения токенов.</p>
        <p>Так как в одной транзакции может быть несколько движений токенов, то определять факт движения токенов надо по логам транзакции.</p>
        <p>На каждое движение токена ERC-20 принято вызывать событие Transfer, я так понял оно отражается в логе транзакции.</p>
        <p>Его тип <code>0xddf252ad1be2c89b69c2b068fc378daa952ba7f163c4a11628f55a4df523b3ef</code></p>
        <p>Далее следует адрес отправителя, получателя и копейки переводимого токена.</p>
        <p>Как посмотреть логи транзакции?</p>
        <pre>$provider = new \avatar\modules\ETH\ServiceInfura();
$data = $provider->_method('eth_getTransactionReceipt', [$hash]);</pre>
        <p><img src="/images/controller/development-avatar-bank/eth-import-chain/2018-03-03_22-10-21.png" class="thumbnail"></p>
        <p><img src="/images/controller/development-avatar-bank/eth-import-chain/2018-03-03_22-36-42.png" class="thumbnail"></p>

        <h2 class="page-header">Составление модели данных</h2>
        <p>Так как одна транзакция подпадает сразу под два кошелька, фигурирует в обоих, то для того чтобы иметь возможность выборки данных по кошельку, можно организовать два вида решения</p>
        <p>1. Использование одной таблицы с инверсией полей from и to. То есть для одной транзакции создается две записи с одним хешем.</p>
        <p>1. Использование двух таблиц. Одна для транзакций. Вторая содержит адрес кошелька, тип операции, идентификатор транзакции. И опять для одной транзакции создается одна запись в таблице 1 и две записи в 2-й таблице.</p>
        <p>Очевидно второй вариант лучше потому что: 1. Нет избыточной перегрузки данных. 2. Просто сделать выборку по одному кошельку. </p>
        <p>Минус здесь только один: Нужно использовать Join или два запроса. Но все равно это лучше чем перегрузка данных.</p>


                <?= \common\services\documentation\DbTable::widget([
                    'name'        => 'eth_token_transaction',
                    'description' => 'Хранит транзакции в которых есть упоминание о движении токенов. (transfer) и те которые уже вошли в блоки и которые выполнились без ошибок',
                    'columns'     => [
                        [
                            'name'        => 'id',
                            'type'        => 'int',
                            'isRequired'  => true,
                            'description' => 'Идентификатор записи',
                        ],
                        [
                            'name'        => 'time',
                            'type'        => 'int',
                            'isRequired'  => true,
                            'description' => 'Момент времени создания транзакции',
                        ],
                        [
                            'name'        => 'block',
                            'type'        => 'int',
                            'isRequired'  => true,
                            'description' => 'Высота блока в которой запакована транзакция',
                        ],
                        [
                            'name'        => 'comission',
                            'type'        => 'bigint',
                            'isRequired'  => true,
                            'description' => 'Комиссия в wei затраченная на выполнение всей транзакции',
                        ],
                        [
                            'name'        => 'hash',
                            'type'        => 'varchar(100)',
                            'isRequired'  => true,
                            'description' => 'Хеш транзакции транзакции, начинается с 0x и только малые буквы',
                        ],
                    ],
                ]) ?>

                <?= \common\services\documentation\DbTable::widget([
                    'name'        => 'eth_token_transfer',
                    'description' => 'Хранит события передвижения токенов',
                    'columns'     => [
                        [
                            'name'        => 'id',
                            'type'        => 'int',
                            'isRequired'  => true,
                            'description' => 'Идентификатор записи',
                        ],
                        [
                            'name'        => 'transaction_id',
                            'type'        => 'int',
                            'isRequired'  => true,
                            'description' => 'Идентификатор транзакции eth_token_transaction',
                        ],
                        [
                            'name'        => 'owner',
                            'type'        => 'varchar(80)',
                            'isRequired'  => true,
                            'description' => 'Идентификатор адреса владельца адреса, начинается с 0x и только малые буквы',
                        ],
                        [
                            'name'        => 'partner',
                            'type'        => 'varchar(80)',
                            'isRequired'  => true,
                            'description' => 'Идентификатор адреса партнера сделки, начинается с 0x и только малые буквы',
                        ],
                        [
                            'name'        => 'amount',
                            'type'        => 'varchar(80)',
                            'isRequired'  => true,
                            'description' => 'Кол-во копеек токена которые были отправлены',
                        ],
                        [
                            'name'        => 'contract',
                            'type'        => 'varchar(80)',
                            'isRequired'  => true,
                            'description' => 'Адрес контракта токена, начинается с 0x и только малые буквы',
                        ],
                        [
                            'name'        => 'type',
                            'type'        => 'tinyint',
                            'isRequired'  => true,
                            'description' => 'Тип операции для владельца адреса. -1 - расход для owner. 1 - приход для owner',
                        ],
                    ],
                ]) ?>

                <p>Поэтому для того чтобы сделать выборку по кошельку надо сделать запрос:</p>

                <pre>SELECT
eth_token_transfer.from,
eth_token_transfer.to,
eth_token_transfer.amount,
eth_token_transfer.type,
eth_token_transaction.hash,
eth_token_transaction.time

FROM eth_token_transfer
INNER JOIN eth_token_transaction ON (eth_token_transaction.id = eth_token_transfer.transaction_id)
WHERE
    eth_token_transfer.from = '{from}'
    AND
    eth_token_transfer.contract = '{contract}'
ORDER BY
    eth_token_transaction.time DESC</pre>


        <h2 class="page-header">Синхронизация блокчейна</h2>
        <p>Для того чтобы синхронизировать базу используется консольная команда</p>
        <pre>php yii eth/sync-chain</pre>
        <p>Она загружает по 100 блоков цепи на каждый вызов.</p>
        <p>Если происходит сброс то она сама начинает догрузку с сохраненного блока</p>
        <p>Обработчик этой функции находится в классе <code>\console\controllers\actions\EthController\actionSyncChain</code></p>

        <h2 class="page-header">Сброс синхронизации</h2>
        <p>Для того чтобы сбросить синхронизизацию используется консольная команда</p>
        <pre>php yii eth/sync-chain-reset</pre>

    </div>

</div>



