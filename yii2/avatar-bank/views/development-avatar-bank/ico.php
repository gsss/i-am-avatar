<?php
use common\services\UsersInCache;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;

/* @var $this yii\web\View */

$this->title = 'ICO Аватар Банка';

?>
<div class="container">
    <div class="col-lg-12">
        <h1 class="page-header text-center"><?= Html::encode($this->title) ?></h1>

        <p>ICO будет проходить на странице <code>https://www.avatar-bank.com/ico</code></p>

        <h2 class="page-header">Данные необходимые для покупки токенов</h2>
        <p>1. Выбрать валюту (платежная система) для оплаты</p>
        <p>2. Email</p>
        <p>3. Кол-во токенов</p>









        <h2 class="page-header">Схема движения клиента по лендингу</h2>

        <p>1. Зайти на страницу</p>
        <p>2. Выбрать валюту для оплаты</p>
        <p>3. Выбрать платежную систему</p>
        <p>4. Прочитать инструкцию о покупке токенов</p>
        <p>5. Выполнить инструкцию о покупке</p>
        <p>6. Если платежная система автоматом происходит, то клиент получает токены на свой кошелек</p>
        <p>7. Если платежная система не автоматом происходит, то клиент получает токены на свой кошелек после
            подтверждения менеджером</p>

        <h3 class="page-header">Проекты</h3>

        <p>
            <a href="https://drive.google.com/file/d/0B9lrcmdS-YbTbGtLVk5QS3QtaDQ/view?usp=sharing" target="_blank">
                <img src="/images/controller/development-avatar-bank/ico/schema.png" class="thumbnail" width="100%">
            </a>
        </p>

        <?= \common\services\documentation\DbTable::widget([
            'name'    => 'projects',
            'columns' => [
                [
                    'name'        => 'id',
                    'isRequired'  => true,
                    'type'        => 'int',
                    'description' => 'Идентификатор',
                ],
                [
                    'name'        => 'name',
                    'isRequired'  => true,
                    'type'        => 'varchar(255)',
                    'description' => 'Наименование проекта',
                ],
                [
                    'name'        => 'key',
                    'isRequired'  => true,
                    'type'        => 'varchar(10)',
                    'description' => 'Идентификатор проекта, случайная строка из 10 символов',
                ],
                [
                    'name'        => 'content',
                    'isRequired'  => true,
                    'type'        => 'text',
                    'description' => 'Текст описания проекта',
                ],
                [
                    'name'        => 'image',
                    'isRequired'  => true,
                    'type'        => 'varchar(255)',
                    'description' => 'Картинка брендированная для проекта',
                ],
                [
                    'name'        => 'user_id',
                    'isRequired'  => true,
                    'type'        => 'int',
                    'description' => 'Идентификатор пользователя кому принадлежит проект',
                ],
                [
                    'name'        => 'status',
                    'isRequired'  => true,
                    'type'        => 'int',
                    'description' => join('<br>', [
                        'Флаг. Прошел врификацию?',
                        '0 - не прошел верификацию и не отправил на верификацию (черновик)',
                        '2 - отправил на верификацию',
                        '3 - верифицировано',
                    ]),
                ],
                [
                    'name'        => 'time_start',
                    'isRequired'  => true,
                    'type'        => 'int',
                    'description' => 'Время начала проекта',
                ],
                [
                    'name'        => 'time_end',
                    'isRequired'  => true,
                    'type'        => 'int',
                    'description' => 'Время окончания проекта',
                ],
                [
                    'name'        => 'currency',
                    'isRequired'  => true,
                    'type'        => 'int',
                    'description' => 'Идентификатор валюты по таблице '.Html::tag('code', 'currency'),
                ],
                [
                    'name'        => 'sum',
                    'isRequired'  => true,
                    'type'        => 'double',
                    'description' => 'Сумма необходимая для сбора денег',
                ],
                [
                    'name'        => 'created_at',
                    'isRequired'  => true,
                    'type'        => 'int',
                    'description' => 'Время добавления проекта',
                ],
                [
                    'name'        => 'type',
                    'isRequired'  => true,
                    'type'        => 'int',
                    'description' => 'Тип проекта 0 - краудфандинг, 1 - ico',
                ],
                [
                    'name'        => 'payments_list_id',
                    'isRequired'  => true,
                    'type'        => 'int',
                    'description' => 'Идентификатор листа приема денег '.Html::tag('code', 'paysystems_config_list.id'),
                ],
                [
                    'name'        => 'is_hide_client',
                    'type'        => 'int',
                    'description' => 'Флаг, скрыть проект с точки зрения клиента',
                ],
                [
                    'name'        => 'is_hide_platform',
                    'type'        => 'int',
                    'description' => 'Флаг, скрыть проект с точки зрения платформы',
                ],
            ],
        ]) ?>
        <h4 class="page-header">Политика показа проекта среди зрителей</h4>
        <p>Если <code>status</code> = STATUS_ASSEPTED и <code>is_hide_client</code> = 0 и <code>is_hide_platform</code> = 0, то проект будет виден.</p>
        <p>Сортировка осуществляется по действующим проектам.</p>

        <h4 class="page-header">Сопоставление проекта и монеты</h4>
        <?= \common\services\documentation\DbTable::widget([
            'name'    => 'projects_ico',
            'columns' => [
                [
                    'name'        => 'id',
                    'isRequired'  => true,
                    'type'        => 'int',
                    'description' => 'Идентификатор проекта, не автоинкремент',
                ],
                [
                    'name'        => 'billing_id',
                    'isRequired'  => true,
                    'type'        => 'int',
                    'description' => 'Идентификатор счета на котором лежат все монеты',
                ],
                [
                    'name'        => 'currency_id',
                    'isRequired'  => true,
                    'type'        => 'int',
                    'description' => 'Идентификатор валюты по таблице currency',
                ],
                [
                    'name'        => 'remained_tokens',
                    'isRequired'  => true,
                    'type'        => 'double',
                    'description' => 'Кол-во токенов которые остались на распродаже',
                ],
                [
                    'name'        => 'all_tokens',
                    'isRequired'  => true,
                    'type'        => 'double',
                    'description' => 'Кол-во токенов которые были выставлены на продажу',
                ],
                [
                    'name'        => 'kurs',
                    'isRequired'  => true,
                    'type'        => 'double',
                    'description' => 'Курс токена в валюте указанной в ' . Html::tag('code', 'kurs_currency_id'),
                ],
                [
                    'name'        => 'kurs_currency_id',
                    'isRequired'  => true,
                    'type'        => 'double',
                    'description' => 'Идентификатор валюты в которой указан курс ' . Html::tag('code', 'kurs'),
                ],
            ],
        ]) ?>

        <p>Для того чтобы сформировать проект нужно указать информацию о проекте и информацию о монете</p>
        <p><code>billing_id</code> нужно создать, <code>currency_id</code> нужно добавить изображение и сделаю это через <code>\common\widgets\FileUpload4\FileUploadMany</code></p>




        <h3 class="page-header">Заказ</h3>
        <p>Модель: <code>\common\models\IcoRequest</code></p>
        <?= \common\services\documentation\DbTable::widget([
            'name'    => 'ico_request',
            'columns' => [
                [
                    'name'        => 'id',
                    'isRequired'  => true,
                    'type'        => 'int',
                    'description' => 'Идентификатор',
                ],
                [
                    'name'        => 'ico_id',
                    'isRequired'  => true,
                    'type'        => 'int',
                    'description' => 'Идентификатор ICO, если нет то это ICO Аватар Банка',
                ],
                [
                    'name'        => 'paysystem_config_id',
                    'isRequired'  => true,
                    'type'        => 'int',
                    'description' => 'Идентификатор настройки платежной систему через которую человек выбрал платить',
                ],
                [
                    'name'        => 'billing_id',
                    'isRequired'  => true,
                    'type'        => 'int',
                    'description' => 'идентификатор счета который прикреплен к заказу',
                ],
                [
                    'name'        => 'address',
                    'type'        => 'varchar(70)',
                    'description' => 'адрес эфира для доставки токенов, если нет то это значит что есть пользователь user_id',
                ],
                [
                    'name'        => 'email',
                    'type'        => 'varchar(70)',
                    'description' => 'адрес почты клиента если он не авторизован, если нет то это значит что есть пользователь user_id',
                ],
                [
                    'name'        => 'created_at',
                    'isRequired'  => true,
                    'type'        => 'int',
                    'description' => 'время создания заявки',
                ],
                [
                    'name'        => 'is_paid',
                    'isRequired'  => true,
                    'type'        => 'int',
                    'description' => 'флаг оплаты заказа, 0 - не оплачено, 1 - оплачено. 0 - по умолчанию',
                ],
                [
                    'name'        => 'user_id',
                    'type'        => 'int',
                    'description' => 'идентификатор пользователя если он авторизован в системе',
                ],
                [
                    'name'        => 'tokens',
                    'isRequired'  => true,
                    'type'        => 'double',
                    'description' => 'колво покупаемых токенов',
                ],
            ],
        ]) ?>
        <?= \common\services\documentation\DbTable::widget([
            'name'    => 'ico_request_success',
            'columns' => [
                [
                    'name'        => 'id',
                    'isRequired'  => true,
                    'type'        => 'int',
                    'description' => 'Идентификатор',
                ],
                [
                    'name'        => 'created_at',
                    'isRequired'  => true,
                    'type'        => 'int',
                    'description' => 'Время добавления транзакции',
                ],
                [
                    'name'        => 'txid',
                    'isRequired'  => true,
                    'type'        => 'varchar(80)',
                    'description' => 'Адрес транзакции',
                ],
            ],
        ]) ?>
        <p>Всего есль два варианта содержимого записи</p>
        <ul>
            <li>Пользователь авторизован, указывается <code>user_id</code>, а <code>address</code> и <code>email</code>
                = NULL
            </li>
            <li>Пользователь не авторизован, указывается <code>user_id</code> = NULL, а <code>address</code> - адрес
                доставки токенов и <code>email</code> = почта для связи
            </li>
        </ul>







        <h2 class="page-header">Ethereum</h2>
        <p>Модель: <code>\common\models\PaymentEthereum</code></p>
        <?= \common\services\documentation\DbTable::widget([
            'name'    => 'payments_ethereum',
            'columns' => [
                [
                    'name'        => 'id',
                    'isRequired'  => true,
                    'type'        => 'int',
                    'description' => 'Идентификатор',
                ],
                [
                    'name'        => 'value',
                    'isRequired'  => true,
                    'type'        => 'double',
                    'description' => 'кол-во денег для перевода в ETH',
                ],
                [
                    'name'        => 'action',
                    'type'        => 'varchar(20)',
                    'description' => 'Идентификатор заказа',
                ],
                [
                    'name'        => 'transaction',
                    'type'        => 'varchar(80)',
                    'description' => 'Идентификатор транзакции, начиная с 0x',
                ],
                [
                    'name'        => 'from',
                    'type'        => 'varchar(60)',
                    'description' => 'Адрес счета от кого пришли деньги',
                ],
            ],
        ]) ?>
        <p>Схема передачи настроек и параметров</p>
        <p>
            <a href="https://drive.google.com/file/d/0BzHYNoEyPNTXYVdvaXZzZ2VBaEk/view?usp=sharing" target="_blank">
                <img src="/images/controller/development-avatar-bank/ico/ethereum.png" class="thumbanil">
            </a>
        </p>



        <h2 class="page-header">Реализация заказов в кабинете</h2>
        <p>URL: <code>/cabinet-ico/index</code></p>

        <h2 class="page-header">Вычисление статистики ICO</h2>
        <p>Вычисление суммы подтвержденных сделок ICO</p>
        <pre>
$sum = \common\models\IcoRequest::find()
->where(['ico_request.is_paid' => 1])
->andWhere(['ico_request.ico_id' => $IcoID])
->innerJoin('billing', 'ico_request.billing_id = billing.id')
->innerJoin('currency', 'currency.id = billing.currency_id')
->select([
    'sum(billing.sum_after * currency.kurs) as sum',
])
->scalar()</pre>

        <p>Вычисление суммы подтвержденных сделок ICO по валюте</p>
        <pre>
$sum = \common\models\IcoRequest::find()
->where(['ico_request.is_paid' => 1])
->andWhere(['ico_request.ico_id' => $IcoID])
->andWhere(['billing.currency_id' => $currency['id']])
->innerJoin('billing', 'ico_request.billing_id = billing.id')
->select([
    'sum(billing.sum_after) as sum',
])
->scalar()</pre>










        <h3 class="page-header">Процедура подтверждения сделки (транзакции)</h3>

        <p>Предлагается вести порядок подтверждения сделок по ethereum следующим образом. Клиент при заказе вводит
            идентификатор транзакции по которой он оплатил заказ.</p>
        <p>В фоне на сервере каждую минуту запускается крон, который смотрит открытые заказы по ethereum, проверяет на
            кол-во подтверждений, если кол-во подтверждений достаточное то закрывать заказ.</p>
        <p>Таким образом все заказы на Ethereum сети все заказы будут закрываться автоматически.</p>
        <p><b>Решение</b></p>
        <p>Крон скрипт: <code>php yii ico/close-requests-ethereum</code></p>

        <h4 class="page-header">Подтверждение платежа</h4>
        <p>Когда клиент указывает что оплатил, нужно проверить его транзакцию</p>
        <p>К тому же вопрос: Если с течением времени курс меняется то значит выставленный счет нужно ограничить по времени.
            Например 1 час он действует. Если клиент не указал транзакцию в течение этого времени то ему нужно запросить новую заявку а старую отменить
            или она сама должна закрываться или не показываться</p>
        <p>После проверки нужно пометить что она уже проверена и поставить результат проверки, успешно или не успешно. И понять что делать в каждом случае.</p>


        <h4 class="page-header">Подтверждение транзакции по платежу</h4>
        <p>Предполагается что выставленный счет действует 1 час после его выставления.</p>
        <p>Для автоматической проверки платежей я сделаю сервис крон который будет запускаться 1 раз в 10 мин, выбирать список платежей и проверять на приход денег.</p>
        <p>Критерий для выборки: Заявки в которых указано что пользователь оплатил и указал транзакцию. Статус заказа: <code>\common\models\investment\IcoRequest::STATUS_USER_CONFIRM</code></p>
        <p>В кроне запускается раз в 10 мин команда</p>
        <pre>php yii ico/close-requests</pre>

        <p>Если я поставлю флаги проверки пришедшей трензакции в статус то поставив, токены высланы я не смогу потом выяснить а были ли эти флаги. Они нужны для истории.</p>
        <p>В итоге надо выяснить теперь куда их разместить.</p>
        <p>1 в заказ</p>
        <p>2 в отдельную таблицу</p>
        <p>Исследование</p>
        <p>для каждого ли заказа нужна проверка такой транзакции? или она может зависеть от ПС?</p>
        <p>Например Альфа Банк проверить нигде нельзя, хотя можно, хотябы вручную. Тогда получается во всех ПС нужен такой флаг
            а раз так то значит эти флаги д.б. прямо в заказе.</p>
        <p>Например</p>
        <p><code>is_verify</code> - 0/1</p>
        <p><code>is_verify_result</code> - null/1/2 null - не проверена, 1 - успешно, 2 - не успешно.</p>

        <p>Критерии для проверки одного платежа:</p>
        <ul>
            <li>Кол-во подтверждений должно быть достаточным для гарантии доверия для транзакции (понять где хранить параметр)</li>
            <li>В транзакции монеты должны прийти на адрес указанный в счете</li>
            <li>В транзакции кол-во переданных монет должно строго соответствовать кол-ву указанных в счете</li>
            <li>По поводу дважды указанной транзакции нужно исследовать</li>
        </ul>
        <p><b>Дважды указанная транзакция</b></p>
        <p>Если транзакцию клиент указал ту же которая была указана ранее, то ее нельхя считать валидной,
            а высылать письмо клиенту что это не валидная транзакция по причине того что кто то уже указал эту транзакцию</p>
        <p><b>Обработка ошибок при платежах</b></p>
        <p>
            Если вознакает ошибка в платеже то для того чтобы ее дважды не обрабатывать я ее помечаю флагом. Каким? <code>ico_request.payment_is_error</code>. Пока так.<br>
            0 - не проверенный платеж, <br>
            1 - проверенный платеж и ошибка в платеже,<br>
            2 - проверенный платеж и успешно поддтвержденный.
            3 - время платежа истекло (предполагается что нужно указать платеж в течение 1 часа)
        </p>
        <p>Так как нет смысла обрабатывать уже подтвержденные платежи и с ошибкой, поэтому корректируется запрос для выборки и добавляется новый параметр <code>ico_request.payment_is_error = 0</code></p>
        <p>Если платеж подтверждается, то для заказа ставится статус <code>\common\models\investment\IcoRequest::STATUS_SHOP_CONFIRM</code>. На основе него будут высылаться уже токены.</p>


        <p>Если транзакция не находится (4 статус) то значит здесь может быть несколько вариантов дальнейших событий</p>
        <p>1. Не верно указана транзакция (ее в блокчейне физически не существует). Решение: нужно закрыть транзакцию</p>
        <p>2. Не показывается транзакция в блокчейне из-за перегрузки системы. Решение: нужно ждать</p>
        <p>Вопрос как отличить первое от второго?</p>


        <h4 class="page-header">Транзакция перевода токена</h4>
        <p>После подтверждения сделки продажи токена, производится перевод токена и нужно где то хранить эту транзакцию.</p>
        <p>Предложение сделать отдельную таблицу, где хранится <code>ico_request_success</code> <code>id, txid</code>.</p>
        <p>Модель: <code>\common\models\IcoRequestSuccess</code></p>
        <?= \common\services\documentation\DbTable::widget([
            'name'    => 'ico_request_success',
            'columns' => [
                [
                    'name'        => 'id',
                    'isRequired'  => true,
                    'type'        => 'int',
                    'description' => 'Идентификатор заказа',
                ],
                [
                    'name'        => 'created_at',
                    'isRequired'  => true,
                    'type'        => 'int',
                    'description' => 'Время добавления записи',
                ],
                [
                    'name'        => 'txid',
                    'isRequired'  => true,
                    'type'        => 'varchar(80)',
                    'description' => 'адрес транзакции',
                ],
            ],
        ]) ?>
        <p>Токен переводится в функции <code>\common\models\IcoRequest::sendTokens()</code>. Там определяется какой токен переводить. И он отправляется.</p>


        <h3 class="page-header">ICO и краудфандинг</h3>
        <p><b>ICO</b></p>
        <p><code>sum</code> - кол-во выпущенных монет</p>
        <p><code>currency</code> - идентификатор валюты выпущенного токена</p>
        <p>нужно еще свойство сколько осталось монет</p>

        <p><b>краудфандинг</b></p>
        <p><code>sum</code> - кол-во необходимых денег для сбора</p>
        <p><code>currency</code> - идентификатор валюты для sum</p>
        <p>нужно еще свойство сколько осталось собрать денег</p>


        <h3 class="page-header">Платежные системы</h3>
        <ul>
            <li><a href="https://blockchain.info/" target="_blank">BitCoin</a></li>
            <li><a href="https://bitinfocharts.com/bitcoin%20cash/explorer/" target="_blank">BitCoinCash</a> ?</li>
            <li><a href="https://etherscan.io/" target="_blank">Ethereum</a></li>
            <li><a href="http://wavesexplorer.com/" target="_blank">Waves</a></li>
            <li><a href="https://emercoin.mintr.org/img/emercoin_header.png" target="_blank">ETC</a></li>
            <li><a href="https://etcchain.com/explorer" target="_blank">LightCoin</a></li>
            <li><a href="http://dogechain.info/" target="_blank">DogeCoin</a></li>
        </ul>

        <h3 class="page-header">Схема покупки токена</h3>
        <p>
            <a href="https://drive.google.com/file/d/0BzHYNoEyPNTXNUpfQ0FXWnltLTA/view?usp=sharing" target="_blank">
                <img src="/images/controller/development-avatar-bank/ico/buy-token.png" class="thumbnail">
            </a>
        </p>

        <h3 class="page-header">Кабинет пользователя</h3>

        <p>
            <img src="/images/controller/development-avatar-bank/ico/requests.png" class="thumbnail">
        </p>
        <p>
            <img src="/images/controller/development-avatar-bank/ico/request.png" width="50%" class="thumbnail">
        </p>

        <p><code>/cabinet-ico/confirm</code></p>
        <pre>txid: $('#field-txid').val(),
id: {$request->id}</pre>

        <p>
            По клику переходит в саму заявку.
        </p>
        <p>
            У клиента может быть много заявок на покупку разных токенов.
        </p>

        <p>
            <code>/cabinet-ico/index</code>
        </p>
        <p>
            <code>/cabinet-ico/request?id=#</code> - заявка
        </p>


        <h3 class="page-header">Событие создание кошелька и заказа</h3>
        <p>Устанавливается статус: <code>STATUS_CREATED</code></p>
        <pre>$IcoRequest->addStatusToShop(IcoRequest::STATUS_CREATED);</pre>

        <h3 class="page-header">Событие отправки токенов</h3>
        <p><code>/cabinet-projects/send</code></p>
        <p><img src="/images/controller/development-avatar-bank/ico/send-example.png" class="thumbnail" width="100%"> </p>

        <h3 class="page-header">Создание ICO</h3>
        <p>Когда создается ICO необходимо:</p>
        <ul>
            <li>Выпустить токен</li>
            <li>Сделать счет</li>
            <li>Сдалать проект</li>
            <li>Привязать счет к проекту</li>
            <li>Указать кошельки на которые будут отправляться деньги за токены</li>
            <li>Запустить ICO</li>
        </ul>

        <h4 class="page-header">Страница создания проекта</h4>
        <p>Есть два поля</p>
        <p>1 - кол-во токенов для продажи</p>
        <p>2 - счет где они размещаются. Формат BitCoin (ВТС) [адрес счета]</p>
        <p><img src="/images/controller/development-avatar-bank/ico/2017-08-25_09-31-28.png" width="50%" class="thumbnail"></p>


        <h4 class="page-header">Бонусная программа</h4>
        <p>Хочу еще разработать универсальную бонусную программу</p>
        <p>Что может быть?</p>
        <p>1 цена токена в зависимости от времени</p>
        <p>Подарки за пакеты</p>
        <p>750 012 000 000 000 000</p>
        <h4 class="page-header">Заказы в проекте</h4>
        <p><img src="/images/controller/development-avatar-bank/ico/2017-08-18_02-11-18.png" width="50%" class="thumbnail"></p>

        <p>Какую транзакуию показывать?</p>
        <p>можно две: сверху ту которую отправил клиент, а снизу отправка токена. так как когда токены отправлены заказ считается закрытым и пропадает из списка то и нет смысла во второй транзакции. только если в архиве.</p>
        <p>Что означает оплачено?</p>
        <p>Для этого надо провести исследование. А сколько же состояний в заказе?</p>
        <ul>
            <li>Сформирован заказ</li>
            <li>Клиент подтвердил что оплатил и указал транзакцию</li>
            <li>Хозяин проекта подтвердил оплату</li>
            <li>Токены отправлены. Идентификатор транзакции</li>
        </ul>
        <p>Указанная транзакция уже является фактом отправки, может ли быть что клиент подтвердил оплату но не указал транзакцию? да. Поэтому нужен идентификатор что оплачен заказ. Ок Решение оставляю. Критерий: Клиент подтвердил оплату.</p>
        <p>Кнопка закрывает 3 и 4 пункт. Хозяин проекта подтвердил оплату, Токены отправлены. Идентификатор транзакции</p>
        <p>Нет все же я укажу транзакцию и факт что есть</p>


        <h5 class="page-header">Класс платежа</h5>
        <p><code>\common\models\piramida\WalletSourceInterface</code></p>
        <p>Для реализации данной потребности я сделаю в консольном контроллере <code>\console\controllers\IcoController</code> функция <code>\console\controllers\IcoController::actionCloseRequests</code></p>
        <p>Проверяются все заказы с подтвержденными заказами и проверяются</p>
        <p>В интерфейсе <code>\common\models\piramida\WalletSourceInterface</code> должна быть функция например getTransaction2</p>
        <p>Возвращает</p>
        <pre>
[
    'confirmations' => 6,
    'from'          => ['address1' => amount],
    'to'            => ['address1' => amount],
]</pre>
        <p>Также можно реализовать функции <code>isEnoughConfirmations(txid)</code>, <code>isExistAddress(txid,address,amount)</code>
            так как для разных платежных систем разные
            варианты реализации подтверждения.</p>

        <p>Или еще проще <code>isTransactionPaidBilling()</code>. Без аргументов потому что они (txid,address,amount) уже есть в таблице платежной системы, либо они могут меняться в зависимости от ПС.</p>

        <h3 class="page-header">Модерация</h3>

        <p>Поле: <code>projects.status</code></p>

        <p>0 - Черновик</p>
        <p>1 - Отправлено</p>
        <p>2 - Утверждено</p>
        <p>3 - Отклонено</p>

        <p>Письма на модерацию отправляются на почту указанную в <code>Yii::$app->params['projects']['moderationMail']</code></p>
        <p>После создания проект получает статус Черновик. После этого человек должен отправить его на модерацию. При этом отправляется письмо на <code>Yii::$app->params['projects']['moderationMail']</code>.</p>
        <p>Модератор заходит в раздел <code>/admin-projects/index</code> и утверждает или отклоняет проект</p>

        <h3 class="page-header">Подтверждение счетов</h3>
        <p>Выполняется скриптом CRON раз в минуту</p>
        <p><code>php yii ico/close-requests</code></p>

    </div>
</div>



