<?php
use common\services\UsersInCache;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;

/* @var $this yii\web\View */

$this->title = 'Авторизация';

?>
<div class="container">
    <div class="col-lg-12">
        <h1 class="page-header text-center"><?= Html::encode($this->title) ?></h1>
        <p>Время жизни сессии 1440 сек.</p>

        <h2 class="page-header">Двухфакторная авторизация</h2>
        <h2 class="page-header">Google authenticator</h2>
        <p>Что делать, если Google authenticator всегда выдает неправильные коды:
            <a href="https://habrahabr.ru/post/257197/" target="_blank">https://habrahabr.ru/post/257197/</a></p>
        <p>Сохранение</p>
        <pre>
$file = Yii::getAlias('@vendor/chregu/GoogleAuthenticator.php/lib/GoogleAuthenticator.php');
require_once($file);

$ga = new \GoogleAuthenticator;
$secret = $ga->generateSecret();
\common\models\investment\IcoRequestSuccess::add(['txid' => $secret]);
Yii::info($secret, 'avatar\controllers\TestController::actionGoogleTest1');
Yii::$app->cache->set('googleKey', $secret);
$url = sprintf("otpauth://totp/%s?secret=%s", 'dram1008@yandex.ru@avatar-bank.com', $secret);

$content = (new \Endroid\QrCode\QrCode())
->setText($url)
->setSize(180)
->setPadding(20)
->setErrorCorrection('high')
->setForegroundColor(['r' => 0, 'g' => 0, 'b' => 0, 'a' => 0])
->setBackgroundColor(['r' => 255, 'g' => 255, 'b' => 255, 'a' => 0])
->setLabelFontSize(16)
->setImageType(\Endroid\QrCode\QrCode::IMAGE_TYPE_PNG)
->get('png');
$src = 'data:image/png;base64,' . base64_encode($content);</pre>

        <p>Проверка</p>
        <pre>
$file = Yii::getAlias('@vendor/chregu/GoogleAuthenticator.php/lib/GoogleAuthenticator.php');
require_once($file);

$ga = new \GoogleAuthenticator;
$rows = \common\models\investment\IcoRequestSuccess::find()->orderBy(['id' => SORT_ASC])->all();
$c = count($rows);
$s = $rows[$c - 2]->txid;
Yii::info($s, 'avatar\\models\\forms\\GoogleTest2::validateCode()');
$code = $ga->getCode($s);
if ($this->code != $code) {
    $this->addError($attribute, 'Не верный код');
}</pre>
        <p>Появился глюк в Google authenticator или при его генерации. Заключается он в том что когда код один раз сгенерировался и отобразился это первый раз. Но когда код с сохранением выполняется почему то два раза.
            Поэтому при сохранении записывается обновленный уже код, а пользователь присоединяет себе первый в приложение. Поэтому при получении секрета проверка не удается.</p>
        <p>Решение: при записи кода если запись второго кода идет после более чем n сек после прошлого сохранения то он не будет сохранен.</p>
        <h2 class="page-header">Решение</h2>
        <p>Где будет храниться код? В таблице <code>user</code> и поле <code>google_auth_code</code>.</p>
        <p>Какая функция сохранения? В классе <code>\common\models\UserAvatar</code> функция <code>saveGoogleAuthCode()</code>. Эта функция сама генерирует код, сохраняет его и возвращает код и url для генерирования QR кода.</p>
        <p>Также в классе <code>\common\models\UserAvatar</code> есть функция <code>validateGoogleAuthCode($code)</code>. Эта функция сравнивает код который ввео пользователь <code>$code</code> с внутренней функцией и секретным кодом. И выдает логический ответ.</p>
        <p>Сохранение</p>
        <pre>
$ret = $user->saveGoogleAuthCode();
$secret = $ret['secret'];
$url = $ret['url'];</pre>

        <p>Проверка</p>
        <pre>
if (!$user->validateGoogleAuthCode($this->code)) {
    $this->addError($attribute, 'Не верный код');
}</pre>

        <h4 class="page-header">Реализация</h4>
        <p>Для реализации google кодов я сделаю страницу установки кода <code>/cabinet/set-google-code</code>, страницу восстановления кода если человек потерял телефон <code>/auth/repare-google-code</code></p>
        <p>Так же необходимо сделать настройку использования этого кода.</p>
        <p>Где его можно применять?</p>
        <p>В исполнении контрактов это точно.</p>
        <p>При переводе денег. Можно сделать в опциях глобальных - для всех счетов или в настройках счета выборочно для каждого.</p>

        <p><code>/cabinet-google-code/set</code></p>
        <p><code>/auth/repare-google-code</code></p>
        <h4 class="page-header">Алгоритм Взаимодействия с КОДОМ</h4>
        <p>
            <a href="https://drive.google.com/file/d/0B9lrcmdS-YbTMWVHaFNybFAyOW8/view?usp=sharing" target="_blank">
                <img src="/images/controller/development-avatar-bank/auth/google-set.png">
            </a>
        </p>
        <h4 class="page-header">Алгоритм при логине</h4>
        <p>
            <a href="https://drive.google.com/file/d/0BzHYNoEyPNTXUnJEVFY4MURuTVk/view?usp=sharing" target="_blank">
                <img src="/images/controller/development-avatar-bank/auth/Auth.png">
            </a>
        </p>
        <h4 class="page-header">Алгоритм при восстановлении</h4>
        <p>Если GA включен то он будет обязателен при восстановлении</p>
        <p>Хотя можно сделать и опционально потому что при восстановлении пароля происходит защита от другого уровня нападения</p>

        <p>Типы взломов:</p>
        <p>1. При входе Google запоминает ваш логин и если кто то другой сядет за ваш компьютер то он может легко попасть в ваш кабинет</p>
        <p>Так же под эту статью попадают нападения следующего вида:</p>
        <p>- установка программы отслеживания паролей на ваш компьютер и скрытая их передача</p>
        <p>- кто то подглядел ваш пароль визуально вручную или через видеокамеру</p>
        <p>Мы используем Google Authorization по двум причинам:</p>
        <p>- потому что код не передается от сервера к клиенту и значит его невозможно перехватить и он не зависит от средств доставки этого кода.</p>
        <p>- этот сервис севершенно безоплатный, поэтому мы за него не будем платить и цена за сервис останется ниже.</p>
        <p>Google Authorization доступен как в iOS версии так и в Android</p>

        <p>2. Если взломают вашу почту то через нее взломщик может запросить пароль и тогда так же попадет в ваш кабинет. Для этого нужно поставить галочку.</p>
        <p>3. Если хакеры залезут на наш сервер то они могут получить системный пароль от кошелька, но не приватный ключ. Что бы этого не произошло необходимо получить ключ восстановления для того чтобы их зашифровать и тогда профиль будет защищен полностью.</p>

        <h3 class="page-header">Комбинация с ключами восстановления</h3>
        <p></p>

        <h3 class="page-header">Настройка</h3>
        <p>Имя сервера с которым будет формироваться логин для пользователя бертся из настроек</p>
        <p>Файл <code>common/config/params.php</code> параметр <code>Yii::$app->params['google-authorisation-code']['server']</code></p>



        <h3 class="page-header">Защита</h3>
        <h4 class="page-header">Попытка подбора пароля</h4>
        <p>Одна из разновидностей попыток взлома. Заключается в том что происходит подбор пароля к кабинету через робота.</p>
        <p>Для защиты ставим captcha после третьего неправильно введенного пароля.</p>

    </div>
</div>



