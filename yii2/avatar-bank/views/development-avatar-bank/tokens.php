<?php
use common\services\UsersInCache;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;

/* @var $this yii\web\View */

$this->title = 'Токены/Жетоны';

?>
<div class="container">
    <div class="col-lg-12">
        <h1 class="page-header text-center"><?= Html::encode($this->title) ?></h1>
        <p class="alert alert-danger">Разработка</p>
        <p>Допусим что токены хранятся в таблице currency</p>
        <p>Какими свойствами обладает токен?</p>
        <ul>
            <li>адрес контракта</li>
            <li>abi</li>
            <li>Логотип</li>
            <li>Название</li>
            <li>Кошелек на котором хранятся все токены</li>
        </ul>
        <p>Для кошелька какие свойства есть?</p>
        <ul>
            <li>идентификатор токена</li>
            <li>кол-во токенов в кошельке</li>
            <li>кол-во эфира в кошельке</li>
        </ul>
        <p>
            <a href="https://drive.google.com/file/d/0BzHYNoEyPNTXdEIyRlNzcU12X3c/view?usp=sharing" target="_blank">
                <img src="/images/controller/development-avatar-bank/tokens/currency.png" class="thumbnail">
            </a>
        </p>
        <p>Вопросы: где показывать сколько эфира в кошельке?</p>
        <p>Где показывать сколько эфира в кошельке?</p>
        <p>Стоит ли самим оплачивать транзакции токенов?</p>

        <h2 class="page-header">Сущности</h2>
        <p><code>\common\models\Token</code> - токен</p>
        <p><code>\common\models\avatar\Currency</code> - валюта</p>

        <h2 class="page-header">Политика кошельков</h2>
        <p>Как известно на одном адресе можно хранить множество токенов, но люди могут запутаться как это на одном адресе множество монет?
            Но с другой строны можно реализовать хранение каждой монеты на разных кошельках,
            но это не удобно так как адреса будут разные, что не очень хорошо, потому что если еать регистрация паспорта то он привязан к одному адресу,
            то лучше конечно на нем все хранить.</p>
        <p>Если хранить много токенов на одном кошельке, то можно показывать монеты с одним кошельком, дублировать.</p>

        <h2 class="page-header">Строительство класса</h2>
        <p>База: <code>\common\components\providers\Token</code></p>
        <p>При конструировании класса нужно обязательно указать параметры</p>
        <p><code>currency</code> идентификатор валюты</p>
        <p><code>contract</code> адрес контракта</p>
        <p><code>abi</code> интерфейс контракта</p>
        <p>
            <a href="https://drive.google.com/file/d/0BzHYNoEyPNTXQWRiUlBwTWlMdlU/view?usp=sharing" target="_blank">
                <img src="/images/controller/development-avatar-bank/tokens/token.png">
            </a>
        </p>
        <h2 class="page-header">Функции для токена</h2>
        <p>При конструировании класса нужно обязательно указать параметры:</p>
        <ul>
            <li><code>abi</code> - интерфейс контракта</li>
            <li><code>contract</code> - адрес контракта</li>
            <li><code>currency</code> - идентификатор валюты, таблица <code>currency.id</code></li>
        </ul>

        <h2 class="page-header">Административное управление токенами</h2>
        <p>Контроллер: <code>\avatar\controllers\AdminTokensController</code></p>
        <p>Функции:</p>
        <ul>
            <li>Регистрация - регистрация уже существующего в Ethereum сети токена для использования в Аватар Банке</li>
            <li>Добавление - выпуск нового токена с размещением контакта в сети Ethereum</li>
            <li>Удаление - удаление токена из Аватар Банка. Проверка на наличие кошельков у других пользоватлей системы.</li>
        </ul>

        <h2 class="page-header">Запуск в пользование монетой</h2>
        <p>Как новая зарегистрированная монета будет появляться в кабинете клиента?</p>
        <p>Если человек создал монету то у клиента появится кошелек с этой монетой с указанной суммой</p>
        <p>Если человек посылает монету человеку у которого нет такого кошелька, то кошелек у клиента назначения появляется и монуты ему зачисляются.</p>
        <p>Если человек хочет зарегистрировать кошелек, то значит что нужно зарегистрировать кошелек и он появится с 0 на балансе.</p>

        <h2 class="page-header">Счет с монетой</h2>
        <p>В модели <code>\common\models\avatar\UserBill</code> указывается валюта <code>currency</code> и Используется для получения баланса и перевода денег класс <code>\common\components\providers\Token</code>.</p>
        <p>Так создается кошелек и получается доступ к провайдеру сервиса.</p>
        <pre>
$token = \common\models\Token::findOne($this->token_id);
/** @var \common\components\providers\Token $provider */
$provider = $token->getProvider();
$wallet = $provider->create($this->name, $this->password, $billingId, Yii::$app->user->id, $this->password_type);</pre>
        <p>Можно от объекта валюты <code>\common\models\avatar\Currency</code> получить объект токена <code>\common\models\Token</code> следующим образом:</p>
        <pre>
/** @var \common\models\avatar\Currency $currency */
$currency = \common\models\avatar\Currency::findOne(1);
/** @var \common\models\Token $token */
$token = $currency->getToken();</pre>
        <p>В модели <code>\common\models\avatar\UserBill</code> указывается валюта <code>currency</code> и используется для получения баланса и перевода денег класс <code>\common\components\providers\Token</code>.</p>

        <p>Для создания кошелька для хранения монеты должен быть кошелек Ethereum.</p>

        <h2 class="page-header">Создание счета с монетой</h2>
        <p>В счетах нажать "Добавить токен"</p>
        <p>
            <img src="/images/controller/development-avatar-bank/tokens/create1.png" class="thumbnail" width="50%">
        </p>
        <p>Откроется модальное окно</p>
        <p>
            <img src="/images/controller/development-avatar-bank/tokens/create2.png" class="thumbnail" width="50%">
        </p>
        <p>Указать:</p>
        <ul>
            <li>название счета</li>
            <li>Пароль от кабинета</li>
            <li>Выбрать токен</li>
            <li>Выбрать счет ETH с которого будут оплачиваться транзакции счета</li>
        </ul>
        <p>Так будет выглядеть:</p>
        <p>
            <img src="/images/controller/development-avatar-bank/tokens/view.png" class="thumbnail" width="50%">
        </p>
        <p>При создании счета для монеты пароль от кошелька ($passwordWallet) используется тот же что и на ETH</p>

        <h2 class="page-header">Счет с монетой в кабинете</h2>
        <p>Так выглядят токены в списке счетов:</p>
        <p>
            <img src="/images/controller/development-avatar-bank/tokens/cabinet.png" class="thumbnail" width="50%">
        </p>
        <p>Счет открывается по ссылке: <code>/cabinet-bills/transactions-token</code></p>
        <p>На этой странице вызывается два AJAX:</p>
        <ul>
            <li><code>/cabinet-bills/balance-internal-token</code> - получает баланс</li>
            <li><code>/cabinet-bills/transactions-list-token</code> - получает список транзакций счета</li>
        </ul>
        <p>Так будет выглядеть транзакции:</p>
        <p>
            <img src="/images/controller/development-avatar-bank/tokens/cabinet-transactions.png" class="thumbnail" width="50%">
        </p>
        <p>Здесь не показывается кол-во подтверждений и комиссия, так как это ограничено спецификой провайдена, который напрямую в списке не предоставляет таких данных.</p>

        <p>Однако в модельном окне приводится кол-во подтверждений и комиссия за транзакцию.</p>
        <p>В колонке "счет" показывается адрес счета получателя или источника монет.</p>

        <h2 class="page-header">Курс монеты</h2>
        <p>Так как у токена есть мировой курс и чтобы выводить в рублях нужно подтягивать курс монеты.</p>
        <p>Это будет делаться в крон.</p>
        <pre>php yii money-rate/update</pre>
        <p>Если же курс монеты не задан то будет выводиться на списке счетов знак вопроса, а на странице счета - в жетонах своих же.</p>


        <h2 class="page-header">Создание монеты</h2>
        <p>Токен можно создать через провайдер <code>\common\components\providers\ETH</code> и функцию
            <code>registerContract($user, $password, $contract, $contractName, $params = [])</code></p>

        <?= \avatar\services\Params::widget([
            'params' => [
                [
                    'name'        => 'user',
                    'isRequired'  => true,
                    'type'        => 'string',
                    'description' => 'Адрес пользователя кто будет регистрировать контракт',
                ],
                [
                    'name'        => 'password',
                    'isRequired'  => true,
                    'type'        => 'string',
                    'description' => 'пароль пользователя от кошелька',
                ],
                [
                    'name'        => 'contract',
                    'isRequired'  => true,
                    'type'        => 'string',
                    'description' => 'Код контракта',
                ],
                [
                    'name'        => 'contractName',
                    'isRequired'  => true,
                    'type'        => 'string',
                    'description' => 'Название контракта который будет инициализироваться',
                ],
                [
                    'name'        => 'params',
                    'type'        => 'array',
                    'description' => 'Параметры инициализации',
                ],
            ],
        ]) ?>

        <p>Возвращает JSON</p>
        <pre>[
transaction: 's',
abi: '{}'
]</pre>

        <h2 class="page-header">Создание проекта</h2>
        <p>Проект создается на странице <code>/cabinet-projects/index</code></p>
        <p>Алгоритм:</p>
        <ul>
            <li>Описать проект</li>
            <li>Описать себя</li>
            <li>Выпустить монету</li>
            <li>Выдать документ о выпуске токена</li>
            <li>Вбить все кошельки</li>
            <li>Вбить команду</li>
            <li>Вбить подарки</li>
        </ul>

        <h2 class="page-header">Выпуск токена</h2>
        <p>Токен создается на странице <code>/cabinet-token/new</code></p>
        <p>
            <img src="/images/controller/development-avatar-bank/tokens/create3.png" class="thumbnail">
        </p>
        <p>Алгоритм:</p>
        <ul>
            <li><code>/cabinet-token/new-ajax</code>, возвращает <code>transactionHash</code>, <code>abi</code></li>
            <li><code>/cabinet-token/new-check-ajax</code> тестирует на предмет подтверждения транзакции. Возвращает <code>[status,address]</code> где <code>address</code> - это адрес токена</li>
            <li><code>/cabinet-token/registration</code> Регистрирует токен в системе, создает счет и добавляет валюту. Возвращает <code>{"wallet":{"address":"0x457ea369b3155c13f9089299bf74bb6265e1cd85"}}</code>  адрес кошелька созданного под хранение токена</li>
        </ul>


    </div>
</div>



