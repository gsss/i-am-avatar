<?php
use common\services\UsersInCache;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;

/* @var $this yii\web\View */

$this->title = 'Кооперативы';

?>
<div class="container">
    <div class="col-lg-12">
        <h1 class="page-header text-center"><?= Html::encode($this->title) ?></h1>

        <h2 class="page-header">Схема</h2>

        <p>
            <a href="https://drive.google.com/file/d/0BzHYNoEyPNTXQjU1N0VqbDI4LXM/view?usp=sharing" target="_blank">
                <img src="/images/controller/development-avatar-bank/koop/k.png"/>
            </a>
        </p>
        <p>
            <a href="https://drive.google.com/file/d/0BzHYNoEyPNTXVXJTa1ZZdldSelU/view?usp=sharing" target="_blank">
                <img src="/images/controller/development-avatar-bank/koop/koop.png"/>
            </a>
        </p>
        <h3 class="page-header">Описание</h3>
        <p>
            Кооператив – это форма сотрудничества, когда несколько компаний хотят объединиться в единую структуру со своей внутренней валютой, которая будет обеспечивать взаимозачет движения товаров и услуг, которые были введены в кооператив.
        </p>
        <p>
            Кооператив может  содержать несколько компаний, компания регистрируется человеком хозяином.
        </p>
        <p>
            Хозяин компании предоставляет совету свои товары и услуги,
            кооператив оценивает стоимость внесенных активов
            делает эмиссию условных единиц для оплаты и переводит их новой компании.
        </p>
        <p>
            Покупатель как участник кооператива может купить товар как участник экосистемы, кооператива или потребительского общества.
        </p>
        <p>
            Чтобы продать товар от имени компании на компанию заводятся кассиры. Кассиров можно сколько угодно создать. Деньги (условные единицы) переводятся на счет кооператива (производится возврат пая). Таким образом производится взаимозачет движения товаров и услуг внутри кооператива.
        </p>
        <p>
            Система учитывает наличие товара, его продажу, учет средств на кошельках участников кооператива.
        </p>
        <p>
            Если товар был испорчен, то и часть связанных с этим товаром условных единиц уничтожается.
        </p>
        <p>
            Хозяин условных единиц может расплачиваться внутри экосистемы или передать их третьим лицам как ликвидный актив.
        </p>
        <h3 class="page-header">Решение</h3>
        <p>
            Организовывается в виде умного контракта, который производит эмиссию токенов и перевод их внутри системы. Есть функция уничтожения токенов в следствии порчи товара.
        </p>
        <p>
            Для кассира будет разработано приложение AватарКасса.
        </p>




        <p>Список сущностей:</p>

        <h2 class="page-header">Кооперативы</h2>

        <?= \common\services\documentation\DbTable::widget([
            'name'   => 'koop',
            'params' => [
                [
                    'name'        => 'id',
                    'isRequired'  => true,
                    'description' => 'Идентификатор',
                    'type'        => 'int',
                ],
                [
                    'name'        => 'title',
                    'isRequired'  => true,
                    'description' => 'Наименование',
                    'type'        => 'varchar(255)',
                ],
                [
                    'name'        => 'user_id',
                    'isRequired'  => true,
                    'description' => 'Регистратор кооператива',
                    'type'        => 'int',
                ],
                [
                    'name'        => 'currency_id',
                    'isRequired'  => true,
                    'description' => 'Идентификатор валюты который используется в кооперативе',
                    'type'        => 'int',
                ],
            ],
        ]) ?>
        <p>Кто регистрирует компанию? Пользователь банка</p>
        <p>Где регистрирует компанию? в кабинете сверху в меню. Кооперативы</p>


        <h2 class="page-header">Компании</h2>

        <?= \common\services\documentation\DbTable::widget([
            'name'   => 'koop_company',
            'params' => [
                [
                    'name'        => 'id',
                    'isRequired'  => true,
                    'description' => 'Идентификатор',
                    'type'        => 'int',
                ],
                [
                    'name'        => 'title',
                    'isRequired'  => true,
                    'description' => 'Наименование',
                    'type'        => 'varchar(255)',
                ],
                [
                    'name'        => 'user_id',
                    'isRequired'  => true,
                    'description' => 'Регистратор компании',
                    'type'        => 'int',
                ],
            ],
        ]) ?>
        <p>Кто регистрирует компанию? Пользователь банка</p>
        <p>Где регистрирует компанию? в кабинете сверху в меню. Компании</p>


        <h2 class="page-header">Ссылки Кооператив-Компания</h2>

        <?= \common\services\documentation\DbTable::widget([
            'name'   => 'koop_company_link',
            'params' => [
                [
                    'name'        => 'id',
                    'isRequired'  => true,
                    'description' => 'Идентификатор',
                    'type'        => 'int',
                ],
                [
                    'name'        => 'koop_id',
                    'isRequired'  => true,
                    'description' => 'Идентификатор кооператива',
                    'type'        => 'int',
                ],
                [
                    'name'        => 'company_id',
                    'isRequired'  => true,
                    'description' => 'Идентификатор компании',
                    'type'        => 'int',
                ],
            ],
        ]) ?>
        <p>Кто подключает компанию к кооперативу? Автор кооператива</p>
        <p>Процедура: автор компании подает заявку на добавление автору коопреатива, автор кооператива утверждает.</p>

        <h2 class="page-header">Спиок товаров</h2>

        <?= \common\services\documentation\DbTable::widget([
            'name'   => 'koop_products',
            'params' => [
                [
                    'name'        => 'id',
                    'isRequired'  => true,
                    'description' => 'Идентификатор',
                    'type'        => 'int',
                ],
                [
                    'name'        => 'company_id',
                    'isRequired'  => true,
                    'description' => 'Идентификатор пользователя',
                    'type'        => 'int',
                ],
                [
                    'name'        => 'code',
                    'isRequired'  => true,
                    'description' => 'Идентификатор товара по штрих коду',
                    'type'        => 'varchar(20)',
                ],
                [
                    'name'        => 'title',
                    'isRequired'  => true,
                    'description' => 'Наименование',
                    'type'        => 'varchar(255)',
                ],
                [
                    'name'        => 'price',
                    'isRequired'  => true,
                    'description' => 'Цена товара',
                    'type'        => 'double',
                ],
                [
                    'name'        => 'currency',
                    'isRequired'  => true,
                    'description' => 'Валюта товара',
                    'type'        => 'int',
                ],
                [
                    'name'        => 'count',
                    'isRequired'  => true,
                    'description' => 'Кол-во товара в резерве',
                    'type'        => 'int',
                ],
                [
                    'name'        => 'created_at',
                    'isRequired'  => true,
                    'description' => 'Момент заведения товара',
                    'type'        => 'int',
                ],
                [
                    'name'        => 'koop_id',
                    'description' => 'идентификатор кооператива на балансе которого числится данный товар',
                    'type'        => 'int',
                ],
                [
                    'name'        => 'image',
                    'description' => 'Картинка товара',
                    'type'        => 'varchar(200)',
                ],
            ],
        ]) ?>
        <p>Кто добавляет продукты? Автор компании или кассир.</p>

        <h2 class="page-header">Кассиры</h2>

        <?= \common\services\documentation\DbTable::widget([
            'name'   => 'koop_kassir',
            'params' => [
                [
                    'name'        => 'id',
                    'isRequired'  => true,
                    'description' => 'Идентификатор',
                    'type'        => 'int',
                ],
                [
                    'name'        => 'user_id',
                    'isRequired'  => true,
                    'description' => 'Идентификатор пользователя кассира user.id',
                    'type'        => 'varchar(255)',
                ],
                [
                    'name'        => 'company_id',
                    'isRequired'  => true,
                    'description' => 'Идентификатор компании koop_company.id',
                    'type'        => 'varchar(255)',
                ],
            ],
        ]) ?>
        <p>Кто добавляет кассиров? Автор компании.</p>

        <h2 class="page-header">Покупки-чек</h2>

        <?= \common\services\documentation\DbTable::widget([
            'name'   => 'koop_buy',
            'params' => [
                [
                    'name'        => 'id',
                    'isRequired'  => true,
                    'description' => 'Идентификатор',
                    'type'        => 'int',
                ],
                [
                    'name'        => 'price',
                    'isRequired'  => true,
                    'description' => 'Цена товара',
                    'type'        => 'double',
                ],
                [
                    'name'        => 'kassir_id',
                    'isRequired'  => true,
                    'description' => 'Идентификатор кассира',
                    'type'        => 'int',
                ],
                [
                    'name'        => 'company_id',
                    'isRequired'  => true,
                    'description' => 'Идентификатор компании',
                    'type'        => 'int',
                ],
                [
                    'name'        => 'currency',
                    'isRequired'  => true,
                    'description' => 'Валюта товара',
                    'type'        => 'int',
                ],
                [
                    'name'        => 'created_at',
                    'isRequired'  => true,
                    'description' => 'Момент заведения товара',
                    'type'        => 'int',
                ],
            ],
        ]) ?>

        <h2 class="page-header">Документы по сделке</h2>

        <?= \common\services\documentation\DbTable::widget([
            'name'   => 'koop_buy_documents',
            'params' => [
                [
                    'name'        => 'id',
                    'isRequired'  => true,
                    'description' => 'Идентификатор',
                    'type'        => 'int',
                ],
                [
                    'name'        => 'document',
                    'isRequired'  => true,
                    'description' => 'ссылка на документ',
                    'type'        => 'varchar(255)',
                ],
                [
                    'name'        => 'buy_id',
                    'isRequired'  => true,
                    'description' => 'Идентификатор чека',
                    'type'        => 'int',
                ],
                [
                    'name'        => 'type',
                    'isRequired'  => true,
                    'description' => 'Тип документа',
                    'type'        => 'int',
                ],
            ],
        ]) ?>

        <h2 class="page-header">Элемент списка чека</h2>
        <?= \common\services\documentation\DbTable::widget([
            'name'   => 'koop_buy_list',
            'params' => [
                [
                    'name'        => 'id',
                    'isRequired'  => true,
                    'description' => 'Идентификатор элемента чека',
                    'type'        => 'int',
                ],
                [
                    'name'        => 'product_id',
                    'isRequired'  => true,
                    'description' => 'Идентификатор продукта',
                    'type'        => 'int',
                ],
                [
                    'name'        => 'count',
                    'isRequired'  => true,
                    'description' => 'кол-во товаров',
                    'type'        => 'double',
                ],
            ],
        ]) ?>
        <p>Так как товары могут быть весовые то и параметр <code>count</code> не целый.</p>

        <h2 class="page-header">Заявка на внесение пая</h2>
        <p>Заявка на внесение пая вносится компанией в кооператив <code>koop_id</code> продуктом <code>product_id</code>.
            Так как у продукта есть идентификатор компании то он здесь не числится</p>
        <?= \common\services\documentation\DbTable::widget([
            'name'   => 'koop_request_pay_in',
            'params' => [
                [
                    'name'        => 'id',
                    'isRequired'  => true,
                    'description' => 'Идентификатор элемента чека',
                    'type'        => 'int',
                ],
                [
                    'name'        => 'product_id',
                    'isRequired'  => true,
                    'description' => 'Идентификатор продукта',
                    'type'        => 'int',
                ],
                [
                    'name'        => 'koop_id',
                    'isRequired'  => true,
                    'description' => 'Идентификатор кооператива куда вносится товар',
                    'type'        => 'int',
                ],
                [
                    'name'        => 'created_at',
                    'isRequired'  => true,
                    'description' => 'Когда была создана заявка',
                    'type'        => 'int',
                ],
            ],
        ]) ?>
        <p>Модель данных: <code>\common\models\koop\RequestPayIn</code></p>

        <h2 class="page-header">Связи</h2>
        <p>Может ли одна компания принадлежать двум кооперативам? да</p>
        <p>Может ли один кассир принадлежать двум команиям? да</p>
        <p>Могут ли компании с разными валютами вступать или только с одной? нет, только одна</p>
        <p>Нужно ли организовывать учет хранения товара? да</p>
        <p>Нужно в столе заказов организовывать доставку или как быть с доставкой? да</p>
        <p>Нужно делать переводы документов? да</p>
        <p>Как подписывать документы? цифровой подписью</p>


        <h2 class="page-header">События</h2>

        <h3 class="page-header">Создание Кооператива</h3>

        <p><code>/cabinet-koop/add</code></p>
        <p>Окружение: Есть N учредителей, они хотят зарегистрировать кооператив</p>

        <ul>
            <li>Один создает кооператив</li>
            <li>Рассылает ссылку всем учредителям</li>
            <li>Каждый учредитель активирует ссылку и по ходу активируется</li>
        </ul>

        <h3 class="page-header">Создание Компании</h3>

        <p><code>/cabinet-koop-company/add</code></p>

        <h3 class="page-header">Подключение компании в Кооператив</h3>

        <p>Окружение: Есть Кооператив и компания которая хочет зарегистрироваться в кооперативе</p>

        <ul>
            <li>Регистрируется представитель компании</li>
            <li>Представитель компании регистрирует компанию</li>
            <li>Учредитель кооператива регистрирует компанию</li>
            <li>После он может обратиться с заявкой на внесение пая</li>
        </ul>
        <h4 class="page-header">Реализация подключения</h4>
        <p>Можно сделать двумы типами</p>
        <p>1. выбор через чекбоксы</p>
        <p>2. добавление через заявки</p>
        <p>1. выбор через чекбоксы - человек нажимает на кнопку "Компании" в кооперативах, переходит к списку всех компаний которые есть, ставит галочку и так обновляет список компаний состоящих в кооперативе.</p>
        <p>Вывод не актуально, нельзя предусмотреть операцию добавления компаниии и выписка документов на кадую. Поэтому нужно добавлять по одной</p>
        <p>2. добавление через заявки - лучше всего когда нажимает на кнопку "Добавить Компанию" и открывается окно где список свободных компаний.</p>
        <p><code>/cabinet-koop/company-add</code></p>

        <h3 class="page-header">Внесение пая</h3>

        <p>Окружение: Есть Кооператив и компания, которая хочет внести пай в кооператив</p>

        <ul>
            <li>Подается заявка на внесение пая</li>
            <li>Проводится оценка</li>
            <li>Подписывается договор внесение пая с эмиссией токенов на хозяина пая</li>
        </ul>
        <p>При внесении пая нужно указать компанию, которая вносит пай и список товаров которые будут вноситься.</p>
        <p>Можно сделать следующим образом: Добавлять для компании товары на баланс компании, а потом перевести их на баланс кооператива. И вот этот перевод на баланс кооператива это и будет внесением пая.</p>
        <p>Тогда нужен флаг какие товары на балансе компаниии а какие на балансе кооператива. Предлогаю флаг <code>is_balance_koop</code> 0 - на балансе компании, 1 - на балансе кооператива</p>
        <hr>
        <p>Рассмотрю какие варианты есть</p>
        <p>1. Я добавляю товар в компанию и потом создаю заявку на внесение пая данного товара в кооператив.</p>
        <p>2. Я создаю создаю заявку на внесение пая как товара в кооператив.</p>
        <p>Выбираю первый вариант</p>
        <p><img src="/images/controller/development-avatar-bank/koop/pay-in.png" class="thumbnail"></p>
        <p>После внесения пая создается заявка, поэтому нужно помечать как то товар чтобы на него нельзя было создать повторную заявку</p>
        <p>Делаю поле <code>is_request</code> в сущность "товар"</p>



        <h3 class="page-header">Покупка (Возврат пая)</h3>


        <p>Окружение: кассир вошел в систему, он принадлежит компании, на которую будет оформляться чек</p>

        <ul>
            <li>Подготовка чека</li>
            <li>Сканирование товара</li>
            <li>Кнопка завершить</li>
            <li>Сделать списание денег</li>
            <li>Сделать списание в 1C</li>
            <li>Сделать списание товара</li>
            <li>Подготовить чек</li>
            <li>Подготовить документы</li>
            <li>Печатается чек</li>
        </ul>


        <p>А нужны ли компании? или достаточно человека который владеет компанией?</p>

        <h2 class="page-header">Стол заказов</h2>
        <p>Страница <code>/cabinet-koop/shop?id=1</code> показываются все ресурсы которые есть в налчии у кооператива
            с возможностью купить за валюту кооператива.</p>
        <p>Сразу встает вопрос а где хранится код валюты кооператива? Очевидно в сущности кооператива. А может один кооператив использовать две валюты? Нет. Значит в сущности кооператива.</p>
        <p><code>koop.currency_id</code> Идентификатор валюты который используется в кооперативе</p>
        <h3 class="page-header">Продукты кооператива</h3>
        <p>Чтобы понять какие продукты есть у кооператива нужно понять какие были внесены на баланс кооператива. Сейчас можно определить только принадлежность компании, а на какой кооператив они были внесены не понятно. Поэтоум нужно понять что делать. Скорее всего нужно создать поле принадлежности кооперативу. Но сразу встает вопрос "А если одна компания отдала один продукт в один кооп а еще того же типа продукта в другой, а нам же все надо учитывать их как два, но продукт один". Ответ сделать внесение ресурса, где указывается что за ресурс компании и в каком количестве.</p>
        <p><a href="https://drive.google.com/file/d/0B73bzLewJ2HMekZpSk85S2xEeTQ/view?usp=sharing" target="_blank"><img src="/images/controller/development-avatar-bank/koop/coop.png"></a></p>
        <p>Введу для сущности товар <code>koop_products</code> поле принадлежности <code>koop_id</code> который обозначает идентификатор на балансе какого кооператива числится данный товар.</p>



    </div>
</div>



