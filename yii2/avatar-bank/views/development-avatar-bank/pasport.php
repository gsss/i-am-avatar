<?php
use common\services\UsersInCache;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;

/* @var $this yii\web\View */

$this->title = 'Паспорт';

?>
<div class="container">
    <div class="col-lg-12">
        <h1 class="page-header text-center"><?= Html::encode($this->title) ?></h1>

        <p>
            <a href="https://drive.google.com/file/d/0BzHYNoEyPNTXUUc3T2RCZHF6NGM/view?usp=sharing" target="_blank">
                <img src="/images/controller/development-avatar-bank/passport/pass.png" class="thumbnail">
            </a>
        </p>
        <p>Здесь есть две функции:</p>
        <ol>
            <li>Установка кошелька от какого адреса подписывать</li>
            <li>Регистрация в контракте</li>
        </ol>

        <h2 class="page-header">Установка кошелька от какого адреса подписывать</h2>
        <p><code>/cabinet-passport/index</code></p>
        <p>Здесь человек указывает от какого адреса будут подписываться все контракты</p>
        <p>Таблица в которой хранится указатель является: <code>passport_link</code>. Модель: <code>\common\models\PassportLink</code></p>
        <?= \common\services\documentation\DbTable::widget([
            'name'    => 'passport_link',
            'columns' => [
                [
                    'name'        => 'id',
                    'isRequired'  => true,
                    'type'        => 'int',
                    'description' => 'Идентификатор записи',
                ],
                [
                    'name'        => 'user_id',
                    'isRequired'  => true,
                    'type'        => 'int',
                    'description' => 'Идентификатор пользователя',
                ],
                [
                    'name'        => 'billing_id',
                    'isRequired'  => true,
                    'type'        => 'int',
                    'description' => 'Идентификатор счета '. Html::tag('code', 'user_bill'),
                ],
                [
                    'name'        => 'created_at',
                    'type'        => 'int',
                    'description' => 'Время записи в таблицу',
                ],
            ]
        ]) ?>

        <p>
            <img src="/images/controller/development-avatar-bank/passport/p.png" class="thumbnail"/>
        </p>

        <h2 class="page-header">Регистрация в контракте</h2>
        <p class="alert alert-danger">В разработке</p>
        <p><code>/cabinet-passport/contract</code></p>
        <p>Контракт: </p>
        <p>Здесь человек указывает свои данные для хранения в блокчейне</p>

    </div>
</div>



