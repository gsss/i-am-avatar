<?php
use common\services\UsersInCache;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;

/* @var $this yii\web\View */

$this->title = 'Страница счетов';

?>
<div class="container">
    <div class="col-lg-12">
        <h1 class="page-header text-center"><?= Html::encode($this->title) ?></h1>
        <p>Кнопка "отправить"</p>
        <p>В кнопке должно быть attribute <code>data-id</code> - идентификатор счета, <code>data-currency</code> - идентификатор валюты</p>

        <p>класс buttonSendBtc - для счетов BTC</p>
        <p>класс buttonSendEth - для счетов ETH</p>
        <p>класс buttonSendEthToken - для счетов ETH Token</p>
        <p>класс buttonSendEtc - для счетов ETC</p>
        <p><img src="/images/controller/development-avatar-bank/page-cabinet-bills-index/2018-01-05_22-29-58.png" class="thumbnail"></p>

        <p>Я хочу сделать круг с диаграмой моего фонда. Во первых я разделяю два списка счетов (это будет список в валюте которая включена для просмотра). Первый это счета которые имеют руночную цену. Второй это те которые не имеют (это будет список с у.е.).</p>
        <p>Так как сразу значений нет то придется собирать значения от запросов о балансе. Собираю для первого списка в рублях. для второго в у.е.</p>
        <p>Еще условие, что добавляются только те которые баланс больше нуля.</p>
        <p><code>pie1</code> переменная JS для первого списка.</p>
        <pre>pie1.push({
    currency: o,
    value: result.data.balance2 * o.price_rub
});</pre>

    </div>
</div>



