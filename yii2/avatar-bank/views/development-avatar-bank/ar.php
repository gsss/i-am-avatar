<?php
use common\services\UsersInCache;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;

/* @var $this yii\web\View */

$this->title = 'Интерфейс дополненной реальности в блокчейне';



?>
<div class="container">
    <div class="col-lg-12">
        <h1 class="page-header text-center"><?= Html::encode($this->title) ?></h1>

        <p>
            <a href="https://drive.google.com/file/d/0BzHYNoEyPNTXRGdwTE1na3N1Ym8/view?usp=sharing" target="_blank">
                <img src="/images/controller/development-avatar-bank/ar/AR.png">
            </a>
        </p>
        <p>
            В очки дополненной реальности встроена видеокамера. Видеокамера соединена с программой распознавания лиц. Программа распознавания лиц выдает уникальный идентификатор человека. Через этот идентификатор посылается запрос в блокчейн контракт AvatarPassport, который выдает уже данные о человеке, его рейтинг и другие данные которые человек открыл для вас или всех. Эти данные выводятся на экран дополненной реальности синхронизируясь с лицом человека и его движением по экрану.
        </p>
        <p>
            <img src="/images/controller/development-avatar-bank/ar/00011.jpg" width="100%">
        </p>

        <h3 class="page-header">Личный Кабинет Аватара</h3>

        <p>Может быть доступен в двух вариантах</p>

        <ul>
            <li>Через мобильный телефон или планшет</li>
            <li>Через систему проекций в воздухе и управление перчаткой или прикрепленной к пальцу датчику</li>
        </ul>

        <p>
            <img src="/images/controller/development-avatar-bank/ar/photo_2017-09-25_20-40-18.jpg">
        </p>

        <h3 class="page-header">Дополненная реальность для получения информации в городе</h3>

        <p>В дополненной картинке будут отображаться </p>

        <ul>
            <li>обмениики на криптовалюты с курсами валют</li>
            <li>места где принимают криптовалюту</li>
        </ul>


    </div>
</div>



