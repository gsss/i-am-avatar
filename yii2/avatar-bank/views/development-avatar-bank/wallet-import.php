<?php
use common\services\UsersInCache;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;

/* @var $this yii\web\View */

$this->title = 'Кошельки. Импортирование';

?>
<div class="container">
    <div class="col-lg-12">
        <h1 class="page-header text-center">
            <?= Html::encode($this->title) ?>
        </h1>

        <p>Импортировать кошелек можно через API</p>
        <p>Импортировать кошелек можно двумя способами:</p>
        <ul>
            <li>Если у вас есть JSON файл и вы знаете к нему пароль</li>
            <li>Если у вас есть приватный ключ</li>
        </ul>

        <h2 class="page-header">Реализация</h2>
        <p>Если вы хотите импортировать ключ то на странице счетов нужно нажать на кнопку импортировать кошелек</p>
        <p>Импортирование через JSON открывается на странице <code>/cabinet-bills/import-json</code></p>
        <p>Импортирование через приватный ключ открывается на странице <code>/cabinet-bills/import-private-key</code></p>


    </div>
</div>



