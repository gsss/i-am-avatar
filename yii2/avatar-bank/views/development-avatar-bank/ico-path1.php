<?php
use common\services\UsersInCache;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;

/* @var $this yii\web\View */

$this->title = 'Страница проекта. Путь 2';

?>
<div class="container">
    <div class="col-lg-12">
        <h1 class="page-header text-center"><?= Html::encode($this->title) ?></h1>

        <p>
            <a href="https://drive.google.com/file/d/1ht-xmxZFVO6ZJrSYBy3tIl6iL1HtsKlg/view?usp=sharing" target="_blank">
                <img src="/images/controller/development-avatar-bank/ico-path1/path11.png">
            </a>
        </p>

        <p><b>Маршрут 1</b></p>
        <p>create-wallet</p>
        <hr>


        <p>Для Биткойна - 1 платежная система, QR код, адрес и карта, следующая страница</p>
        <p>Для Эфира - 1 платежная система, QR код, адрес, следующая страница</p>
        <p>Для Рублей - Альфа, Сбер, Yandex, Qiwi, PayPal, VisaMS, WebMoney: Модальное окно, следующая страница</p>
        <p>Если для валюты есть только одна платежная система, то будет редирект /ico/payment на след страницу иначе
            будет выведено модальное окно.</p>


        <p><code>/ico/index</code></p>
        <p>GET параметр</p>
        <p><code>currency</code> идентификатор валюты выбранной</p>
        <p>Ищется такая валюта, если она есть то ищется есть ли для нее настройка не найдена то выдается ошибка</p>
        <p>По клику вызывается AJAX: <code>/ico/create-request</code> в котором создается <code>\common\models\BillingMain</code>,
            <code>\common\models\MerchantRequest</code></p>

        <p><code>/ico/create-request</code></p>
        <p><code>id</code> - идентификатор платежной системы</p>
        <p><code>/ico/paysystems</code> id - идентификатор платежной системы</p>
        <p><code>/ico/payment</code></p>
        <p><code>id</code> - идентификатор заявки</p>


        <p>Типы кошельков хранится в таблице <code>currency</code>.</p>

        <p>Заявки на покупки токенов будет хранится в меню клиента, пункт <code>Мои покупки токенов</code>.</p>


    </div>
</div>



