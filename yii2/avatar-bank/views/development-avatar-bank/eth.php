<?php
use common\services\UsersInCache;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;

/* @var $this yii\web\View */

$this->title = 'ETH';

?>
<div class="container">
    <div class="col-lg-12">
        <h1 class="page-header text-center"><?= Html::encode($this->title) ?></h1>
        <p class="alert alert-danger">В разработке</p>
        <p>В отличие от биткойн кошельков есть несколько отличий:</p>
        <p>Нельзя использовать множественные отправки, только с одного адреса на другой</p>
        <p>Один кошелек это один адрес</p>
        <p>Пароль не сохраняется</p>

        <h2 class="page-header">Создание</h2>
        <p>Поэтому поле <code>user_bill.identity</code> не заполняется</p>
        <p>Строка <code>\common\models\avatar\UserBillAddress</code> заполняется одним адресом</p>


    </div>
</div>



