<?php
use common\services\UsersInCache;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;

/* @var $this yii\web\View */

$this->title = 'Токены. Выпуск';

?>
<div class="container">
    <div class="col-lg-12">
        <h1 class="page-header text-center"><?= Html::encode($this->title) ?></h1>

        <p>Чтобы выпустить токен нужно оплатить его выпуск в кол-ве 1 AVR.</p>
        <p>Поддерживается формат ERC-20 и ERC-223</p>
        <p><a href="https://github.com/ethereum/EIPs/issues/223">https://github.com/ethereum/EIPs/issues/223</a></p>
        <p><a href="https://github.com/Dexaran/ERC223-token-standard/tree/master/token/ERC223">https://github.com/Dexaran/ERC223-token-standard/tree/master/token/ERC223</a></p>
        <p>Можно выпустить довыпускаемый токен.</p>
        <p>Можно выпустить токен с возможностью уничтожения токенов.</p>

        <h3 class="page-header">Мастер выпуска токенов</h3>
        <p>Задача: Нужно</p>
        <ul>
            <li>выпустить токен</li>
            <li>оплатить его выпуск</li>
            <li>зарегистрировать контракт токена</li>
            <li>дождаться подтверждения регистрации</li>
            <li>получить адрес контракта</li>
            <li>добавить токен в нашу систему</li>
            <li>Добавить кошелек с токеном</li>
        </ul>
        <p>Для реализации данной задачи я предлагаю ввести систему заяввок на создание токена и в ней сохранять сопутствующие параметры</p>
        <p>Заявка и ее свойства: название таблицы <code>requests_token_create</code>, поля:</p>
        <?= \common\services\documentation\DbTable::widget([
            'name' => 'requests_token_create',
            'columns' => [
                [
                    'name'        => 'id',
                    'isRequired'  => true,
                    'type'        => 'int',
                    'description' => 'Идентификатор',
                ],
                [
                    'name'        => 'is_paid_avatars',
                    'isRequired'  => true,
                    'type'        => 'int',
                    'description' => 'Оплачены аватары?  0 - нет, 1 - да',
                ],
                [
                    'name'        => 'txid',
                    'isRequired'  => true,
                    'type'        => 'varchar(80)',
                    'description' => 'Идентификатор транзакции',
                ],
                [
                    'name'        => 'abi',
                    'type'        => 'text',
                    'description' => 'Интерфейс контракта',
                ],
                [
                    'name'        => 'address',
                    'type'        => 'varchar(50)',
                    'description' => 'Адрес контракта',
                ],
                [
                    'name'        => 'init',
                    'isRequired'  => true,
                    'type'        => 'varchar(1000)',
                    'description' => 'Параметры инициализации одной строкой сконвертированные уже для верификации контракта на etherscan.io',
                ],
                [
                    'name'        => 'contract',
                    'isRequired'  => true,
                    'type'        => 'text',
                    'description' => 'Код контракта',
                ],
                [
                    'name'        => 'create_at',
                    'isRequired'  => true,
                    'type'        => 'int',
                    'description' => 'Время добавления заявки',
                ],
                [
                    'name'        => 'owner',
                    'isRequired'  => true,
                    'type'        => 'varchar(50)',
                    'description' => 'Адрес кошелька хозяина',
                ],
                [
                    'name'        => 'name',
                    'isRequired'  => true,
                    'type'        => 'varchar(50)',
                    'description' => 'Название токена',
                ],
                [
                    'name'        => 'code',
                    'isRequired'  => true,
                    'type'        => 'varchar(10)',
                    'description' => 'Код валюты',
                ],
                [
                    'name'        => 'decimals',
                    'isRequired'  => true,
                    'type'        => 'int',
                    'description' => 'Кол-во знаков после запятой',
                ],
                [
                    'name'        => 'coin_emission',
                    'isRequired'  => true,
                    'type'        => 'int',
                    'description' => 'Кол-во эмитируемых токенов',
                ],
                [
                    'name'        => 'image',
                    'isRequired'  => true,
                    'type'        => 'varchar(200)',
                    'description' => 'Картинка токена',
                ],
                [
                    'name'        => 'is_register_in_system',
                    'isRequired'  => true,
                    'type'        => 'tinyint',
                    'description' => 'Зарегистрирован в системе? 0 - нет, 1 - да',
                ],
                [
                    'name'        => 'step',
                    'isRequired'  => true,
                    'type'        => 'tinyint',
                    'description' => 'шаг заявки до которого дошел клиент',
                ],
                [
                    'name'        => 'user_id',
                    'isRequired'  => true,
                    'type'        => 'int',
                    'description' => 'автор заявки',
                ],
            ],
        ]) ?>

        <p>Шаги мастера</p>
        <ul>
            <li>Ввод данных контракта</li>
            <li>Оплата</li>
            <li>Регистрация контракта в блокчейне</li>
            <li>Ожидание подтверждения блока</li>
            <li>Регистрация токена в системе и кошелька</li>
        </ul>

        <h3 class="page-header">Реализация</h3>
        <p>1 шаг это регистрация заявки</p>
        <p>2 шаг это отправка токенов аватаров</p>
        <p>3 шаг это регистрация контракта</p>
        <p>4 шаг это подтверждение блока</p>
        <p>5 шаг это регистрация токена в системе и кошелька токена</p>

        <h4 class="page-header">1. регистрация заявки</h4>
        <p>Здесь можно вбить все данные токена: Название, Код валюты, Кол-во эмиссии, Кол-во десятичных, Картинку токена</p>
        <p>Вызывается <code>/cabinet-token/new-ajax</code></p>

        <h4 class="page-header">2. отправка токенов аватаров</h4>
        <p>Здесь клиенту показываются кошельки аватара с которых он может оплатить и введение пароля</p>
        <p>Страница: <code>/cabinet-token/step2</code></p>
        <p>Вызывается: <code>/cabinet-token/step2-ajax</code></p>
        <p>
            <a href="https://drive.google.com/file/d/0BzHYNoEyPNTXZlZmTV9CN3RzT0E/view?usp=sharing" target="_blank">
                <img src="/images/controller/development-avatar-bank/tokens-new/ana.png">
            </a>
        </p>
        <p>Оплата переводится на кошелек который указан в <code>Yii::$app->params['createTokenWalletPayment']</code></p>
        <p>Пароль до следующего шага хранится в переменной кеша <code>['session_id' => Yii::$app->session->id, 'name' => 'createToken']</code></p>
        <p>На этой стадии может быть что у клиента</p>
        <ul>
            <li>Нет кошелька АватарМонет - купить их по ссылке</li>
            <li>Нет кошелька Аватара - автоматически создается и перекидывается на купить их по ссылке</li>
            <li>Нет кошелька эфира - информация что делать</li>
        </ul>


        <h4 class="page-header">3. расчет стоимости регистрация контракта</h4>
        <p>Этот этап наступает когда зарегистрируется транзакция отправки токена</p>
        <p>Если клинет не попал сюда с предыдущего шага то он должен будет ввести пароль для расчета стоимости</p>
        <p>Здесь клиент наблюдает как расчитывается стоимость регистрации контракта, после показывается цена и кнопка согласия и продолжения</p>
        <p>Вызывается <code>/cabinet-token/registration-calculate-ajax</code></p>

        <h4 class="page-header">3. регистрация контракта</h4>
        <p>Этот этап наступает когда зарегистрируется транзакция отправки токена</p>
        <p>Здесь клиент наблюдает как регистрируется контракт</p>
        <p>Если клинет не попал сюда с предыдущего шага то он должен будет ввести пароль для регистрации контракта</p>
        <p>Вызывается <code>/cabinet-token/registration-ajax</code></p>

        <h4 class="page-header">4. подтверждение блока</h4>
        <p>Этот этап наступает когда зарегистрируется транзакция регистрации контракта</p>
        <p>Здесь клиент наблюдает как подтверждается транзакция. Показывается счетчик сколько времени прошло после регистрации контракта</p>
        <p>Раз в 10 сек отправляется функция проверки <code>/cabinet-token/test-tx</code></p>

        <h4 class="page-header">5. регистрация токена в системе и кошелька токена</h4>
        <p>Этот этап наступает когда подтверждится транзакция регистрации контракта</p>
        <p>Здесь через AJAX регистрируется токен и кошелек токена и можно скачать выписку по контракту а также вызвать функции токена и изменить картинку токена</p>
        <p>Раз в 10 сек отправляется функция проверки <code>/cabinet-token/registration2</code></p>


        <h2 class="page-header">Готовые токены</h2>
        <p>После выпуска создается запись в таблице user_token в которую записывается кто какие токены выпустил. И там же можно будет управлять своим токеном: загружать картинку, вызывать функции и прочее.</p>
        <p>Запись в таблицу производится после получения адреса токена.</p>
        <p>В интерфейсе располагается список в профиле <code>/cabinet-token-list/index</code>.</p>
        <p>Модель: <code>\common\models\UserToken</code>.</p>
        <p>Контроллер: <code>\common\models\UserToken</code>.</p>

        <?= \common\services\documentation\DbTable::widget([
            'name'    => 'user_token',
            'columns' => [
                [
                    'name'        => 'id',
                    'isRequired'  => true,
                    'type'        => 'int',
                    'description' => 'Идентификатор',
                ],
                [
                    'name'        => 'user_id',
                    'isRequired'  => true,
                    'type'        => 'int',
                    'description' => 'Идентификатор того кто выпустил токен',
                ],
                [
                    'name'        => 'token_id',
                    'isRequired'  => true,
                    'type'        => 'int',
                    'description' => 'Идентификатор токена token_id',
                ],
            ],
        ]) ?>
        <p>Чтобы обновить картинку .</p>

    </div>
</div>



