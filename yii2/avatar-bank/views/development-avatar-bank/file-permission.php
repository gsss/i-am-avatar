<?php
use common\services\UsersInCache;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;

/* @var $this yii\web\View */

$this->title = 'Права на файлы';

?>
<div class="container">
    <div class="col-lg-12">
        <h1 class="page-header text-center"><?= Html::encode($this->title) ?></h1>

        <p>
            Корневая папка: <code>/home/avatar</code>
        </p>
        <?php
        $pathPhp = [
            '/avatar-bank/runtime',
            '/console/runtime',
            '/public_html/upload',
            '/public_html/assets',
        ];
        $stand = [
            'web-root',
            'web-root-test',
            'web-root-test-public',
            'web-root-stage',
        ];
        ?>
        <p>
            Для каждого стенда папки должны быть доступны для записи, чтения и изменения для процесса PHP
        </p>
        <?php foreach ($pathPhp as $p) { ?>
            <p>
                <code><?= $p ?></code>
            </p>
        <?php } ?>
        <p>
            Стенды
        </p>
        <?php foreach ($stand as $p) { ?>
            <p>
                <code><?= $p ?></code>
            </p>
        <?php } ?>
        <p>
            Для всех файлов всех стендов должны быть для записи, чтения и изменения для процесса FTP
        </p>


    </div>
</div>



