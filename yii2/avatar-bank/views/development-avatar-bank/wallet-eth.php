<?php
use common\services\UsersInCache;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;

/* @var $this yii\web\View */

$this->title = 'Кошелек ETH';

?>
<div class="container">
    <div class="col-lg-12">
        <h1 class="page-header text-center">
            <?= Html::encode($this->title) ?>
        </h1>
        <p><a href="https://www.blockcypher.com/dev/ethereum/" target="_blank">https://www.blockcypher.com/dev/ethereum/</a></p>
        <p>Стандарт написания адресов выран малыми буквами.</p>

        <p>
            <a href="https://drive.google.com/file/d/0BzHYNoEyPNTXVDlCWVYyOXpFbnM/view?usp=sharing" target="_blank">
                <img src="/images/controller/development-avatar-bank/wallet-eth/ETH.png" style="width: 100%;"/>
            </a>
        </p>

        <p>Класс <code>\avatar\models\WalletETH</code>.</p>
        <p>Интерфейс обслуживающий сервис использования кошельков: Сервер Сириус Атон</p>
        <p><code>https://atlantida.io</code> Рабочий интерфейс</p>
        <p><code>http://sirius-aton.avatar-bank.com</code> тестовый интерфейс</p>






        <h2 class="page-header">
            Список транзакций
        </h2>

        <h3 class="page-header">Возвращает список транзакций по счету</h3>
        <p class="alert alert-success">/eth/transactions-list</p>
        <p>Возвращает список транзакций по счету по 20 шт на страницу</p>

        <p><b>Входные параметры</b></p>
        <?= \avatar\services\Params::widget([
            'params' => [
                [
                    'name'        => 'id',
                    'isRequired'  => true,
                    'description' => 'Идентификатор счета по банку Аватар',
                ],
                [
                    'name'        => 'page',
                    'type'        => 'int',
                    'description' => 'Страница, по умолчанию = 1',
                ],
            ]
        ]) ?>

        <p><b>Выходные данные</b></p>
        <pre>
[
    {
      "blockNumber": "65204",
      "timeStamp": "1439232889",
      "hash": "0x98beb27135aa0a25650557005ad962919d6a278c4b3dde7f4f6a3a1e65aa746c",
      "nonce": "0",
      "blockHash": "0x373d339e45a701447367d7b9c7cef84aab79c2b2714271b908cda0ab3ad0849b",
      "transactionIndex": "0",
      "from": "0x3fb1cd2cd96c6d5c0b5eb3322d807b34482481d4",
      "to": "0xde0b295669a9fd93d5f28d9ec85e40f4cb697bae",
      "value": "0",
      "gas": "122261",
      "gasPrice": "50000000000",
      "isError": "0",
      "input": "0xf00d4b5d000000000000000000000000036c8cecce8d8bbf0831d840d7f29c9e3ddefa63000000000000000000000000c5a96db085dda36ffbe390f455315d30d6d3dc52",
      "contractAddress": "",
      "cumulativeGasUsed": "122207",
      "gasUsed": "122207",
      "confirmations": "3745410"
      },
]</pre>

        <?= \avatar\services\Params::widget([
            'params' => [
                [
                    'name'        => 'hash',
                    'isRequired'  => true,
                    'description' => 'Хеш транзации, идентификатор тразакции в блокчейне BTC',
                ],
                [
                    'name'        => 'timeStamp',
                    'isRequired'  => true,
                    'type'        => 'int',
                    'description' => 'Время создания транзакции',
                ],
                [
                    'name'        => 'value',
                    'isRequired'  => true,
                    'type'        => 'double',
                    'description' => 'Сумма на которую изменился кошелек после траннзакции. Измеряется в wei (10e-18). Сумма всегда положительна. Сумма не включает в себя комиссиию, то есть при отправке денег из кошелька, это реальная сумма отправки денег, а к ней еще плюсуется сумма комиссии',
                ],
                [
                    'name'        => 'confirmations',
                    'isRequired'  => true,
                    'type'        => 'int',
                    'description' => 'Кол-во подтверждений',
                ],
                [
                    'name'        => 'blockNumber',
                    'isRequired'  => true,
                    'type'        => 'int',
                    'description' => 'В каком блоке зарегистрирована транзакция. Если не подтвержена, то = null',
                ],
                [
                    'name'        => 'blockHash',
                    'isRequired'  => true,
                    'type'        => 'int',
                    'description' => 'Хеш блока. Если не подтвержена транзакция, то = null',
                ],
                [
                    'name'        => 'total_fee',
                    'isRequired'  => true,
                    'type'        => 'int',
                    'description' => 'Комиссия за транзакцию. Измеряется в wei (10e-18)',
                ],
                [
                    'name'        => 'nonce',
                    'isRequired'  => true,
                    'description' => '',
                ],
                [
                    'name'        => 'transactionIndex',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => '',
                ],
                [
                    'name'        => 'from',
                    'isRequired'  => true,
                    'description' => '',
                ],
                [
                    'name'        => 'to',
                    'isRequired'  => true,
                    'description' => '',
                ],
                [
                    'name'        => 'gas',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => '',
                ],
                [
                    'name'        => 'gasPrice',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => '',
                ],
                [
                    'name'        => 'isError',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => '',
                ],
                [
                    'name'        => 'input',
                    'isRequired'  => true,
                    'description' => '',
                ],
                [
                    'name'        => 'contractAddress',
                    'isRequired'  => true,
                    'description' => '',
                ],
                [
                    'name'        => 'cumulativeGasUsed',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => '',
                ],
                [
                    'name'        => 'gasUsed',
                    'isRequired'  => true,
                    'description' => '',
                ],

                [
                    'name'        => 'comment',
                    'description' => 'Коментарий к транзакции',
                ],
                [
                    'name'        => 'type',
                    'type'        => 'int',
                    'description' => 'Тип транзакции (направление операции), 1 - приход в кошелек, -1 - изъятие из кошелька',
                ],
                [
                    'name'        => 'data',
                    'type'        => 'object',
                    'description' => 'Данные о партнере сделки (счет, пользователь)',
                ],
                [
                    'name'        => 'value_converted',
                    'type'        => 'double',
                    'description' => 'Кол-во переданных монет. Значение в валюте которой хочет видеть пользователь. Если настройка не задана, то не будет этого параметра. Кол-во запятых после запятой соответствует валюте и значение задается в монетах указанной валюты.',
                ],
                [
                    'name'        => 'fee_converted',
                    'type'        => 'double',
                    'description' => 'Кол-во заплаченной комиссии. Значение в валюте которой хочет видеть пользователь. Если настройка не задана, то не будет этого параметра. Кол-во запятых после запятой соответствует валюте и значение задается в монетах указанной валюты.',
                ],
            ]
        ]) ?>


        <p>Всего есть три типа транзакций:</p>
        <p>1. Перевод эфира</p>
        <p><code>contractAddress</code> = "", <code>input</code> = "0x"</p>
        <p>Если <code>from</code> = настоящему кошельку, то перевод производится из кошелька (OUT)</p>
        <p>Если <code>to</code> = настоящему кошельку, то перевод производится в кошельек (IN)</p>

        <p>2. Исполнение контракта</p>
        <p><code>to</code> = адрес контракта, <code>contractAddress</code> = "", <code>input</code> - "0x...", <code>value</code> = "0"</p>

        <p>3. Создание контракта</p>
        <p><code>to</code> = 0, <code>contractAddress</code> - адрес созданного контракта, <code>input</code> - код контрата</p>


        <p>Параметры объекта <code>data</code></p>
        <p>Если есть параметр <code>user_id</code> то значит будет и <code>billing_id</code>.
            Если задан параметр <code>user_id</code> то будет и параметр <code>user</code>.
            Если задан параметр <code>billing_id</code> то будет и параметр <code>billing</code>.</p>
        <?= \avatar\services\Params::widget([
            'params' => [
                [
                    'name'        => 'user_id',
                    'type'        => 'int',
                    'description' => 'Идентификатор пользователя, от кого или кому была транзакция',
                ],
                [
                    'name'        => 'user',
                    'type'        => 'object',
                    'description' => 'Объект пользователя',
                ],
                [
                    'name'        => 'billing_id',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Идентификатор счета. С какого счта или на какой была транзакция',
                ],
                [
                    'name'        => 'billing',
                    'type'        => 'object',
                    'description' => 'Объект счета',
                ],
            ]
        ]) ?>
        <p>Параметры объекта <code>user</code></p>
        <?= \avatar\services\Params::widget([
            'params' => [
                [
                    'name'        => 'id',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Идентификатор пользователя',
                ],
                [
                    'name'        => 'name',
                    'isRequired'  => true,
                    'description' => 'Имя',
                ],
                [
                    'name'        => 'email',
                    'isRequired'  => true,
                    'description' => 'Почта клиента',
                ],
                [
                    'name'        => 'avatar',
                    'description' => 'Путь к аватарке',
                ],
            ]
        ]) ?>
        <p>Параметры объекта <code>billing</code></p>
        <?= \avatar\services\Params::widget([
            'params' => [
                [
                    'name'        => 'id',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Идентификатор счета',
                ],
                [
                    'name'        => 'name',
                    'isRequired'  => true,
                    'description' => 'Название счета',
                ],
            ]
        ]) ?>
        <p>Этот список описывает массив транзакций для одного кошелька</p>
        <p>Распарсивается со страницы <code>https://etherscan.io/txs?a=0xe34AdB396486c5D53dFe666627b1d0e8c7f55f63</code></p>
        <p><code>http://new-earth.avatar-bank.com/api/eth/transaction-list</code></p>
        <p>
            <a href="https://drive.google.com/file/d/0BzHYNoEyPNTXQ2VyT3EtTEpwazg/view?usp=sharing" target="_blank">
                <img src="/images/controller/development-avatar-bank/wallet-eth/ETH_transaction.png"/>
            </a>
        </p>

        <p><b>Поле <code>to</code></b></p>
        <p>Может быть в трех вариантах</p>


        <h2 class="page-header">
            Счет по умолчанию
        </h2>
        <p class="alert alert-danger">В разработке</p>
        <p>Что такое счет по умолчанию? Это счет который используется для выбора по умолчанию если отправитель монет указывает почту или телефон получателя.</p>

        <p>Устанавливается в настройках кабинета</p>
        <p>Исследование: для биткойна счет по умолчанию устанавливается где? Вопрос: сделать отдельно установку биткойна и эфира? я считаю да! это будет проще для понимания.</p>
        <p>Решение: Для установки счета по умолчанию ETH он будет устанавливаеться отдельно от биткойна.</p>
        <p>Решение: Для установки счета по умолчанию ETH он будет устанавливаеться отдельно от биткойна.</p>



    </div>
</div>



