<?php
use common\services\UsersInCache;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;

/* @var $this yii\web\View */

$this->title = 'Avatar ID';



?>
<div class="container">
    <div class="col-lg-12">
        <h1 class="page-header text-center"><?= Html::encode($this->title) ?></h1>

        <p>
            <a href="https://drive.google.com/file/d/1t47LLqeZuyiGzBqAAscur8tBiW0tDW8u/view?usp=sharing" target="_blank">
                <img src="/images/controller/development-avatar-bank/avatar-id/AvatarID.png" class="thumbnail">
            </a>
        </p>
        <p>
            <a href="https://drive.google.com/file/d/1gZFA5mVrYhZSyKR3fn4pjWyVHPTYd8IY/view?usp=sharing" target="_blank">
                <img src="/images/controller/development-avatar-bank/avatar-id/AvatarID_2.png" class="thumbnail">
            </a>
        </p>
        <p>
            <a href="https://drive.google.com/file/d/1CT7dnflW3WS8bUDyNcS8DYeg_XPKOXnK/view?usp=sharing" target="_blank">
                <img src="/images/controller/development-avatar-bank/avatar-id/AvatarID_3.png" class="thumbnail">
            </a>
        </p>


    </div>
</div>



