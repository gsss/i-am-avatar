<?php
use common\services\UsersInCache;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;

/* @var $this yii\web\View */

$this->title = 'Счета';

?>
<div class="container">
    <div class="col-lg-12">
        <h1 class="page-header text-center"><?= Html::encode($this->title) ?></h1>

        <p>Этот документ описывает систему построения счетов.</p>
        <p>Всего в Аватар Банке есть две валюты:</p>
        <p>BTC</p>
        <p>ETH</p>

        <h2 class="page-header">Описание</h2>

        <p><img src="/images/controller/development-avatar-bank/bills/wallet.png"></p>
        <p>Таблица в которой хранятся счета <code>user_bill</code>.</p>
        <p>За обеспечение и сервис с нии обеспечивает класс модель <code>\common\models\avatar\UserBill</code>.</p>
        <p>Контроллер <code>\avatar\controllers\CabinetBillsController</code>.</p>
        <p>Со счетом связаны его адреса, они сохраняются в таблице <code>user_bill_address</code>.</p>

        <?= \common\services\documentation\DbTable::widget([
            'name'    => 'user_bill',
            'columns' => [
                [
                    'name'        => 'user_id',
                    'isRequired'  => true,
                    'type'        => 'int',
                    'description' => 'Идентификатор пользователя',
                ],
                [
                    'name'        => 'address',
                    'isRequired'  => true,
                    'type'        => 'varchar(40)',
                    'description' => 'Идентификатор пользователя',
                ],
                [
                    'name'        => 'identity',
                    'type'        => 'varchar(40)',
                    'description' => 'Идентификатор кошелька',
                ],
                [
                    'name'        => 'password',
                    'type'        => 'varchar(90)',
                    'description' => 'Пароль от кошелька (хеш)',
                ],
                [
                    'name'        => 'password_type',
                    'type'        => 'tinyint',
                    'description' => 'Тип хранения пароля. 1 - в открытом виде, 2 - в зашифрованном виде, пароль от кабинета, 3 - в зашифрованном виде, свой личный пароль. Если null то считается что это то же самое что и 1',
                ],
                [
                    'name'        => 'pin',
                    'type'        => 'varchar(60)',
                    'description' => 'Хеш от PIN кода',
                ],
                [
                    'name'        => 'name',
                    'type'        => 'varchar(100)',
                    'description' => 'Название счета',
                ],
                [
                    'name'        => 'mark_deleted',
                    'type'        => 'tinyint',
                    'description' => 'Флаг указывающий что счет удален. 0 - активный, 1 - счет удален',
                ],
                [
                    'name'        => 'created_at',
                    'type'        => 'int',
                    'description' => 'Время создания счета',
                ],
                [
                    'name'        => 'is_merchant',
                    'type'        => 'tinyint',
                    'description' => 'Флаг что есть мерчант у счета',
                ],
                [
                    'name'        => 'card_number',
                    'type'        => 'varchar(18)',
                    'description' => 'Номер карты без пробелов',
                ],
                [
                    'name'        => 'is_default',
                    'type'        => 'tinyint',
                    'description' => 'Флаг что это счет по умолчанияю (не используется)',
                ],
                [
                    'name'        => 'currency',
                    'type'        => 'int',
                    'description' => 'Код валюты',
                ],
            ]
        ]) ?>

        <h2 class="page-header">Операции со счетом</h2>
        <?= \common\services\documentation\DbTable::widget([
            'name'    => 'user_bill_operation',
            'columns' => [
                [
                    'name'        => 'id',
                    'isRequired'  => true,
                    'type'        => 'int',
                    'description' => 'Идентификатор операции',
                ],
                [
                    'name'        => 'type',
                    'isRequired'  => true,
                    'type'        => 'int',
                    'description' => 'тип операции. 1 - приход в счет, -1 - расход из счета',
                ],
                [
                    'name'        => 'bill_id',
                    'isRequired'  => true,
                    'type'        => 'int',
                    'description' => 'Идентификатор счета с которым производится операция',
                ],
                [
                    'name'        => 'user_id',
                    'isRequired'  => true,
                    'type'        => 'int',
                    'description' => 'Идентификатор клиента которому принадлежит счет ' . Html::tag('code', 'bill_id'),
                ],
                [
                    'name'        => 'transaction',
                    'isRequired'  => true,
                    'type'        => 'varchar(70)',
                    'description' => 'Идентификатор транзакции',
                ],
                [
                    'name'        => 'message',
                    'isRequired'  => true,
                    'type'        => 'varchar(1000)',
                    'description' => 'Описание',
                ],
                [
                    'name'        => 'data',
                    'type'        => 'varchar(255)',
                    'description' => 'Данные в формате JSON для партнера сделки, если таковые имеются ' . Html::tag('code', 'user_id') . Html::tag('code', 'billing_id'),
                ],
            ],
        ]) ?>


        <h2 class="page-header">Описание</h2>
        <p>Счет заисывается в таблицу.</p>
        <p>Создается для него адрес и сохраняется.</p>

        <h2 class="page-header">Удаление</h2>

        <p>Для счета ставится поле `mark_deleted` = 1.</p>
        <p>Все деньги со счета переводятся на главный кошелек, усли не указан, то на первый созданный.</p>
        <p>И физический кошелек на BlockTrail удаляется.</p>
        <p>Если нет укаателей на счет в операциях, то счет удаляется.</p>
        <p>Если есть, то счет у нас остается и все операции со счетом.</p>

        <h2 class="page-header">Очистка WebHooks</h2>

        <p>Когда создается адрес и на него вешается WebHook на мерчанте то они начинают скапливаться.</p>
        <p>Они очищаются через консольную команду:</p>
        <pre>php yii billing/clear</pre>

        <h2 class="page-header">Счет по умолчанию</h2>

        <p>Счет по умолчанию хранится в таблице <code>user_bill_default</code></p>
        <?= \common\services\documentation\DbTable::widget([
            'name'    => 'user_bill_default',
            'columns' => [
                [
                    'name'        => 'id',
                    'isRequired'  => true,
                    'type'        => 'int',
                    'description' => 'Идентификатор счета '. Html::tag('code', 'user_bill'),
                ],
                [
                    'name'        => 'currency_id',
                    'isRequired'  => true,
                    'type'        => 'int',
                    'description' => 'Идентификатор валюты',
                ],
                [
                    'name'        => 'user_id',
                    'isRequired'  => true,
                    'type'        => 'int',
                    'description' => 'Идентификатор пользователя',
                ],
            ]
        ]) ?>
        <p>Счетов по умолчанию может быть несколько, но по одному на каждую валюту</p>
        <p>У пользователя есть функция <code>\common\models\UserAvatar::getPiramidaBilling</code> - она возвращает
        счет по умолчанию, получая в параметре валюту. Если она не указана, то по умолчанию считается вернуть счет BTC.</p>
        <p>У пользователя есть функция <code>\common\models\UserAvatar::getDefaultBillingList</code> - она возвращает
        счета по умолчанию</p>




        <h2 class="page-header">BTC</h2>
        <p><code>\common\components\providers\BTC</code></p>

        <h2 class="page-header">ETH</h2>

    </div>
</div>



