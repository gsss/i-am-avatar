<?php
use common\services\UsersInCache;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;

/* @var $this yii\web\View */

$this->title = 'Языки';



?>
<div class="container">
    <div class="col-lg-12">
        <h1 class="page-header text-center"><?= Html::encode($this->title) ?></h1>

        <h3 class="page-header">Глюк</h3>
        <p>Есть глюк и нужно его устранить. Непонятнок когда возникает.</p>
        <p>Заключается в следующем: ни с того ни с сего не показываются переводы. Похоже на то что сбрасывается кеш и причем если одна страница работает то другая нет.</p>
        <p class="lead">Кеш</p>
        <p><code>/common/config/main.php</code></p>
    <pre>
    'cacheLanguages' =>
        (!YII_ENV_DEV) ?
            [
                'class'     => 'yii\caching\MemCache',
                'keyPrefix' => 'avatar-bank-languages',
                'servers'   => [
                    [
                        'host' => 'localhost',
                        'port' => 11211,
                    ],
                ],
            ] :
            [
                'class'     => 'yii\caching\FileCache',
                'keyPrefix' => 'avatar-bank-languages',
            ],
        // Языки
        'i18n'           => [
            'translations' => [
                'app*' => [
                    'class'                 => '\common\components\DbMessageSourceSky',
                    'cache'                 => 'cacheLanguages',
                    'forceTranslation'      => true,
                    'enableCaching'         => true,
                    'sourceLanguage'        => 'ru',
                    'on missingTranslation' => ['\common\components\DbMessageSourceSky', 'handleMissingTranslation'],
                ],
                '*'    => [
                    'class'                 => '\common\components\DbMessageSourceSky',
                    'cache'                 => 'cacheLanguages',
                    'forceTranslation'      => true,
                    'enableCaching'         => true,
                    'sourceLanguage'        => 'ru',
                    'on missingTranslation' => ['\common\components\DbMessageSourceSky', 'handleMissingTranslation'],
                ],
            ],
        ],
    </pre>
        <p>Когда сбрасываю кеш в любой категории то все поднимается</p>
        <pre>Yii::$app->cacheLanguages->flush();</pre>


    </div>
</div>



