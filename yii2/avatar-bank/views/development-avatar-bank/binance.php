<?php
use common\services\UsersInCache;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;

/* @var $this yii\web\View */

$this->title = 'Связь с API Binance';


?>
<div class="container">
    <div class="col-lg-12">
        <h1 class="page-header text-center"><?= Html::encode($this->title) ?></h1>


        <p>API: <a href="https://github.com/binance-exchange/binance-official-api-docs" target="_blank">https://github.com/binance-exchange/binance-official-api-docs</a></p>
        <p>Для соединения с API Binance необходимы два параметра для пользователя <code>apiKey</code> и <code>apiSecret</code>, их мы храним в БД.</p>

        <?= \common\services\documentation\DbTable::widget([
            'name'    => 'user_binance',
            'columns' => [
                [
                    'name'        => 'id',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Идентификатор пользователя',
                ],
                [
                    'name'        => 'api_key',
                    'type'        => 'varchar(64)',
                    'isRequired'  => true,
                    'description' => 'apiKey',
                ],
                [
                    'name'        => 'api_secret',
                    'type'        => 'varchar(64)',
                    'isRequired'  => true,
                    'description' => 'apiSecret',
                ],
            ],
        ]) ?>

        <p><code>\common\models\avatar\UserBinance</code> - модель доступа к базе данных</p>
        <p><code>\avatar\modules\Binance\Binance</code> - компонент который автономно подключается в API Binance</p>
        <p><code>\avatar\modules\Binance\UserBinanceApi</code> - компонент который подключается в API Binance <code>\avatar\modules\Binance\Binance</code> с установленными параметрами доступа взятых из <code>\common\models\avatar\UserBinance</code></p>

        <p>Общая схема работы <code>\avatar\modules\Binance\Binance</code></p>
        <p>
            <a href="https://drive.google.com/file/d/1rSC-qtG1zR7JS6ImzeA9FXK-ReeA5Nup/view?usp=sharing" target="_blank">
                <img src="/images/controller/development-avatar-bank/binance/Binance.png">
            </a>
        </p>

        <h2 class="page-header">Как пользоваться</h2>

        <pre>$user = UserAvatar::findOne(1);
$userBinanceApi = $user->getBinanceAPI();</pre>

        <p>Если у пользователя не установлены API KEYS, то будет вызвано исключение с кодом 101.</p>

        <h2 class="page-header">Логирование</h2>
        <p><code>avatar\modules\Binance\Binance::_call::before</code> - вызывается перед отправкой запроса на API</p>
        <p>Содержит <code>[$request, $path, $data]</code></p>
        <p><code>avatar\modules\Binance\Binance::_call::after</code> - вызывается после отправки запроса на API</p>
        <p>Содержит <code>$response</code></p>

        <h2 class="page-header">Установка</h2>
        <p>Для того чтобы установить Ключи Binance нужно зайти в настройки и перейти в раздел API Binance.</p>

    </div>
</div>



