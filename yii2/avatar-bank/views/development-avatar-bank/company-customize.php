<?php
use common\services\UsersInCache;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;

/* @var $this yii\web\View */

$this->title = 'Брендирование компаний';

?>
<div class="container">
    <div class="col-lg-12">
        <h1 class="page-header text-center"><?= Html::encode($this->title) ?></h1>

        <p>Что входит в брендирование:</p>

        <ul>
            <li>Главный лендинг с формой отправки</li>
            <li>Блог и новости</li>
            <li>Форма отправки сообщений</li>
            <li>Реквизиты внизу страницы</li>
        </ul>

        <h2>Где хранятся настройки</h2>
        <p>Настройки хранятся в таблице <code>company_customize</code> и кешируются в мемкеш.</p>
        <?= \common\services\documentation\DbTable::widget([
            'name'    => 'company_customize',
            'columns' => [
                [
                    'name'        => 'id',
                    'isRequired'  => true,
                    'type'        => 'int',
                    'description' => 'Идентификатор настройки',
                ],
                [
                    'name'        => 'config',
                    'isRequired'  => true,
                    'type'        => 'text',
                    'description' => 'Конфигурация. JSON',
                ],
            ]
        ]) ?>


        <h2>Блог и новости</h2>
        <p>В таблицы <code>blog</code> и <code>news</code> добавлен параметр <code>company_id</code>. Он обозначает идентификатор из таблицы <code>company_customize</code>.</p>


        <h2>Реквизиты внизу страницы</h2>
        <p>
            <img src="/images/controller/development-avatar-bank/company-customize/2018-06-09_19-21-53.png" class="thumbnail">
        </p>

        <p>Что входит:</p>
        <ul>
            <li>Адрес почты</li>
            <li>Название организации</li>
            <li>Ссылки на соц сети</li>
        </ul>

        <h2>Параметры</h2>
        <pre>{
  "domain-name": "cabinet.new-earth.space",
  "logo": {
    "small": "/images/controller/landing-new-earth/index/transparent.png",
    "big": "/images/controller/landing-new-earth/index/transparent.png"
  },
  "favicon": "/images-rai/favicon.ico",
  "contact": {
    "mail": "sacred-home@new-earth.space"
  },
  "is-blog": 1,
  "is-news": 1,
  "domain-name-image": "/images-rai/domain-name.png",
  "landing": {
    "layout": "landing-new-earth",
    "view": "/landing-new-earth/index"
  },
  "footer": {
    "mail": "sacred-home@new-earth.space",
    "name": "Проект «РАЙ»",
    "social-nets": [
      {
        "type": "telegram",
        "url": "https://t.me/cityoflight2018"
      },
      {
        "type": "facebook",
        "url": "https://www.facebook.com/paradiseuniverse999/"
      },
      {
        "type": "vk",
        "url": "https://vk.com/public166290693"
      },
      {
        "type": "skype",
        "url": "https://join.skype.com/eH2TP4IwEZwX"
      }
    ]
  }
}</pre>
        <?= \avatar\services\Params::widget([
            'params' => [
                [
                    'name'        => 'domain-name',
                    'isRequired'  => true,
                    'type'        => 'string',
                    'description' => 'Название домена малыми буквами',
                ],
                [
                    'name'        => 'favicon',
                    'isRequired'  => false,
                    'type'        => 'string',
                    'description' => 'путь к картинке ICO для сайта компании',
                ],
                [
                    'name'        => 'is-blog',
                    'isRequired'  => false,
                    'type'        => 'int',
                    'description' => 'Есть ли блог у компании?. 0 - нет, 1 - есть. По умолчанию = 0',
                ],
                [
                    'name'        => 'is-news',
                    'isRequired'  => false,
                    'type'        => 'int',
                    'description' => 'Есть ли новости у компании?. 0 - нет, 1 - есть. По умолчанию = 0',
                ],
            ]
        ]) ?>


        <h2>Лендинг</h2>

        <p>Обработчик вызова страницы лендинга выглядит следующим образом</p>

        <pre>public function actionIndex()
{
    switch ($_SERVER['HTTP_HOST']) {
        case 'cabinet.new-earth.space':
            $layout = 'landing-new-earth';
            $view = '/landing-new-earth/index';
            break;
        default:
            $layout = 'landing';
            $view = '/landing/index';
            break;
    }

    $this->layout = $layout;

    return $this->render($view);
}</pre>

        <h2>Сервис</h2>
        <p>Для облегчения работы с брендированием в коде существует сервис <code>\avatar\services\CustomizeService</code></p>

        <p>Пример использования:</p>

        <pre>/** @var \avatar\services\CustomizeService $CustomizeService */
$CustomizeService = Yii::$app->CustomizeService;
$company_id = $CustomizeService->getParam1('id');</pre>

        <h2>Алгоритм дествия при отсутствии значения в конфигурации</h2>
        <p>Если запрашиваемый параметр не найден в конфигурации, то возвращаемое значение определяется из алгоритма</p>
        <p><code>ALGORITM_DEFAULT</code> - Возвращается значение из главной конфигурации <code>avatarnetwork.io</code></p>
        <p><code>ALGORITM_EMPTY</code> - Возвращается значение null</p>

    </div>
</div>



