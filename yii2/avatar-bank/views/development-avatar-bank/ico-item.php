<?php
use common\services\UsersInCache;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;

/* @var $this yii\web\View */

$this->title = 'Страница проекта';

?>
<div class="container">
    <div class="col-lg-12">
        <h1 class="page-header text-center"><?= Html::encode($this->title) ?></h1>

        <p>Страница проекта находится на странице <code>/projects/item?id=28</code></p>
        <p><img src="/images/controller/development-avatar-bank/ico-item/Screenshot_1.png" class="thumbnail"></p>

        <h3 class="page-header">График</h3>
        <p>По какому критерию мне определять что была продажа токенов? По факту успешной отправки конечно же. Хорошо</p>

        <h3 class="page-header">Страница ICO</h3>
        <p>Как отображать кол-во токенов? и вопрос каких?</p>
        <p>Я считаю что лучшим решением будет показывать сколько реально токенов осталось на счете с которого отправляются токены.</p>
        <p>А если на нем не все токены лежат? То ведь будет не красиво что показано всего несколько.</p>
        <p>Или например что на счет токкены докидываются. Тогда этот случай вообще не подходит.</p>
        <p>Какие могут быть предложения? Или решения?</p>
        <p>Не знаю давай перебирать варианты.</p>

        <p>
            1 решение: в параметре <code>project.count</code> хранится кол-во монет, которые не распроданы для покупателей. После каждой продажи это число вычитается.
            Так клиент всегда видит сколько еще монет есть в наличии.
            Тогда получается что это число можно назвать кол-во монет выставленных на распродажу.
            Тем более если это pre ICO то кол-во монет полюбому будет не равно totalAmount.
        </p>

        <p>
            2 решение: под каждую пачку выпускается краудсейл контракт. Это нужно исследовать сначала. чтобы внедрять. Поэтому это сейчас не актуально.
        </p>

        <p>
            Вывод: за неимением другого варианта испольую первый. При этом время от времени можно производить синхронизацию счета для надежности.
            Как назвать поле кол-во оставшихся токенов? remainedTokens.
        </p>
        <p>
            Также я думаю возникнет необходимость показать общее колво выставленных на продажу токенов.
            За это будет отвечать поле allTokens. Общее количество токенов выставленных на продажу на момент краудсейла.
            Оба поля будут размещаться в таблице <code>project_ico</code>и называться <code>remained_tokens</code>, <code>total_tokens</code>.
        </p>
        <p>
            В итоге я получаю что проект - это программа (краудсейл, акция, мероприятие) продажи определенного объема токенов за выделенный участок времени.
        </p>

        <p style="margin-top: 150px;"><b>Поход по страницам</b></p>
        <p>
            <a href="https://drive.google.com/file/d/1JZQrGFnTBHuQcWEJjiiAL5831dYOJ5GZ/view?usp=sharing" target="_blank">
                <img
                        src="/images/controller/development-avatar-bank/ico-item/path1.png"
                        class="thumbnail"
                />
            </a>
        </p>


        <h3 class="page-header">Варианты использования полей Email и address</h3>
        <p>
            <a href="https://drive.google.com/file/d/0BzHYNoEyPNTXbVVCSFhLYU9Rd0k/view?usp=sharing" target="_blank">
                <img src="/images/controller/development-avatar-bank/ico/mail-address.png" class="thumbnail">
            </a>
        </p>
        <p>Нужно просмотреть все четыре варианта использования этой комбинации.</p>
        <p class="label label-danger">Пользователь не авторизован</p>
        <p>Если пользователь не авторизован и вводит свой email под которым он уже зарегистрирован, то пользователю под
            полем выводится сообщение "Такой Email уже зарегистрирован, если это ваш, то войдите сначала под своим
            логином и после купите"</p>
        <?= \yii\grid\GridView::widget([
            'dataProvider' => new \yii\data\ArrayDataProvider([
                'allModels' => [
                    [
                        'address' => '0',
                        'email'   => '0',
                        'comment' => 'Такое невозможно',
                        'db'      => '',
                    ],
                    [
                        'address' => '0',
                        'email'   => '1',
                        'comment' => 'Пользователю будет создан аккаунт и кошелек',
                        'db'      => 'email сохраняется', // Создается аккаунт и создаются временные кошельки
                    ],
                    [
                        'address' => '1',
                        'email'   => '0',
                        'comment' => 'Можно конечно, но тогда нельзя будет связаться с клиентом если что, поэтому лучше запретим такую ситуацию',
                        'db'      => '', // Ничего
                    ],
                    [
                        'address' => '1',
                        'email'   => '1',
                        'comment' => 'Пользователю будет создан аккаунт, а токены отправлены на его адрес',
                        'db'      => 'email,address сохраняется', // Создается аккаунт и Токены отправляются на address
                    ],
                ],
            ]),
            'tableOptions' => [
                'class' => 'table table-hover table-striped',
                'style' => 'width: auto;',
            ],
            'summary'      => '',
        ]) ?>

        <p class="label label-success">Пользователь авторизован</p>
        <?= \yii\grid\GridView::widget([
            'dataProvider' => new \yii\data\ArrayDataProvider([
                'allModels' => [
                    [
                        'address' => '0',
                        'comment' => 'Клиенту будет создан внутренний кошелек и на него будут зачислены токены',
                        'db'      => '', // Здесь нужно создание кошелька для токена и только потом. Хотя можно и на 1 тип повесить. Тогда нужно предусмотреть что после перепрошивки кошелька на 2 тип хранения пароля не выдавались и не создавались SEEDS
                    ],
                    [
                        'address' => '1',
                        'comment' => 'Токены будет отправлены на указанный адрес',
                        'db'      => 'address сохраняется',
                    ],

                ],
            ]),
            'tableOptions' => [
                'class' => 'table table-hover table-striped',
                'style' => 'width: auto;',
            ],
            'summary'      => '',
        ]) ?>

        <p>Вопрос: что делать с неавторизованными пользователями? Их же надо подтвердить сначала?! Даже по пин коду
            хотябы</p>
        <p>Допустим я его не проверил а доверился, но все равно выслал ему письмо с подтверждением. И только после
            подтверждения ему отсылать токены.</p>
        <p>Так же пользователю нужно пройти полную процедуру входа в кабинет с получением ключей хранения. И только
            после этого ему будут высланы токены.</p>
        <p>Задание пароля сразу при покупке токенов тоже не актуально, так как пользователю нужно получить все ключи от
            кошелька. Поэтому Выдавать ему токены можно только после входа его в кабинет.</p>

        <p>Всего у меня четыре ситуации получилось которые нужно обработать и при всех адрес либо указывается ли тот что авторизован</p>
        <?= \yii\grid\GridView::widget([
            'dataProvider' => new \yii\data\ArrayDataProvider([
                'allModels' => [
                    [
                        'auth'    => '0',
                        'address' => '0',
                        'db'      => 'email сохраняется',
                        'action'  => 'Создается аккаунт и создаются временные кошельки',
                    ],
                    [
                        'auth'    => '0',
                        'address' => '1',
                        'db'      => 'email, address сохраняется',
                        'action'  => 'Создается аккаунт/производится подписка на рассылку и Токены отправляются на address',
                    ],
                    [
                        'auth'    => '1',
                        'address' => '0',
                        'db'      => '',
                        'action'  => 'Здесь нужно создание кошелька для токена и только потом. Хотя можно и на 1 тип повесить. Тогда нужно предусмотреть что после перепрошивки кошелька на 2 тип хранения пароля не выдавались и не создавались SEEDS',
                    ],
                    [
                        'auth'    => '1',
                        'address' => '1',
                        'db'      => 'address сохраняется',
                        'action'  => 'Токены будет отправляются на указанный адрес',
                    ],
                ],
            ]),
            'tableOptions' => [
                'class' => 'table table-hover table-striped',
                'style' => 'width: auto;',
            ],
            'summary'      => '',
        ]) ?>

        <h3 class="page-header">Маршрут покупки токена</h3>
        <p>
            <a href="https://drive.google.com/file/d/0B9lrcmdS-YbTeVN1NkxIalZFNWM/view?usp=sharing" target="_blank">
                <img src="/images/controller/development-avatar-bank/ico/send_token.png" class="thumbnail" width="100%">
            </a>
        </p>
        <p>
            <a href="https://drive.google.com/file/d/0B9lrcmdS-YbTNURGbjdZSkg0Y1k/view?usp=sharing" target="_blank">
                <img src="/images/controller/development-avatar-bank/ico/buy.png" class="thumbnail">
            </a>
        </p>
        <p>Вопрос: Хранить пароль от кошелька токенов или вводить каждый раз по подтверждению?</p>
        <p>Ответ: Лучше подтверждать отдельно каждый заказ. Еще лучше сделать подтверждение по Google authentification</p>
        <hr>
        <p><a href="ico-path1">Маршрут 1</a></p>
        <p><a href="ico-path2">Маршрут 2</a></p>
        <p><a href="ico-path3">Маршрут 3</a></p>
        <p><a href="ico-path4">Маршрут 4</a></p>
    </div>
</div>



