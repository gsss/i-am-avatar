<?php
use common\services\UsersInCache;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;

/* @var $this yii\web\View */

$this->title = 'Номер карты Аватара';

?>
<div class="container">
    <div class="col-lg-12">
        <h1 class="page-header text-center"><?= Html::encode($this->title) ?></h1>
        <p>Биткойн кошелек для карты уже задан жестко на карте, а другие кошельки к ней можно привязать самостоятельно</p>
        <p>Префикс номер или БИН кодом является всегда 8888.</p>
        <p>Префикс платежной системы является 8 и обозначает платежную
            систему AvatarPay (Платежная система для свободных людей - Аватаров).</p>
        <p>Последний номер контрольный и вычисляется так:</p>
        <p>Здесь алгоритм вычисления последнего кода.</p>
        <p>Остальные номера указывают на номер счета привязанный к карте.</p>
        <p>16-4-1 = 11</p>

        <h2 class="page-header">Активация карты</h2>
        <p>Ее можно активировать в кабинете нажав на кнопку "Активировать карту".</p>
        <p>Откроется окно "Активировать карту"</p>
        <p>В окне нужно ввести "название счета" и "Номер карты"</p>
        <p>Нажать кнопку "Активировать"</p>



        <h2 class="page-header">Представление в БД</h2>
        <p>Модель: <code>\common\models\Card</code></p>
        <?= \common\services\documentation\DbTable::widget([
            'name'   => 'card',
            'params' => [
                [
                    'name'        => 'id',
                    'isRequired'  => true,
                    'description' => 'Идентификатор карты',
                    'type'        => 'int',
                ],
                [
                    'name'        => 'number',
                    'description' => 'Номер карты без пробелов',
                    'type'        => 'int',
                ],
                [
                    'name'        => 'design_id',
                    'description' => 'Идентификатор дизайна',
                    'type'        => 'int',
                ],
                [
                    'name'        => 'secret_num',
                    'description' => 'Секретный номер на обратной стороне',
                    'type'        => 'int',
                ],
                [
                    'name'        => 'finish',
                    'description' => 'Дата окончания карты, формат \'MM/YYYY\'',
                    'type'        => 'varchar(10)',
                ],
            ]
        ]) ?>

        <p>Модель: <code>\common\models\CardDesign</code></p>
        <?= \common\services\documentation\DbTable::widget([
            'name'   => 'card_design',
            'params' => [
                [
                    'name'        => 'id',
                    'isRequired'  => true,
                    'description' => 'Идентификатор дизайна',
                    'type'        => 'int',
                ],
                [
                    'name'        => 'image',
                    'description' => 'Ссылка на файл дизайна',
                    'type'        => 'varchar(255)',
                ],
            ]
        ]) ?>
        <h3 class="page-header">Модель данных</h3>
        <p>
            <a href="https://drive.google.com/file/d/0BzHYNoEyPNTXOENJN2V3elB2YVk/view?usp=sharing" target="_blank">
                <img
                    src="/images/controller/development-avatar-bank/card-number/card.png"
                    class="thumbnail"
                />
            </a>
        </p>
        <p>
            <a href="https://drive.google.com/file/d/0B9lrcmdS-YbTZEk0YzlLaFVhV00/view?usp=sharing" target="_blank">
                <img
                    src="/images/controller/development-avatar-bank/card-number/schema.png"
                    class="thumbnail"
                />
            </a>
        </p>

        <p>Поле <code>user_bill.card_number</code> будет не действительно (depricated). Нужно будет пользоваться полем <code>card.number</code></p>



        <h3 class="page-header">Переход на новую систему</h3>
        <p>Будет осуществлен после 14.07.2017</p>
        <pre>php yii cards/new-design</pre>
        <p>Переконвертирует старую модель данных в новую</p>
        <p>При конвертации нашлась одна дырка:</p>
        <p><img src="/images/controller/development-avatar-bank/card-number/2017-07-22_20-52-32.png" class="thumbnail" width="50%"></p>
        <p>Здесь поля <code>from</code> и <code>to</code> указывают на <code>bill_codes.id</code></p>
        <p>А <code>id</code> из таблицы <code>card_design</code></p>
        <?= \yii\grid\GridView::widget([
            'dataProvider' => new \yii\data\ArrayDataProvider([
                'allModels' => [
                    [
                        'id'       => 2,
                        'name'     => 'голубая',
                        'from'     => '69',
                        'to'       => '568',
                        'from_num' => '0',
                        'to_num'   => '499',
                    ],
                    [
                        'id'       => 1,
                        'name'     => 'СССР',
                        'from'     => '569',
                        'to'       => '668',
                        'from_num' => '500',
                        'to_num'   => '599',
                    ],
                    [
                        'id'       => 6,
                        'name'     => 'ГССС',
                        'from'     => '669',
                        'to'       => '768',
                        'from_num' => '600',
                        'to_num'   => '699',
                    ],
                    [
                        'id'       => 4,
                        'name'     => 'Авиалинии БогДан',
                        'from'     => '769',
                        'to'       => '868',
                        'from_num' => '700',
                        'to_num'   => '799',
                    ],
                    [
                        'id'       => 5,
                        'name'     => 'Вторжение с Небес',
                        'from'     => '869',
                        'to'       => '968',
                        'from_num' => '800',
                        'to_num'   => '899',
                    ],
                    [
                        'id'       => 3,
                        'name'     => 'Природа',
                        'from'     => '969',
                        'to'       => '1068',
                        'from_num' => '900',
                        'to_num'   => '999',
                    ],
                ],
            ]),
            'tableOptions' => [
                'class' => 'table table-hover table-striped',
                'style' => 'width: auto;',
                'id'    => 'tableTransaction',
            ],
            'columns' => [
                    'id',
                    'name',
                    [
                        'header' => 'Дизайн',
                        'content' => function($item) {
                            $design = \common\models\CardDesign::findOne($item['id']);
                            return Html::img(\common\widgets\FileUpload3\FileUpload::getOriginal($design->image), ['width' => 50, 'class' => 'thumbnail', 'style' => 'margin-bottom: 0px;']);
                        }
                    ],
                    'from',
                    'to',
                    'from_num',
                    'to_num',
            ],
        ]) ?>

        <h3 class="page-header">Интерфейс управления</h3>
        <p class="alert alert-danger">В разработке</p>
        <p>Так как Биткойн кошелек для карты уже задан жестко на карте, то его менять нельзя, а вот другие кошельки к ней можно привязать самостоятельно. Это делается на странице карты.</p>
        <p>Для этого необходимо зайти в карту. Вопрос Как? Вариант один - там где биткйон счет будет переход в карту + там где привязанный счет то тоже можно перейти на карту.</p>
        <p>Там выбрать привязу счета к карте.</p>
        <p>Там где карта будет ее дизайн и привязанные счета.</p>
        <p>Для показа картинки карты используется оригинальное изображение. (см <code>\common\widgets\FileUpload3\FileUpload</code>)</p>
        <p>Так выглядит страница карты:</p>
        <p>
            <img src="/images/controller/development-avatar-bank/card-number/des.png" class="thumbnail" width="50%">
        </p>
        <h3 class="page-header">Функция добавить счет к какрте</h3>
        <p>Здесь нужно вывести счета, которые не прикреплены ни к отдной карте и за исключением биткойн счетов и тех валют которые на карте уже есть.</p>

        <h3 class="page-header">Шифрование поролей начальных</h3>
        <p>Это необходимо для того чтобы в БД не было открытых паролей.</p>
        <p>Все нераспределенные счета карт зашифрованы по паролю который находится у "управляющего картами" (<code>role_card-admin</code>), он же и активирует карту.</p>
        <p>Получается что для того чтобы по новому алгоритму активировать карту, то сначала заявка на активацию отправляется "управляющему картами". Ему приходит письмо и он вводит пароль для разблокировки и пароль расшифровывается.</p>
        <p>Есть два решения:</p>
        <p>1. Выдавать всем ключи для расшифровки</p>
        <p>2. Разблокировать, перешифровать и выслать на почту временный пароль.</p>

        <h3 class="page-header">NFC</h3>
        <p><a href="https://geektimes.ru/post/138623/">https://geektimes.ru/post/138623/</a></p>
        <p><a href="https://3dnews.ru/627178">https://3dnews.ru/627178</a></p>
        <p>Пример главного окна</p>
        <p><img src="/images/controller/development-avatar-bank/card-number/nfc.png" class="thumbnail"></p>
        <p>Запись производится в параметрами key=ffffffffffff</p>

        <h3 class="page-header">Ситуация "Пользователь потерял карту"</h3>
        <p>В этом случае пользователь должен зайти в кабинет и нажать заблокировать карту. При этом счета не закрываются и если клиент захочет при помощи ключей получить доступ к карте то у него не получится.</p>

        <h2 class="page-header">Изменение типа связи на кодах и картах</h2>
        <p>Меняю на новую структуру</p>
        <p>
            <a href="https://drive.google.com/file/d/1mNNyahTJA4EUQO4stKA5ywd4H-kI7NOj/view?usp=sharing" target="_blank">
                <img
                    src="/images/controller/development-avatar-bank/card-number/new_link.png"
                    class="thumbnail"
                />
            </a>
        </p>
        <p>В итоге что нужно поменять?</p>
        <p>0. Создать поле bill_codes.card_id</p>
        <p>1. В соответствии с каждым неиспользованным кодом найти счет</p>
        <p>2. Найти карту</p>
        <p>3. Установить bill_codes.card_id</p>
        <p>4. В активации карты проверить</p>
        <p>5. В активации карты внутри кабинета проверить</p>
        <p>6. Изменить алгоритм выпуска карт</p>



    </div>
</div>



