<?php
use common\services\UsersInCache;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;

/* @var $this yii\web\View */

$this->title = 'Кошелек ETH - транзакции';

?>
<div class="container">
    <div class="col-lg-12">
        <h1 class="page-header text-center">
            <?= Html::encode($this->title) ?>
        </h1>
        <p>Транзакции вызываются по AJAX <code>/cabinet-bills/transactions-list-eth</code></p>
        <p>- page   - int - номер страницы. Если нет траницы то показываю первую</p>
        <p>- id     - int - идентификатор счета \common\models\avatar\UserBill</p>
        <p>Обращение идет к </p>
        <p>\avatar\controllers\actions\CabinetBills\TransactionsListEth::run</p>
        <p>\avatar\models\WalletETH::transactionListComments</p>
        <p>\avatar\models\WalletETH::transactionList</p>
        <p>\avatar\modules\ETH\ServiceEtherScan::transactionList</p>
        <pre>[
    'module'  => 'account',
    'action'  => 'txlist',
    'address' => $address,
    'sort'    => 'desc',
    'page'    => $page,
    'offset'  => $perPage,
]</pre>

        <p>
            <img src="/images/controller/development-avatar-bank/wallet-eth-page/Screenshot_1.png">
        </p>

    </div>
</div>



