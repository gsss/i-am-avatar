<?php
use common\services\UsersInCache;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;

/* @var $this yii\web\View */

$this->title = 'Карта Криптовалют';

?>
<div class="container">
    <div class="col-lg-12">
        <h1 class="page-header text-center"><?= Html::encode($this->title) ?></h1>
        <p class="alert alert-danger">В разработке</p>

        <p>Таблица хранит точки на карте где расположены банкоматы.</p>

        <p>Кто имеет право добавлять криптоматы?</p>
        <p>Предложение: тот кто имеет какую нибудь роль, допустим <code>role_map</code>.</p>
        <p>На карте могут отображаться как криптоматы так и места где принимают биткойны для оплаты.</p>
        <p>Сейчас предполагается отобразить на карте только криптоматы и обменники.</p>
        <p>Криптоматы и обменники это по сути обменники.</p>
        <p>В итоге на карте могут отображаться обменники и точки оплаты.</p>
        <p>Криптоматы могут как принимать фиатные деньги, так и выдавать.</p>

        <p>Основная модель <code>\common\models\MapBankomat</code>.</p>

        <?= \common\services\documentation\DbTable::widget([
            'name'   => 'map_bankomat',
            'columns' => [
                [
                    'name'        => 'name',
                    'isRequired'  => true,
                    'type'        => 'varchar(255)',
                    'description' => 'Название точки'
                ],
                [
                    'name'        => 'user_id',
                    'isRequired'  => true,
                    'type'        => 'int(11)',
                    'description' => 'Идентификатор пользователя кто добавил'
                ],
                [
                    'name'        => 'created_at',
                    'isRequired'  => true,
                    'type'        => 'int(11)',
                    'description' => 'Время добавления'
                ],
                [
                    'name'        => 'company_id',
                    'type'        => 'int(11)',
                    'description' => 'Компания которой принадлежит банкомат'
                ],
                [
                    'name'        => 'lat',
                    'isRequired'  => true,
                    'type'        => 'double',
                    'description' => 'Широта'
                ],
                [
                    'name'        => 'lng',
                    'isRequired'  => true,
                    'type'        => 'double',
                    'description' => 'Долгота'
                ],
                [
                    'name'        => 'content',
                    'type'        => 'text',
                    'description' => 'Текст полного описания, HTML possible'
                ],
                [
                    'name'        => 'description',
                    'type'        => 'text',
                    'description' => 'Краткое описание'
                ],
                [
                    'name'        => 'image',
                    'type'        => 'varchar(255)',
                    'description' => 'Изображение для криптомата'
                ],
                [
                    'name'        => 'btc',
                    'type'        => 'tinyint(1)',
                    'description' => '1 - можно купить биткойны'
                ],
                [
                    'name'        => 'eth',
                    'type'        => 'tinyint(1)',
                    'description' => '1 - можно купить эфир'
                ],
                [
                    'name'        => 'enc',
                    'type'        => 'tinyint(1)',
                    'description' => '1 - можно купить энергомонеты'
                ],
            ],
        ]) ?>


    </div>
</div>



