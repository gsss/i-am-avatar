<?php
use common\services\UsersInCache;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;

/* @var $this yii\web\View */

$this->title = 'Перевод денег';

?>
<div class="container">
    <div class="col-lg-12">
        <h1 class="page-header text-center">
            <?= Html::encode($this->title) ?>
        </h1>

        <p>Возможности окна (функции)</p>
        <p>Осуществляется в два этапа</p>
        <ul>
            <li>1. заполнение данных</li>
            <li>2. подтверждение даннх (видно кому осуществляется перевод)</li>
            <li>3. под вопросом: подтверждение по СМС</li>
        </ul>
        <p>Рассмотримна примере:</p>

        <p>Для кошелька ETH:</p>
        <p><img src="/images/controller/development-avatar-bank/wallet-send/modal.png" class="thumbnail"/></p>
        <p>Для кошелька BTC:</p>
        <p><img src="/images/controller/development-avatar-bank/wallet-send/send-btc.png" class="thumbnail"/></p>
        <p>Параметры окна:</p>
        <ul>
            <li><code>Кому</code> - Можно указать адрес счета, почту или телефон клиента</li>
            <li><code>Сколько</code> - Можно указать кол-во монет для перевода и указать валюту.
                Эта сумма будет переконвертирована в сумму валюты счета назначения.
                То есть если вы переводит на счет Эфира 1 Биткойн, то он будет переконвертирован то кол-во монет эфира которые равно мировому курсу на данный момент</li>
            <li><code>Пароль</code> - Пароль от кабинета</li>
            <li><code>Комментарий</code> - Коментарий к платежу, он будет отображен в вашем счете и счете поступления</li>
        </ul>
        <p>После заполнения формы и нажатие на кнопку "Перевести" выходит окно подтвердения.</p>
        <p><img src="/images/controller/development-avatar-bank/wallet-send/confirm.png" class="thumbnail"/></p>
        <p>После нажатия на кнопку "Подтвердить" выходит окно успеха.</p>
        <p><img src="/images/controller/development-avatar-bank/wallet-send/success.png" class="thumbnail"/></p>


        <h2 class="page-header">Отправка денег</h2>
        <p>Отправка производится в несколько этапов</p>
        <p>1. Нажать на кнопку отправить
            (со страницы "счета)
            кнопка .buttonSend</p>



        <p>открываю модальное окно `#modalSend`</p>
        <p>После нажатия кнопки '.buttonModalSend' вызываю ajax</p>
        <p>открываю модальное окно `#modalSendConfirm`</p>

        <p>Возможные варианты:</p>
        <ul>
            <li>1. Пользователь счет</li>

            <li>2. Открывается модальное окно (кому, сколько)</li>
            <li>3. Вызывается /cabinet-bills/send</li>
            <li>4. Открывается модальное окно (подтверждение: выводятся личные данные получателя и аватарка)</li>
            <li>5. Вызывается /cabinet-bills/send-confirm</li>
            <li>6. Открывается модальное окно "Успешно"</li>
        </ul>


        <p>
            <a href="https://drive.google.com/file/d/0BzHYNoEyPNTXNE1LV0JCcmRzWFU/view?usp=sharing" target="_blank">
                <img src="/images/controller/development-avatar-bank/wallet-send/wallet-send.png">
            </a>
        </p>

        <p>Возвращаемые значения функцией <code>/cabinet-bills/send</code></p>
        <pre>
[
    'address'  => $this->address,
    'amount'   => $this->amount,
    'currency' => $this->currency,
    'hasUser'  => true,
    'user'     => [
        'id'     => $user->id,
        'name2'  => $user->getName2(),
        'avatar' => $user->getAvatar(),
    ],
    'billing'  => [
        'id'       => $billing->id,
        'address'  => $billing->address,
        'name'     => $billing->name,
        'currency' => $billing->currency == 1 ? 'BTC' : 'ETH',
        'image'    => '/images/controller/cabinet-bills/index/bitcoin2.jpg',
    ],
]
        </pre>
        <p><code>address</code> - адрес счета назначения</p>
        <p><code>amount</code> - кол-во монет для перевода в валюте счета назначения</p>

        <h2 class="page-header">Варианты указания счета пользователя</h2>
        <ul>
            <li>1. Полный адрес счета</li>
            <li>2. Номер карты</li>
            <li>3. Телефон (начиная с 7 и без лишних знаков)</li>
            <li>4. Почта регистрации</li>
        </ul>



        <p>При 3 и 4 варианте счет выбирается тот который по умолчанию клиент указал</p>

        <h2 class="page-header">Счет клиента по умолчанию</h2>

        <p class="alert alert-danger">(Раздел в разработке)</p>
        <p>В настройках можно указать. Идентификатором счета по умолчанию является поле <code>is_default</code> в таблице <code>user_bill</code>.</p>
        <p>0 - счет обычый. 1 - счет по умолчанию.</p>
        <p>Естественно что для каждой валюты может быть только один счет по умолчанию.</p>
        <p>При создании кошелька он сам назначается по умолчанию.</p>


        <h2>Ввод дополнительных данных</h2>

        <p>В поле <code>data</code> заносится дополнительная информаци об оппонентах стелки
        в формате JSON</p>
        <p>Например если это один оппонент то {user_id:15,billing_id:65}</p>
        <p>Если это несколько участников сделки, то есть от одного многим то так</p>

        <pre>
        {
        destionations: [
        {user_id:15,billing_id:65},
        {user_id:15,billing_id:65},
        {user_id:15,billing_id:65}
        ]
        }
        ```

        # Варианты перевода

        Если перевод простой
        ```
        $billing->pay(['address' => $amount]);
        ```
        или
        ```
        $billing->pay([
        'address' => [
        'amount'  => $amount,
        'comment' => '',
        ]
        ]
        );
        ```
        или
        ```
        $billing->pay([
        'address' => [
        'amount'  => $amount,
        'comment' => [
        'to'          => '',
        'from'        => '',
        'transaction' => '',
        ],
        ]
        ]
        );
        ```

        Если перевод сложный
        ```
        $billing->pay(
        [
        'comment'     => '', // комметарий для транзакции кошелька из которой производится перевод
        'transaction' => [
        'address' => [
        'amount'  => $amount,
        'comment' => '', // комметарий для транзакции кошелька в которой производится перевод
        ]
        ]
        ]
        );
        ```

        или
        ```
        $billing->pay([
        'address1' => $amount1,
        'address3' => $amount2,
        'address4' => $amount3,
        'address5' => $amount4,
        ]);
        </pre>



    </div>
</div>



