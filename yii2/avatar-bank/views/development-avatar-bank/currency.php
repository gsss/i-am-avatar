<?php
use common\services\UsersInCache;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;

/* @var $this yii\web\View */

$this->title = 'Валюты';



?>
<div class="container">
    <div class="col-lg-12">
        <h1 class="page-header text-center"><?= Html::encode($this->title) ?></h1>

        <p>Все валюты хранятся в таблице <code>currency</code></p>

        <p>Обновление курсов валют происходит раз в день в 03:00. Там же происходит обновление графика биткойна и мирового рынка</p>
        <pre>php yii money-rate/update2</pre>

        <p>В таблице <code>currency</code> есть поле <code>is_view</code>. Предполагалось его использовать в переводе денег в модальном окне, выбираются только те у кого = 1. Сейчас уже оно не соответствует своему названия плюс появилось много доп потребностей</p>
        <p>Поэтому ввожу дополнительные поля </p>
        <p><code>is_merchant</code> - отображается в мерчанте</p>
        <p><code>is_convert</code> - отображается в конвертации валют</p>

        <p><code>is_avr_wallet</code> - можно ли создать кошелек этой валюты на AvatarNetwork. 0 - нет, 1 - да</p>

        <p><code>is_view2</code> - отображается для вывода currency_view и для задания в пересылке монет.</p>

        <h1 class="page-header">Обновление валют</h1>
        <p><code>https://api.coinmarketcap.com/v1/ticker/</code> - Coin Market Cap API</p>
        <pre>{
    "id": "bancor",
    "name": "Bancor",
    "symbol": "BNT",
    "rank": "100",
    "price_usd": "7.06274",
    "price_btc": "0.00046566",
    "24h_volume_usd": "16444000.0",
    "market_cap_usd": "287968184.0",
    "available_supply": "40772871.0",
    "total_supply": "79384422.0",
    "max_supply": null,
    "percent_change_1h": "-0.86",
    "percent_change_24h": "9.77",
    "percent_change_7d": "35.78",
    "last_updated": "1515119651"
}</pre>
        <?= \avatar\services\Params::widget([
            'params' => [
                [
                    'name' => 'id',
                    'type' => 'int',
                    'isRequired' => true,
                    'description' => 'Идентификатор',
                ],
                [
                    'name' => 'name',
                    'type' => 'string',
                    'isRequired' => true,
                    'description' => 'Наименование на ангийском',
                ],
                [
                    'name' => 'symbol',
                    'type' => 'string',
                    'isRequired' => true,
                    'description' => 'Кодовое обозначение (ISO?)',
                ],
                [
                    'name' => 'rank',
                    'type' => 'int',
                    'isRequired' => true,
                    'description' => 'Позиция в рейтинге',
                ],
                [
                    'name' => 'price_usd',
                    'type' => 'double',
                    'isRequired' => true,
                    'description' => 'Рыночная цена в USD',
                ],
                [
                    'name' => 'price_btc',
                    'type' => 'double',
                    'isRequired' => true,
                    'description' => 'Рыночная цена в BTC',
                ],
                [
                    'name' => '24h_volume_usd',
                    'type' => 'double',
                    'isRequired' => true,
                    'description' => 'Объемы продаж в USD за 24 часа',
                ],
                [
                    'name' => 'market_cap_usd',
                    'type' => 'double',
                    'isRequired' => true,
                    'description' => 'Общая капитализация в USD',
                ],
                [
                    'name' => 'available_supply',
                    'type' => 'double',
                    'isRequired' => true,
                    'description' => 'Всего монет в обращении',
                ],
                [
                    'name' => 'total_supply',
                    'type' => 'double',
                    'isRequired' => true,
                    'description' => 'Лимит монет',
                ],
                [
                    'name' => 'max_supply',
                    'type' => 'double | null',
                    'isRequired' => true,
                    'description' => 'Максимальный лимит монет',
                ],
                [
                    'name' => 'percent_change_1h',
                    'type' => 'double',
                    'isRequired' => true,
                    'description' => 'Изменение в процентах за час',
                ],
                [
                    'name' => 'percent_change_24h',
                    'type' => 'double',
                    'isRequired' => true,
                    'description' => 'Изменение в процентах за день',
                ],
                [
                    'name' => 'percent_change_7d',
                    'type' => 'double',
                    'isRequired' => true,
                    'description' => 'Изменение в процентах за неделю',
                ],
            ]
        ]) ?>
        <p>Если добавляется переменная <code>convert</code> - Добавляет три переменных</p>
        <ul>
            <li><code>24h_volume_</code> + <code>[symbol]</code> - Объемы продаж в указанной валюте</li>
            <li><code>market_cap_</code> + <code>[symbol]</code> - Общая капитализация в указанной валюте</li>
            <li><code>price_</code> + <code>[symbol]</code> - Цена валюты в указанной валюте</li>
        </ul>
        <p>Возможные значения:</p>
        <?php $arr = ["AUD", "BRL", "CAD", "CHF", "CLP", "CNY", "CZK", "DKK", "EUR", "GBP", "HKD", "HUF", "IDR", "ILS", "INR", "JPY", "KRW", "MXN", "MYR", "NOK", "NZD", "PHP", "PKR", "PLN", "RUB", "SEK", "SGD", "THB", "TRY", "TWD", "ZAR",]; ?>
        <p>
            <?php foreach ($arr as $code) { ?>
                <?= $arr2[] = Html::tag('code', $code); ?>
            <?php } ?>
            <?= join(', ', $arr2) ?>
        </p>
        <p>Всего записей выдается по 100 шт, если надо получить другие валюты, то можно указать параметр <code>start</code> и от этой позиции в рейтинге начнется список. А переменной <code>limit</code> можно задать количество выдаваемых элементов в списке.</p>

        <h2 class="page-header">Tokens</h2>
        <p>Чтобы определять токены Эфира нужно их как то отличать. Как?</p>
        <p>Решение. Если в таблице <code>token</code> содержится запись с валютой <code>currency.id</code> то эта валюта является токеном эфира.</p>



    </div>
</div>



