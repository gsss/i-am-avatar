<?php
use common\services\UsersInCache;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;

/* @var $this yii\web\View */

$this->title = 'Мониторинг системы';

?>
<div class="container">
    <div class="col-lg-12">
        <h1 class="page-header text-center"><?= Html::encode($this->title) ?></h1>
        <p class="alert alert-danger">В разработке</p>

        <h2 class="page-header">Мониторинг серверов</h2>
        <p>Какие могут быть?</p>
        <p>Сейчас это PROD и TEST</p>
        <p>Как будет реализовано?</p>
        <p>Раз в 10 мин будет происходить пинг сервера. Пинг значит запрашивать адрес с сервера  и получение кода 200.</p>
        <p>Данные будут записываться в таблицу <code>statistic_ping_server_prod</code> и <code>statistic_ping_server_test</code></p>
        <?= \common\services\documentation\DbTable::widget([
            'name'    => 'statistic_ping_server_prod',
            'columns' => [
                [
                    'name'        => 'id',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Идентификатор записи',
                ],
                [
                    'name'        => 'time',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'момент записи результата пинга',
                ],
                [
                    'name'        => 'status',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'результат пинга 0 - не отвечает, 1 - ответ успешный',
                ],
            ],
        ])?>

        <p><code>\common\models\statistic\PingProd</code> - модель</p>
        <p>На мониторинге будет отображаться активен ли сервер последние 20 мин. и небольшой график активности</p>

    </div>
</div>



