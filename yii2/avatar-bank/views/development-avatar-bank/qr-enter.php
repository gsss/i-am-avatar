<?php
use common\services\UsersInCache;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;

/* @var $this yii\web\View */

$this->title = 'Активация Карты Аватара';

?>
<div class="container">
    <div class="col-lg-12">
        <h1 class="page-header text-center"><?= Html::encode($this->title) ?></h1>

        <p>
            <a href="https://drive.google.com/file/d/0B73bzLewJ2HMaTZMSXY1WVgzc3M/view?usp=sharing" target="_blank">
                <img src="/images/controller/development-avatar-bank/qr-enter/qr-enter.png" width="100%" style="max-width: 1700px;">
            </a>
        </p>
        <p>Для того чтобы активировать свою карту Аватара нужно зайти на страницу <code>https://www.avatar-bank.com/qr/enter</code>.</p>
        <p>На ней вбить код и нажать "активировать".</p>
        <p>При этом код ищется в таблице <code>bill_codes</code> и если удачно найден то человек переходит к регистрации.</p>
        <p>После первой активации код помечается как использованный и не применяется более, но остается в базе данных.</p>
        <h2 class="page-header">Таблица bill_codes</h2>
        <?= \common\services\documentation\DbTable::widget([
            'name' => 'bill_codes',
            'params' => [
                [
                    'name'        => 'id',
                    'isRequired'  => true,
                    'description' => 'Идентификатор',
                    'type'        => 'int',
                ],
                [
                    'name'        => 'code',
                    'isRequired'  => true,
                    'description' => 'Код',
                    'type'        => 'varchar',
                ],
                [
                    'name'        => 'is_used',
                    'isRequired'  => true,
                    'description' => 'Использован или нет? 0 - не использован, 1 - использован',
                    'type'        => 'int',
                ],
                [
                    'name'        => 'created_at',
                    'isRequired'  => true,
                    'description' => 'Момент создания кода',
                    'type'        => 'int',
                ],
            ]
        ]) ?>

        <h3 class="page-header">Каким образом генерируются коды?</h3>
        <p>Коды генерируются в Админке с распечаткой кодов.</p>
        <p>Для доступа в админку используется роль <code>role_qr-code-admin</code> и контроллер <code>AdminQrCodeController</code>.</p>
        <p>Единожды использовав код ему ставится статус = 1. и он в дальнейшем считается "анулироанным".</p>
        <p>Создаются через AJAX по одному.</p>

        <h3 class="page-header">Маршрут активации</h3>
        <ul>
            <li>Пользователь аходит на страницу <code>/qr/enter</code></li>
            <li>Пользователь заполнет все данные</li>
            <li>Нажимает "Активировать"</li>
            <li>Попадаю в \avatar\models\forms\QrEnter::activate.</li>
            <li>Создаю запись в таблице <code>user_enter</code>. Там сохраняеся код</li>
            <li>Ему на почту высылается письмо с ссылкой активации на адрес <code>/auth/qr-enter-activation?code=888</code></li>
            <li>Подтверждает почту</li>
            <li>Счет привязывается</li>
            <li>Происходит логин</li>
        </ul>


        <h3 class="page-header">Генерация скидочных купонов</h3>
        <p>Так как создание купонов происсходит сразу совместно с созданием Кошельков для Новоиспеченных Аватаров на которое нужно
            затратить некоторое время для отправки запроса к API то целесообразным будет сделать это в консоли пакетно.</p>
        <p>В админке по одному.</p>
        <p>В админке также есть возможность распечатать Лист созданных кодов</p>
        <p>Контроллер AdminQrEnterController</p>
        <p>Страница <code>/admin-qr-enter/index</code></p>

        <h3 class="page-header">Активация Карты</h3>
        <p>Делается на странице `/qr/enter`. Для этого нужно ввести код активации в поле формы
            и нажать `Активировать`.</p>
        <p>Эти коды создаюся заранее в админке и используются клентами когда активируют карту.</p>
        <p>Счета для этих карт уже заранее созданы, но еще не распределены клиентам. Поле user_id = null.</p>
        <p>После активации это поле заполняется тем кому он тепрь принадлежит.</p>
        <p>Все коды активации хранятся в таблице `bill_codes`.</p>
    </div>
</div>



