<?php
use common\services\UsersInCache;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;

/* @var $this yii\web\View */

$this->title = 'Рассылки';

?>
<div class="container">
    <div class="col-lg-12">
        <h1 class="page-header text-center"><?= Html::encode($this->title) ?></h1>

        <p>В этом разделе вы можете управлять своими подписками на рассылки.</p>
        <p>Если вы включите опции то вы будете получать рассылку на почту которую указали при регистрации. Если
            выключите опцию то не будете получать. По умолчанию эти опции включены после регистрации.</p>
        <p><img src="/images/controller/development-avatar-bank/subscribes/1.png" width="100%" class="thumbnail"></p>


        <p>Весь сервис состоит из следующих разделов: Подписка, Рассылка, Отписка</p>
        <p>Есть сервер для отправки писем: <code>http://service.galaxysss.com</code></p>

        <h2 class="page-header">Подписка</h2>
        <p>Поля отвечающие за рассылку:</p>
        <p><code>subscribe_is_news</code> int 0 - не рассылать, 1 - рассылать</p>
        <p><code>subscribe_is_blog</code> int 0 - не рассылать, 1 - рассылать</p>

        <h2 class="page-header">Рассылка</h2>
        <h2 class="page-header">Отписка</h2>

        <p>В письме которое отсылается пишется:</p>
        <pre>
&lt;p&gt;Чтобы отписаться от рассылки воспользуйтесь &lt;a href="{linkUnsubscribe}"&gt;ссылкой&lt;/a&gt;&lt;/p&gt;
</pre>

        <pre>
Рассылки организованы в виде очереди.
Поля отвечающие на какие подписки подписан пользователя хранятся в таблице `gs_users`.
- *subscribe_is_news* - подписка на Новости Планеты

За это отвечают следующие поля таблицы gs_users:
subscribe_is_news - новости Планеты
subscribe_is_site_update - обновления сайта

Для обновлений сайта есть таблица gs_site_update:
~~~
CREATE TABLE `gs_site_update` (
 `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
 `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
 `image` varchar(255) CHARACTER SET ascii COLLATE ascii_bin NOT NULL,
 `link` varchar(255) CHARACTER SET ascii COLLATE ascii_bin NOT NULL,
 `date_insert` datetime DEFAULT NULL,
 PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8
~~~

Таблица для сохранения писем для рассылки:
~~~
CREATE TABLE `gs_subscribe_mail_list` (
 `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
 `mail` varchar(255) CHARACTER SET ascii COLLATE ascii_bin NOT NULL,
 `text` TEXT NOT NULL,
 `html` TEXT NOT NULL,
 `date_insert` datetime DEFAULT NULL,
 PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8
~~~

Таблица для сохранения ручных рассылок:
~~~
CREATE TABLE `gs_subscribe_history` (
 `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
 `content` text NOT NULL,
 `date_insert` int(11) DEFAULT NULL,
 `subject` varchar(255) DEFAULT NULL,
 PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8
~~~

Для рассылки используются шаблоны:
`@app/mail/layouts/html/subscribe.php` - для HTML вида
`@app/mail/layouts/text/subscribe.php` - для текстового вида
Местоположение ссылки отписки в шаблоне обозначается ключом '{linkUnsubscribe}'

## Типы подписки

Можно подписаться в двух видах:
1. На каждое добавление статьи высылается письмо. Поле *subscribe_is_after_insert*
2. Рассылка происходит раз в неделю. Поле *subscribe_is_site_update*

## Отписаться от рассылки

Отписаться можно по ссылке:
```
/subscribe/unsubscribe
```

параметры:
- *mail* - string -  почта пользователя
- *type* - integer - тип рассылки `\app\services\Subscribe::TYPE_*`
- *hash* - string - контрольная сумма чтобы никто не мог отписать другого пользователя, зная его почту

## Технология рассылки

Сначала генерируются данные для рассылки в виде класса `\app\models\SubscribeItem`.
Он передается в функцию `\app\services\Subscribe::add()`, которая добавляет записи для рассылки в таблицу рассылки.
На этом этап добавления записи завершен.

Для рассылки подготовленных писем существует скрипт, который этим занимается: `\app\commands\SubscribeController::actionSend()`.
Он запускается из консоли командой: `./yii subscribe/send`. Он запускается раз в пять минут и отправляет по 10 писем за одно выполнение.


Каждую неделю в ПН 00:01 формируется письмо с отчетом о том, что было добавлено на Инструмент Вознесения через функцию \app\services\Subscribe::add().
А рассыльщик './yii subscribe/send' уже отправляет письма как обычно.

## Сделать подписку на рассылку

Если пользователь не имеет учетной записи или зашел через соц сеть в которой не было зарегистрировано email, то он может подписаться на рассылку указав email и свое имя. Для этого служит форма в подвале страницы.
После этого запрос отправляется на адрес AJAХ
~~~
/subscribe/mail
~~~
с параметрами:
- email - string - адрес для подписки
- name - string - имя подписки (не обязательно если пользователь уже в системе)

При этом если пользователь не авторизован то будет создан новый пользователь. И в любом случае будет отправлено письмо подтверждения.
После успешной отправки письма для подтверждения устанавливается COOKIE “subscribeIsStarted” = 1, которое говорит о том что форму подписки отображать не нужно. Таким образом она больше на компьютере пользователя появляться не будет.

## Сопутствующие классы

Есть интерфейс \app\models\SiteContentInterface. Предполагается что он предназначен для модели контента сайта. В нем есть функция которая генерирует данные для рассылки в виде объекта \app\models\SubscribeItem.

Пример применения:
```php
// ...
class Chenneling extends \cs\base\DbRecord implements SiteContentInterface
{
    // ...

    /**
    * @inheritdoc
    */
    public function getMailContent()
    {
        // ...
    }
}
```

## Рассылки обновлений сайта

При добавлении материала на сайт модератор-пользователь имеет возможность сделать рассылку о том что был добавлен материал.

Нажимая на эту кнопку вы сделаете рассылку.
Примечание: для объединений такая рассылка выполняется автоматически после одобрения модератором.


```
где
- unSubscribeUrl - string - префикс ссылки для отписки
- subject - string - тема письма
- mailList - array - мссив адресов куда отправлять письмо находящееся в параметре mail
- mail - объект письма которое нужно разослать. Поле html и text могут быть либо одно из них либо оба.
Если оба тогда письмо будет отправлено в мультиформе. В теле письма будет автоматически проводится подстановка адресата письма в место где прописано
{linkUnsubscribe} на следующие данные
```php
$urlUnSubscribe = Url::to(['subscribe/unsubscribe', 'mail' => $mail, 'type' => $s->type, 'hash' => \app\services\Subscribe::hashGenerate($mail, $s->type)], true);
```
   - html - string - строка которая содержит текст письма
   - text - string - строка текста письма

- attacheList - array - массив объектов которые являются фалами прикрепляемыми к письму
    - name - string - имя файла
    - content - string - содержимое файла закодированное в base64

- from - объект содержащий данные об отправителе который будет указан в отправляемых письмах в разделе отправитель
    - name - string - Имя
    - mail - string - электронный адрес

### GSSS.subscribe()

Функция GSSS.subscribe(selector, url, )

Навешивает обработчик на кнопку с селектором selector который отправляет запрос на создание рассылки на url
которая формируется через function(id) {return url;}

- @param selector  string
- @param url       function

### Рассылка писем физически

Происходит с сервера service.galaxysss.com.

Сама рассылка происходит через консольную команду
```
yii subscribe/send
```
которая рассылает письма по несколько шт раз в мин.
        </pre>

        <h3 class="page-header">Рассылка через внешний сервер</h3>

        <p><img src="/images/controller/development-avatar-bank/subscribe/subscribe_1.png" width="100%"></p>
        <p>! Внимание. Из этой картинки еще не реализовано предпоследний AJAX запрос</p>

        <p>Здесь есть две функции:</p>
        <p>- 1.	Добавление рассылки</p>
        <p>- 2.	Создание рассылки</p>

        <p>Добавление рассылки</p>
        <p>Этим занимается скрипт</p>
        <p>http://service.galaxysss.com/subscribe/add</p>
        <p>POST</p>
        <p>Вход:</p>
        <p>- key - string - ключ для API</p>
        <p>- data – string – строка JSON которая инкапсулирует все данные для рассылки</p>
        <pre>
{
	mailList: [
	    'dram1008@yandex.ru',
	    'god@galaxysss.ru',
	    // ...
	],
	subject: 'text',
	unSubscribeUrl: 'http://www.galaxysss.ru/subscribe/unsubscribe',
	type: 1,
	mail: {
	    html: '<html> ...',
	    text: 'Здравствуйте ...'
	},
	attacheList: [
	    {
	        name: 'text.txt',
	        content: 'sdg8884dfsgdsfre4864 ...'
	    },
	    // ...
	],
	from: {
	    name: 'Святослав',
	    mail: 'god@galaxysss.ru'
	}
}
</pre>

        <h3 class="page-header">Внешний сервис</h3>
        <p><code>https://api.unisender.com/ru/api</code></p>
        <p>Будет использоваться два списка рассылки. Они будут записываться в таблице <code>subscribe_list</code></p>
        <?= \common\services\documentation\DbTable::widget([
            'name'    => 'user_files_upload',
            'columns' => [
                [
                    'name'          => 'id',
                    'type'          => 'int',
                    'isRequired'    => true,
                    'description'   => 'Идентификатор списка',
                ],
                [
                    'name'          => 'name',
                    'type'          => 'varchar(100)',
                    'isRequired'    => true,
                    'description'   => 'Название списка',
                ],
            ]
        ])?>

        <h4 class="page-header">Переходный период</h4>
        <p>Для того чтобы поддерживать список в адекватном состоянии нужно при определенных событиях менять список.</p>
        <p>После подтверждения регистрации и после подтверждения активации карты. <code>\common\models\UserAvatar::activate</code></p>
        <p>После блокировки удалять из списков.</p>
        <p>После изменении пользователем вариантов подписки.</p>
        <p>При изменении Имени.</p>
        <p>При изменении Телефона.</p>

        <p>Для того чтобы не было глюков в переходном периоде нужно сначала подготовить и отладить методику изменения состояния подписки в трех вышеобозначенных событиях и после этого быстро импортировать существующий список и переключить на новый вид поддержки рассытки.</p>
        <p>Для этого ввожу доп поле в параменты приложения <code>isExternalSubscribeReady</code>. 0 - Еще не введена система, ведется разработка, 1 - введена и используется UniSender.</p>
        <p>Для начала нужно добавить две группы рассылки.</p>
        <p>11770401 - Блог</p>
        <p>11770421	- Новости</p>


    </div>
</div>



