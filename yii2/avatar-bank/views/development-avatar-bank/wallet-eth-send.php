<?php
use common\services\UsersInCache;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;

/* @var $this yii\web\View */

$this->title = 'Кошелек ETH. Отправка';

?>
<div class="container">
    <div class="col-lg-12">
        <h1 class="page-header text-center">
            <?= Html::encode($this->title) ?>
        </h1>

        <h2 class="page-header">
            Модальное окно "Отправка денег"
        </h2>

        <p><img src="/images/controller/development-avatar-bank/wallet-eth/send-modal.png" class="thumbnail"></p>
        <h3 class="page-header">
            Кнопка MAX
        </h3>
        <p>Вычисляет кол-во максимального эфира которые можно отправить со счета с учетом цены газа и его лимита.</p>
        <p>Формула для вычисления:</p>
        <pre>$max = $balanceWie - ($gasPrice * $gasLimit);</pre>
        <p>AJAX <code>/cabinet-bills-eth/get-max</code></p>
        <?= \avatar\services\Params::widget([
                'params' => [
                    [
                        'name'        => 'billing_id',
                        'type'        => 'int',
                        'isRequired'  => true,
                        'description' => 'Идентификатор счета',
                    ],
                    [
                        'name'        => 'gasLimit',
                        'type'        => 'int',
                        'isRequired'  => false,
                        'description' => 'Газ лимит',
                    ],
                    [
                        'name'        => 'gasPrice',
                        'type'        => 'int',
                        'isRequired'  => false,
                        'description' => 'Цена газа в Gwei',
                    ],
                ]
        ]) ?>
        <p>Выдает</p>
        <?= \avatar\services\Params::widget([
                'params' => [
                    [
                        'name'        => 'wei',
                        'type'        => 'int',
                        'isRequired'  => true,
                        'description' => 'Кол-во wei',
                    ],
                    [
                        'name'        => 'eth',
                        'type'        => 'int',
                        'isRequired'  => true,
                        'description' => 'Кол-во ETH',
                    ],
                ]
        ]) ?>

        <p>Может возникнуть ошибка</p>
        <p><img src="/images/controller/development-avatar-bank/wallet-eth/send-modalerror.png" class="thumbnail"></p>
        <p>При таком случае возвращается "У вас недостаточно Эфира чтобы оплатить комисиию"</p>
        <p>Ошибка, не сходятся цифры после деления в PHP</p>
        <p><img src="/images/controller/development-avatar-bank/wallet-eth/2018-02-17_03-07-55.png" class="thumbnail"></p>
        <p>Поэтому не используем деление в PHP для эфира</p>
        <p><img src="/images/controller/development-avatar-bank/wallet-eth/2018-02-17_03-08-23.png" class="thumbnail"></p>
        <p>Решения:</p>
        <p><a href="http://php.net/manual/ru/ref.bc.php" target="_blank">BC Math Функции</a></p>
        <p><a href="http://php.net/manual/ru/ref.gmp.php" target="_blank">GMP Функции</a></p>

        <h2 class="page-header">
            Отправка на карту
        </h2>
        <p>Если на карте нет кошелька эфира то он создается, при условии что у клиента установлена простая защита.</p>

    </div>
</div>



