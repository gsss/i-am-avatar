<?php
use common\services\UsersInCache;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;

/* @var $this yii\web\View */

$this->title = 'ETH Контракты';

?>
<div class="container">
    <div class="col-lg-12">
        <h1 class="page-header text-center"><?= Html::encode($this->title) ?></h1>
        <p class="alert alert-danger">В разработке</p>


        <?= \common\services\documentation\DbTable::widget([
            'name'    => 'contract',
            'columns' => [
                [
                    'name'        => 'id',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Идентификатор контракта',
                ],
                [
                    'name'        => 'address',
                    'type'        => 'varchar(128)',
                    'isRequired'  => true,
                    'description' => 'Адрес счета, все малые буквы',
                ],
                [
                    'name'        => 'image',
                    'type'        => 'varchar(255)',
                    'description' => 'Картинка контракта',
                ],
                [
                    'name'        => 'name',
                    'type'        => 'varchar(200)',
                    'description' => 'Название контракта',
                ],
                [
                    'name'        => 'abi',
                    'type'        => 'text',
                    'isRequired'  => true,
                    'description' => 'Интерфейс контракта',
                ],
            ]
        ]) ?>

        <?= \common\services\documentation\DbTable::widget([
            'name'    => 'user_contract',
            'columns' => [
                [
                    'name'        => 'id',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Идентификатор строки',
                ],
                [
                    'name'        => 'user_id',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Идентификатор пользователя кому принадлежит контракт',
                ],
                [
                    'name'        => 'contract_id',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Идентификатор контракта',
                ],
                [
                    'name'        => 'name',
                    'type'        => 'varchar(255)',
                    'description' => 'Название контракта с точки зрения пользователя',
                ],
            ]
        ]) ?>

        <p>Контроллер <code>/cabinet-bills-eth/contract</code></p>
        <p>Таблица <code>contracts</code> содержит все возможные контракты и соответственно все адреса уникальны. Эта таблица заполняется в админке.</p>
        <p>Таблица <code>user_contracts</code> содержит ссылки на контракты <code>contracts</code> и собственные названия этих контрактов. Эту таблицу ведет пользователь.</p>

        <p>Я нахожусь на странице кошелька и нажимаю вызвать контракт.</p>
        <p>Я попадаю на /cabinet-bills-eth/contract-list. Я вижу несколько список свих контрактов и добавить, кнупку "вызвать контракт", удалить.</p>



    </div>
</div>



