<?php
use common\services\UsersInCache;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;

/* @var $this yii\web\View */

$this->title = 'Geth - интерфейс командной строки go-ethereum';

?>
<div class="container">
    <div class="col-lg-12">
        <div>
            <p>В начале:</p>
            <blockquote>Этот перевод Geth, прежде всего создавал для себя, для предварительного, так сказать, изучения. подумалось, что перевод будет полезен и другим людям, желающим начать свой путь в сторону Etherium. В коменты призываю всех, кто захочет указать на неточности (а так же есть кое какие явные пробелы в переводе). Ссылки на оригиналы - внизу статьи. Спасибо.</blockquote>
            <p><code>$ Geth help</code></p>
            <p><strong>ИМЯ:</strong></p>
            <p><code>&nbsp;&nbsp;&nbsp;Geth - интерфейс командной строки go-ethereum</code></p>
            <p><code>Копирайт 2013-2016 гг. Авторы: go-ethereum</code></p>
            <p><br></p>
            <p><strong>ПРИМЕНЕНИЕ (USAGE):</strong></p>
            <p>&nbsp;&nbsp;&nbsp;<code>geth [опции] КОМАНДЫ &nbsp;[параметры команды] [аргументы ...]</code></p>
            <p>&nbsp;&nbsp;&nbsp;<code>geth [options] command [command options] [arguments...]</code></p>
            <p>&nbsp;&nbsp;&nbsp;</p>
            <p><strong>ВЕРСИЯ (VERSION):</strong></p>
            <p>&nbsp;&nbsp;&nbsp;1.5.5, нестабильная (1.5.5-unstable)</p>
            <p>&nbsp;&nbsp;&nbsp;</p>
            <p><strong>КОМАНДЫ (COMMANDS):</strong></p>
            <p><code>&nbsp;&nbsp;&nbsp;init &nbsp; </code>Bootstrap и инициализировать новый генезис блок (Bootstrap and initialize a new genesis block)</p>
            <p><code>&nbsp;&nbsp;&nbsp;import &nbsp; </code>Импортировать блокчейн-файл (Import a blockchain file)</p>
            <p><code>&nbsp;&nbsp;&nbsp;export &nbsp; </code>Экспорт блокчейн в файл (Export blockchain into file)</p>
            <p><code>&nbsp;&nbsp;&nbsp;upgradedb &nbsp; </code>&nbsp;&nbsp;&nbsp;Обновление базы данных блоков (блокчейна) (Upgrade chainblock database)</p>
            <p><code>&nbsp;&nbsp;&nbsp;removedb &nbsp;&nbsp; &nbsp; </code>Удалить блокчейн- и статичную базу данных (Remove blockchain and state databases)</p>
            <p><code>&nbsp;&nbsp;&nbsp;dump &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </code>Дамп определенного блока из хранилища (Dump a specific block from storage)</p>
            <p><code>&nbsp;&nbsp;&nbsp;monitor &nbsp;&nbsp;&nbsp; &nbsp; </code>Мониторинг и визуализация метрики ноды (Monitor and visualize node metrics)</p>
            <p><code>&nbsp;&nbsp;&nbsp;account &nbsp; </code>&nbsp;&nbsp;&nbsp;Управление учетными записями/аккаунтами (Manage accounts)</p>
            <p><code>&nbsp;&nbsp;&nbsp;wallet &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </code>Управление кошельками Эфириума (Manage Ethereum presale wallets)</p>
            <p><code>&nbsp;&nbsp;&nbsp;console &nbsp; </code>Эта опция запускает консоль разработчика. Она поддерживает самый обычный JS и ряд встроенных функций для работы с Ethereum, вот простой <a href="https://habrahabr.ru/post/312008/" rel="nofollow noopener">пример</a> &nbsp;(пункт — Поднимаем ноду) (Start an interactive JavaScript environment).&nbsp;</p>
            <p><code>&nbsp;&nbsp;&nbsp;attach &nbsp;&nbsp;&nbsp;&nbsp; </code>подключиться к Узлу (Ноде) из консоли JavaScript (Start an interactive JavaScript environment (connect to node)</p>
            <p><code>&nbsp;&nbsp;&nbsp;js </code>Выполнить указанные файлы JavaScript (Execute the specified JavaScript files)</p>
            <p><code>&nbsp;&nbsp;&nbsp;makedag </code>Создать ethash DAG (для тестирования) (Generate ethash DAG (for testing))</p>
            <p><code>&nbsp;&nbsp;&nbsp;version &nbsp;&nbsp;&nbsp; </code>Номер версии (Print version numbers)</p>
            <p><code>&nbsp;&nbsp;&nbsp;license &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </code>Отобразить информацию о лицензии (Display license information)</p>
            <p><code>&nbsp;&nbsp;&nbsp;help, h </code>Показывает список команд или помощь для определенной команды (Shows a list of commands or help for one command)</p>
            <p>&nbsp;&nbsp;&nbsp;</p>
            <p><strong>ВАРИАНТЫ ETHEREUM (ETHEREUM OPTIONS):</strong></p>
            <p><code>&nbsp;&nbsp;--datadir "/home/tron/.ethereum" </code>Каталог расположения баз данных и ключей (Data directory for the databases and keystore)</p>
            <p><code>&nbsp;&nbsp;--keystore </code>Каталог расположения ключей (по умолчанию = внутри datadir) (Directory for the keystore (default = inside the datadir))</p>
            <p><code>&nbsp;&nbsp;--networkid value </code>Сетевой идентификатор. Целое число, 0 = Olympic (отключен), 1 = Frontier, 2 = Morden (отключен), 3 = Ropsten. По умолчанию: 1. &nbsp;(Network identifier (integer, 0=Olympic (disused), 1=Frontier, 2=Morden (disused), 3=Ropsten) (default: 1)</p>
            <p><code>&nbsp;&nbsp;--olympic </code>Олимпик сеть: предварительно сконфигурированная тестовая сеть&nbsp;(Olympic network: pre-configured pre-release test network)</p>
            <p><code>&nbsp;&nbsp;--testnet </code>Ропстен сеть: предварительно сконфигурированная тестовая сеть (Ropsten network: pre-configured test network)</p>
            <p><code>&nbsp;&nbsp;--dev </code>Режим разработчика: предварительно настроенная частная сеть с несколькими флагами отладки. Запускает geth в режиме приватного блокчейна, то есть не синхронизирет основную / тестовую ветку. Вместо этого вы получаете стерильную цепочку без единого блока. Это самый удобный вариант в плане разработки, так как, например, майнинг блока занимает несколько секунд и нет никакой нагрузки на сеть или диск. (Developer mode: pre-configured private network with several debugging flags)</p>
            <p><code>&nbsp;&nbsp;--identity value </code>Собственное имя Узла/Ноды (Custom node name)</p>
            <p><code>&nbsp;&nbsp;--fast </code>Включить быструю синхронизацию (Enable fast syncing through state downloads)</p>
            <p><code>&nbsp;&nbsp;--light </code>Включить режим легкого клиента (Enable light client mode)</p>
            <p><code>&nbsp;&nbsp;--lightserv value</code> Максимальный процент времени, разрешенного для обслуживания LES-запросов &nbsp;от 0 до 90. По умолчанию: 0 (Maximum percentage of time allowed for serving LES requests (0-90) (default: 0)</p>
            <p><code>&nbsp;&nbsp;--lightpeers value </code>Максимальное количество пиров LES-клиентов, &nbsp;по умолчанию: 20. (Maximum number of LES client peers (default: 20)</p>
            <p><code>&nbsp;&nbsp;--lightkdf </code>Сокращение использования оперативной памяти и процессора при извлечении ключей при некотором расходе KDF (Reduce key-derivation RAM &amp; CPU usage at some expense of KDF strength)</p>
            <p><br></p>
            <p><strong>ЭКСПЛУАТАЦИОННЫЕ НАСТРОЙКИ (PERFORMANCE TUNING OPTIONS):</strong></p>
            <p><code>&nbsp;&nbsp;--cache value </code>Размер кэша, выделяемых для внутреннего кэширования, в мегабайтах, минимум - 16Mb на базу, по умолчанию: 128. (Megabytes of memory allocated to internal caching (min 16MB / database forced) (default: 128)</p>
            <p><code>&nbsp;&nbsp;--trie-cache-gens value </code>Количество узлов (поколений Нод) для хранения в памяти. По умолчанию: 120. (Number of trie node generations to keep in memory (default: 120)</p>
            <p><br></p>
            <p><strong>НАСТРОЙКИ АККАУНТА (ACCOUNT OPTIONS):</strong></p>
            <p><code>&nbsp;&nbsp;--unlock value </code>Список аккаунтов для разблокировки (через запятую) (Comma separated list of accounts to unlock)</p>
            <p><code>&nbsp;&nbsp;--password value </code>Файл с паролем для деактивации ввода пароля (Password file to use for non-inteactive password input)</p>
            <p><br></p>
            <p><strong>API И ОПЦИИ КОНСОЛИ &nbsp;(API AND CONSOLE OPTIONS):</strong></p>
            <p><code>&nbsp;&nbsp;--rpc </code>Включает RPC-HTTP сервер. По сути это API к вашей ноде — через него сторонние приложения, вроде кошельков или IDE, смогут работать с блокчейном: загружать контракты, отправлять транзакции и так далее. По дефолту запускается на localhost:8545, можно изменить эти параметры с помощью --rpcaddr и --rpcport соответственно. (Enable the HTTP-RPC server)</p>
            <p><code>&nbsp;&nbsp;--rpcaddr value</code> Интерфейс прослушивания сервера HTTP-RPC, по умолчанию - localhost - устанавливает что-то вроде прав доступа для приложений, подключенных к RPC серверу. Например, если вы не укажете "miner", то, подключив к ноде кошелек и запустив майнер, вы получите ошибку. В примере я указал все возможные права, подробнее можете почитать <a href="https://github.com/ethereum/go-ethereum/wiki/Management-APIs" rel="nofollow noopener">здесь</a>. (HTTP-RPC server listening interface (default: "localhost")</p>
            <p><code>&nbsp;&nbsp;--ws </code>Включает WS-RPC сервер (Enable the WS-RPC server)</p>
            <p><code>&nbsp;&nbsp;--wsaddr value </code>Сервер WS-RPC, по умолчанию localhost (WS-RPC server listening interface (default: "localhost")</p>
            <p><code>&nbsp;&nbsp;--wsport value </code>значение порта для сервера WS-RPC, по умолчанию: 8546 (WS-RPC server listening port (default: 8546)</p>
            <p><code>&nbsp;&nbsp;--wsapi value </code>API-интерфейс wsapi, для сервера WS-RPC, по умолчанию: «eth, net, web3» (API's offered over the WS-RPC interface (default: "eth,net,web3")</p>
            <p><code>&nbsp;&nbsp;--wsorigins value </code>Протокол, домен и порт вебсокета откуда отправлен запрос (Origins from which to accept websockets requests)</p>
            <p><code>&nbsp;&nbsp;--ipcdisable </code>Отключает сервер IPC-RPC (Disable the IPC-RPC server)</p>
            <p><code>&nbsp;&nbsp;--ipcapi value </code>API-интерфейсы, предлагаемые через интерфейс IPC-RPC. По умолчанию: «admin, debug, eth, miner, net, personal, shh, txpool, web3» (APIs offered over the IPC-RPC interface (default: "admin,debug,eth,miner,net,personal,shh,txpool,web3")&nbsp;</p>
            <p><code>&nbsp;&nbsp;--ipcpath "geth.ipc" </code>Имя файла для IPC сокета/pipe внутри datadir, явные пути выходят из него. (Filename for IPC socket/pipe within the datadir (explicit paths escape it) &nbsp;</p>
            <p><code>&nbsp;&nbsp;--rpccorsdomain value </code>Список доменов, через запятую, из которых можно принимать запросы в браузере. (Comma separated list of domains from which to accept cross origin requests (browser enforced) &nbsp;</p>
            <p><code>&nbsp;&nbsp;--jspath loadScript </code>Корневой путь JavaScript для loadScript. По умолчанию: "." (JavaScript root path for loadScript (default: ".")</p>
            <p><code>&nbsp;&nbsp;--exec value </code>Выполнить код JavaScript (только в сочетании с консолью) (Execute JavaScript statement (only in combination with console/attach)&nbsp;</p>
            <p><code>&nbsp;&nbsp;--preload value </code>Список файлов JavaScript, через запятую, для предварительной загрузки в консоль. (Comma separated list of JavaScript files to preload into the console)</p>
            <p><strong>&nbsp;&nbsp;</strong></p>
            <p><strong>СЕТЕВЫЕ ОПЦИИ (NETWORKING OPTIONS):</strong></p>
            <p><code>&nbsp;&nbsp;--bootnodes value. </code>УРЛ'Ы &nbsp;для загрузки P2P, через запятую (Comma separated enode URLs for P2P discovery bootstrap)</p>
            <p><code>&nbsp;&nbsp;--port value </code>Сетевой порт прослушивания, по умолчанию: 30303 (Network listening port (default: 30303)</p>
            <p><code>&nbsp;&nbsp;--maxpeers value </code>Максимальное количество пиров. Сеть отключена, если установлено значение 0. Значение &nbsp;по умолчанию - 25. (Maximum number of network peers (network disabled if set to 0) (default: 25))</p>
            <p><code>&nbsp;&nbsp;--maxpendpeers value </code>Максимальное число ожидающих попыток подключения. По умолчанию используется значение 0. (Maximum number of pending connection attempts (defaults used if set to 0)</p>
            <p><code>&nbsp;&nbsp;--nat value </code>Механизм отображения NAT-порта: any | none | upnp | pmp | extip: . По умолчанию: any (любой). (NAT port mapping mechanism (any|none|upnp|pmp|extip:ny</p>
            <p><code>&nbsp;&nbsp;--nodiscover </code>Отключает механизм обнаружения пиров. Переход на ручное добавление пиров. (Disables the peer discovery mechanism (manual peer addition)</p>
            <p><code>&nbsp;&nbsp;--v5disc </code>Включает экспериментальный механизм RLPx V5 (Topic Discovery) (Enables the experimental RLPx V5 (Topic Discovery) mechanism)</p>
            <p><code>&nbsp;&nbsp;--nodekey value </code>файл с ключами узла P2P (P2P node key file)</p>
            <p><code>&nbsp;&nbsp;--nodekeyhex value </code>hex-ключ P2P узла, для тестирования (P2P node key as hex (for testing)</p>
            <p>&nbsp;&nbsp;</p>
            <p><strong>ОПЦИИ МАЙНИНГА (MINER OPTIONS) :</strong></p>
            <p><code>&nbsp;&nbsp;--mine </code>Включает режим майнига (Enable mining)</p>
            <p><code>&nbsp;&nbsp; --minerthreads value </code>Количество потоков ЦП, используемых для майнига, по умолчанию: 8 (Number of CPU threads to use for mining (default: 8)</p>
            <p><code>&nbsp;&nbsp; --autodag </code>Включает автоматическую предварительную генерацию DAG (Enable automatic DAG pregeneration)</p>
            <p><code>&nbsp;&nbsp; --etherbase value. </code>Публичный адрес для вознаграждений за блокировку блоков. По умолчанию - первая созданная учетная запись, или - 0. (Public address for block mining rewards (default = first account created) (default: "0")</p>
            <p><code>&nbsp;&nbsp; -targetgaslimit value </code>Устанавливает лимит Газа для блока при майнинге. По умолчанию: 4712388. (Target gas limit sets the artificial target gas floor for the blocks to mine (default: "4712388")</p>
            <p><code>&nbsp;&nbsp; --gasprice value </code>Устанавливает минимальную стоимость Газа, которую можно принять для добычи транзакций. По умолчанию: 20000000000. (Minimal gas price to accept for mining a transactions (default: "20000000000")</p>
            <p><code>&nbsp;&nbsp; --extradata value </code>Блокировать дополнительные данные, установленные майнером. По умолчанию = версия клиента. (Block extra data set by the miner (default = client version)</p>
            <p><br></p>
            <p><strong>ОПЦИИ ЦЕНООБРЕЗОВАНИЯ ГАЗА &nbsp;(GAS PRICE ORACLE OPTIONS):</strong></p>
            <p><code>&nbsp;&nbsp;--gpomin value </code>Устанавливает минимальную стоимость газа. По умолчанию: 20000000000. (Minimum suggested gas price (default: "20000000000")</p>
            <p><code>&nbsp;&nbsp;--gpomax value &nbsp; </code>Устанавливает максимальную стоимость газа. По умолчанию: 500000000000. (Maximum suggested gas price (default: "500000000000")</p>
            <p><code>&nbsp;&nbsp;--gpofull value </code>Полный порог блока для расчета цены Газа (%). По умолчанию: 80 (Full block threshold for gas price calculation (%) (default: 80)</p>
            <p><code>&nbsp;&nbsp;--gpobasedown value </code>Предлагаемый коэффициент снижения стоимости Газа (1/1000) (по умолчанию: 10). (Suggested gas price base step down ratio (1/1000) (default: 10)</p>
            <p><code>&nbsp;&nbsp;--gpobaseup value </code>Предлагаемый коэффициент повышения стоимости Газа (1/1000) (по умолчанию: 100). (Suggested gas price base step up ratio (1/1000) (default: 100)</p>
            <p><code>&nbsp;&nbsp;--gpobasecf value </code>Рекомендуемый коэффициент коррекции цен на газ (%) (по умолчанию: 110). Suggested gas price base correction factor (%) (default: 110)</p>
            <p>&nbsp;&nbsp;</p>
            <p><strong>ОПЦИИ ВИРУТАЛЬНОЙ МАШИНЫ (VIRTUAL MACHINE OPTION):</strong></p>
            <p><code>&nbsp;&nbsp;--jitvm </code>Включает JIT VM (Enable the JIT VM)</p>
            <p><code>&nbsp;&nbsp;--forcejit </code>Принудительное использование JIT VM (Force the JIT VM to take precedence)</p>
            <p><code>&nbsp;&nbsp;--jitcache value </code>Количество кэшированных программ JIT VM. По умолчанию: 64. (Amount of cached JIT VM programs (default: 64)</p>
            <p>&nbsp;&nbsp;</p>
            <p><strong>ОПЦИИ ОТЛАДКИ И ЛОГОВ (LOGGING AND DEBUGGING OPTIONS):</strong></p>
            <p><code>&nbsp;&nbsp;--ethstats value </code>URL-адрес отчета службы ethstats (nodename:secret@host:port) (Reporting URL of a ethstats service (nodename:secret@host:port))</p>
            <p><code>&nbsp;&nbsp;--metrics </code>Включает сбор &nbsp;и репорт отчетности/логов &nbsp; (Enable metrics collection and reporting)</p>
            <p><code>&nbsp;&nbsp;--fakepow </code>Отключает верификацию proof-of-work (Disables proof-of-work verification)</p>
            <p><code>&nbsp;&nbsp;--verbosity value &nbsp;</code>Условия логгирования: 0=тихий, 1=ошибки, 2=предупреждения, 3=информация, 4=ядро, 5=отладка, 6=детальный. По умолчанию: 3. (Logging verbosity: 0=silent, 1=error, 2=warn, 3=info, 4=core, 5=debug, 6=detail (default: 3)</p>
            <p><code>&nbsp;&nbsp;--vmodule value </code>Per-module verbosity: через запятую =, например, eth/*=6,p2p=5. (Per-module verbosity: comma-separated list of = (e.g. eth/*=6,p2p=5)</p>
            <p><code>&nbsp;&nbsp;--backtrace value </code>Запросить трассировку стека в конкретном протоколировании (например, "block.go: 271"). По умолчанию: 0. (Request a stack trace at a specific logging statement (e.g. "block.go:271") (default: :0)</p>
            <p><code>&nbsp;&nbsp;--pprof </code>Включает HTTP-сервер pprof (Enable the pprof HTTP server)</p>
            <p><code>&nbsp;&nbsp;--pprofaddr value </code>Интерфейс pprof HTTP-сервера, по умолчанию: 127.0.0.1. (pprof HTTP server listening interface (default: "127.0.0.1")</p>
            <p><code>&nbsp;&nbsp;--pprofport value </code>Порт pprof HTTP-сервера, по умолчанию: 6060 (pprof HTTP server listening port (default: 6060)</p>
            <p><code>&nbsp;&nbsp;--memprofilerate value </code>Значение для памяти, по умолчанию: 524288. &nbsp;(Turn on memory profiling with the given rate (default: 524288)</p>
            <p><code>&nbsp;&nbsp;--blockprofilerate value </code>Значение для блока. По умолчанию: 0. (Turn on block profiling with the given rate (default: 0)</p>
            <p><code>&nbsp;&nbsp;--cpuprofile value </code>Записать значение процессора в файл (Write CPU profile to the given file)</p>
            <p><code>&nbsp;&nbsp;--trace value </code>Запись трассировки выполнения в файл (Write execution trace to the given file)</p>
            <p>&nbsp;&nbsp;</p>
            <p><strong>ЭКСПЕРИМЕНТАЛЬНЫЕ ОПЦИИ (EXPERIMENTAL OPTIONS):</strong></p>
            <p><code>&nbsp;&nbsp;--shh </code>Включает Whisper (Enable Whisper)</p>
            <p><code>&nbsp;&nbsp;--ntspec </code>Включает уведомления NatSpec (Enable NatSpec confirmation notice)</p>
            <p>&nbsp;&nbsp;</p>
            <p><strong>ДОПОЛНИТЕЛЬНЫЕ ОПЦИИ (MISCELLANEOUS OPTIONS):</strong></p>
            <p><code>&nbsp;&nbsp;--solc value </code>Использовать команды компилятора Solidity. По умолчанию: «solc» (Solidity compiler command to be used (default: "solc"))</p>
            <p><code>&nbsp;&nbsp;--netrestrict value </code>Ограничивает &nbsp;связь с определенными IP-сетями (маски CIDR) (Restricts network communication to the given IP networks (CIDR masks)</p>
            <p><code>&nbsp;&nbsp;--help, -h </code>показать справку (show help)</p>
            <p><br></p>
            <p>Оригинал взят отсюда: <a href="https://github.com/ethereum/go-ethereum/wiki/Command-Line-Options" rel="nofollow noopener">Command Line Options</a> &nbsp;и немного отсюда: <a href="https://habrahabr.ru/post/327236/" rel="nofollow noopener">Dive into Ethereum</a> .&nbsp;</p></div>
    </div>
</div>



