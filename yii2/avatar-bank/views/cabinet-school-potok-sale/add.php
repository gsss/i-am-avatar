<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;
use app\models\UnionCategory;
use yii\db\Query;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \common\models\school\SchoolSale */
/* @var $kurs \common\models\school\Kurs */
/* @var $school \common\models\school\School */
/* @var $potok \common\models\school\Potok */

$this->title = 'Добавить продажу';

?>
<div class="container">
    <div class="col-lg-12">

    </div>

    <?php \avatar\widgets\SchoolMenu::begin(['school' => $school]) ?>
    <h1 class="page-header"><?= Html::encode($this->title) ?></h1>


    <?php if (!is_null($id = Yii::$app->session->getFlash('form'))) : ?>

        <div class="alert alert-success">
            Успешно добавлено.
        </div>

        <p>
            <a href="<?= \yii\helpers\Url::to(['cabinet-school-potok-sale/index', 'id' => $potok->id]) ?>"
               class="btn btn-success">Все продажи</a>
        </p>

    <?php else: ?>


        <div class="row">
            <div class="col-lg-8">
                <?php $form = ActiveForm::begin([
                    'options' => ['enctype' => 'multipart/form-data'],
                ]); ?>
                <?= $form->field($model, 'name') ?>
                <?= $form->field($model, 'price') ?>
                <?= $form->field($model, 'currency_id')->dropDownList(
                    ArrayHelper::merge(
                        ['- Ничего не выбрано -'],
                        ArrayHelper::map(
                            \common\models\avatar\Currency::find()->all(),
                            'id',
                            'title'
                        )
                    )
                ) ?>


                <hr>
                <div class="form-group">
                    <?= Html::submitButton('Добавить', [
                        'class' => 'btn btn-default',
                        'name'  => 'contact-button',
                        'style' => 'width:100%',
                    ]) ?>
                </div>
                <?php ActiveForm::end(); ?>
            </div>
        </div>

    <?php endif; ?>
    <?php \avatar\widgets\SchoolMenu::end() ?>


</div>
