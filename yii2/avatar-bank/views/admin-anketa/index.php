<?php

/** $this \yii\web\View  */

use yii\helpers\Url;
use app\services\GsssHtml;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use app\services\GetArticle\Collection;

$this->title = 'Все Анкеты';


?>

<div class="container">
    <div class="col-lg-12">
        <h1 class="page-header"><?= Html::encode($this->title) ?></h1>
        <p><a href="<?= Url::to(['admin-anketa/ajail']) ?>" class="btn btn-default">Статусы</a></p>

        <?php \yii\widgets\Pjax::begin(); ?>
        <?php
        $this->registerJS(<<<JS
$('.buttonDelete').click(function (e) {
    e.preventDefault();
    e.stopPropagation();
    if (confirm('Подтвердите удаление')) {
        var button = $(this);
        var id = $(this).data('id');
        ajaxJson({
            url: '/admin-anketa/delete?id=' + id,
            success: function (ret) {
                infoWindow('Успешно', function() {
                    button.parent().parent().remove();
                });
            }
        });
    }
});

$('.gsssTooltip').tooltip();

$('.rowTable').click(function() {
    window.location = '/admin-anketa/view?id=' + $(this).data('id');
});
JS
        );
        $sort = new \yii\data\Sort(
            [
                'attributes'   => [
                    'id'         => ['label' => "ID"],
                    'name_first' => ['label' => "Имя"],
                    'name_last',
                    'name_middle',
                    'contact_email',
                    'created_at' => ['label' => "Создано"],
                ],
                'defaultOrder' => [
                    'created_at' => SORT_DESC,
                ],
            ]
        );
        $model = new \avatar\models\search\Anketa();
        $provider = $model->search(Yii::$app->request->get(), null, $sort);
        ?>
        <?= \yii\grid\GridView::widget([
            'dataProvider' => $provider,
            'filterModel'  => $model,
            'tableOptions' => [
                'class' => 'table table-striped table-hover',
            ],
            'rowOptions'   => function ($item) {
                return [
                    'data'  => ['id' => $item['id']],
                    'role'  => 'button',
                    'class' => 'rowTable',
                ];
            },
            'columns'      => [
                [
                    'attribute'     => 'id',
                    'header'        => $sort->link('id'),
                    'headerOptions' => [
                        'style' => 'width: 10%',
                    ],
                ],
                [
                    'attribute'     => 'name_first',
                    'header'        => $sort->link('name_first'),
                    'headerOptions' => [
                        'style' => 'width: 10%',
                    ],
                ],
                [
                    'attribute'     => 'name_last',
                    'header'        => $sort->link('name_last'),
                    'headerOptions' => [
                        'style' => 'width: 10%',
                    ],
                ],
                [
                    'attribute'     => 'contact_email',
                    'header'        => $sort->link('contact_email'),
                    'headerOptions' => [
                        'style' => 'width: 10%',
                    ],
                ],
                [
                    'header'        => 'Создано',
                    'attribute'     => 'created_at',
                    'headerOptions' => [
                        'style' => 'width: 10%',
                    ],
                    'content'       => function ($item) {
                        $v = \yii\helpers\ArrayHelper::getValue($item, 'created_at', 0);
                        if ($v == 0) return '';

                        return Html::tag('abbr', \cs\services\DatePeriod::back($v, ['isShort' => true]), ['class' => 'gsssTooltip', 'title' => Yii::$app->formatter->asDatetime($v)]);
                    },
                ],

//                [
//                    'header'        => 'Удалить',
//                    'headerOptions' => [
//                        'style' => 'width: 10%',
//                    ],
//                    'content'       => function ($item) {
//                        return Html::button('Удалить', [
//                            'class' => 'btn btn-danger btn-xs buttonDelete',
//                            'data'  => [
//                                'id' => $item['id'],
//                            ],
//                        ]);
//                    },
//                ],
            ],
        ]) ?>
        <?php \yii\widgets\Pjax::end(); ?>
    </div>
</div>
