<?php
/**
 * Created by PhpStorm.
 * User: Ra-m-ha
 * Date: 31.12.2018
 * Time: 0:44
 */

?>
<?php /** @var \common\models\Anketa $item */ ?>

<li class="ui-state-default" data-id="<?= $item->id ?>">
    <span class="label label-info"><a href="/admin-anketa/view?id=<?= $item->id ?>">#<?= $item->id ?></a></span>
    <span class="label label-<?= ($item->comments_count > 0)? 'success' : 'default'; ?>" data-toggle="tooltip" title="Кол-во коментариев"><?= $item->comments_count ?></span><br>
    <?= $item->name_first ?><br>
    <?= $item->name_last ?><br>
    <?= $item->name_middle ?>
    <?php if ($item->user_id) { ?>
        <?php $user = \common\models\UserAvatar::findOne($item->user_id) ?>
        <br>
        <img src="<?= $user->getAvatar() ?>" width="30" class="img-circle" style="border: 1px solid #888;" title="<?= \yii\helpers\Html::encode($user->getName2()) ?>" data-toggle="tooltip"/>
    <?php } ?>
    <br>

</li>