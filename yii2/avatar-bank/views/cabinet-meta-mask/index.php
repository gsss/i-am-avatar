<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;

/* @var $this yii\web\View */

$this->title = 'Сделать исполнение контракта на странице Аватара при помощи MetaMask';


$this->registerJs(<<<JS

var isEthereum = false; 
var ethereum = null;
var isMetaMask = false; 
if (typeof window.ethereum !== 'undefined') { 
    /* deal with it */ 
    isEthereum = true;
    ethereum = window.ethereum;
}
if (isEthereum) {
    if (window.ethereum.isMetaMask) {
        isMetaMask = true;
    }
}

if (isMetaMask) {
    var accounts = ethereum.enable();
    var net = ethereum.networkVersion;
    if (net == 1) console.log('Ethereum Main Network');
    if (net == 2) console.log('Morden Test network');
    if (net == 3) console.log('Ropsten Test Network');
    if (net == 4) console.log('Rinkeby Test Network');
    if (net == 5) console.log('Goerli Test Network');
    if (net == 42) console.log('Kovan Test Network');
}

// var contractAddress = '0x52ed3c202c4652f952a1561ac0c030f1ed9460ff'; // prod
var contractAddress = '0xfF6C78BFedF1bdb2D85071A9040798F8057A0A36'; // ropsten
var abi = '[{"constant":true,"inputs":[],"name":"getDocumentsCount","outputs":[{"name":"","type":"uint256"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":false,"inputs":[{"name":"hash","type":"string"},{"name":"link","type":"string"},{"name":"data","type":"string"}],"name":"registerDocument","outputs":[],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":false,"inputs":[{"name":"id","type":"uint256"}],"name":"addSignature","outputs":[],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":true,"inputs":[{"name":"id","type":"uint256"}],"name":"getDocument","outputs":[{"name":"hash","type":"string"},{"name":"link","type":"string"},{"name":"data","type":"string"},{"name":"creator","type":"address"},{"name":"date","type":"uint256"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":false,"inputs":[],"name":"kill","outputs":[],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":true,"inputs":[{"name":"","type":"uint256"}],"name":"documentsIds","outputs":[{"name":"hash","type":"string"},{"name":"link","type":"string"},{"name":"data","type":"string"},{"name":"creator","type":"address"},{"name":"date","type":"uint256"},{"name":"signsCount","type":"uint256"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":true,"inputs":[{"name":"id","type":"uint256"}],"name":"getDocumentSignsCount","outputs":[{"name":"","type":"uint256"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":false,"inputs":[{"name":"newOwner","type":"address"}],"name":"changeOwner","outputs":[],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":true,"inputs":[{"name":"id","type":"uint256"},{"name":"index","type":"uint256"}],"name":"getDocumentSign","outputs":[{"name":"member","type":"address"},{"name":"date","type":"uint256"}],"payable":false,"stateMutability":"view","type":"function"},{"inputs":[],"payable":false,"stateMutability":"nonpayable","type":"constructor"},{"anonymous":false,"inputs":[{"indexed":false,"name":"id","type":"uint256"},{"indexed":false,"name":"member","type":"address"}],"name":"DocumentSigned","type":"event"},{"anonymous":false,"inputs":[{"indexed":false,"name":"id","type":"uint256"},{"indexed":false,"name":"hash","type":"string"}],"name":"DocumentRegistered","type":"event"}]';


window.addEventListener('load', function() {

  // Check if Web3 has been injected by the browser:
  if (typeof web3 !== 'undefined') {
    // You have a web3 browser! Continue below!
    console.log('web3 exist');
    
    var eth = web3.currentProvider;
    console.log(eth);
    var contract = web3.eth.contract(JSON.parse(abi)).at(contractAddress);
    // var contract = eth.contact;
    console.log(contract);
    
    // contract.registerDocument('sdfsdfsdf', 'http://111', 'data', function(a1,a2,a3,a4,a5) {
    //     console.log([a1,a2,a3,a4,a5]);
    // })
    // ;
    contract.getDocument(64, function(a1,a2,a3,a4,a5) {
        console.log([a1,a2,a3,a4,a5]);
    })
    ;
    
    // var contract = web3.eth.contract(JSON.parse(abi)).at(contractAddress);
    // console.log(contract);
    // var func = 'getDocumentsCount';
    // // var _params = [3];
    // var _params = [];
    // var data = contract[func](_params, function(ret,d,a,b) {
    //     console.log(['callback', ret,d,a,b]);
    // });
    // startApp(web3);
  } else {
     // Warn the user that they need to get a web3 browser
     // Or install MetaMask, maybe with a nice graphic.
  }

})

JS
)

?>
<div class="container">
    <div class="col-lg-12">
        <h1 class="page-header"><?= Html::encode($this->title) ?></h1>

    </div>
    <div class="col-lg-8">

    </div>
</div>



