<?php
use yii\helpers\Url;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use kartik\grid\GridView;


return [
    [
        'attribute'           => 'category_id',
        'value'               => function ($model, $key, $index, $widget) {
            $c = \common\models\task\Category::findOne($model['category_id']);
            if (is_null($c)) {
                return '- Без категории -';
            } else {
                return $c->name;
            }
        },
        'filter'              => ArrayHelper::map(\common\models\task\Category::find()->asArray()->all(), 'id', 'name'),
    ],
    [
        'attribute' => 'id',
    ],
    [
        'header'    => 'Автор',
        'attribute' => 'user_id',
        'filter'    => \avatar\models\search\Task::$listUserID,
        'content'   => function ($item) {
            $user_id = ArrayHelper::getValue($item, 'user_id', '');
            if ($user_id == '') {
                return '';
            }
            $user = \common\models\UserAvatar::findOne($user_id);

            return Html::img($user->getAvatar(), [
                'class'  => "img-circle",
                'width'  => 30,
                'height' => 30,
                'style'  => 'margin-bottom: 0px;',
                'data'   => ['toggle' => 'tooltip'],
                'title'  => \yii\helpers\Html::encode($user->getName2()),
            ]);
        },
    ],
    [
        'header'    => 'Исполнитель',
        'attribute' => 'executer_id',
        'filter'    => \avatar\models\search\Task::$list,
        'content'   => function ($item) {
            $executer_id = ArrayHelper::getValue($item, 'executer_id', '');
            if ($executer_id == '') {
                return '';
            }
            $user = \common\models\UserAvatar::findOne($executer_id);

            return Html::img($user->getAvatar(), [
                'class'  => "img-circle",
                'width'  => 30,
                'height' => 30,
                'style'  => 'margin-bottom: 0px;',
                'data'   => ['toggle' => 'tooltip'],
                'title'  => \yii\helpers\Html::encode($user->getName2()),
            ]);
        },
    ],
    [
        'header'             => $sort->link('name'),
        'attribute'          => 'name',
        'content'   => function ($item) {
            return Html::a($item['name'], [
                'cabinet-school-task-list/view',
                'id'  => $item['id'],
            ], ['data' => ['pjax' => 0]]);
        },
    ],
    [
        'header'          => $sort->link('price'),
        'attribute'       => 'price',
        'format'          => ['decimal', 2],
        'value'           => function ($model) {
            return $model['price'] / 100;
        },
    ],
    [
        'header'    => 'Валюта',
        'attribute' => 'currency_id',
        'content'   => function ($model, $key, $index, $widget) {
            $cur = \common\models\piramida\Currency::findOne($model['currency_id']);
            if (is_null($cur)) return '';

            return Html::tag('span', $cur->code, ['class' => 'label label-info']);
        },
    ],
    [
        'header'    => $sort->link('created_at'),
        'attribute' => 'created_at',
        'content'   => function ($item) {
            $v = \yii\helpers\ArrayHelper::getValue($item, 'created_at', 0);
            if ($v == 0) return '';

            return Html::tag('abbr', \cs\services\DatePeriod::back($v, ['isShort' => true]), ['data' => ['toggle' => 'tooltip'], 'title' => Yii::$app->formatter->asDatetime($v)]);
        },
    ],
    [
        'header'    => $sort->link('last_comment_time'),
        'attribute' => 'last_comment_time',
        'content'   => function ($item) {
            $v = \yii\helpers\ArrayHelper::getValue($item, 'last_comment_time', 0);
            if ($v == 0) return '';

            return Html::tag('abbr', \cs\services\DatePeriod::back($v, ['isShort' => true]), ['data' => ['toggle' => 'tooltip'], 'title' => Yii::$app->formatter->asDatetime($v)]);
        },
    ],
];