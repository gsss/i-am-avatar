<?php

use yii\data\ActiveDataProvider;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;
use yii\helpers\ArrayHelper;

/** @var $this yii\web\View */
/** @var $school \common\models\school\School */

$this->title = 'Денежные вклады';



?>
<div class="container">
    <div class="col-lg-12">
        <h1 class="page-header"><?= Html::encode($this->title) ?></h1>
    </div>
    <div class="col-lg-12">

        <h2 class="page-header">Благотворительная программа</h2>
        <?=
        \yii\grid\GridView::widget([
            'dataProvider'        => new ActiveDataProvider([
                'query'      => \common\models\SchoolBlagoInput::find()
                    ->innerJoin('school_blago_program', 'school_blago_program.id = school_blago_input.program_id')
                    ->innerJoin('school_koop_project', 'school_koop_project.id = school_blago_program.project_id')
                    ->where(['school_koop_project.school_id' => $school->id])
                    ->select(['school_blago_input.*'])
                    ->asArray(),
                'pagination' => [
                    'pageSize' => 200,
                ],
            ]),
            'tableOptions'          => [
                'class' => 'table table-striped table-hover'
            ],
            'rowOptions'          => function ($item) {
                return [
                    'data'  => ['id' => $item['id']],
                    'class' => 'rowTable',
                ];
            },

        ]);
        ?>
        <h2 class="page-header">Инвестиционная программа</h2>
        <?=
        \yii\grid\GridView::widget([
            'dataProvider'        => new ActiveDataProvider([
                'query'      => \common\models\SchoolInvestEvent::find()
                    ->innerJoin('school_invest_program', 'school_invest_program.id = school_invest_event.program_id')
                    ->innerJoin('school_koop_project', 'school_koop_project.id = school_invest_program.project_id')
                    ->where(['school_koop_project.school_id' => $school->id])
                    ->select(['school_blago_input.*'])
                    ->asArray()
                ,
                'pagination' => [
                    'pageSize' => 200,
                ],
            ]),
            'tableOptions'          => [
                'class' => 'table table-striped table-hover'
            ],
            'rowOptions'          => function ($item) {
                return [
                    'data'  => ['id' => $item['id']],
                    'class' => 'rowTable',
                ];
            },

        ]);
        ?>
    </div>

</div>



