<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;
use app\models\UnionCategory;

/** @var $this yii\web\View */
/** @var $school \common\models\school\School */

$this->title = 'Трудовые вклады';

$sort = new \yii\data\Sort([
    'attributes'   => [
        'id'                => ['label' => 'ID'],
        'status'            => [
            'label' => 'Прогресс',
            'asc'   => ['status' => SORT_ASC, 'category_id' => SORT_ASC],
            'desc'  => ['status' => SORT_DESC, 'category_id' => SORT_DESC],
        ],
        'price'             => [
            'label'   => 'Награда',
            'default' => SORT_DESC,
        ],
        'created_at'        => ['label' => 'Созд', 'default' => SORT_DESC],
        'name'              => ['label' => 'Наименование'],
        'last_comment_time' => ['label' => 'Обн', 'default' => SORT_DESC],
        'category_id'       => ['label' => 'category_id'],
        'currency_id '      => ['label' => 'Валюта'],
    ],
    'defaultOrder' => [
        'last_comment_time'       => SORT_DESC,
    ],
]);

$statusList = \common\models\task\Status::find()
    ->where(['school_id' => $school->id])
    ->orderBy(['sort_index' => SORT_ASC])
    ->select('id')
    ->column();
if (count($statusList) == 0) {
    throw new  Exception('count($statusList) == 0');
}

Yii::$app->session->set('$statusList', $statusList);

$model = new \avatar\models\search\Task2();
$provider = $model->search($sort, Yii::$app->request->get(), $statusList, [
    'school_task.school_id' => $school->id,
    'is_hide'               => 0,
    'status'                => $statusList[2],
    'school_task.parent_id' => null,
]);

if (Yii::$app->deviceDetect->isMobile()) {
    $options = [
        'margin-top'    => '0px',
        'margin-bottom' => '200px',
        'margin-left'   => '10px',
        'margin-right'  => '10px',
    ];
} else {
    $options = [
        'margin-top'    => '0px',
        'margin-bottom' => '200px',
        'margin-left'   => '100px',
        'margin-right'  => '100px',
    ];
}
$columns = require('index2-list-columns.php');

?>
<div class="container">
    <div class="col-lg-12">
        <h1 class="page-header"><?= Html::encode($this->title) ?></h1>
    </div>
    <div class="col-lg-12">
        <?=
        \yii\grid\GridView::widget([
            'dataProvider'        => $provider,
            'filterModel'         => $model,
            'options'             => [
                'style' => Html::cssStyleFromArray($options),
            ],
            'tableOptions'          => [
                'class' => 'table table-striped table-hover'
            ],
            'rowOptions'          => function ($item) {
                return [
                    'data'  => ['id' => $item['id']],
                    'class' => 'rowTable',
                ];
            },
            'columns'             => $columns,
        ]);
         ?>
    </div>

</div>



