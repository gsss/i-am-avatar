<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */
/* @var $file string */

$this->title = 'Лог';

$rows = [
    [
        'label' => 'api',
        'url'   => '/log/log-db-api',
    ],
    [
        'label' => 'avatar',
        'url'   => '/log/log-db-avatar',
    ],
    [
        'label' => 'console',
        'url'   => '/log/log-db-console',
    ],
    [
        'label' => 'school',
        'url'   => '/log/log-db-school',
    ],
];

$fileNameArray = explode('/', $file);
$file = $fileNameArray[count($fileNameArray) - 1];
$arr = explode('-', $file);
$arr2 = explode('.', $arr[2]);
$type = $arr2[0];

if ($type == 'api') $rows[0]['active'] = true;
if ($type == 'avatar') $rows[1]['active'] = true;
if ($type == 'console') $rows[2]['active'] = true;
if ($type == 'school') $rows[3]['active'] = true;

?>
<?= \yii\bootstrap\Tabs::widget([
    'items'            => $rows,
    'renderTabContent' => false,
]) ?>