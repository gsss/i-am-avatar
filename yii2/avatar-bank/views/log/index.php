<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */
/* @var $log array */
/* @var $first int индекс первой записи в логе, начинается с 0 */

$this->title = 'Лог';

$first = Yii::$app->request->get('first', 0);
Yii::$app->cache->set('views/site/log.php:$first', $first);

/**
 * Возвращает URL на которой находится пользователь со всеми параметрами которые есть уже и прибавляет еще те которые указаны в аргументе `$params`
 * 
 * @param array $params
 * @param bool  $isScheme
 * 
 * @return string
 */
function getLink($params = [], $isScheme = false)
{
    $p = Yii::$app->request->get();
    $options = array_merge(
        [Yii::$app->requestedRoute],
        $p,
        $params
    );
    return \yii\helpers\Url::to($options, $isScheme);
}
?>
<div style="margin: 80px 20px 140px 20px;">
<h1 class="page-header"><?= Html::encode($this->title) ?></h1>

    <?php if ($first >= 10) { ?>
        <a href="<?= getLink(['first' => $first - 10])?>" class="btn btn-default">Назад</a>
    <?php } ?>
    <a href="<?= getLink(['first' => $first + 10])?>" class="btn btn-success">Далее</a>
    
<?= \yii\grid\GridView::widget([
    'dataProvider' => new \yii\data\ArrayDataProvider([
        'allModels' => $log,
        'pagination' => [
            'pageSize' => 50
        ],
    ]),
    'tableOptions' => [
        'class' => 'table table-hover table-striped table-bordered',
    ],
    'columns'      => [
        [
            'header' => '#',
            'content' => function($model, $key, $index, $column)
            {
                return Yii::$app->cache->get('views/site/log.php:$first') + $key + 1;
            }
        ],
        'date:date:Дата',
        'time:text:Время',
        'ip',
        'user_id',
        'code',
        [
            'attribute' => 'type',
            'filter' => [
                'info'    => 'INFO',
                'danger'  => 'DANGER',
                'warning' => 'WARNING',
                'error'   => 'ERROR',
            ],
        ],
        'category',
        [
            'header'  => 'Сообщение',
            'content' => function ($item) {
                return '<pre>' . Html::encode($item['message']) . '</pre>';
            }
        ],
    ],
]) ?>

<?php if ($first >= 10) { ?>
    <a href="<?= getLink(['first' => $first - 10])?>" class="btn btn-default">Назад</a>
<?php } ?>
<a href="<?= getLink(['first' => $first + 10])?>" class="btn btn-success">Далее</a>