<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;
use app\models\UnionCategory;
use yii\db\Query;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $billing \common\models\avatar\UserBill */
/* @var $model \avatar\models\forms\CabinetBillsEthExportKey */

$this->title = 'Получить приватный ключ';

/** @var \common\models\UserAvatar $user */
$user = Yii::$app->user->identity;

?>
<div class="container">
    <div class="col-lg-12">
        <h1 class="page-header text-center"><?= Html::encode($this->title) ?></h1>

        <div class="row">
            <div class="col-lg-4 col-lg-offset-4">
                <?php $form = ActiveForm::begin(); ?>

                <?= $form->field($model, 'password')->passwordInput() ?>
                <?php if (!empty($user->google_auth_code)) { ?>
                    <?= $form->field($model, 'pin') ?>
                <?php } ?>

                <hr>
                <div class="form-group">
                    <?= Html::submitButton('Выдать', [
                        'class' => 'btn btn-default',
                        'name'  => 'contact-button',
                        'style' => 'width:100%',
                    ]) ?>
                </div>
                <?php ActiveForm::end(); ?>
            </div>
        </div>
    </div>
</div>
