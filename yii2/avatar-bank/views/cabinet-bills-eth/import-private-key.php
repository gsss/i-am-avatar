<?php
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model \avatar\models\forms\CabinetBillsImportJson */

$this->title = 'Импортировать кошелек';

$this->registerJs(<<<JS
JS
);

?>
<div class="container" style="padding-bottom: 70px;">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header text-center"><?= $this->title ?></h1>
        </div>
        <?php if (Yii::$app->session->hasFlash('form')) { ?>
            <p class="alert alert-success">Успешно</p>
        <?php } else { ?>
            <?php $form = ActiveForm::begin([
                'options' => ['enctype' => 'multipart/form-data']
            ]); ?>
            <?= $form->field($model, 'key') ?>
            <?= $form->field($model, 'password') ?>

            <hr>
            <div class="form-group">
                <?= Html::submitButton('Импортировать', [
                    'class' => 'btn btn-default',
                    'style' => 'width:100%',
                ]) ?>
            </div>
            <?php ActiveForm::end(); ?>

        <?php } ?>
    </div>
</div>


