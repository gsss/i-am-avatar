<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;
use app\models\UnionCategory;
use yii\db\Query;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model cs\base\BaseForm */
/* @var $union app\models\Union */

$this->title = $model->name;
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="container">
    <h1 class="page-header"><?= Html::encode($this->title) ?></h1>
    <?= \cs\Widget\BreadCrumbs\BreadCrumbs::widget([
        'items' => [
            [
                'label' => 'Объединения',
                'url'   => Url::to(['cabinet/objects']),
            ],
            [
                'label' => $union->getName(),
                'url'   => Url::to(['cabinet/objects_edit', 'id' => $union->getId()]),
            ],
            [
                'label' => 'Магазин',
                'url'   => Url::to(['cabinet_shop_shop/index', 'id' => $union->getId()]),
            ],
            [
                'label' => 'Товары',
                'url'   => Url::to(['cabinet_shop_shop/product_list', 'id' => $union->getId()]),
            ],
            $this->title
        ],
        'home'  => [
            'name' => 'я',
            'url'  => Url::to(['site/user', 'id' => Yii::$app->user->id])
        ]
    ]) ?>
    <hr>

    <?php if (Yii::$app->session->hasFlash('contactFormSubmitted')): ?>

        <div class="alert alert-success">
            Успешно обновлено.
        </div>

    <?php else: ?>


        <div class="row">
            <div class="col-lg-6 col-lg-offset-3">
                <?php $form = ActiveForm::begin([
                    'id'      => 'contact-form',
                    'options' => ['enctype' => 'multipart/form-data']
                ]); ?>
                <?= $model->field($form, 'name') ?>
                <?= $model->field($form, 'description')->textarea(['rows' => 4]) ?>
                <?= $model->field($form, 'content') ?>
                <?= $model->field($form, 'image') ?>
                <?= $model->field($form, 'price') ?>
                <?= $model->field($form, 'is_piramida') ?>
                <?= $model->field($form, 'tree_node_id') ?>
                <?= $model->field($form, 'shop_node_id') ?>
                <hr>
                <p><a class="btn btn-default" href="<?= Url::to(['cabinet_shop_shop_images/index', 'id' => $model->id])?>">Дополнительные изображения</a></p>
                <h3 class="page-header">Электронный товар</h3>
                <?= $model->field($form, 'is_electron') ?>
                <?= $model->field($form, 'electron_text') ?>
                <div style="display: table">
                    <?= $model->field($form, 'attached_files') ?>
                </div>
                <hr>
                <div class="form-group">
                    <?= Html::submitButton('Обновить', [
                        'class' => 'btn btn-default',
                        'name'  => 'contact-button',
                        'style' => 'width:100%',
                    ]) ?>
                </div>
                <?php ActiveForm::end(); ?>
            </div>
        </div>

    <?php endif; ?>
</div>
