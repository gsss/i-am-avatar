<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;
use app\models\UnionCategory;
use yii\helpers\Url;
use app\models\Shop\Request;

/* @var $this yii\web\View */
/* @var $union_id int */

$union = \app\models\Union::find($union_id);

$this->title = 'Заказы';


$sort = new \yii\data\Sort([
    'attributes' => [
        'id',
        'last_message_time' => [
            'label'   => 'Последнее сообщение',
            'default' => SORT_DESC,
        ],
        'price'             => [
            'label' => 'Цена',
        ],
    ],
    'defaultOrder' => [
        'last_message_time' => SORT_DESC
    ],
]);
$filterModel = new \app\models\search\ShopRequest();
$provider = $filterModel->search2(Yii::$app->request->get(), ['union_id' => $union_id], $sort);
?>
<style>
    a.asc:after {
        content: ' ↓';
        display: inline;
    }
    a.desc:after {
        content: ' ↑';
        display: inline;
    }
</style>
<div class="container">
    <h1 class="page-header"><?= Html::encode($this->title) ?></h1>
    <?= \cs\Widget\BreadCrumbs\BreadCrumbs::widget([
        'items' => [
            [
                'label' => 'Объединения',
                'url'   => Url::to(['cabinet/objects']),
            ],
            [
                'label' => $union->getName(),
                'url'   => Url::to(['cabinet/objects_edit', 'id' => $union->getId()]),
            ],
            [
                'label' => 'Магазин',
                'url'   => Url::to(['cabinet_shop_shop/index', 'id' => $union->getId()]),
            ],
            $this->title
        ],
        'home'  => [
            'name' => 'я',
            'url'  => Url::to(['site/user', 'id' => Yii::$app->user->id])
        ]
    ]) ?>
    <hr>

    <?php \yii\widgets\Pjax::begin() ?>
    <?php
    $this->registerJs(<<<JS
    $('.buttonDelete').click(function (e) {
        e.preventDefault();
        e.stopPropagation();
        if (confirm('Подтвердите удаление')) {
            var button = $(this);
            var id = $(this).data('id');
            ajaxJson({
                url: '/cabinet/shop/requestList/' + id + '/delete',
                success: function (ret) {
                    infoWindow('Успешно', function() {
                        button.parent().parent().remove();
                    });
                }
            });
        }
    });


    $('.rowTable').click(function() {
        window.location = '/cabinet/shop/requestList/' + $(this).data('id');
    });
JS
    );
    ?>
    <?= \yii\grid\GridView::widget([
        'tableOptions' => [
            'class' => 'table table-striped table-hover',
        ],
        'rowOptions'   => function ($item) {
            return [
                'data'  => ['id' => $item['id']],
                'role'  => 'button',
                'class' => 'rowTable',
            ];
        },
        'dataProvider' => $provider,
        'filterModel' => $filterModel,
        'columns'      => [
            'id',
            [
                'header'  => 'Клиент',
                'content' => function ($item) {
                    $u = \app\services\UsersInCache::find($item['user_id']);
                    if (is_null($u)) return '';
                    $arr = [];
                    $arr[] = Html::img($u['avatar'], ['width' => 50, 'class' => 'thumbnail', 'style' => 'margin-bottom: 0px; margin-right: 10px;float: left;']);
                    $arr[] = Html::a($u['name'], '/user/' . $u['id']);
                    $arr[] = '<br>';
                    $arr[] = '<small style="color: #888;">' . $u['email'] . '</small>';

                    return join('', $arr);
                },
                'attribute' => 'user',
            ],

            [
                'header'         => $sort->link('price'),
                'contentOptions' => [
                    'nowrap' => 'nowrap',
                ],
                'content'        => function ($item) {
                    $i = Yii::$app->formatter->asDecimal($item['price'], 0);

                    return $i;
                },
                'attribute' => 'price',
            ],
            [
                'header'  => 'Статус',
                'content' => function ($item) {
                    $s = \yii\helpers\ArrayHelper::getValue(Request::$statusList, $item['status'], null);
                    if (is_null($s)) {
                        $title = '';
                    } else {
                        $title = \yii\helpers\ArrayHelper::getValue($s, 'client', '');
                    }
                    switch ($item['status']) {
                        case Request::STATUS_SEND_TO_SHOP:
                            $progress = 10;
                            break;
                        case Request::STATUS_ORDER_DOSTAVKA:
                            $progress = 20;
                            break;
                        case Request::STATUS_PAID_CLIENT:
                            $progress = 30;
                            break;
                        case Request::STATUS_PAID_SHOP:
                            $progress = 40;
                            break;
                        case Request::STATUS_DOSTAVKA_ADDRESS_PREPARE:
                            $progress = 50;
                            break;
                        case Request::STATUS_DOSTAVKA_ADDRESS_PREPARE_DONE:
                            $progress = 60;
                            break;
                        case Request::STATUS_DOSTAVKA_ADDRESS_SEND_TO_USER:
                            $progress = 70;
                            break;
                        case Request::STATUS_DOSTAVKA_ADDRESS_FINISH_CLIENT:
                            $progress = 90;
                            break;
                        case Request::STATUS_DOSTAVKA_ADDRESS_FINISH_SHOP:
                            $progress = 100;
                            break;
                        case Request::STATUS_DOSTAVKA_SAMOVIVOZ_WAIT:
                            $progress = 50;
                            break;
                        case Request::STATUS_DOSTAVKA_SAMOVIVOZ_FINISH_CLIENT:
                            $progress = 90;
                            break;
                        case Request::STATUS_DOSTAVKA_SAMOVIVOZ_FINISH_SHOP:
                            $progress = 100;
                            break;
                        case Request::STATUS_DOSTAVKA_ELECTRON_DONE:
                            $progress = 100;
                            break;
                        case Request::STATUS_DOSTAVKA_NALOZH_PREPARE:
                            $progress = 50;
                            break;
                        case Request::STATUS_DOSTAVKA_NALOZH_PREPARE_DONE:
                            $progress = 60;
                            break;
                        case Request::STATUS_DOSTAVKA_NALOZH_PIAD:
                            $progress = 100;
                            break;
                        case Request::STATUS_DOSTAVKA_NALOZH_SEND_TO_USER:
                            $progress = 70;
                            break;
                        case Request::STATUS_DOSTAVKA_NALOZH_RETURN:
                            $progress = 100;
                            break;
                        default:
                            $progress = 0;
                            break;
                    }

                    return Html::tag('div',
                        Html::tag('div',
                            Html::tag('span', '40% Complete (success)', ['class' => 'sr-only'])
                            ,
                            ['class' => 'progress-bar progress-bar-success', 'role' => "progressbar", 'aria-valuenow' => $progress, 'aria-valuemin' => "0", 'aria-valuemax' => "100", 'style' => "width: " . $progress . "%"])
                        ,
                        [
                            'class' => 'progress gsssTooltip',
                            'title' => $title,
                        ]
                    );
                },
            ],
            [
                'header'  => 'Создан',
                'content' => function ($item) {
                    $v = \yii\helpers\ArrayHelper::getValue($item, 'date_create', 0);
                    if ($v == 0) return '';

                    return Html::tag('abbr', \cs\services\DatePeriod::back($v, ['isShort' => true]), ['class' => 'gsssTooltip', 'title' => Yii::$app->formatter->asDatetime($v)]);
                }
            ],
            [
                'header'  => $sort->link('last_message_time'),
                'content' => function ($item) {
                    $v = \yii\helpers\ArrayHelper::getValue($item, 'last_message_time', 0);
                    if ($v == 0) return '';

                    return Html::tag('abbr', \cs\services\DatePeriod::back($v, ['isShort' => true]), ['class' => 'gsssTooltip', 'title' => Yii::$app->formatter->asDatetime($v)]);
                }
            ],
            [
                'header'  => 'Оплачен?',
                'filter' => [
                    0 => 'Не оплачен',
                    1 => 'Оплачен',
                ],
                'content' => function ($item) {
                    $v = \yii\helpers\ArrayHelper::getValue($item, 'is_paid', 0);
                    if ($v == 0) return Html::tag('span', 'Нет', ['class' => 'label label-default']);

                    return Html::tag('span', 'Да', ['class' => 'label label-success']);
                },
                'attribute' => 'is_paid',

            ],
            [
                'header'  => 'Есть ответ?',
                'content' => function ($item) {
                    $v = \yii\helpers\ArrayHelper::getValue($item, 'is_answer_from_client', 0);
                    if ($v == 0) return '';

                    return Html::tag('span', Html::tag('span', null, ['class' => 'glyphicon glyphicon-envelope']), ['class' => 'label label-success']);
                }
            ],
            [
                'header'  => 'Удалить',
                'content' => function ($item) {
                    return Html::button('Удалить', ['class' => 'btn btn-danger btn-xs buttonDelete', 'data-id' => $item['id']]);
                }
            ],
            [
                'header'  => 'Скрыть',
                'content' => function ($item) {
                    return Html::button('Скрыть', ['class' => 'btn btn-default btn-xs buttonHide', 'data-id' => $item['id']]);
                }
            ],
        ]
    ]) ?>
    <?php \yii\widgets\Pjax::end() ?>
</div>
