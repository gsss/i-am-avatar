<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;
use app\models\UnionCategory;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $union_id int */

$union = \app\models\Union::find($union_id);

$this->title = 'Товары';

$this->registerJs(<<<JS

    $('.buttonDelete').click(function (e) {
        e.preventDefault();
        e.stopPropagation();
        if (confirm('Подтвердите удаление')) {
            var button = $(this);
            var id = $(this).data('id');
            ajaxJson({
                url: '/cabinet/shop/productList/' + id + '/delete',
                success: function (ret) {
                    infoWindow('Успешно', function() {
                        button.parent().parent().remove();
                    });
                }
            });
        }
    });

    $('.buttonSendModeration').click(function (e) {
        e.preventDefault();
        e.stopPropagation();
        if (confirm('Подтвердите отправку')) {
            var button = $(this);
            var id = $(this).data('id');
            ajaxJson({
                url: '/cabinet/shop/productList/' + id + '/sendModeration',
                success: function (ret) {
                    infoWindow('Успешно', function() {
                        button.parent().html($('<span>', {
                            class: 'label label-info'
                        }).html('Отправлено'));
                    });
                }
            });
        }
    });
    $('.rowTable').click(function() {
        window.location = '/cabinet/shop/productList/' + $(this).data('id') + '/edit';
    });
JS
);
?>
<div class="container">
    <h1 class="page-header"><?= Html::encode($this->title) ?></h1>
    <?= \cs\Widget\BreadCrumbs\BreadCrumbs::widget([
        'items' => [
            [
                'label' => 'Объединения',
                'url'   => Url::to(['cabinet/objects']),
            ],
            [
                'label' => $union->getName(),
                'url'   => Url::to(['cabinet/objects_edit', 'id' => $union->getId()]),
            ],
            [
                'label' => 'Магазин',
                'url'   => Url::to(['cabinet_shop_shop/index', 'id' => $union->getId()]),
            ],
            $this->title
        ],
        'home'  => [
            'name' => 'я',
            'url'  => Url::to(['site/user', 'id' => Yii::$app->user->id])
        ]
    ]) ?>
    <hr>

    <?= \yii\grid\GridView::widget([
        'tableOptions' => [
            'class' => 'table table-striped table-hover',
        ],
        'rowOptions'   => function ($item) {
            return [
                'data' => ['id' => $item['id']],
                'role' => 'button',
                'class' => 'rowTable'
            ];
        },
        'dataProvider' => new \yii\data\ActiveDataProvider([
            'query'      => \app\models\Shop\Product::query(['union_id' => $union_id])
                ->select([
                    'id',
                    'name',
                    'image',
                    'price',
                    'moderation_status',
                ])
            ->orderBy(['sort_index' => SORT_ASC])
            ,
            'pagination' => [
                'pageSize' => 50,
            ],
        ]),
        'columns' => [
            'id',
            [
                'header' => 'Картинка',
                'content' => function($item) {
                    $i = \yii\helpers\ArrayHelper::getValue($item, 'image', '');
                    if ($i == '') return '';
                    return Html::img($i, ['width' => 100]);
                }
            ],
            'name:text:Наименование',
            'price:decimal:Цена',
            [
                'header' => 'Модерация',
                'content' => function ($item) {
                    $moderation_status = \yii\helpers\ArrayHelper::getValue($item, 'moderation_status');
                    if (is_null($moderation_status)) {
                        $arr[] = Html::tag('span', 'На редактировании', ['class' => 'label label-default']);
                        $arr[] = Html::tag('br');
                        $arr[] = Html::tag('br');
                        $arr[] = Html::button('Отправить на модерацию', [
                            'class'   => "btn btn-default btn-xs buttonSendModeration",
                            'data-id' => $item['id'],
                        ]);
                        return join('', $arr);
                    } else if ($moderation_status == 2) {
                        return Html::tag('span', 'На рассмотрении', ['class' => 'label label-warning']);
                    } else if ($moderation_status == 0) {
                        $arr[] = Html::tag('span', 'Отклонено модератором', ['class' => 'label label-danger']);
                        $arr[] = Html::tag('br');
                        $arr[] = Html::tag('br');
                        $arr[] = Html::button('Отправить на модерацию', [
                            'class'   => "btn btn-default btn-xs buttonSendModeration",
                            'data-id' => $item['id'],
                        ]);
                        return join('', $arr);
                    } else {
                        return Html::tag('span', 'Одобрено', ['class' => 'label label-success']);
                    }
                }
            ],
            [
                'header' => 'Удалить',
                'content' => function($item) {
                    return Html::button('Удалить', ['class' => 'btn btn-danger btn-xs buttonDelete', 'data-id' => $item['id']]);
                }
            ],
        ]
    ]) ?>
    <hr>
    <a href="<?= \yii\helpers\Url::to(['cabinet_shop_shop/product_list_add', 'id' => $union_id]) ?>" class="btn btn-default">Добавить</a>
</div>
