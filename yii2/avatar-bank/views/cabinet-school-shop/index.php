<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;
use app\models\UnionCategory;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $school \common\models\school\School */

$this->title = 'Магазин';

?>
<div class="container">

    <?php \avatar\widgets\SchoolMenu::begin(['school' => $school]) ?>
    <h1 class="page-header"><?= Html::encode($this->title) ?></h1>
    <p>
        <a href="<?= \yii\helpers\Url::to(['cabinet-school-shop-goods/index', 'id' => $school->id]) ?>" class="btn btn-default">Товары <span class="badge"><?= \common\models\shop\Product::find()->where(['school_id' => $school->id])->count() ?></span></a>
        <a href="<?= \yii\helpers\Url::to(['cabinet-school-shop-delivery/index', 'id' => $school->id]) ?>" class="btn btn-default">Доставка <span class="badge"><?= \common\models\shop\DeliveryItem::find()->where(['school_id' => $school->id])->count() ?></span></a>
        <a href="<?= \yii\helpers\Url::to(['cabinet-school-shop-catalog/index', 'id' => $school->id]) ?>" class="btn btn-default">Каталог</a>
        <a href="<?= \yii\helpers\Url::to(['cabinet-school-shop-requests/index', 'id' => $school->id]) ?>" class="btn btn-default">Заказы <span class="badge"><?= \common\models\shop\Request::find()->where(['school_id' => $school->id])->count() ?></span></a>
    </p>
    <?php \avatar\widgets\SchoolMenu::end() ?>
</div>
