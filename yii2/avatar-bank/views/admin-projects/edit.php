<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;
use app\models\UnionCategory;
use yii\db\Query;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \avatar\models\forms\MapBankomat */

$this->title = $model->name;

?>
<div class="container">
    <div class="col-lg-12">
        <h1 class="page-header"><?= Html::encode($this->title) ?></h1>

    </div>
    <div class="col-lg-8">

        <?php if (Yii::$app->session->hasFlash('form')): ?>

            <div class="alert alert-success">
                Успешно обновлено.
            </div>
            <a href="<?= \yii\helpers\Url::to(['admin-blog/index']) ?>" class="btn btn-success">Блог</a>

        <?php else: ?>


            <?php $form = ActiveForm::begin([
                'id'      => 'contact-form',
                'options' => ['enctype' => 'multipart/form-data'],
            ]); ?>
            <?= $model->field($form, 'name') ?>
            <?= $model->field($form, 'content') ?>
            <?= $model->field($form, 'image') ?>
            <?= $model->field($form, 'sum') ?>
            <?= $model->field($form, 'currency')
                ->dropDownList(
                    ArrayHelper::merge(
                        ['Ничего не выбрано'],
                        ArrayHelper::map(
                            \common\models\avatar\Currency::find()
                                ->select([
                                    'id',
                                    'concat(title, " (", code, ")") as title'
                                ])
                                ->all(),
                            'id',
                            'title'
                        )
                    )
                )
            ?>

            <hr>
            <div class="form-group">
                <?= Html::submitButton('Обновить', [
                    'class' => 'btn btn-default',
                    'name'  => 'contact-button',
                    'style' => 'width:100%',
                ]) ?>
            </div>
            <?php ActiveForm::end(); ?>
        <?php endif; ?>
    </div>
    <div class="col-lg-4">
        <?= $this->render('../cabinet/_menu') ?>
    </div>
</div>
