<?php

/** $this \yii\web\View  */

use yii\helpers\Url;
use app\services\GsssHtml;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use app\services\GetArticle\Collection;

$this->title = 'Все проекты';


?>

<div class="container">
    <div class="col-lg-12">
        <h1 class="page-header"><?= Html::encode($this->title) ?></h1>



    </div>
    <div class="col-lg-8">
        <?php \yii\widgets\Pjax::begin(); ?>
        <?php
        $this->registerJS(<<<JS
$('[data-toggle="tooltip"]').tooltip();
$('.buttonDelete').click(function (e) {
    e.preventDefault();
    e.stopPropagation();
    if (confirm('Подтвердите удаление')) {
        var button = $(this);
        var id = $(this).data('id');
        ajaxJson({
            url: '/admin-projects/delete' + '?' + 'id' + '=' + id,
            success: function (ret) {
                $('#modalInfo').on('hidden.bs.modal', function() {
                    button.parent().parent().remove();
                }).modal();
            }
        });
    }
});
$('.buttonSendSuccess').click(function (e) {
    e.preventDefault();
    e.stopPropagation();
    if (confirm('Подтвердите отправку')) {
        var button = $(this);
        var id = $(this).data('id');
        ajaxJson({
            url: '/admin-projects/send-success' + '?' + 'id' + '=' + id,
            success: function (ret) {
                $('#modalInfo').on('hidden.bs.modal', function() {
                    window.location.reload();
                }).modal();
            }
        });
    }
});
$('.buttonSendReject').click(function (e) {
    e.preventDefault();
    e.stopPropagation();
    if (confirm('Подтвердите отправку')) {
        var button = $(this);
        var id = $(this).data('id');
        ajaxJson({
            url: '/admin-projects/send-reject' + '?' + 'id' + '=' + id,
            success: function (ret) {
                $('#modalInfo').on('hidden.bs.modal', function() {
                    window.location.reload();
                }).modal();
            }
        });
    }
});
$('.buttonPayments').click(function (e) {
   e.preventDefault();
   e.stopPropagation();
   window.location = '/admin-projects/payments' + '?' + 'id' + '=' + $(this).data('id');
});
$('.buttonRequests').click(function (e) {
   e.preventDefault();
   e.stopPropagation();
   window.location = '/admin-projects/requests' + '?' + 'id' + '=' + $(this).data('id');
});
$('.buttonView').click(function (e) {
   e.preventDefault();
   e.stopPropagation();
   window.location = '/admin-projects/view' + '?' + 'id' + '=' + $(this).data('id');
});

$('.rowTable').click(function() {
    window.location = '/admin-projects/edit' + '?' + 'id' + '=' + $(this).data('id');
});

JS
        );
        $sort = new \yii\data\Sort([
            'defaultOrder' => [
                'created_at' => SORT_DESC
            ]
        ]);
        ?>
        <?= \yii\grid\GridView::widget([
            'dataProvider' => new \yii\data\ActiveDataProvider([
                'query'      => \common\models\investment\Project::find()
                ,
                'pagination' => [
                    'pageSize' => 20,
                ],
            ]),
            'tableOptions' => [
                'class' => 'table table-striped table-hover',
            ],
            'rowOptions'   => function ($item) {
                $data = [
                    'data'  => ['id' => $item['id']],
                    'role'  => 'button',
                    'class' => 'rowTable'
                ];
                return $data;
            },
            'columns'      => [
                [
                    'header'  => 'Картинка',
                    'content' => function ($item) {
                        $i = ArrayHelper::getValue($item, 'image', '');
                        if ($i == '') return '';

                        return Html::img($i, [
                            'class'  => "thumbnail",
                            'width'  => 80,
                            'height' => 80,
                            'style'  => 'margin-bottom: 0px;',
                        ]);
                    }
                ],
                'name',
                [
                    'header'  => 'Создано',
                    'content' => function ($item) {
                        $v = \yii\helpers\ArrayHelper::getValue($item, 'created_at', 0);
                        if ($v == 0) return '';

                        return Html::tag('abbr', \cs\services\DatePeriod::back($v, ['isShort' => true]), ['data' => ['toggle' => 'tooltip'], 'title' => Yii::$app->formatter->asDatetime($v)]);
                    }
                ],
                [
                    'header'  => 'Функции',
                    'content' => function ($item) {
                        $rows = [];
                        $rows[] = Html::button('Платежные системы', ['class' => 'btn btn-default btn-xs buttonPayments', 'data' => ['id' => $item['id']]]);
                        $rows[] = Html::button('Заказы', ['class' => 'btn btn-default btn-xs buttonRequests', 'data' => ['id' => $item['id']], 'style' => 'margin-top:8px;']);
//                        $rows[] = Html::button('Подарки', ['class' => 'btn btn-default btn-xs', 'id' => 'buttonGifts', 'data' => ['id' => $item['id']], 'style' => 'margin-top:8px;']);
                        $rows[] = Html::button('Посмотреть', ['class' => 'btn btn-default btn-xs buttonView', 'data' => ['id' => $item['id']], 'style' => 'margin-top:8px;']);

                        return join('<br>', $rows);
                    }
                ],
                [
                    'header'  => 'Модерация',
                    'content' => function ($item) {
                        $status = \yii\helpers\ArrayHelper::getValue($item, 'status', 0);
                        switch ($status) {
                            case 1:
                                return Html::tag('span', 'Отправлено', ['class' => 'label label-warning'])
                                    .
                                    '<br>' .
                                    Html::tag(
                                        'div',
                                        Html::button(Html::tag('i', null, ['class' => 'glyphicon glyphicon-thumbs-up']), [
                                            'class' => 'btn btn-success btn-xs buttonSendSuccess',
                                            'data'  => ['id' => $item['id']],
                                            'style' => 'margin-top: 10px;',
                                        ]) .
                                        ' ' .
                                        Html::button(Html::tag('i', null, ['class' => 'glyphicon glyphicon-thumbs-down']), [
                                            'class' => 'btn btn-danger btn-xs buttonSendReject',
                                            'data'  => ['id' => $item['id']],
                                            'style' => 'margin-top: 10px;',
                                        ])
                                        ,
                                        ['class' => 'btn-group', 'role' => 'group', 'aria-label' => '...']
                                    );
                                break;
                            case 2:
                                return Html::tag('span', 'Утверждено', ['class' => 'label label-success']);
                                break;
                            case 3:
                                return Html::tag('span', 'Отклонено', ['class' => 'label label-danger']);
                                break;
                            case 0:
                            default:
                                return Html::tag('span', 'Черновик', ['class' => 'label label-default']);
                                break;
                        }
                    }
                ],
                [
                    'header'  => 'Удалить',
                    'content' => function ($item) {
                        return Html::button('Удалить', [
                            'class' => 'btn btn-danger btn-xs buttonDelete',
                            'data'  => [
                                'id' => $item['id'],
                            ]
                        ]);
                    }
                ],
            ],
        ]) ?>
        <?php \yii\widgets\Pjax::end(); ?>
    </div>

</div>

<div class="modal fade" id="modalInfo" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Информация</h4>
            </div>
            <div class="modal-body">
                Успешно!
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
            </div>
        </div>
    </div>
</div>