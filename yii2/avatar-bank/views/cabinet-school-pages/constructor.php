<?php
/**
 * Created by PhpStorm.
 * User: Metatron
 * Date: 22.12.2018
 * Time: 5:44
 */

/** @var \common\models\school\Page $page */
/** @var \common\models\school\School $school */

\avatar\assets\JsonEditor::register($this);

$this->registerJs(<<<JS
$('.buttonAdd').click(function(e) {
    var block_id = $(this).data('id');
    $('#modalAddStep1').collapse('show');
    $('#modalAddStep2').collapse('hide');
    $('#modalAdd').modal();
    $('#modalAdd').attr('data-block_id', block_id);
});

$('.buttonDelete').click(function(e) {
    var id = $(this).data('id');
    if (confirm('Вы уверены?')) {
        ajaxJson({
            url: '/cabinet-school-pages-constructor/block-delete',
            data: {id:id},
            success: function(ret) {
                window.location.reload();
            }
        });
    }
});

$('.buttonUp').click(function(e) {
    var id = $(this).data('id');
    ajaxJson({
        url: '/cabinet-school-pages-constructor/block-up',
        data: {id:id},
        success: function(ret) {
            window.location.reload();
        }
    });
});

$('.buttonDown').click(function(e) {
    var id = $(this).data('id');
    ajaxJson({
        url: '/cabinet-school-pages-constructor/block-down',
        data: {id:id},
        success: function(ret) {
            window.location.reload();
        }
    });
});

$('.buttonCopy').click(function(e) {
    var id = $(this).data('id');
    ajaxJson({
        url: '/cabinet-school-pages-constructor/block-copy',
        data: {id:id},
        success: function(ret) {

        }
    });
});

$('.buttonPaste').click(function(e) {
    var id = $(this).data('id');
    ajaxJson({
        url: '/cabinet-school-pages-constructor/block-paste',
        data: {id:id},
        success: function(ret) {
            window.location.reload();
        }
    });
});

var functionClickBlock = function(e) {
    var id = $(this).data('id');
    var block_id = $('#modalAdd').data('block_id');
    ajaxJson({
        url: '/cabinet-school-pages-constructor/block-add-execute',
        data: {
            id:id,
            block_id:block_id
        },
        success: function(ret) {
            $('#modalAdd').modal('hide');
            $('#modalAddStep1').collapse('show');
            $('#modalAddStep2').collapse('hide');
            window.location.reload();
        }
    });
};

$('.buttonAddCategory').click(function(e) {
    var id = $(this).data('id');
    ajaxJson({
        url: '/cabinet-school-pages-constructor/block-add',
        data: {id:id},
        success: function(ret) {
            var htmlObject = $(ret.html);
            htmlObject.find('.buttonBlockItem').click(functionClickBlock);
            $('#modalAddStep2').html(htmlObject);
            $('#modalAddStep1').collapse('hide');
            $('#modalAddStep2').collapse('show');
        }
    });
});
JS
);
?>

<nav class="navbar navbar-default navbar-fixed-top">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="<?= \yii\helpers\Url::to(['cabinet-school-pages/index', 'id' => $school->id]) ?>" style="padding: 5px 10px 5px 10px;">
                <img src="/images/avatarlogo1.png" width="40" data-toggle="tooltip" data-placement="bottom" title="Все страницы">
            </a>
        </div>
        <div id="navbar" class="collapse navbar-collapse">
            <p style="margin-top: 10px;"><?= $page->name ?> / <a href="<?= $page->getUrl() ?>" target="_blank"><?= $page->getUrl() ?></a></p>
        </div>
    </div>
</nav>


<div class="container" style="
    padding: 0px;
    height: auto;
">
    <div class="col-lg-3">
        <p class="text-center">
            <button class="btn btn-info buttonAdd"  style="width: 100%;">1</button>
        </p>
    </div>
</div>

<?php /** @var \common\models\school\PageBlockContent $block */ ?>
<?php foreach ($page->getBlocks() as $block) { ?>
    <div class="container" id="block_edit_<?= $block->id ?>" style="
    padding: 0px;
    height: auto;
">
        <div class="col-lg-3">
            <div class="row">
                <div class="col-lg-6">
                    <?php $cat = \common\models\school\PageBlockCategory::findOne($block->getBaseBlock()->category_id); ?>
                    <pre data-toggle="tooltip" title="Тип блока: <?= $cat->name ?> / <?= $block->getBaseBlock()->name ?>">C<?= $cat->id ?>/B<?= $block->type_id ?></pre>
                </div>
                <div class="col-lg-6">
                    <p class="text-center">
                        <button class="btn btn-info buttonAdd" data-id="<?= $block->id ?>" style="width: 100%;" data-toggle="tooltip" title="Добавить блок выше">
                            Добавить
                        </button>
                    </p>
                </div>
            </div>
        </div>
        <div class="col-lg-6">
            <p class="text-center">
                <button class="btn btn-default buttonSettings" data-id="<?= $block->id ?>" data-type="<?= $block->type_id ?>" style="width: 100%;">
                    Настройки блока
                </button>
            </p>
        </div>
        <div class="col-lg-3">
            <div class="btn-group" role="group" aria-label="...">
                <button class="btn btn-danger buttonDelete" data-id="<?= $block->id ?>" data-toggle="tooltip" title="Удалить блок который под кнопкой">
                    Удалить
                </button>
                <button class="btn btn-default buttonUp" data-id="<?= $block->id ?>" data-toggle="tooltip" title="Передвинуть блок выше">
                    <span class="glyphicon glyphicon-arrow-up"></span>
                </button>
                <button class="btn btn-default buttonDown" data-id="<?= $block->id ?>" data-toggle="tooltip" title="Передвинуть блок ниже">
                    <span class="glyphicon glyphicon-arrow-down"></span>
                </button>
                <button class="btn btn-default buttonCopy" data-id="<?= $block->id ?>" data-toggle="tooltip" title="Скопировать блок в буфер">
                    <span class="glyphicon glyphicon-copy"></span>
                </button>
                <button class="btn btn-default buttonPaste" data-id="<?= $block->id ?>" data-toggle="tooltip" title="Вставить блок выше из буфера">
                    <span class="glyphicon glyphicon-paste"></span>
                </button>
            </div>
        </div>
    </div>

    <?= $block->renderEdit() ?>

<?php } ?>

<?php $typeList = \common\models\school\PageBlockContent::find()
    ->where(['page_id' => $page->id])
    ->select(['type_id'])
    ->groupBy('type_id')
    ->all();  ?>

<?php /** @var \common\models\school\PageBlockContent $block */ ?>
<?php foreach ($typeList as $block) { ?>
    <?= $block->renderEditSettings($block->type_id, $school->id) ?>
<?php } ?>


<div class="modal fade" id="modalAdd" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="myModalLabel">Информация</h4>
            </div>
            <div class="modal-body">
                <div class="collapse in" id="modalAddStep1">
                    <div class="btn-group-vertical" role="group" aria-label="..." style="width: 100%">
                        <?php $categoryList = \common\models\school\PageBlockCategory::find()->orderBy(['sort_index' => SORT_ASC])->all(); ?>
                        <?php /** @var \common\models\school\PageBlockCategory $c */ ?>
                        <?php foreach ($categoryList as $c) { ?>
                            <button class="btn btn-default buttonAddCategory" data-id="<?= $c->id ?>"
                                    style="width: 100%"><?= $c->name ?></button>
                        <?php } ?>
                    </div>
                </div>
                <div class="collapse" id="modalAddStep2">

                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-success buttonAddAction">Сохранить</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
            </div>
        </div>
    </div>
</div>





