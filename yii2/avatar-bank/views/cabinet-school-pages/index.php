<?php

/** @var $this \yii\web\View */
/** @var $school \common\models\school\School */

use yii\helpers\Url;
use app\services\GsssHtml;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use app\services\GetArticle\Collection;

$this->title = 'Все страницы';


?>

<div class="container">


    <h1 class="page-header"><?= Html::encode($this->title) ?></h1>

    <p>
        <a href="<?= Url::to(['cabinet-school-pages/add', 'id' => $school->id]) ?>" class="btn btn-default">Добавить</a>
        <a href="<?= Url::to(['cabinet-school-pages/header-footer', 'id' => $school->id]) ?>" class="btn btn-default">Шапка
            и подвал</a>
    </p>

    <?php \yii\widgets\Pjax::begin(); ?>
    <?php
    $this->registerJS(<<<JS
$('[data-toggle="tooltip"]').tooltip();
$('.buttonDelete').click(function (e) {
    e.preventDefault();
    e.stopPropagation();
    if (confirm('Подтвердите удаление')) {
        var button = $(this);
        var id = $(this).data('id');
        ajaxJson({
            url: '/cabinet-school-pages/delete' + '?' + 'id' + '=' + id,
            success: function (ret) {
                $('#modalInfo').on('hidden.bs.modal', function() {
                    button.parent().parent().remove();
                }).modal();
            }
        });
    }
});

$('.buttonEdit').click(function(e) {
    e.preventDefault();
    e.stopPropagation();
    window.location = '/cabinet-school-pages/edit' + '?' + 'id' + '=' + $(this).data('id');
});

$('.buttonSeo').click(function(e) {
    e.preventDefault();
    e.stopPropagation();
    window.location = '/cabinet-school-pages/seo' + '?' + 'id' + '=' + $(this).data('id');
});
$('.buttonStat').click(function(e) {
    e.preventDefault();
    e.stopPropagation();
    window.location = '/cabinet-school-pages/stat' + '?' + 'id' + '=' + $(this).data('id');
});
$('.buttonBody').click(function(e) {
    e.preventDefault();
    e.stopPropagation();
    window.location = '/cabinet-school-pages/body' + '?' + 'id' + '=' + $(this).data('id');
});
$('.buttonConstructor').click(function(e) {
    e.preventDefault();
    e.stopPropagation();
    window.location = '/cabinet-school-pages/constructor' + '?' + 'id' + '=' + $(this).data('id');
});
$('.buttonFavicon').click(function(e) {
    e.preventDefault();
    e.stopPropagation();
    window.location = '/cabinet-school-pages/favicon' + '?' + 'id' + '=' + $(this).data('id');
});
$('.buttonSs').click(function(e) {
    e.preventDefault();
    e.stopPropagation();
    window.location = '/cabinet-school-pages/social-net' + '?' + 'id' + '=' + $(this).data('id');
});
$('.buttonHeader').click(function(e) {
    e.preventDefault();
    e.stopPropagation();
    window.location = '/cabinet-school-pages/index-header-footer' + '?' + 'id' + '=' + $(this).data('id');
});
$('.buttonCopy').click(function(e) {
    e.preventDefault();
    e.stopPropagation();
    window.location = '/cabinet-school-pages/copy' + '?' + 'id' + '=' + $(this).data('id');
});
$('.buttonMove').click(function(e) {
    e.preventDefault();
    e.stopPropagation();
    window.location = '/cabinet-school-pages/move' + '?' + 'id' + '=' + $(this).data('id');
});
$('.buttonPotok').click(function(e) {
    e.preventDefault();
    e.stopPropagation();
    window.location = '/cabinet-school-potok/view' + '?' + 'id' + '=' + $(this).data('id');
});
JS
    );
    $model = new \avatar\models\search\Page();
    $provider = $model->search(Yii::$app->request->get(), ['school_id' => $school->id], ['defaultOrder' => ['id' => SORT_ASC]])
    ?>
    <?= \yii\grid\GridView::widget([
        'dataProvider' => $provider,
        'filterModel'  => $model,
        'tableOptions' => [
            'class' => 'table table-striped table-hover',
        ],
        'rowOptions'   => function ($item) {
            $data = [
                'data'  => ['id' => $item['id']],
                'role'  => 'button',
                'class' => 'rowTable',
            ];
            return $data;
        },
        'columns'      => [
            [
                'attribute' => 'id',
                'headerOptions' => ['style' => 'width:5%'],
            ],
            [
                'attribute' => 'name',
                'headerOptions' => ['style' => 'width:30%'],
            ],
            [
                'header'    => 'Ссылка',
                'headerOptions' => ['style' => 'width:30%'],
                'attribute' => 'url',
                'content'   => function (\common\models\school\Page $item) {
                    return Html::tag('code', \common\helpers\Html::a($item->url, $item->getUrl(), ['target' => '_blank']));
                },
            ],
            [
                'header'        => 'Функции',
                'headerOptions' => ['style' => 'width:15%'],
                'content'       => function ($item) {
                    $functions = [
                        [
                            'class' => 'buttonEdit',
                            'name'  => 'Редактировать',
                        ],
                        [
                            'class' => 'buttonCopy',
                            'name'  => 'Копировать',
                        ],
                        [
                            'class' => 'buttonMove',
                            'name'  => 'Переместить',
                        ],
                        [
                            'class' => 'buttonFavicon',
                            'name'  => 'Favicon',
                            'title' => 'Иконка страницы во вкладке браузера',
                            'isOn'  => function ($item) {
                                if (\cs\Application::isEmpty($item['favicon'])) {
                                    return false;
                                } else {
                                    return true;
                                }
                            },
                        ],
                        [
                            'class' => 'buttonSs',
                            'name'  => 'СоцСети',
                            'title' => 'Мета информация для формирования презентационного бока при репосте страницы в СоцСетях',
                            'isOn'  => function ($item) {
                                if (\cs\Application::isEmpty($item['facebook_image'])) {
                                    return false;
                                } else {
                                    return true;
                                }
                            },
                        ],
                        [
                            'class' => 'buttonBody',
                            'name'  => 'Body',
                            'title' => 'HTML код дополнительный для вставки в страницу',
                            'isOn'  => function ($item) {
                                if (\cs\Application::isEmpty($item['body'])) {
                                    return false;
                                } else {
                                    return true;
                                }
                            },
                        ],
                        [
                            'class' => 'buttonSeo',
                            'name'  => 'SEO',
                            'title' => 'Заголовок страницы, описание и ключевые слова',
                            'isOn'  => function ($item) {
                                if (\cs\Application::isEmpty($item['title'])) {
                                    return false;
                                } else {
                                    return true;
                                }
                            },
                        ],
                        [
                            'class' => 'buttonHeader',
                            'name'  => 'Шапка и подвал',
                            'isOn'  => function ($item) {
                                $header_id = $item['header_id'];
                                $footer_id = $item['footer_id'];
                                if ($header_id) {
                                    return true;
                                }
                                if ($footer_id) {
                                    return true;
                                }

                                return false;
                            },
                        ],
                        [
                            'class' => 'buttonStat',
                            'name'  => 'Статистика',
                        ],
                    ];

                    $b1 = Html::button('Конструктор', ['class' => 'btn btn-default btn-xs buttonConstructor', 'data' => ['id' => $item['id']]]);
                    $contentB2 = join('', [
                        Html::tag('span', null, ['class' => 'caret']),
                        Html::tag('span', 'Toggle Dropdown', ['class' => 'sr-only']),
                    ]);
                    $b2 = Html::button($contentB2, ['class' => 'btn btn-default dropdown-toggle btn-xs', 'data' => ['toggle' => 'dropdown'], 'aria-haspopup' => 'true', 'aria-expanded' => ['false']]);
                    $items = [];
                    foreach ($functions as $item1) {
                        $class = [
                            $item1['class'],
                        ];
                        if (isset($item1['isOn'])) {
                            $function = $item1['isOn'];
                            $result = $function($item);
                            if ($result) {
                                $class[] = 'btn-info';
                            }
                        }
                        $options = [
                            'class' => join(' ', $class),
                            'data'  => [
                                'id' => $item['id'],
                            ],
                        ];
                        if (isset($item1['title'])) $options['title'] = $item1['title'];
                        $items[] = Html::tag('li', Html::a($item1['name'], 'javascript:void(0);', $options));
                    }
                    $ul = Html::tag('ul', join('', $items), ['class' => 'dropdown-menu']);

                    $b = Html::tag('div', $b1 . $b2 . $ul, ['class' => 'btn-group']);

                    return $b;
                },
            ],
            [
                'header'  => 'Поток',
                'headerOptions' => ['style' => 'width:10%'],
                'content' => function ($item) {
                    $potok = \common\models\school\Potok::findOne(['page_id' => $item['id']]);
                    if (!is_null($potok)) {
                        return Html::button('Поток', [
                            'class' => 'btn btn-success btn-xs buttonPotok',
                            'data'  => [
                                'id' => $potok->id,
                            ],
                        ]);
                    }
                    return '';
                },
            ],
            [
                'header'  => 'Удалить',
                'headerOptions' => ['style' => 'width:10%'],
                'content' => function ($item) {
                    return Html::button('Удалить', [
                        'class' => 'btn btn-danger btn-xs buttonDelete',
                        'data'  => [
                            'id' => $item['id'],
                        ],
                    ]);
                },
            ],
        ],
    ]) ?>
    <?php \yii\widgets\Pjax::end(); ?>



</div>

<div class="modal fade" id="modalInfo" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="myModalLabel">Информация</h4>
            </div>
            <div class="modal-body">
                Успешно!
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
            </div>
        </div>
    </div>
</div>
