<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;
use app\models\UnionCategory;
use yii\db\Query;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $page \common\models\school\Page */
/* @var $model \avatar\models\forms\CabinetSchoolPagesCopy */
/* @var $school \common\models\school\School */

$this->title = 'Копировать страницу';

?>
<div class="container">

    <h1 class="page-header"><?= Html::encode($this->title) ?></h1>

    <?php if (!is_null($id = Yii::$app->session->getFlash('form'))) : ?>

        <div class="alert alert-success">
            Успешно скопировано.
        </div>

        <p>
            <a href="<?= \yii\helpers\Url::to(['cabinet-school-pages/index', 'id' => $school->id]) ?>" class="btn btn-success">Все страницы</a>
        </p>

    <?php else: ?>


        <div class="row">
            <div class="col-lg-8">
                <?php $form = ActiveForm::begin([
                    'options' => ['enctype' => 'multipart/form-data'],
                ]); ?>
                <?= $form->field($model, 'name') ?>
                <?= $form->field($model, 'link') ?>


                <hr>
                <div class="form-group">
                    <?= Html::submitButton('Копировать', [
                        'class' => 'btn btn-default',
                        'name'  => 'contact-button',
                        'style' => 'width:100%',
                    ]) ?>
                </div>
                <?php ActiveForm::end(); ?>
            </div>
        </div>

    <?php endif; ?>


</div>
