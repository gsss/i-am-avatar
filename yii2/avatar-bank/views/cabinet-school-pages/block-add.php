<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;
use app\models\UnionCategory;
use yii\db\Query;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $items \common\models\school\PageBlock[] */

?>
<?php /** @var \common\models\school\PageBlock $item */ ?>
<?php foreach ($items as $item) { ?>
    <div class="block" id="<?= $item->id ?>">
        <button class="btn btn-default buttonBlockItem" data-id="<?= $item->id ?>"><?= $item->name ?></button>
        <?php if ($item->image) { ?>
            <p><img src="<?= $item->image ?>" style="width: 100%;" class="thumbnail"></p>
        <?php } ?>
    </div>
<?php } ?>