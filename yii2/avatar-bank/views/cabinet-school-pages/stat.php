<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;
use app\models\UnionCategory;
use yii\db\Query;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \avatar\models\forms\school\Page */
/* @var $school \common\models\school\School */

$this->title = $model->name;


$sort = new \yii\data\Sort([
    'attributes' => [
        'created_at',
        'is_bot',
    ],
    'defaultOrder' => [
        'created_at' => SORT_DESC
    ],
]);


?>
<div class="container">

    <h1 class="page-header"><?= Html::encode($this->title) ?></h1>


    <?php
    $period = 24 * 14; // две недели
    $rows = \common\models\school\PageStat::find()
        ->select([
            "unix_timestamp(concat(substr(from_unixtime(created_at),1,13), ':00:00')) as t1",
            'count(*) as c1',
        ])
        ->where(['page_id' => $model->id])
        ->andWhere(['between', 'created_at', time() - 60 * 60 * $period, time()])
        ->groupBy(['substr(from_unixtime(created_at),1,13)'])
        ->asArray()
        ->all()
    ;
    $rows2 = \common\models\school\PageStat::find()
        ->select([
            "unix_timestamp(concat(substr(from_unixtime(created_at),1,13), ':00:00')) as t1",
            'count(*) as c1',
        ])
        ->where(['page_id' => $model->id])
        ->andWhere(['is_bot' => 0])
        ->andWhere(['between', 'created_at', time() - 60 * 60 * $period, time()])
        ->groupBy(['substr(from_unixtime(created_at),1,13)'])
        ->asArray()
        ->all()
    ;

    $rowsJson = \yii\helpers\Json::encode($rows);
    $rows2Json = \yii\helpers\Json::encode($rows2);
    $this->registerJs(<<<JS
        Highcharts.setOptions({
            global: {
                timezoneOffset: -3 * 60
            }
        });
        var rows = {$rowsJson};
        var newRows = [];
        for(i = 0; i < rows.length; i++)
        {
            var item = rows[i];
            newRows.push({
                x: new Date(parseInt(item.t1) * 1000),
                y: parseInt(item.c1)
            });
        }
        var rows2 = {$rows2Json};
        var newRows2 = [];
        for(i = 0; i < rows2.length; i++)
        {
            var item2 = rows2[i];
            newRows2.push({
                x: new Date(parseInt(item2.t1) * 1000),
                y: parseInt(item2.c1)
            });
        }
JS
    );

    $colors = ['#5cb85c', '#cf4440'];

    ?>
    <?= \cs\Widget\HighCharts\HighCharts::widget([
        'chartOptions' => [
            'chart' => [
                'colors' => $colors,
                'zoomType' => 'x',
                'type'     => 'spline',
                'events' => [
                    'selection' => new \yii\web\JsExpression(<<<JS
function(event) {
    if (typeof(event.xAxis) == "undefined") {
        console.log(1);
    } else {
        // log the min and max of the primary, datetime x-axis
        console.log(
            Highcharts.dateFormat(
                '%Y-%m-%d %H:%M:%S',
                event.xAxis[0].min
            ),
            Highcharts.dateFormat(
                '%Y-%m-%d %H:%M:%S',
                event.xAxis[0].max
            )
        );
        // log the min and max of the y axis
        console.log(event.yAxis[0].min, event.yAxis[0].max);
    }
}
JS
                    )
                ]
            ],
            'title' => [
                'text' => 'График',
            ],
            'subtitle' => [
                'text' => 'Выделите область для изменения масштаба',
            ],
            'xAxis' => [
                'type' => 'datetime',
            ],
            'yAxis' => [
                [
                    'title' => [
                        'text' => 'Количество',
                    ],
                ]
            ],
            'legend' => [
                'enabled' => true
            ],
            'tooltip' => [
                'crosshairs' => true,
                'shared' => true,
            ],
            'plotOptions' => [
                'series' => [
                    'turboThreshold' => 0,
                ],
            ],
            'series' => [
                [
                    'type' => 'column',
                    'name' => 'Запросов',
                    'data' => new \yii\web\JsExpression('newRows'),
                ],
                [
                    'type' => 'column',
                    'name' => 'Запросов без ботов',
                    'data' => new \yii\web\JsExpression('newRows2'),
                ],
            ],
        ],
    ]);

    ?>


    <h2 class="page-header">Статистика</h2>
    <p>Всего открытий: <?= \common\models\school\PageStat::find()->andWhere(['page_id' => $model->id])->count() ?></p>
    <p>Всего открытий (люди): <?= \common\models\school\PageStat::find()->where(['is_bot' => 0])->andWhere(['page_id' => $model->id])->count() ?></p>




</div>
