<?php

/** @var $this \yii\web\View */
/** @var $school \common\models\school\School */
/** @var $page  \common\models\school\Page */

use yii\helpers\Url;
use app\services\GsssHtml;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use app\services\GetArticle\Collection;

$this->title = $page->name;

?>

<div class="container">

    <h1 class="page-header"><?= Html::encode($this->title) ?></h1>

    <hr>

    <?= \yii\widgets\DetailView::widget([
        'model'      => $page,
        'attributes' => [
            [
                'label'          => 'Наименование',
                'captionOptions' => [
                    'style' => 'width: 30%',
                ],
                'value'          => $page->name,
            ],
            'url',
        ],
    ]) ?>
    <p><a href="<?= $page->getUrl() ?>" target="_blank"><?= $page->url ?></a></p>

    <p><a href="<?= Url::to(['cabinet-school-pages/edit', 'id' => $page->id]) ?>" class="btn btn-default">Редактировать</a></p>
    <p><a href="<?= Url::to(['cabinet-school-pages/favicon', 'id' => $page->id]) ?>" class="btn btn-default">favicon</a></p>
    <p><a href="<?= Url::to(['cabinet-school-pages/social-net', 'id' => $page->id]) ?>" class="btn btn-default">СоцСети</a></p>
    <p><a href="<?= Url::to(['cabinet-school-pages/body', 'id' => $page->id]) ?>" class="btn btn-default">Body</a></p>
    <p><a href="<?= Url::to(['cabinet-school-pages/seo', 'id' => $page->id]) ?>" class="btn btn-default">SEO</a></p>
    <p><a href="<?= Url::to(['cabinet-school-pages/constructor', 'id' => $page->id]) ?>" class="btn btn-default">Конструктор</a></p>
    <p><a href="<?= Url::to(['cabinet-school-pages/stat', 'id' => $page->id]) ?>" class="btn btn-default">Статистика</a></p>
    <p><a href="<?= Url::to(['cabinet-school-pages/index-header-footer', 'id' => $page->id]) ?>" class="btn btn-default">Шапка и подвал</a></p>

    <?php $potok = \common\models\school\Potok::findOne(['page_id' => $page->id]); ?>
    <?php if (!is_null($potok)) { ?>
        <p><a href="<?= Url::to(['cabinet-school-potok/view', 'id' => $potok->id]) ?>" class="btn btn-default">Поток</a></p>
    <?php } ?>



</div>

<div class="modal fade" id="modalInfo" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="myModalLabel">Информация</h4>
            </div>
            <div class="modal-body">
                Успешно!
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
            </div>
        </div>
    </div>
</div>