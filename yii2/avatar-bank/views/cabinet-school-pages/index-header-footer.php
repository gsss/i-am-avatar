<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;
use app\models\UnionCategory;
use yii\db\Query;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \common\models\school\Page */
/* @var $school \common\models\school\School */

$this->title = $model->name;

?>
<div class="container">


    <h1 class="page-header"><?= Html::encode($this->title) ?></h1>

    <?php if (Yii::$app->session->hasFlash('form')) { ?>
        <div class="alert alert-success">
            Успешно обновлено.
        </div>
        <p>
            <a href="<?= \yii\helpers\Url::to(['cabinet-school-pages/index', 'id' => $school->id]) ?>"
               class="btn btn-success">Все страницы</a>
        </p>

    <?php } else { ?>

        <div class="row">
            <div class="col-lg-8">
                <?php $form = ActiveForm::begin([
                    'id'      => 'contact-form',
                    'options' => ['enctype' => 'multipart/form-data'],
                ]); ?>
                <?= $form->field($model, 'header_id')
                    ->dropDownList(
                        ArrayHelper::merge(
                            [null => '- Ничего не выбрано -'],
                            ArrayHelper::map(
                                \common\models\school\Page::find()
                                    ->where(['school_id' => $school->id])
                                    ->andWhere([
                                        'or',
                                        ['url' => ''],
                                        ['url' => null],
                                    ])
                                    ->all(),
                                'id',
                                'name'
                            )
                        )
                    )
                    ->label('Шапка') ?>
                <?= $form->field($model, 'footer_id')
                    ->dropDownList(
                        ArrayHelper::merge(
                            [null => '- Ничего не выбрано -'],
                            ArrayHelper::map(
                                \common\models\school\Page::find()
                                    ->where(['school_id' => $school->id])
                                    ->andWhere([
                                        'or',
                                        ['url' => ''],
                                        ['url' => null],
                                    ])
                                    ->all(),
                                'id',
                                'name'
                            )
                        )
                    )
                    ->label('Подвал') ?>

                <hr>
                <div class="form-group">
                    <?= Html::submitButton('Обновить', [
                        'class' => 'btn btn-default',
                        'name'  => 'contact-button',
                        'style' => 'width:100%',
                    ]) ?>
                </div>
                <?php ActiveForm::end(); ?>
            </div>
        </div>

    <?php } ?>


</div>
