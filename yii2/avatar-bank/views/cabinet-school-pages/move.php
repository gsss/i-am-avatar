<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;
use app\models\UnionCategory;
use yii\db\Query;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $page \common\models\school\Page */
/* @var $model \avatar\models\forms\CabinetSchoolPagesMove */
/* @var $school \common\models\school\School */

$this->title = 'Переместить страницу';

$list = [];
$list1 = \common\models\school\School::find()->where(['user_id' => Yii::$app->user->id])
    ->andWhere(['not', ['id' => $page->school_id]])
    ->all();
$list2 = \common\models\school\School::find()
    ->where(
        [
            'id' => \common\models\school\AdminLink::find()
                ->where(['user_id' => Yii::$app->user->id])
                ->select('school_id')
                ->column(),
        ]
    )
    ->andWhere(['not', ['id' => $page->school_id]])
    ->all();
$list = ArrayHelper::merge($list1, $list2);

?>
<div class="container">

    <?php \avatar\widgets\SchoolMenu::begin(['school' => $school]) ?>
    <h1 class="page-header"><?= Html::encode($this->title) ?></h1>

    <?php if (!is_null($id = Yii::$app->session->getFlash('form'))) : ?>

        <div class="alert alert-success">
            Успешно скопировано.
        </div>

        <p>
            <a href="<?= \yii\helpers\Url::to(['cabinet-school-pages/index', 'id' => $school->id]) ?>"
               class="btn btn-success">Все страницы</a>
        </p>

    <?php else: ?>


        <div class="row">
            <div class="col-lg-8">
                <?php $form = ActiveForm::begin([
                    'options' => ['enctype' => 'multipart/form-data'],
                ]); ?>
                <?= $form
                    ->field($model, 'to')
                    ->dropDownList(
                        ArrayHelper::merge(
                            ['- Ничего не выбрано -'],
                            ArrayHelper::map($list, 'id', 'name')
                        )
                    )
                    ->label('Куда')
                ?>


                <hr>
                <div class="form-group">
                    <?= Html::submitButton('Переместить', [
                        'class' => 'btn btn-default',
                        'name'  => 'contact-button',
                        'style' => 'width:100%',
                    ]) ?>
                </div>
                <?php ActiveForm::end(); ?>
            </div>
        </div>

    <?php endif; ?>
    <?php \avatar\widgets\SchoolMenu::end() ?>


</div>
