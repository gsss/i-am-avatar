<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;
use app\models\UnionCategory;
use yii\db\Query;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \avatar\models\forms\school\Tilda */
/** @var $school \common\models\school\School */

$this->title = 'Настройки плагина Тильда';

\avatar\assets\Notify::register($this);
\avatar\assets\Clipboard::register($this);

$this->registerJs(<<<JS

var clipboard = new Clipboard('.buttonCopy');
clipboard.on('success', function(e) {

    new Noty({
        timeout: 1000,
        theme: 'sunset',
        type: 'success',
        layout: 'bottomLeft',
        text: 'Скопировано'
    }).show();

});

JS
);
?>
<div class="container">


    <h1 class="page-header"><?= Html::encode($this->title) ?></h1>

    <?php if (!is_null($id = Yii::$app->session->getFlash('form'))) : ?>

        <div class="alert alert-success">
            Успешно добавлено.
        </div>
        <p>
            <a href="<?= \yii\helpers\Url::to(['cabinet-school-tilda/index', 'id' => $school->id]) ?>"
               class="btn btn-success">Назад к настройкам</a>
        </p>
    <?php else: ?>

        <div class="row">
            <div class="col-lg-8">
                <?php $form = ActiveForm::begin([
                    'options' => ['enctype' => 'multipart/form-data'],
                ]); ?>
                <?= $form->field($model, 'publickey') ?>
                <?= $form->field($model, 'secretkey')->passwordInput() ?>

                <hr>
                <div class="form-group">
                    <?= Html::submitButton('Сохранить', [
                        'class' => 'btn btn-default',
                        'name'  => 'contact-button',
                        'style' => 'width:100%',
                    ]) ?>
                </div>
                <?php ActiveForm::end(); ?>

                <hr>


                <div class="form-group field-tilda-publickey required">
                    <label class="control-label" for="tilda-publickey">Путь для изображений</label>
                    <?php $value = '/tilda/' . $school->id . '/images/' ?>
                    <div class="input-group">
                        <input type="text" class="form-control" value="<?= $value ?>">
                        <span class="input-group-btn">
                        <button class="btn btn-default buttonCopy" type="button" data-clipboard-text="<?= $value ?>" data-toggle="tooltip" title="Нажми чтобы скопировать">Копировать</button>
                    </span>
                    </div><!-- /input-group -->
                </div>

                <div class="form-group field-tilda-publickey required">
                    <label class="control-label" for="tilda-publickey">Путь для js файлов</label>
                    <?php $value = '/tilda/' . $school->id . '/js/' ?>
                    <div class="input-group">
                        <input type="text" class="form-control" value="<?= $value ?>">
                        <span class="input-group-btn">
                        <button class="btn btn-default buttonCopy" type="button" data-clipboard-text="<?= $value ?>" data-toggle="tooltip" title="Нажми чтобы скопировать">Копировать</button>
                    </span>
                    </div><!-- /input-group -->
                </div>

                <div class="form-group field-tilda-publickey required">
                    <label class="control-label" for="tilda-publickey">Путь для css файлов</label>
                    <?php $value = '/tilda/' . $school->id . '/css/' ?>
                    <div class="input-group">
                        <input type="text" class="form-control" value="<?= $value ?>">
                        <span class="input-group-btn">
                        <button class="btn btn-default buttonCopy" type="button" data-clipboard-text="<?= $value ?>" data-toggle="tooltip" title="Нажми чтобы скопировать">Копировать</button>
                    </span>
                    </div><!-- /input-group -->
                </div>

                <div class="form-group field-tilda-publickey required">
                    <label class="control-label" for="tilda-publickey">Путь ко всем страницам сайта</label>
                    <?php $value = $school->getUrl() ?>
                    <div class="input-group">
                        <input type="text" class="form-control" value="<?= $value ?>">
                        <span class="input-group-btn">
                        <button class="btn btn-default buttonCopy" type="button" data-clipboard-text="<?= $value ?>" data-toggle="tooltip" title="Нажми чтобы скопировать">Копировать</button>
                    </span>
                    </div><!-- /input-group -->
                </div>

                <hr>
                <div class="form-group field-tilda-publickey required">
                    <label class="control-label" for="tilda-publickey">Ссылка на ваш скрипт (Webhook URL): </label>
                    <?php $value = \yii\helpers\Url::to('/tilda/call-back', true); ?>
                    <div class="input-group">
                        <input type="text" class="form-control" value="<?= $value ?>">
                        <span class="input-group-btn">
                        <button class="btn btn-default buttonCopy" type="button" data-clipboard-text="<?= $value ?>" data-toggle="tooltip" title="Нажми чтобы скопировать">Копировать</button>
                    </span>
                    </div><!-- /input-group -->
                </div>

            </div>
        </div>

    <?php endif; ?>



</div>
