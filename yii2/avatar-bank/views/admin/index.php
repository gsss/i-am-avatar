<?php
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this \yii\web\View */

$this->title = 'Админка';

?>

<div class="container" style="padding-bottom: 70px;">
    <div class="col-lg-12">
        <h1 class="page-header"><?= $this->title ?></h1>

        <h2 class="page-header">Документация</h2>
        <p><a href="/admin-developer/index" class="btn btn-default">Документация</a></p>
        <p><a href="/admin-developer2/index" class="btn btn-default">Документация 2</a></p>

        <h2 class="page-header">Деловая логика</h2>
        <p><a href="/admin-qr-code/index" class="btn btn-default">Карты</a></p>
        <p><a href="/admin-card-id/index" class="btn btn-default">Карты ID</a></p>
        <p><a href="/admin-paysystem/index" class="btn btn-default">Платежные системы</a></p>
        <p><a href="/admin-card-design/index" class="btn btn-default">Карты дизайн</a></p>
        <p><a href="/admin-telegram/subscribe" class="btn btn-default">Telegram рассылка</a></p>
        <p><a href="/admin-user-root/index" class="btn btn-default">UserRoot</a></p>
        <p><a href="/admin-wallets/index" class="btn btn-default">Кошельки</a></p>
        <p><a href="/admin-school/index" class="btn btn-default">Все школы</a></p>
        <p><a href="/admin-school-dns/index" class="btn btn-default">DNS заявки</a></p>

        <p><a href="/admin/mail-test" class="btn btn-default">Временная почта</a></p>
        <p><a href="/admin-subscribe/index" class="btn btn-default">Рассылка</a></p>
        <p><a href="/admin-school-page-category/index" class="btn btn-default">Категории блоков для страниц</a></p>


        <h2 class="page-header">Процессинг</h2>
        <p><a href="/admin-currency/index" class="btn btn-default">Валюты</a></p>
        <p><a href="/admin-currency-int/index" class="btn btn-default">Монеты</a></p>
        <p><a href="/admin-api-login/index" class="btn btn-default">admin-api-login</a></p>

        <h2 class="page-header">Системные процессы</h2>
        <p><a href="/admin-languages/index" class="btn btn-default">Языки</a></p>
        <p><a href="/admin-telegram/index" class="btn btn-default">Telegram</a></p>
        <p><a href="/admin-server-request/index" class="btn btn-default">server-request</a></p>
        <p><a href="/log/log-db-avatar" class="btn btn-default">log</a></p>
    </div>
</div>
