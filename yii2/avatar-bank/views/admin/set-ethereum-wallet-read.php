<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;
use app\models\UnionCategory;

/* @var $this yii\web\View */
/* @var $model \avatar\models\forms\EthereumWalletRead */

$this->title = 'Кошелек для чтения из контрактов';

?>
<div class="container">
    <div class="col-lg-12">
        <h1 class="page-header"><?= Html::encode($this->title) ?></h1>
        <div class="col-lg-8">


            <div class="col-lg-8">
                <?php if (Yii::$app->session->hasFlash('form')) { ?>
                    <p class="alert alert-success">Успешно</p>

                    <a href="/admin/set-ethereum-wallet-read" class="btn btn-default">Назад</a>
                <?php } else { ?>
                    <?php $form = ActiveForm::begin() ?>
                    <?= $form->field($model, 'address') ?>
                    <?= $form->field($model, 'password')->passwordInput() ?>
                    <hr>
                    <?= Html::submitButton('Обновить', [
                        'class' => 'btn btn-success',
                        'style' => 'width: 100%',
                    ]) ?>
                    <?php ActiveForm::end() ?>
                <?php } ?>
            </div>

        </div>
        <div class="col-lg-4">
            <?= $this->render('_menu') ?>
        </div>
    </div>
</div>



