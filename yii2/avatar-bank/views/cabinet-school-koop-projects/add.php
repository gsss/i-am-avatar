<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;
use app\models\UnionCategory;
use yii\db\Query;
use yii\helpers\ArrayHelper;

/** @var $this      yii\web\View */
/** @var $form      yii\bootstrap\ActiveForm */
/** @var $model     \avatar\models\forms\CabinetKoopProject */
/** @var $school    \common\models\school\School */
/** @var $program   \common\models\SchoolKoopProgram */

$this->title = 'Добавить проект';

?>
<div class="container">

    <h1 class="page-header"><?= Html::encode($this->title) ?></h1>

    <div class="row">
        <div class="col-lg-5">
            <?php $form = \iAvatar777\services\FormAjax\ActiveForm::begin([
                'model' => $model,
                'success' => <<<JS
function (ret) {
    $('#modalInfo').on('hidden.bs.modal', function() {
        window.location = '/cabinet-school-koop-projects/index?id=' + $program->id;
    }).modal();
}
JS

            ]); ?>
            <?= $form->field($model, 'name') ?>
            <?= $form->field($model, 'link') ?>
            <?= $form->field($model, 'document') ?>
            <?= $form->field($model, 'type_id')->dropDownList([
                \common\models\SchoolKoopProject::TYPE_BLAGO_PROGRAM => 'Благотворительная программа',
                \common\models\SchoolKoopProject::TYPE_INVEST_PROGRAM => 'Инвестиционная программа',
            ], ['prompt' => '- Ничего не выбрано -']) ?>

            <?php \iAvatar777\services\FormAjax\ActiveForm::end(['label' => 'Добавить']); ?>
        </div>
    </div>


</div>


<div class="modal fade" id="modalInfo" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="myModalLabel">Информация</h4>
            </div>
            <div class="modal-body">
                Успешно!
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
            </div>
        </div>
    </div>
</div>
