<?php

/** $this \yii\web\View  */
/** @var $school \common\models\school\School */
/** @var $program \common\models\SchoolKoopProgram */

use yii\helpers\Url;
use app\services\GsssHtml;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use app\services\GetArticle\Collection;

$this->title = 'Проекты';

\avatar\assets\Notify::register($this);

?>

<div class="container">

    <h1 class="page-header"><?= Html::encode($this->title) ?></h1>

    <p><a href="<?= Url::to(['cabinet-school-koop-projects/add', 'id' => $program->id]) ?>" class="btn btn-default">Добавить</a></p>

    <?php \yii\widgets\Pjax::begin(); ?>
    <?php
    $this->registerJS(<<<JS
$('[data-toggle="tooltip"]').tooltip();

$('.rowTable').click(function() {
    //window.location = '/cabinet-school-koop-aim-programs/edit' + '?' + 'id' + '=' + $(this).data('id');
});

$('.buttonDelete').click(function (e) {
    e.preventDefault();
    e.stopPropagation();
    if (confirm('Подтвердите удаление')) {
        var button = $(this);
        var id = $(this).data('id');
        ajaxJson({
            url: '/cabinet-school-koop-projects/delete',
            data: {id: id},
            success: function (ret) {
                $('#modalInfo').on('hidden.bs.modal', function() {
                    button.parent().parent().remove();
                }).modal();
            }
        });
    }
});
JS
    );
    $sort = new \yii\data\Sort([
        'defaultOrder' => ['id' => SORT_DESC],
    ]);
    ?>

    <?= \yii\grid\GridView::widget([
        'dataProvider' => new \yii\data\ActiveDataProvider([
            'query'      => \common\models\SchoolKoopProject::find()
                ->where(['program_id' => $program->id])
            ,
            'sort'       => $sort,
            'pagination' => [
                'pageSize' => 20,
            ],
        ]),
        'tableOptions' => [
            'class' => 'table table-striped table-hover',
        ],
        'rowOptions'   => function ($item) {
            $data = [
                'data'  => ['id' => $item['id']],
                'class' => 'rowTable',
            ];
            return $data;
        },
        'columns'      => [
            'id',
            'name',
            'link',
            'document',
            [
                'header'  => '',
                'content' => function ($item) {
                    return Html::a('Редактировать', ['cabinet-school-koop-projects/edit', 'id' => $item['id']], [
                        'class' => 'btn btn-primary btn-xs',
                        'data'  => [
                            'id' => $item['id'],
                            'pjax' => 0,
                        ],
                    ]);
                },
            ],
            [
                'header'  => '',
                'content' => function ($item) {
                    $html = [];
                    if ($item['type_id']) {
                        switch ($item['type_id']) {
                            case \common\models\SchoolKoopProject::TYPE_BLAGO_PROGRAM:
                                $html[] = Html::tag('span', 'Благотворительная программа', ['class' => 'label label-info']);
                                $is_view = \common\models\SchoolBlagoProgram::findOne(['project_id' => $item['id']]) != null;
                                break;
                            case \common\models\SchoolKoopProject::TYPE_INVEST_PROGRAM:
                                $html[] = Html::tag('span', 'Инвестиционная программа', ['class' => 'label label-info']);
                                $is_view = \common\models\SchoolInvestProgram::findOne(['project_id' => $item['id']]) != null;
                                break;
                        }


                        $html[] = Html::a('Настройки', ['cabinet-school-koop-projects/settings', 'id' => $item['id']], [
                            'class' => 'btn btn-info btn-xs buttonSettings',
                            'data'  => [
                                'id' => $item['id'],
                            ],
                        ]) . ($is_view ? Html::a('Просмотр', ['cabinet-school-koop-projects/settings-view', 'id' => $item['id']], [
                            'class' => 'btn btn-info btn-xs buttonSettings',
                            'data'  => [
                                'id' => $item['id'],
                            ],
                        ]) : '');
                    }
                    return join('<br><br>', $html);
                },
            ],
            [
                'header'  => '',
                'content' => function ($item) {
                    return Html::button('Удалить', [
                        'class' => 'btn btn-danger btn-xs buttonDelete',
                        'data'  => [
                            'id' => $item['id'],
                        ],
                    ]);
                },
            ],
        ],
    ]) ?>
    <?php \yii\widgets\Pjax::end(); ?>

</div>

<div class="modal fade" id="modalInfo" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="myModalLabel">Информация</h4>
            </div>
            <div class="modal-body">
                Успешно!
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
            </div>
        </div>
    </div>
</div>