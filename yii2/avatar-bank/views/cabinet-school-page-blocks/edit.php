<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;
use app\models\UnionCategory;
use yii\db\Query;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \common\models\school\PageBlockContent */
/* @var $school \common\models\school\School */
/* @var $page \common\models\school\Page */

$this->title = $model->id;

?>
<div class="container">
    <div class="col-lg-12">
        <h1 class="page-header"><?= Html::encode($this->title) ?></h1>
    </div>

    <div class="col-lg-4">
        <?= $this->render('../cabinet-school/_menu', ['school' => $school]) ?>
    </div>
    <div class="col-lg-8">
        <?php if (Yii::$app->session->hasFlash('form')) { ?>
            <div class="alert alert-success">
                Успешно обновлено.
            </div>

        <?php } else { ?>

            <div class="row">
                <div class="col-lg-8">
                    <?php $form = ActiveForm::begin([
                        'id'      => 'contact-form',
                        'options' => ['enctype' => 'multipart/form-data'],
                    ]); ?>
                    <?= $form->field($model, 'type_id')->dropDownList(ArrayHelper::map(
                        \common\models\school\PageBlock::find()->all(),
                        'id',
                        'name'
                    )) ?>
                    <?= $form->field($model, 'content')->textarea(['rows' => 10]) ?>

                    <hr>
                    <div class="form-group">
                        <?= Html::submitButton('Обновить', [
                            'class' => 'btn btn-default',
                            'name'  => 'contact-button',
                            'style' => 'width:100%',
                        ]) ?>
                    </div>
                    <?php ActiveForm::end(); ?>
                </div>
            </div>

        <?php } ?>
    </div>

</div>
