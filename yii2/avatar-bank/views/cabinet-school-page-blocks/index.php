<?php

/** @var $this \yii\web\View  */
/** @var $school \common\models\school\School  */
/** @var $page \common\models\school\Page  */

use yii\helpers\Url;
use app\services\GsssHtml;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use app\services\GetArticle\Collection;

$this->title = 'Все блоки';


?>

<div class="container">
    <div class="col-lg-12">
        <h1 class="page-header"><?= Html::encode($this->title) ?></h1>
    </div>

    <div class="col-lg-4">
        <?= $this->render('../cabinet-school/_menu', ['school' => $school]) ?>
    </div>
    <div class="col-lg-8">
        <p><a href="<?= Url::to(['cabinet-school-page-blocks/add', 'id' => $page->id]) ?>" class="btn btn-default">Добавить</a>
            <a href="<?= Url::to(['cabinet-school-page-blocks/sort', 'id' => $page->id]) ?>" class="btn btn-default">Сортировать</a></p>

        <?php \yii\widgets\Pjax::begin(); ?>
        <?php
        $this->registerJS(<<<JS
$('[data-toggle="tooltip"]').tooltip();
$('.buttonDelete').click(function (e) {
    e.preventDefault();
    e.stopPropagation();
    if (confirm('Подтвердите удаление')) {
        var button = $(this);
        var id = $(this).data('id');
        ajaxJson({
            url: '/cabinet-school-page-blocks/delete' + '?' + 'id' + '=' + id,
            success: function (ret) {
                $('#modalInfo').on('hidden.bs.modal', function() {
                    button.parent().parent().remove();
                }).modal();
            }
        });
    }
});

$('.rowTable').click(function() {
    window.location = '/cabinet-school-page-blocks/edit' + '?' + 'id' + '=' + $(this).data('id');
});

JS
        );
        ?>
        <?= \yii\grid\GridView::widget([
            'dataProvider' => new \yii\data\ActiveDataProvider([
                'query'      => \common\models\school\PageBlockContent::find()
                    ->where(['page_id' => $page->id])
                    ->orderBy(['sort_index' => SORT_ASC])
                ,
                'pagination' => [
                    'pageSize' => 20,
                ],
            ]),
            'tableOptions' => [
                'class' => 'table table-striped table-hover',
            ],
            'rowOptions'   => function ($item) {
                $data = [
                    'data'  => ['id' => $item['id']],
                    'role'  => 'button',
                    'class' => 'rowTable',
                ];
                return $data;
            },
            'columns'      => [
                'id',
                [
                    'header'  => 'Тип',
                    'content' => function ($item) {
                        return \common\models\school\PageBlock::findOne($item['type_id'])->name;
                    },
                ],
                [
                    'header'  => 'content',
                    'content' => function ($item) {
                        return Html::tag('code', Html::encode(\cs\services\Str::sub($item['content'], 0,50)));
                    },
                ],
                [
                    'header'  => 'Удалить',
                    'content' => function ($item) {
                        return Html::button('Удалить', [
                            'class' => 'btn btn-danger btn-xs buttonDelete',
                            'data'  => [
                                'id' => $item['id'],
                            ],
                        ]);
                    },
                ],
            ],
        ]) ?>
        <?php \yii\widgets\Pjax::end(); ?>
    </div>

</div>

<div class="modal fade" id="modalInfo" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="myModalLabel">Информация</h4>
            </div>
            <div class="modal-body">
                Успешно!
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
            </div>
        </div>
    </div>
</div>