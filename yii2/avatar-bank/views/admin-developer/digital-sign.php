<?php

/** $this \yii\web\View  */

use yii\helpers\Url;
use app\services\GsssHtml;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use app\services\GetArticle\Collection;
use BitcoinPHP\BitcoinECDSA\BitcoinECDSA;
use iAvatar777\services\EllipticCurve\PrivateKey;
use iAvatar777\services\EllipticCurve\Ecdsa;


$this->title = 'Цифровые подписи';



$bitcoinECDSA = new BitcoinECDSA();
$bitcoinECDSA->generateRandomPrivateKey(); //generate new random private key
$bitcoinECDSA->checkSignatureForMessage();

$message = "Test messageTest messageTest messageTest messageTest messageTest messageTest messageTest messageTest messageTest messageTest messageTest messageTest messageTest messageTest messageTest messageTest messageTest messageTest messageTest messageTest messageTest messageTest messageTest messageTest messageTest messageTest messageTest message";
$signedMessage = $bitcoinECDSA->signMessage($message);



/**
 * Will print something like this:
-----BEGIN BITCOIN SIGNED MESSAGE-----
Test message
-----BEGIN SIGNATURE-----
1L56ndSQ1LfrAB2xyo3ZN7egiW4nSs8KWS
HxTqM+b3xj2Qkjhhl+EoUpYsDUz+uTdz6RCY7Z4mV62yOXJ3XCAfkiHV+HGzox7Ba/OC6bC0y6zBX0GhB7UdEM0=
-----END BITCOIN SIGNED MESSAGE-----
 */


// If you only want the signature you can do this
$signature = $bitcoinECDSA->signMessage($message, true);




$privateKey = new PrivateKey;
$publicKey = $privateKey->publicKey();

$message = "My test message";

# Generate Signature
$signature1 = Ecdsa::sign($message, $privateKey);
?>

<div class="container">
    <div class="col-lg-12">
        <h1 class="page-header"><?= Html::encode($this->title) ?></h1>

        <p><a href="https://github.com/BitcoinPHP/BitcoinECDSA.php" target="_blank">BitcoinPHP/BitcoinECDSA.php</a></p>
        <p>getAddress = <code><?= $bitcoinECDSA->getAddress() ?></code></p>
        <p>getPrivateKey = <code><?= $bitcoinECDSA->getPrivateKey() ?></code></p>
        <p>getUncompressedAddress = <code><?= $bitcoinECDSA->getUncompressedAddress() ?></code></p>
        <p>getUncompressedPubKey = <code><?= $bitcoinECDSA->getUncompressedPubKey() ?></code></p>
        <p>$signedMessage = </p>
        <pre><?= $signedMessage ?></pre>
        <p>$signature = <code><?= $signature ?></code></p>
        <p>$wif = <code><?=  $bitcoinECDSA->getWif() ?></code></p>
        <p><a href="https://github.com/i-avatar777/service-ecdsa" target="_blank">i-avatar777/service-ecdsa</a></p>
        <p>$publicKey = </p>
        <pre><?=  \yii\helpers\VarDumper::dumpAsString($publicKey) ?></pre>
        <p>$privateKey->toString() = <code><?=  $privateKey->toString() ?></code></p>
        <p>$privateKey->toPem() = </p>
        <pre><?=  $privateKey->toPem() ?></pre>
        <p>$signature1 = <code><?=  $signature1->toBase64() ?></code></p>

        <h2 class="page-header">Хранение ключа</h2>
        <p>Аватар самостоятельно выбирает режим хранения ключа, либо у нас либо у себя. Настройка отвечающая за этот параметр располагается в настройкаъ цифровой подписи. Таблица для хранения данных о цифровой подписи <code>user_digital_sign</code></p>
        <?= \common\services\documentation\DbTable::widget([
            'name'        => 'user_digital_sign',
            'model'       => '\common\models\UserDigitalSign',
            'description' => 'Цифровая подпись пользователя',
            'columns'     => [
                [
                    'name'        => 'id',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Идентификатор строки',
                ],
                [
                    'name'        => 'user_id',
                    'type'        => 'integer',
                    'isRequired'  => true,
                    'description' => 'Хозяин подписи',
                ],
                [
                    'name'        => 'created_at',
                    'type'        => 'integer',
                    'isRequired'  => true,
                    'description' => 'Момент создания',
                ],
                [
                    'name'        => 'address',
                    'type'        => 'string',
                    'isRequired'  => true,
                    'description' => 'Адрес',
                ],
                [
                    'name'        => 'private_key',
                    'type'        => 'string',
                    'isRequired'  => false,
                    'description' => 'Дополнительные данные к файлу',
                ],
                [
                    'name'        => 'address_uncompressed',
                    'type'        => 'string',
                    'isRequired'  => true,
                    'description' => 'Адрес',
                ],
                [
                    'name'        => 'public_key_uncompressed',
                    'type'        => 'string',
                    'isRequired'  => true,
                    'description' => 'Публичный адрес',
                ],
                [
                    'name'        => 'type_id',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Тип хранения подписи: 0 - пользователь хранит у себя, 1 - ключ хранится в системе. По умолчанию 0',
                ],
            ],
        ]) ?>

        <p><code>user_digital_sign_list</code> предназначена для хранения истории всех цифровых подписей, чтобы можно определить кто когда что подписывал</p>
        <?= \common\services\documentation\DbTable::widget([
            'name'        => 'user_digital_sign_list',
            'model'       => '\common\models\UserDigitalSignList',
            'description' => 'История создания цифровых подписей',
            'columns'     => [
                [
                    'name'        => 'id',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Идентификатор строки',
                ],
                [
                    'name'        => 'user_id',
                    'type'        => 'integer',
                    'isRequired'  => true,
                    'description' => 'Хозяин подписи',
                ],
                [
                    'name'        => 'created_at',
                    'type'        => 'integer',
                    'isRequired'  => true,
                    'description' => 'Момент создания',
                ],
                [
                    'name'        => 'address',
                    'type'        => 'string',
                    'isRequired'  => true,
                    'description' => 'Адрес',
                ],
            ],
        ]) ?>

        <h2 class="page-header">TON</h2>
        <p>https://github.com/tonlabs/ton-client-js/blob/049f3108861c8d65b9eaa7366854c2ca253685dd/__tests__/crypto.js#L144</p>

        <h2 class="page-header">Пользовательский интерфейс</h2>
        <p>Страница на которой можно получить цифровую подпись: <code>/cabinet-digital-sign/index</code></p>
        <p>
            <a href="https://www.draw.io/#G1z_6Rvm9wX1laTpw-mO50lkOOJP4Pa41o" target="_blank">
                <img src="/images/controller/admin-developer/digital-sign/shceme.png">
            </a>
        </p>

        <h2 class="page-header">Гос подпись</h2>
        <p>https://egrnonline.ru/esignature</p>

        <h2 class="page-header">Docker образ для подписи КЖ</h2>
        <p>https://bitbucket.org/gsss/digital-key.git</p>

        <p>Установка</p>
        <ul>
            <li>скачать https://platform.qualitylive.su/dkey.docker.tar</li>
            <li>docker load -i {path}/dkey.docker.tar</li>
            <li>docker run --name quality_life_sign -p 8080:3180 -v ./keys1:/var/www/html/runtime/keys -d iavatar/core:3.1</li>
            <li>docker exec -it quality_life_sign service php7.2-fpm start</li>
        </ul>


        <h2 class="page-header">Получение подписи версия 2</h2>
        <p>Эта подпись уже более безопасна для пользователя потому что она генерируется и формируется подпись на локальном компьютере человека</p>
        <p>Чтобы ее получить надо </p>
        <ul>
            <li>в контейнере сгенерировать подпись</li>
            <li>зайти на платформу и заполнить свой профиль: имя фамилия аватар</li>
            <li>получить на платформе хеш</li>
            <li>подписать в контейнере</li>
            <li>передать подпись в платформу</li>
        </ul>
        <p>В этом случает <code>user_digital_sign.type_id</code> = 2 и поля заполнены</p>
        <p>
            <code>user_id</code>
            <code>type_id</code>
            <code>address</code>
            <code>created_at</code>
        </p>
        <p>После подтверждения адреса и подписи, адрес заносится в историю <code>user_digital_sign_list</code> так же как и в первой версии.</p>
        <p>Сохранять ли персональные данные? да, не так много места, а для истории это очень важно.</p>
        <?= \common\services\documentation\DbTable::widget([
            'name'        => 'user_digital_sign_personal_data',
            'model'       => '\common\models\UserDigitalSignPersonalData',
            'description' => 'История подписи персональных данных',
            'columns'     => [
                [
                    'name'        => 'id',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Идентификатор строки',
                ],
                [
                    'name'        => 'user_id',
                    'type'        => 'integer',
                    'isRequired'  => true,
                    'description' => 'Хозяин подписи',
                ],
                [
                    'name'        => 'created_at',
                    'type'        => 'integer',
                    'isRequired'  => true,
                    'description' => 'Момент создания',
                ],
                [
                    'name'        => 'address',
                    'type'        => 'string',
                    'isRequired'  => true,
                    'description' => 'Адрес',
                ],
                [
                    'name'        => 'sign',
                    'type'        => 'string',
                    'isRequired'  => true,
                    'description' => 'Подпись',
                ],
                [
                    'name'        => 'name_first',
                    'type'        => 'string',
                    'isRequired'  => true,
                    'description' => 'Имя',
                ],
                [
                    'name'        => 'name_last',
                    'type'        => 'string',
                    'isRequired'  => true,
                    'description' => 'Фамилия',
                ],
                [
                    'name'        => 'avatar',
                    'type'        => 'string',
                    'isRequired'  => true,
                    'description' => 'Аватар',
                ],
                [
                    'name'        => 'hash',
                    'type'        => 'string',
                    'isRequired'  => true,
                    'description' => 'Хеш персональных данных',
                ],
            ],
        ]) ?>
    </div>
</div>


