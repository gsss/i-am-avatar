<?php

/** $this \yii\web\View  */

use yii\helpers\Url;
use app\services\GsssHtml;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use app\services\GetArticle\Collection;

$this->title = 'Разработка';


?>

<div class="container">
    <div class="col-lg-12">
        <h1 class="page-header"><?= Html::encode($this->title) ?></h1>

        <h2 class="page-header">Глобальные модули</h2>
        <p><a href="users-id">Шина пользователей. AvatarNetwork</a></p>
        <p><a href="prometey">КИИС Прометей</a></p>
        <p><a href="super-company">super-company</a></p>
        <p><a href="1c">1С</a></p>

        <h2 class="page-header">Кооператив</h2>
        <p><a href="koop">Кооператив</a></p>
        <p><a href="/admin-developer2/koop">Кооператив</a></p>
        <p><a href="school-money-income">Учет денежных вложений</a></p>
        <p><a href="counter-6percent">Счетчик на 6%</a></p>

        <h2 class="page-header">Бизнес</h2>
        <p><a href="school-busines">Бизнес план</a></p>
        <p><a href="boomstarter">Boomstarter</a></p>

        <h2 class="page-header">front-end</h2>
        <p><a href="front-end">Введение</a></p>
        <p><a href="widget-masters">widget-masters</a></p>
        <p><a href="widget-anketa">widget-anketa</a></p>
        <p><a href="jquery-library">jquery-library</a></p>
        <p><a href="https://lobanof.ru/news">lobanof.ru</a></p>

        <h2 class="page-header">Терминология</h2>
        <p>Стенд - копия платформы для рабочего использования PROD, для тестирования TEST и для разработки DEV.</p>
        <p>Приложение, точка входа - точка куда настроен веб сервер</p>
        <p>Платформа - вся совокупность системы iAvatar и все сервисы.</p>

        <h2 class="page-header">Карты</h2>
        <p><a href="card">Карта</a></p>
        <p><a href="card-number">Номер карты</a></p>
        <p><a href="avatar-id">Идентификационная карта</a></p>

        <h2 class="page-header">Программирование</h2>
        <p><a href="programming-intro">Введение</a></p>
        <p><a href="https://www.conventionalcommits.org/ru/v1.0.0-beta.4/" target="_blank">Стандарт именования комитов</a></p>
        <p><a href="base-form-active-record">База. FormActiveRecord</a></p>
        <p><a href="packagist">Packagist библиотеки iAvatar777</a></p>
        <p><a href="env">ENV</a></p>
        <p><a href="install">Установка сайта</a></p>
        <p><a href="servers">Сервера и хостеры</a></p>
        <p><a href="programming-rules">Правила для программистов</a></p>
        <p><a href="backup">Резервное копирование</a></p>
        <p><a href="licence">Лицензии</a></p>
        <p><a href="packagist">packagist</a></p>
        <p><a href="programming-file-hash">Hash от большого файла</a></p>
        <p><a href="log">Логирование</a></p>
        <p><a href="crypt">Шифрование</a></p>
        <p><a href="deploy">deploy</a></p>
        <p><a href="docker">docker</a></p>
        <p><a href="school">Организация школ</a></p>
        <p><a href="programming-ton">TON</a></p>
        <p><a href="https://ru.hexlet.io/blog/posts/environment">environment</a></p>
        <p><a href="https://habr.com/ru/post/106912/">Удачная модель ветвления для Git</a></p>
        <ul>
            <li><a href="widget-html-content3">Виджет для редактирования HTML с загрузкой файлов картинок в облако</a></li>
            <li><a href="widget-form">Виджеты для форм</a></li>
            <li><a href="widget-form-file-upload7">FileUpload7 - загружает картинку</a></li>
            <li><a href="widget-form-file-upload8">FileUpload8 - загружает любой файл</a></li>
            <li><a href="widget-form-file-upload9">FileUpload9 - загружает любой файл на локальное облако</a></li>
            <li><a href="widget-placemapyandex">\common\widgets\PlaceMapYandex\PlaceMap - Указание точки на карте</a></li>
            <li><a href="programming-default-ajax">\avatar\controllers\actions\DefaultAjax - Действие контроллера по умолчанию</a></li>
        </ul>

        <h2 class="page-header">Анкеты</h2>
        <p><a href="anketa-school">Анкета для школы</a></p>

        <h2 class="page-header">Сервисы</h2>
        <p><a href="yandex-kassa">yandex-kassa</a></p>
        <p><a href="bot-telegram">Телеграм Бот</a></p>
        <p><a href="bot-facebook">Facebook Бот</a></p>
        <p><a href="bot-vk">VK Бот</a></p>
        <p><a href="chat-helper">Чат помощник на сайте</a></p>
        <p><a href="plugin-export">Плагины на экспорт</a></p>
        <p>https://chat-api.com/ru/ - WhatsApp</p>

        <h2 class="page-header">Деньги</h2>
        <p><a href="school-money">Деньги</a></p>
        <p><a href="school-pay">Платный вход</a></p>
        <p><a href="processing">Процессинг ЯАватар</a></p>

        <h2 class="page-header">Интерфейс</h2>
        <p><a href="master-window">Мастер окно</a></p>

        <h2 class="page-header">Модели поведения (User Expiriense)</h2>

        <p><a href="school-user-models">Пользователи (Модели поведения)</a></p>
        <p><a href="school-user-models-registration">Регистрация на ЯАватар</a></p>
        <p><a href="user-models-buy-elixir">Купить Эликсир</a></p>
        <p><a href="school-user-models-card-activate">Активация карты ЯАватар</a></p>
        <p><a href="school-user-models-iavatar">Регистрация на ЯАватар</a></p>
        <p><a href="school-user-models-telegram">Регистрация через телеграм</a></p>
        <p><a href="blago-token">Получение токенов после благотворительности</a></p>

        <p>Как вообще можно? 1. На вебинар просто записаться, 2. На вебинар с уроком в кабинете. 3. На вебинар с уроком в кабинете с оплатой.</p>
        <p><a href="school-user-models-vebinar"> На вебинар просто записаться</a></p>
        <p><a href="school-user-models-kurs">    На курс с уроком в кабинете</a></p>
        <p><a href="school-user-models-kurs-pay">На курс с уроком в кабинете с оплатой</a></p>
        <p><a href="https://docs.google.com/document/d/19LtqJ4KAKNnN_ezolskD1vg1Ygzhh7RU_UnsGfZEFeo/edit?usp=sharing">https://docs.google.com/document/d/19LtqJ4KAKNnN_ezolskD1vg1Ygzhh7RU_UnsGfZEFeo/edit?usp=sharing</a> От Надежды</p>
        <p><a href="/admin-developer/reg-payer">Регистрация пайщика</a></p>
        <p><a href="/admin-developer/contr-agree">Договор подряда</a></p>
        <p><a href="">Добавление задачи и принятие</a></p>
        <p><a href="">Добавление документа</a></p>
        <p><a href="">Подпись документ</a>а</p>
        <p><a href="/admin-developer/execution-of-works">Выполнение работ</a></p>
        <p><a href="/admin-developer/profit-distribution">Распределение прибыли</a></p>
        <p><a href="/admin-developer/school-settings-coin">Сообщество. Настройки. Монета</a> (school-settings-coin)</p>

        <h2 class="page-header">Мобильное приложение</h2>
        <p><a href="mobile">Мобильное приложение</a></p>

        <h2 class="page-header">Сообщества / Кооперативы / Школы</h2>
        <p><a href="lenta">Лента</a></p>
        <p><a href="digital-sign">Цифровые подписи</a></p>
        <p><a href="modules">Модуль для школы</a></p>
        <p><a href="school-users">Пользователи</a></p>
        <p><a href="documents">Документы</a></p>
        <p><a href="school-potok">Поток / группа</a></p>
        <p><a href="school-events">События</a> <a href="school-events-add">Страница добавления</a></p>
        <p><a href="reklama">Рекламные матриалы</a></p>
        <p><a href="school-files">Файлы проекта</a></p>
        <p><a href="help">Помощь по платформе</a></p>
        <p><a href="sec">Безопасность</a></p>
        <p><a href="smeta">Смета</a></p>
        <p><a href="pay-systems">Платежные системы</a></p>
        <p><a href="school-partner">Партнерская программа</a></p>
        <p><a href="school-domain">Домен</a></p>
        <p><a href="school-role">Роли</a></p>
        <p><a href="school-vote">Голосования</a></p>
        <p><a href="school-blog">Блог</a></p>
        <p><a href="school-news">Новости</a></p>
        <p><a href="school-subscribe">Рассылки</a></p>
        <p><a href="school-cloud">Облако</a></p>
        <p><a href="school-task-list">Задачи</a></p>
        <p><a href="school-task-list-calendar">Задачи. Календарный вид</a></p>
        <p><a href="school-tilda">Импорт страниц с тильды</a></p>
        <p><a href="user-anketa">Анкетирование. Дополнительные поля при заявке</a></p>
        <p><a href="school-sms">СМС рассылка</a></p>
        <p><a href="school-pages">Конструктор сайтов</a></p>
        <ul>
            <li><a href="school-pages-block2">Блок 2. Видео YouTube</a></li>
            <li><a href="school-pages-block35">Блок 35. Подвал на три блока</a></li>
            <li><a href="school-pages-block37">Блок 37. Соц сети</a></li>
            <li><a href="school-pages-block41">Блок 41. Продажа курса</a></li>
            <li><a href="school-pages-block42">Блок 42. Уведомление об использовании файлов cookie</a></li>
            <li><a href="school-pages-block45">Блок 45. Интернет магазин. Каталог всех товаров</a></li>
        </ul>
        <p><a href="vebinar">Вебинар</a></p>
        <p><a href="school-autovoronki">Автоворонки</a></p>
        <p><a href="school-uroki">Уроки</a></p>
        <p><a href="school-dz">Уроки. Домашнее задание</a></p>
        <p><a href="school-input">Плагины для входа данных</a></p>
        <p><a href="school-support">Служба поддержки</a></p>
        <p><a href="koop-podryad">Подряд в кооперативе</a></p>

        <p><a href="shop">Магазин</a></p>
        <p><a href="shop-order">Страница заказа</a></p>
        <p><a href="shop-delivery">Доставка</a></p>

        <h2 class="page-header">Ethereum</h2>
        <p><a href="contract-meta-mask">Сделать исполнение контракта на странице Аватара при помощи MetaMask</a></p>

        <h2 class="page-header">nodejs</h2>
        <p><a href="nodejs">Установка</a></p>

        <h2 class="page-header">Отдельная школа</h2>
        <p><a href="s-index">Введение</a></p>
        <p><a href="school-settings">Настройки</a></p>
        <p><a href="school-pages-at-school">Страницы</a></p>


        <h2 class="page-header">Тестирование</h2>
        <p><a href="test">Тестирование</a></p>

        <h2 class="page-header">Внешние сервисы для интеграции</h2>
        <p><a href="mastodon">mastodon</a></p>

        <p><a href="feniks">Огненный феникс</a></p>


        <h2 class="page-header">Сопутствующие статьи</h2>
        <p><a href="get-course">get-course</a></p>

        <p><a href="https://cloud.yandex.ru/services/speechkit" target="_blank">https://cloud.yandex.ru/services/speechkit</a></p>

    </div>
</div>