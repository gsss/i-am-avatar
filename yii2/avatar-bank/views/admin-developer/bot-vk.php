<?php

/** $this \yii\web\View  */

use yii\helpers\Url;
use app\services\GsssHtml;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use app\services\GetArticle\Collection;

$this->title = 'VK Бот';


?>

<div class="container">
    <div class="col-lg-12">
        <h1 class="page-header"><?= Html::encode($this->title) ?></h1>

        <p><a href="https://github.com/jumper423/yii2-vk" target="_blank">https://github.com/jumper423/yii2-vk</a></p>
        <p><a href="https://habr.com/post/329150/" target="_blank">Как написать чат-бота на PHP для сообщества ВКонтакте</a></p>
        <p><a href="https://vk.com/dev/callback_api" target="_blank">Callback API</a></p>
        <p>https://i-am-avatar.com/vk/call-back</p>


    </div>
</div>

