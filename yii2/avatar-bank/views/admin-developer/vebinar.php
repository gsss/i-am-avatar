<?php

/** $this \yii\web\View  */

use yii\helpers\Url;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;

$this->title = 'Вебинар';


?>

<div class="container">
    <div class="col-lg-12">
        <h1 class="page-header"><?= Html::encode($this->title) ?></h1>


        <p>Ссылка на вебинар: <code>/vebinar/item?id=12</code>. Доступна в суперкомпании и компании. Идентикатор компании это состояние урока. Управление вебинарной комнатой производится из управления уроком. В управлении уроком содержится и презентации и кнопки управления.</p>

        <h4 class="page-header">Чат</h4>
        <p>Пока сделаю чат с пользователями которые идентифицируются через <code>SESSION_ID</code> потому что на чат в основным приходят одноразовые люди котрые не зарегистированы ранее были.</p>
        <p>Сервис чата <code>https://chat.i-am-avatar.com</code></p>
        <p>Отправить сообщение на сервер о новом пользователе <code>room-new-user</code> (room, user)</p>
        <p>Отправить сообщение на сервер <code>room-send-message</code> (room, user, message)</p>

        <?= \common\services\documentation\DbTable::widget([
            'name'          => 'vebinar_chat',
            'description'   => 'Чат для вебинара',
            'model'         => '\common\models\VebinarChatItem',
            'columns'       => [
                [
                    'name'        => 'id',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Идентификатор строки',
                ],
                [
                    'name'        => 'vebinar_id',
                    'type'        => 'varchar',
                    'isRequired'  => true,
                    'description' => 'Идентификатор вебинара',
                ],
                [
                    'name'        => 'session_id',
                    'type'        => 'varchar',
                    'isRequired'  => true,
                    'description' => 'Идентификатор сессии',
                ],
                [
                    'name'        => 'text',
                    'type'        => 'string',
                    'isRequired'  => true,
                    'description' => 'текст сообщения',
                ],
                [
                    'name'        => 'created_at',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Момент регистрации событие',
                ],
            ],
        ]) ?>




    </div>
</div>

