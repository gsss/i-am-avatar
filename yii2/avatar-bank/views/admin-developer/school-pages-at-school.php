<?php

/** $this \yii\web\View  */

use yii\helpers\Url;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;

$this->title = 'Страницы';


?>

<div class="container">
    <div class="col-lg-12">
        <h1 class="page-header"><?= Html::encode($this->title) ?></h1>


        <p>
            <a href="https://www.draw.io/#G1yh28AVhQUykSzfhv-tfrTJdGS-EklSkG" target="_blank">
                <img src="/images/controller/admin-developer/school-pages-at-school/algor.png">
            </a>
        </p>
        <p>Где этот алгоритм реализуется? в странице <code>school/views/page/page.php</code> при рисовании страницы. При этом используется шаблон <code>blank</code>.</p>
        <p>А что если перенести все это в шаблон main  и blank? Да актуально. Перенес в итоге только в main но там ритуется конечно же только главная шапка.</p>

        <h2 class="page-header">Как вообще определяется шаблон?</h2>
        <p>страница в конструкторе <code>blank</code><br>
            кабинет - <code>cabinet</code><br>
            системные <code>main</code></p>

        <h2 class="page-header">Как вообще определяется шаблон?</h2>
        <p><a href="https://docs.google.com/spreadsheets/d/1bxQR_ZtLrsMqn5Zq18G-S5ROhG4dEoobimAoTQkt1Ak/edit?usp=sharing" target="_blank">Шаблоны</a></p>
        <?php

        $rows = [
            [
                'name' => 'авторизаця',
                'layout' => '',
            ],
            [
                'name' => 'Магазин',
                'layout' => '',
            ],

        ];
        ?>
    </div>
</div>

