<?php

/** $this \yii\web\View  */

use yii\helpers\Url;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;

$this->title = 'Школа. Файлы';


?>

<div class="container">
    <div class="col-lg-12">
        <h1 class="page-header"><?= Html::encode($this->title) ?></h1>

        <h2 class="page-header">Структура файлов GIT</h2>
        <p>В таблице школы есть поле <code>school.files_max_size</code> - колво мегабайт которое можно загрузить в облако</p>
        <p>Кол-во отводимого места зависит от выбранного <a href="/site/tarif">тарифа</a>.</p>
        <p>Занимаемое место складывается из файлов для лендингов, картинок для блога, Картинки в интернет магазине и др.</p>
        <p>Где смотреть занимаемое место? <code>/cabinet-school-files/index</code> - файлы проекта</p>

    </div>
</div>

