<?php

/** $this \yii\web\View  */

use yii\helpers\Url;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;

$this->title = 'Конструктор сайтов. Блок 2. Видео YouTube';

?>

<div class="container">
    <div class="col-lg-12">
        <h1 class="page-header"><?= Html::encode($this->title) ?></h1>

        <h2 class="page-header">Настройки в БД</h2>
        <pre><code class="json">{"id":"u-b_I_UWSIM"}</code></pre>

        <p>В поле надо указать идентификатор видео YouTube<br>
            Можно указать адрес сайта www.youtube.com или youtu.be и скрипт сам выберет нужные данные</p>

        </div>
    </div>
</div>

