<?php

/** $this \yii\web\View  */

use yii\helpers\Url;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;

$this->title = 'MASTODON';


?>

<div class="container">
    <div class="col-lg-12">
        <h1 class="page-header"><?= Html::encode($this->title) ?></h1>

        <h2 class="page-header">Концепт</h2>

        <p>https://www.youtube.com/watch?v=Gtl2iSmlGJM</p>
        <p><a href="https://tech.oeru.org/installing-mastodon-docker-compose-ubuntu-1604" target="_blank">Installing Mastodon with Docker-Compose on Ubuntu 16.04</a></p>
        <p><a href="https://github.com/Wonderfall/docker-mastodon" target="_blank">github docker-mastodon</a></p>
        <p><a href="https://www.linode.com/docs/applications/messaging/install-mastodon-on-ubuntu-1604/" target="_blank">How to Install Mastodon on Ubuntu 16.04</a></p>


    </div>
</div>

