<?php

/** $this \yii\web\View  */

use yii\helpers\Url;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;

$this->title = 'Конструктор сайтов. Блок 35. Подвал на три блока';

?>

<div class="container">
    <div class="col-lg-12">
        <h1 class="page-header"><?= Html::encode($this->title) ?></h1>

        <p>В подвале есть три блока. </p>
        <p>Это базовая конфигурация:</p>

        <pre>{
"block1": [
{
"type": "link",
"href": "https://www.i-am-avatar.com",
"text": "https://www.i-am-avatar.com"
}
],
"block2": [

],
"block3": [
{
"type":"text",
"text":"info@i-am-avatar.com"
}
]
}</pre>
        <p>Состоит из трех объектов нод названием <code>block1</code>, <code>block2</code>, <code>block3</code></p>
        <p>Описание объекта внутри массива.</p>

        <?= \avatar\services\Params::widget([
            'params' => [
                [
                    'name' => 'type',
                    'isRequired' => true,
                    'description' => 'Тип блока, может быть link и text',
                ]
            ]
        ]) ?>
        <p>Объект типа <code>link</code>. Дополнительные поля</p>
        <?= \avatar\services\Params::widget([
            'params' => [
                [
                    'name' => 'text',
                    'isRequired' => true,
                    'description' => 'Текст ссылки',
                ],
                [
                    'name' => 'target',
                    'description' => 'цель ссылки',
                ],
            ]
        ]) ?>
        <p>Объект типа <code>text</code>. Дополнительные поля</p>
        <?= \avatar\services\Params::widget([
            'params' => [
                [
                    'name' => 'text',
                    'isRequired' => true,
                    'description' => 'Текст',
                ],
            ]
        ]) ?>



        </div>
    </div>
</div>

