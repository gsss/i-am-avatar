<?php

/** $this \yii\web\View  */

use yii\helpers\Url;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;

$this->title = 'Документы';


?>

<div class="container">
    <div class="col-lg-12">
        <h1 class="page-header"><?= Html::encode($this->title) ?></h1>

        <h2 class="page-header">Подпись статьи</h2>

        <p>Этот сервис формирует файл JSON. Размещает его в облаке и размещает данные о документе в контракте для подписи документов <code>0x52eD3C202c4652F952a1561Ac0c030f1eD9460fF</code></p>
        <p>Делается на странице (роутере) <code>cabinet-school-blog/index</code> школы</p>
        <p>Порядок:</p>
        <ul>
            <li>Вызывается <code>/cabinet-school-blog/sign</code> этот скрипт, отправляет файл в облако и получает ссылку на файл и хеш.</li>
            <li>Ссылка на файл и хеш размещаются в контракте регистрации документов и получаем идентификатор транзакции.</li>
            <li>Далее вызывается <code>/cabinet-school-blog/register</code> и эти все данные записываются в таблицу документов <code>user_documents</code></li>
        </ul>

        <?= \common\services\documentation\DbTable::widget([
            'name'        => 'user_documents',
            'model'       => '\common\models\UserDocument',
            'description' => 'Подписанные документы',
            'columns'     => [
                [
                    'name'        => 'id',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Идентификатор строки',
                ],
                [
                    'name'        => 'user_id',
                    'type'        => 'integer',
                    'isRequired'  => true,
                    'description' => 'Автор внесения документа',
                ],
                [
                    'name'        => 'type_id',
                    'type'        => 'integer',
                    'isRequired'  => true,
                    'description' => 'Тип подписи. 1 - Ethereum, 2 - AvatarChain',
                ],
                [
                    'name'        => 'created_at',
                    'type'        => 'integer',
                    'isRequired'  => true,
                    'description' => 'Момент создания',
                ],
                [
                    'name'        => 'school_id',
                    'type'        => 'integer',
                    'isRequired'  => true,
                    'description' => 'Идентификатор школы',
                ],
                [
                    'name'        => 'name',
                    'type'        => 'string',
                    'isRequired'  => true,
                    'description' => 'Наименование документа',
                ],
                [
                    'name'        => 'txid',
                    'type'        => 'string',
                    'isRequired'  => true,
                    'description' => 'Идентификатор транзакции в Ethereum',
                ],
                [
                    'name'        => 'data',
                    'type'        => 'string',
                    'isRequired'  => false,
                    'description' => 'Дополнительные данные к файлу',
                ],
                [
                    'name'        => 'file',
                    'type'        => 'string',
                    'isRequired'  => false,
                    'description' => 'Ссылка к файлу локальная',
                ],
                [
                    'name'        => 'hash',
                    'type'        => 'string',
                    'isRequired'  => false,
                    'description' => 'Хеш файла',
                ],
                [
                    'name'        => 'link',
                    'type'        => 'string',
                    'isRequired'  => false,
                    'description' => 'Ссылка на файл',
                ],
                [
                    'name'        => 'signature',
                    'type'        => 'string(160)',
                    'isRequired'  => false,
                    'description' => 'Подпись для документа',
                ],
            ],
        ]) ?>
        <?= \common\services\documentation\DbTable::widget([
            'name'        => 'user_documents_signatures',
            'model'       => '\common\models\UserDocumentSignature',
            'description' => 'Подпись',
            'columns'     => [
                [
                    'name'        => 'id',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Идентификатор строки',
                ],
                [
                    'name'        => 'document_id',
                    'type'        => 'integer',
                    'isRequired'  => true,
                    'description' => 'Идентификатор документа',
                ],
                [
                    'name'        => 'user_id',
                    'type'        => 'integer',
                    'isRequired'  => true,
                    'description' => 'Автор внесения документа',
                ],
                [
                    'name'        => 'created_at',
                    'type'        => 'integer',
                    'isRequired'  => true,
                    'description' => 'Момент создания',
                ],
                [
                    'name'        => 'txid',
                    'type'        => 'string',
                    'isRequired'  => true,
                    'description' => 'Транзакция',
                ],
                [
                    'name'        => 'member',
                    'type'        => 'string',
                    'isRequired'  => true,
                    'description' => 'Идентификатор подписывающего',
                ],
                [
                    'name'        => 'signature',
                    'type'        => 'varchar(160)',
                    'isRequired'  => false,
                    'description' => 'Подпись для документа',
                ],
            ],
        ]) ?>

    </div>
</div>

