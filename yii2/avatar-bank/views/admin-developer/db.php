<?php

/** $this \yii\web\View  */

use yii\helpers\Url;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;

$this->title = 'Обслуживание БД';


?>

<div class="container">
    <div class="col-lg-12">
        <h1 class="page-header"><?= Html::encode($this->title) ?></h1>

        <p>Скопировать на тест</p>

        <p>На PROD экспортировать БД</p>
        <pre>docker exec i-am-avatar-mysql mysqldump -uroot -p$1 i_am_avatar_prod_main | gzip > /root/i-am-avatar/db/dumps/i_am_avatar_prod_main.sql.gz
docker exec i-am-avatar-mysql mysqldump -uroot -p$1 i_am_avatar_prod_wallet | gzip > /root/i-am-avatar/db/dumps/i_am_avatar_prod_wallet.sql.gz</pre>

        <p>залить с PROD на STAGE</p>
        <pre></pre>

        <p>залить на STAGE</p>
        <pre></pre>        
        
        
        <h2 class="page-header">Расчет</h2>
        <p><code>\avatar\modules\HumanDesign\calculate\HumanDesignAmericaCom</code></p>
        <p><code>user.human_design</code></p>
    </div>
</div>

