<?php

/** $this \yii\web\View  */

use yii\helpers\Url;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;

$this->title = 'Школа. Пользователи';


?>

<div class="container">
    <div class="col-lg-12">
        <h1 class="page-header"><?= Html::encode($this->title) ?></h1>

        <!-- Nav tabs -->
        <ul class="nav nav-tabs" role="tablist">
            <li role="presentation" class="active"><a href="#clear" aria-controls="clear" role="tab" data-toggle="tab">Чистовик</a></li>
            <li role="presentation"><a href="#potok" aria-controls="potok" role="tab" data-toggle="tab">Поток</a></li>
        </ul>

        <!-- Tab panes -->
        <div class="tab-content">
            <div role="tabpanel" class="tab-pane in active" id="clear">
            </div>
            <div role="tabpanel" class="tab-pane fade" id="potok">
                <h2 class="page-header">Пользователи в школе</h2>
                <p>Учитель может загрузить свою базу с клиентами, учениками.</p>
                <p>Как в таком случае клиент получает доступ в кабинет?</p>
                <p>1. Рассылка с первым входом.</p>
                <p>2. Каждый восстанавливает себе пароль. и входит. Не удобнее, но проще програмировать, потому что нечего
                    програмировать.</p>
                <p>Как в таком случае человек может быть в нескольких школах?</p>
                <p>1. Регистрируется в каждой по отдельности.</p>

                <h3 class="page-header">Организация пользователей в системе</h3>
                <p>Какое смущение меня здесь одолевает? Это взялось с GetCourse. Там пользователь может быть в разных школах. И у меня вопрос нужно ли две таблицы создавать чтобы создавать еще иллюзорные тела ученика для каждой школы или можно обойтись одной таблицей, может с какой то доработкой?</p>
                <p>Попробую методом эксперимента. А можно ли обойтись одной таблицей. А что мне надо реализовать чтобы проверить?</p>
                <p>Пользователь состоит в одной школе и в другой не состоит.</p>
                <p>Еще надо реализовать загрузку базы данных пользователей в школу.</p>
                <p>Когда пользователь регистрируется на основном сайте, то он создается в таблице user</p>
                <p><img src="/images/controller/admin-developer/school/2018-12-18_10-05-20.png"></p>
                <p>Я могу сделать user.id - int user2.id - big int при этом user2.email может повторяться чтобы пользователь мог оставлять email c разными телефонами и никами а вот при регистрации уже только один и неповторимый email.</p>
                <p>Вопрос тогда на какое поле ссылается таблца <code>user_school_link</code>? Очевидно что на user2 так как в общем списке школы может быть и люди из базы и люди из системы.</p>
                <p>Какая ситуация мб? Пользователь регится и в системе он накапливается на несколько школ а в через некоторое время регистрируется и тогда в некоторых школах он будет светиться как лид а в одной как пользователь системы а надо чтобы везде был виден как пользователь, тогда надо ввести поле user_id в таблицу user2. И при регистрации заполнять все одинаковые email в таблицу user2.</p>
                <p>Еще хочу сделать поле user2_id в таблице user чтобы понимать с какого лида пользователь зарегистрировался.</p>
                <p><img src="/images/controller/admin-developer/school/2019-01-03_05-52-19.png"></p>
                <p>При такой конструкции все потребности решаются, но еще есть потребность загрузить базу со своими клиентами. Или например пользователь регистрируется в школе. Вопрос как он связывается со школой?</p>
                <p>Значит надо записывать в поток и/или в user_school_link, так вреде все укладывается, все потребности.</p>
                <p style="font-weight: bold;">Пользователь регистрируется на событие</p>
                <p style="font-style: italic;">Ситуация 1. Его нет еще в БД</p>
                <p><code>user3</code> записывается если его там нет.</p>
                <p><code>user_school_link</code> записывается соответственно школе</p>
                <p><code>school_potok_user3_link</code> добавляется лид</p>
                <p><code>school_potok_user3_ext</code> добавляется расширенные данные о госте</p>
                <p style="font-style: italic;">Ситуация 2. Пользователь уже был зарегистрирован в перврй ситуации</p>
                <p><code>user_school_link</code> записывается соответственно школе, если его там еще нет, если это новая школа</p>
                <p><code>school_potok_user3_link</code> добавляется лид</p>
                <p><code>school_potok_user3_ext</code> добавляется расширенные данные о госте</p>
                <p style="font-style: italic;">Ситуация 3. Пользователь регистриуется в школу или на платформе</p>
                <p><code>user</code> записывается, если его там еще нет</p>
                <p style="font-style: italic;">Ситуация 4. Мастер добавляет свою БД</p>
                <p><code>user3</code> записывается, если его там еще нет</p>
                <p><code>user_school_link</code> записывается соответственно школе</p>
                <p>Как сделать чтобы записывались доп данные о пользователе?</p>
                <p><img src="/images/controller/admin-developer/school/2019-01-03_06-46-06.png"></p>
                <p>Поля <code>link_id</code> и <code>user3_id</code> в таблице <code>school_potok_user3_ext</code> взаимоисключающие. Таким образом для каждого события, потока записывается свои контактные данные пользователя.</p>
                <p>Таблица <code>school_potok_user3_ext</code> является зеркалом таблицы <code>user</code> чтобы хранить небольшую информацию о пользователе и не перегружать таблицу <code>user</code>.</p>
                <pre>select * from user3
inner join user_school_link on (user_school_link.user3_id = user3.id)
</pre>
                <p><img src="/images/controller/admin-developer/school-users/2019-01-03_17-29-04.png"></p>
                <p>
                    <img src="/images/controller/admin-developer/school-users/users.png">
                </p>

                <h4 class="page-header">Исследование</h4>
                <p>Какие запросы надо всегда обеспечить? И при этом обспечить не повторяемость пользователей (email).</p>
                <p>1. Кто в школе</p>
                <p>2. Кто записался на событие</p>

                <h4 class="page-header">User Expirience</h4>
                <p>Когда пользователь должен регистрироваться на платформе?</p>
                <p>Если подписаться на безоплатный вебинар, то надо? Ему только приходит рассылка по почте. Значит не надо, но надо подтвердить свой email.</p>
                <p>Если подписаться на закрытый по паролю вебинар, то надо? Надо потому что он дожен зайти на платформу. А что в иаком случае ему надо сделать? Подтвердить почту, далее ему предлагается заполнить пароль и он входит в кабинет.</p>
                <p>Если покупает вебинар, то надо? Да, схема действий таже.</p>
                <p><b>Подтверждение почты</b></p>
                <p>Стоит ли делать подтверждение по почте?</p>
                <p>Ситуация может быть такая что если пользователь не подтвердит почту то он не добавится. А может он в одной букве ошибся. А так можно было бы по телефону созвониться.</p>
                <p>Поэтому вывод такой: добавлять но высылать письмо на проверку почты, а при рассылке если не подтвержден то не высылать ему рассылку.</p>
                <p>Какие варианты тогда могут быть для потока?</p>
                <p>1. Если допустим вебинар платный то человек должен вообще зарегистрироваться на платформу и конечно подтвердить почту. При этом если он попал сначала как лид но не оплатил, то он попадает в список обычный, если же оплатил, то для таких пользователей создается отдельный статус и он в него переводится.</p>
                <p>2. Если допустим вебинар безоплатный то он попадает как лид но с флагом что не подтвердил почту.</p>
                <p>3. Если допустим событие платное, но на входе оплачивает...</p>
                <p>В итоге прихожу к выводу что всех записываю, только если подтверждает то флаг ему ставлю что подтвержден он.</p>
                <p><b>Решение</b></p>
                <p>Будет статус <code>school_kurs.status</code></p>
                <p>1 - только подтверждение почты</p>
                <p>2 - подтверждение и регистрация</p>
                <p><a href="https://www.draw.io/#G1T0AG18eP2ULyEivblRPJjyA2IQcygVmu" target="_blank"><img src="/images/controller/admin-developer/school-users/map.png"></a></p>
                <p><b>Регистрация</b></p>
                <p>При регистрации заносится <code>user_root</code> и <code>user_password</code> и <code>user_register</code>.
                    После активации создается user и удаляется <code>user_password</code> и <code>user_register</code>.</p>
                <p>А как отправит уведомление о том что он записался на вебинар если при подтверждении никаких данных нет?</p>
                <p>Почему же? Смотрим в таблицу <code>school_potok_user_root_link</code> и там записано куда он записался. и так высылаем ему письмо что все в силе.</p>


                <h3 class="page-header">Роли</h3>
                <p>Администратор школы <code>role_school_admin</code> - Может редактировать все параметры школы, его назначает и снимает творец школы.</p>
                <p>Учитель школы <code>role_school_teacher</code> - Может создавать и редактировать свои курсы, проверяет и ставит оценки за домашние задания.</p>
                <p>Менеджер школы <code>role_school_manager</code> - Может управлять заявками.</p>
                <p>Ученик школы <code>role_school_disciple</code> - Может учиться и выполнять домашние задания.</p>

                <p>Доступ к школе получают только администраторы.</p>
                <p>Доступ к добавлению только создателю школы.</p>

                <h4 class="page-header">Администраторы</h4>
                <?= \common\services\documentation\DbTable::widget([
                    'name'    => 'school_admin_link',
                    'columns' => [
                        [
                            'name'        => 'id',
                            'type'        => 'int',
                            'isRequired'  => true,
                            'description' => 'Идентификатор строки',
                        ],
                        [
                            'name'        => 'school_id',
                            'type'        => 'int',
                            'isRequired'  => true,
                            'description' => 'Идентификатор школы',
                        ],
                        [
                            'name'        => 'user_id',
                            'type'        => 'varchar(10)',
                            'isRequired'  => true,
                            'description' => 'Идентификатор пользователя',
                        ],
                    ],
                ]) ?>
                <p>Добавить администратора может только создатель школы.</p>
                <p>Контроль доступа осуществляеься с помощью контроллера <code>\avatar\controllers\CabinetSchoolBaseController</code>.</p>

                <h3 class="page-header">Связь школы и учителя</h3>
                <?= \common\services\documentation\DbTable::widget([
                    'name'    => 'school_teacher_link',
                    'columns' => [
                        [
                            'name'        => 'id',
                            'type'        => 'int',
                            'isRequired'  => true,
                            'description' => 'Идентификатор строки',
                        ],
                        [
                            'name'        => 'school_id',
                            'type'        => 'int',
                            'isRequired'  => true,
                            'description' => 'Идентификатор школы',
                        ],
                        [
                            'name'        => 'user_id',
                            'type'        => 'varchar(10)',
                            'isRequired'  => true,
                            'description' => 'Идентификатор пользователя',
                        ],
                    ],
                ]) ?>

                <h3 class="page-header">Карточка школы, мастера и ученика</h3>
                <p><a href="https://roomble.com/pro/id12594/" target="_blank">https://roomble.com/pro/id12594/</a></p>

                <p>Карточка ученика и учителя открываются по одной ссылке <code>/user/&lt;user_root_id&gt;</code>. В каждой школе свой массив учеников. Если ученик не зарегистрирован в школе но в других он есть то будет показано что нет такого ученика и код 404.</p>

                <p><a href="https://bootsnipp.com/snippets/O5Z4d" target="_blank">https://bootsnipp.com/snippets/O5Z4d</a></p>

                <h3 class="page-header">Делаю добавление LID</h3>
                <p>Какие могут быть комбинации в струтуре таблиц Пользователи.</p>
                <p>1. Пользователь зарегистрирован в Аватаре. <code>user_root</code>, <code>user</code></p>
                <p>2. Пользователь зарегистрирован в школе. <code>user_root</code>, <code>user</code>, <code>user_school_link</code></p>
                <p>3. LID. <code>user_root</code>, <code>school_potok_user_root_link</code>, <code>school_potok_user_ext</code>, <code>user_school_link</code>.</p>
                <p>4. Пользователь из внешней БД. <code>user_root</code>, <code>user_root_ext</code>, </p>

                <p>Еще надо добавить в каком статусе находится и что делает. Например если пользователь уже зарегистрированный на платформе и хочет записаться на курс то он добавляется только в поток <code>school_potok_user_root_link</code></p>

                <p>
                    <a href="https://docs.google.com/spreadsheets/d/1U-2ud9ecXdaeaGcCFc2uowgvGBGBG5KxcICRiFEIeGo/edit?usp=sharing" target="_blank">
                        <img src="/images/controller/admin-developer/school-users/2019-02-15_03-58-24.png" class="thumbnail">
                    </a>
                </p>

                <h3 class="page-header">Регистрация</h3>
                <p>
                    <a href="https://www.draw.io/#G1giAR7WKkVFs31E9woLGxvVYAZ-FLK3uD" target="_blank">
                        <img src="/images/controller/admin-developer/school-users/registration.png" class="thumbnail">
                    </a>
                </p>
                <h3 class="page-header">Регистрация в школе</h3>
                <h2 class="page-header">Пользователь</h2>
                <?= \common\services\documentation\DbTable::widget([
                    'name'    => 'user',
                    'columns' => [
                        [
                            'name'        => 'id',
                            'type'        => 'int',
                            'isRequired'  => true,
                            'description' => 'Идентификатор строки',
                        ],
                        [
                            'name'        => 'pin',
                            'type'        => 'varchar(60)',
                            'isRequired'  => false,
                            'description' => 'Предполагался пин код пользователя для карт и для моб приложения, сейчас не используется',
                        ],
                        [
                            'name'        => 'password',
                            'type'        => 'varchar(60)',
                            'isRequired'  => true,
                            'description' => 'Хеш пароля',
                        ],
                        [
                            'name'        => 'email',
                            'type'        => 'varchar(60)',
                            'isRequired'  => true,
                            'description' => 'почта, все малые',
                        ],
                        [
                            'name'        => 'name_first',
                            'type'        => 'varchar(100)',
                            'isRequired'  => true,
                            'description' => 'Имя',
                        ],
                        [
                            'name'        => 'name_last',
                            'type'        => 'varchar(100)',
                            'isRequired'  => true,
                            'description' => 'Фамилия',
                        ],
                        [
                            'name'        => 'name_middle',
                            'type'        => 'varchar(100)',
                            'isRequired'  => true,
                            'description' => 'Фамилия',
                        ],
                        [
                            'name'        => 'email_is_confirm',
                            'type'        => 'tinyint',
                            'isRequired'  => false,
                            'description' => 'Флаг, подтверждена почта? Не используется',
                        ],
                        [
                            'name'        => 'telegram_username',
                            'type'        => 'int',
                            'isRequired'  => false,
                            'description' => 'telegram username',
                        ],
                        [
                            'name'        => 'telegram_chat_id',
                            'type'        => 'int',
                            'isRequired'  => false,
                            'description' => 'Идентификатор чата telegram',
                        ],
                        [
                            'name'        => 'is_hide_master_enter_window',
                            'type'        => 'tinyint',
                            'isRequired'  => false,
                            'description' => 'скрыто окно Мастера при старте? 0 - не скрыто - показывать, 1 - скрыто - не показывать. По умолчанию - 0',
                        ],
                        [
                            'name'        => 'birth_lat',
                            'type'        => 'double',
                            'isRequired'  => false,
                            'description' => 'Где родился, LAT',
                        ],
                        [
                            'name'        => 'birth_lng',
                            'type'        => 'double',
                            'isRequired'  => false,
                            'description' => 'Где родился, LNG',
                        ],
                        [
                            'name'        => 'subscribe_is_news',
                            'type'        => 'int',
                            'isRequired'  => false,
                            'description' => 'Подписка на новости, . По умолчанию - 1',
                        ],
                        [
                            'name'        => 'subscribe_is_blog',
                            'type'        => 'int',
                            'isRequired'  => false,
                            'description' => 'Подписка на блог, . По умолчанию - 1',
                        ],
                        [
                            'name'        => 'mark_deleted',
                            'type'        => 'int',
                            'isRequired'  => false,
                            'description' => 'Флаг. Удален пользователь?, . По умолчанию - 0',
                        ],
                        [
                            'name'        => 'email_is_confirm',
                            'type'        => 'int',
                            'isRequired'  => false,
                            'description' => 'Флаг. Подтвержден email?, . По умолчанию - 0, не используется',
                        ],
                        [
                            'name'        => 'wallets_is_locked',
                            'type'        => 'int',
                            'isRequired'  => false,
                            'description' => 'Флаг. Кошельки заблокированы?, . По умолчанию - 0, не используется',
                        ],
                        [
                            'name'        => 'is_master',
                            'type'        => 'int',
                            'isRequired'  => false,
                            'description' => 'Флаг. показывает участвует ли человек в каталоге учителей, 0 - не участвует, 1 - участвует, 0 - по умолчанию',
                        ],
                        [
                            'name'        => 'card_action',
                            'type'        => 'int',
                            'isRequired'  => false,
                            'description' => 'Модель поведения после сканирования QR кода вашей карты. 1 - открыть свой профиль, 2 - открыть карту. 3 - открыть свой профиль + карту, 4 - перекинуть на покупку карты по моей реферальной ссылке, 5 - Спросить что делать. Если ничего не указано то по умолчанию принимается = 1',
                        ],
                        [
                            'name'        => 'gender',
                            'type'        => 'int',
                            'isRequired'  => false,
                            'description' => 'Параметр определяющий пол пользвателя 1 - Мужчина, 2 - Женщина',
                        ],
                    ],
                ]) ?>

                <h3 class="page-header">Ситуация когда пользователь записался на вебинар</h3>
                <p>Если к нему приходит письмо congratulations то ссылку да можно добавить.
                    Если к нему приходит письмо registration_form_1 или registration_form_2 то добавлять ссылку?
                    надо письма смотреть </p>
                <p><a href="https://www.i-am-avatar.com/cabinet-school-task-list/view?id=143" >https://www.i-am-avatar.com/cabinet-school-task-list/view?id=143</a></p>
                <p>В user expirience (модели поведения) нужно пользователю что то сказать, а что? нужно вызвать после подтверждения автоворонку.</p>
                <p>Напримере курса как пользоваться платформой, там внутри есть уроки. а на вебинаре будет урок с вебинаром. Значит на эту ссылку и отправлять.
                    верно?
                    надо делать автоворонку конечно же, но в любом случае тогда встает вопрос а как вызвать автоворонку после подтверждения? А как понять откуда пользователь с подтверждения пришел?
                </p>

                <p>
                    <a href="https://www.draw.io/#G1T0AG18eP2ULyEivblRPJjyA2IQcygVmu" target="_blank">
                        <img src="/images/controller/admin-developer/school-users/user_vebinar.png" class="thumbnail">
                    </a>
                </p>
                <?= \common\services\documentation\DbTable::widget([
                    'name'          => 'user_register_vebinar',
                    'description'   => 'Таблица которая хранит данные пользователя который записался на вебинар и система ждет подтверждения почты',
                    'model'         => '\common\models\UserRegisterVebinar',
                    'columns'       => [
                        [
                            'name'        => 'id',
                            'type'        => 'int',
                            'isRequired'  => true,
                            'description' => 'Идентификатор строки',
                        ],
                        [
                            'name'        => 'potok_id',
                            'type'        => 'int',
                            'isRequired'  => true,
                            'description' => 'Идентификатор потока куда пользователь регистрируется',
                        ],
                        [
                            'name'        => 'user_root_id',
                            'type'        => 'int',
                            'isRequired'  => true,
                            'description' => 'Идентификатор пользователя user_root',
                        ],
                        [
                            'name'        => 'created_at',
                            'type'        => 'int',
                            'isRequired'  => true,
                            'description' => 'Момент регистрации на событие',
                        ],
                    ],
                ]) ?>

                <p><a href="https://www.i-am-avatar.com/cabinet-task-list/view?id=179">Задача Удалить двойников из user_root</a></p>

            </div>
        </div>


    </div>
</div>

