<?php

/** $this \yii\web\View  */

use yii\helpers\Url;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;

$this->title = 'Школа';


?>

<div class="container">
    <div class="col-lg-12">
        <h1 class="page-header"><?= Html::encode($this->title) ?></h1>

        <h2 class="page-header">Настройка NGINX</h2>

        <p>Логи для всех школ единые</p>
        <pre>access_log /home/god/i-am-avatar/log-nginx/access.log;
error_log  /home/god/i-am-avatar/log-nginx/errors.log;</pre>

        <p>Домашняя папка тоже единая</p>
        <pre># Домашняя папка
root /home/god/i-am-avatar/www/school/web;</pre>

        <h2 class="page-header">Структура файлов GIT</h2>
        <p>
            <a href="https://www.draw.io/#G1hZGn4JuUgxU3u8-GLUYC4RIsbEtkf9gz" target="_blank">
                <img src="/images/controller/admin-developer/school/iAvatar-sys.png">
            </a>
        </p>
        <p>
            <a href="https://bitbucket.org/gsss/i-am-avatar.git" target="_blank">https://bitbucket.org/gsss/i-am-avatar.git</a>
        </p>
        <p>WebServer настроен на *.i-am-avatar.com на папку <code>school</code>.</p>

        <p>Папка <code>school</code> отвечает за уже готовую школу.</p>

        <h2 class="page-header">/page/*</h2>
        <p>Для путей <code>/page/*</code> на сайте <code>https://www.i-am-avatar.com</code> идет вывод страниц из школы 2</p>
        <p>Такие настройки указаны для urlManager</p>
        <pre>'page/&lt;controller&gt;'          => 'page/controller',
'page/&lt;controller&gt;/&lt;action&gt;' => 'page/controller-action',</pre>




        <h2 class="page-header">Регистрация на главной</h2>
        <p>После регистрации он входит в кабинет как учитель.</p>
        <p>Может ли один человек иметь несколько школ? То есть одному аккаунту принадлежит несколько школ и он там везде
            учитель. Да.</p>
        <p>Как это реализовано?</p>


        <h3 class="page-header">Приложения</h3>
        <p>Здесь я покажу чем различаются настройки приложений.</p>
        <p>Базы данных везде одинаковые.</p>

        <?php
        $rows = [
            [
                'id'        => 'avatar-root',
                'url'       => 'https://www.i-am-avatar.com',
                'web'       => '/public_html',
                'log'       => '/avatar-bank/runtime/logs',
                'web_log'   => '/home/god/i-am-avatar/log-nginx',
            ],
            [
                'id'        => 'avatar-school',
                'url'       => 'http://*.i-am-avatar.com',
                'web'       => '/school/web',
                'log'       => '/school/runtime/logs',
                'web_log'   => '/home/god/i-am-avatar/log-nginx',
            ],
        ];
        ?>
        <?= \yii\grid\GridView::widget([
            'dataProvider' => new \yii\data\ArrayDataProvider([
                'allModels' => $rows,
            ]),
            'tableOptions' => [
                'class' => 'table table-striped table-hover',
                'style' => 'width: auto',
            ],
            'summary'      => '',
            'columns'      => [
                [
                    'attribute' => 'id',
                    'content'   => function ($i) {
                        return Html::tag('code', $i['id']);
                    },
                ],
                [
                    'attribute' => 'url',
                    'content'   => function ($i) {
                        return Html::a($i['url'], $i['url'], ['target' => '_blank']);
                    },
                ],
                [
                    'attribute' => 'web',
                    'content'   => function ($i) {
                        return Html::tag('code', $i['web']);
                    },
                ],
                [
                    'attribute' => 'log',
                    'content'   => function ($i) {
                        return Html::tag('code', $i['log']);
                    },
                ],
                [
                    'attribute' => 'web_log',
                    'content'   => function ($i) {
                        return Html::tag('code', $i['web_log']);
                    },
                ],

            ],
        ]) ?>
        <h3 class="page-header">Добавление школы</h3>
        <p>При добавлении школы создатель становится администратором и учителем школы. Создаются статусы задач.</p>

        <h3 class="page-header">Структурная схема</h3>
        <p>
            <a href="https://www.draw.io/#G1E-sIB5Vuc15RkUqVIE6lj9m8LldzVe-b" target="_blank">
                <img src="/images/controller/admin-developer/school/scheme.png">
            </a>
        </p>

        <h3 class="page-header">Подключение TOR</h3>
        <p><img src="/images/controller/admin-developer/school/onion.png"></p>


        <h3 class="page-header">Админка</h3>
        <p><a href="http://bootstraptema.ru/_sf/25/2597.html"
              target="_blank">http://bootstraptema.ru/_sf/25/2597.html</a></p>
        <p><a href="http://bootstraptema.ru/_sf/31/3128.html"
              target="_blank">http://bootstraptema.ru/_sf/31/3128.html</a></p>
        <p><a href="http://bootstraptema.ru/_sf/31/3178.html"
              target="_blank">http://bootstraptema.ru/_sf/31/3178.html</a></p>
        <p><a href="http://bootstraptema.ru/_sf/26/2649.html"
              target="_blank">http://bootstraptema.ru/_sf/26/2649.html</a></p>

        <p><a href="https://mdbootstrap.com/docs/jquery/components/demo/"
              target="_blank">https://mdbootstrap.com/docs/jquery/components/demo/</a></p>



        <p>Как определить какой шаблон лучше?</p>
        <p>Надо определить перечень того что надо.</p>
        <p>Список того что надо:</p>
        <p>- сервис чат на странице</p>
        <p>- чат</p>
        <p>- почта сообщения</p>
        <p>- блог</p>
        <p>- комментарии</p>
        <p>- загрузка файла</p>
        <p>- модальные окна</p>
        <p>- Слайд шоу</p>
        <p>- Интернет магазин</p>
        <p>- Календарь</p>


        <h3 class="page-header">Админка</h3>
        <p>В админке пока только просмотр всех школ по ссылке <a href="https://www.i-am-avatar.com/admin-school/index">https://www.i-am-avatar.com/admin-school/index</a></p>

        <h3 class="page-header">Дизайн</h3>
        <p><a href="http://www.bestjquery.com/2015/08/domenu-jquery-drag-drop-nestable/" target="_blank">Дерево</a></p>
        <p><a href="http://stefanerickson.github.io/covervid/" target="_blank">Видео на фоне</a></p>
        <p><a href="http://techmanza.com/jquery-drag-and-drop-example/" target="_blank">Drag and drop todo list</a></p>
        <p><a href="https://ned.im/noty/#/" target="_blank">Уведомления</a></p>


        <h2 class="page-header">Определение школы от админки в коде</h2>
        <p><code>\common\models\school\School::isRoot()</code> - если я нахожусь в админке то выдастся true.</p>
        <p>Применяется в шаблонах которые идут на оба приложения, для того чтобы отличить где я нахожусь</p>

        <h2 class="page-header">Определение школы от админки в коде</h2>
        <p>Каждая школа - это модуль - своя БД, свой почтовик, свои шаблоны, свой репозиторий.</p>



        <h2 class="page-header">Модуль для школы</h2>
        <p>Школа может иметь свой модуль. А значит свои контроллеры, свои модели и свои представления. Свою БД.</p>

        <h3 class="page-header">Машрутизация</h3>
        <pre>$module = \common\models\school\School::getModuleClassName();</pre>
        <p>В менеджере путей есть такая запись:</p>
        <pre>'/' => 'page/root',</pre>
        <p>значит путь 'page/root' будет смотреть если у школы есть модуль то вызвать действие по умолчанию.</p>
        <p>Можно переопределить блог, новости, страницу авторизации и регистрации.</p>

        <h3 class="page-header">Меню</h3>
        <p>
            <a href="https://app.diagrams.net/#G1VAzpSHZ41DqMKPDwiQGCxIAxutMBiBEU" target="_blank">
                <img src="/images/controller/admin-developer/school/menu.png">
            </a>
        </p>

        <p>2. <code>MainGuestFrontend</code> <code>school_design_main.guest_frontend</code></p>
        <p>3. <code>MainAuthBackend</code> <code>school_design_main.auth_backend</code></p>
        <p>4. <code>MainAuthFrontend</code> <code>school_design_main.auth_frontend</code></p>

        <p>6. <code>SubGuestFrontend</code> <code>school_design.guest_frontend</code></p>
        <p>7. <code>SubAuthBackend</code> <code>school_design.cabinet_menu</code></p>
        <p>8. <code>SubAuthFrontend</code> <code>school_design.auth_frontend</code></p>

        <p>Настройка меню производится по путю <code>/cabinet-school-settings/design-main</code> <code>/cabinet-school-settings/design-sub</code></p>

        <p>Показ <code>MainGuestFrontend</code> и <code>MainAuthFrontend</code></p>
        <p>Если <code>MainAuthFrontend</code> нет то используется <code>MainGuestFrontend</code></p>
        <p>Вывод производится в файле <code>/avatar-bank/views/blocks/topMenuData.php</code></p>


        <h3 class="page-header">Магазин</h3>


        <h2 class="page-header">Иконка для главного сайта (favicon)</h2>
        <p>Данные хранятся в <code>school.favicon</code></p>
        <p>Где идет подстановка в страницы:</p>
        <ul>
            <li>cabinet.php</li>
            <li>main.php</li>
            <li>?page.php</li>
        </ul>

        <p>Алгоритм выбора:</p>
        <pre>$school = \common\models\school\School::get();
$favicon = '/images/logo144.png';
if ($school->favicon) {
    $favicon = $school->favicon;
}</pre>
    </div>
</div>

