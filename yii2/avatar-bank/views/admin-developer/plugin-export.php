<?php

/** $this \yii\web\View  */

use yii\helpers\Url;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;

$this->title = 'Плагины на экспорт';


?>

<div class="container">
    <div class="col-lg-12">
        <h1 class="page-header"><?= Html::encode($this->title) ?></h1>

        <p>Планины служат для того чтобы экспортировать лиды во внешние источники.</p>
        <p>Для одной школы можно создать несколько плагинов одного типа, для каждого задается имя чтобы отличать один от
            другого.</p>

        <p>Интерфейс: <code>\common\models\PluginInterface</code></p>


        <?= \common\services\documentation\DbTable::widget([
            'name'        => 'school_out_plugin',
            'model'       => '\common\models\school\PluginExport',
            'description' => 'Плагин',
            'columns'     => [
                [
                    'name'        => 'id',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Идентификатор записи',
                ],
                [
                    'name'        => 'name',
                    'type'        => 'varchar(100)',
                    'isRequired'  => true,
                    'description' => 'Название плагина',
                ],
                [
                    'name'        => 'class',
                    'type'        => 'varchar(100)',
                    'isRequired'  => true,
                    'description' => 'Класс, начинается с \\',
                ],
            ],
        ]) ?>

        <?= \common\services\documentation\DbTable::widget([
            'name'        => 'school_out_plugin_settings',
            'model'       => '\common\models\school\PluginExportSettings',
            'description' => 'Настройки плагина',
            'columns'     => [
                [
                    'name'        => 'id',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Идентификатор записи',
                ],
                [
                    'name'        => 'plugin_id',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Идентификатор плагина',
                ],
                [
                    'name'        => 'school_id',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Идентификатор школы',
                ],
                [
                    'name'        => 'name',
                    'type'        => 'varchar(100)',
                    'isRequired'  => true,
                    'description' => 'Название настройки',
                ],
                [
                    'name'        => 'content',
                    'type'        => 'text',
                    'isRequired'  => true,
                    'description' => 'Настройки сохраняющиеся в JSON',
                ],
            ],
        ]) ?>
        <p>Использование плагина</p>
        <pre>$setting = PluginExportSettings::findOne($id);
$setting->export($fields);</pre>
        <p>Как формируется класс и вызывается экспорт</p>
        <pre>public function export($fields)
{
    $content = $this->content;
    $data = Json::decode($content);
    $plugin = $this->getPlugin();
    $class = $plugin->class;
    /** @var \common\models\PluginInterface $object */
    $object = new $class($data);

    return $object->export($fields);
}</pre>


        <h2 class="page-header">Email</h2>
        <p>Класс: <code>\common\pluginexport\EMail</code></p>

        <p style="font-weight: bold;">Настройки</p>
        <?= \avatar\services\Params::widget([
            'hasNums' => true,
            'params'  => [
                [
                    'name'        => 'to',
                    'type'        => 'varchar(100)',
                    'isRequired'  => true,
                    'description' => 'Почта куда отправить лид',
                ],
            ],
        ]) ?>

        <p>Файл для рендеринга письма лежит в <code>/avatar-bank/mail/html/pluginexport/email.php</code></p>
        <p><img src="/images/controller/admin-developer/pluginexport/2019-02-04_01-35-11.png" class="thumbnail" width="50%"></p>

    </div>
</div>

