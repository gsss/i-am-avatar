<?php

/** $this \yii\web\View  */

use yii\helpers\Url;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;

$this->title = 'Школа. Новости';


?>

<div class="container">
    <div class="col-lg-12">
        <h1 class="page-header"><?= Html::encode($this->title) ?></h1>


        <?= \common\services\documentation\DbTable::widget([
            'name'    => 'school_news_article',
            'model'   => '\common\models\school\NewsItem',
            'columns' => [
                [
                    'name'        => 'id',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Идентификатор школы',
                ],
                [
                    'name'        => 'school_id',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Идентификатор школы',
                ],
                [
                    'name'        => 'created_at',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Момент создания',
                ],
                [
                    'name'        => 'is_signed',
                    'type'        => 'int',
                    'isRequired'  => false,
                    'description' => 'Флаг. Подписана статья? 0 - не подписана, 1 - подписана, идентификатор документа расположен в таблице user_documents',
                ],
                [
                    'name'        => 'is_subscribe',
                    'type'        => 'tinyint',
                    'isRequired'  => false,
                    'description' => 'Флаг. Разослана статья? 0 - не разослана, 1 - разослана, по умолчанию 0',
                ],
                [
                    'name'        => 'document_id',
                    'type'        => 'int',
                    'isRequired'  => false,
                    'description' => 'Если поле is_signed=1 то это поле заполняется. Идентификатор документа в таблице user_documents',
                ],
                [
                    'name'        => 'date',
                    'type'        => 'date',
                    'isRequired'  => true,
                    'description' => 'Дата создания',
                ],
                [
                    'name'        => 'id_string',
                    'type'        => 'varchar(100)',
                    'isRequired'  => true,
                    'description' => 'Идентификатор статьи, формируется из транслитерации названия',
                ],
                [
                    'name'        => 'content',
                    'type'        => 'text',
                    'isRequired'  => true,
                    'description' => 'HTML',
                ],
                [
                    'name'        => 'name',
                    'type'        => 'varchar(100)',
                    'isRequired'  => true,
                    'description' => 'Название страницы',
                ],
                [
                    'name'        => 'image',
                    'type'        => 'varchar(100)',
                    'isRequired'  => true,
                    'description' => 'Картинка',
                ],
            ],
        ]) ?>



        <p>Ссылка на новость /news/2018/10/20/ud_string</p>

        <h2 class="page-header">Настройки новости</h2>
        <p>Ссылка <code>/cabinet-school-settings/news</code></p>
        <p>Параметры:</p>
        <?= \avatar\services\Params::widget([
            'params' => [
                [
                    'name'        => 'school_settings.news_image_cut_id',
                    'type'        => 'int',
                    'description' => 'Вариант обрезка: 0 - обрезать, 1 - вписывать, 2 - вписывать с фоном, по умолчанию - 0',
                ],
                [
                    'name'        => 'school_settings.news_image_width',
                    'type'        => 'int',
                    'description' => 'Ширина картинки',
                ],
                [
                    'name'        => 'school_settings.news_image_height',
                    'type'        => 'int',
                    'description' => 'Высота картинки',
                ],
            ]
        ]) ?>

    </div>
</div>

