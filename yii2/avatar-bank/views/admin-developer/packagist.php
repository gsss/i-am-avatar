<?php

/** $this \yii\web\View  */

use yii\helpers\Url;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;

$this->title = 'Packagist';


?>

<div class="container">
    <div class="col-lg-12">
        <h1 class="page-header"><?= Html::encode($this->title) ?></h1>

        <p><code>i-avatar777</code> - главный логин на github</p>
        <p><code>iAvatar777</code> - главный namespace в классах</p>

        <?php
        $rows = [
            [
                'code'      => 'i-avatar777/yii2-widget-kolada-dar1',
                'desc'      => 'Виджет для Yii2 для рисования календаря Коляда Дар на лето',
                'namespace' => 'iAvatar777\widgets\KaladaDar1',
                'class'     => 'iAvatar777\widgets\KaladaDar1\KaladaDar1'
            ],
            [
                'code'      => 'i-avatar777/service-date-rus',
                'desc'      => 'Сервис для форматирование дат с поддержкой русского календаря',
                'namespace' => 'iAvatar777\services\DateRus',
                'class'     => 'iAvatar777\services\DateRus\DateRus'
            ],
            [
                'code'      => 'i-avatar777/service-ecdsa',
                'desc'      => 'Простая и быстрая реализация ECDSA на PHP используя библиотеку функций openssl',
                'namespace' => 'iAvatar777\services\EllipticCurve',
            ],
        ];

        ?>

        <?= \yii\grid\GridView::widget([
            'dataProvider' => new \yii\data\ArrayDataProvider([
                'allModels' => $rows,
            ]),
            'tableOptions' => [
                'class' => 'table table-striped table-hover',
            ],
            'rowOptions'   => function ($item) {
                $data = [
                    'data'  => ['id' => $item['code']],
                    'role'  => 'button',
                    'class' => 'rowTable'
                ];
                return $data;
            },
        ])
        ?>

    </div>
</div>

