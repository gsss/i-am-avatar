<?php

/** $this \yii\web\View  */

use yii\helpers\Url;
use app\services\GsssHtml;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use app\services\GetArticle\Collection;

$this->title = 'Сделать исполнение контракта на странице Аватара при помощи MetaMask';


?>

<div class="container">
    <div class="col-lg-12">
        <h1 class="page-header"><?= Html::encode($this->title) ?></h1>

        <p><a href="https://metamask.github.io/metamask-docs/" target="_blank">MetaMask Developer Documentation</a> </p>
        <p><a href="https://medium.com/metamask/calling-a-smart-contract-with-a-button-d278b1e76705" target="_blank">Calling a Smart Contract With a Button</a> </p>
        <p><a href="/cabinet-meta-mask/index" target="_blank" class="btn btn-default">Тестовая страница</a></p>

    </div>
</div>

