<?php

/** $this \yii\web\View  */

use yii\helpers\Url;
use app\services\GsssHtml;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use app\services\GetArticle\Collection;

$this->title = 'jquery-library';


?>

<div class="container">
    <div class="col-lg-12">
        <h1 class="page-header"><?= Html::encode($this->title) ?></h1>
        <h2 class="page-header">needim/noty</h2>
        <p>Документация: <a href="https://ned.im/noty/#/" target="_blank">https://ned.im/noty/#/</a></p>
        <p>Уведомления на экране</p>
        <p>AssetBundle <code>\avatar\assets\Notify</code></p>
        <p>Использование:</p>
        <pre>\avatar\assets\Notify::register(\Yii::$app->view);</pre>
        <pre>new Noty({
    theme: 'mint',
    text: 'Some notification text',
    timeout: 3000,
    type: 'success',
    layout: 'topRight',
    animation: {
        open : 'animated fadeInRight',
        close: 'animated fadeOutRight'
    }
}).show();</pre>




    </div>
</div>

