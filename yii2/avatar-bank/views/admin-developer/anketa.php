<?php

/** $this \yii\web\View  */

use yii\helpers\Url;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;

$this->title = 'Анкета для команды ЯАватар';


?>

<div class="container">
    <div class="col-lg-12">
        <h1 class="page-header"><?= Html::encode($this->title) ?></h1>

        <?= \common\services\documentation\DbTable::widget([
            'name'        => 'anketa',
            'description' => 'Анкета для команды ЯАватар',
            'model'       => '\common\models\Anketa',
            'columns'     => [
                [
                    'name'        => 'id',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Идентификатор элемента',
                ],
                [
                    'name'        => 'created_at',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Момент создания записи',
                ],
                [
                    'name'        => 'name_first',
                    'type'        => 'varchar(255)',
                    'isRequired'  => true,
                    'description' => 'Имя',
                ],
                [
                    'name'        => 'name_last',
                    'type'        => 'varchar(255)',
                    'isRequired'  => true,
                    'description' => 'Фамилия',
                ],
                [
                    'name'        => 'name_middle',
                    'type'        => 'varchar(255)',
                    'isRequired'  => true,
                    'description' => 'Отчество',
                ],
                [
                    'name'        => 'contact_email',
                    'type'        => 'varchar(255)',
                    'isRequired'  => true,
                    'description' => 'Почта',
                ],
                [
                    'name'        => 'contact_phone',
                    'type'        => 'varchar(255)',
                    'isRequired'  => true,
                    'description' => 'Телефон',
                ],
                [
                    'name'        => 'contact_vk',
                    'type'        => 'varchar(255)',
                    'isRequired'  => true,
                    'description' => 'ссылка на VK',
                ],
                [
                    'name'        => 'contact_fb',
                    'type'        => 'varchar(255)',
                    'isRequired'  => true,
                    'description' => 'ссылка на FB',
                ],
                [
                    'name'        => 'contact_telegram',
                    'type'        => 'varchar(255)',
                    'isRequired'  => true,
                    'description' => 'логин Telegram',
                ],
                [
                    'name'        => 'contact_whatsapp',
                    'type'        => 'varchar(255)',
                    'isRequired'  => true,
                    'description' => 'телефон whatsapp',
                ],
                [
                    'name'        => 'contact_skype',
                    'type'        => 'varchar(255)',
                    'isRequired'  => true,
                    'description' => 'логин скайп',
                ],
                [
                    'name'        => 'age',
                    'type'        => 'varchar(100)',
                    'isRequired'  => true,
                    'description' => 'возраст',
                ],
                [
                    'name'        => 'place',
                    'type'        => 'varchar(255)',
                    'isRequired'  => true,
                    'description' => 'Место проживания',
                ],
                [
                    'name'        => 'vozmojnosti',
                    'type'        => 'text',
                    'isRequired'  => false,
                    'description' => 'Навыки которыми обладаете, вверху самые наработанные и по убывающейя',
                ],
                [
                    'name'        => 'opit',
                    'type'        => 'text',
                    'isRequired'  => false,
                    'description' => 'Какой опыт был, в каких сферах и в каких ролях',
                ],
                [
                    'name'        => 'status',
                    'type'        => 'int',
                    'isRequired'  => false,
                    'description' => 'Статус анкеты',
                ],
                [
                    'name'        => 'last_message_time',
                    'type'        => 'int',
                    'isRequired'  => false,
                    'description' => 'момент последнего комментария',
                ],
                [
                    'name'        => 'comments_count',
                    'type'        => 'int',
                    'isRequired'  => false,
                    'description' => 'кол-во коментариев',
                ],
            ],
        ]) ?>

        <p>После добавления анкеты школы <code>/avatar-bank/views/anketa/new-school.php</code> для роли
            <code>role_admin</code> выслылается письмо <code>/avatar-bank/mail/html/new_anketa-school.php</code>.</p>

        <p>После добавления анкеты <code>/avatar-bank/views/anketa/new.php</code> для роли
            <code>role_admin_command</code> выслылается письмо <code>/avatar-bank/mail/html/new_anketa.php</code>.</p>


    </div>
</div>

