<?php

/** $this \yii\web\View  */

use yii\helpers\Url;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;

$this->title = 'Сообщество. СНТ';


?>

<div class="container">
    <div class="col-lg-12">
        <h1 class="page-header"><?= Html::encode($this->title) ?></h1>


        <?= \common\services\documentation\DbTable::widget([
            'name'        => 'school_snt_out',
            'description' => 'Растраты',
            'model'       => '\common\models\SchoolSntOut',
            'columns'     => [
                [
                    'name'        => 'id',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Идентификатор',
                ],
                [
                    'name'        => 'school_id',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Идентификатор школы',
                ],
                [
                    'name'        => 'created_at',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Момент создания',
                ],
                [
                    'name'        => 'amount',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Сумма',
                ],
                [
                    'name'        => 'currency_id',
                    'type'        => 'int',
                    'isRequired'  => false,
                    'description' => 'валюта dbWallet.currency.id',
                ],
                [
                    'name'        => 'user_id',
                    'type'        => 'int',
                    'isRequired'  => false,
                    'description' => 'Кто потратил',
                ],
            ],
        ]) ?>

        <?= \common\services\documentation\DbTable::widget([
            'name'        => 'school_snt_pohod',
            'description' => 'Походы',
            'model'       => '\common\models\SchoolSntPohod',
            'columns'     => [
                [
                    'name'        => 'id',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Идентификатор ',
                ],
                [
                    'name'        => 'school_id',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Идентификатор школы',
                ],
                [
                    'name'        => 'created_at',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Момент создания',
                ],
                [
                    'name'        => 'date',
                    'type'        => 'date',
                    'isRequired'  => true,
                    'description' => 'Дата похода',
                ],
                [
                    'name'        => 'description',
                    'type'        => 'int',
                    'isRequired'  => false,
                    'description' => 'Описание похода, что сделано?',
                ],
            ],
        ]) ?>

        <?= \common\services\documentation\DbTable::widget([
            'name'        => 'school_snt_pohod_file',
            'description' => 'файл похода',
            'model'       => '\common\models\SchoolSntPohodFile',
            'columns'     => [
                [
                    'name'        => 'id',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Идентификатор файла',
                ],
                [
                    'name'        => 'pohod_id',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Идентификатор похода',
                ],
                [
                    'name'        => 'created_at',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Момент создания',
                ],
                [
                    'name'        => 'file',
                    'type'        => 'varchar',
                    'isRequired'  => true,
                    'description' => 'файл',
                ],
            ],
        ]) ?>

        <?= \common\services\documentation\DbTable::widget([
            'name'        => 'school_snt_pohod_user',
            'description' => 'участник похода',
            'model'       => '\common\models\SchoolSntPohodUser',
            'columns'     => [
                [
                    'name'        => 'id',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Идентификатор файла',
                ],
                [
                    'name'        => 'pohod_id',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Идентификатор похода',
                ],
                [
                    'name'        => 'created_at',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Момент создания',
                ],
                [
                    'name'        => 'user_id',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'участник похода',
                ],
                [
                    'name'        => 'start',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => '',
                ],
                [
                    'name'        => 'end',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => '',
                ],
                [
                    'name'        => 'tarif',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => '',
                ],
            ],
        ]) ?>



    </div>
</div>

