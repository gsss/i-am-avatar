<?php

/** $this \yii\web\View  */

use yii\helpers\Url;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;

$this->title = 'Сообщество. Список задач';


?>

<div class="container">
    <div class="col-lg-12">
        <h1 class="page-header"><?= Html::encode($this->title) ?></h1>

        <h3 class="page-header">Задача</h3>
        <?= \common\services\documentation\DbTable::widget([
            'name'    => 'school_task',
            'model'    => '\common\models\task\Task',
            'columns' => [
                [
                    'name'        => 'id',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Идентификатор задачи',
                ],
                [
                    'name'        => 'school_id',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Идентификатор школы',
                ],
                [
                    'name'        => 'user_id',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Идентификатор пользователя создателя',
                ],
                [
                    'name'        => 'created_at',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Момент создвания',
                ],
                [
                    'name'        => 'updated_at',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Момент изменения',
                ],
                [
                    'name'        => 'name',
                    'type'        => 'varchar(255)',
                    'isRequired'  => true,
                    'description' => 'Заголовок задачи',
                ],
                [
                    'name'        => 'status',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Статус задачи по списку <code>school_task_status</code>',
                ],
                [
                    'name'        => 'price',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Цена исполнения, атомы',
                ],
                [
                    'name'        => 'currency_id',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Валюта dbWallet.currency.id',
                ],
                [
                    'name'        => 'parent_id',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Оно отвечает за хранение идентификатора родительской задачи',
                ],
                [
                    'name'        => 'is_hide',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Флаг. Задача скрыта? 0 - видна, 1 - скрыта. По умолчанию - 0',
                ],
                [
                    'name'        => 'wallet_type',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => '1 - Брать из кошелька школы, 2 - Брать с кошелька постановщика',
                ],
            ],
        ]) ?>


        <h3 class="page-header">Категория</h3>
        <?= \common\services\documentation\DbTable::widget([
            'name'    => 'school_task_category',
            'columns' => [
                [
                    'name'        => 'id',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Идентификатор категории',
                ],
                [
                    'name'        => 'school_id',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Идентификатор школы',
                ],
                [
                    'name'        => 'name',
                    'type'        => 'varchar(255)',
                    'isRequired'  => true,
                    'description' => 'Наименование',
                ],
            ],
        ]) ?>

        <h3 class="page-header">Список статусов</h3>
        <?= \common\services\documentation\DbTable::widget([
            'name'    => 'school_task_status',
            'columns' => [
                [
                    'name'        => 'id',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Идентификатор статуса',
                ],
                [
                    'name'        => 'school_id',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Идентификатор школы',
                ],
                [
                    'name'        => 'name',
                    'type'        => 'varchar(255)',
                    'isRequired'  => true,
                    'description' => 'Наименование',
                ],
                [
                    'name'        => 'sort_index',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Сортирововчный индекс',
                ],
                [
                    'name'        => 'type_id',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Тип статуса',
                ],
            ],
        ]) ?>

        <h3 class="page-header">Руководители проекта</h3>
        <?= \common\services\documentation\DbTable::widget([
            'name'    => 'school_project_manager_link',
            'columns' => [
                [
                    'name'        => 'id',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Идентификатор статуса',
                ],
                [
                    'name'        => 'school_id',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Идентификатор школы',
                ],
                [
                    'name'        => 'user_id',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Идентификатор пользователя',
                ],
            ],
        ]) ?>

        <h3 class="page-header">Команда</h3>

        <?= \common\services\documentation\DbTable::widget([
            'name'        => 'school_command_link',
            'model'       => '\common\models\school\CommandLink',
            'description' => 'Ссылка на участие в команде',
            'columns'     => [
                [
                    'name'        => 'id',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Идентификатор статуса',
                ],
                [
                    'name'        => 'school_id',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Идентификатор школы',
                ],
                [
                    'name'        => 'user_id',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Идентификатор пользователя',
                ],
                [
                    'name'        => 'is_only_my',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Настройка для члена команды, 0 - показываются все задачи. 1 - показываются только свои задачи, по умолчанию - 1',
                ],
                [
                    'name'        => 'hour_price',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Цена трудочаса в атомах',
                ],
                [
                    'name'        => 'hour_price_currency_id',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Валюта для hour_price, dbWallet.currency.id',
                ],
            ],
        ]) ?>



        <h3 class="page-header">Оплата</h3>
        <p>По завершении задачи награждение выдается валютой <code>school.currency_id</code> из кошелька <code>school.wallet_id</code>.
        </p>
        <p>Если валюта указанная в школе <code>school.currency_id</code> не будет соответствововать указанной кошелька
            <code>school.wallet_id</code>.</p>
        <p>Да избыток получается полей в БД
            Можно убрать task.currency_id и school.currency_id
            Потому что валюту можно вычислить из кошелька из которого брать монеты. и Все </p>
        <p>И еще может возникнуть ситуация когда денег нет в кошельке а задача закроется, но пользователь не поймет,
            здесь по хорошему надо предупреждение выдать.</p>
        <p>
            <a href="https://www.draw.io/#G1k1V-x6NT8jyMFYxM-17b4VzdHkrG-6iW" target="_blank">
                <img src="/images/controller/admin-developer/school-tasks/2019-02-12_03-55-29.png" class="thumbnail">
            </a>
        </p>

        <h3 class="page-header">Стутсы</h3>
        <p>Статус задачи хранится в поле <code>school_task.status</code>. Для каждой школы формируется свой набор
            статусов, для того чтобы можно в последствии было добавлять свои еще.</p>
        <p>Статусы хранятся в таблице <code>school_task_status</code></p>

        <p>Когда пользователь нажимает Я сделаю, задача переходит в статус "выполняется".</p>
        <p>Когда пользователь нажимает Я сделал, задача переходит в статус "проверяется". (Отправляется письмо всем
            проджект менеджерам.)</p>
        <p>Когда пользователь нажимает Принято, задача переходит в статус "выполнено". Отправляется письмо
            исполнителю.</p>
        <p>Кнопка "Принято" видна только, проджект менеджеру с ролью <code>role_project-manager</code>.</p>

        <h3 class="page-header">Дизайн</h3>
        <p>Для вывода списка дел можно использовать плагин.</p>
        <p><a href="http://techmanza.com/jquery-drag-and-drop-example/">Drag and drop todo list</a></p>
        <p><a href="https://github.com/kartik-v/yii2-grid">GridView - для вывода задач по группам</a></p>

        <h3 class="page-header">Группа задач. Подзадачи</h3>
        <p>Нельзя закрыть задачу пока все подзадачи не закрыты.</p>
        <p class="alert alert-danger">Если корневая удаляется подзадачи становятся корневыми? Или если корневая
            удаляется то предлагается подзадачи сделать задачами или удалить все.</p>
        <p class="alert alert-danger">Версия 1 (Максим). Корневая задача = сумма подзадач. Если создаётся подзадача,
            цену можно поставить только в подзадачи, цена корневой считается автоматически.</p>
        <p class="alert alert-danger">Версия 2 (Святослав). Корневая задача = сумма подзадач + сумма корневой задачи
            (она может быть равна нулю).</p>

        <h3 class="page-header">Редактирование задачи</h3>
        <p>Задачу может редактировать только автор, админ или руководитеь проекта</p>

        <h3 class="page-header">Я сделаю задачу</h3>
        <p><code>/cabinet-school-task-list/view-start</code></p>
        <p><code>/cabinet-task-list/view-start</code></p>
        <p>Письмо <code>task/start</code> отправляется всем управляющим проектом.</p>

        <h3 class="page-header">Я сделал(а) задачу</h3>
        <p><code>/cabinet-school-task-list/view-finish</code></p>
        <p>Письмо <code>task/finish</code> отправляется всем управляющим проектом.</p>

        <h3 class="page-header">Я снимаю с себя задачу</h3>
        <p><code>/cabinet-school-task-list/view-finish-reject</code></p>
        <p>Письмо <code>task/?</code> отправляется всем управляющим проектом.</p>

        <h3 class="page-header">Приянть!</h3>
        <p><code>/cabinet-school-task-list/view-done</code></p>
        <p>Вызывается класс: <code>\avatar\controllers\actions\CabinetSchoolTaskListController\actionViewDone</code></p>
        <p>Не учтен момент для рублей, если в рублевом фонде денег нет, то задача будет принята а деньги не будут
            отправлены.</p>

        <h3 class="page-header">Отклонить!</h3>
        <p><code>/cabinet-school-task-list/view-reject</code></p>
        <p>Письмо <code>task/?</code> отправляется исполнителю задачи.</p>

        <h3 class="page-header">Открепление</h3>
        <p>
            <img src="/images/controller/admin-developer/school-tasks/2019-03-07_05-48-33.png" class="thumbnail">
        </p>
        <p>Кто может открепить? Админ, PM, Сам исполнитель.</p>
        <p>Вызывается <code>/cabinet-school-task-list/view-finish-reject</code></p>
        <p>Если исполнитель открепляет себя то уведомляется PM. Письмо <code>task/finish-reject-pm.php</code> Коментарий
            в задачу: Я снимаю с себя ответственность за задачу</p>
        <p>Если PM открепляет исполнителя то уведомляется исполнитель. Письмо
            <code>task/finish-reject-exrcuter.php</code> Коментарий в задачу: Я снимаю с исполнителя задачу</p>

        <h3 class="page-header">Скрыть задачу</h3>
        <p>Кто может скрыть? Скрыть задачу может админ школы или управляющий проектом</p>
        <p>Вызывается <code>/cabinet-school-task-list/hide</code></p>

        <h3 class="page-header">Добавление задачи</h3>
        <p>Вызывается <code>/cabinet-school-task-list/add</code></p>
        <p>Валюта: можно указать в валюте школы и рубли. Валюта школы должна быть указана. И рублевый счет для школы
            должен быть создан.</p>
        <p>Если указана валюта школы (<code>school.currency_id</code>), то она добавляетс в список. Если указан рублевый
            фонд (<code>school.fund_rub</code>) то добавляется рубль в список валют.</p>

        <h3 class="page-header">Добавление помощника</h3>
        <p>Добавлние помощника <code>/cabinet-school-task-list/add-helper</code> - добавляет в БД. Нужно еще сделать
            добавление коментария в чат, и уведомление по почте и телеграм</p>
        <p>Поиск помощника <code>/cabinet-school-task-list/helper-search-ajax</code>. В множество поиска включаются все, кто в команде в школе.</p>

        <?= \common\services\documentation\DbTable::widget([
            'name'        => 'school_task_helper',
            'description' => 'Помощник в задаче',
            'model'       => '\common\models\task\Helper',
            'columns'     => [
                [
                    'name'        => 'id',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Идентификатор записи',
                ],
                [
                    'name'        => 'task_id',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Идентификатор задачи',
                ],
                [
                    'name'        => 'user_id',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Идентификатор пользователя',
                ],
                [
                    'name'        => 'created_at',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Время добавления',
                ],
            ],
        ]) ?>

        <h3 class="page-header">События</h3>
        <p><b>EVENT_TASK_NEW</b> - Новая задача</p>
        <p><b>EVENT_TASK_START</b> - Пользователь взял задачу</p>
        <p><b>EVENT_TASK_COMMENT</b> - Добавлен новый коментарий в задачу</p>
        <p class="alert alert-danger">В разработке - нижние статусы еще не интегрированы в код, то есть не триггерятся,
            но прописаны в классе уже</p>
        <p><b>EVENT_TASK_FINISH</b> - Исполнитель выполнил задачу</p>
        <p><b>EVENT_TASK_ACCEPT</b> - PM принял задачу</p>
        <p><b>EVENT_TASK_REJECT</b> - PM отклонил задачу</p>
        <p><b>EVENT_TASK_UNLINK</b> - Исполнитель снял с себя ответственность за выполнение задачи</p>
        <p><b>EVENT_TASK_HIDE</b> - задачу стала скрытой</p>
        <p><b>EVENT_TASK_PRICE_CHANGE</b> - Изменилась награда за задачу</p>
        <p><b>EVENT_TASK_ADD_HELPER</b> - Добавлен помощник</p>
        <p><b>EVENT_TASK_ADD_SUB_TASK</b> - Добавлена подзадача</p>

        <h3 class="page-header">Только мои задачи</h3>
        <p>Если админ добавляет человека то теперь появилась опция, показывать ему только свои задачи или все задачи.</p>
        <p>Для этого в списке команды появился новый флажок: "Показываются только свои задачи?". Если он равен Да то участнику команды показываются только его задачи. Если он наверн Нет то участнику команды показываются все задачи.</p>
        <p><img src="/images/controller/admin-developer/school-tasks/2019-10-25_11-50-48.png" width="400"></p>
        <p>По умолчанию участнику назначается видеть только свои задачи.</p>
        <p>Если выставлена опция "Показывать только свои задачи" то в задачнике пропадает колонка "Исполнитель".</p>

        <h3 class="page-header">Расчет времени</h3>
        <p>Применяется для того чтобы отсчитывать сколько времени было затрачено на задачу.</p>
        <p>Данные показываются на экране + записываются в БД на случай если пользователь закроет браузер.</p>

        <?= \common\services\documentation\DbTable::widget([
            'name'        => 'school_task_session',
            'description' => 'Учет сессий для задачи',
            'model'       => '\common\models\task\Session',
            'columns'     => [
                [
                    'name'        => 'id',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Идентификатор записи',
                ],
                [
                    'name'        => 'task_id',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Идентификатор задачи',
                ],
                [
                    'name'        => 'start',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Время начала отсчета сессии',
                ],
                [
                    'name'        => 'finish',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Время окончания отсчета сессии',
                ],

            ],
        ]) ?>
        <p>Исключительный случай. Если происходит попытка открыть новую сессию в то время как не закрыта старая, то ничего не происходит и вызывается исключение.</p>
        <p>Для запуска счетчика задачи есть "Действие" <code>\avatar\controllers\actions\CabinetSchoolTaskListController\actionSessionStart</code></p>
        <p>Для остановки счетчика задачи есть "Действие" <code>\avatar\controllers\actions\CabinetSchoolTaskListController\actionSessionFinish</code></p>

        <h3 class="page-header">Сделать чтобы при старте задачи была проверка на запущенные задачи</h3>
        <p>В функции старта задачи надо проверять а есть ли открытые сессии.<br>
            и в итоге формируется изменение модели поведения.<br>
            1 нажимаю кнопку я сделаю.<br>
            2 если открыта сессия то открывается диалоговое окно. Открыта сессия. Что делать? - Закрыть предыдущую и начать эту. - Отмена.<br>
            да все хорошо, принимаю и делаю</p>
        <p>
            <a href="https://www.draw.io/#G1wUbZh6rU4hm_G4bvHUxyEmp-GvT7Mfr2" target="_blank">
                <img src="/images/controller/admin-developer/school-tasks/session-start.png" class="thumbnails">
            </a>
        </p>

        <p>Делаю скрипт <code>/cabinet-school-task-list/view-close-session-start</code> - он закрывает старые сессии и начинает задачу</p>

        <h3 class="page-header">Сделать скрипт остановки задачи если она уже более 24 часов висит</h3>
        <p>В консоли следующая команда:</p>
        <pre>php yii task/close-open-long</pre>
        <p>Если задача открыта более 24 часов, то она останавливается и исполнителю отправляется уведомление <code>task/finish-console</code></p>

        <h2 class="page-header">Календарный вид задач</h2>
        <p>Этот вид предназначен для задач которые повторяются изо дня в день. Поэтому они отображаются в виде календаря. Значит для дней будет отдельная таблица. Так же существуют роли людей значит для каждой роли будет таблица.</p>
        <p><img src="/images/controller/admin-developer/school-tasks/table.png" width="70%" class="thumbnail"></p>
        <p>Определю каждую сущность</p>

        <h3 class="page-header">Назначить исполнителя для задачи</h3>
        <p>Назначить задачу исполнителю может руководитель проекта.</p>
        <p>AJAX для поиска исполнителя: <code>/cabinet-school-task-list/executor-search-ajax</code>.</p>
        <p>Поиск происходит по списку команды.</p>
        <p>далее надо сделать назначение исполнителя
            в этом событии отправляется уведомление самому исполнителю что его назначили. на почту и в телеграм если полключен.</p>
        <p>Если нажать на кнопку не выбрав исполнителя то выйде ошибка </p>
        <p><img src="https://i.imgur.com/gmXFB29.png" class="thumbnail"></p>



        <h3 class="page-header">Монеты Качества жизни</h3>
        <p>QLW - Оплата за труд</p>
        <p>QLP - Начисляется когда человек зачисляет имущество в компанию</p>
        <p>QLM - Начисляется когда человек зачисляет деньги в компанию</p>
        <h3 class="page-header">Награда за задачу с кошелька постановщика задачи</h3>
        <p>Можно сделать так чтобы вознаграждение зачислялось с кошелька постановщика задачи.</p>
        <p>Хорошо а все это могут делать? Нет только руководитель проекта. Хорошо. Пойдет.</p>
        <p>Что делать если у постановщика задачи закончились деньги на момент приемки задачи? Значит выдается диалоговое окно, что нет денег. Будем закрывать без денег или отмена?</p>
        <p>Хорошо, еще предложение сделать чтобы школа могла иметь несколько монет для зачисления. Да. Значит для этого надо сделать настройку.</p>
        <p>А какая модель данных будет?</p>
        <p><code>school_coin</code> - монета которыми сообщество оперирует. <code>\common\models\SchoolCoin</code></p>
        <p>При постановке задачи как сделать? выбор 1 Брать из кошелька школы, 2 Брать с кошелька постановщика</p>

        <h3 class="page-header">Назначение задачи с вознаграждением себе</h3>
        <p>Сейчас нельзя назначить себе задачу, если вознаграждение будет с постановщика задачи. Потому что тогда монеты из моего кошелька будут переводиться мне же, это не получится. Плюс к этому еще и есть риск что эти монеты могут быть пересланы другим людям, а они уже будут являться правом требования. </p>
        <p>Для того чтобы решить две эти проблемы я ввожу еще одну монету <code>QLWB</code> - бюджет на разработку. Эта монета выдается руководителям проектов. По факту успешной задачи QLWB забирается у постановщика и QLW перечисляется в той же сумме исполнителю.</p>
        <p>Нужно чтобы в компании было прописано какая монета используется. за что</p>
        <p><code>school_coin.budjet_currency_id</code> - идентификатор валюты <code>dbWallet.currency.id</code> которая будет как бюджет</p>
        <p><code>school_coin.currency_id</code> - идентификатор валюты <code>dbWallet.currency.id</code> которая будет как вознаграждение для пользователя и долговое обязательство со стороны кооператива.</p>
        <p>Нужен главный кошелек для валюты, где будет эта настройка? <code>currency_link_avatar_processing?</code> а к чему относится эта настройка? к внутренней монете, но я не хочу туда ничего добавлять. Значит еще одна таблица. currency_int. Хотя если учесть что внешней валюты нет без внешней то можно и в currency_link_avatar_processing. Но это не однозначный факт. поэтому currency_int</p>

        <h3 class="page-header">Учет трудочаса человека в команде</h3>
        <p>учет будет вестись в таблице <code>school_command_link</code> поле <code>hour_price</code> <code>hour_price_currency_id</code></p>
        <p>По завершению задачи человек ставит сколько времени он потратил на задачу</p>
        <p>Размышления: а если потом будет отмена задания. То потом исполнитель опять когда говорит что выполнил он пишет сколько занял последний этап работ. и так пока не сделает. И все эти периуды будут суммироваться.</p>

        <h3 class="page-header">Изменение цены задачи перед закрытием</h3>
        <p>Если пользователь не указал изначально цену и валюту то какую валюту использовать? та которая указана в <code>school_coin.budjet_currency_id</code></p>

        <h3 class="page-header">Архитектура модуля задачи</h3>
        <p>
            <a href="https://app.diagrams.net/#G111QV1RdWnavtqfZePsztEXuWFZzbPSpU">
                <img src="/images/controller/admin-developer/school-task-list/a.png" width="100%">
            </a>
        </p>

    </div>
</div>

