<?php

/** $this \yii\web\View  */

use yii\helpers\Url;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;

$this->title = 'Учет денежных вложений';


?>

<div class="container">
    <div class="col-lg-12">
        <h1 class="page-header"><?= Html::encode($this->title) ?></h1>

        <p>Таблица school_money_income</p>
        <?= \common\services\documentation\DbTable::widget([
            'name'    => 'school_money_income',
            'model'   => '\common\models\SchoolMoneyIncome',
            'columns' => [
                [
                    'name'        => 'id',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Идентификатор записи',
                ],
                [
                    'name'        => 'school_id',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Идентификатор школы',
                ],
                [
                    'name'        => 'user_id',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Идентификатор пользователя кто внес',
                ],
                [
                    'name'        => 'created_at',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Момент создания',
                ],
                [
                    'name'        => 'amount',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Кол-во вложенных денег',
                ],
                [
                    'name'        => 'currency_id',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'dbWallet.currency.id',
                ],
                [
                    'name'        => 'date',
                    'type'        => 'date',
                    'isRequired'  => true,
                    'description' => 'Дата платежа',
                ],
                [
                    'name'        => 'description',
                    'type'        => 'varchar(2000)',
                    'isRequired'  => true,
                    'description' => 'Описание платежа',
                ],
                [
                    'name'        => 'image',
                    'type'        => 'varchar(100)',
                    'isRequired'  => true,
                    'description' => 'Картинка чека',
                ],
            ],
        ]) ?>

    </div>
</div>

