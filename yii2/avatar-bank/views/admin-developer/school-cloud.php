<?php

/** $this \yii\web\View  */

use yii\helpers\Url;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;

$this->title = 'Школа. Облако';


?>

<div class="container">
    <div class="col-lg-12">
        <h1 class="page-header"><?= Html::encode($this->title) ?></h1>


        <p>Сейчас все файлы пишутся в <code>/public_html/upload/cloud</code></p>
        <p>Все файлы пишутся в папку школы. Формат ###### - идентификатор школы по school.id с ведущими нулями.</p>
        <p>Формат файла: ########_######.ext</p>
        <p>1-й блок - временная метка создания файла (timestamp)</p>
        <p>2-й блок - случайна послдовательность цифр и букв, для гарантии уникальности имени файла</p>

        <p>В таблицу записываются все файлы которые пользователи загружают чтобы вести учет.</p>
        <p>да на каждую школу по папке.</p>
        <p>но дело в том что скорее всего все облако будет распределенное и все файлы все равно на разные серваки будут
            писаться</p>
        <p>Все равно лучше по папкам.</p>

        <?= \common\services\documentation\DbTable::widget([
            'name'        => 'school_file',
            'description' => 'Файл',
            'columns'     => [
                [
                    'name'        => 'id',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Идентификатор записи',
                ],
                [
                    'name'        => 'school_id',
                    'type'        => 'integer',
                    'isRequired'  => true,
                    'description' => 'Идентификатор школы',
                ],
                [
                    'name'        => 'type_id',
                    'type'        => 'integer',
                    'isRequired'  => true,
                    'description' => 'Тип файла, таблица и поле, см ниже',
                ],
                [
                    'name'        => 'created_at',
                    'type'        => 'integer',
                    'isRequired'  => false,
                    'description' => 'Время добавления записи',
                ],
                [
                    'name'        => 'is_main',
                    'type'        => 'tinyint',
                    'isRequired'  => false,
                    'description' => 'Флаг. Это главный файл? Не обрезок? 0 - это файл оригинал. 1 - Это обрезок. По умолчанию - 0.',
                ],
                [
                    'name'        => 'size',
                    'type'        => 'integer',
                    'isRequired'  => true,
                    'description' => 'Размер файла',
                ],
                [
                    'name'        => 'file',
                    'type'        => 'varchar(255)',
                    'isRequired'  => true,
                    'description' => 'Полный путь к файлу',
                ],
            ],
        ]) ?>

        <h2 class="page-header">AvatarCloud</h2>
        <p>Для работы с облаком Аватара предназначен класс <code>\common\services\AvatarCloud</code>. Он оформлен в виде
            компонента и прошит в главных настройках приложения как компонент приложения, поэтому к нему можно
            обращаться так:</p>
        <pre>/** @var \common\services\AvatarCloud $cloud */
$cloud = Yii::$app->AvatarCloud;</pre>


        <?= \common\services\documentation\DbTable::widget([
            'name'        => 'cloud',
            'description' => 'Таблица для хранения серверов с облаками',
            'columns'     => [
                [
                    'name'        => 'id',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Идентификатор записи',
                ],
                [
                    'name'        => 'url',
                    'type'        => 'varchar(255)',
                    'isRequired'  => true,
                    'description' => 'Ссылка на домен',
                ],
                [
                    'name'        => 'free_space',
                    'type'        => 'integer',
                    'isRequired'  => false,
                    'description' => 'Свободного места в МБ',
                ],
                [
                    'name'        => 'created_at',
                    'type'        => 'integer',
                    'isRequired'  => false,
                    'description' => 'Время создания записи',
                ],
            ],
        ]) ?>


        <h3 class="page-header">Функции</h3>
        <h4 class="page-header">Загрузить картинку</h4>
        <p>
            <code>/upload/image</code> - Загружает картинки, можно несколько
        </p>
        <pre>$file = \cs\services\File::path($path);
$response = $cloud->_post(null, 'upload/image', [
    'update' => Json::encode([
        [
            'function' => 'crop',
            'index'    => 'crop',
            'options'  => [
                'width'  => '300',
                'height' => '300',
                'mode'   => 'MODE_THUMBNAIL_CUT',
            ],
        ],
    ]),
], ['files' => [$file]]);
$data = Json::decode($response->content);
if ($data['success']) {
    $fileList = $data['data']['fileList'];
}</pre>

        <h4 class="page-header">Вычисление SHA256</h4>
        <p>
            <code>/upload/sha256</code> - Вычислет SHA256 для файла
        </p>
        <?= \avatar\services\Params::widget([
            'params' => [
                [
                    'name'        => 'file',
                    'type'        => 'string',
                    'isRequired'  => true,
                    'description' => 'относительный путь к файлу который уже есть на облаке',
                ],
            ],
        ]) ?>

        <h5 class="page-header">Возвращает</h5>
        <?= \avatar\services\Params::widget([
            'params' => [
                [
                    'name'        => 'sha256',
                    'type'        => 'string',
                    'isRequired'  => true,
                    'description' => 'хеш',
                ],
            ],
        ]) ?>

        <h4 class="page-header">Удаляет файлы</h4>
        <p>
            <code>/upload/delete-many</code> - Удаляет файлы
        </p>
        <?= \avatar\services\Params::widget([
            'params' => [
                [
                    'name'        => 'fileList',
                    'type'        => 'array',
                    'isRequired'  => true,
                    'description' => 'Массив ссылок на файл, относительный путь, то есть начинаться со слеша',
                ],
            ],
        ]) ?>

        <h5 class="page-header">Возвращает</h5>
        <p>Ничего</p>

        <h4 class="page-header">Удаляет файлы с индексами</h4>
        <p>
            <code>/upload/delete-many-index</code> - Удаляет файлы с индексами
        </p>
        <?= \avatar\services\Params::widget([
            'params' => [
                [
                    'name'        => 'fileList',
                    'type'        => 'array',
                    'isRequired'  => true,
                    'description' => 'Массив ссылок на файл, относительный путь, то есть начинаться со слеша',
                ],
            ],
        ]) ?>

        <h5 class="page-header">Возвращает</h5>
        <p>Ничего</p>


        <h2 class="page-header">type_id</h2>
        <?php

        $r = [
            [
                'id'      => 555,
                'comment' => 'test',
                'is_done' => 0,
            ],
            [
                'id'      => 1,
                'comment' => 'файл для конструктора сайтов',
                'is_done' => 1,
            ],
            [
                'id'      => 2,
                'comment' => 'комментарий',
            ],
            [
                'id'      => 3,
                'comment' => 'таблицы',
            ],
            [
                'id'      => 4,
                'comment' => 'Задачи (комментарий)',
            ],
            [
                'id'      => 5,
                'comment' => 'Интернет магазин, товар, картинка [gs_unions_shop_product.image]',
                'is_done' => 1,
            ],
            [
                'id'      => 6,
                'comment' => 'Блог школы, картинка [school_blog_article.image]',
                'is_done' => 1,
            ],
            [
                'id'      => 7,
                'comment' => 'Урок школы, картинка [school_kurs_lesson.image]',
                'is_done' => 1,
            ],
            [
                'id'      => 8,
                'comment' => 'Блог, картинка [blog.image]',
                'is_done' => 1,
            ],
            [
                'id'      => 9,
                'comment' => 'Страница школы, картинка соцсети [school_page.facebook_image]',
            ],
            [
                'id'      => 10,
                'comment' => 'Страница школы, картинка favicon [school_page.favicon]',
            ],
            [
                'id'      => 11,
                'comment' => 'Сертификат пользователя [school_user_sertificate.image]',
                'is_done' => 1,
            ],
            [
                'id'      => 12,
                'comment' => 'Загрузка файла в облако с SHA256',
                'is_done' => 1,
            ],
            [
                'id'      => 13,
                'comment' => 'Новости, картинка [news.image]',
                'is_done' => 1,
            ],
            [
                'id'      => 14,
                'comment' => 'Платежная система [paysystems.image]',
                'is_done' => 1,
            ],
            [
                'id'      => 15,
                'comment' => 'Курс [school_kurs.image]',
                'is_done' => 1,
            ],
            [
                'id'      => 16,
                'comment' => 'Валюта Внешняя [currency.image]',
                'is_done' => 1,
            ],
            [
                'id'      => 17,
                'comment' => 'Школа [school.image]',
                'is_done' => 0,
            ],
            [
                'id'      => 18,
                'comment' => 'Аватар [user.avatar]',
                'is_done' => 0,
            ],
            [
                'id'      => 19,
                'comment' => 'Карта идентификационная [card_id.image_top]',
                'is_done' => 1,
            ],
            [
                'id'      => 20,
                'comment' => 'Карта идентификационная [card_id.image_back]',
                'is_done' => 1,
            ],
            [
                'id'      => 21,
                'comment' => 'Паспорт для кооператива [user_enter.passport_scan1]',
                'is_done' => 1,
            ],
            [
                'id'      => 22,
                'comment' => 'Паспорт для кооператива [user_enter.passport_scan2]',
                'is_done' => 1,
            ],
            [
                'id'      => 23,
                'comment' => 'Паспорт для кооператива [card_design.image]',
                'is_done' => 1,
            ],
            [
                'id'      => 24,
                'comment' => 'Файл в коментарии [comments.file]',
                'is_done' => 1,
            ],
            [
                'id'      => 25,
                'comment' => 'Файл в коментарии [user_documents.file]',
                'is_done' => 1,
            ],
            [
                'id'      => 26,
                'comment' => 'Кантинка товара [gs_unions_shop_product_images.image]',
                'is_done' => 1,
            ],
            [
                'id'      => 27,
                'comment' => 'Кантинка чека [school_money_income.image]',
                'is_done' => 1,
            ],
            [
                'id'      => 28,
                'comment' => 'Кантинка чека [school_money_income.protokol]',
                'is_done' => 1,
            ],
            [
                'id'      => 29,
                'comment' => 'Отчет об использовании сырья и материалов [school_money_income.report]',
                'is_done' => 1,
            ],
            [
                'id'      => 30,
                'comment' => 'Техническое задание [school_money_income.tz]',
                'is_done' => 1,
            ],
            [
                'id'      => 31,
                'comment' => 'файл похода [school_snt_pohod_file.file]',
                'is_done' => 1,
            ],

        ];
        ?>
        <?= \yii\grid\GridView::widget([
            'dataProvider' => new \yii\data\ArrayDataProvider(['allModels' => $r]),
            'tableOptions' => [
                'class' => 'table table-striped table-hover',
            ],
        ]) ?>


        <h2 class="page-header">Сервер IPFS</h2>
        <p>
            <a href="https://t4.yalic.pw/zapusk-nody-ipfs-na-svoyom-servere/" target="_blank">https://t4.yalic.pw/zapusk-nody-ipfs-na-svoyom-servere/</a>
        </p>
        <p>
            <a href="https://docs.ipfs.io/" target="_blank">https://docs.ipfs.io/</a>
        </p>
        <p>
            <a href="https://www.cloudflare.com/distributed-web-gateway/" target="_blank">https://www.cloudflare.com/distributed-web-gateway/</a>
        </p>

        <p>HABR</p>
        <ul>
            <li><a href="https://habr.com/post/316468/" target="_blank">Публикуем сайт в межпланетной файловой системе
                    IPFS</li>
            <li><a href="https://habr.com/post/325176/" target="_blank">Хостим сайт в межпланетной файловой системе IPFS
                    под Windows</li>
            <li><a href="https://habr.com/post/331010/" target="_blank">Больше нет необходимости копировать в сеть</li>
            <li><a href="https://habr.com/post/334584/" target="_blank">Переключаем свой сайт на localhost (локальный
                    шлюз IPFS)</li>
            <li><a href="https://habr.com/post/331014/" target="_blank">Локализуем глобальный шлюз или сайты в IPFS</li>
            <li><a href="https://habr.com/post/423073/" target="_blank">Тривиальный хеш (identity), DAG блок и Protocol
                    Buffers</li>
        </ul>

        <p>
            <iframe width="100%" height="315" src="https://www.youtube.com/embed/YXQvGidv0u4" frameborder="0"
                    allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
                    allowfullscreen></iframe>
        </p>

        <p>
            <iframe width="100%" height="315" src="https://www.youtube.com/embed/zcCoWBCiwkI" frameborder="0"
                    allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
                    allowfullscreen></iframe>
        </p>

        <h2 class="page-header">Консольная функция на конвертацию на новый формат</h2>

        <pre>php yii cloud/update 23 \common\models\CardDesign image 2</pre>
        <p>первый параметр - обязятельный - целое число - идентификатор типа картинки в классификаторе облака</p>
        <p>второй параметр - обязятельный - строка - наименовкание класса где лежит ссылка на картинку</p>
        <p>третий параметр - обязятельный - строка - наименовкание аттрибута класса где лежит ссылка на картинку</p>
        <p>четвертый параметр - необязятельный - целое число - идентификатор школы на которую будет присоединен файл, если не указан то берется значение [class->attribute]</p>
        <p>Пока старые файлы не удаляет во избежании ошибок, поэтому после запуска надо вручную почистить файлы.</p>

        <h2 class="page-header">Поворот картинок</h2>
        <p>
            <a href="https://app.diagrams.net/#G1PGiZW80qwzmF3OkVkYPfVW_7sMSch4V0" target="_blank">
                <img src="/images/controller/admin-developer/school-cloud/rotate.png">
            </a>
        </p>
        <pre>$fullpath = '/home/ramha/PhpstormProjects/i-am-avatar/public_html/55274_nvir9gPGS2.jpg';

$image = imagecreatefromstring(file_get_contents($fullpath));

# rotate
$exif = exif_read_data($fullpath);

$angles= [8 => 90, 3 => 180, 6 => -90];
if(!empty($exif['Orientation']) && isset($angles[$exif['Orientation']])) {
    $image = imagerotate($image, $angles[$exif['Orientation']], 0);
}
# rotate

$fullpath2 = '/home/ramha/PhpstormProjects/i-am-avatar/public_html/upload/55274_nvir9gPGS2_'.Security::generateRandomString(6).'.jpg';
imagejpeg($image, $fullpath2);</pre>


    </div>
</div>

