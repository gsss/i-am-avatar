<?php

/** $this \yii\web\View  */

use yii\helpers\Url;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;

$this->title = 'Школа. Деньги';


?>

<div class="container">
    <div class="col-lg-12">
        <h1 class="page-header"><?= Html::encode($this->title) ?></h1>


        <h3 class="page-header">Монета</h3>
        <p><img src="/images/controller/admin-developer/school-money/coin.png" width="300"></p>
        <p>Название: Элексир</p>
        <p>Работает внутри платформы</p>
        <p>Сокращенное название: ELX, EL</p>
        <p><a href="http://dobehappy.ru/eliksir-dlya-dushi" target="_blank">Безусловная любовь – эликсир для души</a>
        </p>
        <p>
            <a href="http://sluzheniyasveta.ucoz.ru/publ/instrumenty_dlja_raboty/praktiki_meditacii/molitva_otche_nash_novogo_vremeni/5-1-0-7"
               target="_blank">ЭЛИКСИР</a>
        </p>
        <p><a href="https://kryonrussia.ru/kristally" target="_blank">ЭЛИКСИР</a>
        </p>

        <h4 class="page-header">Термины</h4>
        <p>Атом - копейка монеты</p>
        <p>Валюта - монета, свойства: название, кол-во атомов в монете, код-валюты</p>
        <p>Кошелек - сущность которая хранит монеты, свойства: валюта, чей, кол-во атомв монеты</p>
        <p>Операция - операция с кошельком</p>
        <p>Транзакция - перевод монеты из одного кошелька в другой, всегда сопровождается двумя операциями.</p>

        <p><img src="/images/controller/admin-developer/school-money/trans.png"></p>


        <h4 class="page-header">Сущности</h4>
        <p>Хранятся в БД <code>i_am_avatar_prod_wallet</code></p>

        <?= \common\services\documentation\DbTable::widget([
            'name'        => 'currency',
            'description' => 'Валюта',
            'columns'     => [
                [
                    'name'        => 'id',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Идентификатор валюты',
                ],
                [
                    'name'        => 'name',
                    'type'        => 'varchar(100)',
                    'isRequired'  => true,
                    'description' => 'Название урока',
                ],
                [
                    'name'        => 'code',
                    'type'        => 'varchar(10)',
                    'isRequired'  => true,
                    'description' => 'Код валюты большими буквами (англ)',
                ],
                [
                    'name'        => 'amount',
                    'type'        => 'integer',
                    'isRequired'  => true,
                    'description' => 'Кол-во эмиттированных монет в атомах',
                ],
                [
                    'name'        => 'decimals',
                    'type'        => 'integer',
                    'isRequired'  => true,
                    'description' => 'Кол-во десятичных знаков',
                ],
            ],
        ]) ?>

        <?= \common\services\documentation\DbTable::widget([
            'name'        => 'operations',
            'description' => 'Операции',
            'columns'     => [
                [
                    'name'        => 'id',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Идентификатор валюты',
                ],
                [
                    'name'        => 'wallet_id',
                    'type'        => 'integer',
                    'isRequired'  => true,
                    'description' => 'Идентификатор кошелька',
                ],
                [
                    'name'        => 'transaction_id',
                    'type'        => 'integer',
                    'isRequired'  => false,
                    'description' => 'Идентификатор транзакции, только для типа <code>1</code> и <code>-1</code>',
                ],
                [
                    'name'        => 'type',
                    'type'        => 'integer',
                    'isRequired'  => true,
                    'description' => 'Тип операции:<br>2 - пополнение админом (эмиссия),<br>1 - прибавление при транзакции,<br>-1 - вычитание при транзакции,<br>-2 - вычитание админом (сжигание)',
                ],
                [
                    'name'        => 'datetime',
                    'type'        => 'double',
                    'isRequired'  => true,
                    'description' => 'Момент операции',
                ],
                [
                    'name'        => 'before',
                    'type'        => 'integer',
                    'isRequired'  => false,
                    'description' => 'Было до операции',
                ],
                [
                    'name'        => 'after',
                    'type'        => 'integer',
                    'isRequired'  => false,
                    'description' => 'Стало после операции',
                ],
                [
                    'name'        => 'amount',
                    'type'        => 'integer',
                    'isRequired'  => true,
                    'description' => 'Кол-во атомов в операции, положительно если вычет',
                ],
                [
                    'name'        => 'comment',
                    'type'        => 'varchar(255)',
                    'isRequired'  => false,
                    'description' => 'Коментарий для операции',
                ],
            ],
        ]) ?>
        <p>Так как есть тип операции без транзакции такие как эмиссия и сжигание то поле <code>transaction_id</code> не
            обязательное.</p>

        <?= \common\services\documentation\DbTable::widget([
            'name'        => 'transactions',
            'description' => 'Транзакции',
            'columns'     => [
                [
                    'name'        => 'id',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Идентификатор валюты',
                ],
                [
                    'name'        => 'from',
                    'type'        => 'integer',
                    'isRequired'  => true,
                    'description' => 'Идентификатор кошелька списания',
                ],
                [
                    'name'        => 'to',
                    'type'        => 'integer',
                    'isRequired'  => true,
                    'description' => 'Идентификатор кошелька зачисления',
                ],
                [
                    'name'        => 'amount',
                    'type'        => 'integer',
                    'isRequired'  => true,
                    'description' => 'Кол-во атомов в транзакции',
                ],
                [
                    'name'        => 'datetime',
                    'type'        => 'double',
                    'isRequired'  => true,
                    'description' => 'Момент операции',
                ],
                [
                    'name'        => 'comment',
                    'type'        => 'varchar(255)',
                    'isRequired'  => false,
                    'description' => 'Коментарий для операции',
                ],
                [
                    'name'        => 'type',
                    'type'        => 'integer',
                    'isRequired'  => false,
                    'description' => 'Тип транзакции',
                ],
            ],
        ]) ?>

        <?= \common\services\documentation\DbTable::widget([
            'name'        => 'wallet',
            'description' => 'Кошелек',
            'model'     => '\common\models\piramida\Wallet',
            'columns'     => [
                [
                    'name'        => 'id',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Идентификатор валюты',
                ],
                [
                    'name'        => 'currency_id',
                    'type'        => 'integer',
                    'isRequired'  => true,
                    'description' => 'Идентификатор валюты',
                ],
                [
                    'name'        => 'is_deleted',
                    'type'        => 'tinyint',
                    'isRequired'  => true,
                    'description' => 'Флаг. Кошелек удален? 0 - рабочий кошелек. 1 - кошелек удален. 0 - по умолчанию.',
                ],
                [
                    'name'        => 'amount',
                    'type'        => 'integer',
                    'isRequired'  => true,
                    'description' => 'Кол-во атомов в транзакции',
                ],
                [
                    'name'        => 'comment',
                    'type'        => 'varchar(255)',
                    'isRequired'  => false,
                    'description' => 'Коментарий для операции',
                ],
            ],
        ]) ?>

        <h4 class="page-header">Привязка кошельков к пользовтелям</h4>
        <?= \common\services\documentation\DbTable::widget([
            'name'        => 'user_wallet',
            'description' => 'Кошелек пользователя',
            'columns'     => [
                [
                    'name'        => 'id',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Идентификатор строки',
                ],
                [
                    'name'        => 'user_id',
                    'type'        => 'integer',
                    'isRequired'  => true,
                    'description' => 'Идентификатор пользователя',
                ],
                [
                    'name'        => 'wallet_id',
                    'type'        => 'integer',
                    'isRequired'  => true,
                    'description' => 'Идентификатор кошелька',
                ],
            ],
        ]) ?>

        <h3 class="page-header">Валюта для учета трудовложений</h3>
        <p>Вот у меня вопрос нужно ли вводить дополнительную валюту для зарплаты?</p>
        <p>А почему возник такой вопрос?
        <p>Потому что когда встанет вопрос обналички Элексира, а он будет через обменник делаться, то нельзя будет
            отличить обычные элексиры от тех, которые были назначены за труд. Поэтому надо ввести новую валюту ELXT. Она
            подлежит обналичиванию и служит так же для оплаты услуг платформы.</p>

        <h3 class="page-header">Как обеспечить гарантию целостности данных</h3>
        <p>Во первых можно проверять контрольные суммы каждую минуту, если после проверки возникнет расхождение то
            останавливать систему расчетов.</p>
        <p>Во вторых каждый час делать бекап, из него можно восстановить данные при разрушении БД.</p>
        <p>Какие проверки надо делать?</p>

        <p>operations.after - operations.after = operations.amount</p>
        <pre>select (`after`-`before`-((if(`type` < 0, -1,1))*`amount`)) as sum, id,`after`,`before`,`amount` from operations</pre>
        <p>Столбец <code>sum</code> должен быть равен 0. Если идет расхождение, то значит в транзакции нарушена
            целостность данных.</p>

        <pre>select t1.am1,t1.currency_id,currency.amount, currency.amount - t1.am1 as balance FROM (
select sum(wallet.amount) as am1,currency_id from wallet GROUP BY currency_id
) as t1
INNER JOIN currency on (currency.id = t1.currency_id)</pre>
        <p>Столбец <code>balance</code> должен быть равен 0. Если идет расхождение, то значит валюту или эмиттировали но
            не записали, или наоборот.</p>

        <p><a href="/admin-wallets/test">/admin-wallets/test</a></p>

        <h3 class="page-header">Исследование</h3>
        <p>Можно вычислять контрольные суммы каждой строки operations и записывать контрольную сумму в поле crc. Так
            можно проверить целостность данных в пределах строки.</p>
        <p>Да ну следуя такой логике я приду к построению обычного блокчейна как они сейчас и строятся со всеми
            проработанными ыопросами которые я ставлю.</p>

        <h3 class="page-header">Сервисы</h3>
        <p><code>php yii wallets/find-empty-bills</code> - Ищет пустые счета котореы не привязани ни к одному пользователю</p>

        <h3 class="page-header">Привязка монет внутеннего процессинга к монетам внешнего обслуживания</h3>
        <p>Внутренний процессинг - это БД <code>i_am_avatar_prod_wallet</code>.</p>
        <?= \common\services\documentation\DbTable::widget([
            'name'        => 'currency_link_avatar_processing',
            'description' => 'Привязка монет внутеннего процессинга к монетам внешнего обслуживания',
            'model'       => '\common\models\avatar\CurrencyLink',
            'columns'     => [
                [
                    'name'        => 'id',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Идентификатор строки',
                ],
                [
                    'name'        => 'currency_int_id',
                    'type'        => 'integer',
                    'isRequired'  => true,
                    'description' => 'внутренняя i_am_avatar_prod_wallet.currency.id',
                ],
                [
                    'name'        => 'currency_ext_id',
                    'type'        => 'integer',
                    'isRequired'  => true,
                    'description' => 'внешняя i_am_avatar_prod_main.currency.id ',
                ],
            ],
        ]) ?>


        <h3 class="page-header">Привязка монет внутеннего процессинга к монетам внешнего обслуживания</h3>

        <p>Содержит все счета для всех заказов. Так как каждый заказ имеет свой класс то он указывается в поле <code>class_id</code> и расшифровывается в таблице <code>billing_class</code>.</p>

        <?= \common\services\documentation\DbTable::widget([
            'name'        => 'billing',
            'description' => 'Счет главный',
            'model'       => '\common\models\BillingMain',
            'columns'     => [
                [
                    'name'        => 'id',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Идентификатор строки',
                ],
                [
                    'name'        => 'sum_before',
                    'type'        => 'integer',
                    'isRequired'  => false,
                    'description' => 'атомы (копейки), сумма до транзакции, сумма которую пользователь фактически оплатил за транзакцию',
                ],
                [
                    'name'        => 'sum_after',
                    'type'        => 'integer',
                    'isRequired'  => true,
                    'description' => 'атомы (копейки), сумма которая постпила в систему после транзакции',
                ],
                [
                    'name'        => 'created_at',
                    'type'        => 'integer',
                    'isRequired'  => true,
                    'description' => 'время создания счета',
                ],
                [
                    'name'        => 'is_paid',
                    'type'        => 'integer',
                    'isRequired'  => false,
                    'description' => 'Флаг, Оплачен счет? 0 - не оплачен, 1 - оплачен. По умолчанию - 0',
                ],
                [
                    'name'        => 'is_paid_client',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Флаг. Оплачен заказ как подтвердил клиент? 0 - нет. 1 - оплачен. По умолчанию - 0',
                ],
                [
                    'name'        => 'source_id',
                    'type'        => 'integer',
                    'isRequired'  => true,
                    'description' => 'идентификатор платежной системы paysystems.id',
                ],
                [
                    'name'        => 'currency_id',
                    'type'        => 'integer',
                    'isRequired'  => true,
                    'description' => 'идентификатор валюты в уотором был выставлен счет currency.id',
                ],
                [
                    'name'        => 'class_id',
                    'type'        => 'integer',
                    'isRequired'  => false,
                    'description' => 'идентификатор Класса к которому относится счет billing_class.id',
                ],
                [
                    'name'        => 'description',
                    'type'        => 'varchar(255)',
                    'isRequired'  => false,
                    'description' => 'Описание заказа/счета',
                ],
                [
                    'name'        => 'destination',
                    'type'        => 'varchar(255)',
                    'isRequired'  => false,
                    'description' => 'Описание Места назначения платежа / получателя',
                ],
                [
                    'name'        => 'successUrl',
                    'type'        => 'varchar(255)',
                    'isRequired'  => false,
                    'description' => 'URL для перехода по факту успешной оплаты',
                ],
                [
                    'name'        => 'failUrl',
                    'type'        => 'varchar(255)',
                    'isRequired'  => false,
                    'description' => 'URL для перехода по факту не успешной оплаты',
                ],
                [
                    'name'        => 'action',
                    'type'        => 'varchar(255)',
                    'isRequired'  => false,
                    'description' => 'код передаваемый в ПС',
                ],
            ],
        ]) ?>

        <h2 class="page-header">Флаги по оплате</h2>
        <p>Оплата может быть в автоматизированном виде или в ручном.</p>
        <p>В автоматизированном виде платеж подтверждается сразу</p>
        <p>В ручном режиме сначала клиент подтверждает а потом магазин подтверждает что платеж пришел.</p>
        <p>Статусы оплаты вынесены в отдельные флаги чтобы не потать их с доставкой.</p>
        <ul>
            <li>В самовывозе оплата может быть заранее а может быть по факту получения.</li>
            <li>В Доставке по почте только предоплата.</li>
            <li>В электронном товаре только предоплата.</li>
            <li>В наложном платеже оплата идет после получения товара клиентом. И еще может быть не оплачен и товар
                вернется назад.
            </li>
        </ul>
        <p>В связи со всем этим вводятся флаги <code>is_paid</code> - флаг который указывает что подтвердил магазин. <code>is_paid_client</code> -
            флаг который указывает что подтвердил магазин. В итоге получаем такой расклад возможных комбинаций:</p>
        <?php
        $rows = [
            [
                'event'           => 'Не оплачено',
                'is_paid_client' => 0,
                'is_paid'        => 0,
            ],
            [
                'event'           => 'Клиент подтвердил',
                'is_paid_client' => 1,
                'is_paid'        => 0,
            ],
            [
                'event'           => 'Магазин подтвердил',
                'is_paid_client' => 0,
                'is_paid'        => 1,
            ],
        ];
        ?>
        <?= \yii\grid\GridView::widget([
            'dataProvider' => new \yii\data\ArrayDataProvider(['allModels' => $rows]),
            'tableOptions' => [
                'class' => 'table table-striped table-hover',
            ],
            'summary' => '',
        ]) ?>


        <?= \common\services\documentation\DbTable::widget([
            'name'        => 'billing_class',
            'description' => 'Название класса на которое ссылается billing.class_id',
            'model'       => '\common\models\BillingMainClass',
            'columns'     => [
                [
                    'name'        => 'id',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Идентификатор строки',
                ],
                [
                    'name'        => 'name',
                    'type'        => 'varchar(255)',
                    'isRequired'  => true,
                    'description' => 'Название класса, спереди слеш обязателен',
                ],
            ],
        ]) ?>
        <pre>create table `billing_class`
(
    id int auto_increment primary key,
    name VARCHAR(255) null
) ENGINE=InnoDB DEFAULT CHARSET=utf8;</pre>

        <h3 class="page-header">Удаление кошелька</h3>

        <p>Сам кошелек удалить нельзя, потому что на него могут быть другие связи и так же для целостности данных и еще чтобы нумерация не сбивалась.</p>
        <p>Для удаления кошелька используется эмуляция. Вводится поле <code>is_deleted</code>. Флаг. Кошелек удален? 0 - рабочий кошелек. 1 - кошелек удален. 0 - по умолчанию.</p>
        <p>В классе <code>\common\models\piramida\Wallet</code> Если пробовать перевести на кошелек удаленный возникнет исключение.
            Если пробовать перевести из кошелека удаленного возникнет исключение.</p>

        <h3 class="page-header">Рублевый фонд</h3>
        <p>Рублевый фонд предназначен для распределения денег пришедших в компанию.</p>
        <p>Поле <code>school.fund_rub</code> сохраняет идентификатор кошелька рублевого RUB id=4 куда будет отчисляться приход денег.</p>
        <p>Поле <code>school.fund_rub2</code> сохраняет идентификатор кошелька рублевого RUB id=4 куда будет отчисляться 80%.</p>
        <p>
            <a href="https://www.draw.io/#G1Clu1Y8nlqUY1ZhHwAr1Deuj9VyrhqcEh" target="_blank">
                <img src="/images/controller/admin-developer/school-money/input.png" class="thumbnail">
            </a>
        </p>

        <?= \common\services\documentation\DbTable::widget([
            'name'        => 'school_input',
            'description' => 'Таблица приходов рублей',
            'model'       => '\common\models\school\Input',
            'columns'     => [
                [
                    'name'        => 'id',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Идентификатор строки',
                ],
                [
                    'name'        => 'school_id',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Идентификатор школы',
                ],
                [
                    'name'        => 'amount',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Кол-во атомов которое пришло',
                ],
                [
                    'name'        => 'comment',
                    'type'        => 'varchar(255)',
                    'isRequired'  => true,
                    'description' => 'Комментарий',
                ],
            ],
        ]) ?>

        <p>Алгоритм распределения поступлений:<br>
        1.	input = кол-во пришедших денег в школу<br>
        2.	Вычисляю сколько школа должна всем аватарам (ELXSKY - ELXSKYRUB) = dolgAvatars<br>
        3.	Вычисляю 20% от input = input20<br>
        4.	Если dolgAvatars < input20? fundAvatars = dolgAvatars,fundSchool=input-fundAvatars : fundAvatars = sum((int)(input20 * (dolgAvatar/dolgAvatars))) ,fundSchool=input-fundAvatars<br>
        5.	Делается эмиссия в кошельке fund_rub в размере fundAvatars и переводится каждому аватару его пропорциональную сумму.<br>
        6.	Делается эмиссия в кошельке fund_rub2 в размере fundSchool</p>


        <h3 class="page-header">API, создание кошелька и пароль</h3>
        <p>При создании кошелька по API нужно указывать пароль, он будет использоваться при отправке денег. После создания кошелька выдается мастер пароль для кошелька.</p>
        <p>Пароль хранится в поле wallet.password в виде хеша выдаваемом функцией <code>password_hash</code></p>
        <p>Мастер ключ хранится зашифрованным через AES по ключу.</p>
        <p>Для того чтобы поменять пароль к кошельку, можно использовать мастер ключ (master key). Функция для смены пароля: <code>wallet/change_password</code></p>

    </div>
</div>

