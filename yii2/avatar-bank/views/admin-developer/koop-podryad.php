<?php

/** $this \yii\web\View  */

use yii\helpers\Url;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;

$this->title = 'Подряд в кооперативе';


?>

<div class="container">
    <div class="col-lg-12">
        <h1 class="page-header"><?= Html::encode($this->title) ?></h1>

        <p><img src="/images/controller/admin-developer/koop-podryad/bp.png" width="100%"></p>

        <p>Подряд будет находиться в таблице <code>koop_podryad</code></p>

        <?= \common\services\documentation\DbTable::widget([
            'name'        => 'koop_podryad',
            'model'       => '\common\models\KoopPodryad',
            'description' => 'Подряд в кооперативе',
            'columns'     => [
                [
                    'name'        => 'id',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Идентификатор записи',
                ],
                [
                    'name'        => 'school_id',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Идентификатор школы',
                ],
                [
                    'name'        => 'created_at',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Время добавления',
                ],
                [
                    'name'        => 'status',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Статус подряда',
                ],
                [
                    'name'        => 'dogovor_podryada',
                    'type'        => 'varchar(200)',
                    'isRequired'  => true,
                    'description' => 'Договор подряда с Контрагентом',
                ],
                [
                    'name'        => 'akt_pp_sirya',
                    'type'        => 'varchar(200)',
                    'isRequired'  => true,
                    'description' => 'Акт приема-передачи сырья и материалов',
                ],
                [
                    'name'        => 'akt_pp_prod',
                    'type'        => 'varchar(200)',
                    'isRequired'  => true,
                    'description' => 'Акт приема передачи готовой продукции',
                ],
                [
                    'name'        => 'report',
                    'type'        => 'varchar(200)',
                    'isRequired'  => true,
                    'description' => 'Отчет об использовании сырья и материалов',
                ],
                [
                    'name'        => 'tz',
                    'type'        => 'varchar(200)',
                    'isRequired'  => true,
                    'description' => 'Техническое задание',
                ],
                [
                    'name'        => 'protokol',
                    'type'        => 'varchar(200)',
                    'isRequired'  => true,
                    'description' => 'Протокол проведения собрания членов ВТК',
                ],
                [
                    'name'        => 'agent',
                    'type'        => 'varchar(200)',
                    'isRequired'  => true,
                    'description' => 'Контрагент',
                ],
            ],
        ]) ?>

        <?= \common\services\documentation\DbTable::widget([
            'name'        => 'koop_podryad_command_user',
            'model'       => '\common\models\KoopPodryadCommandUser',
            'description' => 'Участник команды для подряда',
            'columns'     => [
                [
                    'name'        => 'id',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Идентификатор записи',
                ],
                [
                    'name'        => 'podryad_id',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Идентификатор школы',
                ],
                [
                    'name'        => 'user_id',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Идентификатор пользователя',
                ],
                [
                    'name'        => 'tz_address',
                    'type'        => 'varchar(100)',
                    'isRequired'  => true,
                    'description' => 'Адрес пользователя при подписи ТЗ',
                ],
                [
                    'name'        => 'tz_sign',
                    'type'        => 'varchar(100)',
                    'isRequired'  => true,
                    'description' => 'Подпись пользователя при подписи ТЗ',
                ],
                [
                    'name'        => 'tz_created_at',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Время при подписи ТЗ',
                ],
            ],
        ]) ?>


        <p>Необходимо документы защитить от скачивания</p>
        <p>Можно в облаке выдавать ссылку для скачивания по которой нельзя будет скачать. А как тогда можно будет скачать? например я отправляю POST с данными о пользователе, тогда должен быть какой то ключ для пользователя равный тому что на облаке чтобы можно было проверять</p>
        <p>Пример ссылки? /download?id=111</p>
        <p>При закачке пользователь передает ключ. А при скачивании тоже ключ. Это получается файл пользователя</p>
        <p>А как сделать файл доступный ограниченному числу людей? В облаке будет храниться ключ платформы. И так она можеь получить любой файл, а регулировать доступ будет она. Да актуально.</p>
        <p>В итоге есть приватный файл, общий и публичный</p>

        <h2 class="page-header">Статусы</h2>
        <p>1 - Загружен договор подряда, указан контрагент</p>
        <p>2 - Сформирована команда</p>
        <p>3 - Загружено ТЗ</p>
        <p>4 - Вся команда подписала ТЗ</p>
        <p>5 - Загружен Акт приема-передачи сырья и материалов</p>
        <p>6 - Загружен Акт приема передачи готовой продукции / приемки выполненных работ</p>
        <p>7 - Загружен Отчет об использовании сырья и материалов</p>
        <p>8 - Загружен Протокол о распределении прибыли</p>
        <p>9 - Вся команда подписала Протокол о распределении прибыли</p>

        <h2 class="page-header">Подпись ТЗ</h2>
        <p>Можно подпись хранить в таблице <code>koop_podryad_command_user</code>: Поля: адрес, подпись, дата. tz_address, tz_sign, tz_created_at</p>
    </div>
</div>

