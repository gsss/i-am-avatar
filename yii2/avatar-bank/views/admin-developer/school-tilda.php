<?php

/** $this \yii\web\View  */

use yii\helpers\Url;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;

$this->title = 'Импорт страниц с тильды';


?>

<div class="container">
    <div class="col-lg-12">
        <h1 class="page-header"><?= Html::encode($this->title) ?></h1>

        <?= \common\services\documentation\DbTable::widget([
            'name'        => 'school_tilda',
            'description' => 'Настройки плагина тильды для школы',
            'model'       => '\common\models\school\TildaPlugin',
            'columns'     => [
                [
                    'name'        => 'id',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Идентификатор элемента',
                ],
                [
                    'name'        => 'created_at',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Момент создания записи',
                ],
                [
                    'name'        => 'school_id',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Идентификатор школы',
                ],
                [
                    'name'        => 'publickey',
                    'type'        => 'varchar(64)',
                    'isRequired'  => true,
                    'description' => 'публичный ключ',
                ],
                [
                    'name'        => 'secretkey',
                    'type'        => 'varchar(64)',
                    'isRequired'  => true,
                    'description' => 'секретный ключ',
                ],
            ],
        ]) ?>

        <p>настройка плагина
            <code>/cabinet-school-tilda/index</code></p>
        <p>вот что приходит
            <code>/tilda/call-back?pageid=6897713&projectid=1552727&published=1565869364&publickey=iz0c19uu1h7azcprlsyp</code></p>


        <h2 class="page-header">Связь КЖ и тильды</h2>
        <p>https://youtu.be/JAL9NkuOutI</p>

        <h2 class="page-header">Хранение файлов тильды на Аватаре</h2>
        <p>Все файлы надо копировать в <code>@school/web</code></p>
        <p>Для того чтобы в HTML странице генерируемой тильдой были нормальные пути надо указать в ее настройках подобные пути <code>/tilda/[id]/images</code> - для картинок, <code>/tilda/[id]/js</code> - для js, <code>/tilda/[id]/css</code> - для css. Где id - это идентификатор школы Аватара.</p>
        <p>Сами страницы будут сохраняться в файлах <code>/tilda/[id]/html</code> под именем <code>file[avatar-page-id].html</code></p>
        <p>Ссылка на страницу тильды в странице Аватара будет сохраняться в параметре <code>school_page.tilda_page_id</code></p>
        <h3 class="page-header">Страница тильды для главного сайта</h3>
        <p>Так как файлы должны нашодиться в папке @webroot значит копировать в сообщества нельзя, значит буду копировать в <code>@webroot/tilda</code>. Только как отличить суперкомпанию от простой? Значит будет флаг в сообществе <code>school.is_super_company</code></p>


        <h2 class="page-header">Отображение страницы</h2>
    </div>
</div>

