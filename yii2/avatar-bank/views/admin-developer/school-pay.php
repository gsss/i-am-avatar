<?php

/** $this \yii\web\View  */

use yii\helpers\Url;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;

$this->title = 'Платный вход';


?>

<div class="container">
    <div class="col-lg-12">
        <h1 class="page-header"><?= Html::encode($this->title) ?></h1>

        <!-- Nav tabs -->
        <ul class="nav nav-tabs" role="tablist">
            <li role="presentation" class="active"><a href="#clear" aria-controls="clear" role="tab" data-toggle="tab">Чистовик</a></li>
            <li role="presentation"><a href="#potok" aria-controls="potok" role="tab" data-toggle="tab">Поток</a></li>
        </ul>

        <!-- Tab panes -->
        <div class="tab-content">
            <div role="tabpanel" class="tab-pane in active" id="clear">
            </div>
            <div role="tabpanel" class="tab-pane fade" id="potok">
                <p>Здесь описывается процедура платного входа в кабинет.</p>
                <p>Что здесь есть или может быть?</p>
                <p>1. Надо понять как будет выглядеть кабинет не оплаченный.</p>
                <p>2. Процедура оплаты.</p>
                <p>3. Что видно после оплаты.</p>
                <p>4. Что происходит если заканчиваются деньги на аккаунте.</p>
                <p>Итого раскрываю по порядку</p>

                <h2 class="page-header">Как будет выглядеть кабинет не оплаченный</h2>
                <p>Будет закрыт раздел Школы. То есть как только я нажимаю Школы, то на странице показывается сообщение. "Вы еще не оплатили тариф, поэтому вы не можете управлять школами".</p>

                <h2 class="page-header">Процедура оплаты</h2>
                <p>Оплатить услуги платформы можно только в Золотых Эликсирах по тарифной сетке. За каждую школу оплата происходит отдельно. Если Эликсиры есть на счету то оплата снимается автоматически за каждый месяц. Можно получить скидку оплатив пакет сразу вручную.</p>

                <p><a href="https://www.draw.io/#G1ntT7PHWICm46Yn46hjMrfGXZDMIHoV4K" target="_blank">https://www.draw.io/#G1ntT7PHWICm46Yn46hjMrfGXZDMIHoV4K</a></p>

                <h2 class="page-header">Что видно после оплаты</h2>
                <p>Приходит уведомление что оплата прошла успешно и можно зайти в Школы и там будет полный функционал.</p>

                <h2 class="page-header">Что происходит если заканчиваются деньги на аккаунте</h2>
                <p>Приходит уведомление за неделю и за три дня что деньги закончиваются и нужно оплатить, если оплата не будет произведена то все блокируется, в том числе и кабинеты учеников.</p>


                <h2 class="page-header">Тарифы</h2>
                <?php
                $rows = [
                    [
                        'name'      => 'Ангел',
                        'm1'      => 1,
                        'm2'      => 1,
                    ],
                    [
                        'name'      => 'Архангел',
                        'm1'      => 2.5,
                    ],
                    [
                        'name'      => 'Начало',
                        'm1'      => 5,
                    ],
                    [
                        'name'      => 'Власть',
                        'm1'      => 10,
                    ],
                    [
                        'name'      => 'Сила',
                        'm1'      => 25,
                    ],
                    [
                        'name'      => 'Господство',
                        'm1'      => 50,
                    ],
                    [
                        'name'      => 'Престол',
                        'm1'      => 100,
                    ],
                    [
                        'name'      => 'Херувим',
                        'm1'      => 250,
                    ],
                    [
                        'name'      => 'Серафим',
                        'm1'      => 500,
                        'm2'      => 0.12,
                    ],
                ];
                $users = 1000;
                $price = 3000;
                for($i = 0;$i<count($rows);$i++) {
                    $rows[$i]['users'] = $users * $rows[$i]['m1'];
                    if ($i > 0) {
                        if ($i < count($rows) - 1) {
                            $diff = $rows[0]['m2'] - $rows[count($rows)-1]['m2'];
                            $one = $diff / (count($rows) - 1);
                            $rows[$i]['m2'] = $rows[0]['m2'] - ($one * $i);
                        }
                    }
                    $rows[$i]['price'] = $price * $rows[$i]['m1'] * $rows[$i]['m2'];
                    $rows[$i]['discont3'] = 30;
                    $rows[$i]['discont12'] = 50;
                }
                ?>
                <?= \yii\grid\GridView::widget([
                    'dataProvider' => new \yii\data\ArrayDataProvider([
                        'allModels' => $rows,
                    ]),
                    'tableOptions' => [
                        'class' => 'table table-striped table-hover',
                        'style' => 'width: auto',
                    ],
                    'summary'      => '',
                    'columns'      => [
                        [
                            'header'    => 'Тариф',
                            'attribute' => 'name',
                        ],
                        [
                            'header'    => 'Пользователей',
                            'attribute' => 'users',
                            'format'         => ['decimal', 0],
                            'contentOptions' => [
                                'style' => 'text-align:right;'
                            ],
                            'headerOptions'  => [
                                'style' => 'text-align:right;'
                            ],
                        ],
                        'm1',
                        'm2',
                        [
                            'header'    => 'Места',
                            'attribute' => 'space',
                            'contentOptions' => [
                                'style' => 'text-align:right;'
                            ],
                            'headerOptions'  => [
                                'style' => 'text-align:right;'
                            ],
                            'content'   => function ($item) {
                                return Yii::$app->formatter->asDecimal($item['users'] / 50) . ' Гб';
                            }
                        ],
                        [
                            'header'         => 'Стоимость в мес',
                            'attribute'      => 'price',
                            'format'         => ['decimal', 0],
                            'contentOptions' => [
                                'style' => 'text-align:right;'
                            ],
                            'headerOptions'  => [
                                'style' => 'text-align:right;'
                            ],
                        ],
                        [
                            'header'         => 'Скидка при оплате за 3 мес',
                            'attribute'      => 'discont3',
                            'contentOptions' => [
                                'style' => 'text-align:right;'
                            ],
                            'headerOptions'  => [
                                'style' => 'text-align:right;'
                            ],
                            'content'        => function ($item) {
                                $v = $item['price'] * 3;
                                return Yii::$app->formatter->asDecimal($v * ((100-$item['discont3']) / 100));
                            }
                        ],
                        [
                            'header'         => 'Скидка при оплате за 12 мес',
                            'attribute'      => 'discont12',
                            'contentOptions' => [
                                'style' => 'text-align:right;'
                            ],
                            'headerOptions'  => [
                                'style' => 'text-align:right;'
                            ],
                            'content'        => function ($item) {
                                return Yii::$app->formatter->asDecimal($item['price'] * 12 * ((100-$item['discont12']) / 100));
                            }
                        ],
                    ],
                ]) ?>

                <p>Поле отвечающее за оплату школы это <code>paid_till</code>. В ней хранится момент до которого школа оплачена.</p>
                <pre>установил безоплатного тарифа до 2019-04-21 00:00:00
далее вешаю кнопку какую
1. Обратитесь по почте
2. оплатите
- выберите тариф? без тарифа, пока тест чтобы привыкали к оплате.
- сколько оплатить? пока базовый тариф 100 руб допустим на тестовый период для теста.
хорошо какой алгоритм
я нажимаю на кнопку.
меня перекидывает на страницу описания что делать и там кнопка оплатить.
я сразу попадаю на яндекс для оплаты, и после успешной оплаты мне прибавляется месяц.</pre>
                <p>Так как каждая школа оплачивается отдельно то нужно оплачивать каждую школу отдельно.</p>
                <p>Надо вывести параметр до какого числа оплачена школа.</p>

                <p>Если школа не оплачена то сайты не будут показываться, не будут собираться лиды с тильды и с внешних источников.</p>
                <p>
                    <a href="https://www.draw.io/#G1cMfYCmL8b9A0a9SEY-c6I6s-dNkgKheR" target="_blank">
                        <img src="/images/controller/admin-developer/school-pay/2019-02-16_08-21-14.png">
                    </a>
                </p>

                <p>Сделать заявки на оплату тарифов, чтобы можно было отслеживать где какие оплаты были и какие успешные.</p>

            </div>
        </div>



    </div>
</div>

