<?php

/** $this \yii\web\View  */

use yii\helpers\Url;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;

$this->title = '\avatar\controllers\actions\DefaultAjax - Действие контроллера по умолчанию';


?>



<div class="container">
    <div class="col-lg-12">
        <h1 class="text-center" style="margin-top:0px;padding-top:300px;font-size:400%;color:#000;text-shadow: -1px 0 1px white, 0 -1px 1px white, 0 1px 1px white, 1px 0 1px white, 0 0 8px white, 0 0 8px white, 0 0 8px white, 2px 2px 3px black;"><?= $this->title ?></h1>

        <p>Стандартный обработчик действия контроллера для AJAX вызовов.</p>
        <p>Класс: <code>\avatar\controllers\actions\DefaultAjax</code></p>
    </div>
</div>
