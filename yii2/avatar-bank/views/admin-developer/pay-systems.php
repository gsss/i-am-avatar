<?php

use common\services\UsersInCache;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;

/* @var $this yii\web\View */

$this->title = 'Платежные системы';

?>
<div class="container">
    <div class="col-lg-12">
        <h1 class="page-header text-center"><?= Html::encode($this->title) ?></h1>

        <p><img src="/images/controller/development-avatar-bank/pay-systems/paysytems_1.png"></p>

        <?= \common\services\documentation\DbTable::widget([
            'name'    => 'paysystems',
            'columns' => [
                [
                    'name'        => 'id',
                    'isRequired'  => true,
                    'description' => 'Идентификатор платежной системы',
                    'type'        => 'int',
                ],
                [
                    'name'        => 'code',
                    'isRequired'  => true,
                    'description' => 'Код платежной системы, малые английские буквы и/или дефис',
                ],
                [
                    'name'        => 'title',
                    'isRequired'  => true,
                    'description' => 'Название платежной системы (по русски)',
                ],

                [
                    'name'        => 'class_name',
                    'isRequired'  => true,
                    'description' => 'Название класса (только клас, без пути)',
                ],
                [
                    'name'        => 'image',
                    'isRequired'  => true,
                    'description' => 'Картинка',
                    'type'        => 'varchar(255)',
                ],
            ],
        ]) ?>

        <?= \common\services\documentation\DbTable::widget([
            'name'    => 'paysystems_config',
            'columns' => [
                [
                    'name'        => 'id',
                    'isRequired'  => true,
                    'description' => 'Идентификатор конфига платежной системы',
                    'type'        => 'int',
                ],
                [
                    'name'        => 'school_id',
                    'isRequired'  => true,
                    'description' => 'Идентификатор школы school.id',
                    'type'        => 'int',
                ],
                [
                    'name'        => 'paysystem_id',
                    'isRequired'  => true,
                    'description' => 'Идентификатор платежной системы, paysystems.id',
                    'type'        => 'int',
                ],
                [
                    'name'        => 'config',
                    'isRequired'  => true,
                    'description' => 'Конфигурация платежной системы JSON',
                ],
                [
                    'name'        => 'created_at',
                    'type'        => 'int',
                    'description' => 'Время добавления записи',
                ],
                [
                    'name'        => 'name',
                    'type'        => 'varchar(100)',
                    'description' => 'Название настройки',
                ],
                [
                    'name'        => 'is_hide',
                    'type'        => 'tinyint',
                    'description' => 'Флаг. Скрыть ПС? 0 - не скрывать, показывать. 1 - скрыть. 0 - по умолчанию',
                ],
            ],
        ]) ?>

        <h2 class="page-header">Структура настройки ПС в интерфейсе</h2>
        <p>Есть список платежных систем. Школа может добавить множество настроек любых платежных систем, в том числе и
            множество настроек для одной платежной системы.</p>
        <p>Настройка платежной системы производится по ссылке <code>/cabinet-school-pay-systems/settings?id=1</code> где
            id это идентификатор конфигурациии <code>paysystems_config.id</code>. Эта функция делает редирект на <code>/cabinet-school-pay-systems/&lt;paysystems.code&gt;?id=1</code>
        </p>
        <p><code>/cabinet-school-pay-systems/&lt;paysystems.code&gt;?id=1</code> - все эти функции прописываются в
            <code>\avatar\controllers\CabinetSchoolPaySystemsController::actions()</code>.</p>
        <p>так например для Яндекс кошелька code = <code>rub-yandex-wallet</code> class_name =
            <code>RubYandexWallet</code> значит функция actions() выдаст:</p>
        <pre>[
    'rub-yandex-wallet' => 'avatar\\modules\\PaySystems\\items\\RubYandexWallet\\action',
]</pre>
        <p>В итоге попадя на url <code>/cabinet-school-pay-systems/rub-yandex-wallet?id=1</code> будет вызван обработчик
            <code>\avatar\modules\PaySystems\items\RubYandexWallet\action</code></p>
        <p>В обработчике производится стандартная работа с формой (моделью) которая строится тоже по формуле <code>avatar\\modules\\PaySystems\\items\\RubYandexWallet\\model</code>.
        </p>
        <p>И будет вызвано представление <code>@avatar/views/cabinet-school-pay-systems/settings-edit</code> с передачей
            параметра <code>view</code> который будет вызван внутри для прорисовки только формы. Прорисовк формы тоже
            представлена в виде шаблона <code>avatar\\modules\\PaySystems\\items\\RubYandexWallet\\view</code>.</p>
        <p>После обработки формы все данные собираются моделью в JSON который сохраняется в поле <code>paysystems_config.config</code>
        </p>


        <h3 class="page-header">call-back</h3>
        <p>Некоторые платежные системы могут уведомлять по HTTP о совершенном платеже.</p>
        <p>Для автоматических подтверждений платежей используется на Аватаре специальная ссылка <code>/shop-order/success</code>
        </p>
        <p>В параметре GET <code>type</code> указывается платежная система.</p>


        <h3 class="page-header">Модуль банк</h3>
        <p>
            https://support.modulkassa.ru/upload/medialibrary/abb/API%20%D0%B0%D0%B2%D1%82%D0%BE%D0%BC%D0%B0%D1%82%D0%B8%D1%87%D0%B5%D1%81%D0%BA%D0%BE%D0%B9%20%D1%84%D0%B8%D1%81%D0%BA%D0%B0%D0%BB%D0%B8%D0%B7%D0%B0%D1%86%D0%B8%D0%B8%20%D1%87%D0%B5%D0%BA%D0%BE%D0%B2%20%D0%B8%D0%BD%D1%82%D0%B5%D1%80%D0%BD%D0%B5%D1%82-%D0%BC%D0%B0%D0%B3%D0%B0%D0%B7%D0%B8%D0%BD%D0%BE%D0%B2%20(ver.1.4).pdf
        </p>

        <h3 class="page-header">Робо банк</h3>
        <?= \common\services\documentation\DbTable::widget([
            'name'        => 'payments_robo_kassa',
            'model'       => '\common\models\PaymentRoboKassa',
            'description' => 'Платеж по робокассе',
            'columns'     => [
                [
                    'name'        => 'id',
                    'isRequired'  => true,
                    'description' => 'Идентификатор конфига платежной системы',
                    'type'        => 'int',
                ],
                [
                    'name'        => 'billing_id',
                    'isRequired'  => true,
                    'description' => 'Идентификатор счета billing.id',
                    'type'        => 'int',
                ],
                [
                    'name'        => 'currency_id',
                    'isRequired'  => true,
                    'description' => 'Идентификатор валюты db.currency.id',
                    'type'        => 'int',
                ],
                [
                    'name'        => 'transaction',
                    'isRequired'  => true,
                    'description' => 'Транзакция от Робокассы, JSON',
                    'type'        => 'string',
                ],
                [
                    'name'        => 'amount',
                    'isRequired'  => true,
                    'description' => 'Количество атомов для оплаты',
                    'type'        => 'int',
                ],
                [
                    'name'        => 'created_at',
                    'isRequired'  => true,
                    'type'        => 'int',
                    'description' => 'Время добавления записи',
                ],
            ],
        ]) ?>
        <p><a href="https://docs.robokassa.ru/" target="_blank">https://docs.robokassa.ru/</a></p>
        <p>При выставлении заказа параметр <code>Shp_item</code> = 1, дата <code>ExpirationDate</code> равна текущей + 1 день, параметр <code>IncCurrLabel</code> = <code>BANKOCEAN2R</code>. Параметр <code>Culture</code> = <code>ru</code>.</p>

    </div>
</div>



