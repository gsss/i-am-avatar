<?php

/** $this \yii\web\View  */

use yii\helpers\Url;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;

$this->title = 'СМС рассылка';


?>

<div class="container">
    <div class="col-lg-12">
        <h1 class="page-header"><?= Html::encode($this->title) ?></h1>

        <p>Сейчас рассылка ведется только по телефонам россии (+7). Весь список обрабатывается и делается рассылка. Очищается от коротких и длинных номеров. и после этого делается рассылка.</p>

        <?= \common\services\documentation\DbTable::widget([
            'name'        => 'school_sms_subscribe',
            'description' => 'Запуск СМС рассылки',
            'model'       => '\common\models\school\SubscribeSms',
            'columns'     => [
                [
                    'name'        => 'id',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Идентификатор строки',
                ],
                [
                    'name'        => 'school_id',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Идентификатор школы',
                ],
                [
                    'name'        => 'potok_id',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Идентификатор потока',
                ],
                [
                    'name'        => 'created_at',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Время создания',
                ],
            ],
        ]) ?>

        <?= \common\services\documentation\DbTable::widget([
            'name'        => 'school_sms_subscribe_result',
            'description' => 'отчет СМС рассылки',
            'model'       => '\common\models\school\SubscribeSmsResult',
            'columns'     => [
                [
                    'name'        => 'id',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Идентификатор строки',
                ],
                [
                    'name'        => 'subscribe_id',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Идентификатор рассылки',
                ],
                [
                    'name'        => 'phone',
                    'type'        => 'string',
                    'isRequired'  => true,
                    'description' => 'телефон',
                ],
                [
                    'name'        => 'result',
                    'type'        => 'string',
                    'isRequired'  => true,
                    'description' => 'Результат выполнения',
                ],
                [
                    'name'        => 'created_at',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Время создания',
                ],
            ],
        ]) ?>

        <p>Далее что хочу так это сделать чтобы эта рассылка списывалась с баланса. Как это может происходить?</p>
        <ul>
            <li>Производится оценка стоимости рассылки</li>
            <li>Проверяется баланс</li>
            <li>Если успешно то производится списание стоимости рассылки и производится рассылка</li>
            <li>Если не достаточно денег то сообщается о том чтобы клиент пополнил баланс кошелька.</li>
        </ul>
        <p>Списывание будет производиться с кошелька по умолчанию Золотого Эликсира на счет общий.</p>
        <p>Например даже сейчас можно сделать например так. 1 смс = 2 рубля.</p>
        <p>Значит нужна сначала функция оценки, списания, и рассылки.
            Скорее это одна будет.
            Возвращать будет.
            103 - недостаточно денег нужно 1500 руб.
            Спрашивать перед каждой рассылкой.</p>
        <p><code>/cabinet-school-potok/sms-send</code> - функция которая делает рассылку.</p>
        <p>И еще надо список сделанных рассылок сделать.</p>

    </div>
</div>

