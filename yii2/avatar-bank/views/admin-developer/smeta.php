<?php

/** $this \yii\web\View  */

use yii\helpers\Url;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;

$this->title = 'Смета';


?>

<div class="container">
    <div class="col-lg-12">
        <h1 class="page-header"><?= Html::encode($this->title) ?></h1>

        <h2 class="page-header">Введение</h2>
        <p>Разделю понятия плановые расходы и фактические расходы. Плановые расходы, мы планируем заранее, элемент
            который уже прошел нельзя редактировать. Фактические вычисляются из задач на момент закрытия задачи.</p>

        <h2 class="page-header">Плановые расходы</h2>
        <p>В этой таблице хранится месячный элемент, планы на расходы.</p>
        <p><img src="/images/controller/admin-developer/smeta/smeta_form.gif" class="thumbnail"></p>

        <?= \common\services\documentation\DbTable::widget([
            'name'        => 'school_smeta',
            'description' => 'Массив элементов расходов',
            'model'       => '\common\models\Smeta',
            'columns'     => [
                [
                    'name'        => 'id',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Идентификатор элемента',
                ],
                [
                    'name'        => 'school_id',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Идентификатор школы',
                ],
                [
                    'name'        => 'created_at',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Момент создания записи',
                ],
                [
                    'name'        => 'category_id',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Идентификатор категории school_task_category.id',
                ],
                [
                    'name'        => 'year',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Год',
                ],
                [
                    'name'        => 'month',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Месяц',
                ],
                [
                    'name'        => 'sum',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Сумма планового расхода в этот месяц',
                ],
            ],
        ]) ?>


    </div>
</div>

