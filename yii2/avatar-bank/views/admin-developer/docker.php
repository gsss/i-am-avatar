<?php
/** $this \yii\web\View  */

use yii\helpers\Url;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;

$this->title = 'docker';
?>


<div class="container">
    <div class="col-lg-12">
        <h1 class="page-header"><?= Html::encode($this->title) ?></h1>


        <p><a href="https://youtu.be/a5mxBTGfC5k" target="_blank">https://youtu.be/a5mxBTGfC5k</a> - Установка Docker и Docker-compose на Windows | уроки docker | все о docker | #1.0</p>
        <p><a href="https://youtu.be/BpJevJwkBLQ" target="_blank">https://youtu.be/BpJevJwkBLQ</a> - Описание</p>
        <p><a href="https://youtu.be/2GpWeLJESmc" target="_blank">https://youtu.be/2GpWeLJESmc</a> - Описание</p>
        <p><a href="https://youtu.be/7sJgZNqanII" target="_blank">https://youtu.be/7sJgZNqanII</a> - Установка и работа с Docker</p>
        <p><a href="https://zarbis.me/docker-host-wonders/" target="_blank">https://zarbis.me/docker-host-wonders/</a> - Чудеса DOCKER_HOST</p>

        <p>https://habr.com/ru/post/310460/</p>

        <p>зайти в контейнер:</p>
        <pre>docker exec -it (service-name) /bin/bash</pre>
        <pre>3000 - чат (socket.io)</pre>
        <pre>3081 - главный сайт</pre>
        <pre>3082 - сайт компании</pre>
        <pre>3083 - сайт компании</pre>
        <pre>3084 - сайт компании</pre>
        <pre>3085 - cdn</pre>

    </div>
</div>
