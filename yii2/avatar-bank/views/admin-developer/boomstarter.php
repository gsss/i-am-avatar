<?php

/** $this \yii\web\View  */

use yii\helpers\Url;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;

$this->title = 'Boomstarter';


?>

<div class="container">
    <div class="col-lg-12">
        <h1 class="page-header"><?= Html::encode($this->title) ?></h1>
        <h2 class="page-header">Сценарий ролика</h2>
        <p><b>Текст видеоролика</b></p>
        <p>Текст</p>
        <p>Что мы хотим сделать?</p>
        <p>Как это поможет обществу?</p>

        <h2 class="page-header">Цели платформы</h2>

        <p>Создание программы обучения детей.</p>

        <h2 class="page-header">Подарки</h2>

        <p><b>500 р.</b></p>
        <p>Благодарность и добавлене в список спонсоров. Вы войдете в историю.</p>
        <p>
            Благодарность и добавлене в список спонсоров.
            Вы войдете в историю.
            Получаете 5,000 ELX (Испольование платформы по тарифу и/или скидка 20% при оплате любого курса платформы)
        </p>

        <p><b>1,000 р.</b></p>
        <p>
            Благодарность и добавлене в список спонсоров.
            Вы войдете в историю.
            Получаете 1,000 ELX (Испольование платформы по тарифу и/или скидка 20% при оплате любого курса платформы)
        </p>

        <p><b>2,000 р.</b></p>
        <p>
            Благодарность и добавлене в список спонсоров.
            Вы войдете в историю.
            Получаете 2,000 ELX (Испольование платформы по тарифу и/или скидка 20% при оплате любого курса платформы)
        </p>

        <p><b>5,000 р.</b></p>
        <p>
            Благодарность и добавлене в список спонсоров.
            Вы войдете в историю.
            Получаете 5,000 ELX (Испольование платформы по тарифу и/или скидка 20% при оплате любого курса платформы)
        </p>

        <p><b>10,000 р.</b></p>
        <p>
            Благодарность и добавлене в список спонсоров.
            Вы войдете в историю.
            Получаете 10,000 ELX (Испольование платформы по тарифу и/или скидка 20% при оплате любого курса платформы)
        </p>

        <p><b>20,000 р.</b></p>
        <p>
            Благодарность и добавлене в список спонсоров.
            Вы войдете в историю.
            Получаете 20,000 ELX (Испольование платформы по тарифу и/или скидка 20% при оплате любого курса платформы)
        </p>

        <p><b>50,000 р.</b></p>
        <p>Размещение логотипа во всех рекламных компаниях как партнера и спонсора.</p>
        <p>
            Благодарность и добавлене в список спонсоров.
            Вы войдете в историю.
            Получаете 50,000 ELX (Испольование платформы по тарифу и/или скидка 20% при оплате любого курса платформы)
        </p>

        <p><b>100,000 р.</b></p>
        <p>Размещение логотипа во всех рекламных компаниях как партнера и спонсора.</p>
        <p>
            Благодарность и добавлене в список спонсоров.
            Вы войдете в историю.
            Получаете 100,000 ELX (Испольование платформы по тарифу и/или скидка 20% при оплате любого курса платформы)
        </p>

        <h2 class="page-header">Структурная схема проекта</h2>
        <p><img src="/images/controller/admin-developer/boomstarter/schema.png"></p>

        <h2 class="page-header">Расчетная эдиница</h2>
        <p>Название: Элексир</p>
        <p>Работает внутри платформы</p>
        <p>Сокращенное название: ELX</p>
        <p><a href="http://dobehappy.ru/eliksir-dlya-dushi" target="_blank">Безусловная любовь – эликсир для души</a>
        </p>
        <p>
            <a href="http://sluzheniyasveta.ucoz.ru/publ/instrumenty_dlja_raboty/praktiki_meditacii/molitva_otche_nash_novogo_vremeni/5-1-0-7"
               target="_blank">ЭЛИКСИР</a></p>

        <h2 class="page-header">Тарифы</h2>
        <p><img src="/images/controller/admin-developer/boomstarter/table.jpeg" width="800" class="thumbnail"></p>

        <?php
        $rows = [
            [
                'name'      => 'Ангел',
                'users'     => 1000,
                'space'     => '20',
                'price'     => '4000',
                'discont3'  => '30',
                'discont12' => '50',
            ],
            [
                'name'      => 'Архангел',
                'users'     => '2000',
                'space'     => '30',
                'price'     => '6700',
                'discont3'  => '30',
                'discont12' => '50',
            ],
            [
                'name'      => 'Начало',
                'users'     => '5000',
                'space'     => '50',
                'price'     => '13200',
                'discont3'  => '30',
                'discont12' => '50',
            ],
            [
                'name'      => 'Власть',
                'users'     => '10000',
                'space'     => '100',
                'price'     => '20000',
                'discont3'  => '30',
                'discont12' => '50',
            ],
            [
                'name'      => 'Сила',
                'users'     => '25000',
                'space'     => '250',
                'price'     => '26700',
                'discont3'  => '30',
                'discont12' => '50',
            ],
            [
                'name'      => 'Господство',
                'users'     => '50000',
                'space'     => '500',
                'price'     => '40000',
                'discont3'  => '30',
                'discont12' => '50',
            ],
            [
                'name'      => 'Престол',
                'users'     => '100000',
                'space'     => '500',
                'price'     => '53000',
                'discont3'  => '30',
                'discont12' => '50',
            ],
            [
                'name'      => 'Херувим',
                'users'     => '200000',
                'space'     => '1000',
                'price'     => '71000',
                'discont3'  => '30',
                'discont12' => '50',
            ],
            [
                'name'      => 'Серафим',
                'users'     => '300000',
                'space'     => '2000',
                'price'     => '89000',
                'discont3'  => '30',
                'discont12' => '50',
            ],
        ]; ?>
        <?= \yii\grid\GridView::widget([
            'dataProvider' => new \yii\data\ArrayDataProvider([
                'allModels' => $rows,
            ]),
            'tableOptions' => [
                'class' => 'table table-striped table-hover',
                'style' => 'width: auto',
            ],
            'summary'      => '',
            'columns'      => [
                [
                    'header'    => 'Тариф',
                    'attribute' => 'name',
                ],
                [
                    'header'    => 'Польователей',
                    'attribute' => 'users',
                    'format'         => ['decimal', 0],
                    'contentOptions' => [
                        'style' => 'text-align:right;'
                    ],
                    'headerOptions'  => [
                        'style' => 'text-align:right;'
                    ],
                ],
                [
                    'header'    => 'Места',
                    'attribute' => 'space',
                    'contentOptions' => [
                        'style' => 'text-align:right;'
                    ],
                    'headerOptions'  => [
                        'style' => 'text-align:right;'
                    ],
                    'content'   => function ($item) {
                        return Yii::$app->formatter->asDecimal($item['space']) . ' Гб';
                    }
                ],
                [
                    'header'         => 'Стоимость в мес',
                    'attribute'      => 'price',
                    'format'         => ['decimal', 0],
                    'contentOptions' => [
                        'style' => 'text-align:right;'
                    ],
                    'headerOptions'  => [
                        'style' => 'text-align:right;'
                    ],
                ],
                [
                    'header'         => 'Скидка при оплате за 3 мес',
                    'attribute'      => 'discont3',
                    'contentOptions' => [
                        'style' => 'text-align:right;'
                    ],
                    'headerOptions'  => [
                        'style' => 'text-align:right;'
                    ],
                    'content'        => function ($item) {
                        $v = $item['price'] * 3;
                        return Yii::$app->formatter->asDecimal($v * ((100-$item['discont3']) / 100));
                    }
                ],
                [
                    'header'         => 'Скидка при оплате за 12 мес',
                    'attribute'      => 'discont12',
                    'contentOptions' => [
                        'style' => 'text-align:right;'
                    ],
                    'headerOptions'  => [
                        'style' => 'text-align:right;'
                    ],
                    'content'        => function ($item) {
                        return Yii::$app->formatter->asDecimal($item['price'] * 12 * ((100-$item['discont12']) / 100));
                    }
                ],
            ],
        ]) ?>
        <?php
        $rows = [
            [
                'name'      => 'Ангел',
                'm1'      => 1,
                'm2'      => 1,
            ],
            [
                'name'      => 'Архангел',
                'm1'      => 2.5,
            ],
            [
                'name'      => 'Начало',
                'm1'      => 5,
            ],
            [
                'name'      => 'Власть',
                'm1'      => 10,
            ],
            [
                'name'      => 'Сила',
                'm1'      => 25,
            ],
            [
                'name'      => 'Господство',
                'm1'      => 50,
            ],
            [
                'name'      => 'Престол',
                'm1'      => 100,
            ],
            [
                'name'      => 'Херувим',
                'm1'      => 250,
            ],
            [
                'name'      => 'Серафим',
                'm1'      => 500,
                'm2'      => 0.12,
            ],
        ];
        $users = 1000;
        $price = 3000;
        for($i = 0;$i<count($rows);$i++) {
            $rows[$i]['users'] = $users * $rows[$i]['m1'];
            if ($i > 0) {
                if ($i < count($rows) - 1) {
                    $diff = $rows[0]['m2'] - $rows[count($rows)-1]['m2'];
                    $one = $diff / (count($rows) - 1);
                    $rows[$i]['m2'] = $rows[0]['m2'] - ($one * $i);
                }
            }
            $rows[$i]['price'] = $price * $rows[$i]['m1'] * $rows[$i]['m2'];
            $rows[$i]['discont3'] = 30;
            $rows[$i]['discont12'] = 50;
        }
        ?>
        <?php
        $usdPerGb = 0.18;
        Yii::$app->session->set('$usdPerGb', $usdPerGb);
        ?>
        <p>USD/GB: <?= $usdPerGb ?></p>
        <?= \yii\grid\GridView::widget([
            'dataProvider' => new \yii\data\ArrayDataProvider([
                'allModels' => $rows,
            ]),
            'tableOptions' => [
                'class' => 'table table-striped table-hover',
                'style' => 'width: auto',
            ],
            'summary'      => '',
            'columns'      => [
                [
                    'header'    => 'Тариф',
                    'attribute' => 'name',
                ],
                [
                    'header'    => 'Пользователей',
                    'attribute' => 'users',
                    'format'         => ['decimal', 0],
                    'contentOptions' => [
                        'style' => 'text-align:right;'
                    ],
                    'headerOptions'  => [
                        'style' => 'text-align:right;'
                    ],
                ],
                'm1',
                'm2',
                [
                    'header'    => 'Места',
                    'attribute' => 'space',
                    'contentOptions' => [
                        'style' => 'text-align:right;'
                    ],
                    'headerOptions'  => [
                        'style' => 'text-align:right;'
                    ],
                    'content'   => function ($item) {
                        return Yii::$app->formatter->asDecimal($item['users'] / 50) . ' Гб';
                    }
                ],
                [
                    'header'         => 'Стоимость в мес',
                    'attribute'      => 'price',
                    'format'         => ['decimal', 0],
                    'contentOptions' => [
                        'style' => 'text-align:right;'
                    ],
                    'headerOptions'  => [
                        'style' => 'text-align:right;'
                    ],
                ],
                [
                    'header'         => 'Скидка при оплате за 3 мес',
                    'attribute'      => 'discont3',
                    'contentOptions' => [
                        'style' => 'text-align:right;'
                    ],
                    'headerOptions'  => [
                        'style' => 'text-align:right;'
                    ],
                    'content'        => function ($item) {
                        $v = $item['price'] * 3;
                        return Yii::$app->formatter->asDecimal($v * ((100-$item['discont3']) / 100));
                    }
                ],
                [
                    'header'         => 'Скидка при оплате за 12 мес',
                    'attribute'      => 'discont12',
                    'contentOptions' => [
                        'style' => 'text-align:right;'
                    ],
                    'headerOptions'  => [
                        'style' => 'text-align:right;'
                    ],
                    'content'        => function ($item) {
                        return Yii::$app->formatter->asDecimal($item['price'] * 12 * ((100-$item['discont12']) / 100));
                    }
                ],
                [
                    'header'         => 'USD/GB',
                    'contentOptions' => [
                        'style' => 'text-align:right;'
                    ],
                    'headerOptions'  => [
                        'style' => 'text-align:right;'
                    ],
                    'content'        => function ($item) {
                        $usdPerGb = Yii::$app->session->get('$usdPerGb');

                        return Yii::$app->formatter->asDecimal(($item['users'] / 50) *  $usdPerGb);
                    }
                ],
                [
                    'header'         => 'Прибыль',
                    'contentOptions' => [
                        'style' => 'text-align:right;'
                    ],
                    'headerOptions'  => [
                        'style' => 'text-align:right;'
                    ],
                    'content'        => function ($item) {
                        $usdPerGb = Yii::$app->session->get('$usdPerGb');
                        $usd = ($item['users'] / 50) *  $usdPerGb;

                        return Yii::$app->formatter->asDecimal($item['price'] - ($usd*66));
                    }
                ],
            ],
        ]) ?>

    </div>
</div>