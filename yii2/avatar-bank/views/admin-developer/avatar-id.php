<?php

/** $this \yii\web\View  */

use yii\helpers\Url;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;

$this->title = 'Таблица выданных карт';


?>

<div class="container">
    <div class="col-lg-12">
        <h1 class="page-header"><?= Html::encode($this->title) ?></h1>

        <?= \common\services\documentation\DbTable::widget([
            'name'        => 'card_id',
            'description' => 'Выданные идентификационные карты',
            'model'       => '\common\models\CardId',
            'columns'     => [
                [
                    'name'        => 'id',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Идентификатор элемента',
                ],
                [
                    'name'        => 'name_first',
                    'type'        => 'varchar(100)',
                    'isRequired'  => true,
                    'description' => 'Имя',
                ],
                [
                    'name'        => 'name_patronymic',
                    'type'        => 'varchar(100)',
                    'isRequired'  => true,
                    'description' => 'Отчество',
                ],
                [
                    'name'        => 'name_last',
                    'type'        => 'varchar(100)',
                    'isRequired'  => true,
                    'description' => 'Фамилия',
                ],
                [
                    'name'        => 'date_born',
                    'type'        => 'date',
                    'isRequired'  => true,
                    'description' => 'Дата рождения',
                ],
                [
                    'name'        => 'date_get',
                    'type'        => 'date',
                    'isRequired'  => true,
                    'description' => 'Дата получения',
                ],
                [
                    'name'        => 'place_born',
                    'type'        => 'varchar(100)',
                    'isRequired'  => true,
                    'description' => 'Место рождения',
                ],
                [
                    'name'        => 'image_top',
                    'type'        => 'varchar(100)',
                    'isRequired'  => true,
                    'description' => 'Изображение переднее, ширина 1000 px',
                ],
                [
                    'name'        => 'image_back',
                    'type'        => 'varchar(100)',
                    'isRequired'  => true,
                    'description' => 'Изображение заднее, ширина 1000 px',
                ],
                [
                    'name'        => 'user_id',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Идентификатор пользователя',
                ],
                [
                    'name'        => 'created_at',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Момент создания записи',
                ],
            ],
        ]) ?>



    </div>
</div>

