<?php

/** $this \yii\web\View  */

use yii\helpers\Url;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;

$this->title = 'Анкета для школы';


?>

<div class="container">
    <div class="col-lg-12">
        <h1 class="page-header"><?= Html::encode($this->title) ?></h1>

        <?= \common\services\documentation\DbTable::widget([
            'name'        => 'anketa_school',
            'description' => 'Анкета для школы',
            'model'       => '\common\models\AnketaSchool',
            'columns'     => [
                [
                    'name'        => 'id',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Идентификатор элемента',
                ],
                [
                    'name'        => 'created_at',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Момент создания записи',
                ],
                [
                    'name'        => 'name',
                    'type'        => 'varchar(100)',
                    'isRequired'  => true,
                    'description' => 'Название школы',
                ],
                [
                    'name'        => 'description',
                    'type'        => 'text',
                    'isRequired'  => true,
                    'description' => 'Описание',
                ],
                [
                    'name'        => 'email',
                    'type'        => 'varchar(100)',
                    'isRequired'  => true,
                    'description' => 'Название школы',
                ],
                [
                    'name'        => 'phone',
                    'type'        => 'varchar(100)',
                    'isRequired'  => true,
                    'description' => 'Название школы',
                ],

            ],
        ]) ?>

        <p>После добавления анкеты школы <code>/avatar-bank/views/anketa/new-school.php</code> для роли <code>role_admin</code> выслылается письмо <code>/avatar-bank/mail/html/new_anketa-school.php</code>.</p>

        <p>После добавления анкеты <code>/avatar-bank/views/anketa/new.php</code> для роли <code>role_admin_command</code> выслылается письмо <code>/avatar-bank/mail/html/new_anketa.php</code>.</p>


    </div>
</div>

