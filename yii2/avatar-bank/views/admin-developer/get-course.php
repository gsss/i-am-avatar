<?php

/** $this \yii\web\View  */

use yii\helpers\Url;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;

$this->title = 'GetCourse';


?>

<div class="container">
    <div class="col-lg-12">
        <h1 class="page-header"><?= Html::encode($this->title) ?></h1>

        <p>getcourse.ru</p>

        <h2>Сайт Страницы</h2>
        <p><img src="/images/controller/admin-developer/get-course/2018-11-14_22-31-38.png" class="thumbnail"></p>

        <div>
            <p><img src="/images/controller/admin-developer/get-course/2018-11-14_22-33-07.png" width="100%" class="thumbnail"></p>
            <p><img src="/images/controller/admin-developer/get-course/2018-11-14_22-33-35.png" width="100%" class="thumbnail"></p>
            <p><img src="/images/controller/admin-developer/get-course/2018-11-14_22-33-51.png" width="100%" class="thumbnail"></p>
            <p><img src="/images/controller/admin-developer/get-course/2018-11-14_22-39-00.png" width="100%" class="thumbnail"></p>
        </div>

        <h2>Сайт Виджеты</h2>
        <p><img src="/images/controller/admin-developer/get-course/2018-11-14_22-39-36.png" class="thumbnail"></p>

        <div>
            <p><img src="/images/controller/admin-developer/get-course/2018-11-14_22-40-17.png" width="100%" class="thumbnail"></p>
            <p><img src="/images/controller/admin-developer/get-course/2018-11-14_22-40-35.png" width="100%" class="thumbnail"></p>
        </div>

        <h2>Сайт Вебинары</h2>
        <p><img src="/images/controller/admin-developer/get-course/2018-11-14_22-42-45.png" class="thumbnail"></p>

        <div>
            <p><img src="/images/controller/admin-developer/get-course/2018-11-14_22-43-19.png" width="100%" class="thumbnail"></p>
            <p><img src="/images/controller/admin-developer/get-course/2018-11-14_22-43-44.png" width="100%" class="thumbnail"></p>
            <p><img src="/images/controller/admin-developer/get-course/2018-11-14_22-44-00.png" width="100%" class="thumbnail"></p>
            <p><img src="/images/controller/admin-developer/get-course/2018-11-14_22-44-16.png" width="100%" class="thumbnail"></p>
            <p><img src="/images/controller/admin-developer/get-course/2018-11-14_22-44-25.png" width="100%" class="thumbnail"></p>
            <p><img src="/images/controller/admin-developer/get-course/2018-11-14_22-47-07.png" class="thumbnail"></p>
            <p><img src="/images/controller/admin-developer/get-course/2018-11-14_22-47-33.png" class="thumbnail"></p>
        </div>

        <h2>Сайт Трафик</h2>
        <p><img src="/images/controller/admin-developer/get-course/2018-11-14_22-48-56.png" class="thumbnail"></p>

        <div>
            <p><img src="/images/controller/admin-developer/get-course/2018-11-14_22-50-02.png" width="100%" class="thumbnail"></p>
            <p><img src="/images/controller/admin-developer/get-course/2018-11-14_22-51-03.png" width="100%" class="thumbnail"></p>
            <p><img src="/images/controller/admin-developer/get-course/2018-11-14_22-52-18.png" width="100%" class="thumbnail"></p>
            <p><img src="/images/controller/admin-developer/get-course/2018-11-14_22-52-34.png" width="100%" class="thumbnail"></p>
            <p><img src="/images/controller/admin-developer/get-course/2018-11-14_22-52-48.png" width="100%" class="thumbnail"></p>
            <p><img src="/images/controller/admin-developer/get-course/2018-11-14_22-53-20.png" width="100%" class="thumbnail"></p>
            <p><img src="/images/controller/admin-developer/get-course/2018-11-14_22-54-00.png" width="100%" class="thumbnail"></p>
            <p><img src="/images/controller/admin-developer/get-course/2018-11-14_22-55-13.png" width="100%" class="thumbnail"></p>
            <p><img src="/images/controller/admin-developer/get-course/2018-11-14_22-55-24.png" width="100%" class="thumbnail"></p>
        </div>


        <h2>Обучение Тренинги</h2>
        <p><img src="/images/controller/admin-developer/get-course/2018-11-14_22-34-34.png" class="thumbnail"></p>

        <div>
            <p><img src="/images/controller/admin-developer/get-course/2018-11-14_22-56-39.png" width="100%" class="thumbnail"></p>
            <p><img src="/images/controller/admin-developer/get-course/2018-11-14_22-57-14.png" width="100%" class="thumbnail"></p>
            <p><img src="/images/controller/admin-developer/get-course/2018-11-14_23-00-58.png" width="100%" class="thumbnail"></p>
            <p><img src="/images/controller/admin-developer/get-course/2018-11-14_19-18-38.png" width="100%" class="thumbnail"></p>
            <p><img src="/images/controller/admin-developer/get-course/2018-11-14_19-18-56.png" width="100%" class="thumbnail"></p>
            <p><img src="/images/controller/admin-developer/get-course/2018-11-14_19-19-13.png" width="100%" class="thumbnail"></p>
            <p><img src="/images/controller/admin-developer/get-course/2018-11-14_19-19-21.png" width="100%" class="thumbnail"></p>
            <p><img src="/images/controller/admin-developer/get-course/2018-11-14_19-19-41.png" width="100%" class="thumbnail"></p>
            <p><img src="/images/controller/admin-developer/get-course/2018-11-14_19-20-02.png" width="100%" class="thumbnail"></p>
            <p><img src="/images/controller/admin-developer/get-course/2018-11-14_19-20-12.png" width="100%" class="thumbnail"></p>
            <p><img src="/images/controller/admin-developer/get-course/2018-11-14_19-21-17.png" class="thumbnail"></p>
            <p><img src="/images/controller/admin-developer/get-course/2018-11-14_19-26-33.png" class="thumbnail"></p>
            <p><img src="/images/controller/admin-developer/get-course/2018-11-14_19-30-18.png" width="100%" class="thumbnail"></p>
            <p><img src="/images/controller/admin-developer/get-course/2018-11-14_19-30-27.png" width="100%" class="thumbnail"></p>
            <p><img src="/images/controller/admin-developer/get-course/2018-11-14_19-30-34.png" width="100%" class="thumbnail"></p>
            <p><img src="/images/controller/admin-developer/get-course/2018-11-14_19-31-21.png" width="100%" class="thumbnail"></p>
        </div>


        <h2>Обучение Расписание</h2>
        <p><img src="/images/controller/admin-developer/get-course/2018-11-14_23-02-01.png" class="thumbnail"></p>

        <div>
            <p><img src="/images/controller/admin-developer/get-course/2018-11-14_23-03-46.png" width="100%" class="thumbnail"></p>
        </div>

        <h2>Обучение Лента ответов</h2>
        <p><img src="/images/controller/admin-developer/get-course/2018-11-14_23-03-08.png" class="thumbnail"></p>

        <div>
            <p><img src="/images/controller/admin-developer/get-course/2018-11-14_23-03-56.png" width="100%" class="thumbnail"></p>
        </div>

        <h2>Пользователи Пользователи</h2>
        <p><img src="/images/controller/admin-developer/get-course/2018-11-14_23-06-01.png" class="thumbnail"></p>

        <div>
            <p><img src="/images/controller/admin-developer/get-course/2018-11-14_23-07-30.png" width="100%" class="thumbnail"></p>
            <p><img src="/images/controller/admin-developer/get-course/2018-11-14_23-07-50.png" width="100%" class="thumbnail"></p>
            <p><img src="/images/controller/admin-developer/get-course/2018-11-14_23-07-56.png" width="100%" class="thumbnail"></p>
            <p><img src="/images/controller/admin-developer/get-course/2018-11-14_23-08-18.png" width="100%" class="thumbnail"></p>
            <p><img src="/images/controller/admin-developer/get-course/2018-11-14_23-08-29.png" width="100%" class="thumbnail"></p>
            <p><img src="/images/controller/admin-developer/get-course/2018-11-14_23-08-48.png" width="100%" class="thumbnail"></p>
        </div>

        <h2>Пользователи Звонки и встречи</h2>
        <p><img src="/images/controller/admin-developer/get-course/2018-11-14_23-10-38.png" class="thumbnail"></p>

        <div>
            <p><img src="/images/controller/admin-developer/get-course/2018-11-14_23-10-48.png" width="100%" class="thumbnail"></p>
            <p><img src="/images/controller/admin-developer/get-course/2018-11-14_23-11-09.png" width="100%" class="thumbnail"></p>
        </div>

        <h2>Пользователи Анкеты</h2>
        <p><img src="/images/controller/admin-developer/get-course/2018-11-14_23-13-31.png" class="thumbnail"></p>

        <div>
            <p><img src="/images/controller/admin-developer/get-course/2018-11-14_23-13-41.png" width="100%" class="thumbnail"></p>
            <p><img src="/images/controller/admin-developer/get-course/2018-11-14_23-13-47.png" width="100%" class="thumbnail"></p>
        </div>

        <h2>Пользователи Воронки</h2>
        <p><img src="/images/controller/admin-developer/get-course/2018-11-14_23-15-01.png" class="thumbnail"></p>

        <div>
            <p><img src="/images/controller/admin-developer/get-course/2018-11-14_23-15-17.png" width="100%" class="thumbnail"></p>
            <p><img src="/images/controller/admin-developer/get-course/2018-11-14_23-15-27.png" width="100%" class="thumbnail"></p>
            <p><img src="/images/controller/admin-developer/get-course/2018-11-14_23-15-39.png" width="100%" class="thumbnail"></p>
            <p><img src="/images/controller/admin-developer/get-course/2018-11-14_23-15-46.png" width="100%" class="thumbnail"></p>
        </div>

        <h2>Пользователи Статистика</h2>
        <p><img src="/images/controller/admin-developer/get-course/2018-11-14_23-17-41.png" class="thumbnail"></p>

        <div>
            <p><img src="/images/controller/admin-developer/get-course/2018-11-14_23-18-09.png" width="100%" class="thumbnail"></p>
        </div>

        <h2>Пользователи Настройки</h2>
        <p><img src="/images/controller/admin-developer/get-course/2018-11-14_23-19-39.png" class="thumbnail"></p>

        <div>
            <p><img src="/images/controller/admin-developer/get-course/2018-11-14_23-19-59.png" width="100%" class="thumbnail"></p>
            <p><img src="/images/controller/admin-developer/get-course/2018-11-14_23-20-11.png" width="100%" class="thumbnail"></p>
            <p><img src="/images/controller/admin-developer/get-course/2018-11-14_23-20-23.png" width="100%" class="thumbnail"></p>
            <p><img src="/images/controller/admin-developer/get-course/2018-11-14_23-20-43.png" width="100%" class="thumbnail"></p>
            <p><img src="/images/controller/admin-developer/get-course/2018-11-14_23-20-51.png" width="100%" class="thumbnail"></p>
            <p><img src="/images/controller/admin-developer/get-course/2018-11-14_23-21-12.png" width="100%" class="thumbnail"></p>
            <p><img src="/images/controller/admin-developer/get-course/2018-11-14_23-21-39.png" width="100%" class="thumbnail"></p>
            <p><img src="/images/controller/admin-developer/get-course/2018-11-14_23-21-49.png" width="100%" class="thumbnail"></p>
        </div>

        <h2>Задачи Задачи</h2>
        <p><img src="/images/controller/admin-developer/get-course/2018-11-14_23-25-04.png" class="thumbnail"></p>

        <div>
            <p><img src="/images/controller/admin-developer/get-course/2018-11-14_23-25-17.png" width="100%" class="thumbnail"></p>
            <p><img src="/images/controller/admin-developer/get-course/2018-11-14_23-25-53.png" width="100%" class="thumbnail"></p>
            <p><img src="/images/controller/admin-developer/get-course/2018-11-14_23-25-58.png" width="100%" class="thumbnail"></p>
            <p><img src="/images/controller/admin-developer/get-course/2018-11-14_23-26-09.png" width="100%" class="thumbnail"></p>
            <p><img src="/images/controller/admin-developer/get-course/2018-11-14_23-26-20.png" width="100%" class="thumbnail"></p>
        </div>

        <h2>Задачи Процессы</h2>
        <p><img src="/images/controller/admin-developer/get-course/2018-11-14_23-27-52.png" class="thumbnail"></p>

        <div>
            <p><img src="/images/controller/admin-developer/get-course/2018-11-14_23-28-03.png" width="100%" class="thumbnail"></p>
            <p><img src="/images/controller/admin-developer/get-course/2018-11-14_23-28-18.png" width="100%" class="thumbnail"></p>
            <p><img src="/images/controller/admin-developer/get-course/2018-11-14_23-28-33.png" width="100%" class="thumbnail"></p>
            <p><img src="/images/controller/admin-developer/get-course/2018-11-14_23-28-41.png" width="100%" class="thumbnail"></p>
            <p><img src="/images/controller/admin-developer/get-course/2018-11-14_23-28-48.png" width="100%" class="thumbnail"></p>
            <p><img src="/images/controller/admin-developer/get-course/2018-11-14_23-28-54.png" width="100%" class="thumbnail"></p>
            <p><img src="/images/controller/admin-developer/get-course/2018-11-14_23-28-59.png" width="100%" class="thumbnail"></p>
            <p><img src="/images/controller/admin-developer/get-course/2018-11-14_23-29-12.png" width="100%" class="thumbnail"></p>
        </div>

        <h2>Задачи Статистика</h2>
        <p><img src="/images/controller/admin-developer/get-course/2018-11-14_23-31-13.png" class="thumbnail"></p>

        <div>
            <p><img src="/images/controller/admin-developer/get-course/2018-11-14_23-31-22.png" width="100%" class="thumbnail"></p>
            <p><img src="/images/controller/admin-developer/get-course/2018-11-14_23-31-30.png" width="100%" class="thumbnail"></p>
        </div>

        <h2>Сообщения Входящие</h2>
        <p><img src="/images/controller/admin-developer/get-course/2018-11-14_23-32-21.png" class="thumbnail"></p>

        <div>
            <p><img src="/images/controller/admin-developer/get-course/2018-11-14_23-32-38.png" width="100%" class="thumbnail"></p>
        </div>

        <h2>Сообщения Рассылки</h2>
        <p><img src="/images/controller/admin-developer/get-course/2018-11-14_23-34-06.png" class="thumbnail"></p>

        <div>
            <p><img src="/images/controller/admin-developer/get-course/2018-11-14_23-34-15.png" width="100%" class="thumbnail"></p>
            <p><img src="/images/controller/admin-developer/get-course/2018-11-14_23-34-32.png" width="100%" class="thumbnail"></p>
            <p><img src="/images/controller/admin-developer/get-course/2018-11-14_23-34-54.png" width="100%" class="thumbnail"></p>
            <p><img src="/images/controller/admin-developer/get-course/2018-11-14_23-36-01.png" width="100%" class="thumbnail"></p>
            <p><img src="/images/controller/admin-developer/get-course/2018-11-14_23-36-08.png" width="100%" class="thumbnail"></p>
            <p><img src="/images/controller/admin-developer/get-course/2018-11-14_23-36-13.png" width="100%" class="thumbnail"></p>
            <p><img src="/images/controller/admin-developer/get-course/2018-11-14_23-36-19.png" width="100%" class="thumbnail"></p>
            <p><img src="/images/controller/admin-developer/get-course/2018-11-14_23-36-25.png" width="100%" class="thumbnail"></p>
        </div>

        <h2>Сообщения Шаблоны</h2>
        <p><img src="/images/controller/admin-developer/get-course/2018-11-14_23-37-55.png" class="thumbnail"></p>

        <div>
            <p><img src="/images/controller/admin-developer/get-course/2018-11-14_23-38-09.png" width="100%" class="thumbnail"></p>
        </div>

        <h2>Продажи Заказы</h2>
        <p><img src="/images/controller/admin-developer/get-course/2018-11-14_23-40-49.png" class="thumbnail"></p>

        <div>
            <p><img src="/images/controller/admin-developer/get-course/2018-11-14_23-41-05.png" width="100%" class="thumbnail"></p>
            <p><img src="/images/controller/admin-developer/get-course/2018-11-14_23-41-44.png" width="100%" class="thumbnail"></p>
            <p><img src="/images/controller/admin-developer/get-course/2018-11-14_23-42-44.png" width="100%" class="thumbnail"></p>
            <p><img src="/images/controller/admin-developer/get-course/2018-11-14_23-42-50.png" width="100%" class="thumbnail"></p>
            <p><img src="/images/controller/admin-developer/get-course/2018-11-14_23-43-24.png" width="100%" class="thumbnail"></p>
        </div>


        <h2>Продажи</h2>
        <p><img src="/images/controller/admin-developer/get-course/2018-11-14_23-58-14.png" class="thumbnail"></p>

        <div>
            <p><img src="/images/controller/admin-developer/get-course/2018-11-14_23-55-36.png" width="100%" class="thumbnail"></p>
            <p><img src="/images/controller/admin-developer/get-course/2018-11-14_23-55-55.png" width="100%" class="thumbnail"></p>
            <p><img src="/images/controller/admin-developer/get-course/2018-11-14_23-56-03.png" width="100%" class="thumbnail"></p>
            <p><img src="/images/controller/admin-developer/get-course/2018-11-14_23-56-08.png" width="100%" class="thumbnail"></p>
            <p><img src="/images/controller/admin-developer/get-course/2018-11-14_23-56-13.png" width="100%" class="thumbnail"></p>
            <p><img src="/images/controller/admin-developer/get-course/2018-11-14_23-56-18.png" width="100%" class="thumbnail"></p>
            <p><img src="/images/controller/admin-developer/get-course/2018-11-14_23-56-28.png" class="thumbnail"></p>
            <p><img src="/images/controller/admin-developer/get-course/2018-11-14_23-56-41.png" width="100%" class="thumbnail"></p>
            <p><img src="/images/controller/admin-developer/get-course/2018-11-14_23-56-48.png" width="100%" class="thumbnail"></p>
            <p><img src="/images/controller/admin-developer/get-course/2018-11-14_23-57-06.png" width="100%" class="thumbnail"></p>
            <p><img src="/images/controller/admin-developer/get-course/2018-11-14_23-57-13.png" width="100%" class="thumbnail"></p>
            <p><img src="/images/controller/admin-developer/get-course/2018-11-14_23-57-21.png" width="100%" class="thumbnail"></p>
            <p><img src="/images/controller/admin-developer/get-course/2018-11-14_23-57-35.png" width="100%" class="thumbnail"></p>
        </div>


    </div>
</div>

