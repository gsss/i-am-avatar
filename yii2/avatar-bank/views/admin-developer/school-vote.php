<?php

/** $this \yii\web\View  */

use yii\helpers\Url;
use app\services\GsssHtml;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use app\services\GetArticle\Collection;

$this->title = 'Голосование';

?>

<div class="container">
    <div class="col-lg-12">
        <h1 class="page-header"><?= Html::encode($this->title) ?></h1>

        <p>Во первых надо сделать список всех участков.</p>
        <p>Надо сделать список представителей от каждого участка или от группы участвков кто владеет несколькими участками.</p>
        <p>Надо сделать список голосований</p>
        <p>Само голосование</p>
        <p>С голосованием несколько вопросов.</p>
        <p>Будет ли ограниечние по времени? Для этого надо рассмотреть а какие есть варианты.</p>
        <p>Допустим 1 Голосование безконечное. Вопрос когда считать его завершенным? Когда все проголосуют.</p>
        <p>Допустим 2 Голосование с лимитом по времени. Если время голосования окончено а возникает ситуация 1 никто не проголосовал, 2 проголосовали 50/50. Что делать в каждом случае.</p>
        <p>Кто будет принимать участие в голосовании? 1 все жители, 2 только совет поселка</p>
        <p>По хорошему на сайте должно быть два варианта голосования.</p>

        <?= \common\services\documentation\DbTable::widget([
            'name'        => 'vote_list',
            'description' => 'Голосование',
            'model'       => '\common\models\VoteItem',
            'columns'     => [
                [
                    'name'        => 'id',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Идентификатор процесса',
                ],
                [
                    'name'        => 'user_id',
                    'type'        => 'integer',
                    'isRequired'  => true,
                    'description' => 'Идентификатор автора',
                ],
                [
                    'name'        => 'name',
                    'type'        => 'varchar(255)',
                    'isRequired'  => true,
                    'description' => 'Тема голосования',
                ],
                [
                    'name'        => 'content',
                    'type'        => 'text',
                    'isRequired'  => true,
                    'description' => 'Содержание',
                ],
                [
                    'name'        => 'created_at',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Момент создания',
                ],
                [
                    'name'        => 'price',
                    'type'        => 'varchar(30)',
                    'isRequired'  => true,
                    'description' => 'Стоимость которая начисляется тому кто проголосовал',
                ],
            ],
        ]) ?>
        <?= \common\services\documentation\DbTable::widget([
            'name'        => 'vote_answer',
            'description' => 'Ответ на голосование',
            'model'       => '\common\models\VoteAnswer',
            'columns'     => [
                [
                    'name'        => 'id',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Идентификатор процесса',
                ],
                [
                    'name'        => 'list_id',
                    'type'        => 'integer',
                    'isRequired'  => true,
                    'description' => 'Идентификатор списка',
                ],
                [
                    'name'        => 'name',
                    'type'        => 'varchar(255)',
                    'isRequired'  => true,
                    'description' => 'Тема голосования',
                ],
            ],
        ]) ?>

        <h2 class="page-header">Сценарий голосования</h2>
        <p>Для голосования используется страница <code>/cabinet-vote-action</code></p>

    </div>
</div>

