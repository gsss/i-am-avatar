<?php

/** $this \yii\web\View  */

use yii\helpers\Url;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;

$this->title = 'Яндекс.Кассы';


?>

<div class="container">
    <div class="col-lg-12">
        <h1 class="page-header"><?= Html::encode($this->title) ?></h1>

        <pre>https://kassa.yandex.ru/developers</pre>
        <pre>https://for-people.life/shop-order/success?type=yandexkassa</pre>
        <pre>{
  "id": "wh-e44e8088-bd73-43b1-959a-954f3a7d0c54",
  "event": "payment.succeeded",
  "url": "https://www.merchant-website.com/notification_url"
}</pre>
        <?= \common\services\documentation\DbTable::widget([
            'name'        => 'payments_yandex_kassa',
            'model'       => '\common\models\PaymentYandexKassa',
            'description' => 'данные платежа Яндекс кассы',
            'columns'     => [
                [
                    'name'        => 'id',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Идентификатор строки',
                ],
                [
                    'name'        => 'billing_id',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Идентификатор счета',
                ],
                [
                    'name'        => 'payment_id',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Идентификатор платежа Яндекс',
                ],
                [
                    'name'        => 'currency_id',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Идентификатор валюты db.currency.id',
                ],
                [
                    'name'        => 'amount',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Кол-во атомов на платеж',
                ],
                [
                    'name'        => 'created_at',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Момент создания платежа',
                ],
            ],
        ]) ?>
        <p>https://kassa.yandex.ru/my</p>

        <h2 class="page-header">Практика использования</h2>

        <pre>/** @var \common\services\YandexKassa $YandexKassa */
$YandexKassa = Yii::$app->YandexKassa;
$payment = $YandexKassa->createPatment([
    'amount'       => [
        'value'    => 100.0,
        'currency' => 'RUB',
    ],
    'confirmation' => [
        'type'       => 'redirect',
        'return_url' => 'http://zdravo.i-am-avatar.local/koop/return_url',
    ],
    'capture'      => true,
    'description'  => 'Заказ №1',
]);</pre>
        <p>Уведомление о платеже</p>
        <pre>[
    'type'   => 'notification'
    'event'  => 'payment.succeeded'
    'object' => [
        'id' => '26412a55-000f-5000-8000-1e6c46192d3a'
        'status' => 'succeeded'
        'paid' => true
        'amount' => [
            'value' => '100.00'
            'currency' => 'RUB'
        ]
        'authorization_details' => [
            'rrn' => '030408321972'
            'auth_code' => '321175'
        ]
        'captured_at' => '2020-05-03T19:13:49.745Z'
        'created_at' => '2020-05-03T19:08:05.922Z'
        'description' => 'Заказ №1'
        'metadata' => []
        'payment_method' => [
            'type' => 'bank_card'
            'id' => '26412a55-000f-5000-8000-1e6c46192d3a'
            'saved' => false
            'card' => [
                'first6' => '555555'
                'last4' => '4444'
                'expiry_month' => '05'
                'expiry_year' => '2022'
                'card_type' => 'MasterCard'
                'issuer_country' => 'US'
            ]
            'title' => 'Bank card *4444'
        ]
        'recipient' => [
            'account_id' => '701775'
            'gateway_id' => '1712759'
        ]
        'refundable' => true
        'refunded_amount' => [
            'value' => '0.00'
            'currency' => 'RUB'
        ]
        'test' => true
    ]
]</pre>

    </div>
</div>

