
<?php

/** $this \yii\web\View  */

use yii\helpers\Url;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;

$this->title = 'API';


?>

<div class="container">
    <div class="col-lg-12">
        <h1 class="page-header"><?= Html::encode($this->title) ?></h1>

        <h2 class="page-header">Боевой стенд</h2>
        <p>https://api.qualitylive.su</p>

        <h2 class="page-header">Тестовый стенд</h2>
        <p>https://api.test999.qualitylive.su</p>

        <h2 class="page-header">Авторизация</h2>
        <p>Для методов которые помечены как <span class="label label-primary">Auth required</span></p>

        <p>
            <a href="'https://app.diagrams.net/#G12a388KyidtR9pUHqF7zQCSXOxjVrRZh-" target="_blank">
                <img src="http://cloud1.cloud999.ru/upload/cloud/16437/43761_rulj0saHog.jpg">
            </a>
        </p>

        <p>В запросе передается заголовок</p>
        <pre>Authorization: Bearer {access_token}</pre>
        <p>где <code>access_token</code> - это токен полученный в методе <code>/auth/login</code>, возвращаемый параметр <code>access_token</code></p>
        <p>Ошибки авторизации</p>


        <p>Код <span class="label label-danger">401</span></p>
        <pre>{
    "name":"Unauthorized",
    "message":"Отсутствует обязательный параметр Authorization",
    "code":10,
    "status":401,
    "type":"yii\\web\\UnauthorizedHttpException"
}</pre>
        <pre>{
    "name":"Unauthorized",
    "message":"параметр Authorization должен состоять из двух слов",
    "code":11,
    "status":401,
    "type":"yii\\web\\UnauthorizedHttpException"
}</pre>
        <pre>{
    "name":"Unauthorized",
    "message":"параметр Authorization. Первое слово должно быть Bearer",
    "code":12,
    "status":401,
    "type":"yii\\web\\UnauthorizedHttpException"
}</pre>
        <pre>{
    "name":"Unauthorized",
    "message":"Не найден такой токен",
    "code":13,
    "status":401,
    "type":"yii\\web\\UnauthorizedHttpException"
}</pre>
        <pre>{
    "name":"Unauthorized",
    "message":"Токен не действителен",
    "code":14,
    "status":401,
    "type":"yii\\web\\UnauthorizedHttpException"
}</pre>

        <h2 class="page-header">Базовые правила для всех методов</h2>
        <p>URL начинается с префикса <code>/v1</code></p>
        <p>Пример <code>https://api.qualitylive.su/v1/auth/login</code></p>
        <p>Для POST запросов параметры нужно передавать методом <code>x-www-form-urlencoded</code></p>

        <h2 class="page-header">Функции</h2>
        <h3 class="page-header">/auth/login</h3>
        <p><span class="label label-info">POST</span></p>
        <p>Выдает токен для использования других функций.</p>
        <p>Время жизни токена 60*60*24*150 сек.</p>

        <p>Параметры запроса:</p>
        <?= \avatar\services\Params::widget([
           'params' => [
               [
                   'name'        => 'login',
                   'description' => 'email',
                   'isRequired'  => true,
                   'type'        => 'string',
               ],
               [
                   'name'        => 'password',
                   'description' => 'пароль',
                   'isRequired'  => true,
                   'type'        => 'string',
               ],
               [
                   'name'        => 'company_id',
                   'description' => 'Идентификатор компании',
                   'isRequired'  => true,
                   'type'        => 'integer',
               ],
           ]
        ]) ?>

        <p>Возврат:</p>
        <p>Код <span class="label label-success">200</span> </p>

        <pre>[
    'access_token'  => $access_token,
    'token_type'    => 'Bearer',
    'expire'        => $expire,
    'refresh_token' => $refresh_token,
]</pre>
        <?= \avatar\services\Params::widget([
            'params' => [
                [
                    'name'        => 'access_token',
                    'description' => 'токен',
                    'isRequired'  => true,
                    'type'        => 'string',
                ],
                [
                    'name'        => 'expire',
                    'description' => 'время окончания жизни токена, UNIXTIME',
                    'isRequired'  => true,
                    'type'        => 'integer',
                ],
                [
                    'name'        => 'refresh_token',
                    'description' => 'токен для обновления токена авторизации',
                    'isRequired'  => true,
                    'type'        => 'string',
                ],
            ]
        ]) ?>
        <p>Код <span class="label label-danger">400</span></p>
        <p>Не загружены данные, пустой запрос</p>
        <pre>{
    "name": "Bad Request",
    "message": "Не загружены данные",
    "code": 4,
    "status": 400,
    "type": "yii\\web\\BadRequestHttpException"
}</pre>

        <p>Код <span class="label label-danger">422</span></p>
        <p>Не верный запрос</p>

        <h3 class="page-header">/auth/login-first</h3>
        <p><span class="label label-info">POST</span></p>
        <p>Проверяет логин и пароль и выжает список компаний и флаг: есть ли 2FA.</p>

        <p>Параметры запроса:</p>
        <?= \avatar\services\Params::widget([
           'params' => [
               [
                   'name'        => 'login',
                   'description' => 'email',
                   'isRequired'  => true,
                   'type'        => 'string',
               ],
               [
                   'name'        => 'password',
                   'description' => 'пароль',
                   'isRequired'  => true,
                   'type'        => 'string',
               ],
           ]
        ]) ?>

        <p>Возврат:</p>
        <p>Код <span class="label label-success">200</span> </p>

        <pre>[
    'is_2fa'        => '0/1',
    'company_list'  => $array,
]</pre>
        <?= \avatar\services\Params::widget([
            'params' => [
                [
                    'name'        => 'is_2fa',
                    'description' => 'Флаг: Есть ли 2FA?',
                    'isRequired'  => true,
                    'type'        => 'bool',
                ],
                [
                    'name'        => 'company_list',
                    'description' => 'Массив объектов типа Company',
                    'isRequired'  => true,
                    'type'        => 'array',
                ],

            ]
        ]) ?>

        <p>Объект типа <code>Company</code></p>
        <?= \avatar\services\Params::widget([
            'params' => [
                [
                    'name'        => 'id',
                    'description' => 'Идентификатор компании',
                    'isRequired'  => true,
                    'type'        => 'integer',
                ],
                [
                    'name'        => 'image',
                    'description' => 'Картинка, квадратная, 300*300',
                    'isRequired'  => true,
                    'type'        => 'string',
                ],
                [
                    'name'        => 'name',
                    'description' => 'Наименование',
                    'isRequired'  => true,
                    'type'        => 'string',
                ],
            ]
        ]) ?>


        <p>Код <span class="label label-danger">400</span></p>
        <p>Не загружены данные, пустой запрос</p>
        <pre>{
    "name": "Bad Request",
    "message": "Не загружены данные",
    "code": 4,
    "status": 400,
    "type": "yii\\web\\BadRequestHttpException"
}</pre>

        <p>Код <span class="label label-danger">422</span></p>
        <p>Не верный запрос</p>
        <pre>{
    "name":"Unprocessable entity",
    "message":"{\"fields\":[\"login\",\"password\"],\"errors\":{\"password\":[\"Пароль не верный\"]}}",
    "code":0,
    "status":422,
    "type":"yii\\web\\HttpException"
}</pre>

        <h3 class="page-header">/auth/validate2fa</h3>
        <p><span class="label label-info">POST</span></p>
        <p>Проверяет 2FA</p>

        <p>Параметры запроса:</p>
        <?= \avatar\services\Params::widget([
           'params' => [
               [
                   'name'        => 'login',
                   'description' => 'email',
                   'isRequired'  => true,
                   'type'        => 'string',
               ],
               [
                   'name'        => 'password',
                   'description' => 'пароль',
                   'isRequired'  => true,
                   'type'        => 'string',
               ],
               [
                   'name'        => 'code',
                   'description' => '2FA код',
                   'isRequired'  => true,
                   'type'        => 'string',
               ],
           ]
        ]) ?>

        <p>Возврат:</p>
        <p>Код <span class="label label-success">200</span> </p>

        <pre>[
    'key'           => 'string',
    'company_list'  => $array,
]</pre>
        <?= \avatar\services\Params::widget([
            'params' => [
                [
                    'name'        => 'key',
                    'description' => 'Временный ключ для доступа к функции /auth/login-after2fa',
                    'isRequired'  => true,
                    'type'        => 'string',
                ],
                [
                    'name'        => 'company_list',
                    'description' => 'Массив объектов типа Company',
                    'isRequired'  => true,
                    'type'        => 'array',
                ],

            ]
        ]) ?>

        <p>Ключ <code>key</code> сохраняется в течении часа, если он будет не найден нужно проводить логин заново.</p>

        <p>Объект типа <code>Company</code></p>
        <?= \avatar\services\Params::widget([
            'params' => [
                [
                    'name'        => 'id',
                    'description' => 'Идентификатор компании',
                    'isRequired'  => true,
                    'type'        => 'integer',
                ],
                [
                    'name'        => 'image',
                    'description' => 'Картинка, квадратная, 300*300',
                    'isRequired'  => true,
                    'type'        => 'string',
                ],
                [
                    'name'        => 'name',
                    'description' => 'Наименование',
                    'isRequired'  => true,
                    'type'        => 'string',
                ],
            ]
        ]) ?>


        <p>Код <span class="label label-danger">400</span></p>
        <p>Не загружены данные, пустой запрос</p>
        <pre>{
    "name": "Bad Request",
    "message": "Не загружены данные",
    "code": 4,
    "status": 400,
    "type": "yii\\web\\BadRequestHttpException"
}</pre>

        <p>Код <span class="label label-danger">422</span></p>
        <p>Не верный запрос</p>
        <pre>{
    "name":"Unprocessable entity",
    "message":"{\"fields\":[\"login\",\"password\"],\"errors\":{\"password\":[\"Пароль не верный\"]}}",
    "code":0,
    "status":422,
    "type":"yii\\web\\HttpException"
}</pre>

        <h3 class="page-header">/auth/login-after2fa</h3>
        <p><span class="label label-info">POST</span></p>
        <p>Логинит после 2FA</p>

        <p>Параметры запроса:</p>
        <?= \avatar\services\Params::widget([
           'params' => [
               [
                   'name'        => 'key',
                   'description' => 'Ключ полученный в методе /auth/validate2fa',
                   'isRequired'  => true,
                   'type'        => 'string',
               ],
               [
                   'name'        => 'company_id',
                   'description' => 'идентификатор компании',
                   'isRequired'  => true,
                   'type'        => 'integer',
               ],
           ]
        ]) ?>

        <p>Возврат:</p>
        <p>Код <span class="label label-success">200</span></p>

        <pre>[
    'access_token'  => $access_token,
    'token_type'    => 'Bearer',
    'expire'        => $expire,
    'refresh_token' => $refresh_token,
]</pre>
        <?= \avatar\services\Params::widget([
            'params' => [
                [
                    'name'        => 'access_token',
                    'description' => 'токен',
                    'isRequired'  => true,
                    'type'        => 'string',
                ],
                [
                    'name'        => 'expire',
                    'description' => 'время окончания жизни токена, UNIXTIME',
                    'isRequired'  => true,
                    'type'        => 'integer',
                ],
                [
                    'name'        => 'refresh_token',
                    'description' => 'токен для обновления токена авторизации',
                    'isRequired'  => true,
                    'type'        => 'string',
                ],
            ]
        ]) ?>


        <p>Код <span class="label label-danger">400</span></p>
        <p>Не загружены данные, пустой запрос</p>
        <pre>{
    "name": "Bad Request",
    "message": "Не загружены данные",
    "code": 4,
    "status": 400,
    "type": "yii\\web\\BadRequestHttpException"
}</pre>

        <p>Код <span class="label label-danger">422</span></p>
        <p>Не верный запрос</p>
        <pre>{
    "name":"Unprocessable entity",
    "message":"{\"fields\":[\"login\",\"password\"],\"errors\":{\"password\":[\"Пароль не верный\"]}}",
    "code":0,
    "status":422,
    "type":"yii\\web\\HttpException"
}</pre>




        <h2 class="page-header">Кошелек</h2>
        <p>Все учетные единицы хранятся и измеряются в атомах, копейках. Привести к человеческому виду можно из параметра <code>currency.decimals</code> - Кол-во десятичных разрядов</p>

        <h3 class="page-header">/wallet/list</h3>
        <p><span class="label label-info">GET</span> <span class="label label-primary">Auth required</span></p>
        <p>Выдает список всех кошельков пользователя</p>

        <p>Параметры запроса: нет</p>
        <p>Возврат:</p>
        <p>Код <span class="label label-success">200</span> </p>
        <p>Возвращает массив объектов типа <code>UserBill</code></p>


        <pre>[{
    "id": 200,
    "is_default": 1,
    "card_id": 24,
    "amount": "1600",
    "currency": {
        "id": 1,
        "image": "https://cloud1.cloud999.ru/upload/cloud/15630/54243_zEahubxAg6_crop.png",
        "decimals": 2,
        "code": "QLW"
    }
}]</pre>

        <p>Объект типа <code>UserBill</code></p>
        <?= \avatar\services\Params::widget([
            'params' => [
                [
                    'name'        => 'id',
                    'description' => 'Идентификатор кошелька',
                    'isRequired'  => true,
                    'type'        => 'integer',
                ],
                [
                    'name'        => 'is_default',
                    'description' => 'Флаг. Это кошелек по умолчанию? 0 - нет, это дополнительный кошелек, 1 - да, это кошелек по умолчанию, главный',
                    'isRequired'  => true,
                    'type'        => 'integer',
                ],
                [
                    'name'        => 'card_id',
                    'description' => 'Идентификатор карты к которой привязан счет, если кошелек не привязан, то будет null',
                    'isRequired'  => false,
                    'type'        => 'integer',
                ],
                [
                    'name'        => 'name',
                    'description' => 'Название счета',
                    'isRequired'  => true,
                    'type'        => 'string',
                ],
                [
                    'name'        => 'amount',
                    'description' => 'Кол-во учетных единиц в кошельке, в атомах',
                    'isRequired'  => true,
                    'type'        => 'integer',
                ],
                [
                    'name'        => 'currency',
                    'description' => 'Валюта кошелька',
                    'isRequired'  => true,
                    'type'        => 'Сurrency',
                ],
            ]
        ]) ?>
        <p>Объект типа <code>Сurrency</code></p>
        <?= \avatar\services\Params::widget([
            'params' => [
                [
                    'name'        => 'id',
                    'description' => 'Идентификатор валюты',
                    'isRequired'  => true,
                    'type'        => 'integer',
                ],
                [
                    'name'        => 'image',
                    'description' => 'Картинка, квадратная, 300*300',
                    'isRequired'  => true,
                    'type'        => 'string',
                ],
                [
                    'name'        => 'decimals',
                    'description' => 'Кол-во десятичных разрядов',
                    'isRequired'  => true,
                    'type'        => 'integer',
                ],
                [
                    'name'        => 'code',
                    'description' => 'Кодовое название валюты',
                    'isRequired'  => true,
                    'type'        => 'string',
                ],
            ]
        ]) ?>

        <h3 class="page-header">/wallet/item</h3>
        <p><span class="label label-info">GET</span> <span class="label label-primary">Auth required</span></p>
        <p>Выдает список всех кошельков пользователя</p>

        <p>Параметры запроса:</p>
        <?= \avatar\services\Params::widget([
            'params' => [
                [
                    'name'        => 'bill_id',
                    'description' => 'Идентификатор счета',
                    'isRequired'  => true,
                    'type'        => 'integer',
                ],
            ]
        ]) ?>

        <p>Возврат:</p>
        <p>Код <span class="label label-success">200</span> </p>
        <p>Возвращает</p>

        <pre>{
    "bill"         => [
        "id"       => 200,
        "name"     => "Мой счет",
        "card_id"  => 24,
        "currency" => [
            "id"       => 1,
            "image"    => "https://cloud1.cloud999.ru/upload/cloud/15630/54243_zEahubxAg6_crop.png",
            "code"     => "QLW",
        ],
    ],
    "wallet"       => [
        "id"          => 1,
        "amount"          => 2000,
        "amount_decimals" => "20.00",
        "currency" => [
            "id"       => 16,
            "decimals" => 2,
        ],
    ],
}</pre>

        <?= \avatar\services\Params::widget([
            'params' => [
                [
                    'name'        => 'bill',
                    'description' => 'Объект типа UserBill',
                    'isRequired'  => true,
                    'type'        => 'Bill',
                ],
                [
                    'name'        => 'wallet',
                    'description' => 'Объект типа Wallet',
                    'isRequired'  => true,
                    'type'        => 'Wallet',
                ],
            ]
        ]) ?>

        <p>Объект типа <code>UserBill</code></p>
        <?= \avatar\services\Params::widget([
            'params' => [
                [
                    'name'        => 'id',
                    'description' => 'Идентификатор кошелька',
                    'isRequired'  => true,
                    'type'        => 'integer',
                ],
                [
                    'name'        => 'is_default',
                    'description' => 'Флаг. Это кошелек по умолчанию? 0 - нет, это дополнительный кошелек, 1 - да, это кошелек по умолчанию, главный',
                    'isRequired'  => true,
                    'type'        => 'integer',
                ],
                [
                    'name'        => 'card_id',
                    'description' => 'Идентификатор карты к которой привязан счет, если кошелек не привязан, то будет null',
                    'isRequired'  => false,
                    'type'        => 'integer',
                ],
                [
                    'name'        => 'name',
                    'description' => 'Название счета',
                    'isRequired'  => true,
                    'type'        => 'string',
                ],

                [
                    'name'        => 'currency',
                    'description' => 'Валюта кошелька',
                    'isRequired'  => true,
                    'type'        => 'Сurrency',
                ],
            ]
        ]) ?>
        <p>Объект типа <code>Сurrency</code></p>
        <?= \avatar\services\Params::widget([
            'params' => [
                [
                    'name'        => 'id',
                    'description' => 'Идентификатор валюты',
                    'isRequired'  => true,
                    'type'        => 'integer',
                ],
                [
                    'name'        => 'image',
                    'description' => 'Картинка, квадратная, 300*300',
                    'isRequired'  => true,
                    'type'        => 'string',
                ],
                [
                    'name'        => 'decimals',
                    'description' => 'Кол-во десятичных разрядов',
                    'isRequired'  => true,
                    'type'        => 'integer',
                ],
                [
                    'name'        => 'code',
                    'description' => 'Кодовое название валюты',
                    'isRequired'  => true,
                    'type'        => 'string',
                ],
            ]
        ]) ?>

        <h3 class="page-header">/wallet/transactions</h3>
        <p><span class="label label-info">GET</span> <span class="label label-primary">Auth required</span></p>
        <p>Выдает список транзакций для кошелька, выдает по 20 транзакций</p>

        <p>Параметры запроса:</p>
        <?= \avatar\services\Params::widget([
            'params' => [
                [
                    'name'        => 'bill_id',
                    'description' => 'Идентификатор счета',
                    'isRequired'  => true,
                    'type'        => 'integer',
                ],
                [
                    'name'        => 'page',
                    'description' => 'Страница, первая страница - 1',
                    'isRequired'  => true,
                    'type'        => 'integer',
                ],
            ]
        ]) ?>

        <p>Возврат:</p>
        <p>Код <span class="label label-success">200</span> </p>
        <p>Возвращает</p>

        <pre>{
    "list"         => [
        {
            "id": "751",
            "transaction_id": "348",
            "type": "1",
            "datetime": "1564586145537",
            "amount": "150000",
            "comment": "Посылаю Тебе 1,500.00 RUD! За задачу #582",
            "hash": "2297ac45a4f5c21f7bf16af3e1c17f8f59e8e5688d605088004403eae3155546"
        }
    ],
    "pages"       => [
        "current": 1,
        "count": 10,
    ],
}</pre>

        <?= \avatar\services\Params::widget([
            'params' => [
                [
                    'name'        => 'list',
                    'description' => 'Массив объектов типа Operation',
                    'isRequired'  => true,
                    'type'        => 'Bill',
                ],
                [
                    'name'        => 'pages',
                    'description' => 'Объект типа Pages',
                    'isRequired'  => true,
                    'type'        => 'Pages',
                ],
            ]
        ]) ?>

        <p>Объект типа <code>Operation</code></p>
        <?= \avatar\services\Params::widget([
            'params' => [
                [
                    'name'        => 'id',
                    'description' => 'Идентификатор операции',
                    'isRequired'  => true,
                    'type'        => 'integer',
                ],
                [
                    'name'        => 'transaction_id',
                    'description' => 'Идентификатор транзакции',
                    'isRequired'  => true,
                    'type'        => 'integer',
                ],
                [
                    'name'        => 'type',
                    'description' => 'Тип операции, 1 - прибавление, 2 - эмиссия, -1 - убавление, -2 - сжигание',
                    'isRequired'  => true,
                    'type'        => 'integer',
                ],
                [
                    'name'        => 'amount',
                    'description' => 'атомов учетной единицы участвующей в транзакции',
                    'isRequired'  => true,
                    'type'        => 'integer',
                ],
                [
                    'name'        => 'amount_decimals',
                    'description' => 'Кол-во единиц учетной единицы участвующей в транзакции, разделитель запятой - точка, десятичные знаки указаны до количества currency.decimals',
                    'isRequired'  => true,
                    'type'        => 'string',
                ],
                [
                    'name'        => 'datetime',
                    'description' => 'UNIXTIME * 1000',
                    'isRequired'  => true,
                    'type'        => 'integer',
                ],
                [
                    'name'        => 'comment',
                    'description' => 'Коментарий',
                    'isRequired'  => true,
                    'type'        => 'string',
                ],
                [
                    'name'        => 'hash',
                    'description' => 'Хеш',
                    'isRequired'  => true,
                    'type'        => 'string',
                ],
            ]
        ]) ?>
        <p>Объект типа <code>Pages</code></p>
        <?= \avatar\services\Params::widget([
            'params' => [
                [
                    'name'        => 'current',
                    'description' => 'Текущая страница',
                    'isRequired'  => true,
                    'type'        => 'integer',
                ],
                [
                    'name'        => 'count',
                    'description' => 'Всего страниц',
                    'isRequired'  => true,
                    'type'        => 'integer',
                ],
            ]
        ]) ?>


        <h2 class="page-header">Работа с картами</h2>
        <p>Продавец показывает QR код для оплаты</p>
        <p>
            <a href="https://app.diagrams.net/#G1vjUKcy_S_hOGmpg4Z-QeU-FWbh5i-M3-">
                <img src="http://cloud1.cloud999.ru/upload/cloud/16463/35874_teRgDE9Kkv.jpg">
            </a>
        </p>

        <p>Клиент показывает QR код для оплаты (на карте)</p>
        <p>
            <a href="https://app.diagrams.net/#G1vjUKcy_S_hOGmpg4Z-QeU-FWbh5i-M3-">
                <img src="http://cloud1.cloud999.ru/upload/cloud/16463/35894_5GdV0iYD2X.jpg">
            </a>
        </p>

        <p>Клиент подносит карту к терминалу для оплаты</p>
        <p>
            <a href="https://app.diagrams.net/#G1vjUKcy_S_hOGmpg4Z-QeU-FWbh5i-M3-">
                <img src="http://cloud1.cloud999.ru/upload/cloud/16463/35908_Y0wSgy4tTP.jpg">
            </a>
        </p>

        <h3 class="page-header">/shop/get-billing</h3>
        <h3 class="page-header">/shop/pay</h3>
        <h3 class="page-header">/shop/get-billing-from-shop</h3>
        <h3 class="page-header">/shop/verify-pin-code</h3>

        <h2 class="page-header">Работа с задачами</h2>

        <h3 class="page-header">/task/list</h3>
        <p>Получить весь список задач</p>

        <h3 class="page-header">/task/item</h3>
        <p>Просмотр задачи</p>

    </div>
</div>

