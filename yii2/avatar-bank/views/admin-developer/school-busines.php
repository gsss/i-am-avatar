<?php

/** $this \yii\web\View  */

use yii\helpers\Url;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;

$this->title = 'Школа. Бизнес план';


?>

<div class="container">
    <div class="col-lg-12">
        <h1 class="page-header"><?= Html::encode($this->title) ?></h1>

        <p>Я систематизировал 10 лет исследований и создал карту «Дизайн организационных систем и бизнес-процессов» <a href="https://vc.ru/services/64007-ya-sistematiziroval-10-let-issledovaniy-i-sozdal-kartu-dizayn-organizacionnyh-sistem-i-biznes-processov">https://vc.ru/services/64007-ya-sistematiziroval-10-let-issledovaniy-i-sozdal-kartu-dizayn-organizacionnyh-sistem-i-biznes-processov</a></p>



        <p><a href="https://richpro.ru/biznes/kak-sostavit-biznes-plan-obrazec-s-raschetami-gotovye-primery.html" target="_blank">https://richpro.ru/biznes/kak-sostavit-biznes-plan-obrazec-s-raschetami-gotovye-primery.html</a></p>
        <div class="panel panel-success">
            <div class="panel-heading"><h3 class="panel-title">Определение</h3></div>
            <div class="panel-body">
                <p><b>Бизнес-план</b> – это понятное для создателя документа и инвесторов руководство, которое с помощью механизмов бизнес-системы приводит основную описанную идею к реализации в материальном мире.</p>
                <p>Подобный документ создается на основе <span style="text-decoration: underline;"><strong>трех</strong> знаний о своей идее</span>, которые и лягут в основу всех ваших последующих действий. Только четкое понимание этих вещей может дать ту стартовую площадку, которая в итоге приведет вас к цели.</p>
                <p>Вот эти <span style="color: #ff0000;"><strong>3</strong></span> знания&nbsp;— <span style="text-decoration: underline;">ключевые&nbsp;факторы успеха любого&nbsp;проекта:</span></p>
                <ol>
                    <li><strong>Место, уровень в котором вы находитесь сейчас.</strong> То есть, если вы наемный работник желающий открыть свой магазин, осознайте, каких навыков у вас нет, какая у вас сумма для вложения, какое имеется оборудование, помещения, связи и так далее.</li>
                    <li><strong>Конечный результат.</strong> Это не должна быть мечта из серии «хочу быть богатым». Нужно четко осознавать какой оборот должен быть у вашего бизнеса, какая прибыль, какое место на рынке и все в том же духе;</li>
                    <li><strong>Необходимо четко расписать и понимать какие шаги приведут вас от первого пункта ко второму.</strong> Конечно, все просчитать нельзя, однако стоит максимально точно и подробно, согласно вашим реалиям понимать, как же нужно действовать.</li>
                </ol>
                <p>Разобравшись с этими тремя базами вы можете приступать к следующему этапу подготовки для реализации своей бизнес-идеи.</p>
            </div>
        </div>
        <p><a href="https://rb.ru/story/thiel-pitch-deck/" target="_blank">Как должна выглядеть презентация для инвесторов: шаблон от Питера Тиля</a></p>

        <h2 class="page-header">Формирование Бренда</h2>
        <p><a href="http://go.free-publicity.ru/mybrand" target="_blank">http://go.free-publicity.ru/mybrand</a></p>

        <h3 class="page-header">Исследование рынка</h3>
        <p><a href="http://edmarket.digital/" target="_blank">http://edmarket.digital/</a></p>
        <p><a href="https://drive.google.com/open?id=1oON6zv-pHZAWog87sds6mrnphnSH6qAU" target="_blank">Короткое исследование</a></p>
        <p><a href="https://drive.google.com/open?id=1brNnL81NxxmWNgwEE3nsqnyNQArtzr4Q" target="_blank">Подробное исследование</a></p>
        <p><a href="https://drive.google.com/open?id=12HXxEWVgqW2Zov8ffkElaG4HYYJOFmFR" target="_blank">Анализ рынка образовательных услуг (Ванькина Инна Вячеславовна)</a></p>
        <p><a href="https://drive.google.com/open?id=1kP32Y1d81HytCrR59UDPaDZsY2rH6rBK" target="_blank">Исследование российского рынка онлайн-образования и образовательных технологий (Максим Древаль)</a></p>

        <h4 class="page-header">От Максима Демковича</h4>
        <p><a href="https://drive.google.com/open?id=1CoNTyrizW7w0p7v3UyvaY8efDaGjIfV4" target="_blank">Обзор рынка онлайн образования в мире и в России</a></p>
        <p><a href="https://drive.google.com/open?id=1PBZCQ__U6B9ahgcPAhmel1KWcIOwB7bx" target="_blank">Обзор рынка онлайн образования в России</a></p>
        <p><a href="https://drive.google.com/open?id=1sWyyUEpwHgEKyAYWklDSU9zYd77GE0kn" target="_blank">Общие тенденции стартапов на глобальном рынке. Концепция предложения для рынка РФ</a></p>

        <h3 class="page-header">Инструкции по созданию школы</h3>
        <p><a href="/images/controller/admin-developer/school-busines/pdf-online-school.pdf" target="_blank">pdf-online-school.pdf</a></p>
        <p><a href="/images/controller/admin-developer/school-busines/plan.pdf" target="_blank">plan.pdf</a></p>
        <p><a href="/images/controller/admin-developer/school-busines/a9879b5a9bd6701ad93576769bb494b8.pdf" target="_blank">a9879b5a9bd6701ad93576769bb494b8.pdf</a></p>

        <h3 class="page-header">Описание платформы</h3>

        <p>Название англ: iAvatar</p>
        <p>Название рус: ЯАватар</p>
        <p>Подключен безоплатно дизайн человека. На платформе специалисты могут давать консультации. И проводить свои
            курсы.</p>
        <p>Карточка мастера, Фотоальбом</p>
        <p>Карточка события</p>
        <p>Автоворонки</p>
        <p>Система билетов</p>
        <p>Система сертификатов</p>
        <p>Система аттестации</p>
        <p>Система домашних заданий</p>
        <p>Блог для мастера</p>
        <p>Вебинарная комната с презентациями</p>
        <p>Интернет магазин</p>
        <p>Система ступеней</p>
        <p>Онлайн хранилище</p>
        <p>Система рассылок</p>
        <p>Сейфовая ячейка</p>
        <p>Система шифрования материалов</p>
        <p>Подключение всех сервисов</p>
        <p>Мини конструктор лендингов</p>
        <p>Обучение через виртуальную реальность на будущее</p>
        <p>Мультиязычность</p>


        <h3 class="page-header">Маркетинговое исследование</h3>
        <p>Группы людей кого мы привлекаем и какие у них запросы</p>
        <p><img src="/images/controller/admin-developer/school-busines/media.jpg" class="thumbnail" width="100%"></p>
        <p><a href="https://www.youtube.com/watch?v=zUpeIsM26ZQ" target="_blank">Generation П СКРЫТЫЙ СМЫСЛ фильм и книга Пелевин</a></p>

        <p><img src="/images/controller/admin-developer/school-busines/2019-01-24_19-19-43.png" width="400" class="thumbnail"></p>
        <p><img src="/images/controller/admin-developer/school-busines/2019-01-24_19-19-57.png" width="400" class="thumbnail"></p>
        <p><img src="/images/controller/admin-developer/school-busines/2019-01-24_19-20-18.png" width="400" class="thumbnail"></p>
        <p><img src="/images/controller/admin-developer/school-busines/2019-01-24_19-20-35.png" width="400" class="thumbnail"></p>

        <h3 class="page-header">Маркетинговый план</h3>
        <p>Сколько бюджет в мес на рекламу?</p>
        <p>Где будем рекламироваться?</p>

        <p style="font-weight: bold;">Какой портрет Целевой Аудитории?</p>
        <p>Школы, Психологи, Мастера, Хранители знаний, Специалисты в бизнес процессах.</p>

        <h3 class="page-header">План производства</h3>

        <h3 class="page-header">Блокчейн</h3>
        <p><a href="http://sumus.team/ru" target="_blank">SUMUS blockchain</a></p>
        <p>Блокчейн SUMUS используется для различных решений и задач: для создания публичного или приватного блокчейна, для функционирования криптовалют, для токенизации и блокчейнизации проектов. Блокчейн – это публичность и децентрализация. На нашей платформе и с помощью компетенций команды SUMUS можно разрабатывать решения любого уровня. Именно поэтому мы сделали универсальный блокчейн, позволяющий нашим единомышленникам и партнерам воплотить самые смелые и передовые идеи, создавать глобальные и эффективные проекты, которые, мы верим, принесут настоящую пользу мировому сообществу. Блокчейн SUMUS, на данный момент, уже используют несколько крупных проектов, в том числе хорошо известных мировому сообществу. Также в стадии завершения еще ряд проектов, реализованных на блокчейне SUMUS.</p>
        <p><a href="https://mile.global/" target="_blank">https://mile.global/</a></p>


        <h3 class="page-header">Road Map</h3>
        <?php
        $rows = [
            [
                'date'        => '2019 Март',
                'description' => [
                    'Форкнутый AvatarNetwork',
                    'Уроки',
                    'События',
                    'Курсы',
                    'Продажа курса',
                    'Блог',
                    'Карточка мастера',
                ],
            ],
            [
                'date'        => '2019 Сентябрь',
                'description' => [
                    'Автоворонки',
                    'Система билетов',
                    'Домашнее задание',
                    'Мультиязычность',
                    'Интернет магазин',
                    'Система аттестации',
                    'Мини конструктор лендингов',
                    'Система рассылок',
                ],
            ],
            [
                'date'        => '2020',
                'description' => [
                    'Обучение через виртуальную реальность',
                    'Система шифрования материалов',
                ],
            ],
        ];
        ?>
        <?= \yii\grid\GridView::widget([
            'dataProvider' => new \yii\data\ArrayDataProvider([
                'allModels' => $rows,
            ]),
            'tableOptions' => [
                'class' => 'table table-striped table-hover',
                'style' => 'width: auto',
            ],
            'summary'      => '',
            'columns'      => [
                'date',
                [
                    'attribute' => 'description',
                    'content'   => function ($i) {
                        if (is_array($i)) {
                            return join('<br>', $i['description']);
                        } else {
                            return $i['description'];
                        }
                    },
                ],
            ],
        ]) ?>
        <p class="label label-success">Сделать таблицу с прогресс баром и новости об обновлениях.</p>

        <h3 class="page-header">Организационный план</h3>
        <p>Кто нужен на платформу</p>
        <p>Проджект менеджер</p>
        <p>Програмист PHP YII2</p>
        <p>Администратор</p>
        <p>Дизайнер?</p>
        <p>Служба поддержки</p>
        <p>Верстальщик - редактор лендингов</p>
        <p>Реклама</p>
        <p>Контент менеджер</p>
        <p>Разработчик мобильного приложения - 500 т.р.</p>
        <p>Анимировнное видео</p>
        <p>Открыть компанию за границей - 2000 евро</p>
        <p class="label label-success">Сделать список первой стратегической команды, внутреннее ядро, без которого процесс двигаться не сможет</p>
        <p class="label label-success">Сделать список вторую сферу для масштабирования бизнеса.</p>

        <h3 class="page-header">Монеты</h3>
        <p><a href="https://www.youtube.com/watch?v=3TPjItg_20Q" target="_blank">Право на деньги - Дмитрий Мыльников - Познавательное ТВ</a></p>

        <h4 class="page-header">МЕРА - Звездный спутник</h4>
        <p><img src="http://www.zvezdadushi.ru/_fr/2/7916591.gif" class="thumbnail"></p>
        <p>Безразлично, что ты видишь или чувствуешь во время своих путешествий по ту сторону звёздных врат. Всегда кто-то присутствует рядом, всегда тебя окружает энергия. Передай этой сущности АН'АНАША и ЭЛЕКСИР, так как МЕРА - твой спутник, который заботится и охраняет в пути.</p>
        <p>Вибрации числа: 48</p>
        <h4 class="page-header">ЭЛЬ'АМАРА - Звездные врата</h4>
        <p><img src="http://www.zvezdadushi.ru/_fr/2/3767057.gif" class="thumbnail"></p>
        <p>Пройди через ЭЛЬ'АМАРА и ты попадёшь из мира иллюзии в Мир Действительности. Всё, что ты чувствуешь и воспринимаешь по ту сторону звёздных ворот, является Действительностью. Это реально. Ты испытываешь это на самом деле. Учись делать различие между иллюзией своей реальности, состоящей только из определенных конструкций мыслей, и Действительностью, простирающейся по ту сторону звёздных ворот. Запомни навсегда: Реальное не может когда-либо разрушиться, нереальное не может существовать вечно.</p>
        <p>Энергия: Эль Мория<br>
            Вибрации числа: 12 </p>
        <h4 class="page-header">ЭЛИКСИР - Безусловная Любовь</h4>
        <p><img src="https://i-am-avatar.com/images/controller/cabinet-wallet/warning.png" width="300"></p>
        <p><img src="https://i-am-avatar.com/images/controller/cabinet-wallet/info.png" width="300"></p>
        <p>A ni o'heved o'drach - ты безмерно любим. <br>
            Происхождение кристалла ЭЛИКСИР берет свое начало по ту сторону звёздных ворот. В своей реальности ты способен ощущать всего лишь малую толику того, что означает в Действительности - быть безмерно любимым. Всё сущее связано между собой через кристалл ЭЛИКСИР.</p>
        <p>Энергия: Метатрон, Мария и Иисус<br>
            Вибрации числа: 14</p>

        <h3 class="page-header">Финансовый план</h3>
        <p><a href="https://docs.google.com/document/d/1gwL7DkOEg2WQBQSDk15_svbIuMfcjyvGU4BgIqC8tLk/edit?usp=sharing" target="_blank">https://docs.google.com/document/d/1gwL7DkOEg2WQBQSDk15_svbIuMfcjyvGU4BgIqC8tLk/edit?usp=sharing</a></p>
        <p>
            <a href="https://miro.com/app/board/o9J_k0JrlYQ=/?moveToWidget=3074457346707558063" target="_blank">
                <img src="/images/controller/admin-developer/school-busines/2019-06-21_21-14-28.png" width="100%" title="Структурный анализ" data-toggle="tooltip">
            </a>
        </p>
        <p>Можно ли организовать эскроу? Мы берем деньги за обучение и потом платим мастеру. За вычетом процентов?</p>
        <p>Можно ли организовать процент с продаж?</p>
        <p>Как организовать представительство в другой стране и в России?</p>
        <p>В России какую правовую формы выбрать?</p>
        <p>Чем будет заниматься представительство в другой стране?</p>
        <p>Регистрация торговой марки и торгового бренда. Как?</p>
        <p>Хочу запустить бумстартер. Сколько ставить денег на сборы? Актуально ли?</p>

        <p style="font-weight: bold;">Оплата Золотым элексиром за курс школе</p>
        <p>Что здесь возникает? Так как оплата идет золотыми, а мастеру надо будет возместить эти деньги, то они при покупке конвертируется в небесный Эликсир. Чтобы школа могла его обменять на рубли.</p>


        <hr>
        <p style="font-weight: bold;">Смета расходов</p>
        <p><a href="https://docs.google.com/spreadsheets/d/1qCrE2L7y2o2ytQ2jL0ZuvOh5zQcrki0FJJKeIq9DhAs/edit#gid=0" target="_blank">https://docs.google.com/spreadsheets/d/1qCrE2L7y2o2ytQ2jL0ZuvOh5zQcrki0FJJKeIq9DhAs/edit#gid=0</a></p>
        <p style="font-weight: bold;">Оплата труда</p>
        <p>Для того чтобы можно было организовать людей в трудовой процесс, создана монета в которой производится расчет.</p>
        <p>Сначала формируется эмиссия долгового обязательства. Монета называется ELXT.</p>
        <p>Далее с этого счета распределяются монеты за оплату труда.</p>
        <p>После того как в компанию приходят деньги (прибыль или инвестиции), то часть из них идет на погашение долговых обязателсьв.</p>
        <p>Выплата долговых обязательств распределяется пропорционально долгу.</p>
        <p>Например: Есть три пользователя с монетами ELXT.<br>
            1 - 10,000<br>
            2 - 5,000<br>
            3 - 1,000</p>
        <p>В компанию пришло 5,000 руб. Они рспределяются соответственно по формуле Выплата = Сумма прихода * (долг пользователю / (сумма всех долгов))</p>
        <p>Для первого пользователя соответственно такой расчет 5000 * ( 10000 / ( 10000 + 5000 + 1000 ) ) = 3,125 руб.</p>
        <p>Далее цикл повторяется</p>
        <p style="font-weight: bold;">Схема дивидентов</p>
        <p>Рассматривается какая статья будет являться доходом.</p>
        <p style="font-weight: bold;">Выборная система</p>
        <p>Допустим. 11 человек выдвигают кандидатуру. Все голосуют друг за друга. Побеждает один. Он раздает всем служение. Его выбирают на 6 мес.</p>
        <p>Идем по пути десятников, сотников, тысячников и темников.</p>
        <p class="label label-success">Сделать сертификаты, подарочные купоны для обучения на нашей платформе</p>
        <p>
            Любой человек может приобрести подарочный купон для друга. Он позволяет держателю купона пройти любой курс на стоимость этого купона.
            Реадизация: Мы выпускаем эмиссию ELXT, она является обеспечительным платежом. Живые деньги которые пришли от продажи этого купона приходят к нам.
            Когда он проходит курс то мы эти деньги выплачиваем самому учителю.
            (здесь что то не то с типом выпускаемой монеты, надо еще раз продумать, пока замисам мыслеформу, чтобы этот бизнес процесс не выпал из поля зрения)
            Как будет можно воспользоваться этим купоном? Там может быть какой то код. Значит нужна таблица купонов.
        </p>



        <h3 class="page-header">Финансовый план</h3>
        <p style="font-weight: bold;">Cтатьи расходов</p>
        <p>Аренда сервера</p>
        <p>Аренда сервиса рассылки</p>
        <p>Ключевой кадровый состав:</p>
        <p>Админ, Верстальщик, Програмист, служба пожддержки, менеджеры.</p>

        <p style="font-weight: bold;">Cтатьи приходов</p>
        <p>Абонентское обслуживание за платформу.</p>
        <p>Предоставление базы клиентов для работодателей.</p>


        <h3 class="page-header">Чем мы отличаемся от других</h3>
        <p>Мы являемся уникальным продуктом на рынке. С одной стороны кажется что это платформа для обучения.</p>
        <p>Мы являемся социальной сетью для Богов. Мы формируем все сервисы и инфраструктуру для этого. Мы формируем целую экосреду для игрового обучения для подготовки Человечества к переходу на новый Золотой уровень Сознания.</p>


        <h3 class="page-header">Инвестиционный план</h3>
        <p>Схема привлечения инвестиций строится по следующей схеме: 51% акций остается у основателя. 49% распределяется
            между инвесторами которые верят в проект и хотят получать дивиденты от его реализации.</p>
        <p>Необходимо определить стоимость всего пакета акций. Как это делается? Можно ли потом довыпускать и по какому
            алгоритму?</p>
        <p>Как определить стоимость всего пакета акций?</p>
        <p>1 от балды</p>
        <p>2 расчитать стоимость всех ресурсов на пике развития и от ожидаемой прибыли.</p>
        <p>* Зависит от того можно ли будет довыпускать акции</p>
        <p>Можно ли будет довыпускать акции?</p>
        <p>Пока я не могу ответить на этот вопрос,
            потому что надо проанализировать как будет меняться структура компании,
            интересы и поведение инвесторов,
            экономический план от такого довыпуска.</p>



        <h3 class="page-header">Подключаемые модули</h3>

        <p><a href="http://inpot.ru/?p=16952" target="_blank">Личная карта Таро по дате рождения</a></p>
        <p><a href="http://astroapi.ru/" target="_blank">Дизайн человека и Астрология</a></p>
        <p>Майянский календарь</p>
        <p>Коляды Дар</p>

        <h3 class="page-header">Изучение и проведения анализа относительно возможных рисков</h3>

        <h3 class="page-header">Анализ конкурентов</h3>
        <p><a href="https://4brain.ru/" target="_blank">https://4brain.ru/</a></p>
        <p><a href="https://getcourse.ru/" target="_blank">https://getcourse.ru/</a></p>
        <p><a href="https://geekbrains.ru" target="_blank">https://geekbrains.ru</a></p>
        <p><a href="https://www.udemy.com/" target="_blank">https://www.udemy.com/</a></p>
        <p><a href="https://www.ispring.ru/" target="_blank">https://www.ispring.ru/</a></p>
        <p><a href="http://www.activelearn.ru/" target="_blank">http://www.activelearn.ru/</a></p>
        <p><a href="https://www.mirapolis.ru" target="_blank">https://www.mirapolis.ru</a></p>
        <p><a href="http://koshka.online/" target="_blank">http://koshka.online/</a></p>
        <p><a href="https://justclick.ru/" target="_blank">https://justclick.ru/</a></p>
        <p><a href="https://www.edx.org/" target="_blank">https://www.edx.org/</a></p>
        <p><a href="https://www.coursera.org/" target="_blank">https://www.coursera.org/</a></p>
        <p><a href="http://coursmos.com/" target="_blank">http://coursmos.com/</a></p>
        <p><a href="https://gohighbrow.com/" target="_blank">https://gohighbrow.com/</a></p>
        <p><a href="https://www.skillshare.com/" target="_blank">https://www.skillshare.com/</a></p>
        <p><a href="https://curious.ru/" target="_blank">https://curious.ru/</a></p>
        <p><a href="https://www.lynda.com/" target="_blank">https://www.lynda.com/</a></p>
        <p><a href="https://www.creativelive.com/" target="_blank">https://www.creativelive.com/</a></p>
        <p><a href="https://teachable.com/" target="_blank">https://teachable.com/</a></p>






    </div>
</div>