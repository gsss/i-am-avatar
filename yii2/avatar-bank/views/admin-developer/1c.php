<?php

/** $this \yii\web\View  */

use yii\helpers\Url;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;

$this->title = '1С';


?>

<div class="container">
    <div class="col-lg-12">
        <h1 class="page-header"><?= Html::encode($this->title) ?></h1>

        <a href="https://its.1c.ru/db/fresh" target="_blank">https://its.1c.ru/db/fresh</a>

    </div>
</div>

