<?php

/** $this \yii\web\View  */

use yii\helpers\Url;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;

$this->title = 'Регистрация через телеграм';


?>

<div class="container">
    <div class="col-lg-12">
        <h1 class="page-header"><?= Html::encode($this->title) ?></h1>

        <!-- Nav tabs -->
        <ul class="nav nav-tabs" role="tablist">
            <li role="presentation"><a href="#clear" aria-controls="clear" role="tab" data-toggle="tab">Чистовик</a></li>
            <li role="presentation" class="active"><a href="#potok" aria-controls="potok" role="tab" data-toggle="tab">Поток</a></li>
        </ul>

        <!-- Tab panes -->
        <div class="tab-content">
            <div role="tabpanel" class="tab-pane fade" id="clear">
            </div>
            <div role="tabpanel" class="tab-pane in active" id="potok">

                <h4 class="page-header">Исследование</h4>
                <p>Какие запросы надо всегда обеспечить? И при этом обспечить не повторяемость пользователей (email).</p>
                <p>1. Кто в школе</p>
                <p>2. Кто записался на событие</p>

                <h4 class="page-header">User Expirience</h4>
                <p>Когда пользователь должен регистрироваться на платформе?</p>
                <p>Если подписаться на безоплатный вебинар, то надо? Ему только приходит рассылка по почте. Значит не надо, но надо подтвердить свой email.</p>
                <p>Если подписаться на закрытый по паролю вебинар, то надо? Надо потому что он дожен зайти на платформу. А что в иаком случае ему надо сделать? Подтвердить почту, далее ему предлагается заполнить пароль и он входит в кабинет.</p>
                <p>Если покупает вебинар, то надо? Да, схема действий таже.</p>
                <p><b>Подтверждение почты</b></p>
                <p>Стоит ли делать подтверждение по почте?</p>
                <p>Ситуация может быть такая что если пользователь не подтвердит почту то он не добавится. А может он в одной букве ошибся. А так можно было бы по телефону созвониться.</p>
                <p>Поэтому вывод такой: добавлять но высылать письмо на проверку почты, а при рассылке если не подтвержден то не высылать ему рассылку.</p>
                <p>Какие варианты тогда могут быть для потока?</p>
                <p>1. Если допустим вебинар платный то человек должен вообще зарегистрироваться на платформу и конечно подтвердить почту. При этом если он попал сначала как лид но не оплатил, то он попадает в список обычный, если же оплатил, то для таких пользователей создается отдельный статус и он в него переводится.</p>
                <p>2. Если допустим вебинар безоплатный то он попадает как лид но с флагом что не подтвердил почту.</p>
                <p>3. Если допустим событие платное, но на входе оплачивает...</p>
                <p>В итоге прихожу к выводу что всех записываю, только если подтверждает то флаг ему ставлю что подтвержден он.</p>
                <p><b>Решение</b></p>
                <p>Будет статус <code>school_kurs.status</code></p>
                <p>1 - только подтверждение почты</p>
                <p>2 - подтверждение и регистрация</p>
                <p><a href="https://www.draw.io/#G1T0AG18eP2ULyEivblRPJjyA2IQcygVmu" target="_blank"><img src="/images/controller/admin-developer/school-users/map.png"></a></p>
                <p><b>Регистрация</b></p>
                <p>При регистрации заносится <code>user_root</code> и <code>user_password</code> и <code>user_register</code>.
                    После активации создается user и удаляется <code>user_password</code> и <code>user_register</code>.</p>
                <p>А как отправит уведомление о том что он записался на вебинар если при подтверждении никаких данных нет?</p>
                <p>Почему же? Смотрим в таблицу <code>school_potok_user_root_link</code> и там записано куда он записался. и так высылаем ему письмо что все в силе.</p>


                <h3 class="page-header">Роли</h3>
                <p>Администратор школы <code>role_school_admin</code> - Может редактировать все параметры школы, его назначает и снимает творец школы.</p>
                <p>Учитель школы <code>role_school_teacher</code> - Может создавать и редактировать свои курсы, проверяет и ставит оценки за домашние задания.</p>
                <p>Менеджер школы <code>role_school_manager</code> - Может управлять заявками.</p>
                <p>Ученик школы <code>role_school_disciple</code> - Может учиться и выполнять домашние задания.</p>

                <p>Доступ к школе получают только администраторы.</p>
                <p>Доступ к добавлению только создателю школы.</p>



                <h3 class="page-header">Карточка школы, мастера и ученика</h3>
                <p><a href="https://roomble.com/pro/id12594/" target="_blank">https://roomble.com/pro/id12594/</a></p>

                <p>Карточка ученика и учителя открываются по одной ссылке <code>/user/&lt;user_root_id&gt;</code>. В каждой школе свой массив учеников. Если ученик не зарегистрирован в школе но в других он есть то будет показано что нет такого ученика и код 404.</p>

                <p><a href="https://bootsnipp.com/snippets/O5Z4d" target="_blank">https://bootsnipp.com/snippets/O5Z4d</a></p>

                <h3 class="page-header">Делаю добавление LID</h3>
                <p>Какие могут быть комбинации в струтуре таблиц Пользователи.</p>
                <p>1. Пользователь зарегистрирован в Аватаре. <code>user_root</code>, <code>user</code></p>
                <p>2. Пользователь зарегистрирован в школе. <code>user_root</code>, <code>user</code>, <code>user_school_link</code></p>
                <p>3. LID. <code>user_root</code>, <code>school_potok_user_root_link</code>, <code>school_potok_user_ext</code>, <code>user_school_link</code>.</p>
                <p>4. Пользователь из внешней БД. <code>user_root</code>, <code>user_root_ext</code>, </p>

                <p>Еще надо добавить в каком статусе находится и что делает. Например если пользователь уже зарегистрированный на платформе и хочет записаться на курс то он добавляется только в поток <code>school_potok_user_root_link</code></p>

                <p>
                    <a href="https://docs.google.com/spreadsheets/d/1U-2ud9ecXdaeaGcCFc2uowgvGBGBG5KxcICRiFEIeGo/edit?usp=sharing" target="_blank">
                        <img src="/images/controller/admin-developer/school-users/2019-02-15_03-58-24.png" class="thumbnail">
                    </a>
                </p>

                <h3 class="page-header">Регистрация</h3>
                <p>
                    <a href="https://www.draw.io/#G1giAR7WKkVFs31E9woLGxvVYAZ-FLK3uD" target="_blank">
                        <img src="/images/controller/admin-developer/school-users/registration.png" class="thumbnail">
                    </a>
                </p>

                <h3 class="page-header">Ситуация когда пользователь записался на вебинар</h3>
                <p>Если к нему приходит письмо congratulations то ссылку да можно добавить.
                    Если к нему приходит письмо registration_form_1 или registration_form_2 то добавлять ссылку?
                    надо письма смотреть </p>
                <p><a href="https://www.i-am-avatar.com/cabinet-school-task-list/view?id=143" >https://www.i-am-avatar.com/cabinet-school-task-list/view?id=143</a></p>
                <p>В user expirience (модели поведения) нужно пользователю что то сказать, а что? нужно вызвать после подтверждения автоворонку.</p>
                <p>Напримере курса как пользоваться платформой, там внутри есть уроки. а на вебинаре будет урок с вебинаром. Значит на эту ссылку и отправлять.
                    верно?
                    надо делать автоворонку конечно же, но в любом случае тогда встает вопрос а как вызвать автоворонку после подтверждения? А как понять откуда пользователь с подтверждения пришел?
                </p>

                <p>
                    <a href="https://www.draw.io/#G1T0AG18eP2ULyEivblRPJjyA2IQcygVmu" target="_blank">
                        <img src="/images/controller/admin-developer/school-users/user_vebinar.png" class="thumbnail">
                    </a>
                </p>
                <?= \common\services\documentation\DbTable::widget([
                    'name'          => 'user_register_vebinar',
                    'description'   => 'Таблица которая хранит данные пользователя который записался на вебинар и система ждет подтверждения почты',
                    'model'         => '\common\models\UserRegisterVebinar',
                    'columns'       => [
                        [
                            'name'        => 'id',
                            'type'        => 'int',
                            'isRequired'  => true,
                            'description' => 'Идентификатор строки',
                        ],
                        [
                            'name'        => 'potok_id',
                            'type'        => 'int',
                            'isRequired'  => true,
                            'description' => 'Идентификатор потока куда пользователь регистрируется',
                        ],
                        [
                            'name'        => 'user_root_id',
                            'type'        => 'int',
                            'isRequired'  => true,
                            'description' => 'Идентификатор пользователя user_root',
                        ],
                        [
                            'name'        => 'created_at',
                            'type'        => 'int',
                            'isRequired'  => true,
                            'description' => 'Момент регистрации на событие',
                        ],
                    ],
                ]) ?>



            </div>
        </div>


    </div>
</div>

