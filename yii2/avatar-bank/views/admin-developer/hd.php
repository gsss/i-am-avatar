<?php

/** $this \yii\web\View  */

use yii\helpers\Url;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;

$this->title = 'Дизайн Аватара';


?>

<div class="container">
    <div class="col-lg-12">
        <h1 class="page-header"><?= Html::encode($this->title) ?></h1>

        <p>http://astroapi.ru/doc.html</p>

        <?= \common\services\documentation\DbTable::widget([
            'name'        => 'gs_hd_gen_keys',
            'description' => 'Генный ключ',
            'model'       => '\common\models\HdGenKeys',
            'columns'     => [
                [
                    'name'        => 'id',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Идентификатор элемента',
                ],
                [
                    'name'        => 'num',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Номер ключа',
                ],
                [
                    'name'        => 'name',
                    'type'        => 'varchar(100)',
                    'isRequired'  => true,
                    'description' => 'Название ключа',
                ],
                [
                    'name'        => 'ten',
                    'type'        => 'varchar(100)',
                    'isRequired'  => true,
                    'description' => 'Название тени',
                ],
                [
                    'name'        => 'dar',
                    'type'        => 'varchar(100)',
                    'isRequired'  => true,
                    'description' => 'Название дара',
                ],
                [
                    'name'        => 'siddhi',
                    'type'        => 'varchar(100)',
                    'isRequired'  => true,
                    'description' => 'Название сиддхи',
                ],
                [
                    'name'        => 'image',
                    'type'        => 'varchar(255)',
                    'isRequired'  => true,
                    'description' => 'Картинка',
                ],
                [
                    'name'        => 'image_codon',
                    'type'        => 'varchar(255)',
                    'isRequired'  => true,
                    'description' => 'Картинка',
                ],
                [
                    'name'        => 'image_graph',
                    'type'        => 'varchar(255)',
                    'isRequired'  => true,
                    'description' => 'Картинка',
                ],
                [
                    'name'        => 'codon',
                    'type'        => 'varchar(255)',
                    'isRequired'  => true,
                    'description' => 'Картинка кодона',
                ],
                [
                    'name'        => 'amin',
                    'type'        => 'varchar(255)',
                    'isRequired'  => true,
                    'description' => 'amin',
                ],
                [
                    'name'        => 'patern',
                    'type'        => 'varchar(255)',
                    'isRequired'  => true,
                    'description' => 'patern',
                ],
                [
                    'name'        => 'fiz',
                    'type'        => 'varchar(255)',
                    'isRequired'  => true,
                    'description' => 'fiz',
                ],
                [
                    'name'        => 'content',
                    'type'        => 'longlext',
                    'isRequired'  => true,
                    'description' => 'Полное содержание',
                ],
                [
                    'name'        => 'content_ten',
                    'type'        => 'lext',
                    'isRequired'  => true,
                    'description' => 'Полное содержание тени',
                ],
                [
                    'name'        => 'content_dar',
                    'type'        => 'lext',
                    'isRequired'  => true,
                    'description' => 'Полное содержание дара',
                ],
                [
                    'name'        => 'content_siddhi',
                    'type'        => 'lext',
                    'isRequired'  => true,
                    'description' => 'Полное содержание сиддхи',
                ],
            ],
        ]) ?>

        <h2 class="page-header">Расчет</h2>
        <p><code>\avatar\modules\HumanDesign\calculate\HumanDesignAmericaCom</code></p>
        <p><code>user.human_design</code></p>
    </div>
</div>

