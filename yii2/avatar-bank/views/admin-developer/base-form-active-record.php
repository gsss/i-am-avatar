<?php

/** $this \yii\web\View  */

use yii\helpers\Url;
use app\services\GsssHtml;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use app\services\GetArticle\Collection;

$this->title = 'FormActiveRecord';


?>

<div class="container">
    <div class="col-lg-12">
        <h1 class="page-header"><?= Html::encode($this->title) ?></h1>

        <p>Класс упрощающий использование с формами. </p>
        <p>По сути объединяет модель данных и форму.</p>
        <p>И также интегрирует виджеты которые могут поддерживать события.</p>
        <p>Класс который является базовым: <code>\cs\base\FormActiveRecord</code>.</p>
        <p>В виджетах можно использовать следующие события:</p>
        <ul>
            <li><code>onLoad</code> - после загрузки POST, после <code>$model->load()</code></li>
            <li><code>onLoadDb</code> - после загрузки из БД после <code>class::findOne($id)</code></li>
            <li><code>onDelete</code> - перед удалением, перед <code>$model->delete()</code></li>
            <li><code>onUpdate</code> - перед сохранением в БД, перед <code>$model->save()</code></li>
        </ul>
        <p>В таком формате все события описываются в классе виджета:</p>
        <pre>public function onLoad($filed)
{
    // ...
}</pre>
        <p>где <code>$filed</code> - одно из полей, которые описываются в <code>formAttributes()</code>.</p>
        <p>В классе есть функция которая рисует поле и применяет <code>label</code>, <code>hint</code>, <code>widget</code> если они заданы.</p>
        <pre>&lt;?= $model->field($form, 'header') ?&gt;</pre>
        <p>Так же можно как обычно далее как с полем работать</p>
        <pre>&lt;?= $model->field($form, 'description')->textarea(['rows' => 5]) ?&gt;</pre>
        <p>Задаются настройки полей в классе следующим образом:</p>
        <pre>class Penalty extends FormActiveRecord
{
    public static function tableName()
    {
        return 'gs_penalty';
    }

    public function formAttributes()
    {
        return [
            // ...
            [
                'image',
                'Фото',
                0,
                'app\common\widgets\FileUpload3\Validator',
                'widget' => [
                    'app\common\widgets\FileUpload3\FileUpload', [
                        'options' => [
                            'small' => GsssHtml::$formatIcon,
                        ],
                    ],
                ],
            ],
        ];
    }

    public function rules()
    {
        return ArrayHelper::merge(parent::rules(), [
            [['id_penalty', 'user_id', 'date_insert'], 'required', 'on' => 'blank'],
            [['id_penalty', 'user_id', 'date_insert'], 'integer', 'on' => 'blank'],
        ]);
    }

    public function scenarios()
    {
        $scenarios = parent::scenarios();
        $scenarios['blank'] = ['id_penalty', 'user_id', 'date_insert'];

        return $scenarios;
    }</pre>
        <p>Когда формируется класс виджета, то в него добавляются следующие события:</p>
        <ul>
            <li>onLoad</li>
            <li>onLoadDb</li>
            <li>onDelete</li>
            <li>onUpdate</li>
        </ul>
        <p>Пример создания нового класса модели данных:</p>
        <pre>class Penalty extends FormActiveRecord
{
    public static function tableName()
    {
        return 'gs_penalty';
    }

    public function formAttributes()
     {
         return [
             // ...
         ];
     }

     // ...
}</pre>

        <h2 class="page-header">Стандарт функций</h2>
        <p>onLoad</p>
        <p>После загрузки POST загружает значения в переменную модели. Эта функция вызывается после основноего метода модели load и поэтому значение уже будет загружено если поле было названо по стандарту именования полей в формах. По факту эта функция нужна для того чтобы догрузить дополнительные поля или модифицировать то что загружено после POST.</p>
        <p>Без изменения значения функция выглядит так. Хотя она показана для примена, но она избыточна так как она делает тоже самое что делает и стандартная функция загрузки load.</p>

        <pre>public function onLoad($field)
    {
        $attribute = $this->attribute;
        $fieldName = $this->attribute;
        $modelName = $this->model->formName();

        $value = ArrayHelper::getValue(Yii::$app->request->post(), $modelName . '.' . $attribute);

        $this->model->$fieldName = $value;
    }</pre>

        <p>onUpdate</p>


        <pre>public function onUpdate($field)
    {
        $fieldName = $this->attribute;
        $this->model->setAttributes([
            $this->attribute => $this->model->$fieldName,
        ]);

        return true;
    }</pre>

        <h2 class="page-header">Версия 1</h2>
        <p>Используется для старых классов <code>DbRecord</code>. В функциях событий onInser и onUpdate возвращается массив с обновляемыми полями.</p>

        <h2 class="page-header">Версия 2</h2>
        <p>Используется для <code>ActiveRecord</code>. В функциях событий onInsert и onUpdate возвращается логическое значение, было ли изменение полей. Поля устанавливаются через метод <code>setAttributes</code></p>

        <h2 class="page-header">Версия 3</h2>

        <p>Изменена возможность указания виджетов.
            Теперь можно указать функцию <code>attributeWidgets</code> в которой указываются виджеты и их настройки для формы.
            Причем если есть функции <code>attributeWidgets</code> и <code>formAttributes</code> то предпочтение отдается первой.</p>

        <p>Функция <code>formAttributes</code> стала необязательной.</p>
    </div>
</div>

