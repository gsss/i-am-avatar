<?php

/** $this \yii\web\View  */

use yii\helpers\Url;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;

$this->title = 'Поток / группа';

?>

<div class="container">
    <div class="col-lg-12">
        <h1 class="page-header"><?= Html::encode($this->title) ?></h1>


        <h3 class="page-header">Поток</h3>
        <?= \common\services\documentation\DbTable::widget([
            'name'        => 'school_potok_user_root_link',
            'model'       => '\common\models\school\PotokUser3Link',
            'description' => 'Регистрация лида в поток',
            'columns'     => [
                [
                    'name'        => 'id',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Идентификатор задачи',
                ],
                [
                    'name'        => 'user_root_id',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Идентификатор лида',
                ],
                [
                    'name'        => 'is_avatar',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Флаг. Эсть пользователь зарегистрированный?',
                ],
                [
                    'name'        => 'created_at',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Момент создвания',
                ],
                [
                    'name'        => 'potok_id',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Идентификатор потока на который человек зарегистрирован',
                ],
                [
                    'name'        => 'status',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Статус заявки',
                ],
                [
                    'name'        => 'is_unsubscribed',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Флаг. Отписан от рассылки?',
                ],
                [
                    'name'        => 'is_unsubscribed_telegram',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Флаг. Отписан от рассылки в телеграмм?',
                ],
            ],
        ]) ?>

        <h3 class="page-header">Сообщения для регистрации</h3>
        <?= \common\services\documentation\DbTable::widget([
            'name'        => 'school_potok_user_root_link_messages',
            'model'       => '\common\models\school\PotokUser3LinkMessage',
            'description' => 'Сообщения для регистрации',
            'columns'     => [
                [
                    'name'        => 'id',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Идентификатор задачи',
                ],
                [
                    'name'        => 'link_id',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Идентификатор регистрации <code>school_potok_user_root_link.id</code>',
                ],
                [
                    'name'        => 'direction',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Направление сообщения, ',
                ],
                [
                    'name'        => 'created_at',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Момент создвания',
                ],

                [
                    'name'        => 'status',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Статус заявки',
                ],
                [
                    'name'        => 'message',
                    'type'        => 'text',
                    'isRequired'  => true,
                    'description' => 'Сообщение',
                ],
            ],
        ]) ?>
        <pre>create table school_potok_user_root_link_messages
(
	`id` int(11) NOT NULL AUTO_INCREMENT,
	link_id int null,
	direction int null,
	status int null,
	created_at int null,
	message text null,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8');</pre>
        <p>Статусы могут быть разными для разных потоков, поэтому нужно для каждого потока името свои статусы.</p>


        <h3 class="page-header">Статусы для регистрации</h3>
        <?= \common\services\documentation\DbTable::widget([
            'name'        => 'school_potok_user_root_link_status',
            'model'       => '\common\models\school\PotokUser3LinkStatus',
            'description' => 'Сообщения для регистрации',
            'columns'     => [
                [
                    'name'        => 'id',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Идентификатор задачи',
                ],
                [
                    'name'        => 'potok_id',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Идентификатор потока',
                ],
                [
                    'name'        => 'name',
                    'type'        => 'varchar(100)',
                    'isRequired'  => true,
                    'description' => 'Наименование статуса',
                ],
                [
                    'name'        => 'sort_index',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Индекс сортировки',
                ],
            ],
        ]) ?>

        <pre>create table school_potok_user_root_link_status
(
	`id` int(11) NOT NULL AUTO_INCREMENT,
	potok_id int null,
	name varchar(100) null,
	sort_index int null,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8');</pre>


        <h3 class="page-header">\avatar\controllers\PagesController::actionForm()</h3>
        <p>Можно передать параметры в двух вариантах</p>
        <h4 class="page-header">Вариант 1</h4>
        <?= \avatar\services\Params::widget([
            'params' => [
                [
                    'name'        => 'block_id',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Идентификатор блока',
                ],
            ],
        ]) ?>
        <p>Дополнительно передаются поля формы которые описаны в блоке</p>

        <h4 class="page-header">Вариант 2</h4>
        <?= \avatar\services\Params::widget([
            'params' => [
                [
                    'name'        => 'potok_id',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Идентификатор потока',
                ],
                [
                    'name'        => 'email',
                    'type'        => 'string',
                    'isRequired'  => true,
                    'description' => 'Почта',
                ],
                [
                    'name'        => 'name',
                    'type'        => 'string',
                    'isRequired'  => true,
                    'description' => 'Имя заказчика',
                ],
                [
                    'name'        => 'phone',
                    'type'        => 'string',
                    'isRequired'  => true,
                    'description' => 'Телефон',
                ],
            ],
        ]) ?>

    </div>
</div>

