<?php

/** $this \yii\web\View  */

use yii\helpers\Url;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;

$this->title = 'Модули в школе';


?>

<div class="container">
    <div class="col-lg-12">
        <h1 class="page-header"><?= Html::encode($this->title) ?></h1>


        <p>Чтобы подключить модуль к школе нужно создать модуль в папке <code>@school/modules</code>.</p>
        <p>Файл <code>Module.php</code> и <code>config.php</code></p>
        <p>Список роутеров можно добавить так:</p>
        <pre>$app->urlManager->addRules([]);</pre>

        <p><code>/school/config/school.php</code> конфигурация всех школ.</p>
        <p>Каждый модуль конфигурируется в файле <code>/school/config/main.php</code> в bootstrap сессии.</p>
        <p>Если модуль никакой не указан то подключается модуль <code>index</code> и в нем же находятся все <code>rules</code></p>



    </div>
</div>

