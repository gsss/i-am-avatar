<?php

/** $this \yii\web\View  */

use yii\helpers\Url;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;

$this->title = 'Мобильное приложение';


?>

<div class="container">
    <div class="col-lg-12">
        <h1 class="page-header"><?= Html::encode($this->title) ?></h1>

        <h2 class="page-header">Концепт</h2>

        <p>Первая реализация для разработки: Android.</p>
        <p>Модули:</p>
        <ul>
            <li>Список задач</li>
            <li>Курсы для клиентов</li>
            <li>Кошелек</li>
            <li>Карты</li>
            <li>Новости</li>
            <li>Блог</li>
            <li>Чат - возможно телеграм</li>
            <li>Сканер для проверки билетов на событие</li>
            <li>Сканер для курьеров в интернет магазине</li>
            <li>Интернет магазин</li>
        </ul>

        <p>API для мобильного телефона: <code>https://api.i-am-avatar.com</code></p>
        <p><a href="https://docs.google.com/document/d/1J-m7muvbbMtQZImLY52sw6NeQqE2-XcRsIlR9hpekBM/edit#heading=h.3mr9rgjm7b9t" target="_blank">https://docs.google.com/document/d/1J-m7muvbbMtQZImLY52sw6NeQqE2-XcRsIlR9hpekBM/edit#heading=h.3mr9rgjm7b9t</a> - ТЗ для UI для приложения “ЯАватар”</p>

    </div>
</div>



