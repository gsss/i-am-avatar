Магазин
=======

Этот документ описывает общую структуру магазина.

# Терминология


# Введение

Если пользователь набирает товары из разных магазинов, то заказы формируются и потом оплачиваются по отдельности. А если из одного то сразу идет оплата.

![shop-index_2](images/shop-index_2.png)

![shop-index_3](images/shop-index_3.png)

Для обеспечения этого раздела используются следующие таблицы:

- gs_shop_tree - общий каталог магазина
- gs_shop_tree_products_link - связь продукта и категории магазина, то есть определяет к какой 
категори принадлежит товар. Имеет связи многие ко многим и значит один товар может принадлежать нескольким категориям.
- gs_unions_shop - описание и настройки магазина для объединения.
- gs_unions_shop_dostavka - содержит список доставки для магазина. Связь один-ко-многим: `gs_unions_shop_dostavka on (gs_unions_shop.union_id = gs_unions_shop_dostavka.union_id)`.
- gs_unions_shop_product - содержит список продуктов магазина-объединения. Связь один-ко-многим: `gs_unions_shop_product on (gs_unions_shop.union_id = gs_unions_shop_product.union_id)`.
- gs_unions_shop_product_images - содержит дополнительные картинки для товара. Связь один-ко-многим: `gs_unions_shop_product_images on (gs_unions_shop_product.id = gs_unions_shop_product_images.product_id)`.
- gs_unions_shop_tree - содержит каталог магазина. Связь один-ко-многим: `gs_unions_shop_tree on (gs_unions_shop.union_id = gs_unions_shop_tree.union_id)`.
- gs_users_shop_requests - заказы пользователей из магазина. Связь один-ко-многим: `gs_users_shop_requests on (gs_unions_shop.union_id = gs_users_shop_requests.union_id)`.
- gs_users_shop_requests_messages - история исполнения заказа. Связь один-ко-многим: `gs_users_shop_requests_messages on (gs_users_shop_requests.id = gs_users_shop_requests_messages.request_id)`.
- gs_users_shop_requests_products - продукты привязанные к заказу. Связь один-ко-многим: `gs_users_shop_requests_products on (gs_users_shop_requests.id = gs_users_shop_requests_products.request_id)`.

Каждое объединение может создать себе интернет магазин.
В настройках магазина можна задать картинку для писем магазина, подпись, и кошелек Яндекс.Деньги куда будут приходить деньги от покупок.

У магазина есть параметр is_active - 0/1
если он 0 то магазин не отображется в списке магазинов.

# Партнерская ссылка

Партнерская ссылка ведет на магазин. Если пользователь зашел по ней, то вне зависимости от того какой товар он купил он 
записывается как приглашенный к тому кто его пригласил (по чьей ссылке он пришел).

Виды ссылок:
- Главный каталог магазина /shop
- Раздел магазина /shop/category
- Карточка товара /shop/category/product

# Товар

Товар может быть как физическим, так и электронным.
Первый доставляется физический, а второй по почте текстом или прикрепляемыми файлами.
Свойства товара:
- Наименование (name) - Как он отображается в магазине
- Цена - цена которую оплачивает клиент при покупке товара.
- Картинка - картинка показывающая наглядно товар.


## Электронный товар

Если товар электронный то у него проставляется поле `is_electron = 1`.
И поле `electron_text` становится обязательным, оно содержит HTML текст, который будет в теле письма 
отправляемое пользователю при факте оплаты.
Если содержатся еще прикрепленные файлы поля `attached_files`, то они будует прикреплены к письму.

Если в заказе все товары электронные, то:

- Список доставки игнорируется при форме оплаты. Вместо него выводится сообщение "Доставка мгновенно на почту!".
- Также после оплаты, ставится статус заказа `\app\models\Shop\Request::STATUS_DOSTAVKA_ELECTRON_DONE`, 
если в заказе все товары электронные. 

# Статусы заказа

# Доставка



# Модерация

Всего есть два варианта размещения товаров в магазин.
1. Модерация при каждом добавлении/редактироваении
2. Модерация отключена

За это отвечает поле `gs_unions_shop.moderation_mode` `tinyint(1)`
“Свободное размещение товаров без модерации?”
!!!
- 1 - Модерация отключена
- 0, null - Модерация при каждом добавлении/редактировании

Это поле могут редактировать только администраторы

если выбран первый режим то это значит на каждое добавление и редактирование (в том числе и редактирование изображений)
будет высылаться письмо модератору.

Поле `is_show_shop_help_window` означает “скрывать ли окно помощи после первого зобавления в корзину?”

# Модификации товара

Варианты исполнения товаров - это возможность для каждого товара добавить варианты исполнения товаров. Например для одежды это размер.
Возможно задать несколько вариантов задания.
- Один вариант исполнения для всего магазина
- Один вариант исполнения для раздела каталога
- Один вариант исполнения для отдельного товара

Всего есть такие варианты:
- Вариант исполнения не влияет на цену товара.
- Вариант исполнения указывает процентную ставку от первоначальной цены товара.
- Вариант исполнения указывает абсолютную прибавку к цене или вычет.

Модификация товара - это тот же товар но в другом размере, цвете, качестве. Он может влиять на цену а может и не влиять.

Для упрощения системы у нас используется только один список додификации товара с возможностью менять цену в зависимости от выбранного пункта на абсолютную величину или на процент от цены.

Если продавцу необходимо ввести для товара два списка модификации товара, например цвет и размер, то указывается примерно следующим образом
- Белый/5м
- Белый/7м
- Белый/10м
- Черный/5м
- Черный/7м
- Черный/10м

Списки хранятся в таблице `gs_unions_shop_modification_list` со связью на `gs_unions` таким сравнением
`gs_unions.id = gs_unions_shop_modification_list.union_id`.

![Списки](images/shop-index_1.png)

# Таблица "Заказ"

Название таблицы в базе данных: `gs_users_shop_requests`


# Отмена Заказа

Пользователь может отменить заказ. Для этого необходимо нажать на кнопку "Отменить". В заказе есть флаг "Отменен?" в БД `is_canceled`. Таким образом заказ помечается 
отмененным и еще добавлятся запись со статусом "Заказ Отменен". STATUS_REJECTED

## Поля

- sum - float - сумма заказа без учета доставки
- price - float - сумма заказа с учетом доставки

# Таблица "Продукт"

Название таблицы в базе данных: `gs_unions_shop_product`

## Поля

- is_piramida - tinyint - 0 - не участвует в пирамиде, 1 - участвует в пирамиде