<?php

/** $this \yii\web\View  */

use yii\helpers\Url;
use app\services\GsssHtml;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use app\services\GetArticle\Collection;

$this->title = 'Магазин';


?>

<div class="container">
    <div class="col-lg-12">
        <h1 class="page-header"><?= Html::encode($this->title) ?></h1>

        <p>Этот документ описывает общую структуру магазина.</p>
        <h2 class="page-header">Введение</h2>
        <p>Если пользователь набирает товары из разных магазинов, то заказы формируются и потом оплачиваются по
            отдельности. А если из одного то сразу идет оплата.</p>

        <p><img src="/images/controller/admin-developer/shop/shop-index_1.png"></p>
        <p><img src="/images/controller/admin-developer/shop/shop-index_2.png"></p>

        <p>Для обеспечения этого раздела используются следующие таблицы:</p>
        <?php
        $rows = [
            [
                'name'        => 'gs_shop_tree',
                'description' => 'общий каталог магазина',
                'link'        => '',
            ],
            [
                'name'        => 'gs_shop_tree_products_link',
                'description' => 'связь продукта и категории магазина, то есть определяет к какой',
                'link'        => '',
            ],
            [
                'name'        => 'gs_unions_shop',
                'description' => 'описание и настройки магазина для объединения',
                'link'        => '',
            ],
        ];
        ?>
        <?= \yii\grid\GridView::widget([
            'dataProvider' => new \yii\data\ArrayDataProvider([
                'models' => $rows,
            ]),
            'tableOptions' => [
                'class' => 'table table-striped table-hover',
            ],
            'summary'      => '',
        ]) ?>
        <p>Каждое объединение может создать себе интернет магазин.</p>
        <p>В настройках магазина можна задать картинку для писем магазина, подпись, и кошелек Яндекс.Деньги куда будут
            приходить деньги от покупок.</p>


        <?= \common\services\documentation\DbTable::widget([
            'name'    => 'gs_unions_shop_product',
            'columns' => [
                [
                    'name'        => 'id',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Идентификатор строки',
                ],
                [
                    'name'        => 'school_id',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Идентификатор школы',
                ],
                [
                    'name'        => 'name',
                    'type'        => 'varchar(255)',
                    'isRequired'  => true,
                    'description' => 'Название',
                ],
                [
                    'name'        => 'school_id',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Идентификатор школы',
                ],
                [
                    'name'        => 'currency_id',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Идентификатор валюты db.currency.id',
                ],
            ],
        ]) ?>

        <h2 class="page-header">Исследование</h2>
        <p>Ссылка на заказ товара <code>/shop/order?id=12</code>.
            Если заказ происходит без указания параметров, то берется для заказа корзина
        </p>

        <h2 class="page-header">Заказ</h2>
        <p>
            <a href="https://www.draw.io/#G1_dRUB49k_LNc7yoJH04VXVRUwHslG-fT" target="_blank">
                <img src="/images/controller/admin-developer/school-shop/analiz.png" class="thumbnail">
            </a>
        </p>

        <h2 class="page-header">Исследование</h2>
        <pre>
Можно ли считать продажу курса товаром?
Чтобы проводить их по заказам
Или лучше их разделить
Для того чтобы ответить на этот вопрос рассмотрим какими свойствами обладает товар и курс и какими функциями можно пользоваться
Товар
Нужно доставлять
Имеет цену

Курс
Нужно доставлять по другому
Имеет цену

Лучше разделить
Где разместить заказы курсов?
1 можно для каждого курса свой список
2 общий список
Общий список где разместить?
Надо смотреть платформу

Как в бд определять оплату?
Можно поставить флаг. Оплачено полностью
Кстати есть еще один курс и разные его модификации. Как с ними быть?
Так и делать модификация курса, или продажа курса. У меня была сущность продажа курса как предложение курса в определенной его модификации.
Хорошо эту модификацию можно снабжать правилами изменения цены. Если надо в зависимости от условий. Но это позже.

А как насчет рассрочки?
Здесь есть несколько вариантов
1 сделать график платежей и до установленной даты человек должен внести сумму.
2 отслеживать самостоятельно.
Ну в обоих случаях есть методология накопления суммы, но в первом случае есть четкий график. А во втором его нет.
Значит график можно сделать как опцию по умолчанию.

И еще момент который надо рассмотреть.
Что будет если человек не уплатил вовремя?
Автоматически блокируется доступ к курсу?
Да
        </pre>


        <p>Все переменные сохраняются в переменную сессии 'order'. Так конечно же нельзя будет сделать два заказа
            паралельно. Но это не актуально.</p>

        <p>Если задается параметр продукта то создается переменная <code>$request['school_id'] = $product->school_id;
                Yii::$app->session->set('request', $request)</code></p>


        <p>Шаг 1 Авторизация</p>
        <p>Вызывается <code>/shop/login</code> или <code>/shop/registration</code></p>
        <pre>[
    'school_id'     => $product->school_id,
    'price'         => $product->price,
    'currency_id'   => $product->currency_id,
    'productList'   => [$product->id => 1],
]</pre>

        <p>Шаг 2 Доставка</p>
        <p>Вызывается <code>/shop/delivery</code></p>
        <p>Переменные записываются в <code>Yii::$app->session->set('request', $request['delivery'])</code></p>

        <p>Ошибки</p>
        <p>Если валюта доставки отличается от заказа то будет вызвана ошибка</p>
        <p>Если пользователь не авторизован то будет вызвана ошибка 10</p>

        <p>Шаг 3 Адрес доставки</p>
        <p>Вызывается <code>/shop/address</code></p>
        <p>Переменные записываются в <code>Yii::$app->session->set('request', $request['address'])</code></p>

        <pre>$request['address']['index'] = $this->index;
$request['address']['address'] = $this->address;
$request['address']['fio'] = $this->fio;
$request['address']['comment'] = $this->comment;
$request['address']['phone'] = $this->phone;</pre>
        <p>Ошибки</p>
        <p>Если отсутствуют обязательные поля то выводятся ошибки</p>
        <p>Если $request['delivery']['type'] == 1, то это самовывоз и значит проверять адрес и индекс и ФИО не
            нужно.</p>

        <p>Шаг 4 Выбор платежной системы</p>
        <p>Вызывается <code>/shop/pay-system</code></p>
        <p>После выбора платежной системы создается заявка в магазин. Request</p>
        <p>Создается счет db.billing</p>
        <p>добавляются товары gs_users_shop_requests_products</p>

        <p>Шаг 5 Оплата товара или выславление счета на оплату</p>
        <p>происходит редирект на <code>'/shop/pay?id=' + ret.request.id</code></p>

        <?= \common\services\documentation\DbTable::widget([
            'name'        => 'gs_users_shop_requests',
            'model'       => '\common\models\shop\Request',
            'description' => 'Заказ в интернет магазин',
            'columns'     => [
                [
                    'name'        => 'id',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Идентификатор строки',
                ],
                [
                    'name'        => 'user_id',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Пользователь создавший заказ',
                ],
                [
                    'name'        => 'school_id',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Идентификатор школы',
                ],
                [
                    'name'        => 'address',
                    'type'        => 'varchar(255)',
                    'isRequired'  => false,
                    'description' => 'Доставка. Адрес',
                ],
                [
                    'name'        => 'comment',
                    'type'        => 'varchar(255)',
                    'isRequired'  => false,
                    'description' => 'Доставка. Коментарий',
                ],
                [
                    'name'        => 'phone',
                    'type'        => 'varchar(20)',
                    'isRequired'  => false,
                    'description' => 'Доставка. Телефон',
                ],
                [
                    'name'        => 'status',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Статус заказа',
                ],
                [
                    'name'        => 'created_at',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Время создания заказа',
                ],
                [
                    'name'        => 'is_answer_from_shop',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Флаг. есть ответ от магазина? 0 - нет, 1 - да есть ответ.',
                ],
                [
                    'name'        => 'is_answer_from_client',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Флаг. есть ответ от клиента? 0 - нет, 1 - да есть ответ.',
                ],
                [
                    'name'        => 'last_message_time',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Время последнего ответа',
                ],
                [
                    'name'        => 'dostavka_id',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Идентификатор доставки gs_unions_shop_dostavka.id',
                ],
                [
                    'name'        => 'price',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Стоимость заказа. Исключая доставку',
                ],
                [
                    'name'        => 'sum',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Стоимость заказа. Включая доставку',
                ],
                [
                    'name'        => 'currency_id',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Валюта стоимости заказа db.currency.id',
                ],
                [
                    'name'        => 'is_paid',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Флаг. Оплачен заказ? 0 - нет. 1 - оплачен. По умолчанию - 0',
                ],
                [
                    'name'        => 'is_paid_client',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Флаг. Оплачен заказ как подтвердил клиент? 0 - нет. 1 - оплачен. По умолчанию - 0',
                ],
                [
                    'name'        => 'is_hide',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Флаг. Скрыт заказ? 0 - нет. 1 - скрыт. По умолчанию - 0',
                ],
                [
                    'name'        => 'is_canceled',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Флаг. Отменен заказ? 0 - нет. 1 - отменен пользователем. По умолчанию - 0',
                ],
                [
                    'name'        => 'billing_id',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Счет на оплату billing.id',
                ],
                [
                    'name'        => 'parent_id',
                    'type'        => 'int',
                    'isRequired'  => false,
                    'description' => 'Идентификатор пользователя. родитель по рефералке',
                ],
            ],
        ]) ?>

        <h2 class="page-header">Доставка</h2>
        <p>
            <a href="https://www.draw.io/#G0B73bzLewJ2HMRVdpN1pkNHJqYXM" target="_blank">
                <img src="/images/controller/admin-developer/shop/delivery.png" width="100%">
            </a>
        </p>




        <h2 class="page-header">Флаги по оплате</h2>
        <p>Оплата может быть в автоматизированном виде или в ручном.</p>
        <p>В автоматизированном виде платеж подтверждается сразу</p>
        <p>В ручном режиме сначала клиент подтверждает а потом магазин подтверждает что платеж пришел.</p>
        <p>Статусы оплаты вынесены в отдельные флаги чтобы не потать их с доставкой.</p>
        <ul>
            <li>В самовывозе оплата может быть заранее а может быть по факту получения.</li>
            <li>В Доставке по почте только предоплата.</li>
            <li>В электронном товаре только предоплата.</li>
            <li>В наложном платеже оплата идет после получения товара клиентом. И еще может быть не оплачен и товар
                вернется назад.
            </li>
        </ul>
        <p>В связи со всем этим вводятся флаги <code>is_paid</code> - флаг который указывает что подтвердил магазин. <code>is_paid_client</code> -
            флаг который указывает что подтвердил магазин. В итоге получаем такой расклад возможных комбинаций:</p>
        <?php
        $rows = [
            [
                'event'           => 'Не оплачено',
                'is_paid_client' => 0,
                'is_paid'        => 0,
            ],
            [
                'event'           => 'Клиент подтвердил',
                'is_paid_client' => 1,
                'is_paid'        => 0,
            ],
            [
                'event'           => 'Магазин подтвердил',
                'is_paid_client' => 0,
                'is_paid'        => 1,
            ],
        ];
        ?>
        <?= \yii\grid\GridView::widget([
            'dataProvider' => new \yii\data\ArrayDataProvider(['allModels' => $rows]),
            'tableOptions' => [
                'class' => 'table table-striped table-hover',
            ],
            'summary' => '',
        ]) ?>
        <p>Так как счета могут подтверждать клиенты то я предполагаю что нужно разместить поле is_paid_client в таблицу db.billing и так получится что не нужно в заказы добавлять это поле.</p>

        <h2 class="page-header">Параметр рефералки в URL</h2>
        <p>Для параметра определяющего идентификатор партнера определено имя <code>partner_id</code>.</p>
        <p>Этот параметр может быть указан в любом месте сайта.</p>
        <p>Обработчик стоит в событии <code>on beforeRequest</code></p>
        <p>Значение сохраняется в переменную сессии <code>ReferalProgram => ['parent_id' => $value]</code></p>
        <p>Параметр из сессии сохраняется в событии <code>\avatar\models\forms\ShopPaySystem::action()</code> события <code>\avatar\controllers\ShopController::actionPaySystem()</code> в заказ <code>\common\models\shop\Request</code></p>
        <p>Далее по факту поступления денег вызывается событие <code>\common\models\shop\Request::successShop()</code> и в нем уже он попадает в таблицу <code>school_user_partner</code> (<code>\common\models\school\UserPartner</code>)</p>

        <h2 class="page-header">Настройка партнеской программы</h2>
        <p>/cabinet-school-mlm/index</p>

        <h2 class="page-header">На будущее</h2>
        <p>Склады</p>
        <p>Курьеры</p>
        <p>Модификации товара</p>
        <p>Промокоды</p>
        <p>Накладные</p>
        <p>Параметры (для сравнения)</p>

        <h2 class="page-header">Авторизация</h2>
        <p>Все это будет в школе, поэтому надо проверять наличие человека в школе</p>

        <p>Самый тонкий момент во всей авторизации это регистрация при покупке об этом в разделе регистрация</p>


        <h3 class="page-header">Логин</h3>
        <p>Каков алгоритм проверки пользователя?</p>
        <p>Проверяется наличие в таблице user. Проверяется нет ли флага mark_deleted.</p>
        <p>Если пользователь не присоединен к школе, что делать? Я предлагаю молча добавлять потому что он же клиент школы становится. Хотя такая ситуация минимальна. ТО есть сложно представить что пользователь зарегистрировался в другой школе или аватаре а потом зашел на незнакомый сайт и он скорее будет регистрироваться. А вот здесь уже еще одна ситуация возникает.</p>

        <h3 class="page-header">заказ</h3>

        <p>
            <a href="https://www.draw.io/#G1rn3nYuJKBhV7wZ8q93TL8CL3s9gvkx_q" target="_blank">
                <img src="/images/controller/admin-developer/school-shop/order.png" >
            </a>
        </p>

        <h3 class="page-header">Регистрация</h3>
        <p>Самый тонкий момент во всей авторизации это регистрация при покупке потому что пользователь указывает email и пароль и сразу он помещается в таблицу user. Но дело в том что email еще не подтвержден, поэтому вероятна ситуация что человек введет адрес и вследствии ошибки или намеренно в таблицу попадет не проверенный адрес. Человек конечно же с большей вероятностью будет указывать свой. Но все же так как остается такая вероятность, допустим даже и по ошибке, что вполне возможно. Поэтому я введу еще одно поле показывающее что пользователь после регистрации и еще не подтвердил свою регистрацию. Например поле is_email_confirm.</p>
        <p>Пользователю высылается письмо со ссылкой активации в любом случае.</p>
        <p>Рассмотрю две ситуации</p>
        <p>Ситуация 1. Пользователь ввел верный email</p>
        <p>Пользователь активирует ссылку и флаг is_email_confirm ставится в положение 1.</p>
        <p>В остальном считается что пользователь ввел верный email.</p>
        <p>Ситуация 2. Пользователь ввел не верный email</p>
        <p>Такое может быть в следствии ошибки или намеренно.</p>
        <p>Если пользователь ошибся и оплатил. То нельзя будет достучаться на пользователя. Пока он сам в магазин не позвонит и не идентифицирует себя.</p>
        <p>Это можно сгладить тем что пользователя обяжут вводить мобильный номер и по нему можно будет связаться.</p>

        <p>Ситуация 3. Пользователь уже зарегистрирован в системе Аватар и регистрируется</p>
        <p>Здесь ему надо сказать: "Вы уже зарегистрированы во вселенной ЯАватар и надо войти."</p>

        <h3 class="page-header">Заказ от имени другого клиента</h3>
        <p>Иногда возникает необходимсоть сделать заказ за клиента, например когда он звонит менеджеру по телефону.</p>
        <p>Мастер так же как и мастер добавления события<br>
            1 Выбор пользователя<br>
            2 Вариант доставки<br>
            3 Поля для доставки<br>
            4 Платежная система</p>
        <p>Модели для шагов: <code>\avatar\models\forms\shop\RequestAdd\Step1</code></p>
        <p>Действие для обработки форм шагов по AJAX: <code>\avatar\controllers\actions\CabinetSchoolShopRequestsAjax\actionStep</code></p>
        <p>Контроллер для обработки форм шагов по AJAX: <code>\avatar\controllers\CabinetSchoolShopRequestsAjaxController</code></p>



        <h3 class="page-header">Каталог</h3>

        <p>Один товар может принядлежать одной категории.</p>

        <?= \common\services\documentation\DbTable::widget([
            'name'        => 'gs_unions_shop_product',
            'model'       => '\common\models\shop\CatalogItem',
            'description' => 'Каталог магазина',
            'columns'     => [
                [
                    'name'        => 'id',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Идентификатор строки',
                ],
                [
                    'name'        => 'school_id',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Идентификатор школы',
                ],
                [
                    'name'        => 'parent_id',
                    'type'        => 'int',
                    'isRequired'  => false,
                    'description' => '? Скорее удалить надо',
                ],
                [
                    'name'        => 'name',
                    'type'        => 'varchar(255)',
                    'isRequired'  => true,
                    'description' => 'Название',
                ],
                [
                    'name'        => 'description',
                    'type'        => 'varchar(200)',
                    'isRequired'  => true,
                    'description' => 'Описание каталога',
                ],
                [
                    'name'        => 'id_string',
                    'type'        => 'varchar(100)',
                    'isRequired'  => true,
                    'description' => 'Идентификатор для URL',
                ],
                [
                    'name'        => 'sort_index',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Индекс сортировки',
                ],
                [
                    'name'        => 'created_at',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Момент создания записи',
                ],
            ],
        ]) ?>


        <h3 class="page-header">Подтверждение платежа</h3>

        <p>Для того чтобы вручную подтвердить платеж для разных платежных системы нужно собрать от пользователя дополнительные поля чтобы идентифицировать платеж.</p>
        <p>Для этого в таблице <code>paysystems_success</code> содержатся модель и представление для показа диалогового окна на странице <code>avatar-bank/views/shop/pay.php</code>.</p>
        <p>
            <a href="https://www.draw.io/#G1rHyNCcD-pBgp4_h7cey1oVRL15RHk8-8" target="_blank">
                <img src="/images/controller/admin-developer/shop/success.png" class="thumbnail">
            </a>
        </p>

        <?= \common\services\documentation\DbTable::widget([
            'name'        => 'paysystems_success',
            'model'       => '\common\models\PaySystemSuccess',
            'description' => 'Данные для модального окна подтверждения платежа',
            'columns'     => [
                [
                    'name'        => 'id',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Идентификатор строки',
                ],
                [
                    'name'        => 'paysystem_id',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Идентификатор платежной системы',
                ],
                [
                    'name'        => 'model',
                    'type'        => 'varchar(100)',
                    'isRequired'  => true,
                    'description' => 'Модель начиная со слеша',
                ],
                [
                    'name'        => 'view',
                    'type'        => 'varchar(100)',
                    'isRequired'  => true,
                    'description' => 'Описание каталога',
                ],
            ],
        ]) ?>

        <h2 class="page-header">Форма оплаты</h2>
        <p>
            <a href="https://www.draw.io/#G1iwyseHB8y8O_U0KdZSOTsC6ls59gFp4p" target="_blank">
                <img src="/images/controller/admin-developer/shop/pay.png" class="thumbnail" width="100%">
            </a>
        </p>
    </div>
</div>

