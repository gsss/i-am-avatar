<?php

/** $this \yii\web\View  */

use yii\helpers\Url;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;

$this->title = 'FileUpload8 - загружает файл';


?>

<div class="container">
    <div class="col-lg-12">
        <h1 class="page-header"><?= Html::encode($this->title) ?></h1>


        <h2 class="page-header">Виджеты для загрузки FileUpload8</h2>
        <p>Строится на основе FileUpload6. Предназначен для загрузки любых файлов.</p>

        <p>Если загружается картинка JPG, JPEG, PNG то для нее автоматически будет создана превью с индексом <code>crop</code>. То есть если название файла <code>https://cloud1.i-am-avatar.com/upload/cloud/15568/46243_hNCURaKz4n.jpg</code> то предпросмотр будет под именем <code>https://cloud1.i-am-avatar.com/upload/cloud/15568/46243_hNCURaKz4n_crop.jpg</code>.</p>

        <h2 class="page-header">Запись в таблицу <code>school_file</code></h2>
        <p>Так как каждый загруженный файл для платформы сохраняется, то он должен быть записан в таблицу <code>school_file</code>.</p>
        <p>Записать его можно только после загрузки на сервер, поэтому придется вызывать AJAX после каждой загрузки файла.</p>
        <p>Например AJAX <code>/cabinet/file-upload8-save</code>.</p>
        <p>Какие должны быть обеспечены уровни доступа? Только для авторизованных.</p>
        <p><b>Исследование на угрозы</b></p>
        <p>А что можно сделать? Послать запросы якобы я загрузил. Но нужен будет CSRF чего автономно сделать нельзя.</p>
        <hr>
        <p>Еще хочу ввести параметр типа объекта, по мимо школы и его также надо будет передавать в запрос сохранения файла в таблицу <code>school_file</code></p>
        <p>Какие типы данных могут быть?</p>
        <p>- Файл для конструктора</p>
        <p>- Файл для комментария</p>
        <p>- Файл для таблицы (блог, школа, уроки и др.)</p>

    </div>
</div>

