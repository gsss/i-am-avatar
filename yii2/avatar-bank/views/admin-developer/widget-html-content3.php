<?php

/** $this \yii\web\View  */

use yii\helpers\Url;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;

$this->title = 'Виджет для редактирования HTML с загрузкой файлов картинок в облако HtmlContent3';


?>

<div class="container">
    <div class="col-lg-12">
        <h1 class="page-header"><?= Html::encode($this->title) ?></h1>

        <p><code>common\widgets\HtmlContent3</code></p>
        <p>Его код размеще одновременно в двух репозиториях.</p>
        <ul>
            <li>Аватар Root</li>
            <li>Аватар Cloud</li>
        </ul>

        <p>Сервер Аватар Cloud принимает файлы по пути <code>/html-content3/upload</code> и сохраняет у себя в папке <code>/upload/HtmlContent3/1557488638_iZWX8hCB2m.jpg</code></p>

        <p>Сейчас глюк какойто есть <a href="https://yadi.sk/i/yiK2UHQY7Ps2cw" target="_blank">https://yadi.sk/i/yiK2UHQY7Ps2cw</a></p>




    </div>
</div>

