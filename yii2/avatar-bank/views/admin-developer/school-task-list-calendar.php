<?php

/** $this \yii\web\View  */

use yii\helpers\Url;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;

$this->title = 'Школа. Список задач. Календарный вид';


?>

<div class="container">
    <div class="col-lg-12">
        <h1 class="page-header"><?= Html::encode($this->title) ?></h1>

        <h2 class="page-header">Календарный вид задач</h2>
        <p>Этот вид предназначен для задач которые повторяются изо дня в день. Поэтому они отображаются в виде календаря. Значит для дней будет отдельная таблица. Так же существуют роли людей значит для каждой роли будет таблица.</p>
        <p><img src="/images/controller/admin-developer/school-tasks/table.png" width="70%" class="thumbnail"></p>
        <p>Определю каждую сущность</p>

        <h3 class="page-header">Календарный день</h3>
        <p>Так как день для всех школ один то сделаю его пока общим</p>

        <?= \common\services\documentation\DbTable::widget([
            'name'        => 'school_task_day',
            'description' => 'День (сутки) календаря',
            'model'       => '\common\models\task\Day',
            'columns'     => [
                [
                    'name'        => 'id',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Идентификатор записи',
                ],
                [
                    'name'        => 'day',
                    'type'        => 'date',
                    'isRequired'  => true,
                    'description' => 'День',
                ],
            ],
        ]) ?>

        <h3 class="page-header">Роль</h3>
        <?= \common\services\documentation\DbTable::widget([
            'name'        => 'school_task_role',
            'description' => 'Роль для календаря',
            'model'       => '\common\models\task\Role',
            'columns'     => [
                [
                    'name'        => 'id',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Идентификатор записи',
                ],
                [
                    'name'        => 'school_id',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Идентификатор школы',
                ],
                [
                    'name'        => 'name',
                    'type'        => 'varchar(100)',
                    'isRequired'  => true,
                    'description' => 'Наименование роли',
                ],
            ],
        ]) ?>

        <p>Так же удобно сделать календари в виде досок, в доске есть группы, в группы набиваются роли как строки, а на роль уже назначаются конкретные исполнители. Значит еще добавлются две сущности доска и группа. Досок может быть несколько в одной школе. Группа это дочерний элемент для доски.</p>

        <?= \common\services\documentation\DbTable::widget([
            'name'        => 'school_task_table',
            'description' => 'Доска задач',
            'model'       => '\common\models\task\Table',
            'columns'     => [
                [
                    'name'        => 'id',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Идентификатор записи',
                ],
                [
                    'name'        => 'school_id',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Идентификатор школы',
                ],
                [
                    'name'        => 'name',
                    'type'        => 'varchar(100)',
                    'isRequired'  => true,
                    'description' => 'Наименование доски',
                ],
            ],
        ]) ?>

        <?= \common\services\documentation\DbTable::widget([
            'name'        => 'school_task_role',
            'description' => 'Роль для календаря',
            'model'       => '\common\models\task\Role',
            'columns'     => [
                [
                    'name'        => 'id',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Идентификатор записи',
                ],
                [
                    'name'        => 'name',
                    'type'        => 'varchar(100)',
                    'isRequired'  => true,
                    'description' => 'Наименование роли',
                ],
            ],
        ]) ?>
        
        <p><img src="/images/controller/admin-developer/school-tasks/table2.png" width="50%" class="thumbnail"></p>
        <p>Нужен виджет для рисования такой таблицы <code>TaskTable</code></p>
        <p><code>\avatar\widgets\TaskTable\TaskTable</code></p>
        <p>Выводит на две недели график. В клетке дня указывается исполнитель или несколько и тот кто принимает задачу.</p>
    </div>
</div>

