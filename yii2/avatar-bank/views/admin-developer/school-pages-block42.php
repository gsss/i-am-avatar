<?php

/** $this \yii\web\View  */

use yii\helpers\Url;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;

$this->title = 'Конструктор сайтов. Блок 42. Уведомление об использовании файлов cookie';


?>

<div class="container">
    <div class="col-lg-12">
        <h1 class="page-header"><?= Html::encode($this->title) ?></h1>


        <p class="alert alert-success">Мы используем файлы cookie, чтобы улучшить Ваше пребывание на нашем сайте. Продолжая пользоваться сайтом, вы соглашаетесь с политикой использования cookie-файлов.</p>

        <h2 class="page-header">Настройки в БД</h2>
        <pre><code class="json">{}</code></pre>



    </div>
</div>
