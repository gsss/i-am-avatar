<?php

/** $this \yii\web\View  */

use yii\helpers\Url;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;

$this->title = 'Deploy';


?>


<div class="container">
    <div class="col-lg-12">
        <h1 class="page-header"><?= Html::encode($this->title) ?></h1>

        <p>
            <a href="https://app.diagrams.net/#G1oRIaJAOTpYXOGZnwY5K2rZ-30Z5MTnIA" target="_blank">
                <img src="/images/controller/admin-developer/deploy/map.png">
            </a>
        </p>

        <pre>server {
  listen 80;
  // listen [::]:80;

  server_name pull.test777.i-am-avatar.com;

  location / {
    gzip off;
    fastcgi_pass unix:/var/run/fcgiwrap.socket;
    include /etc/nginx/fastcgi_params;
    fastcgi_param DOCUMENT_ROOT /home/god/i-am-avatar;
    fastcgi_param SCRIPT_FILENAME /home/god/i-am-avatar/deploy.sh;
  }
}</pre>
        <p>deploy.sh</p>
        <pre>cd /home/god/i-am-avatar/www
git pull</pre>
        <p><a href="https://www.youtube.com/watch?v=NaYyHL1uYUg" target="_blank">https://www.youtube.com/watch?v=NaYyHL1uYUg</a>
        <p><a href="https://www.youtube.com/watch?v=yrTavxEF7a0" target="_blank">https://www.youtube.com/watch?v=yrTavxEF7a0</a>

    </div>
</div>