<?php

/** $this \yii\web\View  */

use yii\helpers\Url;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;

$this->title = 'env';


?>

<div class="container">
    <div class="col-lg-12">
        <h1 class="page-header"><?= Html::encode($this->title) ?></h1>

        <?php
        $rows = [
            [
                'url'               => 'https://www.i-am-avatar.com',
                'db_main'           => 'i_am_avatar_prod_main',
                'db_stat'           => 'i_am_avatar_prod_stat',
                'db_wallet'         => 'i_am_avatar_prod_wallet',
                'ip'                => '89.40.117.156',
                'YII_ENV'           => 'prod',
                'YII_DEBUG'         => 'false',
                'telegram'          => '@iAvatar_bot',
                'telegram_callback' => 'https://www.i-am-avatar.com/telegram/call-back',
            ],
            [
                'url'               => 'https://test777.i-am-avatar.com',
                'db_main'           => 'i_am_avatar_test_main',
                'db_stat'           => 'i_am_avatar_test_stat',
                'db_wallet'         => 'i_am_avatar_test_wallet',
                'ip'                => '89.40.117.156',
                'YII_ENV'           => 'test',
                'YII_DEBUG'         => 'true',
                'telegram'          => '@iAvatarTest_bot',
                'telegram_callback' => 'https://www.i-am-avatar.com/telegram/call-back',
            ],
        ];
        ?>
        <?= \yii\grid\GridView::widget([
            'dataProvider' => new \yii\data\ArrayDataProvider([
                'allModels' => $rows,
            ]),
            'tableOptions' => [
                'class' => 'table table-striped table-hover',
                'style' => 'width: auto',
            ],
            'summary'      => '',
            'columns'      => [
                [
                    'attribute' => 'url',
                    'content'   => function ($i) {
                        return Html::a($i['url'], $i['url'], ['target' => '_blank']);
                    },
                ],
                [
                    'attribute' => 'db_main',
                    'content'   => function ($i) {
                        return Html::tag('code', $i['db_main']);
                    },
                ],
                [
                    'attribute' => 'db_stat',
                    'content'   => function ($i) {
                        return Html::tag('code', $i['db_stat']);
                    },
                ],
                [
                    'attribute' => 'db_wallet',
                    'content'   => function ($i) {
                        return Html::tag('code', $i['db_wallet']);
                    },
                ],
                [
                    'attribute' => 'ip',
                    'content'   => function ($i) {
                        return Html::tag('code', $i['ip']);
                    },
                ],
                [
                    'attribute' => 'YII_ENV',
                    'content'   => function ($i) {
                        return Html::tag('code', $i['YII_ENV']);
                    },
                ],
                [
                    'attribute' => 'YII_DEBUG',
                    'content'   => function ($i) {
                        return Html::tag('code', $i['YII_DEBUG']);
                    },
                ],
                [
                    'attribute' => 'telegram',
                    'content'   => function ($i) {
                        return Html::tag('code', $i['telegram']);
                    },
                ],
                [
                    'attribute' => 'telegram_callback',
                    'content'   => function ($i) {
                        return Html::tag('code', $i['telegram_callback']);
                    },
                ],
            ],
        ]) ?>

    </div>
</div>

