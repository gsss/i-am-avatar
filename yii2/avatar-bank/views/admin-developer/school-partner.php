<?php

/** $this \yii\web\View  */

use yii\helpers\Url;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;

$this->title = 'Школа. Партнерская программа';


?>

<div class="container">
    <div class="col-lg-12">
        <h1 class="page-header"><?= Html::encode($this->title) ?></h1>

        <p>Задача:
            сделать так, чтобы по ссылке можно было зайти и заказать товар и можно было понять, что заказ от меня и я
            мог получить реферему.</p>
        <p>Что для этого надо?</p>
        <p>1.Документация.</p>
        <p>Какого вида будет реферальная ссылка, например: <code>http://www.aura999.ru/request?partner_id=4278</code>
        </p>
        <p>Так любой заказ может получить и партнёр, только те, по которым прошли по его реферальной ссылке.</p>
        <p>Где клиент будет видеть свои заказы?</p>
        <p>В личном меню/заказы курсов.</p>
        <p>В личном меню/заказы товаров.</p>
        <p>Где будет формироваться структура, кто под кем?</p>
        <p>Лиды здесь точно не актуальны, значит это только пользователи. Причем нужно же чтобы я один в разных школах
            на разных уровнях был.</p>
        <p>Значит это какая то внешняя таблица. Например school_user_pirmaida. Что там надо указывать?</p>
        <p>id, school_id, uid, parent_id, created_at</p>
        <p>uid - пользователь который купил товар, parent_id - его родитель, created_at - время когда произошло
            присоединение.</p>


        <?= \common\services\documentation\DbTable::widget([
            'name'        => 'school_user_partner',
            'model'       => '\common\models\school\UserPartner',
            'description' => 'Записи о партнерах в школе',
            'columns'     => [
                [
                    'name'        => 'id',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Идентификатор задачи',
                ],
                [
                    'name'        => 'school_id',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Идентификатор школы',
                ],
                [
                    'name'        => 'user_id',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Идентификатор пользователь который купил товар',
                ],
                [
                    'name'        => 'parent_id',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Идентификатор родителя',
                ],
                [
                    'name'        => 'created_at',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Момент создвания',
                ],
            ],
        ]) ?>
        <p>Как понять что товар или школа участвуют в партнерской программе? Нужен какой то флаг в школе что партнерка включена. Например давай это будет <code>school.is_partner</code>. Допустим. Далее что? Далее надо делать заказ со ссылкой реферальной. </p>
        <p>Где заказ? <code>/shop/order?id=1</code></p>
        <p>Когда гость заходит в школу например по ссыле <code>http://www.aura999.ru/request?partner_id=4278</code> то параметр <code>partner_id</code> сохраняется в переменную сессии <code>partner_id</code> и потом по факту покупки уже будет распределение. Хотя если подтверждение будет уже в другом месте и по другой сессии то надо его запомнить в заказе в интернет магазине, то есть в таблице <code>gs_users_shop_requests</code>.</p>

        <h2 class="page-header">Сохрание в сессии</h2>
        <p>Идентификатор пригласителя сохраняется в сессии в параметре <code>ReferalProgram[parent_id]</code></p>
        <p>То есть в сессии он выглядит так:</p>
        <pre>[
    'ReferalProgram' => [
        'parent_id' => 458
    ]
]</pre>
        <h2 class="page-header">Где взять ссылку</h2>
        <p>В разделе меню пользователя Профиль и далее "Реферальная ссылка" или по ссылке<code>/cabinet/referal-link</code> в кабинете.</p>

        <h2 class="page-header">Заказ товара /shop/order</h2>
        <p>Сейчас я сделаю пока заказ одного товара. Чтобы заказать один товар нужно вызвать <code>/shop/order?id=1</code> где id - идентификатор товара</p>


        <h2 class="page-header">Реферальные начисления</h2>
        <p>Для реферальных начислений создаю две таблицы:<br>
            school_referal_level - указание процентов по уровням<br>
            school_referal_transaction - указание транзакций всех отчислений
        </p>

        <?= \common\services\documentation\DbTable::widget([
            'name'        => 'school_referal_level',
            'model'       => '',
            'description' => 'процент по уровням',
            'columns'     => [
                [
                    'name'        => 'id',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Идентификатор строки',
                ],
                [
                    'name'        => 'school_id',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Идентификатор школы',
                ],

                [
                    'name'        => 'level',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Уровень. нулевого нет. 1 - первый после покупки',
                ],
                [
                    'name'        => 'percent',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Процент для уровня * 100',
                ],
            ],
        ]) ?>

        <?= \common\services\documentation\DbTable::widget([
            'name'        => 'school_referal_transaction',
            'model'       => '',
            'description' => 'Транзакций всех отчислений',
            'columns'     => [
                [
                    'name'        => 'id',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Идентификатор строки',
                ],
                [
                    'name'        => 'school_id',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Идентификатор школы',
                ],
                [
                    'name'        => 'request_id',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Идентификатор заявки',
                ],
                [
                    'name'        => 'from_uid',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Идентификатор пользователя. От кого',
                ],
                [
                    'name'        => 'to_uid',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Идентификатор пользователя. Кому',
                ],
                [
                    'name'        => 'to_wid',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Идентификатор кошелька. Кому',
                ],
                [
                    'name'        => 'level',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Идентификатор уровня. см school_referal_level.level',
                ],
                [
                    'name'        => 'amount',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Кол-во монет',
                ],
                [
                    'name'        => 'transaction_id',
                    'type'        => 'bigint',
                    'isRequired'  => true,
                    'description' => 'Идентификатор транзакции',
                ],
                [
                    'name'        => 'currency_id',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Идентификатор валюты, dbWallet.currency.id',
                ],
                [
                    'name'        => 'created_at',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Время создания отчисления',
                ],
            ],
        ]) ?>


        <p>Кошелек откуда берутся реферальные начисления указывается в параметре school.referal_wallet_id</p>


        <p>Если пользователь уже встроен в пирамиду, то ссылка не актуальна. school_user_partner</p>
        <p>Если ссылки нет то пользователь может быть в уже в реферальной структуре. school_user_partner</p>
        <p>Если в школе используется партнерка то выставляется флаг school.is_mlm</p>
        <p>Если включается партнерка то<br>
            создается монета<br>
            делается эмиссия<br>
            указываются проценты
        </p>

        <h2 class="page-header">Жизненный цикл реферальной ссылки</h2>
        <p>Когда приглашенный заходит по ссылке на сайт, то сначала она в событии <code>on beforeRequest</code> вызывается <code>\avatar\modules\Shop\Shop::onBeforeRequest()</code>. Там она сохраняется в сессию.</p>

        <p><code>\common\models\shop\Request::successShop()</code></p>
        <p>
            <a href="https://www.draw.io/#G1LNVjFN7mH0Ig77cIFuaaR2_DFd8xNZYu" target="_blank">
                <img src="/images/controller/admin-developer/school-partner/p.png" width="100%">
            </a>
        </p>
        <h2 class="page-header">Реферальные начисления</h2>
        <p><code>/cabinet/referal-transactions</code></p>

        <h2 class="page-header">Настройки партнерки</h2>
        <p>\avatar\controllers\CabinetSchoolMlmController()</p>

        <h2 class="page-header">QR код</h2>
        <p>Если карта не зарегистрирована то идет редирект на страницу регистрации карты, если пользователь уже авторизован то присоединение карты.</p>
        <p>Если карта зарегистрирована, то учитывается настрока <code>user.card_action</code> что делать.</p>
        <p>1 - открыть свой профиль</p>
        <p>2 - открыть карту</p>
        <p>3 - открыть свой профиль + карту</p>
        <p>4 - перекинуть на покупку карты по моей реферальной ссылке</p>
        <p>5 - Спросить что делать</p>
        <p>Настрока <code>user.card_action</code> Задается в профиле <code>/cabinet/card-action</code>.</p>


        <h2 class="page-header">Бизнес процесс реферальной системы</h2>
        <p>
            <a href="https://www.draw.io/#G1u52e86RUvOu2Sq8UjpSNEy6pWikcn2ig" target="_blank">
                <img src="/images/controller/admin-developer/school-partner/bp.png" class="thumbnail">
            </a>
        </p>

        <h2 class="page-header">Вывод денежных знаков</h2>

        <p>В таблице будут храниться заявки на вывод</p>
        <?= \common\services\documentation\DbTable::widget([
            'name'        => 'school_referal_out_request',
            'model'       => '\common\models\school\ReferalRequestOut',
            'description' => 'Заявка на вывод',
            'columns'     => [
                [
                    'name'        => 'id',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Идентификатор строки',
                ],
                [
                    'name'        => 'school_id',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Идентификатор школы',
                ],
                [
                    'name'        => 'user_id',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Идентификатор пользователя заказавшего вывод',
                ],
                [
                    'name'        => 'currency_id',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Идентификатор валюты партнерской программы db.currency.id',
                ],
                [
                    'name'        => 'to',
                    'type'        => 'string',
                    'isRequired'  => true,
                    'description' => 'Адрес кошелька для вывода',
                ],
                [
                    'name'        => 'amount',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Кол-во атомов для вывода',
                ],
                [
                    'name'        => 'status',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Статус заявки, 0 - создана, 1 - выведено успешно, 2 - отвергнута',
                ],
                [
                    'name'        => 'transaction_id',
                    'type'        => 'bigint',
                    'isRequired'  => true,
                    'description' => 'Идентификатор транзакции списания партнерских монет',
                ],
                [
                    'name'        => 'created_at',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Время создания отчисления',
                ],
            ],
        ]) ?>
        <p>Встает вопрос: а как определить какая монета партнерская? <code>school.referal_wallet_id</code></p>
        <p>Далее делаю создание заявки на вывод в кабинете.</p>
        <p>Кого уведомлять? Пока админов. Письмом. Это же в школе должно проходить. Письмо <code>referal_request_out</code>. Письмо формируется в action=<code>\avatar\controllers\CabinetReferalRequestController::actionAdd()</code></p>
        <p>Нужно рассмотреть ситуацию когда рефералка в школе не создана а человек заходит в вывод реф начислений <a href="https://yadi.sk/i/a7bcsJWWaOYMRg">https://yadi.sk/i/a7bcsJWWaOYMRg</a></p>

    </div>
</div>

