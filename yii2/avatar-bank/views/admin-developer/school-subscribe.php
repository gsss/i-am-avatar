<?php

/** $this \yii\web\View  */

use yii\helpers\Url;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;

$this->title = 'Школа. Рассылки';


?>

<div class="container">
    <div class="col-lg-12">
        <h1 class="page-header"><?= Html::encode($this->title) ?></h1>

        <p>Где сделать рассылку письма?</p>
        <p>Можно в классе Subscribe метод sendArray. Ну можно например.</p>
        <p><code>\common\services\Subscribe::sendArray</code></p>
        <pre>/**
* Добавляет в очередь пакет писем
*
* @param array  $emailList пакет писем
* @param string $subject   тема письма
* @param string $view      представление котореое лежит относительно '@avatar/mail/html'
* @param array  $options   настройки для представления
* @param array  $layout    default 'layouts/html'
*/</pre>
        <p><code>$layout</code> если это строка то то путь к обложке (layout) письма от пути '@avatar/mail' в конце по умолчанию добавляется '.php'. В итоге если <code>$layout</code> = <code>layouts/html</code> то это значит файлом обложки является <code>@avatar/mail/layouts/html.php</code>.</p>
        <p>если это массив то следующие параметры:</p>

        <p><code>layout</code> то же что и этот параметр в виде строки.</p>
        <p><code>image</code> Картинка для письма</p>
        <p><code>from_name</code> от кого имя</p>
        <p><code>from_email</code> от кого почта</p>

        <h2 class="page-header">Шаблон (обложка) <code>layouts/html</code></h2>
        <p>
            <img src="/images/controller/admin-developer/school-subscribe/2019-02-24_01-52-42.png" class="thumbnail" width="400">
        </p>
        <h2 class="page-header">Шаблон (обложка) <code>layouts/html/subscribe</code></h2>
        <p><img src="/images/controller/admin-developer/school-subscribe/2019-03-25_03-03-27.png" class="thumbnail" width="400"></p>


        <h2 class="page-header">gs_subscribe</h2>
        <?= \common\services\documentation\DbTable::widget([
            'name'        => 'gs_subscribe',
            'description' => 'Рассылка действующая, очередь',
            'model'       => '\common\models\subscribe\Subscribe',
            'columns'     => [
                [
                    'name'        => 'id',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Идентификатор процесса',
                ],
                [
                    'name'        => 'html',
                    'type'        => 'longtext',
                    'isRequired'  => true,
                    'description' => 'html для письма',
                ],
                [
                    'name'        => 'text',
                    'type'        => 'longtext',
                    'isRequired'  => true,
                    'description' => 'text для письма, уже практически не используется, подразумевалось что письмо может иметь HTML и TEXT и если HTML не поддерживается то открывается текст. Но ввиду того что HTML поддерживается везде то необходимость уже отпала',
                ],
                [
                    'name'        => 'date_insert',
                    'type'        => 'integer',
                    'isRequired'  => true,
                    'description' => 'Момент добавления рассылки',
                ],
                [
                    'name'        => 'subject',
                    'type'        => 'varchar(255)',
                    'isRequired'  => true,
                    'description' => 'Тема письма',
                ],
                [
                    'name'        => 'image',
                    'type'        => 'varchar(255)',
                    'isRequired'  => true,
                    'description' => 'Картинка для письма',
                ],
                [
                    'name'        => 'from_name',
                    'type'        => 'varchar(100)',
                    'isRequired'  => true,
                    'description' => 'Имя для обратного адреса',
                ],
                [
                    'name'        => 'from_email',
                    'type'        => 'varchar(100)',
                    'isRequired'  => true,
                    'description' => 'Почта для обратного адреса',
                ],
            ],
        ]) ?>


        <?= \common\services\documentation\DbTable::widget([
            'name'    => 'school_potok_subscribe',
            'columns' => [
                [
                    'name'        => 'id',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Идентификатор рассылки',
                ],
                [
                    'name'        => 'potok_id',
                    'type'        => 'integer',
                    'isRequired'  => true,
                    'description' => 'Идентификатор потока',
                ],
                [
                    'name'        => 'subject',
                    'type'        => 'varchar(255)',
                    'isRequired'  => true,
                    'description' => 'Тема письма',
                ],
                [
                    'name'        => 'content',
                    'type'        => 'text',
                    'isRequired'  => true,
                    'description' => 'Содержание письма',
                ],
                [
                    'name'        => 'created_at',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Момент создания',
                ],
            ],
        ]) ?>
        <?= \common\services\documentation\DbTable::widget([
            'name'        => 'school_potok_subscribe_item',
            'description' => 'Запущенный процесс рассылки',
            'model'       => '\common\models\school\SubscribeItem',
            'columns'     => [
                [
                    'name'        => 'id',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Идентификатор процесса',
                ],
                [
                    'name'        => 'subscribe_id',
                    'type'        => 'integer',
                    'isRequired'  => true,
                    'description' => 'Идентификатор подготовленной рассылки',
                ],
                [
                    'name'        => 'gs_subscribe_id',
                    'type'        => 'integer',
                    'isRequired'  => false,
                    'description' => 'Идентификатор запущенной рассылки',
                ],
                [
                    'name'        => 'created_at',
                    'type'        => 'int',
                    'isRequired'  => false,
                    'description' => 'Момент создания',
                ],
                [
                    'name'        => 'unsubscribe_count',
                    'type'        => 'int',
                    'isRequired'  => false,
                    'description' => 'Счетчик отписок',
                ],
                [
                    'name'        => 'views_count',
                    'type'        => 'int',
                    'isRequired'  => false,
                    'description' => 'Счетчик просмотров/открытий',
                ],
                [
                    'name'        => 'email_count',
                    'type'        => 'int',
                    'isRequired'  => false,
                    'description' => 'Кол-во подписчиков которым делается рассылка',
                ],
            ],
        ]) ?>
        <p>Еще нужна статистика как прошла рассылка, а для этого нужно сделать дополнительный параметр <code>school_potok_subscribe_item.count</code>.
            Туда буду записывать коль-во email которое сформировано для рассылки. И тогда я смогу сравнивать с имеющимся
            списком в таблице <code>gs_subscribe_mails</code></p>
        <p>Еще нужен параметр сколько письмо открыли. Для этого делаю пиксель в письме и он будет считать кол-во
            открытий. Кол-во открытий можно считать в параметре <code>school_potok_subscribe_item.opened</code>. А
            таблицу открытий можно вести в таблице <code>school_potok_subscribe_item_open</code></p>

        <h2 class="page-header">Обратный адрес</h2>
        <p>Обратный адрес находится в полях <code>school.subscribe_from_name</code> и
            <code>school.subscribe_from_email</code>.</p>

        <h2 class="page-header">Картинка для писмьма</h2>
        <p>Картинка для писмьма находится в поле <code>school.subscribe_image</code>.</p>

        <h2 class="page-header">Консоль</h2>
        <p>Рассылкой писем занимается консольный контроллер <code>\console\controllers\SubscribeController</code>. Каждую минуту вызывается консольная команда.</p>
        <pre>php yii subscribe/send</pre>
        <p>Она рассылает по 8 писем за проход. Эта цифра взята из ограничений почтовика timeweb в 500 писем в час и 2000 в день.</p>
        <p>Параметр который хранит счетчик хранится в таблице <code>config</code>. Параметр называется <code>mailCounterPerDay</code>.</p>

        <p>Если я указываю обратным адресом другой отличный от того на который настроен SMTP то возникает ошибка</p>
        <pre>Не удалось доствить: dram1008@yandex.ru Expected response code 354 but got code "503", with message "503-All RCPT commands were rejected with this error:
503-Disabled
503 Valid RCPT command must precede DATA</pre>

        <p>При генерации письма делается подствновка в поле <code>{{email}}</code> самого email кому отправляется письмо.</p>

        <h2 class="page-header">Рассылка в участниках</h2>
        <p>Эта рассылка работает также как и рассылка в потоке, только рассылка по всем подписчикам школы происходит.</p>

        <h2 class="page-header">Отписка</h2>
        <p><a href="https://www.i-am-avatar.com/cabinet-school-task-list/view?id=204">https://www.i-am-avatar.com/cabinet-school-task-list/view?id=204</a></p>
        <p><a href="https://www.i-am-avatar.com/cabinet-school-task-list/view?id=225">https://www.i-am-avatar.com/cabinet-school-task-list/view?id=225</a></p>
        <p>Защита отписки: добавляю поле hash <code>password_hash('iAvatar777', PASSWORD_BCRYPT)</code>. А проверяю <code>password_verify('iAvatar777',$hash);</code></p>
        <p><code>school_potok_subscribe_item.unsubscribe_count</code> Считает кол-во отписок.</p>


        <h3 class="page-header">Отписка</h3>
        <p>а где формируется ссылка на отписку?</p>
        <p>Например в файле <code>avatar-bank/mail/html/subscribe/manual2.php</code> формируется ссылка с параметрами potok_id sid</p>
        <pre>$url = '{{link}}' . $url->query . '?'</pre>
        <p>Далее когда происходит сама рассылка из очереди то там производится подстановка</p>
        <pre>$pass = \Yii::$app->params['subscribe_unsubscribe_pass'];
$query = 'sid' . '=' . $sid . '&' . 'email' . '=' . urlencode($mail) . '&' . 'password' . '=' . urlencode($pass);
$hash = md5($query);
$url2 = Url::to(['subscribe/unsubscribe3', 'sid' => $sid, 'email' => $mail, 'hash' => $hash], true);
$html = str_replace('{{link}}' . $query1 . '?', $url2, $html);</pre>

        <h3 class="page-header">Обработчик отписки</h3>
        <p>/subscribe/unsubscribe3</p>
        <p><code>potok_id</code></p>
        <p><code>email</code></p>
        <p><code>hash</code></p>
        <p><code>sid</code> - school_potok_subscribe_item.id</p>

        <h2 class="page-header">Рассылка в поток</h2>
        <p><code>subscribe/manual2</code> - шаблон для рассылки. Пока там еще есть контактные данные Аватара. Но скоро уберем. Сейчас этот шаблон выводит картинку школы которая указана в насстройках как картинка. И выводится ссылка на отписку с учетом <code>school_potok_subscribe_item.id</code>. В ссылке на отписку присутствует шаблон <code>{{email}}</code> который заменяется при отправке.</p>

        <h2 class="page-header">Рассылка урока</h2>
        <p>Надо расписать.</p>

        <h2 class="page-header">Рассылка Блог</h2>
        <p><img src="/images/controller/admin-developer/school-subscribe/2019-03-25_03-03-27.png" class="thumbnail" width="400"></p>
        <p>Шаблон <code>layouts/html/subscribe</code>, <code>subscribe/blog-item</code></p>

        <h2 class="page-header">Рассылка Новости</h2>
        <p>Шаблон <code>layouts/html/subscribe</code>, <code>subscribe/news-item</code></p>

        <h2 class="page-header">Подстчет открытий писем</h2>
        <p><code>school_potok_subscribe_item.views_count</code> это поле сохраняет.</p>
        <p>Куда будет вести ссылка которая будет открывать пиксель в письме который будет подсчитывать открытия?</p>
        <p><code>subscribe/pixel</code>. Какие параметры? school_potok_subscribe_item.id. Думаю хватит.</p>


        <h2 class="page-header">Флаги для пользователей на разные виды событий</h2>
        <?= \common\services\documentation\DbTable::widget([
            'name'        => 'user_subscribe',
            'description' => 'Флаги для пользователей на разные виды событий',
            'model'       => '\common\models\subscribe\UserSubscribe',
            'columns'     => [
                [
                    'name'        => 'id',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Идентификатор процесса',
                ],
                [
                    'name'        => 'user_id',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Идентификатор пользователя',
                ],
                [
                    'name'        => 'action_id',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Идентификатор события user_subscribe_action.id',
                ],
                [
                    'name'        => 'is_mail',
                    'type'        => 'tinyint',
                    'isRequired'  => true,
                    'description' => 'Флаг "Есть подписка на уведомления в виде почты?" 0 - нет подписки, 1 - есть подписка. 0 - по умолчанию',
                ],
                [
                    'name'        => 'is_telegram',
                    'type'        => 'tinyint',
                    'isRequired'  => true,
                    'description' => 'Флаг "Есть подписка на уведомления в виде telegram?" 0 - нет подписки, 1 - есть подписка. 0 - по умолчанию',
                ],
            ],
        ]) ?>


        <h2 class="page-header">Виды событий</h2>
        <?= \common\services\documentation\DbTable::widget([
            'name'        => 'user_subscribe_action',
            'description' => 'Виды событий',
            'model'       => '\common\models\subscribe\UserSubscribeAction',
            'columns'     => [
                [
                    'name'        => 'id',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Идентификатор процесса',
                ],
                [
                    'name'        => 'name',
                    'type'        => 'varchar(100)',
                    'isRequired'  => true,
                    'description' => 'Наименование события',
                ],
            ],
        ]) ?>

        <p>Сделать раздел рассылка в профиле пользователя: https://www.i-am-avatar.com/cabinet-school-task-list/view?id=357</p>


        <pre>При добавлении в БД в тело письма вставляется ссылка href=”{{link}}sid=2&potok_id=3?”
Знак вопроса завершает строку запроса. Так можно распарсить параметры для формирования ссылки отписки.
При отсылке письма в письмо производится подстановка. Формируется \cs\helpers\Url. Это будет url1. Добавляется параметр email. Делается дубликат url2
Для url2 добавляется параметр test=iAvatar777
Вычисляется hash=md5(queryString(url2)).
Параметр hash добавляется в url1.
Ссылка для отписки готова.
Пример кода:</pre>
    </div>
</div>

