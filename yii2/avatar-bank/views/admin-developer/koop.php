<?php

/** $this \yii\web\View  */

use yii\helpers\Url;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;

$this->title = 'Кооператив';


?>

<div class="container">
    <div class="col-lg-12">
        <h1 class="page-header"><?= Html::encode($this->title) ?></h1>

        <h2 class="page-header">Настройки</h2>

        <?= \common\services\documentation\DbTable::widget([
            'name'        => 'school_koop',
            'model'       => '\avatar\models\validate\SchoolKoop',
            'description' => 'Данные о кооперативе',
            'columns'     => [
                [
                    'name'        => 'id',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Идентификатор записи',
                ],
                [
                    'name'        => 'inn',
                    'type'        => 'varchar(12)',
                    'isRequired'  => true,
                    'description' => 'ИНН',
                ],
                [
                    'name'        => 'email',
                    'type'        => 'varchar(100)',
                    'isRequired'  => true,
                    'description' => 'Адрес эл.почты',
                ],
                [
                    'name'        => 'name',
                    'type'        => 'varchar(100)',
                    'isRequired'  => false,
                    'description' => 'Наименование',
                ],
                [
                    'name'        => 'address_isp',
                    'type'        => 'varchar(100)',
                    'isRequired'  => false,
                    'description' => 'Адрес исп.органа',
                ],
                [
                    'name'        => 'phone',
                    'type'        => 'varchar(12)',
                    'isRequired'  => false,
                    'description' => 'Номер телефона',
                ],
                [
                    'name'        => 'okved',
                    'type'        => 'varchar(10)',
                    'isRequired'  => false,
                    'description' => 'ОКВЭД',
                ],
                [
                    'name'        => 'okved_dop',
                    'type'        => 'varchar(1000)',
                    'isRequired'  => false,
                    'description' => 'дополнительные ОКВЭД',
                ],
                [
                    'name'        => 'school_id',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Идентификатор школы',
                ],
                [
                    'name'        => 'registration_is_video_verify',
                    'type'        => 'int',
                    'isRequired'  => false,
                    'description' => 'Флаг нужна видео верификация при регистрации? 0 - нет, 1 - да, по умолчанию - 0',
                ],
            ],
        ]) ?>


        <h2 class="page-header">Настройки</h2>
        <p>
            <a href="https://app.diagrams.net/#G1R350WVBE1L3xk_gPP11W91uw3QCS4g_I" target="_blank">
                <img src="/images/controller/admin-developer/koop/reg.png" class="thumbnail" width="100%">
            </a>
        </p>
        <p>
            <a href="https://cloud1.cloud999.ru/upload/cloud/15878/48383_94TPzs8D2q.pdf">
                Заявление о вступлении
            </a>
        </p>
        <p>
            <a href="https://cloud1.cloud999.ru/upload/cloud/15878/49184_zW0lJT5vYu.pdf">
                Здраво
            </a>
        </p>
        <p>
            <a href="https://cloud1.cloud999.ru/upload/cloud/15878/49237_phgNPiWJUm.pdf">
                ЗДРАВО Положение о кооп участке
            </a>
        </p>
        <p>
            <a href="https://cloud1.cloud999.ru/upload/cloud/15878/49274_lxaZMyBnbF.pdf">
                ПК ЗДРАВО
            </a>
        </p>
        <p>
            <a href="https://cloud1.cloud999.ru/upload/cloud/15878/49307_Dw6F4vCKNo.pdf">
                Процессы ЗДРАВО
            </a>
        </p>
        <p>
            <a href="https://cloud1.cloud999.ru/upload/cloud/15878/49334_2mIbvFD4Bc.pdf">
                Финансовая модель ЗДРАВО
            </a>
        </p>
        <p>
            <a href="https://cloud1.cloud999.ru/upload/cloud/15898/87934_WSMErIbgwt.pdf">
                Интеграция с 1С
            </a>
        </p>
        <p>
            <a href="https://cloud1.cloud999.ru/upload/cloud/15898/88435_GQcbCpojhr.pdf">
                <img src="/images/controller/admin-developer/koop/mind.png" class="thumbnail" width="100%">
            </a>
        </p>

        <h2 class="page-header">Верификация</h2>

        <p>Для того чтобы принять заявку нужно пройти процедуру верификации. Верификация это когда один человек подтверждает данные паспорта другого. Обычно делается под запись. Значит нужна следующая таблица:</p>

        <?= \common\services\documentation\DbTable::widget([
            'name'        => 'koop_user_verification',
            'model'       => '\common\models\koop\UserVerification',
            'description' => 'Данные о подтверждении личности',
            'columns'     => [
                [
                    'name'        => 'id',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Идентификатор записи',
                ],
                [
                    'name'        => 'school_id',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Идентификатор школы, какой принадлежит заявка',
                ],
                [
                    'name'        => 'user1_id',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Идентификатор пользователя который проводит верификацию',
                ],
                [
                    'name'        => 'user2_id',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Идентификатор пользователя которого верификацируют',
                ],
                [
                    'name'        => 'created_at',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Момент создания заявки',
                ],
                [
                    'name'        => 'public_key1',
                    'type'        => 'varchar(100)',
                    'isRequired'  => true,
                    'description' => 'Публичный ключ первого пользователя',
                ],
                [
                    'name'        => 'public_key2',
                    'type'        => 'varchar(100)',
                    'isRequired'  => true,
                    'description' => 'Публичный ключ второго пользователя',
                ],
                [
                    'name'        => 'hash',
                    'type'        => 'varchar(100)',
                    'isRequired'  => true,
                    'description' => 'Хеш набора данных для верификации по алгоритму sha256',
                ],
                [
                    'name'        => 'signature1',
                    'type'        => 'varchar(100)',
                    'isRequired'  => true,
                    'description' => 'Подпись первого пользователя набора данных для верификации',
                ],
                [
                    'name'        => 'signature2',
                    'type'        => 'varchar(100)',
                    'isRequired'  => true,
                    'description' => 'Подпись второго пользователя набора данных для верификации',
                ],
                [
                    'name'        => 'file',
                    'type'        => 'varchar(1000)',
                    'isRequired'  => true,
                    'description' => 'Данные о видео общего созвона',
                ],
            ],
        ]) ?>


        <?= \common\services\documentation\DbTable::widget([
            'name'        => 'user_enter',
            'model'       => '\common\models\UserEnter',
            'description' => 'Данные регистрации заявки',
            'columns'     => [
                [
                    'name'        => 'id',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Идентификатор записи',
                ],
                [
                    'name'        => 'email',
                    'type'        => 'varchar(100)',
                    'isRequired'  => true,
                    'description' => 'Почта регистрирующегося',
                ],
                [
                    'name'        => 'iof',
                    'type'        => 'varchar(150)',
                    'isRequired'  => false,
                    'description' => 'ИОФ',
                ],
                [
                    'name'        => 'pay_id',
                    'type'        => 'varchar(100)',
                    'isRequired'  => false,
                    'description' => 'Идентификатор платежа',
                ],
                [
                    'name'        => 'is_payd',
                    'type'        => 'tinyint',
                    'isRequired'  => false,
                    'description' => 'Флаг оплачено? 0 - нет, 1 - да, по умолчанию - 0',
                ],
                [
                    'name'        => 'is_paid_client',
                    'type'        => 'tinyint',
                    'isRequired'  => false,
                    'description' => 'Флаг оплачено по словам клиента? 0 - нет, 1 - да, по умолчанию - 0',
                ],
            ],
        ]) ?>

        <p>Набор данных для верификации следующий:</p>

        <ul>
            <li>user_enter.id</li>
            <li>user_enter.user1_id</li>
            <li>user_enter.user2_id</li>
            <li>user_enter.public_key1</li>
            <li>user_enter.public_key2</li>
            <li>user_enter.created_at</li>
            <li>user_enter.file</li>
            <li>user_enter.name_first</li>
            <li>user_enter.name_last</li>
            <li>user_enter.name_middle</li>
            <li>user_enter.address_pochta</li>
            <li>user_enter.phone</li>
            <li>user_enter.passport_ser</li>
            <li>user_enter.passport_number</li>
            <li>user_enter.passport_vidan_kem</li>
            <li>user_enter.passport_vidan_date</li>
            <li>user_enter.passport_vidan_num</li>
            <li>user_enter.passport_born_date</li>
            <li>user_enter.passport_born_place</li>
            <li>user_enter.passport_scan1</li>
            <li>user_enter.passport_scan2</li>
            <li>user_enter.school_id</li>
        </ul>

        <p>Эти данные собираются в строку и вычиляется <code>hash</code> по алгоритму <code>sha256</code>. Далее этот <code>hash</code> подписывается числовой подписью при помощи библиотеки <a href="https://github.com/BitcoinPHP/BitcoinECDSA.php" target="_blank">https://github.com/BitcoinPHP/BitcoinECDSA.php</a> обоими пользователями и сохраняется в <code>user_verification.signature1</code> и <code>user_verification.signature2</code>.</p>
        <p><b>Порядок действий следующий:</b></p>
        <ul>
            <li>Созвониться под запись с тем кто проходит верификацию</li>
            <li>Запись сохранить в облаке или на YouTube</li>
            <li>Валидатор создает запись и указывает второго участника и записанный файл.</li>
            <li>Валидатор делает числовую подпись.</li>
            <li>Валидатор скидывает ссылку на подпись тому кто проходит верификацию.</li>
            <li>Второй участник открывает ссылку, проверяет данные и ставит числовую подпись.</li>
        </ul>

        <p>Верификация проходит в <code>/cabinet-school-koop/verify</code>.</p>
        <p>Оплата происходит в <code>/cabinet-school-koop/pay</code>.</p>

        <h2 class="page-header">Оплата членского взноса</h2>
        <p>Данные о платежной системе берутся из платежной школы</p>

        <p>
            <a href="https://app.diagrams.net/#G1K5LzrSxzxEXnLU-TQQHnfBxblPC3dIh9" target="_blank">
                <img src="/images/controller/admin-developer/koop/YandexKass.png">
            </a>
        </p>

    </div>
</div>

