<?php

/** $this \yii\web\View  */

use yii\helpers\Url;
use app\services\GsssHtml;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use app\services\GetArticle\Collection;

$this->title = 'Карта Аватара';


?>




<div class="container">
    <div class="col-lg-12">

        <h1 class="page-header"><?= Html::encode($this->title) ?></h1>

        <h2 class="page-header">Предназначение</h2>

        <p>Для хранения монет.</p>
        <p>Для перебрасывания монет.</p>
        <p>Для оплаты в интернет-магазине во вселенной ЯАватар.</p>
        <p>Для прохода на событие ЯАватар при проверке через мобильное приложение ЯАватар.</p>
        <p>Для идентификации пользователя. QR код сзади является идентификатором Аватара.</p>
        <p>Для партнерской программы. QR код перенаправляет на покупку карты по реферальной ссылке.</p>

        <h2 class="page-header">Описание</h2>
        <p>На карте расположен QR код со ссылкой <a href="https://www.i-am-avatar.com/partner?card=################" target="_blank">https://www.i-am-avatar.com/partner?card=################</a>.</p>

        <p>Если карта свободная то идет редирект на Активацию карты <code>'https://www.i-am-avatar.com/qr/enter?card=' . $card</code><br>
Если карта привязанная то идет редирект на покупку карты <code>'http://galaxysss.ru/shop/category/20?card=' . $card</code></p>

        <p>Номер карты состоит из 16 цифр.</p>
        <p>БИН 8999</p>
        <p>Код валюты 09 означает что на QR коде номер карты.</p>

        <h2 class="page-header">NFC</h2>

        <p>Записан номер карты.</p>

        <h2 class="page-header">Модели поведения с картами</h2>

        <p>1. Человек купил карту в интернет магазине без Эликсиров.</p>
        <p>2. Человек купил карту в интернет магазине с Эликсирами.</p>
        <p>3. Человек купил карту по партнерской ссылке с Эликсирами</p>
        <p>4. Человек купил карту у человека с рук с Эликсирами и начинает ее активировать в на сайте Аватара (аккаунта у него нет на Аватаре).</p>
        <p>5. Человек купил карту у человека с рук с Эликсирами и начинает ее активировать в на сайте Аватара (аккаунт уже есть).</p>

        <h2 class="page-header">Вопросы</h2>
        <p>А можно ли человеку две карты иметь? По хорошему да. Для начала 0 или 1.</p>
        <p>А можно ли человеку несколько счетов Золотого Эликсира иметь? База данных позволяет, интерфейс предполагает сейчас что все на одном месте</p>
        <p>А можно ли на карте иметь несколько счетов Золотого Эликсира? Нет, на карте только по одному счету на одну монету.</p>


        <h2 class="page-header">Задачи</h2>
        <p>Добавить карты в платформу https://www.i-am-avatar.com/cabinet-school-task-list/view?id=264</p>
        <p>base58 https://www.darklaunch.com/base58-encode-and-decode-using-php-with-example-base58-encode-base58-decode </p>
        <p>base56 https://gist.github.com/csnover/1110664</p>

        <h2 class="page-header">Сканирование QR кода</h2>
        <p>Если карта не зарегистрирована то идет редирект на страницу регистрации карты, если пользователь уже авторизован то присоединение карты.</p>
        <p>Если карта зарегистрирована, то учитывается настрока <code>user.card_action</code> что делать.</p>
        <p>1 - открыть свой профиль</p>
        <p>2 - открыть карту</p>
        <p>3 - открыть свой профиль + карту</p>
        <p>4 - перекинуть на покупку карты по моей реферальной ссылке</p>
        <p>5 - Спросить что делать</p>
        <p>6 - Быстрая покупка</p>
        <p>Настрока <code>user.card_action</code> Задается в профиле <code>/cabinet/card-action</code>.</p>

        <h3 class="page-header">Карта не занята (свободна)</h3>
        <p>При сканировании происходит переход на регистрацию</p>

        <h3 class="page-header">Карта школы и быстрая покупка</h3>
        <?= \common\services\documentation\DbTable::widget([
            'name'        => 'card_school_link',
            'model'       => '\common\models\CardSchoolLink',
            'description' => 'Ссыка карты и школы',
            'params'      => [
                [
                    'name'        => 'id',
                    'isRequired'  => true,
                    'description' => 'Идентификатор ссылки',
                    'type'        => 'int',
                ],
                [
                    'name'        => 'card_id',
                    'description' => 'Идентификатор каты',
                    'type'        => 'int',
                ],
                [
                    'name'        => 'school_id',
                    'description' => 'Идентификатор школы',
                    'type'        => 'int',
                ],
            ]
        ]) ?>
        <pre>/shop/card
сюда буду перекидывать тех кто по карте.
Но вопрос как определить к какой школе прикреплена карта?
нужна таблица присоединения
а может ли одна карта принадлежать нескольким школам, теоретически да но для редиректа нужна только одна привязка
где будет задаваться привязка карты?
в меню настроек карты "как действовать".

Если карта активируется из школы то она сразу туда и привязывается

Можно сделать так чтобы можно было привязать к нескольким школам
и предлагать при редиректе на выбор
а если привязка только к одной школе то сразу редирект по умолчанию

да актуально
значит деалаю соотношение многие ко многим
в настройке можно будет выбрать к какой школе присоединяется карта, если ничего не задано значит Аватар</pre>
        <p>/shop/card - быстрая покупка</p>
        <p>Если к карте привязано несколько школ то выбор будет здесь <code>https://www.i-am-avatar.com/card/ask-quick</code></p>

        <pre>Сделал разделение по счетам
К категории привязывается настройка платежной системы (в которой прописан счет перечислений)
К категории привязываются товары

Деньги списываются при быстрой покупке
со счета на карте (GU) (счет должен быть заранее там прикреплен)

При активации карты в кабинете гуслицы карта привязывается сама к гуслице</pre>
    </div>
</div>

