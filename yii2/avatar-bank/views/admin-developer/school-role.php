<?php

/** $this \yii\web\View  */

use yii\helpers\Url;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;

$this->title = 'Роли';


?>

<div class="container">
    <div class="col-lg-12">
        <h1 class="page-header"><?= Html::encode($this->title) ?></h1>

        <?= \common\services\documentation\DbTable::widget([
            'name'        => 'school_role',
            'description' => 'Роль',
            'model'       => '\common\models\school\Role',
            'columns'     => [
                [
                    'name'        => 'id',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Идентификатор элемента',
                ],
                [
                    'name'        => 'school_id',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Идентификатор школы',
                ],
                [
                    'name'        => 'name',
                    'type'        => 'varchar(255)',
                    'isRequired'  => true,
                    'description' => 'Имя',
                ],

            ],
        ]) ?>

        <?= \common\services\documentation\DbTable::widget([
            'name'        => 'school_role_link',
            'description' => 'Роль-Человек ссылка',
            'model'       => '\common\models\school\RoleLink',
            'columns'     => [
                [
                    'name'        => 'id',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Идентификатор элемента',
                ],
                [
                    'name'        => 'school_id',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Идентификатор школы',
                ],
                [
                    'name'        => 'role_id',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Идентификатор роли',
                ],
                [
                    'name'        => 'user_id',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Идентификатор пользователя',
                ],


            ],
        ]) ?>

        <p>После добавления анкеты школы <code>/avatar-bank/views/anketa/new-school.php</code> для роли
            <code>role_admin</code> выслылается письмо <code>/avatar-bank/mail/html/new_anketa-school.php</code>.</p>

        <p>После добавления анкеты <code>/avatar-bank/views/anketa/new.php</code> для роли
            <code>role_admin_command</code> выслылается письмо <code>/avatar-bank/mail/html/new_anketa.php</code>.</p>


    </div>
</div>

