<?php

/** $this \yii\web\View  */

use yii\helpers\Url;
use app\services\GsssHtml;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use app\services\GetArticle\Collection;

$this->title = 'Сервера и хостеры';


?>

<div class="container">
    <div class="col-lg-12">
        <h1 class="page-header"><?= Html::encode($this->title) ?></h1>

        <p><a href="https://contabo.com/" target="_blank">https://contabo.com/</a></p>

        <p><a href="https://www.arubacloud.com/" target="_blank">https://www.arubacloud.com/</a></p>

    </div>
</div>

