<?php

/** $this \yii\web\View  */
/** @var $Potok  \common\models\school\Potok  */
/** @var $school \common\models\school\School */

use yii\helpers\Url;
use app\services\GsssHtml;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use app\services\GetArticle\Collection;

$this->title = 'Все уроки';


?>

<div class="container">
    <div class="col-lg-12">
        <h1 class="page-header"><?= Html::encode($this->title) ?></h1>
    </div>

    <div class="col-lg-12">
        <?php \yii\widgets\Pjax::begin(); ?>
        <?php
        $this->registerJS(<<<JS

$('[data-toggle="tooltip"]').tooltip();

$('.rowTable').click(function() {
    window.location = '/cabinet-kurs-list/lesson-view' + '?' + 'id' + '=' + $(this).data('id');
});

JS
        );
        ?>
        <?= \yii\grid\GridView::widget([
            'dataProvider' => new \yii\data\ActiveDataProvider([
                'query'      => \common\models\school\Lesson::find()
                    ->where(['school_kurs_lesson.kurs_id' => $Potok->getKurs()->id])
                    ->innerJoin('school_urok_potok_state', 'school_urok_potok_state.lesson_id = school_kurs_lesson.id and school_urok_potok_state.potok_id = ' . $Potok->id)
                    ->select([
                        'school_kurs_lesson.*',
                        'school_urok_potok_state.id as state_id',
                    ])
                    ->asArray()
                ,
                'pagination' => [
                    'pageSize' => 20,
                ],
            ]),
            'tableOptions' => [
                'class' => 'table table-striped table-hover',
            ],
            'rowOptions'   => function ($item) {
                $data = [
                    'data'  => ['id' => $item['state_id']],
                    'role'  => 'button',
                    'class' => 'rowTable',
                ];
                return $data;
            },
            'columns'      => [
                'id',
                [
                    'header'  => 'Картинка',
                    'content' => function ($item) {
                        $i = ArrayHelper::getValue($item, 'image', '');
                        if ($i == '') return '';

                        return Html::img(
                            \common\widgets\FileUpload7\FileUpload::getFile($i, 'crop'),
                            [
                                'class'  => "thumbnail",
                                'width'  => 80,
                                'height' => 80,
                                'style'  => 'margin-bottom: 0px;',
                            ]);
                    },
                ],
                'name',
                [
                    'header'  => 'Описание',
                    'content' => function ($item) {
                        $i = ArrayHelper::getValue($item, 'description', '');

                        return nl2br(Html::encode($i));
                    },
                ],
                [
                    'header'  => 'Тип',
                    'content' => function ($item) {
                        $i = ArrayHelper::getValue($item, 'type_id', '');
                        if ($i == '') return '-';
                        if ($i == 1) return 'Документ';
                        if ($i == 2) return 'Видео';
                        if ($i == 3) return 'Аудио';
                        return '';
                    },
                ],
                [
                    'header'  => 'Создано',
                    'content' => function ($item) {
                        $v = \yii\helpers\ArrayHelper::getValue($item, 'created_at', 0);
                        if ($v == 0) return '';

                        return Html::tag('abbr', \cs\services\DatePeriod::back($v, ['isShort' => true]), ['data' => ['toggle' => 'tooltip'], 'title' => Yii::$app->formatter->asDatetime($v)]);
                    },
                ],
            ],
        ]) ?>
        <?php \yii\widgets\Pjax::end(); ?>
    </div>


</div>
