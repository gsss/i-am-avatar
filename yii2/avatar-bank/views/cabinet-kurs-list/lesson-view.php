<?php

/** $this \yii\web\View  */
/** @var $kurs \common\models\school\Kurs  */
/** @var $lesson \common\models\school\Lesson  */

use yii\helpers\Url;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;

$this->title = $lesson->name;

?>

<div class="container">
    <div class="col-lg-12">
        <h1 class="page-header"><?= Html::encode($this->title) ?></h1>
    </div>

    <div class="col-lg-12">
        <?php if ($lesson->type_id == 2) {?>
            <?php // VideoYoutube ?>
            <?php $lessonVideo = \common\models\school\LessonVideo::findOne(['lesson_id' => $lesson->id]) ?>
            <p class="thumbnail"><iframe width="100%" height="315" src="https://www.youtube.com/embed/<?= $lessonVideo->video_id ?>" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe></p>
        <?php }?>

        <?php if ($lesson->type_id == 5) {?>
            <?php // Page ?>
            <?php $lessonPage = \common\models\school\LessonPage::findOne(['lesson_id' => $lesson->id]) ?>
            <p class="thumbnail">
                <img src="<?= \cs\Widget\FileUpload3\FileUpload::getOriginal($lesson->image) ?>" width="100%">
            </p>

            <p>
                <a href="<?= \common\models\school\Page::findOne($lessonPage->page_id)->getUrl() ?>" target="_blank" class="btn btn-success">
                    Перейти на страницу
                </a>
            </p>
        <?php }?>
        <?= \avatar\modules\Comment\Module::getComments2(2, $lesson->id); ?>

    </div> </div>
