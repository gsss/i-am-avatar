<?php

/** @var $this \yii\web\View */


use yii\helpers\Url;
use app\services\GsssHtml;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use app\services\GetArticle\Collection;

$this->title = 'Все курсы в которых я обучаюсь';


?>

<div class="container">
    <div class="col-lg-12">
        <h1 class="page-header"><?= Html::encode($this->title) ?></h1>
    </div>


    <div class="col-lg-12">

        <?php \yii\widgets\Pjax::begin(); ?>
        <?php
        $this->registerJS(<<<JS
$('[data-toggle="tooltip"]').tooltip();

$('.rowTable').click(function(e) {
    window.location = '/cabinet-kurs-list/lesson-list' + '?' + 'id' + '=' + $(this).data('id');
});

$('.buttonLessonList').click(function(e) {
    e.preventDefault();
    e.stopPropagation();
    window.location = '/cabinet-kurs-list/lesson-list' + '?' + 'id' + '=' + $(this).data('id');
});

JS
        );
        ?>
        <?php /** @var \common\models\UserAvatar $user */ ?>
        <?php $user = Yii::$app->user->identity; ?>
        <?= \yii\grid\GridView::widget([
            'dataProvider' => new \yii\data\ActiveDataProvider([
                'query' => \common\models\school\Kurs::find()
                    ->innerJoin('school_potok', 'school_potok.kurs_id = school_kurs.id')
                    ->innerJoin('school_potok_user_root_link', 'school_potok_user_root_link.potok_id = school_potok.id')
                    ->where(['school_potok_user_root_link.user_root_id' => $user->user_root_id])
                    ->andWhere(['school_kurs.status' => 2])
                    ->select([
                        'school_potok.id',
                        'school_kurs.name',
                        'school_kurs.image',
                        'school_potok.date_start',
                        'school_potok.date_end',
                        'school_potok.name as potok_name',
                    ])
                    ->asArray()
                ,
                'pagination' => [
                    'pageSize' => 20,
                ],
            ]),
            'tableOptions' => [
                'class' => 'table table-striped table-hover',
            ],
            'rowOptions' => function ($item) {
                $data = [
                    'data' => ['id' => $item['id']],
                    'role' => 'button',
                    'class' => 'rowTable'
                ];
                return $data;
            },
            'columns' => [
                'id',
                [
                    'header'  => 'Картинка',
                    'content' => function ($item) {
                        $i = ArrayHelper::getValue($item, 'image', '');
                        if ($i == '') return '';

                        return Html::img(\common\widgets\FileUpload7\FileUpload::getFile($i, 'crop'), [
                            'class'  => "img-circle",
                            'width'  => 60,
                            'height' => 60,
                            'style'  => 'margin-bottom: 0px;',
                        ]);
                    },
                ],
                'name:text:Курс',
                'potok_name:text:Поток/группа',
                'date_start:date:Старт',
                'date_end:date:Окончание',
                [
                    'header' => 'Уроки',
                    'content' => function ($item) {
                        return Html::button('Уроки', [
                            'class' => 'btn btn-info btn-xs buttonLessonList',
                            'data'  => [
                                'id' => $item['id'],
                            ]
                        ]);
                    }
                ],
            ],
        ]) ?>
        <?php \yii\widgets\Pjax::end(); ?>
    </div>

</div>

<div class="modal fade" id="modalInfo" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="myModalLabel">Информация</h4>
            </div>
            <div class="modal-body">
                Успешно!
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
            </div>
        </div>
    </div>
</div>