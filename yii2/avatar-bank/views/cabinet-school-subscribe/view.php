<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;
use app\models\UnionCategory;
use yii\db\Query;
use yii\helpers\ArrayHelper;

/** @var $this yii\web\View */
/** @var $form yii\bootstrap\ActiveForm */
/** @var $model \common\models\school\Subscribe */
/** @var $school \common\models\school\School */

$this->title = $model->subject;

?>
<div class="container">

    <?php \avatar\widgets\SchoolMenu::begin(['school' => $school]) ?>
    <h1 class="page-header"><?= Html::encode($this->title) ?></h1>


    <?= \yii\helpers\VarDumper::dumpAsString($model); ?>

    <?php ?>
    <?php \avatar\widgets\SchoolMenu::end() ?>


</div>
