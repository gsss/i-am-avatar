<?php
use common\services\UsersInCache;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;

/* @var $this yii\web\View */

$this->title = 'EOS';

?>
<div class="container">
    <div class="col-lg-12">
        <h1 class="page-header text-center"><?= Html::encode($this->title) ?></h1>


        <p><a href="https://golos.io/eos/@chebykin/eosdev-1-ustanovka-nody-eos">EOSDEV #1. Установка ноды EOS</a></p>
        <p><a href="https://golos.io/eos/@chebykin/eosdev-2-pishem-hello-world-smart-kontrakt-dlya-eos">EOSDEV #2. Пишем Hello World смарт-контракт для EOS</a></p>
        <p><a href="https://golos.io/eos/@chebykin/eosdev-3-obrabotka-vkhodyashikh-soobshenii-v-smart-kontrakte-eos">EOSDEV #3. Обработка входящих сообщений в смарт-контракте EOS</a></p>
        <p><a href="https://golos.io/eos/@chebykin/eosdev-4-sushnosti-v-blokchein-seti-eos">EOSDEV #4. Сущности в блокчейн сети EOS</a></p>
        <hr>
        <p><a href="https://steemit.com/eos/@blockchained/eos-in-a-box-nachnite-ispolzovat-eos-uzhe-segodnya-someguy123">EOS-In-A-Box – начните использовать EOS уже сегодня! (@someguy123)</a></p>

    </div>
</div>



