<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;
use app\models\UnionCategory;

/* @var $this yii\web\View */
/* @var $model \avatar\models\forms\CabinetTradeBinanceApiSet */

$this->title = Yii::t('c.dRUdxhFpJM', 'Установка Binance API keys');

?>
<div class="container">
    <div class="col-lg-12">
        <h1 class="page-header"><?= Html::encode($this->title) ?></h1>
    </div>
    <div class="col-lg-8">
        <?php if (Yii::$app->session->hasFlash('form')): ?>

            <div class="alert alert-success">
                <?= \Yii::t('c.dRUdxhFpJM', 'Успешно обновлено') ?>.
            </div>
            <p><a href="view" class="btn btn-success">Yii::t('c.dRUdxhFpJM', 'Просмотр портфеля')</a></p>

        <?php else: ?>

            <?php $form = ActiveForm::begin(); ?>

            <?= $form->field($model, 'api_key', ['inputOptions' => ['class' => 'form-control', 'autocomplete' => 'off']]) ?>
            <?= $form->field($model, 'api_secret')->passwordInput() ?>

            <hr class="featurette-divider">
            <div class="form-group">
                <?= Html::submitButton(Yii::t('c.dRUdxhFpJM', 'Обновить'), [
                    'class' => 'btn btn-default',
                    'name'  => 'contact-button',
                    'style' => 'width: 100%;',
                ]) ?>
            </div>
            <?php ActiveForm::end(); ?>

        <?php endif; ?>
    </div>
    <div class="col-lg-4">
        <?= $this->render('../cabinet/_menu') ?>
    </div>
</div>



