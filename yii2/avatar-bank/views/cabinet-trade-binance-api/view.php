<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;
use app\models\UnionCategory;

/* @var $this yii\web\View */

$this->title = Yii::t('c.dRUdxhFpJM', 'Просмотр портфеля Binance');

/** @var \common\models\UserAvatar $user */
$user = Yii::$app->user->identity;
try {
    $binanceApi = $user->getBinanceAPI();
} catch (Exception $e) {
    throw new Exception('Не установлено API для user=' . Yii::$app->user->id);
}
/**
 * [
 *  [
 *      'asset' => "BTC", // код валюты по классификатору Binance
 *      'free' => "4723846.89208129",
 *      'locked' => "0.00000000"
 *  ]
 * ]
 */
$accountData = $binanceApi->account();
$balances = $accountData['balances'];
$balancesNotNull = [];
foreach ($balances as $item) {
    if ($item['asset'] == 'ETF') continue;
    if ($item['free'] != 0 || $item['locked'] != 0) {
        $amount = $item['locked'] + $item['free'];
        $item['all'] = $amount;
        $currency = \common\models\avatar\Currency::findOne(['code_coinmarketcap' => $item['asset']]);
        if (is_null($currency)) {
            Yii::warning('not found ' . $item['asset'], 'avatar\avatar-bank\views\cabinet-trade-binance-api\view.php');
            continue;
        }

        $item['currency_id'] = $currency->id;
        try {
            $item['btc'] = \common\models\avatar\Currency::convert($amount, $currency->code, 'BTC');
        } catch (Exception $e) {
            $item['btc'] = null;
        }
        try {
            $item['usd'] = \common\models\avatar\Currency::convert($amount, $currency->code, 'USD');
        } catch (Exception $e) {
            $item['usd'] = null;
        }
        if ($item['usd'] >= 1) {
            $balancesNotNull[] = $item;
        }
    }
}

/**
 *
 * Здесь может быть две группы валют
 * 1. Для которых есть курс на AvNet
 * 2. Для которых нет курса на AvNet
 *
 * Для первой группы я могу вывести диаграмму в рублях
 * Для второй группы я могу вывести диаграмму в монетах
 *
 */
?>
<div class="container">
    <div class="col-lg-12">
        <h1 class="page-header"><?= Html::encode($this->title) ?></h1>
    </div>
    <div class="col-lg-8">
        <p>Key: <code><?= $binanceApi->apiKey ?></code></p>
        <p><a href="set" class="btn btn-primary"><?= \Yii::t('c.dRUdxhFpJM', 'Установить') ?></a></p>
        <hr>
        <?= \yii\grid\GridView::widget([
            'dataProvider' => new \yii\data\ArrayDataProvider([
                'allModels'  => $balancesNotNull,
                'sort' => [
                    'attributes' => ['asset', 'all' => ['default' => SORT_DESC], 'btc' => ['default' => SORT_DESC], 'usd' => ['default' => SORT_DESC]],
                ],
                'pagination' => [
                    'pageSize' => 100,
                ],
            ]),
            'tableOptions' => [
                'class' => 'table table-striped table-hover',
                'align' => 'center',
            ],
            'rowOptions'   => function ($item) {
                $data = [
                    'data'  => [
                            'currency_id' => $item['currency_id'],
                        ],
                    'role'  => 'button',
                    'class' => 'rowTable'
                ];
                return $data;
            },
            'columns'      => [
                [
                    'header'  => '',
                    'content' => function ($item) {
                        $currency = \yii\helpers\ArrayHelper::getValue($item, 'asset');
                        $currencyObject = \common\models\avatar\Currency::findOne(['code_coinmarketcap' => $currency]);
                        if (is_null($currencyObject)) {
                            throw new \yii\base\Exception('Не указана валюта у счета');
                        }
                        $src = $currencyObject->image;
                        $title = $currencyObject->title . ' '. '(' . $currencyObject->code . ')';
                        $imgOptions = [
                            'width' => 30,
                            'class' => 'img-circle',
                            'data'  => [
                                'toggle' => 'tooltip',
                                'title'  => $title,
                            ],
                        ];

                        return Html::img($src, $imgOptions);
                    },
                ],
                [
                    'label'    => Yii::t('c.dRUdxhFpJM', 'Валюта'),
                    'attribute' => 'asset',
                ],
                [
                    'label'         => Yii::t('c.dRUdxhFpJM', 'Монет'),
                    'attribute'      => 'all',
                    'contentOptions' => [
                        'style' => 'text-align:right;',
                    ],
                    'headerOptions'  => [
                        'style' => 'text-align:right;',
                    ],
                    'format'         => ['decimal', 8],
                ],
                [
                    'label'         => Yii::t('c.dRUdxhFpJM', 'Сумма') . ', BTC',
                    'contentOptions' => [
                        'style' => 'text-align:right;',
                    ],
                    'headerOptions'  => [
                        'style' => 'text-align:right;',
                    ],
                    'attribute'      => 'btc',
                    'format'         => ['decimal', 8],
                ],
                [
                    'label'         => Yii::t('c.dRUdxhFpJM', 'Сумма') . ', USD',
                    'contentOptions' => [
                        'style' => 'text-align:right;',
                    ],
                    'headerOptions'  => [
                        'style' => 'text-align:right;',
                    ],
                    'attribute'      => 'usd',
                    'format'         => ['decimal', 2],
                ],
            ],
        ]) ?>
        <?php
        $data = [];
        foreach ($balancesNotNull as $item) {
            $data[] = [
                'name' => $item['asset'],
                'y'    => $item['usd'],
            ];
        }
        ?>
        <?php \yii\widgets\Pjax::begin() ?>
        <?= \common\widgets\HighCharts\HighCharts::widget([
            'chartOptions' => [
                'chart' => [
                    'plotBackgroundColor' => null,
                    'plotBorderWidth'     => null,
                    'plotShadow'     => false,
                    'type'     => 'pie',
                ],
                'title' => [
                    'text' => 'График',
                ],
                'tooltip' => [
                    'pointFormat' => '{series.name}: <b>{point.percentage:.1f}%</b>',
                ],

                'plotOptions' => [
                    'pie' => [
                        'allowPointSelect' => true,
                        'cursor' => 'pointer',
                        'dataLabels' => [
                                'enabled' => true,
                                'format' => '<b>{point.name}</b>: {point.percentage:.1f} %',
                                'style' => [
                                    'color' => new \yii\web\JsExpression('(Highcharts.theme && Highcharts.theme.contrastTextColor) || \'black\'')
                                ],
                        ],
                    ],
                ],
                'series' => [
                    [
                        'name'         => 'balances',
                        'colorByPoint' => true,
                        'data'         => $data,
                    ],
                ],
            ],
        ]);

        ?>
        <?php \yii\widgets\Pjax::end() ?>
        <?php
        $sum = 0;
        foreach ($balancesNotNull as $item) {
            $sum += $item['usd'];
        }
        ?>
        <p class="text-center" style="font-size: 300%"><?= Yii::$app->formatter->asDecimal($sum, 2) ?> <code>USD</code></p>


        <?php
        $period = 24 * 30 * 12; // часов
        $rows = \common\models\statistic\UserBinanceStatisticItem::find()
            ->select([
                'time',
                'usd',
            ])
            ->where(['between', 'time', time() - 60 * 60 * $period, time()])
            ->andWhere(['user_id' => Yii::$app->user->id])
            ->all()
        ;
        $rowsJson = \yii\helpers\Json::encode($rows);
        $this->registerJs(<<<JS


Highcharts.setOptions({
    global: {
        timezoneOffset: -3 * 60
    }
});
var rows = {$rowsJson};
var newRows = [];
for(i = 0; i < rows.length; i++)
{
    var item = rows[i];
    newRows.push({
        x: new Date(item.time * 1000),
        y: item.usd
    });
}
        
$('.rowTable').click(function() {
    window.location = '/statistic/item' + '?' + 'id' + '=' + $(this).data('currency_id');
});


JS
        );
        ?>
        <?= \common\widgets\HighCharts\HighCharts::widget([
            'chartOptions' => [
                'chart' => [
                    'zoomType' => 'x',
                    'type'     => 'spline',
                ],
                'title' => [
                    'text' => Yii::t('c.dRUdxhFpJM', 'График'),
                ],
                'subtitle' => [
                    'text' => Yii::t('c.dRUdxhFpJM', 'Выделите область для изменения масштаба'),
                ],
                'xAxis' => [
                    'type' => 'datetime',
                ],
                'yAxis' => [
                    [
                        'title' => [
                            'text' => 'USD',
                        ],
                    ]
                ],
                'legend' => [
                    'enabled' => true
                ],
                'tooltip' => [
                    'crosshairs' => true,
                    'shared' => true,
                ],
                'plotOptions' => [
                    'series' => [
                        'turboThreshold' => 0,
                    ],
                ],
                'series' => [
                    [
                        'type' => 'spline',
                        'name' => 'USD',
                        'data' => new \yii\web\JsExpression('newRows'),
                    ],
                ],
            ],
        ]);

        ?>

    </div>
    <div class="col-lg-4">
        <?= $this->render('../cabinet/_menu') ?>
    </div>
</div>



