<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;
use yii\db\Query;
use yii\helpers\ArrayHelper;

/** @var $this yii\web\View */
/** @var $request \common\models\school\RequestInput */

$this->title = 'Оплата заявки #' . $request->id;


?>
    <div class="container">
        <div class="col-lg-12">
            <h1 class="page-header"><?= Html::encode($this->title) ?></h1>

            <p class="alert alert-success">Вам успешно начислены <?= Yii::$app->formatter->asDecimal($request->amount/100,2) ?> ELXGOLD.</p>
        </div>
    </div>

