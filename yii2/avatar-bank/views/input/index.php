<?php

/** @var $this \yii\web\View */


use yii\helpers\Url;
use app\services\GsssHtml;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use app\services\GetArticle\Collection;

$this->title = 'Пополнить кошелек платформы';


?>

<div class="container">
    <div class="col-lg-12">
        <h1 class="page-header text-center"><?= Html::encode($this->title) ?></h1>
    </div>


    <div class="col-lg-4 col-lg-offset-4">

        <?php
        $this->registerJs(<<<JS
var fBuy = function(e) {
    e.preventDefault();
    e.stopPropagation();
    var b = $(this);
    var bText = b.html();
    b.off('click');
    b.html($('<i>', {class: 'fa fa-spinner fa-spin fa-fw'}));
    b.attr('disabled', 'disabled');
    
    ajaxJson({
        url: '/cabinet-input/request-create',
        data: $('#formPay').serializeArray(),
        success: function(ret) {
            window.location = '/cabinet-input/pay?id=' + ret.id;
        },
        errorScript: function(ret) {
            switch (ret.id) {
                case 102:
                    var f = $('#formPay');
                    for (var key in ret.data) {
                        if (ret.data.hasOwnProperty(key)) {
                            var name = key;
                            var value = ret.data[key];
                            var t = f.find('.field-cabinetinputrequestcreate-' + name);
                            t.addClass('has-error');
                            t.find('p.help-block-error').html(value.join('<br>')).show();
                        }
                    }
                    break;
            }
            var b = $('.buttonBuy');
            b.off('click');
            b.on('click', fBuy);
            b.html(bText);
            b.removeAttr('disabled');
        }
    })
};

$('.buttonBuy').click(fBuy);


$('#formPay .form-control').on('focus', function() {
    var o = $(this);
    var p = o.parent().parent();
    
    p.find('.form-group').each(function(i, e) {
        $(e).removeClass('has-error');
        $(e).find('p.help-block-error').hide();
    });
});
JS
        )
        ?>
        <?php
        $model = new \avatar\models\validate\CabinetInputRequestCreate();
        $form = \yii\bootstrap\ActiveForm::begin([
            'id'                 => 'formPay',
            'options'            => ['enctype' => 'multipart/form-data'],
            'enableClientScript' => false,
        ]);
        ?>
        <?= $form->field($model, 'amount')->label('Кол-во покупаемых монет') ?>
        <?= $form->field($model, 'ps_config_id')->widget('\avatar\widgets\PaySystemList', [
            'rows' => \common\models\PaySystemConfig::find()
                ->innerJoin('paysystems', 'paysystems_config.paysystem_id = paysystems.id')
                ->where(['paysystems_config.school_id' => 2])
                ->andWhere(['paysystems.currency' => 'RUB'])
                ->andWhere(['paysystems_config.is_hide' => 0])
        ])->label('Платежная система') ?>

        <hr>
        <?php \yii\bootstrap\ActiveForm::end(); ?>
        <div class="form-group">
            <?= Html::button('Купить', [
                'class' => 'btn btn-default buttonBuy',
                'name'  => 'contact-button',
                'style' => 'width:100%',
            ]) ?>
        </div>


    </div>

</div>

<div class="modal fade" id="modalInfo" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="myModalLabel">Информация</h4>
            </div>
            <div class="modal-body">
                Успешно!
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
            </div>
        </div>
    </div>
</div>