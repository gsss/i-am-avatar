<?php

/** @var $this \yii\web\View  */
/** @var $school \common\models\school\School  */
/** @var $page \common\models\school\Page  */

use yii\helpers\Url;
use app\services\GsssHtml;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use app\services\GetArticle\Collection;

$this->title = 'Все категории. Сортировка';

$items = \common\models\school\PageBlockCategory::find()
    ->orderBy(['sort_index' => SORT_ASC])
    ->all();
$arr = [];
$id1 = \cs\services\Security::generateRandomString();
foreach ($items as $item) {
    $arr[] = ['content' => Html::tag('span', $item['name'], ['data-id' => $item['id'], 'class' => 'sortable_' . $id1])];
}
$items = $arr;

?>

<div class="container">
    <div class="col-lg-12">
        <h1 class="page-header"><?= Html::encode($this->title) ?></h1>
    </div>

    <div class="col-lg-8">


        <?= \kartik\sortable\Sortable::widget([
            'showHandle'   => true,
            'pluginEvents' => [
                'sortupdate' => <<<JS
function(e, ui) {
    var i;
    var newIds = [];
    for(i = 0; i < e.originalEvent.detail.origin.items.length; i++) {
        $(e.originalEvent.detail.origin.items[i]).each(function(i,v) {
            $(v).find('span.sortable_' + '{$id1}').each(function(i2,v2) {
                newIds.push($(v2).data('id'));
            });
        });
    }
    ajaxJson({
        url: '/admin-school-page-category/sort-ajax',
        data: {
            ids: newIds
        },
        success: function(ret) {
            alert('Удачно');
        }
    });
}
JS
                ,
            ],
            'items'        => $items
        ]); ?>
    </div>

</div>

<div class="modal fade" id="modalInfo" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="myModalLabel">Информация</h4>
            </div>
            <div class="modal-body">
                Успешно!
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
            </div>
        </div>
    </div>
</div>