<?php

/** @var $this \yii\web\View */
/** @var $school \common\models\school\School */

/** @var $kurs \common\models\school\Kurs */

use yii\helpers\Url;
use app\services\GsssHtml;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use app\services\GetArticle\Collection;

$this->title = 'Все потоки';

$potok = \common\models\school\Potok::findOne(['kurs_id' => $kurs->id]);
?>

<div class="container">
    <div class="col-lg-12">
        <h1 class="page-header"><?= Html::encode($this->title) ?></h1>
    </div>

    <div class="col-lg-4">
        <?= $this->render('../cabinet-school/_menu', ['school' => $school]) ?>
    </div>
    <div class="col-lg-8">
        <p><a href="<?= Url::to(['cabinet-school-potok/add', 'id' => $kurs->id]) ?>" class="btn btn-default">Добавить</a></p>

        <?php \yii\widgets\Pjax::begin(); ?>
        <?php
        $this->registerJS(<<<JS
$('.buttonLid').click(function(e) {
    e.preventDefault();
    e.stopPropagation();
    window.location = '/cabinet-school-potok/users' + '?' + 'id' + '=' + $(this).data('id');
});
$('.buttonSubscribe').click(function(e) {
    e.preventDefault();
    e.stopPropagation();
    window.location = '/cabinet-school-subscribe/index' + '?' + 'id' + '=' + $(this).data('id');
});
$('.buttonAutoVoronki').click(function(e) {
    e.preventDefault();
    e.stopPropagation();
    window.location = '/cabinet-school-auto-voronki/index' + '?' + 'id' + '=' + $(this).data('id');
});
$('.buttonReklama').click(function(e) {
    e.preventDefault();
    e.stopPropagation();
    window.location = '/cabinet-school-reklama/index' + '?' + 'id' + '=' + $(this).data('id');
});
$('.rowTable').click(function(e) {
    window.location = '/cabinet-school-potok/edit' + '?' + 'id' + '=' + $(this).data('id');
});
$('.buttonPage').click(function(e) {
    e.preventDefault();
    e.stopPropagation();
    var is_page = $(this).data('is_page');
    console.log([is_page,is_page == 0]);
    if (is_page == 0) {
        window.location = '/cabinet-school-pages/add' + '?' + 'id' + '=' + $(this).data('school_id');
    } else {
        window.location = '/cabinet-school-pages/edit' + '?' + 'id' + '=' + $(this).data('page_id');
    } 
});
JS
        );
        ?>
        <?= \yii\grid\GridView::widget([
            'dataProvider' => new \yii\data\ActiveDataProvider([
                'query'      => \common\models\school\Potok::find()
                    ->where(['kurs_id' => $kurs->id])
                ,
                'pagination' => [
                    'pageSize' => 20,
                ],
            ]),
            'tableOptions' => [
                'class' => 'table table-striped table-hover',
            ],
            'rowOptions'   => function ($item) {
                $data = [
                    'data'  => ['id' => $item['id']],
                    'role'  => 'button',
                    'class' => 'rowTable',
                ];
                return $data;
            },
            'columns'      => [
                'id',
                'name',
                'date_start',
                'date_end',
                [
                    'header'  => 'Лиды',
                    'content' => function ($item) {
                        return Html::button('Лиды', [
                            'class' => 'btn btn-info btn-xs buttonLid',
                            'data'  => [
                                'id' => $item['id'],
                            ],
                        ]);
                    },
                ],
                [
                    'header'  => 'Страница',
                    'content' => function (\common\models\school\Potok $item) {
                        $page = null;
                        if (isset($item['page_id'])) {
                            if ($item['page_id']) {
                                $page = \common\models\school\Page::findOne($item['page_id']);
                            }
                        }

                        $data = [
                            'id'      => $item['id'],
                            'is_page' => (is_null($page)) ? 0 : 1,
                        ];
                        if (!is_null($page)) {
                            $data['page_id'] = $page->id;
                        }
                        $kurs = \common\models\school\Kurs::findOne($item['kurs_id']);
                        $school = \common\models\school\School::findOne($kurs->school_id);
                        $data['school_id'] = $school->id;

                        if ($data['is_page'] == 1) {
                            return Html::tag('code', \common\helpers\Html::a($item->url, $page->getUrl(), ['target' => '_blank']));
                        }

                        return Html::button('Страница', [
                            'class' => 'btn btn-primary btn-xs buttonPage',
                            'data'  => $data,
                        ]);
                    },
                ],
                [
                    'header'  => 'Рассылки',
                    'content' => function ($item) {
                        return Html::button('Рассылки', [
                            'class' => 'btn btn-info btn-xs buttonSubscribe',
                            'data'  => [
                                'id' => $item['id'],
                            ],
                        ]);
                    },
                ],
                [
                    'header'  => 'Реклама',
                    'content' => function ($item) {
                        return Html::button('Реклама', [
                            'class' => 'btn btn-info btn-xs buttonReklama',
                            'data'  => [
                                'id' => $item['id'],
                            ],
                        ]);
                    },
                ],
                [
                    'header'  => 'Автоворонки',
                    'content' => function ($item) {
                        return Html::button('Автоворонки', [
                            'class' => 'btn btn-info btn-xs buttonAutoVoronki',
                            'data'  => [
                                'id' => $item['id'],
                            ],
                        ]);
                    },
                ],
                [
                    'header'  => 'Удалить',
                    'content' => function ($item) {
                        return Html::button('Удалить', [
                            'class' => 'btn btn-danger btn-xs buttonDelete',
                            'data'  => [
                                'id' => $item['id'],
                            ],
                        ]);
                    },
                ],
            ],
        ]) ?>
        <?php \yii\widgets\Pjax::end(); ?>
    </div>

</div>

<div class="modal fade" id="modalInfo" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="myModalLabel">Информация</h4>
            </div>
            <div class="modal-body">
                Успешно!
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
            </div>
        </div>
    </div>
</div>