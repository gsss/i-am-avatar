<?php

/** $this \yii\web\View  */
/** @var $school \common\models\school\School */

use yii\helpers\Url;
use app\services\GsssHtml;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use app\services\GetArticle\Collection;

$this->title = 'Все статьи';
\avatar\assets\Notify::register($this);

$str = '';

if (!empty($school->blog_header_id)) {
    $str = Html::tag('u', 'Шапка');
} else {
    $str = 'Шапка';
}

$str .= ' ';

if (!empty($school->blog_footer_id)) {
    $str .= Html::tag('u', 'подвал');
} else {
    $str .= 'подвал';
}

?>

<div class="container">


    <h1 class="page-header"><?= Html::encode($this->title) ?></h1>

    <p>
        <a href="<?= Url::to(['cabinet-school-blog/add', 'id' => $school->id]) ?>" class="btn btn-default">Добавить</a>
        <a href="<?= Url::to(['cabinet-school-blog/header-footer', 'id' => $school->id]) ?>" class="btn btn-default"><?= $str ?></a>
    </p>

    <?php \yii\widgets\Pjax::begin(); ?>
    <?php
    $isProd = YII_ENV_PROD ? 1 : 0;
    $start = ($isProd)? 'https://etherscan.io' : 'https://ropsten.etherscan.io';
    $contractAddress = ($isProd)? '0x52ed3c202c4652f952a1561ac0c030f1ed9460ff' : '0xfF6C78BFedF1bdb2D85071A9040798F8057A0A36';
    $this->registerJS(<<<JS
$('[data-toggle="tooltip"]').tooltip();
$('.buttonDelete').click(function (e) {
    e.preventDefault();
    e.stopPropagation();
    if (confirm('Подтвердите удаление')) {
        var button = $(this);
        var id = $(this).data('id');
        ajaxJson({
            url: '/cabinet-school-blog/delete' + '?' + 'id' + '=' + id,
            success: function (ret) {
                $('#modalInfo').on('hidden.bs.modal', function() {
                    button.parent().parent().remove();
                }).modal();
            }
        });
    }
});
$('.buttonSubscribe').click(function (e) {
    e.preventDefault();
    e.stopPropagation();
    $('#cabinetschoolblogsubscribe-id').val($(this).data('id'));
    $('#modalSubscribe').modal();
});
$('.buttonRegister').click(function (e) {
    e.preventDefault();
    e.stopPropagation();
    if (confirm('Подтвердите подпись')) {
        var button = $(this);
        var id = $(this).data('id');
        ajaxJson({
            url: '/cabinet-school-blog/sign',
            data: {id: id},
            success: function (ret1) {
                var name = ret1.name;
                var hash = ret1.hash;
                var url = ret1.url;
                
                register(name, url, hash, function(ret2) {
                    
                    var txid = ret2.txid;
                    $('#txid').html(ret2.txid);
                    var start = '{$start}';
                    $('#txid_link').attr('href', start + '/tx/' + txid);
                    
                    ajaxJson({
                        url: '/cabinet-school-blog/register',
                        data: {
                            name: name,
                            hash: hash,
                            url: url,
                            txid: txid,
                            id: id
                        },
                        success: function (ret3) {
                            $('#modalInfo2').on('hidden.bs.modal', function() {
                                window.location.reload();
                            }).modal();
                        }
                    });
                });
            }
        });
    }
});

function  register(name, url, hash, callback) {
    var isEthereum = false; 
    var ethereum = null;
    var isMetaMask = false; 
    if (typeof window.ethereum !== 'undefined') { 
        /* deal with it */ 
        isEthereum = true;
        ethereum = window.ethereum;
    }
    if (!isEthereum) {
        alert ('У вас не установлен MetaMask');
        return;
    }
    
    if (window.ethereum.isMetaMask) {
        isMetaMask = true;
    } else {
        alert ('У вас не установлен MetaMask');
        return;
    }
    
    if (isMetaMask) {
        var accounts = ethereum.enable();
        var net = ethereum.networkVersion;
        if (net == 1) console.log('Ethereum Main Network');
        if (net == 2) console.log('Morden Test network');
        if (net == 3) console.log('Ropsten Test Network');
        if (net == 4) console.log('Rinkeby Test Network');
        if (net == 5) console.log('Goerli Test Network');
        if (net == 42) console.log('Kovan Test Network');
    }
    
    var contractAddress = '{$contractAddress}';
    var abi = '[{"constant":true,"inputs":[],"name":"getDocumentsCount","outputs":[{"name":"","type":"uint256"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":false,"inputs":[{"name":"hash","type":"string"},{"name":"link","type":"string"},{"name":"data","type":"string"}],"name":"registerDocument","outputs":[],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":false,"inputs":[{"name":"id","type":"uint256"}],"name":"addSignature","outputs":[],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":true,"inputs":[{"name":"id","type":"uint256"}],"name":"getDocument","outputs":[{"name":"hash","type":"string"},{"name":"link","type":"string"},{"name":"data","type":"string"},{"name":"creator","type":"address"},{"name":"date","type":"uint256"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":false,"inputs":[],"name":"kill","outputs":[],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":true,"inputs":[{"name":"","type":"uint256"}],"name":"documentsIds","outputs":[{"name":"hash","type":"string"},{"name":"link","type":"string"},{"name":"data","type":"string"},{"name":"creator","type":"address"},{"name":"date","type":"uint256"},{"name":"signsCount","type":"uint256"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":true,"inputs":[{"name":"id","type":"uint256"}],"name":"getDocumentSignsCount","outputs":[{"name":"","type":"uint256"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":false,"inputs":[{"name":"newOwner","type":"address"}],"name":"changeOwner","outputs":[],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":true,"inputs":[{"name":"id","type":"uint256"},{"name":"index","type":"uint256"}],"name":"getDocumentSign","outputs":[{"name":"member","type":"address"},{"name":"date","type":"uint256"}],"payable":false,"stateMutability":"view","type":"function"},{"inputs":[],"payable":false,"stateMutability":"nonpayable","type":"constructor"},{"anonymous":false,"inputs":[{"indexed":false,"name":"id","type":"uint256"},{"indexed":false,"name":"member","type":"address"}],"name":"DocumentSigned","type":"event"},{"anonymous":false,"inputs":[{"indexed":false,"name":"id","type":"uint256"},{"indexed":false,"name":"hash","type":"string"}],"name":"DocumentRegistered","type":"event"}]';
    
    // Check if Web3 has been injected by the browser:
    if (typeof web3 !== 'undefined') {
        // You have a web3 browser! Continue below!
        // console.log('web3 exist');
        
        var eth = web3.currentProvider;
        var contract = web3.eth.contract(JSON.parse(abi)).at(contractAddress);
        
        contract.registerDocument(hash, url, '', function(a1,a2,a3,a4,a5) {
            if (a1 !== null) {
                console.log(a1);
                new Noty({
                    timeout: 5000,
                    theme: 'relax',
                    type: 'warning',
                    layout: 'bottomRight',
                    text: 'Code = ' + a1.code + ', Message = ' + a1.message + ', Stack = ' + a1.stack 
                }).show();
            } else {
                callback({txid: a2});
            }
        })
        ;
    } else {
         // Warn the user that they need to get a web3 browser
         // Or install MetaMask, maybe with a nice graphic.
    }
    
}

$('.rowTable').click(function() {
    window.location = '/cabinet-school-blog/edit' + '?' + 'id' + '=' + $(this).data('id');
});
JS
    );
    ?>

    <?= \yii\grid\GridView::widget([
        'dataProvider' => new \yii\data\ActiveDataProvider([
            'query'      => \common\models\blog\Article::find()
                ->where(['school_id' => $school->id])
                ->orderBy(['created_at' => SORT_DESC])
            ,
            'pagination' => [
                'pageSize' => 20,
            ],
        ]),
        'tableOptions' => [
            'class' => 'table table-striped table-hover',
        ],
        'rowOptions'   => function ($item) {
            $data = [
                'data'  => ['id' => $item['id']],
                'role'  => 'button',
                'class' => 'rowTable',
            ];
            return $data;
        },
        'columns'      => [
            'id',
            [
                'header'  => 'Картинка',
                'content' => function ($item) {
                    $i = ArrayHelper::getValue($item, 'image', '');
                    if ($i == '') return '';

                    return Html::img(
                        \common\widgets\FileUpload7\FileUpload::getFile($i, 'crop'),
                        [
                            'class'  => "thumbnail",
                            'width'  => 80,
                            'style'  => 'margin-bottom: 0px;',
                        ]);
                },
            ],
            'name',
            [
                'header'  => 'Создано',
                'content' => function ($item) {
                    $v = \yii\helpers\ArrayHelper::getValue($item, 'created_at', 0);
                    if ($v == 0) return '';

                    return Html::tag('abbr', \cs\services\DatePeriod::back($v, ['isShort' => true]), ['data' => ['toggle' => 'tooltip'], 'title' => Yii::$app->formatter->asDatetime($v)]);
                },
            ],
            'views_counter:text:Счетчик просмотров',
            [
                'header'  => 'Разослать',
                'content' => function ($item) {
                    if ($item['is_subscribe'] == 1) return Html::tag('span', null, ['class' => 'glyphicon glyphicon-check', 'data' => ['toggle' => 'tooltip'], 'title' => 'Разослано']);

                    return Html::button('Разослать', [
                        'class' => 'btn btn-success btn-xs buttonSubscribe',
                        'data'  => [
                            'id' => $item['id'],
                        ],
                    ]);
                },
            ],
            [
                'header'  => 'Подписать',
                'content' => function ($item) {
                    if ($item['is_signed'] == 1) return Html::tag('span', null, ['class' => 'glyphicon glyphicon-check', 'data' => ['toggle' => 'tooltip'], 'title' => 'Подписано']);

                    return Html::button('Подписать', [
                        'class' => 'btn btn-info btn-xs buttonRegister',
                        'data'  => [
                            'id' => $item['id'],
                        ],
                    ]);
                },
            ],
            [
                'header'  => 'Удалить',
                'content' => function ($item) {
                    return Html::button('Удалить', [
                        'class' => 'btn btn-danger btn-xs buttonDelete',
                        'data'  => [
                            'id' => $item['id'],
                        ],
                    ]);
                },
            ],
        ],
    ]) ?>
    <?php \yii\widgets\Pjax::end(); ?>


</div>

<div class="modal fade" id="modalInfo" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="myModalLabel">Информация</h4>
            </div>
            <div class="modal-body">
                Успешно!
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modalInfo2" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="myModalLabel">Информация</h4>
            </div>
            <div class="modal-body">
                <p>Успешно!</p>
                <p>TXID = <a id='txid_link' href="https://ropsten.etherscan.io/tx/0xb13ffc710bb02a1542afd339331e4939e7cc859c483e4686f697ebe1a65c7555" target="_blank">
                    <code id="txid"></code>
                    </a>
                </p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="modalSubscribe" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="myModalLabel">Информация</h4>
            </div>
            <div class="modal-body">
                <!-- Список потоков по которым можно сделать рассылку -->
                <?php $model = new \avatar\models\validate\CabinetSchoolBlogSubscribe() ?>
                <?php $form = \common\services\FormAjax\ActiveForm::begin([
                    'model'   => $model,
                    'formUrl' => '/cabinet-school-blog/subscribe',
                    'success' => <<<JS
function (ret) {
    $('#modalSubscribe').on('hidden.bs.modal', function(e) {
        $('#modalInfo').on('hidden.bs.modal', function(e) {
            window.location.reload();
        }).modal();
    }).modal('hide');
    console.log(ret);
}
JS

                ])?>

                    <?=  Html::activeHiddenInput($model, 'id') ?>
                    <?=  $form->field($model, 'potok_id')->dropDownList(ArrayHelper::map(
                            \common\models\school\Potok::find()
                                ->select(['school_potok.*'])
                                ->innerJoin('school_kurs', 'school_kurs.id = school_potok.kurs_id')
                                ->where(['school_kurs.school_id' => $school->id])->all(),
                            'id',
                            'name'
                    ))->label('Поток') ?>

                <?php \common\services\FormAjax\ActiveForm::end(['label' => 'Разослать']) ?>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
            </div>
        </div>
    </div>
</div>