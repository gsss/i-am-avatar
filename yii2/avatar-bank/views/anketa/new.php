<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;
use app\models\UnionCategory;
use yii\db\Query;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \common\models\Anketa */

$this->title = 'Анкета для вступления в звездную команду ЯАватар';

?>
<div class="container">
    <div class="page-header">
        <h1 class="text-center"><?= Html::encode($this->title) ?></h1>
    </div>
    <p>
        <img src="/images/controller/anketa/new/header.jpg"  width="100%" class="thumbnail">
    </p>
        <div class="row">
            <div class="col-lg-3">
            </div>
            <div class="col-lg-6" style="margin-bottom: 200px;">
                <?php if (!is_null($id = Yii::$app->session->getFlash('anketa'))) : ?>

                    <div class="alert alert-success">
                        Успешно добавлено.
                    </div>

                <?php else: ?>

                    <?php $form = ActiveForm::begin([
                        'id'        => 'contact-form',
                        'options'   => ['enctype' => 'multipart/form-data']
                    ]); ?>
                    <?= $form->field($model, 'name_first') ?>
                    <?= $form->field($model, 'name_last') ?>
                    <?= $form->field($model, 'name_middle') ?>
                    <?= $form->field($model, 'contact_email') ?>
                    <?= $form->field($model, 'contact_phone') ?>
                    <?= $form->field($model, 'contact_vk') ?>
                    <?= $form->field($model, 'contact_fb') ?>
                    <?= $form->field($model, 'contact_telegram') ?>
                    <?= $form->field($model, 'contact_whatsapp') ?>
                    <?= $form->field($model, 'contact_skype')->hint('') ?>
                    <?= $form->field($model, 'age') ?>
                    <?= $form->field($model, 'place') ?>
                    <?= $form->field($model, 'vozmojnosti')->textarea(['rows' => 10]) ?>
                    <?= $form->field($model, 'opit')->textarea(['rows' => 10]) ?>

                    <hr>
                    <div class="form-group">
                        <?= Html::submitButton('Добавить', [
                            'class' => 'btn btn-success',
                            'name'  => 'contact-button',
                            'style' => 'width:100%',
                        ]) ?>
                    </div>
                    <?php ActiveForm::end(); ?>

                <?php endif; ?>
            </div>
        </div>

</div>
