<?php
use cs\Widget\FileUpload3\FileUpload;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;
use yii\db\Query;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \common\models\AnketaSchool */

$this->title = 'Анкета для подключения школы';


?>
<style>
    .s201903021702 p {
        font-size: 150%;
    }
</style>
<div class="container">
    <div class="col-lg-12">
        <h1 class="text-center page-header"><?= Html::encode($this->title) ?></h1>
        <p>
            <img src="/images/controller/anketa/new/header.jpg" width="100%" class="thumbnail">
        </p>
    </div>
    <div class="col-lg-6 col-lg-offset-3 s201903021702">

        <?php if (!is_null($id = Yii::$app->session->getFlash('anketa'))) : ?>

            <p>
                Мы сейчас разрабатываем платформу для обучения <a href="https://www.i-am-avatar.com/" target="_blank">https://www.i-am-avatar.com/</a>. Она сейчас находится на начальном этапе разработке и в тестовом режиме. На сайте находится весь перечень функционала который мы планируем внедрить по окончании разработок.
            </p>
            <p>
                Хотя начального готового функционала уже достаточна для того чтобы собрать мини сайт, проводить курсы и делать рассылки. Сейчас мы набираем школы которые хотят пройти обучение "<a href="http://root.i-am-avatar.com/kurs-i-avatar" target="_blank">Как на нашей платформе собрать себе школу, зарабатывать на ней и масштабировать</a>".
            </p>
            <p>
                Для такого сотрудничества мы сейчас выстраиваем следующие взаимоотношения: школа выделяет администратора, который в дальнейшем и будет вести школу, мы его обучаем.
            </p>
            <p>

                Далее если у вас все будет успешно и при помощи нас вы будете зарабатывать больше то предложим выбрать по дружески платный тариф в замен на наши услуги и обучение вас как масштабироваться. Для всех кто будет с нами с начала мы предложим выгодные тарифы в будущем в отличие от уже известных платформ действующих на рынке. Сейчас это безоплатно.
            </p>
            <p>
                Такое сотрудничество будет интересным для нас.
            </p>

            <h2 class="text-center page-header">Анкета</h2>

            <div class="alert alert-success">
                Успешно добавлено.
            </div>

        <?php else: ?>

            <?php $form = ActiveForm::begin([
                'id'      => 'contact-form',
                'options' => ['enctype' => 'multipart/form-data'],
            ]); ?>
            <?= $form->field($model, 'name')->label('Название школы') ?>
            <?= $form->field($model, 'description')->textarea(['rows' => 10])->label('Описание школы') ?>
            <?= $form->field($model, 'email')->label('Email для связи') ?>
            <?= $form->field($model, 'phone')->label('Телефон для связи') ?>

            <hr>
            <div class="form-group">
                <?= Html::submitButton('Добавить', [
                    'class' => 'btn btn-success',
                    'name'  => 'contact-button',
                    'style' => 'width:100%',
                ]) ?>
            </div>
            <?php ActiveForm::end(); ?>

        <?php endif; ?>

        <?= $this->render('../blocks/share', [
            'image'       => Url::to(\avatar\modules\Share\Share::image("Анкета для\nподключения\nшколы", __FILE__)->getPath(), true),
            'title'       => $this->title,
            'url'         => Url::current([], true),
            'description' => 'Сейчас мы набираем школы которые хотят пройти обучение "Как на нашей платформе собрать себе школу, зарабатывать на ней и масштабировать"',
        ]) ?>

    </div>
</div>
