<?php

/**
 * @var $this         \yii\web\View
 * @var $table_schema string
 */

use yii\widgets\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;
use common\assets\HighCharts\HighChartsAsset;
use common\widgets\highCharts\HighCharts;

$this->title = $table_schema;

?>
<div class="container">

    <h1 class="page-header"><?= $table_schema ?></h1>

    <?php
    $rows = \common\models\information_schema\Tables::find()
        ->select([
                'ROUND(SUM(data_length + index_length) / 1024 / 1024, 2) AS c_all',
                'ROUND(SUM(data_length) / 1024 / 1024, 2) AS c_data',
                'ROUND(SUM(index_length) / 1024 / 1024, 2) AS c_index',
                'TABLE_NAME',
            ])
        ->where(['TABLE_SCHEMA' => $table_schema])
        ->groupBy(['TABLE_NAME'])
        ->orderBy(['c_all' => SORT_ASC])
        ->asArray()
        ->all();
    $r = [];
    $c = 0;
    foreach ($rows as $row) {
        $r[] = [
            'name' => $row['TABLE_NAME'],
            'y'    => (float)$row['c_all'],
        ];
        $c += (float)$row['c_all'];
    }
    ?>
    <p>all: <?= $c ?> Mb</p>
    <?= \cs\Widget\HighCharts\HighCharts::widget([
        'chartOptions' => [
            'chart'       => [
                'plotBackgroundColor' => null,
                'plotBorderWidth'     => null,
                'plotShadow'          => false,
                'type'                => 'pie',
            ],
            'title'       => [
                'text' => 'График',
            ],
            'tooltip'     => [
                'pointFormat' => '{series.name}: <b>{point.percentage:.1f}%</b>',
            ],
            'plotOptions' => [
                'pie' => [
                    'allowPointSelect' => true,
                    'cursor'           => 'pointer',
                    'dataLabels'       => [
                        'enabled'        => true,
                        'format'         => '<b>{point.name}</b>: {point.percentage:.1f} %',
                        'style'          => [
                            'color' => new \yii\web\JsExpression('(Highcharts.theme && Highcharts.theme.contrastTextColor) || \'black\''),
                        ],
                        'connectorColor' => 'silver',
                    ],
                ],
            ],


            'series' => [
                [
                    'name' => 'Share',
                    'data' => $r,
                ],

            ],
        ],
    ]);

    ?>


    <?= \yii\grid\GridView::widget([
        'dataProvider' => new \yii\data\ArrayDataProvider(['allModels' => $rows]),
        'tableOptions' => [
            'class' => 'table table-striped table-hover',
        ],
        'columns'      => [
            'TABLE_NAME',
            'c_all',
            'c_data',
            'c_index',
        ],
    ]) ?>

</div>
