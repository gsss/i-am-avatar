<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;
use app\models\UnionCategory;
use yii\db\Query;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/** @var $school \common\models\school\School */

$this->title = 'Добавить Заказ';



?>

<?php
$this->registerJs(<<<JS
 // $('#step' + 1).collapse('hide');
 // $('#step' + 7).collapse('show');

JS
);
?>
<div class="container">

    <?php \avatar\widgets\SchoolMenu::begin(['school' => $school]) ?>
    <h1 class="page-header"><?= Html::encode($this->title) ?></h1>

    <div class="col-lg-4">
        <div class="row">
            <ul class="list-group">
                <?php
                $items = [];
                $isFirst = true;
                $c = 1;
                foreach (\avatar\controllers\CabinetSchoolShopRequestsAjaxController::$list as $i) {
                    $class = ['list-group-item'];
                    if ($isFirst) {
                        $class[] = 'active';
                        $isFirst = false;
                    }
                    $class[] = 'item-step' . $c;
                    $c++;
                    $items[] = Html::tag('li', $i, ['class' => $class]);
                }
                ?>
                <?= join('', $items) ?>
            </ul>
        </div>
    </div>
    <div class="col-lg-8">
        <?php $num = 0; ?>
        <?php $num++; ?>

        <?php // Контент ?>
        <div class="collapse in" id="step<?= $num ?>">
            <?php $name = '\avatar\models\forms\shop\RequestAdd\Step' . $num; ?>
            <?php $model = new $name(); ?>

            <?php $form = ActiveForm::begin([
                'id'                   => 'form_step' . $num,
                'enableClientScript'   => false,
            ]); ?>
            <?= $form->field($model, 'name') ?>
            <?= $form->field($model, 'content_short')->textarea(['rows' => 3]) ?>
            <?= $form->field($model, 'content')->textarea(['rows' => 3]) ?>
            <?= $form->field($model, 'image')->widget(\common\widgets\FileUpload7\FileUpload::className(), [
                'update'   => \avatar\controllers\CabinetSchoolPagesConstructorController::getUpdate(),
                'settings' => \avatar\controllers\CabinetSchoolPagesConstructorController::getSettings(2, 15),
                'events'   => [
                    'onDelete' => function (\common\models\BlogItem $item) {
                        $r = new \cs\services\Url($item->image);
                        $d = pathinfo($r->path);
                        $start = $d['dirname'] . '/' . $d['filename'];

                        \common\models\school\File::deleteAll(['like', 'file', $start]);
                    },
                ],
            ]) ?>
            <?php \yii\bootstrap\ActiveForm::end();?>
            <button class="btn btn-success buttonStep<?= $num ?>" style="width: 100%;">Далее</button>
        </div>


        <?php // Место проведения ?>
        <?php $num++; ?>
        <div class="collapse" id="step<?= $num ?>">
            <?php $name = '\avatar\models\forms\shop\RequestAdd\Step' . $num; ?>
            <?php $model = new $name(); ?>

            <?php $form = ActiveForm::begin([
                'id'                   => 'form_step' . $num,
                'enableClientScript'   => false,
            ]); ?>
            <?= $form->field($model, 'is_online')->widget('\common\widgets\CheckBox2\CheckBox') ?>
            <?= $form->field($model, 'place_name') ?>
            <?= $form->field($model, 'place')->widget('\common\widgets\PlaceMapYandex\PlaceMap') ?>
            <?= $form->field($model, 'online_url') ?>

            <?php \yii\bootstrap\ActiveForm::end();?>
            <button class="btn btn-success buttonStep<?= $num ?>" style="width: 100%;">Далее</button>

        </div>


        <?php // Дата проведения ?>
        <?php $num++; ?>
        <div class="collapse" id="step<?= $num ?>">
            <?php $name = '\avatar\models\forms\shop\RequestAdd\Step' . $num; ?>
            <?php $model = new $name(); ?>

            <?php $form = ActiveForm::begin([
                'id'                   => 'form_step' . $num,
                'enableClientScript'   => false,
            ]); ?>
            <?= $form->field($model, 'date_start')->widget('\common\widgets\DatePicker\DatePicker') ?>
            <?= $form->field($model, 'time_start')->widget('\kartik\time\TimePicker', [
                'pluginOptions' => [
                    'showSeconds' => false,
                    'showMeridian' => false,
                    'minuteStep' => 5,
                ],
                'addonOptions' => [
                    'asButton' => true,
                    'buttonOptions' => ['class' => 'btn btn-info']
                ],
            ]) ?>
            <?= $form->field($model, 'time_zone')->widget('\kartik\select2\Select2', [
                'data' => DateTimeZone::listIdentifiers(),
                'options' => ['placeholder' => 'Выберитие зону ...'],
                'pluginOptions' => [
                    'allowClear' => true
                ],
            ]) ?>
            <?= $form->field($model, 'date_end')->widget('\common\widgets\DatePicker\DatePicker') ?>
            <?= $form->field($model, 'time_end')->widget('\kartik\time\TimePicker', [
                'pluginOptions' => [
                    'showSeconds' => false,
                    'showMeridian' => false,
                    'minuteStep' => 5,
                ],
                'addonOptions' => [
                    'asButton' => true,
                    'buttonOptions' => ['class' => 'btn btn-info']
                ],
            ]) ?>
            <?php \yii\bootstrap\ActiveForm::end();?>
            <button class="btn btn-success buttonStep<?= $num ?>" style="width: 100%;">Далее</button>

        </div>


        <?php // Кто ведет ?>
        <?php $num++; ?>
        <div class="collapse" id="step<?= $num ?>">
            <?php $name = '\avatar\models\forms\shop\EventAdd\Step' . $num; ?>
            <?php $model = new $name(); ?>
            <?php $model->school_id = $school->id; ?>

            <?php $form = ActiveForm::begin([
                'id'                   => 'form_step' . $num,
                'enableClientScript'   => false,
            ]); ?>
            <?= $form->field($model, 'masters')->widget('\avatar\widgets\Masters\Widget', ['school' => $school]) ?>

            <?php \yii\bootstrap\ActiveForm::end();?>
            <button class="btn btn-success buttonStep<?= $num ?>" style="width: 100%;">Далее</button>

        </div>

        <?php // Анкета регистрации ?>
        <?php $num++; ?>
        <div class="collapse" id="step<?= $num ?>">
            <?php $name = '\avatar\models\forms\shop\EventAdd\Step' . $num; ?>
            <?php $model = new $name(); ?>
            <?php $model->school_id = $school->id; ?>

            <?php $form = ActiveForm::begin([
                'id'                   => 'form_step' . $num,
                'enableClientScript'   => false,
            ]); ?>
            <?= $form->field($model, 'fields')->widget('\avatar\widgets\Anketa\Widget', ['school' => $school]) ?>

            <?php \yii\bootstrap\ActiveForm::end();?>
            <button class="btn btn-success buttonStep<?= $num ?>" style="width: 100%;">Далее</button>

        </div>

        <?php // Билеты ?>
        <?php $num++; ?>
        <div class="collapse" id="step<?= $num ?>">

            <?php $name = '\avatar\models\forms\shop\EventAdd\Step' . $num; ?>
            <?php $model = new $name(); ?>
            <?php $model->school_id = $school->id; ?>
            <?php $form = ActiveForm::begin([
                'id'                   => 'form_step' . $num,
                'enableClientScript'   => false,
            ]); ?>

            <div id="online">

                <!-- Nav tabs -->
                <ul class="nav nav-tabs" role="tablist" style="margin-bottom: 20px;">
                    <li role="presentation" class="active"><a href="#free" aria-controls="home" role="tab" data-toggle="tab">Безоплатно</a></li>
                    <li role="presentation"><a href="#both" aria-controls="profile" role="tab" data-toggle="tab">Смешанно</a></li>
                    <li role="presentation"><a href="#buy" aria-controls="messages" role="tab" data-toggle="tab">Платно</a></li>
                </ul>

                <?php
                $this->registerJs(<<<JS
$('#step6-vebinar_is_password').change(function(e) {
    if ($(this).is(':checked')) {
        $('.field-step6-vebinar_password').show();
    } else {
        $('.field-step6-vebinar_password').hide();
    }
});
$('.field-step6-vebinar_password').hide();
JS
)

                ?>

                <!-- Tab panes -->
                <div class="tab-content">
                    <div role="tabpanel" class="tab-pane active" id="free">
                        <?= $form->field($model, 'vebinar_is_password')->widget('\common\widgets\CheckBox2\CheckBox') ?>
                        <?= $form->field($model, 'vebinar_password')->passwordInput() ?>
                        <?= $form->field($model, 'vebinar_max') ?>
                    </div>
                    <div role="tabpanel" class="tab-pane" id="both">
                        <?= $form->field($model, 'vebinar_both_tickets')->widget('\avatar\widgets\Tickets\Widget', ['school' => $school]) ?>
                    </div>
                    <div role="tabpanel" class="tab-pane" id="buy">
                        <?= $form->field($model, 'vebinar_buy_tickets')->widget('\avatar\widgets\Tickets\Widget', ['school' => $school]) ?>
                    </div>
                    <?= Html::hiddenInput(Html::getInputName($model, 'vebinar_tab'),1) ?>
                </div>

            </div>

            <div id="offline" style="display: none;">

                <!-- Nav tabs -->
                <ul class="nav nav-tabs" role="tablist" style="margin-bottom: 20px;">
                    <li role="presentation" class="active"><a href="#offline_free" aria-controls="home" role="tab" data-toggle="tab">Безоплатно</a></li>
                    <li role="presentation"><a href="#offline_both" aria-controls="profile" role="tab" data-toggle="tab">Смешанно</a></li>
                    <li role="presentation"><a href="#offline_buy" aria-controls="messages" role="tab" data-toggle="tab">Платно</a></li>
                </ul>

                <?php
                $this->registerJs(<<<JS
$('#step6-vebinar_is_password').change(function(e) {
    if ($(this).is(':checked')) {
        $('.field-step6-vebinar_password').show();
    } else {
        $('.field-step6-vebinar_password').hide();
    }
});
$('.field-step6-vebinar_password').hide();
JS
)

                ?>

                <!-- Tab panes -->
                <div class="tab-content">
                    <div role="tabpanel" class="tab-pane active" id="offline_free">
                        <?= $form->field($model, 'offline_max') ?>
                    </div>
                    <div role="tabpanel" class="tab-pane" id="offline_both">
                        <?= $form->field($model, 'offline_both_tickets')->widget('\avatar\widgets\Tickets\Widget', ['school' => $school]) ?>
                    </div>
                    <div role="tabpanel" class="tab-pane" id="offline_buy">
                        <?= $form->field($model, 'offline_buy_tickets')->widget('\avatar\widgets\Tickets\Widget', ['school' => $school]) ?>
                    </div>
                    <?= Html::hiddenInput(Html::getInputName($model, 'offline_tab'),1) ?>
                </div>

            </div>



            <?php \yii\bootstrap\ActiveForm::end();?>
            <button class="btn btn-success buttonStep<?= $num ?>" style="width: 100%;">Далее</button>

        </div>

        <?php // Страница ?>
        <?php $num++; ?>
        <div class="collapse" id="step<?= $num ?>">
            <?php $name = '\avatar\models\forms\shop\EventAdd\Step' . $num; ?>
            <?php $model = new $name(); ?>
            <?php $model->school_id = $school->id; ?>

            <?php $form = ActiveForm::begin([
                'id'                   => 'form_step' . $num,
                'enableClientScript'   => false,
            ]); ?>
            <?= $form->field($model, 'url')->label('Ссылка на страницу анонса') ?>

            <?php \yii\bootstrap\ActiveForm::end();?>
            <button class="btn btn-success buttonStep<?= $num ?>" style="width: 100%;">Далее</button>

        </div>

        <?php // Публикация ?>
        <?php $num++; ?>
        <div class="collapse" id="step<?= $num ?>">
            <?php
            $this->registerJs(<<<JS
$('.buttonFinish').click(function(e) {
    ajaxJson({
        url: '/cabinet-school-events-ajax/finish' + '?' + 'id' + '=' + {$school->id},
        success: function(ret) {
            window.location = ret.href;
        },
        errorScript: function(ret) {
            
        }
    });
});
$('#step2-is_online').change(function(e) {
    var value = $(this).val();
    if ($(this).is(':checked')) {
        $('.field-step2-place').hide();
        $('.field-step2-place_name').hide();
        $('.field-step2-online_url').show();
        $('#online').show();
        $('#offline').hide();
    } else {
        $('.field-step2-place').show();
        $('.field-step2-place_name').show();
        $('.field-step2-online_url').hide();
        $('#online').hide();
        $('#offline').show();
    }
});
if ($('#step2-is_online').is(':checked')) {
    $('.field-step2-place').hide();
    $('.field-step2-place_name').hide();
    $('.field-step2-online_url').show();
    $('#online').show();
    $('#offline').hide();
} else {
    $('.field-step3-place').show();
    $('.field-step3-place_name').show();
    $('.field-step3-online_url').hide();
    $('#online').hide();
    $('#offline').show();
}
$('#step3-time_start').val('');
$('#step3-time_end').val('');
JS
)
            ?>

            <button class="btn btn-success buttonFinish" style="width: 100%;">Публикация</button>

        </div>
    </div>



    <?php for ($num = 1; $num <= \avatar\controllers\CabinetSchoolEventsAjaxController::$count; $num++) { ?>
        <?php
        $this->registerJs(<<<JS
$('.buttonStep{$num}').click(function(e) {
    ajaxJson({
        url: '/cabinet-school-events-ajax/step{$num}' + '?' + 'id' + '=' + {$school->id},
        data: $('#form_step{$num}').serializeArray(),
        success: function(ret) {
            var num = {$num};
            $('#step' + num).collapse('hide');
            $('.item-step' + (num + 1)).addClass('active');
            $('#step' + (num + 1)).collapse('show');
        },
        errorScript: function(ret) {
            switch (ret.id) {
                case 102:
                    var f = $('#form_step{$num}');
                    for (var key in ret.data) {
                        if (ret.data.hasOwnProperty(key)) {
                            var name = key;
                            var value = ret.data[key];
                            var t = f.find('.field-' + name);
                            t.addClass('has-error');
                            t.find('p.help-block-error').html(value.join('<br>')).show();
                        }
                    }
                    break;
            }
        }
    });
});

$('#form_step{$num} .form-control').on('focus', function() {
    var o = $(this);
    var p = o.parent();
    if (p.hasClass('input-group')) {
        p = p.parent();
    }
    p.removeClass('has-error');
    p.find('p.help-block-error').hide();
});
JS
        )
        ?>
    <?php } ?>
    <?php \avatar\widgets\SchoolMenu::end() ?>

</div>
