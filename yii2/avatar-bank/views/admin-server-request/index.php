<?php

/** $this \yii\web\View  */

use yii\helpers\Url;
use app\services\GsssHtml;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use app\services\GetArticle\Collection;

$this->title = 'ServerRequest';


?>

<div class="container">
    <div class="col-lg-12">
        <h1 class="page-header"><?= Html::encode($this->title) ?></h1>



    </div>
    <div class="col-lg-8">
        <?php \yii\widgets\Pjax::begin(); ?>
        <?php
        $this->registerJS(<<<JS
$('[data-toggle="tooltip"]').tooltip();
$('.buttonGet').click(function (e) {
    e.preventDefault();
    e.stopPropagation();
    var button = $(this);
    var id = $(this).data('id');
    ajaxJson({
        url: '/admin-monitoring/index-get?id=' + id,
        success: function (ret) {
            $('#modalInfo').find('.modal-body').html(ret.html);
            $('#modalInfo').modal();
        }
    });
});
$('.buttonServer').click(function (e) {
    e.preventDefault();
    e.stopPropagation();
    var button = $(this);
    var id = $(this).data('id');
    ajaxJson({
        url: '/admin-monitoring/index-server?id=' + id,
        success: function (ret) {
            $('#modalInfo').find('.modal-body').html(ret.html);
            $('#modalInfo').modal();
        }
    });
});
$('.buttonHeaders').click(function (e) {
    e.preventDefault();
    e.stopPropagation();
    var button = $(this);
    var id = $(this).data('id');
    ajaxJson({
        url: '/admin-monitoring/index-headers?id=' + id,
        success: function (ret) {
            $('#modalInfo').find('.modal-body').html(ret.html);
            $('#modalInfo').modal();
        }
    });
});
$('.buttonSession').click(function (e) {
    e.preventDefault();
    e.stopPropagation();
    var button = $(this);
    var id = $(this).data('id');
    ajaxJson({
        url: '/admin-monitoring/index-session?id=' + id,
        success: function (ret) {
            $('#modalInfo').find('.modal-body').html(ret.html);
            $('#modalInfo').modal();
        }
    });
});
$('.buttonRowText').click(function (e) {
    e.preventDefault();
    e.stopPropagation();
    var button = $(this);
    var id = $(this).data('id');
    ajaxJson({
        url: '/admin-monitoring/index-row-text?id=' + id,
        success: function (ret) {
            $('#modalInfo').find('.modal-body').html(ret.html);
            $('#modalInfo').modal();
        }
    });
});
$('.buttonPost').click(function (e) {
    e.preventDefault();
    e.stopPropagation();
    var button = $(this);
    var id = $(this).data('id');
    ajaxJson({
        url: '/admin-monitoring/index-post?id=' + id,
        success: function (ret) {
            $('#modalInfo').find('.modal-body').html(ret.html);
            $('#modalInfo').modal();
        }
    });
});

JS
        );
        $sort = new \yii\data\Sort([
            'defaultOrder' => [
                'log_time' => SORT_DESC
            ]
        ]);
        ?>
        <?= \yii\grid\GridView::widget([
            'dataProvider' => new \yii\data\ActiveDataProvider([
                'query'      => \common\models\statistic\ServerRequest::find()
                ,
                'pagination' => [
                    'pageSize' => 100,
                ],
                'sort' => ['defaultOrder' => ['log_time' => SORT_DESC]],
            ]),
            'tableOptions' => [
                'class' => 'table table-striped table-hover',
            ],
            'rowOptions'   => function ($item) {
                $data = [
                    'data'  => ['id' => $item['id']],
                    'role'  => 'button',
                    'class' => 'rowTable'
                ];
                return $data;
            },
            'columns'      => [
                'id',
                [
                    'header'  => 'mode',
                    'content' => function (\common\models\statistic\ServerRequest $item) {
                        if ($item->mode == \common\models\statistic\ServerRequest::MODE_GET) return Html::tag('span', 'GET', ['class' => 'label label-warning']);
                        if ($item->mode == \common\models\statistic\ServerRequest::MODE_POST) return Html::tag('span', 'POST', ['class' => 'label label-info']);
                        if ($item->mode == \common\models\statistic\ServerRequest::MODE_HEAD) return Html::tag('span', 'HEAD', ['class' => 'label label-danger']);
                        if ($item->mode == \common\models\statistic\ServerRequest::MODE_OPTIONS) return Html::tag('span', 'OPTIONS', ['class' => 'label label-danger']);
                        return '';
                    }
                ],
                [
                    'header'  => 'uri',
                    'content' => function (\common\models\statistic\ServerRequest $item) {
                        $u = parse_url($item->uri);
                        return Html::a($u['path'], Url::to($u['path'], true));
                    }
                ],
                [
                    'header'  => 'get',
                    'content' => function (\common\models\statistic\ServerRequest $item) {
                        if ($item->id >= 256237) {
                            if (count(unserialize($item->get)) == 0) return '';

                            return Html::button(Html::tag('span', null, ['class' => 'glyphicon glyphicon-eye-open']), ['class' => 'btn btn-default btn-xs buttonGet', 'data' => ['id' => $item->id]]);
                        }
                        return \yii\helpers\VarDumper::dumpAsString($item->server, 10, true);
                    }
                ],

                [
                    'header'  => 'post',
                    'content' => function (\common\models\statistic\ServerRequest $item) {
                        if ($item->id >= 256237) {
                            if (count(unserialize($item->post)) == 0) return '';

                            return Html::button(Html::tag('span', null, ['class' => 'glyphicon glyphicon-eye-open']), ['class' => 'btn btn-default btn-xs buttonPost', 'data' => ['id' => $item->id]]);
                        }
                        return \yii\helpers\VarDumper::dumpAsString($item->server, 10, true);
                    }
                ],
                [
                    'header'  => 'server',
                    'content' => function (\common\models\statistic\ServerRequest $item) {
                        if ($item->id >= 256237) {
                            return Html::button(Html::tag('span', null, ['class' => 'glyphicon glyphicon-eye-open']), ['class' => 'btn btn-default btn-xs buttonServer', 'data' => ['id' => $item->id]]);
                        }
                        return \yii\helpers\VarDumper::dumpAsString($item->server, 10, true);
                    }
                ],
                [
                    'header'  => 'is_bot',
                    'content' => function (\common\models\statistic\ServerRequest $item) {
                        if ($item->id >= 256237) {
                            $server = unserialize($item->server);
                            if (!isset($server['HTTP_USER_AGENT'])) return 'Alarm';
                            $u = $server['HTTP_USER_AGENT'];
                            $result = \common\models\school\Page::isBot($u);

                            if ($result) return Html::tag('span', 'Да', ['class' => 'label label-success']);
                            return Html::tag('span', 'Нет', ['class' => 'label label-default']);
                        }
                        return '';
                    }
                ],
                [
                    'header'  => 'session_id',
                    'content' => function (\common\models\statistic\ServerRequest $item) {
                        return Html::tag('code', $item->session_id);
                    }
                ],
                [
                    'header'  => 'session',
                    'content' => function (\common\models\statistic\ServerRequest $item) {
                        if ($item->id >= 256237) {
                            if (unserialize($item->session) === false) return '';
                            return Html::button(Html::tag('span', null, ['class' => 'glyphicon glyphicon-eye-open']), ['class' => 'btn btn-default btn-xs buttonSession', 'data' => ['id' => $item->id]]);
                        }
                        return \yii\helpers\VarDumper::dumpAsString($item->server, 10, true);
                    }
                ],
                [
                    'header'  => 'log_time',
                    'content' => function (\common\models\statistic\ServerRequest $item) {
                        $t = (int)$item->log_time;

                        return Html::tag('abbr', \cs\services\DatePeriod::back($t, ['isShort' => true]), ['data' => ['toggle' => 'tooltip'], 'title' => Yii::$app->formatter->asDatetime($t)]);
                    }
                ],
                'ip',
                'user_id',
                [
                    'header'  => 'row_text',
                    'content' => function (\common\models\statistic\ServerRequest $item) {
                        if (\cs\Application::isEmpty(unserialize($item->row_text))) return '';

                        return Html::button(Html::tag('span', null, ['class' => 'glyphicon glyphicon-eye-open']), ['class' => 'btn btn-default btn-xs buttonRowText', 'data' => ['id' => $item->id]]);
                    }
                ],
                [
                    'header'  => 'headers',
                    'content' => function (\common\models\statistic\ServerRequest $item) {
                        if ($item->id >= 256237) {
                            return Html::button(Html::tag('span', null, ['class' => 'glyphicon glyphicon-eye-open']), ['class' => 'btn btn-default btn-xs buttonHeaders', 'data' => ['id' => $item->id]]);
                        }
                        return \yii\helpers\VarDumper::dumpAsString($item->headers, 10, true);
                    }
                ],

            ],
        ]) ?>
        <?php \yii\widgets\Pjax::end(); ?>
    </div>

</div>

<div class="modal fade" id="modalInfo" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Информация</h4>
            </div>
            <div class="modal-body">
                Успешно!
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
            </div>
        </div>
    </div>
</div>