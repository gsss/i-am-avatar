<?php

/** @var $url */
/** @var $image */
/** @var $title */
/** @var $summary */
/** @var $description */
/** @var $text */

use yii\helpers\Html;
use yii\bootstrap\ButtonDropdown;
use yii\helpers\Url;
use cs\services\Url as csUrl;

if (!isset($text)) {
    $text = 'Поделиться';
}
?>
<?= ButtonDropdown::widget([
    'containerOptions' => ['style' => 'margin-bottom:10px;'],
    'label'       => "<span class='fa fa-share-alt'></span> " . $text,
    'encodeLabel' => false,
    'dropdown'    => [
        'options'      => ['class' => 'pull-top pull-center'],
        'encodeLabels' => false,
        'items'        => [
            [
                'label'       => "Facebook",
                'linkOptions' => ['target' => '_blank'],
                'url'         => (string)(new csUrl('http://www.facebook.com/sharer.php', [
                    'u' => $url,
                ]))
            ],
            [
                'label'       => 'Vkontakte',
                'linkOptions' => ['target' => '_blank'],
                'url'         => (string)(new csUrl('http://vkontakte.ru/share.php', ['url' => $url]))
            ],

        ],
    ],
]) ?>