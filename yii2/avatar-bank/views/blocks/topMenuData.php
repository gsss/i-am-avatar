<?php

use yii\helpers\Url;
use yii\helpers\Html;


$menu = [];

$school = \common\models\school\School::get();

$design = \common\models\school\SchoolDesignMain::findOne(['school_id' => $school->id]);
if (!is_null($design)) {
    if (!\cs\Application::isEmpty($design->guest_frontend)) {
        return \yii\helpers\Json::decode($design->guest_frontend);
    }
}

$menu[] = [
    'label'   => Yii::t('c.oeVo5Oaket', 'Новости'),
    'route'   => 'news/index',
    'urlList' => [
        ['controller', 'news'],
    ]
];
$menu[] = [
    'label'   => Yii::t('c.oeVo5Oaket', 'Блог'),
    'route'   => 'blog/index',
    'urlList' => [
        ['controller', 'blog'],
    ]
];
$menu[] = [
    'label' => Yii::t('c.oeVo5Oaket', 'О нас'),
    'route' => 'site/about',
];
$menu[] = [
    'label'   => 'Школы',
    'route'   => 'school/list',
    'urlList' => [
        ['controller', 'school'],
    ]
];
$menu[] = [
    'label'   => 'Курсы',
    'route'   => 'kurs/index',
    'urlList' => [
        ['controller', 'kurs'],
    ]
];
$menu[] = [
    'label'   => 'Учителя',
    'route'   => 'user/list',
    'urlList' => [
        ['controller', 'user'],
    ]
];
$menu[] = [
    'label'   => 'Монеты',
    'route'   => 'coin-explorer/index',
    'urlList' => [
        ['controller', 'coin-explorer'],
    ]
];

$menu[] = [
    'label' => 'Тарифы',
    'route' => 'site/tarif',
];

$menu[] = [
    'label' => 'Эликсир',
    'route' => 'site/elixir',
];

$menu[] = [
    'label' => Yii::t('c.oeVo5Oaket', 'Контакты'),
    'route' => 'site/contact',
];
$menu[] = [
    'label'   => 'Календарь',
    'route'   => 'services/kolada-dar',
    'urlList' => [
        ['route', 'services/kolada-dar'],
        ['route', 'services/kolada-dar2'],
    ]
];

return $menu;
