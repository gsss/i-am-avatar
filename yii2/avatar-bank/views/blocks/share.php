<?php

/** @var $url */
/** @var $image */
/** @var $title */
/** @var $summary */
/** @var $description */
/** @var $text */
/** @var $this \yii\web\View */

use yii\helpers\Html;
use yii\bootstrap\ButtonDropdown;
use yii\helpers\Url;
use cs\services\Url as csUrl;

if (!isset($text)) {
    $text = Yii::t('c.FkZ5OTJRwK', 'Поделиться');
}

?>
<?= $this->render('shareMeta', [
    'image'       => $image,
    'url'         => $url,
    'title'       => $title,
    'description' => $description,
]) ?>
<?= $this->render('shareButton', [
    'image'       => $image,
    'url'         => $url,
    'title'       => $title,
    'description' => $description,
    'text'        => $text,
]) ?>