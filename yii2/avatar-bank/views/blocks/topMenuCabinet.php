<?php
use yii\helpers\Url;
use yii\helpers\Html;


$school = \common\models\school\School::get();

$design = \common\models\school\SchoolDesignMain::findOne(['school_id' => $school->id]);
if (!is_null($design)) {
    if (!\cs\Application::isEmpty($design->auth_backend)) {
        return \yii\helpers\Json::decode($design->auth_backend);
    }
}

$menu = [
    [
        'label'   => 'Мои сообщества',
        'url'     => '/cabinet-school/index',
        'urlList' => [
            ['startsWith', '/cabinet-school'],
        ],
    ],
    [
        'label'   => 'Мои курсы',
        'url'     => '/cabinet-kurs-list/index?id=1',
        'urlList' => [
            ['controller', 'cabinet-kurs-list'],
        ],
    ],
];
$menu[] = [
    'label'   => 'Календарь',
    'url'     => '/services/kolada-dar',
    'urlList' => [
        ['controller', 'services'],
    ],
];
$menu[] = [
    'label'   => 'Чат',
    'url'     => '/chat/index',
    'urlList' => [
        ['startsWith', '/chat'],
    ],
];
if (\common\models\school\CommandLink::find()->where(['user_id' => Yii::$app->user->id])->exists()) {
    $menu[] = [
        'label'   => 'Задачи',
        'url'     => '/cabinet-task-list/index',
        'urlList' => [
            ['controller', 'cabinet-task-list'],
        ],
    ];
}



?>

<?php
$rows = [];
foreach ($menu as $item) {
    $class = ['dropdown'];
    if (\avatar\services\Html1::isSelected20190516($item)) {
        $class[] = 'active';
    }
    if (\yii\helpers\ArrayHelper::keyExists('items', $item)) {
        $html = Html::tag(
            'li',
            Html::a(
                $item['label']
                .
                ' '
                .
                Html::tag('span', null, ['class' => 'caret'])
                ,
                '#'
                ,
                [
                    'class'         => "dropdown-toggle",
                    'data-toggle'   => "dropdown",
                    'role'          => "button",
                    'aria-expanded' => "false",
                ]
            )
            .
            Html::tag('ul',
                \avatar\services\Html1::liMenu20190516(\yii\helpers\ArrayHelper::map($item['items'], 'route', 'label'))
                , [
                    'class' => "dropdown-menu",
                    'role'  => "menu",
                ])
            ,
            ['class' => join(' ', $class)]);
    } else {
        $html = Html::tag(
            'li',
            Html::a($item['label'], $item['url']),
            ['class' => join(' ', $class)]
        );
    }

    $rows[] = $html;
}


?>
<?= join("\n", $rows) ?>
