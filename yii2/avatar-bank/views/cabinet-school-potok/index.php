<?php

/** @var $this \yii\web\View */
/** @var $school \common\models\school\School */

/** @var $kurs \common\models\school\Kurs */

use yii\helpers\Url;
use app\services\GsssHtml;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use app\services\GetArticle\Collection;

$this->title = 'Все потоки';

$potok = \common\models\school\Potok::findOne(['kurs_id' => $kurs->id]);
?>

<div class="container">

    <?php \avatar\widgets\SchoolMenu::begin(['school' => $school]) ?>
    <h1 class="page-header"><?= Html::encode($this->title) ?></h1>


    <p><a href="<?= Url::to(['cabinet-school-potok/add', 'id' => $kurs->id]) ?>" class="btn btn-default">Добавить</a>
    </p>

    <?php \yii\widgets\Pjax::begin(); ?>
    <?php
    $this->registerJS(<<<JS
$('.buttonLid').click(function(e) {
    e.preventDefault();
    e.stopPropagation();
    window.location = '/cabinet-school-potok/users' + '?' + 'id' + '=' + $(this).data('id');
});
$('.buttonSubscribe').click(function(e) {
    e.preventDefault();
    e.stopPropagation();
    window.location = '/cabinet-school-subscribe/index' + '?' + 'id' + '=' + $(this).data('id');
});
$('.buttonAutoVoronki').click(function(e) {
    e.preventDefault();
    e.stopPropagation();
    window.location = '/cabinet-school-auto-voronki/index' + '?' + 'id' + '=' + $(this).data('id');
});
$('.buttonReklama').click(function(e) {
    e.preventDefault();
    e.stopPropagation();
    window.location = '/cabinet-school-reklama/index' + '?' + 'id' + '=' + $(this).data('id');
});
$('.buttonSale').click(function(e) {
    e.preventDefault();
    e.stopPropagation();
    window.location = '/cabinet-school-potok-sale/index' + '?' + 'id' + '=' + $(this).data('id');
});
$('.buttonRequest').click(function(e) {
    e.preventDefault();
    e.stopPropagation();
    window.location = '/cabinet-school-potok-request/index' + '?' + 'id' + '=' + $(this).data('id');
});
$('.rowTable').click(function(e) {
    window.location = '/cabinet-school-potok/edit' + '?' + 'id' + '=' + $(this).data('id');
});
$('.buttonPage').click(function(e) {
    e.preventDefault();
    e.stopPropagation();
    var is_page = $(this).data('is_page');
    if (is_page == 0) {
        window.location = '/cabinet-school-pages/add' + '?' + 'id' + '=' + $(this).data('school_id') + '&' + 'potok_id' + '=' + $(this).data('id');
    } else {
        window.location = '/cabinet-school-pages/edit' + '?' + 'id' + '=' + $(this).data('page_id');
    } 
});
$('.buttonSMS').click(function (e) {
    e.preventDefault();
    e.stopPropagation();
    if (confirm('Подтвердите удаление')) {
        var button = $(this);
        var id = $(this).data('id');
        ajaxJson({
            url: '/cabinet-school-potok/sms-send',
            data: {
                id: $(this).data('id')
            },
            success: function (ret) {
                $('#modalInfo').on('hidden.bs.modal', function() {
                    button.remove();
                }).modal();
            }
        });
    }
});

JS
    );
    $cols = [
        'id',
        'name',
        'date_start',
        'date_end',
        [
            'header'  => 'Лиды',
            'content' => function ($item) {
                return Html::button('Лиды', [
                    'class' => 'btn btn-info btn-xs buttonLid',
                    'data'  => [
                        'id' => $item['id'],
                    ],
                ]);
            },
        ],
        [
            'header'  => 'Страница',
            'content' => function (\common\models\school\Potok $item) {
                $page = null;
                if (isset($item['page_id'])) {
                    if ($item['page_id']) {
                        $page = \common\models\school\Page::findOne($item['page_id']);
                    }
                }

                $data = [
                    'id'      => $item['id'],
                    'is_page' => (is_null($page)) ? 0 : 1,
                ];
                if (!is_null($page)) {
                    $data['page_id'] = $page->id;
                }
                $kurs = \common\models\school\Kurs::findOne($item['kurs_id']);
                $school = \common\models\school\School::findOne($kurs->school_id);
                $data['school_id'] = $school->id;

                if ($data['is_page'] == 1) {
                    return Html::tag('code', \common\helpers\Html::a($page->url, $page->getUrl(), ['target' => '_blank']));
                }

                return Html::button('Страница', [
                    'class' => 'btn btn-primary btn-xs buttonPage',
                    'data'  => $data,
                ]);
            },
        ],
        [
            'header'  => 'Рассылки',
            'content' => function ($item) {
                return Html::button('Рассылки', [
                    'class' => 'btn btn-info btn-xs buttonSubscribe',
                    'data'  => [
                        'id' => $item['id'],
                    ],
                ]);
            },
        ],
        [
            'header'  => 'Продажи',
            'content' => function ($item) {
                return Html::button('Продажи', [
                    'class' => 'btn btn-info btn-xs buttonSale',
                    'data'  => [
                        'id' => $item['id'],
                    ],
                ]);
            },
        ],
        [
            'header'  => 'Заявки на покупку',
            'content' => function ($item) {
                return Html::button('Заявки на покупку', [
                    'class' => 'btn btn-info btn-xs buttonRequest',
                    'data'  => [
                        'id' => $item['id'],
                    ],

                ]);
            },
        ],
    ];
    if (\Yii::$app->user->id == 9) {
        $cols[] = [
            'header'  => 'СМС рассылка',
            'content' => function ($item) {
                return Html::button('СМС рассылка', [
                    'class' => 'btn btn-info btn-xs buttonSMS',
                    'data'  => [
                        'id' => $item['id'],
                    ],
                ]);
            },
        ];
    }
    ?>
    <?= \yii\grid\GridView::widget([
        'dataProvider' => new \yii\data\ActiveDataProvider([
            'query'      => \common\models\school\Potok::find()
                ->where(['kurs_id' => $kurs->id])
            ,
            'pagination' => [
                'pageSize' => 20,
            ],
        ]),
        'tableOptions' => [
            'class' => 'table table-striped table-hover',
        ],
        'rowOptions'   => function ($item) {
            $data = [
                'data'  => ['id' => $item['id']],
                'role'  => 'button',
                'class' => 'rowTable',
            ];
            return $data;
        },
        'columns'      => $cols,
    ]) ?>
    <?php \yii\widgets\Pjax::end(); ?>
    <?php \avatar\widgets\SchoolMenu::end() ?>


</div>

<div class="modal fade" id="modalInfo" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="myModalLabel">Информация</h4>
            </div>
            <div class="modal-body">
                Успешно!
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
            </div>
        </div>
    </div>
</div>