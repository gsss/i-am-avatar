<?php

/** @var $this \yii\web\View */
/** @var $school \common\models\school\School */
/** @var $potok  \common\models\school\Potok */

use yii\helpers\Url;
use app\services\GsssHtml;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use app\services\GetArticle\Collection;

$this->title = $potok->name;

?>

<div class="container">

    <?php \avatar\widgets\SchoolMenu::begin(['school' => $school]) ?>
    <h1 class="page-header"><?= Html::encode($this->title) ?></h1>

    <hr>

    <?= \yii\widgets\DetailView::widget([
        'model'      => $potok,
        'attributes' => [
            [
                'label'          => 'Наименование',
                'captionOptions' => [
                    'style' => 'width: 30%',
                ],
                'value'          => $potok->name,
            ],
            'date_start:date:Дата начала',
            'date_end:date:Дата окончания',
        ],
    ]) ?>
    <p><a href="<?= Url::to(['cabinet-school-potok/users', 'id' => $potok->id]) ?>" class="btn btn-default">Лиды</a></p>
    <p><a href="<?= Url::to(['cabinet-school-subscribe/index', 'id' => $potok->id]) ?>" class="btn btn-default">Рассылки</a></p>
    <?php if (!\cs\Application::isEmpty($potok->page_id)) { ?>
        <p><a href="<?= Url::to(['cabinet-school-pages/view', 'id' => $potok->page_id]) ?>" class="btn btn-default">Страница</a></p>
        <?php $page = \common\models\school\Page::findOne($potok->page_id); ?>
        <p><a href="<?= $page->getUrl() ?>" target="_blank"><?= $page->url ?></a></p>
    <?php } ?>

    <?php \avatar\widgets\SchoolMenu::end() ?>


</div>

<div class="modal fade" id="modalInfo" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="myModalLabel">Информация</h4>
            </div>
            <div class="modal-body">
                Успешно!
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
            </div>
        </div>
    </div>
</div>