<?php

/** @var $this \yii\web\View */
/** @var $school \common\models\school\School */
/** @var $kurs \common\models\school\Kurs */
/** @var $potok \common\models\school\Potok */

use yii\helpers\Url;
use app\services\GsssHtml;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use app\services\GetArticle\Collection;

$this->title = 'Лиды';

$potok = \common\models\school\Potok::findOne(['kurs_id' => $kurs->id]);
?>

<div class="container">

    <?php \avatar\widgets\SchoolMenu::begin(['school' => $school]) ?>
    <h1 class="page-header"><?= Html::encode($this->title) ?></h1>



    <p><a href="<?= Url::to(['cabinet-school-potok/users-add', 'id' => $potok->id]) ?>"
          class="btn btn-default">Добавить</a>

        <?php \yii\widgets\Pjax::begin(); ?>
        <?php
        $this->registerJS(<<<JS
$('.buttonDelete').click(function (e) {
    e.preventDefault();
    e.stopPropagation();
    if (confirm('Подтвердите удаление')) {
        var button = $(this);
        var id = $(this).data('id');
        ajaxJson({
            url: '/cabinet-school-potok/users-delete' + '?' + 'id' + '=' + id,
            success: function (ret) {
                $('#modalInfo').on('hidden.bs.modal', function() {
                    button.parent().parent().remove();
                }).modal();
            }
        });
    }
});
$('[data-toggle="tooltip"]').tooltip();
JS
        );

        $model = new \avatar\models\search\UserPotok();
        $provider = $model->search(Yii::$app->request->get(), ['school_potok_user_root_link.potok_id' => $potok->id]);
        ?>
        <?= \yii\grid\GridView::widget([
            'dataProvider' => $provider,
            'filterModel'  => $model,
            'tableOptions' => [
                'class' => 'table table-striped table-hover',
            ],
            'rowOptions'   => function ($item) {
                $data = [
                    'data'  => ['id' => $item['id']],
                    'role'  => 'button',
                    'class' => 'rowTable',
                ];
                return $data;
            },
            'columns'      => [
                [
                    'attribute'     => 'id',
                    'headerOptions' => [
                        'style' => 'width: 7%',
                    ],
                ],
                [
                    'header'        => 'Статус',
                    'attribute'     => 'avatar_status',
                    'headerOptions' => [
                        'style' => 'width: 15%',
                    ],
                    'filter'        => [
                        0 => 'Не активирован',
                        1 => 'Гость подтвердил',
                        2 => 'Пользователь',
                    ],
                    'content'       => function ($item) {
                        $v = \yii\helpers\ArrayHelper::getValue($item, 'avatar_status', 0);
                        if ($v == 0) return Html::tag('span', 'Не активирован', ['class' => 'label label-default', 'data' => ['toggle' => 'tooltip'], 'title' => 'Не подтвердил свою почту после регистрации']);
                        if ($v == 1) return Html::tag('span', 'Гость подтвердил', ['class' => 'label label-info', 'data' => ['toggle' => 'tooltip'], 'title' => 'Подтвердил свою почту после регистрации, но не пользователь платформы']);
                        if ($v == 2) return Html::tag('span', 'Пользователь', ['class' => 'label label-success', 'data' => ['toggle' => 'tooltip'], 'title' => 'Зарегистрированный пользователь платформы']);

                        return '';
                    },
                ],
                [
                    'header'        => 'Почта',
                    'attribute'     => 'email',
                    'headerOptions' => [
                        'style' => 'width: 23%',
                    ],
                ],
                [
                    'header'        => 'Телефон',
                    'attribute'     => 'phone',
                    'headerOptions' => [
                        'style' => 'width: 15%',
                    ],
                ],
                [
                    'header'        => 'Имя',
                    'attribute'     => 'name',
                    'headerOptions' => [
                        'style' => 'width: 26%',
                    ],
                ],
                [
                    'header'        => 'Создан',
                    'headerOptions' => [
                        'style' => 'width: 7%',
                    ],
                    'content'       => function ($item) {
                        $v = \yii\helpers\ArrayHelper::getValue($item, 'created_at', 0);
                        if ($v == 0) return '';

                        return Html::tag('abbr', \cs\services\DatePeriod::back($v, ['isShort' => true]), ['data' => ['toggle' => 'tooltip'], 'title' => Yii::$app->formatter->asDatetime($v)]);
                    },
                ],
                [
                    'header'        => 'Удалить',
                    'headerOptions' => [
                        'style' => 'width: 7%',
                    ],
                    'content'       => function ($item) {
                        return Html::button('Удалить', [
                            'class' => 'btn btn-danger btn-xs buttonDelete',
                            'data'  => [
                                'id' => $item['id'],
                            ],
                        ]);
                    },
                ],
            ],
        ]) ?>
        <?php \yii\widgets\Pjax::end(); ?>
        <?php \avatar\widgets\SchoolMenu::end() ?>


</div>

<div class="modal fade" id="modalInfo" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="myModalLabel">Информация</h4>
            </div>
            <div class="modal-body">
                Успешно!
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
            </div>
        </div>
    </div>
</div>