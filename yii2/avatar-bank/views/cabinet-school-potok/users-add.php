<?php

/** @var $this \yii\web\View */
/** @var $model \avatar\models\forms\school\Lid */
/** @var $school \common\models\school\School */
/** @var $kurs \common\models\school\Kurs */
/** @var $potok \common\models\school\Potok */

use yii\bootstrap\ActiveForm;
use yii\helpers\Url;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;

$this->title = 'Добавить LID';

?>

<div class="container">

    <?php \avatar\widgets\SchoolMenu::begin(['school' => $school]) ?>
    <h1 class="page-header"><?= Html::encode($this->title) ?></h1>



    <?php if (!is_null($id = Yii::$app->session->getFlash('form'))) : ?>

        <div class="alert alert-success">
            Успешно добавлено.
        </div>

        <p>
            <a href="<?= \yii\helpers\Url::to(['cabinet-school-potok/users', 'id' => $potok->id]) ?>" class="btn btn-success">Все пользователи</a>
        </p>

    <?php else: ?>

        <div class="row">
            <div class="col-lg-8">
                <?php $form = ActiveForm::begin([
                    'options' => ['enctype' => 'multipart/form-data'],
                ]); ?>
                <?= $form->field($model, 'name') ?>
                <?= $form->field($model, 'email') ?>
                <?= $form->field($model, 'phone') ?>

                <hr>
                <div class="form-group">
                    <?= Html::submitButton('Добавить', [
                        'class' => 'btn btn-default',
                        'name'  => 'contact-button',
                        'style' => 'width:100%',
                    ]) ?>
                </div>
                <?php ActiveForm::end(); ?>
            </div>
        </div>

    <?php endif; ?>

    <?php \avatar\widgets\SchoolMenu::end() ?>


</div>
