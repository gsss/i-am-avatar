<?php


/* @var $this yii\web\View */

$this->title = 'Проект «РАЙ». Проект новой Золотой Эры Человечества';

$whitePaperLink = \Yii::t('c.zlOfQkItiW', 'ссылка на WP');
?>

<!--  First  -->
<div class="menu-container" style="visibility: hidden;">
    <span class="menu-close"></span>
    <ul class="menu">
        <li><a class="inner-link" href="#solutions"><?= \Yii::t('c.qnnk6XQWp0', 'Наши решения') ?></a></li>
        <li><a class="inner-link" href="#avatarpay-system"><?= \Yii::t('c.qnnk6XQWp0', 'Система AvatarPay') ?></a></li>
        <li><a class="inner-link" href="#dnk-passport"><?= \Yii::t('c.qnnk6XQWp0', 'ДНК пасспорт') ?></a></li>
        <li><a class="inner-link" href="#avatar-map"><?= \Yii::t('c.qnnk6XQWp0', 'Карта Аватара') ?></a></li>
        <li><a class="inner-link" href="#roadmap"><?= \Yii::t('c.qnnk6XQWp0', 'Дорожная карта') ?></a></li>
        <li><a class="inner-link" href="#preico"><?= \Yii::t('c.qnnk6XQWp0', 'preICO') ?></a></li>
        <li><a class="inner-link" href="#contacts"><?= \Yii::t('c.qnnk6XQWp0', 'Контакты') ?></a></li>
    </ul>
</div>


<section class="top-layer" style="background: url('/images/controller/landing/index/bg/bg123.png') no-repeat 50%;">
    <header>
        <div class="container">
            <div class="row align-items-center">
                <div class="col header-contain">
                    <div class="lang-switcher-container">
                        <div class="avatar-btn--header-white lang-switcher"><?= strtoupper(Yii::$app->language) ?></div>
                        <div class="lang-choose-layer">
                            <ul>
                                <li><a href="javascript:void(0);" class="buttonLanguageSet" data-code="ru">Ru</a></li>
                                <li><a href="javascript:void(0);" class="buttonLanguageSet" data-code="en">En</a></li>
                            </ul>
                        </div>
                    </div>

                    <a href="/auth/login" class="avatar-btn--header-login"><?= \Yii::t('c.zlOfQkItiW', 'Войти') ?></a>
                </div>
            </div>
        </div>
    </header>
    <div class="container full-height">
        <div class="row top-layer__row full-height justify-content-center align-items-center">
            <div class="col text-center top-layer-col">
                <?php if (Yii::$app->deviceDetect->isMobile()) { ?>

                    <div class="top-layer__logo reveal">
                        <img src="/images/controller/landing-new-earth/index/transparent.png" alt="logo" style="margin-bottom: 0px;" width="150">
                    </div>

                    <p style="color: #000; font-size: 200%;"><?= Yii::t('c.wUlUghXGiM', 'Заголовок') ?></p>
                <?php } else { ?>
                    <div class="top-layer__logo reveal">
                        <img src="/images/controller/landing-new-earth/index/transparent.png" alt="logo" style="margin-bottom: 0px;" width="500">
                    </div>

                    <p style="color: #000; font-size: 200%;"><?= Yii::t('c.wUlUghXGiM', 'Заголовок') ?></p>
                <?php } ?>

                <a href="/rai-akcioneri/index" class="avatar-btn avatar-btn--white reveal">АО «Золотая платформа» →</a>

            </div>
        </div>
    </div>
</section>




<section id="contacts" class="main-gradient contacts section-padding-sm color-white">
    <div class="container reveal">
        <div class="row">
            <div class="col-12">
                <h2 class="text-header-sm text-center"><?= \Yii::t('c.zlOfQkItiW', 'Рассматриваем ваши предложения по сотрудничеству') ?></h2>
            </div>
        </div>
        <form class="contacts__form" action="">
            <?= \yii\helpers\Html::hiddenInput('_csrf', Yii::$app->request->csrfToken) ?>
            <div class="row">
                <div class="col-12 col-md-5 offset-md-1">
                    <input type="text" placeholder="<?= \Yii::t('c.zlOfQkItiW', 'Ваше имя') ?>" name="name">
                </div>
                <div class="col-12 col-md-5">
                    <input type="text" placeholder="Email" name="email">
                </div>
                <div class="col-12 col-md-10 offset-md-1">
                    <textarea name="text" placeholder="<?= \Yii::t('c.zlOfQkItiW', 'Сообщение') ?>" cols="30" rows="5"></textarea>

                    <?php if (Yii::$app->language == 'ru') { ?>
                        <p class="agreement-text text-center">Нажимая "Отправить", вы даёте согласие на обработку своих персональных данных в соответствии с Федеральным законом №152-ФЗ «О персональных данных» и принимаете условия
                            <a href="javascript:void(0);" target="_blank" class="text-bold"><?= \Yii::t('c.zlOfQkItiW', 'Пользовательского соглашения') ?></a>
                        </p>
                    <?php } ?>


                    <div class="text-right">
                        <input type="button" value="<?= \Yii::t('c.zlOfQkItiW', 'связаться с нами') ?>" class="avatar-btn avatar-btn--white popup-open-mail" data-form="success" style="border: 0">
                    </div>
                </div>
            </div>
        </form>
    </div>
</section>
<footer class="section-padding-lg">
    <div class="container">
        <div class="row">
            <div class="col-12 col-md-4 footer__col">
            </div>
            <div class="col-12 col-md-4 footer__col">
                <div class="footer-soc">
                    <span><?= \Yii::t('c.zlOfQkItiW', 'Мы в соц сетях') ?></span><br><br>
                    <a target="_blank" href="https://www.youtube.com/channel/UC5_n6x3PFK_pA0w2So6lciQ"><img src="/images/controller/landing/index/media.png" alt="vk" width="43"></a>
                    <a target="_blank" class="vk" href="https://www.facebook.com/AvatarNetwork.io/"><img src="/images/controller/landing/index/fb.png" alt="vk"></a>
                    <a target="_blank" class="vk" href="https://t.me/AvatarNetwork"><img src="/network/assets/images/Telegram.png" alt="Telegram"></a>
                    <a target="_blank" href="https://twitter.com/Avatarnetworkio"><img src="https://pngimg.com/uploads/twitter/twitter_PNG8.png" alt="Tweeter" width="43"></a>
                </div>
            </div>
            <div class="col-12 col-md-4 copy footer__col">
                <p>&copy; Copyright 2017</p>
            </div>
        </div>
    </div>
</footer>
<div class="avatar__overlay"></div>
<div class="currency__popup">
    <div class="currency__popup-inner" style="width: 500px;">

    </div>
</div>

<script>
    var path = document.getElementsByClassName('desicion-svg-path');

    for (i=0;i < path.length;i++){
        path[i].style.strokeDasharray = path[i].getTotalLength();
        path[i].style.strokeDashoffset = path[i].getTotalLength();
    }
</script>
<script src="/network/assets/js/jquery-3.2.1.min.js"></script>
<script src="/network/assets/js/countdown.js"></script>
<script src="/network/assets/js/scrollreveal.min.js"></script>
<script src="/network/assets/js/wow.min.js"></script>
<script>
    <?php $time = (new DateTime('2017-12-01 00:00:00'))->format('U') * 1000 ?>
    new WOW().init();
    jQuery(document).ready(function ($) {
        $('#pageTimer').countdown(new Date(<?= $time ?>), function(event) {
            var htmlOut;
            htmlOut = '<div class="countdown-item">' +
                '<span class="countdown-num">'+event.strftime('%D')+'</span>' +
                '<p class="countdown-stamp">дней</p>' +
                '</div>' +
                '<div class="countdown-item">' +
                '<span class="countdown-num">'+event.strftime('%H')+'</span>' +
                '<p class="countdown-stamp">часов</p>' +
                '</div>' +
                '<div class="countdown-item">' +
                '<span class="countdown-num">'+event.strftime('%M')+'</span>' +
                '<p class="countdown-stamp">минут</p>' +
                '</div>' +
                '<div class="countdown-item">' +
                '<span class="countdown-num">'+event.strftime('%S')+'</span>' +
                '<p class="countdown-stamp">секунд</p>' +
                '</div>';

            $(this).html(htmlOut);
        });
    });
</script>
<script src="/network/assets/js/slick/slick.min.js"></script>
<script src="/network/assets/js/main.js?ver=1.0.3311"></script>

<?= $this->render('../blocks/shareMeta', [
    'image'       => \yii\helpers\Url::to('/images/controller/landing/index/share2.png', true),
    'title'       => 'AvatarNetwork',
    'url'         => \yii\helpers\Url::current([], true),
    'description' => \Yii::t('c.zlOfQkItiW', 'Технологии доверия в свободных платежах', [], 'en'),
]) ?>