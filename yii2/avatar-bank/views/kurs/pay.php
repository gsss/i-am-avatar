<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;
use app\models\UnionCategory;
use yii\db\Query;
use yii\helpers\ArrayHelper;

/** @var $this yii\web\View */
/** @var $request \common\models\school\SchoolSaleRequest */

$this->title = 'Оплата заявки #' . $request->id;

/** @var \common\models\PaySystemConfig $config */
$config = \common\models\PaySystemConfig::findOne($request->ps_config_id);

$class = $config->getClass();

$billing = \common\models\BillingMain::findOne($request->billing_id);

?>
<div class="container">
    <div class="col-lg-12">
        <h1 class="page-header"><?= Html::encode($this->title) ?></h1>

        <?= $class->getForm(
            $billing,
            'Оплата заявки #' . $request->id,
            'Получатель: ' . $request->getSale()->getSchool()->name,
            [
                'action'     => 'saleKurs.' . $request->id,
                'successURL' => \yii\helpers\Url::to(['kurs/success', 'id' => $request->id]),
            ]
        ); ?>
    </div>
</div>
