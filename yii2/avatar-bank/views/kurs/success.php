<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;
use app\models\UnionCategory;
use yii\db\Query;
use yii\helpers\ArrayHelper;

/** @var $this yii\web\View */
/** @var $request \common\models\school\SchoolSaleRequest */

$this->title = 'Оплата прошла успешно';

?>
<div class="container">
    <div class="col-lg-12">
        <h1 class="page-header text-center"><?= Html::encode($this->title) ?></h1>

        <div class="col-lg-4">
        </div>
        <div class="col-lg-4">
            <p class="alert alert-success lead text-center">Оплата прошла успешно. Вам придет уведомление по почте о начале встречи.</p>
        </div>
    </div>
</div>

