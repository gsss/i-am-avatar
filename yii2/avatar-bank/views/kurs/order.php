<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;
use app\models\UnionCategory;
use yii\db\Query;
use yii\helpers\ArrayHelper;

/** @var $this yii\web\View */
/** @var $form yii\bootstrap\ActiveForm */
/** @var $model \avatar\models\forms\school\KursSale */

$this->title = 'Покупка курса';

/** @var \common\models\school\SchoolSale $sale */
$sale = \common\models\school\SchoolSale::findOne($model->id);

$currency = \common\models\avatar\Currency::findOne($sale->currency_id);

$paySystems = \common\models\PaySystem::find()
    ->where(['currency' => $currency->code])
    ->select('id')
    ->column();

$config = \common\models\PaySystemConfig::find()
    ->where(['school_id' => $sale->school_id])
    ->andWhere(['paysystem_id' => $paySystems]);

?>
<div class="container">
    <div class="col-lg-12">
        <h1 class="page-header text-center"><?= Html::encode($this->title) ?></h1>
    </div>


    <?php if (Yii::$app->session->hasFlash('form')) : ?>

        <div class="col-lg-12">
            <p class="alert alert-success">
                Успешно добавлено. Перехожу к клатежной системе
            </p>
        </div>


        <?php // Перехожу к платежной системе ?>
        <?php $request_id = Yii::$app->session->getFlash('form') ?>
        <?php
        $this->registerJs(<<<JS
window.location.href = '/kurs/pay?id=' + {$request_id}
JS
        );
        ?>


    <?php else: ?>

        <?php
        $this->registerJs(<<<JS
if ($('.field-kurssale-psconfig table tbody tr').length == 1) {
    var tr = $('.field-kurssale-psconfig table tbody tr')[0];
    var radio = $(tr).find('input[type="radio"]');
    radio.prop("checked", "checked");
}
JS
        );
        ?>
        <div class="col-lg-8 col-lg-offset-2">
            <?php $form = ActiveForm::begin([
                'options' => ['enctype' => 'multipart/form-data'],
            ]); ?>
            <?= Html::activeHiddenInput($model, 'id') ?>
            <?= $form->field($model, 'name') ?>
            <?= $form->field($model, 'phone') ?>
            <?= $form->field($model, 'email') ?>
            <?= $form->field($model, 'psConfig')->widget('\avatar\widgets\PaySystemList', ['rows' => $config]) ?>


            <hr>
            <div class="form-group">
                <?= Html::submitButton('Купить', [
                    'class' => 'btn btn-default',
                    'name'  => 'contact-button',
                    'style' => 'width:100%',
                ]) ?>
            </div>
            <?php ActiveForm::end(); ?>
        </div>

    <?php endif; ?>

</div>
