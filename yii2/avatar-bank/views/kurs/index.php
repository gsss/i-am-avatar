<?php
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $user \common\models\UserAvatar */

$this->title = 'Курсы';
?>

<div class="container" style="padding-bottom: 70px;">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header text-center"><?= $this->title ?></h1>

            <?php \yii\widgets\Pjax::begin(); ?>
            <?php
            $this->registerJS(<<<JS
$('[data-toggle="tooltip"]').tooltip();

$('.rowTable').click(function() {
    window.location = '/kurs/' +  $(this).data('id');
});
$('.buttonPage').click(function(e) {
    e.preventDefault();
    e.stopPropagation();
    window.location = $(this).data('href');
});
JS
            );
            ?>
            <?= \yii\grid\GridView::widget([
                'dataProvider' => new \yii\data\ActiveDataProvider([
                    'query'      => \common\models\school\Kurs::find()
                        ->where(['school_kurs.is_show' => 1])
                        ->andWhere([
                            'or',
                            ['school_potok.date_end' => null],
                            ['>=', 'school_potok.date_end', date('Y-m-d')],
                        ])
                        ->innerJoin('school_potok', 'school_potok.kurs_id = school_kurs.id')
                        ->innerJoin('school', 'school.id = school_kurs.school_id')
                        ->select([
                            'school_kurs.*',
                            'school_potok.date_start',
                            'school_potok.date_end',
                            'school_potok.page_id',
                            'school.name as school_name',
                            'school.image as school_image',
                        ])
                        ->asArray()
                    ,
                    'pagination' => [
                        'pageSize' => 20,
                    ],
                    'sort'       => [
                        'defaultOrder' => ['created_at' => SORT_DESC],
                    ],
                ]),
                'tableOptions' => [
                    'class' => 'table table-striped table-hover',
                ],
                'rowOptions'   => function ($item) {
                    $data = [
                        'data'  => ['id' => $item['id']],
                        'role'  => 'button',
                        'class' => 'rowTable',
                    ];
                    return $data;
                },
                'columns'      => [
                    'id',
                    [
                        'header'  => '',
                        'content' => function ($item) {
                            $i = ArrayHelper::getValue($item, 'school_image', '');
                            if ($i == '') return '';

                            return Html::img($i, [
                                'class'  => "img-circle",
                                'width'  => 80,
                                'height' => 80,
                                'style'  => 'margin-bottom: 0px;',
                            ]);
                        },
                    ],
                    [
                        'header'    => 'Школа',
                        'attribute' => 'school_name',
                    ],
                    [
                        'header'  => '',
                        'content' => function ($item) {
                            $i = ArrayHelper::getValue($item, 'image', '');
                            if ($i == '') return '';

                            return Html::img(\common\widgets\FileUpload7\FileUpload::getFile($i, 'crop'), [
                                'class'  => "img-circle",
                                'width'  => 80,
                                'height' => 80,
                                'style'  => 'margin-bottom: 0px;',
                            ]);
                        },
                    ],
                    [
                        'header'    => 'Название',
                        'attribute' => 'name',
                    ],
                    'date_start:date:Начало',
                    'date_end:date:Окончание',
                    [
                        'header'  => 'Страница',
                        'content' => function ($item) {
                            $i = ArrayHelper::getValue($item, 'page_id', '');
                            if ($i == '') return '';
                            $page = \common\models\school\Page::findOne($i);
                            $href = $page->getUrl();


                            return Html::button('Страница', [
                                'class'  => "btn btn-default buttonPage",
                                'data' => [
                                    'id' => $i,
                                    'href' => $href,
                                ],
                            ]);
                        },
                    ],
                ],
            ]) ?>
            <?php \yii\widgets\Pjax::end(); ?>

        </div>
    </div>
</div>


