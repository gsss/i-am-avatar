<?php

use yii\helpers\Html;
use yii\helpers\Url;


/** @var \common\models\school\Kurs $item */


/** @var \yii\web\View $this */
$this->title = \yii\helpers\Html::encode($item->name);


\common\assets\iLightBox\Asset::register($this);
$this->registerJs(<<<JS
$('.ilightbox').iLightBox();
JS
);
\avatar\assets\Paralax::register($this);


$potokList = \common\models\school\Potok::find()->where(['kurs_id' => $item->id])->all();
if (count($potokList) == 0) throw new \cs\web\Exception('Курс не содержит потока');
/** @var \common\models\school\Potok $potok */
$potok = $potokList[count($potokList) - 1];
?>

<div class="parallax-window"
     data-parallax="scroll"
     data-image-src="https://cloud1.cloud999.ru/upload/cloud/15609/46797_HR6Ov29Cgz.jpg"

     style="min-height: 450px; background: transparent;">

    <div class="container">
        <div class="col-sm-4 col-sm-offset-4">
            <?php if (!\cs\Application::isEmpty($item->image)) { ?>
                <p style="padding-top: 70px; margin-bottom: 0px;"><img
                            src="<?= \common\widgets\FileUpload7\FileUpload::getFile($item->image, 'crop') ?>"
                            class="img-circle" width="100%"
                            style="text-shadow: 0px 10px 20px rgba(0, 0, 0, 0.5);color:white;"></p>
            <?php } ?>

        </div>
    </div>
</div>
<style>
    .iconCodon {
        font-size: 24px;
    }

    .tdCenter {
        text-align: center;
    }

    .thumbnail2 {
        border-radius: 5px;
        border: 1px solid #888;
    }
</style>
<div class="container" style="padding-bottom: 70px;">
    <div class="col-lg-12">
        <h1 class="page-header text-center">
            <?= \yii\helpers\Html::encode($item->name) ?>
        </h1>
    </div>


    <!-- Описание -->
    <div class="col-lg-12">
        <h1 class="page-header text-center">
            Описание
        </h1>
    </div>
    <div class="row">
        <div class="col-lg-8 col-lg-offset-2">
            <?= \yii\widgets\DetailView::widget([
                'model'      => $potok,
                'options'    => ['class' => 'table table-striped table-hover detail-view'],
                'attributes' => [
                    'date_start' => ['label' => 'Начало', 'value' => $potok->date_start],
                    'date_end'   => ['label' => 'Окончание', 'value' => $potok->date_end],
                ],
            ]) ?>
        </div>
    </div>

    <!-- Для кого -->
    <div class="col-lg-12">
        <h1 class="page-header text-center">
            Для кого
        </h1>
    </div>
    <div class="row">
        <div class="col-lg-8 col-lg-offset-2">
            <?= nl2br($item->for_who) ?>
        </div>
    </div>

    <!-- Что дает курс -->
    <div class="col-lg-12">
        <h1 class="page-header text-center">
            Что дает курс
        </h1>
    </div>
    <div class="row">
        <div class="col-lg-8 col-lg-offset-2">
            <?= nl2br($item->daet) ?>
        </div>
    </div>

    <div class="col-lg-12">
        <h1 class="page-header text-center">
            Автор
        </h1>
    </div>
</div>

<?php

$user = \common\models\UserAvatar::findOne($item->getSchool()->user_id);
$userMaster = \common\models\UserMaster::findOne(['user_id' => $user->id]);

$certList = \common\models\school\Sertificate::find()->where(['user_id' => $user->id])->all();

$rows = (count($certList) + 2) / 3;
$rows = (int)$rows;
?>
<div class="parallax-window"
     data-parallax="scroll"
     data-image-src="https://www.i-am-avatar.com/upload/cloud/1546392582_SLqjARoGrO.jpg"

     style="min-height: 450px; background: transparent;">

    <div class="container">
        <div class="col-sm-4 col-sm-offset-4">
            <p style="padding-top: 70px; margin-bottom: 0px;"><img
                        src="<?= $user->getAvatar() ?>"
                        class="img-circle" width="100%"
                        style="text-shadow: 0px 10px 20px rgba(0, 0, 0, 0.5);color:white;"></p>
        </div>
    </div>

</div>
<div class="container" style="padding-bottom: 70px;">
    <div class="col-lg-12">
        <h1 class="page-header text-center">
            <?= \yii\helpers\Html::encode($user->getName2()) ?>
        </h1>
        <p class="text-center"><a class="btn btn-success" href="<?= Url::to(['user/item', 'id' => $user->id]) ?>">Подробнее о мастере</a></p>
    </div>
</div>


<?php
$data = '{
  "header": "Запишитесь на курс обучения",
  "button": {
    "text": "Подписаться"
  },
  "items": [
    {
      "name": "email",
      "type": "email",
      "placeholder": "E-mail"
    },
    {
      "name": "name",
      "type": "name",      
      "placeholder": "Имя"
    },
    {
      "name": "phone",
      "type": "phone",
      "placeholder": "Телефон"
    }
  ]
}';

/** @var \yii\web\View $this */
/** @var string $data json */
/** @var \common\models\school\PageBlockContent $blockContent */

$object = \yii\helpers\Json::decode($data);
$rows = count($object['items']);

\avatar\assets\Notify::register($this);

$this->registerJs(<<<JS

// снимает ошибочные признаки поля при фокусе
$('.text-input').on('focus', function() {
    var o = $(this);
    var p = o.parent();
    if (p.hasClass('input-group')) {
        p = p.parent();
    }
    p.removeClass('has-error');
    p.find('p.help-block-error').hide();
});

var functionButtonSendEth = function() {
    var b = $(this);
    b.off('click');
    var buttonText = b.html();
    b.html($('<i>', {class: 'fa fa-spinner fa-spin fa-fw'}));
    b.attr('disabled', 'disabled');
    var formId = b.data('form'); 
    var id = b.data('id'); 
    
    ajaxJson({
        url: '/pages/form',
        data: $('#form1').serializeArray(),
        success: function(ret) {
            b.on('click', functionButtonSendEth);
            b.html(buttonText);
            b.removeAttr('disabled');
            if (ret.result == 1) {
                $('#collapse_alert_' + id).find('p.text2019').html('Пройдите на свою почту и подтвердите свою регистрацию. Мы вам выслали письмо от iAvatar.');
            }
            if (ret.result == 2) {
                $('#collapse_alert_' + id).find('p.text2019').html('Вы успешно подписались на событие.');
            }
            if (ret.result == 3) {
                $('#collapse_alert_' + id).find('p.text2019').html('Вы успешно подписались на событие');
            }
            $('#collapse_form_' + id).collapse('hide');
            $('#collapse_alert_' + id).collapse('show');
            
        },
        errorScript: function(ret) {
            switch (ret.id) {
                case 101:
                    alert('Ошибка');
                    break;
                case 102:
                    var f = $('#form1');
                    $.each(ret.data,function(i,v) {
                        var name = v.name;
                        var value = v.value;
                        var t = f.find('.field-' + name);
                        t.addClass('has-error');
                        t.find('p.help-block-error').html(value.join('<br>')).show();
                    });

                    break;
            }
            b.on('click', functionButtonSendEth);
            b.html(buttonText);
            b.removeAttr('disabled');
        },
        error: function(e) {
            b.on('click', functionButtonSendEth);
            b.html(buttonText);
            b.removeAttr('disabled');
            new Noty({
                timeout: 3000,
                theme: 'sunset',
                type: 'error',
                layout: 'bottomLeft',
                text: 'Произошла ошибка'
            }).show();
        }
    });
};
$('.buttonSend').click(functionButtonSendEth);

JS
);


?>
<div class="container">

    <div class="col-lg-12">
        <h1 class="text-center page-header"><?= $object['header'] ?></h1>
    </div>

    <div class="col-lg-12">
        <div class="row" style="margin-bottom: 50px;">
            <div class="col-lg-6 col-lg-offset-3">

                <div class="collapse in" id="collapse_form_1">
                    <form id="form1">
                        <input type="hidden" name="_csrf" value="<?= Yii::$app->request->csrfToken ?>">
                        <input type="hidden" name="potok_id" value="<?= $potok->id ?>">
                        <?php for ($i = 0; $i < $rows; $i++) { ?>
                            <div class="form-group required field-<?= $object['items'][$i]['name'] ?>">
                                <label class="control-label"
                                ><?= $object['items'][$i]['placeholder'] ?></label>
                                <input type="text"
                                       class="form-control text-input" name="<?= $object['items'][$i]['name'] ?>"
                                       aria-required="true"
                                       aria-invalid="true">
                                <p class="help-block help-block-error"></p>
                            </div>
                        <?php } ?>
                        <hr>
                        <div class="form-group">
                            <button type="submit"
                                    class="btn btn-success buttonSend"
                                    name="contact-button"
                                    style="width:100%"
                                    data-id="1"
                                    data-form="form_1"
                            ><?= $object['button']['text'] ?></button>
                        </div>
                    </form>
                </div>
                <div class="collapse" id="collapse_alert_1">
                    <p class="text-center">
                        <img src="//www.i-am-avatar.com/images/mail.png" width="100">
                    </p>
                    <p class="text-center lead text2019">
                        Пройдите на свою почту и подтвердите свою регистрацию.<br>Мы вам выслали письмо от iAvatar.
                    </p>
                </div>
            </div>
        </div>
    </div>

    <?php if (!\cs\Application::isEmpty($item->social_net_image)) {  ?>
        <div class="col-lg-8 col-lg-offset-2">

            <hr>

            <?= $this->render('../blocks/share', [
                'image'       => $item->social_net_image,
                'title'       => $item->social_net_title,
                'url'         => Url::current([], true),
                'description' => $item->social_net_description,

            ]) ?>
        </div>
    <?php  } ?>

</div>