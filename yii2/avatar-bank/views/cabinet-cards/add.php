<?php

use common\models\avatar\Currency;
use common\models\avatar\UserBill;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $card \common\models\Card */

$this->title = 'Карта ' . $card->getNumbberWithSpace();

$currencyBTC = Currency::BTC;
$currencyETH = Currency::ETH;

$str1 = 'Вы уверены?';


$this->registerJs(<<<JS
$('.rowTable').click(function() {
    window.location = '/cabinet-wallet/item?id=' + $(this).data('id');
});





$('.buttonAdd').click(function(e) {
    e.preventDefault();
    e.stopPropagation();
    var b = $(this);
    if (confirm('{$str1}')) {
        ajaxJson({
            url: '/cabinet-cards/link',
            data: {
                billing_id: b.data('id'),
                card_id: {$card->id}
            },
            success: function(ret) {
                b.parent().parent().remove();
                $('#modalInfo').modal();
            }
        });
    }
});

JS
);
?>


<div class="modal fade" tabindex="-1" role="dialog" id="modalInfo">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Уведомление</h4>
            </div>
            <div class="modal-body">
                <p>Успешно</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal"><?= \Yii::t('c.TsqnzVaJuC', 'Закрыть') ?></button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div><!-- /.modal -->

<div class="container" style="padding-bottom: 70px;">
    <div class="col-lg-12">
        <h1 class="page-header text-center">
            <?= $this->title ?>
        </h1>
    </div>
    <div class="row">
        <?php /** @var $card \common\models\Card */ ?>
        <div class="col-lg-4 col-lg-offset-4">
            <p>
                <a href="<?= Url::to(['cabinet-cards/card', 'id' => $card->id]) ?>">
                    <img src="<?= $card->getDesign()->image ?>" width="100%">
                </a>
            </p>
            <p class="lead text-center"><code><?= $card->getNumbberWithSpace() ?></code></p>

        </div>
    </div>

    <div class="row">
        <div class="col-lg-12">
            <?php $currencyList = $card->getBillingListObject()->select(['currency'])->column() ?>
            <?php
            $columns = [
                [
                    'header'  => '',
                    'content' => function ($item) {
                        $currency = \yii\helpers\ArrayHelper::getValue($item, 'currency', Currency::BTC);
                        $title = '';
                        switch ($currency) {
                            case Currency::BTC:
                                $src = '/images/controller/cabinet-bills/transactions/btc.jpg';
                                $title = 'BitCoin (BTC)';
                                break;
                            case Currency::ETH:
                                $src = '/images/controller/cabinet-bills/transactions/eth.png';
                                $title = 'Ethereum (ETH)';
                                break;
                            default:
                                $currencyObject = Currency::findOne($currency);
                                if (is_null($currencyObject)) {
                                    throw new \yii\base\Exception('Не указана валюта у счета');
                                }
                                $src = $currencyObject->image;
                                $title = $currencyObject->title . ' '. '(' . $currencyObject->code . ')';
                                break;
                        }
                        $imgOptions = [
                            'width' => 50,
                            'class' => 'img-circle',
                            'data'  => [
                                'toggle' => 'tooltip',
                                'title'  => $title,
                            ],
                        ];

                        $passwordType = \yii\helpers\ArrayHelper::getValue($item, 'password_type', UserBill::PASSWORD_TYPE_OPEN);
                        if ($passwordType == UserBill::PASSWORD_TYPE_HIDE_CABINET) {
                            /** @var \common\models\UserAvatar $user */
                            $user = Yii::$app->user->identity;
                            if ($user->wallets_is_locked) {
                                $imgOptions['style']['opacity'] = '0.3';
                            }
                        }

                        if (isset($imgOptions['style'])) {
                            if (is_array($imgOptions['style'])) {
                                $s = [];
                                foreach ($imgOptions['style'] as $key => $value) {
                                    $s[] = $key . ':' . $value . ';';
                                }
                                $imgOptions['style'] = join(' ', $s);
                            }
                        }

                        return Html::img($src, $imgOptions);
                    },
                ],
            ];

            if (!YII_ENV_PROD) {
                $columns[] = [
                    'header'  => '',
                    'content' => function ($item) {
                        $passwordType = \yii\helpers\ArrayHelper::getValue($item, 'password_type', UserBill::PASSWORD_TYPE_OPEN);
                        if ($passwordType == UserBill::PASSWORD_TYPE_HIDE_CABINET) {
                            /** @var \common\models\UserAvatar $user */
                            $user = Yii::$app->user->identity;
                            if ($user->wallets_is_locked) {
                                $color = '#d9534f';
                                return Html::tag('i', null, [
                                    'class' => 'fa fa-key',
                                    'style' => 'color: ' . $color . ';',
                                    'title' => 'Пароль от кабинета сменен, доступ к кошелькам нужно восстановить',
                                    'data'  => ['toggle' => 'tooltip'],
                                ]);
                            }
                        }
                    },
                ];
            }

            $columns[] = [
                'header'    => 'WID',
                'content'   => function (\common\models\avatar\UserBill $item) {
                    $currency = $item->currency;
                    if (!\common\models\avatar\CurrencyLink::find()->where(['currency_ext_id' => $currency])->exists()) {
                        return '';
                    }
                    $v = \yii\helpers\ArrayHelper::getValue($item, 'address');
                    if (is_null($v)) return '';
                    $w = new \common\models\piramida\Wallet(['id' => $v]);
                    $addressShort = $w->getAddressShort();

                    return Html::tag('code', $addressShort);
                },
            ];

            $columns = \yii\helpers\ArrayHelper::merge($columns, [
                [
                    'header' => Yii::t('c.TsqnzVaJuC', 'Наименование'),
                    'attribute' => 'name',
                ],
                [
                    'header'         => Yii::t('c.TsqnzVaJuC', 'Баланс'),
                    'contentOptions' => function ($item) {
                        return [
                            'class' => 'rowBill',
                            'id'    => 'bill_confirmed_' . $item['id'],
                            'data'  => ['id' => $item['id']],
                            'style' => 'text-align: right;',
                        ];
                    },
                    'content'        => function ($item) {
                        $wallet_id = $item['address'];
                        $wallet = \common\models\piramida\Wallet::findOne($wallet_id);
                        $currency = Currency::findOne($item['currency']);

                        return Yii::$app->formatter->asDecimal($wallet->amount / 100, $currency->decimals);
                    },
                ],
            ]);

            $columns[] = [
                'header'  => 'Прикрепить',
                'content' => function ($item) {
                    if ($item['currency'] == Currency::BTC) {
                        return '';
                    }

                    return
                        Html::button(
                            'Прикрепить',
                            [
                                'class' => 'btn btn-success buttonAdd btn-xs',
                                'role'  => 'button',
                                'data'  => [
                                    'id' => $item['id'],
                                ],
                            ]
                        );
                },
            ];

            ?>
            <?= \yii\grid\GridView::widget([
                'dataProvider' => new \yii\data\ActiveDataProvider([
                    'query' => UserBill::find()
                        ->where([
                            'card_id'      => null,
                            'user_id'      => Yii::$app->user->id,
                            'mark_deleted' => 0,
                        ])
                        ->andWhere(['not', ['currency' => Currency::BTC]])
                        ->andWhere(['not', ['in', 'currency', $currencyList]])
                ]),
                'tableOptions' => [
                    'class' => 'table table-hover table-striped',
                    'style' => 'width: auto;',
                    'id'    => 'tableTransaction',
                    'align'    => 'center',
                ],
                'rowOptions'   => function ($item) {
                    $data = [
                        'data'  => [
                            'id'       => $item['id'],
                            'currency' => $item['currency'],
                        ],
                        'role'  => 'button',
                        'class' => 'rowTable',
                    ];

                    return $data;
                },
                'summary' => '',
                'columns' => $columns
            ]) ?>
        </div>
    </div>
</div>

