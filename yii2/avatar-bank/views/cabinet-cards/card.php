<?php
use common\models\avatar\Currency;
use common\models\avatar\UserBill;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $card \common\models\Card */

$this->title = 'Карта ' . $card->getNumbberWithSpace();


$currencyBTC = Currency::BTC;
$currencyETH = Currency::ETH;

$strCopy = Yii::t('c.TsqnzVaJuC', 'Скопировано');

\avatar\assets\Clipboard::register($this);
$this->registerJs(<<<JS
var clipboard = new Clipboard('.js-buttonTransactionInfo');
clipboard.on('success', function(e) {
    $(e.trigger).tooltip({
        title: '{$strCopy}',
        placement: 'bottom'
    });
    $(e.trigger).tooltip('show');
    setTimeout(function(ee) {
        $(e.trigger).tooltip('destroy');
    }, 1000);
});

$('.rowTable').click(function() {
    window.location = '/cabinet-wallet/item?id=' + $(this).data('id');
});


JS
);

/** @var \common\models\UserAvatar $user */
$user = Yii::$app->user->identity;
$defaultBillingIds = \common\models\avatar\UserBillDefault::find()->where(['user_id' => Yii::$app->user->id])->select('id')->column();
Yii::$app->session->set('defaultBillingIds', $defaultBillingIds);


if ($user->wallets_is_locked == 1) {
    // кошельки заблокированы
    $headerLock = Html::tag('span', Html::tag('i', null, ['class' => 'fa fa-lock']), [
        'class' => 'label label-danger',
        'title' => Yii::t('c.TsqnzVaJuC', 'Вам нужно разблокировать'),
        'data'  => [
            'toggle' => 'tooltip',
        ],
    ]);
} else {
    // кошельки разблокированы
    if ($user->password_save_type == UserBill::PASSWORD_TYPE_OPEN) {
        $headerLock = Html::tag('span', Html::tag('i', null, ['class' => 'fa fa-lock']), [
            'class' => 'label label-default',
            'title' => Yii::t('c.TsqnzVaJuC', 'У вас слабая степень защиты'),
            'data'  => [
                'toggle' => 'tooltip',
            ],
            'style' => 'opacity: 0.3;'
        ]);
    } else {
        $headerLock = Html::tag('span', Html::tag('i', null, ['class' => 'fa fa-lock']), [
            'class' => 'label label-default',
            'title' => Yii::t('c.TsqnzVaJuC', 'У вас кошельки надежно сохранены'),
            'data'  => [
                'toggle' => 'tooltip',
            ],
        ]);
    }
}


\avatar\assets\Notify::register($this);
\avatar\assets\Clipboard::register($this);

?>
<?php
$this->registerJs(<<<JS

var clipboard = new Clipboard('.buttonCopy');
clipboard.on('success', function(e) {

    new Noty({
        timeout: 1000,
        theme: 'sunset',
        type: 'success',
        layout: 'bottomLeft',
        text: 'Скопировано'
    }).show();

});

JS
);
?>

<style>
    .textDecorated {
        text-decoration-line: underline;
        text-decoration-style: dotted;
    }
</style>


<div class="container" style="padding-bottom: 70px;">
    <div class="row">
        <div class="col-lg-12">
            <div class="row">
                <?php /** @var $card \common\models\Card */ ?>
                <div class="col-lg-4 col-lg-offset-4">
                    <p>
                        <a href="<?= Url::to(['cabinet-cards/card', 'id' => $card->id]) ?>">
                            <img src="<?= $card->getDesign()->image ?>" width="100%">
                        </a>
                    </p>
                    <p class="lead text-center"><code><?= $card->getNumbberWithSpace() ?></code></p>
                </div>
                <div class="col-lg-4">
                    <p>
                        <a class="btn btn-default" href="<?= Url::to(['cabinet-cards/pin', 'id' => $card->id]) ?>">
                            <i class="glyphicon glyphicon-option-horizontal"></i> Установить PIN
                        </a>
                    </p>
                </div>
            </div>

            <?php
            $columns = [
                [
                    'header'  => '',
                    'content' => function ($item) {
                        $currency = \yii\helpers\ArrayHelper::getValue($item, 'currency', Currency::BTC);
                        $title = '';
                        switch ($currency) {
                            case Currency::BTC:
                                $src = '/images/controller/cabinet-bills/transactions/btc.jpg';
                                $title = 'BitCoin (BTC)';
                                break;
                            case Currency::ETH:
                                $src = '/images/controller/cabinet-bills/transactions/eth.png';
                                $title = 'Ethereum (ETH)';
                                break;
                            default:
                                $currencyObject = Currency::findOne($currency);
                                if (is_null($currencyObject)) {
                                    throw new \yii\base\Exception('Не указана валюта у счета');
                                }
                                $src = $currencyObject->image;
                                $title = $currencyObject->title . ' '. '(' . $currencyObject->code . ')';
                                break;
                        }
                        $imgOptions = [
                            'width' => 50,
                            'class' => 'img-circle',
                            'data'  => [
                                'toggle' => 'tooltip',
                                'title'  => $title,
                            ],
                        ];

                        $passwordType = \yii\helpers\ArrayHelper::getValue($item, 'password_type', UserBill::PASSWORD_TYPE_OPEN);
                        if ($passwordType == UserBill::PASSWORD_TYPE_HIDE_CABINET) {
                            /** @var \common\models\UserAvatar $user */
                            $user = Yii::$app->user->identity;
                            if ($user->wallets_is_locked) {
                                $imgOptions['style']['opacity'] = '0.3';
                            }
                        }

                        if (isset($imgOptions['style'])) {
                            if (is_array($imgOptions['style'])) {
                                $s = [];
                                foreach ($imgOptions['style'] as $key => $value) {
                                    $s[] = $key . ':' . $value . ';';
                                }
                                $imgOptions['style'] = join(' ', $s);
                            }
                        }

                        return Html::img($src, $imgOptions);
                    },
                ],
            ];

            if (!YII_ENV_PROD) {
                $columns[] = [
                    'header'  => '',
                    'content' => function ($item) {
                        $passwordType = \yii\helpers\ArrayHelper::getValue($item, 'password_type', UserBill::PASSWORD_TYPE_OPEN);
                        if ($passwordType == UserBill::PASSWORD_TYPE_HIDE_CABINET) {
                            /** @var \common\models\UserAvatar $user */
                            $user = Yii::$app->user->identity;
                            if ($user->wallets_is_locked) {
                                $color = '#d9534f';
                                return Html::tag('i', null, [
                                    'class' => 'fa fa-key',
                                    'style' => 'color: ' . $color . ';',
                                    'title' => 'Пароль от кабинета сменен, доступ к кошелькам нужно восстановить',
                                    'data'  => ['toggle' => 'tooltip'],
                                ]);
                            }
                        }
                    },
                ];
            }

            $columns[] = [
                'header'    => 'WID',
                'content'   => function (\common\models\avatar\UserBill $item) {
                    $currency = $item->currency;
                    if (!\common\models\avatar\CurrencyLink::find()->where(['currency_ext_id' => $currency])->exists()) {
                        return '';
                    }
                    $v = \yii\helpers\ArrayHelper::getValue($item, 'address');
                    if (is_null($v)) return '';
                    $w = new \common\models\piramida\Wallet(['id' => $v]);
                    $addressShort = $w->getAddressShort();

                    return Html::tag('code', $addressShort);
                },
            ];

            $columns = \yii\helpers\ArrayHelper::merge($columns, [
                [
                    'header' => Yii::t('c.TsqnzVaJuC', 'Наименование'),
                    'attribute' => 'name',
                ],
                [
                    'header'         => Yii::t('c.TsqnzVaJuC', 'Баланс'),
                    'contentOptions' => function ($item) {
                        return [
                            'class' => 'rowBill',
                            'id'    => 'bill_confirmed_' . $item['id'],
                            'data'  => ['id' => $item['id']],
                            'style' => 'text-align: right;',
                        ];
                    },
                    'content'        => function ($item) {
                        $wallet_id = $item['address'];
                        $wallet = \common\models\piramida\Wallet::findOne($wallet_id);
                        $currency = Currency::findOne($item['currency']);

                        return Yii::$app->formatter->asDecimal($wallet->amount / 100, $currency->decimals);
                    },
                ],
                [
                    'header'  => 'Открепить',
                    'content' => function ($item) {
                        if ($item['currency'] == Currency::BTC) {
                            return '';
                        }

                        return
                            Html::button(
                                'Открепить',
                                [
                                    'class' => 'btn btn-danger buttonClose btn-xs',
                                    'role'  => 'button',
                                    'data'  => [
                                        'id' => $item['id'],
                                    ],
                                ]
                            );
                    },
                ]
            ]);

            ?>
            <?= \yii\grid\GridView::widget([
                'dataProvider' => new \yii\data\ActiveDataProvider([
                    'query' => UserBill::find()->where(['card_id' => $card->id]),
                    'pagination' => [
                        'pageSize' => 100,
                    ],
                ]),
                'tableOptions' => [
                    'class' => 'table table-hover table-striped',
                    'style' => 'width: auto;',
                    'id'    => 'tableTransaction',
                    'align' => 'center',
                ],
                'summary' => '',
                'rowOptions'   => function (UserBill $item) {
                    $data = [
                        'data'  => [
                            'id'       => $item['id'],
                            'currency' => $item['currency'],
                        ],
                        'role'  => 'button',
                        'class' => 'rowTable',
                    ];

                    return $data;
                },
                'columns'      => $columns,
            ]) ?>
            <p class="text-center"><a href="<?= Url::to(['cabinet-cards/add', 'id' => $card->id])?>" class="btn btn-success">Добавить счет</a></p>

            <?php if ($card->is_personal) { ?>
                <div class="row">
                    <div class="col-lg-6 col-lg-offset-3">
                        <p>Текст для репоста</p>
                        <p><button class="btn btn-success buttonCopy" data-toggle="tooltip" title="Скопировать в буфер" data-clipboard-text="<?= Html::encode($this->render('card-text', ['card' => $card])) ?>">Скопировать в буфер</button></p>
                        <pre><?= Html::encode($this->render('card-text', ['card' => $card])) ?></pre>
                    </div>
                </div>
            <?php } ?>

            <?php
            $arrayBillsIds = \yii\helpers\Json::encode(
                UserBill::find()
                    ->where([
                        'user_id'      => Yii::$app->user->id,
                        'mark_deleted' => 0,
                    ])
                    ->select('id')
                    ->column()
            );

            $str1 = Yii::t('c.TsqnzVaJuC', 'Вы уверены?');
            $str2 = Yii::t('c.TsqnzVaJuC', 'Успешно завершено!');
            $isProd = YII_ENV_PROD ? 'prod' : 'test';
            $this->registerJs(<<<JS
            
$('.buttonClose').click(function(e) {
    e.preventDefault();
    e.stopPropagation();
    var b = $(this);
    if (confirm('{$str1}')) {
        ajaxJson({
            url: '/cabinet-cards/unlink',
            data: {
                id: b.data('id')
            },
            success: function(ret) {
                b.parent().parent().remove();
                $('#modalInfo').modal();
            }
        });
    }
});


var i = 0;
var operationList = [];
$('#tableTransaction').find('tr').each(function(i,v) {
    if (typeof($(v).data('id')) != 'undefined') {
        operationList.push([
            $(v).data('id'),
            $(v).data('currency'),
            $(v).data('address'),
            $(v).data('contract'),
            $(v).data('convert')
        ]);
    } 
}); 


JS
            );
            ?>

        </div>
    </div>
</div>