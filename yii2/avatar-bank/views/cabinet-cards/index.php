<?php

use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
$this->title = 'Карты';

$carListObjects = \common\models\Card::find()
    ->where([
        'user_id' => Yii::$app->user->id,
    ])
    ->all();
$this->registerJs(<<<JS
$('.buttonAdd').click(function(e) {
    $('#modalAdd').modal();
});

var functionButtonAddCard = function(e) {
    e.preventDefault();
    e.stopPropagation();
    var b = $(this);
    var bText = b.html();
    b.off('click');
    b.html($('<i>', {class: 'fa fa-spinner fa-spin fa-fw'}));
    b.attr('disabled', 'disabled');
    
    ajaxJson({
        url: '/cabinet-cards/add-card',
        data: {
            number: $('#form_25 input[name="number"]').val()
        },
        success: function(ret) {
            b.on('click', functionButtonAddCard);
            b.html(bText);
            b.removeAttr('disabled');
            $('#modalAdd').modal('hide');
            window.location.reload();
        },
        errorScript: function(ret) {
            if (ret.id == 102) {
                 var f = $('#form_25');
                 $.each(ret.data, function(i,v) {
                    var name = v.name;
                    var value = v.value;
                    var t = f.find('.recipient-field-' + name);
                    t.addClass('has-error');
                    t.find('p.help-block-error').html(value.join('<br>')).show();
                });
            }
            b.on('click', functionButtonAddCard);
            b.html(bText);
            b.removeAttr('disabled');
        }
    });
};
$('#form_25 .form-control').on('focus', function() {
    var o = $(this);
    var p = o.parent();
    if (p.hasClass('input-group')) {
        p = p.parent();
    }
    p.removeClass('has-error');
    p.find('p.help-block-error').hide();
});
$('.buttonAddCard').click(functionButtonAddCard);
JS
);
?>

<div class="container" style="padding-bottom: 70px;">
    <div class="col-lg-12">
        <h1 class="page-header text-center">
            <?= $this->title ?>
        </h1>
    </div>
    <div class="row">
        <?php /** @var $card \common\models\Card */ ?>
        <?php foreach ($carListObjects as $card) { ?>
            <div class="col-lg-4">
                <p>
                    <a href="<?= Url::to(['cabinet-cards/card', 'id' => $card->id]) ?>">
                        <img src="<?= $card->getDesign()->image ?>"
                             width="100%">
                    </a>
                </p>
                <p class="lead text-center"><code><?= $card->getNumbberWithSpace() ?></code></p>
            </div>
        <?php } ?>
    </div>

    <div class="row">
        <div class="col-lg-4">
            <div class="col-lg-4">
                <button class="btn btn-primary buttonAdd">Активировать карту</button>
            </div>
        </div>
    </div>

</div>

<div class="modal fade" id="modalAdd" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="myModalLabel">Активировать карту</h4>
            </div>
            <div class="modal-body">
                <form id="form_25">
                    <div class="form-group required recipient-field-number">
                        <label class="control-label">Номер карты</label>
                        <input name="number" class="form-control"></input>
                        <p class="help-block help-block-error"></p>
                    </div>

                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary buttonAddCard">Активировать</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
            </div>
        </div>
    </div>
</div>
