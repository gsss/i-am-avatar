<?php
use common\models\avatar\Currency;
use common\models\avatar\UserBill;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $card \common\models\Card */

$this->title = 'Карта ' . $card->getNumbberWithSpace();


$currencyBTC = Currency::BTC;
$currencyETH = Currency::ETH;

$strCopy = Yii::t('c.TsqnzVaJuC', 'Скопировано');

\avatar\assets\Clipboard::register($this);
$this->registerJs(<<<JS
var clipboard = new Clipboard('.js-buttonTransactionInfo');
clipboard.on('success', function(e) {
    $(e.trigger).tooltip({
        title: '{$strCopy}',
        placement: 'bottom'
    });
    $(e.trigger).tooltip('show');
    setTimeout(function(ee) {
        $(e.trigger).tooltip('destroy');
    }, 1000);
});

$('.rowTable').click(function() {
    window.location = '/cabinet-wallet/item?id=' + $(this).data('id');
});


JS
);

/** @var \common\models\UserAvatar $user */
$user = Yii::$app->user->identity;
$defaultBillingIds = \common\models\avatar\UserBillDefault::find()->where(['user_id' => Yii::$app->user->id])->select('id')->column();
Yii::$app->session->set('defaultBillingIds', $defaultBillingIds);


if ($user->wallets_is_locked == 1) {
    // кошельки заблокированы
    $headerLock = Html::tag('span', Html::tag('i', null, ['class' => 'fa fa-lock']), [
        'class' => 'label label-danger',
        'title' => Yii::t('c.TsqnzVaJuC', 'Вам нужно разблокировать'),
        'data'  => [
            'toggle' => 'tooltip',
        ],
    ]);
} else {
    // кошельки разблокированы
    if ($user->password_save_type == UserBill::PASSWORD_TYPE_OPEN) {
        $headerLock = Html::tag('span', Html::tag('i', null, ['class' => 'fa fa-lock']), [
            'class' => 'label label-default',
            'title' => Yii::t('c.TsqnzVaJuC', 'У вас слабая степень защиты'),
            'data'  => [
                'toggle' => 'tooltip',
            ],
            'style' => 'opacity: 0.3;'
        ]);
    } else {
        $headerLock = Html::tag('span', Html::tag('i', null, ['class' => 'fa fa-lock']), [
            'class' => 'label label-default',
            'title' => Yii::t('c.TsqnzVaJuC', 'У вас кошельки надежно сохранены'),
            'data'  => [
                'toggle' => 'tooltip',
            ],
        ]);
    }
}


\avatar\assets\Notify::register($this);
\avatar\assets\Clipboard::register($this);

?>
<?php
$this->registerJs(<<<JS

var clipboard = new Clipboard('.buttonCopy');
clipboard.on('success', function(e) {

    new Noty({
        timeout: 1000,
        theme: 'sunset',
        type: 'success',
        layout: 'bottomLeft',
        text: 'Скопировано'
    }).show();

});

JS
);
?>

<style>
    .textDecorated {
        text-decoration-line: underline;
        text-decoration-style: dotted;
    }
</style>


<div class="container" style="padding-bottom: 70px;">
    <div class="row">
        <div class="col-lg-12">
            <div class="row">
                <?php /** @var $card \common\models\Card */ ?>
                <div class="col-lg-4 col-lg-offset-4">
                    <p>
                        <a href="<?= Url::to(['cabinet-cards/card', 'id' => $card->id]) ?>">
                            <img src="<?= $card->getDesign()->image ?>" width="100%">
                        </a>
                    </p>
                    <p class="lead text-center"><code><?= $card->getNumbberWithSpace() ?></code></p>

                </div>
            </div>
        </div>
        <div class="col-lg-4 col-lg-offset-4">
            <input class="form-control" id="pin" type="password" disabled="disabled" style="border: none; font-size: 1600%; height: 100px; font-family: "Helvetica Neue", Helvetica, Arial, sans-serif" >
            <div class="row" style="margin-top: 20px;">
                <div class="col-xs-4">
                    <button class="btn btn-default buttonNumber" style="width: 100%">1</button>
                </div>
                <div class="col-xs-4">
                    <button class="btn btn-default buttonNumber" style="width: 100%">2</button>
                </div>
                <div class="col-xs-4">
                    <button class="btn btn-default buttonNumber" style="width: 100%">3</button>
                </div>
            </div>
            <div class="row" style="margin-top: 20px;">
                <div class="col-xs-4">
                    <button class="btn btn-default buttonNumber" style="width: 100%">4</button>
                </div>
                <div class="col-xs-4">
                    <button class="btn btn-default buttonNumber" style="width: 100%">5</button>
                </div>
                <div class="col-xs-4">
                    <button class="btn btn-default buttonNumber" style="width: 100%">6</button>
                </div>
            </div>
            <div class="row" style="margin-top: 20px;">
                <div class="col-xs-4">
                    <button class="btn btn-default buttonNumber" style="width: 100%">7</button>
                </div>
                <div class="col-xs-4">
                    <button class="btn btn-default buttonNumber" style="width: 100%">8</button>
                </div>
                <div class="col-xs-4">
                    <button class="btn btn-default buttonNumber" style="width: 100%">9</button>
                </div>
            </div>
            <div class="row" style="margin-top: 20px;">
                <div class="col-xs-4">
                </div>
                <div class="col-xs-4">
                    <button class="btn btn-default buttonNumber" style="width: 100%">0</button>
                </div>
                <div class="col-xs-4">
                    <button class="btn btn-default buttonDel" data-id="7" style="width: 100%"><i class="glyphicon glyphicon-menu-left"></i></button>
                </div>
            </div>
            <hr>
            <?php
            $this->registerJs(<<<JS
$('#pin').focus();
// $('#pin').keyup(function(event) {
//     event.keyCode == 13;
//     console.log(event);
//     return false;
// });
// $('#pin').on('input', function(event) {
//     event.keyCode == 13;
//     console.log(event);
//     return false;
// });
var pin = '';
$('.buttonNumber').click(function() {
    if (pin.length >= 4) return;
    var num = $(this).html();
    console.log(num);
    pin = pin + num;
    $('#pin').val(pin);
    if (pin.length == 4) {
        $('.buttonNumber').attr('disabled', 'disabled');
    }
});
$('.buttonDel').click(function() {
    if (pin.length == 0) return;
    if (pin.length == 4) {
        $('.buttonNumber').removeAttr('disabled');
    }
    var n = pin.substr(0,pin.length - 1);
    pin = n;
    $('#pin').val(n);
});
$('.buttonPay').click(function(e) {
    ajaxJson({
        url: '/cabinet-cards/pin-ajax',
        data: {
            "id": {$card->id}, 
            "pin": $('#pin').val()
        },
        success: function(ret) {
            $('#modalInfo').on('hidden.bs.modal', function() {
                window.location = '/cabinet-cards/card?id=' + {$card->id};
            }).modal();
        },
        errorScript: function(ret) {
            if (ret.id == 102) {
                for (var key in ret.data) {
                    if (ret.data.hasOwnProperty(key)) {
                        var name = key;
                        var value = ret.data[key];
                        new Noty({
                            timeout: 1000,
                            theme: 'relax',
                            type: 'warning',
                            layout: 'bottomLeft',
                            text: value.join('<br>')
                        }).show();
                    }
                }
            }
        }
    });
});
JS
            );

            ?>
            <button class="btn btn-success buttonPay" style="width: 100%;">Установить</button>
        </div>
    </div>
</div>

<div class="modal fade" id="modalInfo" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Информация</h4>
            </div>
            <div class="modal-body">
                Успешно!
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
            </div>
        </div>
    </div>
</div>