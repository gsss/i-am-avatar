<?php
/**
 * Created by PhpStorm.
 * User: s.arhangelskiy
 * Date: 23.01.2017
 * Time: 10:21
 */

use common\models\avatar\Currency;

/* @var $this yii\web\View */

$currencyBTC = Currency::BTC;
$currencyETH = Currency::ETH;

$this->registerJs(<<<JS
$('.buttonSendEth').click(function(e) {
    e.preventDefault();
    e.stopPropagation();
    var o = $(this);
    var currency = o.data('currency');
    
    $('#formOutEth input[name="billing_id"]').val(o.data('id'));

    $('#formOutEth input[name="address"]').val('');
    $('#formOutEth input[name="amount"]').val('');
    $('#formOutEth input[name="password"]').val('');
    $('#formOutEth input[name="comment"]').val('');
    $('#formOutEth input[name="gasLimit"]').val('');
    $('#formOutEth input[name="gasPrice"]').val('');

    $('#modalSendEth').modal();
});

// устанавливает максимально возможное кол-во эфира которое можно забрать с кошелька
$('#formOutEth .buttonMax').click(function(e) {
    e.preventDefault();
    e.stopPropagation();
    var modal = $('#modalSendEth');
});

// снимает ошибочные признаки поля при фокусе
$('#formOutEth .form-control').on('focus', function() {
    var o = $(this);
    var p = o.parent();
    if (p.hasClass('input-group')) {
        p = p.parent();
    }
    p.removeClass('has-error');
    p.find('p.help-block-error').hide();
});

JS
);
?>


<div class="modal fade" tabindex="-1" role="dialog" id="modalSendEth">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">&times;</span></button>
                <h4 class="modal-title"><?= \Yii::t('c.TsqnzVaJuC', 'Перевод денег') ?></h4>
            </div>
            <div class="modal-body">
                <form id="formOutEth">
                    <input type="hidden" name="billing_id">

                    <div class="form-group recipient-field-address">
                        <label class="control-label"><?= \Yii::t('c.TsqnzVaJuC', 'Кому') ?>:</label>
                        <input type="text" class="form-control" name="address" style="font-family: Consolas, Courier New, monospace"  autocomplete="off">
                        <p class="help-block"><?= \Yii::t('c.TsqnzVaJuC', 'Можно указать адрес счета, почту или телефон клиента') ?></p>

                        <p class="help-block help-block-error" style="display: none;"></p>
                    </div>
                    <div class="form-group recipient-field-amount">
                        <label class="control-label"><?= \Yii::t('c.TsqnzVaJuC', 'Сколько') ?>:</label>

                        <div class="input-group">
                            <input type="text"
                                   name="amount"
                                   class="form-control"
                                   aria-label="..."
                                   placeholder="0.00000001"
                                   autocomplete="off"
                                   style="font-family: Consolas, Courier New, monospace">
                            <input type="hidden"
                                   name="currency"
                                   value="ETH"

                            >
                            <span class="input-group-btn">
                                <?php
                                $this->registerJs(<<<JS
var buttonMaxFunction = function() {
    var b = $(this);
    b.off('click');
    b.html($('<i>', {class: 'fa fa-spinner fa-spin fa-fw'}));
    b.attr('disabled', 'disabled');
    
    ajaxJson({
         url: '/cabinet-bills-eth/get-max',
         data: {
             billing_id: $('#formOutEth input[name="billing_id"]').val(),
             gasPrice: $('#formOutEth input[name="gasPrice"]').val(),
             gasLimit: $('#formOutEth input[name="gasLimit"]').val()
         },
         success: function(ret) {
             $('#formOutEth input[name="amount"]').val(ret.wei / 1000000000000000000);
             b.on('click', buttonMaxFunction);
             b.html('Max');
             b.removeAttr('disabled');
         },
         errorScript: function(ret) {
             if (ret.id == 102) {
                 var f = $('#modalSendEth');
                 $.each(ret.data,function(i,v) {
                    var name = v.name;
                    var value = v.value;
                    var o = f.find('input[name="'+ name + '"]');
                    var t = f.find('.recipient-field-' + name);
                    t.addClass('has-error');
                    t.find('p.help-block-error').html(value.join('<br>')).show();
                });
             }
             b.on('click', buttonMaxFunction);
             b.html('Max');
             b.removeAttr('disabled');
         }
    });
};
$('.buttonMax').click(buttonMaxFunction);
JS
                                );
                                ?>
                                <button class="btn btn-default buttonMax" type="button">Max</button>
                            </span>
                            <div class="input-group-btn">
                                <?php
                                $this->registerJs(<<<JS
$('.buttonToggleSettings').click(function() {
    var b = $('.buttonToggleSettings');
    if (b.hasClass('active')) {
        b.removeClass('active');
    } else {
        b.addClass('active');
    }
    $('#modalEthSettings').collapse('toggle');
});
JS
);
                                ?>
                                <button type="button"
                                        class="btn btn-default buttonToggleSettings active"
                                        title="<?= \Yii::t('c.TsqnzVaJuC', 'Настройка цены газа и его лимита') ?>"
                                        data-toggle="tooltip"
                                ><i class="glyphicon glyphicon-cog"></i></button>
                                <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true"
                                        aria-expanded="false"><span class="buttonCurrency">ETH</span> <span class="caret"></span></button>
                                <ul class="dropdown-menu dropdown-menu-right">
                                    <?php
                                    $str1= Yii::t('c.TsqnzVaJuC', 'Клиент');
                                    $str2= Yii::t('c.TsqnzVaJuC', 'Счет');
                                    $str3= Yii::t('c.TsqnzVaJuC', 'Перевести');
                                    $this->registerJs(<<<JS
$('#formOutEth.itemCurrency').click(function(e) {
    $('#formOutEth input[name="currency"]').val($(this).data('code'));
    $('#formOutEth .buttonCurrency').html($(this).html());
    var i, s='', c = $(this).data('decimals');
    for(i = 0; i < c; i++) {
        s = s + '0';
    }
    $('#formOutEth input[name="amount"]').attr('placeholder', '0.'+ s)
});


var functionButtonSendEth = function() {
    var b = $(this);
    b.off('click');
    b.html($('<i>', {class: 'fa fa-spinner fa-spin fa-fw'}));
    b.attr('disabled', 'disabled');
    ajaxJson({
        url: '/cabinet-bills-eth/send',
        data: $('#formOutEth').serializeArray(),
        success: function(ret) {
            $('#modalSendEth').data('ret', ret);
            $('#modalSendEth').modal('hide').on('hidden.bs.modal', function(e) {
                if (ret.hasUser) {
                    $('#modalSendConfirmEth .modal-body').html(
                        $('<p>').append(
                            $('<img>', {src:ret.user.avatar,width:'300',class: 'img-circle'})
                        )
                    )
                    .append(
                        $('<p>').html(ret.user.name2)
                    )
                    .append(
                        $('<p>').html($('<b>').html('{$str2}'))
                    )
                    .append(
                        $('<p>').append(
                            $('<code>').html(ret.billing.address)
                        )
                    )
                    .append(
                        $('<p>').html(ret.billing.name)
                    )
                    ;
                } else {
                    $('#modalSendConfirmEth .modal-body').html(
                        $('<p>').html($('<b>').html('{$str2}'))
                    )
                    .append(
                        $('<p>').append(
                            $('<code>').html(ret.address)
                        )
                    )
                }
                $('#modalSendConfirmEth').modal();
                $('#modalSendEth').off('hidden.bs.modal');
                b.on('click', functionButtonSendEth);
                b.html('{$str3}');
                b.removeAttr('disabled');
            });
        },
        errorScript: function(ret) {
            switch (ret.id) {
                case 101:
                case 102:
                    var f = $('#modalSendEth');
                    $.each(ret.data,function(i,v) {
                        var name = v.name;
                        var value = v.value;
                        var o = f.find('input[name="'+ name + '"]');
                        var t = f.find('.recipient-field-' + name);
                        t.addClass('has-error');
                        t.find('p.help-block-error').html(value.join('<br>')).show();
                    });

                    break;
            }
            b.on('click', functionButtonSendEth);
            b.html('{$str3}');
            b.removeAttr('disabled');
        }
    });
};
$('.buttonModalSendEth').click(functionButtonSendEth);
JS
);

                                    ?>
                                    <?php /** @var Currency $currency */ ?>
                                    <?php foreach(Currency::find()->where(['in', 'id', [1,2,3,4,6]])->all() as $currency) { ?>
                                        <li>
                                            <a
                                                href="javascript:void(0);"
                                                class="itemCurrency"
                                                data-code="<?= $currency->code ?>"
                                                data-kurs="<?= $currency->kurs ?>"
                                                data-decimals="<?= $currency->decimals ?>"
                                                data-id="<?= $currency->id ?>">
                                                <?= $currency->code ?>
                                            </a>
                                        </li>
                                    <?php } ?>
                                </ul>
                            </div><!-- /btn-group -->
                        </div><!-- /input-group -->
                        <p class="help-block help-block-error" style="display: none;"></p>
                    </div>
                    <div class="row collapse in" id="modalEthSettings">
                        <div class="col-lg-6">
                            <div class="form-group recipient-field-gasPrice">
                                <label for="recipient-name" class="control-label"><?= \Yii::t('c.TsqnzVaJuC', 'gasPrice') ?>:</label>
                                <input type="text" class="form-control" name="gasPrice" placeholder="21" autocomplete="off">

                                <p class="help-block"><?= \Yii::t('c.TsqnzVaJuC', 'От 1 до 50, по умолчанию 21') ?></p>
                                <p class="help-block help-block-error" style="display: none;"></p>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="form-group recipient-field-gasLimit">
                                <label for="recipient-name" class="control-label"><?= \Yii::t('c.TsqnzVaJuC', 'gasLimit') ?>:</label>
                                <input type="text" class="form-control" name="gasLimit" placeholder="21000" autocomplete="off">
                                <p class="help-block"><?= \Yii::t('c.TsqnzVaJuC', 'От 21000, по умолчанию 21000') ?></p>
                                <p class="help-block help-block-error" style="display: none;"></p>
                            </div>
                        </div>
                    </div>
                    <div class="form-group recipient-field-password">
                        <label for="recipient-name" class="control-label"><?= \Yii::t('c.TsqnzVaJuC', 'Пароль') ?>:</label>
                        <input type="password" class="form-control" name="password" autocomplete="off">

                        <p class="help-block"><?= \Yii::t('c.TsqnzVaJuC', 'Пароль от кабинета') ?></p>
                        <p class="help-block help-block-error" style="display: none;"></p>
                    </div>

                    <div class="form-group recipient-field-comment">
                        <label for="message-text" class="control-label"><?= \Yii::t('c.TsqnzVaJuC', 'Комментарий') ?>:</label>
                        <textarea class="form-control" id="recipient-comment" name="comment"></textarea>

                        <p class="help-block help-block-error" style="display: none;"></p>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary buttonModalSendEth" style="width: 100%;"><?= \Yii::t('c.TsqnzVaJuC', 'Перевести') ?></button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div><!-- /.modal -->


<div class="modal fade" tabindex="-1" role="dialog" id="modalSendConfirmEth">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">&times;</span></button>
                <h4 class="modal-title  text-center"><?= \Yii::t('c.TsqnzVaJuC', 'Подтверждение') ?></h4>
            </div>
            <div class="modal-body text-center">
                content
            </div>
            <div class="modal-footer">
                <?php
                $str4 = Yii::t('c.TsqnzVaJuC', 'Транзакция успешно отправлена');
                $str5 = Yii::t('c.TsqnzVaJuC', 'Подтвердить');
                $p = new \avatar\modules\ETH\ServiceEtherScan();
                $url = $p->getUrl('/tx/');
                $this->registerJs(<<<JS
var functionButtonConfirm = function() {
    // устанавливаю активный кружок крутящийся и снимаю событие с кнопки и деактивирую ее
    var b = $(this);
    b.off('click');
    b.html($('<i>', {class: 'fa fa-spinner fa-spin fa-fw'}));
    b.attr('disabled', 'disabled');
    
    var ret = $('#modalSendEth').data('ret'); // TODO убрать и проверить

    $('#modalSendConfirmEth').off('hidden.bs.modal');
    $('#modalSendConfirmEth').on('hidden.bs.modal', function(e) {
        $('#modalSendConfirmEth').off('hidden.bs.modal');
        $('.buttonModalConfirmEth').show();
    });
    ajaxJson({
        url: '/cabinet-bills-eth/send-confirm',
        data: $('#formOutEth').serializeArray(),
        success: function(ret) {
            $('#modalSendConfirmEth').on('hidden.bs.modal', function(e) {
                $('#modalInfoEth .modal-body')
                .append(
                    $('<p>').html('{$str4}')
                )
                .append(
                    $('<p>').append(
                        $('<a>', {target: '_blank', href: '{$url}' + ret.address}).html(
                            $('<code>').html(
                                ret.address
                            )
                        )
                    )
                )
                ;
                $('#modalInfoEth').on('hidden.bs.modal', function(e) {
                    window.location.reload();
                });
                $('#modalInfoEth').modal();
            }).modal('hide');
            b.on('click', functionButtonConfirm);
            b.html('{$str5}');
            b.removeAttr('disabled');
        },
        errorScript: function(ret) {
            switch (ret.id) {
                case 101:
                case 102:
                    $.each(ret.data,function(i,v) {
                        var name = v.name;
                        var value = v.value;
                        $('#modalSendConfirmEth .modal-body')
                        .append(
                            $('<p>', {class: 'alert alert-danger'}).html(value.join('<br>'))
                        );
                    });
                    $('#modalSendConfirmEth .modal-body')
                        .append(
                            $('<button>', {class: 'btn btn-primary', style: 'width: 100%'}).html('Назад').click(function(e){
                                $('#modalSendConfirmEth').on('hidden.bs.modal', function(e) {
                                    $('#modalSendEth').modal();
                                }).modal('hide');
                            })
                        );
                    $('.buttonModalConfirmEth').hide();
                    break;
            }
            b.on('click', functionButtonConfirm);
            b.html('{$str5}');
            b.removeAttr('disabled');
        }
    });

};
$('.buttonModalConfirmEth').click(functionButtonConfirm);
JS
                );
                ?>
                <button type="button" class="btn btn-primary buttonModalConfirmEth" style="width: 100%;"><?= \Yii::t('c.TsqnzVaJuC', 'Подтвердить') ?></button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div><!-- /.modal -->

<div class="modal fade" id="modalInfoEth" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">&times;</span></button>
                <h4 class="modal-title text-center" id="myModalLabel"><?= \Yii::t('c.TsqnzVaJuC', 'Информация') ?></h4>
            </div>
            <div class="modal-body text-center">

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" style="width: 100%;" data-dismiss="modal">
                    <?= \Yii::t('c.TsqnzVaJuC', 'Закрыть') ?>
                </button>
            </div>
        </div>
    </div>
</div>
