<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;
use app\models\UnionCategory;
use yii\db\Query;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model \common\models\avatar\UserBill */

$this->title = 'Сохранить ключи';

?>
<div class="container">
    <div class="col-lg-12">
        <h1 class="page-header text-center"><?= Html::encode($this->title) ?></h1>
        <p class="alert alert-success text-center">Успешно</p>
        <p class="text-center"><a href="/cabinet-bills/password-type-pdf" class="btn btn-primary"><i class="glyphicon glyphicon-download-alt"></i> Скачать PDF</a></p>
    </div>
</div>
