<?php
/**
 * Created by PhpStorm.
 * User: s.arhangelskiy
 * Date: 23.01.2017
 * Time: 10:21
 */
/* @var $this yii\web\View */

use common\models\avatar\Currency;



$this->registerJs(<<<JS
$('.buttonNew').click(function() {
    $('#modalNew').modal();
});

var tryCount = 5;

var functionAjax = function(b, dataPost) {
    ajaxJson({
        url: '/cabinet-bills/new',
        data: dataPost,
        success: function(ret) {
            window.location.reload();
        },
        errorScript: function(ret) {
            switch (ret.id) {
                case 102:
                    var f = $('#formNew');
                    $.each(ret.data,function(i,v) {
                        var name = v.name;
                        var value = v.value;
                        var o = f.find('#recipient-'+ name);
                        var t = f.find('.recipient-field-' + name);
                        t.addClass('has-error');
                        t.find('p.help-block-error').html(value.join('<br>')).show();
                    });

                    break;
                case 103:
                    console.log([103, tryCount]);
                    if (tryCount > 0) {
                        tryCount--;
                        console.log(['alert setTimeout']);
                        setTimeout(function() {
                            console.log([b, dataPost]);
                            functionAjax(b, dataPost)
                        }, 3000);
                    } else {
                        var f = $('#formNew');
                        var name = 'password';
                        var value = ['Не удалось создать после 5 попыток'];
                        var o = f.find('#recipient-'+ name);
                        var t = f.find('.recipient-field-' + name);
                        t.addClass('has-error');
                        t.find('p.help-block-error').html(value.join('<br>')).show();
                    }
                    break;
            }
        }

    });
};

var functionButtonModalNew = function() {
    var b = $(this);
    b.off('click');
    b.html($('<i>', {class: 'fa fa-spinner fa-spin fa-fw'}));
    b.attr('disabled', 'disabled');
   
    var dataPost = $('#formNew').serializeArray();
    ajaxJson({
        url: '/cabinet-bills/new',
        data: dataPost,
        success: function(ret) {
            window.location.reload();
        },
        errorScript: function(ret) {
            switch (ret.id) {
                case 102:
                    var f = $('#formNew');
                    $.each(ret.data,function(i,v) {
                        var name = v.name;
                        var value = v.value;
                        var t = f.find('.recipient-field-' + name);
                        console.log(t);
                        t.addClass('has-error');
                        t.find('p.help-block-error').html(value.join('<br>')).show();
                    });
                    
                    b.on('click', functionButtonModalNew);
                    b.html('Создать');
                    b.removeAttr('disabled');
                    
                    break;
                case 103:
                    console.log([103, tryCount]);
                    if (tryCount > 0) {
                        tryCount--;
                        console.log(['click']);
                        functionAjax(b, dataPost);
                    } else {
                        var f = $('#formNew');
                        var name = 'password';
                        var value = ['Не удалось создать после 5 попыток'];
                        var o = f.find('#recipient-'+ name);
                        var t = f.find('.recipient-field-' + name);
                        t.addClass('has-error');
                        t.find('p.help-block-error').html(value.join('<br>')).show();
                    }
                    break;
            }
            
        }

    });
};

$('.buttonModalNew').click(functionButtonModalNew);
$('#formNew .form-control').on('focus', function() {
    var o = $(this);
    var p = o.parent();
    if (p.hasClass('input-group')) {
        p = p.parent();
    }
    p.removeClass('has-error');
    p.find('p.help-block-error').hide();
});

JS
);

?>


<div class="modal fade" tabindex="-1" role="dialog" id="modalNew">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                        aria-hidden="true">&times;</span></button>
                <h4 class="modal-title"><?= \Yii::t('c.TsqnzVaJuC', 'Создание счета') ?></h4>
            </div>
            <div class="modal-body">
                <form id="formNew">
                    <div class="form-group recipient-field-name">
                        <label for="recipient-name" class="control-label"><?= \Yii::t('c.TsqnzVaJuC', 'Название счета') ?>:</label>
                        <input type="text" class="form-control" id="field-name" name="name">
                        <p class="help-block"></p>

                        <p class="help-block help-block-error" style="display: none;"></p>
                    </div>
                    <div class="form-group recipient-field-password">
                        <label for="recipient-name" class="control-label"><?= \Yii::t('c.TsqnzVaJuC', 'Пароль от кабинета') ?>:</label>
                        <input type="password" class="form-control" id="field-password" name="password">
                        <p class="help-block"></p>

                        <p class="help-block help-block-error" style="display: none;"></p>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary buttonModalNew" style="width: 100%;"><?= \Yii::t('c.TsqnzVaJuC', 'Создать') ?></button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div><!-- /.modal -->

