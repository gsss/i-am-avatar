<?php
/**
 * Created by PhpStorm.
 * User: god
 * Date: 05.07.2017
 * Time: 0:46
 */

$this->registerJs(<<<JS

$('.buttonActivate').click(function() {
    $('#modalActivateCard').modal();
});

JS
);

?>

<div class="modal fade" id="modalActivateCard" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                        aria-hidden="true">&times;</span></button>
                <h4 class="modal-title text-center" id="myModalLabel"><?= \Yii::t('c.TsqnzVaJuC', 'Активировать карту') ?></h4>
            </div>
            <div class="modal-body">
                <p><?= \Yii::t('c.TsqnzVaJuC', 'Введите название счета и номер карты') ?>:</p>
                <div class="form-group field-name">
                    <label class="control-label" for="profilephone-sms"><?= \Yii::t('c.TsqnzVaJuC', 'Название счета') ?></label>
                    <input type="text" id="profilephone-name" class="form-control" value="">
                    <div class="help-block help-block-error"></div>
                </div>
                <div class="form-group field-card_number">
                    <label class="control-label" for="profilephone-sms"><?= \Yii::t('c.TsqnzVaJuC', 'Номер карты') ?></label>
                    <input type="text" id="profilephone-card_number" class="form-control" value="">
                    <div class="help-block help-block-error"></div>
                </div>
            </div>
            <div class="modal-footer">
                <?php
                $this->registerJs(<<<JS
$('.buttonActivateCard').click(function() {
    ajaxJson({
        url: '/cabinet-bills/activate-card',
        data: {
            ActivateCard: {
                name: $('#profilephone-name').val(),
                card_number: $('#profilephone-card_number').val()
            }
        },
        success: function(ret) {
            $('#modalActivateCard').on('hidden.bs.modal', function(e){ window.location.reload(); }).modal('hide');
        }
    });
});
JS
                );
                ?>
                <button type="button" class="btn btn-primary buttonActivateCard" style="width: 100%;">
                    <?= \Yii::t('c.TsqnzVaJuC', 'Активировать') ?>
                </button>
            </div>
        </div>
    </div>
</div>
