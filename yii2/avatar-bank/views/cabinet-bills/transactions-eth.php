<?php
/**
 * Created by PhpStorm.
 * User: Бог-Творец
 * Date: 17.11.2016
 * Time: 2:44
 */

/* @var $this yii\web\View */
/* @var $billing \common\models\avatar\UserBill */

$this->title = $billing->name;

$src = '/images/controller/cabinet-bills/transactions/eth.png';
$options = ['src' => $src, 'style' => 'width: 100%;max-width: 308px;'];
$rate = 'RUB/ETH' . ' ' . '=' . ' ' . \yii\helpers\Html::tag('b', Yii::$app->formatter->asDecimal(\common\models\avatar\Currency::getRate('ETH'),2), ['id' => 'kurs-rub-eth', 'data-value' => 1]);

\avatar\assets\Clipboard::register($this);
$str1 = Yii::t('c.Slee99ZnRa', 'Скопировано');

$this->registerJs(<<<JS
var clipboard = new Clipboard('.buttonCopy');
clipboard.on('success', function(e) {
    $(e.trigger).tooltip({
        title: '{$str1}',
        placement: 'bottom'
    }).tooltip('show');
});
JS
);
$this->registerJs(<<<JS
var address = '{$billing->address}';
var icon = document.getElementById('icon');
icon.style.backgroundImage = 'url(' + blockies.create({ seed:address, size: 8, scale: 16}).toDataURL()+')'
JS
);
\common\assets\EthereumAddressIcon::register($this);

$path = Yii::$app->assetManager->getBundle('avatar\assets\App\Asset')->baseUrl . '/images/ajax-loader.gif';

?>

<style>
    #icon {
        width: 64px;
        height: 64px;
        background-size: cover;
        background-repeat: no-repeat;
        border-radius: 50%;
        box-shadow: inset rgba(255, 255, 255, 0.6) 0 2px 2px, inset rgba(0, 0, 0, 0.3) 0 -2px 6px;
    }
</style>
<style>
    .textDecorated {
        text-decoration-line: underline;
        text-decoration-style: dotted;
    }
</style>
<div class="container">
<h2 class="page-header text-center"><?= $billing->name ?></h2>

<p class="text-center">
    <?= $rate ?> <span style="color: #ccc;">руб.</span>
</p>
<p class="text-center" style="margin: 0px;">
    <?= \yii\helpers\Html::tag('img', null, $options) ?>
</p>
<center style="margin: 0px 0px 20px 0px;">
    <div id="icon"></div>
</center>

<div class="row" style="margin: 20px 0px 20px 0px;">
    <?php \common\assets\HighCharts\HighChartsAsset::register($this); ?>
    <div id="chartETH"></div>
</div>



<div class="input-group">
        <span class="input-group-btn">
            <button class="btn btn-default buttonCopy" title="<?= \Yii::t('c.dEj9SM57Nh', 'Скопировать адрес счета') ?>" data-clipboard-text="<?= $billing->address ?>" data-toggle="tooltip">
                <i class="glyphicon glyphicon-copy"></i>
            </button>
        </span>
        <input type="text" class="form-control" placeholder="<?= \Yii::t('c.Slee99ZnRa', 'Адрес кошелька') ?>"
               style="font-family: Consolas, Courier New, monospace;"
               value="<?= $billing->address ?>"
               id="internalAddress"
            >
    <span class="input-group-btn">
              <?php
              $url = (new \avatar\modules\ETH\ServiceEtherScan())->getUrl('/address/' . $billing->address);
              ?>
        <a
            href="<?= $url ?>"
            class="btn btn-default"
            title="<?= \Yii::t('c.dEj9SM57Nh', 'Перейти на EtherScan') ?>"
            data-toggle="tooltip"
            target="_blank"
        >
            <i class="glyphicon glyphicon-new-window"></i>
        </a>
        <a
            href="<?= \yii\helpers\Url::to(['cabinet/get-qr-code', 'address' => $billing->address]) ?>"
            class="btn btn-default"
            title="<?= \Yii::t('c.dEj9SM57Nh', 'Скачать QR код') ?>"
            data-toggle="tooltip"
            >
                <i class="fa fa-qrcode"></i>
        </a>

        </span>
</div>


<div class="modal fade" id="modalTransaction" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">&times;</span></button>
                <h4 class="modal-title text-center" id="myModalLabel"><?= \Yii::t('c.Slee99ZnRa', 'Транзакция') ?></h4>
            </div>
            <div class="modal-body">
                <table class="table table-hover table-striped">
                    <tr>
                        <td>TxID</td>
                        <td class="txid"></td>
                    </tr>
                    <tr>
                        <td><?= \Yii::t('c.Slee99ZnRa', 'Сумма') ?></td>
                        <td class="amount"></td>
                    </tr>
                    <tr>
                        <td><?= \Yii::t('c.Slee99ZnRa', 'Комиссия') ?></td>
                        <td class="fee"></td>
                    </tr>
                    <tr>
                        <td><?= \Yii::t('c.Slee99ZnRa', 'Тип') ?></td>
                        <td class="type"></td>
                    </tr>
                </table>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal"><?= \Yii::t('c.TsqnzVaJuC', 'Закрыть') ?></button>
            </div>
        </div>
    </div>
</div>

<h2 class="alert alert-success text-center balanceArea">
    <span id="external-wallet-confirmed"><img src="<?= $path ?>"></span><br>
    <small id="currencyString">ETH</small>
</h2>

    <?php

$this->registerJs(<<<JS


var functionShowTransaction = function(e) {
    var transaction = $(this).data('transaction');
    $('#modalTransaction .txid').html($('<code>').html(transaction.hash));
    $('#modalTransaction .fee').html($('<code>').html(transaction.fee));
    $('#modalTransaction .amount').html($('<code>').html(transaction.amount));
    $('#modalTransaction .type').html(
        transaction.direction == 'minus' ? $('<span>', {class: 'label label-danger'}).html('Расход') : $('<span>', {class: 'label label-success'}).html('Приход')
    );
    $('#modalTransaction').modal();
};

var functionPageClick = function(e) {
    var o = $(this);
    var href1 = o.data('href');
    ajaxJson({
        url: href1,
        success: function(ret) {
            var o = $(ret.html);
            o.find('.js-buttonTransactionInfo').click(functionShowTransaction);
            o.find('.paginationItem').each(function(i, v) {
                var o = $(v);
                var href1 = o.attr('href');
                o.attr('href', 'javascript:void(0)');
                o.attr('data-href', href1);
                o.click(functionPageClick);
            });
            o.find('.gsssTooltip').tooltip();
            o.find('[data-toggle="tooltip"]').tooltip();
            $('#transactions').html(o);
        }
    });
};
var operationList = [
    {
        url: '/cabinet-bills/balance-internal-eth' + '?' + 'id' + '=' + {$billing->id},
        success: function(ret) {
            
            if (ret.avatarNetwork.currencyView > 0) {
                $('#external-wallet-confirmed').html(ret.avatarNetwork.convertFormated);
                $('#currencyString').html(ret.avatarNetwork.currencyString);
            } else {
                $('#external-wallet-confirmed').html(ret.avatarNetwork.coinsFormated);
            }
            return true;
        }
    },
    {
        url: '/cabinet-bills/transactions-list-eth',
        data: function() {
            return {
                balance: $('#internalAddress').attr('data-balance'),
                id: {$billing->id}
            };
        },
        beforeSend: function() {
            $('#transactions').html(
                $('<img>', {src: '{$path}' })
            );
        },
        success: function(ret) {
            var o = $(ret.html);
            
            // всплывание модального окна транзакции
            o.find('.js-buttonTransactionInfo').click(functionShowTransaction);
            o.find('.paginationItem').each(function(i, v) {
                var o = $(v);
                var href1 = o.attr('href');
                o.attr('href', 'javascript:void(0)');
                o.attr('data-href', href1);
                o.click(functionPageClick);
            });
            o.find('[data-toggle="tooltip"]').tooltip();

            $('#transactions').html(o);
            return true;
        }
    },
    {
        url: '/cabinet-bills-eth/kurs',
        success: function(ret) {
            draw2(ret.rows, ret.currency.code);
            return true;
        }
    }
];
var i = 0;

var functionCalc = function(i, arr) {

    var item = arr[i];
    var options = {
        url: item.url,
        success: function(ret) {
            i++;
            ret =  item.success(ret);
            if (i < operationList.length) {
                if (ret) functionCalc(i, arr);
            }
        },
        errorScript: function(ret) {
            if (typeof(item.onErrorRepeat) != "undefined") {
                if (item.onErrorRepeat) {
                    i++;
                    ret =  item.success(ret);
                    if (i <= operationList.length) {
                        if (ret) functionCalc(i, arr);
                    }
                }
            }
        }
    };
    if (typeof(item.beforeSend) != "undefined") {
        options.beforeSend = item.beforeSend;
    }
    if (typeof(item.data) != "undefined") {
        options.data = item.data();
    }

    ajaxJson(options);
};
functionCalc(i, operationList);


/**
* 
* @param data array 
* [
* [
*   'time' => ''
*   'value' => ''
* ]
* ]
* @param lineName string название строки
* 
*/
function draw2(data, lineName) {
    var rows = data;
    var newRows = [];
    for(i = 0; i < rows.length; i++)
    {
        var item = rows[i];
        newRows.push({
            x: new Date(item.time * 1000),
            y: item.usd
        });
    }
    
    window.chart = $('#chartETH').highcharts({
        chart: {
            zoomType: "x",
            type     : "spline"
        },
        title : {
            text : 'ETH'
        },
        subtitle : {
           text: 'subtitle'
        },
        xAxis: {
            type: "datetime"
        },
        yAxis : [
            {
                title : {
                    text : "Количество"
                }
            }
        ],
        legend : {
            enabled : true
        },
        tooltip : {
            crosshairs : true,
            shared : true
        },
        plotOptions : {
            series : {
                turboThreshold : 0
            }
        },
        series : [
            {
                type : "spline",
                name : lineName,
                data : newRows
            }
        ]
    });
}


JS
);
?>
<div class="row">
    <div class="col-lg-10 col-lg-offset-1">
        <center>
            <div class="input-group">
                <span class="input-group-btn">
                    <a href="https://localethereum.com/offers/Buy/Sort:Popularity" class="btn btn-default" target="_blank">
                        <i class="glyphicon glyphicon-import" style="margin-right: 5px;"></i><?= \Yii::t('c.dEj9SM57Nh', 'Купить') ?>
                    </a>
                    <a href="<?= \yii\helpers\Url::to(['cabinet-bills/settings', 'id' => $billing->id])?>" class="btn btn-default">
                        <i class="glyphicon glyphicon-cog" style="margin-right: 5px;"></i><?= \Yii::t('c.Slee99ZnRa', 'Настройка') ?>
                    </a>
                    <a href="https://localethereum.com/offers/Sell/Sort:Popularity" class="btn btn-default" target="_blank">
                        <i class="glyphicon glyphicon-export" style="margin-right: 5px;"></i><?= \Yii::t('c.dEj9SM57Nh', 'Продать') ?>
                    </a>
                    <a href="<?= \yii\helpers\Url::to(['cabinet-bills-eth/contract', 'id' => $billing->id])?>" class="btn btn-default">
                        <i class="glyphicon glyphicon-cog" style="margin-right: 5px;"></i> Вызвать контракт
                    </a>
                    <a href="<?= \yii\helpers\Url::to(['cabinet-bills-eth/export-json', 'id' => $billing->id])?>" class="btn btn-default">
                        <i class="glyphicon glyphicon-cog" style="margin-right: 5px;"></i> <?= \Yii::t('c.Slee99ZnRa', 'Получить приватный ключ') ?>
                    </a>
                 </span>
            </div>
        </center>
    </div>
</div>

<h4 class="page-header text-center"><?= \Yii::t('c.Slee99ZnRa', 'Транзакции') ?></h4>
<div id="transactions">

</div>


</div>