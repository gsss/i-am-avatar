<?php
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $billing \common\models\avatar\UserBill */

$this->title = 'Номер карты';

$this->registerJs(<<<JS
JS
);

?>
<div class="container" style="padding-bottom: 70px;">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header text-center"><?= $this->title ?></h1>
        </div>
        <?php if ($billing->card_number != '') { ?>
            <?php
            $cardNumber = $billing->card_number;
            $cardNumber = substr($cardNumber, 0, 4) . ' ' . substr($cardNumber, 4, 4) . ' ' . substr($cardNumber, 8, 4) . ' ' . substr($cardNumber, 12, 4);
            ?>
            <p class="text-center lead">Ваш номер:<br><code><?= $cardNumber ?></code></p>
        <?php } else { ?>
            <p>Чтобы у вас был номер карты вам нужно <a href="/qr/enter">заказать карту Аватара</a>.</p>
        <?php } ?>
    </div>
</div>