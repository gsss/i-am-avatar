<?php
/**
 * Created by PhpStorm.
 * User: Бог-Творец
 * Date: 17.11.2016
 * Time: 2:44
 */
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $billing \common\models\avatar\UserBill */

$this->title = $billing->name;

$token = $billing->getWalletToken()->token;
$currencyObject = $token->getCurrency();
$currencyId = $currencyObject->id;

$options = [
    'class' => 'img-circle',
    'src'   => $currencyObject->image,
    'style' => 'width: 100%;max-width: 308px;',
    'data'  => [
        'toggle'   => 'tooltip',
        'placement' => 'bottom',
    ],
    'title' => $currencyObject->title . ' ' . '(' . $currencyObject->code . ')',
];
$rate = 'RUB/ETH' . ' ' . '=' . ' ' . \yii\helpers\Html::tag('b', Yii::$app->formatter->asDecimal(\common\models\avatar\Currency::getRate('ETH'),2), ['id' => 'kurs-rub-eth', 'data-value' => 1]);

\avatar\assets\Clipboard::register($this);
$this->registerJs(<<<JS
var clipboard = new Clipboard('.buttonCopy');
clipboard.on('success', function(e) {
    $(e.trigger).tooltip({
        title: 'Скопировано',
        placement: 'bottom'
    }).tooltip('show');
});
JS
);

$path = Yii::$app->assetManager->getBundle('avatar\assets\App\Asset')->baseUrl . '/images/ajax-loader.gif';

$provider = new \avatar\modules\ETH\ServiceEtherScan();
$url = $provider->getUrl('/token/' . $token->address);
?>

<style>
    .textDecorated {
        text-decoration-line: underline;
        text-decoration-style: dotted;
    }
</style>
<div class="container">
<h2 class="page-header text-center"><?= $billing->name ?></h2>

<p class="text-center">
    <?= \yii\helpers\Html::tag('img', null, $options) ?>
</p>
<p class="text-center"
   title="<?= \Yii::t('c.oifM0YkVx7', 'Адрес токена') ?>"
   data-toggle="tooltip"
   data-placement="bottom"
>
    <?= \yii\helpers\Html::a(\yii\helpers\Html::tag('code', $token->address), $url, ['target' => '_blank']) ?>
</p>


<?php if ($currencyObject->day_volume_usd > 0) { ?>
    <div class="row" style="margin: 20px 0px 20px 0px;">
        <?php \common\assets\HighCharts\HighChartsAsset::register($this); ?>
        <div id="chartETH"></div>
    </div>
<?php } ?>




<div class="input-group">
    <span class="input-group-btn">
        <button class="btn btn-default buttonCopy" title="<?= \Yii::t('c.dEj9SM57Nh', 'Скопировать адрес счета') ?>" data-clipboard-text="<?= $billing->address ?>" data-toggle="tooltip">
            <i class="glyphicon glyphicon-copy"></i>
        </button>
    </span>
    <input type="text" class="form-control" placeholder="Адрес кошелька"
           style="font-family: Consolas, Courier New, monospace;"
           value="<?= $billing->address ?>"
           id="internalAddress"
           title="<?= \Yii::t('c.oifM0YkVx7', 'Адрес счета') ?>"
           data-toggle="tooltip"
           data-placement="bottom"
        >
    <span class="input-group-btn">
        <?php
        $url = (new \avatar\modules\ETH\ServiceEtherScan())->getUrl('/token/' . $token->address . '?a=' . $billing->address);
        ?>
        <a href="<?= $url ?>" class="btn btn-default"
           title="<?= \Yii::t('c.dEj9SM57Nh', 'Перейти на EtherScan') ?>" data-toggle="tooltip" target="_blank">
            <i class="glyphicon glyphicon-new-window"></i>
        </a>
        <a href="<?= Url::to(['cabinet/get-qr-code', 'address' => $billing->address])?>" class="btn btn-default" title="<?= \Yii::t('c.dEj9SM57Nh', 'Скачать QR код') ?>" data-toggle="tooltip">
            <i class="fa fa-qrcode"></i>
        </a>
    </span>
</div>


<div class="modal fade" id="modalTransaction" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">&times;</span></button>
                <h4 class="modal-title text-center" id="myModalLabel"><?= \Yii::t('c.dEj9SM57Nh', 'Транзакция') ?></h4>
            </div>
            <div class="modal-body">
                <table class="table table-hover table-striped">
                    <tr>
                        <td><?= \Yii::t('c.dEj9SM57Nh', 'txid') ?></td>
                        <td class="txid"></td>
                    </tr>
                    <tr>
                        <td><?= \Yii::t('c.dEj9SM57Nh', 'amount') ?></td>
                        <td class="amount"></td>
                    </tr>
                    <tr>
                        <td><?= \Yii::t('c.dEj9SM57Nh', 'Тип') ?></td>
                        <td class="type"></td>
                    </tr>
                </table>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal"><?= \Yii::t('c.TsqnzVaJuC', 'Закрыть') ?></button>
            </div>
        </div>
    </div>
</div>


<h2 class="alert alert-success text-center">
    <span id="external-wallet-confirmed"><img src="<?= $path ?>"></span><br>
    <small id="currencyString"><?= $currencyObject->code ?></small>
</h2>
<?php
$s1 = Yii::t('c.dEj9SM57Nh', 'Расход');
$s2 = Yii::t('c.dEj9SM57Nh', 'Приход');
$provider = new \avatar\modules\ETH\ServiceEtherScan();
$urlEtherScan = $provider->getUrl('/tx/');

$this->registerJs(<<<JS


var functionShowTransaction = function(e) {
    var transaction = $(this).data('transaction');
    $('#modalTransaction .txid').html(
        $('<a>', {href: '{$urlEtherScan}' +  transaction.hash, target: '_blank'}).html(
            $('<code>').html(transaction.hash)
        )
    );
    $('#modalTransaction .amount').html($('<code>').html(transaction.value));
    $('#modalTransaction .type').html(
        transaction.direction == 'minus' ? $('<span>', {class: 'label label-danger'}).html('{$s1}') : $('<span>', {class: 'label label-success'}).html('{$s2}')
    );
    $('#modalTransaction').modal();
};

var functionPageClick = function(e) {
    var o = $(this);
    var href1 = o.data('href');
    ajaxJson({
        url: href1,
        success: function(ret) {
            var o = $(ret.html);
            o.find('.js-buttonTransactionInfo').click(functionShowTransaction);
            o.find('.paginationItem').each(function(i, v) {
                var o = $(v);
                var href1 = o.attr('href');
                o.attr('href', 'javascript:void(0)');
                o.attr('data-href', href1);
                o.click(functionPageClick);
            });
            o.find('.gsssTooltip').tooltip();
            $('#transactions').html(o);
        }
    });
};

ajaxJson({
    url: '/cabinet-bills/balance-internal-token' + '?' + 'id' + '=' + {$billing->id},
    success: function(ret) {
        $('#currencyString').html(ret.currencyString);
        if (ret.currencyView > 0) {
            $('#external-wallet-confirmed').html(ret.confirmedConverted);
        } else {
            $('#external-wallet-confirmed').html(ret.confirmed);
        }
        
        ajaxJson({
            url: '/cabinet-bills/transactions-list-token',
            data: {
                balance: $('#internalAddress').attr('data-balance'),
                id: {$billing->id}
            },
            beforeSend: function() {
                $('#transactions').html(
                    $('<img>', {src: '{$path}' })
                );
            },
            success: function(ret) {
                var o = $(ret.html);
                
                // всплывание модального окна транзакции
                o.find('.js-buttonTransactionInfo').click(functionShowTransaction);
                o.find('.paginationItem').each(function(i, v) {
                    var o = $(v);
                    var href1 = o.attr('href');
                    o.attr('href', 'javascript:void(0)');
                    o.attr('data-href', href1);
                    o.click(functionPageClick);
                });
                o.find('[data-toggle="tooltip"]').tooltip();
    
                $('#transactions').html(o);
                
                ajaxJson({
                    url: '/cabinet-bills-token/kurs',
                    data: {
                        token_id: {$token->id}
                    },
                    success: function(ret) {
                        draw2(ret.rows, ret.currency.code);
                    }
                });
            },
            errorScript: function(ret) {
                if (ret.id == 102) {
                    $('#transactions').html(
                        $('<p>', {class: 'alert alert-warning'}).html('Неудачно. Перейдите <a href="' + ret.data.url + '" target="_blank">' + ret.data.url + '</a>' )
                        
                    );
                }
            }
        });
    }
});


/**
* 
* @param data array 
* [
* [
*   'time' => ''
*   'value' => ''
* ]
* ]
* @param lineName string название строки
* 
*/
function draw2(data, lineName) {
    var rows = data;
    var newRows = [];
    for(i = 0; i < rows.length; i++)
    {
        var item = rows[i];
        newRows.push({
            x: new Date(item.time * 1000),
            y: item.usd
        });
    }
    
    window.chart = $('#chartETH').highcharts({
        chart: {
            zoomType: "x",
            type     : "spline"
        },
        title : {
            text : 'ETH'
        },
        subtitle : {
           text: 'subtitle'
        },
        xAxis: {
            type: "datetime"
        },
        yAxis : [
            {
                title : {
                    text : "Количество"
                }
            }
        ],
        legend : {
            enabled : true
        },
        tooltip : {
            crosshairs : true,
            shared : true
        },
        plotOptions : {
            series : {
                turboThreshold : 0
            }
        },
        series : [
            {
                type : "spline",
                name : lineName,
                data : newRows
            }
        ]
    });
}
JS
);

?>
<div class="row">
    <div class="col-lg-10 col-lg-offset-1">
        <center>
            <div class="input-group">
            <span class="input-group-btn">
                <a href="<?= Url::to(['cabinet-bills/settings', 'id' => $billing->id])?>" class="btn btn-default">
                    <i class="glyphicon glyphicon-cog" style="margin-right: 5px;"></i><?= \Yii::t('c.oifM0YkVx7', 'Настройка') ?>
                </a>

                <a href="<?= Url::to(['cabinet-bills-token/call-function', 'id' => $billing->id])?>" class="btn btn-default">
                    <i class="glyphicon glyphicon-edit" style="margin-right: 5px;"></i> <?= \Yii::t('c.TsqnzVaJuC', 'Вызвать контракт') ?>
                </a>

            </span>
            </div>
        </center>
    </div>
</div>

<h4 class="page-header text-center"><?= \Yii::t('c.oifM0YkVx7', 'Транзакции') ?></h4>
    <div id="transactions"></div>
</div>

<div class="modal fade" id="modalInfo" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">&times;</span></button>
                <h4 class="modal-title text-center" id="myModalLabel"><?= \Yii::t('c.TsqnzVaJuC', 'Информация') ?></h4>
            </div>
            <div class="modal-body text-center">
                Успешно
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" style="width: 100%;" data-dismiss="modal">
                    <?= \Yii::t('c.TsqnzVaJuC', 'Закрыть') ?>
                </button>
            </div>
        </div>
    </div>
</div>