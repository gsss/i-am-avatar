<?php
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $billing \common\models\avatar\UserBill */

$this->title = 'Карты';

$this->registerJs(<<<JS

var functionRun = function (i) {
    $('#counter').html(i);
    ajaxJson({
        url: '/cabinet-bills/build',
        data: {
            offset: i
        },
        success: function(ret) {
            ajaxJson({
                url: '/cabinet-bills/build',
                data: {
                    'offset': i + 2,
                    'file': ret.path,
                    'row': 1
                },
                success: function(ret) {
                    ajaxJson({
                        url: '/cabinet-bills/build',
                        data: {
                            'offset': i + 4,
                            'file': ret.path,
                            'row': 2
                        },
                        success: function(ret) {
                            ajaxJson({
                                url: '/cabinet-bills/build',
                                data: {
                                    'offset': i + 6,
                                    'file': ret.path,
                                    'row': 3
                                },
                                success: function(ret) {
                                    ajaxJson({
                                        url: '/cabinet-bills/build',
                                        data: {
                                            'offset': i + 8,
                                            'file': ret.path,
                                            'row': 4
                                        },
                                        success: function(ret) {
                                           $('#result').append('<br>').append(ret.path);
                                            i = i + 10;
                                            if (i < 1000) {
                                                functionRun(i);
                                            }
                                        }
                                    });  
                                }
                            });  
                        }
                    });  
                }
            });  
        }
    });  
};
$('.buttonStart').click(function() {
  functionRun(160);
});
JS
);

?>
<div class="container" style="padding-bottom: 70px;">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header text-center"><?= $this->title ?></h1>
        </div>
        <button class="btn btn-success buttonStart">Старт</button>

        <p><code id="counter"></code></p>
        <pre id="result"></pre>

    </div>
</div>


