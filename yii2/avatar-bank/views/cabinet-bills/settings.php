<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;
use app\models\UnionCategory;
use yii\db\Query;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \common\models\avatar\UserBill */

$this->title = $model->name;

?>
<div class="container">
    <div class="col-lg-12">
        <h1 class="page-header text-center"><?= Html::encode($this->title) ?></h1>
        <?php if (Yii::$app->session->hasFlash('form')): ?>
            <div class="alert alert-success">
                Успешно обновлено.
            </div>
            <a href="<?= \yii\helpers\Url::to(['cabinet-bills/settings', 'id' => $model->id])?>" class="btn btn-default">Назад</a>
            <a href="<?= \yii\helpers\Url::to(['cabinet-bills/index'])?>" class="btn btn-success">Счета</a>
        <?php else: ?>
            <div class="row">
                <div class="col-lg-4 col-lg-offset-4">
                    <?php $form = ActiveForm::begin([
                        'id'      => 'contact-form',
                        'options' => ['enctype' => 'multipart/form-data'],
                    ]); ?>
                    <?= $form->field($model, 'name') ?>
                    <hr>
                    <div class="form-group">
                        <?= Html::submitButton('Обновить', [
                            'class' => 'btn btn-default',
                            'name'  => 'contact-button',
                            'style' => 'width:100%',
                        ]) ?>
                    </div>
                    <?php ActiveForm::end(); ?>
                </div>
            </div>
        <?php endif; ?>
    </div>
</div>
