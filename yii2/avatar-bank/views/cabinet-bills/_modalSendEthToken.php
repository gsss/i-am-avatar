<?php
/**
 * Created by PhpStorm.
 * User: s.arhangelskiy
 * Date: 23.01.2017
 * Time: 10:21
 */

use common\models\avatar\Currency;

/* @var $this yii\web\View */

$currencyBTC = Currency::BTC;
$currencyETH = Currency::ETH;

$this->registerJs(<<<JS

$('.buttonSendEthToken').click(function(e) {
    e.preventDefault();
    e.stopPropagation();
    var o = $(this);
    var currency = o.data('currency');
    
    $('#formOutEthToken input[name="currency"]').val(currency);
    $('#formOutEthToken input[name="billing_id"]').val(o.data('id'));
    $('#formOutEthToken input[name="address"]').val('');
    $('#formOutEthToken input[name="amount"]').val('');
    $('#formOutEthToken input[name="password"]').val('');
    $('#formOutEthToken input[name="comment"]').val('');

    $('#modalSendEthToken').modal();
});
$('#formOutEthToken .form-control').on('focus', function() {
    var o = $(this);
    var p = o.parent();
    if (p.hasClass('input-group')) {
        p = p.parent();
    }
    p.removeClass('has-error');
    p.find('p.help-block-error').hide();
});

JS
);
?>


<div class="modal fade" tabindex="-1" role="dialog" id="modalSendEthToken">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">&times;</span></button>
                <h4 class="modal-title"><?= \Yii::t('c.TsqnzVaJuC', 'Перевод денег') ?></h4>
            </div>
            <div class="modal-body">
                <form id="formOutEthToken">
                    <input type="hidden" name="billing_id">

                    <div class="form-group recipient-field-address">
                        <label class="control-label"><?= \Yii::t('c.TsqnzVaJuC', 'Кому') ?>:</label>
                        <input type="text" class="form-control" name="address" style="font-family: Consolas, Courier New, monospace">
                        <p class="help-block"><?= \Yii::t('c.TsqnzVaJuC', 'Можно указать адрес счета, почту или телефон клиента') ?></p>

                        <p class="help-block help-block-error" style="display: none;"></p>
                    </div>
                    <div class="form-group recipient-field-amount">
                        <label class="control-label"><?= \Yii::t('c.TsqnzVaJuC', 'Сколько') ?>:</label>

                        <input type="text"
                               name="amount"
                               class="form-control"
                               aria-label="..."
                               placeholder="0.00000001"
                               autocomplete="off"
                               style="font-family: Consolas, Courier New, monospace">
                        <input type="hidden"
                               name="currency"
                               value="ETH">
                        <p class="help-block help-block-error" style="display: none;"></p>
                    </div>
                    <div class="form-group recipient-field-gasPrice">
                        <label class="control-label"><?= \Yii::t('c.TsqnzVaJuC', 'gasPrice') ?>:</label>
                        <input type="text" class="form-control" name="gasPrice" autocomplete="off">

                        <p class="help-block"><?= \Yii::t('c.TsqnzVaJuC', 'Gwei, Минимум 21, до 50') ?></p>
                        <p class="help-block help-block-error" style="display: none;"></p>
                    </div>
                    <div class="form-group recipient-field-password">
                        <label class="control-label"><?= \Yii::t('c.TsqnzVaJuC', 'Пароль') ?>:</label>
                        <input type="password" class="form-control" name="password" autocomplete="off">

                        <p class="help-block"><?= \Yii::t('c.TsqnzVaJuC', 'Пароль от кабинета') ?></p>
                        <p class="help-block help-block-error" style="display: none;"></p>
                    </div>
                    <div class="form-group recipient-field-comment">
                        <label class="control-label"><?= \Yii::t('c.TsqnzVaJuC', 'Комментарий') ?>:</label>
                        <textarea class="form-control" name="comment"></textarea>

                        <p class="help-block help-block-error" style="display: none;"></p>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <?php
                $str1= Yii::t('c.TsqnzVaJuC', 'Клиент');
                $str2= Yii::t('c.TsqnzVaJuC', 'Счет');
                $str3= Yii::t('c.TsqnzVaJuC', 'Перевести');
                $this->registerJs(<<<JS

var functionButtonSendEthToken = function() {
    var b = $(this);
    b.off('click');
    b.html($('<i>', {class: 'fa fa-spinner fa-spin fa-fw'}));
    b.attr('disabled', 'disabled');
    ajaxJson({
        url: '/cabinet-bills-token/send',
        data: $('#formOutEthToken').serializeArray(),
        success: function(ret) {
            $('#modalSendEthToken').data('ret', ret);
            $('#modalSendEthToken').modal('hide').on('hidden.bs.modal', function(e) {
                if (ret.hasUser) {
                    $('#modalSendConfirmEthToken .modal-body').html(
                        $('<p>').append(
                            $('<img>', {src:ret.user.avatar,width:'300',class: 'img-circle'})
                        )
                    )
                    .append(
                        $('<p>').html(ret.user.name2)
                    )
                    .append(
                        $('<p>').html($('<b>').html('{$str2}'))
                    )
                    .append(
                        $('<p>').append(
                            $('<code>').html(ret.billing.address)
                        )
                    )
                    .append(
                        $('<p>').html(ret.billing.name)
                    )
                    ;
                } else {
                    $('#modalSendConfirmEthToken .modal-body').html(
                        $('<p>').html($('<b>').html('{$str2}'))
                    )
                    .append(
                        $('<p>').append(
                            $('<code>').html(ret.address)
                        )
                    )
                }
                $('#modalSendConfirmEthToken').modal();
                $('#modalSendEthToken').off('hidden.bs.modal');
                b.on('click', functionButtonSendEthToken);
                b.html('{$str3}');
                b.removeAttr('disabled');
            });
        },
        errorScript: function(ret) {
            switch (ret.id) {
                case 101:
                case 102:
                    var f = $('#modalSendEthToken');
                    $.each(ret.data,function(i,v) {
                        console.log([i,v]);
                        var name = v.name;
                        var value = v.value;
                        var t = f.find('.recipient-field-' + name);
                        console.log([t,value.join('<br>')]);    
                        t.addClass('has-error');
                        t.find('p.help-block-error').html(value.join('<br>')).show();
                    });

                    break;
            }
            b.on('click', functionButtonSendEthToken);
            b.html('{$str3}');
            b.removeAttr('disabled');
        }
    });
};
$('.buttonModalSendEthToken').click(functionButtonSendEthToken);
JS
                );

                ?>
                <button type="button" class="btn btn-primary buttonModalSendEthToken" style="width: 100%;"><?= \Yii::t('c.TsqnzVaJuC', 'Перевести') ?></button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div><!-- /.modal -->


<div class="modal fade" tabindex="-1" role="dialog" id="modalSendConfirmEthToken">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">&times;</span></button>
                <h4 class="modal-title  text-center"><?= \Yii::t('c.TsqnzVaJuC', 'Подтверждение') ?></h4>
            </div>
            <div class="modal-body text-center">
                content
            </div>
            <div class="modal-footer">
                <?php
                $str4 = Yii::t('c.TsqnzVaJuC', 'Транзакция успешно отправлена');
                $str5 = Yii::t('c.TsqnzVaJuC', 'Подтвердить');
                $p = new \avatar\modules\ETH\ServiceEtherScan();
                $url = $p->getUrl('/tx/');
                $this->registerJs(<<<JS
var functionButtonConfirmEthToken = function() {
    var b = $(this);
    b.off('click');
    b.html($('<i>', {class: 'fa fa-spinner fa-spin fa-fw'}));
    b.attr('disabled', 'disabled');
    
    var ret = $('#modalSendEthToken').data('ret');
    
    $('#modalSendConfirmEthToken').off('hidden.bs.modal');
    $('#modalSendConfirmEthToken').on('hidden.bs.modal', function(e) {
        $('#modalSendConfirmEthToken').off('hidden.bs.modal');
        $('.buttonModalConfirmEthToken').show();
    });
    ajaxJson({
        url: '/cabinet-bills-token/send-confirm',
        data: $('#formOutEthToken').serializeArray(),
        success: function(ret) {
            $('#modalSendConfirmEthToken').on('hidden.bs.modal', function(e) {
                $('#modalInfoEthToken .modal-body')
                .append(
                    $('<p>').html('{$str4}')
                )
                .append(
                    $('<p>').append(
                        $('<a>', {target: '_blank', href: '{$url}' + ret.address}).html(
                            $('<code>').html(
                                ret.address
                            )
                        )
                    )
                )
                ;
                $('#modalInfoEthToken').on('hidden.bs.modal', function(e) {
                    window.location.reload();
                });
                $('#modalInfoEthToken').modal();
            }).modal('hide');
            b.on('click', functionButtonConfirmEthToken);
            b.html('{$str5}');
            b.removeAttr('disabled');
        },
        errorScript: function(ret) {
            switch (ret.id) {
                case 101:
                case 102:
                    $.each(ret.data,function(i,v) {
                        var name = v.name;
                        var value = v.value;
                        $('#modalSendConfirmEthToken .modal-body')
                        .append(
                            $('<p>', {class: 'alert alert-danger'}).html(value.join('<br>'))
                        );
                    });
                    $('#modalSendConfirmEthToken .modal-body')
                        .append(
                            $('<button>', {class: 'btn btn-primary', style: 'width: 100%'}).html('Назад').click(function(e){
                                $('#modalSendConfirmEthToken').on('hidden.bs.modal', function(e) {
                                    $('#modalSendEthToken').modal();
                                }).modal('hide');
                            })
                        );
                    $('.buttonModalConfirmEthToken').hide();
                    break;
            }
            b.on('click', functionButtonConfirmEthToken);
            b.html('{$str5}');
            b.removeAttr('disabled');
        }
    });

};
$('.buttonModalConfirmEthToken').click(functionButtonConfirmEthToken);
JS
                );
                ?>
                <button type="button" class="btn btn-primary buttonModalConfirmEthToken" style="width: 100%;"><?= \Yii::t('c.TsqnzVaJuC', 'Подтвердить') ?></button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div><!-- /.modal -->

<div class="modal fade" id="modalInfoEthToken" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">&times;</span></button>
                <h4 class="modal-title text-center" id="myModalLabel"><?= \Yii::t('c.TsqnzVaJuC', 'Информация') ?></h4>
            </div>
            <div class="modal-body text-center">

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" style="width: 100%;" data-dismiss="modal">
                    <?= \Yii::t('c.TsqnzVaJuC', 'Закрыть') ?>
                </button>
            </div>
        </div>
    </div>
</div>
