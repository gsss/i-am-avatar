<?php
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $billing \common\models\avatar\UserBill */

$this->title = 'Карты';

$this->registerJs(<<<JS
JS
);

$count = 10;
$list = [];
for ($i = 1; $i <= $count; $i++) {
    $name = str_repeat('0', 2 - strlen($i)) . $i;
    $list[] = [
        'id'   => $i,
        'file' => '/images/controller/cabinet-bills/cards/base/' . $name . '.' . 'jpg'
    ];
}
?>
<div class="container" style="padding-bottom: 70px;">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header text-center"><?= $this->title ?></h1>
        </div>
        <?php foreach ($list as $item) { ?>
            <div class="col-lg-4">
                <a href="<?= Url::to(['cabinet-bills/card-download', 'billing_id' => $billing->id, 'card' => $item['id']]) ?>">
                    <img src="<?= $item['file'] ?>" width="100%" class="thumbnail" data-id="<?= $item['id'] ?>">
                </a>
            </div>
        <?php } ?>
    </div>
</div>


