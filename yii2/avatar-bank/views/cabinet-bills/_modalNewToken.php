<?php
/**
 * Created by PhpStorm.
 * User: s.arhangelskiy
 * Date: 23.01.2017
 * Time: 10:21
 */
/* @var $this yii\web\View */

use common\models\avatar\Currency;

$strAdd = \Yii::t('c.tvITsCQPKS', 'Добавить');

/** @var \common\models\UserAvatar $user */
$user = Yii::$app->user->identity;

$this->registerJs(<<<JS
$('.buttonNewToken').click(function() {
    $('#modalNewToken').modal();
});

var buttonModalNewTokenEvent = function() {
    var b = $(this);
    b.off('click');
    b.html($('<i>', {class: 'fa fa-spinner fa-spin fa-fw'}));
    b.attr('disabled', 'disabled');

    var dataPost = $('#formNewToken').serializeArray();
    console.log(dataPost);
    ajaxJson({
        url: '/cabinet-bills/new-token',
        data: dataPost,
        success: function(ret) {
            $('#modalNewToken').modal('hide');
            window.location.reload();
        },
        errorScript: function(ret) {
            switch (ret.id) {
                case 102:
                    var f = $('#formNewToken');
                    $.each(ret.data,function(i, v) {
                        var name = v.name;
                        var value = v.value;
                        var o = f.find('#recipient-'+ name);
                        var t = f.find('.recipient-field-' + name);
                        t.addClass('has-error');
                        t.find('p.help-block-error').html(value.join('<br>')).show();
                    });
                
                    break;
            }
            b.on('click', buttonModalNewTokenEvent);
            b.html('{$strAdd}');
            b.removeAttr('disabled');
        }
    });
};

$('#formNewToken .form-control').on('focus', function() {
    var o = $(this);
    var p = o.parent();
    if (p.hasClass('input-group')) {
        p = p.parent();
    }
    p.removeClass('has-error');
    p.find('p.help-block-error').hide();
});


$('.buttonModalNewToken').click(buttonModalNewTokenEvent);

//$('#field-token_id').selectpicker();

JS
);
//\common\assets\SelectPicker::register($this);

$tokenList = \common\models\Token::find()
    ->innerJoin('currency', 'currency.id = token.currency_id')
    ->select([
        'token.id',
        'token.address',
        'currency.title',
        'currency.code',
        'currency.image',
    ])
    ->asArray()
    ->all();

/**
 * @var array
 * [
 *  'id'
 *  'name'
 *  'address'
 * ]
 */
$billingList = \common\models\avatar\UserBill::find()
    ->where([
        'user_id'      => Yii::$app->user->id,
        'mark_deleted' => 0,
        'currency'     => Currency::ETH,
    ])
    ->select([
        'id',
        'name',
        'address',
    ])
    ->asArray()
    ->all();

?>


<div class="modal fade" tabindex="-1" role="dialog" id="modalNewToken">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">&times;</span></button>
                <h4 class="modal-title"><?= \Yii::t('c.tvITsCQPKS', 'Добавление токена') ?></h4>
            </div>
            <div class="modal-body">
                <form id="formNewToken">
                    <div class="form-group recipient-field-address">
                        <label for="recipient-name"
                               class="control-label"><?= \Yii::t('c.TsqnzVaJuC', 'Название счета') ?>:</label>
                        <input type="text" class="form-control" id="field-name" name="name">
                        <p class="help-block"></p>

                        <p class="help-block help-block-error" style="display: none;"></p>
                    </div>
                    <div class="form-group recipient-field-address">
                        <label for="recipient-name" class="control-label"><?= \Yii::t('c.tvITsCQPKS', 'Пароль от кабинета') ?>:</label>
                        <input type="password" class="form-control" id="field-password" name="password">
                        <p class="help-block"></p>

                        <p class="help-block help-block-error" style="display: none;"></p>
                    </div>
                    <div class="form-group recipient-field-token_id">

                        <label for="token_id" class="control-label"><?= \Yii::t('c.tvITsCQPKS', 'Токен') ?>:</label>

                        <select class="form-control" id="field-token_id" name="token_id" data-width="300px">
                            <option value=""><?= \Yii::t('c.tvITsCQPKS', 'Ничего не выбрано') ?></option>
                            <?php foreach ($tokenList as $token) { ?>
                                <option value="<?= $token['id'] ?>"><?= $token['title'] . ' (' . $token['code'] . ')' . ' ' . $token['address'] ?></option>
                            <?php } ?>
                        </select>

                        <p class="help-block"></p>

                        <p class="help-block help-block-error" style="display: none;"></p>
                    </div>

                    <div class="form-group recipient-field-billing_id">
                        <label for="billing_id" class="control-label"><?= \Yii::t('c.tvITsCQPKS', 'Счет') ?>:</label>
                        <select class="form-control" id="field-billing_id" name="billing_id">
                            <option value=""><?= \Yii::t('c.tvITsCQPKS', 'Ничего не выбрано') ?></option>
                            <?php foreach ($billingList as $token) { ?>
                                <option value="<?= $token['id'] ?>"><?= $token['name'] . ' (' . $token['address'] . ')' ?></option>
                            <?php } ?>
                        </select>
                        <p class="help-block"></p>

                        <p class="help-block help-block-error" style="display: none;"></p>
                    </div>

                    <input type="hidden" value="<?= $user->password_save_type ?>"
                           name="password_type">
                </form>
            </div>
            <div class="modal-footer">
                <button type="button"
                        class="btn btn-primary buttonModalNewToken" style="width: 100%;"><?= \Yii::t('c.tvITsCQPKS', 'Добавить') ?>
                </button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div><!-- /.modal -->



