<?php
/**
 * Created by PhpStorm.
 * User: Бог-Творец
 * Date: 29.09.2016
 * Time: 19:49
 */

use yii\helpers\Html;

/** @var array $transactions */
/** @var int $page номер страницы */
/** @var int $id идентификатор счета */
/** @var \common\models\avatar\UserBill $billing счет */

/** @var \common\models\UserAvatar $user */
$user = Yii::$app->user->identity;
$currencyView = $user->currency_view;

$header1 = 'BTC';
$header2 = Yii::t('c.dEj9SM57Nh', 'Комиссия') . ' BTC';
if (!is_null($currencyView)) {
    switch ($currencyView) {
        case 1: // RUB
            $header1 = 'RUB';
            $header2 = Yii::t('c.dEj9SM57Nh', 'Комиссия') . ' RUB';
            break;
        case 2: // USD
            $header1 = 'USD';
            $header2 = Yii::t('c.dEj9SM57Nh', 'Комиссия') . ' USD';
            break;
    }
}

/**
 * $transactions - транзакции = itewmsPerPage + 1, чтобы последнюю не показывать и понять что есть еще чего запрашивать, поэтому вывожу кнопку Далее!
 * $start - int - старт от которой транзакции сейчас если первая то 1
 */
$columns = [
    [
        'header'         => 'ID',
        'contentOptions' => [
            'class' => 'code',
        ],
        'content'        => function ($item) {
            // сколько было переведено денег
            {
                $direction = ($item['wallet_value_change'] < 0) ? 'minus' : 'plus';
                $v = $item['wallet_value_change'];
                if ($v < 0) {
                    $v += $item['total_fee'];
                }
                $amount = $v / 100000000;
            }
            // комиссия, если $direction == 'plus' то $fee = 0
            {
                $fee = 0;
                if ($direction == 'minus') {
                    $fee = $item['total_fee'] / 100000000;
                }
            }

            return
                Html::tag(
                    'span',
                    substr($item['hash'], 0, 8) . '...',
                    [
                        'class' => 'js-buttonTransactionInfo textDecorated',
                        'role'  => 'button',
                        'title' => Yii::t('c.dEj9SM57Nh', 'Подробнее'),
                        'data'  => [
                            'toggle'      => 'tooltip',
                            'placement'   => 'bottom',
                            'transaction' => [
                                'hash'      => $item['hash'],
                                'amount'    => $amount,
                                'direction' => $direction,
                                'fee'       => $fee,
                            ],
                        ],
                    ]
                );
        },
    ],
    [
        'header'         => Yii::t('c.dEj9SM57Nh', 'Подтверждений'),
        'headerOptions'  => [
            'style' => 'text-align:right',
        ],
        'contentOptions' => [
            'style' => 'text-align:right',
        ],
        'content'        => function ($item) {
            $confirmations = $item['confirmations'];
            if ($confirmations == 0) {
                $class = 'label label-danger';
            } else
                if ($confirmations < 6) {
                    $class = 'label label-warning';
                } else {
                    $class = 'label label-success';
                }
            return
                Html::tag(
                    'span',
                    Yii::$app->formatter->asDecimal($confirmations, 0),
                    ['class' => $class]
                );
        },
    ],

    [
        'header'         => $header1,
        'headerOptions'  => [
            'style' => 'text-align:right;',
        ],
        'contentOptions' => function ($item) {
            $style = ['text-align:right'];
            if ($item['wallet_value_change'] < 0) {
                $style[] = 'color: #F45F5F';
            } else {
                $style[] = 'color: #6ED098';
            }
            return [
                'style' => join(';', $style),
                'class' => 'code',
            ];
        },
        'content'        => function ($item) {
            if (isset($item['wallet_value_change_currency'])) {
                return $item['wallet_value_change_currency']['value'];
            }
            $v = $item['wallet_value_change'];
            if ($v < 0) {
                $v += $item['total_fee'];
            }
            return Yii::$app->formatter->asDecimal($v / 100000000, 5);
        },
    ],
    [
        'header'         => $header2,
        'headerOptions'  => [
            'style' => 'text-align:right',
        ],
        'contentOptions' => [
            'style' => 'color: #F45F5F;text-align:right;',
            'class' => 'code',
        ],
        'content'        => function ($item) {
            if ($item['wallet_value_change'] > 0) {
                return '';
            }
            if (isset($item['wallet_value_change_currency'])) {
                return $item['wallet_value_change_currency']['fee'];
            }

            return '-' . Yii::$app->formatter->asDecimal($item['total_fee'] / 100000000, 5);
        },
    ],
    [
        'header'    => Yii::t('c.dEj9SM57Nh', 'Время'),
        'contentOptions' => [
            'nowrap' => 'nowrap',
        ],
        'content'   => function ($item) {
            $t = $item['time'];
            $t = str_replace('+00:00', '', $t);
            $t = str_replace('T', ' ', $t);

            return Html::tag('span', \cs\services\DatePeriod::back($t, ['isShort' => true]), [
                'title' => Yii::$app->formatter->asDatetime($t),
                'data' => ['toggle' => 'tooltip', 'placement' => 'bottom'],
                'class' => 'textDecorated',
            ]);
        },
        'attribute' => 'time',
    ],
    [
        'header'  => Yii::t('c.dEj9SM57Nh', 'Коментарий'),
        'content' => function ($item) {
            return $item['comment'];
        },
    ],
    [
        'header'  => Yii::t('c.dEj9SM57Nh', 'Направление'),
        'headerOptions'  => [
            'style' => 'text-align:right',
        ],
        'contentOptions' => [
            'style' => 'text-align:right',
        ],
        'content' => function ($item) {
            if (isset($item['type'])) {
                if ($item['type'] == 1) {
                    return Html::tag('i', Html::tag('i', null, ['class' => 'glyphicon glyphicon-chevron-left']), ['class' => 'label label-success']);
                } else {
                    return Html::tag('i', Html::tag('i', null, ['class' => 'glyphicon glyphicon-chevron-right']), ['class' => 'label label-danger']);
                }
            } else {
                if ($item['wallet_value_change'] > 0) {
                    return Html::tag('i', Html::tag('i', null, ['class' => 'glyphicon glyphicon-chevron-left']), ['class' => 'label label-success']);
                } else {
                    return Html::tag('i', Html::tag('i', null, ['class' => 'glyphicon glyphicon-chevron-right']), ['class' => 'label label-danger']);
                }
            }
        },
    ],
    [
        'header'  => Yii::t('c.dEj9SM57Nh', 'Счет'),
        'content' => function ($item) {
            if (isset($item['data'])) {
                if ($item['data'] != '') {
                    $config = \yii\helpers\ArrayHelper::toArray(json_decode($item['data']));
                    $billing = \common\models\avatar\UserBill::findOne($config['billing_id']);
                    if ($config['user_id'] == Yii::$app->user->id) {
                        return Html::a(Html::img('/images/controller/cabinet-bills/transactions/btc.jpg', [
                            'width' => 30,
                            'class' => 'img-circle',
                            'data'  => [
                                'toggle'    => 'tooltip',
                                'placement' => 'left',
                            ],
                            'title' => $billing->name,
                        ]), '/cabinet-bills/transactions/' . $config['billing_id']);
                    } else {
                        return Html::a(Html::img('/images/controller/cabinet-bills/transactions/btc.jpg', [
                            'width' => 30,
                            'class' => 'img-circle',
                            'data'  => [
                                'toggle'    => 'tooltip',
                                'placement' => 'left',
                            ],
                            'title' => $billing->name,
                        ]), '/user/billing?id=' . $config['billing_id']);
                    }
                }
            }

            return '';
        },
    ],
    [
        'header'  => Yii::t('c.dEj9SM57Nh', 'Пользователь'),
        'content' => function ($item) {
            if (isset($item['data'])) {
                if ($item['data'] != '') {
                    $config = \yii\helpers\ArrayHelper::toArray(json_decode($item['data']));
                    $user = \common\models\UserAvatar::findOne($config['user_id']);
                    if ($config['user_id'] != Yii::$app->user->id) {
                        return Html::a(Html::img($user->getAvatar(), [
                            'width' => 30,
                            'class' => 'img-circle',
                            'data'  => [
                                'toggle'    => 'tooltip',
                                'placement' => 'right',
                            ],
                            'title' => \yii\helpers\Html::encode($user->getName2()),
                        ]), '/user/' . $config['user_id'], []);
                    }
                }
            }

            return '';
        },
    ],
];

if (Yii::$app->deviceDetect->isMobile()) {
    $columns = [
        $columns[4],
        $columns[2],
        $columns[3],
    ];
}
?>
<style>
    .table .code {
        font-family: 'Courier New', Monospace;
    }

</style>

<?= \yii\grid\GridView::widget([
    'dataProvider' => new \yii\data\ArrayDataProvider([
        'allModels' => $transactions['data'],
    ]),
    'tableOptions' => [
        'class' => 'table table-hover table-striped',
        'id'    => 'tableTransaction',
    ],
    'columns'      => $columns,
    'summary'      => '',

]);
?>

<ul class="pagination">
    <?php if ($page == 1) { ?>
        <li class="prev disabled"><span>«</span></li>
    <?php } else { ?>
        <li class="prev"><a href="<?= \yii\helpers\Url::to([
                'cabinet-bills/transactions-list',
                'id'   => $id,
                'page' => $page - 1,
            ]) ?>" data-page="0" class="paginationItem">«</a></li>
    <?php } ?>
    <?php for ($i = 1; $i <= $page; $i++) { ?>
        <li><a href="<?= \yii\helpers\Url::to([
                'cabinet-bills/transactions-list',
                'id'   => $id,
                'page' => $i,
            ]) ?>"
               data-page="1" class="paginationItem"><?= $i ?></a></li>
    <?php } ?>
    <?php if (count($transactions['data']) == 20) { ?>
        <li class="next"><a
                    href="<?= \yii\helpers\Url::to([
                        'cabinet-bills/transactions-list',
                        'id'   => $id,
                        'page' => $page + 1,
                    ]) ?>"
                    data-page="<?= $page ?>" class="paginationItem">»</a></li>
    <?php } ?>
</ul>
