<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;
use app\models\UnionCategory;

/* @var $this yii\web\View */
/* @var $seeds string */
/* @var $user \common\models\UserAvatar */

?>
<html>
<head>
    <title>Документ</title>
    <style>
        td {
            font-size: 150%;
            font-family: "Courier New", Courier, monospace;
        }
    </style>
</head>
<body>
<table width="100%">
    <tr>
        <td width="33%">&nbsp;</td>
        <td width="33%" style="text-align: center;"><img src="/images/pdf/lovo-avr-04.png" width="200"></td>
        <td width="33%">&nbsp;</td>
    </tr>
</table>
<hr>
<h4 style="text-align: center;font-weight: normal;">
    <span style="font-size: 250%;">Ключи от кошелька</span>
</h4>
<table class="table table-hover table-striped">
    <tr>
        <td style="text-align: right; vertical-align: top;"><b>SEEDS</b></td>
        <td style='font-family: "Courier New", Courier, monospace'><?= $seeds ?></td>
    </tr>
    <tr>
        <td style="text-align: right;"><b>Email</b></td>
        <td><?= $user->email ?></td>
    </tr>
    <tr>
        <td style="text-align: right;"><b>Time</b></td>
        <td><?= Yii::$app->formatter->asDatetime(time(),'php:Y-m-d H:i:s (P)') ?></td>
    </tr>
</table>

<?php $link = Url::to(['help/item', 'id' => 1], true); ?>
<p>О том как восстановить доступ к кошельку через эти ключи смотрите инструкцию по ссылке <?= Html::a($link, $link) ?>

</body>
</html>



