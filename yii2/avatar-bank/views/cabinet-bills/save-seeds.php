<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;
use app\models\UnionCategory;
use yii\db\Query;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model \common\models\avatar\UserBill */

$this->title = 'Сохранить ключи';

?>
<div class="container">
    <div class="col-lg-12">
        <h1 class="page-header text-center"><?= Html::encode($this->title) ?></h1>
    </div>
    <div class="col-lg-4 col-lg-offset-4">
        <?php if (\common\models\UserSeed::has()) { ?>
            <p class="alert alert-danger text-center">У вас уже есть SEEDS</p>
        <?php }else { ?>
            <?php $form = ActiveForm::begin([
                'options' => ['enctype' => 'multipart/form-data']
            ]); ?>
            <?= $form->field($model, 'password')->passwordInput() ?>

            <hr>
            <div class="form-group">
                <?= Html::submitButton('Сохранить', [
                    'class' => 'btn btn-primary',
                    'style' => 'width:100%',
                ]) ?>
            </div>
            <?php ActiveForm::end(); ?>
        <?php } ?>
    </div>
</div>
