<?php
/**
 * Created by PhpStorm.
 * User: Бог-Творец
 * Date: 29.09.2016
 * Time: 19:49
 */

use yii\helpers\Html;

/**
 * {
 * "blockNumber": "65204",
 * "timeStamp": "1439232889",
 * "hash": "0x98beb27135aa0a25650557005ad962919d6a278c4b3dde7f4f6a3a1e65aa746c",
 * "nonce": "0",
 * "blockHash": "0x373d339e45a701447367d7b9c7cef84aab79c2b2714271b908cda0ab3ad0849b",
 * "transactionIndex": "0",
 * "from": "0x3fb1cd2cd96c6d5c0b5eb3322d807b34482481d4",
 * "to": "0xde0b295669a9fd93d5f28d9ec85e40f4cb697bae",
 * "value": "0",
 * "gas": "122261",
 * "gasPrice": "50000000000",
 * "isError": "0",
 * "input": "0xf00d4b5d000000000000000000000000036c8cecce8d8bbf0831d840d7f29c9e3ddefa63000000000000000000000000c5a96db085dda36ffbe390f455315d30d6d3dc52",
 * "contractAddress": "",
 * "cumulativeGasUsed": "122207",
 * "gasUsed": "122207",
 * "confirmations": "3745410"
 *
 * "t": "1" - тип транзакции
 * "comment": "1"
 * "type": "1" - направление перевода
 * "data": "{}"
 *
 * "value_converted" - float - значение сконвентированное Если `$user->currency_view` > 0
 * "fee_converted"   - float - значение сконвентированное Если `$user->currency_view` > 0
 * }
 */

/** @var array                          $transactions */
/** @var int                            $page номер страницы */
/** @var int                            $id идентификатор счета */
/** @var \common\models\avatar\UserBill $billing счет */

/** @var \common\models\UserAvatar $user */
$user = Yii::$app->user->identity;
$currencyView = $user->currency_view;

$header1 = 'ETH';
$header2 = 'Комиссия ETH';
if (!is_null($currencyView)) {
    switch ($currencyView) {
        case 1: // RUB
            $header1 = 'RUB';
            $header2 = 'Комиссия RUB';
            break;
        case 2: // USD
            $header1 = 'USD';
            $header2 = 'Комиссия USD';
            break;
    }
}

/**
 * $transactions - транзакции = itewmsPerPage + 1, чтобы последнюю не показывать и понять что есть еще чего
 * запрашивать, поэтому вывожу кнопку Далее!
 * $start - int - старт от которой транзакции сейчас если первая то 1
 */
$columns = [
    [
        'header'         => 'ID',
        'contentOptions' => [
            'class' => 'code',
        ],
        'content'        => function ($item) {
            $data = [
                'hash' => $item['hash'],
            ];

            // fee
            $gasPrice = $item['gasPrice'];
            $gasUsed = $item['gasUsed'];
            $feeWei = $gasPrice * $gasUsed;
            $feeEth = $feeWei / 1000000000000000000;
            $data['fee'] = Yii::$app->formatter->asDecimal($feeEth, 18);
            if (isset($item['fee_converted'])) {
                $data['fee_converted'] = $item['fee_converted'];
            }

            // type
            if (isset($item['type'])) {
                if ($item['type'] == -1) {
                    $data['direction'] = 'minus';
                } else {
                    $data['direction'] = 'plus';
                }
            }

            // amount
            $feeWei = $item['value'];
            $feeEth = $feeWei / 1000000000000000000;
            $data['amount'] = Yii::$app->formatter->asDecimal($feeEth, 18);
            if (isset($item['value_converted'])) {
                $data['amount_converted'] = $item['value_converted'];
            }

            return
                Html::tag(
                    'span',
                    substr($item['hash'], 0, 8) . '...',
                    [
                        'class' => 'js-buttonTransactionInfo textDecorated',
                        'role'  => 'button',
                        'title' => 'Подробнее',
                        'data'  => [
                            'toggle'      => 'tooltip',
                            'placement'   => 'bottom',
                            'transaction' => $data,
                        ],
                    ]
                );
        },
    ],
    [
        'header'         => 'Подтверждений',
        'headerOptions'  => [
            'style' => 'text-align:right',
        ],
        'contentOptions' => [
            'style' => 'text-align:right',
        ],
        'content'        => function ($item) {
            $confirmations = $item['confirmations'];
            if ($confirmations == 0) {
                $class = 'label label-danger';
            } else {
                if ($confirmations < 6) {
                    $class = 'label label-warning';
                } else {
                    $class = 'label label-success';
                }
            }

            return
                Html::tag(
                    'span',
                    Yii::$app->formatter->asDecimal($confirmations, 0),
                    ['class' => $class]
                );
        },
    ],

    [
        'header'         => $header1,
        'headerOptions'  => [
            'style' => 'text-align:right;',
        ],
        'contentOptions' => function ($item) {
            $style = ['text-align:right'];
            if ($item['type'] < 0) {
                $style[] = 'color: #F45F5F';
            } else {
                $style[] = 'color: #6ED098';
            }
            return [
                'style' => join(';', $style),
                'class' => 'code',
            ];
        },
        'content'        => function ($item) {
            if (isset($item['value_converted'])) {
                return Yii::$app->formatter->asDecimal($item['value_converted'], 2);
            } else {
                $feeWei = $item['value'];
                $feeEth = $feeWei / 1000000000000000000;

                return Yii::$app->formatter->asDecimal($feeEth, 18);
            }
        },
    ],
    [
        'header'         => $header2,
        'headerOptions'  => [
            'style' => 'text-align:right',
        ],
        'contentOptions' => [
            'style' => 'color: #F45F5F;text-align:right;',
            'class' => 'code',
        ],
        'content'        => function ($item) {
            if (isset($item['type'])) {
                if ($item['type'] == -1) {
                    if (isset($item['fee_converted'])) {
                        return Yii::$app->formatter->asDecimal($item['fee_converted'], 2);
                    } else {
                        $gasPrice = $item['gasPrice'];
                        $gasUsed = $item['gasUsed'];
                        $feeWei = $gasPrice * $gasUsed;
                        $feeEth = $feeWei / 1000000000000000000;

                        return Yii::$app->formatter->asDecimal($feeEth, 18);
                    }
                }
            }
            return '';
        },
    ],
    [
        'header'    => 'Время',
        'content'   => function ($item) {
            $t = $item['timeStamp'];

            return Html::tag('span', \cs\services\DatePeriod::back($t, ['isShort' => true]), [
                'title' => Yii::$app->formatter->asDatetime($t),
                'data'  => ['toggle' => 'tooltip', 'placement' => 'bottom'],
                'class' => 'textDecorated',
            ]);
        },
        'attribute' => 'time',
    ],
    [
        'header'  => 'Коментарий',
        'content' => function ($item) {
            return $item['comment'];
        },
    ],
    [
        'header'         => 'Направление',
        'headerOptions'  => [
            'style' => 'text-align:right',
        ],
        'contentOptions' => [
            'style' => 'text-align:right',
        ],
        'content'        => function ($item) {
            if (isset($item['type'])) {
                if ($item['type'] == 1) {
                    return Html::tag('i', Html::tag('i', null, ['class' => 'glyphicon glyphicon-chevron-left']), ['class' => 'label label-success']);
                } else {
                    return Html::tag('i', Html::tag('i', null, ['class' => 'glyphicon glyphicon-chevron-right']), ['class' => 'label label-danger']);
                }
            } else {
                return '';
            }
        },
    ],
    [
        'header'  => 'Счет',
        'content' => function ($item) {
            $p = new \avatar\modules\ETH\ServiceEtherScan();
            $url = $p->getUrl();

            if (isset($item['data'])) {
                if ($item['data'] != '') {
                    $config = $item['data'];
                    $billing = \common\models\avatar\UserBill::findOne($config['billing_id']);
                    if ($config['user_id'] == Yii::$app->user->id) {
                        return Html::a(Html::img('/images/controller/cabinet-bills/index/eth.png', [
                            'width' => 30,
                            'class' => 'img-circle',
                            'data'  => [
                                'toggle'    => 'tooltip',
                                'placement' => 'left',
                            ],
                            'title' => $billing->name,
                        ]), '/cabinet-bills/transactions-eth' . '?' . 'id' . '=' . $config['billing_id']);
                    } else {
                        return Html::a(Html::img('/images/controller/cabinet-bills/index/eth.png', [
                            'width' => 30,
                            'class' => 'img-circle',
                            'data'  => [
                                'toggle'    => 'tooltip',
                                'placement' => 'left',
                            ],
                            'title' => $billing->name,
                        ]), '/user/billing?id=' . $config['billing_id']);
                    }
                }
            } else {
                if ($item['value'] == 0 and strlen($item['input']) > 2 and $item['to'] != '') {
                    // контракт
                    $a = '/address/'.$item['to'];
                    $link = $url . $a;

                    return Html::a(Html::img('https://cdn2.iconfinder.com/data/icons/investment/512/Icon_9-512.png', [
                        'width'  => 24,
                        'target' => '_blank',
                        'data'   => [
                            'toggle'    => 'tooltip',
                            'placement' => 'left',
                        ],
                        'title'  => substr($item['to'], 0, 8) . ' ...',
                    ]), $link, ['target' => '_blank']);
                } else {
                    if ($item['type'] == 1) {
                        $a = '/address/' . $item['from'];
                    } else {
                        $a = '/address/' . $item['to'];
                    }
                    $link = $url . $a;

                    return Html::a(Html::img('/images/controller/cabinet-bills/index/eth.png', [
                        'width'  => 24,
                        'target' => '_blank',
                        'data'   => [
                            'toggle'    => 'tooltip',
                            'placement' => 'left',
                        ],
                        'title'  => substr($item['to'], 0, 8) . ' ...',
                    ]), $link, ['target' => '_blank']);
                }
            }

            return '';
        },
    ],
    [
        'header'  => 'Пользователь',
        'content' => function ($item) {
            if (isset($item['data'])) {
                if ($item['data'] != '') {
                    $config = $item['data'];
                    $user = \common\models\UserAvatar::findOne($config['user_id']);
                    if ($config['user_id'] != Yii::$app->user->id) {
                        return Html::a(Html::img($user->getAvatar(), [
                            'width' => 30,
                            'class' => 'img-circle',
                            'data'  => [
                                'toggle'    => 'tooltip',
                                'placement' => 'right',
                            ],
                            'title' => \yii\helpers\Html::encode($user->getName2()),
                        ]), '/user/' . $config['user_id'], []);
                    }
                }
            }

            return '';
        },
    ],
];

if (Yii::$app->deviceDetect->isMobile()) {
    $columns = [
        $columns[4],
        $columns[2],
        $columns[3],
    ];
}
?>
<style>
    .table .code {
        font-family: 'Courier New', Monospace;
    }

</style>

<?= \yii\grid\GridView::widget([
    'dataProvider' => new \yii\data\ArrayDataProvider([
        'allModels' => $transactions,
    ]),
    'tableOptions' => [
        'class' => 'table table-hover table-striped',
        'id'    => 'tableTransaction',
    ],
    'columns'      => $columns,
    'summary'      => '',

]);
?>

<ul class="pagination">
    <?php if ($page == 1) { ?>
        <li class="prev disabled"><span>«</span></li>
    <?php } else { ?>
        <li class="prev"><a href="<?= \yii\helpers\Url::to([
                'cabinet-bills/transactions-list-eth',
                'id'   => $id,
                'page' => $page - 1,
            ]) ?>" data-page="0" class="paginationItem">«</a></li>
    <?php } ?>
    <?php for ($i = 1; $i <= $page; $i++) { ?>
        <?php
        $liOptions = [];
        if ($page == $i) {
            $liOptions = ['class' => 'active'];
            $html = Html::tag('li', Html::a(
                $i,
                'javascript:void(0)',
                [
                    'data'  => ['page' => $i],
                    'class' => 'paginationItem',
                ]
            ), $liOptions);
        } else {
            $html = Html::tag('li', Html::a(
                $i,
                [
                    'cabinet-bills/transactions-list-eth',
                    'id'   => $id,
                    'page' => $i,
                ],
                [
                    'data'  => ['page' => $i],
                    'class' => 'paginationItem',
                ]
            ), $liOptions);
        }
        ?>
        <?= $html ?>
    <?php } ?>
    <?php if (count($transactions) == 20) { ?>
        <li class="next"><a
                    href="<?= \yii\helpers\Url::to([
                        'cabinet-bills/transactions-list-eth',
                        'id'   => $id,
                        'page' => $page + 1,
                    ]) ?>"
                    data-page="<?= $page ?>" class="paginationItem">»</a></li>
    <?php } ?>
</ul>
