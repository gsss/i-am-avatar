<?php
/**
 * Created by PhpStorm.
 * User: Бог-Творец
 * Date: 29.09.2016
 * Time: 19:49
 */

use yii\helpers\Html;

/** @var array                              $transactions */
/** @var int                                $page номер страницы */
/** @var int                                $id идентификатор счета */
/** @var \common\models\avatar\UserBill     $billing счет */


/** @var \common\models\UserAvatar $user */
$user = Yii::$app->user->identity;
$currencyView = $user->currency_view;

$currencyId = $billing->currency;
$currencyObject = \common\models\avatar\Currency::findOne($currencyId);

Yii::$app->session->set('$currencyObject->image', $currencyObject->image);
Yii::$app->session->set('$currencyObject->decimals', $currencyObject->decimals);

$header1 = $currencyObject->code;

/**
 * $transactions - транзакции = itewmsPerPage + 1, чтобы последнюю не показывать и понять что есть еще чего
 * запрашивать, поэтому вывожу кнопку Далее!
 * $start - int - старт от которой транзакции сейчас если первая то 1
 */
$columns = [
    [
        'header'         => 'ID',
        'contentOptions' => [
            'class' => 'code',
        ],
        'content'        => function ($item) {

            // amount
            $value = $item['value'];
            $decimals = Yii::$app->session->get('$currencyObject->decimals');
            $value = $value / pow(10, $decimals);

            $data = [
                'hash' => $item['transactionHash'],
                'value'       => $value,
                'from'        => $item['from'],
                'to'          => $item['to'],
            ];

            // type
            if (isset($item['direction'])) {
                if ($item['direction'] == -1) {
                    $data['direction'] = 'minus';
                } else {
                    $data['direction'] = 'plus';
                }
            }

            return
                Html::tag(
                    'span',
                    substr($item['transactionHash'], 0, 8) . '...',
                    [
                        'class' => 'js-buttonTransactionInfo textDecorated',
                        'role'  => 'button',
                        'title' => 'Подробнее',
                        'data'  => [
                            'toggle'      => 'tooltip',
                            'placement'   => 'bottom',
                            'transaction' => $data,
                        ],
                    ]
                );
        },
    ],
    [
        'header'         => $header1,
        'headerOptions'  => [
            'style' => 'text-align:right;',
        ],
        'contentOptions' => function ($item) {
            $style = ['text-align:right'];
            if ($item['direction'] < 0) {
                $style[] = 'color: #F45F5F';
            } else {
                $style[] = 'color: #6ED098';
            }
            return [
                'style' => join(';', $style),
                'class' => 'code',
            ];
        },
        'content'        => function ($item) {
            if ($item['direction'] < 0) {
                $v = -$item['value'];
            } else {
                $v = $item['value'];
            }
            if (isset($item['tokenInfo'])) {
                $v = $v / pow(10, $item['tokenInfo']['decimals']);
            } else {
                $decimals = Yii::$app->session->get('$currencyObject->decimals');
                $v = $v / pow(10, $decimals);
            }
            if (isset($item['value_converted'])) {
                return $item['value_converted'];
            } else {
                return Yii::$app->formatter->asDecimal($v, 2);
            }
        },
    ],
    [
        'header'    => Yii::t('c.oifM0YkVx7', 'Время'),
        'content'   => function ($item) {
            $t = $item['timestamp'];

            return Html::tag('span', \cs\services\DatePeriod::back($t, ['isShort' => true]), [
                'title' => Yii::$app->formatter->asDatetime($t),
                'data'  => ['toggle' => 'tooltip', 'placement' => 'bottom'],
                'class' => 'textDecorated',
            ]);
        },
    ],
    [
        'header'  => Yii::t('c.oifM0YkVx7', 'Коментарий'),
        'content' => function ($item) {
            return $item['comment'];
        },
    ],
    [
        'header'         => Yii::t('c.oifM0YkVx7', 'Направление'),
        'headerOptions'  => [
            'style' => 'text-align:right',
        ],
        'contentOptions' => [
            'style' => 'text-align:right',
        ],
        'content'        => function ($item) {
            if (isset($item['direction'])) {
                if ($item['direction'] == 1) {
                    return Html::tag('i', Html::tag('i', null, ['class' => 'glyphicon glyphicon-chevron-left']), ['class' => 'label label-success']);
                } else {
                    return Html::tag('i', Html::tag('i', null, ['class' => 'glyphicon glyphicon-chevron-right']), ['class' => 'label label-danger']);
                }
            } else {
                return '';
            }
        },
    ],
    [
        'header'  => Yii::t('c.oifM0YkVx7', 'Счет'),
        'content' => function ($item) {
            $v = Yii::$app->session->get('$currencyObject->image');
            if (isset($item['data'])) {
                if ($item['data'] != '') {
                    $config = $item['data'];
                    $billing = \common\models\avatar\UserBill::findOne($config['billing_id']);
                    if ($config['user_id'] == Yii::$app->user->id) {
                        return Html::a(Html::img($v, [
                            'width' => 30,
                            'class' => 'img-circle',
                            'data'  => [
                                'toggle'    => 'tooltip',
                                'placement' => 'left',
                            ],
                            'title' => $billing->name,
                        ]), '/cabinet-bills/transactions-token/' . $config['billing_id']);
                    } else {
                        return Html::a(Html::img($v, [
                            'width' => 30,
                            'class' => 'img-circle',
                            'data'  => [
                                'toggle'    => 'tooltip',
                                'placement' => 'left',
                            ],
                            'title' => $billing->name,
                        ]), '/user/billing?id=' . $config['billing_id']);
                    }
                }
            } else {
                $provider = new \avatar\modules\ETH\ServiceEtherScan();

                if ($item['direction'] == -1) {
                    $url = $provider->getUrl('/token/' . $item['tokenInfo']['address'] . '?a=' . $item['to']);
                    $title = substr($item['to'], 0, 10) . ' ...';
                } else {
                    $url = $provider->getUrl('/token/' . $item['tokenInfo']['address'] . '?a=' . $item['from']);
                    $title = substr($item['from'], 0, 10) . ' ...';
                }

                return Html::a(
                    Html::img($v, [
                        'width' => 30,
                        'class' => 'img-circle',
                        'data'  => [
                            'toggle'    => 'tooltip',
                            'placement' => 'left',
                        ],
                        'title' => $title,
                    ]),
                    $url,
                    ['target' => '_blank']
                );
            }

            return '';
        },
    ],
    [
        'header'  => Yii::t('c.oifM0YkVx7', 'Пользователь'),
        'content' => function ($item) {
            if (isset($item['data'])) {
                if ($item['data'] != '') {
                    $config = $item['data'];
                    $user = \common\models\UserAvatar::findOne($config['user_id']);
                    if ($config['user_id'] != Yii::$app->user->id) {
                        return Html::a(Html::img($user->getAvatar(), [
                            'width' => 30,
                            'class' => 'img-circle',
                            'data'  => [
                                'toggle'    => 'tooltip',
                                'placement' => 'right',
                            ],
                            'title' => \yii\helpers\Html::encode($user->getName2()),
                        ]), '/user/' . $config['user_id'], []);
                    }
                }
            }

            return '';
        },
    ],
];

if (Yii::$app->deviceDetect->isMobile()) {
    $columns = [
        $columns[4],
        $columns[2],
        $columns[3],
    ];
}
?>
<style>
    .table .code {
        font-family: 'Courier New', Monospace;
    }

</style>

<?= \yii\grid\GridView::widget([
    'dataProvider' => new \yii\data\ArrayDataProvider([
        'allModels' => $transactions,
    ]),
    'tableOptions' => [
        'class' => 'table table-hover table-striped',
        'id'    => 'tableTransaction',
    ],
    'columns'      => $columns,
    'summary'      => '',

]);
?>

<ul class="pagination">
    <?php if ($page == 1) { ?>
        <li class="prev disabled"><span>«</span></li>
    <?php } else { ?>
        <li class="prev"><a href="<?= \yii\helpers\Url::to([
                'cabinet-bills/transactions-list-token',
                'id'   => $id,
                'page' => $page - 1,
            ]) ?>" data-page="0" class="paginationItem">«</a></li>
    <?php } ?>
    <?php for ($i = 1; $i <= $page; $i++) { ?>
        <li><a href="<?= \yii\helpers\Url::to([
                'cabinet-bills/transactions-list-token',
                'id'   => $id,
                'page' => $i,
            ]) ?>"
               data-page="1" class="paginationItem"><?= $i ?></a></li>
    <?php } ?>
    <?php if (count($transactions) == 20) { ?>
        <li class="next"><a
                    href="<?= \yii\helpers\Url::to([
                        'cabinet-bills/transactions-list-token',
                        'id'   => $id,
                        'page' => $page + 1,
                    ]) ?>"
                    data-page="<?= $page ?>" class="paginationItem">»</a></li>
    <?php } ?>
</ul>
