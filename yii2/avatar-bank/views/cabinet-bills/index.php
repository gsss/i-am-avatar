<?php
use common\models\avatar\Currency;
use common\models\avatar\UserBill;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */

$this->title = Yii::t('c.TsqnzVaJuC', 'Мои счета');

$currencyBTC = Currency::BTC;
$currencyETH = Currency::ETH;
$currencyETC = Currency::ETC;

$strCopy = Yii::t('c.TsqnzVaJuC', 'Скопировано');

\avatar\assets\Clipboard::register($this);

$this->registerJs(<<<JS
var clipboard = new Clipboard('.js-buttonTransactionInfo');
clipboard.on('success', function(e) {
    $(e.trigger).tooltip({
        title: '{$strCopy}',
        placement: 'bottom'
    });
    $(e.trigger).tooltip('show');
    setTimeout(function(ee) {
        $(e.trigger).tooltip('destroy');
    }, 1000);
});
JS
);

$this->registerJs(<<<JS

/**
*
* @param val
* @param separator
* @param lessOne int кол-во символов после точки, если 0 то точка не выводится, добавляются задние нули работает пока только для 0..2
* @returns {string}
*/
function formatAsDecimal (val, separator, lessOne)
{
    if (typeof separator == 'undefined') separator = ',';
    if (typeof lessOne == 'undefined') lessOne = 0;
    var pStr = '';
    var original = val;
    val = parseInt(val);
    if (val >= 1000) {
        var t = parseInt(val/1000);
        var ost = (val - t * 1000);
        var ostStr = ost;
        if  (ost == 0) {
            ostStr = '000';
        } else {
            if (ost < 10) {
                ostStr = '00' + ost;
            } else if (ost < 100) {
                ostStr = '0' + ost;
            }
        }
        pStr = t + separator + ostStr;
    } else {
        pStr = val;
    }
    var oStr = '';
    if (lessOne > 0) {
        var d = original - parseInt(original);
        var i;
        d = d * Math.pow(10, lessOne);
        d = parseInt(d);
        if (d == 0) {
            for (i = 0; i < lessOne; i++) {
                oStr = oStr + '0';
            }
        } else {
            var p;
            // определить склько знаков в числе после заятой
            if (lessOne == 1) {
                oStr = d;
            } else {
                oStr = repeat('0', lessOne - getNums(d));
                oStr += d;
            }
        }
    }

    return pStr + '.' + oStr;
}
function formatAsDecimal2 (val, separator, lessOne)
{
    if (typeof separator == 'undefined') separator = ',';
    if (typeof lessOne == 'undefined') lessOne = 0;
    
    var pStr = '';
    var original = val;
    val = parseInt(val);
    var j = 1;
    var blocks = [];
    var valChacngable = val;
    while (true) {
        if (1000 <= valChacngable) { // 1000 < 1001
            // формирую ведущие нули 
            pStr = vedNullInt(valChacngable % 1000);
            blocks.push(pStr);
        } else { // 1000 > 52
            blocks.push(valChacngable);
        }
        if (valChacngable < 1000) break;
        valChacngable = parseInt(valChacngable / 1000);
        j++;
    }
    blocks = blocks.reverse();
    pStr = blocks.join(',');
    
    
    var oStr = '';
    if (lessOne > 0) {
        var d = original - parseInt(original);
        var i;
        d = d * Math.pow(10, lessOne);
        d = parseInt(d);
        if (d == 0) {
            for (i = 0; i < lessOne; i++) {
                oStr = oStr + '0';
            }
        } else {
            var p;
            // определить склько знаков в числе после заятой
            if (lessOne == 1) {
                oStr = d;
            } else {
                oStr = repeat('0', lessOne - getNums(d));
                oStr += d;
            }
        }
    }

    return pStr + '.' + oStr;
}

/**
* Возвращает число с ведущими нулями
* Например в числе 9 = 009 чифра
*
* @param  d integer
*/
function vedNullInt(d)
{
    var len = getNums(d);
    if (len == 3) return d;
    var prefix = repeat('0', 3 - len);
    
    return prefix + d;
}

/**
* Возвращает кол-во цифр в числе
* Например в числе 150 = 3 чифры
* Например в числе 999 = 3 чифры
* Например в числе 9 = 1 чифра
*
* @param  d integer
*/
function getNums(d)
{
    if (d == 0) return 1;
    var i = 0;
    while(true) {
        p = Math.pow(10, i);
        if (d - p < 0) return i;
        i++;
    }
}

/**
* 
* @param  s string
* @param  n integer
* 
* @returns {string}
*/
function repeat(s, n)
{
    var a = [];
    while(a.length < n){
        a.push(s);
    }
    return a.join('');
}

$('.rowTable').click(function(e) {
    if (!$(e.target).hasClass('js-buttonTransactionInfo')) {
        if ($(this).data('currency') == {$currencyBTC}) {
            window.location = '/cabinet-bills/transactions/' + $(this).data('id');
        } else if ($(this).data('currency') == $currencyETH) {
            window.location = '/cabinet-bills/transactions-eth' + '?' + 'id' + '=' + $(this).data('id');
        } else if ($(this).data('currency') == $currencyETC) {
            window.location = '/cabinet-bills/transactions-etc' + '?' + 'id' + '=' + $(this).data('id');
        } else {
            window.location = '/cabinet-bills/transactions-token' + '?' + 'id' + '=' + $(this).data('id');
        }
    }
});

JS
);

/** @var \common\models\UserAvatar $user */
$user = Yii::$app->user->identity;
$defaultBillingIds = \common\models\avatar\UserBillDefault::find()->where(['user_id' => Yii::$app->user->id])->select('id')->column();
Yii::$app->session->set('defaultBillingIds', $defaultBillingIds);


if ($user->wallets_is_locked == 1) {
    // кошельки заблокированы
    $headerLock = Html::tag('span', Html::tag('i', null, ['class' => 'fa fa-lock']), [
        'class' => 'label label-danger',
        'title' => Yii::t('c.TsqnzVaJuC', 'Вам нужно разблокировать'),
        'data'  => [
            'toggle' => 'tooltip',
        ],
    ]);
} else {
    // кошельки разблокированы
    if ($user->password_save_type == UserBill::PASSWORD_TYPE_HIDE_CABINET) {
        $headerLock = Html::tag('span', Html::tag('i', null, ['class' => 'fa fa-lock']), [
            'class' => 'label label-default',
            'title' => Yii::t('c.TsqnzVaJuC', 'У вас кошельки надежно сохранены'),
            'data'  => [
                'toggle' => 'tooltip',
            ],
        ]);
    } else {
        $headerLock = Html::tag('span', Html::tag('i', null, ['class' => 'fa fa-lock']), [
            'class' => 'label label-default',
            'title' => Yii::t('c.TsqnzVaJuC', 'У вас слабая степень защиты'),
            'data'  => [
                'toggle' => 'tooltip',
            ],
            'style' => 'opacity: 0.3;'
        ]);
    }
}


// залоговые счета
/** @var array $zalogBillingList идентификаторы счета user_bill которые для пользователя сейчас находятся в состоянии залога */
$zalogBillingList = \common\models\UserZalog::find()
    ->where([
        'owner_user_id' => Yii::$app->user->id,
        'status'        => 0,
        ])
    ->andWhere(['>', 'finish', time()])
    ->all();
$map = \yii\helpers\ArrayHelper::map($zalogBillingList, 'owner_billing_id', function ($item) { return $item; });
Yii::$app->session->set('page::/cabinet-bills/index:$zalogBillingList', $map);

?>

<style>
    .textDecorated {
        text-decoration-line: underline;
        text-decoration-style: dotted;
    }
</style>


<?= $this->render('_modalActivateCard') ?>
<?= $this->render('_modalSend') ?>
<?= $this->render('_modalSendEth') ?>
<?= $this->render('_modalSendEthToken') ?>
<?= $this->render('_modalNew') ?>
<?= $this->render('_modalNewToken') ?>
<?= $this->render('_modalQr') ?>

<div class="container" style="padding-bottom: 70px;">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header text-center"><?= $this->title ?></h1>

            <p class="alert alert-info"><i class="glyphicon glyphicon-alert"></i> Внимание! С 20.09.2018 г. мы вынуждены уменьшить количество предоставляемых услуг. Ознакомьтесь с <a href="/news/item?id=23">новостью</a> пожалуйста.</p>

            <?php
            $columns[] = [
                'header'  => '',
                'content' => function ($item) {
                    return Html::img('/images/controller/cabinet-bills/index/wallet.png', ['width' => 30, 'style' => 'margin-top: 13px;']);
                },
            ];
            $columns[] = [
                'header'        => $headerLock,
                'headerOptions' => [
                    'style' => 'text-align: center;',
                ],
                'content'       => function ($item) {
                    $currency = \yii\helpers\ArrayHelper::getValue($item, 'currency', Currency::BTC);
                    $title = '';
                    switch ($currency) {
                        case Currency::BTC:
                            $src = '/images/controller/cabinet-bills/transactions/btc.jpg';
                            $title = 'BitCoin (BTC)';
                            break;
                        case Currency::ETH:
                            $src = '/images/controller/cabinet-bills/transactions/eth.png';
                            $title = 'Ethereum (ETH)';
                            break;
                        case Currency::ETC:
                            $src = '/images/controller/cabinet-bills/transactions/etc.png';
                            $title = 'Ethereum Classic (ETC)';
                            break;
                        default:
                            $currencyObject = Currency::findOne($currency);
                            if (is_null($currencyObject)) {
                                throw new \yii\base\Exception('Не указана валюта у счета');
                            }
                            $src = $currencyObject->image;
                            $title = $currencyObject->title . ' ' . '(' . $currencyObject->code . ')';
                            break;
                    }
                    $imgOptions = [
                        'width' => 50,
                        'class' => 'img-circle',
                        'data'  => [
                            'toggle' => 'tooltip',
                            'title'  => $title,
                        ],
                    ];

                    $passwordType = \yii\helpers\ArrayHelper::getValue($item, 'password_type', UserBill::PASSWORD_TYPE_OPEN);
                    if ($passwordType == UserBill::PASSWORD_TYPE_HIDE_CABINET) {
                        /** @var \common\models\UserAvatar $user */
                        $user = Yii::$app->user->identity;
                        if ($user->wallets_is_locked) {
                            $imgOptions['style']['opacity'] = '0.3';
                        }
                    }

                    if (isset($imgOptions['style'])) {
                        if (is_array($imgOptions['style'])) {
                            $imgOptions['style'] = Html::cssStyleFromArray($imgOptions['style']);
                        }
                    }

                    return Html::img($src, $imgOptions);
                },
            ];

            $columns[] = [
                'header'  => Yii::t('c.TsqnzVaJuC', 'Изменения за 24ч'),
                'headerOptions'  => [
                    'style' => 'text-align: right;'
                ],
                'contentOptions'  => [
                    'style' => 'text-align: right;'
                ],
                'content' => function ($item) {
                    $v = $item['currency'];
                    $currency = Currency::findOne($v);
                    if (is_null($currency)) return '';

                    if (is_null($currency->chanche_1d))  return '';
                    if ($currency->chanche_1d < 0) {
                        $style[] = 'color: #F45F5F';
                    } else {
                        $style[] = 'color: #6ED098';
                    }

                    return Html::tag('span', (($currency->chanche_1d > 0)? '+':''). Yii::$app->formatter->asDecimal($currency->chanche_1d, 2), ['style' => join(';', $style)]);
                },
            ];
            $columns[] = [
                'header'  => Yii::t('c.TsqnzVaJuC', 'Цена'),
                'headerOptions'  => [
                    'style' => 'text-align: right;'
                ],
                'contentOptions'  => [
                    'style' => 'text-align: right;'
                ],
                'content' => function ($item) {
                    $v = $item['currency'];
                    $currency = Currency::findOne($v);
                    if (is_null($currency)) return '';
                    if (is_null($currency->price_rub)) return '';

                    $html[] = Html::tag('code', Yii::$app->formatter->asDecimal($currency->price_usd, 2)). Html::tag('span', 'USD', ['class' => 'label label-info']);
                    $html[] = Html::tag('br');
                    $html[] = Html::tag('code', Yii::$app->formatter->asDecimal($currency->price_rub, 2)). Html::tag('span', 'RUB', ['class' => 'label label-info']);

                    return join('',$html);
                },
            ];

            if (!YII_ENV_PROD) {
                $columns[] = [
                    'header'  => '',
                    'content' => function ($item) {
                        $passwordType = \yii\helpers\ArrayHelper::getValue($item, 'password_type', UserBill::PASSWORD_TYPE_OPEN);
                        if ($passwordType == UserBill::PASSWORD_TYPE_HIDE_CABINET) {
                            /** @var \common\models\UserAvatar $user */
                            $user = Yii::$app->user->identity;
                            if ($user->wallets_is_locked) {
                                $color = '#d9534f';
                                return Html::tag('i', null, [
                                    'class' => 'fa fa-key',
                                    'style' => 'color: ' . $color . ';',
                                    'title' => 'Пароль от кабинета сменен, доступ к кошелькам нужно восстановить',
                                    'data'  => ['toggle' => 'tooltip'],
                                ]);
                            }
                        }
                    },
                ];
            }

            $columns[] = Yii::$app->deviceDetect->isMobile() ? [
                'header'  => Yii::t('c.TsqnzVaJuC', 'Адрес'),
                'content' => function ($item) {
                    return
                        Html::tag(
                            'span',
                            substr($item['address'], 0, 8) . '...',
                            [
                                'class' => 'js-buttonTransactionInfo textDecorated',
                                'style' => 'font-family: "Courier New", Courier, monospace;',
                                'role'  => 'button',
                                'title' => Yii::t('c.TsqnzVaJuC', 'Подробнее'),
                                'data' => [
                                    'placement'      => 'bottom',
                                    'toggle'         => 'tooltip',
                                    'clipboard-text' => $item['address'],
                                ],
                            ]
                        );
                },
            ] : [
                'header'  => Yii::t('c.TsqnzVaJuC', 'Адрес'),
                'content' => function ($item) {
                    return
                        Html::tag(
                            'span',
                            substr($item['address'], 0, 8) . '...',
                            [
                                'class' => 'js-buttonTransactionInfo textDecorated',
                                'style' => 'font-family: "Courier New", Courier, monospace;',
                                'role'  => 'button',
//                                'title' => Yii::t('c.TsqnzVaJuC', 'Подробнее'),
                                'data' => [
                                    'placement'      => 'bottom',
//                                    'toggle'         => 'tooltip',
                                    'clipboard-text' => $item['address'],
                                ],
                            ]
                        );
                },
            ];

            $columns = \yii\helpers\ArrayHelper::merge($columns, [
                [
                    'header' => Yii::t('c.TsqnzVaJuC', 'Наименование'),
                    'attribute' => 'name',
                ],
                [
                    'header'         => Yii::t('c.TsqnzVaJuC', 'Баланс'),
                    'contentOptions' => function ($item) {
                        return [
                            'class' => 'rowBill',
                            'id'    => 'bill_confirmed_' . $item['id'],
                            'data'  => ['id' => $item['id']],
                            'style' => 'text-align: right;',
                        ];
                    },
                    'content'        => function ($item) {
                        return Html::img(
                            Yii::$app->assetManager->getBundle('avatar\assets\App\Asset')->baseUrl . '/images/ajax-loader.gif'
                        );
                    },
                ],
                [
                    'header'  => 'QR',
                    'content' => function ($item) {
                        return Html::button(Html::tag('i', '', ['class' => 'fa fa-qrcode']), [
                            'class' => 'btn btn-default buttonQrCode',
                            'data'  => [
                                'id'      => $item['id'],
                                'address' => $item['address'],
                            ],
                        ]);
                    },
                ],
                [
                    'header'  => Yii::t('c.TsqnzVaJuC', 'Отправить'),
                    'content' => function ($item) {
                        $options = [
                            'class' => ['btn', 'btn-info'],
                            'data'  => [
                                'id'       => $item['id'],
                                'currency' => $item['currency'],
                            ],
                        ];

                        /** @var \common\models\UserAvatar $user */
                        $user = Yii::$app->user->identity;
                        if (!$user->wallets_is_locked) {
                            if (($item['currency'] == Currency::ETH)) {

                                $options['class'][] = 'buttonSendEth1';
                                $options['data']['placement'] = 'bottom';
                                $options['data']['toggle'] = 'tooltip';
                                $options['title'] = 'Поддержка временно прекращена, смотрите новости';
                                $options['style'] = 'opacity: 0.3;';

                            } else if (in_array($item['currency'], [162, 18])) { // MWC

                                $options['class'][] = 'buttonSendEthToken1';
                                $options['data']['placement'] = 'bottom';
                                $options['data']['toggle'] = 'tooltip';
                                $options['title'] = 'Поддержка токена прекращена';
                                $options['style'] = 'opacity: 0.3;';

                            } else if ($item['currency'] == Currency::BTC) {
                                $options['class'][] = 'buttonSend';
                            } else {
                                $token = \common\models\Token::findOne(['currency_id' => $item['currency']]);
                                if (!is_null($token)) {
                                    $options['class'][] = 'buttonSendEthToken1';
                                    $options['data']['placement'] = 'bottom';
                                    $options['data']['toggle'] = 'tooltip';
                                    $options['title'] = 'Поддержка временно прекращена, смотрите новости';
                                    $options['style'] = 'opacity: 0.3;';

                                } else {
                                    $options['class'][] = 'buttonSendUnknown';
                                }
                            }

                            if (is_array($options['class'])) {
                                $options['class'] = join(' ', $options['class']);
                            }

                            return Html::button(Yii::t('c.TsqnzVaJuC', 'Отправить'), $options);
                        }
                    },
                ],
            ]);

            $columns[] = [
                'header'  => Yii::t('c.TsqnzVaJuC', 'Карта'),
                'content' => function ($item) {
                    if (!isset($item['card_id'])) {
                        return '';
                    }

                    return
                        Html::a(
                            Yii::t('c.TsqnzVaJuC', 'Карта'),
                            ['cabinet-cards/card', 'id' => $item['card_id']],
                            [
                                'class' => 'btn btn-xs btn-success',
                                'data'  => [
                                    'id' => $item['id'],
                                ],
                            ]
                        );
                },
            ];
            $columns[] = [
                'header'  => Yii::t('c.TsqnzVaJuC', 'Закрыть'),
                'content' => function ($item) {
                    return
                        Html::button(
                            Yii::t('c.TsqnzVaJuC', 'Закрыть'),
                            [
                                'class' => 'btn btn-danger buttonClose btn-xs',
                                'role'  => 'button',
                                'title' => Yii::t('c.TsqnzVaJuC', 'Подробнее'),
                                'data'  => [
                                    'id' => $item['id'],
                                ],
                            ]
                        );
                },
            ];
            $columns[] = [
                'header'  => Html::tag('span', Html::tag('i', null, ['class' => 'glyphicon glyphicon-ok']), [
                    'class' => 'label label-default',
                    'title' => Yii::t('c.TsqnzVaJuC', 'По умолчанию'),
                    'data'  => [
                        'toggle' => 'tooltip',
                    ],
                ]),
                'content' => function (\common\models\avatar\UserBill $item) {
                    $ids = Yii::$app->session->get('defaultBillingIds');
                    if (in_array($item['id'], $ids)) {
                        $url = ['cabinet/default-btc'];
                        if ($item->currency == Currency::BTC) {
                            $url = ['cabinet/default-btc'];
                        }
                        if ($item->currency == Currency::ETH) {
                            $url = ['cabinet/default-eth'];
                        }
                        return Html::a(
                            Html::tag('i', null, ['class' => 'glyphicon glyphicon-ok']),
                            $url,
                            [
                                'class' => 'btn btn-default btn-xs',
                                'title' => Yii::t('c.TsqnzVaJuC', 'По умолчанию'),
                                'data'  => [
                                    'toggle' => 'tooltip',
                                ],
                            ]
                        );
                    }
                },
            ];
            $rows = [];
            $rowsBtc = UserBill::find()->where([
                'user_id' => Yii::$app->user->id,
                'mark_deleted' => 0,
                'currency' => Currency::BTC,
            ])->all();
            $rows = $rowsBtc;
            $rowsEth = UserBill::find()->where([
                'user_id' => Yii::$app->user->id,
                'mark_deleted' => 0,
                'currency' => Currency::ETH,
            ])->all();
            /** @var \common\models\avatar\UserBill $rowEth */
            foreach ($rowsEth as $rowEth) {
                $rows[] = $rowEth;
                $rowsTokens = UserBill::find()
                    ->where([
                        'user_id'      => Yii::$app->user->id,
                        'mark_deleted' => 0,
                    ])
                    ->andWhere(['not in', 'currency', [
                        Currency::BTC,
                        Currency::ETH,
                        Currency::ETC,
                    ]])
                    ->andWhere(['address' => $rowEth->address])
                    ->all();
                $rows = \yii\helpers\ArrayHelper::merge($rows, $rowsTokens);
            }

            ?>
            <?= \yii\grid\GridView::widget([
                'dataProvider' => new \yii\data\ArrayDataProvider([
                    'allModels'  => $rows,
                    'pagination' => [
                        'pageSize' => 100,
                    ],
                ]),
                'tableOptions' => [
                    'class' => 'table table-hover table-striped',
                    'style' => 'width: auto;',
                    'id'    => 'tableTransaction',
                ],
                'summary' => '',
                'rowOptions'   => function (UserBill $item) {
                    $data = [
                        'data'  => [
                            'id'             => $item['id'],
                            'currency'       => $item['currency'],
                            'currencyObject' => \yii\helpers\ArrayHelper::toArray(Currency::findOne($item['currency'])),
                        ],
                        'role'  => 'button',
                        'class' => 'rowTable',
                    ];

                    if (!in_array($item['currency'], [Currency::ETC, Currency::ETH, Currency::BTC])) {
                        $data['data']['address'] = $item['address'];
                        $data['data']['contract'] = $item->getCurrencyObject()->getToken()->address;
                    } else {
                        if (in_array($item['currency'], [Currency::ETC, Currency::ETH])) {
                            $data['data']['convert'] = Currency::convertBySettings(1, 'ETH');
                            $data['data']['address'] = $item['address'];
                            $data['style'][] = 'border-top: 3px solid #888';
                        }
                    }
                    if (isset($data['style'])) {
                        if (is_array($data['style'])) {
                            $data['style'] = join(';', $data['style']);
                        }
                    }

                    return $data;
                },
                'columns'      => $columns,
            ]) ?>



            <?php
            $arrayBillsIds = json_encode(
                UserBill::find()
                    ->where([
                        'user_id'      => Yii::$app->user->id,
                        'mark_deleted' => 0,
                    ])
                    ->select('id')
                    ->column()
            );
            \common\assets\HighCharts\HighChartsAsset::register($this);

            $str1 = Yii::t('c.TsqnzVaJuC', 'Вы уверены?');
            $str2 = Yii::t('c.TsqnzVaJuC', 'Успешно завершено!');
            $isProd = YII_ENV_PROD ? 'prod' : 'test';
            $this->registerJs(<<<JS
$('.buttonClose').click(function(e) {
    e.preventDefault();
    e.stopPropagation();
    var b = $(this);
    if (confirm('{$str1}')) {
        ajaxJson({
            url: '/cabinet-bills/close',
            data: {
                id: b.data('id')
            },
            success: function(ret) {
                b.parent().parent().remove();
                $('#modalQrCode .modal-body').html('{$str2}');
                $('#modalQrCode').modal();
            }
        });
    }
});






var i = 0;
var operationList = [];
$('#tableTransaction').find('tr').each(function(i,v) {
    if (typeof($(v).data('id')) != 'undefined') {
        operationList.push([
            $(v).data('id'),
            $(v).data('currency'),
            $(v).data('address'),
            $(v).data('contract'),
            $(v).data('convert')
        ]);
    } 
}); 

var listEth = [];
var pie1 = [];


function findPieItem(name, elements)
{
    for(i=0;i<elements.length;i++) {
        if (elements[i].name == name) return {success: true,data: elements[i]};
    }   
    return {success: false};
}
var functionFinish = function() {
    console.log(pie1);
    var i;
    var elements = [];
    var sum = 0;
    for(i=0;i<pie1.length;i++) {
        var res = findPieItem(pie1[i].currency.code, elements);
        if (res.success) {
            res.data.y += pie1[i].value
        } else {
            elements.push({
                'name': pie1[i].currency.code,
                'y': pie1[i].value
            });
        }
        
        sum += pie1[i].value;
    }
    $('#result2').html(formatAsDecimal2(sum, ',', 2) + ' RUB');
    window.chart = $('#w1').highcharts({
    "chart":{
        "plotBackgroundColor":null,
        "plotBorderWidth":null,
        "plotShadow":false,
        "type":"pie"
        },
        "title":{
            "text":"Распределение фонда"
        },
        "tooltip":{
            "pointFormat":"{series.name}: <b>{point.percentage:.1f}%</b>"
        },
        "plotOptions":{
            "pie":{
                "allowPointSelect":true,
                "cursor":"pointer",
                "dataLabels":{
                    "enabled":true,
                    "format":"<b>{point.name}</b>: {point.percentage:.1f} %",
                    "style":{
                        "color":(Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                    }
                }
            }
        },
        "series":[
            {
                "name":"Токенов",
                "colorByPoint":true,
                "data": elements
            }
        ]
    });

};

var functionCalc = function(i, arr) {
    var item = arr[i];
    var id = item[0];
    var currency = item[1];

    if (currency == 14) { // ETC
        ajaxJson({
            url: '/cabinet-bills/balance-internal-etc' + '?' + 'id' + '=' + id,
            success: function(ret) {
                i++;
                if (ret.currencyView > 0) {
                    $('#bill_confirmed_'+ id).html(
                        formatAsDecimal2(ret.confirmedConverted, ',', 2)
                    );
                    
                } else {
                    $('#bill_confirmed_'+ id).html(
                        formatAsDecimal2(ret.confirmed, ',', 2)
                    );
                }
                if (i < arr.length) {
                    functionCalc(i, arr);
                } else { functionFinish(); }
            },
            error: function(ret) {
                i++;
                if (i < arr.length) {
                    functionCalc(i, arr);
                } else { functionFinish(); }
            },
            errorScript: function(ret) {
                i++;
                if (i < arr.length) {
                    functionCalc(i, arr);
                } else { functionFinish(); }
            }
        });
    } else {
        if (currency != 3 && currency != 4) {
            var result = findToken(item[2], item[3]);
    
            if (result.success) {
                i++;
                var o = $('.rowTable[data-id=' + id + ']').data('currencyobject');
                var stringCode = $('<code>').html(formatAsDecimal2(result.data.balance2, ',', 8))[0].outerHTML + 
                    $('<span>', {class: 'label label-info'}).html(o.code)[0].outerHTML;
                if (o.price_rub) {
                    stringCode += '<br>' + 
                    $('<code>').html(formatAsDecimal2(result.data.balance2 * o.price_rub, ',', 2))[0].outerHTML +
                    $('<span>', {class: 'label label-info'}).html('RUB')[0].outerHTML;
                }
                if (result.data.balance2 == 0) {
                    $('#bill_confirmed_'+ id).html($('<span>', {style: 'opacity:0.5'}).html(stringCode));
                } else {
                    $('#bill_confirmed_'+ id).html(stringCode);
                    if (o.price_rub) {
                        pie1.push({
                            currency: o,
                            value: result.data.balance2 * o.price_rub
                        });
                    }
                }
    
                if (i < arr.length) {
                    functionCalc(i, arr);
                } else { functionFinish(); }
            } else {
                if (result.data == 1) { // не найден адрес
                    ajaxJson({
                        url: '/cabinet-bills/get-balance',
                        data: { id: id },
                        success: function(ret) {
                            listEth.push(ret);
                            i++;
                            var result2 = findToken(item[2], item[3]);
                            if (result2.success) {
                                var o = $('.rowTable[data-id=' + id + ']').data('currencyobject');
                                var stringCode = $('<code>').html(formatAsDecimal2(result2.data.balance2, ',', 8))[0].outerHTML + 
                                    $('<span>', {class: 'label label-info'}).html(o.code)[0].outerHTML;
                                if (o.price_rub) {
                                    stringCode += '<br>' + 
                                    $('<code>').html(formatAsDecimal2(result2.data.balance2 * o.price_rub, ',', 2))[0].outerHTML +
                                    $('<span>', {class: 'label label-info'}).html('RUB')[0].outerHTML;
                                }
                                if (result2.data.balance2 == 0) {
                                    $('#bill_confirmed_'+ id).html($('<span>', {style: 'opacity:0.5'}).html(stringCode));
                                } else {
                                    $('#bill_confirmed_'+ id).html(stringCode);
                                    if (o.price_rub) {
                                        pie1.push({
                                            currency: o,
                                            value: result2.data.balance2 * o.price_rub
                                        });
                                    }
                                }
            
                                if (i < arr.length) {
                                    functionCalc(i, arr);
                                } else { functionFinish(); }
                            } else {
                                $('#bill_confirmed_'+ id).html(
                                    ''
                                );
            
                                if (i < arr.length) {
                                    functionCalc(i, arr);
                                } else { functionFinish(); }
                                
                            }
                        },
                        error: function(ret) {
                            i++;
                            $('#bill_confirmed_'+ id).html(
                                '--'
                            );
        
                            if (i < arr.length) {
                                functionCalc(i, arr);
                            } else { functionFinish(); }
                        },
                        errorScript: function(ret) {
                            i++;
                            if (i < arr.length) {
                                functionCalc(i, arr);
                            } else { functionFinish(); }
                        }
                    });
                } else {
                    // не найден токен в буфере
                    i++;
                    var o = $('.rowTable[data-id=' + id + ']').data('currencyobject');
                    var stringCode = $('<code>').html(formatAsDecimal2(0, ',', 8))[0].outerHTML + 
                        $('<span>', {class: 'label label-info'}).html(o.code)[0].outerHTML;
                    if (o.price_rub) {
                        stringCode += '<br>' + 
                        $('<code>').html(formatAsDecimal2(0, ',', 2))[0].outerHTML +
                        $('<span>', {class: 'label label-info'}).html('RUB')[0].outerHTML;
                    }
                    $('#bill_confirmed_'+ id).html($('<span>', {style: 'opacity:0.5'}).html(stringCode));
    
                    if (i < arr.length) {
                        functionCalc(i, arr);
                    } else { functionFinish(); }
                }
            }
        } else if (currency == 4) {
            // ETH
            var result = findAddress(item[2]);
            // если найден адрес?
            if (result.success) {
                var ret = result.data;
                i++;
                if (item[4] == 1) {
                    $('#bill_confirmed_'+ id).html(
                        formatAsDecimal(ret.ETH.balance, ',', 18)
                    );
                } else {
                    $('#bill_confirmed_'+ id).html(
                        formatAsDecimal(ret.ETH.balance * item[4], ',', 2)
                    );
                }
    
                if (i < arr.length) {
                    functionCalc(i, arr);
                } else { functionFinish(); }
            } else {
                // получаю баланс на внешнем источнике
                ajaxJson({
                    url: '/cabinet-bills/get-balance',
                    data: { id: id },
                    success: function(ret) {
                        listEth.push(ret);
                        i++;
                        var o = $('.rowTable[data-id=' + id + ']').data('currencyobject');
                        var stringCode = $('<code>').html(ret.avatarNetwork.coinsFormated)[0].outerHTML + 
                            $('<span>', {class: 'label label-info'}).html(o.code)[0].outerHTML;
                        if (o.price_rub) {
                            stringCode += '<br>' + 
                            $('<code>').html(ret.avatarNetwork.convertFormated)[0].outerHTML +
                            $('<span>', {class: 'label label-info'}).html('RUB')[0].outerHTML;
                        }
                        
                        if (ret.avatarNetwork.coins == 0) {
                            $('#bill_confirmed_'+ id).html($('<span>', {style: 'opacity:0.5'}).html(stringCode));
                        } else {
                            $('#bill_confirmed_'+ id).html(stringCode);
                            if (o.price_rub) {
                                pie1.push({
                                    currency: o,
                                    value: ret.avatarNetwork.coins * o.price_rub
                                });
                            }
                        }
                        
                        if (i < arr.length) {
                            functionCalc(i, arr);
                        } else { functionFinish(); }
                    },
                    error: function(ret) {
                        i++;
                        $('#bill_confirmed_'+ id).html(
                            ''
                        );
                        
                        if (i < arr.length) {
                            functionCalc(i, arr);
                        } else { functionFinish(); }
                    },
                    errorScript: function(ret) {
                        i++;
                        if (i < arr.length) {
                            functionCalc(i, arr);
                        } else { functionFinish(); }
                    }
                });
            }
            
        } else {
            // BTC
            ajaxJson({
                url: '/cabinet-bills/get-balance',
                data: { id: id },
                success: function(ret) {
                    i++;
                    var o = $('.rowTable[data-id=' + id + ']').data('currencyobject');
                    var stringCode = $('<code>').html(ret.coinsFormated)[0].outerHTML + 
                        $('<span>', {class: 'label label-info'}).html(o.code)[0].outerHTML;
                    if (o.price_rub) {
                        stringCode += '<br>' + 
                        $('<code>').html(ret.convertFormated)[0].outerHTML +
                        $('<span>', {class: 'label label-info'}).html('RUB')[0].outerHTML;
                    }
                    
                    if (ret.coins == 0) {
                        $('#bill_confirmed_'+ id).html($('<span>', {style: 'opacity:0.5'}).html(stringCode));
                    } else {
                        $('#bill_confirmed_'+ id).html(stringCode);
                        if (o.price_rub) {
                            pie1.push({
                                currency: o,
                                value: ret.coins * o.price_rub
                            });
                        }
                    }
        
                    if (i < arr.length) {
                        functionCalc(i, arr);
                    } else { functionFinish(); }
                },
                error: function(ret) {
                    i++;
                    $('#bill_confirmed_'+ id).html(
                        ''
                    );
                    
                    if (i < arr.length) {
                        functionCalc(i, arr);
                    } else { functionFinish(); }
                },
                errorScript: function(ret) {
                    i++;
                    if (i < arr.length) {
                        functionCalc(i, arr);
                    } else { functionFinish(); }
                }
            });
        }
    }

};
functionCalc(i, operationList);



function findAddress2(addess) 
{
    var result = findAddress(address);
    if (result.success) {
        return result;
    }
    
    ajaxJson({
        url: 'https://Ethplorer.io/getAddressInfo/' + addess + '?apiKey=freekey',
        success: function(ret) {
            listEth.push(ret);
            
        }
    })
}

/**
* Ищет адрес
* 
* @param address
* @returns 
* {
*   success
* }
*/
function findAddress(address)
{
    for (var i = 0; i < listEth.length; i++) {
        var item = listEth[i];
        if (address == item.address) {
            return {
                success: true,
                data: item
            };
        }
    }
    
    return {
        success: false
    };
}

/**
* Возвращает баланс токена
* 
* @param address1 string - адрес эфировского кошелька
* @param token1 string - адрес токена 
* 
* @returns 
* 
* errors
* 1 - не найден адрес
* 2 - не найден токен
*/
function findToken(address1, token1)
{
    token1 = token1.toLowerCase();
    var result = findAddress(address1);
    if (result.success == false) {
        return {
            success: false,
            data: 1
        };
    }
    var addressInfo = result.data;
    
    if (typeof(addressInfo.tokens) != 'undefined') {
        for (var i = 0; i < addressInfo.tokens.length; i++) {
            var item = addressInfo.tokens[i];
            if (token1 == item.tokenInfo.address) {
                var nulls = 1;
                for(var j = 0; j < item.tokenInfo.decimals; j++) {
                    nulls = nulls * 10;
                }
                item.nulls = nulls;
                item.balance2 = item.balance / nulls;
                
                return {
                    success: true,
                    data: item
                };
            }
        }
    }

    return {
        success: false,
        data: 2
    };
}


JS
            );
            ?>
            <div class="input-group" style="margin-bottom: 100px;">
                <span class="input-group-btn">
                    <?php if (false) { ?>
                        <?php
                        /** @var \common\models\UserAvatar $user */
                        $user = Yii::$app->user->identity;
                        if ($user->wallets_is_locked == 0) {
                            ?>
                            <button type="button" class="btn btn-success btn-lg buttonNew">
                                <i class="glyphicon glyphicon-plus"
                                   style="margin-right: 5px;"></i> <?= \Yii::t('c.TsqnzVaJuC', 'Создать') ?>
                            </button>
                        <?php } else { ?>
                            <a href="/cabinet/password-type21" class="btn btn-danger btn-lg">
                                <?= \Yii::t('c.TsqnzVaJuC', 'Восстановить доступ к кошелькам') ?>
                            </a>
                        <?php } ?>
                    <?php } ?>


                    <button type="button" class="btn btn-success btn-lg buttonNewToken">
                        <i class="glyphicon glyphicon-plus" style="margin-right: 5px;"></i> <?= \Yii::t('c.TsqnzVaJuC', 'Добавить токен') ?>
                    </button>

                    <?php if (YII_DEBUG) { ?>
                        <div class="btn-group">
                          <button type="button" class="btn btn-success btn-lg dropdown-toggle" data-toggle="dropdown"
                                  aria-haspopup="true" aria-expanded="false">
                            Добавить <span class="caret"></span>
                          </button>
                          <ul class="dropdown-menu">
                            <li><a href="#" class="buttonNew">Создать кошелек</a></li>
                            <li><a href="#" class="buttonNewToken">Создать кошелек токена</a></li>
                            <li role="separator" class="divider"></li>
                            <li><a href="/cabinet-bills-eth/import-json">Импортировать кошелек ETH JSON</a></li>
                            <li><a href="/cabinet-bills-eth/import-private-key">Импортировать кошелек ETH private-key</a></li>
                          </ul>
                        </div>
                    <?php } ?>

                    <button type="button" class="btn btn-success btn-lg buttonActivate">
                        <i class="glyphicon glyphicon-credit-card"
                           style="margin-right: 5px;"></i><?= \Yii::t('c.TsqnzVaJuC', 'Активировать карту') ?>
                    </button>
                </span>
            </div>


            <div class="row" style="margin: 100px 0px 70px 0px;">
                <div id="w1"></div>
                <p class="text-center"><code id="result2"></code></p>
            </div>
        </div>
    </div>
</div>