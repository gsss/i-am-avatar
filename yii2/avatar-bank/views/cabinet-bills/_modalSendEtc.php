<?php
/**
 * Created by PhpStorm.
 * User: s.arhangelskiy
 * Date: 23.01.2017
 * Time: 10:21
 */

use common\models\avatar\Currency;

/* @var $this yii\web\View */

$currencyBTC = Currency::BTC;
$currencyETH = Currency::ETH;

$this->registerJs(<<<JS
$('.buttonSendEtc').click(function(e) {
    e.preventDefault();
    e.stopPropagation();
    var o = $(this);
    var currency = o.data('currency');
    
    $('#recipient-etc-billing_id').val(o.data('id'));
    $('#formOutEtc .itemCurrency[data-id=' + currency + ']').click();

    $('#recipient-etc-address').val('');
    $('#recipient-etc-amount').val('');
    $('#recipient-etc-password').val('');
    $('#recipient-etc-comment').val('');

    $('#modalSendEtc').modal();
});
$('#formOutEtc .form-control').on('focus', function() {
    var o = $(this);
    var p = o.parent();
    if (p.hasClass('input-group')) {
        p = p.parent();
    }
    p.removeClass('has-error');
    p.find('p.help-block-error').hide();
});

JS
);
?>


<div class="modal fade" tabindex="-1" role="dialog" id="modalSendEtc">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">&times;</span></button>
                <h4 class="modal-title"><?= \Yii::t('c.TsqnzVaJuC', 'Перевод денег') ?></h4>
            </div>
            <div class="modal-body">
                <form id="formOutEtc">
                    <input type="hidden" id="recipient-etc-billing_id" name="billing_id">

                    <div class="form-group recipient-etc-field-address">
                        <label for="recipient-etc-name" class="control-label"><?= \Yii::t('c.TsqnzVaJuC', 'Кому') ?>:</label>
                        <input type="text" class="form-control" id="recipient-etc-address" name="address"
                               style="font-family: Consolas, Courier New, monospace"
                        >
                        <p class="help-block"><?= \Yii::t('c.TsqnzVaJuC', 'Можно указать адрес счета, почту или телефон клиента') ?></p>

                        <p class="help-block help-block-error" style="display: none;"></p>
                    </div>
                    <div class="form-group recipient-etc-field-amount">
                        <label for="recipient-etc-name" class="control-label"><?= \Yii::t('c.TsqnzVaJuC', 'Сколько') ?>:</label>

                        <div class="input-group">
                            <input type="text"
                                   name="amount"
                                   class="form-control"
                                   aria-label="..."
                                   placeholder="0.00000001"
                                   autocomplete="off"
                                   id="recipient-etc-amount"
                                   style="font-family: Consolas, Courier New, monospace">
                            <input type="hidden"
                                   name="currency"
                                   id="recipient-etc-currency"
                                   value="BTC"
                                   >
                            <div class="input-group-btn">
                                <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true"
                                        aria-expanded="false"><span class="buttonCurrency">BTC</span> <span class="caret"></span></button>
                                <ul class="dropdown-menu dropdown-menu-right">
                                    <?php
                                    $str1= Yii::t('c.TsqnzVaJuC', 'Клиент');
                                    $str2= Yii::t('c.TsqnzVaJuC', 'Счет');
                                    $str3= Yii::t('c.TsqnzVaJuC', 'Перевести');
                                    $this->registerJs(<<<JS
$('.itemCurrency').click(function(e) {
    $('#recipient-etc-currency').val($(this).data('code'));
    $('.buttonCurrency').html($(this).html());
    var i,s='',c=$(this).data('decimals');
    for(i = 0; i < c; i++) {
        s = s + '0';
    }
    $('#recipient-etc-amount').attr('placeholder', '0.'+ s)
});


var functionButtonSendEtc = function() {
    var b = $(this);
    b.off('click');
    b.html($('<i>', {class: 'fa fa-spinner fa-spin fa-fw'}));
    b.attr('disabled', 'disabled');
    ajaxJson({
        url: '/cabinet-bills-etc/send',
        data: $('#formOutEtc').serializeArray(),
        success: function(ret) {
            $('#modalSendEtc').data('ret', ret);
            $('#modalSendEtc').modal('hide').on('hidden.bs.modal', function(e) {
                if (ret.hasUser) {
                    $('#modalSendConfirmEtc .modal-body').html(
                        $('<p>').append(
                            $('<img>', {src:ret.user.avatar,width:'300',class: 'img-circle'})
                        )
                    )
                    .append(
                        $('<p>').html(ret.user.name2)
                    )
                    .append(
                        $('<p>').html($('<b>').html('{$str2}'))
                    )
                    .append(
                        $('<p>').append(
                            $('<code>').html(ret.billing.address)
                        )
                    )
                    .append(
                        $('<p>').html(ret.billing.name)
                    )
                    ;
                } else {
                    $('#modalSendConfirmEtc .modal-body').html(
                        $('<p>').html($('<b>').html('{$str2}'))
                    )
                    .append(
                        $('<p>').append(
                            $('<code>').html(ret.address)
                        )
                    )
                }
                $('#modalSendConfirmEtc').modal();
                $('#modalSendEtc').off('hidden.bs.modal');
                b.on('click', functionButtonSendEtc);
                b.html('{$str3}');
                b.removeAttr('disabled');
            });
        },
        errorScript: function(ret) {
            switch (ret.id) {
                case 101:
                    var f = $('#modalSendEtc');
                    $.each(ret.data,function(i,v) {
                        var name = v.name;
                        var value = v.value;
                        var o = f.find('#recipient-etc-'+ name);
                        var t = f.find('.recipient-etc-field-' + name);
                        t.addClass('has-error');
                        t.find('p.help-block-error').html(value.join('<br>')).show();
                    });

                    break;
            }
            b.on('click', functionButtonSendEtc);
            b.html('{$str3}');
            b.removeAttr('disabled');
        }
    });
};
$('.buttonModalSendEtc').click(functionButtonSendEtc);
JS
);

                                    ?>
                                    <?php /** @var Currency $currency */ ?>
                                    <?php foreach(Currency::find()->where(['in', 'id', [1,2,3,4,6]])->all() as $currency) { ?>
                                        <li>
                                            <a
                                                href="javascript:void(0);"
                                                class="itemCurrency"
                                                data-code="<?= $currency->code ?>"
                                                data-kurs="<?= $currency->kurs ?>"
                                                data-decimals="<?= $currency->decimals ?>"
                                                data-id="<?= $currency->id ?>">
                                                <?= $currency->code ?>
                                            </a>
                                        </li>
                                    <?php } ?>
                                </ul>
                            </div><!-- /btn-group -->
                        </div><!-- /input-group -->
                        <p class="help-block help-block-error" style="display: none;"></p>
                    </div>
                    <div class="form-group recipient-etc-field-password">
                        <label for="recipient-etc-name" class="control-label"><?= \Yii::t('c.TsqnzVaJuC', 'Пароль') ?>:</label>
                        <input type="password" class="form-control" id="recipient-etc-password" name="password">

                        <p class="help-block"><?= \Yii::t('c.TsqnzVaJuC', 'Пароль от кабинета') ?></p>
                        <p class="help-block help-block-error" style="display: none;"></p>
                    </div>
                    <div class="form-group recipient-etc-field-comment">
                        <label for="message-text" class="control-label"><?= \Yii::t('c.TsqnzVaJuC', 'Комментарий') ?>:</label>
                        <textarea class="form-control" id="recipient-etc-comment" name="comment"></textarea>

                        <p class="help-block help-block-error" style="display: none;"></p>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary buttonModalSendEtc" style="width: 100%;"><?= \Yii::t('c.TsqnzVaJuC', 'Перевести') ?></button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div><!-- /.modal -->


<div class="modal fade" tabindex="-1" role="dialog" id="modalSendConfirmEtc">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">&times;</span></button>
                <h4 class="modal-title  text-center"><?= \Yii::t('c.TsqnzVaJuC', 'Подтверждение') ?></h4>
            </div>
            <div class="modal-body text-center">
                content
            </div>
            <div class="modal-footer">
                <?php
                $str4 = Yii::t('c.TsqnzVaJuC', 'Транзакция успешно отправлена');
                $str5 = Yii::t('c.TsqnzVaJuC', 'Подтвердить');
                $p = new \avatar\modules\ETH\ServiceEtherScan();
                $url = $p->getUrl('/tx/');
                $this->registerJs(<<<JS
var functionButtonConfirm = function() {
    var b = $(this);
    b.off('click');
    b.html($('<i>', {class: 'fa fa-spinner fa-spin fa-fw'}));
    b.attr('disabled', 'disabled');
    var ret = $('#modalSendEtc').data('ret');
    var data = {
        address: ret.address,
        amount: ret.amount,
        comment: $('#recipient-etc-comment').val(),
        currency: ret.currency,
        billing_id: $('#recipient-etc-billing_id').val(),
        password: $('#recipient-etc-password').val()
    };
    $('#modalSendConfirmEtc').off('hidden.bs.modal');
    $('#modalSendConfirmEtc').on('hidden.bs.modal', function(e) {
        $('#modalSendConfirmEtc').off('hidden.bs.modal');
        $('.buttonModalConfirmEtc').show();
    });
    ajaxJson({
        url: '/cabinet-bills-etc/send-confirm',
        data: data,
        success: function(ret) {
            $('#modalSendConfirmEtc').on('hidden.bs.modal', function(e) {
                $('#modalInfoEtc .modal-body')
                .append(
                    $('<p>').html('{$str4}')
                )
                .append(
                    $('<p>').append(
                        $('<code>').html(ret.address)
                    )
                )
                ;
                $('#modalInfoEtc').on('hidden.bs.modal', function(e) {
                    window.location.reload();
                });
                $('#modalInfoEtc').modal();
            }).modal('hide');
            b.on('click', functionButtonConfirm);
            b.html('{$str5}');
            b.removeAttr('disabled');
        },
        errorScript: function(ret) {
            switch (ret.id) {
                case 101:
                    $.each(ret.data,function(i,v) {
                        var name = v.name;
                        var value = v.value;
                        $('#modalSendConfirmEtc .modal-body')
                        .append(
                            $('<p>', {class: 'alert alert-danger'}).html(value.join('<br>'))
                        );
                    });
                    $('#modalSendConfirmEtc .modal-body')
                        .append(
                            $('<button>', {class: 'btn btn-primary',style: 'width: 100%'}).html('Назад').click(function(e){
                                $('#modalSendConfirmEtc').on('hidden.bs.modal', function(e) {
                                    $('#modalSendEtc').modal();
                                }).modal('hide');
                            })
                        );
                    $('.buttonModalConfirm').hide();
                    break;
            }
            b.on('click', functionButtonConfirm);
            b.html('{$str5}');
            b.removeAttr('disabled');
        }
    });

};
$('.buttonModalConfirmEtc').click(functionButtonConfirm);
JS
                );
                ?>
                <button type="button" class="btn btn-primary buttonModalConfirmEtc" style="width: 100%;"><?= \Yii::t('c.TsqnzVaJuC', 'Подтвердить') ?></button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div><!-- /.modal -->

<div class="modal fade" id="modalInfoEtc" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">&times;</span></button>
                <h4 class="modal-title text-center" id="myModalLabel"><?= \Yii::t('c.TsqnzVaJuC', 'Информация') ?></h4>
            </div>
            <div class="modal-body text-center">

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" style="width: 100%;" data-dismiss="modal">
                    <?= \Yii::t('c.TsqnzVaJuC', 'Закрыть') ?>
                </button>
            </div>
        </div>
    </div>
</div>
