<?php
/**
 * Created by PhpStorm.
 * User: Бог-Творец
 * Date: 17.11.2016
 * Time: 2:44
 */

/* @var $this yii\web\View */
/* @var $billing \common\models\avatar\UserBill */

$this->title = $billing->name;

$src = '/images/controller/cabinet-bills/transactions/bitcoin2.jpg';
$options = [
        'src' => '/images/controller/cabinet-bills/transactions/bitcoin2.jpg',
    'title' => 'Свобода Истина Справедливость (В криптографию МЫ верим)',
    'style' => 'width: 100%;max-width: 308px;'
];
$rate = 'RUB/BTC' . ' ' . '=' . ' ' . \yii\helpers\Html::tag('b', Yii::$app->formatter->asDecimal(\common\models\avatar\Currency::getRate('BTC'),2), ['id' => 'kurs-rub-btc', 'data-value' => 1]);
$path = Yii::$app->assetManager->getBundle('avatar\assets\App\Asset')->baseUrl . '/images/ajax-loader.gif';

\avatar\assets\Clipboard::register($this);
$this->registerJs(<<<JS
var clipboard = new Clipboard('.buttonCopy');
clipboard.on('success', function(e) {
    $(e.trigger).tooltip({
        title: 'Скопировано',
        placement: 'bottom'
    }).tooltip('show');
});
JS
);


?>

<style>
    .textDecorated {
        text-decoration-line: underline;
        text-decoration-style: dotted;
    }
</style>
<div class="container">
<h2 class="page-header text-center"><?= $billing->name ?></h2>

<p class="text-center">
    <?= $rate ?> <span style="color: #ccc;">руб.</span>
</p>
<p class="text-center">
    <?= \yii\helpers\Html::tag('img', null, $options) ?>
</p>

    <div class="row" style="margin: 20px 0px 20px 0px;">
        <?php \common\assets\HighCharts\HighChartsAsset::register($this); ?>
        <div id="chartBTC"></div>
    </div>


<div class="input-group">
        <span class="input-group-btn">
            <button class="btn btn-default buttonCopy" title="<?= \Yii::t('c.dEj9SM57Nh', 'Скопировать адрес счета') ?>" data-clipboard-text="<?= $billing->address ?>" data-toggle="tooltip">
                <i class="glyphicon glyphicon-copy"></i>
            </button>
        </span>
        <input type="text" class="form-control" placeholder="Адрес кошелька"
               style="font-family: Consolas, Courier New, monospace;"
               value="<?= $billing->address ?>"
               id="internalAddress"
            >
        <span class="input-group-btn">
            <a href="<?= \yii\helpers\Url::to(['cabinet/get-qr-code', 'address' => $billing->address])?>" class="btn btn-default" data-toggle="tooltip" title="<?= \Yii::t('c.dEj9SM57Nh', 'Скачать QR код') ?>">
                <i class="fa fa-qrcode"></i>
            </a>
        </span>
</div>


<div class="modal fade" id="modalTransaction" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">&times;</span></button>
                <h4 class="modal-title text-center" id="myModalLabel"><?= \Yii::t('c.dEj9SM57Nh', 'Транзакция') ?></h4>
            </div>
            <div class="modal-body">
                <table class="table table-hover table-striped">
                    <tr>
                        <td><?= \Yii::t('c.dEj9SM57Nh', 'txid') ?></td>
                        <td class="txid"></td>
                    </tr>
                    <tr>
                        <td><?= \Yii::t('c.dEj9SM57Nh', 'amount') ?></td>
                        <td class="amount"></td>
                    </tr>
                    <tr>
                        <td><?= \Yii::t('c.dEj9SM57Nh', 'Комиссия1') ?></td>
                        <td class="fee"></td>
                    </tr>
                    <tr>
                        <td><?= \Yii::t('c.dEj9SM57Nh', 'Тип') ?></td>
                        <td class="type"></td>
                    </tr>
                </table>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal"><?= \Yii::t('c.TsqnzVaJuC', 'Закрыть') ?></button>
            </div>
        </div>
    </div>
</div>

<h2 class="alert alert-success text-center">
    <span id="external-wallet-confirmed"><img src="<?= $path ?>"></span><br>
    <small id="currencyString">BTC</small>
</h2>
<p style="display: none;"><?= \Yii::t('c.dEj9SM57Nh', 'Неподтвержденне транзакции') ?>:<br>
    <code><span id="external-wallet-unconfirmed"></span></code>
</p>
<?php
$s1 = Yii::t('c.dEj9SM57Nh', 'Расход');
$s2 = Yii::t('c.dEj9SM57Nh', 'Приход');
$this->registerJs(<<<JS
$('.buttonIn').click(function(e) {

    ajaxJson({
        url: '/cabinet-bills/in',
        data: {
            id: {$billing->id}
        },
        success: function(ret) {
            $('#modalIn .modal-body').html(ret.html);
            $('#modalIn').modal();
        }
    });
});
$('.buttonOut').click(function(e) {
    $('#modalOut').modal();
});

var functionShowTransaction = function(e) {
    var transaction = $(this).data('transaction');
    $('#modalTransaction .txid').html($('<code>').html(transaction.hash));
    $('#modalTransaction .fee').html($('<code>').html(transaction.fee));
    $('#modalTransaction .amount').html($('<code>').html(transaction.amount));
    $('#modalTransaction .type').html(
        transaction.direction == 'minus' ? $('<span>', {class: 'label label-danger'}).html('{$s1}') : $('<span>', {class: 'label label-success'}).html('{$s2}')
    );
    $('#modalTransaction').modal();
};

var functionPageClick = function(e) {
    var o = $(this);
    var href1 = o.data('href');
    ajaxJson({
        url: href1,
        success: function(ret) {
            var o = $(ret.html);
            o.find('.js-buttonTransactionInfo').click(functionShowTransaction);
            o.find('.paginationItem').each(function(i, v) {
                var o = $(v);
                var href1 = o.attr('href');
                o.attr('href', 'javascript:void(0)');
                o.attr('data-href', href1);
                o.click(functionPageClick);
            });
            o.find('[data-toggle="tooltip"]').tooltip();
            $('#transactions').html(o);
        }
    });
};
var operationList = [
    {
        url: '/cabinet-bills/get-balance' + '?' + 'id' + '=' + {$billing->id},
        success: function(ret) {

            if (ret.currencyView > 0) {
                $('#external-wallet-confirmed').html(ret.convertFormated);
                $('#currencyString').html(ret.currencyString);
            } else {
                $('#external-wallet-confirmed').html(ret.coinsFormated);
            }

            return true;
        },
        onErrorRepeat: true
    },
    {
        url: '/cabinet-bills/transactions-list',
        data: function() {
            return {
                balance: $('#internalAddress').attr('data-balance'),
                id: {$billing->id}
            };
        },
        beforeSend: function() {
            $('#transactions').html(
                $('<img>', {src: '{$path}' })
            );
        },
        success: function(ret) {
            var o = $(ret.html);
            
            // всплывание модального окна транзакции
            o.find('.js-buttonTransactionInfo').click(functionShowTransaction);
            o.find('.paginationItem').each(function(i, v) {
                var o = $(v);
                var href1 = o.attr('href');
                o.attr('href', 'javascript:void(0)');
                o.attr('data-href', href1);
                o.click(functionPageClick);
            });
            o.find('[data-toggle="tooltip"]').tooltip();

            $('#transactions').html(o);
            return true;
        },
        onErrorRepeat: true
    },
    {
        url: '/cabinet-bills-btc/kurs',
        success: function(ret) {
            draw2(ret.rows, ret.currency.code);
            return true;
        },
        onErrorRepeat: true
    }
];
var i = 0;

var functionCalc = function(i, arr) {
    var item = arr[i];
    var options = {
        url: item.url,
        success: function(ret) {
            i++;
            ret =  item.success(ret);
            if (i < operationList.length) {
                if (ret) functionCalc(i, arr);
            }
        },
        errorScript: function(ret) {
            if (typeof(item.onErrorRepeat) != "undefined") {
                if (item.onErrorRepeat) {
                    i++;
                    ret =  item.success(ret);
                    if (i <= operationList.length) {
                        if (ret) functionCalc(i, arr);
                    }
                }
            }
        },
        error: function(ret) {
            if (typeof(item.onErrorRepeat) != "undefined") {
                if (item.onErrorRepeat) {
                    i++;
                    ret =  item.success(ret);
                    if (i <= operationList.length) {
                        if (ret) functionCalc(i, arr);
                    }
                }
            }
        }
    };
    if (typeof(item.beforeSend) != "undefined") {
        options.beforeSend = item.beforeSend;
    }
    if (typeof(item.data) != "undefined") {
        options.data = item.data();
    }

    ajaxJson(options);
};
functionCalc(i, operationList);


/**
* 
* @param data array 
* [
* [
*   'time' => ''
*   'value' => ''
* ]
* ]
* @param lineName string название строки
* 
*/
function draw2(data, lineName) {
    var rows = data;
    var newRows = [];
    for(i = 0; i < rows.length; i++)
    {
        var item = rows[i];
        newRows.push({
            x: new Date(item.time * 1000),
            y: item.usd
        });
    }
    
    window.chart = $('#chartBTC').highcharts({
        chart: {
            zoomType: "x",
            type     : "spline"
        },
        title : {
            text : 'BTC'
        },
        subtitle : {
           text: 'subtitle'
        },
        xAxis: {
            type: "datetime"
        },
        yAxis : [
            {
                title : {
                    text : "Количество"
                }
            }
        ],
        legend : {
            enabled : true
        },
        tooltip : {
            crosshairs : true,
            shared : true
        },
        plotOptions : {
            series : {
                turboThreshold : 0
            }
        },
        series : [
            {
                type : "spline",
                name : lineName,
                data : newRows
            }
        ]
    });
}

JS
);
?>
<div class="row">
    <div class="col-lg-10 col-lg-offset-1">
        <center>
            <div class="input-group">
                <span class="input-group-btn">
                    <a href="https://localbitcoins.net/ru/buy_bitcoins" class="btn btn-default" target="_blank">
                        <i class="glyphicon glyphicon-import" style="margin-right: 5px;"></i><?= \Yii::t('c.dEj9SM57Nh', 'Купить') ?>
                    </a>
                    <a href="<?= \yii\helpers\Url::to(['cabinet-bills/settings', 'id' => $billing->id])?>" class="btn btn-default">
                        <i class="glyphicon glyphicon-cog" style="margin-right: 5px;"></i><?= \Yii::t('c.dEj9SM57Nh', 'Настройка') ?>
                    </a>
                    <a href="https://localbitcoins.net/ru/sell_bitcoins" class="btn btn-default" target="_blank">
                        <i class="glyphicon glyphicon-export" style="margin-right: 5px;"></i><?= \Yii::t('c.dEj9SM57Nh', 'Продать') ?>
                    </a>
                </span>
            </div>
            <!-- /input-group -->
        </center>
    </div>
</div>
<h4 class="page-header text-center"><?= \Yii::t('c.dEj9SM57Nh', 'Транзакции') ?></h4>
<div id="transactions">

</div>


</div>