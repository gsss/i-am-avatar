<?php
/**
 * Created by PhpStorm.
 * User: s.arhangelskiy
 * Date: 23.01.2017
 * Time: 10:21
 */

use common\models\avatar\Currency;

/* @var $this yii\web\View */

$currencyBTC = Currency::BTC;
$currencyETH = Currency::ETH;

$this->registerJs(<<<JS
$('.buttonSend').click(function(e) {
    e.preventDefault();
    e.stopPropagation();
    var o = $(this);
    var currency = o.data('currency');
    
    $('#recipient-billing_id').val(o.data('id'));
    $('.recipient-field-feeStrategy').hide();
    if (currency == {$currencyBTC}) {
        $('.recipient-field-feeStrategy').show();
        $('.itemCurrency[data-id={$currencyBTC}]').click();
    } else {
        $('.itemCurrency[data-id=' + currency + ']').click();
    }

    $('#recipient-address').val('');
    $('#recipient-amount').val('');
    $('#recipient-password').val('');
    $('#recipient-comment').val('');

    $('#modalSend').modal();
});
$('#formOut .form-control').on('focus', function() {
    var o = $(this);
    var p = o.parent();
    if (p.hasClass('input-group')) {
        p = p.parent();
    }
    p.removeClass('has-error');
    p.find('p.help-block-error').hide();
});

JS
);
?>


<div class="modal fade" tabindex="-1" role="dialog" id="modalSend">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">&times;</span></button>
                <h4 class="modal-title"><?= \Yii::t('c.TsqnzVaJuC', 'Перевод денег') ?></h4>
            </div>
            <div class="modal-body">
                <form id="formOut">
                    <input type="hidden" id="recipient-billing_id" name="billing_id"
                    >

                    <div class="form-group recipient-field-address">
                        <label for="recipient-name" class="control-label"><?= \Yii::t('c.TsqnzVaJuC', 'Кому') ?>:</label>
                        <input type="text" class="form-control" id="recipient-address" name="address"
                               style="font-family: Consolas, Courier New, monospace"
                        >
                        <p class="help-block"><?= \Yii::t('c.TsqnzVaJuC', 'Можно указать адрес счета, почту или телефон клиента') ?></p>

                        <p class="help-block help-block-error" style="display: none;"></p>
                    </div>
                    <div class="form-group recipient-field-amount">
                        <label for="recipient-name" class="control-label"><?= \Yii::t('c.TsqnzVaJuC', 'Сколько') ?>:</label>

                        <div class="input-group">
                            <input type="text"
                                   name="amount"
                                   class="form-control"
                                   aria-label="..."
                                   placeholder="0.00000001"
                                   autocomplete="off"
                                   id="recipient-amount"
                                   style="font-family: Consolas, Courier New, monospace">
                            <input type="hidden"
                                   name="currency"
                                   id="recipient-currency"
                                   value="BTC"
                                   >
                            <div class="input-group-btn">
                                <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true"
                                        aria-expanded="false"><span class="buttonCurrency">BTC</span> <span class="caret"></span></button>
                                <ul class="dropdown-menu dropdown-menu-right">
                                    <?php
                                    $str1= Yii::t('c.TsqnzVaJuC', 'Клиент');
                                    $str2= Yii::t('c.TsqnzVaJuC', 'Счет');
                                    $str3= Yii::t('c.TsqnzVaJuC', 'Перевести');
                                    $this->registerJs(<<<JS
$('.itemCurrency').click(function(e) {
    $('#recipient-currency').val($(this).data('code'));
    $('.buttonCurrency').html($(this).html());
    var i,s='',c=$(this).data('decimals');
    for(i = 0; i < c; i++) {
        s = s + '0';
    }
    $('#recipient-amount').attr('placeholder', '0.'+ s)
});


var functionButtonSend = function() {
    var b = $(this);
    b.off('click');
    b.html($('<i>', {class: 'fa fa-spinner fa-spin fa-fw'}));
    b.attr('disabled', 'disabled');
    ajaxJson({
        url: '/cabinet-bills/send',
        data: $('#formOut').serializeArray(),
        success: function(ret) {
            $('#modalSend').data('ret', ret);
            $('#modalSend').modal('hide').on('hidden.bs.modal', function(e) {
                if (ret.hasUser) {
                    $('#modalSendConfirm .modal-body').html(
                        $('<p>').append(
                            $('<img>', {src:ret.user.avatar,width:'300',class: 'img-circle'})
                        )
                    )
                    .append(
                        $('<p>').html(ret.user.name2)
                    )
                    .append(
                        $('<p>').html($('<b>').html('{$str2}'))
                    )
                    .append(
                        $('<p>').append(
                            $('<code>').html(ret.billing.address)
                        )
                    )
                    .append(
                        $('<p>').html(ret.billing.name)
                    )
                    ;
                } else {
                    $('#modalSendConfirm .modal-body').html(
                        $('<p>').html($('<b>').html('{$str2}'))
                    )
                    .append(
                        $('<p>').append(
                            $('<code>').html(ret.address)
                        )
                    )
                }
                $('#modalSendConfirm').modal();
                $('#modalSend').off('hidden.bs.modal');
                b.on('click', functionButtonSend);
                b.html('{$str3}');
                b.removeAttr('disabled');
            });
        },
        errorScript: function(ret) {
            switch (ret.id) {
                case 101:
                    var f = $('#modalSend');
                    $.each(ret.data,function(i,v) {
                        var name = v.name;
                        var value = v.value;
                        var o = f.find('#recipient-'+ name);
                        var t = f.find('.recipient-field-' + name);
                        t.addClass('has-error');
                        t.find('p.help-block-error').html(value.join('<br>')).show();
                    });

                    break;
            }
            b.on('click', functionButtonSend);
            b.html('{$str3}');
            b.removeAttr('disabled');
        }
    });
};
$('.buttonModalSend').click(functionButtonSend);
JS
);

                                    ?>
                                    <?php /** @var Currency $currency */ ?>
                                    <?php foreach(Currency::find()->where(['in', 'id', [1,2,3,4,6]])->all() as $currency) { ?>
                                        <li>
                                            <a
                                                href="javascript:void(0);"
                                                class="itemCurrency"
                                                data-code="<?= $currency->code ?>"
                                                data-kurs="<?= $currency->kurs ?>"
                                                data-decimals="<?= $currency->decimals ?>"
                                                data-id="<?= $currency->id ?>">
                                                <?= $currency->code ?>
                                            </a>
                                        </li>
                                    <?php } ?>
                                </ul>
                            </div><!-- /btn-group -->
                        </div><!-- /input-group -->
                        <p class="help-block help-block-error" style="display: none;"></p>
                    </div>
                    <div class="form-group recipient-field-password">
                        <label for="recipient-name" class="control-label"><?= \Yii::t('c.TsqnzVaJuC', 'Пароль') ?>:</label>
                        <input type="password" class="form-control" id="recipient-password" name="password">

                        <p class="help-block"><?= \Yii::t('c.TsqnzVaJuC', 'Пароль от кабинета') ?></p>
                        <p class="help-block help-block-error" style="display: none;"></p>
                    </div>
                    <div class="form-group recipient-field-comment">
                        <label for="message-text" class="control-label"><?= \Yii::t('c.TsqnzVaJuC', 'Комментарий') ?>:</label>
                        <textarea class="form-control" id="recipient-comment" name="comment"></textarea>

                        <p class="help-block help-block-error" style="display: none;"></p>
                    </div>
                    <div class="form-group recipient-field-feeStrategy">
                        <label for="recipient-name" class="control-label">Использовать оптимальную комиссию для скорости?</label>
                        <?= \cs\Widget\CheckBox2\CheckBox::widget([
                            'model'     => new \avatar\models\forms\PiramidaSend(['feeStrategy' => true]),
                            'attribute' => 'feeStrategy',
                        ]) ?>
                        <p class="help-block">Да - транзакция будет подтверждена через 10 мин (дороже), Нет - стандартная комиссиия, нет гарантии на подтверждение (дешевле)</p>
                        <p class="help-block help-block-error" style="display: none;"></p>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary buttonModalSend" style="width: 100%;"><?= \Yii::t('c.TsqnzVaJuC', 'Перевести') ?></button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div><!-- /.modal -->


<div class="modal fade" tabindex="-1" role="dialog" id="modalSendConfirm">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">&times;</span></button>
                <h4 class="modal-title  text-center"><?= \Yii::t('c.TsqnzVaJuC', 'Подтверждение') ?></h4>
            </div>
            <div class="modal-body text-center">
                content
            </div>
            <div class="modal-footer">
                <?php
                $str4 = Yii::t('c.TsqnzVaJuC', 'Транзакция успешно отправлена');
                $str5 = Yii::t('c.TsqnzVaJuC', 'Подтвердить');
                $p = new \avatar\modules\ETH\ServiceEtherScan();
                $url = $p->getUrl('/tx/');
                $this->registerJs(<<<JS
var functionButtonConfirm = function() {
    var b = $(this);
    b.off('click');
    b.html($('<i>', {class: 'fa fa-spinner fa-spin fa-fw'}));
    b.attr('disabled', 'disabled');
    var ret = $('#modalSend').data('ret');
    var data = {
        address: ret.address,
        amount: ret.amount,
        comment: $('#recipient-comment').val(),
        currency: ret.currency,
        billing_id: $('#recipient-billing_id').val(),
        password: $('#recipient-password').val(),
        feeStrategy: $('#piramidasend-feestrategy').is(':checked') ? 1 : 0 
    };
    $('#modalSendConfirm').off('hidden.bs.modal');
    $('#modalSendConfirm').on('hidden.bs.modal', function(e) {
        $('#modalSendConfirm').off('hidden.bs.modal');
        $('.buttonModalConfirm').show();
    });
    ajaxJson({
        url: '/cabinet-bills/send-confirm',
        data: data,
        success: function(ret) {
            $('#modalSendConfirm').on('hidden.bs.modal', function(e) {
                $('#modalInfo .modal-body')
                .append(
                    $('<p>').html('{$str4}')
                )
                .append(
                    $('<p>').append(
                        $('<code>').html(ret.address)
                    )
                )
                ;
                $('#modalInfo').on('hidden.bs.modal', function(e) {
                    window.location.reload();
                });
                $('#modalInfo').modal();
            }).modal('hide');
            b.on('click', functionButtonConfirm);
            b.html('{$str5}');
            b.removeAttr('disabled');
        },
        errorScript: function(ret) {
            switch (ret.id) {
                case 101:
                    $.each(ret.data,function(i,v) {
                        var name = v.name;
                        var value = v.value;
                        $('#modalSendConfirm .modal-body')
                        .append(
                            $('<p>', {class: 'alert alert-danger'}).html(value.join('<br>'))
                        );
                    });
                    $('#modalSendConfirm .modal-body')
                        .append(
                            $('<button>', {class: 'btn btn-primary',style: 'width: 100%'}).html('Назад').click(function(e){
                                $('#modalSendConfirm').on('hidden.bs.modal', function(e) {
                                    $('#modalSend').modal();
                                }).modal('hide');
                            })
                        );
                    $('.buttonModalConfirm').hide();
                    break;
            }
            b.on('click', functionButtonConfirm);
            b.html('{$str5}');
            b.removeAttr('disabled');
        }
    });

};
$('.buttonModalConfirm').click(functionButtonConfirm);
JS
                );
                ?>
                <button type="button" class="btn btn-primary buttonModalConfirm" style="width: 100%;"><?= \Yii::t('c.TsqnzVaJuC', 'Подтвердить') ?></button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div><!-- /.modal -->

<div class="modal fade" id="modalInfo" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">&times;</span></button>
                <h4 class="modal-title text-center" id="myModalLabel"><?= \Yii::t('c.TsqnzVaJuC', 'Информация') ?></h4>
            </div>
            <div class="modal-body text-center">

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" style="width: 100%;" data-dismiss="modal">
                    <?= \Yii::t('c.TsqnzVaJuC', 'Закрыть') ?>
                </button>
            </div>
        </div>
    </div>
</div>
