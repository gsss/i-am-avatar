<?php

/** @var $this \yii\web\View */
/** @var $school \common\models\school\School */
/** @var $model \avatar\models\forms\DnsRequest */

use yii\helpers\Url;
use app\services\GsssHtml;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use app\services\GetArticle\Collection;

$this->title = $school->name;

$request = \common\models\school\DnsRequest::findOne(['school_id' => $school->id, 'is_dns_verified' => 0]);

$ip = Yii::$app->params['schoolIp'];

?>

<div class="container">

    <h1 class="page-header"><?= Html::encode($this->title) ?></h1>

    <?php if ($request) { ?>
        <?php // заявка отправлена ?>
        <div class="alert alert-success">
            Заявка отправлена <?= Yii::$app->formatter->asDatetime($request->created_at) ?>. Скрок рассмотрения может продлиться до трех дней.
        </div>
        <p>В DNS таблице своего домена создайте запись типа A и укажите IP:
            <code><?= $ip ?></code>.
        </p>
    <?php } else { ?>
        <?php // отправить заявку ?>
        <div class="row">
            <div class="col-lg-8">

                <?php if ($school->dns) : ?>

                    <p class="alert alert-success">Ваш домен подключен!</p>
                    <p>Домен: <code>
                            <a href="<?= $school->getUrl() ?>" target="_blank">
                                <?= $school->getUrl() ?>
                            </a>
                        </code>
                    </p>
                    <p>Если он не показывается то проверьте что вы указали в DNS таблице своего домена запись типа A и укажите IP:
                        <code><?= $ip ?></code>.
                    </p>
                <?php
                    $sid = $school->id;
                    $this->registerJs(<<<JS
$('.buttonCancel').click(function(e) {  
    if (confirm('Подтвердите отмену')) {
        ajaxJson({
            url: '/cabinet-school-dns/reject',
            data: {id: {$sid} },
            success: function(ret) {
                $('#modalInfo').modal().on('hidden.bs.modal', function(e) {
                    window.location.reload();    
                });
            }
        });
    }
})
JS
);
                    ?>
                    <p><button class="btn btn-success buttonCancel">Отменить</button></p>

                <?php else: ?>
                    <p>Чтобы подключить свой домен пропишите в DNS таблице своего домена запись типа A и укажите IP:
                        <code><?= $ip ?></code>. Ниже впишите его адрес без www (например <code>domain.ru</code>).
                    </p>
                    <p>Если вы хотите выбрать домен третьего уровня типа domain.people4people.su, то впишите ниже
                        название домена (например <code>domain</code>).</p>

                    <div class="row">
                        <div class="col-lg-8">

                            <?php $form = \yii\bootstrap\ActiveForm::begin([
                                'options'              => ['enctype' => 'multipart/form-data'],
                                'enableAjaxValidation' => true,
                            ]); ?>
                            <?= $form->field($model, 'name', ['inputOptions' => ['placeholder' => 'domain']])
                                ->label('domain', ['class' => 'hide'])
                                ->hint('Возможны только малые буквы латинского алфавита, цифры, дефис, подчеркивание и точка для домена второго уровня')
                            ?>

                            <hr>
                            <div class="form-group">
                                <?= Html::submitButton('Отправить заявку', [
                                    'class' => 'btn btn-success',
                                    'name'  => 'contact-button',
                                    'style' => 'width:100%',
                                ]) ?>
                            </div>
                            <?php \yii\bootstrap\ActiveForm::end(); ?>
                        </div>
                    </div>
                <?php endif; ?>
            </div>
        </div>
    <?php } ?>


</div>

<div class="modal fade" id="modalInfo" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Информация</h4>
            </div>
            <div class="modal-body">
                Успешно!
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
            </div>
        </div>
    </div>
</div>