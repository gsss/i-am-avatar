<?php

/** @var $this \yii\web\View */
/** @var $school \common\models\school\School */
/** @var $model \common\models\school\School */

use yii\helpers\Url;
use app\services\GsssHtml;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use app\services\GetArticle\Collection;

$this->title = 'Поиск домена';

$request = \common\models\school\DnsRequest::findOne(['school_id' => $school->id, 'is_dns_verified' => 0]);
?>

<div class="container">
    <?php \avatar\widgets\SchoolMenu::begin(['school' => $school]) ?>
    <h1 class="page-header"><?= Html::encode($this->title) ?></h1>

    <div class="row">
        <div class="col-lg-8">

            <?php
            $sid = $school->id;
            $this->registerJs(<<<JS
$('.buttonFind').click(function(e) {  
    ajaxJson({
        url: '/cabinet-school-dns/search-ajax',
        data: {id: {$sid}, term: $('.inputCode').val() },
        success: function(ret) {
            if (ret.result) {
                $('#result')
                .append($('<p>').html('Цена = ' + ret.priceFormatted + ' RUB'))
                .append($('<button>', {class: 'btn btn-success'}).html('Купить').click(function(ret) {
                    console.log(1);
                }));
            }
        }
    });
})
JS
            );
            ?>

            <input class="form-control inputCode" type="text" value="" placeholder="Введите ключевое слово ...">

            <hr>
            <button class="btn btn-success buttonFind" name="contact-button" style="width:100%">Найти</button>

            <div id="result" style="margin-top: 50px;"></div>
        </div>
    </div>



    <?php \avatar\widgets\SchoolMenu::end() ?>

</div>
