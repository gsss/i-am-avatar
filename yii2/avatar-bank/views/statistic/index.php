<?php
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
$this->title = 'Статистика рынка';


?>

<div class="container" style="padding-bottom: 70px;">
    <div class="col-lg-12">
        <h1 class="page-header text-center"><?= $this->title ?></h1>
    </div>
    <div class="row" style="margin: 20px 0px 20px 0px;">
        <div class="col-lg-8 col-lg-offset-2">
            <h2 class="page-header text-center">Общая капитализация всех криптовалют</h2>
            <?php
            $period = 24 * 2;

            $rows = \common\models\CryptoCap::find()
                ->select([
                    'value as rub',
                    'time',
                ])
                ->where(['>', 'time', time() - (60 * 60 * 24 * 30)])
                ->orderBy(['time' => SORT_ASC])
                ->asArray()
                ->all();

            $rowsJson = \yii\helpers\Json::encode($rows);
            $this->registerJs(<<<JS
Highcharts.setOptions({
    global: {
        timezoneOffset: -3 * 60
    }
});
var rows = {$rowsJson};
var newRows = [];
for(i = 0; i < rows.length; i++)
{
    var item = rows[i];
    newRows.push({
        x: new Date(item.time * 1000),
        y: parseFloat(item.rub)
    });
}
JS
            );
            ?>


        <?= \common\widgets\HighCharts\HighCharts::widget([
                'chartOptions' => [
                    'chart'       => [
                        'zoomType' => 'x',
                        'type'     => 'spline',
                    ],
                    'title'       => [
                        'text' => 'График',
                    ],
                    'subtitle'    => [
                        'text' => 'Выделите область для изменения масштаба',
                    ],
                    'xAxis'       => [
                        'type' => 'datetime',
                    ],
                    'yAxis'       => [
                        [
                            'title' => [
                                'text' => 'Количество',
                            ],
                        ],
                    ],
                    'legend'      => [
                        'enabled' => true,
                    ],
                    'tooltip'     => [
                        'crosshairs' => true,
                        'shared'     => true,
                    ],
                    'plotOptions' => [
                        'series' => [
                            'turboThreshold' => 0,
                        ],
                    ],
                    'series'      => [
                        [
                            'type' => 'spline',
                            'name' => 'Млрд $',
                            'data' => new \yii\web\JsExpression('newRows'),
                        ],
                    ],
                ],
            ]);
            ?>
        </div>
    </div>
    <div class="col-lg-12">
            <h2 class="page-header text-center">Валюты</h2>
            <?php \yii\widgets\Pjax::begin(); ?>
            <?php

            $this->registerJS(<<<JS
$('[data-toggle="tooltip"]').tooltip();


$('.rowTable').click(function() {
    window.location = '/statistic/item' + '?' + 'id' + '=' + $(this).data('id');
});
JS
            );
            $sort = new \yii\data\Sort([
                'attributes'   => [
                    'code',
                    'title'            => ['label' => 'Наименование'],
                    'price_usd'        => ['label' => 'Цена, $', 'default' => SORT_DESC],
                    'chanche_1h'       => ['label' => '1час, %', 'default' => SORT_DESC],
                    'chanche_1d'       => ['label' => '1день, %', 'default' => SORT_DESC],
                    'chanche_1w'       => ['label' => '1нед, %', 'default' => SORT_DESC],
                    'day_volume_usd'   => ['label' => 'Объем за 24ч., USD', 'default' => SORT_DESC],
                    'day_volume_btc'   => ['label' => 'Объем за 24ч., BTC', 'default' => SORT_DESC],
                    'market_cap_usd'   => ['label' => 'Капитализация, USD', 'default' => SORT_DESC],
                    'available_supply' => ['label' => 'Монет в обороте', 'default' => SORT_DESC],
                    'total_supply',
                    'max_supply',
                    'rank'             => ['label' => Html::tag('span', '#', ['title' => 'Рейтинг', 'data' => ['toggle' => 'tooltip']])],
                ],
                'defaultOrder' => [
                    'rank' => SORT_ASC
                ]
            ]);
            $model = new \avatar\models\search\StatisticCurrency();
            $where = [
                'and',
                ['is_crypto' => 1],
                ['not', ['code_coinmarketcap' => '']],
                ['not in', 'id', [8, 9]],
            ];
            $provider = $model->search(Yii::$app->request->get(), $where, $sort);

            ?>
            <?= \yii\grid\GridView::widget([
                'dataProvider' => $provider,
                'filterModel' => $model,
                'tableOptions' => [
                    'class' => 'table table-striped table-hover',
                    'align' => 'center'
                ],
                'rowOptions'   => function ($item) {
                    $data = [
                        'data'  => ['id' => $item['id']],
                        'role'  => 'button',
                        'class' => 'rowTable'
                    ];
                    return $data;
                },
                'columns' => [
                    [
                        'attribute'     => 'rank',
                        'header'        => $sort->link('rank'),
                    ],
                    [
                        'header'  => 'Картинка',
                        'content' => function ($item) {
                            $i = \yii\helpers\ArrayHelper::getValue($item, 'image', '');
                            if ($i == '') return '';

                            return Html::img($i, [
                                'class'  => "img-circle",
                                'width'  => 50,
                                'height' => 50,
                                'style'  => 'margin-bottom: 0px;',
                            ]);
                        },
                    ],
                    'code',
                    'title',
                    [
                        'header'         => $sort->link('market_cap_usd'),
                        'attribute'      => 'market_cap_usd',
                        'contentOptions' => [
                            'style' => 'text-align:right;',
                        ],
                        'headerOptions'  => [
                            'style' => 'text-align:right;',
                        ],
                        'format'         => ['decimal', 0],
                    ],
                    [
                        'attribute'      => 'price_usd',
                        'contentOptions' => [
                            'style' => 'text-align:right;font-weight: bold;',
                        ],
                        'headerOptions'  => [
                            'style' => 'text-align:right;',
                        ],
                        'content'        => function ($item) {
                            $i = \yii\helpers\ArrayHelper::getValue($item, 'price_usd', '');
                            if ($i == '') return '';

                            if ($i < 0) {
                                $style[] = 'color: #F45F5F';
                            } else {
                                $style[] = 'color: #6ED098';
                            }
                            $v = Yii::$app->formatter->asDecimal($i, 4);
                            $arr = explode('.', $v);
                            $style[] = 'color: #888';
                            $nullsHtml = [];
                            $arr2 = \cs\services\Str::getChars($arr[1]);
                            $colors = [
                                '#888',
                                '#999',
                                '#aaa',
                                '#bbb',
                            ];
                            for ($i = 0; $i < 4; $i++) {
                                $nullsHtml[] = Html::tag(
                                    'span',
                                    $arr2[ $i ],
                                    ['style' => 'color: ' . $colors[ $i ]]
                                );
                            }

                            return $arr[0] . '.' . join('', $nullsHtml);
                        },
                    ],
                    [
                        'header'         => $sort->link('day_volume_usd'),
                        'attribute'      => 'day_volume_usd',
                        'contentOptions' => [
                            'style' => 'text-align:right;',
                        ],
                        'headerOptions'  => [
                            'style' => 'text-align:right;',
                        ],
                        'format'         => ['decimal', 0],
                    ],
                    [
                        'header'         => $sort->link('day_volume_btc'),
                        'attribute'      => 'day_volume_btc',
                        'contentOptions' => [
                            'style' => 'text-align:right;',
                        ],
                        'headerOptions'  => [
                            'style' => 'text-align:right;',
                        ],
                        'format'         => ['decimal', 0],
                    ],
                    [
                        'header'         => $sort->link('chanche_1h'),
                        'attribute'      => 'chanche_1h',
                        'contentOptions' => [
                            'style' => 'text-align:right;',
                        ],
                        'headerOptions'  => [
                            'style' => 'text-align:right;',
                        ],
                        'content'        => function ($item) {
                            $i = \yii\helpers\ArrayHelper::getValue($item, 'chanche_1h', '');
                            if ($i == '') return '';

                            if ($i < 0) {
                                $style[] = 'color: #F45F5F';
                            } else {
                                $style[] = 'color: #6ED098';
                            }

                            return Html::tag('span', (($i > 0) ? '+' : '') . Yii::$app->formatter->asDecimal($i, 2), ['style' => join(';', $style)]);
                        },
                    ],
                    [
                        'header'         => $sort->link('chanche_1d'),
                        'attribute'      => 'chanche_1d',
                        'contentOptions' => [
                            'style' => 'text-align:right;',
                        ],
                        'headerOptions'  => [
                            'style' => 'text-align:right;',
                        ],
                        'content'        => function ($item) {
                            $i = \yii\helpers\ArrayHelper::getValue($item, 'chanche_1d', '');
                            if ($i == '') return '';

                            if ($i < 0) {
                                $style[] = 'color: #F45F5F';
                            } else {
                                $style[] = 'color: #6ED098';
                            }

                            return Html::tag('span', (($i > 0) ? '+' : '') . Yii::$app->formatter->asDecimal($i, 2), ['style' => join(';', $style)]);
                        },
                    ],
                    [
                        'header'         => $sort->link('chanche_1w'),
                        'attribute'      => 'chanche_1w',
                        'contentOptions' => [
                            'style' => 'text-align:right;',
                        ],
                        'headerOptions'  => [
                            'style' => 'text-align:right;',
                        ],
                        'content'        => function ($item) {
                            $i = \yii\helpers\ArrayHelper::getValue($item, 'chanche_1w', '');
                            if ($i == '') return '';

                            if ($i < 0) {
                                $style[] = 'color: #F45F5F';
                            } else {
                                $style[] = 'color: #6ED098';
                            }

                            return Html::tag('span', (($i > 0) ? '+' : '') . Yii::$app->formatter->asDecimal($i, 2), ['style' => join(';', $style)]);
                        },
                    ],
                    [
                        'header'         => $sort->link('available_supply'),
                        'attribute'      => 'available_supply',
                        'contentOptions' => [
                            'style' => 'text-align:right;',
                        ],
                        'headerOptions'  => [
                            'style' => 'text-align:right;',
                        ],
                        'format'         => ['decimal', 0],
                    ],
                ],
            ]) ?>
            <?php \yii\widgets\Pjax::end(); ?>
        <hr>
        <p class="alert alert-success">Статистика предоставлена сервисом <a href="https://coinmarketcap.com/" target="_blank">https://coinmarketcap.com/</a>.</p>
    </div>
    <div class="col-lg-12">
        <hr>
    </div>
    <center>
        <?= $this->render('../blocks/share', [
            'image'       => Url::to('/images/controller/site/about/logo.jpg', true),
            'title'       => $this->title,
            'url'         => Url::current([], true),
            'description' => 'Банк в блокчейн сети для криптовалют — это возможность построить более экономически справедливый и более технически совершенный мир. Безусловно, для существования подобного мира недостаточно одной лишь технологии: необходимо и достаточное количество людей, разделяющих трансперсональные ценности — не ради только себя, но и ради человечества как единого организма.',

        ]) ?>
    </center>
</div>


