<?php

/**
 * Created by PhpStorm.
 * User: god
 * Date: 13.04.2017
 * Time: 23:58
 */

use yii\helpers\Html;


/* @var $this yii\web\View */


$this->title = 'AvatarCoin';

?>
<div class="container">
    <p class="col-lg-12">
        <h1 class="page-header text-center"><?= Html::encode($this->title) ?></h1>
        <h3 class="page-header">Описание</h3>

        <p>Есть функция которая делегирет хозяина контракта. <code>changeOwner(owner)</code></p>
        <p>Kill не используем.</p>
        <p>Название токена "AVR" (AvatarCoin).</p>

        <p>Эмиссия токенов</p>
        <p>Кол-во эмиссии 100,000,000 AVR</p>
        <p>Кол-во знаков после запятой 8</p>
        <p>Краудсейл контракт встроенный</p>

        <p>Токены распределяются в три этапа в соответствии с <a href="/images/controller/development-contracts/avr-token/ico-avatar-network.xlsx" target="_blank">таблицей</a></p>
        <p>Распродаются токены из фондов "Инвесторы" и "Инвестиционный фонд".</p>
        <p>Есть функция <code>setEthPrice(value)</code> устанавливает цену эфира в долларах во время продаж на случай если цена будет меняться.</p>


        <p>Цена эфира - <code>EthPrice</code></p>
        <p>Цена токена на этап продаж - <code>TokenPrice</code></p>
        <p>Кол-во Эфира высланных на контракт клиентом для покупки токена - <code>EtherAmount</code></p>
        <p>Кол-во отгружаемых токенов отпределяется по формуле <code>tokenAmount = (EtherAmount * EthPrice) / TokenPrice</code></p>

        <p>Есть функция <code>setStartPreSale(time)</code> устанавливает старт продаж этапа PreSale, можно вызвать только если setStartPreIco и setStartIco не вызывались.</p>
        <p>Есть функция <code>setStartPreIco(time)</code> устанавливает старт продаж этапа PreIco, можно вызвать только после setStartPreSale.</p>
        <p>Есть функция <code>setStartIco(time)</code> устанавливает старт продаж этапа Ico, можно вызвать только после setStartPreIco.</p>
        <p>Есть функция <code>sendFund(address)</code> – отправляет собранные эфиры на address.</p>

        <p>При инициализации контракта можно задать параметр продаж токена, включена или выключена и установить цену токена.</p>
        <p>Продажи токена производятся через безымянную функцию, которая срабатывает на прием эфира.</p>

        <p>Все нераспроданные токены на предыдущем этапе переходят на следущий</p>
        <p>Если выделенное кол-во токенов на этап продажи заканчивается то продажи останавливаются и доп токены не выделяются.</p>
        <p>После завершения ICO все нераспроданные токены сжигаются и выводятся на биржу.</p>


<p>Код контракта:</p>
    <pre><?= file_get_contents(Yii::getAlias('@avatar/views/development-contracts/avr-token.sol')) ?></pre>



    <h3 class="page-header">Предположение на довыпуск токенов</h3>
    <p>Я хочу ввести довыпуск токенов на следующих условиях: Если больше половины веса токенов проголосуют за то чтобы выпустить токены которые будут обеспечены реальыми вложениями в компанию AvatarNetwork.</p>
    <p>Что значит "больше половины веса токенов проголосуют"? Это значит что если инвестор держит у себя 10,000 AVR то он голосуя "да" высказывает их в весе равным 10,000. То есть он может перебить 9,999 инвесторов имеющих по 1 AVR проголосовавших "нет".</p>
    <p>Допустим ситуация: к нам приходит компания и хочет вложить в нас свои ресурсы (допкстим заводы, лаборатории, земли) за часть прибыли которые будут проходить под нашими брендами, логично при ограниченном кол-ве токенов они будут сразу расти, а если довыпустить токены то эти токены можно выдать этой же компании.</p>
    <p>Меня смущает факто что раз выпустившись токенв потом уже для самой компании становлтся неактуальными. Вообщем для рассмотрения оставляю пока.</p>

    <div class="row" style="margin: 100px 0px 70px 0px;">
        <h1 class="page-header text-center">Распределение токенов по фондам</h1>
        <?php

        $this->registerJs(<<<JS
Highcharts.getOptions().plotOptions.pie.colors = (function () {
    var colors = [],
        base = Highcharts.getOptions().colors[0],
        i;

    for (i = 0; i < 10; i += 1) {
        // Start out with a darkened base color (negative brighten), and end
        // up with a much brighter color
        colors.push(Highcharts.Color(base).brighten((i - 3) / 7).get());
    }
    return colors;
}());
JS
        );
        ?>
        <?= \common\widgets\HighCharts\HighCharts::widget([
            'chartOptions' => [
                'chart'     => [
                    'plotBackgroundColor' => null,
                    'plotBorderWidth' => null,
                    'plotShadow' => false,
                    'type' => 'pie',
                ],
                'title'     => [
                    'text' => 'Распределение токенов по фондам',
                ],
                'tooltip' => [
                    'pointFormat' => '{series.name}: <b>{point.percentage:.1f}%</b>'
                ],
                'plotOptions' => [
                    'pie' => [
                        'allowPointSelect' => true,
                        'cursor' => 'pointer',
                        'dataLabels' => [
                            'enabled' => true,
                            'format' => '<b>{point.name}</b>: {point.percentage:.1f} %',
                            'style' => [
                                'color' => new \yii\web\JsExpression('(Highcharts.theme && Highcharts.theme.contrastTextColor) || \'black\''),
                            ]
                        ]
                    ]
                ],
                'series' => [
                    [
                        'name' => 'Токенов',
                        'colorByPoint' => true,
                        'data' => [
                            [
                                'name' => 'Инвесторы',
                                'y'    => 65,
                            ],
                            [
                                'name' => 'Адвайзеры',
                                'y'    => 5,
                            ],
                            [
                                'name' => 'Баунти программа',
                                'y'    => 10,
                            ],
                            [
                                'name' => 'Инвестиционный фонд',
                                'y'    => 10,
                            ],
                            [
                                'name' => 'Основатели',
                                'y'    => 10,
                            ],
                        ]
                    ]
                ]
            ],
        ]);
        ?>
    </div>
    <div class="row" style="margin: 100px 0px 70px 0px;">
        <h1 class="page-header text-center">Распределение токенов по этапам продаж</h1>
        <?= \common\widgets\HighCharts\HighCharts::widget([
            'chartOptions' => [
                'chart'     => [
                    'type' => 'column',
                ],
                'title'     => [
                    'text' => 'Распределение токенов по этапам продаж',
                ],
                'colors' => ['#7cb5ec', '#5891c8', '#346da4', '#0f487f'],
                'xAxis' => [
                    'categories' => [
                        'preSale',
                        'preICO',
                        'ICO',
                    ],
                    'crosshair' => true
                ],
                'yAxis' => [
                    'min' => 0,
                    'title' => [
                        'text' => 'AVR',
                    ]
                ],
                'tooltip' => [
                    'valueSuffix' => ' AVR',
                ],
                'plotOptions' => [
                    'column' => [
                        'pointPadding' => 0.2,
                        'borderWidth' => 0,
                    ]
                ],
                'series' => [
                    [
                        'name' => 'Токены',
                        'data' => [3000000, 9000000, 88000000],
                    ],
                ]
            ],
        ]);
        ?>
    </div>
    <div class="row" style="margin: 100px 0px 70px 0px;">
        <h1 class="page-header text-center">Цена токена на этапах продаж</h1>
        <?= \common\widgets\HighCharts\HighCharts::widget([
            'chartOptions' => [
                'chart'     => [
                    'type' => 'column',
                ],
                'title'     => [
                    'text' => 'Цена токена на этапах продаж',
                ],
                'xAxis' => [
                    'categories' => [
                        'preSale',
                        'preICO',
                        'ICO',
                    ],
                    'crosshair' => true
                ],
                'yAxis' => [
                    'min' => 0,
                    'title' => [
                        'text' => 'USD',
                    ]
                ],
                'tooltip' => [
                    'valueSuffix' => ' USD',
                ],
                'plotOptions' => [
                    'column' => [
                        'pointPadding' => 0.2,
                        'borderWidth'  => 0,
                    ],
                ],
                'colors' => ['#7cb5ec', '#5891c8', '#346da4', '#0f487f'],
                'series' => [
                    [
                        'name' => '1 неделя',
                        'data' => [0.05, 0.2, 0.33],
                    ],
                    [
                        'name' => '2 неделя',
                        'data' => [0.09, 0.23, 0.37],
                    ],
                    [
                        'name' => '3 неделя',
                        'data' => [0.12, 0.26, 0.44],
                    ],
                    [
                        'name' => '4 неделя',
                        'data' => [0.15, 0.30, 0.50],
                    ],
                ],
            ],
        ]);
        ?>
    </div>
</div>

