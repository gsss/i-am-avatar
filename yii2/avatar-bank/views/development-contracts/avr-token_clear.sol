pragma solidity ^0.4.8;

/*
AvatarNetwork Copyright

https://avatarnetwork.io
*/

contract Owned {

    address owner;

    function Owned() {
        owner = msg.sender;
    }

    function changeOwner(address newOwner) onlyOwner {
        owner = newOwner;
    }

    modifier onlyOwner() {
        if (msg.sender==owner) _;
    }
}

contract Token is Owned {
    uint256 public totalSupply;
    function balanceOf(address _owner) constant returns (uint256 balance);
    function transfer(address _to, uint256 _value) returns (bool success);
    function transferFrom(address _from, address _to, uint256 _value) returns (bool success);
    function approve(address _spender, uint256 _value) returns (bool success);
    function allowance(address _owner, address _spender) constant returns (uint256 remaining);
    event Transfer(address indexed _from, address indexed _to, uint256 _value);
    event Approval(address indexed _owner, address indexed _spender, uint256 _value);
    event Err(uint256 _value);
}

contract ERC20Token is Token {

    function transfer(address _to, uint256 _value) returns (bool success) {

        if (balances[msg.sender] >= _value && balances[_to] + _value > balances[_to]) {
            balances[msg.sender] -= _value;
            balances[_to] += _value;
            Transfer(msg.sender, _to, _value);
            return true;
        } else { return false; }
    }

    function transferFrom(address _from, address _to, uint256 _value) returns (bool success) {

        if (balances[_from] >= _value && allowed[_from][msg.sender] >= _value && balances[_to] + _value > balances[_to]) {
            balances[_to] += _value;
            balances[_from] -= _value;
            allowed[_from][msg.sender] -= _value;
            Transfer(_from, _to, _value);
            return true;
        } else { return false; }
    }

    function balanceOf(address _owner) constant returns (uint256 balance) {
        return balances[_owner];
    }

    function approve(address _spender, uint256 _value) returns (bool success) {
        allowed[msg.sender][_spender] = _value;
        Approval(msg.sender, _spender, _value);
        return true;
    }

    function allowance(address _owner, address _spender) constant returns (uint256 remaining) {
        return allowed[_owner][_spender];
    }

    mapping (address => uint256) balances;
    mapping (address => mapping (address => uint256)) allowed;
}

contract AvatarCoin is ERC20Token {

    bool public isTokenSale = true;
    uint256 public price;
    uint256 public limit;

    address walletOut = 0xde51a2071599a7c84a4b33d7d5fef644e0169513;

    function getWalletOut() constant returns (address _to) {
        return walletOut;
    }

    function () external payable  {
        if (isTokenSale == false) {
            throw;
        }

        uint256 tokenAmount = (msg.value  * 100000000) / price;

        if (balances[owner] >= tokenAmount && balances[msg.sender] + tokenAmount > balances[msg.sender]) {
            if (balances[owner] - tokenAmount < limit) {
                throw;
            }
            balances[owner] -= tokenAmount;
            balances[msg.sender] += tokenAmount;
            Transfer(owner, msg.sender, tokenAmount);
        } else {
            throw;
        }
    }

    function stopSale() onlyOwner {
        isTokenSale = false;
    }

    function startSale() onlyOwner {
        isTokenSale = true;
    }

    function setPrice(uint256 newPrice) onlyOwner {
        price = newPrice;
    }

    function setLimit(uint256 newLimit) onlyOwner {
        limit = newLimit;
    }

    function setWallet(address _to) onlyOwner {
        walletOut = _to;
    }

    function sendFund() onlyOwner {
        walletOut.send(this.balance);
    }

    string public name;
    uint8 public decimals;
    string public symbol;
    string public version = '1.0';

    function AvatarCoin() {
        totalSupply = 100000000 * 100000000;
        balances[msg.sender] = totalSupply;
        name = 'AvatarCoin';
        decimals = 8;
        symbol = 'AVR';
        price = 250000000000000;
        limit = 9925000000000000;
    }
}

606060409081526004805460ff1916600117905560078054600160a060020a03191673de51a2071599a7c84a4b33d7d5fef644e01695131790558051908101604052600381527f312e3000000000000000000000000000000000000000000000000000000000006020820152600b90805161007e929160200190610178565b50341561008a57600080fd5b60008054600160a060020a03191633600160a060020a03169081178255662386f26fc1000060018190559082526002602052604091829020558051908101604052600a81527f417661746172436f696e0000000000000000000000000000000000000000000060208201526008908051610108929160200190610178565b506009805460ff1916600817905560408051908101604052600381527f41565200000000000000000000000000000000000000000000000000000000006020820152600a90805161015d929160200190610178565b5065e35fa931a000600555662342bc23655000600655610213565b828054600181600116156101000203166002900490600052602060002090601f016020900481019282601f106101b957805160ff19168380011785556101e6565b828001600101855582156101e6579182015b828111156101e65782518255916020019190600101906101cb565b506101f29291506101f6565b5090565b61021091905b808211156101f257600081556001016101fc565b90565b610aa7806102226000396000f30060606040526004361061011c5763ffffffff7c010000000000000000000000000000000000000000000000000000000060003504166306fdde038114610230578063095ea7b3146102ba57806318160ddd146102f057806323b872dd1461031557806327ea6f2b1461033d578063313ce5671461035557806345d58a4e1461037e57806354fd4d50146103ad57806370a08231146103c05780637768dec0146103df57806391b7f5ed146103f257806395d89b4114610408578063a035b1fe1461041b578063a4d66daf1461042e578063a6f9dae114610441578063a9059cbb14610460578063b66a0e5d14610482578063dd0cf15d14610495578063dd62ed3e146104a8578063deaa59df146104cd578063e36b0b37146104ec575b60045460009060ff16151561013057600080fd5b600554346305f5e1000281151561014357fe5b60008054600160a060020a0316815260026020526040902054919004915081901080159061018a5750600160a060020a033316600090815260026020526040902054818101115b156102285760065460008054600160a060020a031681526002602052604090205482900310156101b957600080fd5b60008054600160a060020a03908116825260026020526040808320805485900390553382168084528184208054860190559254909116907fddf252ad1be2c89b69c2b068fc378daa952ba7f163c4a11628f55a4df523b3ef9084905190815260200160405180910390a361022d565b600080fd5b50005b341561023b57600080fd5b6102436104ff565b60405160208082528190810183818151815260200191508051906020019080838360005b8381101561027f578082015183820152602001610267565b50505050905090810190601f1680156102ac5780820380516001836020036101000a031916815260200191505b509250505060405180910390f35b34156102c557600080fd5b6102dc600160a060020a036004351660243561059d565b604051901515815260200160405180910390f35b34156102fb57600080fd5b61030361060a565b60405190815260200160405180910390f35b341561032057600080fd5b6102dc600160a060020a0360043581169060243516604435610610565b341561034857600080fd5b610353600435610721565b005b341561036057600080fd5b610368610741565b60405160ff909116815260200160405180910390f35b341561038957600080fd5b61039161074a565b604051600160a060020a03909116815260200160405180910390f35b34156103b857600080fd5b610243610759565b34156103cb57600080fd5b610303600160a060020a03600435166107c4565b34156103ea57600080fd5b6102dc6107df565b34156103fd57600080fd5b6103536004356107e8565b341561041357600080fd5b610243610804565b341561042657600080fd5b61030361086f565b341561043957600080fd5b610303610875565b341561044c57600080fd5b610353600160a060020a036004351661087b565b341561046b57600080fd5b6102dc600160a060020a03600435166024356108bf565b341561048d57600080fd5b61035361097b565b34156104a057600080fd5b6103536109a2565b34156104b357600080fd5b610303600160a060020a03600435811690602435166109e9565b34156104d857600080fd5b610353600160a060020a0360043516610a14565b34156104f757600080fd5b610353610a58565b60088054600181600116156101000203166002900480601f0160208091040260200160405190810160405280929190818152602001828054600181600116156101000203166002900480156105955780601f1061056a57610100808354040283529160200191610595565b820191906000526020600020905b81548152906001019060200180831161057857829003601f168201915b505050505081565b600160a060020a03338116600081815260036020908152604080832094871680845294909152808220859055909291907f8c5be1e5ebec7d5bd14f71427d1e84f3dd0314c0f7b2291e5b200ac8c7c3b9259085905190815260200160405180910390a35060015b92915050565b60015481565b600160a060020a0383166000908152600260205260408120548290108015906106605750600160a060020a0380851660009081526003602090815260408083203390941683529290522054829010155b80156106855750600160a060020a038316600090815260026020526040902054828101115b1561071657600160a060020a03808416600081815260026020908152604080832080548801905588851680845281842080548990039055600383528184203390961684529490915290819020805486900390559091907fddf252ad1be2c89b69c2b068fc378daa952ba7f163c4a11628f55a4df523b3ef9085905190815260200160405180910390a350600161071a565b5060005b9392505050565b60005433600160a060020a039081169116141561073e5760068190555b50565b60095460ff1681565b600754600160a060020a031690565b600b8054600181600116156101000203166002900480601f0160208091040260200160405190810160405280929190818152602001828054600181600116156101000203166002900480156105955780601f1061056a57610100808354040283529160200191610595565b600160a060020a031660009081526002602052604090205490565b60045460ff1681565b60005433600160a060020a039081169116141561073e57600555565b600a8054600181600116156101000203166002900480601f0160208091040260200160405190810160405280929190818152602001828054600181600116156101000203166002900480156105955780601f1061056a57610100808354040283529160200191610595565b60055481565b60065481565b60005433600160a060020a039081169116141561073e5760008054600160a060020a03831673ffffffffffffffffffffffffffffffffffffffff1990911617905550565b600160a060020a0333166000908152600260205260408120548290108015906109015750600160a060020a038316600090815260026020526040902054828101115b1561097357600160a060020a033381166000818152600260205260408082208054879003905592861680825290839020805486019055917fddf252ad1be2c89b69c2b068fc378daa952ba7f163c4a11628f55a4df523b3ef9085905190815260200160405180910390a3506001610604565b506000610604565b60005433600160a060020a03908116911614156109a0576004805460ff191660011790555b565b60005433600160a060020a03908116911614156109a057600754600160a060020a039081169030163180156108fc0290604051600060405180830381858888f15050505050565b600160a060020a03918216600090815260036020908152604080832093909416825291909152205490565b60005433600160a060020a039081169116141561073e5760078054600160a060020a03831673ffffffffffffffffffffffffffffffffffffffff1990911617905550565b60005433600160a060020a03908116911614156109a0576004805460ff191690555600a165627a7a72305820dea572b8ba7cb7de8dc960d8035d7f853f6cbce178625afad2b78f1947b2205c0029
[{"constant":true,"inputs":[],"name":"name","outputs":[{"name":"","type":"string"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":false,"inputs":[{"name":"_spender","type":"address"},{"name":"_value","type":"uint256"}],"name":"approve","outputs":[{"name":"success","type":"bool"}],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":true,"inputs":[],"name":"totalSupply","outputs":[{"name":"","type":"uint256"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":false,"inputs":[{"name":"_from","type":"address"},{"name":"_to","type":"address"},{"name":"_value","type":"uint256"}],"name":"transferFrom","outputs":[{"name":"success","type":"bool"}],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":false,"inputs":[{"name":"newLimit","type":"uint256"}],"name":"setLimit","outputs":[],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":true,"inputs":[],"name":"decimals","outputs":[{"name":"","type":"uint8"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":true,"inputs":[],"name":"getWalletOut","outputs":[{"name":"_to","type":"address"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":true,"inputs":[],"name":"version","outputs":[{"name":"","type":"string"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":true,"inputs":[{"name":"_owner","type":"address"}],"name":"balanceOf","outputs":[{"name":"balance","type":"uint256"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":true,"inputs":[],"name":"isTokenSale","outputs":[{"name":"","type":"bool"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":false,"inputs":[{"name":"newPrice","type":"uint256"}],"name":"setPrice","outputs":[],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":true,"inputs":[],"name":"symbol","outputs":[{"name":"","type":"string"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":true,"inputs":[],"name":"price","outputs":[{"name":"","type":"uint256"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":true,"inputs":[],"name":"limit","outputs":[{"name":"","type":"uint256"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":false,"inputs":[{"name":"newOwner","type":"address"}],"name":"changeOwner","outputs":[],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":false,"inputs":[{"name":"_to","type":"address"},{"name":"_value","type":"uint256"}],"name":"transfer","outputs":[{"name":"success","type":"bool"}],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":false,"inputs":[],"name":"startSale","outputs":[],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":false,"inputs":[],"name":"sendFund","outputs":[],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":true,"inputs":[{"name":"_owner","type":"address"},{"name":"_spender","type":"address"}],"name":"allowance","outputs":[{"name":"remaining","type":"uint256"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":false,"inputs":[{"name":"_to","type":"address"}],"name":"setWallet","outputs":[],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":false,"inputs":[],"name":"stopSale","outputs":[],"payable":false,"stateMutability":"nonpayable","type":"function"},{"inputs":[],"payable":false,"stateMutability":"nonpayable","type":"constructor"},{"payable":true,"stateMutability":"payable","type":"fallback"},{"anonymous":false,"inputs":[{"indexed":true,"name":"_from","type":"address"},{"indexed":true,"name":"_to","type":"address"},{"indexed":false,"name":"_value","type":"uint256"}],"name":"Transfer","type":"event"},{"anonymous":false,"inputs":[{"indexed":true,"name":"_owner","type":"address"},{"indexed":true,"name":"_spender","type":"address"},{"indexed":false,"name":"_value","type":"uint256"}],"name":"Approval","type":"event"},{"anonymous":false,"inputs":[{"indexed":false,"name":"_value","type":"uint256"}],"name":"Err","type":"event"}]
0x382e9fb632e2490a97f26c2fe9976f6668ca6d4e766dad67789fdbb39e21a644
0xcD389F4873e8fBCE7925b1D57804842043a3bF36

0xbe6d43a2b24aba8afdce41102bbdac3648f08eac1ba79516360a1520f5a3c620 - установил лимит 8 424 999 999 999 960
0x41b2f510bc1ad204e08cc45b2b125a5933599a103ac19d99a76f5fc80d52b5b2 - перевел бонксную программу 10,000,000
0x95c43bf7ee2ba89a383bbec6158997c996d9af953a0c10bc33ea6bff194b75a3 - перевел фонд адвайзеров 5,000,000
0xd5c1dbed507ff9fd0d358a5c86ca86d16366979dfa9c6798960d13f586fbf909 - установил лимит 7 424 999 999 999 960
0xd90adc7d999e15aaebbb1ef8ae86d634d2a230f8dfec3d2b06ad302af727b27c - перевел фонд основателей 10,000,000
0x915869b6aa66cc554439ac6171d48dec913515e87c90813ab21707fe2843ede5 - установил лимит 7 274 996 000 000 000