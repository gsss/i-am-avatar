<?php
use common\services\UsersInCache;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;

/* @var $this yii\web\View */

$this->title = 'Регистрация';

?>
<div class="container">
    <div class="col-lg-12">
        <h1 class="page-header text-center"><?= Html::encode($this->title) ?></h1>

        <p>Версия 1.2</p>
        <p>Предназначен для регистрации Аватаров в нашей Державе Света "Новая Земля".</p>
        <p>В контракте существует функции:</p>
        <ul>
            <li><code>addMember</code> - добавить зарегистрировать участника</li>
            <li><code>getMemberCount</code> - Функция получения количества юзеров</li>
            <li><code>getMember</code> - Функция получения юзера по id
            возвращает массив из полей [имя, фамилия, отчество, дата_рождения, хэш аватара, id аватара]</li>
        </ul>
        <p>Можно оставить зашифрованные данные, или открытые</p>
        <p>Если зашифрованные, то значит <code>namef</code>, <code>namel</code>, <code>link</code> пустые, в data записано JSON </code>{"cryptData":"sdfe/sdf..."}</code></p>
        <p>Если открытые данные, то значит <code>namef</code>, <code>namel</code>, <code>link</code> заполнены, в data записано JSON </code>{"cryptData":"sdfe/sdf..."}</code></p>

        <h2 class="page-header">Алгоритм подтверждения личности по блокчейн Avatar.Passport</h2>

        <p>Данные в контракт размещаются зашифрованные по паролю, который знает владелец паспорта, таким образом во вне никто эти данные посмотреть не может и сохраняется приватность.</p>
        <p>Для наглядности возьмем ситуацию, когда Алиса имеет Avatar.Passport в системе и ей нужно подтвердить свою личность перед Бобом.</p>
        <p>Например: Алиса разместила в блокчейне данные своего ДНК профиля для подтверждения личности и своего анонимного адреса.</p>
        <p>Если нужно подтвердить Бобу что это кошелек Алисы, то Алиса дает Бобу свой ДНК материал лично при нем. Боб делает независимую экспертизу ДНК анализов Алисы. После этого они встречаются с Алисой. Алиса при Бобе расшифровывает по своему личному паролю зашифрованные данные и показывает свой профиль ДНК. Боб сверяет свою независимую экспертизу с данными которые расшифровались и при полном совпадении профиля Боб поймет что ее кошелек действительно ее.</p>


        <h2 class="page-header">Схематическое пояснение связки юрисдикций</h2>
        <p>Связь паспорта с регистрацией и с подписью документов.</p>
        <p><img src="/images/controller/development-contracts/registration/dna.png" width="100%"/></p>
        <p><a href="https://drive.google.com/file/d/0BzHYNoEyPNTXcU43a2Z1QnV2V00/view?usp=sharing
" target="_blank">Документ</a></p>


        <pre>
struct Member {
    address member;
    string name;
    string surname;
    string patronymic;
    uint birthDate;
    string birthPlace;
    string avatarHash;
    uint avatarID;
    bool approved;
    uint memberSince;
}
        </pre>


        <p><b>Обновление данных</b></p>
        <p>Если человеку нужно обновить данные о себе то он их переписывает более расширенной информацией.</p>

        <h2 class="page-header">Функции</h2>
        <h3 class="page-header">addMember</h3>
        <p>функция добавления и обновления участника, параметры - адрес, имя, фамилия,
            отчество, дата рождения (linux time), место рождения, хэш аватара, ID аватара, дополнительные данные в формате JSON
            если пользователь с таким адресом не найден, то будет создан новый, в конце вызовется событие
            MemberAdded, если пользователь найден, то будет произведено обновление полей и проставлен флаг
            подтверждения approved
        </p>
        <h2 class="page-header">События</h2>
        <p>MemberAdded</p>
        <p>MemberChanged</p>
        <h2 class="page-header">Что такое AvatarID и AvatarHash</h2>
        <p>Это идентификатор картинки-аватара и его хеш по базе данных https://www.avatar-bank.com/data-base/avatar.
            По мимо картинки можно сохранить дополнительные данные.
            Hash вычисляется по алгоритму SHA256 для содержимого картинки и дополнительных данных (актуально для ID = 8 и выше, для ID = 7 и наже актуален старый алгоритм SHA1)
        </p>
        <pre>
$hash = hash('sha256', $imageContent . $dataContent);
</pre>
        <h2 class="page-header">Интерфейс</h2>




        <pre>
[{"constant":true,"inputs":[{"name":"","type":"uint256"}],"name":"memberData","outputs":[{"name":"","type":"string"}],"payable":false,"type":"function"},{"constant":true,"inputs":[{"name":"","type":"address"}],"name":"memberId","outputs":[{"name":"","type":"uint256"}],"payable":false,"type":"function"},{"constant":false,"inputs":[],"name":"kill","outputs":[],"payable":false,"type":"function"},{"constant":true,"inputs":[{"name":"","type":"uint256"}],"name":"members","outputs":[{"name":"member","type":"address"},{"name":"name","type":"string"},{"name":"surname","type":"string"},{"name":"patronymic","type":"string"},{"name":"birthDate","type":"uint256"},{"name":"birthPlace","type":"string"},{"name":"avatarHash","type":"string"},{"name":"avatarID","type":"uint256"},{"name":"approved","type":"bool"},{"name":"memberSince","type":"uint256"}],"payable":false,"type":"function"},{"constant":true,"inputs":[],"name":"getMemberCount","outputs":[{"name":"","type":"uint256"}],"payable":false,"type":"function"},{"constant":false,"inputs":[{"name":"newOwner","type":"address"}],"name":"changeOwner","outputs":[],"payable":false,"type":"function"},{"constant":true,"inputs":[{"name":"id","type":"uint256"}],"name":"getMember","outputs":[{"name":"name","type":"string"},{"name":"surname","type":"string"},{"name":"patronymic","type":"string"},{"name":"birthDate","type":"uint256"},{"name":"birthPlace","type":"string"},{"name":"avatarHash","type":"string"},{"name":"avatarID","type":"uint256"},{"name":"data","type":"string"}],"payable":false,"type":"function"},{"constant":true,"inputs":[{"name":"","type":"uint256"}],"name":"pks","outputs":[{"name":"","type":"string"}],"payable":false,"type":"function"},{"constant":true,"inputs":[{"name":"id","type":"uint256"}],"name":"getPK","outputs":[{"name":"","type":"string"}],"payable":false,"type":"function"},{"constant":false,"inputs":[{"name":"name","type":"string"},{"name":"surname","type":"string"},{"name":"patronymic","type":"string"},{"name":"birthDate","type":"uint256"},{"name":"birthPlace","type":"string"},{"name":"avatarHash","type":"string"},{"name":"avatarID","type":"uint256"},{"name":"data","type":"string"}],"name":"addMember","outputs":[],"payable":false,"type":"function"},{"inputs":[],"payable":false,"type":"constructor"},{"anonymous":false,"inputs":[{"indexed":false,"name":"member","type":"address"},{"indexed":false,"name":"id","type":"uint256"}],"name":"MemberAdded","type":"event"},{"anonymous":false,"inputs":[{"indexed":false,"name":"member","type":"address"},{"indexed":false,"name":"id","type":"uint256"}],"name":"MemberChanged","type":"event"}]
</pre>

        <p><b>TestNet</b></p>
        <p><code>0x5d1f0541cbaa6f2ca7f50674d4d112beeea1e87c</code></p>

        <p><b>LiveNet</b></p>
        <p><code><a href="https://etherscan.io/address/0xf180e0f0b8ae56f01427817c2627f1fc75202929" target="_blank">0xf180E0F0b8ae56F01427817C2627F1fc75202929</a></code></p>
        <p>Код:</p>
<pre>
pragma solidity ^0.4.2;
/*
AvatarNetwork Copyright
*/


/* Контракт регистрации пользователей 1.2 */

/* Родительский контракт */
contract Owned {

    /* Адрес владельца контракта*/
    address owner;

    /* Конструктор контракта, вызывается при первом запуске */
    function Owned() {
        owner = msg.sender;
    }

    /* Изменить владельца контракта, newOwner - адрес нового владельца */
    function changeOwner(address newOwner) onlyowner {
        owner = newOwner;
    }

    /* Модификатор для ограничения доступа к функциям только для владельца */
    modifier onlyowner() {
        if (msg.sender==owner) _;
    }

    /* Удалить контракт */
    function kill() onlyowner {
        if (msg.sender == owner) suicide(owner);
    }
}

/* Основной контракт, наследует контракт Owned */
contract Documents is Owned {

    /* Структура представляющая документ */
    struct Document {
        string hash;
        string namef;
        string namel;
        string link;
        string data;
        address creator;
        uint date;
        uint signsCount;
        mapping (uint => Sign) signs;
    }

    /* Структура представляющая подпись */
    struct Sign {
        address member;
        uint date;
    }

    /* Маппинг ID документа -> документ */
    mapping (uint => Document) public documentsIds;

    /* Кол-во документов */
    uint documentsCount = 0;

    /* Событие при подписи документа участником, параметры - адрес участника, ID документа */
    event DocumentSigned(uint id, address member);

    /* Событие при регистрации документа, параметры - ID документа */
    event DocumentRegistered(uint id, string hash);

     /* Конструктор контракта, вызывается при первом запуске */
    function Documents() {
    }

    /* функция добавления документа, параметры - хэш, ссылка, дополнительные данные, создатель.
    Если не передаётся адрес создателя, то будет указан адрес отправителя, в конце вызовется событие DocumentRegistered
    с параметрами id - документа (позиция в массиве documents) и hash - хэш сумма */
    function registerDocument(string hash,
                       string namef,
                       string namel,
                       string link,
                       string data) {
        address creator = msg.sender;

        uint id = documentsCount + 1;
        documentsIds[id] = Document({
           hash: hash,
           namef: namef,
           namel: namel,
           link: link,
           data: data,
           creator: creator,
           date: now,
           signsCount: 0
        });
        documentsCount = id;
        DocumentRegistered(id, hash);
    }

    /* функция добавления подписи в документ, параметры - ID Документа, адрес подписчика.
    Адрес подписчика используется тот кто вызывает контракт.
    Если подпись уже есть то она не будет добавлена. То есть подписать можно один раз.
    в конце вызовется событие DocumentSigned */
    function addSignature(uint id) {
        address member = msg.sender;

        Document d = documentsIds[id];
        uint count = d.signsCount;
        bool signed = false;
        if (count != 0) {
            for (uint i = 0; i < count; i++) {
                if (d.signs[i].member == member) {
                    signed = true;
                    break;
                }
            }
        }

        if (!signed) {
            d.signs[count] = Sign({
                    member: member,
                    date: now
                });
            documentsIds[id].signsCount = count + 1;
            DocumentSigned(id, member);
        }
    }

    /* Функция получения количества документов */
    function getDocumentsCount() constant returns (uint) {
        return documentsCount;
    }

    /* Функция получения документа по ID */
    function getDocument(uint id) constant returns (string hash,
                       string namef,
                       string namel,
                       string link,
                       string data,
                       address creator,
                       uint date,
                       uint count) {
        Document d = documentsIds[id];
        hash = d.hash;
        namef = d.namef;
        namel = d.namel;
        link = d.link;
        data = d.data;
        creator = d.creator;
        date = d.date;
    }

    /* Функция получения количества подписей по ID документа */
    function getDocumentSignsCount(uint id) constant returns (uint) {
        Document d = documentsIds[id];
        return d.signsCount;
    }

    /* Функция получения подписи документов, параметры - ID документа, позиция подписи в массиве */
    function getDocumentSign(uint id, uint index) constant returns (
                        address member,
                        uint date) {
        Document d = documentsIds[id];
        Sign s = d.signs[index];
        member = s.member;
        date = s.date;
	}
}</pre>

    </div>
</div>



