pragma solidity ^0.4.8;

/*
AvatarNetwork Copyright

https://avatarnetwork.io
*/

/* Родительский контракт */
contract Owned {

    /* Адрес владельца контракта*/
    address owner;

    /* Конструктор контракта, вызывается при первом запуске */
    function Owned() {
        owner = msg.sender;
    }

    /* Изменить владельца контракта, newOwner - адрес нового владельца */
    function changeOwner(address newOwner) onlyOwner {
        owner = newOwner;
    }

    /* Модификатор для ограничения доступа к функциям только для владельца */
    modifier onlyOwner() {
        if (msg.sender==owner) _;
    }

}

// Абстрактный контракт для токена стандарта ERC 20
// https://github.com/ethereum/EIPs/issues/20
contract Token is Owned {

    /// Общее кол-во токенов
    uint256 public totalSupply;

    /// @param _owner адрес, с которого будет получен баланс
    /// @return Баланс
    function balanceOf(address _owner) constant returns (uint256 balance);

    /// @notice Отправить кол-во `_value` токенов на адрес `_to` с адреса `msg.sender`
    /// @param _to Адрес получателя
    /// @param _value Кол-во токенов для отправки
    /// @return Была ли отправка успешной или нет
    function transfer(address _to, uint256 _value) returns (bool success);

    /// @notice Отправить кол-во `_value` токенов на адрес `_to` с адреса `_from` при условии что это подтверждено отправителем `_from`
    /// @param _from Адрес отправителя
    /// @param _to The address of the recipient
    /// @param _value The amount of token to be transferred
    /// @return Whether the transfer was successful or not
    function transferFrom(address _from, address _to, uint256 _value) returns (bool success);

    /// @notice Вызывающий функции `msg.sender` подтверждает что с адреса `_spender` спишется `_value` токенов
    /// @param _spender Адрес аккаунта, с которого возможно списать токены
    /// @param _value Кол-во токенов к подтверждению для отправки
    /// @return Было ли подтверждение успешным или нет
    function approve(address _spender, uint256 _value) returns (bool success);

    /// @param _owner Адрес аккаунта владеющего токенами
    /// @param _spender Адрес аккаунта, с которого возможно списать токены
    /// @return Кол-во оставшихся токенов разрешённых для отправки
    function allowance(address _owner, address _spender) constant returns (uint256 remaining);

    event Transfer(address indexed _from, address indexed _to, uint256 _value);
    event Approval(address indexed _owner, address indexed _spender, uint256 _value);
    event Err(uint256 _value);
}

/*
Контракт реализует ERC 20 Token standard: https://github.com/ethereum/EIPs/issues/20
*/
contract ERC20Token is Token {

    function transfer(address _to, uint256 _value) returns (bool success) {
        // По-умолчанию предполагается, что totalSupply не может быть больше (2^256 - 1).

        if (balances[msg.sender] >= _value && balances[_to] + _value > balances[_to]) {
            balances[msg.sender] -= _value;
            balances[_to] += _value;
            Transfer(msg.sender, _to, _value);
            return true;
        } else { return false; }
    }

    function transferFrom(address _from, address _to, uint256 _value) returns (bool success) {
        // По-умолчанию предполагается, что totalSupply не может быть больше (2^256 - 1).
        if (balances[_from] >= _value && allowed[_from][msg.sender] >= _value && balances[_to] + _value > balances[_to]) {
            balances[_to] += _value;
            balances[_from] -= _value;
            allowed[_from][msg.sender] -= _value;
            Transfer(_from, _to, _value);
            return true;
        } else { return false; }
    }

    function balanceOf(address _owner) constant returns (uint256 balance) {
        return balances[_owner];
    }

    function approve(address _spender, uint256 _value) returns (bool success) {
        allowed[msg.sender][_spender] = _value;
        Approval(msg.sender, _spender, _value);
        return true;
    }

    function allowance(address _owner, address _spender) constant returns (uint256 remaining) {
        return allowed[_owner][_spender];
    }

    mapping (address => uint256) balances;
    mapping (address => mapping (address => uint256)) allowed;
}

/* Основной контракт токена, наследует ERC20Token */
contract AvatarCoin is ERC20Token {

    bool public isTokenSale = true;      // флаг отвечает на вопрос, можно ли купить токен отправив эфир на контракт. false - продажа не возможна, true - продажа возможна
    uint256 public price;                // цена токена в wei
    uint256 public limit;                // кол-во токенов которое должно остаться на кошельке хозяина на этапе продаж, см функцию "function ()"

    address walletOut = 0xde51a2071599a7c84a4b33d7d5fef644e0169513;

    function getWalletOut() constant returns (address _to) {
        return walletOut;
    }

    /* Отправляет токены за присланные эфиры в соответствии с ценой price */
    function () external payable  {
        if (isTokenSale == false) {
            throw;
        }

        uint256 tokenAmount = (msg.value  * 100000000) / price;

        if (balances[owner] >= tokenAmount && balances[msg.sender] + tokenAmount > balances[msg.sender]) {
            if (balances[owner] - tokenAmount < limit) {
                throw;
            }
            balances[owner] -= tokenAmount;
            balances[msg.sender] += tokenAmount;
            Transfer(owner, msg.sender, tokenAmount);
        } else {
            throw;
        }
    }

    /* останавливает продажу токена */
    function stopSale() onlyOwner {
        isTokenSale = false;
    }

    /* запускает продажу токена */
    function startSale() onlyOwner {
        isTokenSale = true;
    }

    /* Устанавливает цену для продажи */
    function setPrice(uint256 newPrice) onlyOwner {
        price = newPrice;
    }

    /* Устанавливает лимит для продажи, см описание переменной */
    function setLimit(uint256 newLimit) onlyOwner {
        limit = newLimit;
    }

    /* отправляет собранные эфиры на address */
    function setWallet(address _to) onlyOwner {
        walletOut = _to;
    }

    /* отправляет собранные эфиры на address */
    function sendFund() onlyOwner {
        walletOut.send(this.balance);
    }

    /* Публичные переменные токена */
    string public name;                 // Название
    uint8 public decimals;              // Сколько десятичных знаков отображать.
    string public symbol;               // Идентификатор
    string public version = '1.0';      // Версия

    /* Конструктор */
    function AvatarCoin() {
        totalSupply = 100000000 * 100000000;
        balances[msg.sender] = totalSupply;  // Передача создателю всех выпущенных монет
        name = 'AvatarCoin';
        decimals = 8;
        symbol = 'AVR';
        price = 250000000000000; // 1 AVR = 0.1 USD
        limit = 9925000000000000;  // без лимита
    }
}



9925000000000000
606060409081526004805460ff1916600117905560078054600160a060020a031916730d2495e9d5b890958be4901f7bdced37fe91b8621790558051908101604052600381527f312e3000000000000000000000000000000000000000000000000000000000006020820152600b90805161007e929160200190610174565b50341561008a57600080fd5b60008054600160a060020a03191633600160a060020a03169081178255662386f26fc1000060018190559082526002602052604091829020558051908101604052600a81527f417661746172436f696e0000000000000000000000000000000000000000000060208201526008908051610108929160200190610174565b506009805460ff1916600817905560408051908101604052600381527f41565200000000000000000000000000000000000000000000000000000000006020820152600a90805161015d929160200190610174565b50670de0b6b3a7640000600555600060065561020f565b828054600181600116156101000203166002900490600052602060002090601f016020900481019282601f106101b557805160ff19168380011785556101e2565b828001600101855582156101e2579182015b828111156101e25782518255916020019190600101906101c7565b506101ee9291506101f2565b5090565b61020c91905b808211156101ee57600081556001016101f8565b90565b610bd18061021e6000396000f3006060604052600436106101325763ffffffff7c010000000000000000000000000000000000000000000000000000000060003504166306fdde03811461027a578063095ea7b3146103045780631003e2d21461033a57806318160ddd1461035057806323b872dd1461037557806327ea6f2b1461039d578063313ce567146103b557806342966c68146103de57806345d58a4e146103f457806354fd4d501461042357806370a08231146104365780637768dec01461045557806391b7f5ed1461046857806395d89b411461047e578063a035b1fe14610491578063a4d66daf146104a4578063a6f9dae1146104b7578063a9059cbb146104d6578063b66a0e5d146104f8578063dd0cf15d1461050b578063dd62ed3e1461051e578063deaa59df14610543578063e36b0b3714610562575b60045460009060ff16151561014657600080fd5b600554346305f5e1000281151561015957fe5b60008054600160a060020a031681526002602052604090205491900491508190108015906101a05750600160a060020a033316600090815260026020526040902054818101115b156102725760065460008054600160a060020a03168152600260205260409020548290031015610203577f39eabce61287198fda55b2dd4ffd836301fe4f68152857110cb8ec2b7f60c2c6600160405190815260200160405180910390a1600080fd5b60008054600160a060020a03908116825260026020526040808320805485900390553382168084528184208054860190559254909116907fddf252ad1be2c89b69c2b068fc378daa952ba7f163c4a11628f55a4df523b3ef9084905190815260200160405180910390a3610277565b600080fd5b50005b341561028557600080fd5b61028d610575565b60405160208082528190810183818151815260200191508051906020019080838360005b838110156102c95780820151838201526020016102b1565b50505050905090810190601f1680156102f65780820380516001836020036101000a031916815260200191505b509250505060405180910390f35b341561030f57600080fd5b610326600160a060020a0360043516602435610613565b604051901515815260200160405180910390f35b341561034557600080fd5b610326600435610680565b341561035b57600080fd5b6103636106c5565b60405190815260200160405180910390f35b341561038057600080fd5b610326600160a060020a03600435811690602435166044356106cb565b34156103a857600080fd5b6103b36004356107dc565b005b34156103c057600080fd5b6103c86107fc565b60405160ff909116815260200160405180910390f35b34156103e957600080fd5b610326600435610805565b34156103ff57600080fd5b610407610874565b604051600160a060020a03909116815260200160405180910390f35b341561042e57600080fd5b61028d610883565b341561044157600080fd5b610363600160a060020a03600435166108ee565b341561046057600080fd5b610326610909565b341561047357600080fd5b6103b3600435610912565b341561048957600080fd5b61028d61092e565b341561049c57600080fd5b610363610999565b34156104af57600080fd5b61036361099f565b34156104c257600080fd5b6103b3600160a060020a03600435166109a5565b34156104e157600080fd5b610326600160a060020a03600435166024356109e9565b341561050357600080fd5b6103b3610aa5565b341561051657600080fd5b6103b3610acc565b341561052957600080fd5b610363600160a060020a0360043581169060243516610b13565b341561054e57600080fd5b6103b3600160a060020a0360043516610b3e565b341561056d57600080fd5b6103b3610b82565b60088054600181600116156101000203166002900480601f01602080910402602001604051908101604052809291908181526020018280546001816001161561010002031660029004801561060b5780601f106105e05761010080835404028352916020019161060b565b820191906000526020600020905b8154815290600101906020018083116105ee57829003601f168201915b505050505081565b600160a060020a03338116600081815260036020908152604080832094871680845294909152808220859055909291907f8c5be1e5ebec7d5bd14f71427d1e84f3dd0314c0f7b2291e5b200ac8c7c3b9259085905190815260200160405180910390a35060015b92915050565b6000805433600160a060020a03908116911614156106c057506001805482018155600160a060020a03331660009081526002602052604090208054830190555b919050565b60015481565b600160a060020a03831660009081526002602052604081205482901080159061071b5750600160a060020a0380851660009081526003602090815260408083203390941683529290522054829010155b80156107405750600160a060020a038316600090815260026020526040902054828101115b156107d157600160a060020a03808416600081815260026020908152604080832080548801905588851680845281842080548990039055600383528184203390961684529490915290819020805486900390559091907fddf252ad1be2c89b69c2b068fc378daa952ba7f163c4a11628f55a4df523b3ef9085905190815260200160405180910390a35060016107d5565b5060005b9392505050565b60005433600160a060020a03908116911614156107f95760068190555b50565b60095460ff1681565b6000805433600160a060020a03908116911614156106c057600160a060020a03331660009081526002602052604090205482901015610846575060006106c0565b50600180548290038155600160a060020a033316600090815260026020526040902080548390039055919050565b600754600160a060020a031690565b600b8054600181600116156101000203166002900480601f01602080910402602001604051908101604052809291908181526020018280546001816001161561010002031660029004801561060b5780601f106105e05761010080835404028352916020019161060b565b600160a060020a031660009081526002602052604090205490565b60045460ff1681565b60005433600160a060020a03908116911614156107f957600555565b600a8054600181600116156101000203166002900480601f01602080910402602001604051908101604052809291908181526020018280546001816001161561010002031660029004801561060b5780601f106105e05761010080835404028352916020019161060b565b60055481565b60065481565b60005433600160a060020a03908116911614156107f95760008054600160a060020a03831673ffffffffffffffffffffffffffffffffffffffff1990911617905550565b600160a060020a033316600090815260026020526040812054829010801590610a2b5750600160a060020a038316600090815260026020526040902054828101115b15610a9d57600160a060020a033381166000818152600260205260408082208054879003905592861680825290839020805486019055917fddf252ad1be2c89b69c2b068fc378daa952ba7f163c4a11628f55a4df523b3ef9085905190815260200160405180910390a350600161067a565b50600061067a565b60005433600160a060020a0390811691161415610aca576004805460ff191660011790555b565b60005433600160a060020a0390811691161415610aca57600754600160a060020a039081169030163180156108fc0290604051600060405180830381858888f15050505050565b600160a060020a03918216600090815260036020908152604080832093909416825291909152205490565b60005433600160a060020a03908116911614156107f95760078054600160a060020a03831673ffffffffffffffffffffffffffffffffffffffff1990911617905550565b60005433600160a060020a0390811691161415610aca576004805460ff191690555600a165627a7a72305820a372258de014dbe23a3a54803b28a26aa48cb418a3ea12dbdbe8d2e3599461140029
[{"constant":true,"inputs":[],"name":"name","outputs":[{"name":"","type":"string"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":false,"inputs":[{"name":"_spender","type":"address"},{"name":"_value","type":"uint256"}],"name":"approve","outputs":[{"name":"success","type":"bool"}],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":false,"inputs":[{"name":"_value","type":"uint256"}],"name":"add","outputs":[{"name":"success","type":"bool"}],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":true,"inputs":[],"name":"totalSupply","outputs":[{"name":"","type":"uint256"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":false,"inputs":[{"name":"_from","type":"address"},{"name":"_to","type":"address"},{"name":"_value","type":"uint256"}],"name":"transferFrom","outputs":[{"name":"success","type":"bool"}],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":false,"inputs":[{"name":"newLimit","type":"uint256"}],"name":"setLimit","outputs":[],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":true,"inputs":[],"name":"decimals","outputs":[{"name":"","type":"uint8"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":false,"inputs":[{"name":"_value","type":"uint256"}],"name":"burn","outputs":[{"name":"success","type":"bool"}],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":true,"inputs":[],"name":"getWalletOut","outputs":[{"name":"_to","type":"address"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":true,"inputs":[],"name":"version","outputs":[{"name":"","type":"string"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":true,"inputs":[{"name":"_owner","type":"address"}],"name":"balanceOf","outputs":[{"name":"balance","type":"uint256"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":true,"inputs":[],"name":"isTokenSale","outputs":[{"name":"","type":"bool"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":false,"inputs":[{"name":"newPrice","type":"uint256"}],"name":"setPrice","outputs":[],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":true,"inputs":[],"name":"symbol","outputs":[{"name":"","type":"string"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":true,"inputs":[],"name":"price","outputs":[{"name":"","type":"uint256"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":true,"inputs":[],"name":"limit","outputs":[{"name":"","type":"uint256"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":false,"inputs":[{"name":"newOwner","type":"address"}],"name":"changeOwner","outputs":[],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":false,"inputs":[{"name":"_to","type":"address"},{"name":"_value","type":"uint256"}],"name":"transfer","outputs":[{"name":"success","type":"bool"}],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":false,"inputs":[],"name":"startSale","outputs":[],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":false,"inputs":[],"name":"sendFund","outputs":[],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":true,"inputs":[{"name":"_owner","type":"address"},{"name":"_spender","type":"address"}],"name":"allowance","outputs":[{"name":"remaining","type":"uint256"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":false,"inputs":[{"name":"_to","type":"address"}],"name":"setWallet","outputs":[],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":false,"inputs":[],"name":"stopSale","outputs":[],"payable":false,"stateMutability":"nonpayable","type":"function"},{"inputs":[],"payable":false,"stateMutability":"nonpayable","type":"constructor"},{"payable":true,"stateMutability":"payable","type":"fallback"},{"anonymous":false,"inputs":[{"indexed":true,"name":"_from","type":"address"},{"indexed":true,"name":"_to","type":"address"},{"indexed":false,"name":"_value","type":"uint256"}],"name":"Transfer","type":"event"},{"anonymous":false,"inputs":[{"indexed":true,"name":"_owner","type":"address"},{"indexed":true,"name":"_spender","type":"address"},{"indexed":false,"name":"_value","type":"uint256"}],"name":"Approval","type":"event"},{"anonymous":false,"inputs":[{"indexed":false,"name":"_value","type":"uint256"}],"name":"Err","type":"event"}]
0x42a4Aa5571e5128E2d7de251C12E93E5cb28016B
    0x3e5860b81fc279c507a6a33c17a2f1676b8efb8f2dceebf8d582c8db73ad466a - установил лимит 9925000000000000
    установить цену токена такую чтобы я за 0.075 эфира выкупил все 750000 токенов
    75000000000000000 wei
    75000000000000 AVR

    если tokenAmount = (msg.value  * 100000000) / price;
    то price = (msg.value  * 100000000) / tokenAmount = (75000000000000000 * 100000000) / 75000000000000 = 100000000000 wei/avr;
    проверка tokenAmount = (msg.value  * 100000000) / price = (75000000000000000 * 100000000) / 100000000000 =  75000000000000
    0x7506c1c71ba7bd91ffbbbb1a0df536a49c83fca09db2e092210cecc06c508ee3 - установил цену 100000000000
    0xcf6dd7a7f74d2add274465a655c56e4810a3f872e0cf8cdb1571ddd007fc0652 - отправл 0.075 с кошелька 0x6fa53d5bF26CCA57CB4e75e579e2b57EB0AF0C2E
    0xbf85d27d7de4e224a7e231d9012f81c73b3536149b5c4c5e142773ef6d770eea - попытался отправить 0.00000001
    10000000000 - хочу сжечь
    0x1c5ec75a6e54353c379433c9e0260647ad711a5e29292b1cbd3c3f7122cb0fec - сжигание 10000000000
    хочу добавить 100000000000
    0xc4d8df4c2b2086dc3b5ababf3d8c5622a46c388593797f7799b56304ac58512e - добавил 100000000000
    0xd77afd6cd68e5e40bd4a495bf8fd5d1d8ef258c81f50ca38182c24ae84484648 - установил кошелек на вывод 0x342fD4e18dFd89Bee6ACF6176E19A031e208A753
    0xd17de9bcd0a7a312554b6d62b2dc66a4844f7acc9f5050c6d439f0182759395c - вывел деньги с контракта
