<?php
/**
 * Created by PhpStorm.
 * User: god
 * Date: 13.04.2017
 * Time: 23:58
 */

use yii\helpers\Html;


/* @var $this yii\web\View */


$this->title = 'Франшиза';

?>
<div class="container">
    <div class="col-lg-12">
        <h1 class="page-header text-center">Контракт для выдачи разрешений продавать продукцию по франшизе</h1>

        <h2 class="page-header text-center">Синопсис</h2>

        <p>Есть автор методики обучающей - Максим Сергеевич Кравцов</p>
        <p>Он регистрирует в сети контракт</p>

        <p>Для выдачи разрешения (франшизы) принимать деньги за обучающие уроки по авторской методике Максим вызывает функцию <code>registerMember(address)</code></p>
        <p>Эту функцию может вызвать только автор контракта</p>
        <p><code>address</code> - это адрес представителя кому была выдана франшиза</p>


        <p>Для регистрации товаров за которые можно принимать оплату можно предусмотреть функцию <code>registerProduct(name)</code></p>
        <p><code>name</code> - наименование товара</p>
        <p>Выдаёт идентификатор зарегистрированного товара который надо будет указывать представителям для приема денег за этот товар</p>
        <p>Эту функцию может вызвать только автор контракта</p>

        <p>Для продажи товара представителем нужно указать адрес представителя и идентификатор продаваемого товара, клиент переводит деньги эфиры и они отправляются на кошелек представителя и процент на кошелек автора контракта Максима

        <h2 class="page-header text-center">Реализация</h2>

        <p>Продажу продукции можно реализовать в два этапа</p>
        <p>Представитель запрашивает в контракте выставление счета на оплату услуги например <code>getNewInvoice(product)</code> и подписывает транзакцию</p>
        <p>На выходе получает идентификатор выставленного счета</p>
        <p>Эту функцию может вызвать только представитель Максима</p>

        <p>После этого представитель предлагает оплатить выставленный счёт за услуги по идентификатору выставленного счета и вызывает функцию например pay(invoice)</p>
        <p>И переводит вместе с подписью эфиры</p>

        <p>Контракт отправляет эти эфиры на счёт представителя и счёт Максима</p>

        <p>Можно ещё предусмотреть функцию отзыва франшизы <code>removeMember(address)</code></p>
        <p>Опять же она доступна только автору контракта</p>


        <p>
            <a href="https://drive.google.com/file/d/0B9lrcmdS-YbTV29TUkxSV0JHLVk/view?usp=sharing" target="_blank">
                <img src="/images/controller/development-contracts/franshiza/1.png">
            </a>
        </p>
        <p>
            <a href="https://drive.google.com/file/d/0B9lrcmdS-YbTV29TUkxSV0JHLVk/view?usp=sharing" target="_blank">
                <img src="/images/controller/development-contracts/franshiza/2.png">
            </a>
        </p>
        <p>
            <a href="https://drive.google.com/file/d/0B9lrcmdS-YbTV29TUkxSV0JHLVk/view?usp=sharing" target="_blank">
                <img src="/images/controller/development-contracts/franshiza/3.png">
            </a>
        </p>
        <p>
            <a href="https://drive.google.com/file/d/0B9lrcmdS-YbTV29TUkxSV0JHLVk/view?usp=sharing" target="_blank">
                <img src="/images/controller/development-contracts/franshiza/4.png">
            </a>
        </p>
        <p>
            <a href="https://drive.google.com/file/d/0B9lrcmdS-YbTV29TUkxSV0JHLVk/view?usp=sharing" target="_blank">
                <img src="/images/controller/development-contracts/franshiza/5.png">
            </a>
        </p>
        <p>
            <a href="https://drive.google.com/file/d/0B9lrcmdS-YbTV29TUkxSV0JHLVk/view?usp=sharing" target="_blank">
                <img src="/images/controller/development-contracts/franshiza/6.png">
            </a>
        </p>

    </div>
</div>

