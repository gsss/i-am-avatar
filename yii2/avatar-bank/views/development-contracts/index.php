<?php
use common\services\UsersInCache;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;

/* @var $this yii\web\View */

$this->title = 'Контракты';

?>
<div class="container">
    <div class="col-lg-12">
        <h1 class="page-header text-center"><?= Html::encode($this->title) ?></h1>

        <p><a href="/development-contracts/documents">Документы</a></p>
        <p><a href="/development-contracts/registration">Регистрация</a></p>
        <p><a href="/development-contracts/enc-token">EnergyCoin</a></p>
    </div>
</div>



