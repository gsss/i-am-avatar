<?php
use common\services\UsersInCache;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;

/* @var $this yii\web\View */

$this->title = 'Подпись документов';

?>
<div class="container">
    <div class="col-lg-12">
        <h1 class="page-header text-center"><?= Html::encode($this->title) ?></h1>

        <p>Версия 1.1</p>
        <h3 class="page-header">Описание</h3>
        <p><b>Предназначение</b></p>
        <p>Этот контракт предназначен для такой сферы жизни человека как заверение документов электронной подписью.</p>
        <p><b>Что делает?</b></p>
        <p>Размещает хеш документа в блокчейн и подписывает его цифровыми подписями.</p>


        <h3 class="page-header">Функции</h3>
        <p><b>registerDocument</b></p>
        <p>Регистрирует документ</p>
        <p>Входные параметры:</p>

        <?= \avatar\services\Params::widget([
            'params' => [
                [
                    'name'        => 'hash',
                    'isRequired'  => true,
                    'type'        => 'string',
                    'description' => 'Хеш документа SHA256',
                ],
                [
                    'name'        => 'link',
                    'isRequired'  => true,
                    'type'        => 'string',
                    'description' => 'Ссылка на документ',
                ],
                [
                    'name'        => 'data',
                    'type'        => 'string',
                    'description' => 'Данные которые будут размещены в блокчейне для документа',
                ],
            ],
        ]) ?>

        <p><b>addSignature</b></p>
        <p>Ставит подпись в документ. Подписать документ можно любым количеством лиц. Достаточно знать идентификатор
            документа который появляется в событии <code>DocumentRegistered</code>.</p>
        <p>Входные параметры:</p>
        <?= \avatar\services\Params::widget([
            'params' => [
                [
                    'name'        => 'id',
                    'isRequired'  => true,
                    'type'        => 'uint',
                    'description' => 'Идентификатор документа',
                ],
            ],
        ]) ?>

        <h3 class="page-header">События</h3>
        <p><b>DocumentRegistered</b></p>
        <p>Индекс документов начинается с 1.</p>
        <p>Возвращает <code>id</code>, <code>hash</code></p>

        <p><b>DocumentSigned</b></p>
        <p>Возвращает <code>id</code>, <code>member</code></p>

        <h3 class="page-header">Чтение</h3>


        <h3 class="page-header">Интерфейс</h3>
        <p>TestNet</p>
        <p><a href="https://ropsten.etherscan.io/address/0xff6c78bfedf1bdb2d85071a9040798f8057a0a36" target="_blank"><code>0xff6c78bfedf1bdb2d85071a9040798f8057a0a36</code></a></p>
        <p><a href="https://ropsten.etherscan.io/address/0xdd72e26c31b2e4b700c1aec0d9a622b211eb07c6" target="_blank"><code>0xdd72e26c31b2e4b700c1aec0d9a622b211eb07c6</code></a></p>
        <p>LiveNet</p>
        <p><code>0xfa1bcd27da169c69bfbeda681c2a8277b8e08171</code></p>
        <p>Интерфейс контракта:</p>
        <pre>
[{"constant":true,"inputs":[],"name":"getDocumentsCount","outputs":[{"name":"","type":"uint256"}],"payable":false,"type":"function"},{"constant":false,"inputs":[{"name":"hash","type":"string"},{"name":"link","type":"string"},{"name":"data","type":"string"}],"name":"registerDocument","outputs":[],"payable":false,"type":"function"},{"constant":false,"inputs":[{"name":"id","type":"uint256"}],"name":"addSignature","outputs":[],"payable":false,"type":"function"},{"constant":true,"inputs":[{"name":"id","type":"uint256"}],"name":"getDocument","outputs":[{"name":"hash","type":"string"},{"name":"link","type":"string"},{"name":"data","type":"string"},{"name":"creator","type":"address"},{"name":"date","type":"uint256"},{"name":"count","type":"uint256"}],"payable":false,"type":"function"},{"constant":false,"inputs":[],"name":"kill","outputs":[],"payable":false,"type":"function"},{"constant":true,"inputs":[{"name":"","type":"uint256"}],"name":"documentsIds","outputs":[{"name":"hash","type":"string"},{"name":"link","type":"string"},{"name":"data","type":"string"},{"name":"creator","type":"address"},{"name":"date","type":"uint256"},{"name":"signsCount","type":"uint256"}],"payable":false,"type":"function"},{"constant":true,"inputs":[{"name":"id","type":"uint256"}],"name":"getDocumentSignsCount","outputs":[{"name":"","type":"uint256"}],"payable":false,"type":"function"},{"constant":false,"inputs":[{"name":"newOwner","type":"address"}],"name":"changeOwner","outputs":[],"payable":false,"type":"function"},{"constant":true,"inputs":[{"name":"id","type":"uint256"},{"name":"index","type":"uint256"}],"name":"getDocumentSign","outputs":[{"name":"member","type":"address"},{"name":"date","type":"uint256"}],"payable":false,"type":"function"},{"inputs":[],"payable":false,"type":"constructor"},{"anonymous":false,"inputs":[{"indexed":false,"name":"id","type":"uint256"},{"indexed":false,"name":"member","type":"address"}],"name":"DocumentSigned","type":"event"},{"anonymous":false,"inputs":[{"indexed":false,"name":"id","type":"uint256"},{"indexed":false,"name":"hash","type":"string"}],"name":"DocumentRegistered","type":"event"}]
</pre>

        <p>Код контракта:</p>
        <pre>
pragma solidity ^0.4.2;

/* Родительский контракт */
contract Owned {

    /* Адрес владельца контракта*/
    address owner;

    /* Конструктор контракта, вызывается при первом запуске */
    function Owned() {
        owner = msg.sender;
    }

    /* Изменить владельца контракта, newOwner - адрес нового владельца */
    function changeOwner(address newOwner) onlyowner {
        owner = newOwner;
    }

    /* Модификатор для ограничения доступа к функциям только для владельца */
    modifier onlyowner() {
        if (msg.sender==owner) _;
    }

    /* Удалить контракт */
    function kill() onlyowner {
        if (msg.sender == owner) suicide(owner);
    }
}

/* Основной контракт, наследует контракт Owned */
contract Documents is Owned {

    /* Структура представляющая документ */
    struct Document {
        string hash;
        string link;
        string data;
        address creator;
        uint date;
        uint signsCount;
        mapping (uint => Sign) signs;
    }

    /* Структура представляющая подпись */
    struct Sign {
        address member;
        uint date;
    }

    /* Маппинг ID документа -> документ */
    mapping (uint => Document) public documentsIds;

    /* Кол-во документов */
    uint documentsCount = 0;

    /* Событие при подписи документа участником, параметры - адрес участника, ID документа */
    event DocumentSigned(uint id, address member);

    /* Событие при регистрации документа, параметры - ID документа */
    event DocumentRegistered(uint id, string hash);

     /* Конструктор контракта, вызывается при первом запуске */
    function Documents() {
    }

    /* функция добавления документа, параметры - хэш, ссылка, дополнительные данные, создатель.
    Если не передаётся адрес создателя, то будет указан адрес отправителя, в конце вызовется событие DocumentRegistered
    с параметрами id - документа (позиция в массиве documents) и hash - хэш сумма */
    function registerDocument(string hash,
                       string link,
                       string data) {
        address creator = msg.sender;

        uint id = documentsCount + 1;
        documentsIds[id] = Document({
           hash: hash,
           link: link,
           data: data,
           creator: creator,
           date: now,
           signsCount: 0
        });
        documentsCount = id;
        DocumentRegistered(id, hash);
    }

    /* функция добавления подписи в документ, параметры - ID Документа, адрес подписчика.
    Если не передаётся адрес подписчика, то будет указан адрес отправителя,
    в конце вызовется событие DocumentSigned */
    function addSignature(uint id) {
        address member = msg.sender;

        Document d = documentsIds[id];
        uint count = d.signsCount;
        bool signed = false;
        if (count != 0) {
            for (uint i = 0; i < count; i++) {
                if (d.signs[i].member == member) {
                    signed = true;
                    break;
                }
            }
        }

        if (!signed) {
            d.signs[count] = Sign({
                    member: member,
                    date: now
                });
            documentsIds[id].signsCount = count + 1;
            DocumentSigned(id, member);
        }
    }

    /* Функция получения количества документов */
    function getDocumentsCount() constant returns (uint) {
        return documentsCount;
    }

    /* Функция получения документа по ID */
    function getDocument(uint id) constant returns (string hash,
                       string link,
                       string data,
                       address creator,
                       uint date,
                       uint count) {
        Document d = documentsIds[id];
        hash = d.hash;
        link = d.link;
        data = d.data;
        creator = d.creator;
        date = d.date;
    }

    /* Функция получения количества подписей по ID документа */
    function getDocumentSignsCount(uint id) constant returns (uint) {
        Document d = documentsIds[id];
        return d.signsCount;
    }

    /* Функция получения подписи документов, параметры - ID документа, позиция подписи в массиве */
    function getDocumentSign(uint id, uint index) constant returns (
                        address member,
                        uint date) {
        Document d = documentsIds[id];
        Sign s = d.signs[index];
        member = s.member;
        date = s.date;
	}
}
        </pre>

        <h3 class="page-header">Доолнительные потребности</h3>

        <p><b>функция hasSignature</b></p>
        <p>Проверяет есть ли подпись уже в документе. Ситуация: я хочу поставить подпись в документе, но я не помню ставил ли я ее или нет, для этого и нужна эта функция</p>
        <p>Возвращает 0 - нет подписи, 1 - есть подпись.</p>

        <p><b>функция AddSignature</b></p>
        <p>Она должна проверять есть ли подпись уже в документе или нет и если есть то вызывать ошибку.</p>
        <p>Она должна проверять есть ли документ который указан для подписи или нет.</p>


        <div class="text_insider">
            <p>Компания SkyWay предоставляет возможность цифрового заверения документов с использованием технологии
                блокчейн. Инновация носит <strong>экспериментальный характер</strong> и необязательна для всех, к ней
                можно прибегнуть по желанию.</p>
            <p>Цифровое заверение (простой вид «умного контракта») происходит с использованием специального сервиса
                Stampery. Stampery – это сервис, позволяющий в автоматическом режиме заверять представленный в
                электронном виде документ. Stampery стремительно набирает популярность во всём мире: он позволяет
                создавать неизменные и верифицируемые документы на блокчейнах биткоина и Ethereum.</p>
            <p>Сервис уже был успешно интегрирован компанией Microsoft в программный пакет Office, <a
                        href="https://www.microsoft.com/reallifecode/2017/04/10/stampery-blockchain-add-microsoft-office/"
                        target="_blank">об этом сообщается на сайте компании</a>. Активно внедряется блокчейн и в
                евразийском регионе. Широкий резонанс вызвало <a href="http://tass.ru/ekonomika/4055000"
                                                                 target="_blank">заявление</a> премьер-министра России
                на форуме инвестиций в Сочи – 2017. <strong>Дмитрий Медведев</strong> заявил, что "многие
                бизнес-процессы и многие социальные среды будут организованы на принципах блокчейн". <a
                        href="http://belapan.by/archive/2017/04/21/899936/" target="_blank"><strong>Александр
                        Лукашенко</strong> задал курс на применение в Беларуси</a> «всего, что реализуется в сфере
                высоких технологий: искусственного интеллекта, дополненной реальности, беспилотных автомобилей,
                технологии блокчейн и цифровых валют».&nbsp;</p>
            <h6><span style="color: #111111; font-size: 100%; letter-spacing: 1px;">Россия: применение блокчейна Центробанком и службой госрегистрации</span>
            </h6>
            <p>ЦБ РФ уже направил в профильные министерства предложение о создании электронной базы нотариально
                заверенных доверенностей на основе технологии блокчейн. Речь идет о включении проекта в дорожную карту
                «Цифровая экономика»: предлагается «создать распределенную сеть данных на основе блокчейн для проверки
                доверенностей, либо сформировать центральную базу нотариально заверенных доверенностей, которую будет
                вести нотариат.</p>
            <p>Сегодня, когда человек приходит в банк с нотариальной доверенностью, то максимум, что может сделать
                кредитная организация, — попросить небольшое время на проверку документа. Если нотариус, к примеру, не
                ответит на звонок и не подтвердит подлинность доверенности, то нет правовых оснований не доверять этой
                бумаге, банк обязан совершить операцию. Как правило, банки выявляют поддельные нотариальные
                доверенности, однако вопрос определения их подлинности открыт.</p>
            <p>Централизованный реестр доверенностей в сети уже создан, однако он уязвим для хакерских или DDoS-атак.
                Если злоумышленники захотят снять большие суммы по поддельной доверенности, они в нужный момент с
                помощью DDoS-атаки выведут из строя сервер, где хранятся данные. Сотрудники не смогут проверить
                достоверность документа — и им придется выдать деньги, технология блокчейн такую атаку делает
                невозможной.</p>
            <p>Также с инициативой задействовать блокчейн на вторичном рынке недвижимости&nbsp;<a
                        href="http://forklog.com/glava-rosreestra-predlagaet-zadejstvovat-blokchejn-na-vtorichnom-rynke-nedvizhimosti/"
                        target="_blank">выступила</a>&nbsp;Федеральная служба государственной регистрации, кадастра и
                картографии.</p>
            <p>&nbsp;</p>
            <p data-size="594x786"><img src="/assets/images/news/042017/stampery.jpg" alt=""
                                        data-src="/assets/images/news/042017/stampery.jpg" style="opacity: 1;"></p>
            <h6><span style="font-size: 100%;"><strong>Зачем нужно заверять сертификат при помощи </strong><strong>Stampery</strong></span>
            </h6>
            <p>Блокчейн представляет собой распределенную учетную книгу записей о событиях в цифровом мире.
                Распределённую – значит не сосредоточенную ни в каком конкретном месте, ни на каком отдельном носителе.
                Записи, в том числе, та запись, что фиксирует параметры сертификата (уникального файла)
                мультиплицированы в глобальной сети компьютеров. Они многократно дублируются, а потому удалить их из
                сети, изменить или подделать практически невозможно. Таким образом, Stampery помогает обезопасить
                обладателя сертификата от попыток его фальсификации.</p>
            <p>Преимущества Stampery:</p>
            <p>– Безопасность. Ваши данные защищены самой сильной сетью в мире.</p>
            <p>– Приватность. Никто не может получить доступ к вашей информации. Stampery выдаёт криптографический
                идентификатор и публикует его в блокчейне.</p>
            <p>– Прозрачность. Блокчейн приватен, но при этом доступ к нему есть у любого юзера из любой точки.</p>
            <p>– Достоверность, подтверждённая глобально. Сертификаты подтверждаются общемировой сетью.</p>
            <p>– Возможность аудита. Основанное на блокчейне хэширование и подтверждение даты является неопровержимым
                доказательством существования и целостности данных.</p>
            <p class="justifyleft">&nbsp;</p>
            <h6><strong>По пунктам на примере сертификата инвестора </strong><strong>SkyWay</strong></h6>
            <p><strong><em>Повторимся, что использование блокчейн носит экспериментальный характер</em></strong><strong><em>
                        и необязательно для всех, к нему можно прибегнуть по желанию.</em></strong></p>
            <p>В разделе «сертификаты» личного кабинета на сайте rsw-systems.com имеется возможность заказать
                сертификат, в котором указывается определённый набор данных. &nbsp;Обновлённый набор функций личного
                кабинета позволяет получить не только бумажный вариант этого документа, но и его электронную версию.
                Электронная версия документа может быть заверена с помощью сервиса Stampery.</p>
            <p>- Для того, чтобы заверить электронную версию сертификата, необходимо выбрать соответствующую функцию
                раздела «сертификаты» вашего личного кабинета на сайте rsw-systems.com.</p>
            <p>- Процедура заверения происходит в автоматическом режиме и может занять некоторое время.</p>
            <p>-&nbsp;По окончании процедуры вы получаете код проверки соответствия параметров заверенного документа
                предъявляемому (Hash).</p>
            <p>-&nbsp;С помощью этого кода вы в любой момент сможете подтвердить право собственности на сертификат,
                существование его в определённый момент времени и его целостность (подтверждение того, что данные не
                были изменены).</p>
            <p>Процедура заверения обозначенных параметров (собственность, существование, целостность) происходит за
                счёт оцифровки сертификата (уникального файла) в блокчейн.</p>
            <p>Блокчейн – это выстроенная по определённым правилам цепочка из формируемых блоков транзакций. В данном
                случае в качестве таковых цепочек выступают две базы данных Bitcoin и Ethereum. С их помощью фиксируются
                данные о сертификате в оцифрованном виде. В качестве завершённой «транзакции заверения» – группа
                последовательных операций с базой данных. Актуальная информация не хранится на одном сервере, а
                распределена по всей сети и хранится у каждого участника обмена данными. Когда один человек вносит
                изменение, оно сразу записывается у всех участников и сохраняется у них на устройствах.</p>
            <p>&nbsp;</p>
            <p>&nbsp;</p>
            <div class="clear"></div>
            <div class="gallery" itemscope="" data-pswp-uid="1">
            </div>
        </div>
    </div>
</div>



