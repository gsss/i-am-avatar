<?php
/**
 * Created by PhpStorm.
 * User: god
 * Date: 13.04.2017
 * Time: 23:58
 */

use yii\helpers\Html;


/* @var $this yii\web\View */


$this->title = 'AvatarPay';

?>
<div class="container">
    <div class="col-lg-12">
        <h1 class="page-header text-center"><?= Html::encode($this->title) ?></h1>

        <p><b>Описание контракта</b></p>
        <p>Аккредитив - процедура покупки торвара с резервацией денег на эскроу счете до момента получения товара.</p>
        <p>Продавец товара видит что деньги были зарезрвированы на эскроу счете для его продажи и отравляет товар.</p>
        <p>После получения товара покупатель разблокирует деньги и они отправляются продавцу.</p>

        <h3 class="page-header">Решение</h3>
        <p>Аккредитив - процедура покупки торвара с резервацией денег на эскроу счете до момента получения товара.</p>

        <p>
            <a href="https://drive.google.com/file/d/0BzHYNoEyPNTXdkswTkhteXdfS2c/view?usp=sharing" target="_blank">
                <img src="/images/controller/development-contracts/accreditiv/a.png">
            </a>
        </p>
        <p>
            <a href="https://yadi.sk/d/ycPQy0Ac3LC3bZ" target="_blank">
                <img src="/images/controller/development-contracts/accreditiv/8.png">
            </a>
        </p>
        <p>Для реализации нужны функции:</p>
        <ul>
            <li><b>Зарезервировать деньги для № продавца</b> - <code>registerPurchasing(addressSeller)</code>, выдает идентификатор сделки (<code>idPurchasing</code>)</li>
            <li><b>Проверить зарезервированные деньги по сделке</b> - <code>getPurchasing(idPurchasing)</code>. Выдает <code>addressSeller</code>, <code>value</code></li>
            <li><b>Подтвердить получение товара</b> - <code>confirmGetting(idPurchasing)</code>. Можно подтвердить только свою сделку</li>
            <li><b>Отклонить получение товара</b> - <code>rejectGetting(idPurchasing)</code>. Может отклонить только <code>addressSeller</code></li>
            <li><b>Вернуть деньги</b> - <code>returnMoney(idPurchasing)</code>. Может вернуть только <code>addressBuyer</code> и только по истечению времени закладки которое он сам и ставил</li>

            <li><b>Поставить рейтинг покупателю</b> - <code>setRatingToBuyer(idPurchasing, rating)</code>. Может поставить только продавец в сделке</li>
            <li><b>Поставить рейтинг продавцу</b> - <code>setRatingToSeller(idPurchasing, rating)</code>. Может поставить только покупатель в сделке</li>
            <li><b>Получить рейтинг как покупателя</b> - <code>getRatingAvgRatingBuyer(address)</code>. Если ни разу не был зарегистрирован то будет выдана пустая строка ''</li>
            <li><b>Получить рейтинг как продавца</b> - <code>getRatingAvgRatingSeller(address)</code>. Если ни разу не был зарегистрирован то будет выдана пустая строка ''</li>
            <li><b>Получить рейтинг общий</b> - <code>getRatingAvgRating(address)</code>. Если ни разу не был зарегистрирован то будет выдана пустая строка ''</li>
        </ul>

        <h3 class="page-header">Модель данных</h3>
        <p><b>Сущность сделки</b></p>
        <?= \avatar\services\Params::widget([
            'params' => [
                [
                    'name'        => 'id',
                    'isRequired'  => true,
                    'type'        => 'uint',
                    'description' => 'Идентификатор сделки',
                ],
                [
                    'name'        => 'seller',
                    'isRequired'  => true,
                    'type'        => 'address',
                    'description' => 'Адрес продавца, начинается с 0x',
                ],
                [
                    'name'        => 'buyer',
                    'isRequired'  => true,
                    'type'        => 'address',
                    'description' => 'Адрес покупателя, начинается с 0x',
                ],
                [
                    'name'        => 'status',
                    'isRequired'  => true,
                    'type'        => 'uint',
                    'description' => 'Идентификатор ситуации, в которой находится сделка',
                ],
                [
                    'name'        => 'value',
                    'isRequired'  => true,
                    'type'        => 'float',
                    'description' => 'кол-во монет (ETH), которое было зарезервировано в сделке',
                ],
                [
                    'name'        => 'sec',
                    'isRequired'  => true,
                    'type'        => 'uint',
                    'description' => 'кол-во секунд до возможности вернуть деньги',
                ],
            ],
        ]) ?>

        <pre>mapping (uint => Purchasing) public purchasingList;

// Структура сделки
struct Purchasing {
    address seller;
    address buyer;
    uint status;
    uint date;           // мгновение начала сделки
    uint value;          // кол-во эфира зарезервированное для сделки
}
        uint purchasingListCount;
        </pre>


        <h2 class="page-header">Данные блокчейн</h2>
        <p>Интерфейс контракта:</p>
        <pre>
---
</pre>
<p>Код контракта:</p>
        <pre>
---
</pre>

        <h2 class="page-header">Функции аккредитива</h2>
        <p>Всего для осуществления сейфовой ячейки достаточно пять функций:</p>
        <ul>
            <li>Ввод денег на контракт</li>
            <li>Проверка продавцом сделки</li>
            <li>Подтверждение получения товара покупателем и перевод денег продавцу</li>
            <li>Отклонение сделки продавцом и перевод денег покупателю</li>
            <li>Возврат денег по истечении срока закладки</li>
        </ul>

        <h3 class="page-header">Ввод денег на контракт</h3>
        <p>Вызывается функция контракта с переводом эфира. Функцию надо обьявить как <code>payable</code></p>
        <p><a href="https://ethereum.stackexchange.com/questions/20874/payable-function-in-solidity" target="_blank">https://ethereum.stackexchange.com/questions/20874/payable-function-in-solidity</a></p>
        <p><a href="http://solidity.readthedocs.io/en/develop/contracts.html#fallback-function" target="_blank">http://solidity.readthedocs.io/en/develop/contracts.html#fallback-function</a></p>
        <pre>function registerPurchasing(address _to) payable
{
    // ...
}</pre>

        <h3 class="page-header">Проверка продавцом сделки</h3>
        <h3 class="page-header">Подтверждение получения товара покупателем и перевод денег продавцу</h3>
        <h3 class="page-header">Отклонение сделки продавцом и перевод денег покупателю</h3>
        <h3 class="page-header">Возврат денег по истечении срока закладки</h3>

        <h2 class="page-header">Рейтинговая система</h2>

        <p><b>Сущность рейтинга</b></p>
        <p>массив кде ключ - адрес, а значение массив элементов [idPurchasing, rating, type, link, hash]</p>
        <p>type - 1 - продавец, 2 - покупатель</p>
        <p>Исследование</p>
        <p>Я покупатель. Я заложил деньги на 30 дней. Продавец убежал. Мне нужно ждать 30 дней. Я ставлю рейтинг продавцу</p>


        <pre>mapping (address => RaitingList) public raiting;

struct RaitingList {
    uint signsCount;                    // кол-во элементов в массиве list
    mapping (uint => RaitingItem) list;
}

struct RaitingItem {
    string hash;        // хеш документа
    string link;        // ссылка на документ
    address author;     // автор рейтинга
    uint idPurchasing;  // идентификатор сделки
    uint date;          // мгновение постановки рейтинга
    uint rating;        // рейтинг от 0 до 200;
    uint type;          // В качестве кого представляется тот кому ставится рейтинг 1 - продавец, 2 - покупатель
}
</pre>
        <p>Я создаю сущность <code>RaitingList</code>. В ней будет хранится массив рейтингов (сущностей <code>RaitingItem</code>)</p>


        <h2 class="page-header">Полный код контракта</h2>

        <pre><?= file_get_contents(Yii::getAlias('@avatar/views/development-contracts/accreditiv1.sol')) ?></pre>


        <h2 class="page-header">Код контракта с LocalEthereum</h2>

        <p><a href="https://etherscan.io/address/0x09678741bd50c3e74301f38fbd0136307099ae5d#code" target="_blank">https://etherscan.io/address/0x09678741bd50c3e74301f38fbd0136307099ae5d#code</a></p>

        <pre><?= file_get_contents(Yii::getAlias('@avatar/views/development-contracts/accreditiv2.sol')) ?></pre>

        <h2 class="page-header">Использование контракта для токенов</h2>
        <p><b>Первый вариант использования</b></p>
        <p>Для того чтобы участники рынка могли воспользоваться контрактом аккредитива для токенов необходимо:</p>
        <p>1. Контракту токена дать разрешение котонтрукту аккредитива списать у вас N токенов. Это делается так: вызываете функцию <code>approve</code> контракта вашего токена с параметрами <code>Адрес контракта аккредитива</code>, кол-во монет которое будет участвовать в сделке.</p>
        <p>2. Вызвать контракт аккредитива <code>tokenRegisterPurchasing(addressSeller, addressToken, value)</code>, и он вызовет transferFrom и положит себе</p>
        <p>3. В положительном исходе вызвать контракт аккредитива <code>tokenConfirmGetting(idPurchasing)</code>, и контракт вызовет функцию transfer в адрес партнера сделки</p>
        <p>4. В отрицательном исходе вызвать контракт аккредитива <code>tokenRejectGetting(idPurchasing)</code>, и контракт вызовет функцию transfer в адрес отправителя токенов</p>

        <p><b>Еще один вариант использования</b></p>
        <p>1. Покупатель переводит токены на адрес контракта</p>
        <p>2. Вызывает контракт аккредитива <code>tokenRegisterPurchasing(addressSeller, addressToken, value)</code> (эта функция внутри по другому реализована в отличие от той же в предыдущем примере)</p>
        <p>3. В положительном исходе вызвать контракт аккредитива <code>tokenConfirmGetting(idPurchasing)</code></p>
        <p>4. В отрицательном исходе вызвать контракт аккредитива <code>tokenRejectGetting(idPurchasing)</code></p>
        <p>Анализ</p>
        <p>Вопрос: сможет ли контракт проверить поступили ли от отправителя токены? скорее нет чем да. </p>
        <p>Тогда вся эта ветка решения под сомнением</p>

        <h2 class="page-header">Оспаривание сдеки</h2>
        <p>Это следующий уровень усложнения этого контракта, когда у обоих участников сделки появляется возможность оспорить сделку. При этом они заранее выбирают себе арбитра.</p>

        <h2 class="page-header">Проба реализации tokenRegisterPurchasing</h2>
        <p>Анализ</p>
        <p>Вопрос: Как вызвать transferFrom если нужно знать адрес контракта. Решение: задать его после создания контракта. переменная contractAddress </p>
        <pre>tokenReward.transferFrom(msg.sender, contractAddress, value);</pre>
        <p>Встает вопрос а как узнать результат этой функции?</p>
        <p>Можно проверить баланс на кошельке отправителя.</p>
        <pre>// проверяю наличие токенов на счету
if (tokenReward.balanceOf(msg.sender) < value) {
    Error(1, 'No money');
    return;
}</pre>
        <p>Для регистрации сделок с токенами нужна отдельная таблица с добавлением адреса токена</p>
        <pre>// Структура сделки с токенами
struct PurchasingToken {
    address seller;
    address buyer;
    address token;
    uint status;
    uint date;              // мгновение начала сделки
    uint256 value;          // кол-во копеек токена зарезервированное для сделки
    uint256 sec;            // Кол-во секунд на которое закладываются деньги
}</pre>
        <p>А это для создания массива сделок</p>
        <pre>mapping (uint => PurchasingToken) public purchasingTokenList;</pre>
        <p>А это для хранения верхнего индекса массива сделок с токенами</p>
        <pre>uint purchasingTokenListCount;</pre>
        <p>Добавляю новое событие для регистрации сделки с токеном</p>
        <pre>event PurchasingTokenRegistered(uint id, address _from, address _to, address addressToken, uint256 value);</pre>
        <pre>PurchasingTokenRegistered(id, msg.sender, _to, addressToken, value);</pre>

        <p>Добавляю новую функцию</p>
        <pre>function setContractAddress(address _to) onlyowner
{
    contractAddress = _to;
}</pre>
        <p>Нужно правда сделать чтобы никто потом не смог поменять, но это потом</p>
        <p>Итого теперь контракт выглядит так:</p>
        <pre><?= file_get_contents(Yii::getAlias('@avatar/views/development-contracts/accreditiv1.2.sol')) ?></pre>
        <p><a href="https://etherscan.io/address/0x09678741bd50c3e74301f38fbd0136307099ae5d" target="_blank"><code>0x09678741bd50c3e74301f38fbd0136307099ae5d</code></a></p>

        <h2 class="page-header">Тестирование функции tokenRegisterPurchasing</h2>
        <p>Сеть Ropsten</p>
        <p>Алгоритм:</p>
        <p>1. Зарегистрировать контракт</p>
        <p>2. Верифицировать контракт</p>
        <p>3. Установить адрес</p>
        <p>4. сделать approve для контракта</p>
        <p>5. Заложить токены</p>
        <p>6. Проверить если токенов не хватает, что будет</p>

        <h2 class="page-header">Редактирование</h2>
        <p>Добавляю в функцию confirmGetting проверку на условие</p>
        <p>Если сделка уже завершена то ее нельзя вызвать второй раз</p>
        <pre>// если сделка уже закрыта то не дать выполнить
if (purchasing.status == 2) {
    Error(3, 'Сделка уже закрыта');
    return false;
}</pre>

        <h2 class="page-header">Редактирование</h2>
        <p>Меняю в функции confirmGetting send на transfer</p>
        <pre>purchasing.seller.send(purchasing.value);</pre>
        <pre>purchasing.seller.transfer(purchasing.value);</pre>

        <h2 class="page-header">Дальнейшее редактирование</h2>
        <p>Буду далее тестировать позитивный сценарий закладки эфира и негативный, чтобы внедрить уже версию альфа</p>



    </div>
</div>

