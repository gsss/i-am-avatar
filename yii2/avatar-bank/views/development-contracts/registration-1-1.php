<?php
use common\services\UsersInCache;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;

/* @var $this yii\web\View */

$this->title = 'Регистрация';

?>
<div class="container">
    <div class="col-lg-12">
        <h1 class="page-header text-center"><?= Html::encode($this->title) ?></h1>

        <p>Версия 1.1</p>
        <p>Предназначен для регистрации Аватаров в нашей Державе Света "Новая Земля".</p>
        <p>В контракте существует функции:</p>
        <ul>
            <li><code>kill</code> - удалить контракт из сети, только для создателя контракта</li>
            <li><code>addMember</code> - добавить зарегистрировать участника</li>
            <li><code>getPK</code> - Функция получения приватного ключа по ID юзера (только для владельца контракта, не используется)</li>
            <li><code>getMemberCount</code> - Функция получения количества юзеров</li>
            <li><code>getMember</code> - Функция получения юзера по id
            возвращает массив из полей [имя, фамилия, отчество, дата_рождения, хэш аватара, id аватара]</li>
        </ul>


        <h2 class="page-header">Схематическое пояснение связки юрисдикций</h2>
        <p>Связь паспорта с регистрацией и с подписью документов.</p>
        <p><img src="/images/controller/development-contracts/registration/dna.png" width="100%"/></p>
        <p><a href="https://drive.google.com/file/d/0BzHYNoEyPNTXcU43a2Z1QnV2V00/view?usp=sharing
" target="_blank">Документ</a></p>


        <pre>
struct Member {
    address member;
    string name;
    string surname;
    string patronymic;
    uint birthDate;
    string birthPlace;
    string avatarHash;
    uint avatarID;
    bool approved;
    uint memberSince;
}
        </pre>


        <p><b>Обновление данных</b></p>
        <p>Если человеку нужно обновить данные о себе то он их переписывает более расширенной информацией.</p>

        <h2 class="page-header">Функции</h2>
        <h3 class="page-header">addMember</h3>
        <p>функция добавления и обновления участника, параметры - адрес, имя, фамилия,
            отчество, дата рождения (linux time), место рождения, хэш аватара, ID аватара, дополнительные данные в формате JSON
            если пользователь с таким адресом не найден, то будет создан новый, в конце вызовется событие
            MemberAdded, если пользователь найден, то будет произведено обновление полей и проставлен флаг
            подтверждения approved
        </p>
        <h2 class="page-header">События</h2>
        <p>MemberAdded</p>
        <p>MemberChanged</p>
        <h2 class="page-header">Что такое AvatarID и AvatarHash</h2>
        <p>Это идентификатор картинки-аватара и его хеш по базе данных https://www.avatar-bank.com/data-base/avatar.
            По мимо картинки можно сохранить дополнительные данные.
            Hash вычисляется по алгоритму SHA256 для содержимого картинки и дополнительных данных (актуально для ID = 8 и выше, для ID = 7 и наже актуален старый алгоритм SHA1)
        </p>
        <pre>
$hash = hash('sha256', $imageContent . $dataContent);
</pre>
        <h2 class="page-header">Интерфейс</h2>




        <pre>
[{"constant":true,"inputs":[{"name":"","type":"uint256"}],"name":"memberData","outputs":[{"name":"","type":"string"}],"payable":false,"type":"function"},{"constant":true,"inputs":[{"name":"","type":"address"}],"name":"memberId","outputs":[{"name":"","type":"uint256"}],"payable":false,"type":"function"},{"constant":false,"inputs":[],"name":"kill","outputs":[],"payable":false,"type":"function"},{"constant":true,"inputs":[{"name":"","type":"uint256"}],"name":"members","outputs":[{"name":"member","type":"address"},{"name":"name","type":"string"},{"name":"surname","type":"string"},{"name":"patronymic","type":"string"},{"name":"birthDate","type":"uint256"},{"name":"birthPlace","type":"string"},{"name":"avatarHash","type":"string"},{"name":"avatarID","type":"uint256"},{"name":"approved","type":"bool"},{"name":"memberSince","type":"uint256"}],"payable":false,"type":"function"},{"constant":true,"inputs":[],"name":"getMemberCount","outputs":[{"name":"","type":"uint256"}],"payable":false,"type":"function"},{"constant":false,"inputs":[{"name":"newOwner","type":"address"}],"name":"changeOwner","outputs":[],"payable":false,"type":"function"},{"constant":true,"inputs":[{"name":"id","type":"uint256"}],"name":"getMember","outputs":[{"name":"name","type":"string"},{"name":"surname","type":"string"},{"name":"patronymic","type":"string"},{"name":"birthDate","type":"uint256"},{"name":"birthPlace","type":"string"},{"name":"avatarHash","type":"string"},{"name":"avatarID","type":"uint256"},{"name":"data","type":"string"}],"payable":false,"type":"function"},{"constant":true,"inputs":[{"name":"","type":"uint256"}],"name":"pks","outputs":[{"name":"","type":"string"}],"payable":false,"type":"function"},{"constant":true,"inputs":[{"name":"id","type":"uint256"}],"name":"getPK","outputs":[{"name":"","type":"string"}],"payable":false,"type":"function"},{"constant":false,"inputs":[{"name":"name","type":"string"},{"name":"surname","type":"string"},{"name":"patronymic","type":"string"},{"name":"birthDate","type":"uint256"},{"name":"birthPlace","type":"string"},{"name":"avatarHash","type":"string"},{"name":"avatarID","type":"uint256"},{"name":"data","type":"string"}],"name":"addMember","outputs":[],"payable":false,"type":"function"},{"inputs":[],"payable":false,"type":"constructor"},{"anonymous":false,"inputs":[{"indexed":false,"name":"member","type":"address"},{"indexed":false,"name":"id","type":"uint256"}],"name":"MemberAdded","type":"event"},{"anonymous":false,"inputs":[{"indexed":false,"name":"member","type":"address"},{"indexed":false,"name":"id","type":"uint256"}],"name":"MemberChanged","type":"event"}]
</pre>

        <p><b>TestNet</b></p>
        <p><code>0x5d1f0541cbaa6f2ca7f50674d4d112beeea1e87c</code></p>

        <p><b>LiveNet</b></p>
        <p><code><a href="https://etherscan.io/address/0xf180e0f0b8ae56f01427817c2627f1fc75202929" target="_blank">0xf180E0F0b8ae56F01427817C2627F1fc75202929</a></code></p>
        <p>Код:</p>
<pre>
pragma solidity ^0.4.2;

/* Родительский контракт */
contract Owned {

    /* Адрес владельца контракта*/
    address owner;

    /* Конструктор контракта, вызывается при первом запуске */
    function Owned() {
        owner = msg.sender;
    }

    /* Изменить владельца контракта, newOwner - адрес нового владельца */
    function changeOwner(address newOwner) onlyowner {
        owner = newOwner;
    }

    /* Модификатор для ограничения доступа к функциям только для владельца */
    modifier onlyowner() {
        if (msg.sender==owner) _;
    }

    /* Удалить контракт */
    function kill() onlyowner {
        if (msg.sender == owner) suicide(owner);
    }
}

/* Основной контракт, наследует контракт Owned */
contract Gods is Owned {

    /* Структура представляющая участника */
    struct Member {
        address member;
        string name;
        string surname;
        string patronymic;
        uint birthDate;
        string birthPlace;
        string avatarHash;
        uint avatarID;
        bool approved;
        uint memberSince;
    }

    /* Массив участников */
    Member[] public members;

    /* Маппинг адрес участника -> id участника */
    mapping (address => uint) public memberId;

    /* Маппинг id участника -> приватный ключ кошелька */
    mapping (uint => string) public pks;

    /* Маппинг id участника -> дополнительные данные на участника в формате JSON */
    mapping (uint => string) public memberData;

    /* Событие при добавлении участника, параметры - адрес, ID */
    event MemberAdded(address member, uint id);

    /* Событие при изменении участника, параметры - адрес, ID */
    event MemberChanged(address member, uint id);

    /* Конструктор контракта, вызывается при первом запуске */
    function Gods() {
        /* Добавляем пустого участника для инициализации */
        addMember('', '', '', 0, '', '', 0, '');
    }

    /* функция добавления и обновления участника, параметры - адрес, имя, фамилия,
     отчество, дата рождения (linux time), место рождения, хэш аватара, ID аватара
     если пользователь с таким адресом не найден, то будет создан новый, в конце вызовется событие
     MemberAdded, если пользователь найден, то будет произведено обновление полей и проставлен флаг
     подтверждения approved */
    function addMember(string name,
        string surname,
        string patronymic,
        uint birthDate,
        string birthPlace,
        string avatarHash,
        uint avatarID,
        string data) onlyowner {
        uint id;
        address member = msg.sender;
        if (memberId[member] == 0) {
            memberId[member] = members.length;
            id = members.length++;
            members[id] = Member({
                member: member,
                name: name,
                surname: surname,
                patronymic: patronymic,
                birthDate: birthDate,
                birthPlace: birthPlace,
                avatarHash: avatarHash,
                avatarID: avatarID,
                approved: (owner == member),
                memberSince: now
            });
            memberData[id] = data;
            if (member != 0) {
                MemberAdded(member, id);
            }
        } else {
            id = memberId[member];
            Member m = members[id];
            m.approved = true;
            m.name = name;
            m.surname = surname;
            m.patronymic = patronymic;
            m.birthDate = birthDate;
            m.birthPlace = birthPlace;
            m.avatarHash = avatarHash;
            m.avatarID = avatarID;
            memberData[id] = data;
            MemberChanged(member, id);
        }
    }

    /* Функция получения приватного ключа по ID юзера */
    function getPK(uint id) onlyowner constant returns (string) {
        return pks[id];
    }

    /* Функция получения количества юзеров */
    function getMemberCount() constant returns (uint) {
        return members.length - 1;
    }

    /* Функция получения юзера по id
     возвращает массив из полей [имя, фамилия, отчество, дата_рождения, хэш аватара, id аватара] */
    function getMember(uint id) constant returns (
        string name,
        string surname,
        string patronymic,
        uint birthDate,
        string birthPlace,
        string avatarHash,
        uint avatarID,
        string data) {
        Member m = members[id];
        name = m.name;
        surname = m.surname;
        patronymic = m.patronymic;
        birthDate = m.birthDate;
        birthPlace = m.birthPlace;
        avatarHash = m.avatarHash;
        avatarID = m.avatarID;
        data = memberData[id];
    }
}
</pre>

    </div>
</div>



