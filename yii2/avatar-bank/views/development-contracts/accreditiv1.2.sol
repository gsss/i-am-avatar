pragma solidity ^0.4.2;
/*
AvatarNetwork Copyright

https://avatarnetwork.io
*/

interface token {
    function transfer(address receiver, uint256 amount) returns (bool success);
    function transferFrom(address from, address to, uint256 amount) returns (bool success);
    function balanceOf(address owner) constant returns (uint256 balance);
    function allowance(address _owner, address _spender) constant returns (uint256 remaining);
}


/* Родительский контракт */
contract Owned {

    /* Адрес владельца контракта*/
    address owner;

    /* Конструктор контракта, вызывается при первом запуске */
    function Owned() {
        owner = msg.sender;
    }

    /* Изменить владельца контракта, newOwner - адрес нового владельца */
    function changeOwner(address newOwner) onlyowner {
        owner = newOwner;
    }

    /* Модификатор для ограничения доступа к функциям только для владельца */
    modifier onlyowner() {
        if (msg.sender==owner) _;
    }
}

/* Основной контракт, наследует контракт Owned */
contract AvatarPay is Owned {

    address contractAddress;

    //    /* меппинг адрес (на кого) => СписокРетинговыхОценок */
    //    mapping (address => RaitingList) public raiting;
    //
    //    /* Кол-во адресов */
    //    uint256 raitingCount = 0;
    //
    //    /* сущность СписокРейтинговыхОценок содержащая один элемент list представляющий из себя массив оценок */
    //    struct RaitingList {
    ////        uint length;                         // кол-во элементов в массиве list
    //        mapping (uint => RaitingItem) list;  // массив рейтингов
    //    }
    //
    //    /* сущность "Оценка" */
    //    struct RaitingItem {
    //        string hash;        // хеш документа
    //        string link;        // ссылка на документ
    //        address author;     // автор рейтинга
    //        uint idPurchasing;  // идентификатор сделки
    //        uint date;          // мгновение постановки рейтинга
    //        uint rating;        // рейтинг от 0 до 200;
    //        uint type1;          // В качестве кого представляется тот кому ставится рейтинг 1 - продавец, 2 - покупатель
    //    }

    mapping (uint => Purchasing) public purchasingList;

    // Структура сделки
    struct Purchasing {
        address seller;
        address buyer;
        uint status;
        uint date;           // мгновение начала сделки
        uint256 value;          // кол-во wei зарезервированное для сделки
        uint256 sec;            // Кол-во секунд на которое закладываются деньги
    }
    uint purchasingListCount;

    mapping (uint => PurchasingToken) public purchasingTokenList;

    // Структура сделки с токенами
    struct PurchasingToken {
        address seller;
        address buyer;
        address token;
        uint status;
        uint date;              // мгновение начала сделки
        uint256 value;          // кол-во копеек токена зарезервированное для сделки
        uint256 sec;            // Кол-во секунд на которое закладываются деньги
    }
    uint purchasingTokenListCount;


    /* Событие при подписи документа участником, параметры - ID сделки, адрес кому предназначены деньги */
    event PurchasingRegistered(uint id, address member);

    /* Событие когда регистрируется сделка с токеном */
    event PurchasingTokenRegistered(uint id, address _from, address _to, address addressToken, uint256 value);

    /* Событие когда возникает ошибка */
    event Error(uint id, string message);

    /* Событие когда покупатель подтверждает получение товара */
    event ConfirmGetting(uint value, address from, address to);

    /* Событие когда продавец отменяет сделку в результате того что товар клиенту не понравился и товар вернулся продавцу */
    event RejectGetting(uint value, address from, address to);

    /* Событие когда покупатель возвращает деньги себе по истечение времени на сделку и не исполнение обязательств перед покупателем */
    event ReturnMoney(uint value, address to);

    /* Конструктор контракта, вызывается при первом запуске */
    function AvatarPay() {
        purchasingListCount = 0;
    }

    /* Пользователь закладывает деньги через функцию */
    /* @params address _to для кого закладываются деньги */
    /* @params uint days на какое кол-во дней закладываются деньги */
    function registerPurchasing(address _to, uint256 day) payable
    {
        // кол-во денег которые пользователь заложил
        uint value = msg.value;

        // Если денег не отправлено то значит и сделки нет
        if (value == 0) {
            Error(1, 'No money');
            return;
        }

        uint id = purchasingListCount + 1;

        // добавляю сделку
        purchasingList[id] = Purchasing({
            seller: _to,
            buyer: msg.sender,
            status: 1,
            date: now,
            value: msg.value,
            sec: (day * 60 * 60 * 24)
            });
        purchasingListCount = id;

        // вызываю событие
        PurchasingRegistered(id, _to);
    }

    function setContractAddress(address _to) onlyowner
    {
        contractAddress = _to;
    }

    /* Регистрирует сделку в токенах */
    function tokenRegisterPurchasing(address _to, address addressToken, uint256 value, uint256 days1)
    {
        // определяю токен
        token tokenReward;

        // формирую контракт как переменную
        tokenReward = token(addressToken);

        // проверяю наличие токенов на счету
        if (tokenReward.balanceOf(msg.sender) < value) {
            Error(1, 'No money');
            return;
        }

        // проверяю наличие возможности списать токены
        if (!tokenReward.allowance(msg.sender, contractAddress)) {
            Error(2, 'Not possible to get tokens');
            return;
        }

        // перевожу токены
        tokenReward.transferFrom(msg.sender, contractAddress, value);

        uint id = purchasingTokenListCount + 1;

        // добавляю сделку
        purchasingTokenList[id] = PurchasingToken({
            seller: _to,
            buyer: msg.sender,
            status: 1,
            token: addressToken,
            date: now,
            value: value,
            sec: (days1 * 60 * 60 * 24)
            });
        purchasingTokenListCount = id;

        // вызываю событие
        PurchasingTokenRegistered(id, msg.sender, _to, addressToken, value);
    }

    function getPurchasing(uint idPurchasing) constant returns (
        address seller,
        address buyer,
        uint status,
        uint date,
        uint value,
        uint sec) {

        Purchasing m = purchasingList[idPurchasing];
        seller = m.seller;
        buyer = m.buyer;
        status = m.status;
        date = m.date;
        value = m.value;
        sec = m.sec;
    }

    /* Подтверждает получение товара и отправляет деньги продавцу */
    function confirmGetting(uint idPurchasing) returns (bool) {

        // если идентификатор сделки превыщает их кол-во, то значит что запрашивается не существующая сделка, поэтому будет вызвана ошибка
        if (idPurchasing > purchasingListCount) {
            Error(1, 'Purchasing not found');
            return false;
        }

        Purchasing purchasing = purchasingList[idPurchasing];

        // если функцию вызывает не покупатель то не дать выполнить
        if (purchasing.buyer != msg.sender) {
            Error(2, 'Access denied. This is not your purchasing');
            return false;
        }

        // если сделка уже закрыта то не дать выполнить
        if (purchasing.status == 2) {
            Error(3, 'Сделка уже закрыта');
            return false;
        }

        // отправляю эфиры
        // TODO функцию найти send
        purchasing.seller.transfer(purchasing.value);
        ConfirmGetting(purchasing.value, purchasing.buyer, purchasing.seller);

        return true;
    }

    /* Продавец отменяет сделку и деньги возвращаются покупателюПодтверждает получение товара и отправляет деньги продавцу */
    function rejectGetting(uint idPurchasing) returns (bool) {

        // если идентификатор сделки превыщает их кол-во, то значит что запрашивается не существующая сделка, поэтому будет вызвана ошибка
        if (idPurchasing > purchasingListCount) {
            Error(1, 'Purchasing not found');
            return false;
        }

        Purchasing purchasing = purchasingList[idPurchasing];

        // если функцию вызывает не покупатель то не дать выполнить
        if (purchasing.buyer != msg.sender) {
            Error(2, 'Access denied. This is not your purchasing');
            return false;
        }

        // отправляю эфиры покупателю
        purchasing.buyer.send(purchasing.value);
        RejectGetting(purchasing.value, purchasing.buyer, purchasing.seller);

        return true;
    }

    /* Возвращает деньги покупателю, если продавец не соизволил разблокировать деньги сам, может вызвать только сам же покупатель */
    // TODO поставить модификатор
    function returnMoney(uint idPurchasing) returns (bool) {

        // если идентификатор сделки превыщает их кол-во, то значит что запрашивается не существующая сделка, поэтому будет вызвана ошибка
        if (idPurchasing > purchasingListCount) {
            Error(1, 'Purchasing not found');
            return false;
        }

        Purchasing purchasing = purchasingList[idPurchasing];

        // если функцию вызывает не покупатель то не дать выполнить
        if (purchasing.buyer != msg.sender) {
            Error(2, 'Access denied. This is not your purchasing');
            return false;
        }

        // Проверка времени
        // время которое прошло после закладки денег
        var timeFromIn = now - purchasing.date;
        if (purchasing.sec > timeFromIn) {
            Error(3, 'Время сделки еще не вышло');
            return false;
        }

        // отправляю деньги покупателю
        purchasing.buyer.send(purchasing.value);
        ReturnMoney(purchasing.value, purchasing.buyer);

        return true;
    }

}