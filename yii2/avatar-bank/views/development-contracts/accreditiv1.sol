pragma solidity ^0.4.2;
/*
AvatarNetwork Copyright

https://avatarnetwork.io
*/


/* Родительский контракт */
contract Owned {

    /* Адрес владельца контракта*/
    address owner;

    /* Конструктор контракта, вызывается при первом запуске */
    function Owned() {
        owner = msg.sender;
    }

    /* Изменить владельца контракта, newOwner - адрес нового владельца */
    function changeOwner(address newOwner) onlyowner {
        owner = newOwner;
    }

    /* Модификатор для ограничения доступа к функциям только для владельца */
    modifier onlyowner() {
        if (msg.sender==owner) _;
    }
}

/* Основной контракт, наследует контракт Owned */
contract AvatarPay is Owned {

//    /* меппинг адрес (на кого) => СписокРетинговыхОценок */
//    mapping (address => RaitingList) public raiting;
//
//    /* Кол-во адресов */
//    uint256 raitingCount = 0;
//
//    /* сущность СписокРейтинговыхОценок содержащая один элемент list представляющий из себя массив оценок */
//    struct RaitingList {
////        uint length;                         // кол-во элементов в массиве list
//        mapping (uint => RaitingItem) list;  // массив рейтингов
//    }
//
//    /* сущность "Оценка" */
//    struct RaitingItem {
//        string hash;        // хеш документа
//        string link;        // ссылка на документ
//        address author;     // автор рейтинга
//        uint idPurchasing;  // идентификатор сделки
//        uint date;          // мгновение постановки рейтинга
//        uint rating;        // рейтинг от 0 до 200;
//        uint type1;          // В качестве кого представляется тот кому ставится рейтинг 1 - продавец, 2 - покупатель
//    }
//
    mapping (uint => Purchasing) public purchasingList;

    // Структура сделки
    struct Purchasing {
        address seller;
        address buyer;
        uint status;
        uint date;           // мгновение начала сделки
        uint256 value;          // кол-во эфира зарезервированное для сделки
        uint256 sec;            // Кол-во секунд на которое закладываются деньги
    }

    uint purchasingListCount;

    /* Событие при подписи документа участником, параметры - ID сделки, адрес кому предназначены деньги */
    event PurchasingRegistered(uint id, address member);

    /* Событие когда возникает ошибка */
    event Error(uint id, string message);

    /* Событие когда покупатель подтверждает получение товара */
    event ConfirmGetting(uint value, address from, address to);

    /* Событие когда продавец отменяет сделку в результате того что товар клиенту не понравился и товар вернулся продавцу */
    event RejectGetting(uint value, address from, address to);

    /* Событие когда покупатель возвращает деньги себе по истечение времени на сделку и не исполнение обязательств перед покупателем */
    event ReturnMoney(uint value, address to);

    /* Конструктор контракта, вызывается при первом запуске */
    function AvatarPay() {
        purchasingListCount = 0;
    }

    /* Пользователь закладывает деньги через функцию */
    /* @params address _to для кого закладываются деньги */
    /* @params uint days на какое кол-во дней закладываются деньги */
    function registerPurchasing(address _to, uint256 day) payable
    {
        // кол-во денег которые пользователь заложил
        uint value = msg.value;

        // Если денег не отправлено то значит и сделки нет
        if (value == 0) {
            Error(1, 'No money');
            return;
        }

        uint id = purchasingListCount + 1;

        // добавляю сделку
        purchasingList[id] = Purchasing({
            seller: _to,
            buyer: msg.sender,
            status: 1,
            date: now,
            value: msg.value,
            sec: (day * 60 * 60 * 24)
        });
        purchasingListCount = id;

        // вызываю событие
        PurchasingRegistered(id, _to);
    }

    function getPurchasing(uint idPurchasing) constant returns (
        address seller,
        address buyer,
        uint status,
        uint date,
        uint value,
        uint sec) {

        Purchasing m = purchasingList[idPurchasing];
        seller = m.seller;
        buyer = m.buyer;
        status = m.status;
        date = m.date;
        value = m.value;
        sec = m.sec;
    }

    /* Подтверждает получение товара и отправляет деньги продавцу */
    function confirmGetting(uint idPurchasing) returns (bool) {

        // если идентификатор сделки превыщает их кол-во, то значит что запрашивается не существующая сделка, поэтому будет вызвана ошибка
        if (idPurchasing > purchasingListCount) {
            Error(1, 'Purchasing not found');
            return false;
        }

        Purchasing purchasing = purchasingList[idPurchasing];

        // если функцию вызывает не покупатель то не дать выполнить
        if (purchasing.buyer != msg.sender) {
            Error(2, 'Access denied. This is not your purchasing');
            return false;
        }

        // отправляю эфиры
        // TODO функцию найти send
        purchasing.seller.send(purchasing.value);
        ConfirmGetting(purchasing.value, purchasing.buyer, purchasing.seller);

        return true;
    }

    /* Продавец отменяет сделку и деньги возвращаются покупателюПодтверждает получение товара и отправляет деньги продавцу */
    function rejectGetting(uint idPurchasing) returns (bool) {

        // если идентификатор сделки превыщает их кол-во, то значит что запрашивается не существующая сделка, поэтому будет вызвана ошибка
        if (idPurchasing > purchasingListCount) {
            Error(1, 'Purchasing not found');
            return false;
        }

        Purchasing purchasing = purchasingList[idPurchasing];

        // если функцию вызывает не покупатель то не дать выполнить
        if (purchasing.buyer != msg.sender) {
            Error(2, 'Access denied. This is not your purchasing');
            return false;
        }

        // отправляю эфиры покупателю
        purchasing.buyer.send(purchasing.value);
        RejectGetting(purchasing.value, purchasing.buyer, purchasing.seller);

        return true;
    }

    /* Возвращает деньги покупателю, если продавец не соизволил разблокировать деньги сам, может вызвать только сам же покупатель */
    // TODO поставить модификатор
    function returnMoney(uint idPurchasing) returns (bool) {

        // если идентификатор сделки превыщает их кол-во, то значит что запрашивается не существующая сделка, поэтому будет вызвана ошибка
        if (idPurchasing > purchasingListCount) {
            Error(1, 'Purchasing not found');
            return false;
        }

        Purchasing purchasing = purchasingList[idPurchasing];

        // если функцию вызывает не покупатель то не дать выполнить
        if (purchasing.buyer != msg.sender) {
            Error(2, 'Access denied. This is not your purchasing');
            return false;
        }

        // Проверка времени
        // время которое прошло после закладки денег
        var timeFromIn = now - purchasing.date;
        if (purchasing.sec > timeFromIn) {
            Error(3, 'Время сделки еще не вышло');
            return false;
        }

        // отправляю деньги покупателю
        purchasing.buyer.send(purchasing.value);
        ReturnMoney(purchasing.value, purchasing.buyer);

        return true;
    }

}


6060604052341561000f57600080fd5b336000806101000a81548173ffffffffffffffffffffffffffffffffffffffff021916908373ffffffffffffffffffffffffffffffffffffffff1602179055506000600281905550611031806100666000396000f300606060405260043610610082576000357c0100000000000000000000000000000000000000000000000000000000900463ffffffff1680628b33c4146100875780632570c2e8146100c25780633785fc1a146101745780637105689e146101af578063a6f9dae114610261578063b036e50c1461029a578063b970fe69146102d1575b600080fd5b341561009257600080fd5b6100a8600480803590602001909190505061030c565b604051808215151515815260200191505060405180910390f35b34156100cd57600080fd5b6100e360048080359060200190919050506105f5565b604051808773ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff1681526020018673ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff168152602001858152602001848152602001838152602001828152602001965050505050505060405180910390f35b341561017f57600080fd5b6101956004808035906020019091905050610671565b604051808215151515815260200191505060405180910390f35b34156101ba57600080fd5b6101d060048080359060200190919050506109ba565b604051808773ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff1681526020018673ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff168152602001858152602001848152602001838152602001828152602001965050505050505060405180910390f35b341561026c57600080fd5b610298600480803573ffffffffffffffffffffffffffffffffffffffff16906020019091905050610a4e565b005b6102cf600480803573ffffffffffffffffffffffffffffffffffffffff16906020019091908035906020019091905050610ae7565b005b34156102dc57600080fd5b6102f26004808035906020019091905050610d1c565b604051808215151515815260200191505060405180910390f35b600080600254831115610393577fc548eaddad03c946ad5228a88cfbd752439e312a7b29b7e8791a0b5fe143584c60016040518082815260200180602001828103825260148152602001807f50757263686173696e67206e6f7420666f756e640000000000000000000000008152506020019250505060405180910390a1600091506105ef565b6001600084815260200190815260200160002090503373ffffffffffffffffffffffffffffffffffffffff168160010160009054906101000a900473ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff161415156104a1577fc548eaddad03c946ad5228a88cfbd752439e312a7b29b7e8791a0b5fe143584c600260405180828152602001806020018281038252602a8152602001807f4163636573732064656e6965642e2054686973206973206e6f7420796f75722081526020017f70757263686173696e67000000000000000000000000000000000000000000008152506040019250505060405180910390a1600091506105ef565b8060010160009054906101000a900473ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff166108fc82600401549081150290604051600060405180830381858888f19350505050507f4b087b0cee2c94e0d48b043fad4ecbc7bb75fe90e3b627837dc172b0b6b9571481600401548260010160009054906101000a900473ffffffffffffffffffffffffffffffffffffffff168360000160009054906101000a900473ffffffffffffffffffffffffffffffffffffffff16604051808481526020018373ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff1681526020018273ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff168152602001935050505060405180910390a1600191505b50919050565b60016020528060005260406000206000915090508060000160009054906101000a900473ffffffffffffffffffffffffffffffffffffffff16908060010160009054906101000a900473ffffffffffffffffffffffffffffffffffffffff16908060020154908060030154908060040154908060050154905086565b60008060006002548411156106fa577fc548eaddad03c946ad5228a88cfbd752439e312a7b29b7e8791a0b5fe143584c60016040518082815260200180602001828103825260148152602001807f50757263686173696e67206e6f7420666f756e640000000000000000000000008152506020019250505060405180910390a1600092506109b3565b6001600085815260200190815260200160002091503373ffffffffffffffffffffffffffffffffffffffff168260010160009054906101000a900473ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff16141515610808577fc548eaddad03c946ad5228a88cfbd752439e312a7b29b7e8791a0b5fe143584c600260405180828152602001806020018281038252602a8152602001807f4163636573732064656e6965642e2054686973206973206e6f7420796f75722081526020017f70757263686173696e67000000000000000000000000000000000000000000008152506040019250505060405180910390a1600092506109b3565b81600301544203905080826005015411156108bd577fc548eaddad03c946ad5228a88cfbd752439e312a7b29b7e8791a0b5fe143584c600360405180828152602001806020018281038252602e8152602001807fd092d180d0b5d0bcd18f20d181d0b4d0b5d0bbd0bad0b820d0b5d189d0b520d081526020017fbdd0b520d0b2d18bd188d0bbd0be0000000000000000000000000000000000008152506040019250505060405180910390a1600092506109b3565b8160010160009054906101000a900473ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff166108fc83600401549081150290604051600060405180830381858888f19350505050507f70259ebbf839c315354476a55cbc2d985bc690c4c25b287f321fd3aeb993346d82600401548360010160009054906101000a900473ffffffffffffffffffffffffffffffffffffffff16604051808381526020018273ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff1681526020019250505060405180910390a1600192505b5050919050565b60008060008060008060006001600089815260200190815260200160002090508060000160009054906101000a900473ffffffffffffffffffffffffffffffffffffffff1696508060010160009054906101000a900473ffffffffffffffffffffffffffffffffffffffff169550806002015494508060030154935080600401549250806005015491505091939550919395565b6000809054906101000a900473ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff163373ffffffffffffffffffffffffffffffffffffffff161415610ae457806000806101000a81548173ffffffffffffffffffffffffffffffffffffffff021916908373ffffffffffffffffffffffffffffffffffffffff1602179055505b50565b6000803491506000821415610b6c577fc548eaddad03c946ad5228a88cfbd752439e312a7b29b7e8791a0b5fe143584c60016040518082815260200180602001828103825260088152602001807f4e6f206d6f6e65790000000000000000000000000000000000000000000000008152506020019250505060405180910390a1610d16565b600160025401905060c0604051908101604052808573ffffffffffffffffffffffffffffffffffffffff1681526020013373ffffffffffffffffffffffffffffffffffffffff168152602001600181526020014281526020013481526020016018603c80870202028152506001600083815260200190815260200160002060008201518160000160006101000a81548173ffffffffffffffffffffffffffffffffffffffff021916908373ffffffffffffffffffffffffffffffffffffffff16021790555060208201518160010160006101000a81548173ffffffffffffffffffffffffffffffffffffffff021916908373ffffffffffffffffffffffffffffffffffffffff16021790555060408201518160020155606082015181600301556080820151816004015560a08201518160050155905050806002819055507f93f22d77614060d6dfea1916d2afc4a62f975581a430de47d1b9cd767c273d588185604051808381526020018273ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff1681526020019250505060405180910390a15b50505050565b600080600254831115610da3577fc548eaddad03c946ad5228a88cfbd752439e312a7b29b7e8791a0b5fe143584c60016040518082815260200180602001828103825260148152602001807f50757263686173696e67206e6f7420666f756e640000000000000000000000008152506020019250505060405180910390a160009150610fff565b6001600084815260200190815260200160002090503373ffffffffffffffffffffffffffffffffffffffff168160010160009054906101000a900473ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff16141515610eb1577fc548eaddad03c946ad5228a88cfbd752439e312a7b29b7e8791a0b5fe143584c600260405180828152602001806020018281038252602a8152602001807f4163636573732064656e6965642e2054686973206973206e6f7420796f75722081526020017f70757263686173696e67000000000000000000000000000000000000000000008152506040019250505060405180910390a160009150610fff565b8060000160009054906101000a900473ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff166108fc82600401549081150290604051600060405180830381858888f19350505050507f0b54eab76b5a93d6b5f39909d80824aedfea21556c91e13ff66c952d5097cdd181600401548260010160009054906101000a900473ffffffffffffffffffffffffffffffffffffffff168360000160009054906101000a900473ffffffffffffffffffffffffffffffffffffffff16604051808481526020018373ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff1681526020018273ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff168152602001935050505060405180910390a1600191505b509190505600a165627a7a72305820fb0314126a66af74393f44415e45361f25d33e0b756a91b7c969e7ca535b6d890029
[{"constant":false,"inputs":[{"name":"idPurchasing","type":"uint256"}],"name":"rejectGetting","outputs":[{"name":"","type":"bool"}],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":true,"inputs":[{"name":"","type":"uint256"}],"name":"purchasingList","outputs":[{"name":"seller","type":"address"},{"name":"buyer","type":"address"},{"name":"status","type":"uint256"},{"name":"date","type":"uint256"},{"name":"value","type":"uint256"},{"name":"sec","type":"uint256"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":false,"inputs":[{"name":"idPurchasing","type":"uint256"}],"name":"returnMoney","outputs":[{"name":"","type":"bool"}],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":true,"inputs":[{"name":"idPurchasing","type":"uint256"}],"name":"getPurchasing","outputs":[{"name":"seller","type":"address"},{"name":"buyer","type":"address"},{"name":"status","type":"uint256"},{"name":"date","type":"uint256"},{"name":"value","type":"uint256"},{"name":"sec","type":"uint256"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":false,"inputs":[{"name":"newOwner","type":"address"}],"name":"changeOwner","outputs":[],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":false,"inputs":[{"name":"_to","type":"address"},{"name":"day","type":"uint256"}],"name":"registerPurchasing","outputs":[],"payable":true,"stateMutability":"payable","type":"function"},{"constant":false,"inputs":[{"name":"idPurchasing","type":"uint256"}],"name":"confirmGetting","outputs":[{"name":"","type":"bool"}],"payable":false,"stateMutability":"nonpayable","type":"function"},{"inputs":[],"payable":false,"stateMutability":"nonpayable","type":"constructor"},{"anonymous":false,"inputs":[{"indexed":false,"name":"id","type":"uint256"},{"indexed":false,"name":"member","type":"address"}],"name":"PurchasingRegistered","type":"event"},{"anonymous":false,"inputs":[{"indexed":false,"name":"id","type":"uint256"},{"indexed":false,"name":"message","type":"string"}],"name":"Error","type":"event"},{"anonymous":false,"inputs":[{"indexed":false,"name":"value","type":"uint256"},{"indexed":false,"name":"from","type":"address"},{"indexed":false,"name":"to","type":"address"}],"name":"ConfirmGetting","type":"event"},{"anonymous":false,"inputs":[{"indexed":false,"name":"value","type":"uint256"},{"indexed":false,"name":"from","type":"address"},{"indexed":false,"name":"to","type":"address"}],"name":"RejectGetting","type":"event"},{"anonymous":false,"inputs":[{"indexed":false,"name":"value","type":"uint256"},{"indexed":false,"name":"to","type":"address"}],"name":"ReturnMoney","type":"event"}]
0.4.19+commit.c4cbbb05