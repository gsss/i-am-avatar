# Таблицы БД

Описания таблиц содержится в таблице `table`.

{%table name="table" %}

Класс который показывает таблицу: `\common\services\documentation\DbTable`

В нем сейчас будет скинироваться таблицы и переносится в `table`.

information_schema.TABLES

information_schema.COLUMNS