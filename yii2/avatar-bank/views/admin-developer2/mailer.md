# Почта

`mailer`

Проблематика: в консоли параметры почты могут отличаться от того что в приложении указано.

Если `Yii::$app->mailer->transport->username != Yii::$app->params->mailer->from` то выдаст ошибку.

Поэтому чтобы решить эту проблему я записываю в очередь настройки почтовика. В поле `mailer`.

```json
{
    transport: {
        host: "",
        port: "",
        username: "",
        password: ""
    }
}
```

