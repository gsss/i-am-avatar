# Целевые программы

Добавляются в разделе "Кооператив / Целевые программы". Ссылка: `/cabinet-school-koop-aim-programs/index`

Таблица:
- `school_koop_aim`
- `school_koop_program`
- id
- school_id
- name
- document
- link - ссылка
- created_at  

В целевых программах находятся "проекты"

- `school_koop_project`
- id
- school_id
- program_id
- name
- document
- link
- author_id
- type_id


К проекту прикрепляется "благотворительная программа"

# Благотворительные программы

`\common\models\SchoolBlagoProgram`
`school_blago_program`
- id
- school_id
- project_id - связь на проект: 1 к 0-1
- name
- success_url
- error_url

{%table name="school_blago_program" %}

{%table name="school_blago_input" %}