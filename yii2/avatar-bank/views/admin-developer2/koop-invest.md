# Инвестиционный сценарий

Предназначена для сбора инвестиций в проект или в программу.

Присоединяется к разделу "проекты" и "программа".

Сущность: Настройки инвестиционного сценария
Таблица: `school_invest_program`
Модель: `\common\models\SchoolInvestProgram`

Поля:
- id
- name
- created_at
- document
- success_url
- error_url

Для каждого инвестиционного сценария формируется инвестиционное событие.

Сущность: Инвестиционное событие
Таблица: `school_invest_event`
Модель: `\common\models\SchoolInvestEvent`

Поля:
- id
- program_id
- amount
- currency_id
- created_at
- document
- user_id

Инвестиционный сценарий присоединяется в проект путем указания там поля `invest_program_id`.

Как приходят деньги?
На сайте сообщества есть форма где клиенты могут сделать инвестицию.
Эта форма только для авторизованных пользователей.
Форма находится по ссылке `/invest-program?id={id}`
где `id` - это идентификатор `Инвестиционного сценария`

https://app.diagrams.net/#G1PXfKIdgvIbLtAXiFQhqtkjE20iYxSoTj

![](/images/controller/admin-developer2/koop-invest/invest_program.png)

# Настройка программы

# Сценарий инвестиции