# 1c в кооперативе

Список сущностей для синхронизации с 1С:
1. Контрагенты
2. Договора
3. Номенклатура/товары
4. Оплаты (ввод/вывод пая)
5. Акты сверок (наличие товара)
6. Регистрация в кооперативе
7. Возврат товара и оплаты

## API

https://app.swaggerhub.com/apis/i-avatar777/cooperative-api/1.0.0

https://Ra-m-ha@bitbucket.org/Ra-m-ha/koop_1c_api.git

`git@bitbucket.org:Ra-m-ha/koop_1c_api.git`