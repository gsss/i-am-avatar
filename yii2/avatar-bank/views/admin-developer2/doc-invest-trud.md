# Документ по инвестициям и учету трудозатратам

Этот документ описывает Как учитываются денежные и трудовые вложения каждого участника движения Качество Жизни.

## Введение и предпосылки

В жизни любой организации всегда есть периуд становления и в этот периуд нет денежных ресурсов для того чтобы запустить рабочий процесс, закупить материалы и привлечь проффессиональных сотрудников для проведения работ.

На энтузиазме мадо кто может продержаться, да и если вклад не учивается это значит что и вложенный труд не ценится, то есть обесценивается.

Но в связи с появлением в законодательном поле понятия цифровых прав это можно использовать.

И вот как. 

Вводится понятие "Цифровое право на получение вознаграждение по факту выхода общего дело на стадию прибыли".

Расшифрую это понятие по частям. 

Цифровое означает что учет ведется в электронно-цифровом виде при помощи Электронных подписей, что обеспечивает надежность учета без вмешательства третьей стороны в процедуру учета.

Право на получение вознаграждения означает что человек который вносит в общее дело свою инвестицию в виде денег или труда получает возможность и инструмент реализации возврата этих вложений после того как общее дело выйдет на стадию прибыли. Это право обеспечивается путем введения учетной единицы инвестиций и трудозатрат.

Стадия прибыли означает что Кол-во входящих денег и произведенных товаров потребления при помощи вложенного труда превышает Кол-во растрат на закупку оборудования и оплату труда.

## Способ реализации

Для того чтобы обеспечить учет вкладов, это можно сделать при помощи двух способов.
1. Одна учетная единица приравнивается одному рублю. Этот способ проще для понимания, но подвержен инфляционной составляющей обусловленной привязкой к индексу капиталистической экономики.
2. Одна учетная единица приравнивается индексу совокупности ресурсов для производства миниманого уровня Качества жизни для одного человека. Этот способ сложнее для понимания, и будет понятен только после прочтения всего документа, но является более стабильным показателем. 

Начну с разборе первого способа учета.

Здесь все просто: формируем три учетные единицы:
1. Вклад в виде денег. Одна единица учета = Одному рублю, что вытекает из правила способа.
2. Вклад в виде товара или ресурса. Товар или ресурс оценивается в рублях и зачисляется соответствующее кол-во учетных единиц.
3. Вклад в виде труда. Труд оценивается в трудочасах, трудочас оценивается в ставке за соответствующий труд. По факту выполнения труда соответствующее кол-во учетных единиц зачисляется на счет участника движения.

Для понимания формирования и движения денежной массы, давайте разберемся в том что это такое и как работает.

## Денежная масса и способ ее передвижения

Возьмем множество кошельков SW и одну учетную единицу например "учет денег" первый из списка указанного выше.
В начале сумма всех учетных единиц на всех кошельках равна 0.

Далее есть две возможности реализовать учет по факту поступления инвестиции.

1. Производится эмиссия учетной единицы на кошельке инвестора в соответствующем количестве.
2. Изначально производится эмиссия денежной массы учетных единиц на каком то кошельке кто будет начислять соответствующее количество учетных единиц на кошелек инвестора.

Всего существует две операции 