# Инвестиционные программы

Инвестиционная программа это проект целевой программы для инвестирования.

В виду того что пользователь инвестируя в нас получает цифровые права на пользование платформой, то для того чтобы их ему передать его надо зарегистрировать или надо чтобы он вошел в систему.

Поэтому сначала пользователь авторизуется а потом переводит нам деньги.

Форма инвестирования располагается по ссылке `/invest-program?id=#`
где id это school_invest_program.id

`\common\models\InvestProgram`
`school_invest_program`
- id
- school_id
- project_id
- name
- success_url
- error_url

{%table name="school_invest_program" %}


![](/images/controller/admin-developer2/koop-invest/i.png)

{%table name="school_invest_input" %}
- currency_id - dbWallet.currency.id