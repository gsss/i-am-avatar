<?php
/**
 * Created by PhpStorm.
 * User: Бог-Творец
 * Date: 04.01.2017
 * Time: 18:54
 */
/* @var $this yii\web\View */
/* @var $file string */


$c = file_get_contents(__DIR__ . '/' . $file . '.md');

$rows = explode( "\n", $c);
if (\yii\helpers\StringHelper::startsWith($rows[0], '# ')) {
    $this->title = substr($rows[0], 2);
    array_shift($rows);
    $r = array_splice($rows, 1);
    $c = join("\n", $r);
} else {
    $this->title = 'Документ';
}

?>
<div class="container">

    <div class="col-lg-3">

        <p>Главная</p>
        <ul>
            <li><a href="chat">chat</a></li>
            <li><a href="api-component">api-component</a></li>
            <li><a href="api-component-fns">api-component-fns</a></li>
        </ul>
        <p>Сообщество</p>
        <ul>
            <li><a href="school-url">Доменное имя</a></li>
            <li><a href="school-blago-program">Благотворительные программы</a></li>
            <li><a href="invest-program">Инвестиционные программы</a></li>
            <li><a href="school-shop-images">Товары. Картинки</a></li>
            <li><a href="school-shop-auth">Магазин. Авторизация</a></li>
            <li><a href="school-shop-pay">Магазин. Оплата</a></li>
            <li><a href="tarif">Сервисы и тарифы</a></li>
        </ul>

        <p>Кооператив</p>
        <ul>
            <li><a href="koop">Введение</a></li>
            <li><a href="koop-aim">Целевые программы</a></li>
            <li><a href="koop-wallets">Кошельки</a></li>
            <li><a href="koop-invest">Инвестиции</a></li>
            <li>Сценарии:</li>
            <ul>
                <li><a href="koop-user-flow-reg">Вступление в кооператив физ лица</a></li>
            </ul>
            <li><a href="koop-1c">1С</a></li>
            <li><a href="koop-widget">Платежный виджет в кооперативе</a></li>
        </ul>

        <p>Программирование</p>
        <ul>
            <li><a href="programming-install">Установка</a></li>
            <li><a href="programming-env">ENV</a></li>
        </ul>
        <p>Числовые подписи</p>
        <ul>
            <li><a href="rsa">RSA</a></li>
        </ul>

    </div>
    <div class="col-lg-9">
        <h1 class="page-header"><?= \yii\helpers\Html::encode($this->title) ?></h1>

        <?php
        $Parsedown = new Parsedown();
        echo $Parsedown->text($c);
        ?>

    </div>
</div>
