<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;
use app\models\UnionCategory;
use yii\db\Query;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \avatar\models\forms\AvatarHash */

$this->title = 'Добавить аватар';

?>
<div class="container">
    <div class="col-lg-12">
        <h1 class="page-header text-center"><?= Html::encode($this->title) ?></h1>


        <div class="row">
            <div class="col-lg-6 col-lg-offset-3">
                <?php if (!is_null($id = Yii::$app->session->getFlash('form'))) : ?>

                    <div class="alert alert-success">
                        Успешно добавлено.
                    </div>
                    <a class="btn btn-default" href="<?= \yii\helpers\Url::to(['data-base/avatar-view', 'id' => $id])?>">Посмотреть</a>
                    <a class="btn btn-default" href="<?= \yii\helpers\Url::to(['data-base/avatar'])?>">Список</a>
                <?php else: ?>

                    <?php $form = ActiveForm::begin([
                        'options' => ['enctype' => 'multipart/form-data']
                    ]); ?>
                    <?= $model->field($form, 'image') ?>
                    <?= $model->field($form, 'data')->textarea(['rows' => 20]) ?>

                    <hr>
                    <div class="form-group">
                        <?= Html::submitButton('Добавить', [
                            'class' => 'btn btn-default',
                            'name'  => 'contact-button',
                            'style' => 'width:100%',
                        ]) ?>
                    </div>
                    <?php ActiveForm::end(); ?>


                <?php endif; ?>

            </div>
        </div>

    </div>
</div>
