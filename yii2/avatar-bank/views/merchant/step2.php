<?php
/**
 * Created by PhpStorm.
 * User: Бог-Творец
 * Date: 07.01.2017
 * Time: 4:38
 *

'merchant'   => $merchant,
'title'      => $title,
'label'      => $label,
'data'       => $data,
'price'      => $price,
'currency'   => $currency,
'successUrl' => $successUrl,
'errorUrl'   => $errorUrl,

 */

/* @var $price float */
/* @var $label string */
/* @var $title string */
/* @var $merchant \common\models\avatar\UserBillMerchant */
/* @var $this yii\web\View */

$this->title = 'Мерчант';

$payConfig = 3;

?>
<style>
    .download-section {
        width: 100%;
        padding: 50px 0;
        color: #fff;
        background: url('<?= $merchant->image_top ?>') no-repeat center center scroll;
        background-color: #000;
        -webkit-background-size: cover;
        -moz-background-size: cover;
        background-size: cover;
        -o-background-size: cover;
    }
</style>
<section id="contact" class="content-section text-center" style="">
    <div class="download-section" style="
    border-bottom: 1px solid #87aad0;
    border-top: 1px solid #87aad0;
    height: 300px;
    ">
        <div class="container">
            <div class="col-lg-8 col-lg-offset-2" style="color: #000000">

            </div>
        </div>
    </div>
</section>
<div class="container" style="padding-bottom: 70px;">
    <div class="col-lg-12">
        <h1 class="page-header text-center">
            <?= $merchant->name ?>
        </h1>
        <h2 class="page-header text-center">
            Список платежных систем
        </h2>

        <?php
        $this->registerJs(<<<JS
    $('.paymentType').click(function () {
        $('#paymentType').attr('value', $(this).data('value'));
        if (!$(this).hasClass('active')) {
            $('.paymentType').removeClass('active');
            $(this).addClass('active');
        }
    });
JS
        );
        ?>
        <input type="hidden" value="" id="paymentType">
        <?php /** @var \common\models\PaySystem $paySystem */ ?>
        <?php
        $this->registerJs(<<<JS
$('.rowTable').click(function() {
    $('#paymentType').val($(this).data('id'));
    $(this).parent().find('.js-radio').prop('checked','');
    $(this).find('.js-radio').prop('checked', 'checked');
});
$('.buttonSuccess').click(function(e) {
    if ($('#paymentType').val() == '') {
        alert('нужно выбрать платежную систему');
        return;
    }
    ajaxJson({
        url: '/merchant/step2-ajax',
        data: {
            paymentType: $('#paymentType').val(),
            product: 1
        },
        success: function(ret) {
            window.location = '/merchant/step3?id=' + ret.id;
        }
    });
});
JS
        );
        ?>
        <?= \yii\grid\GridView::widget([
            'showHeader'   => false,
            'summary'      => '',
            'dataProvider' => new \yii\data\ActiveDataProvider([
                'query' => \common\models\PaySystem::find()
                    ->select([
                        'paysystems_config.id',
                        'paysystems.code',
                        'paysystems.title',
                        'paysystems.image',
                    ])
                    ->innerJoin('paysystems_config', 'paysystems_config.paysystem_id = paysystems.id')
                    ->andWhere(['parent_id' => $payConfig])
            ]),
            'tableOptions' => [
                'class' => 'table table-hover table-striped',
            ],
            'rowOptions'   => function ($item) {
                $data = [
                    'data'  => ['id' => $item['id']],
                    'role'  => 'button',
                    'class' => 'rowTable'
                ];

                return $data;
            },
            'columns'      => [
                [
                    'content' => function ($item) {
                        return \yii\helpers\Html::radio(null, false, [
                            'value' => \yii\helpers\ArrayHelper::getValue($item, 'id'),
                            'class' => 'js-radio',
                        ]);
                    }
                ],
                [
                    'attribute' => 'image',
                    'format'    => ['image', ['width' => 100, 'class' => 'thumbnail', 'style' => 'margin-bottom:0px;']]
                ],
                'title',
            ]
        ]) ?>

        <hr>
        <button class="btn btn-success buttonSuccess">Оплатить</button>
    </div>
</div>
