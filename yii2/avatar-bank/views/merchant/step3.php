<?php
/**
 * Created by PhpStorm.
 * User: Бог-Творец
 * Date: 07.01.2017
 * Time: 4:38
 *
 * 'merchant'   => $merchant,
 * 'title'      => $title,
 * 'label'      => $label,
 * 'data'       => $data,
 * 'price'      => $price,
 * 'currency'   => $currency,
 * 'successUrl' => $successUrl,
 * 'errorUrl'   => $errorUrl,
 */
use yii\helpers\Html;

/* @var $request \common\models\MerchantRequest */
/* @var $merchant \common\models\avatar\UserBillMerchant */

/* @var $this yii\web\View */
$this->title = 'Заказ #' . $request->id;

$paySystem = \common\models\PaySystem::findOne($request->paysystem_id);
$source = $paySystem->getClass([
    'config' => [
        'merchant' => $merchant,
    ],
]);
Yii::trace('start', 'avatar\\payments\bitcoin\\page\\step3');

?>
<style>
    .download-section {
        width: 100%;
        padding: 50px 0;
        color: #fff;
        background: url('<?= $merchant->image_top ?>') no-repeat center center scroll;
        background-color: #000;
        -webkit-background-size: cover;
        -moz-background-size: cover;
        background-size: cover;
        -o-background-size: cover;
    }
</style>
<section id="contact" class="content-section text-center" style="">
    <div class="download-section" style="
    border-bottom: 1px solid #87aad0;
    border-top: 1px solid #87aad0;
    height: 300px;
    ">
        <div class="container">
            <div class="col-lg-8 col-lg-offset-2" style="color: #000000">

            </div>
        </div>
    </div>
</section>
<div class="container" style="padding-bottom: 70px;">
    <div class="col-lg-12">
        <h1 class="page-header text-center">
            <?= $merchant->name ?>
        </h1>
    </div>
    <div class="col-lg-6 col-lg-offset-3">
        <?= $source->getForm(\common\models\BillingMain::findOne($request->billing_id), $request->title, $merchant->getBilling()->name, json_encode([
            'merchant_id' => $merchant->id,
        ]), [
            'action' => 'merchantRequest' . '.' . $request->id,
        ]) ?>

        <hr>
        <p class="text-center lead" style="margin-top: 50px;">Перевод по карте</p>


        <div class="form-group field-card_number">
            <label class="control-label" for="profilephone-card_number">Номер карты:</label>
            <input class="form-control" name="card_number" id="card-number">
            <div class="errorString help-block help-block-error"></div>
        </div>

        <hr>
        <?php
        $this->registerJs(<<<JS
$('#card-number').on('focus', function(e) {
  $('.errorString').html('');
})
JS
        );
        $merchant_id = $merchant->id;
        $amount = $request->price;
        $request_id = $request->id;
        $this->registerJs(<<<JS
var timer = 20;
$('#card-number').on('focus', function() {
    var prefix = 'card_number';
    $('.field-' + prefix).removeClass('has-error');
    $('.field-' + prefix + ' .help-block-error').hide();
});
$('#buttonPay').click(function(e) {
    ajaxJson({
        url: '/merchant/step3-ajax',
        data: {
            merchant_id: {$merchant_id},
            card_number: $('#card-number').val(),
            amount: {$amount},
            request_id: {$request_id}
        },
        success: function(ret) {
            $('#modalSend').modal();
            
            $('#spanTimer').html(timer);
            setInterval(function() {
                timer--;
                if (timer == 0) {
                    $('.alertCounter').hide();
                    $('.buttonSendMore').removeClass('hide').show();
                } else {
                    $('#spanTimer').html(timer);
                    return false;
                }
            }, 1000);
        },
        errorScript: function(ret) {
            var prefix = 'card_number';
            switch (ret.id) {
                case 101:
                    $('.field-' + prefix).addClass('has-error');
                    $('.field-' + prefix + ' .help-block-error').html(ret.data).show();
                    
                    break;
                case 103:
                    $('.field-' + prefix).addClass('has-error');
                    $('.field-' + prefix + ' .help-block-error').html(ret.data).show();

                    break;
                case 102:
                    $.each(ret.data,function(i,v) {
                        $('.field-' + prefix).addClass('has-error');
                        $('.field-' + prefix + ' .help-block-error').html(v.join('<br>')).show();
                    });

                    break;
            }
        }
    });
});
var functionButtonCheck = function() {
    var b = $(this);
    b.off('click');
    b.attr('disabled','disabled');
    
    ajaxJson({
        url: '/merchant/step3-sms-check',
        data: {
            sms: $('#profilephone-sms').val()
        },
        success: function(ret) {
            console.log(ret);
            $('#modalSend').on('hidden.bs.modal', function() {
                $('#modalOk').on('hidden.bs.modal', function() {
                    window.location = ret.return_url;
                }).modal();
            }).modal('hide');
        },
        errorScript: function(ret) {
            b.on('click', functionButtonCheck);
            b.removeAttr('disabled');
            if (ret.id == 101) {
                $('.field-profilephone-sms .help-block-error').html(ret.data);
                $('.field-profilephone-sms .help-block-error').show();
                $('.field-profilephone-sms').addClass('has-error');
            }
            if (ret.id == 102 || ret.id == 103) {
                $('.field-profilephone-sms .help-block-error').html(ret.data);
                $('.field-profilephone-sms .help-block-error').show();
                $('.field-profilephone-sms').addClass('has-error');
            }
        }
    })
};
$('.buttonCheck').click(functionButtonCheck);
$('.buttonSendMore').click(function() {
    timer = 20;
    ajaxJson({
        url: '/merchant/step3-sms-more',
        success: function(ret) {
            $('.field-profilephone-sms').removeClass('has-error');
            $('.field-profilephone-sms .help-block-error').hide();
            $('.buttonSendMore').hide();
            $('.alertCounter').show();
            $('#spanTimer').html(timer);
        }
    });
});
$('#profilephone-sms').on('focus', function(e){
    $('.field-profilephone-sms').removeClass('has-error');
    $('.field-profilephone-sms .help-block-error').hide();
});
JS
        );
        ?>
        <button class="btn btn-success" id="buttonPay" style="width: 100%;margin-bottom: 50px;">Оплатить</button>
    </div>

        <div class="modal fade" tabindex="-1" role="dialog" id="modalSend">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                    aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title">Оплата счета</h4>
                    </div>
                    <div class="modal-body">
                        <p class="alert alert-success">Вам на номер была выслана СМС с кодом подтверждения сделки. Введите ее в поле ниже:</p>


                        <div class="form-group field-profilephone-sms" style="margin-bottom: 10px;">
                            <label class="control-label" for="profilephone-sms">Код из СМС</label>
                            <input type="text" id="profilephone-sms" class="form-control"
                                   value="" size="4"
                            >
                            <div class="help-block help-block-error"></div>
                        </div>


                        <div class="form-group groupSend" style="margin-bottom: 10px;">
                            <p class="alert alert-success alertCounter">Если СМС не пришло, через <span id="spanTimer">20</span> сек можно запросить еще одну СМС, код остается прежним</p>
                            <?= Html::button('Выслать еще раз', [
                                'class' => 'btn btn-default buttonSendMore hide',
                                'style' => 'width:100%',
                            ]) ?>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button
                                type="button"
                                class="btn btn-primary buttonCheck"
                                style="width: 100%;">Подтвердить</button>
                    </div>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div><!-- /.modal -->

        <div class="modal fade" tabindex="-1" role="dialog" id="modalOk">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                    aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title">Оплата счета</h4>
                    </div>
                    <div class="modal-body">
                        <p class="alert alert-success">Платеж успешно сделан</p>
                    </div>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div><!-- /.modal -->


    </div>
</div>
