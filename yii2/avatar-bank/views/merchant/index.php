<?php
/**
 * Created by PhpStorm.
 * User: Бог-Творец
 * Date: 07.01.2017
 * Time: 4:38
 *
 * 'merchant'   => $merchant,
 * 'title'      => $title,
 * 'label'      => $label,
 * 'data'       => $data,
 * 'price'      => $price,
 * 'currency'   => $currency,
 * 'successUrl' => $successUrl,
 * 'errorUrl'   => $errorUrl,
 */

/* @var $data string */
/* @var $key string */
/* @var $price float не обязательное, мб null */
/* @var $label string */
/* @var $title string не обязательное, мб null */
/* @var $currency string */
/* @var $merchant \common\models\avatar\UserBillMerchant */
/* @var $this yii\web\View */

$this->title = 'Мерчант';

$decimals = \common\models\avatar\Currency::getListDecimals()[$currency];

$this->render('../blocks/shareMeta', [
    'image'       => \yii\helpers\Url::to($merchant->image_top, true),
    'title'       => $merchant->name,
    'url'         => \yii\helpers\Url::current([], true),
    'description' => 'Мерчант для приема платежей на счет ' . $merchant->name,

])

/**
 * Если
 */
?>
<style>
    .download-section {
        width: 100%;
        padding: 50px 0;
        color: #fff;
        background: url('<?= $merchant->image_top ?>') no-repeat center center scroll;
        background-color: #000;
        -webkit-background-size: cover;
        -moz-background-size: cover;
        background-size: cover;
        -o-background-size: cover;
    }
</style>
<section id="contact" class="content-section text-center" style="">
    <div class="download-section" style="
    border-bottom: 1px solid #87aad0;
    border-top: 1px solid #87aad0;
    height: 300px;
    ">
        <div class="container">
            <div class="col-lg-8 col-lg-offset-2" style="color: #000000">

            </div>
        </div>
    </div>
</section>
<div class="container" style="padding-bottom: 70px;">
    <div class="col-lg-12">
        <h1 class="page-header text-center">
            <?= $merchant->name ?>
        </h1>
    </div>
    <div class="col-lg-6 col-lg-offset-3">

        <table class="table table-hover table-striped">
            <tr>
                <td><?= Yii::t('c.FJivP9kpZN', 'Платеж') ?></td>
                <td>
                    <?php if (empty($title)) { ?>
                        <input type="text" class="form-control" id="title">
                    <?php } else { ?>
                        <?= $title ?>
                    <?php } ?>
                </td>
            </tr>
            <tr>
                <td><?= Yii::t('c.FJivP9kpZN', 'Сумма') ?></td>
                <td>
                    <?php if (empty($price)) { ?>
                        <div class="input-group">
                            <input type="text"
                                   name="amount"
                                   class="form-control"
                                   aria-label="..."
                                   placeholder="0.00000001"
                                   autocomplete="off"
                                   id="recipient-amount"
                                   style="font-family: Consolas, Courier New, monospace">
                            <input type="hidden"
                                   name="currency"
                                   id="recipient-currency"
                                   value="BTC"
                            >
                            <div class="input-group-btn">
                                <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown"
                                        aria-haspopup="true"
                                        aria-expanded="false"><span class="buttonCurrency">BTC</span> <span
                                            class="caret"></span></button>
                                <ul class="dropdown-menu dropdown-menu-right">
                                    <?php

                                    $this->registerJs(<<<JS
var currency = 'BTC';
var functionUpdate = function() {
    console.log(123); 
    $('.buttonNext').attr('data-data', JSON.stringify({
        title: $('#title').val(),
        price: $('#recipient-amount').val(),
        key: '{$key}',
        label: '{$label}',
        data: '{$data}',
        currency: currency
     }));
};
$('.itemCurrency').click(function(e) {
    $('#recipient-currency').val($(this).data('code'));
    currency = $(this).data('code');
    $('.buttonCurrency').html($(this).html());
    var i,s='',c=$(this).data('decimals');
    for(i = 0; i < c; i++) {
        s = s + '0';
    }
    $('#recipient-amount').attr('placeholder', '0.'+ s);
    functionUpdate();
});

$('#title').on('input', functionUpdate);
$('#recipient-amount').on('input', functionUpdate);
JS
                                    );
                                    ?>

                                    <?php /** @var \common\models\avatar\Currency $currency */ ?>
                                    <?php foreach (\common\models\avatar\Currency::find()->where(['is_merchant' => 1])->all() as $currency) { ?>
                                        <li>
                                            <a
                                                    href="javascript:void(0);"
                                                    class="itemCurrency"
                                                    data-code="<?= $currency->code ?>"
                                                    data-kurs="<?= $currency->kurs ?>"
                                                    data-decimals="<?= $currency->decimals ?>"
                                                    data-id="<?= $currency->id ?>">
                                                <?= $currency->code ?>
                                            </a>
                                        </li>
                                    <?php } ?>
                                </ul>
                            </div><!-- /btn-group -->
                        </div><!-- /input-group -->


                        <div class="help-block help-block-error "></div>

                    <?php } else { ?>
                        <?= Yii::$app->formatter->asDecimal($price, $decimals) . ' ' . $currency ?>
                    <?php } ?>
                </td>
            </tr>
        </table>

        <hr>
        <?php
        $this->registerJs(<<<JS
$('.buttonNext').click(function(e) {
    ajaxJson({
        url: '/merchant/index-ajax',
        data: $(this).data('data'),
        success: function(ret) {
            window.location = '/merchant/step3' + '?' + 'id' + '=' + ret.id
        }
    });
});
JS
        );
        ?>
        <button class="btn btn-success btn-lg buttonNext" style="width: 100%;"
                data-data="<?= \yii\helpers\Html::encode(json_encode([
                    'title'    => $title,
                    'price'    => $price,
                    'key'      => $key,
                    'label'    => $label,
                    'data'     => $data,
                    'currency' => $currency,
                ], JSON_UNESCAPED_UNICODE)) ?>">
            <?= \Yii::t('c.FJivP9kpZN', 'Перейти к оплате') ?>
        </button>

    </div>
</div>
