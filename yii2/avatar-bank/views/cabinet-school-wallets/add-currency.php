<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;
use app\models\UnionCategory;
use yii\db\Query;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \avatar\models\forms\school\Potok */
/* @var $kurs \common\models\school\Kurs */
/* @var $school \common\models\school\School */

$this->title = 'Добавить поток';

?>
<div class="container">

    <?php \avatar\widgets\SchoolMenu::begin(['school' => $school]) ?>
    <h1 class="page-header"><?= Html::encode($this->title) ?></h1>
    <?= \cs\Widget\BreadCrumbs\BreadCrumbs::widget([
        'items' => [
            [
                'label' => 'Школы',
                'url'   => '/cabinet-school/index',
            ],
            [
                'label' => $school->name,
                'url'   => ['cabinet-school/view', 'id' => $school->id],
            ],
            [
                'label' => 'Курсы',
                'url'   => ['cabinet-school-kurs-list/index', 'id' => $school->id],
            ],
            [
                'label' => $kurs->name,
                'url'   => ['cabinet-school-kurs-list/edit', 'id' => $kurs->id],
            ],
            [
                'label' => 'Потоки',
                'url'   => ['cabinet-school-potok/index', 'id' => $kurs->id],
            ],
            'Добавить',
        ],
        'home'  => [
            'name' => '<i class="glyphicon glyphicon-home"></i>',
            'url'  => '/cabinet/index',
        ],
    ]) ?>
    <hr>

    <?php if (!is_null($id = Yii::$app->session->getFlash('form'))) : ?>

        <div class="alert alert-success">
            Успешно добавлено.
        </div>

        <p>
            <a href="<?= \yii\helpers\Url::to(['cabinet-school-potok/index', 'id' => $kurs->id]) ?>" class="btn btn-success">Все потоки</a>
        </p>

    <?php else: ?>


        <div class="row">
            <div class="col-lg-8">
                <?php $form = ActiveForm::begin([
                    'options' => ['enctype' => 'multipart/form-data'],
                ]); ?>
                <?= $model->field($form, 'name') ?>
                <?= $model->field($form, 'date_start') ?>
                <?= $model->field($form, 'date_end') ?>


                <hr>
                <div class="form-group">
                    <?= Html::submitButton('Добавить', [
                        'class' => 'btn btn-default',
                        'name'  => 'contact-button',
                        'style' => 'width:100%',
                    ]) ?>
                </div>
                <?php ActiveForm::end(); ?>
            </div>
        </div>

    <?php endif; ?>
    <?php \avatar\widgets\SchoolMenu::end() ?>


</div>
