<?php

/** @var $this \yii\web\View */

/** @var $school \common\models\school\School */
/** @var $input \common\models\school\Input */

use yii\helpers\Url;
use app\services\GsssHtml;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use app\services\GetArticle\Collection;

$this->title = 'Кошельки';

$this->registerJS(<<<JS

JS
);
?>

<div class="container">

    <?php \avatar\widgets\SchoolMenu::begin(['school' => $school]) ?>
    <h1 class="page-header"><?= Html::encode($this->title) ?></h1>

    <?php if (!\cs\Application::isEmpty($school->currency_id)) { ?>
        <?php if (\cs\Application::isEmpty($school->fund_rub)) { ?>
            <p>
                <button
                        class="btn btn-default buttonAddFundRub">Создать фонд развития рублевый</button>
            </p>
        <?php } else { ?>
            <p>
                <a href="/cabinet-school-wallets/add-input?id=<?= $school->id ?>"
                   class="btn btn-default">Добавить приход</a>
            </p>
        <?php } ?>
    <?php } else { ?>
        <p>
            <a href="<?= Url::to(['cabinet-school-wallets/add-currency', 'id' => $school->id]) ?>"
               class="btn btn-default">Добавить монету в школу</a>
        </p>
    <?php } ?>

    <?php \yii\widgets\Pjax::begin(); ?>
    <?php
    $this->registerJS(<<<JS
$('[data-toggle="tooltip"]').tooltip();

JS
    );
    ?>
    <?= \yii\grid\GridView::widget([
        'dataProvider' => new \yii\data\ActiveDataProvider([
            'query' => \common\models\school\InputTransaction::find()->where(['input_id' => $input->id])
            ,
        ]),
        'tableOptions' => [
            'class' => 'table table-striped table-hover',
        ],
        'rowOptions'   => function ($item) {
            $data = [
                'data'  => ['id' => $item['id']],
                'role'  => 'button',
                'class' => 'rowTable',
            ];
            return $data;
        },
        'columns'      => [
            [
                'attribute'     => 'id',
                'headerOptions' => [
                    'style' => 'width: 10%',
                ],
            ],
            'input_id',
            'transaction_id',
            [
                'header'        => 'transaction',
                'headerOptions' => [
                    'class' => 'text-right',
                ],
                'contentOptions' => [
                    'class' => 'text-right',
                ],
                'content'       => function ($i) {
                    $t = \common\models\piramida\Transaction::findOne($i['transaction_id']);
                    return Yii::$app->formatter->asDecimal($t['amount'] / 100, 2);
                },
            ],
            [
                'header'        => 'user',
                'content'       => function ($i) {
                    $t = \common\models\piramida\Transaction::findOne($i['transaction_id']);
                    $bill = \common\models\avatar\UserBill::findOne(['address' => $t->to]);
                    $user = \common\models\UserAvatar::findOne($bill->user_id);

                    return $user->getName2();
                },
            ],
            [
                'header'        => 'balance_elxsky',
                'headerOptions' => [
                    'class' => 'text-right',
                ],
                'contentOptions' => [
                    'class' => 'text-right',
                ],
                'content'       => function ($i) {
                    return Yii::$app->formatter->asDecimal($i['balance_elxsky'] / 100, 2);
                },
            ],
            [
                'header'        => 'balance_rub',
                'headerOptions' => [
                    'class' => 'text-right',
                ],
                'contentOptions' => [
                    'class' => 'text-right',
                ],
                'content'       => function ($i) {
                    return Yii::$app->formatter->asDecimal($i['balance_rub'] / 100, 2);
                },
            ],
        ],
    ]) ?>
    <?php \yii\widgets\Pjax::end(); ?>


    <?php \avatar\widgets\SchoolMenu::end() ?>

</div>

<div class="modal fade" id="modalInfo" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="myModalLabel">Информация</h4>
            </div>
            <div class="modal-body">
                Успешно!
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
            </div>
        </div>
    </div>
</div>