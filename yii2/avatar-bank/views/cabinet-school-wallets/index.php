<?php

/** @var $this \yii\web\View */

/** @var $school \common\models\school\School */

use yii\helpers\Url;
use app\services\GsssHtml;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use app\services\GetArticle\Collection;

$this->title = 'Кошельки';

$this->registerJS(<<<JS
$('.buttonAddFundRub').click(function (e) {
    e.preventDefault();
    e.stopPropagation();
    if (confirm('Подтвердите создание')) {
        ajaxJson({
            url: '/cabinet-school-wallets/add-fund-rub' + '?' + 'id' + '=' + {$school->id},
            success: function (ret) {
                $('#modalInfo').on('hidden.bs.modal', function() {
                    window.location.reload();
                }).modal();
            }
        });
    }
});
$('.rowTable2').click(function(e) {
    window.location = '/cabinet-school-wallets/view' + '?' + 'id' + '=' + $(this).data('id');
});
JS
);
?>

<div class="container">

    <?php \avatar\widgets\SchoolMenu::begin(['school' => $school]) ?>
    <h1 class="page-header"><?= Html::encode($this->title) ?></h1>

    <?php if (!\cs\Application::isEmpty($school->currency_id)) { ?>
        <?php if (\cs\Application::isEmpty($school->fund_rub)) { ?>
            <p>
                <button
                        class="btn btn-default buttonAddFundRub">Создать фонд развития рублевый
                </button>
            </p>
        <?php } else { ?>
            <p>В
                фонде: <?= Yii::$app->formatter->asDecimal(\common\models\piramida\Wallet::findOne($school->fund_rub2)->amount / 100, 2) ?>
                руб.</p>

                <?= \yii\grid\GridView::widget([
                    'dataProvider' => new \yii\data\ActiveDataProvider([
                        'query'      => \common\models\piramida\Wallet::find()
                            ->where(['in', 'id', [$school->fund_rub2, $school->fund_rub]])
                        ,
                        'pagination' => [
                            'pageSize' => 20,
                        ],
                    ]),
                    'summary'      => '',
                    'tableOptions' => [
                        'class' => 'table table-striped table-hover',
                    ],
                    'rowOptions'   => function ($item) {
                        $data = [
                            'data'  => ['id' => $item['id']],
                            'role'  => 'button',
                            'class' => 'rowTable2',
                        ];
                        return $data;
                    },
                    'columns'      => [
                        [
                            'header'  => 'WID',
                            'content' => function (\common\models\piramida\Wallet $item) {
                                $addressShort = $item->getAddressShort();

                                return Html::tag('code', $addressShort);
                            },
                        ],
                        [
                            'header'         => 'Монет',
                            'headerOptions'  => ['class' => 'text-right'],
                            'contentOptions' => ['class' => 'text-right'],
                            'content'        => function (\common\models\piramida\Wallet $item) {
                                $i = ArrayHelper::getValue($item, 'currency_id', '');
                                if ($i == '') return '';
                                $currency = \common\models\piramida\Currency::findOne($i);
                                if (is_null($currency)) return '';

                                return Yii::$app->formatter->asDecimal($item->getAmountWithDecimals(), $currency->decimals);
                            },
                        ],
                        [
                            'header'  => 'Валюта',
                            'content' => function ($item) {
                                $i = ArrayHelper::getValue($item, 'currency_id', '');
                                if ($i == '') return '';
                                $currency = \common\models\piramida\Currency::findOne($i);
                                if (is_null($currency)) return '';

                                return Html::tag('span', $currency->code, ['class' => 'label label-info']);
                            },
                        ],
                    ],
                ]) ?>
            <p>
                <a href="/cabinet-school-wallets/add-input?id=<?= $school->id ?>"
                   class="btn btn-default">Добавить приход</a>
            </p>
        <?php } ?>
    <?php } else { ?>
        <p>
            <a href="<?= Url::to(['cabinet-school-wallets/add-currency', 'id' => $school->id]) ?>"
               class="btn btn-default">Добавить монету в школу</a>
        </p>
    <?php } ?>

    <?php \yii\widgets\Pjax::begin(); ?>
    <?php
    $this->registerJS(<<<JS
$('[data-toggle="tooltip"]').tooltip();
$('.rowTable').click(function(e) {
    e.preventDefault();
    e.stopPropagation();
    window.location = '/cabinet-school-wallets/item' + '?' + 'id' + '=' + $(this).data('id');
});
JS
    );
    ?>
    <?= \yii\grid\GridView::widget([
        'dataProvider' => new \yii\data\ActiveDataProvider([
            'query' => \common\models\school\Input::find()->where(['school_id' => $school->id]),
        ]),
        'tableOptions' => [
            'class' => 'table table-striped table-hover',
        ],
        'rowOptions'   => function ($item) {
            $data = [
                'data'  => ['id' => $item['id']],
                'role'  => 'button',
                'class' => 'rowTable',
            ];
            return $data;
        },
        'columns'      => [
            [
                'attribute'     => 'id',
                'headerOptions' => [
                    'style' => 'width: 10%',
                ],
            ],
            [
                'header'         => 'Сколько',
                'attribute'      => 'amount',
                'format'         => ['decimal', 2],
                'headerOptions'  => [
                    'class' => 'text-right',
                ],
                'contentOptions' => [
                    'class' => 'text-right',
                ],
                'content'        => function ($i) {
                    return Yii::$app->formatter->asDecimal($i['amount'] / 100, 2);
                },
            ],
            [
                'header'    => 'Создано',
                'attribute' => 'created_at',
                'format'    => 'datetime',
            ],
            'comment',
            'emission_operation_id',
            'emission2_operation_id',
        ],
    ]) ?>
    <?php \yii\widgets\Pjax::end(); ?>


    <?php \avatar\widgets\SchoolMenu::end() ?>

</div>

<div class="modal fade" id="modalInfo" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="myModalLabel">Информация</h4>
            </div>
            <div class="modal-body">
                Успешно!
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
            </div>
        </div>
    </div>
</div>