<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;

/* @var $this \yii\web\View */
/* @var $content string */

//\common\assets\GantelaLayout\FastClick::register($this);
//\common\assets\GantelaLayout\FontAwesome::register($this);
//\common\assets\GantelaLayout\NProgress::register($this);
//\common\assets\GantelaLayout\iCheck::register($this);
//\common\assets\GantelaLayout\CustomTheme::register($this);
\common\assets\GantelaLayout\GantelaLayout::register($this);

$logo = '/images/logo144.png';
$favicon = '/controller/site/index/images/favicon.png';

$this->registerJs(<<<JS
$('.js-buttonChat').click(function(e) {
    window.location  = '/chat/index';
})
JS
);

/** @var \common\models\UserAvatar $user */
$user = Yii::$app->user->identity;
$UserEnter = \common\models\UserEnter::findOne(['user_id' => $user->id]);

?>

<?php $this->beginPage(); ?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" href="<?= $favicon ?>">

    <?= Html::csrfMetaTags() ?>
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->

    <title><?= $this->title ?></title>
    <?php $this->head() ?>
</head>

<body class="nav-md">
<?php $this->beginBody(); ?>

<div class="container body">
    <div class="main_container">
        <?php $school = Yii::$app->session->get('_school1'); ?>
        <?php if (true) { ?>
            <div class="col-md-3 left_col">
                <div class="left_col scroll-view">


                    <div class="clearfix"></div>

                    <!-- menu profile quick info -->
                    <div class="profile clearfix">
                        <div class="profile_pic">
                            <a href="/">
                                <img
                                    style="width: 100%"
                                    src="<?= \iAvatar777\widgets\FileUpload7\FileUpload::getFile('https://cloud1.iampau.pro/upload/cloud/15942/03139_d5Dhsyb9WM.png', 'crop') ?>" alt="..." class="img-circle profile_img">
                            </a>
                        </div>

                    </div>
                    <br />

                    <!-- sidebar menu -->
                    <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
                        <div class="menu_section">
                            <ul class="nav side-menu">

                                <?php if (true) {  ?>
                                    <li><a><i class="fa fa-file-o"></i> Декларация <span class="fa fa-chevron-down"></span></a>
                                        <ul class="nav child_menu">

                                            <?php

                                            /** @var \common\models\UserIdenty $userIdenty */
                                            $userIdenty = null;

                                            $screen = 0;
                                            // Есть ли хоть одна декларация?
                                            if (false) {
                                                // да
                                                // Выбираю самую последнюю заявку
                                                $userIdenty = \common\models\UserIdenty::find()
                                                    ->where([
                                                        'user_id' => Yii::$app->user->id,
                                                    ])
                                                    ->orderBy(['created_at' => SORT_DESC])
                                                    ->one();

                                            } else {
                                                // нет
                                                $userIdenty = null;
                                            }
                                            ?>
                                            <?php if (is_null($userIdenty)) { ?>
                                                <li><a href="<?= Url::to(['docs/add']) ?>">Заполнить персональные данные для декларации</a></li>
                                            <?php } else { ?>
                                                <?php if ($userIdenty->status == \common\models\UserIdenty::STATUS_CREATED) { ?>
                                                    <li><a href="<?= Url::to(['docs/add']) ?>">Редактировать персональные данные</a></li>
                                                    <li><a href="<?= Url::to(['docs/download']) ?>">Скачать декларацию</a></li>
                                                    <li><a href="<?= Url::to(['docs/add2']) ?>">Загрузить видео декларацию</a></li>

                                                <?php } ?>
                                                <?php if ($userIdenty->status == \common\models\UserIdenty::STATUS_SEND) { ?>
                                                    <p class="alert alert-success">Деларация отослана</p>

                                                <?php } ?>
                                                <?php if ($userIdenty->status == \common\models\UserIdenty::STATUS_MODERATION) { ?>
                                                    <p class="alert alert-success">Деларация разбирается модератором</p>

                                                <?php } ?>
                                                <?php if ($userIdenty->status == \common\models\UserIdenty::STATUS_ACCEPT) { ?>
                                                    <li><a href="<?= Url::to(['docs/download2']) ?>">Скачать декларацию полную</a></li>
                                                    <?php
                                                    $videoFile = \common\models\VideoFile::findOne($userIdenty->video_id);
                                                    ?>
                                                    <li><a href="/watch?v=<?= $videoFile->identity ?>">Посмотреть свое видео</a></li>

                                                <?php } ?>
                                                <?php if ($userIdenty->status == \common\models\UserIdenty::STATUS_REJECT) { ?>
                                                    <li><a href="/docs/add">Создать новую анкету на основании старых данных</a></li>
                                                    <p class="text-center"><a href="/docs/add" class="btn btn-primary">Создать новую анкету на основании старых данных</a></p>
                                                <?php } ?>
                                            <?php } ?>


                                        </ul>
                                    </li>
                                <?php } ?>


                            </ul>
                        </div>
                    </div>
                    <!-- /sidebar menu -->

                </div>
            </div>
        <?php } ?>


        <!-- top navigation -->
        <div class="top_nav">
            <div class="nav_menu">
                <nav>
                    <div class="nav toggle">
                        <a id="menu_toggle"><i class="fa fa-bars"></i></a>
                    </div>

                    <ul class="nav navbar-nav navbar-right">


                        <li class="">
                            <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                <?php
                                /** @var \common\models\UserAvatar $user */
                                $user = Yii::$app->user->identity;
                                ?>
                                <img src="<?= $user->getAvatar() ?>" alt=""><?= $user->name_first ?>
                                <span class=" fa fa-angle-down"></span>
                            </a>
                            <ul class="dropdown-menu dropdown-usermenu pull-right">
                                <li><a href="/cabinet/index"> Мой кабинет</a></li>
                                <li><a href="/cabinet/delete"> Удалить свой профиль</a></li>
                                <li><a href="/auth/logout" data-method="post"><i class="fa fa-sign-out pull-right"></i> Выйти</a></li>
                            </ul>
                        </li>
                    </ul>
                </nav>
            </div>
        </div>
        <!-- /top navigation -->

        <!-- page content -->
        <div class="right_col" role="main">
            <div class="">



                <?= $content ?>



            </div>
        </div>
        <!-- /page content -->

        <!-- footer content -->
        <footer>
            <div class="pull-right">
                Я Человек <?= \iAvatar777\services\DateRus\DateRus::format('b/Y') ?>
            </div>
            <div class="clearfix"></div>
        </footer>
        <!-- /footer content -->
    </div>
</div>


<?php $this->endBody() ?>

</body>
</html>
<?php $this->endPage() ?>
