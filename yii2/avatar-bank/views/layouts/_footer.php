<?php
/**
 * Created by PhpStorm.
 * User: Бог-Творец
 * Date: 21.01.2017
 * Time: 18:16
 */

?>

<footer class="footer" style="opacity: 1;">
    <div class="container">
        <div class="row" style="margin-top: 20px;">
            <div class="col-lg-4">
                <p style="margin-bottom: 10px;">
                    <a href="https://www.i-am-avatar.com/auth/login">https://www.i-am-avatar.com</a>
                </p>
                <p style="margin-bottom: 30px;">
                    <?php
                    $path = Yii::getAlias('@avatar/../composer.json');
                    $content = file_get_contents($path);
                    $data = \yii\helpers\Json::decode($content);

                    ?>
                    <?= $data['version'] ?>
                </p>
            </div>
            <div class="col-lg-4">
                <p>
                    <a href="https://www.i-am-avatar.com/page/help">Помощь</a>
                </p>
            </div>
            <div class="col-lg-4">
                <p style="margin-bottom: 30px;">
                    info@i-am-avatar.com
                </p>

            </div>
        </div>
    </div>
</footer>

