<?php

/** @var $school \common\models\school\School */

$isAdmin = \common\models\school\AdminLink::find()->where(['school_id' => $school->id, 'user_id' => Yii::$app->user->id])->exists();
$isCommand = \common\models\school\CommandLink::find()->where(['school_id' => $school->id, 'user_id' => Yii::$app->user->id])->exists();

if ($isAdmin) {
    $type = 'admin';
} else {
    if ($isCommand) {
        $type = 'command';
    } else {
        $type = '';
    }
}
if ($type == 'admin') {
    $menuMain = [
        'items' => [
            [
                'items' => [
                    [
                        'title' => 'О Сообществе',
                        'icon'  => 'fa fa-table',
                        'items' => [
                            [
                                'title'   => 'Общая информация',
                                'url'     => ['cabinet-school/view', 'id' => $school->id],
                                'urlList' => [
                                    ['route', 'cabinet-school/view'],
                                ],
                            ],
                            [
                                'title'   => 'Органы управления',
                            ],
                            [
                                'title'   => 'Реквизиты',
                                'url'     => ['cabinet-school-koop-settings/index', 'id' => $school->id],
                                'urlList' => [
                                    ['route', 'cabinet-school-koop-settings/index'],
                                ],
                            ],
                            ['title'   => 'Миссия'],
                            ['title'   => 'Контакты'],
                            ['title'   => 'Расчетные счета'],
                            [
                                'title'   => 'Платежные системы',
                                'url'     => ['cabinet-school-pay-systems/index', 'id' => $school->id],
                                'urlList' => [
                                    ['controller', 'cabinet-school-pay-systems'],
                                ],
                            ],
                        ],
                    ],
                    [
                        'title' => 'Пр. кооператив',
                        'icon'  => 'fa fa-cubes',
                        'items' => [
                            [
                                'title'   => 'Подряды',
                                'url'     => ['cabinet-koop-podryad/index', 'id' => $school->id],
                                'urlList' => [
                                    ['controller', 'cabinet-koop-podryad'],
                                ],
                            ],
                            [
                                'title'   => 'Заявки',
                                'url'     => ['cabinet-school-koop/index', 'id' => $school->id],
                                'urlList' => [
                                    ['controller', 'cabinet-school-koop'],
                                ],
                            ],
                        ],
                    ],
                    [
                        'title' => 'Документы',
                        'icon'  => 'fa fa-file-text-o',
                        'items' => [
                            [
                                'title'   => 'Учредительные и регламенты',
                                'url'     => ['cabinet-school-documents/index', 'id' => $school->id],
                                'urlList' => [['route', 'cabinet-school-documents/index']],
                            ],
                            [
                                'title'   => 'Протоколы',
                                'url'     => ['cabinet-school-documents/index', 'id' => $school->id],
                                'urlList' => [['route', 'cabinet-school-documents/index']],
                            ],
                            [
                                'title'   => 'Соглашения и договора',
                                'url'     => ['cabinet-school-documents/index', 'id' => $school->id],
                                'urlList' => [['route', 'cabinet-school-documents/index']],
                            ],
                            [
                                'title'   => 'Корреспонденция',
                                'url'     => ['cabinet-school-documents/index', 'id' => $school->id],
                                'urlList' => [['route', 'cabinet-school-documents/index']],
                            ],

                        ],
                    ],
                    [
                        'title' => 'Голосования',
                        'icon'  => 'fa fa-hand-paper-o',
                        'items' => [
                            [
                                'title'   => 'Управление',
                                'url'     => ['cabinet-vote/index', 'id' => $school->id],
                                'urlList' => [['controller', 'cabinet-vote']],
                            ],
                            [
                                'title'   => 'Участие',
                                'url'     => ['cabinet-vote-action/index', 'id' => $school->id],
                                'urlList' => [['controller', 'cabinet-vote-action']],
                            ],
                        ],
                    ],
                    [
                        'title' => 'Деятельность',
                        'icon'  => 'fa fa-windows',
                        'items' => [
                            [
                                'title'   => 'Программы и проекты',
                                'url'     => ['cabinet-school-koop-aim-programs/index', 'id' => $school->id],
                                'urlList' => [
                                    ['controller', 'cabinet-school-koop-aim-programs'],
                                    ['controller', 'cabinet-school-koop-projects'],
                                    ['controller', 'cabinet-school-blago-program'],
                                ],
                            ],
                            [
                                'title'   => 'События',
                            ],
                            [
                                'title'   => 'Новости',
                                'url'     => ['cabinet-school-news/index', 'id' => $school->id],
                                'urlList' => [['controller', 'cabinet-school-news']],
                            ],
                            [
                                'title'   => 'Объявления',
                            ],
                        ],
                    ],
                    [
                        'title' => 'Задачи',
                        'icon'  => 'fa fa-tasks',
                        'items' => [
                            [
                                'title'   => 'Добавить',
                                'url'     => ['cabinet-school-task-list/add', 'id' => $school->id],
                                'urlList' => [['route', 'cabinet-school-task-list/add']],
                            ],
                            [
                                'title'   => 'Список',
                                'url'     => ['cabinet-school-task-list/index2', 'id' => $school->id],
                                'urlList' => [
                                    ['route', 'cabinet-school-task-list/index2'],
                                    ['route', 'cabinet-school-task-list/view'],
                                ],
                            ],
                            [
                                'title'   => 'Доска',
                                'url'     => ['cabinet-school-task-list/ajail', 'id' => $school->id],
                                'urlList' => [['route', 'cabinet-school-task-list/ajail']],
                            ],
                            [
                                'title'   => 'Руководители проектов',
                                'url'     => ['cabinet-school-task-list/project-manager-list', 'id' => $school->id],
                                'urlList' => [['route', 'cabinet-school-task-list/project-manager-list']],
                            ],
                            [
                                'title'   => 'Категории',
                                'url'     => ['cabinet-school-task-list-category/index', 'id' => $school->id],
                                'urlList' => [['controller', 'cabinet-school-task-list-category']],
                            ],
                            [
                                'title'   => 'Команда',
                                'url'     => ['cabinet-school-task-list-command/index', 'id' => $school->id],
                                'urlList' => [['route', 'cabinet-school-task-list-command/index']],
                            ],
                        ],
                    ],
                    [
                        'title' => 'Бюджеты',
                        'icon'  => 'fa fa-balance-scale',
                        'items' => [
                            [
                                'title'   => 'Трудовые вклады',
                                'url'     => ['cabinet-school-budjet/trud', 'id' => $school->id],
                                'urlList' => [['route', 'cabinet-school-budjet/trud']],
                            ],
                            [
                                'title'   => 'Денежные вклады',
                                'url'     => ['cabinet-school-budjet/money', 'id' => $school->id],
                                'urlList' => [['route', 'cabinet-school-budjet/money']],
                            ],
                            [
                                'title'   => 'Денежные вложения',
                                'url'     => ['cabinet-school-money-income/index', 'id' => $school->id],
                                'urlList' => [['controller', 'cabinet-school-money-income']],
                            ],
                            [
                                'title'   => 'Имущественные вклады',
                                'url'     => ['cabinet-school-budjet/imush', 'id' => $school->id],
                                'urlList' => [['route', 'cabinet-school-budjet/imush']],
                            ],
                            [
                                'title'   => 'Расходы',
                                'url'     => ['cabinet-school-budjet/rashod', 'id' => $school->id],
                                'urlList' => [['route', 'cabinet-school-budjet/rashod']],
                            ],

                        ],
                    ],
                    [
                        'title' => 'Участники',
                        'icon'  => 'fa fa-users',
                        'items' => [
                            [
                                'title'   => 'Участники',
                                'url'     => ['cabinet-school-users/index', 'id' => $school->id],
                                'urlList' => [['route', 'cabinet-school-users/index']],
                            ],
                            [
                                'title'   => 'Группы',
                            ],
                        ],
                    ],
                    [
                        'title' => 'Каталог',
                        'icon'  => 'fa fa-laptop',
                        'items' => [
                            [
                                'title'   => 'Каталог товаров, работ, услуг',
                            ],
                        ],
                    ],
                    [
                        'title' => 'Настройки',
                        'icon'  => 'fa fa-sun-o',
                        'items' => [
                            [
                                'title'   => 'Домен',
                                'url'     => ['cabinet-school-dns/index', 'id' => $school->id],
                                'urlList' => [
                                    ['controller', 'cabinet-school-dns'],
                                ],
                            ],
                            [
                                'title'   => 'Администраторы',
                                'url'     => ['cabinet-school-admins/index', 'id' => $school->id],
                                'urlList' => [
                                    ['controller', 'cabinet-school-admins'],
                                ],
                            ],
                            [
                                'title'   => 'Платежные системы',
                                'url'     => ['cabinet-school-pay-systems/index', 'id' => $school->id],
                                'urlList' => [
                                    ['controller', 'cabinet-school-pay-systems'],
                                ],
                            ],
                            [
                                'title'   => 'Учетные единицы',
                                'url'     => ['cabinet-company-coin/index', 'id' => $school->id],
                                'urlList' => [
                                    ['controller', 'cabinet-company-coin'],
                                ],
                            ],
                            [
                                'title'   => 'Роли',
                                'url'     => ['cabinet-school-role/index', 'id' => $school->id],
                                'urlList' => [
                                    ['controller', 'cabinet-school-role'],
                                ],
                            ],

                            [
                                'title'   => 'Учителя',
                                'url'     => ['cabinet-school-teachers/index', 'id' => $school->id],
                                'urlList' => [
                                    ['controller', 'cabinet-school-teachers'],
                                ],
                            ],

                            [
                                'title'   => 'Плагины экспорта лидов',
                                'url'     => ['cabinet-school-plug-out/index', 'id' => $school->id],
                                'urlList' => [
                                    ['controller', 'cabinet-school-plug-out'],
                                ],
                            ],
                            [
                                'title'   => 'Настройки задач',
                                'url'     => ['cabinet-school-settings/task', 'id' => $school->id],
                                'urlList' => [
                                    ['route', 'cabinet-school-settings/task'],
                                ],
                            ],
                            [
                                'title'   => 'Облако',
                                'url'     => ['cabinet-school-files/index', 'id' => $school->id],
                                'urlList' => [
                                    ['controller', 'cabinet-school-files'],
                                ],
                            ],
                            [
                                'title'   => '1C API',
                                'url'     => ['cabinet-school-settings/onec', 'id' => $school->id],
                                'urlList' => [
                                    ['route', 'cabinet-school-settings/onec'],
                                ],
                            ],
                            [
                                'title'   => 'SMS шлюз',
                                'url'     => ['cabinet-school-settings/sms', 'id' => $school->id],
                                'urlList' => [
                                    ['route', 'cabinet-school-settings/sms'],
                                ],
                            ],
                            [
                                'title'   => 'Загрузить файл в облако',
                                'url'     => ['cabinet-school/file-sha256', 'id' => $school->id],
                                'urlList' => [
                                    ['route', 'cabinet-school/file-sha256'],
                                ],
                            ],
                            [
                                'title'   => 'Настройка партнеской программы',
                                'url'     => ['cabinet-school-mlm/index', 'id' => $school->id],
                                'urlList' => [
                                    ['controller', 'cabinet-school-mlm'],
                                ],
                            ],
                            [
                                'title'   => 'Настройки почты',
                                'url'     => ['cabinet-school-settings/index', 'id' => $school->id],
                                'urlList' => [
                                    ['route', 'cabinet-school-settings/index'],
                                ],
                            ],
                            [
                                'title'   => 'Настройки магазина',
                                'url'     => ['cabinet-school-settings/shop', 'id' => $school->id],
                                'urlList' => [
                                    ['route', 'cabinet-school-settings/shop'],
                                ],
                            ],
                            [
                                'title'   => 'Настройки блога',
                                'url'     => ['cabinet-school-settings/blog', 'id' => $school->id],
                                'urlList' => [
                                    ['route', 'cabinet-school-settings/blog'],
                                ],
                            ],
                            [
                                'title'   => 'Настройки новостей',
                                'url'     => ['cabinet-school-settings/news', 'id' => $school->id],
                                'urlList' => [
                                    ['route', 'cabinet-school-settings/news'],
                                ],
                            ],
                            [
                                'title'   => 'Настройки дизайна',
                                'url'     => ['cabinet-school-settings/design-sub', 'id' => $school->id],
                                'urlList' => [
                                    ['route', 'cabinet-school-settings/design-sub'],
                                ],
                            ],
                            [
                                'title'   => 'Настройки дизайна главная',
                                'url'     => ['cabinet-school-settings/design-main', 'id' => $school->id],
                                'urlList' => [
                                    ['route', 'cabinet-school-settings/design-main'],
                                ],
                            ],
                            [
                                'title'   => 'Плагин Тильды',
                                'url'     => ['cabinet-school-tilda/index', 'id' => $school->id],
                                'urlList' => [
                                    ['controller', 'cabinet-school-tilda'],
                                ],
                            ],
                        ],
                    ],
                    [
                        'title' => 'ДОМ',
                        'icon'  => 'fa fa-home',
                        'items' => [
                            [
                                'title'   => 'Курсы',
                                'url'     => ['cabinet-school-kurs-list/index', 'id' => $school->id],
                                'urlList' => [
                                    ['controller', 'cabinet-school-kurs-list/index'],
                                ],
                            ],
                            [
                                'title'   => 'Страницы',
                                'url'     => ['cabinet-school-pages/index', 'id' => $school->id],
                                'urlList' => [
                                    ['controller', 'cabinet-school-pages/index'],
                                ],
                            ],
                            [
                                'title'   => 'Блог',
                                'url'     => ['cabinet-school-blog/index', 'id' => $school->id],
                                'urlList' => [
                                    ['controller', 'cabinet-school-blog/index'],
                                ],
                            ],
                            [
                                'title'   => 'Новости',
                                'url'     => ['cabinet-school-news/index', 'id' => $school->id],
                                'urlList' => [
                                    ['controller', 'cabinet-school-news/index'],
                                ],
                            ],
                            [
                                'title'   => 'Голосования',
                                'url'     => ['cabinet-vote/index', 'id' => $school->id],
                                'urlList' => [
                                    ['controller', 'cabinet-vote/index'],
                                ],
                            ],
                            [
                                'title'   => 'Участники',
                                'url'     => ['cabinet-school-users/index', 'id' => $school->id],
                                'urlList' => [
                                    ['controller', 'cabinet-school-users/index'],
                                ],
                            ],
                            [
                                'title'   => 'Документы',
                                'url'     => ['cabinet-school-documents/index', 'id' => $school->id],
                                'urlList' => [
                                    ['controller', 'cabinet-school-documents/index'],
                                ],
                            ],
                            [
                                'title'   => 'Благотворительные программы',
                                'url'     => ['cabinet-school-blago-program/index', 'id' => $school->id],
                                'urlList' => [
                                    ['controller', 'cabinet-school-blago-program/index'],
                                ],
                            ],
                        ],
                    ],

                    [
                        'title' => 'Интернет магазин',
                        'icon'  => 'fa fa-desktop',
                        'items' => [
                            [
                                'title'   => 'Товары',
                                'url'     => ['cabinet-school-shop-goods/index', 'id' => $school->id],
                                'urlList' => [
                                    ['controller', 'cabinet-school-shop-goods/index'],
                                ],
                            ],
                            [
                                'title'   => 'Доставки',
                                'url'     => ['cabinet-school-shop-delivery/index', 'id' => $school->id],
                                'urlList' => [
                                    ['controller', 'cabinet-school-shop-delivery/index'],
                                ],
                            ],
                            [
                                'title'   => 'Каталог',
                                'url'     => ['cabinet-school-shop-catalog/index', 'id' => $school->id],
                                'urlList' => [
                                    ['controller', 'cabinet-school-shop-catalog/index'],
                                ],
                            ],
                            [
                                'title'   => 'Заказы',
                                'url'     => ['cabinet-school-shop-requests/index', 'id' => $school->id],
                                'urlList' => [
                                    ['controller', 'cabinet-school-shop-requests/index'],
                                ],
                            ],

                        ],
                    ],
                ],
            ],
        ],
    ];
    if ($school->id == 112) {
        $menuMain['items'][0]['items'][9]['items'][] = [
            'title'   => 'Сертификаты',
            'url'     => ['cabinet-company-cert/index', 'id' => $school->id],
            'urlList' => [
                ['controller', 'cabinet-company-cert'],
            ],
        ];
    }

}
if ($type == 'command') {
    $menuMain = [
        'items' => [
            [
                'items' => [
                    [
                        'title' => 'Регистрация',
                        'icon'  => 'fa fa-user',
                        'items' => [
                            [
                                'title'   => 'Получение подписи',
                                'url'     => ['cabinet-digital-sign/index'],
                                'urlList' => [
                                    ['route', 'cabinet-digital-sign/index'],
                                ],
                            ],
                            [
                                'title'   => 'Указание телефона',
                                'url'     => ['cabinet-profile-phone/index'],
                                'urlList' => [
                                    ['route', 'cabinet-profile-phone/index'],
                                ],
                            ],
                        ],
                    ],
                    [
                        'title' => 'Задачи',
                        'icon'  => 'fa fa-tasks',
                        'items' => [
                            [
                                'title'   => 'Задачи',
                                'url'     => ['cabinet-school-task-list/index2', 'id' => $school->id],
                                'urlList' => [
                                    ['controller', 'cabinet-school-task-list'],
                                ],
                            ],
                        ],
                    ],
                    [
                        'title' => 'Голосования',
                        'icon'  => 'fa fa-hand-paper-o',
                        'items' => [
                            [
                                'title'   => 'Участие',
                                'url'     => ['cabinet-vote-action/index', 'id' => $school->id],
                                'urlList' => [['controller', 'cabinet-vote-action']],
                            ],
                        ],
                    ],

                    [
                        'title' => 'Документы',
                        'icon'  => 'fa fa-file-text',
                        'items' => [
                            [
                                'title'   => 'Список',
                                'url'     => ['cabinet-school-documents/index'],
                                'urlList' => [
                                    ['controller', 'cabinet-school-documents'],
                                ],
                            ],
                        ],
                    ],
                ],
            ],
        ],
    ];
}

?>

<?= \common\widgets\MenuCompany\MenuCompany::widget($menuMain) ?>

