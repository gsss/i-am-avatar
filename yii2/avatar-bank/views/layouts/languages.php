<?php
/**
 * Created by PhpStorm.
 * User: Бог-Творец
 * Date: 18.05.2016
 * Time: 7:45
 *
 */

use yii\helpers\Url;
use yii\helpers\Html;

/** @var $this \yii\web\View */

\avatar\assets\Language\Asset::register($this);

$items1 = \common\models\Language::find()->all();

$this->registerJs(<<<JS
$('.buttonLanguageSet').click(function(e){
    function setCookie2 (name, value) {
        var date = new Date();
        var now = new Date();
        var time = now.getTime();
        var expireTime = time + (1000 * 60 * 60 * 24 * 365);
        now.setTime(expireTime);
        document.cookie = name + "=" + value +
        "; expires=" + now.toGMTString() +
        "; path=/";
    }
    setCookie2('language', $(this).data('code'));
    window.location.reload();
});
JS
);


/** @var \common\models\Language $language */
$language = \common\models\Language::findOne(['code' => Yii::$app->language]);
?>
<li class="dropdown" id="userBlockLi">
    <a
            href="#"
            class="dropdown-toggle"
            data-toggle="dropdown"
            aria-expanded="false"
            role="button"
            style="padding: 15px 10px 15px 10px;"
    >
        <?= Html::img('//www.i-am-avatar.com' . $language->image, [
            'height' => '20',
            'class'  => 'img-circle',
            'style'  => 'border: 1px solid #888',
        ]) ?>
        <span class="caret"></span>
    </a>
    <ul class="dropdown-menu" role="menu">
        <?= \avatar\services\Html1::getItems2($items1); ?>
    </ul>
</li>


