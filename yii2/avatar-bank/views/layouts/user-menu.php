<?php
/**
 * Created by PhpStorm.
 * User: Бог-Творец
 * Date: 18.05.2016
 * Time: 7:45
 *
 */

use yii\helpers\Url;
use yii\helpers\Html;

/** @var $this \yii\web\View */


?>
<li>
    <p style="margin-top: 13px; margin-right: 5px; margin-left: 5px;">
        <a href="/shop/cart">
            <?php if (\common\models\shop\Basket::getCount() == 0) { ?>
                <span id="basketCounterConteiner"></span>
            <?php } else { ?>
                <span id="basketCounter" class="label label-success">
                    <?= \common\models\shop\Basket::getCount() ?>
                </span>
            <?php } ?>
        </a>
        <a href="/cabinet-wallet/index">
            <img src="/images/controller/cabinet-bills/index/wallet.png" width="30" data-toggle="tooltip" title="Кошелек" data-placement="bottom">
        </a>
    </p>
</li>
<li class="dropdown" id="userBlockLi">
    <a
        href="#"
        class="dropdown-toggle"
        data-toggle="dropdown"
        aria-expanded="false"
        role="button"
        style="padding: 5px 10px 5px 10px;"
        >
        <?= Html::img(Yii::$app->user->identity->getAvatar(), [
            'height' => '40px',
            'class' => 'img-circle'
        ]) ?>
        <span class="caret"></span>
    </a>
    <ul class="dropdown-menu" role="menu">
        <li><a href="<?= Url::to(['cabinet-school/index']) ?>">Мои сообщества</a></li>
        <li><a href="<?= Url::to(['cabinet/index']) ?>"><?= \Yii::t('c.qvYMPl9ABq', 'Мой кабинет') ?></a></li>

        <li class="divider"></li>
        <li><a href="<?= Url::to(['cabinet/profile']) ?>"><i class="glyphicon glyphicon-cog" style="padding-right: 5px;"></i><?= \Yii::t('c.qvYMPl9ABq', 'Мой профиль') ?></a></li>
        <li><a href="<?= Url::to(['cabinet-cards/index']) ?>"><i class="glyphicon glyphicon-credit-card" style="padding-right: 5px;"></i>Карты</a></li>


        <?php if (Yii::$app->id == 'avatar-school') { ?>
            <li><a href="<?= Url::to(['cabinet-task-list/index', 'id' => 1]) ?>"><i class="glyphicon glyphicon-check" style="padding-right: 5px;"></i>Задачи</a></li>
        <?php } else { ?>
            <?php if (Yii::$app->user->can('permission_team')) { ?>
                <li><a href="<?= Url::to(['cabinet-task-list/index', 'id' => 1]) ?>"><i class="glyphicon glyphicon-check" style="padding-right: 5px;"></i>Задачи</a></li>
            <?php } ?>
        <?php }?>

        <?php if (Yii::$app->user->can('role_admin_command')) { ?>
            <li><a href="<?= Url::to(['admin-anketa/index']) ?>"><i class="glyphicon glyphicon-user" style="padding-right: 5px;"></i>Команда</a></li>
        <?php } ?>

        <?php if (Yii::$app->user->can('permission_admin')) { ?>
            <li class="divider"></li>
            <li><a href="<?= Url::to(['admin/index']) ?>"><i class="glyphicon glyphicon-btc" style="padding-right: 5px;"></i>Админка</a></li>
            <li><a href="<?= Url::to(['admin-monitoring/index']) ?>"><i class="glyphicon glyphicon-btc" style="padding-right: 5px;"></i>Мониторинг</a></li>
            <li><a href="<?= Url::to(['admin-users/index']) ?>"><i class="glyphicon glyphicon-user" style="padding-right: 5px;"></i>Пользователи</a></li>
            <li><a href="<?= Url::to(['admin-anketa-school/index']) ?>"><i class="glyphicon glyphicon-user" style="padding-right: 5px;"></i>Заявки в школы</a></li>
            <li><a href="<?= Url::to(['admin-reklama/index']) ?>"><i class="glyphicon glyphicon-user" style="padding-right: 5px;"></i>Рекламные материалы</a></li>
        <?php } ?>
        <?php if (Yii::$app->user->can('permission_languages')) { ?>
            <li class="divider"></li>
            <li role="presentation" class="dropdown-header">Языки</li>
            <li><a href="<?= Url::to(['languages/category']) ?>"><i class="glyphicon glyphicon-btc" style="padding-right: 5px;"></i>Категории</a></li>
        <?php } ?>
        <?php if (Yii::$app->user->can('permission_languages-admin')) { ?>
            <li><a href="<?= Url::to(['languages-admin/config']) ?>"><i class="glyphicon glyphicon-btc" style="padding-right: 5px;"></i>Config</a></li>
        <?php } ?>
        <li class="divider"></li>

        <li><a href="<?= Url::to(['auth/logout']) ?>" data-method="post"><i class="glyphicon glyphicon-off" style="padding-right: 5px;"></i><?= \Yii::t('c.qvYMPl9ABq', 'Выйти') ?></a></li>
    </ul>
</li>