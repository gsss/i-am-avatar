<?php

use cs\Application;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;

/* @var $this \yii\web\View */
/* @var $content string */

//\common\assets\GantelaLayout\FastClick::register($this);
//\common\assets\GantelaLayout\FontAwesome::register($this);
//\common\assets\GantelaLayout\NProgress::register($this);
//\common\assets\GantelaLayout\iCheck::register($this);
//\common\assets\GantelaLayout\CustomTheme::register($this);
\common\assets\GantelaLayout\GantelaLayout::register($this);
\avatar\assets\AjaxJson\Asset::register($this);

$school = \common\models\school\School::get();
$logo = '/images/logo144.png';
if ($school->image) {
    $logo = \iAvatar777\widgets\FileUpload7\FileUpload::getFile($school->image, 'crop');
}
$favicon = '/images/logo144.png';
if ($school->favicon) {
    $favicon = $school->favicon;
}

$this->registerJs(<<<JS
$('.js-buttonChat').click(function(e) {
    window.location  = '/chat/index';
})
JS
)


?>

<?php $this->beginPage(); ?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" href="<?= $favicon ?>">

    <?= Html::csrfMetaTags() ?>
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->

    <title><?= $this->title ?></title>
    <?php $this->head() ?>
</head>

<body class="nav-md">
<?php $this->beginBody(); ?>

<div class="container body">
    <div class="main_container">
        <?php $school = Yii::$app->session->get('_school1'); ?>
        <?php if (!is_null($school)) { ?>
            <div class="col-md-3 left_col">
                <div class="left_col scroll-view">


                    <div class="clearfix"></div>

                    <!-- menu profile quick info -->
                    <div class="profile clearfix">
                        <div class="profile_pic">
                            <a href="/">
                                <?php if (Application::isEmpty($school->image)) { ?>
                                    <img src="/images/school/school.jpg" alt="..." class="img-circle profile_img">
                                <?php } else { ?>
                                    <img src="<?= \iAvatar777\widgets\FileUpload7\FileUpload::getFile($school->image, 'crop') ?>" alt="..." class="img-circle profile_img">
                                <?php }  ?>
                            </a>
                        </div>
                        <div class="profile_info">
                            <span>Сообщество</span>
                            <h2><?= $school->name ?></h2>
                        </div>
                    </div>
                    <!-- /menu profile quick info -->

                    <br />

                    <!-- sidebar menu -->
                    <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
                        <?= $this->render('cabinet2_main_menu', ['school' => $school]) ?>
                    </div>
                </div>
                <!-- /sidebar menu -->
            </div>
        <?php } else { ?>
            <?= $this->render('menu-root') ?>
        <?php } ?>


        <!-- top navigation -->
        <div class="top_nav">
            <div class="nav_menu">
                <nav>
                    <div class="nav toggle">
                        <a id="menu_toggle"><i class="fa fa-bars"></i></a>
                    </div>

                    <ul class="nav navbar-nav navbar-right">

                        <li class="">
                            <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                <?php
                                /** @var \common\models\UserAvatar $user */
                                $user = Yii::$app->user->identity;
                                ?>
                                <img src="<?= $user->getAvatar() ?>" alt=""><?= $user->name_first ?>
                                <span class=" fa fa-angle-down"></span>
                            </a>
                            <ul class="dropdown-menu dropdown-usermenu pull-right">
                                <li><a href="<?= Url::to(['cabinet-school/index']) ?>">Мои сообщества</a></li>
                                <li><a href="<?= Url::to(['cabinet/index']) ?>"><?= \Yii::t('c.qvYMPl9ABq', 'Мой кабинет') ?></a></li>

                                <li class="divider"></li>
                                <li><a href="<?= Url::to(['cabinet/profile']) ?>"><i class="glyphicon glyphicon-cog" style="padding-right: 5px;"></i><?= \Yii::t('c.qvYMPl9ABq', 'Мой профиль') ?></a></li>
                                <li><a href="<?= Url::to(['cabinet-cards/index']) ?>"><i class="glyphicon glyphicon-credit-card" style="padding-right: 5px;"></i>Карты</a></li>


                                <?php if (Yii::$app->id == 'avatar-school') { ?>
                                    <li><a href="<?= Url::to(['cabinet-task-list/index', 'id' => 1]) ?>"><i class="glyphicon glyphicon-check" style="padding-right: 5px;"></i>Задачи</a></li>
                                <?php } else { ?>
                                    <?php if (Yii::$app->user->can('permission_team')) { ?>
                                        <li><a href="<?= Url::to(['cabinet-task-list/index', 'id' => 1]) ?>"><i class="glyphicon glyphicon-check" style="padding-right: 5px;"></i>Задачи</a></li>
                                    <?php } ?>
                                <?php }?>

                                <?php if (Yii::$app->user->can('role_admin_command')) { ?>
                                    <li><a href="<?= Url::to(['admin-anketa/index']) ?>"><i class="glyphicon glyphicon-user" style="padding-right: 5px;"></i>Команда</a></li>
                                <?php } ?>

                                <?php if (Yii::$app->user->can('permission_admin')) { ?>
                                    <li class="divider"></li>
                                    <li><a href="<?= Url::to(['admin/index']) ?>"><i class="glyphicon glyphicon-btc" style="padding-right: 5px;"></i>Админка</a></li>
                                    <li><a href="<?= Url::to(['admin-monitoring/index']) ?>"><i class="glyphicon glyphicon-btc" style="padding-right: 5px;"></i>Мониторинг</a></li>
                                    <li><a href="<?= Url::to(['admin-users/index']) ?>"><i class="glyphicon glyphicon-user" style="padding-right: 5px;"></i>Пользователи</a></li>
                                    <li><a href="<?= Url::to(['admin-anketa-school/index']) ?>"><i class="glyphicon glyphicon-user" style="padding-right: 5px;"></i>Заявки в школы</a></li>
                                    <li><a href="<?= Url::to(['admin-reklama/index']) ?>"><i class="glyphicon glyphicon-user" style="padding-right: 5px;"></i>Рекламные материалы</a></li>
                                <?php } ?>
                                <?php if (Yii::$app->user->can('permission_languages')) { ?>
                                    <li class="divider"></li>
                                    <li role="presentation" class="dropdown-header">Языки</li>
                                    <li><a href="<?= Url::to(['languages/category']) ?>"><i class="glyphicon glyphicon-btc" style="padding-right: 5px;"></i>Категории</a></li>
                                <?php } ?>
                                <?php if (Yii::$app->user->can('permission_languages-admin')) { ?>
                                    <li><a href="<?= Url::to(['languages-admin/config']) ?>"><i class="glyphicon glyphicon-btc" style="padding-right: 5px;"></i>Config</a></li>
                                <?php } ?>
                                <li class="divider"></li>

                                <li><a href="<?= Url::to(['auth/logout']) ?>" data-method="post"><i class="glyphicon glyphicon-off" style="padding-right: 5px;"></i><?= \Yii::t('c.qvYMPl9ABq', 'Выйти') ?></a></li>                            </ul>
                        </li>

                        <li class="" data-toggle="tooltip" title="Кошелек" data-placement="bottom">
                            <a href="/cabinet-wallet/index">
                                <img src="/images/controller/cabinet-bills/index/wallet.png" width="30">
                            </a>
                        </li>
                        <li class="" data-toggle="tooltip" title="Все сообщества" data-placement="bottom">
                            <a href="/cabinet-school/index" >

                                <i class="glyphicon glyphicon-th" style="
    font-size: 24px;
    margin-top: 3px;
"></i>
                            </a>
                        </li>

                    </ul>
                </nav>
            </div>
        </div>
        <!-- /top navigation -->

        <!-- page content -->
        <div class="right_col" role="main">
            <div class="">



                <?= $content ?>



            </div>
        </div>
        <!-- /page content -->

        <!-- footer content -->
        <footer>
            <div class="pull-right">
                <?php
                $path = Yii::getAlias('@avatar/../composer.json');
                $content = file_get_contents($path);
                $data = \yii\helpers\Json::decode($content);

                ?>
                <p>(с) Платформа Качество Жизни <?= $data['version'] ?></p>
            </div>
            <div class="clearfix"></div>
        </footer>
        <!-- /footer content -->
    </div>
</div>


<?php $this->endBody() ?>

</body>
</html>
<?php $this->endPage() ?>
