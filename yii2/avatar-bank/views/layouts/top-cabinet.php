<?php
/**
 * Created by PhpStorm.
 * User: Бог-Творец
 * Date: 11.11.2016
 * Time: 1:44
 */

/** @var $this \yii\web\View */

use yii\helpers\Html;
use yii\helpers\Url;

$school = \common\models\school\School::get();

$mainLogo = '/images/avatarlogo1.png';
if ($school->image) {
    $mainLogo = \common\widgets\FileUpload7\FileUpload::getFile($school->image, 'crop');
}

?>


<!-- Fixed navbar -->
<nav class="navbar navbar-default navbar-fixed-top">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar"
                    aria-expanded="false" aria-controls="navbar">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a
                class="navbar-brand"
                href="/"
                style="padding: 5px 10px 5px 10px;"
                >
                <img
                    src="<?= $mainLogo ?>"
                    width="40"
                    data-toggle="tooltip"
                    data-placement="bottom"
                    title="Главная"
                    class="img-circle"
                >
            </a>
        </div>
        <div id="navbar" class="collapse navbar-collapse">
            <ul class="nav navbar-nav">
                <?= $this->render('../blocks/topMenuCabinet') ?>
            </ul>
            <ul class="nav navbar-nav navbar-right">
                <?= $this->render('user-menu') ?>
            </ul>
        </div>

    </div>
</nav>
