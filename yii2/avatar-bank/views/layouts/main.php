<?php
/**
 * Created by PhpStorm.
 * User: Бог-Творец
 * Date: 01.11.2016
 * Time: 9:42
 */

/** @var $this \yii\web\View */

\avatar\assets\App\Asset::register($this);
\avatar\assets\LayoutMenu\Asset::register($this);
\avatar\assets\FontAwesome::register($this);

use yii\helpers\Html;
use yii\helpers\Url;

$this->registerJs(<<<JS
$('[data-toggle="tooltip"]').tooltip();
JS
);


$school = \common\models\school\School::get();
$favicon = '/images/logo144.png';
if ($school->favicon) {
    $favicon = $school->favicon;
}


?>
<?php $this->beginPage(); ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?= $this->title ?></title>
    <link rel="shortcut icon" href="<?= $favicon ?>">
    <?= Html::csrfMetaTags() ?>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody(); ?>

<?= $this->render('top') ?>

<?= $content ?>


<?= $this->render('_counters') ?>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>