<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;
use app\models\UnionCategory;
use yii\db\Query;
use yii\helpers\ArrayHelper;

/** @var $this  yii\web\View */
/** @var $form  yii\bootstrap\ActiveForm */
/** @var $model \avatar\models\forms\Table */
/** @var $school \common\models\school\School */

$this->title = 'Добавить категорию';

?>
<div class="container">

    <h1 class="page-header"><?= Html::encode($this->title) ?></h1>

    <?php if (Yii::$app->session->hasFlash('form')) : ?>

        <div class="alert alert-success">
            Успешно добавлено.
        </div>
        <p>
            <a href="<?= \yii\helpers\Url::to(['cabinet-school-task-list-table/index', 'id' => $school->id]) ?>"
               class="btn btn-success">Все доски</a>
        </p>

    <?php else: ?>

        <div class="row">
            <div class="col-lg-8">
                <?php $form = ActiveForm::begin([
                    'options' => ['enctype' => 'multipart/form-data'],
                ]); ?>
                <?= $form->field($model, 'name') ?>

                <hr>
                <div class="form-group">
                    <?= Html::submitButton('Добавить', [
                        'class' => 'btn btn-default',
                        'name'  => 'contact-button',
                        'style' => 'width:100%',
                    ]) ?>
                </div>
                <?php ActiveForm::end(); ?>
            </div>
        </div>

    <?php endif; ?>

</div>

