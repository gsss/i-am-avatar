<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;
use app\models\UnionCategory;
use yii\db\Query;
use yii\helpers\ArrayHelper;

/** @var $this  \yii\web\View */
/** @var $form  \yii\bootstrap\ActiveForm */
/** @var $model \common\models\task\Table */
/** @var $school \common\models\school\School */

$this->title = $model->name;

?>
<div class="container">

    <h1 class="page-header"><?= Html::encode($this->title) ?></h1>

    <table class="table table-hover table-striped">
        <tr>
            <th>Наименование</th>
            <th>Стоимость</th>
            <th>Ед изм</th>
            <th>27 апр</th>
            <th>28 апр</th>
            <th>29 апр</th>
            <th>01 мая</th>
            <th>02 мая</th>
            <th>03 мая</th>
        </tr>
        <tr>
            <td>раздача+кассир</td>
            <td>220</td>
            <td>час</td>
            <td><img class="img-circle" src="https://cloud1.cloud999.ru/upload/cloud/15732/92236_1DUw7WNREa_crop.jpg" height="20px" alt=""></td>
            <td><img class="img-circle" src="https://cloud1.cloud999.ru/upload/cloud/15732/92236_1DUw7WNREa_crop.jpg" height="20px" alt=""></td>
            <td><img class="img-circle" src="https://cloud1.cloud999.ru/upload/cloud/15732/92236_1DUw7WNREa_crop.jpg" height="20px" alt=""></td>
            <td><img class="img-circle" src="https://cloud1.cloud999.ru/upload/cloud/15732/92236_1DUw7WNREa_crop.jpg" height="20px" alt=""></td>
            <td><img class="img-circle" src="https://cloud1.cloud999.ru/upload/cloud/15732/92236_1DUw7WNREa_crop.jpg" height="20px" alt=""></td>
            <td><img class="img-circle" src="https://cloud1.cloud999.ru/upload/cloud/15732/92236_1DUw7WNREa_crop.jpg" height="20px" alt=""></td>
        </tr>
        <tr>
            <td>раздача+кассир</td>
            <td>220</td>
            <td>час</td>
            <td><img class="img-circle" src="https://cloud1.cloud999.ru/upload/cloud/15732/92236_1DUw7WNREa_crop.jpg" height="20px" alt=""></td>
            <td><img class="img-circle" src="https://cloud1.cloud999.ru/upload/cloud/15732/92236_1DUw7WNREa_crop.jpg" height="20px" alt=""></td>
            <td><img class="img-circle" src="https://cloud1.cloud999.ru/upload/cloud/15732/92236_1DUw7WNREa_crop.jpg" height="20px" alt=""></td>
            <td><img class="img-circle" src="https://cloud1.cloud999.ru/upload/cloud/15732/92236_1DUw7WNREa_crop.jpg" height="20px" alt=""></td>
            <td><img class="img-circle" src="https://cloud1.cloud999.ru/upload/cloud/15732/92236_1DUw7WNREa_crop.jpg" height="20px" alt=""></td>
            <td><img class="img-circle" src="https://cloud1.cloud999.ru/upload/cloud/15732/92236_1DUw7WNREa_crop.jpg" height="20px" alt=""></td>
        </tr>
        <tr>
            <td>раздача+кассир</td>
            <td>220</td>
            <td>час</td>
            <td><img class="img-circle" src="https://cloud1.cloud999.ru/upload/cloud/15732/92236_1DUw7WNREa_crop.jpg" height="20px" alt=""></td>
            <td><img class="img-circle" src="https://cloud1.cloud999.ru/upload/cloud/15732/92236_1DUw7WNREa_crop.jpg" height="20px" alt=""></td>
            <td><img class="img-circle" src="https://cloud1.cloud999.ru/upload/cloud/15732/92236_1DUw7WNREa_crop.jpg" height="20px" alt=""></td>
            <td><img class="img-circle" src="https://cloud1.cloud999.ru/upload/cloud/15732/92236_1DUw7WNREa_crop.jpg" height="20px" alt=""></td>
            <td><img class="img-circle" src="https://cloud1.cloud999.ru/upload/cloud/15732/92236_1DUw7WNREa_crop.jpg" height="20px" alt=""></td>
            <td><img class="img-circle" src="https://cloud1.cloud999.ru/upload/cloud/15732/92236_1DUw7WNREa_crop.jpg" height="20px" alt=""></td>
        </tr>
        <tr>
            <td>раздача+кассир</td>
            <td>220</td>
            <td>час</td>
            <td><img class="img-circle" src="https://cloud1.cloud999.ru/upload/cloud/15732/92236_1DUw7WNREa_crop.jpg" height="20px" alt=""></td>
            <td><img class="img-circle" src="https://cloud1.cloud999.ru/upload/cloud/15732/92236_1DUw7WNREa_crop.jpg" height="20px" alt=""></td>
            <td><img class="img-circle" src="https://cloud1.cloud999.ru/upload/cloud/15732/92236_1DUw7WNREa_crop.jpg" height="20px" alt=""></td>
            <td><img class="img-circle" src="https://cloud1.cloud999.ru/upload/cloud/15732/92236_1DUw7WNREa_crop.jpg" height="20px" alt=""></td>
            <td><img class="img-circle" src="https://cloud1.cloud999.ru/upload/cloud/15732/92236_1DUw7WNREa_crop.jpg" height="20px" alt=""></td>
            <td><img class="img-circle" src="https://cloud1.cloud999.ru/upload/cloud/15732/92236_1DUw7WNREa_crop.jpg" height="20px" alt=""></td>
        </tr>
        <tr>
            <td>раздача+кассир</td>
            <td>220</td>
            <td>час</td>
            <td><img class="img-circle" src="https://cloud1.cloud999.ru/upload/cloud/15732/92236_1DUw7WNREa_crop.jpg" height="20px" alt=""></td>
            <td><img class="img-circle" src="https://cloud1.cloud999.ru/upload/cloud/15732/92236_1DUw7WNREa_crop.jpg" height="20px" alt=""></td>
            <td><img class="img-circle" src="https://cloud1.cloud999.ru/upload/cloud/15732/92236_1DUw7WNREa_crop.jpg" height="20px" alt=""></td>
            <td><img class="img-circle" src="https://cloud1.cloud999.ru/upload/cloud/15732/92236_1DUw7WNREa_crop.jpg" height="20px" alt=""></td>
            <td><img class="img-circle" src="https://cloud1.cloud999.ru/upload/cloud/15732/92236_1DUw7WNREa_crop.jpg" height="20px" alt=""></td>
            <td><img class="img-circle" src="https://cloud1.cloud999.ru/upload/cloud/15732/92236_1DUw7WNREa_crop.jpg" height="20px" alt=""></td>
        </tr>
        <tr>
            <td>раздача+кассир</td>
            <td>220</td>
            <td>час</td>
            <td><img class="img-circle" src="https://cloud1.cloud999.ru/upload/cloud/15732/92236_1DUw7WNREa_crop.jpg" height="20px" alt=""></td>
            <td><img class="img-circle" src="https://cloud1.cloud999.ru/upload/cloud/15732/92236_1DUw7WNREa_crop.jpg" height="20px" alt=""></td>
            <td><img class="img-circle" src="https://cloud1.cloud999.ru/upload/cloud/15732/92236_1DUw7WNREa_crop.jpg" height="20px" alt=""></td>
            <td><img class="img-circle" src="https://cloud1.cloud999.ru/upload/cloud/15732/92236_1DUw7WNREa_crop.jpg" height="20px" alt=""></td>
            <td><img class="img-circle" src="https://cloud1.cloud999.ru/upload/cloud/15732/92236_1DUw7WNREa_crop.jpg" height="20px" alt=""></td>
            <td><img class="img-circle" src="https://cloud1.cloud999.ru/upload/cloud/15732/92236_1DUw7WNREa_crop.jpg" height="20px" alt=""></td>
        </tr>
        <tr>
            <td>раздача+кассир</td>
            <td>220</td>
            <td>час</td>
            <td><img class="img-circle" src="https://cloud1.cloud999.ru/upload/cloud/15732/92236_1DUw7WNREa_crop.jpg" height="20px" alt=""></td>
            <td><img class="img-circle" src="https://cloud1.cloud999.ru/upload/cloud/15732/92236_1DUw7WNREa_crop.jpg" height="20px" alt=""></td>
            <td><img class="img-circle" src="https://cloud1.cloud999.ru/upload/cloud/15732/92236_1DUw7WNREa_crop.jpg" height="20px" alt=""></td>
            <td><img class="img-circle" src="https://cloud1.cloud999.ru/upload/cloud/15732/92236_1DUw7WNREa_crop.jpg" height="20px" alt=""></td>
            <td><img class="img-circle" src="https://cloud1.cloud999.ru/upload/cloud/15732/92236_1DUw7WNREa_crop.jpg" height="20px" alt=""></td>
            <td><img class="img-circle" src="https://cloud1.cloud999.ru/upload/cloud/15732/92236_1DUw7WNREa_crop.jpg" height="20px" alt=""></td>
        </tr>
        <tr>
            <td>раздача+кассир</td>
            <td>220</td>
            <td>час</td>
            <td><img class="img-circle" src="https://cloud1.cloud999.ru/upload/cloud/15732/92236_1DUw7WNREa_crop.jpg" height="20px" alt=""></td>
            <td><img class="img-circle" src="https://cloud1.cloud999.ru/upload/cloud/15732/92236_1DUw7WNREa_crop.jpg" height="20px" alt=""></td>
            <td><img class="img-circle" src="https://cloud1.cloud999.ru/upload/cloud/15732/92236_1DUw7WNREa_crop.jpg" height="20px" alt=""></td>
            <td><img class="img-circle" src="https://cloud1.cloud999.ru/upload/cloud/15732/92236_1DUw7WNREa_crop.jpg" height="20px" alt=""></td>
            <td><img class="img-circle" src="https://cloud1.cloud999.ru/upload/cloud/15732/92236_1DUw7WNREa_crop.jpg" height="20px" alt=""></td>
            <td><img class="img-circle" src="https://cloud1.cloud999.ru/upload/cloud/15732/92236_1DUw7WNREa_crop.jpg" height="20px" alt=""></td>
        </tr>
        <tr>
            <td>раздача+кассир</td>
            <td>220</td>
            <td>час</td>
            <td><img class="img-circle" src="https://cloud1.cloud999.ru/upload/cloud/15732/92236_1DUw7WNREa_crop.jpg" height="20px" alt=""></td>
            <td><img class="img-circle" src="https://cloud1.cloud999.ru/upload/cloud/15732/92236_1DUw7WNREa_crop.jpg" height="20px" alt=""></td>
            <td><img class="img-circle" src="https://cloud1.cloud999.ru/upload/cloud/15732/92236_1DUw7WNREa_crop.jpg" height="20px" alt=""></td>
            <td><img class="img-circle" src="https://cloud1.cloud999.ru/upload/cloud/15732/92236_1DUw7WNREa_crop.jpg" height="20px" alt=""></td>
            <td><img class="img-circle" src="https://cloud1.cloud999.ru/upload/cloud/15732/92236_1DUw7WNREa_crop.jpg" height="20px" alt=""></td>
            <td><img class="img-circle" src="https://cloud1.cloud999.ru/upload/cloud/15732/92236_1DUw7WNREa_crop.jpg" height="20px" alt=""></td>
        </tr>
        <tr>
            <td>раздача+кассир</td>
            <td>220</td>
            <td>час</td>
            <td><img class="img-circle" src="https://cloud1.cloud999.ru/upload/cloud/15732/92236_1DUw7WNREa_crop.jpg" height="20px" alt=""></td>
            <td><img class="img-circle" src="https://cloud1.cloud999.ru/upload/cloud/15732/92236_1DUw7WNREa_crop.jpg" height="20px" alt=""></td>
            <td><img class="img-circle" src="https://cloud1.cloud999.ru/upload/cloud/15732/92236_1DUw7WNREa_crop.jpg" height="20px" alt=""></td>
            <td><img class="img-circle" src="https://cloud1.cloud999.ru/upload/cloud/15732/92236_1DUw7WNREa_crop.jpg" height="20px" alt=""></td>
            <td><img class="img-circle" src="https://cloud1.cloud999.ru/upload/cloud/15732/92236_1DUw7WNREa_crop.jpg" height="20px" alt=""></td>
            <td><img class="img-circle" src="https://cloud1.cloud999.ru/upload/cloud/15732/92236_1DUw7WNREa_crop.jpg" height="20px" alt=""></td>
        </tr>
    </table>



</div>

