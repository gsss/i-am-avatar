<?php

/** @var $this \yii\web\View */

use yii\helpers\Url;
use app\services\GsssHtml;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use app\services\GetArticle\Collection;

$this->title = 'Все Идеи';

?>

<div class="container">
    <div class="col-lg-12">
        <h1 class="page-header"><?= Html::encode($this->title) ?></h1>
    </div>

    <div class="col-lg-8">
        <a href="<?= Url::to(['cabinet-idea/add']) ?>" class="btn btn-default">Добавить</a>
        <?php \yii\widgets\Pjax::begin(); ?>
        <?php
        $this->registerJS(<<<JS

$('.rowTable').click(function(e) {
    window.location = '/cabinet-idea/view' + '?' + 'id' + '=' + $(this).data('id');
});

JS
        );
        ?>
        <?= \yii\grid\GridView::widget([
            'dataProvider' => new \yii\data\ActiveDataProvider([
                'query'      => \common\models\UserIdea::find()
                    ->where(['is_hide' => 0])
                    ->orderBy(['created_at' => SORT_DESC])

                ,
                'pagination' => [
                    'pageSize' => 100,
                ],
            ]),
            'tableOptions' => [
                'class' => 'table table-striped table-hover',
            ],
            'rowOptions'   => function ($item) {
                $data = [
                    'data'  => ['id' => $item['id']],
                    'role'  => 'button',
                    'class' => 'rowTable',
                ];
                return $data;
            },
            'columns'      => [
                'id',
                [
                    'header'  => 'Кто',
                    'content' => function ($item) {
                        $user = \common\models\UserAvatar::findOne($item['user_id']);

                        return Html::img($user->getAvatar(), [
                            'class'  => "img-circle",
                            'width'  => 40,
                            'height' => 40,
                            'title' => \yii\helpers\Html::encode($user->getName2()),
                            'data' => ['toggle' => 'tooltip'],
                            'style'  => 'margin-bottom: 0px;',
                        ]);
                    }
                ],
                'name',
                [
                    'header'  => 'Создано',
                    'content' => function ($item) {
                        $v = \yii\helpers\ArrayHelper::getValue($item, 'created_at', 0);
                        if ($v == 0) return '';

                        return Html::tag('abbr', \cs\services\DatePeriod::back($v, ['isShort' => true]), ['data' => ['toggle' => 'tooltip'], 'title' => Yii::$app->formatter->asDatetime($v)]);
                    }
                ],
            ],
        ]) ?>
        <?php \yii\widgets\Pjax::end(); ?>


    </div>
    <div class="col-lg-4">
        <?= $this->render('../cabinet/_menu') ?>
    </div>
</div>

<div class="modal fade" id="modalInfo" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="myModalLabel">Информация</h4>
            </div>
            <div class="modal-body">
                Успешно!
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
            </div>
        </div>
    </div>
</div>