<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;
use app\models\UnionCategory;
use yii\db\Query;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \common\models\UserIdea */

$this->title = $model->name;

?>
<div class="container">
    <div class="col-lg-12">
        <h1 class="page-header"><?= Html::encode($this->title) ?></h1>
    </div>

        <div class="col-lg-8">

        <?= \yii\widgets\DetailView::widget([
            'model' => $model,
            'attributes' => [
                [
                    'label' => 'Заголовок',
                    'captionOptions' => [
                            'style' => 'width: 30%'
                    ],
                    'value' => $model->name,
                ],
                'created_at:datetime:Создано',
                [
                    'label' => 'Автор',
                    'format' => 'html',
                    'value' => Html::img($model->getUser()->getAvatar(), ['class' => 'img-circle', 'width' => 30]),
                ],
                [
                    'label' => 'Содержание',
                    'format' => 'html',
                    'value' => nl2br($model->content),
                ],
            ],
        ]) ?>
        <?= \avatar\modules\Comment\Module::getComments2(3, $model->id); ?>


    </div>
    <div class="col-lg-4">
        <?= $this->render('../cabinet/_menu') ?>
    </div>

</div>
