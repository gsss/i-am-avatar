<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;

/* @var $this yii\web\View */

$this->title = Yii::t('c.THcu48fLi3', 'Автор подписи в контрактах');

$passport = \common\models\PassportLink::findOne(['user_id' => Yii::$app->user->id]);

if (is_null($passport)) {
    $default = null;
} else {
    $default = $passport->billing_id;
}

Yii::$app->session->set('default', $default);

?>
<div class="container">
    <div class="col-lg-12">
        <h1 class="page-header"><?= Html::encode($this->title) ?></h1>

    </div>
    <div class="col-lg-8">

        <?php if (Yii::$app->session->hasFlash('form')) { ?>
            <p class="alert alert-success"><?= \Yii::t('c.THcu48fLi3', 'Успешно') ?></p>
            <a href="/cabinet-passport/index" class="btn btn-default"><?= \Yii::t('c.THcu48fLi3', 'Назад') ?></a>
            <a href="/cabinet-bills/index" class="btn btn-default"><?= \Yii::t('c.THcu48fLi3', 'Счета') ?></a>
        <?php } else { ?>
            <div id="address"></div>
            <?php $form = ActiveForm::begin() ?>
            <?= \yii\grid\GridView::widget([
                'dataProvider' => new \yii\data\ActiveDataProvider([
                    'query' => \common\models\avatar\UserBill::find()
                        ->where([
                            'mark_deleted' => 0,
                            'user_id'      => Yii::$app->user->id,
                            'currency'     => \common\models\avatar\Currency::ETH,
                        ])
                ]),
                'tableOptions' => [
                    'class' => 'table table-hover table-striped',
                    'id'    => 'tableTransaction',
                ],
                'columns'      => [
                    [
                        'header'  => '#',
                        'content' => function ($item) {
                            return Html::img('/images/controller/cabinet-passport/index/eth.png', ['width' => 40]);
                        },
                    ],
                    'name',
                    [
                        'header'  => 'address',
                        'content' => function ($item) {
                            return Html::tag('code', $item['address']);
                        },
                    ],
                    [
                        'header'  => Yii::t('c.THcu48fLi3', 'По умолчанию'),
                        'content' => function ($item) {
                            $default = Yii::$app->session->get('default');
                            if (is_null($default)) {
                                return Html::submitButton(Yii::t('c.THcu48fLi3', 'Включить'), ['name' => 'billing', 'value' => $item['id'], 'class' => 'btn btn-default btn-xs buttonSet', 'data' => ['id' => $item['id']]]);
                            } else {
                                if ($item->id == $default) {
                                    return Html::button(Yii::t('c.THcu48fLi3', 'Активен'), ['class' => 'btn btn-info btn-xs']);
                                } else {
                                    return Html::submitButton(Yii::t('c.THcu48fLi3', 'Включить'), ['name' => 'billing', 'value' => $item['id'], 'class' => 'btn btn-default btn-xs buttonSet', 'data' => ['id' => $item['id']]]);
                                }
                            }
                        },
                    ],
                ]
            ]) ?>
            <?php ActiveForm::end() ?>
        <?php } ?>
    </div>
    <div class="col-lg-4">
        <?= $this->render('../cabinet/_menu') ?>
    </div>
</div>



