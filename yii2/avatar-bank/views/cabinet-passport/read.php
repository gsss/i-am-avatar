<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;
use app\models\UnionCategory;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model cs\base\BaseForm */

$this->title = 'Чтение из контракта';


?>
<div class="container">
    <div class="col-lg-12">
        <h1 class="page-header"><?= Html::encode($this->title) ?></h1>

    </div>
    <div class="col-lg-8">

        <div id="formRead" class="collapse in">

            <div class="form-group recipient-field-password1">
                <label for="recipient-password1" class="control-label">password1</label>
                <input type="password" class="form-control" id="recipient-password1" name="password1"
                       style="font-family: Consolas, Courier New, monospace"
                >
                <p class="help-block"></p>
                <p class="help-block help-block-error" style="display: none;"></p>
            </div>
            <div class="form-group recipient-field-id">
                <label for="recipient-id" class="control-label">ID</label>
                <input type="text" class="form-control" id="recipient-id" name="id"
                       style="font-family: Consolas, Courier New, monospace"
                >
                <p class="help-block"></p>
                <p class="help-block help-block-error" style="display: none;"></p>
            </div>


            <hr class="featurette-divider">
            <div class="form-group">
                <?= Html::button('Получить', [
                    'class' => 'btn btn-default',
                    'id'    => 'buttonRead',
                    'style' => 'width: 100%;',
                ]) ?>
            </div>
        </div>
        <p id="result"></p>
        <?php
        $this->registerJs(<<<JS
        
var functionRead = function(e) {
    var b = $(this);
    b.off('click');
    b.html($('<i>', {class: 'fa fa-spinner fa-spin fa-fw'}));
    b.attr('disabled', 'disabled');
    ajaxJson({
        url: '/cabinet-passport/read-ajax',
        data: {
             id: $('#recipient-id').val(),
             password: $('#recipient-password1').val()
        },
        success: function(ret) {
            $('#formRead').collapse('hide');
            $('#result').html(ret.jsonString);
        },
        errorScript: function(ret) {
            b.on('click', functionRead);
            b.html('Получить');
            b.removeAttr('disabled');
        }
    });
};
$('#buttonRead').click(functionRead);

JS
);
        ?>



    </div>
    <div class="col-lg-4">
        <?= $this->render('../cabinet/_menu') ?>
    </div>
</div>



