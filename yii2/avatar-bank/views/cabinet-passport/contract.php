<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;
use app\models\UnionCategory;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model cs\base\BaseForm */

$this->title = 'Регистрация в контракте';

$p = new \avatar\modules\ETH\ServiceEtherScan();

?>
<div class="container">
    <div class="col-lg-12">
        <h1 class="page-header"><?= Html::encode($this->title) ?></h1>

    </div>
    <div class="col-lg-8">
        <?php if (Yii::$app->session->hasFlash('form')): ?>

            <div class="alert alert-success">
                Успешно обновлено.
            </div>
            <?php $hash = Yii::$app->session->getFlash('form') ?>
            <p><code><?= Html::a($hash, $p->getUrl('/tx/'.$hash), ['target' => '_blank']) ?></code></p>

        <?php else: ?>

            <p><a href="/cabinet-passport/read" class="btn btn-default">Чтение</a></p>

            <?php $form = ActiveForm::begin([
                'id'      => 'contact-form',
                'options' => ['enctype' => 'multipart/form-data'],
            ]); ?>

            <?= $form->field($model, 'password')->passwordInput() ?>
            <?= $form->field($model, 'name_first') ?>
            <?= $form->field($model, 'name_last') ?>
            <?= $form->field($model, 'name_middle') ?>
            <?= $form->field($model, 'born_time')->widget(
                '\kartik\time\TimePicker',
                [
                    'pluginOptions' => [
                        'showSeconds'  => true,
                        'showMeridian' => false,
                        'minuteStep'   => 1,
                        'secondStep'   => 5,
                    ]
                ]) ?>
            <?= $form->field($model, 'born_date')->widget('\common\widgets\DatePicker\DatePicker', ['dateFormat' => 'php:d.m.Y']) ?>
            <?= $form->field($model, 'born_place') ?>
            <?= $form->field($model, 'born_point')->widget('\common\widgets\PlaceMapYandex\PlaceMap') ?>
            <?= $form->field($model, 'data')->textarea(['rows' => 7]) ?>
            <?= $form->field($model, 'image')->widget('\common\widgets\FileUpload4\FileUploadMany') ?>

            <hr class="featurette-divider">
            <div class="form-group">
                <?= Html::submitButton('Добавить', [
                    'class' => 'btn btn-default',
                    'name'  => 'contact-button',
                    'style' => 'width: 100%;',
                ]) ?>
            </div>
            <?php ActiveForm::end(); ?>

        <?php endif; ?>
    </div>
    <div class="col-lg-4">
        <?= $this->render('../cabinet/_menu') ?>
    </div>
</div>



