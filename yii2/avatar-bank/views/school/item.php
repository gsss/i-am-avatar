<?php

use yii\helpers\Html;
use yii\helpers\Url;


/** @var \common\models\school\School $item */


/** @var \yii\web\View $this */
$this->title = \yii\helpers\Html::encode($item->name);


\common\assets\iLightBox\Asset::register($this);
$this->registerJs(<<<JS
$('.ilightbox').iLightBox();
JS
);
\avatar\assets\Paralax::register($this);


?>

<div class="parallax-window"
     data-parallax="scroll"
     data-image-src="https://cloud1.cloud999.ru/upload/cloud/15609/46797_HR6Ov29Cgz.jpg"

     style="min-height: 450px; background: transparent;">

    <div class="container">
        <div class="col-sm-4 col-sm-offset-4">
            <p style="padding-top: 70px; margin-bottom: 0px;"><img
                        src="<?= $item->image ?>"
                        class="img-circle" width="100%"
                        style="text-shadow: 0px 10px 20px rgba(0, 0, 0, 0.5);color:white;"></p>
        </div>
    </div>

</div>
<style>
    .iconCodon {
        font-size: 24px;
    }

    .tdCenter {
        text-align: center;
    }

    .thumbnail2 {
        border-radius: 5px;
        border: 1px solid #888;
    }
</style>
<div class="container" style="padding-bottom: 70px;">
    <div class="col-lg-12">
        <h1 class="page-header text-center">
            <?= \yii\helpers\Html::encode($item->name) ?>
        </h1>
    </div>


    <!-- Биография -->
    <div class="col-lg-12">
        <h1 class="page-header text-center">
            Школа
        </h1>
    </div>
    <div class="row">
        <div class="col-lg-8 col-lg-offset-2">
            <?= \yii\widgets\DetailView::widget([
                'model'      => $item,
                'options'    => ['class' => 'table table-striped table-hover detail-view'],
                'attributes' => [
                    'name'        => [
                        'label'          => 'Название',
                        'captionOptions' => [
                            'style' => 'width: 30%',
                            'class' => 'text-right',
                        ],
                        'value'          => $item->name,
                    ],
                    'description' => [
                        'label'          => 'Описание',
                        'captionOptions' => [
                            'style' => 'width: 30%',
                            'class' => 'text-right',
                        ],
                        'format'         => 'html',
                        'value'          => nl2br(Html::encode($item->description)),
                    ],
                    'dns'         => [
                        'captionOptions' => [
                            'style' => 'width: 30%',
                            'class' => 'text-right',
                        ],
                        'label'          => 'Ссылка',
                        'format'         => 'html',
                        'value'          => Html::a($item->getUrl(), $item->getUrl(), ['target' => '_blank']),
                    ],
                    'created_at'  => [
                        'captionOptions' => [
                            'style' => 'width: 30%',
                            'class' => 'text-right',
                        ],
                        'label'          => 'Создана',
                        'format'         => 'datetime',
                        'value'          => $item->created_at,
                    ],
                ],
            ]) ?>
        </div>
    </div>

    <!-- Биография -->
    <div class="col-lg-12">
        <h1 class="page-header text-center">
            Описание
        </h1>
    </div>
    <div class="row">
        <div class="col-lg-8 col-lg-offset-2">
            <?= \avatar\services\Html::getHtml($item->about) ?>
        </div>
    </div>

    <div class="col-lg-12">
        <h1 class="page-header text-center">
            Автор
        </h1>
    </div>
</div>

<?php

$user = \common\models\UserAvatar::findOne($item->user_id);
$userMaster = \common\models\UserMaster::findOne(['user_id' => $user->id]);

$certList = \common\models\school\Sertificate::find()->where(['user_id' => $user->id])->all();

$rows = (count($certList) + 2) / 3;
$rows = (int)$rows;
?>
<div class="parallax-window"
     data-parallax="scroll"
     data-image-src="//www.i-am-avatar.com/upload/cloud/1546392582_SLqjARoGrO.jpg"

     style="min-height: 450px; background: transparent;">

    <div class="container">
        <div class="col-sm-4 col-sm-offset-4">
            <p style="padding-top: 70px; margin-bottom: 0px;"><img
                        src="<?= $user->getAvatar() ?>"
                        class="img-circle" width="100%"
                        style="text-shadow: 0px 10px 20px rgba(0, 0, 0, 0.5);color:white;"></p>
        </div>
    </div>

</div>
<?php if (!is_null($userMaster)) { ?>

    <div class="container" style="padding-bottom: 70px;">
        <div class="col-lg-12">
            <h1 class="page-header text-center">
                <?= \yii\helpers\Html::encode($user->getName2()) ?>
            </h1>
        </div>
        <?php if ($userMaster->is_design) { ?>
            <?php if ($user->hasHumanDesign()) { ?>
                <?php $humanDesign = $user->getHumanDesign(); ?>
                <div class="col-lg-12">
                    <p class="text-center">
                        <img src="<?= $humanDesign->image ?>" style="width: 100%; max-width: 600px;">
                    </p>
                </div>

                <?php
                $text = $humanDesign->cross->text;
                $arr = explode('(', $text);
                $text = trim($arr[0]);
                $isError = false;
                if (count($arr) == 1) {
//                                \cs\Application::mail('admin@galaxysss.ru', 'Ошибка', 'admin', [
//                                    'text' => '/galaxysss/views/site/user.php:187 ' . \yii\helpers\VarDumper::dumpAsString([$arr, $humanDesign])
//                                ]);
                    $isError = true;
                } else {
                    $arr = explode(')', $arr[1]);
                    $arr = explode('|', $arr[0]);
                    $new = [];
                    foreach ($arr as $i) {
                        $a = explode('/', $i);
                        $new[] = trim($a[0]);
                        $new[] = trim($a[1]);
                    }
                }
                $gen_sun_personal = \common\models\HdGenKeys::findOne(['num' => $new[0]]);
                $gen_moon_personal = \common\models\HdGenKeys::findOne(['num' => $new[1]]);
                $gen_sun_design = \common\models\HdGenKeys::findOne(['num' => $new[2]]);
                $gen_moon_design = \common\models\HdGenKeys::findOne(['num' => $new[3]]);
                ?>
                <div class="col-lg-12">
                    <h3 class="page-header text-center">Генные ключи</h3>
                    <table align="center" class="table table-striped table-hover" style="width: 800px;">
                        <tr>
                            <td class="iconCodon tdCenter" width="200"><img src="/images/controller/user/item/zred.png"
                                                                            data-toggle="tooltip" title="Дизайн земля"
                                                                            width="30"></td>
                            <td class="iconCodon tdCenter" width="200"><img src="/images/controller/user/item/sred.png"
                                                                            data-toggle="tooltip" title="Дизайн солнце"
                                                                            width="30"></td>
                            <td class="iconCodon tdCenter" width="200"><img
                                        src="/images/controller/user/item/sblack.png"
                                        data-toggle="tooltip" title="Личность солнце"
                                        width="30"></td>
                            <td class="iconCodon tdCenter" width="200"><img
                                        src="/images/controller/user/item/zblack.png"
                                        data-toggle="tooltip" title="Личность земля"
                                        width="30"></td>
                        </tr>
                        <tr>
                            <td class="tdCenter">
                                <a href="<?= Url::to(['human-design/gen-keys-item', 'id' => $gen_moon_design->num]) ?>">
                                    <img src="http://www.galaxysss.ru<?= $gen_moon_design->image_codon ?>" width="50"
                                         class="thumbnail2" data-toggle="tooltip" title="Кодон"
                                         style="text-align: center;">
                                </a>
                            </td>
                            <td class="tdCenter">
                                <a href="<?= Url::to(['human-design/gen-keys-item', 'id' => $gen_sun_design->num]) ?>">
                                    <img src="http://www.galaxysss.ru<?= $gen_sun_design->image_codon ?>" width="50"
                                         class="thumbnail2" data-toggle="tooltip" title="Кодон">
                                </a>
                            </td>
                            <td class="tdCenter">
                                <a href="<?= Url::to(['human-design/gen-keys-item', 'id' => $gen_sun_personal->num]) ?>">
                                    <img src="http://www.galaxysss.ru<?= $gen_sun_personal->image_codon ?>" width="50"
                                         class="thumbnail2" data-toggle="tooltip" title="Кодон">
                                </a>
                            </td>
                            <td class="tdCenter">
                                <a href="<?= Url::to(['human-design/gen-keys-item', 'id' => $gen_moon_personal->num]) ?>">
                                    <img src="http://www.galaxysss.ru<?= $gen_moon_personal->image_codon ?>" width="50"
                                         class="thumbnail2" data-toggle="tooltip" title="Кодон">
                                </a>
                            </td>
                        </tr>
                        <tr>
                            <td class="tdCenter"><span data-toggle="tooltip"
                                                       title="Название"><?= $gen_moon_design->name ?></span></td>
                            <td class="tdCenter"><span data-toggle="tooltip"
                                                       title="Название"><?= $gen_sun_design->name ?></span></td>
                            <td class="tdCenter"><span data-toggle="tooltip"
                                                       title="Название"><?= $gen_sun_personal->name ?></span></td>
                            <td class="tdCenter"><span data-toggle="tooltip"
                                                       title="Название"><?= $gen_moon_personal->name ?></span></td>
                        </tr>
                        <tr>
                            <td class="tdCenter"><span data-toggle="tooltip"
                                                       title="Дар"><?= $gen_moon_design->dar ?></span>
                            </td>
                            <td class="tdCenter"><span data-toggle="tooltip"
                                                       title="Дар"><?= $gen_sun_design->dar ?></span>
                            </td>
                            <td class="tdCenter"><span data-toggle="tooltip"
                                                       title="Дар"><?= $gen_sun_personal->dar ?></span></td>
                            <td class="tdCenter"><span data-toggle="tooltip"
                                                       title="Дар"><?= $gen_moon_personal->dar ?></span></td>
                        </tr>
                        <tr>
                            <td class="tdCenter"><span data-toggle="tooltip"
                                                       title="Тень"><?= $gen_moon_design->ten ?></span></td>
                            <td class="tdCenter"><span data-toggle="tooltip"
                                                       title="Тень"><?= $gen_sun_design->ten ?></span>
                            </td>
                            <td class="tdCenter"><span data-toggle="tooltip"
                                                       title="Тень"><?= $gen_sun_personal->ten ?></span></td>
                            <td class="tdCenter"><span data-toggle="tooltip"
                                                       title="Тень"><?= $gen_moon_personal->ten ?></span></td>
                        </tr>
                        <tr>
                            <td class="tdCenter"><span data-toggle="tooltip"
                                                       title="Сиддхи"><?= $gen_moon_design->siddhi ?></span></td>
                            <td class="tdCenter"><span data-toggle="tooltip"
                                                       title="Сиддхи"><?= $gen_sun_design->siddhi ?></span></td>
                            <td class="tdCenter"><span data-toggle="tooltip"
                                                       title="Сиддхи"><?= $gen_sun_personal->siddhi ?></span></td>
                            <td class="tdCenter"><span data-toggle="tooltip"
                                                       title="Сиддхи"><?= $gen_moon_personal->siddhi ?></span></td>
                        </tr>
                    </table>
                    <table class="table table-striped table-hover" style="width: auto;" align="center">
                        <tr>
                            <td>Тип</td>
                            <td>
                                <?= $humanDesign->type->text ?>
                            </td>
                        </tr>
                        <tr>
                            <td>Профиль</td>
                            <td>
                                <?= $humanDesign->profile->text ?>
                            </td>
                        </tr>
                        <tr>
                            <td>Определение</td>
                            <td><?= $humanDesign->definition->text ?></td>
                        </tr>
                        <tr>
                            <td>Внутренний Авторитет</td>
                            <td><?= $humanDesign->inner->text ?></td>
                        </tr>
                        <tr>
                            <td>Стратегия</td>
                            <td><?= $humanDesign->strategy->text ?></td>
                        </tr>
                        <tr>
                            <td>Тема ложного Я</td>
                            <td><?= $humanDesign->theme->text ?></td>
                        </tr>
                        <tr>
                            <td>Инкарнационный крест</td>

                            <td><?= $text ?>
                                <?php if (!$isError) { ?>
                                    (<a
                                            href="<?= Url::to(['human-design/gen-keys-item', 'id' => $new[0]]) ?>"><?= $new[0] ?></a>/
                                    <a
                                            href="<?= Url::to(['human-design/gen-keys-item', 'id' => $new[1]]) ?>"><?= $new[1] ?></a>
                                    | <a
                                            href="<?= Url::to(['human-design/gen-keys-item', 'id' => $new[2]]) ?>"><?= $new[2] ?></a>/
                                    <a
                                            href="<?= Url::to(['human-design/gen-keys-item', 'id' => $new[3]]) ?>"><?= $new[3] ?></a>)
                                <?php } ?>
                            </td>
                        </tr>
                    </table>

                </div>


            <?php } ?>
        <?php } ?>

        <!-- Биография -->
        <?php if (!\cs\Application::isEmpty($userMaster->bio)) { ?>
            <div class="col-lg-12">
                <h1 class="page-header text-center">
                    Биография
                </h1>
            </div>
            <div class="row">
                <div class="col-lg-8 col-lg-offset-2">
                    <?= $userMaster->bio ?>
                </div>
            </div>
        <?php } ?>

        <!-- Возможности -->
        <?php if (!\cs\Application::isEmpty($userMaster->vozm)) { ?>
            <div class="col-lg-12">
                <h1 class="page-header text-center">
                    Возможности
                </h1>
            </div>
            <div class="row">
                <div class="col-lg-8 col-lg-offset-2">
                    <?= $userMaster->vozm ?>
                </div>
            </div>
        <?php } ?>


        <!-- Сертификаты -->
        <?php if ($userMaster->is_sert) { ?>
            <?php if (count($certList) > 0) { ?>
                <div class="col-lg-12">
                    <h1 class="page-header text-center">
                        Сертификаты
                    </h1>
                    <div class="popup-gallery">
                        <?php for ($count = 0; $count < count($certList); $count++) { ?>
                            <?php /** @var \common\models\school\Sertificate $c */ ?>
                            <?php $c = $certList[$count]; ?>
                            <div class="col-lg-4">
                                <a href="<?= $c->image ?>" class="ilightbox">
                                    <img src="<?= \common\widgets\FileUpload7\FileUpload::getFile($c->image, 'crop') ?>" width="100%" class="thumbnail">
                                </a>
                                <p class="lead text-center">
                                    <?= $c->name ?>
                                </p>
                            </div>
                        <?php } ?>
                    </div>
                </div>
            <?php } ?>
        <?php } ?>

        <!-- Контакты -->
        <div class="col-lg-12">
            <h1 class="page-header text-center">
                Контакты
            </h1>
        </div>
        <div class="row">
            <div class="col-lg-8 col-lg-offset-2">

                <?= \yii\widgets\DetailView::widget([
                    'model'      => $userMaster,
                    'attributes' => [
                        [
                            'label'          => 'ВКонтакте',
                            'captionOptions' => [
                                'style' => 'width: 30%',
                            ],
                            'format'         => 'html',
                            'value'          => Html::a($userMaster->contact_vk, $userMaster->contact_vk, ['target' => '_blank']),
                        ],
                        [
                            'label'  => 'Facebook',
                            'format' => 'html',
                            'value'  => Html::a($userMaster->contact_fb, $userMaster->contact_fb, ['target' => '_blank']),
                        ],
                        [
                            'label'  => 'YouTube',
                            'format' => 'html',
                            'value'  => Html::a($userMaster->contact_youtube, $userMaster->contact_youtube, ['target' => '_blank']),
                        ],
                        [
                            'label'  => 'Instagram',
                            'format' => 'html',
                            'value'  => Html::a($userMaster->contact_instagram, $userMaster->contact_instagram, ['target' => '_blank']),
                        ],
                        [
                            'label'  => 'tweeter',
                            'format' => 'html',
                            'value'  => Html::a($userMaster->contact_tweeter, $userMaster->contact_tweeter, ['target' => '_blank']),
                        ],
                        [
                            'label'  => 'Telegram',
                            'format' => 'html',
                            'value'  => \yii\helpers\StringHelper::startsWith($userMaster->contact_telegram, '@') ? Html::a($userMaster->contact_telegram, 'tg://resolve?domain=' . substr($userMaster->contact_telegram, 1), ['target' => '_blank']) : $userMaster->contact_telegram,
                        ],
                        [
                            'label' => 'WhatsApp',
                            'value' => $userMaster->contact_whatsapp,
                        ],
                        [
                            'label' => 'Viber',
                            'value' => $userMaster->contact_viber,
                        ],
                        [
                            'label' => 'Телефон',
                            'value' => $userMaster->contact_phone,
                        ],
                        [
                            'label' => 'Почта',
                            'value' => $userMaster->contact_email,
                        ],
                    ],
                ]) ?>
            </div>
        </div>


    </div>

<?php } else { ?>
    <div class="container" style="padding-bottom: 70px;">
        <div class="col-lg-12">
            <h1 class="page-header text-center">
                ❤❤❤
            </h1>
        </div>
    </div>
<?php } ?>
