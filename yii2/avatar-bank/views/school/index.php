<?php
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $user \common\models\UserAvatar */

$this->title = 'Школы';
?>

<div class="container" style="padding-bottom: 70px;">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header text-center"><?= $this->title ?></h1>

            <?php \yii\widgets\Pjax::begin(); ?>
            <?php
            $this->registerJS(<<<JS
$('[data-toggle="tooltip"]').tooltip();

$('.rowTable').click(function() {
    window.location = '/school/' +  $(this).data('id');
});
JS
            );
            ?>
            <?= \yii\grid\GridView::widget([
                'dataProvider' => new \yii\data\ActiveDataProvider([
                    'query'      => \common\models\school\School::find()
                        ->where(['is_catalog' => 1])
                        ->andWhere(['school.is_hide' => 0])

                    ,
                    'pagination' => [
                        'pageSize' => 20,
                    ],
                    'sort'       => [
                        'defaultOrder' => ['created_at' => SORT_DESC],
                    ],
                ]),
                'tableOptions' => [
                    'class' => 'table table-striped table-hover',
                ],
                'rowOptions'   => function ($item) {
                    $data = [
                        'data'  => ['id' => $item['id']],
                        'role'  => 'button',
                        'class' => 'rowTable',
                    ];
                    return $data;
                },
                'columns'      => [
                    'id',
                    [
                        'header'  => 'Фото',
                        'content' => function ($item) {
                            $i = ArrayHelper::getValue($item, 'image', '');
                            if ($i == '') return '';

                            return Html::img($i, [
                                'class'  => "img-circle",
                                'width'  => 80,
                                'height' => 80,
                                'style'  => 'margin-bottom: 0px;',
                            ]);
                        },
                    ],
                    [
                        'header'    => 'Название',
                        'attribute' => 'name',
                    ],
                    [
                        'header'  => 'Описание',
                        'content' => function ($item) {
                            $i = ArrayHelper::getValue($item, 'description', '');
                            if ($i == '') return '';

                            return nl2br(Html::encode($i));
                        },
                    ],
                ],
            ]) ?>
            <?php \yii\widgets\Pjax::end(); ?>

        </div>
    </div>
</div>


