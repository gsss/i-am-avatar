<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;
use app\models\UnionCategory;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $listItem \common\models\PaySystemListItem */

$this->title = $listItem->name . ': Платежные системы';

$this->registerJS(<<<JS
    $('.rowTable').click(function() {
        var paysystem_id = $(this).data('paysystem_id');
        var list_id = $(this).data('list_id');
        window.location = '/admin-paysystem-config/item-edit?' +
         'paysystem_id' + '=' + paysystem_id
         + '&' +
         'list_id' + '=' + list_id
         ;
    });

JS
);
Yii::$app->session->set('/admin-paysystem-config/item.php::list_id', $listItem->id);
?>
<div class="container">
    <h1 class="page-header"><?= Html::encode($this->title) ?></h1>

    <style>
        .tableMy .right {
            text-align: right;
        }
    </style>
    <?= \yii\grid\GridView::widget([
        'tableOptions' => [
            'class' => 'table tableMy table-striped table-hover',
        ],
        'rowOptions'   => function ($item) {
            $params = [
                'data'  => [
                    'paysystem_id' => $item['paysystem_id'],
                    'list_id'      => Yii::$app->session->get('/admin-paysystem-config/item.php::list_id')
                ],
                'role'  => 'button',
                'class' => 'rowTable'
            ];
            if (!isset($item['id'])) {
                $params['style'] = 'opacity: 0.3;';
            }
            return $params;
        },
        'dataProvider' => new \yii\data\ActiveDataProvider([
            'query'      => \common\models\PaySystemConfig::find()
                ->rightJoin('paysystems', 'paysystems.id = paysystems_config.paysystem_id and paysystems_config.parent_id = ' . $listItem->id)
                ->select([
                    'paysystems_config.id',
                    'paysystems_config.config',
                    'paysystems.title',
                    'paysystems.image',
                    'paysystems.currency',
                    'paysystems.class_name',
                    'paysystems.id as paysystem_id',
                ])
                ->orderBy(['paysystems.currency' => SORT_ASC])
                ->asArray()
            ,
            'pagination' => [
                'pageSize' => 50,
            ],
        ]),
        'columns'      => [
            'id',
            [
                'header'  => 'currency',
                'content' => function ($item) {
                    $i = \yii\helpers\ArrayHelper::getValue($item, 'currency', '');

                    return Html::tag('span', $i, ['class' => 'label label-default']);
                }
            ],
            [
                'header'  => 'Задана?',
                'content' => function ($item) {
                    $i = \yii\helpers\ArrayHelper::getValue($item, 'id', '');
                    if ($i == '') return Html::tag('span', 'Нет', ['class' => 'label label-danger']);

                    return Html::tag('span', 'Да', ['class' => 'label label-success']);
                }
            ],
            [
                'header'  => 'Картинка',
                'content' => function ($item) {
                    $i = \yii\helpers\ArrayHelper::getValue($item, 'image', '');
                    if ($i == '') return '';

                    return Html::img($i, [
                        'class' => "thumbnail",
                        'width' => 80,
                        'style' => 'margin-bottom: 0px;',
                    ]);
                }
            ],
            'title:text:Название',
            [
                'header'  => 'Конфиг',
                'content' => function ($item) {
                    return \yii\helpers\ArrayHelper::getValue($item, 'config');
                }
            ],
        ]
    ]) ?>
</div>
