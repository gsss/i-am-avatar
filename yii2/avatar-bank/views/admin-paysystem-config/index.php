<?php

/** $this \yii\web\View  */

use yii\helpers\Url;
use app\services\GsssHtml;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use app\services\GetArticle\Collection;

$this->title = 'Множество: Настройка платежных систем';


?>

<div class="container">
    <div class="col-lg-12">
        <h1 class="page-header"><?= Html::encode($this->title) ?></h1>
        <div class="btn-group" style="margin-bottom: 20px;" role="group" aria-label="...">
            <a href="<?= Url::to(['admin-paysystem-config/add']) ?>" class="btn btn-default">Добавить</a>
        </div>
        <?php \yii\widgets\Pjax::begin(); ?>
        <?php
        $this->registerJS(<<<JS
    $('.buttonDelete').click(function (e) {
        e.preventDefault();
        e.stopPropagation();
        if (confirm('Подтвердите удаление')) {
            var button = $(this);
            var id = $(this).data('id');
            ajaxJson({
                url: '/admin-paysystem-config/delete?id=' + id,
                success: function (ret) {
                    infoWindow('Успешно', function() {
                        button.parent().parent().remove();
                    });
                }
            });
        }
    });
    $('.buttonEdit').click(function (e) {
        e.preventDefault();
        e.stopPropagation();
        window.location = '/admin-paysystem-config/edit?id=' + $(this).data('id');
    });

    $('.rowTable').click(function() {
        window.location = '/admin-paysystem-config/item?id=' + $(this).data('id');
    });
JS
        );
        $sort = new \yii\data\Sort([
            'attributes' => [
                'id',
                'name',
                'created_at',
            ],
            'defaultOrder' => [
                'id' => SORT_ASC,
            ],
        ]);
        ?>
        <?= \yii\grid\GridView::widget([
            'dataProvider' => new \yii\data\ActiveDataProvider([
                'query'      => \common\models\PaySystemListItem::find(),
                'pagination' => [
                    'pageSize' => 20,
                ],
                'sort' => $sort,
            ]),
            'tableOptions' => [
                'class' => 'table table-striped table-hover',
            ],
            'rowOptions'   => function ($item) {
                $data = [
                    'data'  => ['id' => $item['id']],
                    'role'  => 'button',
                    'class' => 'rowTable'
                ];

                return $data;
            },
            'columns'      => [
                'id',
                'name',
                [
                    'header'  => 'Редактировать',
                    'content' => function ($item) {
                        return Html::button('Редактировать', [
                            'class' => 'btn btn-info btn-xs buttonEdit',
                            'data'  => [
                                'id' => $item['id'],
                            ]
                        ]);
                    }
                ],
                [
                    'header'  => 'Удалить',
                    'content' => function ($item) {
                        return Html::button('Удалить', [
                            'class' => 'btn btn-danger btn-xs buttonDelete',
                            'data'  => [
                                'id' => $item['id'],
                            ]
                        ]);
                    }
                ]
            ],
        ]) ?>
        <?php \yii\widgets\Pjax::end(); ?>
    </div>
</div>