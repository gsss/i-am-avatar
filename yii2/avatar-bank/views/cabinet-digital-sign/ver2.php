<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;

/** @var $this \yii\web\View */
/** @var $model \avatar\models\validate\CabinetDigitalSignVer2 */

$this->title = 'Цифровая подпись';

\avatar\assets\Notify::register($this);
\avatar\assets\Clipboard::register($this);

$this->registerJs(<<<JS

var clipboard = new Clipboard('.buttonCopy');
clipboard.on('success', function(e) {

    new Noty({
        timeout: 1000,
        theme: 'sunset',
        type: 'success',
        layout: 'bottomLeft',
        text: 'Скопировано'
    }).show();

});

JS
);
?>

<div class="container" style="padding-bottom: 70px;">
    <div class="col-lg-12">
        <h1 class="page-header"><?= $this->title ?></h1>
    </div>
    <div class="col-lg-8">

        <?php if (
                !\cs\Application::isEmpty(Yii::$app->user->identity->name_first) &&
                !\cs\Application::isEmpty(Yii::$app->user->identity->name_last) &&
                !\cs\Application::isEmpty(Yii::$app->user->identity->getAvatar())
        ) { ?>
            <?php $model = new \avatar\models\validate\CabinetDigitalSignVer2(); ?>
            <?php
            $f = new \cs\services\Url(Yii::$app->user->identity->getAvatar());
            if ($f->host == 'localhost') {
                $file = '';
            } else {
                $file = file_get_contents(Yii::$app->user->identity->getAvatar());
            }
            ?>
            <?php
            $data = [
                Yii::$app->user->identity->name_first,
                Yii::$app->user->identity->name_last,
                $file
            ];
            $hash = hash('sha256', join('', $data));
            $model->message = strtolower($hash);
            ?>
            <p>HASH:</p>
            <p class="alert alert-info" style="font-family: MONOSPACE;"><?= $model->message ?> <button class="btn btn-default btn-xs buttonCopy" data-clipboard-text="<?= $model->message ?>">Копировать</button></p>
            <?php $form = \iAvatar777\services\FormAjax\ActiveForm::begin([
            'model'   => $model,
            'success' => <<<JS
function (ret) {
    $('#modalInfo').on('hidden.bs.modal', function() {
        window.location = '/cabinet-digital-sign/index';
    }).modal();
}
JS

        ]) ?>
            <?= $form->field($model, 'address', ['inputOptions' => ['style' => 'font-family: MONOSPACE;']]) ?>
            <?= Html::activeHiddenInput($model, 'message') ?>
            <?= $form->field($model, 'sign', ['inputOptions' => ['style' => 'font-family: MONOSPACE;']]) ?>
            <hr>
            <?php \iAvatar777\services\FormAjax\ActiveForm::end(['label' => 'Проверить и сохранить']) ?>

            <hr>
            <p>Чтобы получить цифровую подпись вам надо</p>
            <ul>
                <li>скачать и установить <a href="https://www.docker.com/products/docker-desktop" target="_blank">https://www.docker.com/products/docker-desktop</a> </li>
                <li>запустить в консоли: <code>docker pull qualitylife/digital-key-type1:1.3</code></li>
                <li>запустить в консоли: <code>docker run --restart=always --name quality_life_sign -p 8080:80 -d qualitylife/digital-key-type1:1.3</code></li>
                <li>запустить в браузере: <code>http://localhost:8080</code></li>
            </ul>
        <?php } else { ?>
            <p class="alert alert-danger">В вашем профиле обязательно должны быть заполнены Имя Фамилия и Аватарка</p>
            <p><a href="/cabinet/profile" class="btn btn-default" >Перейти в профиль и заполнить</a></p>
        <?php }?>



    </div>
    <div class="col-lg-4">
        <?= $this->render('../cabinet/_menu') ?>
    </div>
</div>

<div class="modal fade" id="modalInfo" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Информация</h4>
            </div>
            <div class="modal-body">
                Успешно!
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
            </div>
        </div>
    </div>
</div>