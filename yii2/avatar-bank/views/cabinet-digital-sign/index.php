<?php
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this \yii\web\View */

$this->title = 'Цифровая подпись';

?>

<div class="container" style="padding-bottom: 70px;">
    <div class="col-lg-12">
        <h1 class="page-header"><?= $this->title ?></h1>
    </div>
    <div class="col-lg-8">

        <?php $row = \common\models\UserDigitalSign::findOne(['user_id' => Yii::$app->user->id]);  ?>
        <?php if (!is_null($row)) {  ?>
            <p>Адрес: <code><?= $row->address ?></code></p>
            <?php
            $this->registerJs(<<<JS
$('.buttonRemove').click(function(e) {
    ajaxJson({
        url: '/cabinet-digital-sign/reset',
        success: function(ret) {
            window.location.reload();
        }
    });
});
JS
);
            ?>
            <p><button class="btn btn-default buttonRemove">Сбросить</button></p>

        <?php } else { ?>

            <p>Чтобы получить числовую (цифровую) подпись нажмите кнопку ниже:</p>
            <p>
                Для хранения ключа электронной подписи на Платформе
                <a href="step2" class="btn btn-default">Получить подпись</a>
            </p>
            <p>
                Для хранения ключа электронной подписи на собственном носителе
                <a href="ver2" class="btn btn-default">Получить подпись</a>
            </p>

        <?php } ?>
    </div>
    <div class="col-lg-4">
        <?= $this->render('../cabinet/_menu') ?>
    </div>
</div>