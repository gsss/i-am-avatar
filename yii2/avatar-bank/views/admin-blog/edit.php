<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;
use app\models\UnionCategory;
use yii\db\Query;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \avatar\models\forms\BlogItem */

$this->title = $model->name;

?>
<div class="container">
    <div class="col-lg-12">
        <h1 class="page-header"><?= Html::encode($this->title) ?></h1>

        <?php if (Yii::$app->session->hasFlash('form')) { ?>
            <div class="alert alert-success">
                Успешно обновлено.
            </div>

            <?php $id = Yii::$app->session->getFlash('form'); ?>
            <p>
                <a href="<?= \yii\helpers\Url::to(['admin-blog/edit', 'id' => $id]) ?>" class="btn btn-success">Вернуться к редактированию</a>
                <a href="<?= \yii\helpers\Url::to(['admin-blog/index']) ?>" class="btn btn-default">Блог в админке</a>
                <a href="<?= \yii\helpers\Url::to(['blog/index']) ?>" class="btn btn-default">Блог на сайте</a>
                <a href="<?= \yii\helpers\Url::to(['blog/item', 'id' => $id]) ?>" class="btn btn-success">Посмотреть на сайте</a>
            </p>

        <?php  } else { ?>

            <div class="row">
                <div class="col-lg-5">
                    <?php $form = ActiveForm::begin([
                        'id'      => 'contact-form',
                        'options' => ['enctype' => 'multipart/form-data']
                    ]); ?>
                    <?= $model->field($form, 'name') ?>
                    <?= $model->field($form, 'link') ?>
                    <?= $model->field($form, 'description')->textarea(['rows' => 5]) ?>
                    <?= $model->field($form, 'content') ?>
                    <?= $model->field($form, 'image') ?>

                    <hr>
                    <div class="form-group">
                        <?= Html::submitButton('Обновить', [
                            'class' => 'btn btn-default',
                            'name'  => 'contact-button',
                            'style' => 'width:100%',
                        ]) ?>
                    </div>
                    <?php ActiveForm::end(); ?>
                </div>
            </div>

        <?php } ?>
    </div>
</div>
