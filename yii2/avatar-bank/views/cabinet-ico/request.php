<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;
use app\models\UnionCategory;

/* @var $this yii\web\View */
/* @var $request \common\models\investment\IcoRequest */

$this->title = 'Заявка #' . $request->id;

?>
<?php $provider = new \avatar\modules\ETH\ServiceEtherScan(); ?>
<div class="container">
    <div class="col-lg-12">
        <h1 class="page-header"><?= Html::encode($this->title) ?></h1>

        <h3 class="page-header">Заявка</h3>
        <?= \yii\widgets\DetailView::widget([
            'model'      => $request,
            'options'    => ['class' => 'table table-striped detail-view'],
            'attributes' => [
                [
                    'label'          => 'Идентификатор заявки',
                    'value'          => $request->id,
                    'captionOptions' => [
                        'style' => 'width:30%',
                    ],
                ],
                'created_at:datetime:Создана',
                [
                    'label'  => 'Оплачена?',
                    'format' => 'html',
                    'value'  => $request->is_paid ? Html::tag('span', 'Да', ['class' => 'label label-success']) : Html::tag('span', 'Нет', ['class' => 'label label-default']),
                ],
                'tokens:text:Кол-во заказанных токнов',
                [
                    'label'  => 'Адрес получения токенов',
                    'format' => 'html',
                    'value'  => Html::a(
                        Html::tag('code', $request->address),
                        $provider->getUrl('/address/' . $request->address),
                        ['target' => '_blank']
                    ),
                ],
            ],
        ]) ?>
        <?php

        $this->registerJs(<<<JS
$('.buttonSet').click(function(e) {
    $('#modalTransaction').modal();
});
JS
);

        ?>
        <h3 class="page-header">Счет на оплату</h3>
        <?php
        $billing = $request->getBilling();
        $payment = $request->getPaymentI();
        $txid = $payment->getTransaction();
        $address = $payment->getAddress();
        ?>
        <?= \yii\widgets\DetailView::widget([
            'model'      => $billing,
            'options'    => ['class' => 'table table-striped detail-view'],
            'attributes' => [
                [
                    'label'          => 'Идентификатор счета',
                    'value'          => $billing->id,
                    'captionOptions' => [
                        'style' => 'width:30%',
                    ],
                ],
                [
                    'label'  => 'Платежная система',
                    'format' => ['image', ['width' => 50, 'class' => 'img-circle', 'style' => 'border: 1px solid #888;']],
                    'value'  => $billing->getPaySystem()->image,
                ],
                [
                    'label'  => 'Адрес для оплаты',
                    'format' => 'html',
                    'value'  => Html::tag('code', $address),
                ],
                [
                    'label'  => 'Кол-во монет для оплаты',
                    'value'  => Yii::$app->formatter->asDecimal($billing->sum_after, $billing->getPaySystem()->getCurrencyObject()->decimals),
                ],
                [
                    'label'  => 'Транзакция',
                    'format' => 'html',
                    'value'  => $txid ? Html::tag('code', $txid) : Html::a('Указать', 'javascript:void(0)', ['class' => 'btn btn-success buttonSet']),
                ],
            ],
        ]) ?>
        <h3 class="page-header">Проект</h3>
        <?php $project = $request->getProject(); ?>
        <?= \yii\widgets\DetailView::widget([
            'model'      => $project,
            'options'    => ['class' => 'table table-striped detail-view'],
            'attributes' => [
                [
                    'label'          => 'Наименование проекта',
                    'value'          => $project->name,
                    'captionOptions' => [
                        'style' => 'width:30%',
                    ],
                ],
                [
                    'label'  => 'Ссылка',
                    'format' => ['url', ['target' => '_blank']],
                    'value'  => Url::to(['projects/item', 'id' => $project->id], true),
                ],
            ],
        ]) ?>
        <h3 class="page-header">Покупаемый токен</h3>
        <?php $currency = $project->getIco()->getCurrency(); ?>
        <?= \yii\widgets\DetailView::widget([
            'model'      => $currency,
            'options'    => ['class' => 'table table-striped detail-view'],
            'attributes' => [
                [
                    'label'          => 'Монета',
                    'format'         => ['image', ['width' => 50, 'class' => 'img-circle', 'style' => 'border: 1px solid #888;']],
                    'value'          => $currency->image,
                    'captionOptions' => [
                        'style' => 'width:30%',
                    ],
                ],
                [
                    'label' => 'Название',
                    'value' => $currency->title,
                ],
                [
                    'label' => 'Код',
                    'value' => $currency->code,
                ],
                [
                    'label'  => 'Адрес получения токенов',
                    'format' => 'html',
                    'value'  => Html::a(
                        Html::tag('code', $currency->getToken()->address),
                        $provider->getUrl('/token/'.$currency->getToken()->address),
                        ['target' => '_blank']
                    ),
                ],

            ],
        ]) ?>

    </div>
</div>
<div class="modal fade" tabindex="-1" role="dialog" id="modalTransaction">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Транзакция об оплате</h4>
            </div>
            <div class="modal-body">
                <p>Транзакция:</p>

                <p><input id="field-txid" class="form-control" type="text"></p>
            </div>
            <div class="modal-footer">
                <?php
                $this->registerJs(<<<JS
$('.buttonModalSet').click(function(e) {
    ajaxJson({
        url: '/cabinet-ico/confirm',
        data: {
            txid: $('#field-txid').val(),
            id: {$request->id}
        },
        success: function(ret) {
            window.location.reload();
        }
    });
});

JS
                );

                ?>
                <button type="button" class="btn btn-primary buttonModalSet" style="width: 100%;">Подтвердить</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div><!-- /.modal -->



