<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;
use app\models\UnionCategory;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model cs\base\BaseForm */

$this->title = 'Заявки на покупку токенов';

$sort = new \yii\data\Sort([
    'attributes'   => [
        'created_at' => [
            'asc'     => ['ico_request.created_at' => SORT_ASC],
            'desc'    => ['ico_request.created_at' => SORT_DESC],
            'default' => SORT_DESC,
            'label'   => 'Создан',
        ],
    ],
    'defaultOrder' => [
        'created_at' => SORT_DESC
    ]
]);

$this->registerJs(<<<JS
$('.rowTable').click(function() {
    window.location = '/cabinet-ico/request' + '?' + 'id' + '=' + $(this).data('id');
});

JS
);
?>
<style>
    .textDecorated {
        text-decoration-line: underline;
        text-decoration-style: dotted;
    }
</style>
<div class="container">
    <div class="col-lg-12">
        <h1 class="page-header"><?= Html::encode($this->title) ?></h1>
        <?= \yii\grid\GridView::widget([
            'dataProvider' => new \yii\data\ActiveDataProvider([
                'query' => \common\models\investment\IcoRequest::find()
                    ->select([
                        'ico_request.id',
                        'ico_request.created_at',
                        'ico_request.is_paid',
                        'ico_request.tokens',
                        'ico_request.address',

                        'ico_request_success.txid',
                        'ico_request_success.created_at as time_confirm',

                        'projects.name',
                        'projects_ico.currency_id',

                        'billing.sum_after as billing_sum_after',

                        'currency.id as currency_id',
                        'currency.image as currency_image',
                        'currency.code as currency_code',
                        'currency.title as currency_title',

                        'paysystems.id as paysystems_id',
                        'paysystems.title as paysystems_title',
                        'paysystems.image as paysystems_image',
                    ])
                    ->innerJoin('billing', 'billing.id = ico_request.billing_id')
                    ->innerJoin('paysystems', 'paysystems.id = billing.source_id')
                    ->innerJoin('projects', 'projects.id = ico_request.ico_id')
                    ->innerJoin('projects_ico', 'projects_ico.id = projects.id')
                    ->innerJoin('currency', 'currency.id = projects_ico.currency_id')
                    ->leftJoin('ico_request_success', 'ico_request_success.id = ico_request.id')
                    ->where([
                        'ico_request.user_id' => Yii::$app->user->id,
                    ])
                    ->asArray()
                ,
                'sort' => $sort,
            ]),
            'tableOptions' => [
                'class' => 'table table-hover table-striped',
            ],
            'rowOptions'   => function ($item) {
                $data = [
                    'data'  => ['id' => $item['id']],
                    'role'  => 'button',
                    'class' => 'rowTable'
                ];
                return $data;
            },
            'columns' => [
                [
                    'attribute' => 'id',
                ],
                [
                    'attribute' => 'name',
                ],
                [
                    'header'    => 'Создано',
                    'attribute' => 'created_at',
                    'content'   => function ($item) {
                        $v = \yii\helpers\ArrayHelper::getValue($item, 'created_at', 0);
                        if ($v == 0) return '';

                        return Html::tag('span', \cs\services\DatePeriod::back($v, ['isShort' => true]), [
                            'data'  => [
                                'toggle'    => 'tooltip',
                                'placement' => 'bottom',
                            ],
                            'title' => Yii::$app->formatter->asDatetime($v),
                            'class' => 'textDecorated'
                        ]);
                    },
                ],
                [
                    'header'  => 'Валюта',
                    'content' => function ($item) {
                        $v = \yii\helpers\ArrayHelper::getValue($item, 'paysystems_image', '');

                        return Html::img($v, [
                            'width' => 50,
                            'class' => 'img-circle',
                            'title' => \yii\helpers\ArrayHelper::getValue($item, 'paysystems_title', ''),
                            'data'  => [
                                'toggle'    => 'tooltip',
                                'placement' => 'bottom',
                                'id'        => \yii\helpers\ArrayHelper::getValue($item, 'paysystems_id', '')
                            ],
                        ]);
                    },
                ],
                [
                    'header'  => 'Выставлено к оплате',
                    'headerOptions'  => [
                        'style' => 'text-align: right;',
                    ],
                    'contentOptions' => [
                        'style' => 'text-align: right;',
                    ],
                    'content' => function ($item) {
                        $v = \yii\helpers\ArrayHelper::getValue($item, 'billing_sum_after', '');

                        return Yii::$app->formatter->asDecimal($v, 8);
                    },
                ],
                [
                    'attribute' => '',
                    'content' => function ($item) {
                        return Html::tag('span', Html::tag('i',null, ['class' => 'glyphicon glyphicon-chevron-right']), [
                            'class' => 'label label-success',
                        ]);
                    },
                ],
                [
                    'header'  => 'tokens',

                    'content' => function ($item) {
                        $v = \yii\helpers\ArrayHelper::getValue($item, 'currency_image', '');

                        return Html::img($v, [
                            'width' => 50,
                            'class' => 'img-circle',
                            'title' => \yii\helpers\ArrayHelper::getValue($item, 'currency_title', '') . ' ('.  \yii\helpers\ArrayHelper::getValue($item, 'currency_code', '') . ')',
                            'data' => [
                                'toggle'    => 'tooltip',
                                'placement' => 'bottom',
                                'id' => \yii\helpers\ArrayHelper::getValue($item, 'currency_id', '')
                            ],
                        ]);
                    },
                ],
                [
                    'header'         => '',
                    'attribute'      => 'tokens',
                    'format'         => ['decimal',2],
                    'headerOptions'  => [
                        'style' => 'text-align: right;',
                    ],
                    'contentOptions' => [
                        'style' => 'text-align: right;',
                    ],
                ],
                [
                    'header'  => 'Оплачено?',
                    'content' => function ($item) {
                        $v = \yii\helpers\ArrayHelper::getValue($item, 'is_paid', 0);
                        if ($v == 0) return Html::tag('span', 'Нет', ['class' => 'label label-default']);
                        return Html::tag('span', 'Да', ['class' => 'label label-success']);
                    },
                ],
                [
                    'header'  => 'address',
                    'content' => function ($item) {
                        $v = \yii\helpers\ArrayHelper::getValue($item, 'address', '');
                        if ($v == '') return '';

                        return Html::tag('code', substr($v, 0, 10) . ' ...');
                    },
                ],
                [
                    'header'  => 'txid',
                    'content' => function ($item) {
                        $v = \yii\helpers\ArrayHelper::getValue($item, 'txid', '');
                        if ($v == '') return '';

                        return Html::tag('code', substr($v, 0, 10) . ' ...');
                    },
                ],
                [
                    'header'  => 'Подтверждено',
                    'content' => function ($item) {
                        $v = \yii\helpers\ArrayHelper::getValue($item, 'time_confirm', 0);
                        if ($v == 0) return '';

                        return Html::tag('span', \cs\services\DatePeriod::back($v, ['isShort' => true]), [
                            'data'  => [
                                'toggle'    => 'tooltip',
                                'placement' => 'bottom',
                            ],
                            'title' => Yii::$app->formatter->asDatetime($v),
                            'class' => 'textDecorated'
                        ]);
                    },
                ],
            ],
        ]) ?>
    </div>
</div>



