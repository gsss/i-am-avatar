<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;
use app\models\UnionCategory;
use yii\db\Query;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \common\models\VoteAnswer */
/* @var $list \common\models\VoteList */
/** @var $school \common\models\school\School */

$this->title = 'Добавить ответ';

?>
<div class="container">
        <h1 class="page-header"><?= Html::encode($this->title) ?></h1>

        <?php if (!is_null($id = Yii::$app->session->getFlash('form'))) : ?>

            <div class="alert alert-success">
                Успешно добавлено.
            </div>

            <a class="btn btn-info" href="/cabinet-vote-answer/index?id=<?= $list->id ?>">Все ответы</a>

        <?php else: ?>


            <div class="row">
                <div class="col-lg-5">
                    <?php $form = ActiveForm::begin([
                        'options' => ['enctype' => 'multipart/form-data']
                    ]); ?>
                    <?= $form->field($model, 'name') ?>

                    <hr>
                    <div class="form-group">
                        <?= Html::submitButton('Добавить', [
                            'class' => 'btn btn-default',
                            'name'  => 'contact-button',
                            'style' => 'width:100%',
                        ]) ?>
                    </div>
                    <?php ActiveForm::end(); ?>
                </div>
            </div>

        <?php endif; ?>
</div>
