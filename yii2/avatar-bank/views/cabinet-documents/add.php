<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;
use app\models\UnionCategory;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \avatar\models\forms\UserDocument */

$this->title = 'Добавление документа';

/**
 * Алгоритм такой:
 * Если указывается ссылка то скачивается документ и расчитывается хеш
 * Если указан файл то он загружается, берется ссылка и расчитывается хеш
 * Блок с файлом показывается в зависимости от условий
 * Если у тебя кол-во возможных загрузить файлы (поле user_files_upload.count) больше ноля
 * В начале после регистрации дается только 10 файлов загрузить, Далее по цене 100 файлов 1 AVR.
 */

$userFiles = \common\models\UserFilesUpload::findOne(Yii::$app->user->id);
if (is_null($userFiles)) {
    $userFiles = new \common\models\UserFilesUpload([
        'id'             => Yii::$app->user->id,
        'upload_count'   => 0,
        'register_count' => 0,
    ]);
    $userFiles->save();
}

$this->registerJs(<<<JS
JS
);
?>
<div class="container">
    <div class="col-lg-12">
        <h1 class="page-header"><?= Html::encode($this->title) ?></h1>

    </div>
    <div class="col-lg-8">

        <div class="collapse in" id="step1">
            <?php $form = \iAvatar777\services\FormAjax\ActiveForm::begin([
                'model' => $model,
                'success' => <<<JS
function (ret) {
  if (ret.is_show_secret) {
      $('#userdocumentsecret-id').val(ret.document.id);
      $('#step1').on('hidden.bs.collapse', function(ret) {
          $('#step2').collapse();
      }).collapse('hide');  
  } else {
      ajaxJson({
        url: '/cabinet-documents/sign',
        data: {id:ret.document.id},
        success: function (ret) {
            $('#modalInfo').on('hidden.bs.modal', function() {
                window.location = '/cabinet-documents/index';
            }).modal();     
        }
      });
  }
  
}
JS

            ]); ?>
            <?= $form->field($model, 'name') ?>
            <?= $form->field($model, 'data')->textarea(['rows' => 5]) ?>
            <?= $form->field($model, 'file') ?>

            <hr class="featurette-divider">

            <?php \iAvatar777\services\FormAjax\ActiveForm::end(); ?>

        </div>
        <div class="collapse" id="step2">
            <p>
                <?php
                $this->registerJs(<<<JS
$('.buttonSign').click(function(e) {
      $('#step2').on('hidden.bs.collapse', function(ret) {
          $('#step3').collapse();
      }).collapse('hide');  
});
$('.buttonCancel').click(function(e) {
      window.location = '/cabinet-documents/index';
});

JS
)
                ?>
                <button class="btn btn-success buttonSign">Подписать</button>
                <button class="btn btn-default buttonCancel">Пропустить</button>
            </p>
        </div>
        <div class="collapse" id="step3">
            <?php $model = new \avatar\models\forms\UserDocumentSecret(); ?>
            <?php $form = \iAvatar777\services\FormAjax\ActiveForm::begin([
                'model' => $model,
                'formUrl' => '/cabinet-documents/secret',
                'success' => <<<JS
function (ret) {
    $('#modalInfo').on('hidden.bs.modal', function() {
        window.location = '/cabinet-documents/index';
    }).modal();  
}
JS

            ]); ?>
            <?= \yii\helpers\Html::activeHiddenInput($model, 'id') ?>
            <?= $form->field($model, 'secret')->passwordInput() ?>

            <hr class="featurette-divider">

            <?php \iAvatar777\services\FormAjax\ActiveForm::end(); ?>
        </div>
    </div>
    <div class="col-lg-4">
        <?= $this->render('../cabinet/_menu') ?>
    </div>
</div>


<div class="modal fade" id="modalInfo" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Информация</h4>
            </div>
            <div class="modal-body">
                Успешно!
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
            </div>
        </div>
    </div>
</div>
