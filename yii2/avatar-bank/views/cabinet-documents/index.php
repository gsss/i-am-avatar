<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;
use app\models\UnionCategory;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model cs\base\BaseForm */

$this->title = 'Портфель документов';


?>
<style>
    .textDecorated {
        text-decoration-line: underline;
        text-decoration-style: dotted;
    }
</style>
<style>
    #icon {
        width: 64px;
        height: 64px;
        background-size: cover;
        background-repeat: no-repeat;
        border-radius: 50%;
        box-shadow: inset rgba(255, 255, 255, 0.6) 0 2px 2px, inset rgba(0, 0, 0, 0.3) 0 -2px 6px;
    }
</style>
<div class="container">
    <div class="col-lg-12">
        <h1 class="page-header"><?= Html::encode($this->title) ?></h1>

    </div>
    <div class="col-lg-8">
        <a href="<?= Url::to(['cabinet-documents/add']) ?>" class="btn btn-default">Добавить</a>
        <hr>
        <?php \yii\widgets\Pjax::begin() ?>
        <?php
        $this->registerJs(<<<JS
$('[data-toggle="tooltip"]').tooltip();
$('.rowTable').click(function() {
    window.location = '/cabinet-documents/view' + '?' + 'id' + '=' + $(this).data('id');
});
$('.buttonRead').click(function(e) {
    e.preventDefault();
    e.stopPropagation();
    window.location = '/documents/read' + '?' + 'id' + '=' + $(this).data('id');
});
JS
        );
        ?>
        <?php
        $sort = new \yii\data\Sort([
            'defaultOrder' => [
                'created_at' => SORT_DESC,
            ],
        ]);
        $model = new \avatar\models\search\Documents();
        $provider = $model->search($sort, Yii::$app->request->get(), ['user_documents.user_id' => Yii::$app->user->id]);
        ?>
        <?= \yii\grid\GridView::widget([
            'filterModel'  => $model,
            'dataProvider' => $provider,
            'tableOptions' => [
                'class' => 'table table-hover table-striped',
            ],
            'rowOptions'   => function ($item) {
                return [
                    'data'  => ['id' => $item['id']],
                    'role'  => 'button',
                    'class' => 'rowTable',
                ];
            },
            'columns'      => [
                'id',
                [
                    'header'  => 'Файл',
                    'content' => function ($item) {
                        $v = \yii\helpers\ArrayHelper::getValue($item, 'file', '');
                        if ($v == '') return '';
                        $file = pathinfo($v);

                        return Html::img('/images/controller/cabinet/documents/file.png', ['width' => 50]) . '.' . strtoupper($file['extension']);
                    },
                ],
                'name:text:Наименование',
                [
                    'header'    => 'hash',
                    'attribute' => 'hash',
                    'content'   => function ($item) {
                        return
                            Html::tag(
                                'span',
                                substr(\yii\helpers\ArrayHelper::getValue($item, 'hash'), 0, 6) . '...',
                                [
                                    'class' => 'js-buttonTransactionInfo textDecorated',
                                    'style' => 'font-family: "Courier New", Courier, monospace;',
                                    'role'  => 'button',

                                    'title' => 'Подробнее',
                                    'data'  => [
                                        'toggle'    => 'tooltip',
                                        'placement' => 'bottom',
                                    ],
                                ]
                            );
                    },
                ],
                [
                    'header'    => 'Создано',
                    'attribute' => 'created_at',
                    'content'   => function ($item) {
                        $v = \yii\helpers\ArrayHelper::getValue($item, 'created_at', 0);
                        if ($v == 0) return '';

                        return Html::tag('span', \cs\services\DatePeriod::back($v, ['isShort' => true]), [
                            'data'  => [
                                'toggle'    => 'tooltip',
                                'placement' => 'bottom',
                            ],
                            'title' => Yii::$app->formatter->asDatetime($v),
                            'class' => 'textDecorated'
                        ]);
                    },
                ],
                [
                    'header'  => 'Подписей',
                    'content' => function ($item) {
                        $v = \yii\helpers\ArrayHelper::getValue($item, 'counter', 0);
                        if ($v == 0) return Html::tag('span', $v, ['class' => 'label label-warning']);


                        return Html::tag('span', $v, ['class' => 'label label-success']);
                    },
                ],
            ],
        ]) ?>
        <?php \yii\widgets\Pjax::end() ?>


    </div>
    <div class="col-lg-4">
        <?= $this->render('../cabinet/_menu') ?>
    </div>
</div>



