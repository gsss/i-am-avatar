<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;
use app\models\UnionCategory;

/* @var $this yii\web\View */
/* @var $document \common\models\UserDocument */

$this->title = $document->id;

$v = \yii\helpers\ArrayHelper::getValue($document, 'file', 0);
if (\cs\Application::isEmpty($v)) {
    $v = \yii\helpers\ArrayHelper::getValue($document, 'link', 0);
    $url = new \cs\services\Url($v);
    $file = pathinfo($url->path);
} else {
    $file = pathinfo($v);
}

$html = $document->link;
?>
<html>
<head>
    <title>Документ</title>
</head>
<body>
<p>www.I-AM-AVATAR.com</p>
<hr>
<h4 style="text-align: center;font-weight: normal;">
    <span style="font-size: 110%;">Патентное бюро<br>
    <span style="font-size: 110%;">Сертификат на авторское право<br>

    <span style="font-size: 110%;">Документ #<?= $document->id ?></span>
        </span>
        </span>
        </span>

</h4>
<table class="table table-hover table-striped">
    <tr>
        <td style="text-align: right;">ID</td>
        <td><?= $document->id ?></td>
    </tr>
    <tr>
        <td style="text-align: right;">Наименование</td>
        <td><?= $document->name ?></td>
    </tr>
    <tr>
        <td style="text-align: right;">Контракт</td>
        <td><code>0x52eD3C202c4652F952a1561Ac0c030f1eD9460fF</code></td>
    </tr>
    <tr>
        <td style="text-align: right;">Hash</td>
        <td><?= Html::tag('code', $document->hash) ?></td>
    </tr>
    <tr>
        <td style="text-align: right;vertical-align: top;">TXID</td>
        <td>
            <p>
                <?= Html::tag('code', Html::a($document->txid,'http://etherscan.io/tx/' . $document->txid, ['target' => '_blank'])) ?>
            </p>
            <?php
            $content = (new \Endroid\QrCode\QrCode())
                ->setText('http://etherscan.io/tx/' . $document->txid)
                ->setSize(200)
                ->setPadding(0)
                ->setErrorCorrection('high')
                ->setForegroundColor(['r' => 0, 'g' => 0, 'b' => 0, 'a' => 0])
                ->setBackgroundColor(['r' => 255, 'g' => 255, 'b' => 255, 'a' => 0])
                ->setLabelFontSize(16)
                ->setImageType(\Endroid\QrCode\QrCode::IMAGE_TYPE_PNG)
                ->get('png');
            $src = 'data:image/png;base64,' . base64_encode($content);

            ?>
            <?= Html::tag('p', Html::img($src, ['style' => 'margin-top:10px;'])) ?>
        </td>
    </tr>
    <tr>
        <td style="text-align: right;">Data</td>
        <td><?= Html::tag('pre', $document->data) ?></td>
    </tr>
    <tr>
        <td style="text-align: right;">Добавлен</td>
        <td><?= Yii::$app->formatter->asDatetime($document->created_at, 'php:c') ?></td>
    </tr>
    <tr>
        <td style="text-align: right;">Файл</td>
        <?php
        if ($document->link) {
            $link = $document->link;
        } else {
            $link = Url::to(['cabinet/documents-download', 'id' => $document->id]);
        }
        ?>
        <td><a href="<?= $link ?>"><?= $html ?></td>
    </tr>
</table>




<br class="page-break-after: always;">

<h3>Подписи</h3>
<table>
<?php /** @var $signature \common\models\UserDocumentSignature */ ?>
<?php foreach (\common\models\UserDocumentSignature::find()->where(['document_id' => $document->id])->all() as $signature) { ?>
    <tr>
        <td>

        </td>
        <td>
            <img src="<?= Url::to('/images/controller/cabinet/documents-view/blockchain-blog-620x347.png', true) ?>" width="100" style="border-radius: 10px;"/>
        </td>
    </tr>
    <tr>
        <td>
            Address
        </td>
        <td>
            <code><?= $signature->member ?></code>
        </td>
    </tr>
    <tr>
        <td>
            TXID
        </td>
        <td>
            <code><?= $signature->txid ?></code>
        </td>
    </tr>
    <tr>
        <td>
            Время
        </td>
        <td>
            <code><?= Yii::$app->formatter->asDatetime($signature->created_at, 'php:c') ?></code>
        </td>
    </tr>
    <tr>
        <td>

        </td>
        <td>

        </td>
    </tr>
<?php } ?>
</table>



<br class="page-break-after: always;">

</body>
</html>



