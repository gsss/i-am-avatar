<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;
use app\models\UnionCategory;

/* @var $this yii\web\View */
/* @var $model \avatar\models\forms\UserMap */

$this->title = 'Расположение на карте';


?>
<div class="container">
    <div class="col-lg-12">
        <h1 class="page-header"><?= Html::encode($this->title) ?></h1>
    </div>
    <div class="col-lg-8">

        <?php if (Yii::$app->session->hasFlash('form')): ?>

            <div class="alert alert-success">
                <?= \Yii::t('c.FMg0mLqFR9', 'Успешно обновлено') ?>.
            </div>

        <?php else: ?>

            <?php $form = ActiveForm::begin([
                'id'      => 'contact-form',
                'options' => ['enctype' => 'multipart/form-data'],
            ]); ?>
            <?= $form->field($model, 'place')->widget('\common\widgets\PlaceMapYandex\PlaceMap')->label('Место') ?>
            <?= $form->field($model, 'type_id')->dropDownList([
                1 => 'Город',
                2 => 'Район',
                3 => 'Дом',
            ])->label('Точность указания места') ?>


            <hr class="featurette-divider">
            <div class="form-group">
                <?= Html::submitButton(Yii::t('c.FMg0mLqFR9', 'Обновить'), [
                    'class' => 'btn btn-default',
                    'name'  => 'contact-button',
                    'style' => 'width: 100%;',
                ]) ?>
            </div>
            <?php ActiveForm::end(); ?>

        <?php endif; ?>

    </div>
    <div class="col-lg-4">
        <?= $this->render('../cabinet/_menu') ?>
    </div>
</div>



