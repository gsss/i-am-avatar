<?php

/** @var $this \yii\web\View  */

/** @var $item \avatar\models\UserEnter */

use yii\helpers\Url;
use app\services\GsssHtml;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use app\services\GetArticle\Collection;

$this->title = 'Заявление #' . $item->id;
$school = \common\models\school\School::findOne($item->school_id);
\avatar\assets\Notify::register($this);

?>

<div class="container">

    <?php \avatar\widgets\SchoolMenu::begin(['school' => $school]) ?>
    <h1 class="page-header"><?= Html::encode($this->title) ?></h1>

    <div class="row">
        <div class="col-lg-6">
            <h2 class="page-header">Человек</h2>
            <?= \yii\widgets\DetailView::widget([
                'model'      => $item,
                'attributes' => [
                    [
                        'label'          => 'ID',
                        'value'          => $item->id,
                        'captionOptions' => ['style' => 'width: 40%'],
                    ],
                    [
                        'label' => 'Имя',
                        'value' => $item->name_first,
                    ],
                    [
                        'label' => 'Фамилия',
                        'value' => $item->name_last,
                    ],
                    [
                        'label' => 'Отчество',
                        'value' => $item->name_middle,
                    ],
                    [
                        'label' => 'Телефон',
                        'value' => $item->phone,
                    ],
                    [
                        'label' => 'Почтовый адрес',
                        'value' => $item->address_pochta,
                    ],
                ],
            ]) ?>
            <div class="row">
                <?php if (!\cs\Application::isEmpty($item->signature1)) { ?>
                    <div class="col-lg-2">
                        <?php $user = \common\models\UserAvatar::findOne($item->user1_id) ?>
                        <img src="<?= $user->getAvatar() ?>" class="img-circle" width="100%" data-toggle="tooltip" title="<?= $user->getName2() ?>"><br>
                        <img src="https://cloud1.cloud999.ru/upload/cloud/15898/42366_co3C4jVul6.jpg" class="img-circle" width="50" data-toggle="tooltip" title="Подписано ЭЦП" style="margin-top: 30px;">
                    </div>
                <?php } ?>
                <?php if (!\cs\Application::isEmpty($item->signature2)) { ?>
                    <div class="col-lg-2">
                        <?php $user = \common\models\UserAvatar::findOne($item->user2_id) ?>
                        <img src="<?= $user->getAvatar() ?>" class="img-circle" width="100%" data-toggle="tooltip" title="<?= $user->getName2() ?>"><br>
                        <img src="https://cloud1.cloud999.ru/upload/cloud/15898/42366_co3C4jVul6.jpg" class="img-circle" width="50" data-toggle="tooltip" title="Подписано ЭЦП" style="margin-top: 30px;">
                    </div>
                <?php } ?>
            </div>
        </div>
        <div class="col-lg-6">
            <h2 class="page-header">Паспорт</h2>
            <?= \yii\widgets\DetailView::widget([
                'model'      => $item,
                'attributes' => [
                    [
                        'label'          => 'Серия',
                        'value'          => $item->passport_ser,
                        'captionOptions' => ['style' => 'width: 40%'],
                    ],
                    [
                        'label' => 'Номер',
                        'value' => $item->passport_number,
                    ],
                    [
                        'label' => 'Выдан кем',
                        'value' => $item->passport_vidan_kem,
                    ],
                    [
                        'label'  => 'Выдан когда',
                        'format' => 'date',
                        'value'  => $item->passport_vidan_date,
                    ],
                    [
                        'label' => 'Выдан подразделение',
                        'value' => $item->passport_vidan_num,
                    ],
                    [
                        'label'  => 'Родился когда',
                        'format' => 'date',
                        'value'  => $item->passport_born_date,
                    ],
                    [
                        'label' => 'Родился где',
                        'value' => $item->passport_born_place,
                    ],
                ],
            ]) ?>
            <div class="row">
                <div class="col-lg-3">
                    <a href="<?= $item->passport_scan1 ?>" target="_blank">
                        <img src="<?= \iAvatar777\widgets\FileUpload7\FileUpload::getFile($item->passport_scan1, 'crop') ?>"
                             style="width: 100%;">
                    </a>
                </div>
                <div class="col-lg-3">
                    <a href="<?= $item->passport_scan2 ?>" target="_blank">
                        <img src="<?= \iAvatar777\widgets\FileUpload7\FileUpload::getFile($item->passport_scan2, 'crop') ?>"
                             style="width: 100%;">
                    </a>
                </div>
            </div>
        </div>
    </div>

    <hr>
    <?php
    \avatar\assets\Notify::register($this);
    $uDigSig = \common\models\UserDigitalSign::findOne(['user_id' => Yii::$app->user->id]);
    if ($uDigSig->hasPK()) {
        $this->registerJs(<<<JS
$('.buttonSign').click(function(e) {
    if (confirm('Подтвердите подпись')) {
        ajaxJson({
            url: '/cabinet-school-koop-requests/sign-ajax',
            data: {id: {$item->id}},
            success: function(ret) {
                new Noty({
                    timeout: 1000,
                    theme: 'sunset',
                    type: 'success',
                    layout: 'bottomLeft',
                    text: ret.sign
                }).show();
                
                $('#modalInfo').on('hidden.bs.modal', function(e) {
                    window.location = '/cabinet-school-koop-requests/index?id=' + {$school->id};
                }).modal();
            }
        });
    }
});
JS
        );

    } else {
        $this->registerJs(<<<JS
$('.buttonSign').click(function(e) {
    $('#modalForm').modal();
});
JS
        );

    }
    ?>
    <p>
        <?php if ($item->status == \common\models\UserEnter::STATUS_AFTER_REGISTRATION) { ?>
            <?php if ($item->user2_id != Yii::$app->user->id) { ?>
                <button class="btn btn-success buttonSign">Верифицировать</button>
            <?php } ?>
        <?php } ?>
    </p>

    <?php \avatar\widgets\SchoolMenu::end() ?>
</div>

<div class="modal fade" id="modalInfo" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="myModalLabel">Подпись информации</h4>
            </div>
            <div class="modal-body">
                Успешно!
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modalForm" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="myModalLabel">Подпись информации</h4>
            </div>
            <div class="modal-body">
                <?php $model = new \avatar\models\validate\CabinetSchoolKoopSign2(['id' => $item->id]); ?>
                <?php $form = \iAvatar777\services\FormAjax\ActiveForm::begin([
                    'model'        => $model,
                    'formUrl'      => '/cabinet-school-koop-requests/sign2-ajax',
                    'success' => <<<JS
function (ret) {
    $('#modalForm').on('hidden.bs.modal', function(e) {
        $('#modalInfo').on('hidden.bs.modal', function(e) {
            window.location = '/cabinet-school-koop-requests/index?id=' + {$school->id};
        }).modal();
    }).modal('hide');
}
JS

                ]) ?>
                    <?= Html::activeHiddenInput($model, 'id') ?>
                    <?= $form->field($model, 'secret')->passwordInput()->label('Приватный ключ') ?>
                <?php \iAvatar777\services\FormAjax\ActiveForm::end() ?>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
            </div>
        </div>
    </div>
</div>