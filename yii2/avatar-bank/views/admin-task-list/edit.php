<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;
use app\models\UnionCategory;
use yii\db\Query;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \avatar\models\forms\school\Potok */
/* @var $school \common\models\school\School */

$this->title = $model->name;


?>
<div class="container">
    <div class="col-lg-12">
        <h1 class="page-header"><?= Html::encode($this->title) ?></h1>

    <?php if (Yii::$app->session->hasFlash('form')) { ?>
        <div class="alert alert-success">
            Успешно обновлено.
        </div>

    <?php } else { ?>

        <div class="row">
            <div class="col-lg-8">
                <?php $form = ActiveForm::begin([
                    'id'      => 'contact-form',
                    'options' => ['enctype' => 'multipart/form-data'],
                ]); ?>
                <?= $form->field($model, 'name') ?>
                <?= $form->field($model, 'price')->hint('Кол-во атомов (копеек)') ?>
                <?= $form->field($model, 'currency_id')->dropDownList(
                    ArrayHelper::merge(
                        ['- Не выбрано -'],
                        ArrayHelper::map(
                            \common\models\piramida\Currency::find()->orderBy(['code' => SORT_ASC])->all(),
                            'id',
                            'code'
                        )
                    )
                ) ?>
                <?= $form->field($model, 'category_id')->dropDownList(
                    ArrayHelper::map(
                        \common\models\task\Category::find()->orderBy(['sort_index' => SORT_ASC])->where(['school_id' => $school->id])->all(),
                        'id',
                        'name'
                    )
                ) ?>
                <?= $form->field($model, 'content')->textarea(['rows' => 10]) ?>


                <hr>
                <div class="form-group">
                    <?= Html::submitButton('Обновить', [
                        'class' => 'btn btn-default',
                        'name'  => 'contact-button',
                        'style' => 'width:100%',
                    ]) ?>
                </div>
                <?php ActiveForm::end(); ?>
            </div>
        </div>

    <?php } ?>
    </div>

</div>
