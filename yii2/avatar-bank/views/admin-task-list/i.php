<?php
/**
 * Created by PhpStorm.
 * User: Ra-m-ha
 * Date: 31.12.2018
 * Time: 0:44
 */

?>
<?php /** @var \common\models\task\Task $item */ ?>

<li class="ui-state-default" data-id="<?= $item->id ?>">
    <span class="label label-info"><a href="/admin-task-list/edit?id=<?= $item->id ?>">#<?= $item->id ?></a></span> <?= $item->name ?>
    <?php if ($item->executer_id) { ?>
    <?php $user = \common\models\UserAvatar::findOne($item->executer_id) ?>
        <br>
        <img src="<?= $user->getAvatar() ?>" width="30" class="img-circle" title="<?= \yii\helpers\Html::encode($user->getName2()) ?>" data-toggle="tooltip"/>
    <?php } ?>
</li>