<?php

/** $this \yii\web\View  */
/** @var $school \common\models\school\School */

use yii\helpers\Url;
use app\services\GsssHtml;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use app\services\GetArticle\Collection;

$this->title = 'Все сертификаты';



?>

<div class="container">


    <h1 class="page-header"><?= Html::encode($this->title) ?></h1>

    <p>
        <a href="<?= Url::to(['add', 'id' => $school->id]) ?>" class="btn btn-default">Добавить</a>
    </p>

    <?php \yii\widgets\Pjax::begin(); ?>

    <?php
    $this->registerJS(<<<JS
$('[data-toggle="tooltip"]').tooltip();

$('.rowTable').click(function() {
});


JS
    );

    \avatar\assets\Notify::register($this);
    \avatar\assets\Clipboard::register($this);

    $this->registerJs(<<<JS

var clipboard = new Clipboard('.buttonCopy');
clipboard.on('success', function(e) {

    new Noty({
        timeout: 1000,
        theme: 'sunset',
        type: 'success',
        layout: 'bottomLeft',
        text: 'Скопировано'
    }).show();

});

JS
    );
    ?>

    <?= \yii\grid\GridView::widget([
        'dataProvider' => new \yii\data\ActiveDataProvider([
            'query'      => \common\models\CovCert::find()
                ->where(['company_id' => $school->id])
            ,
            'pagination' => [
                'pageSize' => 20,
            ],
        ]),
        'tableOptions' => [
            'class' => 'table table-striped table-hover',
        ],
        'rowOptions'   => function ($item) {
            $data = [
                'data'  => ['id' => $item['id']],
                'role'  => 'button',
                'class' => 'rowTable',
            ];
            return $data;
        },
        'columns' => [
            'id',
            [
                'header'  => 'Пользователь',
                'content' => function ($item) {
                    $i = ArrayHelper::getValue($item, 'user_id', '');
                    if ($i == '') return '';
                    $user = \common\models\UserAvatar::findOne($i);

                    return Html::img($user->getAvatar(), [
                        'class'  => "img-circle",
                        'width'  => 40,
                        'height' => 40,
                        'style'  => 'margin-bottom: 0px;',
                    ]);
                },
            ],
            'born_date:date',
            'end_date:date',
            'passport_num',
            [
                'header'  => 'QR',
                'content' => function ($item) {
                    $address = 'https://www.gosuslugi.ru.qualitylive.su/covid-cert/qr-code/' . $item->uid;
                    return Html::button('QR', [
                        'class' => 'btn btn-default btn-xs buttonCopy',
                        'data'  => [
                            'toggle'         => 'tooltip',
                            'clipboard-text' => $address,
                        ],
                        'title' => 'Нажми чтобы скопировать',
                    ]);
                }
            ],
            [
                'header'  => 'status',
                'content' => function ($item) {
                    $address = 'https://www.gosuslugi.ru.qualitylive.su/covid-cert/status/' . $item->uid;
                    return Html::button('status', [
                        'class' => 'btn btn-default btn-xs buttonCopy',
                        'data'  => [
                            'toggle'         => 'tooltip',
                            'clipboard-text' => $address,
                        ],

                    ]);
                }
            ],
        ]
    ]) ?>
    <?php \yii\widgets\Pjax::end(); ?>


</div>

<div class="modal fade" id="modalInfo" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="myModalLabel">Информация</h4>
            </div>
            <div class="modal-body">
                Успешно!
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
            </div>
        </div>
    </div>
</div>