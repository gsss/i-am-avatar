<?php

/** @var $this \yii\web\View */
/** @var $school \common\models\school\School */
/** @var $model \avatar\models\forms\CabinetSchoolPay */

use yii\helpers\Url;
use app\services\GsssHtml;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use app\services\GetArticle\Collection;

$this->title = 'Оплата тарифа';

Yii::$app->session->set('yii2/avatar-bank/views/cabinet-school/pay.php', $model);


?>

<div class="container">
    <div class="col-lg-12">
        <h1 class="page-header"><?= Html::encode($this->title) ?></h1>


        <?php $form = \iAvatar777\services\FormAjax\ActiveForm::begin([
            'model' => $model,
            'success' => <<<JS
function (ret) {
    $('#modalInfo').on('hidden.bs.modal', function() {
        window.location = '/cabinet-school/index';
    }).modal();
}
JS
            ,
        ]);
        Yii::$app->session->set('yii2/avatar-bank/views/cabinet-school/pay.php_form', $form);

        ?>
        <?= \yii\grid\GridView::widget([
            'dataProvider' => new \yii\data\ActiveDataProvider([
                'query' => \common\models\Tarif::find()
            ]),
            'tableOptions' => [
                'class' => 'table table-striped table-hover',
            ],
            'summary' => '',
            'rowOptions'   => function ($item) {
                $data = [
                    'data'  => ['id' => $item['id']],
                    'role'  => 'button',
                    'class' => 'rowTable',
                ];
                return $data;
            },
            'columns'      => [
                'id',
                'name',
                'price',
                [
                    'header'  => 'Месяцев',
                    'content' => function ($item) {
                        $model = Yii::$app->session->get('yii2/avatar-bank/views/cabinet-school/pay.php');

                        /** @var \iAvatar777\services\FormAjax\ActiveForm $form */
                        $form = Yii::$app->session->get('yii2/avatar-bank/views/cabinet-school/pay.php_form');

                        return $form->field($model, 'name' . $item['id'] . '_month')->label('', ['class' => 'hide']);
                    }
                ],

            ],
        ]) ?>
        <?= $form->field($model, 'pay_id') ?>
        <?= $form->field($model, 'confirm') ?>
        <p><a href="/files/lic.pdf">Лицензионный договор</a></p>
        <hr>
        <?php $form = \iAvatar777\services\FormAjax\ActiveForm::end(['label' => 'Оплатить']); ?>


    </div>

</div>

<div class="modal fade" id="modalInfo" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Информация</h4>
            </div>
            <div class="modal-body">
                Успешно!
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
            </div>
        </div>
    </div>
</div>
