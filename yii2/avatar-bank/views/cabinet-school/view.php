<?php

/** @var $this \yii\web\View */

/** @var $school \common\models\school\School */

use yii\helpers\Url;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;

$this->title = $school->name;

\Yii::$app->view->registerJs(<<<JS

JS

);
?>

<div class="container">


    <h1 class="page-header"><?= Html::encode($this->title) ?></h1>

    <div class="panel panel-default">
        <div class="panel-heading">
            <h3 class="panel-title"><?= $school->name ?></h3>
        </div>
        <div class="panel-body">
            <div class="col-lg-4">
                <?php if ($school->image) { ?>
                    <img src="<?= \common\widgets\FileUpload7\FileUpload::getFile($school->image, 'crop') ?>"
                         width="200" class="img-circle"/>
                <?php } else { ?>
                    <img src="/images/school/school.jpg" alt="..." width="200" class="img-circle">

                <?php }  ?>
            </div>
            <div class="col-lg-8">
                <?php if ($school->dns) { ?>
                    <p>Домен: <a href="<?= $school->getUrl() ?>"><?= $school->getUrl() ?></a></p>
                <?php } else { ?>
                    <a href="<?= Url::to(['cabinet-school-dns/index', 'id' => $school->id]) ?>">Указать домен</a>
                <?php } ?>
                <?php
                $с = \common\models\piramida\Currency::findOne(['code' => 'QL']);
                $sufix = Html::tag('span', $с->code, ['class' => 'label label-info']);
                $w = \common\models\piramida\Wallet::findOne($school->pay_wallet_id);

                $v = Yii::$app->formatter->asDecimal(\common\models\piramida\Currency::getValueFromAtom($w->amount, $с)) .  ' ' .$sufix;
                ?>
                <p>Счет: <?= $v ?></p>
            </div>
        </div>
    </div>


</div>
