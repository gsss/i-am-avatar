<?php

/** @var $this \yii\web\View */
/** @var $school \common\models\school\School */

use yii\helpers\Url;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;

$this->title = 'Загрузить файл в облако';

$this->registerJs(<<<JS

var clipboard = new Clipboard('.buttonCopy');
clipboard.on('success', function(e) {

    new Noty({
        timeout: 1000,
        theme: 'sunset',
        type: 'success',
        layout: 'bottomLeft',
        text: 'Скопировано'
    }).show();

});

JS
);

\avatar\assets\Notify::register($this);
\avatar\assets\Clipboard::register($this);

$config = [];
$one = \common\models\school\Cloud::findOne(['school_id' => $school->id, 'is_active' => 1]);
if (is_null($one)) {
    $config['server'] = Yii::$app->AvatarCloud->url;
} else {
    $config['server'] = $one->url;
}

?>

<div class="container">

    <h1 class="page-header"><?= Html::encode($this->title) ?></h1>

    <p>File: <code id="file"></code> <button class="btn btn-info btn-xs buttonCopy buttonCopyFile" data-toggle="tooltip" title="Нажми чтобы скопировать">Скопировать</button></p>
    <p>sha256: <code id="sha256"></code> <button class="btn btn-info btn-xs buttonCopy buttonCopySha256" data-toggle="tooltip" title="Нажми чтобы скопировать">Скопировать</button></p>

    <?= \iAvatar777\widgets\FileUpload8\FileUpload::widget([
        'id'        => 'upload' . 1,
        'name'      => 'upload' . 1,
        'attribute' => 'upload' . 1,
        'model'     => new \avatar\models\forms\Upload1(),

        'update'    => [
            [
                'function' => 'crop',
                'index'    => 'crop',
                'options'  => [
                    'width'  => '300',
                    'height' => '300',
                    'mode'   => 'MODE_THUMBNAIL_CUT',
                ],
            ],
        ],

        'settings'  => [
            'server'          => $config['server'],
            'accept'          => '*/*',
            'maxSize'         => 20 * 1000,
            'functionSuccess' => new \yii\web\JsExpression(<<<JS
function (response) {
    // Вызываю AJAX для записи в school_file
    $('#file').html(response.url);
    $('.buttonCopyFile').attr('data-clipboard-text', response.url);
    
    ajaxJson({
        url: '/cabinet/file-upload7-save',
        data: {
            file: response.url,
            school_id: {$school->id},
            type_id: 12,
            size: response.size // Размер файла в байтах
        },
        success: function (ret) {
            ajaxJson({
                url: '/cabinet-school/file-sha256-cloud',
                data: {
                    file: response.url
                },
                success: function (ret) {
                    $('#sha256').html(ret.hash);
                    $('.buttonCopySha256').attr('data-clipboard-text', ret.hash);
                }
            });
        }
    });
}

JS
            ),
        ],
    ]); ?>


</div>
