<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;
use yii\db\Query;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \avatar\models\forms\School */

$this->title = 'Добавить сообщество';

?>
<div class="container">
    <div class="col-lg-12">
        <h1 class="page-header"><?= Html::encode($this->title) ?></h1>
    </div>

    <div class="col-lg-12">


        <?php if (!is_null($id = Yii::$app->session->getFlash('form'))) : ?>

            <div class="alert alert-success">
                Успешно добавлено.
            </div>

            <p>
                <a href="<?= \yii\helpers\Url::to(['cabinet-school/index']) ?>" class="btn btn-success">Все сообщества</a>
            </p>

        <?php else: ?>


            <div class="row">
                <div class="col-lg-8">
                    <?php $form = ActiveForm::begin([
                        'options' => ['enctype' => 'multipart/form-data'],
                    ]); ?>
                    <?= $model->field($form, 'name') ?>
                    <?= $model->field($form, 'description')->textarea(['rows' => 5]) ?>
                    <?= $model->field($form, 'about')->textarea(['rows' => 25]) ?>


                    <hr>
                    <div class="form-group">
                        <?= Html::submitButton('Добавить', [
                            'class' => 'btn btn-default',
                            'name'  => 'contact-button',
                            'style' => 'width:100%',
                        ]) ?>
                    </div>
                    <?php ActiveForm::end(); ?>
                </div>
            </div>

        <?php endif; ?>

    </div>
</div>
