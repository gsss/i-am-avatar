<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;
use app\models\UnionCategory;
use yii\db\Query;
use yii\helpers\Url;

/** @var $school  \common\models\school\School */
/* @var  $this    yii\web\View */


$requestedRoute = Yii::$app->requestedRoute;
$menu           = [
    [
        'label'   => 'Курсы',
        'url'     => ['cabinet-school-kurs-list/index', 'id' => $school->id],
        'urlList' => [
            ['controller', 'cabinet-school-kurs-list'],
            ['controller', 'cabinet-school-potok'],
            ['controller', 'cabinet-school-subscribe'],
        ],
    ],
    [
        'label'   => 'Страницы',
        'url'     => ['cabinet-school-pages/index', 'id' => $school->id],
        'urlList' => [
            ['controller', 'cabinet-school-pages'],
            ['controller', 'cabinet-school-page-blocks'],
        ],
    ],
    [
        'label'   => 'Пользователи',
        'url'     => ['cabinet-school-users/index', 'id' => $school->id],
        'urlList' => [
            ['controller', 'cabinet-school-users'],
        ],
    ],
    [
        'label'   => 'Блог',
        'url'     => ['cabinet-school-blog/index', 'id' => $school->id],
        'urlList' => [
            ['controller', 'cabinet-school-blog'],
        ],
    ],
    [
        'label'   => 'Домен',
        'url'     => ['cabinet-school/dns', 'id' => $school->id],
        'urlList' => [
            ['route', 'cabinet-school/dns'],
        ],
    ],
    [
        'label'   => 'Учителя',
        'url'     => ['cabinet-school-teachers/index', 'id' => $school->id],
        'urlList' => [
            ['controller', 'cabinet-school-teachers'],
        ],
    ],
    [
        'label'   => 'Администраторы',
        'url'     => ['cabinet-school-admins/index', 'id' => $school->id],
        'urlList' => [
            ['controller', 'cabinet-school-admins'],
        ],
    ],
    [
        'label'   => 'Плагины экспорта лидов',
        'url'     => ['cabinet-school-plug-out/index', 'id' => $school->id],
        'urlList' => [
            ['controller', 'cabinet-school-plug-out'],
        ],
    ],
    [
        'label'   => 'Задачи',
        'url'     => ['cabinet-school-task-list/index', 'id' => $school->id],
        'urlList' => [
            ['controller', 'cabinet-school-task-list'],
        ],
    ],
];



/**
 * @param array $item Текущая на вывод элемент списка
 *
 * @return bool
 */
function isActive3($item)
{
    if (isset($item['items'])) {
        foreach ($item['items'] as $i) {
            if (isActive3($i)) {
                return true;
            }
        }
    }
    $route = Yii::$app->requestedRoute;
    $uriCurrent = Url::current();
    if (\yii\helpers\ArrayHelper::keyExists('urlList', $item)) {
        foreach ($item['urlList'] as $i) {
            switch ($i[0]) {
                case 'route':
                    if ($route == $i[1]) return true;
                    break;
                case 'startsWith':
                    if (\yii\helpers\StringHelper::startsWith($uriCurrent, $i[1])) return true;
                    break;
                case 'controller':
                    $arr = explode('/', $route);
                    if ($arr[0] == $i[1]) return true;
                    break;
            }
        }
    }

    return false;
}

?>

<div class="list-group">
    <?php
    foreach ($menu as $item) {
        $options = ['class' => ['list-group-item']];
        if (isActive3($item, Yii::$app->requestedRoute)) {
            $options['class'][] = 'active';
        }
        if (\yii\helpers\ArrayHelper::keyExists('data', $item)) {
            $options['data'] = $item['data'];
        }
        $options['class'] = join(' ', $options['class']);
        echo Html::a($item['label'], $item['url'], $options);
    }
    ?>
</div>
