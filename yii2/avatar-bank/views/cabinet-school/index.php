<?php

/** $this \yii\web\View  */

use yii\helpers\Url;
use app\services\GsssHtml;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use app\services\GetArticle\Collection;

$this->title = 'Все сообщества';

\avatar\assets\Notify::register($this);
\avatar\assets\Clipboard::register($this);
$this->registerJs(<<<JS

var clipboard = new Clipboard('.buttonCopy');
clipboard.on('success', function(e) {
    e.preventDefault();
    e.stopPropagation();
    
    new Noty({
        timeout: 1000,
        theme: 'sunset',
        type: 'success',
        layout: 'bottomLeft',
        text: 'Скопировано'
    }).show();

});

JS
);
?>

<div class="container">
    <div class="col-lg-12">
        <h1 class="page-header"><?= Html::encode($this->title) ?></h1>
    </div>

    <div class="col-lg-12">

        <p><a href="<?= Url::to(['cabinet-school/add']) ?>" class="btn btn-default">Добавить</a></p>

        <?php \yii\widgets\Pjax::begin(); ?>
        <?php
        $this->registerJS(<<<JS
$('[data-toggle="tooltip"]').tooltip();

$('.rowTable').click(function() {
    window.location = '/cabinet-school/view' + '?' + 'id' + '=' + $(this).data('id');
});
$('.buttonEdit').click(function(e) {
    e.preventDefault();
    e.stopPropagation();
    window.location = '/cabinet-school/edit' + '?' + 'id' + '=' + $(this).data('id');
});
$('.buttonPay').click(function(e) {
    e.preventDefault();
    e.stopPropagation();
    window.location = '/cabinet-school/pay' + '?' + 'id' + '=' + $(this).data('id');
});


$('.buttonHide').click(function (e) {
    e.preventDefault();
    e.stopPropagation();
    if (confirm('Подтвердите сокрытие школы')) {
        var button = $(this);
        var id = $(this).data('id');
        ajaxJson({
            url: '/cabinet-school/hide' + '?' + 'id' + '=' + id,
            success: function (ret) {
                $('#modalInfo').on('hidden.bs.modal', function() {
                    window.location.reload();
                }).modal();
            }
        });
    }
});
JS
        );
        $idsAdmin = \common\models\school\AdminLink::find()->where(['user_id' => Yii::$app->user->id])->select('school_id')->column();
//        $idsCommand = \common\models\school\CommandLink::find()->where(['user_id' => Yii::$app->user->id])->select('school_id')->column();
        $idsCommand = [];
        $ids = ArrayHelper::merge($idsAdmin, $idsCommand);
        $ids = array_unique($ids);

        ?>
        <?= \yii\grid\GridView::widget([
            'dataProvider' => new \yii\data\ActiveDataProvider([
                'query'      => \common\models\school\School::find()
                    ->where(['id' => $ids])
                    ->andWhere(['school.is_hide' => 0])
                ,
                'sort' => ['defaultOrder' => [
                    'id' => SORT_DESC,
                ]],
                'pagination' => [
                    'pageSize' => 50,
                ],
            ]),
            'tableOptions' => [
                'class' => 'table table-striped table-hover',
            ],
            'rowOptions'   => function ($item) {
                $data = [
                    'data'  => ['id' => $item['id']],
                    'role'  => 'button',
                    'class' => 'rowTable',
                ];
                return $data;
            },
            'columns'      => [
                'id',
                [
                    'header'  => 'Картинка',
                    'content' => function ($item) {
                        $i = ArrayHelper::getValue($item, 'image', '');
                        if ($i == '') return Html::img('/images/school/school.jpg', [
                                'class'  => "img-circle",
                                'width'  => 60,
                                'height' => 60,
                                'style'  => 'margin-bottom: 0px; opacity: 0.2;',
                            ]
                        );

                        return Html::img(\common\widgets\FileUpload7\FileUpload::getFile($i, 'crop'), [
                            'class'  => "img-circle",
                            'width'  => 60,
                            'height' => 60,
                            'style'  => 'margin-bottom: 0px;',
                        ]);
                    },
                ],
                [
                    'header'  => 'Название',
                    'content' => function (\common\models\school\School $item) {
                        return Html::a($item['name'], ['cabinet-school/view', 'id' => $item['id']], ['data' => ['pjax' => 0]]);
                    },
                ],
                [
                    'header'  => 'Ссылка',
                    'content' => function (\common\models\school\School $item) {
                        $v = \yii\helpers\ArrayHelper::getValue($item, 'dns');
                        if (is_null($v)) return '';

                        return Html::tag('abbr', $item->getUrl());
                    },
                ],
                [
                    'header'  => 'Счет',
                    'content' => function (\common\models\school\School $item) {
                        $с = \common\models\piramida\Currency::findOne(['code' => 'QL']);
                        $sufix = Html::tag('span', $с->code, ['class' => 'label label-info']);
                        $w = \common\models\piramida\Wallet::findOne($item->pay_wallet_id);

                        return Yii::$app->formatter->asDecimal(\common\models\piramida\Currency::getValueFromAtom($w->amount, $с)) .  ' ' .$sufix;
                    },
                ],

                [
                    'header'  => 'Редактировать',
                    'content' => function ($item) {
                        return Html::button('Редактировать', [
                            'class' => 'btn btn-info btn-xs buttonEdit',
                            'data'  => [
                                'id' => $item['id'],
                            ],
                        ]);
                    },
                ],
                [
                    'header'  => 'Скрыть',
                    'content' => function ($item) {
                        return Html::button('Скрыть', [
                            'class' => 'btn btn-default btn-xs buttonHide',
                            'data'  => [
                                'id' => $item['id'],
                            ],
                        ]);
                    },
                ],
            ],
        ]) ?>
        <?php \yii\widgets\Pjax::end(); ?>

    </div>
</div>

<div class="modal fade" id="modalInfo" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="myModalLabel">Информация</h4>
            </div>
            <div class="modal-body">
                Успешно!
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
            </div>
        </div>
    </div>
</div>