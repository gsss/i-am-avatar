<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;
use app\models\UnionCategory;
use yii\db\Query;
use yii\helpers\Url;

/** @var $school  \common\models\school\School */
/** @var $content  string */
/* @var  $this    yii\web\View */

$menu = [
    [
        'buttonList' => [
            [
                'label'   => 'Курсы',
                'url'     => ['cabinet-school-kurs-list/index', 'id' => $school->id],
                'urlList' => [
                    ['controller', 'cabinet-school-kurs-list'],
                    ['controller', 'cabinet-school-potok'],
                    ['controller', 'cabinet-school-subscribe'],
                    ['controller', 'cabinet-school-lesson'],
                    ['controller', 'cabinet-school-potok-sale'],
                    ['controller', 'cabinet-school-potok-request'],
                ],
                'count'   => function (\common\models\school\School $school) {
                    return \common\models\school\Kurs::find()->where(['school_id' => $school->id])->count();
                },
            ],
            [
                'label'   => 'Страницы',
                'url'     => ['cabinet-school-pages/index', 'id' => $school->id],
                'urlList' => [
                    ['controller', 'cabinet-school-pages'],
                    ['controller', 'cabinet-school-page-blocks'],
                ],
                'count'   => function (\common\models\school\School $school) {
                    return \common\models\school\Page::find()->where(['school_id' => $school->id])->count();
                },
            ],
            [
                'label'   => 'Блог',
                'url'     => ['cabinet-school-blog/index', 'id' => $school->id],
                'urlList' => [
                    ['controller', 'cabinet-school-blog'],
                ],
                'count'   => function (\common\models\school\School $school) {
                    return \common\models\blog\Article::find()->where(['school_id' => $school->id])->count();
                },
            ],
            [
                'label'   => 'Новости',
                'url'     => ['cabinet-school-news/index', 'id' => $school->id],
                'urlList' => [
                    ['controller', 'cabinet-school-news'],
                ],
            ],
            [
                'label'   => 'Голосования',
                'url'     => ['cabinet-vote/index', 'id' => $school->id],
                'urlList' => [
                    ['controller', 'cabinet-vote'],
                    ['controller', 'cabinet-vote-answer'],
                ],
            ],
            [
                'label'   => 'Участники',
                'url'     => ['cabinet-school-users/index', 'id' => $school->id],
                'urlList' => [
                    ['controller', 'cabinet-school-users'],
                    ['controller', 'cabinet-school-subscribe2'],
                ],
            ],
            [
                'label'   => 'Документы',
                'url'     => ['cabinet-school-documents/index', 'id' => $school->id],
                'urlList' => [
                    ['controller', 'cabinet-school-documents'],
                ],
            ],
        ],
    ],
    [
        'dropDownList' => [
            [
                'label' => 'Задачи',
                'items' => [
                    [
                        'label'   => 'Добавить',
                        'url'     => ['cabinet-school-task-list/add', 'id' => $school->id],
                        'urlList' => [
                            ['route', 'cabinet-school-task-list/add'],
                        ],
                    ],
                    [
                        'label'   => 'Список',
                        'url'     => ['cabinet-school-task-list/index', 'id' => $school->id],
                        'urlList' => [
                            ['controller', 'cabinet-school-task-list'],
                        ],
                    ],
                    [
                        'label'   => 'Список2',
                        'url'     => ['cabinet-school-task-list/index2', 'id' => $school->id],
                        'urlList' => [
                            ['controller', 'cabinet-school-task-list'],
                        ],
                    ],
                    [
                        'label'   => 'Доска',
                        'url'     => ['cabinet-school-task-list/ajail', 'id' => $school->id],
                        'urlList' => [
                            ['route', 'cabinet-school-task-list/ajail'],
                        ],
                    ],
                    [
                        'label'   => 'Руководители проектов',
                        'url'     => ['cabinet-school-task-list/project-manager-list', 'id' => $school->id],
                        'urlList' => [
                            ['route', 'cabinet-school-task-list/project-manager-list'],
                        ],
                    ],
                    [
                        'label'   => 'Категории',
                        'url'     => ['cabinet-school-task-list-category/index', 'id' => $school->id],
                        'urlList' => [
                            ['controller', 'cabinet-school-task-list-category'],
                        ],
                    ],
                    [
                        'label'   => 'Команда',
                        'url'     => ['cabinet-school-task-list-command/index', 'id' => $school->id],
                        'urlList' => [
                            ['controller', 'cabinet-school-task-list-command'],
                        ],
                    ],
                    [
                        'label'   => 'Доски',
                        'url'     => ['cabinet-school-task-list-table/index', 'id' => $school->id],
                        'urlList' => [
                            ['controller', 'cabinet-school-task-list-table'],
                        ],
                    ],
                ],
            ],
        ],
    ],
    [
        'dropDownList' => [
            [
                'label' => 'Интернет магазин',
                'items' => [
                    [
                        'label'   => 'Товары',
                        'url'     => ['cabinet-school-shop-goods/index', 'id' => $school->id],
                        'urlList' => [
                            ['controller', 'cabinet-school-shop-goods'],
                        ],
                    ],
                    [
                        'label'   => 'Доставки',
                        'url'     => ['cabinet-school-shop-delivery/index', 'id' => $school->id],
                        'urlList' => [
                            ['controller', 'cabinet-school-shop-delivery'],
                        ],
                    ],
                    [
                        'label'   => 'Каталог',
                        'url'     => ['cabinet-school-shop-catalog/index', 'id' => $school->id],
                        'urlList' => [
                            ['controller', 'cabinet-school-shop-catalog'],
                        ],
                    ],
                    [
                        'label'   => 'Заказы',
                        'url'     => ['cabinet-school-shop-requests/index', 'id' => $school->id],
                        'urlList' => [
                            ['controller', 'cabinet-school-shop-requests'],
                        ],
                    ],
                ],
            ],
        ],
    ],
    [
        'dropDownList' => [
            [
                'label' => 'Кооператив',
                'items' => [
                    [
                        'label'   => 'Заявки',
                        'url'     => ['cabinet-school-koop-requests/index', 'id' => $school->id],
                        'urlList' => [
                            ['controller', 'cabinet-school-koop-requests'],
                        ],
                    ],
                    [
                        'label'   => 'Целевые программы',
                        'url'     => ['cabinet-school-koop-aim-programs/index', 'id' => $school->id],
                        'urlList' => [
                            ['controller', 'cabinet-school-koop-aim-programs'],
                        ],
                    ],
                    [
                        'label'   => 'Кошельки',
                        'url'     => ['cabinet-school-koop/wallets', 'id' => $school->id],
                        'urlList' => [
                            ['route', 'cabinet-school-koop/wallets'],
                        ],
                    ],
                    [
                        'label'   => 'Настройки',
                        'url'     => ['cabinet-school-koop-settings/index', 'id' => $school->id],
                        'urlList' => [
                            ['controller', 'cabinet-school-koop-settings'],
                        ],
                    ],
                ],
            ],
        ],
    ],
    [
        'dropDownList' => [
            [
                'label' => 'Настройка',
                'items' => [
                    [
                        'label'   => 'Домен',
                        'url'     => ['cabinet-school-dns/index', 'id' => $school->id],
                        'urlList' => [
                            ['controller', 'cabinet-school-dns'],
                        ],
                    ],
                    [
                        'label'   => 'Роли',
                        'url'     => ['cabinet-school-role/index', 'id' => $school->id],
                        'urlList' => [
                            ['controller', 'cabinet-school-role'],
                        ],
                    ],

                    [
                        'label'   => 'Учителя',
                        'url'     => ['cabinet-school-teachers/index', 'id' => $school->id],
                        'urlList' => [
                            ['controller', 'cabinet-school-teachers'],
                        ],
                    ],
                    [
                        'label'   => 'Администраторы',
                        'url'     => ['cabinet-school-admins/index', 'id' => $school->id],
                        'urlList' => [
                            ['controller', 'cabinet-school-admins'],
                        ],
                    ],
                    [
                        'label'   => 'Платежные системы',
                        'url'     => ['cabinet-school-pay-systems/index', 'id' => $school->id],
                        'urlList' => [
                            ['controller', 'cabinet-school-pay-systems'],
                        ],
                    ],
                    [
                        'label'   => 'Плагины экспорта лидов',
                        'url'     => ['cabinet-school-plug-out/index', 'id' => $school->id],
                        'urlList' => [
                            ['controller', 'cabinet-school-plug-out'],
                        ],
                    ],
                    [
                        'label'   => 'Настройки задач',
                        'url'     => ['cabinet-school-settings/task', 'id' => $school->id],
                        'urlList' => [
                            ['route', 'cabinet-school-settings/task'],
                        ],
                    ],
                    [
                        'label'   => 'Облако',
                        'url'     => ['cabinet-school-files/index', 'id' => $school->id],
                        'urlList' => [
                            ['controller', 'cabinet-school-files'],
                        ],
                    ],
                    [
                        'label'   => '1C API',
                        'url'     => ['cabinet-school-settings/onec', 'id' => $school->id],
                        'urlList' => [
                            ['route', 'cabinet-school-settings/onec'],
                        ],
                    ],
                    [
                        'label'   => 'SMS шлюз',
                        'url'     => ['cabinet-school-settings/sms', 'id' => $school->id],
                        'urlList' => [
                            ['route', 'cabinet-school-settings/sms'],
                        ],
                    ],
                    [
                        'label'   => 'Загрузить файл в облако',
                        'url'     => ['cabinet-school/file-sha256', 'id' => $school->id],
                        'urlList' => [
                            ['route', 'cabinet-school/file-sha256'],
                        ],
                    ],
                    [
                        'label'   => 'Настройка партнеской программы',
                        'url'     => ['cabinet-school-mlm/index', 'id' => $school->id],
                        'urlList' => [
                            ['controller', 'cabinet-school-mlm'],
                        ],
                    ],
                    [
                        'label'   => 'Настройки почты',
                        'url'     => ['cabinet-school-settings/index', 'id' => $school->id],
                        'urlList' => [
                            ['route', 'cabinet-school-settings/index'],
                        ],
                    ],
                    [
                        'label'   => 'Настройки магазина',
                        'url'     => ['cabinet-school-settings/shop', 'id' => $school->id],
                        'urlList' => [
                            ['route', 'cabinet-school-settings/shop'],
                        ],
                    ],
                    [
                        'label'   => 'Настройки блога',
                        'url'     => ['cabinet-school-settings/blog', 'id' => $school->id],
                        'urlList' => [
                            ['route', 'cabinet-school-settings/blog'],
                        ],
                    ],
                    [
                        'label'   => 'Настройки новостей',
                        'url'     => ['cabinet-school-settings/news', 'id' => $school->id],
                        'urlList' => [
                            ['route', 'cabinet-school-settings/news'],
                        ],
                    ],
                    [
                        'label'   => 'Настройки дизайна',
                        'url'     => ['cabinet-school-settings/design-sub', 'id' => $school->id],
                        'urlList' => [
                            ['route', 'cabinet-school-settings/design-sub'],
                        ],
                    ],
                    [
                        'label'   => 'Настройки дизайна главная',
                        'url'     => ['cabinet-school-settings/design-main', 'id' => $school->id],
                        'urlList' => [
                            ['route', 'cabinet-school-settings/design-main'],
                        ],
                    ],
                    [
                        'label'   => 'Плагин Тильды',
                        'url'     => ['cabinet-school-tilda/index', 'id' => $school->id],
                        'urlList' => [
                            ['controller', 'cabinet-school-tilda'],
                        ],
                    ],
                ],
            ],
        ],
    ],
];
//if (Yii::$app->user->can('permission_admin')) {
//    $menu['dropDownList'][] = [
//        'label'   => 'Кошельки школы',
//        'url'     => ['cabinet-school-wallets/index', 'id' => $school->id],
//        'urlList' => [
//            ['controller', 'cabinet-school-wallets'],
//        ],
//    ];
//}
//if (Yii::$app->user->can('permission_admin')) {
//    $menu['buttonList'][] = [
//        'label'   => 'События',
//        'url'     => ['cabinet-school-events/index', 'id' => $school->id],
//        'urlList' => [
//            ['controller', 'cabinet-school-events'],
//        ],
//    ];
//}

return $menu;
