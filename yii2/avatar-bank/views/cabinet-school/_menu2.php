<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;
use app\models\UnionCategory;
use yii\db\Query;
use yii\helpers\Url;

/** @var $school  \common\models\school\School */
/** @var $content  string */
/** @var  $this    yii\web\View */


$requestedRoute = Yii::$app->requestedRoute;
$menu = require('_menu2_data.php');

$i = $school->image;
if (\cs\Application::isEmpty($i)) {
    $i = 'https://for-people.life/images/widget/school-menu/1.png';
} else {
    $i = \common\widgets\FileUpload7\FileUpload::getFile($i, 'crop');
}

?>


<div class="col-lg-12">

    <a href="<?= Url::to(['cabinet-school/view', 'id' => $school->id]) ?>">
        <img  class="img-circle" src="<?= $i ?>" width="30" title="Перейти на главную школы" data-toggle="tooltip">
    </a>

    <div class="btn-group" role="group" aria-label="...">
        <?php foreach ($menu as $itemRoot) { ?>
            <?php if (isset($itemRoot['buttonList'])) { ?>
                <?php foreach ($itemRoot['buttonList'] as $item) { ?>
                    <?php $isActive = \avatar\widgets\SchoolMenu::isActive3($item); ?>
                    <?php $color = $isActive ? 'info' : 'default'; ?>

                    <a href="<?= Url::to($item['url']) ?>" class="btn btn-<?= $color ?>"><?= $item['label'] ?></a>
                <?php } ?>
            <?php } ?>
            <?php if (isset($itemRoot['dropDownList'])) { ?>
                <?php foreach ($itemRoot['dropDownList'] as $item2) { ?>
                    <div class="btn-group" role="group">
                        <?php
                        $isActive2 = false;
                        try {
                            foreach ($item2['items'] as $item) {
                                $isActive = \avatar\widgets\SchoolMenu::isActive3($item);
                                if ($isActive) {
                                    $isActive2 = true;
                                    break;
                                }
                            }

                        } catch (Exception $e) {
                            \cs\services\VarDumper::dump($item2);
                        }
                        $color = $isActive2 ? 'info' : 'default';
                        ?>

                        <button type="button" class="btn btn-<?= $color ?> dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <?= $item2['label'] ?> <span class="caret"></span>
                        </button>
                        <ul class="dropdown-menu">
                            <?php foreach ($item2['items'] as $item) { ?>
                                <?php $isActive = \avatar\widgets\SchoolMenu::isActive3($item); ?>
                                <?php $options = $isActive ? ['class' => 'active'] : []; ?>

                                <li>
                                    <?= Html::a($item['label'], $item['url'], $options) ?>
                                </li>
                            <?php } ?>
                        </ul>
                    </div>
                <?php } ?>

            <?php } ?>
        <?php } ?>






    </div>
</div>
<div class="col-lg-12">
    <hr>
</div>

<div class="col-lg-12">
    <?= $content ?>
</div>

