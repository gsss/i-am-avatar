<?php

/** @var $this \yii\web\View  */

use yii\helpers\Url;
use app\services\GsssHtml;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use app\services\GetArticle\Collection;

$this->title = 'Все заявки DNS';


?>

<div class="container">
    <div class="col-lg-12">
        <h1 class="page-header"><?= Html::encode($this->title) ?></h1>


        <?php \yii\widgets\Pjax::begin(); ?>
        <?php
        $this->registerJS(<<<JS
$('[data-toggle="tooltip"]').tooltip();

$('.buttonRejectDone').click(function (e) {
    e.preventDefault();
    e.stopPropagation();
    if (confirm('Подтвердите')) {
        var button = $(this);
        var id = $('#modalReject').data('id');
        ajaxJson({
            url: '/admin-school-dns/reject',
            data: {
               text: $('#textarea1').val(),
               id: id
            },
            success: function (ret) {
                $('#modalReject').on('hidden.bs.modal', function() {
                    window.location.reload();
                }).modal('hide');
            }
        });
    }
});

$('.buttonReject').click(function (e) {
    e.preventDefault();
    e.stopPropagation();
    var id = $(this).data('id');
    $('#modalReject').data('id', id);
    $('#modalReject').modal();
});

$('.buttonAccept').click(function (e) {
    e.preventDefault();
    e.stopPropagation();
    if (confirm('Подтвердите')) {
        var button = $(this);
        var id = $(this).data('id');
        ajaxJson({
            url: '/admin-school-dns/accept',
            data: {
                id: id
            },
            success: function (ret) {
                $('#modalInfo').on('hidden.bs.modal', function() {
                    window.location.reload();
                }).modal();
            }
        });
    }
});

JS
        );
        ?>
        <?= \yii\grid\GridView::widget([
            'dataProvider' => new \yii\data\ActiveDataProvider([
                'query'      => \common\models\school\DnsRequest::find()
                    ->innerJoin('school', 'school.id = school_dns_request.school_id')
                    ->select([
                        'school_dns_request.*',
                        'school.name as school_name',
                        'school.image as school_image',
                    ])
                    ->orderBy(['created_at' => SORT_DESC])
                    ->asArray()
                ,
                'pagination' => [
                    'pageSize' => 20,
                ],
            ]),
            'tableOptions' => [
                'class' => 'table table-striped table-hover',
            ],
            'rowOptions'   => function ($item) {
                $data = [
                    'data'  => ['id' => $item['id']],
                    'role'  => 'button',
                    'class' => 'rowTable'
                ];
                return $data;
            },
            'columns'      => [
                'id',
                [
                    'header'  => 'Картинка',
                    'content' => function ($item) {
                        $i = ArrayHelper::getValue($item, 'school_image', '');
                        if ($i == '') return Html::img('/images/1x1.png', [
                                'class'  => "img-circle",
                                'width'  => 40,
                                'height' => 40,
                                'style'  => 'margin-bottom: 0px;',
                            ]
                        );

                        return Html::img($i, [
                            'class'  => "img-circle",
                            'width'  => 40,
                            'height' => 40,
                            'style'  => 'margin-bottom: 0px;',
                        ]);
                    },
                ],
                'school_name',
                'name',
                [
                    'header'    => 'Результат',
                    'filter'    => [
                        -1 => 'Отказ',
                        0 => 'В ожидании',
                        1 => 'Одобрено',
                    ],
                    'content'   => function ($item) {
                        $v = \yii\helpers\ArrayHelper::getValue($item, 'is_dns_verified', 0);
                        if ($v == -1) return Html::tag('span', 'Отказ', ['class' => 'label label-danger']);
                        if ($v == 0) return Html::tag('span', 'В ожидании', ['class' => 'label label-default']);

                        return Html::tag('span', 'Одобрено', ['class' => 'label label-success']);
                    },
                    'attribute' => 'is_dns_verified',
                ],
                [
                    'header'  => 'Создано',
                    'content' => function ($item) {
                        $v = \yii\helpers\ArrayHelper::getValue($item, 'created_at', 0);
                        if ($v == 0) return '';

                        return Html::tag('abbr', \cs\services\DatePeriod::back($v, ['isShort' => true]), ['data' => ['toggle' => 'tooltip'], 'title' => Yii::$app->formatter->asDatetime($v)]);
                    }
                ],
                [
                    'header'  => 'Утвердить',
                    'content' => function ($item) {
                        $v = \yii\helpers\ArrayHelper::getValue($item, 'is_dns_verified', 0);
                        if (in_array($v, [-1, 1])) return '';

                        return Html::button('Утвердить', [
                            'class' => 'btn btn-success btn-xs buttonAccept',
                            'data'  => [
                                'id' => $item['id'],
                            ]
                        ]);
                    }
                ],
                [
                    'header'  => 'Отказать',
                    'content' => function ($item) {
                        $v = \yii\helpers\ArrayHelper::getValue($item, 'is_dns_verified', 0);
                        if (in_array($v, [-1, 1])) return '';

                        return Html::button('Отказать', [
                            'class' => 'btn btn-danger btn-xs buttonReject',
                            'data'  => [
                                'id' => $item['id'],
                            ]
                        ]);
                    }
                ],
            ],
        ]) ?>
        <?php \yii\widgets\Pjax::end(); ?>
    </div>
</div>

<div class="modal fade" id="modalInfo" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Информация</h4>
            </div>
            <div class="modal-body">
                Успешно!
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modalReject" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Информация</h4>
            </div>
            <div class="modal-body">
                <textarea id="textarea1" rows="10" class="form-control"></textarea>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary buttonRejectDone" >Отправить отказ</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
            </div>
        </div>
    </div>
</div>