<?php
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
$this->title = 'Пирамида Света';

?>

<div class="container" style="padding-bottom: 70px;">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header text-center">
                Пирамида Света
            </h1>
            <p class="page-header text-center">
                <a href="/docs/game">
                    <img src="/images/controller/docs/game/15094309_1043941682377832_3771919297186223001_n.jpg" width="100%">
                </a>
            </p>
            <p class="page-header text-center">
                <a href="http://zarfund.network-vitneu.de/" target="_blank">
                    http://zarfund.network-vitneu.de/
                </a>
            </p>

        </div>
    </div>
</div>


