<?php

use common\models\task\Session;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;
use yii\db\Query;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $task \common\models\task\Task */
/* @var $school \common\models\school\School */

?>

<?php
$this->registerJs(<<<JS
$('.buttonStart').click(function(e) {
    ajaxJson({
        url: '/cabinet-school-task-list/view-start',
        data: {
            id: $(this).data('id')
        },
        success: function(ret) {
            window.location.reload();
        }
    })
});
$('.buttonStartConfirm').click(function(e) {
    ajaxJson({
        url: '/cabinet-school-task-list/view-close-session-start',
        data: {
            id: $(this).data('id')
        },
        success: function(ret) {
            window.location.reload();
        }
    })
});
JS
);
?>
<button class="btn btn-default buttonStart" data-id="<?= $model->id ?>">Я сделаю это</button>

<div class="modal fade" id="modalStart" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="myModalLabel">Предупреждение</h4>
            </div>
            <div class="modal-body">
                <p>У тебя уже существует открытая сессия. Что делать?</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary buttonStartConfirm" data-id="<?= $model->id ?>">Закрыть предыдущую и начать эту</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Отмена</button>
            </div>
        </div>
    </div>
</div>