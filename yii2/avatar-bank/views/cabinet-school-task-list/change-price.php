<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;
use app\models\UnionCategory;
use yii\db\Query;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \common\models\task\Task */
/* @var $school \common\models\school\School */

$this->title = 'Изменить награду на задачу';

$currencyList = [];
$SchoolCoin = \common\models\SchoolCoin::find()
    ->where(['school_id' => $school->id])
    ->all();
$rows = [];
foreach ($SchoolCoin as $r) {
    $c = \common\models\piramida\Currency::findOne($r['currency_id']);
    $rows[$r['currency_id']] = $c->code;
}
$currencyList = $rows;
?>
<div class="container">

    <h1 class="page-header"><?= Html::encode($this->title) ?></h1>


    <?php if (Yii::$app->session->hasFlash('form')) : ?>
        <div class="alert alert-success">
            Успешно добавлено.
        </div>
        <p>
            <a href="<?= \yii\helpers\Url::to(['cabinet-school-task-list/index', 'id' => $school->id]) ?>" class="btn btn-success">Все задачи</a>
            <a href="<?= \yii\helpers\Url::to(['cabinet-school-task-list/view', 'id' => Yii::$app->session->getFlash('form')]) ?>" class="btn btn-success">Эта задача</a>
        </p>
    <?php else: ?>
        <?php $form = ActiveForm::begin([
            'options' => ['enctype' => 'multipart/form-data'],
        ]); ?>
        <?= $form->field($model, 'price')->hint('Кол-во монет')->label('Награда') ?>
        <?= $form->field($model, 'currency_id')->label('Валюта')->dropDownList($currencyList) ?>

        <hr>
        <div class="form-group">
            <?= Html::submitButton('Обновить', [
                'class' => 'btn btn-default',
                'name'  => 'contact-button',
                'style' => 'width:100%',
            ]) ?>
        </div>
        <?php ActiveForm::end(); ?>
    <?php endif; ?>
</div>
