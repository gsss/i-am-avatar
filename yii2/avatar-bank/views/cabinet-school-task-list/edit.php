<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;
use yii\db\Query;
use yii\helpers\ArrayHelper;

/** @var $this yii\web\View */
/** @var $form yii\bootstrap\ActiveForm */
/** @var $model \avatar\models\forms\school\Potok */
/** @var $school \common\models\school\School */

$this->title = $model->name;

$currencyList = [];
$currencySchool = $school->currency_id;
if (!is_null($currencySchool)) {
    $currencyList[$currencySchool] = \common\models\piramida\Currency::findOne($currencySchool)->code;
}
$fundRub = $school->fund_rub2;
if (!is_null($fundRub)) {
    $currencyList[7] = 'RUB';
}
?>
<div class="container">

    <h1 class="page-header"><?= Html::encode($this->title) ?></h1>

    <?php if (Yii::$app->session->hasFlash('form')) { ?>
        <div class="alert alert-success">
            Успешно обновлено.
        </div>
        <p>
            <a href="<?= \yii\helpers\Url::to(['cabinet-school-task-list/index', 'id' => $school->id]) ?>"
               class="btn btn-success">Все задачи</a>
            <a href="<?= \yii\helpers\Url::to(['cabinet-school-task-list/view', 'id' => Yii::$app->session->getFlash('form')]) ?>"
               class="btn btn-success">Эта задача</a>
        </p>

    <?php } else { ?>

        <?php $form = ActiveForm::begin([
            'id'      => 'contact-form',
            'options' => ['enctype' => 'multipart/form-data'],
        ]); ?>
        <?= $form->field($model, 'name')->label('Наименование') ?>
        <?= $form->field($model, 'category_id')
            ->label('Категория')
            ->dropDownList(
                ArrayHelper::map(
                    \common\models\task\Category::find()
                        ->orderBy(['sort_index' => SORT_ASC])
                        ->where(['school_id' => $school->id])
                        ->all(),
                    'id',
                    'name'
                ), ['prompt' => '- Не выбрано -']
            ) ?>
        <?= $form->field($model, 'content')->textarea(['rows' => 10]) ?>


        <hr>
        <div class="form-group">
            <?= Html::submitButton('Обновить', [
                'class' => 'btn btn-default',
                'name'  => 'contact-button',
                'style' => 'width:100%',
            ]) ?>
        </div>
        <?php ActiveForm::end(); ?>

    <?php } ?>

</div>

