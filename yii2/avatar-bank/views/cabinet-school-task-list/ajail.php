<?php

/** @var $this \yii\web\View */

/** @var $school \common\models\school\School */
/** @var $category null | \common\models\task\Category */
/** @var $author null | \common\models\UserAvatar */
/** @var $executor null | \common\models\UserAvatar */

use yii\helpers\Url;
use app\services\GsssHtml;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use app\services\GetArticle\Collection;

$this->title = 'Все задачи';

$statusList = \common\models\task\Status::find()
    ->where(['school_id' => $school->id])
    ->orderBy(['sort_index' => SORT_ASC])
    ->select('id')
    ->column();

\avatar\assets\Notify::register($this);
\common\assets\Clipboard\Asset::register($this);
\common\assets\iLightBox\Asset::register($this);
//\common\assets\AutoComplete\Asset::register($this);
\iAvatar777\widgets\FileUpload8\Asset::register($this);
\iAvatar777\services\FormAjax\Asset::register($this);

$cloud1 = $school->getCloud();
if (is_null($cloud1)) {
    $urlCloud = Yii::$app->AvatarCloud->url;
} else {
    $urlCloud = $cloud1->url;
}

?>
<?php
$rows1Query = \common\models\task\Task::find()
    ->where(['school_id' => $school->id])
    ->andWhere(['or', ['status' => null], ['status' => 0]])
    ->andWhere(['is_hide' => 0])
    ->orderBy(['sort_index' => SORT_ASC]);

$rows2Query = \common\models\task\Task::find()
    ->where(['school_id' => $school->id, 'status' => $statusList[0]])
    ->andWhere(['is_hide' => 0])
    ->orderBy(['sort_index' => SORT_ASC]) ;

$rows3Query = \common\models\task\Task::find()
    ->where(['school_id' => $school->id, 'status' => $statusList[1]])
    ->andWhere(['is_hide' => 0])
    ->orderBy(['sort_index' => SORT_ASC]);

if (!is_null($category)) {
    $rows1Query->andWhere(['category_id' => $category->id]);
    $rows2Query->andWhere(['category_id' => $category->id]);
    $rows3Query->andWhere(['category_id' => $category->id]);
}
if (!is_null($executor)) {
    $rows1Query->andWhere(['executer_id' => $executor->id]);
    $rows2Query->andWhere(['executer_id' => $executor->id]);
    $rows3Query->andWhere(['executer_id' => $executor->id]);
}
if (!is_null($author)) {
    $rows1Query->andWhere(['user_id' => $author->id]);
    $rows2Query->andWhere(['user_id' => $author->id]);
    $rows3Query->andWhere(['user_id' => $author->id]);
}

$rows1 = $rows1Query->all();
$rows2 = $rows2Query->all();
$rows3 = $rows3Query->all();

$executorList = [];
/** @var \common\models\task\Task $i */
foreach ($rows1 as $i) {
    if (!\cs\Application::isEmpty($i->executer_id)) {
        if (!in_array($i->executer_id, $executorList)) $executorList[] = $i->executer_id;
    }
}
foreach ($rows2 as $i) {
    if (!\cs\Application::isEmpty($i->executer_id)) {
        if (!in_array($i->executer_id, $executorList)) $executorList[] = $i->executer_id;
    }
}
foreach ($rows3 as $i) {
    if (!\cs\Application::isEmpty($i->executer_id)) {
        if (!in_array($i->executer_id, $executorList)) $executorList[] = $i->executer_id;
    }
}
?>



<div class="container">
    <h1 class="page-header"><?= Html::encode($this->title) ?></h1>

    <p>
        <a href="<?= Url::to(['cabinet-school-task-list/add', 'id' => $school->id]) ?>"
           class="btn btn-default">Добавить</a>
    </p>

    <div class="row">
    <div class="col-lg-4">
        <p>Категория</p>
        <?php
        $this->registerJs(<<<JS
$('#categoryItem').change(function(e) {
    var eid = $('#executorItem');
    var cid = $('#categoryItem');
    window.location = '/cabinet-school-task-list/ajail?id=' + {$school->id} + '&cid=' + cid.val() + '&eid=' + eid.val();
});
$('#executorItem').change(function(e) {
    var eid = $('#executorItem');
    var cid = $('#categoryItem');
    window.location = '/cabinet-school-task-list/ajail?id=' + {$school->id} + '&cid=' + cid.val() + '&eid=' + eid.val();
});
JS
)
        ?>
        <select class="form-control" id="categoryItem">
            <option value="">Ничего не выбрано</option>
            <?php foreach (\common\models\task\Category::find()->where(['school_id' => $school->id])->all() as $i) { ?>
                <?php if (!is_null($category)) { ?>
                    <?php if ($category->id == $i->id) { ?>
                        <option value="<?= $i->id ?>" selected><?= $i->name ?></option>
                    <?php } else { ?>
                        <option value="<?= $i->id ?>"><?= $i->name ?></option>
                    <?php } ?>

                <?php } else { ?>
                    <option value="<?= $i->id ?>"><?= $i->name ?></option>
                <?php } ?>
            <?php } ?>
        </select>
    </div>
        <div class="col-lg-4">
            <p>Постановщик</p>
            <?php
            $this->registerJs(<<<JS

JS
            )
            ?>
            <select class="form-control" id="authorItem">
                <option value="">Ничего не выбрано</option>
                <?php foreach (\common\models\task\Category::find()->where(['school_id' => $school->id])->all() as $i) { ?>
                    <?php if (!is_null($category)) { ?>
                        <?php if ($category->id == $i->id) { ?>
                            <option value="<?= $i->id ?>" selected><?= $i->name ?></option>
                        <?php } else { ?>
                            <option value="<?= $i->id ?>"><?= $i->name ?></option>
                        <?php } ?>

                    <?php } else { ?>
                        <option value="<?= $i->id ?>"><?= $i->name ?></option>
                    <?php } ?>
                <?php } ?>
            </select>
        </div>
    <div class="col-lg-4">
        <p>Исполняет</p>
        <?php
        $this->registerJs(<<<JS

JS
)
        ?>
        <select class="form-control" id="executorItem">
            <option value="">Ничего не выбрано</option>
            <?php /** @var int $i */ ?>
            <?php foreach ($executorList as $i) { ?>
                <?php $u  = \common\models\UserAvatar::findOne($i) ?>

                <?php if (!is_null($executor)) { ?>
                    <?php if ($executor->id == $i) { ?>
                        <option value="<?= $i ?>" selected><?= $u->getName2() ?></option>
                    <?php } else { ?>
                        <option value="<?= $i ?>"><?= $u->getName2() ?></option>
                    <?php } ?>

                <?php } else { ?>
                    <option value="<?= $i ?>"><?= $u->getName2() ?></option>
                <?php } ?>
            <?php } ?>
        </select>
    </div>

    </div>

    <?php
    $this->registerJS(<<<JS


JS
    );
    ?>
    <style>
        .container123 {
            margin: 0 auto;
        }

        .connectedSortable {
            /*border: 1px solid #eee;*/
            width: 330px;
            min-height: 20px;
            list-style-type: none;
            margin: 0;
            padding: 5px;
            float: left;
            margin-right: 10px;
        }

        .connectedSortable li {
            margin: 0 5px 5px 5px;
            padding: 5px;
            width: 310px;
            border: 1px solid #ccc;
            border-radius: 5px;
            background-color: #eeeeee;
        }

        .img-user
        {
            margin-top: 10px;
        }

        /*.ui-state-highlight { height: 1.2em; line-height: 1.2em; }*/
    </style>
    <?php
    $this->registerJsFile('https://code.jquery.com/ui/1.12.1/jquery-ui.js', ['depends' => ['\yii\web\JqueryAsset']]);
    $this->registerJs(<<<JS

// изменение статуса
$(".connectedSortable").on("sortreceive", function(event, ui) {
    console.log([event, ui, $(this)]);
    var item_id = $(ui.item[0]).data('id');
    var list_id = $(event.currentTarget).data('id');
    console.log([item_id,list_id]);
    
    return false;
    // ajaxJson({
    //     url:"/admin-task-list/set",
    //     data: {
    //         item_id:item_id,
    //         list_id:list_id
    //     },
    //     success: function(ret) {
    //        
    //     }
    // });
});
JS
    );

    ?>


    <table border="0" style="height: 50px;">
        <tr>
            <td style="width: 340px;font-weight: bold;">Не распределено</td>
            <td style="width: 340px;font-weight: bold;">Исполняется</td>
            <td style="width: 340px;font-weight: bold;">Проверяется</td>
        </tr>
    </table>

    <div class="row">
        <div class="container123">


            <?php
            $this->registerJs(<<<JS
$( ".sortable1, .sortable2, .sortable3" ).sortable({
    placeholder: "ui-state-highlight",
    connectWith: ".connectedSortable",
    change: function( event, ui ) {
        console.log(["change", event, ui]);
    },
    // изменение сортировки
    update: function( event, ui ) {
        // пересортировка
        console.log(["update", event, ui]);
        var item_id = $(ui.item[0]).data('id');
        var list_id = $(event.target).data('id');
        var idList = [];
        
        $('.connectedSortable[data-id="'+list_id+'"] li').each(function(i,o) {
            idList.push($(o).data('id'));
        });
        if ($.inArray(item_id, idList) >= 0) {
            ajaxJson({
                url: "/cabinet-school-task-list/sort",
                data: {
                    school_id: {$school->id},
                    task_id: item_id,
                    list_id: list_id,
                    list: idList
                },
                success: function(ret) {
                    new Noty({
                        timeout: 1000,
                        theme: 'sunset',
                        type: 'success',
                        layout: 'bottomLeft',
                        text: 'Сортировка обновлена'
                    }).show();
                },
                errorScript: function(ret) {
                    if (ret.id == 102) {
                        for (var key2 in ret.data) {
                            if (ret.data.hasOwnProperty(key2)) {
                                var name2 = key2;
                                var value2 = ret.data[key2];
                                new Noty({
                                    timeout: 1000,
                                    theme: 'sunset',
                                    type: 'warning',
                                    layout: 'bottomLeft',
                                    text: value2.join('')
                                }).show();
                            }
                        }
                        
                    }
                }
            });
        }
    }
});

var is_counter = 0;

function setTimer(i) {
    var mi = parseInt(i/60);
    var s = i - mi*60;
    
    var hi = parseInt(mi/60);
    var m = mi - hi*60;
    $('.timer_h').text(hi);

    var ss,ms,hs;
    
    if (s < 10) ss = '0' + s;
    else ss =  s;
    if (m < 10) ms = '0' + m;
    else ms =  m;

    $('.timer_s').text(ss);
    $('.timer_m').text(ms);

}

var functionOpen = function(html, task_id, school_id) {
    
    // комментарии
    html.find('.ilightbox').iLightBox();
    html.find('.buttonDelete').click(function(e) {
        if (confirm('Подтвердите удаление')) {
            ajaxJson({
                url: '/comments/delete',
                data: {
                    id: $(this).data('id')
                },
                success: function (e) {
                    window.location.reload(); 
                }
            });
        }
    });
    
    
    var formID = html.find('form').attr('id');
    console.log([formID, html.find('#comments form'), html.find('div#comments'), html.find('form'), html]);
    
    // Инициализация формы коментариев
    iAvatar777_ActiveForm.init(
        formID, 
        '#' + formID, 
        '/comments/add-ajax',
         function (ret) {
            console.log(1122);
            var selectorComments = '#comments';
            $(selectorComments).append(ret.html2);
            $(selectorComments + ' [name="Model[text]').val('');
            $(selectorComments + ' [name="Model[file]').val('');
            $(selectorComments + ' .field-model-file').find('.fileUploadedUrl').hide(); 
            console.log(ret);
        }, 
        2
    );
    
    // загрузка файла
    FileUpload8.init({
        "selector": "#w1",
        "server": "{$urlCloud}",
        "data":{
            "update":"[{\"function\":\"crop\",\"index\":\"crop\",\"options\":{\"width\":\"300\",\"height\":\"300\",\"mode\":\"MODE_THUMBNAIL_CUT\"}}]"
        },
        "controller":"upload",
        "accept":"*/*",
        "maxSize":20000,
        "functionSuccess":function (response) {
            // Вызываю AJAX для записи в school_file
            ajaxJson({
                url: '/cabinet/file-upload7-save',
                data: {
                    file: response.url,
                    school_id: {$school->id},
                    type_id: 24,
                    size: response.size // Размер файла в байтах
                },
                success: function (ret) {
                   
                }
            });
        }
    });
    
    // задачи
    html.find('.buttonEdit').click(function(e) {
        e.preventDefault();
        e.stopPropagation();
        window.location = '/cabinet-school-task-list/edit' + '?' + 'id' + '=' + $(this).data('id');
    });
    html.find('[data-toggle="tooltip"]').tooltip();
    html.find('.rowTable').click(function(e) {
        window.location = '/cabinet-school-task-list/view' + '?' + 'id' + '=' + $(this).data('id');
    });

    
    var clipboard = new Clipboard('.buttonCopy');
    clipboard.on('success', function(e) {
        new Noty({
            timeout: 1000,
            theme: 'sunset',
            type: 'success',
            layout: 'bottomLeft',
            text: 'Скопировано'
        }).show();
    });
    
    html.find('.buttonSessionStart').click(function(e) {
        ajaxJson({
            url: '/cabinet-school-task-list/session-start',
            data: {
                id: task_id
            },
            success: function(ret) {
                is_counter = 1;
                $('.buttonSessionStart').attr('disabled', 'disabled');
                $('.buttonSessionFinish').removeAttr('disabled');
            }
        });
    });
    
    html.find('.buttonSessionFinish').click(function(e) {
        ajaxJson({
            url: '/cabinet-school-task-list/session-finish',
            data: {
                id: task_id
            },
            success: function(ret) {
                is_counter = 0;
                $('.buttonSessionStart').removeAttr('disabled');
                $('.buttonSessionFinish').attr('disabled', 'disabled');
            }
        });
    });
    
    html.find('.buttonStart').click(function(e) {
        ajaxJson({
            url: '/cabinet-school-task-list/view-start',
            data: {
                id: $(this).data('id')
            },
            success: function(ret) {
                window.location.reload();
            }
        })
    });
    
    html.find('.inputExecutor').on('autocomplete.select', function(evt, item) {
        console.log([evt, item]); 
        $('#inputExecutor-value').val(item.value);
    });

    html.find('.buttonAddExecutor').click(function(e) {
        if ($('#inputExecutor-value').val() == '') {
            new Noty({
                timeout: 1000,
                theme: 'relax',
                type: 'warning',
                layout: 'bottomLeft',
                text: 'Вы должны выбрать сначала кого-то из поиска и списка'
            }).show();
            return;
        }
        ajaxJson({
            url: '/cabinet-school-task-list/add-executor',
            data: {
                id: $('#inputExecutor-value').val(),
                task_id: task_id
            },
            success: function(ret) {
                window.location.reload();
            }
        });
    });
    
    html.find('.buttonFinish').on('click', function(e) {
        ajaxJson({
            url: '/cabinet-school-task-list/view-finish',
            data: {
                id: $(this).data('id')
            },
            success: function(ret) {
                window.location.reload();
            }
        })
    });
    
    html.find('.buttonFinishReject').on('click', function(e) {
        if (confirm('Вы уверены?')) {
            ajaxJson({
                url: '/cabinet-school-task-list/view-finish-reject',
                data: {
                    id: $(this).data('id')
                },
                success: function(ret) {
                    window.location.reload();
                }
            })
        }
    });
    
    html.find('.buttonDone').on('click', function(e) {
        ajaxJson({
            url: '/cabinet-school-task-list/view-done',
            data: {
                id: $(this).data('id')
            },
            success: function(ret) {
                window.location.reload();
            }
        })
    });
    
    html.find('.buttonReject').on('click', function(e) {
        $('#modalInfo').modal();
    });
    
    html.find('.buttonRejectModal').on('click', function(e) {
        ajaxJson({
            url: '/cabinet-school-task-list/view-reject',
            data: {
                id: $(this).data('id'),
                text: $('#textarea').val()
            },
            success: function(ret) {
                window.location.reload();
            }
        })
    });
    
    html.find('.buttonChangePrice').on('click', function(e) {
        window.location = '/cabinet-school-task-list/change-price' + '?' + 'id' + '=' + $(this).data('id');
    });

};
var functionSuccessComments = function (response) {
    // Вызываю AJAX для записи в school_file
    ajaxJson({
        url: '/cabinet/file-upload7-save',
        data: {
            file: response.url,
            school_id: {$school->id},
            type_id: 24,
            size: response.size // Размер файла в байтах
        },
        success: function (ret) {
            
        }
    });
};


var is_set_timer = 0;
var timer;
$('.buttonOpen').click(function (e) {
    var id = $(this).data('id');
    ajaxJson({
        url: '/cabinet-school-task-list/view-ajax',
        data: {
            id: id
        },
        success: function (ret) {
            console.log(ret);
            $('#modalInfo .modal-title').html(ret.name);
            var html = $(ret.html);
            var task_id = id;
            var school_id = {$school->id};
            $('#modalInfo .modal-body').html(html);
            $('#modalInfo').modal();
            functionOpen(html, task_id, school_id);
            
            is_counter = ret.is_counter;
            setTimer(ret.time);
            
            if (is_counter) {
                $('.buttonSessionStart').attr('disabled', 'disabled');
                $('.buttonSessionFinish').removeAttr('disabled');
            }
            
            timer = ret.time;
            
            if (is_set_timer == 0) {
                setInterval(function(e) {
                    if (is_counter) {
                        timer++;
                        setTimer(timer);
                    }
                }, 1000);
                is_set_timer = 1;
            }
        }
    });
});

JS
            );
            ?>
            <ul class="connectedSortable sortable1" data-id="-1">
                <?php /** @var \common\models\task\Task $item */ ?>
                <?php foreach ($rows1 as $item) { ?>
                    <?= $this->render('i', ['item' => $item]) ?>
                <?php } ?>
            </ul>

            <ul class="connectedSortable sortable2" data-id="<?= $statusList[0] ?>">
                <?php /** @var \common\models\task\Task $item */ ?>
                <?php foreach ($rows2 as $item) { ?>
                    <?= $this->render('i', ['item' => $item]) ?>
                <?php } ?>
            </ul>

            <ul class="connectedSortable sortable3" data-id="<?= $statusList[1] ?>">
                <?php /** @var \common\models\task\Task $item */ ?>
                <?php foreach ($rows3 as $item) { ?>
                    <?= $this->render('i', ['item' => $item]) ?>
                <?php } ?>
            </ul>

        </div>
    </div>


</div>


<div class="modal fade bs-example-modal-lg" id="modalInfo" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="myModalLabel">Информация</h4>
            </div>
            <div class="modal-body">
                Успешно!
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
            </div>
        </div>
    </div>
</div>