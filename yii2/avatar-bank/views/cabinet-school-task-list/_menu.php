<?php
use \yii\helpers\Url;

/** @var $school \common\models\school\School */
?>
<p>
    <a href="<?= Url::to(['cabinet-school-task-list/add', 'id' => $school->id]) ?>" class="btn btn-default">Добавить</a>
    <a href="<?= Url::to(['cabinet-school-task-list/ajail', 'id' => $school->id]) ?>" class="btn btn-default">Статусы</a>
    <a href="<?= Url::to(['cabinet-school-task-list/project-manager-list', 'id' => $school->id]) ?>"
       class="btn btn-default">Руководители проектов</a>
    <a href="<?= Url::to(['cabinet-school-task-list-category/index', 'id' => $school->id]) ?>"
       class="btn btn-default">Категории</a>
    <a href="<?= Url::to(['cabinet-school-task-list-command/index', 'id' => $school->id]) ?>"
       class="btn btn-default">Команда</a>
    <a href="<?= Url::to(['cabinet-school-task-list-table/index', 'id' => $school->id]) ?>"
       class="btn btn-default">Доски</a>
</p>
