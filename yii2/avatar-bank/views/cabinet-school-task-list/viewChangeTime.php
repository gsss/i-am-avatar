<?php
/** @var $this yii\web\View */
/** @var $model \common\models\task\Task */
/** @var $school \common\models\school\School */

use common\models\task\Session;

?>

<?php
if ( $session = Session::find()->where(['task_id' => $model->id])->exists()) {
    /** @var Session $session */
    $session = Session::find()
        ->where(['task_id' => $model->id, 'is_finish' => 0])
        ->one();
    // если не найдены не закрытые сессии, то все закрыты и счетчик остановлен
    if (is_null($session)) {
        $all = Session::find()
            ->where([
                'task_id' => $model->id,
            ])
            ->select([
                'sum(finish - start) as ss',
            ])
            ->scalar();
        if ($all === false) $all = 0;
    } else {
        $all = Session::find()
            ->where(['task_id' => $model->id, 'is_finish' => 1])
            ->select([
                'sum(finish - start) as ss',
            ])
            ->scalar();

        $all += time() - $session->start;
    }
} else {
    $all = 0;
}


$hour = (int)($all / (60*60));
$min = (int)(($all - $hour*60*60) / 60);
$sec = $all - $hour*60*60 - $min*60;

$item = \common\models\school\CommandLink::find()->where(['user_id' =>  $model->executer_id, 'school_id' => $school->id])->one();
if (is_null($item)) {
    $price = null;
} else {
    $price =  \common\models\piramida\Currency::getValueFromAtom($item->hour_price, \common\models\piramida\Currency::RUB);
}

$this->registerJs(<<<JS
$('.buttonChangeTimeAndAccept').on('click', function(e) {
    ajaxJson({
        url: '/cabinet-school-task-list/view-done-change-time',
        data: {
            id: $(this).data('id'),
            hour: $('#text-time-hour').val(),
            min: $('#text-time-min').val()
        },
        success: function(ret) {
            window.location.reload();
        }
    })
});
JS
)
?>
<div class="modal fade" id="modalChangeTime" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="myModalLabel">Изменить время и принять задачу</h4>
            </div>
            <div class="modal-body">
                <p>Трудочас = <?= $price ?></p>
                <p>Час</p>
                <p><input class="form-control" id="text-time-hour" value="<?= $hour ?>"></p>
                <p>Мин</p>
                <p><input class="form-control" id="text-time-min" value="<?= $min ?>"></p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary buttonChangeTimeAndAccept" data-id="<?= $model->id ?>">Изменить время и принять задачу</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
            </div>
        </div>
    </div>
</div>

