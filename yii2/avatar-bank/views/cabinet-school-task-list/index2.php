<?php

/** @var $this \yii\web\View */

/** @var $school \common\models\school\School */

use yii\helpers\Url;
use app\services\GsssHtml;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use app\services\GetArticle\Collection;
use kartik\grid\GridView;

$this->title = 'Все задачи';

$statusList = \common\models\task\Status::find()
    ->where(['school_id' => $school->id])
    ->orderBy(['sort_index' => SORT_ASC])
    ->select('id')
    ->column();
if (count($statusList) == 0) {
    throw new  Exception('count($statusList) == 0');
}

Yii::$app->session->set('$statusList', $statusList);

$isProjectManager = \common\models\school\ProjectManagerLink::find()->where(['user_id' => Yii::$app->user->id, 'school_id' => $school->id])->exists();
$isAdmin = \common\models\school\AdminLink::find()->where(['user_id' => Yii::$app->user->id, 'school_id' => $school->id])->exists();
?>

<div class="container" style="padding-bottom: 30px;">

    <h1 class="page-header"><?= Html::encode($this->title) ?></h1>
    <p>
        <a class="btn-default btn" href="<?= Url::to(['cabinet-school-task-list/add', 'id' => $school->id]) ?>">Добавить</a>
        <?php
        $this->registerJs(<<<JS

$('.buttonExport').click(function(e) {
    var url;
    window.open('/cabinet-school-task-list/export?id='+{$school->id}+'&search=' + encodeURIComponent(window.location.search));
    
});
JS
)
        ?>
        <button class="btn-default btn buttonExport">Экспорт</button>
    </p>

</div>
<?= $this->render('index2-list', [
        'school' => $school,
        'isp'    => $isProjectManager,
        'isa'    => $isAdmin,
    ]) ?>

<div class="modal fade" id="modalInfo" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="myModalLabel">Информация</h4>
            </div>
            <div class="modal-body">
                Успешно!
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
            </div>
        </div>
    </div>
</div>