<?php

/** @var $this \yii\web\View */
/** @var $school \common\models\school\School */
/** @var $isp  bool  is PM */

/** @var $isa  bool  is Admin */

use yii\helpers\Url;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use kartik\grid\GridView;

$statusList = \common\models\task\Status::find()
    ->where(['school_id' => $school->id])
    ->orderBy(['sort_index' => SORT_ASC])
    ->select('id')
    ->column();

Yii::$app->session->set('$statusList', $statusList);

?>

<?php
$sort = new \yii\data\Sort([
    'attributes'   => [
        'id'                => ['label' => 'ID'],
        'status'            => [
            'label' => 'Прогресс',
            'asc'   => ['status' => SORT_ASC, 'category_id' => SORT_ASC],
            'desc'  => ['status' => SORT_DESC, 'category_id' => SORT_DESC],
        ],
        'price'             => [
            'label'   => 'Награда',
            'default' => SORT_DESC,
        ],
        'created_at'        => ['label' => 'Созд', 'default' => SORT_DESC],
        'name'              => ['label' => 'Наименование'],
        'last_comment_time' => ['label' => 'Обн', 'default' => SORT_DESC],
        'category_id'       => ['label' => 'category_id'],
        'currency_id '      => ['label' => 'Валюта'],
    ],
    'defaultOrder' => [
        'category_id' => SORT_ASC,
        'last_comment_time' => SORT_DESC,
    ],
]);
$model = new \avatar\models\search\Task();
$provider = $model->search($sort, Yii::$app->request->get(), $statusList, [
    'school_task.school_id' => $school->id,
    'is_hide'               => 0,
]);

/** @var \yii\db\Query $q */
$q = $provider->query;
?>

<?php
$this->registerJs(<<<JS
$('[data-toggle="tooltip"]').tooltip();
JS
);
$columns = require(Yii::getAlias('@avatar/views/cabinet-school-task-list/index-list-columns.php'));

if ($isp || $isa) {
    $this->registerJs(<<<JS
$('.buttonHide').click(function (e) {
    e.preventDefault();
    e.stopPropagation();
    if (confirm('Подтвердите скрытие')) {
        var button = $(this);
        var id = $(this).data('id');
        ajaxJson({
            url: '/cabinet-school-task-list/hide' + '?' + 'id' + '=' + id,
            success: function (ret) {
                button.parent().parent().remove();
            }
        });
    }
});
$('.buttonDone').click( function(e) {
    var button = $(this);
    ajaxJson({
        url: '/cabinet-school-task-list/view-done',
        data: {
            id: $(this).data('id')
        },
        success: function(ret) {
            button.parent().parent().parent().parent().remove();
        }
    })
});
$('.buttonFinish').on('click', function(e) {
    ajaxJson({
        url: '/cabinet-school-task-list/view-finish',
        data: {
            id: $(this).data('id')
        },
        success: function(ret) {
            window.location.reload();
        }
    })
});
$('.buttonStart').click(function(e) {
    ajaxJson({
        url: '/cabinet-school-task-list/view-start',
        data: {
            id: $(this).data('id')
        },
        success: function(ret) {
            window.location.reload();
        }
    })
});
JS
    );
    $columns[] = [
        'header'  => 'Ред',
        'content' => function ($item) {
            return Html::a('Ред', ['cabinet-school-task-list/edit', 'id' => $item['id']], ['class' => 'btn btn-primary btn-xs buttonEdit', 'data' => ['id' => $item['id'], 'pjax' => 0]]);
        },
    ];

    $columns[] = [
        'header'  => Html::tag('span', null, ['class' => 'glyphicon glyphicon-eye-close', 'data' => ['toggle' => 'tooltip'], 'title' => 'Скрыть задачу']),
        'content' => function ($item) {
            return Html::button(Html::tag('span', null, ['class' => 'glyphicon glyphicon-eye-close']), ['class' => 'btn btn-default btn-xs buttonHide', 'data' => ['id' => $item['id'], 'toggle' => 'tooltip'], 'title' => 'Скрыть задачу']);
        },
    ];
}
$isFa = true;
$pdfHeader = '';
$pdfFooter = '';
$title = '$title';
// $isFa below determines if export['fontAwesome'] property is set to true.
$defaultExportConfig = [
    \kartik\grid\GridView::HTML  => [
        'label'           => Yii::t('kvgrid', 'HTML'),
        'icon'            => $isFa ? 'file-text' : 'floppy-saved',
        'iconOptions'     => ['class' => 'text-info'],
        'showHeader'      => true,
        'showPageSummary' => true,
        'showFooter'      => true,
        'showCaption'     => true,
        'filename'        => Yii::t('kvgrid', 'grid-export'),
        'alertMsg'        => Yii::t('kvgrid', 'The HTML export file will be generated for download.'),
        'options'         => ['title' => Yii::t('kvgrid', 'Hyper Text Markup Language')],
        'mime'            => 'text/html',
        'config'          => [
            'cssFile' => 'https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css',
        ],
    ],
    \kartik\grid\GridView::CSV   => [
        'label'           => Yii::t('kvgrid', 'CSV'),
        'icon'            => $isFa ? 'file-code-o' : 'floppy-open',
        'iconOptions'     => ['class' => 'text-primary'],
        'showHeader'      => true,
        'showPageSummary' => true,
        'showFooter'      => true,
        'showCaption'     => true,
        'filename'        => Yii::t('kvgrid', 'grid-export'),
        'alertMsg'        => Yii::t('kvgrid', 'The CSV export file will be generated for download.'),
        'options'         => ['title' => Yii::t('kvgrid', 'Comma Separated Values')],
        'mime'            => 'application/csv',
        'config'          => [
            'colDelimiter' => ",",
            'rowDelimiter' => "\r\n",
        ],
    ],
    \kartik\grid\GridView::TEXT  => [
        'label'           => Yii::t('kvgrid', 'Text'),
        'icon'            => $isFa ? 'file-text-o' : 'floppy-save',
        'iconOptions'     => ['class' => 'text-muted'],
        'showHeader'      => true,
        'showPageSummary' => true,
        'showFooter'      => true,
        'showCaption'     => true,
        'filename'        => Yii::t('kvgrid', 'grid-export'),
        'alertMsg'        => Yii::t('kvgrid', 'The TEXT export file will be generated for download.'),
        'options'         => ['title' => Yii::t('kvgrid', 'Tab Delimited Text')],
        'mime'            => 'text/plain',
        'config'          => [
            'colDelimiter' => "\t",
            'rowDelimiter' => "\r\n",
        ],
    ],
    \kartik\grid\GridView::EXCEL => [
        'label'           => Yii::t('kvgrid', 'Excel'),
        'icon'            => $isFa ? 'file-excel-o' : 'floppy-remove',
        'iconOptions'     => ['class' => 'text-success'],
        'showHeader'      => true,
        'showPageSummary' => true,
        'showFooter'      => true,
        'showCaption'     => true,
        'filename'        => Yii::t('kvgrid', 'grid-export'),
        'alertMsg'        => Yii::t('kvgrid', 'The EXCEL export file will be generated for download.'),
        'options'         => ['title' => Yii::t('kvgrid', 'Microsoft Excel 95+')],
        'mime'            => 'application/vnd.ms-excel',
        'config'          => [
            'worksheet' => Yii::t('kvgrid', 'ExportWorksheet'),
            'cssFile'   => '',
        ],
    ],
//    \kartik\grid\GridView::PDF   => [
//        'label'           => Yii::t('kvgrid', 'PDF'),
//        'icon'            => $isFa ? 'file-pdf-o' : 'floppy-disk',
//        'iconOptions'     => ['class' => 'text-danger'],
//        'showHeader'      => true,
//        'showPageSummary' => true,
//        'showFooter'      => true,
//        'showCaption'     => true,
//        'filename'        => Yii::t('kvgrid', 'grid-export'),
//        'alertMsg'        => Yii::t('kvgrid', 'The PDF export file will be generated for download.'),
//        'options'         => ['title' => Yii::t('kvgrid', 'Portable Document Format')],
//        'mime'            => 'application/pdf',
//        'config'          => [
//            'mode'          => 'c',
//            'format'        => 'A4-L',
//            'destination'   => 'D',
//            'marginTop'     => 20,
//            'marginBottom'  => 20,
//            'cssInline'     => '.kv-wrap{padding:20px;}' .
//                '.kv-align-center{text-align:center;}' .
//                '.kv-align-left{text-align:left;}' .
//                '.kv-align-right{text-align:right;}' .
//                '.kv-align-top{vertical-align:top!important;}' .
//                '.kv-align-bottom{vertical-align:bottom!important;}' .
//                '.kv-align-middle{vertical-align:middle!important;}' .
//                '.kv-page-summary{border-top:4px double #ddd;font-weight: bold;}' .
//                '.kv-table-footer{border-top:4px double #ddd;font-weight: bold;}' .
//                '.kv-table-caption{font-size:1.5em;padding:8px;border:1px solid #ddd;border-bottom:none;}',
//            'methods'       => [
//                'SetHeader' => [
//                    ['odd' => $pdfHeader, 'even' => $pdfHeader],
//                ],
//                'SetFooter' => [
//                    ['odd' => $pdfFooter, 'even' => $pdfFooter],
//                ],
//            ],
//            'options'       => [
//                'title'    => $title,
//                'subject'  => Yii::t('kvgrid', 'PDF export generated by kartik-v/yii2-grid extension'),
//                'keywords' => Yii::t('kvgrid', 'krajee, grid, export, yii2-grid, pdf'),
//            ],
//            'contentBefore' => '',
//            'contentAfter'  => '',
//        ],
//    ],
    \kartik\grid\GridView::JSON  => [
        'label'           => Yii::t('kvgrid', 'JSON'),
        'icon'            => $isFa ? 'file-code-o' : 'floppy-open',
        'iconOptions'     => ['class' => 'text-warning'],
        'showHeader'      => true,
        'showPageSummary' => true,
        'showFooter'      => true,
        'showCaption'     => true,
        'filename'        => Yii::t('kvgrid', 'grid-export'),
        'alertMsg'        => Yii::t('kvgrid', 'The JSON export file will be generated for download.'),
        'options'         => ['title' => Yii::t('kvgrid', 'JavaScript Object Notation')],
        'mime'            => 'application/json',
        'config'          => [
            'colHeads'     => [],
            'slugColHeads' => false,
            'jsonReplacer' => null,
            'indentSpace'  => 4,
        ],
    ],
];


if (Yii::$app->deviceDetect->isMobile()) {
    $options = [
        'margin-top'    => '0px',
        'margin-bottom' => '200px',
        'margin-left'   => '10px',
        'margin-right'  => '10px',
    ];
} else {
    $options = [
        'margin-top'    => '0px',
        'margin-bottom' => '200px',
        'margin-left'   => '100px',
        'margin-right'  => '100px',
    ];
}
?>
<?=
GridView::widget([
    'dataProvider'        => $provider,
    'filterModel'         => $model,
    'showPageSummary'     => true,
    'pjax'                => true,
    'options'             => [
        'style' => Html::cssStyleFromArray($options),
    ],
    'toolbar'             => [
        '{export}',
        '{toggleData}',
    ],
    'pjaxSettings'        => [
        'beforeGrid' => <<< HTML
            
            

<script>
$('[data-toggle="tooltip"]').tooltip();

$('.buttonHide').click(function (e) {
    e.preventDefault();
    e.stopPropagation();
    if (confirm('Подтвердите скрытие')) {
        var button = $(this);
        var id = $(this).data('id');
        ajaxJson({
            url: '/cabinet-school-task-list/hide' + '?' + 'id' + '=' + id,
            success: function (ret) {
                window.location.reload();
            }
        });
    }
});
$('.buttonDone').click( function(e) {
    var button = $(this);
    ajaxJson({
        url: '/cabinet-school-task-list/view-done',
        data: {
            id: $(this).data('id')
        },
        success: function(ret) {
            button.parent().parent().parent().parent().remove();
        }
    })
});
$('.buttonStart').click(function(e) {
    ajaxJson({
        url: '/cabinet-school-task-list/view-start',
        data: {
            id: $(this).data('id')
        },
        success: function(ret) {
            window.location.reload();
        }
    })
});
</script>
HTML
    ,

    ],
    'striped'             => true,
    'hover'               => true,
    'panel'               => ['type' => 'default', 'heading' => 'Задачи'],
    'toggleDataContainer' => ['class' => 'btn-group mr-2'],
    'rowOptions'          => function ($item) {
        return [
            'data'  => ['id' => $item['id']],
            'class' => 'rowTable',
        ];
    },
    'columns'             => $columns,
    'exportConfig'        => $defaultExportConfig,
]);
?>