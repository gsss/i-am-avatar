<?php

/** @var $this \yii\web\View */

/** @var $school \common\models\school\School */
/** @var $category null | \common\models\task\Category */
/** @var $author null | \common\models\UserAvatar */
/** @var $executor null | \common\models\UserAvatar */

use yii\helpers\Url;
use app\services\GsssHtml;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use app\services\GetArticle\Collection;

$this->title = 'Все задачи';

$statusList = \common\models\task\Status::find()
    ->where(['school_id' => $school->id])
    ->orderBy(['sort_index' => SORT_ASC])
    ->select('id')
    ->column();

\common\assets\Jqwidgets::register($this);
\avatar\assets\Notify::register($this);
\common\assets\Clipboard\Asset::register($this);
\common\assets\iLightBox\Asset::register($this);
//\common\assets\AutoComplete\Asset::register($this);
\iAvatar777\widgets\FileUpload8\Asset::register($this);
\iAvatar777\services\FormAjax\Asset::register($this);


$this->registerJs(<<<JS
var fields = [
         { name: "id", type: "string" },
         { name: "status", map: "state", type: "string" },
         { name: "text", map: "label", type: "string" },
         { name: "tags", type: "string" },
         { name: "color", map: "hex", type: "string" },
         { name: "resourceId", type: "number" }
];
var source =
 {
     localData: [
          { id: "1161", state: "new", label: "Combine Orders", tags: "orders, combine", hex: "#5dc3f0", resourceId: 3 },
          { id: "1645", state: "work", label: "Change Billing Address", tags: "billing", hex: "#f19b60", resourceId: 1 },
          { id: "9213", state: "new", label: "One item added to the cart", tags: "cart", hex: "#5dc3f0", resourceId: 3 },
          { id: "6546", state: "done", label: "Edit Item Price", tags: "price, edit", hex: "#5dc3f0", resourceId: 4 },
          { id: "9034", state: "new", label: "Login 404 issue", tags: "issue, login", hex: "#6bbd49" }
     ],
     dataType: "array",
     dataFields: fields
 };
var dataAdapter = new $.jqx.dataAdapter(source);
var resourcesAdapterFunc = function () {
    var resourcesSource =
    {
        localData: [
              { id: 0, name: "No name", image: "../../jqwidgets/styles/images/common.png", common: true },
              { id: 1, name: "Andrew Fuller", image: "../../images/andrew.png" },
              { id: 2, name: "Janet Leverling", image: "../../images/janet.png" },
              { id: 3, name: "Steven Buchanan", image: "../../images/steven.png" },
              { id: 4, name: "Nancy Davolio", image: "../../images/nancy.png" },
              { id: 5, name: "Michael Buchanan", image: "../../images/Michael.png" },
              { id: 6, name: "Margaret Buchanan", image: "../../images/margaret.png" },
              { id: 7, name: "Robert Buchanan", image: "../../images/robert.png" },
              { id: 8, name: "Laura Buchanan", image: "../../images/Laura.png" },
              { id: 9, name: "Laura Buchanan", image: "../../images/Anne.png" }
        ],
        dataType: "array",
        dataFields: [
             { name: "id", type: "number" },
             { name: "name", type: "string" },
             { name: "image", type: "string" },
             { name: "common", type: "boolean" }
        ]
    };
    var resourcesDataAdapter = new $.jqx.dataAdapter(resourcesSource);
    return resourcesDataAdapter;
}
$('#kanban').jqxKanban({
    resources: resourcesAdapterFunc(),
    source: dataAdapter,
    columns: [
        { text: "Не распределены", dataField: "new" },
        { text: "В работе", dataField: "work" },
        { text: "Сделаны", dataField: "done" }
    ]
}); 
JS
)
?>




<div class="container">
    <h1 class="page-header"><?= Html::encode($this->title) ?></h1>

    <p>
        <a href="<?= Url::to(['cabinet-school-task-list/add', 'id' => $school->id]) ?>"
           class="btn btn-default">Добавить</a>
    </p>

    <div class="row">
        <div id='kanban'>
        </div>
    </div>
</div>


<div class="modal fade bs-example-modal-lg" id="modalInfo" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="myModalLabel">Информация</h4>
            </div>
            <div class="modal-body">
                Успешно!
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
            </div>
        </div>
    </div>
</div>