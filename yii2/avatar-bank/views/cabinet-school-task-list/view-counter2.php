<?php

/** @var $model \common\models\task\Task */

use common\models\task\Session;

?>
<?php
if ( $session = Session::find()->where(['task_id' => $model->id])->exists()) {
    /** @var Session $session */
    $session = Session::find()
        ->where(['task_id' => $model->id, 'is_finish' => 0])
        ->one();
    // если не найдены не закрытые сессии, то все закрыты и счетчик остановлен
    if (is_null($session)) {
        $all = Session::find()
            ->where([
                'task_id' => $model->id,
            ])
            ->select([
                'sum(finish - start) as ss',
            ])
            ->scalar();
        if ($all === false) $all = 0;
    } else {
        $all = Session::find()
            ->where(['task_id' => $model->id, 'is_finish' => 1])
            ->select([
                'sum(finish - start) as ss',
            ])
            ->scalar();

        $all += time() - $session->start;
    }
} else {
    $all = 0;
}


$hour = (int)($all / (60*60));
$min = (int)(($all - $hour*60*60) / 60);
if ($min < 10) $min = '0'. $min;
$sec = $all - $hour*60*60 - $min*60;
if ($sec < 10) $sec = '0'. $sec;
?>

<p><?= $hour ?>:<?= $min ?>:<?= $sec ?></p>
<hr>
