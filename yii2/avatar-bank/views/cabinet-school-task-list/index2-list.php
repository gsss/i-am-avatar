<?php

/** @var $this \yii\web\View */
/** @var $school \common\models\school\School */
/** @var $isp  bool  is PM */

/** @var $isa  bool  is Admin */

use yii\helpers\Url;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use kartik\grid\GridView;

$statusList = \common\models\task\Status::find()
    ->where(['school_id' => $school->id])
    ->orderBy(['sort_index' => SORT_ASC])
    ->select('id')
    ->column();

Yii::$app->session->set('$statusList', $statusList);

?>

<?php
$sort = new \yii\data\Sort([
    'attributes'   => [
        'id'                => ['label' => 'ID'],
        'status'            => [
            'label' => 'Прогресс',
            'asc'   => ['status' => SORT_ASC, 'category_id' => SORT_ASC],
            'desc'  => ['status' => SORT_DESC, 'category_id' => SORT_DESC],
        ],
        'price'             => [
            'label'   => 'Награда',
            'default' => SORT_DESC,
        ],
        'created_at'        => ['label' => 'Созд', 'default' => SORT_DESC],
        'name'              => ['label' => 'Наименование'],
        'last_comment_time' => ['label' => 'Обн', 'default' => SORT_DESC],
        'category_id'       => ['label' => 'category_id'],
        'currency_id '      => ['label' => 'Валюта'],
    ],
    'defaultOrder' => [
        'last_comment_time' => SORT_DESC,
        'created_at' => SORT_DESC,
    ],
]);
$model = new \avatar\models\search\Task();
$provider = $model->search($sort, Yii::$app->request->get(), $statusList, [
    'school_task.school_id' => $school->id,
    'is_hide'               => 0,
    'school_task.parent_id' => null,
]);
$sum = '';
if (!\cs\Application::isEmpty($school->wallet_id)) {
    $provider2 = $model->search($sort, Yii::$app->request->get(), $statusList, [
        'school_task.school_id' => $school->id,
        'is_hide'               => 0,
    ]);
    /** @var \yii\db\Query $q */
    $q = $provider2->query;
    $n = $q->select(['sum(price)'])->scalar();
    $w = \common\models\piramida\Wallet::findOne($school->wallet_id);
    $c = \common\models\piramida\Currency::findOne($w->currency_id);
    $sum = Yii::$app->formatter->asDecimal($n / pow(10, $c->decimals), $c->decimals) . ' ' . $c->code;

}
?>
    <p>Итого: <?= $sum ?></p>
<?php
$this->registerJs(<<<JS
$('[data-toggle="tooltip"]').tooltip();
$('.rowTable').click(function(e) {
});

JS
);
$columns = require('index2-list-columns.php');

if ($isp || $isa) {
    $this->registerJs(<<<JS
$('.buttonHide').click(function (e) {
    e.preventDefault();
    e.stopPropagation();
    if (confirm('Подтвердите скрытие')) {
        var button = $(this);
        var id = $(this).data('id');
        ajaxJson({
            url: '/cabinet-school-task-list/hide' + '?' + 'id' + '=' + id,
            success: function (ret) {
                button.parent().parent().remove();
            }
        });
    }
});
$('.buttonDone').click( function(e) {
    var button = $(this);
    ajaxJson({
        url: '/cabinet-school-task-list/view-done',
        data: {
            id: $(this).data('id')
        },
        success: function(ret) {
            button.parent().parent().parent().parent().remove();
        }
    })
});
$('.buttonFinish').on('click', function(e) {
    ajaxJson({
        url: '/cabinet-school-task-list/view-finish',
        data: {
            id: $(this).data('id')
        },
        success: function(ret) {
            window.location.reload();
        }
    })
});
$('.buttonStart').click(function(e) {
    ajaxJson({
        url: '/cabinet-school-task-list/view-start',
        data: {
            id: $(this).data('id')
        },
        success: function(ret) {
            window.location.reload();
        }
    })
});
JS
    );
    $columns[] = [
        'header'  => 'Ред',
        'content' => function ($item) {
            return Html::a('Ред', ['cabinet-school-task-list/edit', 'id' => $item['id']], ['class' => 'btn btn-primary btn-xs buttonEdit', 'data' => ['id' => $item['id'], 'pjax' => 0]]);
        },
    ];

    $columns[] = [
        'header'  => Html::tag('span', null, ['class' => 'glyphicon glyphicon-eye-close', 'data' => ['toggle' => 'tooltip'], 'title' => 'Скрыть задачу']),
        'content' => function ($item) {
            return Html::button(Html::tag('span', null, ['class' => 'glyphicon glyphicon-eye-close']), ['class' => 'btn btn-default btn-xs buttonHide', 'data' => ['id' => $item['id'], 'toggle' => 'tooltip'], 'title' => 'Скрыть задачу']);
        },
    ];
}
$isFa = true;
$pdfHeader = '';
$pdfFooter = '';
$title = '$title';
// $isFa below determines if export['fontAwesome'] property is set to true.


if (Yii::$app->deviceDetect->isMobile()) {
    $options = [
        'margin-top'    => '0px',
        'margin-bottom' => '200px',
        'margin-left'   => '10px',
        'margin-right'  => '10px',
    ];
} else {
    $options = [
        'margin-top'    => '0px',
        'margin-bottom' => '200px',
        'margin-left'   => '100px',
        'margin-right'  => '100px',
    ];
}
?>
<?=
\yii\grid\GridView::widget([
    'dataProvider'        => $provider,
    'filterModel'         => $model,
    'options'             => [
        'style' => Html::cssStyleFromArray($options),
    ],
    'tableOptions'          => [
        'class' => 'table table-striped table-hover'
    ],
    'rowOptions'          => function ($item) {
        return [
            'data'  => ['id' => $item['id']],
            'class' => 'rowTable',
        ];
    },
    'columns'             => $columns,
]);
?>