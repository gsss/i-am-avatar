<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;
use yii\db\Query;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \common\models\task\Task */
/* @var $school \common\models\school\School */

$this->title = 'Добавить задачу';

$currencyList = [];
$currencySchool = $school->currency_id;
if (!is_null($currencySchool)) {
    $currencyList[$currencySchool] = \common\models\piramida\Currency::findOne($currencySchool)->code;
}
$fundRub = $school->fund_rub2;
if (!is_null($fundRub)) {
    $currencyList[7] = 'RUB';
}
?>
<div class="container">

    <h1 class="page-header"><?= Html::encode($this->title) ?></h1>


    <?php if (!is_null($id = Yii::$app->session->getFlash('form'))) : ?>
        <div class="alert alert-success">
            Успешно добавлено.
        </div>
        <p>
            <a href="<?= \yii\helpers\Url::to(['cabinet-school-task-list/index', 'id' => $school->id]) ?>" class="btn btn-success">Все задачи</a>
            <a href="<?= \yii\helpers\Url::to(['cabinet-school-task-list/view', 'id' => Yii::$app->session->getFlash('form')]) ?>" class="btn btn-success">Эта задача</a>
        </p>
    <?php else: ?>
        <?php $form = ActiveForm::begin([
            'options' => ['enctype' => 'multipart/form-data'],
        ]); ?>
        <?= $form->field($model, 'name')->label('Наименование') ?>
        <?= $form->field($model, 'price')->hint('Кол-во монет')->label('Награда') ?>
        <?= $form->field($model, 'currency_id')->label('Валюта')->dropDownList($currencyList) ?>
        <?= $form->field($model, 'content')->textarea(['rows' => 10])->label('Содержание задачи и критерии исполнения') ?>


        <hr>
        <div class="form-group">
            <?= Html::submitButton('Добавить', [
                'class' => 'btn btn-default',
                'name'  => 'contact-button',
                'style' => 'width:100%',
            ]) ?>
        </div>
        <?php ActiveForm::end(); ?>
    <?php endif; ?>
</div>
