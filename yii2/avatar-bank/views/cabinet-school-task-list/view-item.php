<?php

use common\models\task\Session;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;
use yii\db\Query;
use yii\helpers\ArrayHelper;

/** @var $this yii\web\View */
/** @var $model \common\models\task\Task */
/** @var $school \common\models\school\School */

$isAuthor = $model->user_id ==  Yii::$app->user->id;
$isProjectManager = \common\models\school\ProjectManagerLink::find()->where(['user_id' => Yii::$app->user->id, 'school_id' => $school->id])->exists();
$isAdmin = \common\models\school\AdminLink::find()->where(['user_id' => Yii::$app->user->id, 'school_id' => $school->id])->exists();

$statusList = \common\models\task\Status::find()
    ->where(['school_id' => $school->id])
    ->orderBy(['sort_index' => SORT_ASC])
    ->select('id')
    ->column();


try {
    $Executer = $model->getExecuter();
} catch (Exception $e) {
    $Executer = null;
}

// Награда
if ($model->price > 0 && $model->getCurrency()) {
    $c = \common\models\piramida\Currency::findOne($model['currency_id']);
    $priceHtml = Yii::$app->formatter->asDecimal(\common\models\piramida\Currency::getValueFromAtom($model['price'], $c), $c->decimals) . ' ' . Html::tag('span', $model->getCurrency()->code, ['class' => 'label label-info']);
} else {
    $priceHtml = '';
}

\yii\jui\JuiAsset::register($this);

?>

<h1 class="page-header"><?= Html::encode($model->name) ?></h1>

<p>
    <a href="<?= \yii\helpers\Url::to(['cabinet-school-task-list/add-sub', 'id' => $school->id, 'parent_id' => $model->id]) ?>" class="btn btn-default">Добавить</a>

    <?php /** Если это автор или админ или управляющий проектом */ ?>
    <?php if ($isAuthor || $isProjectManager || $isAdmin) { ?>
        <a href="<?= \yii\helpers\Url::to(['cabinet-school-task-list/edit', 'id' => $model->id]) ?>" class="btn btn-default">Редактировать</a>
    <?php } ?>
    <?php
    \avatar\assets\Notify::register($this);
    \common\assets\Clipboard\Asset::register($this);

    $this->registerJs(<<<JS

var clipboard = new Clipboard('.buttonCopy');
clipboard.on('success', function(e) {
    new Noty({
        timeout: 1000,
        theme: 'sunset',
        type: 'success',
        layout: 'bottomLeft',
        text: 'Скопировано'
    }).show();
});
JS
    );

    ?>
    <button class="btn btn-default buttonCopy" data-toggle="tooltip" data-clipboard-text="<?= \yii\helpers\Url::to(['cabinet-task-list/view', 'id' => $model->id], true) ?>" title="Нажми чтобы скопировать ссылку на эту задачу"><i class="glyphicon glyphicon-link"></i> Копировать ссылку</button>
</p>

<?php
$sort = new \yii\data\Sort([
    'attributes'   => [
        'id'                => ['label' => 'ID'],
        'status'            => [
            'label' => 'Прогресс',
            'asc'   => ['status' => SORT_ASC, 'category_id' => SORT_ASC],
            'desc'  => ['status' => SORT_DESC, 'category_id' => SORT_DESC],
        ],
        'price'             => [
            'label'   => 'Награда',
            'default' => SORT_DESC,
        ],
        'created_at'        => ['label' => 'Создано', 'default' => SORT_DESC],
        'name'              => ['label' => 'Наименование'],
        'last_comment_time' => ['label' => 'Обновлено'],
        'category_id'       => ['label' => 'category_id'],
    ],
    'defaultOrder' => [
        'category_id' => SORT_ASC,
        'created_at'  => SORT_DESC,
    ],
]);
$model2 = new \avatar\models\search\Task();
$provider = $model2->search($sort, Yii::$app->request->get(), $statusList, [
    'school_task.school_id' => $school->id,
    'parent_id'             => $model->id,
    'is_hide'               => 0,
]);
?>


<?php if ($provider->query->count() > 0) { ?>

    <?php
    $this->registerJs(<<<JS
$('[data-toggle="tooltip"]').tooltip();
$('.rowTable').click(function(e) {
    window.location = '/cabinet-school-task-list/view' + '?' + 'id' + '=' + $(this).data('id');
});
JS
    );
    $columns = [
        [
            'attribute' => 'id',
        ],
        [
            'header'  => 'Исп',
            'content' => function ($item) {
                $executer_id = ArrayHelper::getValue($item, 'executer_id', '');
                if ($executer_id == '') {
                    $user = \common\models\UserAvatar::findOne(9);
                    return Html::img($user->getAvatar(), [
                        'class'  => "img-circle",
                        'width'  => 30,
                        'height' => 30,
                        'style'  => 'margin-bottom: 0px;opacity: 0;',
                    ]);
                }
                $user = \common\models\UserAvatar::findOne($executer_id);

                return Html::img($user->getAvatar(), [
                    'class'  => "img-circle",
                    'width'  => 30,
                    'height' => 30,
                    'style'  => 'margin-bottom: 0px;',
                    'data'   => ['toggle' => 'tooltip'],
                    'title'  => \yii\helpers\Html::encode($user->getName2()),
                ]);
            },
        ],
        [
            'header'             => $sort->link('name'),
            'attribute'          => 'name',
        ],
        [
            'header'    => $sort->link('status'),
            'attribute' => 'status',
            'filter'    => [
                1 => 'Показать Выполненные',
                2 => 'Скрыть Выполненные',
            ],
            'content'   => function ($item) {
                if (empty($item['status'])) $item['status'] = 0;
                $statusList = Yii::$app->session->get('$statusList');
                $j = 0;
                if (!(is_null($item['status']) or $item['status'] == 0)) {
                    for ($i = 0; $i < count($statusList); $i++) {
                        $j++;
                        if ($item['status'] == $statusList[$i]) break;
                    }
                }

                $percent = 0;
                if ($j == 1) $percent = 33;
                if ($j == 2) $percent = 66;
                if ($j == 3) $percent = 100;

                return Html::tag(
                    'div',
                    Html::tag(
                        'div',
                        Html::tag(
                            'div',
                            $percent . '% Complete'
                            ,
                            ['class' => "sr-only"]
                        )
                        ,
                        [
                            'class'         => "progress-bar",
                            'role'          => "progressbar",
                            'aria-valuenow' => $percent,
                            'aria-valuemin' => "0",
                            'aria-valuemax' => "100",
                            'style'         => "width: $percent%;",
                        ]
                    )
                    ,
                    [
                        'class' => "progress",
                        'style' => "margin-bottom: 0px;",
                    ]
                );
            },
        ],
        [
            'header'          => $sort->link('price'),
            'attribute'       => 'price',
            'format'          => ['decimal', 2],
            'value'           => function ($model, $key, $index, $widget) {
                if (\cs\Application::isEmpty($model['price'])) return '';

                return \common\models\piramida\Currency::getValueFromAtom($model['price'], $model['currency_id']);
            },
        ],
        [
            'header'    => $sort->link('created_at'),
            'attribute' => 'created_at',
            'content'   => function ($item) {
                $v = \yii\helpers\ArrayHelper::getValue($item, 'created_at', 0);
                if ($v == 0) return '';

                return Html::tag('abbr', \cs\services\DatePeriod::back($v, ['isShort' => true]), ['data' => ['toggle' => 'tooltip'], 'title' => Yii::$app->formatter->asDatetime($v)]);
            },
        ],
        [
            'header'    => $sort->link('last_comment_time'),
            'attribute' => 'last_comment_time',
            'content'   => function ($item) {
                $v = \yii\helpers\ArrayHelper::getValue($item, 'last_comment_time', 0);
                if ($v == 0) return '';

                return Html::tag('abbr', \cs\services\DatePeriod::back($v, ['isShort' => true]), ['data' => ['toggle' => 'tooltip'], 'title' => Yii::$app->formatter->asDatetime($v)]);
            },
        ],
    ];
    if ($isProjectManager || $isAdmin) {
        $this->registerJs(<<<JS
$('.buttonEdit').click(function(e) {
    e.preventDefault();
    e.stopPropagation();
    window.location = '/cabinet-school-task-list/edit' + '?' + 'id' + '=' + $(this).data('id');
});
JS
        );
        $columns[] = [
            'header'    => 'Ред',
            'content'   => function ($item) {
                return Html::button('Ред', ['class' => 'btn btn-primary btn-xs buttonEdit', 'data' => ['id' => $item['id']]]);
            },
        ];
    }

    ?>
    <h4 class="page-header">
        Подзадачи
    </h4>
    <?=
    \yii\grid\GridView::widget([
        'dataProvider'        => $provider,
        'filterModel'         => $model2,
        'tableOptions'          => [
            'class' => 'table table-striped table-hover'
        ],
        'summary'             => '',
        'rowOptions'          => function ($item) {
            return [
                'data'  => ['id' => $item['id']],
                'role'  => 'button',
                'class' => 'rowTable',
            ];
        },
        'columns'             => $columns,
    ]);
    ?>

<?php } ?>

<?php
$c = \common\models\task\Category::findOne($model->category_id);
$executer = [];
$executer[] = (is_null($Executer)) ? '' : Html::img($Executer->getAvatar(), ['class' => 'img-circle', 'width' => 30, 'data-toggle' => 'tooltip', 'title' => \yii\helpers\Html::encode($Executer->getName2())]);

/** @var \common\models\task\Helper $helper */
foreach (\common\models\task\Helper::find()->where(['task_id' => $model->id])->all() as $helper) {
    $user = $helper->getUser();
    $executer[] = Html::img($user->getAvatar(), ['class' => 'img-circle', 'width' => 30, 'data-toggle' => 'tooltip', 'title' => \yii\helpers\Html::encode($user->getName2()), 'style' => 'margin-left: 5px;']);
}

$budjet = 1;
switch ($model->wallet_type) {
    case \common\models\task\Task::WALLET_TYPE_SCHOOL:
        $budjet = 'С кошелька сообщества';
        break;
    case \common\models\task\Task::WALLET_TYPE_AUTHOR:
        $budjet = 'С кошелька постановщика';
        break;
    case \common\models\task\Task::WALLET_TYPE_USER:
        $u = \common\models\UserAvatar::findOne($model->wallet_type3_user_id);
        $budjet = 'С кошелька пользователя ' . Html::img($u->getAvatar(), ['class' => 'img-circle', 'width' => 30, 'title' => $u->getName2()]);
        break;
}
?>
<?= \yii\widgets\DetailView::widget([
    'model'      => $model,
    'attributes' => [
        'id:text:ID',
        [
            'label'          => 'Заголовок',
            'captionOptions' => [
                'style' => 'width: 30%',
            ],
            'value'          => $model->name,
        ],
        'created_at:datetime:Создано',
        'updated_at:datetime:Изменено',
        [
            'label'  => 'Автор',
            'format' => 'html',
            'value'  => Html::img(
                $model->getUser()->getAvatar(),
                [
                    'class'       => 'img-circle',
                    'width'       => 30,
                    'data-toggle' => 'tooltip',
                    'title'       => \yii\helpers\Html::encode($model->getUser()->getName2()),
                ]
            ),
        ],
        [
            'label'  => 'Исполнитель',
            'format' => 'html',
            'value'  => join('', $executer),
            'contentOptions' => ['id' => 'helperList']
        ],
        [
            'label'  => 'Бюджет',
            'format' => 'html',
            'value'  => $budjet,
        ],
        [
            'label'  => 'Награда',
            'format' => 'html',
            'value'  => $priceHtml,
        ],
        [
            'label' => 'Категория',
            'value' => (is_null($c)) ? '' : $c->name,
        ],
        [
            'label'  => 'Содержание',
            'format' => 'html',
            'value'  => nl2br($model->content),
        ],
    ],
]) ?>




<?php if (is_null($model->status)) { ?>
    <div class="row">
        <div class="col-lg-4">
            <?php
            $this->registerJs(<<<JS
$('.buttonStart').click(function(e) {
    ajaxJson({
        url: '/cabinet-school-task-list/view-start',
        data: {
            id: $(this).data('id')
        },
        success: function(ret) {
            window.location.reload();
        }
    })
});
JS
            );
            ?>
            <button class="btn btn-default buttonStart" data-id="<?= $model->id ?>">Я сделаю это</button>
        </div>
        <div class="col-lg-8">
            <div class="input-group">
                <input type="hidden" id="inputExecutor-value" value="">
                <input type="text" value="" class="form-control inputExecutor" placeholder="Введите для поиска имени или email...">
                <?php
                $this->registerJs(<<<JS
$('.inputExecutor').autocomplete({
    minLength: 1,
    source: function(qry, callback1) {
            ajaxJson({
                url: '/cabinet-school-task-list/executor-search-ajax',
                data: {
                    term: qry.term,
                    school_id: {$school->id},
                    task_id: {$model->id}
                },
                success: function(ret) {
                    var rows = [];
                    var i = 0;
                    for(i = 0; i < ret.length; i++) {
                        rows.push({
                            value: ret[i].id,
                            label: ret[i].value
                        });
                    }
                    callback1(rows);
                }
            });
        }
});
$('.inputExecutor').on('autocompleteselect', function(event, ui) {
    $('#inputExecutor-value').val(ui.item.value);
    $('.inputExecutor').val(ui.item.label);
    return false;
});
JS
                );

                ?>

                <span class="input-group-btn">
            <?php
            $this->registerJs(<<<JS
$('.buttonAddExecutor').click(function(e) {
    if ($('#inputExecutor-value').val() == '') {
        new Noty({
            timeout: 1000,
            theme: 'relax',
            type: 'warning',
            layout: 'bottomLeft',
            text: 'Вы должны выбрать сначала кого-то из поиска и списка'
        }).show();
        return;
    }
    ajaxJson({
        url: '/cabinet-school-task-list/executor-add',
        data: {
            id: $('#inputExecutor-value').val(),
            task_id: {$model->id}
        },
        success: function(ret) {
            window.location.reload();
        }
    });
});
JS
            )
            ?>
                    <button class="btn btn-default buttonAddExecutor" type="button">Назначить исполнителя</button></span>
            </div>
        </div>
    </div>

<?php } ?>


<div class="row">
    <div class="col-lg-4">
        <?php if ($model->status == $statusList[0]) { ?>
            <?php if ($model->executer_id == Yii::$app->user->id) { ?>
                <?php
                $this->registerJs(<<<JS
$('.buttonFinish').on('click', function(e) {
    ajaxJson({
        url: '/cabinet-school-task-list/view-finish',
        data: {
            id: $(this).data('id')
        },
        success: function(ret) {
            window.location.reload();
        }
    })
});
JS
                );
                ?>
                <button class="btn btn-success buttonFinish" data-id="<?= $model->id ?>"><i
                            class="glyphicon glyphicon-ok"></i> Я сделал(а)!
                </button>

            <?php } ?>
            <?php if ($model->executer_id == Yii::$app->user->id || $isProjectManager || $isAdmin) { ?>
                <?php
                $this->registerJs(<<<JS
$('.buttonFinishReject').on('click', function(e) {
    if (confirm('Вы уверены?')) {
        ajaxJson({
            url: '/cabinet-school-task-list/view-finish-reject',
            data: {
                id: $(this).data('id')
            },
            success: function(ret) {
                window.location.reload();
            }
        })
    }
});
JS
                );
                ?>

                <button class="btn btn-warning buttonFinishReject" data-id="<?= $model->id ?>"><i
                            class="glyphicon glyphicon-remove"></i> Я снимаю с себя задачу
                </button>
            <?php } ?>
        <?php } ?>

        <?php if ($model->status == $statusList[1]) { ?>
            <?php if ($isProjectManager || $isAdmin) { ?>
                <?php
                $this->registerJs(<<<JS
$('.buttonDone').on('click', function(e) {
    ajaxJson({
        url: '/cabinet-school-task-list/view-done',
        data: {
            id: $(this).data('id')
        },
        success: function(ret) {
            window.location.reload();
        }
    })
});
$('.buttonReject').on('click', function(e) {
    $('#modalInfo').modal();
});
$('.buttonRejectModal').on('click', function(e) {
    ajaxJson({
        url: '/cabinet-school-task-list/view-reject',
        data: {
            id: $(this).data('id'),
            text: $('#textarea').val()
        },
        success: function(ret) {
            window.location.reload();
        }
    })
});

$('.buttonChangeTime').click(function() {
  $('#modalChangeTime').modal();
});
JS
                );
                ?>

                <div class="btn-group" style="margin-top: -5px;">
                    <button type="button" class="btn btn-success buttonDone" data-id="<?= $model->id ?>"><i
                                class="glyphicon glyphicon-ok"></i> Принять!</button>
                    <button type="button" class="btn btn-success dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <span class="caret"></span>
                        <span class="sr-only">Toggle Dropdown</span>
                    </button>
                    <ul class="dropdown-menu">
                        <li><a href="javascript:void(0);" class="buttonChangeTime">Изменить время и принять</a></li>
                    </ul>
                </div>

                <button class="btn btn-danger buttonReject" data-id="<?= $model->id ?>"><i
                            class="glyphicon glyphicon-remove"></i> Отклонить!
                </button>
            <?php } ?>
        <?php } ?>

        <?php if (in_array($model->status, [$statusList[0],$statusList[1]])) { ?>
            <?php if ($isProjectManager || $isAdmin) { ?>
                <?php
                $this->registerJs(<<<JS
$('.buttonChangePrice').on('click', function(e) {
    window.location = '/cabinet-school-task-list/change-price' + '?' + 'id' + '=' + $(this).data('id');
});
JS
                );
                ?>
                <button class="btn btn-default buttonChangePrice" data-id="<?= $model->id ?>">
                    Изменить награду
                </button>
            <?php } ?>
        <?php } ?>
    </div>

    <div class="col-lg-8">
        <p>Соисполнитель</p>
        <div class="input-group">
            <input type="hidden" id="inputHelper-value" value="">
            <input type="text" value="" class="form-control inputHelper" placeholder="Введите для поиска имени или email...">
            <?php

            $this->registerJs(<<<JS
$('.inputHelper').autocomplete({
    minLength: 1,
    source: function(qry, callback1) {
        ajaxJson({
            url: '/cabinet-school-task-list/helper-search-ajax',
            data: {
                term: qry.term,
                school_id: {$school->id},
                task_id: {$model->id}
            },
            success: function(ret) {
                var rows = [];
                var i = 0;
                for(i = 0; i < ret.length; i++) {
                    rows.push({
                        value: ret[i].id,
                        label: ret[i].value
                    });
                }
                callback1(rows);
            }
        });
    }
});
$('.inputHelper').on('autocompleteselect', function(evt, ui) {
    $('#inputHelper-value').val(ui.item.value);
    $('.inputHelper').val(ui.item.label);
    return false;
});
JS
            );
            ?>

            <span class="input-group-btn">
            <?php
            $this->registerJs(<<<JS
$('.buttonAddHelper').click(function(e) {
    if ($('#inputHelper-value').val() == '') {
        new Noty({
            timeout: 1000,
            theme: 'relax',
            type: 'warning',
            layout: 'bottomLeft',
            text: 'Вы должны выбрать сначала кого-то из поиска и списка'
        }).show();
        return;
    }
    ajaxJson({
        url: '/cabinet-school-task-list/helper-add',
        data: {
            id: $('#inputHelper-value').val(),
            task_id: {$model->id}
        },
        success: function(ret) {
            $('#helperList').append($('<img>', {src: ret.user.avatar, class: 'img-circle', title: ret.user.name, style: 'margin-left: 5px;', "data-toggle": "tooltip"}).tooltip().attr('width', 30));
            $('.inputHelper').val('');
            $('#inputHelper-value').val('');
        }
    });
});

JS
            )
            ?>
                <button class="btn btn-default buttonAddHelper" type="button">Назначить Соисполнителя</button></span>
        </div>
    </div>
</div>


<hr>
<?php if (!is_null($model->status) and !in_array($model->status, [$statusList[1], $statusList[2]])) { ?>
    <?= $this->render('view-counter', ['model' => $model]) ?>
<?php } ?>

<?php if (in_array($model->status, [$statusList[1], $statusList[2]])) { ?>
    <?= $this->render('view-counter2', ['model' => $model]) ?>
<?php } ?>

<?= \avatar\modules\Comment\Module::getComments2(
    1,
    $model->id,
    [
        'school_id'       => $school->id,
        'functionSuccess' => new \yii\web\JsExpression(<<<JS
function (response) {
    // Вызываю AJAX для записи в school_file
    ajaxJson({
        url: '/cabinet/file-upload7-save',
        data: {
            file: response.url,
            school_id: {$school->id},
            type_id: 24,
            size: response.size // Размер файла в байтах
        },
        success: function (ret) {
            
        }
    });
}

JS
        ),
    ]
); ?>

<?php if ($model->status == $statusList[1]) { ?>
    <?php if ($isProjectManager || $isAdmin) { ?>
        <?= $this->render('viewChangeTime', ['model' => $model, 'school' => $school])?>
    <?php } ?>
<?php } ?>
