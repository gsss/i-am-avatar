<?php

?>


<div class="input-group">
    <input type="hidden" id="helper" value="">
    <input type="text" id="helper-text" value="" class="form-control inputHelper" placeholder="Введите для поиска имени или email...">
    <?php
    \common\assets\AutoComplete\Asset::register($this);
    $this->registerJs(<<<JS

$('.inputHelper').autoComplete({
    minLength: 1,
    events: {
        search: function(qry, callback1) {
            ajaxJson({
                url: '/cabinet-school-task-list/helper-search-ajax',
                data: {
                    term: qry,
                    school_id: {$school->id},
                    task_id: {$model->id}
                },
                success: function(ret) {
                    console.log(ret);
                    var rows = [];
                    var i = 0;
                    for(i = 0; i < ret.length; i++) {
                        rows.push({
                            value: ret[i].id,
                            text: ret[i].value
                        });
                    }
                    callback1(rows);
                }
            });
        }
    }
});
$('.inputHelper').on('autocomplete.select', function(evt, item) {
    console.log([evt, item]); 
    $('#helper').val(item.value);
});
JS
    );
    ?>

    <span class="input-group-btn">
            <?php
            $this->registerJs(<<<JS
$('.buttonAddHelper').click(function(e) {
    if ($('#helper').val() == '') {
        alert('Вы должны выбрать сначала кого-то из поиска и списка');
        return;
    }
    ajaxJson({
        url: '/cabinet-school-task-list/add-helper',
        data: {
            id: $('#helper').val(),
            task_id: {$model->id}
        },
        success: function(ret) {
            $('#helperList').append($('<img>', {src: ret.user.avatar, class: 'img-circle', title: ret.user.name, style: 'margin-left: 5px;', "data-toggle": "tooltip"}).tooltip().attr('width', 30));
            $('#w1').val('');
            $('#helper').val('')
        }
    });
});
JS
            )
            ?>
            <button class="btn btn-default buttonAddHelper" type="button">Добавить помощника</button></span>
</div>
