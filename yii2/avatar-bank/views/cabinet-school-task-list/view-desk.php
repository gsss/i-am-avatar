<?php

use common\models\task\Session;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;
use yii\db\Query;
use yii\helpers\ArrayHelper;

/** @var $this yii\web\View */
/** @var $form yii\bootstrap\ActiveForm */
/** @var $model \common\models\task\Task */
/** @var $school \common\models\school\School */

$this->title = $model->name;

?>
<div class="container">


    <?= $this->render('view-desk-item', [
        'school' => $school,
        'model'  => $model,
    ]) ?>


</div>

<div class="modal fade" id="modalInfo" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="myModalLabel">Информация</h4>
            </div>
            <div class="modal-body">
                <textarea class="form-control" rows="10" id="textarea"></textarea>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary buttonRejectModal" data-id="<?= $model->id ?>">Отправить</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
            </div>
        </div>
    </div>
</div>