<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;
use yii\db\Query;
use yii\helpers\ArrayHelper;

/** @var $this yii\web\View */
/** @var $model \avatar\models\forms\SchoolMoneyIncome */
/** @var $school \common\models\school\School */

$this->title = $model->id;

?>
<div class="container">

    <h1 class="page-header"><?= Html::encode($this->title) ?></h1>

    <?php $form = \iAvatar777\services\FormAjax\ActiveForm::begin([
        'model'      => $model,
        'success' => <<<JS
function (ret) {
    $('#modalInfo').on('hidden.bs.modal', function() {
        window.location = '/cabinet-school-money-income/index?id=' + {$school->id};
    }).modal();
}
JS
        ,
    ]); ?>

    <?= $form->field($model, 'amount') ?>
    <?= $form->field($model, 'image') ?>
    <?= $form->field($model, 'description') ?>
    <?= $form->field($model, 'date') ?>
    <hr>

    <?php  \iAvatar777\services\FormAjax\ActiveForm::end(); ?>


</div>
