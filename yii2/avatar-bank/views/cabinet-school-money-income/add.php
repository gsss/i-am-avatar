<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;
use app\models\UnionCategory;
use yii\db\Query;
use yii\helpers\ArrayHelper;

/** @var $this yii\web\View */
/** @var $model \avatar\models\forms\SchoolMoneyIncome */
/** @var $school \common\models\school\School */

$this->title = 'Добавить вложение';

?>
<div class="container">

    <h1 class="page-header"><?= Html::encode($this->title) ?></h1>

    <div class="row">
        <div class="col-lg-8">
            <?php $form = \iAvatar777\services\FormAjax\ActiveForm::begin([
                'model'      => $model,
                'success' => <<<JS
function (ret) {
    $('#modalInfo').on('hidden.bs.modal', function() {
        window.location = '/cabinet-school-money-income/index?id=' + {$school->id};
    }).modal();
}
JS
                ,
            ]); ?>

            <?= $form->field($model, 'amount') ?>
            <?= $form->field($model, 'image') ?>
            <?= $form->field($model, 'description') ?>
            <?= $form->field($model, 'date') ?>
            <hr>

            <?php  \iAvatar777\services\FormAjax\ActiveForm::end(); ?>


        </div>
    </div>

</div>

<div class="modal fade" id="modalInfo" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="myModalLabel">Информация</h4>
            </div>
            <div class="modal-body">
                Успешно!
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
            </div>
        </div>
    </div>
</div>