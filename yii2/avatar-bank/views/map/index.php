<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */

$this->title = 'Карта Аватаров';

$rows = \common\models\shop\UserMap::find()->asArray()->all();
$rows2 = [];
foreach ($rows as $row) {
    $user = \common\models\UserAvatar::findOne($row['user_id']);
    $item = $row;
    $item['lat'] = $item['lat'] / 1000000000;
    $item['lng'] = $item['lng'] / 1000000000;
    $item['user'] = [
        'id' => $user->id,
        'name' => $user->getName2(),
        'avatar' => $user->getAvatar(),
    ];
    $rows2[] = $item;
}

$rows2Json = \yii\helpers\Json::encode($rows2);

\common\assets\YandexMaps::register($this);
$this->registerJs(<<<JS
ymaps.ready(function(e) {
    
    var PlaceMapYandex = {};
    
    var items = {$rows2Json};
    
    // При возникновении событий, изменяющих состояние карты,
    // ее параметры передаются в адресную строку браузера (после символа #).
    // При загрузке страницы карта устанавливается в состояние,
    // соответствующее переданным параметрам.
    // http://.../savemap.html#type=hybrid&center=93.3218,60.0428&zoom=12
    var myMap = new ymaps.Map("map", {
            center: [48.707787, 44.515933], // Волгоград
            zoom: 4
        }),
        
        myPlacemarkCollection = new ymaps.GeoObjectCollection(),
        lastOpenedBalloon = false;
    
    
    
    for(i = 0; i < items.length; i++) { 
        var myPlacemark1 = new ymaps.Placemark([items[i].lng, items[i].lat], {
            balloonContent: items[i].user.name + '<br>' + '<img src="'+items[i].user.avatar+'" width="50" class="img-circle">',
            myId: items[i].user.id
        });
        myPlacemarkCollection.add(myPlacemark1);
        myMap.geoObjects.add(myPlacemarkCollection);
        myMap.geoObjects.remove(myPlacemark1);
    }
    
    myMap.controls.add('typeSelector');
    
    // Обработка событий карты:
    // - boundschange - изменение границ области показа;
    // - type - изменение типа карты;
    // - balloonclose - закрытие балуна.
    myMap.events.add(['boundschange', 'typechange', 'balloonclose'], setLocationHash);
    
    // Обработка событий открытия балуна для любого элемента
    // коллекции.
    // В данном случае на карте находятся только метки одной коллекции.
    // Чтобы обработать события любых геообъектов карты можно использовать
    // myMap.geoObjects.events.add(['balloonopen'],function (e) { ...
    myPlacemarkCollection.events.add(['balloonopen'], function (e) {
        lastOpenedBalloon = e.get('target').properties.get('myId');
        setLocationHash();
    });
    
    setMapStateByHash();
    
    // Получение значение параметра name из адресной строки
    // браузера.
    function getParam (name, location) {
        location = location || window.location.hash;
        var res = location.match(new RegExp('[#&]' + name + '=([^&]*)', 'i'));
        return (res && res[1] ? res[1] : false);
    }
    
    // Передача параметров, описывающих состояние карты,
    // в адресную строку браузера.
    function setLocationHash () {
        var params = [
            'type=' + myMap.getType().split('#')[1],
            'center=' + myMap.getCenter(),
            'zoom=' + myMap.getZoom()
        ];
        if (myMap.balloon.isOpen()) {
            params.push('open=' + lastOpenedBalloon);
        }
        window.location.hash = params.join('&');
    }
    
    // Установка состояния карты в соответствии с переданными в адресной строке
    // браузера параметрами.
    function setMapStateByHash () {
        var hashType = getParam('type'),
            hashCenter = getParam('center'),
            hashZoom = getParam('zoom'),
            open = getParam('open');
        if (hashType) {
            myMap.setType('yandex#' + hashType);
        }
        if (hashCenter) {
            myMap.setCenter(hashCenter.split(','));
        }
        if (hashZoom) {
            myMap.setZoom(hashZoom);
        }
        if (open) {
            myPlacemarkCollection.each(function (geoObj) {
                var id = geoObj.properties.get('myId');
                if (id == open) {
                    geoObj.balloon.open();
    
                }
            });
        }
    }

});



JS
);
?>
<div class="container" style="padding-bottom: 70px;">
    <div class="col-lg-12">
        <h1 class="page-header text-center"><?= $this->title ?></h1>
    </div>
</div>
<div id="map" style="width: 100%; height: 500px;"></div>
