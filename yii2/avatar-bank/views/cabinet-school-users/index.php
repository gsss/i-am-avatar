<?php

/** @var $this \yii\web\View */

/** @var $school \common\models\school\School */

use yii\helpers\Url;
use app\services\GsssHtml;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use app\services\GetArticle\Collection;

$this->title = 'Все пользователи';


?>

<div class="container">


    <h1 class="page-header"><?= Html::encode($this->title) ?></h1>

    <p>
        <a href="<?= Url::to(['cabinet-school-subscribe2/index', 'id' => $school->id]) ?>"
           class="btn btn-default">Рассылки</a>
    </p>


    <?php \yii\widgets\Pjax::begin(); ?>
    <?php
    $this->registerJS(<<<JS
$('[data-toggle="tooltip"]').tooltip();

JS
    );
    $model = new \avatar\models\search\UserSchool();
    $provider = $model->search(Yii::$app->request->get(), ['user_school_link.school_id' => $school->id]);
    ?>
    <?= \yii\grid\GridView::widget([
        'dataProvider' => $provider,
        'filterModel'  => $model,
        'tableOptions' => [
            'class' => 'table table-striped table-hover',
        ],
        'rowOptions'   => function ($item) {
            $data = [
                'data'  => ['id' => $item['id']],
                'role'  => 'button',
                'class' => 'rowTable',
            ];
            return $data;
        },
        'columns'      => [
            [
                'attribute'     => 'id',
                'headerOptions' => [
                    'style' => 'width: 10%',
                ],
            ],
            [
                'header'        => 'Почта',
                'attribute'     => 'email',
                'headerOptions' => [
                    'style' => 'width: 75%',
                ],
            ],
            [
                'header'        => 'Статус',
                'attribute'     => 'avatar_status',
                'headerOptions' => [
                    'style' => 'width: 15%',
                ],
                'filter'        => [
                    0 => 'Не активирован',
                    1 => 'Гость подтвердил',
                    2 => 'Пользователь',
                ],
                'content'       => function ($item) {
                    $v = \yii\helpers\ArrayHelper::getValue($item, 'avatar_status', 0);
                    if ($v == 0) return Html::tag('span', 'Не активирован', ['class' => 'label label-default', 'data' => ['toggle' => 'tooltip'], 'title' => 'Не подтвердил свою почту после регистрации']);
                    if ($v == 1) return Html::tag('span', 'Гость подтвердил', ['class' => 'label label-info', 'data' => ['toggle' => 'tooltip'], 'title' => 'Подтвердил свою почту после регистрации, но не пользователь платформы']);
                    if ($v == 2) return Html::tag('span', 'Пользователь', ['class' => 'label label-success', 'data' => ['toggle' => 'tooltip'], 'title' => 'Зарегистрированный пользователь платформы']);

                    return '';
                },
            ],
        ],
    ]) ?>
    <?php \yii\widgets\Pjax::end(); ?>



</div>

<div class="modal fade" id="modalInfo" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="myModalLabel">Информация</h4>
            </div>
            <div class="modal-body">
                Успешно!
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
            </div>
        </div>
    </div>
</div>