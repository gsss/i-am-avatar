<?php

use common\models\avatar\Currency;
use common\models\avatar\UserBill;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;

/** @var $this yii\web\View */
/** @var $school \common\models\school\School */
/** @var $card string номер карты */

\avatar\assets\Notify::register($this);
\avatar\assets\QrCode\Asset::register($this);

$c1 = \common\models\Card::findOne(37);
$card = $c1->number;
$this->title = 'Взнос';

/** @var \common\models\Card $c */
$c = \common\models\Card::findOne(['number' => $card]);
?>

<div class="container" style="padding-bottom: 70px;">
    <div class="col-lg-12">
        <h1 class="page-header text-center">
            <?= $this->title ?>
        </h1>
    </div>


    <div class="col-lg-4 col-lg-offset-4">
        <div class="collapse in" id="step0">
            <p>Номер карты</p>
            <input class="form-control" id="card_number">
            <hr>
            <?php
            $this->registerJs(<<<JS
$('.buttonCard').click(function(e) {
    $('#step0').on('hidden.bs.collapse', function() {
        $('#collapseExample').collapse('show');
    }).collapse('hide');
});
JS
)
            ?>
            <button class="btn btn-success buttonCard" style="width: 100%;">Далее</button>
        </div>
        <div class="collapse" id="collapseExample">


            <p><img src="<?= $c->getDesign()->image ?>" width="100%"></p>
            <p class="lead text-center"><code>8999 0970 2620 4125</code></p>
            <input class="form-control" id="pin" type="password" disabled="disabled" style="border: none; font-size: 1600%; height: 100px; font-family: "Helvetica Neue", Helvetica, Arial, sans-serif" >
            <div class="row" style="margin-top: 20px;">
                <div class="col-xs-4">
                    <button class="btn btn-default buttonNumber" style="width: 100%">1</button>
                </div>
                <div class="col-xs-4">
                    <button class="btn btn-default buttonNumber" style="width: 100%">2</button>
                </div>
                <div class="col-xs-4">
                    <button class="btn btn-default buttonNumber" style="width: 100%">3</button>
                </div>
            </div>
            <div class="row" style="margin-top: 20px;">
                <div class="col-xs-4">
                    <button class="btn btn-default buttonNumber" style="width: 100%">4</button>
                </div>
                <div class="col-xs-4">
                    <button class="btn btn-default buttonNumber" style="width: 100%">5</button>
                </div>
                <div class="col-xs-4">
                    <button class="btn btn-default buttonNumber" style="width: 100%">6</button>
                </div>
            </div>
            <div class="row" style="margin-top: 20px;">
                <div class="col-xs-4">
                    <button class="btn btn-default buttonNumber" style="width: 100%">7</button>
                </div>
                <div class="col-xs-4">
                    <button class="btn btn-default buttonNumber" style="width: 100%">8</button>
                </div>
                <div class="col-xs-4">
                    <button class="btn btn-default buttonNumber" style="width: 100%">9</button>
                </div>
            </div>
            <div class="row" style="margin-top: 20px;">
                <div class="col-xs-4">
                </div>
                <div class="col-xs-4">
                    <button class="btn btn-default buttonNumber" style="width: 100%">0</button>
                </div>
                <div class="col-xs-4">
                    <button class="btn btn-default buttonDel" data-id="7" style="width: 100%"><i class="glyphicon glyphicon-menu-left"></i></button>
                </div>
            </div>
            <hr>
            <?php
            $this->registerJs(<<<JS
$('#pin').focus();
// $('#pin').keyup(function(event) {
//     event.keyCode == 13;
//     console.log(event);
//     return false;
// });
// $('#pin').on('input', function(event) {
//     event.keyCode == 13;
//     console.log(event);
//     return false;
// });
var pin = '';
$('.buttonNumber').click(function() {
    if (pin.length >= 4) return;
    var num = $(this).html();
    console.log(num);
    pin = pin + num;
    $('#pin').val(pin);
    if (pin.length == 4) {
        $('.buttonNumber').attr('disabled', 'disabled');
    }
});
$('.buttonDel').click(function() {
    if (pin.length == 0) return;
    if (pin.length == 4) {
        $('.buttonNumber').removeAttr('disabled');
    }
    var n = pin.substr(0,pin.length - 1);
    pin = n;
    $('#pin').val(n);
});
$('.buttonPay').click(function(e) {
    ajaxJson({
        url: '/pay/card-pay-ajax',
        data: {
            "card": {$card}, 
            "pin": $('#pin').val()
        },
        success: function(ret) {
            var div = $('<p>', {class: 'text-center', style: 'margin-bottom: 50px;'});
            div.qrcode('https://cloud1.cloud999.ru/upload/cloud/15880/91407_qvl9yuDNU6_crop.png');
            div.find('canvas');
            $('#qr').append(div);
            $('#collapseExample').on('hidden.bs.collapse', function() {
                $('#step2').collapse('show');
            }).collapse('hide');
            
            // $('#modalInfo').on('hidden.bs.modal', function() {
            //    
            // }).modal();
        },
        errorScript: function(ret) {
            if (ret.id == 102) {
                for (var key in ret.data) {
                    if (ret.data.hasOwnProperty(key)) {
                        var name = key;
                        var value = ret.data[key];
                        new Noty({
                            timeout: 1000,
                            theme: 'relax',
                            type: 'warning',
                            layout: 'bottomLeft',
                            text: value.join('<br>')
                        }).show();
                    }
                }
            }
        }
    });
});
JS
            );

            ?>
            <button class="btn btn-success buttonPay" style="width: 100%;">Оплатить</button>
        </div>
        <div class="collapse" id="step2">
            <p class="text-center"><img src="https://cloud1.cloud999.ru/upload/cloud/15880/91407_qvl9yuDNU6_crop.png" width="50%" class="img-circle"></p>
            <div id="qr"></div>
            <p class="alert-success alert text-center">Транзация прошла успешно</p>
        </div>
    </div>






</div>

<div class="modal fade" id="modalInfo" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Информация</h4>
            </div>
            <div class="modal-body">
                Успешно!
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
            </div>
        </div>
    </div>
</div>
