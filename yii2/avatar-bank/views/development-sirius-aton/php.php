<?php
use common\services\UsersInCache;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;

/* @var $this yii\web\View */

$this->title = 'Сервер Центрального Солнца Вселенной (Sirius Aton)';

?>
<style>
    .headerAPI {
        margin-top: 100px;
    }
</style>
<div class="container">
    <div class="col-lg-12">
        <h1 class="page-header text-center"><img src="/images/controller/development-sirius-aton/index/eth2.png" width="300"><br>Сервер Центрального Солнца Вселенной<br><small>Sirius Aton</small></h1>

        <h2 class="page-header">API</h2>

        <p>Репозитарий: <code>git@bitbucket.org:Ra-m-ha/sirius-aton-php.git</code></p>



        <h3 class="page-header">Классический ответ</h3>
        <p><b>Положительно</b></p>
        <pre>
{
    success: true,
    data: mixed
}</pre>
        <p><b>Отрицательно</b></p>
        <pre>
{
    success: false,
    data: mixed
}</pre>

        <h3 class="page-header">Ошибки</h3>

        <h3 class="page-header">Исключения</h3>

        <h3 class="page-header">Логирование</h3>

        <h2 class="page-header">Функции</h2>


        <p class="alert alert-success headerAPI">site/export</p>
        <p>Экспортирует кошелек</p>

        <p><a href="https://github.com/ethereumjs/keythereum" target="_blank">https://github.com/ethereumjs/keythereum</a></p>


        <p><b>Входные параметры</b></p>
        <p><code>POST</code></p>
        <?= \avatar\services\Params::widget([
            'params' => [
                [
                    'name'        => 'address',
                    'type'        => 'string',
                    'isRequired'  => true,
                    'description' => 'Адрес кошелька начинающийся с 0x, чувствительность к регистру отсутствует',
                ],
                [
                    'name'        => 'password',
                    'type'        => 'string',
                    'isRequired'  => true,
                    'description' => 'Пароль от кошелька',
                ],
            ]
        ]) ?>

        <p>Эта функция обращается к NodeJS интерфейсу для проверки пароля.</p>
        <p>В параметре <code>Yii::$app->params['serverNodeJS']</code> должен быть задан путь к серверу nodeJS без слеша в конце.</p>

        <p><b>Выходные данные</b></p>
        <pre>
{
    data: '{...}'
}</pre>
        <p>101, Не передан параметр address или password</p>
        <p>102, Кошелек не найден</p>
        <p>103, Не верный пароль или сервер не отвечает</p>
        <p>104, ошибка при проверке пароля</p>
        <p><b>$e->getMessage()</b></p>
        <p>could not decrypt key with given passphrase - не верный пароль</p>
        <p>no key for given address or file - нет файла на сервере</p>
        <p>Enter valid Ethereum address. - не верный адрес передан</p>


        <p class="alert alert-success headerAPI">site/mem</p>
        <p>Показывает память</p>



        <p><b>Входные параметры</b></p>
        <p><code>POST</code></p>
        <p>нет</p>

        <p><img src="/images/controller/development-sirius-aton/php/2018-02-11_04-14-39.png" class="thumbnail"></p>

        <p><b>Выходные данные</b></p>
        <pre>
{
    data: {
           mem: {
                total: 5665,
                used: 564654,
                free: 564654,
                shred: 564654,
                cache: 564654,
                available: 564654
            },
            swap: {
                total: 5665,
                used: 564654,
                free: 564654
            }
        }
}</pre>


        <p class="alert alert-success headerAPI">site/processor</p>
        <p>Показывает загрузку процессора</p>



        <p><b>Входные параметры</b></p>
        <p><code>POST</code></p>
        <p>нет</p>

        <p><img src="/images/controller/development-sirius-aton/php/2018-02-11_16-00-45.png" class="thumbnail"></p>

        <p><b>Выходные данные</b></p>
        <pre>
{
    data: {
           items: [
            {
                usr: 0.1,
                nice: 0.1,
                sys: 0.1,
                iowait: 0.1,
                irq: 0.1,
                soft: 0.1,
                steal: 0.1,
                guest: 0.1,
                gnice: 0.1,
                idle: 0.1,
            },
            // ...
            ],
            all: {
                usr: 0.1,
                nice: 0.1,
                sys: 0.1,
                iowait: 0.1,
                irq: 0.1,
                soft: 0.1,
                steal: 0.1,
                guest: 0.1,
                gnice: 0.1,
                idle: 0.1,
            }
        }
}</pre>






    </div>
</div>



