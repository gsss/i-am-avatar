<?php
use common\services\UsersInCache;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;

/* @var $this yii\web\View */

$this->title = 'Сервер Центрального Солнца Вселенной (Sirius Aton)';

?>
<style>
    .page-header {
        margin-top: 100px;
    }
</style>
<div class="container">
    <div class="col-lg-12">
        <h1 class="page-header text-center"><img src="/images/controller/development-sirius-aton/index/eth2.png" width="300"><br>Сервер Центрального Солнца Вселенной<br><small>Sirius Aton</small></h1>

        <h2 class="page-header">API</h2>

        <p>Сборка:</p>
        <pre>$ npm install</pre>
        <p>Запуск:</p>
        <pre>$ DEBUG=myapp:* npm start</pre>
        <p>Это API предоставляет доступ к ноде Ethereum</p>
        <p>Главный Url: <code>https://atlantida.io/api</code></p>

        <p>Тестовый Url: <code>http://sirius-aton.avatar-bank.com/api</code> (не защищенный)</p>
        <p>IP: <code>185.47.62.223</code></p>


        <h3 class="page-header">Классический запрос</h3>
        <p>Метод: <code>POST</code></p>
        <p>Обязательные параметры передаются по Basic Authentification</p>
        <p>В приложении используется метод авторизации <a href="https://www.netangels.ru/support/hosting-howto/php-http-auth/" target="_blank">Basic Authentication Scheme</a>.</p>
        <pre>
$credentials = "calcuser:testpwd";

header("POST /svcs/CalculationEng HTTP/1.0");
header("Content-Type: text/xml; charset=utf-8");
header("Accept: application/soap+xml, application/dime, multipart/related, text/*");
header("Cache-Control: no-cache");
header("Pragma: no-cache");
header("SOAPAction: \"run\"");
header("Content-Length: 2815");
header("Authorization: Basic " . base64_encode($credentials));
</pre>
        <p>Используется авторизация настраиваемая через Модуль.</p>
        <p>Телефон передается в переменной <code>$_SERVER['PHP_AUTH_USER'];</code> без + вначале и без спец символов</p>
        <p>Пароль в <code>$_SERVER['PHP_AUTH_PW'];</code></p>
        <p>Если логин и пароль будут не верны то будет выдана HTTP ошибка 401 (HTTP/1.1 401 Unauthorized).</p>

        <h3 class="page-header">Классический ответ</h3>
        <p><b>Положительно</b></p>
<pre>
{
    success: true,
    data: mixed
}</pre>
        <p><b>Отрицательно</b></p>
        <pre>
{
    success: false,
    data: mixed
}</pre>
        <h3 class="page-header">Ошибки</h3>
        <p>Может выдавать:</p>
        <pre>
&lt;html&gt;
&lt;head&gt;&lt;title&gt;502 Bad Gateway&lt;/title&gt;&lt;/head&gt;
&lt;body bgcolor=\&quot;white\&quot;&gt;
&lt;center&gt;&lt;h1&gt;502 Bad Gateway&lt;/h1&gt;&lt;/center&gt;
&lt;hr&gt;&lt;center&gt;nginx/1.10.0 (Ubuntu)&lt;/center&gt;
&lt;/body&gt;
&lt;/html&gt;</pre>
        <h3 class="page-header">Исключения</h3>
        <h3 class="page-header">Логирование</h3>


        <h2 class="page-header">Функции</h2>
        <h3 class="page-header">Создать кошелек</h3>
        <p class="alert alert-success">/new</p>
        <p>Создает кошелек ETH</p>

        <p><img src="/images/controller/development-sirius-aton/index/ETH.png"></p>
        <p><a href="https://github.com/ethereumjs/keythereum" target="_blank">https://github.com/ethereumjs/keythereum</a></p>


        <p><b>Входные параметры</b></p>
        <p><code>POST</code></p>
        <?= \avatar\services\Params::widget([
            'params' => [
                [
                    'name'        => 'password',
                    'isRequired'  => true,
                    'description' => 'пароль от кошелька',
                ]
            ]
        ]) ?>

        <p>Хранение кошельков <code>~/.ethereum/testnet/keystore</code> на тест и <code>~/.ethereum/keystore</code> на прод</p>
        <p><b>Выходные данные</b></p>
        <pre>
{
    address: '0xfde...'
    sedds: 'seel ded flowing ...'
}</pre>

        <h3 class="page-header">Перевести деньги</h3>
        <p class="alert alert-success">/send</p>
        <p>Перевести деньги ETH</p>

        <p><b>Входные параметры</b></p>
        <p><code>POST</code></p>

        <?= \avatar\services\Params::widget([
            'params' => [
                [
                    'name'        => 'user',
                    'isRequired'  => true,
                    'description' => 'Адрес отправителя начинающийся с 0x',
                ],
                [
                    'name'        => 'password',
                    'isRequired'  => true,
                    'description' => 'Пароль отпрателя для адреса ' . Html::tag('code', 'user'),
                ],
                [
                    'name'        => 'address',
                    'isRequired'  => true,
                    'description' => 'Адрес получателя начинающийся с 0x',
                ],
                [
                    'name'        => 'amount',
                    'isRequired'  => true,
                    'description' => 'Кол-во эфира для отправки, десятичная точка',
                    'type'        => 'float',
                ],
                [
                    'name'        => 'gasLimit',
                    'description' => 'Максимальное кол-во газа, которое может быть потрачено за транзакцию. Обычно применяется когда клиент отправляет эфир на адрес краудсейл контракта, значение по умолчанию 21000',
                    'type'        => 'int',
                ],
                [
                    'name'        => 'gasPrice',
                    'description' => 'Цена газа в wei, значение по умолчанию 21*10E9',
                    'type'        => 'int',
                ],
            ]
        ]) ?>

        <p><b>Выходные данные</b></p>
        <pre>
{
    transaction: {
        address: '0x...'
    }
}</pre>

        <p>Ошибки:</p>
        <p><code>Enter valid Ethereum address.</code> 102</p>
        <p><code>Enter valid amount.</code> 103</p>
        <p><code>Error: no key for given address or file ...</code> 100</p>

        <h3 class="page-header">Получить приватный ключ</h3>
        <p class="alert alert-success">/get-private-key</p>
        <p>Получить приватный Ключ</p>

        <p><b>Входные параметры</b></p>
        <p><code>POST</code></p>

        <?= \avatar\services\Params::widget([
            'params' => [
                [
                    'name'        => 'user',
                    'isRequired'  => true,
                    'description' => 'адрес отправителя начинающийся с 0x',
                ],
                [
                    'name'        => 'password',
                    'isRequired'  => true,
                    'description' => 'пароль отпрателя для адреса ' . Html::tag('code', 'user'),
                ],
            ]
        ]) ?>

        <p><b>Выходные данные</b></p>
        <pre>{
    key: '...'
}</pre>


        <h3 class="page-header">Проверить пароль к кошельку</h3>
        <p class="alert alert-success">/testPassword</p>
        <p>Проверяет пароль к кошельку</p>

        <p><b>Входные параметры</b></p>
        <p><code>POST</code></p>

        <?= \avatar\services\Params::widget([
            'params' => [
                [
                    'name'        => 'user',
                    'isRequired'  => true,
                    'description' => 'адрес отправителя начинающийся с 0x',
                ],
                [
                    'name'        => 'password',
                    'isRequired'  => true,
                    'description' => 'пароль для адреса ' . Html::tag('code', 'user'),
                ],
            ]
        ]) ?>

        <p><b>Выходные данные</b></p>
        <p><b>Положительный исход</b></p>

        <pre>{
    success: true
}</pre>
        <p><b>Отрицательный исход</b></p>

        <pre>{"error": "could not decrypt key with given passphrase", "code":100}</pre>
        <pre>{"error": "no key for given address or file", "code":100}</pre>
        <pre>{"error": "Enter valid Ethereum address.", "code": 102}</pre>


        <h3 class="page-header">Импортировать кошелек по JSON файлу</h3>
        <p class="alert alert-success">/importWalletJson</p>
        <p>Импортировать кошелек</p>

        <p><b>Входные параметры</b></p>
        <p><code>POST</code></p>

        <?= \avatar\services\Params::widget([
            'params' => [
                [
                    'name'        => 'jsonFile',
                    'isRequired'  => true,
                    'description' => 'Содержимое файла JSON',
                ],
                [
                    'name'        => 'password',
                    'isRequired'  => true,
                    'description' => 'Пароль от расшифровки файла JSON',
                ],
            ]
        ]) ?>

        <p>Функция проверяет может ли она расшифровать и если нет то выдает ошибку что пароль не верный, а если может то возвращает адрес кошелька</p>
        <p><b>Выходные данные</b></p>
        <pre>{
    address: '...' // адрес кошелька
}</pre>

        <h3 class="page-header">Импортировать кошелек по приватному ключу</h3>
        <p class="alert alert-success">/importWalletKey</p>
        <p>Импортировать кошелек</p>

        <p><b>Входные параметры</b></p>
        <p><code>POST</code></p>

        <?= \avatar\services\Params::widget([
            'params' => [
                [
                    'name'        => 'key',
                    'isRequired'  => true,
                    'description' => 'приватный ключ',
                ],
                [
                    'name'        => 'password',
                    'isRequired'  => true,
                    'description' => 'Пароль для доступа к кошельку через этот интерфейс',
                ],
            ]
        ]) ?>

        <p><b>Выходные данные</b></p>
        <pre>{
    address: '...' // адрес кошелька
}</pre>

        <h3 class="page-header">Получить файл JSON кошелька</h3>
        <p class="alert alert-success">/exportWallet</p>
        <p>Получить файл JSON кошелька</p>

        <p><b>Входные параметры</b></p>
        <p><code>POST</code></p>

        <?= \avatar\services\Params::widget([
            'params' => [
                [
                    'name'        => 'user',
                    'isRequired'  => true,
                    'description' => 'Адрес кошелька начинающийся с 0x',
                ],
                [
                    'name'        => 'password',
                    'isRequired'  => true,
                    'description' => 'Пароль от расшифровки файла JSON',
                ],
            ]
        ]) ?>

        <p>Функция проверяет может ли она расшифровать и если нет то выдает ошибку что пароль не верный, а если может то возвращает адрес кошелька</p>
        <p><b>Выходные данные</b></p>
        <pre>{
    fileJson: '...' // текст JSON файла
}</pre>


        <h3 class="page-header">Получить баланс</h3>
        <p class="alert alert-success">/get-balance</p>
        <p>Возвращает баланс кошелька</p>

        <p><b>Входные параметры</b></p>
        <p><code>GET</code></p>
        <?= \avatar\services\Params::widget([
            'params' => [
                [
                    'name'        => 'address',
                    'isRequired'  => true,
                    'description' => 'адрес кошелька начинающийся 0x',
                ]
            ]
        ]) ?>

        <p><b>Выходные данные</b></p>
        <pre>
{
    balance: '0.003223'
    address: '0x'
}
</pre>

        <p><code>unconfirmed</code> - сумма неподтвержденных транзакций, если был вычет с кошелька, то значение будет минусовым, если приход - то равно сумме прихода.</p>
        <p>Ошибки: - 101, неверный кошелек</p>

        <h3 class="page-header">Получить баланс</h3>
        <p class="alert alert-success">/syncing</p>
        <p>Возвращает статус синхронизации</p>
        <p><a href="https://ethereum.stackexchange.com/questions/29222/status-of-ethereum-sync-using-geth-for-beginners" target="_blank">https://ethereum.stackexchange.com/questions/29222/status-of-ethereum-sync-using-geth-for-beginners</a></p>

        <p><code>GET</code></p>
        <p><b>Входные параметры</b>: нет</p>

        <p><b>Выходные данные</b></p>
        <?= \avatar\services\Params::widget([
            'params' => [
                [
                    'name'        => 'currentBlock',
                    'isRequired'  => true,
                    'description' => 'currentBlock',
                ],
                [
                    'name'        => 'highestBlock',
                    'isRequired'  => true,
                    'description' => 'highestBlock',
                ],
                [
                    'name'        => 'knownStates',
                    'isRequired'  => true,
                    'description' => 'knownStates',
                ],
                [
                    'name'        => 'pulledStates',
                    'isRequired'  => true,
                    'description' => 'pulledStates',
                ],
                [
                    'name'        => 'startingBlock',
                    'isRequired'  => true,
                    'description' => 'startingBlock',
                ],
            ]
        ]) ?>



        <h3 class="page-header">Поменять пароль на кошелек</h3>
        <p class="alert alert-success">/change-password</p>
        <p>Меняет пароль на ранее созданный кошелек, при этом нужно указать старый пароль</p>

        <p><b>Входные параметры</b></p>
        <?= \avatar\services\Params::widget([
            'params' => [
                [
                    'name'        => 'address',
                    'isRequired'  => true,
                    'description' => 'адрес кошелька начинающийся 0x',
                ],
                [
                    'name'        => 'passwordOld',
                    'isRequired'  => true,
                    'description' => 'старый пароль',
                ],
                [
                    'name'        => 'passwordNew',
                    'isRequired'  => true,
                    'description' => 'новый пароль',
                ],
            ]
        ]) ?>
        <p><b>Выходные данные</b></p>
        <pre>
'ok'
</pre>

        <p>Ошибки: - 101, неверный пароль</p>



        <h3 class="page-header">Вызвать контракт</h3>
        <p class="alert alert-success">/contract</p>
        <p>Вызывает контракт</p>

        <p><b>Входные параметры</b></p>
        <?= \avatar\services\Params::widget([
                'params' => [
                    [
                        'name'        => 'user',
                        'isRequired'  => true,
                        'description' => 'Адрес кошелька начинающийся 0x от чьего имени будет подписана транзакция',
                    ],
                    [
                        'name'        => 'password',
                        'isRequired'  => true,
                        'description' => 'Пароль от кошелька',
                    ],
                    [
                        'name'        => 'contract',
                        'isRequired'  => true,
                        'description' => 'Адрес контракта начинающийся 0x',
                    ],
                    [
                        'name'        => 'abi',
                        'isRequired'  => true,
                        'description' => 'Интерфейс контракта',
                    ],
                    [
                        'name'        => 'func',
                        'isRequired'  => true,
                        'description' => 'Название функции контракта',
                    ],
                    [
                        'name'        => 'params',
                        'type'        => 'string',
                        'description' => 'Параметры контракта, в массиве зашифрованном в JSON. Например: ' . Html::tag('code', '[1, 2, "0x654sfs64we654asd646"]'),
                    ],
                    [
                        'name'        => 'gasPrice',
                        'type'        => 'int',
                        'description' => 'Цена газа в wei, от 21E9 до 50E9, по умолчанию 21E9',
                    ],
                    [
                        'name'        => 'write',
                        'type'        => 'int',
                        'description' => 'Флаг на чтение. 0 - чтение, 1 - запись. По умолчанию - 0',
                    ],
                ]
        ]) ?>


        <p><b>Выходные данные</b></p>
        <pre>
{
    'transaction' : {
            'txid' : '0x23423424234234...' // Идентификатор иранзакции (хеш)
            }
}</pre>

        <p><b>Ошибки</b></p>
        <pre>
'Error: could not decrypt key with given passphrase ....' - не верный пароль (дословно не могу расшифровать ключ по данному паролю)
'Invalid number of arguments to Solidity function'
'Array.prototype.slice called on null or undefined'
'parameters is not defined'</pre>

        <h3 class="page-header">Рассчитать стоимость вызова контракта</h3>
        <p class="alert alert-success">/calculateCostCall</p>
        <p>Рассчитать стоимость вызова контракта</p>

        <p>Входные параметры такиеже как и у <code>contract</code></p>
        <?= \avatar\services\Params::widget([
                'params' => [
                    [
                        'name'        => 'user',
                        'isRequired'  => true,
                        'description' => 'Адрес кошелька начинающийся 0x от чьего имени будет подписана транзакция',
                    ],
                    [
                        'name'        => 'password',
                        'isRequired'  => true,
                        'description' => 'Пароль от кошелька',
                    ],
                    [
                        'name'        => 'contract',
                        'isRequired'  => true,
                        'description' => 'Адрес контракта начинающийся 0x',
                    ],
                    [
                        'name'        => 'abi',
                        'isRequired'  => true,
                        'description' => 'Интерфейс контракта',
                    ],
                    [
                        'name'        => 'func',
                        'isRequired'  => true,
                        'description' => 'Название функции контракта',
                    ],
                    [
                        'name'        => 'params',
                        'type'        => 'string',
                        'description' => 'Параметры контракта, в массиве зашифрованном в JSON. Например: ' . Html::tag('code', '[1, 2, "0x654sfs64we654asd646"]'),
                    ],
                    [
                        'name'        => 'write',
                        'type'        => 'int',
                        'description' => 'Флаг на чтение. 0 - чтение, 1 - запись. По умолчанию - 0',
                    ],
                ]
        ]) ?>


        <p><b>Выходные данные</b></p>
        <pre>
{
    'data' : <mixed>
}</pre>

        <p><b>Ошибки</b></p>
        <pre>
'Error: could not decrypt key with given passphrase ....' - не верный пароль (дословно: не могу расшифровать ключ по данному паролю)
'Invalid number of arguments to Solidity function'
'Array.prototype.slice called on null or undefined'
'parameters is not defined'</pre>


        <h3 class="page-header">Вычисляет комиссию перед регистрацией контракта</h3>
        <p class="alert alert-success">/calculateCostRegister</p>
        <p>Вычисляет комиссию перед регистрацией контракта</p>

        <p>Входные параметры такие же как и у <code>registerContract</code></p>
        <?= \avatar\services\Params::widget([
            'params' => [
                [
                    'name'        => 'user',
                    'isRequired'  => true,
                    'description' => 'Адрес кошелька начинающийся 0x, кто будет создателем контракта',
                ],
                [
                    'name'        => 'password',
                    'isRequired'  => true,
                    'description' => 'Пароль от кошелька создателя контракта',
                ],
                [
                    'name'        => 'contract',
                    'isRequired'  => true,
                    'description' => 'Код контракта',
                ],
                [
                    'name'        => 'contractName',
                    'isRequired'  => true,
                    'description' => 'Название контракта для инициализации',
                ],
                [
                    'name'        => 'params',
                    'description' => 'Параметры инициализации контракта, в массиве зашифрованном в JSON. Например: ' . Html::tag('code', '[1, 2, "0x654sfs64we654asd646"]'),
                ],
            ]
        ]) ?>


        <p><b>Выходные данные</b></p>
        <pre>
{
    'transaction' : '0x...'
}</pre>

        <h3 class="page-header">Регистрирует контракт</h3>
        <p class="alert alert-success">/registerContract</p>
        <p>Регистрирует контракт</p>

        <p><b>Входные параметры</b></p>
        <?= \avatar\services\Params::widget([
            'params' => [
                [
                    'name'        => 'user',
                    'isRequired'  => true,
                    'description' => 'Адрес кошелька начинающийся 0x, кто будет создателем контракта',
                ],
                [
                    'name'        => 'password',
                    'isRequired'  => true,
                    'description' => 'Пароль от кошелька создателя контракта',
                ],
                [
                    'name'        => 'contract',
                    'isRequired'  => true,
                    'description' => 'Код контракта',
                ],
                [
                    'name'        => 'contractName',
                    'isRequired'  => true,
                    'description' => 'Название контракта для инициализации',
                ],
//                [
//                    'name'        => 'gasPrice',
//                    'type'        => 'int',
//                    'description' => 'Цена газа в wei, от 21E9 до 50E9, по умолчанию 21E9',
//                ],
                [
                    'name'        => 'params',
                    'description' => 'Параметры инициализации контракта, в массиве зашифрованном в JSON. Например: ' . Html::tag('code', '[1, 2, "0x654sfs64we654asd646"]'),
                ],
            ]
        ]) ?>


        <p><b>Выходные данные</b></p>
        <pre>
{
    'transaction' : '0x...'
}</pre>



        <h3 class="page-header">Конвертирует входные переменные для контракта</h3>
        <p class="alert alert-success">/convertConstructorParams</p>

        <p><b>Входные параметры</b></p>
        <?= \avatar\services\Params::widget([
            'params' => [
                [
                    'name'        => 'params',
                    'isRequired'  => true,
                    'description' => 'JSON ' . Html::tag('code', '[{"name":"v1","type":"string"}, ...}]'),
                ],
            ]
        ]) ?>

        <p>Типы данных: <?= Html::a('http://solidity.readthedocs.io/en/develop/types.html', 'http://solidity.readthedocs.io/en/develop/types.html', ['target' => '_blank']) ?></p>

        <p><b>Выходные данные</b></p>
        <pre>
{
    'init' : 'string'
}</pre>


        <h3 class="page-header">Выдает файл JSON</h3>
        <p class="alert alert-success">/exportKeyJson</p>

        <p><b>Входные параметры</b></p>
        <?= \avatar\services\Params::widget([
            'params' => [
                [
                    'name'        => 'address',
                    'isRequired'  => true,
                    'description' => 'Адрес кошелька начиная с 0x',
                ],
                [
                    'name'        => 'password',
                    'isRequired'  => true,
                    'description' => 'Пароль',
                ],
            ]
        ]) ?>


        <p><b>Выходные данные</b></p>
        <pre>
{
    
}</pre>





    </div>
</div>



