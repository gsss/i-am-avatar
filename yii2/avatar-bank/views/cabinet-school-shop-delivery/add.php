<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;
use app\models\UnionCategory;
use yii\db\Query;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/** @var $model \common\models\shop\DeliveryItem */
/** @var $school \common\models\school\School */

$this->title = 'Добавить вариант досавки';

$model->currency_id = 6;

?>
<div class="container">

    <?php \avatar\widgets\SchoolMenu::begin(['school' => $school]) ?>
    <h1 class="page-header"><?= Html::encode($this->title) ?></h1>

    <?php if (!is_null($id = Yii::$app->session->getFlash('form'))) : ?>

        <div class="alert alert-success">
            Успешно добавлено.
        </div>

        <p>
            <a href="<?= \yii\helpers\Url::to(['cabinet-school-shop-delivery/index', 'id' => $school->id]) ?>" class="btn btn-success">Все варианты доставки</a>
            <a href="<?= \yii\helpers\Url::to(['cabinet-school-shop/index', 'id' => $school->id]) ?>" class="btn btn-default">Интернет магазин</a>
        </p>

    <?php else: ?>

        <div class="row">
            <div class="col-lg-8">
                <?php $form = ActiveForm::begin([
                    'options' => ['enctype' => 'multipart/form-data'],
                ]); ?>
                <?= $form->field($model, 'name') ?>
                <?= $form->field($model, 'description') ?>
                <?= $form->field($model, 'price') ?>
                <?= $form->field($model, 'currency_id')->dropDownList(
                    ArrayHelper::map(
                        \common\models\avatar\Currency::find()->all(),
                        'id',
                        'code'
                    ), ['prompt' => '- Ничего не выбрано -']
                ) ?>
                <?= $form->field($model, 'type')->radioList([
                    1 => 'Самовывоз',
                    2 => 'доставка по почте',
                    3 => 'Наложный платеж',
                ]) ?>


                <hr>
                <div class="form-group">
                    <?= Html::submitButton('Добавить', [
                        'class' => 'btn btn-default',
                        'name'  => 'contact-button',
                        'style' => 'width:100%',
                    ]) ?>
                </div>
                <?php ActiveForm::end(); ?>
            </div>
        </div>

    <?php endif; ?>
    <?php \avatar\widgets\SchoolMenu::end() ?>

</div>
