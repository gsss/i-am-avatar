<?php

/** @var $this \yii\web\View */
/** @var $item \common\models\VoteList */

use yii\helpers\Url;
use app\services\GsssHtml;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use app\services\GetArticle\Collection;

$this->title = $item->name;

$isGuest = (Yii::$app->user->isGuest) ? 1 : 0;

function getHtml($text)
{
    $rows = explode("\n", $text);
    $rows2 = [];
    foreach ($rows as $row) {
        $arr = explode(' ', $row);
        $arr2 = [];
        foreach ($arr as $word) {
            $w = trim($word);
            if (\yii\helpers\StringHelper::startsWith($w, 'http')) {
                $w = Html::a($w, $w, ['target' => '_blank']);
            }
            $arr2[] = $w;
        }
        $rows2[] = join(' ', $arr2);
    }

    return join('<br>', $rows2);
}

?>

<div class="container">
    <div class="col-lg-12">
        <h1 class="page-header"><?= Html::encode($this->title) ?></h1>

        <p><?= getHtml($item->content) ?></p>

        <h2 class="page-header">Возможные ответы</h2>
        <?php
        $this->registerJS(<<<JS
$('.buttonAnswer').click(function (e) {
    e.preventDefault();
    e.stopPropagation();
    
    if ({$isGuest} == 1) {
        $('#modalWarning').modal();
    } else {
        if (confirm('Подтвердите ваш ответ')) {
            var button = $(this);
            var id = $(this).data('id');
            ajaxJson({
                url: '/vote/answer' + '?' + 'id' + '=' + id,
                data: {
                    comment: $('#text1').val()
                },
                success: function (ret) {
                    $('#modalInfo').on('hidden.bs.modal', function() {
                        window.location.reload();
                    }).modal();
                },
                errorScript: function(ret) {
                    //alert(ret.data);
                }
            });
        }
    }
});



JS
        );
        ?>
        <?= \yii\grid\GridView::widget([
            'dataProvider' => new \yii\data\ActiveDataProvider([
                'query'      => \common\models\VoteAnswer::find()
                    ->where(['list_id' => $item->id])
                ,
                'pagination' => [
                    'pageSize' => 20,
                ],
            ]),
            'summary'      => '',
            'tableOptions' => [
                'class' => 'table table-striped table-hover',
            ],
            'rowOptions'   => function ($item) {
                $data = [
                    'data'  => ['id' => $item['id']],
                    'role'  => 'button',
                    'class' => 'rowTable',
                ];
                return $data;
            },
            'columns'      => [
                ['class' => '\yii\grid\SerialColumn'],
                'name:text:Формулировка ответа',
                [
                    'header'  => 'Ответить',
                    'content' => function ($item) {
                        if (!Yii::$app->user->isGuest) {
                            if (\common\models\VoteItem::find()->where(['list_id' => $item['list_id'], 'user_id' => Yii::$app->user->id])->exists()) return '';
                        }

                        return Html::tag('button', 'Ответить', ['class' => 'btn btn-primary buttonAnswer', 'data' => ['id' => $item['id']]]);
                    },
                ],
            ],
        ]) ?>

        <?php
        $isShow = false;
        if (!Yii::$app->user->isGuest) {
            if (!\common\models\VoteItem::find()->where(['list_id' => $item->id, 'user_id' => Yii::$app->user->id])->exists()) $isShow = true;
        }
        ?>
        <?php if ($isShow) { ?>
            <p>Комментарий:</p>
            <p><textarea id="text1" class="form-control" rows="5"></textarea></p>
        <?php } ?>



        <?php
        $answerList = \common\models\VoteAnswer::find()
            ->where(['list_id' => $item->id])->all();
        $rows = [];
        /** @var \common\models\VoteAnswer $answer */
        foreach ($answerList as $answer) {
            $rows[] = [
                'name' =>  $answer->name,
                'y'    => (int)\common\models\VoteItem::find()->where(['list_id' => $item->id, 'answer_id' => $answer->id])->count(),

            ];
        }

        ?>
        <?= \cs\Widget\HighCharts\HighCharts::widget([
            'chartOptions' => [
                'chart'   => [
                    'plotBackgroundColor' => null,
                    'plotBorderWidth'     => null,
                    'plotShadow'          => false,
                    'type'                => 'pie',
                ],
                'title'   => [
                    'text' => 'Результаты голосования',
                ],
                'tooltip' => [
                    'pointFormat' => '{series.name}: <b>{point.percentage:.1f}%</b>',
                ],

                'plotOptions' => [
                    'pie' => [
                        'allowPointSelect' => true,
                        'cursor'           => 'pointer',
                        'dataLabels'       => [
                            'enabled' => true,
                            'format'  => '<b>{point.name}</b>: {point.percentage:.1f} %',
                            'style'   => [
                                'color' => new \yii\web\JsExpression('(Highcharts.theme && Highcharts.theme.contrastTextColor) || \'black\''),
                            ],
                        ],
                    ],
                ],
                'series'      => [
                    [
                        'name'         => 'Brands',
                        'colorByPoint' => true,
                        'data'         => $rows,
                    ],
                ],
            ],
        ]);

        ?>


        <h2 class="page-header">Ответы собственников</h2>
        <?php \yii\widgets\Pjax::begin(); ?>
        <?php
        $this->registerJS(<<<JS
$('[data-toggle="tooltip"]').tooltip();
JS
        );
        ?>
        <?= \yii\grid\GridView::widget([
            'dataProvider' => new \yii\data\ActiveDataProvider([
                'query'      => \common\models\VoteItem::find()
                    ->where(['list_id' => $item->id])
                    ->orderBy(['created_at' => SORT_DESC])
                ,
                'pagination' => [
                    'pageSize' => 100,
                ],
            ]),
            'tableOptions' => [
                'class' => 'table table-striped table-hover',
            ],
            'rowOptions'   => function ($item) {
                $data = [
                    'data'  => ['id' => $item['id']],
                    'role'  => 'button',
                    'class' => 'rowTable',
                ];
                return $data;
            },
            'columns'      => [
                'id',
                [
                    'header'  => 'Пользователь',
                    'content' => function ($item) {
                        $i = ArrayHelper::getValue($item, 'user_id', '');
                        $user = \common\models\UserAvatar::findOne($i);

                        return Html::img($user->getAvatar(), [
                            'class'  => "img-circle",
                            'width'  => 80,
                            'height' => 80,
                            'style'  => 'margin-bottom: 0px;',
                            'title'  => $user->getName2(),
                            'data'   => [
                                'toggle' => 'tooltip',
                            ],
                        ]);
                    },
                ],
                [
                    'header'  => 'Ответ',
                    'content' => function ($item) {
                        $i = ArrayHelper::getValue($item, 'answer_id', '');
                        if ($i == '') return '';

                        return \common\models\VoteAnswer::findOne($i)->name;
                    },
                ],
                [
                    'header'  => 'Комментарий',
                    'content' => function ($item) {
                        $v = \yii\helpers\ArrayHelper::getValue($item, 'comment', '');
                        if ($v == '') return '';

                        return nl2br($v);
                    },
                ],
                [
                    'header'  => 'Создано',
                    'content' => function ($item) {
                        $v = \yii\helpers\ArrayHelper::getValue($item, 'created_at', 0);
                        if ($v == 0) return '';

                        return Html::tag('abbr', \cs\services\DatePeriod::back($v, ['isShort' => true]), ['data' => ['toggle' => 'tooltip'], 'title' => Yii::$app->formatter->asDatetime($v)]);
                    },
                ],
            ],
        ]) ?>
        <?php \yii\widgets\Pjax::end(); ?>


        <h2 class="page-header">Комментарии собственников</h2>
        <?= \avatar\modules\Comment\Module::getComments2(6, $item->id); ?>


        <hr>
        <?= $this->render('../blocks/share', [
            'title'       => $item->name,
            'image'       => \yii\helpers\Url::to('/images/controller/site/s1200.jpg', true),
            'description' => \cs\services\Str::sub($item->content, 0, 500),
            'url'         => Url::current([], true),
        ]) ?>
    </div>
</div>

<div class="modal fade" id="modalInfo" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="myModalLabel">Информация</h4>
            </div>
            <div class="modal-body">
                Успешно
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="modalWarning" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="myModalLabel">Информация</h4>
            </div>
            <div class="modal-body">
                Вам необходимо сначала войти
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
            </div>
        </div>
    </div>
</div>