<?php

/** $this \yii\web\View  */
/** @var $school \common\models\school\School */
/** @var $model \avatar\models\validate\SchoolKoop */

use yii\helpers\Url;
use app\services\GsssHtml;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use app\services\GetArticle\Collection;

$this->title = 'Кооператив';

\avatar\assets\Notify::register($this);


?>

<div class="container">

    <h1 class="page-header"><?= Html::encode($this->title) ?></h1>

    <?php $form = \iAvatar777\services\FormAjax\ActiveForm::begin([
            'model'     => $model,
            'success'  => <<<JS
function (ret) {
    $('#modalInfo').on('hidden.bs.modal', function() {
        
    }).modal();
}
JS

        ])?>
        <?= $form->field($model, 'name') ?>
        <?= $form->field($model, 'inn') ?>
        <?= $form->field($model, 'address_isp') ?>
        <?= $form->field($model, 'phone') ?>
        <?= $form->field($model, 'email') ?>
        <?= $form->field($model, 'okved') ?>
        <?= $form->field($model, 'okved_dop') ?>
        <?= $form->field($model, 'input_amount') ?>
        <hr>
    <?php \iAvatar777\services\FormAjax\ActiveForm::end() ?>
</div>

<div class="modal fade" id="modalInfo" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="myModalLabel">Информация</h4>
            </div>
            <div class="modal-body">
                Успешно!
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
            </div>
        </div>
    </div>
</div>