<?php
use common\models\Card;
use common\models\UserAvatar;

/** @var $this yii\web\View */
/** @var $card string */


$this->title = 'Выбор школ для перехода';

/** @var Card $cardObject */
$cardObject = Card::findOne(['number' => $card]);
if (is_null($cardObject)) {
    throw new \cs\web\Exception('Нет такой карты');
}
$user = UserAvatar::findOne($cardObject->user_id);
$rows = \common\models\CardSchoolLink::find()->where(['card_id' => $cardObject->id])->all();
?>

<div class="container" style="padding-bottom: 70px;">
    <div class="col-lg-12">
        <h1 class="page-header text-center">
            Школы
        </h1>
    </div>
    <div class="col-lg-6 col-lg-offset-3">
        <?php /** @var \common\models\CardSchoolLink $row */ ?>
        <?php foreach ($rows as $row) { ?>
            <?php $s = \common\models\school\School::findOne($row->school_id); ?>
            <p><a href="<?= $s->getUrl(['shop/card', 'card' => $cardObject->number]) ?>" class="btn btn-default" style="width: 100%;"><?= $s->name ?></a></p>
        <?php } ?>
    </div>
</div>