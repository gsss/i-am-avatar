<?php
use common\models\Card;
use common\models\UserAvatar;

/* @var $this yii\web\View */
/* @var $card string */


$this->title = 'Что делать?';

/** @var Card $cardObject */
$cardObject = Card::findOne(['number' => $card]);
if (is_null($cardObject)) {
    throw new \cs\web\Exception('Нет такой карты');
}
$user = UserAvatar::findOne($cardObject->user_id);

?>

<div class="container" style="padding-bottom: 70px;">
    <div class="col-lg-12">
        <h1 class="page-header text-center">
            Карта Аватара
        </h1>
    </div>
    <div class="col-lg-6 col-lg-offset-3">

        <p><a href="https://www.i-am-avatar.com/user/<?= $cardObject->user_id ?>" class="btn btn-default" style="width: 100%;">Показать профиль</a></p>
        <p><a href="https://www.i-am-avatar.com/user/<?= $cardObject->user_id ?>" class="btn btn-default" style="width: 100%;">Показать карту</a></p>
        <p><a href="https://www.i-am-avatar.com/user/<?= $cardObject->user_id ?>" class="btn btn-default" style="width: 100%;">Показать профиль и карту</a></p>
        <p><a href="https://www.i-am-avatar.com/shop/order?id=1&partner_id=<?= $cardObject->user_id ?>" class="btn btn-default" style="width: 100%;">Перекинуть на покупку карты</a></p>
    </div>
</div>


