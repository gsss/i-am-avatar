<?php
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $card \common\models\Card */

$this->title = $card->getNumbberWithSpace();


?>

<div class="container" style="padding-bottom: 70px;">
    <div class="col-lg-12">
        <h1 class="page-header text-center">
            <?= $this->title ?>
        </h1>
    </div>
    <div class="col-lg-8 col-lg-offset-2">
        <p class="text-center">
            <img src="<?= $card->getDesign()->image ?>"
                 width="100%">
        </p>

        <?= \yii\grid\GridView::widget([
            'dataProvider' => new \yii\data\ActiveDataProvider([
                'query' => $card->getBillingListObject()
            ]),
            'columns' => [
                'id',
                'name',
                'address',
            ]
        ]) ?>
    </div>
</div>


