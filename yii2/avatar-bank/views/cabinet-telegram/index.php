<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;

/** @var $this yii\web\View */
/** @var $form yii\bootstrap\ActiveForm */

$this->title = 'Telegram';

/** @var \common\models\UserAvatar $user */
$user = Yii::$app->user->identity;

$this->registerJs(<<<JS

$('.buttonPin').click(function(e) {
    ajaxJson({
        url: '/cabinet-telegram/pin',
        success: function(ret) {
            window.location.reload();
        }
    });
});

$('.buttonUnPin').click(function(e) {
    ajaxJson({
        url: '/cabinet-telegram/un-pin',
        success: function(ret) {
            window.location.reload();
        }
    });
});

JS
);

?>
<div class="container">
    <div class="col-lg-12">
        <h1 class="page-header"><?= Html::encode($this->title) ?></h1>

    </div>
    <div class="col-lg-8">

        <?php if (!empty($user->telegram_chat_id)): ?>

            <div class="alert alert-info">
                Аккаунт: <?= (empty($user->telegram_username))? $user->telegram_chat_id . '(chat_id)' : $user->telegram_username ?>
            </div>

            <button class="btn btn-primary buttonUnPin">Отсоединить</button>

        <?php else: ?>

            <p>Инструкция:</p>
            <ul>
                <li>Зайди в бот <a href="tg://resolve?domain=qualitylive_bot" target="_blank">@qualitylive_bot</a></li>
                <li>Напиши "Привет"</li>
                <li>Ответь что у тебя есть аккаунт уже на платформе</li>
                <li>Укажи почту</li>
                <li>Далее у тебя два пути. Ты можешь выбрать любой</li>
                <ul>
                    <li>Здесь обновить страницу и нажать кнопку "Присоединить"</li>
                    <li>Зайти на почту и в письме перейти по ссылке и ты перейдешь сюда.</li>
                </ul>
            </ul>

            <?php if (\common\models\UserTelegramTemp::findOne(['email' => Yii::$app->user->identity->email])) { ?>
                <button class="btn btn-primary buttonPin" style="margin-bottom: 30px;">Присоединить</button>
            <?php } ?>

        <?php endif; ?>
    </div>
    <div class="col-lg-4">
        <?= $this->render('../cabinet/_menu') ?>
    </div>
</div>



