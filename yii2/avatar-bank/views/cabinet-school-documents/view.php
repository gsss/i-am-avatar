<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;
use app\models\UnionCategory;

/** @var $this yii\web\View */
/** @var $document \common\models\UserDocument */
/** @var $school \common\models\school\School */

$this->title = $document->name;

$v = \yii\helpers\ArrayHelper::getValue($document, 'file', 0);
$file = pathinfo($v);

$html = Html::img('/images/controller/cabinet/documents/file.png', ['width' => 50]) . '.' . strtoupper($file['extension']);


\avatar\assets\Notify::register($this);
\avatar\assets\Clipboard::register($this);


$this->registerJs(<<<JS

var clipboard = new Clipboard('.buttonCopy');
clipboard.on('success', function(e) {

    new Noty({
        timeout: 1000,
        theme: 'sunset',
        type: 'success',
        layout: 'bottomLeft',
        text: 'Скопировано'
    }).show();

});


JS
);


?>
<?php $ds = \common\models\UserDigitalSign::findOne(['user_id' => Yii::$app->user->id]);  ?>

<div class="container">
    <div class="col-lg-12">
        <h1 class="page-header"><?= Html::encode($this->title) ?></h1>

    </div>

    <div class="col-lg-4">
        <?= $this->render('_tree', ['school' => $school]) ?>
    </div>
    <div class="col-lg-8">

        <table class="table table-hover table-striped">
            <tr>
                <td style="text-align: right;">ID</td>
                <td><?= $document->id ?></td>
            </tr>
            <tr>
                <td style="text-align: right;">Наименование</td>
                <td><?= $document->name ?></td>
            </tr>
            <tr>
                <td style="text-align: right;">Hash</td>
                <td><?= Html::tag('code', $document->hash) ?></td>
            </tr>
            <?php if ($document->type_id == \common\models\UserDocument::TYPE_ETHEREUM)  { ?>
                <tr>
                    <td style="text-align: right;">TXID</td>
                    <td><?= Html::tag('code', Html::a($document->txid, $ethScan . '/tx/' . $document->txid, ['target' => '_blank'])) ?></td>
                </tr>
            <?php } ?>

            <tr>
                <td style="text-align: right;">Data</td>
                <td><?= ($document->data) ? Html::tag('pre', $document->data) : '' ?></td>
            </tr>
            <tr>
                <td style="text-align: right;">Добавлен</td>
                <td><?= Yii::$app->formatter->asDatetime($document->created_at) ?></td>
            </tr>
            <tr>
                <td style="text-align: right;">Файл</td>

                <td><a href="<?= $document->file ?>" target="_blank"><?= $html ?></td>
            </tr>
        </table>

        <?php $rows = \common\models\UserDocumentSignature::find()->where(['document_id' => $document->id])->all(); ?>
        <?php if (count($rows) > 0) { ?>
            <h3 class="page-header">Подписи</h3>



            <?php \yii\widgets\Pjax::begin(); ?>
            <?php
            $this->registerJS(<<<JS
$('[data-toggle="tooltip"]').tooltip();


$('.rowTable').click(function() {
});
JS
            );
            ?>
            <?= \yii\grid\GridView::widget([
                'dataProvider' => new \yii\data\ActiveDataProvider([
                    'query'      => \common\models\UserDocumentSignature::find()->where(['document_id' => $document->id])
                    ,
                    'pagination' => [
                        'pageSize' => 20,
                    ],
                ]),
                'tableOptions' => [
                    'class' => 'table table-striped table-hover',
                ],
                'rowOptions'   => function ($item) {
                    $data = [
                        'data'  => ['id' => $item['id']],
                        'role'  => 'button',
                        'class' => 'rowTable'
                    ];
                    return $data;
                },
                'columns'      => [
                    'id',
                    [
                        'header'  => '',
                        'content' => function ($item) {
                            return Html::img('/images/controller/cabinet/documents-view/blockchain-blog-620x347.png', ['width' => 50]);
                        }
                    ],
                    [
                        'header'  => 'Автор',
                        'content' => function ($item) {
                            $i = \yii\helpers\ArrayHelper::getValue($item, 'user_id', '');
                            if ($i == '') return '';
                            $u = \common\models\UserAvatar::findOne($i);

                            return Html::img(
                                $u->getAvatar(),
                                [
                                    'class'  => "img-circle",
                                    'width'  => 30,
                                    'style'  => 'margin-bottom: 0px;',
                                    'data'  => [
                                        'toggle'         => 'tooltip',
                                    ],
                                    'title' => $u->getName2(),
                                ]);
                        },
                    ],
                    [
                        'header'  => 'Подпись',
                        'content' => function ($item) {
                            return Html::tag('code', substr($item['signature'],0,24) . '...', [
                                'data'  => [
                                    'toggle'         => 'tooltip',
                                    'clipboard-text' => $item['signature'],
                                ],
                                'class' => 'buttonCopy',
                                'title' => 'Нажми чтобы скопировать',
                            ]);
                        }
                    ],
                    [
                        'header'  => 'Создано',
                        'content' => function ($item) {
                            $v = \yii\helpers\ArrayHelper::getValue($item, 'created_at', 0);
                            if ($v == 0) return '';

                            return Html::tag('abbr', \cs\services\DatePeriod::back($v, ['isShort' => true]), ['data' => ['toggle' => 'tooltip'], 'title' => Yii::$app->formatter->asDatetime($v)]);
                        }
                    ],

                ],
            ]) ?>
            <?php \yii\widgets\Pjax::end(); ?>






        <?php } ?>

        <?php if (is_null($ds)) {  ?>

            <p><a class="btn btn-default" href="/cabinet-digital-sign/index">Получить подпись</a></p>

        <?php } else {  ?>




            <?php

            if ($ds->type_id == \common\models\UserDigitalSign::TYPE_SAVE_SYSTEM) {
                $this->registerJs(<<<JS
$('.buttonSign').click(function(e) {
    if (confirm('Подтверждаете подпись?')) {
        ajaxJson({
            url: '/cabinet-school-documents/sign',
            data: {
                id: {$document->id}
            },
            success: function(ret) {
                $('#modalInfo2').on('hidden.bs.modal', function() {
                    window.location = '/cabinet-school-documents/view?id=' + {$document->id};
                }).modal();
            }
        });
    }
});
JS
                );
            }
            if ($ds->type_id == \common\models\UserDigitalSign::TYPE_VER2) {

                $this->registerJs(<<<JS
$('.buttonSign').click(function(e) {
    $('#modalInfo').modal();
});
JS
                );

            }

            ?>
            <p><button class="btn btn-default buttonSign" data-id="<?= $document->id ?>">Подписать</button></p>
        <?php }  ?>

    </div>
</div>

<?php if (!is_null($ds)) {?>
    <div class="modal fade" id="modalInfo" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Подпись документа</h4>
                </div>
                <div class="modal-body">


                    <?php
                    $model = new \avatar\models\validate\CabinetDocumentSignVer2();
                    $model->message = strtolower($document->hash);
                    $model->id = $document->id;
                    ?>
                    <p>HASH:</p>

                    <p><?= Html::tag('code', $model->message, [
                            'class' => 'buttonCopy',
                        ]); ?></p>

                    <?php $form = \iAvatar777\services\FormAjax\ActiveForm::begin([
                        'model'   => $model,
                        'formUrl'   => '/cabinet-school-documents/sign2',
                        'success' => <<<JS
function (ret) {
    $('#modalInfo2').on('hidden.bs.modal', function() {
        window.location = '/cabinet-school-documents/view?id=' + {$document->id};
    }).modal();
}
JS

                    ]) ?>
                    <p>Address: <span class="label label-info" style="font-family: MONOSPACE; font-size: 10pt;"><?= $ds->address ?></span></p>
                    <?= Html::activeHiddenInput($model, 'message') ?>
                    <?= Html::activeHiddenInput($model, 'id') ?>
                    <?= $form->field($model, 'sign', ['inputOptions' => ['style' => 'font-family: MONOSPACE;']]) ?>
                    <hr>
                    <p><a href="http://localhost:8080/" target="_blank">Открыть окно для подписи</a></p>
                    <hr>
                    <?php \iAvatar777\services\FormAjax\ActiveForm::end(['label' => 'Продписать']) ?>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
                </div>
            </div>
        </div>
    </div>
<?php }?>


<div class="modal fade" id="modalInfo2" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Информация</h4>
            </div>
            <div class="modal-body">
                Успешно!
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
            </div>
        </div>
    </div>
</div>

