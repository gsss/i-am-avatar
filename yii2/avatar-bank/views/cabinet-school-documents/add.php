<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;
use app\models\UnionCategory;

/** @var $this yii\web\View */
/** @var $form yii\bootstrap\ActiveForm */
/** @var $model \avatar\models\forms\SchoolDocument */
/** @var $school \common\models\school\School */

$this->title = 'Добавление документа';

/**

 */

$this->registerJs(<<<JS
JS
);
?>
<div class="container">

    <div class="col-lg-12">
        <h1 class="page-header"><?= Html::encode($this->title) ?></h1>

    </div>

    <div class="col-lg-4">
        <?= $this->render('_tree', ['school' => $school]) ?>
    </div>
    <div class="col-lg-8">

        <div class="collapse in" id="step1">
            <?php $form = \iAvatar777\services\FormAjax\ActiveForm::begin([
                'model'   => $model,
                'success' => <<<JS
function (ret) {
    $('#modalInfo').on('hidden.bs.modal', function() {
        window.location = '/cabinet-school-documents/index?id=' + {$school->id};
    }).modal(); 
  
}
JS

            ]); ?>
            <?= $form->field($model, 'name') ?>
            <?= $form->field($model, 'data')->textarea(['rows' => 5]) ?>
            <?= $form->field($model, 'file') ?>

            <hr class="featurette-divider">

            <?php \iAvatar777\services\FormAjax\ActiveForm::end(); ?>

        </div>

    </div>
</div>


<div class="modal fade" id="modalInfo" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Информация</h4>
            </div>
            <div class="modal-body">
                Успешно!
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
            </div>
        </div>
    </div>
</div>
