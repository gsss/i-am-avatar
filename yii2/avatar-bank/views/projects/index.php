<?php
use common\models\investment\Project;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
$this->title = 'Проекты';

?>

<div class="container" style="padding-bottom: 70px;">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header text-center">
                Проекты
            </h1>
        </div>
    </div>
    <div class="row">

        <?php /** @var  Project  $project */ ?>
        <?php
        /**
         * 1 - в ожидании
         * 2 - уже прошло
         * 3 - В процессе
         */

        $rows = Project::find()
            ->where(['status' => Project::STATUS_ASSEPTED])
            ->andWhere([
                'and',
                [
                    'or',
                    ['is_hide_client' => null],
                    ['is_hide_client' => 0],
                ],
                [
                    'or',
                    ['is_hide_platform' => null],
                    ['is_hide_platform' => 0],
                ],
            ])
            ->orderBy(['if(time_start > '.time().',1,if (time_end < '.time().',2,3))' => SORT_DESC])
            ->all()
        ;

        ?>

        <?php foreach ($rows as $project) { ?>
            <div class="col-lg-3">
                <div class="thumbnail">
                    <a href="<?= Url::to([ 'projects/item', 'id' => $project->id ]) ?>">
                        <img alt="<?= $project->name ?>" src="<?= $project->image ?>" width="100%">
                    </a>

                    <div class="caption" style="height: 250px;">
                        <h3><?= $project->name ?></h3>
                        <p><?= \cs\services\Str::sub(strip_tags($project->content), 0, 200) . '...' ?></p>
                    </div>



                    <?php $ico = $project->getIco(); ?>
                    <?php if (!is_null($ico)) { ?>
                        <?php if (!is_null($ico->all_tokens) and !is_null($ico->remained_tokens)) { ?>
                            <?php $percent = (int)((($ico->all_tokens - $ico->remained_tokens) / $ico->all_tokens) * 100); ?>
                        <?php } else { ?>
                            <?php $percent = 0; ?>
                        <?php } ?>
                    <?php } else { ?>
                        <?php $percent = 0; ?>
                    <?php } ?>

                    <div class="progress" style="margin-top: 20px;" data-toggle="tooltip" title="<?= $percent ?>%" data-placement="bottom">
                        <div class="progress-bar" role="progressbar" aria-valuenow="<?= $percent ?>" aria-valuemin="0" aria-valuemax="100" style="width: <?= $percent ?>%;" id="progressBar">
                            <span class="sr-only"><?= $percent ?>% Complete</span>
                        </div>
                    </div>

                    <p>
                        <span class="label label-info">ICO</span>

                        <?php if ($project->time_start > time()) { ?>
                            <?php // в ожидании ?>
                            <span class="label label-info">в ожидании</span>
                        <?php } else { ?>
                            <?php if ($project->time_end < time()) { ?>
                                <?php // уже прошло ?>
                                <span class="label label-danger">уже прошло</span>
                            <?php } else { ?>
                                <?php // В процессе ?>
                                <span class="label label-success">В процессе</span>
                            <?php } ?>
                        <?php } ?>
                    </p>
                    <p>
                        <a  class="btn btn-success" href="<?= Url::to([ 'projects/item', 'id' => $project->id ]) ?>" style="width: 100%;">
                            Подробнее
                        </a>
                    </p>
                </div>
            </div>
        <?php }?>

    </div>
</div>


