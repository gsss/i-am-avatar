<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;
use app\models\UnionCategory;
use yii\db\Query;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \common\models\school\PluginExportSettings */
/* @var $school \common\models\school\School */

$this->title = 'Добавить настройку';

?>
<div class="container">

    <?php \avatar\widgets\SchoolMenu::begin(['school' => $school]) ?>
    <h1 class="page-header"><?= Html::encode($this->title) ?></h1>

    <?php if (!is_null($id = Yii::$app->session->getFlash('form'))) : ?>

            <div class="alert alert-success">
                Успешно добавлено.
            </div>

        <?php else: ?>


            <div class="row">
                <div class="col-lg-8">
                    <?php $form = ActiveForm::begin([
                        'options' => ['enctype' => 'multipart/form-data'],
                    ]); ?>
                    <?= $form->field($model, 'name') ?>
                    <?= $form->field($model, 'content')->textarea(['rows' => 10]) ?>
                    <?= $form->field($model, 'plugin_id')->dropDownList(ArrayHelper::map(
                        \common\models\school\PluginExport::find()->all(),
                        'id',
                        'name'
                    )) ?>


                    <hr>
                    <div class="form-group">
                        <?= Html::submitButton('Добавить', [
                            'class' => 'btn btn-default',
                            'name'  => 'contact-button',
                            'style' => 'width:100%',
                        ]) ?>
                    </div>
                    <?php ActiveForm::end(); ?>
                </div>
            </div>

        <?php endif; ?>
    <?php \avatar\widgets\SchoolMenu::end() ?>


</div>
