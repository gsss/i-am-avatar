<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;
use app\models\UnionCategory;
use yii\db\Query;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \avatar\models\forms\Project */

$this->title = $model->name;

?>
<div class="container">
    <div class="col-lg-12">
        <h1 class="page-header"><?= Html::encode($this->title) ?></h1>

        <?php if (Yii::$app->session->hasFlash('form')): ?>

            <div class="alert alert-success">
                Успешно обновлено.
            </div>
            <a href="<?= \yii\helpers\Url::to(['cabinet-projects/index']) ?>" class="btn btn-success">Проекты</a>
        <?php else: ?>


            <?php $form = ActiveForm::begin([
                'id'      => 'contact-form',
                'options' => ['enctype' => 'multipart/form-data'],
            ]); ?>
            <?= $form->field($model, 'name') ?>
            <?= $form->field($model, 'content')->widget('\common\widgets\HtmlContent\HtmlContent', ['value' => $model->content]) ?>
            <?= $form->field($model, 'image')->widget(
                '\common\widgets\FileUpload3\FileUpload',
                ArrayHelper::merge(
                    \avatar\models\forms\Project::$imageOptions['widget'][1],
                    [
                        'value' => $model->image,
                        'attribute' => 'image',
                        'model' => $model
                    ]
                )
            ) ?>
            <?= $form->field($model, 'time_start_date')->widget('\common\widgets\DatePicker\DatePicker', ['dateFormat' => 'php:d.m.Y']) ?>
            <?= $form->field($model, 'time_start_time')->widget(
                '\kartik\time\TimePicker',
                [
                    'pluginOptions' => [
                        'showSeconds'  => true,
                        'showMeridian' => false,
                        'minuteStep'   => 1,
                        'secondStep'   => 5,
                    ]
                ]) ?>

            <?= $form->field($model, 'time_end_date')->widget('\common\widgets\DatePicker\DatePicker', ['dateFormat' => 'php:d.m.Y']) ?>
            <?= $form->field($model, 'time_end_time')->widget(
                '\kartik\time\TimePicker',
                [
                    'pluginOptions' => [
                        'showSeconds'  => true,
                        'showMeridian' => false,
                        'minuteStep'   => 1,
                        'secondStep'   => 5,
                    ]
                ]) ?>

            <hr>
            <div class="form-group">
                <?= Html::submitButton('Обновить', [
                    'class' => 'btn btn-default',
                    'name'  => 'contact-button',
                    'style' => 'width:100%',
                ]) ?>
            </div>
            <?php ActiveForm::end(); ?>
        <?php endif; ?>
    </div>

</div>
