<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;
use yii\db\Query;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $project \common\models\investment\Project */

$this->title = $project->name;


$this->registerJs(<<<JS
$('.rowTable').click(function() {
    window.location = '/cabinet-projects/request?' +
     'id' + '=' + $(this).data('id')
     ;
});

JS
);

?>

<style>
    .textDecorated {
        text-decoration-line: underline;
        text-decoration-style: dotted;
    }
</style>

<div class="container">
    <div class="col-lg-12">
        <h1 class="page-header"><?= Html::encode($this->title) ?></h1>

        <?php
        $this->registerJs(<<<JS
                
$('.buttonSend').click(function(e) {
    e.preventDefault();
    e.stopPropagation();
    var bid = $(this).data('id');
    $('#modalTransaction').attr('data-id', bid);
    $('#modalTransaction').modal();
});

$('.buttonLink').click(function(e) {
    e.preventDefault();
    e.stopPropagation();
    window.location = $(this).attr('href');
});

JS
        );

        ?>
        <?= \yii\grid\GridView::widget([
            'dataProvider' => new \yii\data\ActiveDataProvider([
                'query' => \common\models\investment\IcoRequest::find()
                    ->where(['ico_request.ico_id' => $project->id])
                    ->innerJoin('paysystems_config', 'paysystems_config.id = ico_request.paysystem_config_id')
                    ->innerJoin('paysystems', 'paysystems.id = paysystems_config.paysystem_id')
                    ->leftJoin('ico_request_success', 'ico_request.id = ico_request_success.id')
                    ->select([
                        'ico_request.*',
                        'paysystems_config.paysystem_id',
                        'paysystems.currency',
                        'paysystems.title',
                        'paysystems.image',
                        'ico_request_success.txid',
                        'ico_request_success.created_at as txid_created_at',
                    ])
                    ->orderBy(['ico_request.created_at' => SORT_DESC])
                    ->asArray()
            ]),
            'tableOptions' => [
                'class' => 'table table-hover table-striped',
                'style' => 'width: auto;',
                'id'    => 'tableTransaction',
            ],
            'rowOptions'   => function ($item) {
                $params = [
                    'data'  => [
                        'id' => $item['id'],
                    ],
                    'role'  => 'button',
                    'class' => 'rowTable'
                ];

                return $params;
            },
            'columns' => [
                'id',
                [
                    'header'  => 'Платежная система',
                    'content' => function ($item) {
                        $v = \yii\helpers\ArrayHelper::getValue($item, 'image', '');

                        return Html::img($v, [
                            'width' => 50,
                            'class' => 'img-circle',
                            'title' => \yii\helpers\ArrayHelper::getValue($item, 'title', ''),
                            'data'  => [
                                'toggle'    => 'tooltip',
                                'placement' => 'bottom',
                            ],
                        ]);
                    },
                ],
                [
                    'header'    => 'Создано',
                    'attribute' => 'created_at',
                    'content'   => function ($item) {
                        $v = \yii\helpers\ArrayHelper::getValue($item, 'created_at', 0);
                        if ($v == 0) return '';

                        return Html::tag('span', \cs\services\DatePeriod::back($v, ['isShort' => true]), [
                            'data'  => [
                                'toggle'    => 'tooltip',
                                'placement' => 'bottom',
                            ],
                            'title' => Yii::$app->formatter->asDatetime($v),
                            'class' => 'textDecorated'
                        ]);
                    },
                ],
                [
                    'header'         => 'Токенов',
                    'format'         => ['decimal', 2],
                    'attribute'      => 'tokens',
                    'headerOptions'  => [
                        'style' => 'text-align:right',
                    ],
                    'contentOptions' => [
                        'style' => 'text-align:right',
                    ],

                ],
                [
                    'header'  => 'Пользователь',
                    'content' => function ($item) {
                        $user_id = \yii\helpers\ArrayHelper::getValue($item, 'user_id', '');
                        $email = \yii\helpers\ArrayHelper::getValue($item, 'email', '');
                        if (is_null($user_id)) {
                            return $email;
                        }
                        $user = \common\models\UserAvatar::findOne($user_id);
                        $v = $user->getAvatar();

                        return
                            Html::a(Html::img($v, [
                                'width' => 50,
                                'class' => 'img-circle',
                                'title' => $user->email,
                                'data'  => [
                                    'toggle'    => 'tooltip',
                                    'placement' => 'bottom',
                                ],
                            ]), ['user/index', 'id' => $user_id])
                            ;
                    },
                ],
                [
                    'header'  => 'Транзакция',
                    'content' => function ($item) {
                        $request = \common\models\investment\IcoRequest::findOne($item['id']);
                        try {
                            $payment = $request->getBilling()->getPayment();
                        } catch (Exception $e) {
                            return '';
                        }
                        $txid = $payment->getTransaction();
                        if (is_null($txid)) {
                            $txid = '';
                        }
                        if ($txid == '') return '';

                        return Html::tag('code', substr($txid,0,8).'...');
                    },
                ],
                [
                    'header'  => 'Оплачено?',
                    'content' => function ($item) {
                        $request = \common\models\investment\IcoRequest::findOne($item['id']);
                        try {
                            $payment = $request->getBilling()->getPayment();
                        } catch (Exception $e) {
                            return '';
                        }
                        $txid = $payment->getTransaction();
                        if (is_null($txid)) {
                            $txid = '';
                        }
                        if ($txid == '') return Html::tag('span', 'Нет', ['class' => 'label label-default']);
                        return Html::tag('span', 'Да', ['class' => 'label label-success']);
                    },
                ],
                [
                    'header'    => 'Отправлено',
                    'content'   => function ($item) {
                        $v = \yii\helpers\ArrayHelper::getValue($item, 'txid', '');
                        if ($v == '') return '';
                        $provider = new \avatar\modules\ETH\ServiceEtherScan();

                        return Html::a(
                            Html::tag('code', substr($v, 0, 8) . '...', []),
                            $provider->getUrl('/tx/' . $v),
                            [
                                'target' => '_blank',
                                'class'  => 'buttonLink',
                            ]
                        );
                    },
                ],
                [
                    'header'         => '',
                    'contentOptions' => [
                        'nowrap' => 'nowrap',
                    ],
                    'content'        => function ($item) {
                        $v = \yii\helpers\ArrayHelper::getValue($item, 'txid_created_at', 0);
                        if ($v == 0) return '';

                        return Html::tag('span', \cs\services\DatePeriod::back($v, ['isShort' => true]), [
                            'data'  => [
                                'toggle'    => 'tooltip',
                                'placement' => 'bottom',
                            ],
                            'title' => Yii::$app->formatter->asDatetime($v),
                            'class' => 'textDecorated'
                        ]);
                    },
                ],
            ]

        ]) ?>
    </div>

</div>
