<?php


use common\models\investment\IcoRequest;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $project \common\models\investment\Project */

$this->title = $project->name;


$list = $project->payments_list_id;
$IcoID = $project->id;

$query = \common\models\PaySystemListItem::find()
    ->where(['paysystems_config_list.id' => $list])
    ->innerJoin('paysystems_config', 'paysystems_config.parent_id = paysystems_config_list.id')
    ->innerJoin('paysystems', 'paysystems.id = paysystems_config.paysystem_id')
    ->innerJoin('currency', 'currency.code = paysystems.currency')
    ->groupBy([
        'currency.id',
    ])
    ->select([
        'currency.code',
        'currency.title',
        'currency.image',
        'currency.id',
    ])
    ->asArray();
$model = new \avatar\models\forms\Ico(['icoId' => $IcoID]);
$ico = $project->getIco();


?>


<style>
    .download-section {
        width: 100%;
        padding: 50px 0;
        background: url('<?= \common\widgets\FileUpload3\FileUpload::getOriginal($project->image) ?>') no-repeat top center scroll;
        background-color: #fff;
        -webkit-background-size: cover;
        -moz-background-size: cover;
        background-size: cover;
        -o-background-size: cover;
    }
</style>

<section id="contact" class="content-section text-center" style="">
    <div class="download-section" style="
    border-bottom: 1px solid #87aad0;
    border-top: 1px solid #87aad0;
    height: 400px;
    ">
        <div class="container">
            <div class="col-lg-8 col-lg-offset-2" style="color: #fff; margin-top: 100px;">
            </div>
        </div>
    </div>
</section>


<div class="container" style="padding-bottom: 70px;">
    <div class="row">
        <div class="col-lg-8 col-lg-offset-2">
            <h1 style="font-size: 56px;" class="text-center page-header"><?= $project->name ?></h1>
            <?= $project->content ?>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-8 col-lg-offset-2">
            <hr>
            <p>
                <?php $date = null; ?>
                <?php if ($project->time_start > time()) { ?>
                    <?php // в ожидании ?>
                    <span class="label label-info">в ожидании</span>
                    <?php $date = Yii::$app->formatter->asDatetime($project->time_start, 'php:Y-m-d 00:00:00'); ?>
                <?php } else { ?>
                    <?php if ($project->time_end < time()) { ?>
                        <?php // уже прошло ?>
                        <span class="label label-danger">уже прошло</span>
                    <?php } else { ?>
                        <span class="label label-success">В процессе</span>
                        <?php $date = Yii::$app->formatter->asDatetime($project->time_end, 'php:Y-m-d 00:00:00'); ?>
                    <?php } ?>
                <?php } ?>
            </p>
            <?php if (!is_null($date)) { ?>
                <?php $this->registerCssFile('/files/FlipClock-master/compiled/flipclock.css'); ?>
                <?php $this->registerJsFile('/files/FlipClock-master/compiled/flipclock.js', ['depends' => ['yii\web\JqueryAsset']]); ?>
                <?php $this->registerJs(<<<JS
var clock;

var d2 = new Date('{$date}');    
var d1 = new Date();
var delta = d2.getTime() - d1.getTime();
var deltaInt = parseInt(delta / 1000);

clock = $('.clock').FlipClock(deltaInt, {
    clockFace: 'DailyCounter',
    autoStart: false,
    callbacks: {
        stop: function() {
            $('.message').html('The clock has stopped!')
        }
    },
    language: 'ru'
});

clock.setCountdown(true);
clock.start();


JS
                ); ?>
                <div class="clock text-center" style="margin: 50px 0px 50px 0px;"></div>
            <?php } ?>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-4 col-lg-offset-4">
            <h3 class="page-header text-center">ICO продажа токенов</h3>
            <?php
            $currency = $ico->getCurrency();

            $image = $currency->image;
            $code = $currency->code;
            $address = $currency->getToken()->address;
            $title = $currency->title;

            /** @var float $backersCount Кол-во человек вложивших в проект */
            $backersCount = IcoRequest::find()
                ->where(['is_paid' => 1])
                ->select(['user_id'])
                ->andWhere(['ico_id' => $IcoID])
                ->groupBy(['user_id'])
                ->count();

            ?>

            <?php $currencyObjectToken = \common\models\avatar\Currency::findOne($ico->kurs_currency_id) ?>

            <p>1 <?= $code ?> = <?= Yii::$app->formatter->asDecimal($ico->kurs, $currencyObjectToken->decimals) ?> <?= $currencyObjectToken->code ?></p>
            <p>Всего: <?= Yii::$app->formatter->asDecimal($ico->all_tokens, 0) ?></p>
            <p>с: <?= Yii::$app->formatter->asDatetime($project->time_start) ?></p>
            <p>по: <?= Yii::$app->formatter->asDatetime($project->time_end) ?></p>




            <h4 class="text-center">
                <?= Yii::$app->formatter->asDecimal($ico->remained_tokens, 0) ?> <small title="<?= $title ?>" data-toggle="tooltip"><?= $code ?></small>
            </h4>
            <p class="text-center">
                <img
                        class="img-circle"
                        src="<?= $image ?>"
                        style="width: 100%;max-width: 150px;"></p>
            <?php $url = (new \avatar\modules\ETH\ServiceEtherScan())->getUrl('/token/' . $address) ?>
            <p class="text-center" title="Адрес токена" data-toggle="tooltip" data-placement="bottom">
                <code>
                    <a href="<?= $url ?>" target="_blank">
                        <?= $address ?>
                    </a>
                </code>
            </p>


        </div><!-- /.col-lg-6 -->
    </div><!-- /.row -->


    <?php if ($project->time_start > time()) { ?>
        <?php // в ожидании ?>
    <?php } else { ?>
        <?php if ($project->time_end < time()) { ?>
            <?php // уже прошло ?>
        <?php } else { ?>
            <?php // В процессе ?>
            <hr>
            <div class="row">
                <div class="col-lg-4 col-lg-offset-4">

                    <p>Осталось: <?= Yii::$app->formatter->asDecimal($ico->remained_tokens, 0) ?></p>
                    <p>Кол-во покупателей: <?= Yii::$app->formatter->asDecimal($backersCount, 0) ?></p>

                    <?php $percent = (int)((($ico->all_tokens - $ico->remained_tokens) / $ico->all_tokens) * 100); ?>

                    <div class="progress" style="margin-top: 20px;" data-toggle="tooltip" title="<?= $percent ?>%" data-placement="bottom">
                        <div class="progress-bar" role="progressbar" aria-valuenow="<?= $percent ?>" aria-valuemin="0" aria-valuemax="100" style="width: <?= $percent ?>%;" id="progressBar">
                            <span class="sr-only"><?= $percent ?>% Complete</span>
                        </div>
                    </div>


                    <?php $form = \yii\bootstrap\ActiveForm::begin(['options' => ['id' => 'formBuy']]) ?>

                    <?php
                    if (Yii::$app->user->isGuest) {
                        $options = ['options' => ['class' => 'form-group']];
                    } else {
                        $options = ['options' => ['class' => 'form-group', 'style' => 'display:none;']];
                    }
                    ?>

                    <?= Html::activeHiddenInput($model, 'icoId'); ?>

                    <?= $form->field($model, 'email', $options)
                        ->hint('Почта для регистрации')
                    ?>

                    <?= $form->field($model, 'address', ['inputOptions' => ['data' => ['toggle' => 'tooltip'], 'title' => 'Если поле вы оставите пустым то платформа создаст для вас кошелек для хранения']])
                        ->hint('Адрес куда будут доставлены токены (не обязательно)')
                        ->label('Адрес кошелька ETH')
                    ?>

                    <?= $form->field($model, 'tokens')
                        ->hint('Введите кол-во токенов которое вы хотите приобрести')
                        ->label('Кол-во токенов')
                    ?>

                    <?php \yii\bootstrap\ActiveForm::end() ?>
                </div><!-- /.col-lg-6 -->
            </div><!-- /.row -->
            <div class="row">
                <div class="col-lg-12">
                    <?php /** @var Currency $currency */ ?>
                    <?php
                    $this->registerJs(<<<JS
$('#ico-tokens').on('input', function(e) {
    $('.buttonCurrency').each(function(i,v) {
        var o = $(v);
        var price = o.data('price');
        var val = $('#ico-tokens').val();
        var find = o.parent().parent().find('code');
        var findItem = find[0];
        $(findItem).html(formatAsDecimal2(val * price, ',', 8));
    });
});
JS
                    );

                    ?>
                    <?php foreach ($query->all() as $currency) { ?>
                        <div class="col-lg-1">
                            <p class="text-center"><code><?= Yii::$app->formatter->asDecimal(0, 8)?></code></p>
                            <p>
                                <?= Html::img($currency['image'], [
                                    'width' => '100%',
                                    'class' => 'img-circle buttonCurrency',
                                    'title' => $currency['title'],
                                    'data'  => [
                                        'toggle' => 'tooltip',
                                        'id'     => $currency['id'],
                                        'price'  => \common\models\avatar\Currency::convert($ico->kurs, $currencyObjectToken->code, $currency['code']), // сколько монет в валюте для картинки равна 1 монете продаваемой на ICO
                                    ],
                                    'role'  => 'button',
                                ]) ?>
                            </p>

                            <?php
                            /** @var float $sum сумма собранных стредств по валюте */
                            $sum = IcoRequest::find()
                                ->where(['ico_request.is_paid' => 1])
                                ->andWhere(['ico_request.ico_id' => $IcoID])
                                ->andWhere(['billing.currency_id' => $currency['id']])
                                ->innerJoin('billing', 'ico_request.billing_id = billing.id')
                                ->select([
                                    'sum(billing.sum_after) as sum',
                                ])
                                ->scalar()
                            ;
                            if (is_null($sum)) $sum = 0;
                            if ($sum === false) $sum = 0;
                            ?>
                            <p class="text-center"><?= Yii::$app->formatter->asDecimal($sum, 2)?></p>
                        </div>
                    <?php } ?>
                </div>
            </div>

            <div class="row">
                <div class="col-lg-12">
                    <?php
                    $currencyObject = \common\models\avatar\Currency::findOne(['code' => 'USD']);
                    $rows = IcoRequest::find()
                        ->where(['ico_request.is_paid' => 1])
                        ->andWhere(['ico_request.ico_id' => $IcoID])
                        ->innerJoin('billing', 'ico_request.billing_id = billing.id')
                        ->innerJoin('currency', 'currency.id = billing.currency_id')
                        ->select([
                            'ico_request.id',
                            'ico_request.created_at',
                            '(`billing`.`sum_after` * `currency`.`kurs`) as sum',
                        ])
                        ->asArray()
                        ->all()
                    ;
                    $rows2 = [
                        [
                            'created_at' => $project->time_start,
                            'sum'        => 0,
                        ]
                    ];
                    $sum1 = 0;
                    foreach ($rows as $row) {
                        $sum1 += (float)$row['sum'] / $currencyObject->kurs;
                        $rows2[] = [
                            'id'         => $row['id'],
                            'created_at' => $row['created_at'],
                            'sum'        => round($sum1, 2),
                        ];
                    }

                    ?>

                    <?php
                    $rowsJson = \yii\helpers\Json::encode($rows2);
                    $this->registerJs(<<<JS
Highcharts.setOptions({
    global: {
        timezoneOffset: -3 * 60
    }
});
var rows = {$rowsJson};
var newRows = [];
for(i = 0; i < rows.length; i++)
{
    var item = rows[i];
    newRows.push({
        x: new Date(item.created_at * 1000),
        y: item.sum
    });
}
JS
                    );
                    ?>
                    <?= \common\widgets\HighCharts\HighCharts::widget([
                        'chartOptions' => [
                            'chart'       => [
                                'zoomType' => 'x',
                                'type'     => 'spline',
                            ],
                            'title'       => [
                                'text' => 'График',
                            ],
                            'subtitle'    => [
                                'text' => 'Выделите область для изменения масштаба',
                            ],
                            'xAxis'       => [
                                'type' => 'datetime',
                            ],
                            'yAxis'       => [
                                [
                                    'title' => [
                                        'text' => 'Количество',
                                    ],
                                ],
                            ],
                            'legend'      => [
                                'enabled' => true,
                            ],
                            'tooltip'     => [
                                'crosshairs' => true,
                                'shared'     => true,
                            ],
                            'plotOptions' => [
                                'series' => [
                                    'turboThreshold' => 0,
                                ],
                            ],
                            'series'      => [
                                [
                                    'type' => 'spline',
                                    'name' => 'USD',
                                    'data' => new \yii\web\JsExpression('newRows'),
                                ],
                            ],
                        ],
                    ]);
                    ?>
                    <?php

                    /** @var float $sum сумма собранных стредств по проекту */
                    $sum = IcoRequest::find()
                        ->where(['ico_request.is_paid' => 1])
                        ->andWhere(['ico_request.ico_id' => $IcoID])
                        ->innerJoin('billing', 'ico_request.billing_id = billing.id')
                        ->innerJoin('currency', 'currency.id = billing.currency_id')
                        ->select([
                            'sum(billing.sum_after * currency.kurs) as sum',
                        ])
                        ->scalar();
                    if (is_null($sum)) $sum = 0;
                    if ($sum === false) $sum = 0;
                    ?>
                    <p>Всего собрано: <?= Yii::$app->formatter->asDecimal($sum, 2) ?> р.</p>
                    <p>Всего собрано: <?= Yii::$app->formatter->asDecimal($sum / $currencyObject->kurs, 2) ?> $</p>

                </div>
            </div>
        <?php } ?>
    <?php } ?>


    <hr>
    <div class="row">
        <div class="col-lg-8 col-lg-offset-2">
            <center>
                <?= $this->render('../blocks/share', [
                    'image'       => Url::to(\common\widgets\FileUpload3\FileUpload::getOriginal($project->image), true),
                    'title'       => $project->name,
                    'url'         => Url::current([], true),
                    'description' => substr(strip_tags($project->content), 0, 500),
                ]) ?>
            </center>
        </div>
    </div>
</div>


<?php
$isGuest = Yii::$app->user->isGuest ? 1 : 0;

$this->registerJs(<<<JS
function formatAsDecimal2 (val, separator, lessOne)
{
    if (typeof separator == 'undefined') separator = ',';
    if (typeof lessOne == 'undefined') lessOne = 0;
    
    var pStr = '';
    var original = val;
    val = parseInt(val);
    var j = 1;
    var blocks = [];
    var valChacngable = val;
    while (true) {
        if (1000 <= valChacngable) { // 1000 < 1001
            // формирую ведущие нули 
            pStr = vedNullInt(valChacngable % 1000);
            blocks.push(pStr);
        } else { // 1000 > 52
            blocks.push(valChacngable);
        }
        if (valChacngable < 1000) break;
        valChacngable = parseInt(valChacngable / 1000);
        j++;
    }
    blocks = blocks.reverse();
    pStr = blocks.join(',');
    
    
    var oStr = '';
    if (lessOne > 0) {
        var d = original - parseInt(original);
        var i;
        d = d * Math.pow(10, lessOne);
        d = parseInt(d);
        if (d == 0) {
            for (i = 0; i < lessOne; i++) {
                oStr = oStr + '0';
            }
        } else {
            var p;
            // определить склько знаков в числе после заятой
            if (lessOne == 1) {
                oStr = d;
            } else {
                oStr = repeat('0', lessOne - getNums(d));
                oStr += d;
            }
        }
    }

    return pStr + '.' + oStr;
}

/**
* Возвращает число с ведущими нулями
* Например в числе 9 = 009 чифра
*
* @param  d integer
*/
function vedNullInt(d)
{
    var len = getNums(d);
    if (len == 3) return d;
    var prefix = repeat('0', 3 - len);
    
    return prefix + d;
}

/**
* Возвращает кол-во цифр в числе
* Например в числе 150 = 3 чифры
* Например в числе 999 = 3 чифры
* Например в числе 9 = 1 чифра
*
* @param  d integer
*/
function getNums(d)
{
    if (d == 0) return 1;
    var i = 0;
    while(true) {
        p = Math.pow(10, i);
        if (d - p < 0) return i;
        i++;
    }
}

/**
* 
* @param  s string
* @param  n integer
* 
* @returns {string}
*/
function repeat(s, n)
{
    var a = [];
    while(a.length < n){
        a.push(s);
    }
    return a.join('');
}



function validateForm() {
    if ({$isGuest}) {
        if ($('#ico-email').val() == '') {
            alert(1);
            return false;
        }
    }
    if ($('#ico-tokens').val() == '') {
        alert(1);
        return false;
    }
    return true;
}

$('#formBuy .form-control').on('focus', function() {
    var o = $(this);
    var p = o.parent();
    if (p.hasClass('input-group')) {
        p = p.parent();
    }
    p.removeClass('has-error');
    p.find('p.help-block-error').hide();
});

JS
);
?>

