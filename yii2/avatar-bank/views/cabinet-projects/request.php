<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;
use app\models\UnionCategory;

/* @var $this       yii\web\View */
/* @var $request    \common\models\investment\IcoRequest */

$this->title = 'Заявка #' . $request->id;

$icoObject = $request->getIco();
$icoBilling = $icoObject->getBilling();

?>
<style>
    .textDecorated {
        text-decoration-line: underline;
        text-decoration-style: dotted;
    }
</style>
<?php $provider = new \avatar\modules\ETH\ServiceEtherScan(); ?>
<div class="container">
    <div class="col-lg-12">
        <h1 class="page-header"><?= Html::encode($this->title) ?></h1>

        <h3 class="page-header">Заявка</h3>

        <?php
        $user = $request->getUser();
        //        $userBilling = \common\models\avatar\UserBill::findOne(['user_id' => $request->user_id, 'currency' => $icoBilling->currency]);

        $address = $request->address;
        ?>
        <?= \yii\widgets\DetailView::widget([
            'model'      => $request,
            'options'    => ['class' => 'table table-striped detail-view'],
            'attributes' => [
                [
                    'label'          => 'Идентификатор заявки',
                    'value'          => $request->id,
                    'captionOptions' => [
                        'style' => 'width:30%',
                    ],
                ],
                'created_at:datetime:Создана',
                [
                    'label'  => 'Оплачена?',
                    'format' => 'html',
                    'value'  => $request->is_paid ? Html::tag('span', 'Да', ['class' => 'label label-success']) : Html::tag('span', 'Нет', ['class' => 'label label-default']),
                ],
                'tokens:text:Кол-во заказанных токнов',
                [
                    'label'  => 'Адрес получения токенов',
                    'format' => 'html',
                    'value'  => Html::a(
                        Html::tag('code', $address),
                        $provider->getUrl('/address/' . $address),
                        ['target' => '_blank']
                    ),
                ],
            ],
        ]) ?>
        <?php

        $this->registerJs(<<<JS
$('.buttonSet').click(function(e) {
    $('#modalTransaction').modal();
});
JS
        );

        ?>
        <h3 class="page-header">Счет на оплату</h3>
        <?php
        $billing = $request->getBilling();
        $requestSuccess = $request->getPaymentI();
        $txid = $requestSuccess->getTransaction();
        $address = $requestSuccess->getAddress();
        ?>
        <?= \yii\widgets\DetailView::widget([
            'model'      => $billing,
            'options'    => ['class' => 'table table-striped detail-view'],
            'attributes' => [
                [
                    'label'          => 'Идентификатор счета',
                    'value'          => $billing->id,
                    'captionOptions' => [
                        'style' => 'width:30%',
                    ],
                ],
                [
                    'label'  => 'Платежная система',
                    'format' => ['image', ['width' => 50, 'class' => 'img-circle', 'style' => 'border: 1px solid #888;']],
                    'value'  => $billing->getPaySystem()->image,
                ],
                [
                    'label'  => 'Адрес для оплаты',
                    'format' => 'html',
                    'value'  => Html::tag('code', $address),
                ],
                [
                    'label'  => 'Кол-во монет для оплаты',
                    'value'  => Yii::$app->formatter->asDecimal($billing->sum_after, $billing->getPaySystem()->getCurrencyObject()->decimals),
                ],
                [
                    'label'  => 'Транзакция',
                    'format' => 'html',
                    'value'  => $txid ? Html::tag('code', $txid) : Html::a('Указать', 'javascript:void(0)', ['class' => 'btn btn-success buttonSet']),
                ],

            ],
        ]) ?>
        <h3 class="page-header">Проект</h3>
        <?php $project = $request->getProject(); ?>
        <?= \yii\widgets\DetailView::widget([
            'model'      => $project,
            'options'    => ['class' => 'table table-striped detail-view'],
            'attributes' => [
                [
                    'label'          => 'Наименование проекта',
                    'value'          => $project->name,
                    'captionOptions' => [
                        'style' => 'width:30%',
                    ],
                ],
                [
                    'label'  => 'Ссылка',
                    'format' => ['url', ['target' => '_blank']],
                    'value'  => Url::to(['projects/item', 'id' => $project->id], true),
                ],
            ],
        ]) ?>
        <h3 class="page-header">Покупаемый токен</h3>
        <?php $currency = $project->getIco()->getCurrency(); ?>
        <?php $provider = new \avatar\modules\ETH\ServiceEtherScan(); ?>
        <?= \yii\widgets\DetailView::widget([
            'model'      => $currency,
            'options'    => ['class' => 'table table-striped detail-view'],
            'attributes' => [
                [
                    'label'          => 'Монета',
                    'format'         => ['image', ['width' => 50, 'class' => 'img-circle', 'style' => 'border: 1px solid #888;']],
                    'value'          => $currency->image,
                    'captionOptions' => [
                        'style' => 'width:30%',
                    ],
                ],
                [
                    'label' => 'Название',
                    'value' => $currency->title,
                ],
                [
                    'label' => 'Код',
                    'value' => $currency->code,
                ],
                [
                    'label'  => 'Адрес',
                    'format' => 'html',
                    'value'  => Html::a(
                        Html::tag('code', $currency->getToken()->address),
                        $provider->getUrl('/token/' . $currency->getToken()->address),
                        ['target' => '_blank']
                    ),
                ],
            ],
        ]) ?>

        <h3 class="page-header">История</h3>
        <?= \yii\grid\GridView::widget([
            'dataProvider' => new \yii\data\ActiveDataProvider([
                'query' => \common\models\investment\IcoRequestMessage::find()
                    ->where(['request_id' => $request->id])
                    ->orderBy(['datetime' => SORT_DESC])
                    ->asArray()
            ]),
            'tableOptions' => [
                'class' => 'table table-hover table-striped',
                'style' => 'width: auto;',
                'id'    => 'tableTransaction',
            ],
            'summary' => '',
            'rowOptions'   => function ($item) {
                $params = [
                    'data'  => [
                        'id' => $item['id'],
                    ],
                    'role'  => 'button',
                    'class' => 'rowTable'
                ];

                return $params;
            },
            'columns' => [
                [
                    'header'         => 'Направление',
                    'content'        => function ($item) {
                        if (isset($item['direction'])) {
                            if ($item['direction'] == 1) {
                                return Html::tag('i', Html::tag('i', null, ['class' => 'glyphicon glyphicon-chevron-left']), ['class' => 'label label-success']);
                            } else {
                                return Html::tag('i', Html::tag('i', null, ['class' => 'glyphicon glyphicon-chevron-right']), ['class' => 'label label-danger']);
                            }
                        } else {
                            return '';
                        }
                    },
                ],
                [
                    'header'    => 'Создано',
                    'attribute' => 'created_at',
                    'content'   => function ($item) {
                        $v = \yii\helpers\ArrayHelper::getValue($item, 'datetime', 0);
                        if ($v == 0) return '';

                        return Html::tag('span', \cs\services\DatePeriod::back($v, ['isShort' => true]), [
                            'data'  => [
                                'toggle'    => 'tooltip',
                                'placement' => 'bottom',
                            ],
                            'title' => Yii::$app->formatter->asDatetime($v, 'php:d.m.Y H:i:s'),
                            'class' => 'textDecorated'
                        ]);
                    },
                ],
                [
                    'header'    => 'Сообщение',
                    'content'   => function ($item) {
                        $v = \yii\helpers\ArrayHelper::getValue($item, 'message', '');
                        return $v;
                    },
                ],
            ]
        ]) ?>
    </div>


</div>
<div class="modal fade" tabindex="-1" role="dialog" id="modalTransaction">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Транзакция об оплате</h4>
            </div>
            <div class="modal-body">
                <p>Транзакция:</p>

                <p><input id="field-txid" class="form-control" type="text"></p>
            </div>
            <div class="modal-footer">
                <?php
                $str4 = Yii::t('c.TsqnzVaJuC', 'Транзакция успешно отправлена');
                $this->registerJs(<<<JS
$('.buttonModalSet').click(function(e) {
    ajaxJson({
        url: '/cabinet-ico/confirm',
        data: {
            txid: $('#field-txid').val(),
            id: {$request->id}
        },
        success: function(ret) {
            $('#modalTransaction').on('hidden.bs.modal', function(e) {
                $('#modalInfo .modal-body')
                .append(
                    $('<p>').html('{$str4}')
                )
                .append(
                    $('<p>').append(
                        $('<code>').html(ret.address)
                    )
                )
                ;
                $('#modalInfo').on('hidden.bs.modal', function(e) {
                    window.location.reload();
                }).modal();
            }).modal('hide');
        }
    });
});

JS
                );

                ?>
                <button type="button" class="btn btn-primary buttonModalSet" style="width: 100%;">Подтвердить
                </button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div><!-- /.modal -->



<div class="modal fade" id="modalInfo" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">&times;</span></button>
                <h4 class="modal-title text-center" id="myModalLabel"><?= \Yii::t('c.TsqnzVaJuC', 'Информация') ?></h4>
            </div>
            <div class="modal-body text-center">

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" style="width: 100%;" data-dismiss="modal">
                    <?= \Yii::t('c.TsqnzVaJuC', 'Закрыть') ?>
                </button>
            </div>
        </div>
    </div>
</div>