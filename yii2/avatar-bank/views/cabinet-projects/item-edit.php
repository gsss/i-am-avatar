<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;
use app\models\UnionCategory;
use yii\db\Query;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model \yii\base\Model */
/* @var $class string */
/* @var $title string */
/* @var $project \common\models\investment\Project */

$this->title = $title;

$path = '@avatar/modules/Piramida/PaySystems/' . $class . '/template';
$pathPhp = '@avatar/modules/Piramida/PaySystems/' . $class . '/template.php';
$file = Yii::getAlias($pathPhp);
if (!file_exists($file)) {
    $path = '@avatar/modules/Piramida/PaySystems/Base/template';
}
?>
<div class="container">
    <div class="col-lg-12">
        <h1 class="page-header"><?= Html::encode($this->title) ?></h1>


        <?php if (Yii::$app->session->hasFlash('contactFormSubmitted')): ?>

            <div class="alert alert-success">
                Успешно обновлено.
            </div>

            <p>
                <a href="<?= Url::to(['cabinet-projects/payments', 'id' => $project->id]) ?>" class="btn btn-primary">
                    Платежные системы
                </a>
                <a href="<?= Url::to(['cabinet-projects/edit', 'id' => $project->id]) ?>" class="btn btn-primary">
                    ICO
                </a>
                <a href="/cabinet-projects/index" class="btn btn-primary">
                    Проекты
                </a>
            </p>
        <?php else: ?>
            <?= $this->render($path, ['model' => $model]); ?>
        <?php endif; ?>
    </div>



</div>
