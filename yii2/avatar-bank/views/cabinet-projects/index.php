<?php

/** $this \yii\web\View  */

use yii\helpers\Url;
use app\services\GsssHtml;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use app\services\GetArticle\Collection;

$this->title = 'Все проекты';


?>

<div class="container">
    <div class="col-lg-12">
        <h1 class="page-header"><?= Html::encode($this->title) ?></h1>


        <a href="<?= Url::to(['cabinet-projects/add']) ?>" class="btn btn-default" style="margin-bottom: 50px;">Добавить</a>
        <?php \yii\widgets\Pjax::begin(); ?>
        <?php
        $this->registerJS(<<<JS
$('[data-toggle="tooltip"]').tooltip();
$('.buttonDelete').click(function (e) {
    e.preventDefault();
    e.stopPropagation();
    if (confirm('Подтвердите удаление')) {
        var button = $(this);
        var id = $(this).data('id');
        ajaxJson({
            url: '/cabinet-projects/delete' + '?' + 'id' + '=' + id,
            success: function (ret) {
                $('#modalInfo').on('hidden.bs.modal', function() {
                    button.parent().parent().remove();
                }).modal();
            }
        });
    }
});
$('.buttonSendModeration').click(function (e) {
    e.preventDefault();
    e.stopPropagation();
    if (confirm('Подтвердите отправку')) {
        var button = $(this);
        var id = $(this).data('id');
        ajaxJson({
            url: '/cabinet-projects/send-moderation' + '?' + 'id' + '=' + id,
            success: function (ret) {
                $('#modalInfo').on('hidden.bs.modal', function() {
                    window.location.reload();
                }).modal();
            }
        });
    }
});
$('.buttonPayments').click(function (e) {
   e.preventDefault();
   e.stopPropagation();
   window.location = '/cabinet-projects/payments' + '?' + 'id' + '=' + $(this).data('id');
});

$('.buttonRequests').click(function (e) {
   e.preventDefault();
   e.stopPropagation();
   window.location = '/cabinet-projects/requests' + '?' + 'id' + '=' + $(this).data('id');
});

$('.buttonIco').click(function (e) {
   e.preventDefault();
   e.stopPropagation();
   window.location = '/cabinet-projects/edit-ico' + '?' + 'id' + '=' + $(this).data('id');
});

$('.buttonView').click(function (e) {
   e.preventDefault();
   e.stopPropagation();
   window.location = '/cabinet-projects/view' + '?' + 'id' + '=' + $(this).data('id');
});

$('.rowTable').click(function() {
    window.location = '/cabinet-projects/edit' + '?' + 'id' + '=' + $(this).data('id');
});

JS
        );
        $sort = new \yii\data\Sort([
            'defaultOrder' => [
                'created_at' => SORT_DESC
            ]
        ]);
        ?>
        <?= \yii\grid\GridView::widget([
            'dataProvider' => new \yii\data\ActiveDataProvider([
                'query'      => \common\models\investment\Project::find()
                    ->innerJoin('projects_ico', 'projects_ico.id = projects.id')
                    ->select([
                        'projects.*',
                        'projects_ico.*',
                    ])
                    ->where(['projects.user_id' => Yii::$app->user->id])
                    ->andWhere(['projects.mark_deleted' => 0])
                    ->asArray()
                ,
                'pagination' => [
                    'pageSize' => 20,
                ],
            ]),
            'tableOptions' => [
                'class' => 'table table-striped table-hover',
            ],
            'rowOptions'   => function ($item) {
                $data = [
                    'data'  => ['id' => $item['id']],
                    'role'  => 'button',
                    'class' => 'rowTable'
                ];
                return $data;
            },
            'columns'      => [
                [
                    'header'  => 'Картинка',
                    'content' => function ($item) {
                        $i = ArrayHelper::getValue($item, 'image', '');
                        if ($i == '') return '';

                        return Html::img($i, [
                            'class'  => "thumbnail",
                            'width'  => 80,
                            'height' => 80,
                            'style'  => 'margin-bottom: 0px;',
                        ]);
                    }
                ],
                'name',
                [
                    'header' => 'Токенов',
                    'contentOptions' => [
                        'style' => 'text-align: right;',
                    ],
                    'headerOptions' => [
                        'style' => 'text-align: right;',
                    ],
                    'content' => function ($item) {
                        $remained_tokens = \yii\helpers\ArrayHelper::getValue($item, 'remained_tokens', 0);
                        $all_tokens = \yii\helpers\ArrayHelper::getValue($item, 'all_tokens', 0);
                        $html[] = Html::tag('code', Yii::$app->formatter->asDecimal($remained_tokens, 0));
                        $html[] = Html::tag('br');
                        $html[] = Html::tag('code', Yii::$app->formatter->asDecimal($all_tokens, 0));
                        $html[] = Html::tag('br');

                        $currency_id = \yii\helpers\ArrayHelper::getValue($item, 'currency_id', 0);
                        $currency = \common\models\avatar\Currency::findOne($currency_id);
                        $html[] = Html::tag('span', $currency->code, ['class' => 'label label-info']);

                        return join('', $html);
                    }
                ],
                [
                    'header' => 'Цена токена',
                    'contentOptions' => [
                        'style' => 'text-align: right;',
                    ],
                    'headerOptions' => [
                        'style' => 'text-align: right;',
                    ],
                    'content' => function ($item) {
                        $kurs_currency_id = \yii\helpers\ArrayHelper::getValue($item, 'kurs_currency_id', 0);
                        $currency = \common\models\avatar\Currency::findOne($kurs_currency_id);
                        $html[] = Html::tag('code', Yii::$app->formatter->asDecimal($item['kurs'], 2) . ' ' . $currency->code);

                        return join('', $html);
                    }
                ],
                [
                    'header'  => 'Создано',
                    'content' => function ($item) {
                        $v = \yii\helpers\ArrayHelper::getValue($item, 'created_at', 0);
                        if ($v == 0) return '';

                        return Html::tag('abbr', \cs\services\DatePeriod::back($v, ['isShort' => true]), ['data' => ['toggle' => 'tooltip'], 'title' => Yii::$app->formatter->asDatetime($v)]);
                    }
                ],
                [
                    'header'  => 'Окончание',
                    'content' => function ($item) {
                        $v = \yii\helpers\ArrayHelper::getValue($item, 'time_end');
                        if (is_null($v)) return '';
                        // если окончание уже прошло
                        if ($v < time()) {
                            $class = 'danger';
                            $text = \cs\services\DatePeriod::back($v, ['isShort' => true]);
                        } else {
                            $class = 'success';
                            $text = \cs\services\DatePeriod::forward($v);
                        }

                        return Html::tag(
                            'span',
                            $text,
                            [
                                'class'    => 'label label-'.$class,
                                'data'     => [
                                    'toggle' => 'tooltip',
                                    'placement' => 'bottom',
                                ],
                                'title' => Yii::$app->formatter->asDatetime($v),
                            ]
                        );
                    }
                ],
                [
                    'header'  => 'Функции',
                    'content' => function ($item) {
                        $rows = [];
                        $rows[] = Html::button('Платежные системы', ['class' => 'btn btn-default btn-xs buttonPayments', 'data' => ['id' => $item['id']]]);
                        $rows[] = Html::button('Заказы', ['class' => 'btn btn-default btn-xs buttonRequests', 'data' => ['id' => $item['id']], 'style' => 'margin-top:8px;']);
                        $rows[] = Html::button('Посмотреть', ['class' => 'btn btn-default btn-xs buttonView', 'data' => ['id' => $item['id']], 'style' => 'margin-top:8px;']);
//                        $rows[] = Html::button('Доп данные', ['class' => 'btn btn-default btn-xs buttonIco', 'data' => ['id' => $item['id']], 'style' => 'margin-top:8px;']);
//                        $rows[] = Html::button('Подарки', ['class' => 'btn btn-default btn-xs', 'id' => 'buttonGifts', 'data' => ['id' => $item['id']], 'style' => 'margin-top:8px;']);

                        return join('<br>', $rows);
                    }
                ],
                [
                    'header'  => 'Модерация',
                    'content' => function ($item) {
                        $status = \yii\helpers\ArrayHelper::getValue($item, 'status', 0);
                        switch ($status) {
                            case 1:
                                return Html::tag('span', 'Отправлено', ['class' => 'label label-warning']);
                                break;
                            case 2:
                                return Html::tag('span', 'Утверждено', ['class' => 'label label-success']);
                                break;
                            case 3:
                                return Html::tag('span', 'Отклонено', ['class' => 'label label-danger']) . '<br>' .
                                    Html::button('Отправить', [
                                        'class' => 'btn btn-default btn-xs buttonSendModeration',
                                        'data'  => ['id' => $item['id']],
                                        'style' => 'margin-top: 10px;',
                                    ]);
                                break;
                            case 0:
                            default:
                                return Html::tag('span', 'Черновик', ['class' => 'label label-default']) . '<br>' .
                                    Html::button('Отправить', [
                                        'class' => 'btn btn-default btn-xs buttonSendModeration',
                                        'data'  => ['id' => $item['id']],
                                        'style' => 'margin-top: 10px;',
                                    ]);
                                break;
                        }
                    }
                ],
                [
                    'header'  => 'Удалить',
                    'content' => function ($item) {
                        return Html::button('Удалить', [
                            'class' => 'btn btn-danger btn-xs buttonDelete',
                            'data'  => [
                                'id' => $item['id'],
                            ]
                        ]);
                    }
                ],
            ],
        ]) ?>
        <?php \yii\widgets\Pjax::end(); ?>

    </div>

</div>

<div class="modal fade" id="modalInfo" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Информация</h4>
            </div>
            <div class="modal-body">
                Успешно!
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
            </div>
        </div>
    </div>
</div>