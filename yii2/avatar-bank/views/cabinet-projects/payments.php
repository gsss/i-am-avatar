<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;
use app\models\UnionCategory;
use yii\db\Query;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $project \common\models\investment\Project */

$this->title = $project->name;

if (is_null($project->payments_list_id)) {
    $i = new \common\models\PaySystemListItem(['name' => 'ICO '. $project->name, 'created_at' => time()]);
    $i->save();
    $i->id = $i::getDb()->lastInsertID;

    $project->payments_list_id = $i->id;
    $project->save();
}

Yii::$app->session->set('/admin-paysystem-config/item.php::list_id', $project->payments_list_id);

$this->registerJs(<<<JS
$('.rowTable').click(function() {
    var paysystem_id = $(this).data('paysystem_id');
    var list_id = $(this).data('list_id');
    window.location = '/cabinet-projects/item-edit?' +
     'paysystem_id' + '=' + paysystem_id
     + '&' +
     'list_id' + '=' + list_id
     ;
});
$('.buttonRemove').click(function(e) {
    e.preventDefault();
    e.stopPropagation();
    if (confirm('Вы подтверждаете?')) {
        var b = $(this);
        var id = b.data('id');
        ajaxJson({
            url: '/cabinet-projects/payments-remove',
            data: {
                id: id
            },
            success: function(ret) {
                $('#modalInfo').on('hidden.bs.modal', function(e) {
                    window.location.reload();
                }).modal();
            }
        });
    }
});

JS
);

?>
<div class="container">
    <div class="col-lg-12">
        <h1 class="page-header"><?= Html::encode($this->title) ?></h1>

        <?= \yii\grid\GridView::widget([
            'dataProvider' => new \yii\data\ActiveDataProvider([
                'query' => \common\models\PaySystemConfig::find()
                    ->rightJoin('paysystems', 'paysystems.id = paysystems_config.paysystem_id and paysystems_config.parent_id = ' . $project->payments_list_id)
                    ->select([
                        'paysystems_config.id',
                        'paysystems_config.config',
                        'paysystems.title',
                        'paysystems.image',
                        'paysystems.currency',
                        'paysystems.class_name',
                        'paysystems.id as paysystem_id',
                    ])
                    ->where(['paysystems.id' => [
                        4,  // BTC
                        9,  // ETH
//                        10, // EMC
//                        11, // WAV
//                        13, // XMR
//                        14, // LTC
//                        15, // ZCASH
//                        17, // ETC
//                        18, // DASH
//                        19, // BCC
//                        12, // NEM
                        20, // Prizm
                    ]])
                    ->orderBy(['paysystems.currency' => SORT_ASC])
                    ->asArray()
            ]),
            'tableOptions' => [
                'class' => 'table table-hover table-striped',
                'style' => 'width: auto;',
                'id'    => 'tableTransaction',
            ],
            'rowOptions'   => function ($item) {
                $params = [
                    'data'  => [
                        'paysystem_id' => $item['paysystem_id'],
                        'list_id'      => Yii::$app->session->get('/admin-paysystem-config/item.php::list_id')
                    ],
                    'role'  => 'button',
                    'class' => 'rowTable'
                ];
                if (!isset($item['id'])) {
                    $params['style'] = 'opacity: 0.3;';
                }
                return $params;
            },
            'columns'      => [
                'id',
                [
                    'header'  => 'currency',
                    'content' => function ($item) {
                        $i = \yii\helpers\ArrayHelper::getValue($item, 'currency', '');

                        return Html::tag('span', $i, ['class' => 'label label-default']);
                    }
                ],
                [
                    'header'  => 'Задана?',
                    'content' => function ($item) {
                        $i = \yii\helpers\ArrayHelper::getValue($item, 'id', '');
                        if ($i == '') return Html::tag('span', 'Нет', ['class' => 'label label-default']);

                        return Html::tag('span', 'Да', ['class' => 'label label-success']);
                    }
                ],
                [
                    'header'  => 'Картинка',
                    'content' => function ($item) {
                        $i = \yii\helpers\ArrayHelper::getValue($item, 'image', '');
                        if ($i == '') return '';

                        return Html::img($i, [
                            'class' => "thumbnail",
                            'width' => 80,
                            'style' => 'margin-bottom: 0px;',
                        ]);
                    }
                ],
                'title:text:Название',
                [
                    'header'  => 'Конфиг',
                    'content' => function ($item) {
                        if (is_null($item['config'])) {
                            return Html::tag('span', 'Нет', ['class' => 'label label-default']);
                        }
                        return Html::tag('code', substr(\yii\helpers\ArrayHelper::getValue($item, 'config'),0,20). ' ...');
                    }
                ],
                [
                    'header'  => 'Убрать',
                    'content' => function ($item) {
                        if (is_null($item['config'])) {
                            return '';
                        }
                        return Html::button('Убрать',[
                            'class' => 'btn btn-danger btn-xs buttonRemove',
                            'data' => [
                                'id' => $item['id']
                            ]
                        ]);
                    },
                ],
            ]
        ]) ?>
    </div>
</div>

<div class="modal fade" id="modalInfo" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">&times;</span></button>
                <h4 class="modal-title text-center" id="myModalLabel"><?= \Yii::t('c.TsqnzVaJuC', 'Информация') ?></h4>
            </div>
            <div class="modal-body text-center">
                Успешно
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" style="width: 100%;" data-dismiss="modal">
                    <?= \Yii::t('c.TsqnzVaJuC', 'Закрыть') ?>
                </button>
            </div>
        </div>
    </div>
</div>