<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;
use app\models\UnionCategory;
use yii\db\Query;
use yii\helpers\ArrayHelper;

/* @var $this   yii\web\View */
/* @var $form   yii\bootstrap\ActiveForm */
/* @var $model  \common\models\investment\ProjectIco */

$this->title = 'Добавить проект. Шаг 2';

?>
<div class="container">
    <div class="col-lg-12">
        <h1 class="page-header"><?= Html::encode($this->title) ?></h1>

        <?php if (!is_null($id = Yii::$app->session->getFlash('form'))) : ?>

            <div class="alert alert-success">
                Успешно добавлено.
            </div>

        <?php else: ?>
            <?php $form = ActiveForm::begin([
                'options' => ['enctype' => 'multipart/form-data']
            ]); ?>

            <?= $form->field($model, 'billing_id')
                ->dropDownList(
                    ArrayHelper::map(
                        \common\models\avatar\UserBill::find()
                            ->where([
                                'user_id'      => Yii::$app->user->id,
                                'mark_deleted' => 0,
                            ])
                            ->select([
                                'user_bill.id',
                                'user_bill.address',
                                'currency.code',
                                'currency.title',
                            ])
                            ->innerJoin('currency', 'currency.id = user_bill.currency')
                            ->innerJoin('token', 'currency.id = token.currency_id')
                            ->asArray()
                            ->all()
                        ,
                        'id',
                        function ($item) {
                            return $item['title'] . ' ' . '(' . $item['code'] . ')' . ' ' . $item['address'];
                        }
                    )
                ) ?>
            <?= $form->field($model, 'remained_tokens') ?>
            <?= $form->field($model, 'kurs') ?>
            <?= $form->field($model, 'kurs_currency_id')
                ->dropDownList(
                    ArrayHelper::map(
                        \common\models\avatar\Currency::find()
                            ->asArray()
                            ->all()
                        ,
                        'id',
                        function ($item) {
                            return $item['title'] . ' ' . '(' . $item['code'] . ')';
                        }
                    )
                ) ?>

            <hr>
            <div class="form-group">
                <?= Html::submitButton('Добавить', [
                    'class' => 'btn btn-default',
                    'name'  => 'contact-button',
                    'style' => 'width:100%',
                ]) ?>
            </div>
            <?php ActiveForm::end(); ?>

        <?php endif; ?>
    </div>

</div>
