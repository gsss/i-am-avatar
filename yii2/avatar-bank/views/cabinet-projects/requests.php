<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;
use app\models\UnionCategory;
use yii\db\Query;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $project \common\models\investment\Project */

$this->title = $project->name;


$this->registerJs(<<<JS
$('.rowTable').click(function() {
    window.location = '/cabinet-projects/request?' +
     'id' + '=' + $(this).data('id')
     ;
});

JS
);

?>

<style>
    .textDecorated {
        text-decoration-line: underline;
        text-decoration-style: dotted;
    }
</style>

<div class="container">
    <div class="col-lg-12">
        <h1 class="page-header"><?= Html::encode($this->title) ?></h1>

        <p><a href="<?= \yii\helpers\Url::to(['cabinet-projects/requests-archive', 'id' => $project->id]) ?>" class="btn btn-default">Архив</a></p>


        <?php \yii\widgets\Pjax::begin() ?>

        <?php
        $this->registerJs(<<<JS
                
$('.buttonSend').click(function(e) {
    e.preventDefault();
    e.stopPropagation();
    var bid = $(this).data('id');
    $('#modalTransaction').attr('data-id', bid);
    $('#modalTransaction').modal();
});
                
$('.buttonHide').click(function(e) {
    e.preventDefault();
    e.stopPropagation();
    if (confirm('Вы подтверждаете?')) {
        var b = $(this);
        var id = b.data('id');
        ajaxJson({
            url: '/cabinet-projects/hide',
            data: {
                id: id
            },
            success: function(ret) {
                b.parent().parent().remove();
                alert('Успешно');
            }
        });
    }
});

JS
        );

        ?>

        <?= \yii\grid\GridView::widget([
            'dataProvider' => new \yii\data\ActiveDataProvider([
                'query' => \common\models\investment\IcoRequest::find()
                    ->where([
                        'ico_request.ico_id'  => $project->id,
                        'ico_request.is_paid' => 0,
                        'ico_request.is_hide' => 0,
                    ])
                    ->innerJoin('paysystems_config', 'paysystems_config.id = ico_request.paysystem_config_id')
                    ->innerJoin('paysystems', 'paysystems.id = paysystems_config.paysystem_id')
                    ->select([
                        'ico_request.*',
                        'paysystems_config.paysystem_id',
                        'paysystems.currency',
                        'paysystems.title',
                        'paysystems.image',
                    ])
                    ->orderBy(['ico_request.created_at' => SORT_DESC])
                    ->asArray()
            ]),
            'tableOptions' => [
                'class' => 'table table-hover table-striped',
                'style' => 'width: auto;',
                'id'    => 'tableTransaction',
            ],
            'rowOptions'   => function ($item) {
                $params = [
                    'data'  => [
                        'id' => $item['id'],
                    ],
                    'role'  => 'button',
                    'class' => 'rowTable'
                ];

                return $params;
            },
            'columns' => [
                'id',
                [
                    'header'  => 'Платежная система',
                    'content' => function ($item) {
                        $v = \yii\helpers\ArrayHelper::getValue($item, 'image', '');

                        return Html::img($v, [
                            'width' => 50,
                            'class' => 'img-circle',
                            'title' => \yii\helpers\ArrayHelper::getValue($item, 'title', ''),
                            'data'  => [
                                'toggle'    => 'tooltip',
                                'placement' => 'bottom',
                            ],
                        ]);
                    },
                ],
                [
                    'header'    => 'Создано',
                    'attribute' => 'created_at',
                    'content'   => function ($item) {
                        $v = \yii\helpers\ArrayHelper::getValue($item, 'created_at', 0);
                        if ($v == 0) return '';

                        return Html::tag('span', \cs\services\DatePeriod::back($v, ['isShort' => true]), [
                            'data'  => [
                                'toggle'    => 'tooltip',
                                'placement' => 'bottom',
                            ],
                            'title' => Yii::$app->formatter->asDatetime($v),
                            'class' => 'textDecorated'
                        ]);
                    },
                ],
                [
                    'header'         => 'Токенов',
                    'format'         => ['decimal', 2],
                    'attribute'      => 'tokens',
                    'headerOptions'  => [
                        'style' => 'text-align:right',
                    ],
                    'contentOptions' => [
                        'style' => 'text-align:right',
                    ],

                ],
                [
                    'header'  => 'Польз',
                    'content' => function ($item) {
                        $user_id = \yii\helpers\ArrayHelper::getValue($item, 'user_id', '');
                        $email = \yii\helpers\ArrayHelper::getValue($item, 'email', '');
                        if (is_null($user_id)) {
                            return $email;
                        }
                        $user = \common\models\UserAvatar::findOne($user_id);
                        $v = $user->getAvatar();

                        return
                            Html::a(Html::img($v, [
                                'width' => 50,
                                'class' => 'img-circle',
                                'title' => $user->email,
                                'data'  => [
                                    'toggle'    => 'tooltip',
                                    'placement' => 'bottom',
                                ],
                            ]), ['user/index', 'id' => $user_id])
                            ;
                    },
                ],
                [
                    'header'  => 'Транзакция',
                    'content' => function ($item) {
                        $request = \common\models\investment\IcoRequest::findOne($item['id']);
                        try {
                            $payment = $request->getBilling()->getPayment();
                        } catch (Exception $e) {
                            return '';
                        }
                        $txid = $payment->getTransaction();
                        if (is_null($txid)) {
                            $txid = '';
                        }
                        if ($txid == '') return '';

                        return Html::tag('code', substr($txid,0,8).'...');
                    },
                ],
                [
                    'header'  => 'Опл?',
                    'content' => function ($item) {
                        $billing = \common\models\BillingMain::findOne($item['billing_id']);
                        try {
                            $payment = $billing->getPayment();
                        } catch (Exception $e) {
                            return '';
                        }
                        $txid = $payment->getTransaction();
                        if (is_null($txid)) {
                            $txid = '';
                        }
                        if ($txid == '') return Html::tag('span', 'Нет', ['class' => 'label label-default']);

                        return Html::tag('span', 'Да', ['class' => 'label label-success']);
                    },
                ],
                [
                    'header'  => 'Подтв?',
                    'content' => function ($item) {
                        $status = ArrayHelper::getValue($item, 'status', 0);
                        if ($status == \common\models\investment\IcoRequest::STATUS_SHOP_CONFIRM) {
                            return Html::tag('span', 'Да', ['class' => 'label label-success']);
                        }

                        return Html::tag('span', 'Нет', ['class' => 'label label-default']);
                    },
                ],
                [
                    'header'  => 'Отправить токены',
                    'content' => function ($item) {
                        return Html::button('Отправить',[
                            'class' => 'btn btn-success buttonSend',
                            'data' => [
                                'id' => $item['id']
                            ]
                        ]);
                    },
                ],
                [
                    'header'  => Html::tag('span', Html::tag('i', null, ['class' => 'glyphicon glyphicon-eye-close']), [
                        'class' => 'label label-default',
                        'title' => Yii::t('c.TsqnzVaJuC', 'Скрыть заказ'),
                        'data'  => [
                            'toggle' => 'tooltip',
                        ],
                    ]),
                    'content' => function ($item) {
                        return Html::button(Html::tag('i', null, ['class' => 'glyphicon glyphicon-eye-close']),[
                            'class' => 'btn btn-default btn-xs buttonHide',
                            'data' => [
                                'id' => $item['id']
                            ]
                        ]);
                    },
                ],
            ]

        ]) ?>
        <?php \yii\widgets\Pjax::end() ?>
    </div>


</div>

<div class="modal fade" tabindex="-1" role="dialog" id="modalTransaction">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header text-center">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Отправить токены</h4>
            </div>
            <div class="modal-body">

                <div class="form-group recipient-field-password">
                    <label for="recipient-name" class="control-label">Пароль:</label>
                    <input type="password" class="form-control" name="password" style="font-family: Consolas, Courier New, monospace" id="recipient-password">
                    <p class="help-block"></p>

                    <p class="help-block help-block-error" style="display: none;"></p>
                </div>

            </div>
            <div class="modal-footer">
                <?php
                $str4 = Yii::t('c.TsqnzVaJuC', 'Транзакция успешно отправлена');
                $p = new \avatar\modules\ETH\ServiceEtherScan();
                $url = $p->getUrl('/tx/');
                $this->registerJs(<<<JS
var countRepeat = 3; // Кол-во повторов на отправку токенов при неудачной отправке, если -1 то бесконечное кол-во
var functionButtonSend = function() {
    var bid = $('#modalTransaction').data('id');
    
    var b = $(this);
    b.off('click');
    b.html($('<i>', {class: 'fa fa-spinner fa-spin fa-fw'}));
    b.attr('disabled', 'disabled');
    ajaxJson({
        url: '/cabinet-projects/send',
        data: {
            password: $('#recipient-password').val(),
            id: bid
        },
        success: function(ret) {
            $('#modalTransaction').on('hidden.bs.modal', function(e) {
                $('#modalInfo .modal-body')
                .append(
                    $('<p>').html('{$str4}')
                )
                .append(
                    $('<p>').append(
                        $('<a>', {href:'{$url}' + ret.txid,target:'_blank'}).append(
                            $('<code>').html(ret.txid)
                        )
                    )
                )
                ;
                $('#modalInfo').on('hidden.bs.modal', function(e) {
                    window.location.reload();
                });
                $('#modalInfo').modal();
            }).modal('hide');
        }, 
        errorScript: function(ret) {
            b.on('click', functionButtonSend);
            b.html('Отправить');
            b.removeAttr('disabled'); 
            switch (ret.id) {
                case 101:
                    alert('Не загружены данные');
                    break;
                case 102:
                    var f = $('#modalTransaction');
                    $.each(ret.data,function(i,v) {
                        var name = v.name;
                        var value = v.value;
                        var t = f.find('.recipient-field-' + name);
                        t.addClass('has-error');
                        t.find('p.help-block-error').html(value.join('<br>')).show();
                    });

                    break;
                case 103: // не удачная отправка
                    if (countRepeat == -1) {
                        $('.buttonModalSet').click();
                    } else if (countRepeat > 0) {
                        countRepeat--;
                        $('.buttonModalSet').click();
                    } else {
                        var f = $('#modalTransaction');
                        $.each(ret.data,function(i,v) {
                            var name = v.name;
                            var value = v.value;
                            var o = f.find('#recipient-password');
                            var t = f.find('.recipient-field-password');
                            t.addClass('has-error');
                            t.find('p.help-block-error').html('Не удалось отправить токены').show();
                        });
                    }
                    break;
            }
        }
    });
};             
$('.buttonModalSet').click(functionButtonSend);

$('#modalTransaction .form-control').on('focus', function() {
    var o = $(this);
    var p = o.parent();
    if (p.hasClass('input-group')) {
        p = p.parent();
    }
    p.removeClass('has-error');
    p.find('p.help-block-error').hide();
});

JS
                );

                ?>
                <button type="button" class="btn btn-primary buttonModalSet" style="width: 100%;">Отправить</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div><!-- /.modal -->


<div class="modal fade" id="modalInfo" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">&times;</span></button>
                <h4 class="modal-title text-center" id="myModalLabel"><?= \Yii::t('c.TsqnzVaJuC', 'Информация') ?></h4>
            </div>
            <div class="modal-body text-center">

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" style="width: 100%;" data-dismiss="modal">
                    <?= \Yii::t('c.TsqnzVaJuC', 'Закрыть') ?>
                </button>
            </div>
        </div>
    </div>
</div>