<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;
use app\models\UnionCategory;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model cs\base\BaseForm */

$this->title = Yii::t('c.8Qu3iAsnps', 'Безопасность');
?>
<div class="container">
    <div class="col-lg-12">
        <h1 class="page-header"><?= Html::encode($this->title) ?></h1>



    </div>
    <div class="col-lg-8">
        <p><?= \Yii::t('c.8Qu3iAsnps', 'Использовать двухступенчатую авторизацию?') ?></p>


        <div class="btn-group" role="group" aria-label="...">
            <?php
            $rows = [
                0 => 'Выключена',
                1 => 'Только для неверифицированных браузеров',
                2 => 'Всегда',
            ];
            ?>
            <?php foreach ($rows as $key => $name) { ?>
                <?php $class = ['btn', 'btn-default', 'btn-option']; ?>
                <?php if ($key == Yii::$app->user->identity->is_2step_login) $class[] = 'active'; ?>
                <?= Html::button($name,['class' => join(' ', $class), 'data' => ['id' => $key]]); ?>
            <?php } ?>
        </div>

        <?php
        $this->registerJs(<<<JS
$('.btn-option').on('click', function(e) {
    var b = $(this);
    if (!b.hasClass('active')) {
        $('.btn-option').removeClass('active');
        b.addClass('active');
        ajaxJson({
            url: '/cabinet/profile-security-ajax',
            data: {
                is_2step_login: b.data('id')
            }, 
            success: function(ret) {
                console.log(true);
            }
        });
    }
});
JS
        );
        ?>

        <h3 class="page-header"><?= \Yii::t('c.8Qu3iAsnps', 'Устройства') ?></h3>
        <?php \yii\widgets\Pjax::begin() ?>
        <?php if (Yii::$app->request->isPjax) {
            $this->registerJs(<<<JS
$('[data-toggle="tooltip"]').tooltip();
JS
            );
        } ?>
        <?= \yii\grid\GridView::widget([
            'dataProvider' => new \yii\data\ActiveDataProvider([
                'query' => \common\models\UserDevice::find()
                    ->where(['user_id' => Yii::$app->user->id])
                    ->orderBy(['created_at' => SORT_DESC])
                ,
            ]),
            'tableOptions' => [
                'class' => 'table table-striped table-hover',
            ],
            'columns'      => [
                [
                    'header'  => 'Добавлен',
                    'content' => function ($item) {
                        $v = \yii\helpers\ArrayHelper::getValue($item, 'created_at', 0);
                        if ($v == 0) return '';

                        return Html::tag('abbr', \cs\services\DatePeriod::back($v, ['isShort' => true]), ['data' => ['toggle' => 'tooltip'], 'title' => Yii::$app->formatter->asDatetime($v)]);
                    }
                ],
                'name:text:Браузер',
            ],
        ]) ?>
        <?php \yii\widgets\Pjax::end() ?>
        <h3 class="page-header"><?= \Yii::t('c.8Qu3iAsnps', 'Входы') ?></h3>
        <?php \yii\widgets\Pjax::begin() ?>
        <?php if (Yii::$app->request->isPjax) {
            $this->registerJs(<<<JS
$('[data-toggle="tooltip"]').tooltip();
JS
            );
        } ?>
        <?php
        $sort = new \yii\data\Sort([
            'defaultOrder' => [
                'time' => SORT_DESC,
            ],
        ]);
        ?>
        <?= \yii\grid\GridView::widget([
            'dataProvider' => new \yii\data\ActiveDataProvider([
                'query' => \avatar\models\UserLogin::find()
                    ->where(['user_id' => Yii::$app->user->id])
                ,
                'sort'  => $sort,
            ]),
            'tableOptions' => [
                'class' => 'table table-striped table-hover',
            ],
            'columns'      => [
                [
                    'header'  => 'Когда',
                    'contentOptions' => ['nowrap' => 'nowrap'],
                    'content' => function ($item) {
                        $v = \yii\helpers\ArrayHelper::getValue($item, 'time', 0);
                        if ($v == 0) return '';

                        return Html::tag('abbr', \cs\services\DatePeriod::back($v, ['isShort' => true]), ['data' => ['toggle' => 'tooltip'], 'title' => Yii::$app->formatter->asDatetime($v)]);
                    }
                ],
                'ip:text:IP',
                'browser:text:Браузер',
            ],
        ]) ?>
        <?php \yii\widgets\Pjax::end() ?>
    </div>
    <div class="col-lg-4">
        <?= $this->render('_menu') ?>
    </div>

</div>