<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;
use app\models\UnionCategory;

/* @var $this \yii\web\View */
/* @var $model \avatar\models\forms\ProfilePin */

$this->title = 'Пин код на мобильном приложении';


?>
<div class="container">
    <div class="col-lg-12">
        <h1 class="page-header"><?= Html::encode($this->title) ?></h1>

    </div>
    <div class="col-lg-8">

        <?php if (Yii::$app->session->hasFlash('form')) { ?>
            <p class="alert alert-success">Успешно</p>
            <a href="/cabinet/pin-code-mobile" class="btn btn-default">Назад</a>
        <?php } else { ?>
            <?php $form = ActiveForm::begin() ?>
            <?= $form->field($model, 'pin')->passwordInput() ?>
            <hr class="featurette-divider">
            <div class="form-group">
                <?= Html::submitButton('Обновить', [
                    'class' => 'btn btn-default',
                    'name'  => 'contact-button',
                    'style' => 'width: 100%;',
                ]) ?>
            </div>
            <?php ActiveForm::end() ?>
        <?php } ?>

    </div>
    <div class="col-lg-4">
        <?= $this->render('_menu') ?>
    </div>
</div>



