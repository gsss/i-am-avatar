<?php

/** @var $this \yii\web\View */
/** @var $school \common\models\school\School */
/** @var $schoolList array список школ в которых состоит человек */

use yii\helpers\Url;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use kartik\grid\GridView;


?>

<?php
$sort = new \yii\data\Sort([
    'attributes'   => [
        'id'                => ['label' => 'ID'],
        'status'            => [
            'label' => 'Прогресс',
            'asc'   => ['status' => SORT_ASC, 'category_id' => SORT_ASC],
            'desc'  => ['status' => SORT_DESC, 'category_id' => SORT_DESC],
        ],
        'price'             => [
            'label'   => 'Награда',
            'default' => SORT_DESC,
        ],
        'created_at'        => ['label' => 'Созд', 'default' => SORT_DESC],
        'name'              => ['label' => 'Наименование'],
        'last_comment_time' => ['label' => 'Обн', 'default' => SORT_DESC],
        'category_id'       => ['label' => 'category_id'],
        'school_id',
    ],
    'defaultOrder' => [
        'created_at'  => SORT_DESC,
    ],
]);
$model = new \avatar\models\search\Task();

$where = [
    'school_task.school_id' => $schoolList,
//    'parent_id' => $model->id,
    'is_hide'   => 0,
];
$init = [];

$params = ArrayHelper::merge(Yii::$app->request->get(), $init);
$provider = $model->search($sort, $params, [], $where);
$sList = ArrayHelper::map(
        \common\models\school\School::find()->where(['id' => $schoolList])->all(),
        'id',
        'name'
)
?>
<?php
$this->registerJs(<<<JS
$('[data-toggle="tooltip"]').tooltip();
$('.rowTable').click(function(e) {
    // window.location = '/cabinet-school-task-list/view' + '?' + 'id' + '=' + $(this).data('id');
});
JS
);

$columns = [
    [
        'attribute' => 'id',
    ],
    [
        'header'    => 'Сообщ.',
        'attribute' => 'school_id',
        'filter'    => $sList,
        'content'   => function ($item) {
            $school_id = ArrayHelper::getValue($item, 'school_id', '');
            if ($school_id == '') {
                return '';
            }
            $s = \common\models\school\School::findOne($school_id);
            $si = '/images/school/school.jpg';
            try {
                if ($s->image != '') {
                    $si = \iAvatar777\widgets\FileUpload7\FileUpload::getFile($s->image, 'crop');
                }
            } catch (Exception $e) {
                \cs\services\VarDumper::dump([
                    $s,$school_id
                ]);
            }

            return Html::img(
                $si,
                [
                    'class'  => "img-circle",
                    'width'  => 30,
                    'height' => 30,
                    'style'  => 'margin-bottom: 0px;',
                    'data'   => ['toggle' => 'tooltip'],
                    'title'  => \yii\helpers\Html::encode($s->name),
                ]
            );
        },
    ],
    [
        'header'    => 'Автор',
        'attribute' => 'user_id',
        'filter'    => \avatar\models\search\Task::$listUserID,
        'content'   => function ($item) {
            $user_id = ArrayHelper::getValue($item, 'user_id', '');
            if ($user_id == '') {
                return '';
            }
            $user = \common\models\UserAvatar::findOne($user_id);

            return Html::img($user->getAvatar(), [
                'class'  => "img-circle",
                'width'  => 30,
                'height' => 30,
                'style'  => 'margin-bottom: 0px;',
                'data'   => ['toggle' => 'tooltip'],
                'title'  => \yii\helpers\Html::encode($user->getName2()),
            ]);
        },
    ],

];

$columns2 = [
    [
        'header'             => $sort->link('name'),
        'attribute'          => 'name',
        'content'   => function ($item) {
            return Html::a($item['name'], [
                'cabinet-school-task-list/view',
                'id'  => $item['id'],
            ], ['data' => ['pjax' => 0]]);
        },
    ],
    [
        'header'          => $sort->link('price'),
        'attribute'       => 'price',
        'format'          => ['decimal', 2],
        'value' => function ($model) {
            return $model['price'] / 100;
        },
    ],
    [
        'header'          => 'Валюта',
        'attribute'       => 'currency_id',
        'content'         => function ($model, $key, $index, $widget) {
            $cur = \common\models\piramida\Currency::findOne($model['currency_id']);
            if (is_null($cur)) return '';

            return Html::tag('span', $cur->code, ['class' => 'label label-info'] );
        },
    ],
    [
        'header'    => $sort->link('created_at'),
        'attribute' => 'created_at',
        'content'   => function ($item) {
            $v = \yii\helpers\ArrayHelper::getValue($item, 'created_at', 0);
            if ($v == 0) return '';

            return Html::tag('abbr', \cs\services\DatePeriod::back($v, ['isShort' => true]), ['data' => ['toggle' => 'tooltip'], 'title' => Yii::$app->formatter->asDatetime($v)]);
        },
    ],
    [
        'header'    => $sort->link('last_comment_time'),
        'attribute' => 'last_comment_time',
        'content'   => function ($item) {
            $v = \yii\helpers\ArrayHelper::getValue($item, 'last_comment_time', 0);
            if ($v == 0) return '';

            return Html::tag('abbr', \cs\services\DatePeriod::back($v, ['isShort' => true]), ['data' => ['toggle' => 'tooltip'], 'title' => Yii::$app->formatter->asDatetime($v)]);
        },
    ],
];
$columns = ArrayHelper::merge($columns, $columns2);

?>

<div>
    <?=
    \yii\grid\GridView::widget([
        'dataProvider'        => $provider,
        'filterModel'         => $model,
        'tableOptions'          => [
            'class' => 'table table-striped table-hover'
        ],

        'rowOptions'          => function ($item) {
            return [
                'data'  => ['id' => $item['id']],
//                'role'  => 'button',
                'class' => 'rowTable',
            ];
        },
        'columns'             => $columns,
    ]);
    ?>
</div>


<div class="modal fade" id="modalInfo" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="myModalLabel">Информация</h4>
            </div>
            <div class="modal-body">
                Успешно!
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
            </div>
        </div>
    </div>
</div>