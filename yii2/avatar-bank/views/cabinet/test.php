<?php
/**
 * Created by PhpStorm.
 * User: ramha
 * Date: 18.02.2021
 * Time: 9:54
 */
?>
    <div class="container">

<?=  \yii\jui\AutoComplete::widget([
    'id'            => 'select1',
    'clientOptions' => [
//        'serviceUrl' => '/cabinet-school-task-list/executor-search-ajax',
//        'source'            => '/cabinet/test-ajax',
        'source'            =>
    new \yii\web\JsExpression(    <<<JS
function(request, response) {
            ajaxJson({
                url: '/cabinet/test-ajax',
                data: {
                    term: request.term
                },
                success: function(ret) {
                    response(ret);
                }
            });
        }

JS
)
,
        'minChars' => 2,
        'zIndex' => 9999,
        'select' => new \yii\web\JsExpression(<<<JS
function(event, ui) {
    console.log([event, ui]);
    $('#select1').val(ui.item.label);
    return false;
    //$('#').val(ui.item.id);
}
JS
        )
    ],
    'options'       => [
        'class'       => 'form-control',
    ],
]);
?>
    </div>

