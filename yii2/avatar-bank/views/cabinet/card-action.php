<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;
use app\models\UnionCategory;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \avatar\models\forms\CardAction */

$this->title = 'Модель поведения после сканирования QR кода вашей карты';


?>
<div class="container">
    <div class="col-lg-12">
        <h1 class="page-header"><?= Html::encode($this->title) ?></h1>

    </div>
    <div class="col-lg-8">
        <?php if (Yii::$app->session->hasFlash('form')): ?>

            <p class="alert alert-success">
                Успешно обновлено.
            </p>

        <?php else: ?>

            <?php $form = ActiveForm::begin([
                'id'      => 'contact-form',
                'options' => ['enctype' => 'multipart/form-data'],
            ]); ?>
            <?= $form->field($model, 'card_action')
                ->dropDownList([
                    1 => 'Открыть свой профиль',
                    2 => 'Открыть карту',
                    3 => 'Открыть свой профиль + карту',
                    4 => 'Перекинуть на покупку карты по моей реферальной ссылке',
                    5 => 'Спросить что делать',
                    6 => 'Быстрая покупка',
                ], ['prompt' => '- Ничего не выбрано -']) ?>

            <p>По умолчанию используется вариант: Открыть свой профиль.</p>
            <hr class="featurette-divider">
            <div class="form-group">
                <?= Html::submitButton('Сохранить', [
                    'class' => 'btn btn-default',
                    'name'  => 'contact-button',
                    'style' => 'width: 100%;',
                ]) ?>
            </div>
            <?php ActiveForm::end(); ?>

        <?php endif; ?>
    </div>
    <div class="col-lg-4">
        <?= $this->render('_menu') ?>
    </div>
</div>



