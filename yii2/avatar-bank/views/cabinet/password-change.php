<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \avatar\models\forms\PasswordNew */

$this->title = 'Смена пароля';

/**
 * Если я меняю пароль и у меня установлены SEEDS то нужны будут еще и SEEDS
 */

/** @var \common\models\UserAvatar $user */
$user = Yii::$app->user->identity;
?>
<div class="container">
    <div class="col-lg-12">
        <h1 class="page-header"><?= Html::encode($this->title) ?></h1>
    </div>
    <div class="col-lg-8">
        <?php if (Yii::$app->session->hasFlash('contactFormSubmitted')): ?>
            <div class="alert alert-success">
                Поздравляем вы успешно установили новый пароль.
            </div>
        <?php else: ?>
            <div class="row">
                <div class="col-lg-6">
                    <?php $form = ActiveForm::begin([
                        'id'                   => 'contact-form',
                        'enableAjaxValidation' => true,
                    ]); ?>
                    <?= $form->field($model, 'passwordOld')->passwordInput()->label('Старый пароль от кабинета') ?>
                    <?php
                    $field = $form->field($model, 'password1')->passwordInput()->label('Пароль');
                    $field->validateOnBlur = true;
                    echo $field;
                    ?>
                    <?= $form->field($model, 'password2')->passwordInput()->label('Повторите пароль') ?>
                    <?php if ($user->password_save_type == \common\models\avatar\UserBill::PASSWORD_TYPE_HIDE_CABINET) { ?>
                        <?= $form->field($model, 'seeds')->textarea(['rows' => 7])->label('SEEDS') ?>
                    <?php } ?>
                    <hr>
                    <div class="form-group">
                        <?= Html::submitButton('Изменить', [
                            'class' => 'btn btn-primary',
                            'name'  => 'contact-button',
                            'style' => 'width: 100%',
                        ]) ?>
                    </div>
                    <?php ActiveForm::end(); ?>
                </div>
            </div>
        <?php endif; ?>
    </div>
    <div class="col-lg-4">
        <?= $this->render('_menu') ?>
    </div>
</div>