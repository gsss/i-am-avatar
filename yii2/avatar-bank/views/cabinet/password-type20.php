<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;
use app\models\UnionCategory;

/* @var $this yii\web\View */
/* @var $model \avatar\models\forms\PasswordType20 */

$this->title = 'Снять шифрование к кошелькам';

/** @var \common\models\UserAvatar $user */
$user = Yii::$app->user->identity;

?>
<div class="container">
    <div class="col-lg-12">
        <h1 class="page-header"><?= Html::encode($this->title) ?></h1>
    </div>
    <div class="col-lg-8">

        <div class="row">
            <div class="col-lg-6">
                <?php if (Yii::$app->session->hasFlash('form')) { ?>

                    <p class="alert alert-success">Успешно. Теперь после сброса пароля вам не нужно будет восстанавливать доступ к своим кошелькам.</p>
                    <p>
                        <a class="btn btn-default" href="/cabinet-bills/index">Перейти к счетам</a>
                        <a class="btn btn-default" href="/cabinet/password-type">Установить защиту еще раз</a>
                    </p>
                <?php } else { ?>
                    <p class="alert alert-success">У вас установлен ключ третьего уровня защиты. Если вы хотите его снять вам нужно ввести ниже этот ключ SEEDS.</p>
                    <?php $form = ActiveForm::begin() ?>


                    <?= $form->field($model, 'seeds')->textarea(['rows' => 7]) ?>

                    <hr>
                    <?= Html::submitButton('Восстановить', [
                        'class' => 'btn btn-primary',
                        'name'  => 'contact-button',
                        'style' => 'width:100%',
                    ]) ?>

                    <?php ActiveForm::end() ?>
                <?php } ?>
            </div>
        </div>
    </div>
    <div class="col-lg-4">
        <?= $this->render('_menu') ?>
    </div>
</div>



