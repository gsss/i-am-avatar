<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;
use app\models\UnionCategory;

/* @var $this yii\web\View */
/* @var $model \avatar\models\forms\RepareWallets */

$this->title = 'Восстановление доступа к кошелькам';

/** @var \common\models\UserAvatar $user */
$user = Yii::$app->user->identity;

?>
<div class="container">
    <div class="col-lg-12">
        <h1 class="page-header"><?= Html::encode($this->title) ?></h1>

    </div>
    <div class="col-lg-8">
        <p>
            <a href="/cabinet/password-type211" class="btn btn-primary">Восстановить</a>
        </p>
        <p>
            <a href="/cabinet/password-type212" class="btn btn-primary">Снять</a>
        </p>
    </div>
    <div class="col-lg-4">
        <?= $this->render('_menu') ?>
    </div>
</div>



