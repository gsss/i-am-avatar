<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;
use app\models\UnionCategory;

/* @var $this yii\web\View */
/* @var $model \avatar\models\forms\PasswordType1 */

$this->title = 'Установка шифрования ключей';

/** @var \common\models\UserAvatar $user */
$user = Yii::$app->user->identity;

?>
<div class="container">
    <div class="col-lg-12">
        <h1 class="page-header"><?= Html::encode($this->title) ?></h1>

    </div>
    <div class="col-lg-8">

        <div class="row">
            <div class="col-lg-6">
                <?php if (Yii::$app->session->hasFlash('form')) { ?>

                    <p class="alert alert-success">Защита установлена успешно</p>

                <?php } else { ?>
                    <?php $form = ActiveForm::begin() ?>
                    <p>Для того чтобы зашифровать ваши ключи введите пароль от вашего кабинета:</p>
                    <?= $form->field($model, 'password')->passwordInput() ?>
                    <p><a href="/cabinet-bills/password-type-pdf" class="btn btn-success"><i class="glyphicon glyphicon-download"></i> Скачать PDF</a></p>
                    <?= $form->field($model, 'seeds')->textarea(['rows' => 7])->label('Скачайте документ и введите значение SEEDS в это поле') ?>
                    <?= $form->field($model, 'isAgree')->checkbox()->label('Поставив галочку вы соглашаетесь с тем что установив защиту третьего уровня вы получаете полную защиту своего кабинета и мы лично не сможем вам восстановить доступ если вы потеряете эти ключи') ?>

                    <hr>
                    <?= Html::submitButton('Установить', [
                        'class' => 'btn btn-primary',
                        'name'  => 'contact-button',
                        'style' => 'width:100%',
                    ]) ?>

                    <?php ActiveForm::end() ?>
                <?php } ?>
            </div>
        </div>
    </div>
    <div class="col-lg-4">
        <?= $this->render('_menu') ?>
    </div>
</div>



