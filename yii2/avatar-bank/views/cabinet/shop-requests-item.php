<?php

/** @var $this \yii\web\View  */
/** @var $school \common\models\school\School */
/** @var $request \common\models\shop\Request */

use cs\services\DatePeriod;
use yii\helpers\Url;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use common\models\shop\Request;


$this->title = 'Заказ #' . $request->id;

\avatar\assets\Timeline\Asset::register($this);
?>


<div class="container">
    <div class="col-lg-12">
        <h1 class="page-header"><?= Html::encode($this->title) ?></h1>

    <?php
    $this->registerJS(<<<JS
$('[data-toggle="tooltip"]').tooltip();

$('.buttonPaid1').click(function (e) {
    e.preventDefault();
    e.stopPropagation();
    if (confirm('Подтвердите действие')) {
        var button = $(this);
        var id = $(this).data('id');
        ajaxJson({
            url: '/cabinet-school-shop-requests/paid' + '?' + 'id' + '=' + id,
            success: function (ret) {
                $('#modalInfo').on('hidden.bs.modal', function() {
                    window.location.reload();
                }).modal();
            }
        });
    }
});
$('#buttonSendMessage').click(function() {
    $('#messageModal').modal('show');
    $('#buttonSendMessageForm').click(function() {
        var text = $('#messageModal textarea').val();
        ajaxJson({
            url: '/cabinet-school-shop-requests/message',
            data: {
                id: {$request->id}, 
                text: text
            },
            success: function(ret) {
                $('#messageModal').modal('hide');
                window.location.reload();
            }
        });
    });
});
JS
    );
    $c = \common\models\avatar\Currency::findOne($request->currency_id);
    $user = \common\models\UserAvatar::findOne($request->user_id);
    $d = \common\models\shop\DeliveryItem::findOne($request->dostavka_id);
    $billing = \common\models\BillingMain::findOne($request->billing_id);
    ?>


    <?= \yii\widgets\DetailView::widget([
        'model'      => $request,
        'attributes' => [
            'dostavka_id'     => [
                'label'          => 'Доставка',
                'value'          => $d->name,
            ],
            'address' => [
                'label'          => 'Адрес',
                'value'          => $request->address,
                'captionOptions' => [
                    'style' => 'width: 30%;',
                ],
            ],
            'price'   => [
                'label'          => 'Стоимость без доставки',
                'value'          => $request->price / pow(10, $c->decimals),
                'format'         => ['decimal', $c->decimals],
                'captionOptions' => [
                    'style' => 'width: 30%;',
                ],
            ],
            'sum'   => [
                'label'          => 'Стоимость с доставкой',
                'value'          => $request->sum / pow(10, $c->decimals),
                'format'         => ['decimal', $c->decimals],
                'captionOptions' => [
                    'style' => 'width: 30%;',
                ],
            ],
            'currency_id'   => [
                'label'          => 'Валюта',
                'value'          => Html::tag('span', $c->code, ['class' => 'label label-info']),
                'format'         => 'html',
                'captionOptions' => [
                    'style' => 'width: 30%;',
                ],
            ],
            'user_id'   => [
                'label'          => 'Заказчик',
                'value'          => Html::img($user->getAvatar(), ['class' => 'img-circle', 'width' => 30, 'title' => $user->getName2()]),
                'format'         => 'html',
                'captionOptions' => [
                    'style' => 'width: 30%;',
                ],
            ],
            'created_at'   => [
                'label'          => 'Создан',
                'value'          => $request->created_at,
                'format'         => 'datetime',
                'captionOptions' => [
                    'style' => 'width: 30%;',
                ],
            ],
            'ps'     => [
                'label'          => 'Платежная система',
                'value'          => $billing->getPaySystem()->title,
            ],
            'is_paid_client'     => [
                'label'          => 'Оплачен? (клиент)',
                'value'          => $request->is_paid_client ? Html::tag('span', 'Да', ['class' => 'label label-success']) : Html::tag('span', 'Нет', ['class' => 'label label-default']),
                'format'         => 'html',
            ],

            'is_paid'   => [
                'label'          => 'Оплачен?',
                'value'          => $request->is_paid? Html::tag('span', 'Да', ['class' => 'label label-success']) : Html::tag('span', 'Нет', ['class' => 'label label-default']),
                'format'         => 'html',
                'captionOptions' => [
                    'style' => 'width: 30%;',
                ],
            ],
        ],
    ]) ?>

    <?php if ($request->is_paid_client == 0 && $request->is_paid == 0) { ?>
        <p><button class="btn btn-default buttonPaid" data-id="<?= $request->id ?>">Подтвердить оплату</button></p>
    <?php } ?>

        <h2 class="page-header">Товары</h2>
        <?php
        $rows = \common\models\shop\RequestProduct::find()->where(['request_id' => $request->id])->asArray()->all();
        $ids = ArrayHelper::getColumn($rows, 'product_id');
        $products = \common\models\shop\Product::find()->where(['id' => $ids])->all();
        $p = ArrayHelper::map($products,'id', function ($item) { return $item; });
        $r2 = [];
        foreach ($rows as $r) {
            $d = $r;
            $d['product'] = $p[$r['product_id']];
            $r2[] = $d;
        }
        ?>
        <?= \yii\grid\GridView::widget([
            'dataProvider' => new \yii\data\ArrayDataProvider([
                'allModels'  => $r2,
                'pagination' => [
                    'pageSize' => 20,
                ],
            ]),
            'tableOptions' => [
                'class' => 'table table-striped table-hover',
            ],
            'rowOptions'   => function ($item) {
                $data = [
                    'data'  => ['id' => $item['id']],
                    'role'  => 'button',
                    'class' => 'rowTable'
                ];
                return $data;
            },
            'columns'      => [
                'id',
                [
                    'header'  => 'Картинка',
                    'content' => function ($item) {
                        $i = ArrayHelper::getValue($item, 'product.image', '');
                        if ($i == '') return '';

                        $i = \iAvatar777\widgets\FileUpload7\FileUpload::getFile($i, 'crop');

                        return Html::img($i, [
                            'class'  => "thumbnail",
                            'width'  => 80,
                            'height' => 80,
                            'style'  => 'margin-bottom: 0px;',
                        ]);
                    }
                ],
                [
                    'header'  => 'Наименование',
                    'content' => function ($item) {
                        $i = ArrayHelper::getValue($item, 'product.name', '');
                        if ($i == '') return '';

                        return $i;
                    }
                ],
                [
                    'header'  => 'Цена',
                    'content' => function ($item) {
                        $i = ArrayHelper::getValue($item, 'product.price', '');
                        if ($i == '') return '';

                        return Yii::$app->formatter->asDecimal($i/100, 2);
                    }
                ],
                [
                    'header'  => 'Кол-во',
                    'attribute'  => 'count',
                ],
            ],
        ]) ?>

    <h2 class="page-header">История заказа</h2>
    <?php $this->registerJs('$(".timeBack").tooltip()'); ?>
    <ul class="timeline">
        <?php
        $requestStatus = $request->status;
        foreach ($request->getMessages()->all() as $item) {
            ?>
            <?php
            $side = 'client';
            $liSuffix = '';
            $itemStatus = \yii\helpers\ArrayHelper::getValue($item, 'status', 0);
            if ($item['direction'] == (($side == 'client') ? Request::DIRECTION_TO_SHOP : Request::DIRECTION_TO_CLIENT)) {
                $liSuffix = ' class="timeline-inverted"';
            }
            if ($itemStatus == 0) {
                $type = 'message';
                $header = 'Сообщение';
                $icon = 'glyphicon-envelope';
                $color = 'info';
                $message = $item['message'];
            } else {
                $type = 'status';
                $header = Request::$statusList[$item['status']][$side];
                $icon = Request::$statusList[$item['status']]['timeLine']['icon'];
                $color = Request::$statusList[$item['status']]['timeLine']['color'];
                $message = \yii\helpers\ArrayHelper::getValue($item, 'message', '');
            }
            ?>
            <li<?= $liSuffix ?>>
                <div class="timeline-badge <?= $color ?>"><i class="glyphicon <?= $icon ?>"></i></div>
                <div class="timeline-panel">
                    <div class="timeline-heading">
                        <h4 class="timeline-title"><?= $header ?></h4>

                        <p>
                            <small class="text-muted"><i class="glyphicon glyphicon-time"></i> <abbr
                                        title="<?= Yii::$app->formatter->asDatetime($item['created_at']) ?>"
                                        class="timeBack"><?= DatePeriod::back($item['created_at']) ?></abbr></small>
                        </p>
                    </div>
                    <?php if ($message != '') { ?>
                        <div class="timeline-body">
                            <?= nl2br($message); ?>
                        </div>
                    <?php } ?>

                    <?php if (($requestStatus == Request::STATUS_ORDER_DOSTAVKA and $itemStatus == Request::STATUS_ORDER_DOSTAVKA) or ($requestStatus == Request::STATUS_PAID_CLIENT and $itemStatus == Request::STATUS_PAID_CLIENT)) { ?>
                        <hr>
                        <button class="btn btn-info" id="buttonAnswerPay">Подтвердить оплату</button>
                    <?php } ?>

                    <?php if ($requestStatus == Request::STATUS_DOSTAVKA_ADDRESS_PREPARE and $itemStatus == Request::STATUS_DOSTAVKA_ADDRESS_PREPARE) { ?>
                        <hr>
                        <button class="btn btn-default btn-xs buttonPrepared">Заказ сформирован</button>
                        <button class="btn btn-default btn-xs buttonSend">Заказ отправлен</button>
                    <?php } ?>

                    <?php if ($requestStatus == Request::STATUS_DOSTAVKA_ADDRESS_PREPARE_DONE and $itemStatus == Request::STATUS_DOSTAVKA_ADDRESS_PREPARE_DONE) { ?>
                        <hr>
                        <button class="btn btn-default btn-xs buttonSend">Заказ отправлен</button>
                    <?php } ?>

                    <?php if ($requestStatus == Request::STATUS_DOSTAVKA_SAMOVIVOZ_WAIT and $itemStatus == Request::STATUS_DOSTAVKA_SAMOVIVOZ_WAIT) { ?>
                        <hr>
                        <button class="btn btn-info buttonSamovivozDone">Заказ вручен</button>
                    <?php } ?>

                    <?php if ($requestStatus == Request::STATUS_DOSTAVKA_NALOZH_PREPARE and $itemStatus == Request::STATUS_DOSTAVKA_NALOZH_PREPARE) { ?>
                        <hr>
                        <button class="btn btn-info buttonNalozhniiPrepared">Сфомирован</button>
                        <button class="btn btn-info buttonNalozhniiSended">Отправлен</button>
                    <?php } ?>

                    <?php if ($requestStatus == Request::STATUS_DOSTAVKA_NALOZH_PREPARE_DONE and $itemStatus == Request::STATUS_DOSTAVKA_NALOZH_PREPARE_DONE) { ?>
                        <hr>
                        <button class="btn btn-info buttonNalozhniiSended">Отправлен</button>
                    <?php } ?>

                    <?php if ($requestStatus == Request::STATUS_DOSTAVKA_NALOZH_SEND_TO_USER and $itemStatus == Request::STATUS_DOSTAVKA_NALOZH_SEND_TO_USER) { ?>
                        <hr>
                        <button class="btn btn-success buttonNalozhniiPaid">Оплачено</button>
                        <button class="btn btn-danger buttonNalozhniiCanceled">Отменен</button>
                    <?php } ?>
                </div>
            </li>
        <?php } ?>
    </ul>

    <hr>


    </div>
</div>

<div class="modal fade" id="modalInfo" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="myModalLabel">Информация</h4>
            </div>
            <div class="modal-body">
                Успешно!
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
            </div>
        </div>
    </div>
</div>


<div class="modal fade" id="messageModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
     aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Сообщение</h4>
            </div>
            <div class="modal-body">
                <textarea class="form-control" rows="10"></textarea>
                <hr>
                <button class="btn btn-primary" style="width:100%;" id="buttonSendMessageForm">Отправить</button>
            </div>
        </div>
    </div>
</div>