<?php

use yii\helpers\Html;
use yii\helpers\Url;


/** @var \common\models\UserAvatar $user */
$user = Yii::$app->user->identity;

/** @var \yii\web\View $this */
$this->title = \yii\helpers\Html::encode($user->getName2());

$certList = \common\models\school\Sertificate::find()->where(['user_id' => $user->id])->all();

$rows = (count($certList) + 2) / 3;
$rows = (int)$rows;

\avatar\assets\Paralax::register($this);

\avatar\assets\SocketIO\Asset::register($this);
$serverName = \avatar\assets\SocketIO\Asset::getHost();

$background = 'https://for-people.life/upload/cloud/1546392582_SLqjARoGrO.jpg';

$school = null;
try {
    $school = \common\models\school\School::get();
    $design = \common\models\school\SchoolDesign::findOne(['school_id' => $school->id]);
    if (!is_null($design)) {
        if ($design->background_cabinet) {
            $background = $design->background_cabinet;
        }
    }
} catch (Exception $e) {

}
$video = 'XNY3YBZstLU';

$is_show = true;
if (is_null($school)) {
    // Я нахожусь на аватаре
    if ($user->is_hide_master_enter_window == 0) {
        $is_closed_master_enter_window = Yii::$app->request->cookies->get('is_closed_master_enter_window');
        if (is_null($is_closed_master_enter_window)) {
            // показывать
        } else {
            if ($is_closed_master_enter_window->value == 1) {
                $is_show = false;
            }
        }
    } else {
        // показывать
        $is_show = false;
    }
} else {
    $is_show = false;
    if (isset($design)) {
        if (!is_null($design)) {
            if ($design->video_intro) {
                $is_show = true;
                $video = $design->video_intro;
            }
        }
    }
}
if ($is_show) {
    $this->registerJs(<<<JS
$('#modalInfo').on('hide.bs.modal', function(e) {
    setCookie2('is_closed_master_enter_window', 1);
}).modal();

function setCookie2 (name, value) {
    var date = new Date();
    var now = new Date();
    var time = now.getTime();
    var expireTime = time + (1000 * 60 * 60 * 24 * 365);
    now.setTime(expireTime);
    document.cookie = name + "=" + value +
    "; expires=" + now.toGMTString() +
    "; path=/";
}

JS
    );

    $name = 'is_show';
    $nameMd5 = md5($name);
    $this->registerJs(<<<JS
$('#m2-is_show').change(function(e) {
    ajaxJson({
        url: '/cabinet/modal-master-set-value',
        data: {value: 1},
        success: function(ret) {
            
        }
    });
});
JS
    );
}

?>


<?php

$this->registerJs(<<<JS

var socket = io.connect('{$serverName}');

socket.emit('room-new-user', 1, 1);
JS
);
?>



<div class="parallax-window"
     data-parallax="scroll"
     data-image-src="<?= $background ?>"

     style="min-height: 450px; background: transparent;" >

    <div class="">

        <div class="clearfix"></div>

        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h2><?= \yii\helpers\Html::encode($user->getName2()) ?></h2>

                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                        <div class="col-md-3 col-sm-3 col-xs-12 profile_left">
                            <div class="profile_img">
                                <div id="crop-avatar">
                                    <!-- Current avatar -->
                                    <img class="img-responsive avatar-view" src="<?= $user->getAvatar() ?>" alt="Avatar" title="Change the avatar">
                                </div>
                            </div>


                            <p style="margin-top: 20px;"><a class="btn btn-success" href="/cabinet/profile"><i class="fa fa-edit m-right-xs"></i> Редактировать профиль</a></p>


                            <h3>Цифровая подпись</h3>
                            <?php $row = \common\models\UserDigitalSign::findOne(['user_id' => Yii::$app->user->id]);  ?>
                            <?php if (!is_null($row)) {  ?>
                            <p>Адрес: <code><?= $row->address ?></code></p>

                            <?php } else { ?>

                                <p>Чтобы получить цифровую подпись нажмите кнопку ниже:</p>
                                <p><a href="/cabinet-digital-sign/step2" class="btn btn-default">Получить подпись</a></p>
                                <p><a href="/cabinet-digital-sign/ver2" class="btn btn-default">Получить подпись (версия 2)</a></p>

                            <?php } ?>

                            <h3>Телефон</h3>
                            <?php if (!is_null(Yii::$app->user->identity->phone)) {  ?>
                                <p><code><?= Yii::$app->user->identity->phone ?></code></p>

                            <?php } else { ?>

                                <p><a href="/cabinet-profile-phone/index" class="btn btn-default">Указать телефон</a></p>

                            <?php } ?>

                        </div>
                        <div class="col-md-9 col-sm-9 col-xs-12">


                            <div class="" role="tabpanel" data-example-id="togglable-tabs">
                                <ul id="myTab" class="nav nav-tabs bar_tabs" role="tablist">
                                    <li role="presentation" class="active"><a href="#tab_content1" id="home-tab" role="tab" data-toggle="tab" aria-expanded="true">Задачи</a>
                                    </li>
                                </ul>
                                <div id="myTabContent" class="tab-content">
                                    <div role="tabpanel" class="tab-pane fade active in" id="tab_content1" aria-labelledby="home-tab">

                                        <?php
                                        $schoolList = \common\models\school\CommandLink::find()
                                            ->where([
                                                'user_id' => Yii::$app->user->id,
                                            ])
                                            ->select(['school_id'])
                                            ->column();

                                        $id = Yii::$app->request->get('id', \common\models\school\School::get()->id);


                                        /** @var $school \common\models\school\School */
                                        $school = \common\models\school\School::findOne($id);

                                        ?>

                                        <?= $this->render('index-list', [
                                            'school'     => $school,
                                            'schoolList' => $schoolList,
                                        ]) ?>

                                    </div>
                                    <div role="tabpanel" class="tab-pane fade" id="tab_content2" aria-labelledby="profile-tab">

                                        <!-- start user projects -->
                                        <table class="data table table-striped no-margin">
                                            <thead>
                                            <tr>
                                                <th>#</th>
                                                <th>Project Name</th>
                                                <th>Client Company</th>
                                                <th class="hidden-phone">Hours Spent</th>
                                                <th>Contribution</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <tr>
                                                <td>1</td>
                                                <td>New Company Takeover Review</td>
                                                <td>Deveint Inc</td>
                                                <td class="hidden-phone">18</td>
                                                <td class="vertical-align-mid">
                                                    <div class="progress">
                                                        <div class="progress-bar progress-bar-success" data-transitiongoal="35"></div>
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>2</td>
                                                <td>New Partner Contracts Consultanci</td>
                                                <td>Deveint Inc</td>
                                                <td class="hidden-phone">13</td>
                                                <td class="vertical-align-mid">
                                                    <div class="progress">
                                                        <div class="progress-bar progress-bar-danger" data-transitiongoal="15"></div>
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>3</td>
                                                <td>Partners and Inverstors report</td>
                                                <td>Deveint Inc</td>
                                                <td class="hidden-phone">30</td>
                                                <td class="vertical-align-mid">
                                                    <div class="progress">
                                                        <div class="progress-bar progress-bar-success" data-transitiongoal="45"></div>
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>4</td>
                                                <td>New Company Takeover Review</td>
                                                <td>Deveint Inc</td>
                                                <td class="hidden-phone">28</td>
                                                <td class="vertical-align-mid">
                                                    <div class="progress">
                                                        <div class="progress-bar progress-bar-success" data-transitiongoal="75"></div>
                                                    </div>
                                                </td>
                                            </tr>
                                            </tbody>
                                        </table>
                                        <!-- end user projects -->

                                    </div>
                                    <div role="tabpanel" class="tab-pane fade" id="tab_content3" aria-labelledby="profile-tab">
                                        <p>xxFood truck fixie locavore, accusamus mcsweeney's marfa nulla single-origin coffee squid. Exercitation +1 labore velit, blog sartorial PBR leggings next level wes anderson artisan four loko farm-to-table craft beer twee. Qui
                                            photo booth letterpress, commodo enim craft beer mlkshk </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>

<div class="modal fade" id="modalInfo" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="myModalLabel">Информация</h4>
            </div>
            <div class="modal-body">
                Успешно!
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
            </div>
        </div>
    </div>
</div>


