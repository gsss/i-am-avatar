<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;
use app\models\UnionCategory;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \avatar\models\forms\Profile */

$this->title = Yii::t('c.FMg0mLqFR9', 'Профиль');

\avatar\assets\Notify::register($this);
\avatar\assets\Clipboard::register($this);
?>
<div class="container">
    <div class="col-lg-12">
        <h1 class="page-header"><?= Html::encode($this->title) ?></h1>

    </div>
    <div class="col-lg-8">
        <?php if (Yii::$app->session->hasFlash('contactFormSubmitted')): ?>

            <div class="alert alert-success">
                <?= \Yii::t('c.FMg0mLqFR9', 'Успешно обновлено') ?>.
            </div>

        <?php else: ?>

            <?php
            $this->registerJs(<<<JS

var clipboard = new Clipboard('.buttonCopy');
clipboard.on('success', function(e) {

    new Noty({
        timeout: 1000,
        theme: 'sunset',
        type: 'success',
        layout: 'bottomLeft',
        text: 'Скопировано'
    }).show();

});

JS
            );
            ?>
            <div class="row">
                <div class="col-sm-3 text-right">
                    <b>Логин</b>
                </div>
                <div class="col-sm-6"><?= $model->email ?><button class="btn btn-default btn-xs buttonCopy" style="margin-left: 10px;" data-clipboard-text="<?= $model->email ?>">Скопировать</button></div>
            </div>
            <hr>
            <?php $form = ActiveForm::begin([
                'id'      => 'contact-form',
                'options' => ['enctype' => 'multipart/form-data'],
                'layout'  => 'horizontal',
            ]); ?>
            <?= $model->field($form, 'name_first') ?>
            <?= $model->field($form, 'name_last') ?>
            <?= $model->field($form, 'name_middle') ?>
            <?= $model->field($form, 'avatar') ?>
            <?= $model->field($form, 'gender')->dropDownList([
                1 => 'Мужской',
                2 => 'Женский',
            ],  ['prompt' => '- Ничего не выбрано -']) ?>


            <hr class="featurette-divider">
            <div class="form-group">
                <?= Html::submitButton(Yii::t('c.FMg0mLqFR9', 'Обновить'), [
                    'class' => 'btn btn-default',
                    'name'  => 'contact-button',
                    'style' => 'width: 100%;',
                ]) ?>
            </div>
            <?php ActiveForm::end(); ?>

        <?php endif; ?>
    </div>
    <div class="col-lg-4">
        <?= $this->render('_menu') ?>
    </div>
</div>



