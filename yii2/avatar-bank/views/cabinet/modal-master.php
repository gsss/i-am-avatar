<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;
use app\models\UnionCategory;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model cs\base\BaseForm */

$this->title = 'Мастер помощник';

/** @var \common\models\UserAvatar $user */
$user = Yii::$app->user->identity;

$this->registerJs(<<<JS
$('#m2-is_show').change(function(e) {
    var v = ($(this).is(':checked'))? 0 : 1;
    if (v == 0) {
        setCookie2('is_closed_master_enter_window', 0);
    }
    ajaxJson({
        url: '/cabinet/modal-master-set-value',
        data: {value: v},
        success: function(ret) {
            
        }
    });
});

function setCookie2 (name, value) {
    var date = new Date();
    var now = new Date();
    var time = now.getTime();
    var expireTime = time + (1000 * 60 * 60 * 24 * 365);
    now.setTime(expireTime);
    document.cookie = name + "=" + value +
    "; expires=" + now.toGMTString() +
    "; path=/";
}
JS
);

?>
<div class="container">
    <div class="col-lg-12">
        <h1 class="page-header"><?= Html::encode($this->title) ?></h1>

    </div>
    <div class="col-lg-8">

        <p class="alert alert-info">Это мастер помощник с видео который показывается после авторизации.</p>
        <div class="row" style="margin-top: 50px;">
            <div class="col-lg-2">
                <?= \common\widgets\CheckBox2\CheckBox::widget([
                    'model'     => new \avatar\models\M2(['is_show' => ($user->is_hide_master_enter_window == 0)]),
                    'attribute' => 'is_show',
                ]); ?>
            </div>
            <div class="col-lg-10">
                Показывать всегда при авторизации?
            </div>
        </div>
    </div>
    <div class="col-lg-4">
        <?= $this->render('_menu') ?>
    </div>
</div>



