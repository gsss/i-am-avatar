<?php

/** @var $this \yii\web\View */
/** @var $school \common\models\school\School */
/** @var $model \avatar\models\forms\CabinetKoopPodryadStep3 */
/** @var $item \common\models\KoopPodryad */

use yii\helpers\Url;
use app\services\GsssHtml;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use app\services\GetArticle\Collection;

$this->title = 'Шаг 9. Подпись протокола проведения собрания членов ВТК';

\avatar\assets\Notify::register($this);
\avatar\assets\Clipboard::register($this);

$this->registerJs(<<<JS

var clipboard = new Clipboard('.buttonCopy');
clipboard.on('success', function(e) {

    new Noty({
        timeout: 1000,
        theme: 'sunset',
        type: 'success',
        layout: 'bottomLeft',
        text: 'Скопировано'
    }).show();

});

JS
);
?>

<div class="container">
    <div class="col-lg-12">
        <h1 class="page-header"><?= Html::encode($this->title) ?></h1>
    </div>

    <div class="col-lg-12">

        <?= \yii\grid\GridView::widget([
            'dataProvider' => new \yii\data\ActiveDataProvider([
                'query'      => \common\models\KoopPodryadCommandUser::find()
                    ->where(['podryad_id' => $item->id])
                ,
                'pagination' => [
                    'pageSize' => 100,
                ],
            ]),
            'tableOptions' => [
                'class' => 'table table-striped table-hover',
            ],
            'rowOptions'   => function ($item) {
                $data = [
                    'data'  => ['id' => $item['id']],
                    'role'  => 'button',
                    'class' => 'rowTable',
                ];
                return $data;
            },
            'columns'      => [
                'id',
                [
                    'header'  => 'Пользователь',
                    'content' => function ($item) {
                        $i = ArrayHelper::getValue($item, 'user_id', '');
                        if ($i == '') return '';
                        $user = \common\models\UserAvatar::findOne($i);

                        return Html::img($user->getAvatar(), [
                            'class'  => "img-circle",
                            'width'  => 40,
                            'height' => 40,
                            'style'  => 'margin-bottom: 0px;',
                        ]);
                    },
                ],
                [
                    'header'  => 'Автор',
                    'content' => function ($item) {
                        $i = $item['protokol_address'];
                        if ($i == '') return '';
                        $short = substr($i, 0, 20) . '...';
                        $short = $i;

                        return Html::tag('code', $short, [
                            'class'  => "buttonCopy",
                            'title' => 'Нажми чтобы скопировать',
                            'data'  => [
                                'toggle' => 'tooltip',
                                'clipboard-text' => $i,
                            ],
                        ]);
                    },
                ],
                [
                    'header'  => 'Подпись',
                    'content' => function ($item) {
                        $i = $item['protokol_sign'];
                        if ($i == '') return '';
                        $short = substr($i, 0, 20) . '...';

                        return Html::tag('code', $short, [
                            'class'  => "buttonCopy",
                            'title' => 'Нажми чтобы скопировать',
                            'data'  => [
                                'toggle' => 'tooltip',
                                'clipboard-text' => $i,
                            ],
                        ]);
                    },
                ],
                'protokol_created_at:datetime:Когда',

            ],
        ]) ?>

    </div>

</div>

<div class="modal fade" id="modalInfo" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="myModalLabel">Информация</h4>
            </div>
            <div class="modal-body">
                Успешно!
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
            </div>
        </div>
    </div>
</div>