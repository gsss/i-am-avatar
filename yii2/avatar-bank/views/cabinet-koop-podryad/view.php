<?php

/** @var $this \yii\web\View */
/** @var $school \common\models\school\School */
/** @var $item \common\models\KoopPodryad */

use yii\helpers\Url;
use app\services\GsssHtml;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use app\services\GetArticle\Collection;

$this->title = 'Подряд №' . $item->id;

if (!\cs\Application::isEmpty($item['tz'])) {
    $file = pathinfo($item['tz']);
    $html = Html::img('/images/controller/cabinet/documents/file.png', ['width' => 50]) . '.' . strtoupper($file['extension']);
}

\avatar\assets\Notify::register($this);
\avatar\assets\Clipboard::register($this);

$this->registerJs(<<<JS

var clipboard = new Clipboard('.buttonCopy');
clipboard.on('success', function(e) {

    new Noty({
        timeout: 1000,
        theme: 'sunset',
        type: 'success',
        layout: 'bottomLeft',
        text: 'Скопировано'
    }).show();

});

JS
);
?>

<div class="container">
    <div class="col-lg-12">
        <h1 class="page-header"><?= Html::encode($this->title) ?></h1>
    </div>

    <div class="col-lg-12">
        <p>
            <a href="<?= Url::to(['step1', 'id' => $school->id]) ?>" class="btn btn-default" title="Договор подряда с Контрагентом" data-toggle="tooltip">Шаг 1</a>
            <a href="<?= Url::to(['step2', 'id' => $item->id]) ?>" class="btn btn-default" title="Формирование команды" data-toggle="tooltip">Шаг 2</a>
            <a href="<?= Url::to(['step3', 'id' => $item->id]) ?>" class="btn btn-default" title="Загрузка Техническое задание" data-toggle="tooltip">Шаг 3</a>
            <a href="<?= Url::to(['step4', 'id' => $item->id]) ?>" class="btn btn-default" title="Подпись ТЗ бригадой" data-toggle="tooltip">Шаг 4</a>
            <a href="<?= Url::to(['step5', 'id' => $item->id]) ?>" class="btn btn-default" title="Акт приема-передачи сырья и материалов" data-toggle="tooltip">Шаг 5</a>
            <a href="<?= Url::to(['step6', 'id' => $item->id]) ?>" class="btn btn-default" title="Акт приема передачи готовой продукции / приемки выполненных работ" data-toggle="tooltip">Шаг 6</a>
            <a href="<?= Url::to(['step7', 'id' => $item->id]) ?>" class="btn btn-default" title="Отчет об использовании сырья и материалов" data-toggle="tooltip">Шаг 7</a>
            <a href="<?= Url::to(['step8', 'id' => $item->id]) ?>" class="btn btn-default" title="Протокол проведения собрания членов ВТК" data-toggle="tooltip">Шаг 8</a>
            <a href="<?= Url::to(['step9', 'id' => $item->id]) ?>" class="btn btn-default" title="Подпись протокола проведения собрания членов ВТК" data-toggle="tooltip">Шаг 9</a>
        </p>

        <div class="col-lg-2">
            <?= $this->render('view-doc', [
                'link' => $item['dogovor_podryada'],
                'name' => 'Договор подряда с Контрагентом',
            ]) ?>
        </div>
        <div class="col-lg-2">
            <?= $this->render('view-doc', [
                'link' => $item['tz'],
                'hash' => $item['tz_hash'],
                'name' => 'Техническое задание',
            ]) ?>
        </div>
        <div class="col-lg-2">
            <?= $this->render('view-doc', [
                'link' => $item['akt_pp_sirya'],
                'name' => 'Акт приема-передачи сырья и материалов',
            ]) ?>
        </div>
        <div class="col-lg-2">
            <?= $this->render('view-doc', [
                'link' => $item['akt_pp_prod'],
                'name' => 'Акт приема передачи готовой продукции / приемки выполненных работ',
            ]) ?>
        </div>
        <div class="col-lg-2">
            <?= $this->render('view-doc', [
                'link' => $item['report'],
                'name' => 'Отчет об использовании сырья и материалов',
            ]) ?>
        </div>
        <div class="col-lg-2">
            <?= $this->render('view-doc', [
                'link' => $item['protokol'],
                'hash' => $item['protokol_hash'],
                'name' => 'Протокол проведения собрания членов ВТК',
            ]) ?>
        </div>



        <?= \yii\widgets\DetailView::widget([
            'model' => $item,
            'attributes' => [
                [
                    'label'          => 'Контрагент',
                    'captionOptions' => ['style' => 'width: 30%;'],
                    'value'          => $item->agent,
                ],
                'status',
                'created_at:datetime', // creation date formatted as datetime
            ],
        ]) ?>
    </div>

</div>

<div class="modal fade" id="modalInfo" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="myModalLabel">Информация</h4>
            </div>
            <div class="modal-body">
                Успешно!
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
            </div>
        </div>
    </div>
</div>