<?php

/** @var $this \yii\web\View */
/** @var $school \common\models\school\School */
/** @var $item \common\models\KoopPodryad */

use yii\helpers\Url;
use app\services\GsssHtml;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use app\services\GetArticle\Collection;

$this->title = 'Шаг 2. Формирование команды';

\yii\jui\JuiAsset::register($this);

?>

<div class="container">
    <div class="col-lg-12">
        <h1 class="page-header"><?= Html::encode($this->title) ?></h1>
    </div>

    <div class="col-lg-12">

        <div class="col-lg-8">
            <div class="input-group">
                <input type="hidden" id="inputExecutor-value" value="">
                <input type="text" value="" class="form-control inputExecutor" placeholder="Введите для поиска имени или email...">
                <?php
                $this->registerJs(<<<JS
$('.inputExecutor').autocomplete({
    minLength: 1,
    source: function(qry, callback1) {
            ajaxJson({
                url: '/cabinet-koop-podryad/executor-search-ajax',
                data: {
                    term: qry.term,
                    school_id: {$school->id},
                    task_id: {$item->id}
                },
                success: function(ret) {
                    var rows = [];
                    var i = 0;
                    for(i = 0; i < ret.length; i++) {
                        rows.push({
                            value: ret[i].id,
                            label: ret[i].value
                        });
                    }
                    callback1(rows);
                }
            });
        }
});
$('.inputExecutor').on('autocompleteselect', function(event, ui) {
    $('#inputExecutor-value').val(ui.item.value);
    $('.inputExecutor').val(ui.item.label);
    return false;
});
JS
                );

                ?>

                <span class="input-group-btn">
            <?php
            $this->registerJs(<<<JS
$('.buttonAddExecutor').click(function(e) {
    if ($('#inputExecutor-value').val() == '') {
        new Noty({
            timeout: 1000,
            theme: 'relax',
            type: 'warning',
            layout: 'bottomLeft',
            text: 'Вы должны выбрать сначала кого-то из поиска и списка'
        }).show();
        return;
    }
    ajaxJson({
        url: '/cabinet-koop-podryad/executor-add',
        data: {
            id: $('#inputExecutor-value').val(),
            task_id: {$item->id}
        },
        success: function(ret) {
            // console.log(ret);
            window.location.reload();
        }
    });
});
JS
            )
            ?>
                    <button class="btn btn-default buttonAddExecutor" type="button">Назначить исполнителя</button></span>
            </div>


            <?= \yii\grid\GridView::widget([
                'dataProvider' => new \yii\data\ActiveDataProvider([
                    'query'      => \common\models\KoopPodryadCommandUser::find()
                        ->where(['podryad_id' => $item->id])
                    ,
                    'pagination' => [
                        'pageSize' => 100,
                    ],
                ]),
                'tableOptions' => [
                    'class' => 'table table-striped table-hover',
                ],
                'rowOptions'   => function ($item) {
                    $data = [
                        'data'  => ['id' => $item['id']],
                        'role'  => 'button',
                        'class' => 'rowTable',
                    ];
                    return $data;
                },
                'columns'      => [
                    'id',
                    [
                        'header'  => 'Пользователь',
                        'content' => function ($item) {
                            $i = ArrayHelper::getValue($item, 'user_id', '');
                            if ($i == '') return '';
                            $user = \common\models\UserAvatar::findOne($i);

                            return Html::img($user->getAvatar(), [
                                'class'  => "img-circle",
                                'width'  => 40,
                                'height' => 40,
                                'style'  => 'margin-bottom: 0px;',
                            ]);
                        },
                    ],

                ],
            ]) ?>

            <?php
            $this->registerJs(<<<JS
$('.buttonFinish').click(function(e) {
    // if ($('#inputExecutor-value').val() == '') {
    //     new Noty({
    //         timeout: 1000,
    //         theme: 'relax',
    //         type: 'warning',
    //         layout: 'bottomLeft',
    //         text: 'Вы должны выбрать сначала кого-то из поиска и списка'
    //     }).show();
    //     return;
    // }
    ajaxJson({
        url: '/cabinet-koop-podryad/step2-finish',
        data: {
            task_id: {$item->id}
        },
        success: function(ret) {
            // console.log(ret);
            window.location = '/cabinet-koop-podryad/view?id=' + {$item->id};
        }
    });
});
JS
            )
            ?>
            <p><button class="btn-default btn buttonFinish" >Закончить и загрузить ТЗ</button> </p>
        </div>

    </div>

</div>

<div class="modal fade" id="modalInfo" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="myModalLabel">Информация</h4>
            </div>
            <div class="modal-body">
                Успешно!
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
            </div>
        </div>
    </div>
</div>