<?php

/** @var $this \yii\web\View */
/** @var $school \common\models\school\School */
/** @var $model \avatar\models\forms\CabinetKoopPodryadStep7 */
/** @var $item \common\models\KoopPodryad */

use yii\helpers\Url;
use app\services\GsssHtml;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use app\services\GetArticle\Collection;

$this->title = 'Шаг 7. Добавление Отчета об использовании сырья и материалов';

?>

<div class="container">
    <div class="col-lg-12">
        <h1 class="page-header"><?= Html::encode($this->title) ?></h1>
    </div>

    <div class="col-lg-12">

        <?php $form = \iAvatar777\services\FormAjax\ActiveForm::begin([
            'model'      => $model,
            'success' => <<<JS
function (ret) {
    $('#modalInfo').on('hidden.bs.modal', function() {
        window.location = '/cabinet-koop-podryad/view?id=' + {$item->id};
    }).modal();
}
JS
            ,
        ]); ?>

        <?= $form->field($model, 'report') ?>
        <hr>

        <?php  \iAvatar777\services\FormAjax\ActiveForm::end(); ?>

    </div>

</div>

<div class="modal fade" id="modalInfo" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="myModalLabel">Информация</h4>
            </div>
            <div class="modal-body">
                Успешно!
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
            </div>
        </div>
    </div>
</div>