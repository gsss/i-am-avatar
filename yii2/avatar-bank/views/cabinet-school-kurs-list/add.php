<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;
use app\models\UnionCategory;
use yii\db\Query;
use yii\helpers\ArrayHelper;

/** @var $this yii\web\View */
/** @var $form yii\bootstrap\ActiveForm */
/** @var $model \avatar\models\forms\school\Kurs */
/** @var $school \common\models\school\School */

$this->title = 'Добавить курс';

?>
<div class="container">

    <?php \avatar\widgets\SchoolMenu::begin(['school' => $school]) ?>
    <h1 class="page-header"><?= Html::encode($this->title) ?></h1>


    <?php if (Yii::$app->session->hasFlash('form')) : ?>

        <div class="alert alert-success">
            Успешно добавлено.
        </div>

        <p>
            <?php $id = Yii::$app->session->getFlash('form'); ?>
            <a href="<?= \yii\helpers\Url::to(['cabinet-school-kurs-list/index', 'id' => $school->id]) ?>" class="btn btn-success">Все курсы</a>
            <a href="<?= \yii\helpers\Url::to(['cabinet-school-potok/add', 'id' => $id]) ?>" class="btn btn-success">Добавить поток</a>
            <a href="<?= \yii\helpers\Url::to(['cabinet-school-potok/index', 'id' => $id]) ?>" class="btn btn-success">Все потоки</a>
        </p>

    <?php else: ?>


        <div class="row">
            <div class="col-lg-8">
                <?php $form = ActiveForm::begin([
                    'options' => ['enctype' => 'multipart/form-data'],
                ]); ?>
                <?= $model->field($form, 'name') ?>
                <?= $model->field($form, 'image') ?>
                <?= $model->field($form, 'status')->dropDownList([
                    \common\models\school\Kurs::STATUS_CONFIRM => 'Только подтверждение почты',
                    \common\models\school\Kurs::STATUS_CONFIRM_AND_REGISTRATION => 'Подтверждение и регистрация',
                ]) ?>
                <?= $model->field($form, 'daet')->textarea(['rows' => 10]) ?>
                <?= $model->field($form, 'for_who')->textarea(['rows' => 10]) ?>
                <?= $model->field($form, 'is_show') ?>
                <?= $model->field($form, 'social_net_image') ?>
                <?= $model->field($form, 'social_net_title') ?>
                <?= $model->field($form, 'social_net_description')->textarea(['rows' => 5]) ?>


                <hr>
                <div class="form-group">
                    <?= Html::submitButton('Добавить', [
                        'class' => 'btn btn-default',
                        'name'  => 'contact-button',
                        'style' => 'width:100%',
                    ]) ?>
                </div>
                <?php ActiveForm::end(); ?>
            </div>
        </div>

    <?php endif; ?>
    <?php \avatar\widgets\SchoolMenu::end() ?>


</div>
