<?php

/** @var $this \yii\web\View */
/** @var $school \common\models\school\School */

use yii\helpers\Url;
use app\services\GsssHtml;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use app\services\GetArticle\Collection;

$this->title = 'Все курсы';


?>

<div class="container">


    <h1 class="page-header"><?= Html::encode($this->title) ?></h1>

    <p><a href="<?= Url::to(['cabinet-school-kurs-list/add', 'id' => $school->id]) ?>"
          class="btn btn-default">Добавить</a></p>

    <?php \yii\widgets\Pjax::begin(); ?>
    <?php
    $this->registerJS(<<<JS
$('[data-toggle="tooltip"]').tooltip();
$('.buttonDelete').click(function (e) {
    e.preventDefault();
    e.stopPropagation();
    if (confirm('Подтвердите удаление')) {
        var button = $(this);
        var id = $(this).data('id');
        ajaxJson({
            url: '/cabinet-school-kurs-list/delete' + '?' + 'id' + '=' + id,
            success: function (ret) {
                $('#modalInfo').on('hidden.bs.modal', function() {
                    button.parent().parent().remove();
                }).modal();
            }
        });
    }
});

$('.rowTable').click(function(e) {
    window.location = '/cabinet-school-potok/index' + '?' + 'id' + '=' + $(this).data('id');
});

$('.buttonLessonList').click(function(e) {
    e.preventDefault();
    e.stopPropagation();
    window.location = '/cabinet-school-lesson/index' + '?' + 'id' + '=' + $(this).data('id');
});
$('.buttonEdit').click(function(e) {
    e.preventDefault();
    e.stopPropagation();
    window.location = '/cabinet-school-kurs-list/edit' + '?' + 'id' + '=' + $(this).data('id');
});

JS
    );
    ?>
    <?= \yii\grid\GridView::widget([
        'dataProvider' => new \yii\data\ActiveDataProvider([
            'query'      => \common\models\school\Kurs::find()
                ->where(['school_id' => $school->id])
            ,
            'pagination' => [
                'pageSize' => 20,
            ],
        ]),
        'tableOptions' => [
            'class' => 'table table-striped table-hover',
        ],
        'rowOptions'   => function ($item) {
            $data = [
                'data'  => ['id' => $item['id']],
                'role'  => 'button',
                'class' => 'rowTable',
            ];
            return $data;
        },
        'columns'      => [
            'id',
            [
                'header'  => 'Картинка',
                'content' => function ($item) {
                    $i = ArrayHelper::getValue($item, 'image', '');
                    if ($i == '') return Html::img('/images/school/kurs.png', [
                            'class'  => "img-circle",
                            'width'  => 60,
                            'height' => 60,
                            'style'  => 'margin-bottom: 0px; opacity: 0.2;',
                        ]
                    );

                    return Html::img(\common\widgets\FileUpload7\FileUpload::getFile($i, 'crop'), [
                        'class'  => "img-circle",
                        'width'  => 60,
                        'height' => 60,
                        'style'  => 'margin-bottom: 0px;',
                    ]);
                },
            ],
            'name:text:Наименование',
            [
                'header'  => 'Редактировать',
                'content' => function ($item) {
                    return Html::button('Редактировать', [
                        'class' => 'btn btn-info btn-xs buttonEdit',
                        'data'  => [
                            'id' => $item['id'],
                        ],
                    ]);
                },
            ],
            [
                'header'  => 'Уроки',
                'content' => function ($item) {
                    return Html::button('Уроки', [
                        'class' => 'btn btn-info btn-xs buttonLessonList',
                        'data'  => [
                            'id' => $item['id'],
                        ],
                    ]);
                },
            ],
            [
                'header'  => 'Показывать?',
                'content' => function ($item) {
                    if ($item['is_show'] == 1) return Html::tag('span', 'Да', ['class' => 'label label-success']);
                    return Html::tag('span', 'Нет', ['class' => 'label label-default']);
                },
            ],
            [
                'header'  => 'Удалить',
                'content' => function ($item) {
                    return Html::button('Удалить', [
                        'class' => 'btn btn-danger btn-xs buttonDelete',
                        'data'  => [
                            'id' => $item['id'],
                        ],
                    ]);
                },
            ],
        ],
    ]) ?>
    <?php \yii\widgets\Pjax::end(); ?>


</div>

<div class="modal fade" id="modalInfo" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="myModalLabel">Информация</h4>
            </div>
            <div class="modal-body">
                Успешно!
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
            </div>
        </div>
    </div>
</div>