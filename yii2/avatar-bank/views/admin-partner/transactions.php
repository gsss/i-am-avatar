<?php
/**
 * Created by PhpStorm.
 * User: Бог-Творец
 * Date: 17.11.2016
 * Time: 2:44
 */

/* @var $this yii\web\View */
/* @var $billing \common\models\avatar\UserBill */
$this->title = $billing->name;
?>

<div class="container">
<h2 class="page-header text-center"><?= $billing->name ?></h2>

<p class="text-center">
    RUB/BTC = <b id="kurs-rub-btc" data-value="1"><?= \Yii::$app->formatter->asDecimal(
            \common\models\Config::get('rate.BTC/RUB')
            , 2) ?></b> <span style="color: #ccc;">руб.</span>
</p>
<p class="text-center">
    <img src="/images/controller/cabinet-bills/transactions/bitcoin2.jpg" title="Свобода Истина Справедливость (В криптографию МЫ верим)" class="gsssTooltip" style="width: 100%;max-width: 308px;">
</p>


<div class="input-group">
        <span class="input-group-btn">

                                <?php
                                \avatar\assets\ZeroClipboard\Asset::register($this);
                                $this->registerJs(<<<JS
/**
*
* @param val
* @param separator
* @param int lessOne кол-во символов после точки, если 0 то точка не выводится, добавляются задние нули работает пока только для 0..2
* @returns {string}
*/
function formatAsDecimal (val, separator, lessOne)
{
    if (typeof separator == 'undefined') separator = ',';
    if (typeof lessOne == 'undefined') lessOne = 0;
    var pStr = '';
    var original = val;
    val = parseInt(val);
    if (val >= 1000) {
        var t = parseInt(val/1000);
        var ost = (val - t * 1000);
        var ostStr = ost;
        if  (ost == 0) {
            ostStr = '000';
        } else {
            if (ost < 10) {
                ostStr = '00' + ost;
            } else if (ost < 100) {
                ostStr = '0' + ost;
            }
        }
        pStr = t + separator + ostStr;
    } else {
        pStr = val;
    }
    var oStr = '';
    if (lessOne > 0) {
        oStr = '.';
        var d = original - parseInt(original);
        d = d * Math.pow(10, lessOne);
        d = parseInt(d);
        if (d == 0) {
            console.log(d);

            for (var i=0;i<lessOne;i++){
                oStr = oStr + '0';
            }
        } else {
            if (lessOne == 1) {
                oStr = d;
            } else {
                if (d < 10) {
                    oStr = oStr + '0' + d;
                } else {
                    oStr = oStr + d;
                }
            }
        }
    }

    return pStr + oStr;
}




        // Сюда указываем элемент, по которому будет запускаться процедура копирования.
		var target = $(".buttonCopy");
		var client = new ZeroClipboard(target);

		target.click(function (e) {
			e.preventDefault();
		});

		client.on("copy", function (event) {
			// Текст, который вставится в буфер.
			var textToCopy = $('#internalAddress').val();
			var clipboard = event.clipboardData;

			clipboard.setData("text/plain", textToCopy);
		});

		// Событие, которое вызывается после копирования. Можно, например, вывести какое-то окошко.
		client.on('aftercopy', function (event) {
			infoWindow('Скопировано!');
		});



JS
                                );

                                ?>
            <button type="button" class="btn btn-default buttonCopy">
                <i class="fa fa-files-o " style="margin-right: 5px;"></i> Скопировать в буфер
            </button>
        </span>
        <input type="text" class="form-control" placeholder="Адрес кошелька"
               style="font-family: Consolas, Courier New, monospace;"
               value="<?= $billing->address ?>"
               id="internalAddress"
            >
        <span class="input-group-btn">
            <a href="/cabinet_wallet/get-qr-code" class="btn btn-default gsssTooltip" title="Скачать QR код">
                <i class="fa fa-qrcode"></i>
            </a>
        </span>
</div>
<div class="modal fade" id="modalIn" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                        aria-hidden="true">&times;</span></button>
                <h4 class="modal-title text-center" id="exampleModalLabel">Принять на счет</h4>
            </div>
            <div class="modal-body" style="text-align: center;">

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal" style="width: 100%">Закрыть
                </button>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="modalOut" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                        aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="exampleModalLabel">Отправить на счет</h4>
            </div>
            <div class="modal-body">
                <form id="formOut">
                    <input type="hidden" id="recipient-billing_id" name="billing_id" value="<?= $billing->id ?>"
                        >
                    <div class="form-group recipient-field-address">
                        <label for="recipient-name" class="control-label">Кому:</label>
                        <input type="text" class="form-control" id="recipient-address" name="address"
                               style="font-family: Consolas, Courier New, monospace"
                            >
                        <p class="help-block help-block-error" style="display: none;"></p>
                    </div>
                    <div class="form-group recipient-field-amount">
                        <label for="recipient-name" class="control-label">Сколько:</label>

                        <div class="input-group">
                                <span class="input-group-addon" id="basic-addon1"><i class="glyphicon glyphicon-btc"
                                                                                     style="padding-right: 7px; padding-left: 2px;"></i></span>
                            <input
                                type="text"
                                class="form-control"
                                placeholder="0.00000001"
                                name="amount"
                                style="font-family: Consolas, Courier New, monospace"

                                aria-describedby="basic-addon1" autocomplete="off" id="recipient-amount">
                        </div>
                        <p class="help-block help-block-error" style="display: none;"></p>
                    </div>
                    <div class="form-group recipient-field-password">
                        <label for="recipient-name" class="control-label">Пароль:</label>
                        <input type="password" class="form-control" id="recipient-password" name="password">
                        <p class="help-block help-block-error" style="display: none;"></p>
                    </div>
                    <div class="form-group recipient-field-comment">
                        <label for="message-text" class="control-label">Комментарий:</label>
                        <textarea class="form-control" id="recipient-amount" name="comment"></textarea>
                        <p class="help-block help-block-error" style="display: none;"></p>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <?php
                $path = Yii::$app->assetManager->getBundle('avatar\assets\App\Asset')->baseUrl . '/images/ajax-loader.gif';

                $this->registerJs(<<<JS
$('#formOut .form-control').on('focus', function() {
    var o = $(this);
    var p = o.parent();
    if (p.hasClass('input-group')) {
        p = p.parent();
    }
    p.removeClass('has-error');
    p.find('p.help-block-error').hide();
});
$('.buttonSend').click(function() {
    ajaxJson({
        url: '/admin-partner/send',
        data: $('#formOut').serializeArray(),
        success: function(ret) {
            $('#modalOut').modal('hide').on('hidden.bs.modal', function(e) {
                $('#modalInfo .modal-body')
                .append(
                    $('<p>').html('Транзакция успешно отправлена')
                )
                .append(
                    $('<p>').append(
                        $('<code>').html(ret.address)
                    )
                )
                ;
                $('#modalInfo').modal();
            });
        },
        errorScript: function(ret) {
            switch (ret.id) {
                case 101:
                    $.each(ret.data,function(i,v) {
                        var name = v.name;
                        var value = v.value;
                        var o = $('#recipient-'+ name);
                        var t = $('.recipient-field-' + name);
                        t.addClass('has-error');
                        t.find('p.help-block-error').html(value.join('<br>')).show();
                    });

                    break;
            }
        }
    });
});
JS
                );
                ?>
                <button type="button" class="btn btn-success buttonSend" style="width: 100%">Оплатить</button>
            </div>
        </div>
    </div>
</div>
<h2 class="alert alert-success text-center">
    <span id="external-wallet-confirmed"><img src="<?= $path ?>"></span><br>
    <small>BTC</small>
</h2>
<h2 class="alert alert-success text-center">
    <span id="external-wallet-confirmed-rub"><img src="<?= $path ?>"></span><br>
    <small>RUB</small>
</h2>
<p style="display: none;">Неподтвержденне транзакции:<br>
    <code><span id="external-wallet-unconfirmed"></span></code>
</p>
<?php

$this->registerJs(<<<JS
$('.buttonIn').click(function(e) {

    ajaxJson({
        url: '/admin-partner/in',
        data: {
            id: {$billing->id}
        },
        success: function(ret) {
            $('#modalIn .modal-body').html(ret.html);
            $('#modalIn').modal();
        }
    });
});
$('.buttonOut').click(function(e) {
    $('#modalOut').modal();
});


var functionPageClick = function(e) {
    var o = $(this);
    var href1 = o.data('href');
    ajaxJson({
        url: href1,
        success: function(ret) {
            var o = $(ret.html);
            o.find('.js-buttonTransactionInfo').click(function(e) {
                var data = $(this).data('html');
                $('.js-hash').html(data.hash);
                $('.js-time').html(data.time);
                $('.js-confirmations').html(data.confirmations);
                $('.js-block_height').html(data.block_height);
                $('.js-block_hash').html(data.block_hash);
                $('.js-total_input_value').html(data.total_input_value);
                $('.js-total_output_value').html(data.total_output_value);
                $('.js-total_fee').html(data.total_fee);
                $('.js-download-bill').data('hash', data.hash);
                $('#modalTransactionInfo').modal();
            });
            o.find('.paginationItem').each(function(i, v) {
                var o = $(v);
                var href1 = o.attr('href');
                o.attr('href', 'javascript:void(0)');
                o.attr('data-href', href1);
                o.click(functionPageClick);
            });
            o.find('.gsssTooltip').tooltip();
            $('#transactions').html(o);
        }
    });
};
var operationList = [
    {
        url: '/admin-partner/balance-internal/' + {$billing->id},
        success: function(ret) {
            $('#internal-balance').val(ret.confirmedBalance);
            $('#external-wallet-confirmed').html(ret.confirmedBalance);
            $('#internal-balance').attr('data-balance', ret.all);
            $('#external-wallet-confirmed-rub').html(
                formatAsDecimal(
                    ret.confirmedBalance * $('#kurs-rub-btc').data('value'), ',', 2
                )
            );
            $('#transactions').data('unconfirmed', ret.unconfirmedBalance);
            $('#transactions').data('confirmed', ret.confirmedBalance);

            if (ret.unconfirmedBalance != 0) {
                $('#external-wallet-unconfirmed').html(ret.unconfirmedBalance);
                $('#external-wallet-unconfirmed').parent().parent().show();
            }
            return true;
        }
    },
    {
        url: '/admin-partner/transactions-list',
        data: function() {
            return {
                balance: $('#internal-balance').attr('data-balance'),
                id: {$billing->id}
            };
        },
        beforeSend: function() {
            $('#transactions').html(
                $('<img>', {src: '{$path}' })
            );
        },
        success: function(ret) {
            var o = $(ret.html);
            o.find('.js-buttonTransactionInfo').click(function(e) {
                var data = $(this).data('html');
                $('.js-hash').html(data.hash);
                $('.js-time').html(data.time);
                $('.js-confirmations').html(data.confirmations);
                $('.js-block_height').html(data.block_height);
                $('.js-block_hash').html(data.block_hash);
                $('.js-total_input_value').html(data.total_input_value);
                $('.js-total_output_value').html(data.total_output_value);
                $('.js-total_fee').html(data.total_fee);
                $('.js-download-bill').data('hash', data.hash);
                $('#modalTransactionInfo').modal();
            });
            o.find('.paginationItem').each(function(i, v) {
                var o = $(v);
                var href1 = o.attr('href');
                o.attr('href', 'javascript:void(0)');
                o.attr('data-href', href1);
                o.click(functionPageClick);
            });
            o.find('.gsssTooltip').tooltip();
            $('#transactions').html(o);
            return true;
        }
    }
];
var i = 0;

var functionCalc = function(i, arr) {
    var item = arr[i];
    var options = {
        url: item.url,
        success: function(ret) {
            i++;
            ret =  item.success(ret);
            if (i < operationList.length) {
                if (ret) functionCalc(i, arr);
            }
        },
        errorScript: function(ret) {
            if (typeof(item.onErrorRepeat) != "undefined") {
                if (item.onErrorRepeat) {
                    i++;
                    ret =  item.success(ret);
                    if (i <= operationList.length) {
                        if (ret) functionCalc(i, arr);
                    }
                }
            }
        }
    };
    if (typeof(item.beforeSend) != "undefined") {
        options.beforeSend = item.beforeSend;
    }
    if (typeof(item.data) != "undefined") {
        options.data = item.data();
    }

    ajaxJson(options);
};
functionCalc(i, operationList);

JS
);
?>
<div class="row">
    <div class="col-lg-10 col-lg-offset-1">
        <center>
            <div class="input-group">
                <span class="input-group-btn">
                    <a href="https://localbitcoins.net/ru/buy_bitcoins" class="btn btn-default" target="_blank">
                        <i class="glyphicon glyphicon-import" style="margin-right: 5px;"></i>Купить
                    </a>
                    <button type="button" class="btn btn-default buttonIn">
                        <i class="glyphicon glyphicon-import" style="margin-right: 5px;"></i>Принять
                    </button>
                </span>
                <input type="text" class="form-control" placeholder="0.00000001"
                       value=""
                       id="internal-balance"
                       data-balance=""
                    >

                <span class="input-group-btn">
                    <button type="button" class="btn btn-default buttonOut">
                        <i class="glyphicon glyphicon-export" style="margin-right: 5px;"></i>Перевести
                    </button>
                <a href="https://localbitcoins.net/ru/sell_bitcoins" class="btn btn-default" target="_blank">
                    <i class="glyphicon glyphicon-export" style="margin-right: 5px;"></i>Продать
                </a>
                </span>
            </div>
            <!-- /input-group -->
        </center>
    </div>
</div>
<h4 class="page-header text-center">Транзакции</h4>
<div id="transactions">

</div>


</div>