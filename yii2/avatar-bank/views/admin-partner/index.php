<?php
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */

$this->title = 'Мои счета';

$this->registerJs(<<<JS

/**
*
* @param val
* @param separator
* @param lessOne int кол-во символов после точки, если 0 то точка не выводится, добавляются задние нули работает пока только для 0..2
* @returns {string}
*/
function formatAsDecimal (val, separator, lessOne)
{
    if (typeof separator == 'undefined') separator = ',';
    if (typeof lessOne == 'undefined') lessOne = 0;
    var pStr = '';
    var original = val;
    val = parseInt(val);
    if (val >= 1000) {
        var t = parseInt(val/1000);
        var ost = (val - t * 1000);
        var ostStr = ost;
        if  (ost == 0) {
            ostStr = '000';
        } else {
            if (ost < 10) {
                ostStr = '00' + ost;
            } else if (ost < 100) {
                ostStr = '0' + ost;
            }
        }
        pStr = t + separator + ostStr;
    } else {
        pStr = val;
    }
    var oStr = '';
    if (lessOne > 0) {
        var d = original - parseInt(original);
        d = d * Math.pow(10, lessOne);
        d = parseInt(d);
        if (d == 0) {
            for (var i=0;i<lessOne;i++){
                oStr = oStr + '0';
            }
        } else {
            var i;
            var p;
            // определить склько знаков в числе после заятой
            if (lessOne == 1) {
                oStr = d;
            } else {
                oStr = repeat('0', lessOne - getNums(d));
                oStr += d;
            }
        }
    }

    return pStr + '.' + oStr;
}

/**
* Возвращает кол-во цифр в числе
* Например в числе 150 = 3 чифры
* Например в числе 999 = 3 чифры
* Например в числе 9 = 1 чифра
*
* @param  d integer
*/
function getNums(d)
{
    var i = 0;
    while(true) {
        p = Math.pow(10, i);
        if (d - p < 0) return i;
        i++;
    }
}

/**
* 
* @param  s string
* @param  n integer
* 
* @returns {string}
*/
function repeat(s, n)
{
    var a = [];
    while(a.length < n){
        a.push(s);
    }
    return a.join('');
}

    $('.rowTable').click(function() {
        window.location = '/admin-partner/transactions/' + $(this).data('id');
    });

JS
);
?>

<div class="modal fade" tabindex="-1" role="dialog" id="modalNew">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                        aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Создание счета</h4>
            </div>
            <div class="modal-body">
                <p>Название счета:</p>

                <p><input id="field-name" class="form-control"></p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary buttonModalNew">Создать</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div><!-- /.modal -->

<div class="modal fade" tabindex="-1" role="dialog" id="modalPin">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                        aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Установка PIN кода на счет</h4>
            </div>
            <div class="modal-body">
                <p>PIN код (4 символа):</p>

                <p><input id="field-pin" class="form-control"></p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary buttonModalPin">Создать</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div><!-- /.modal -->

<div class="modal fade" tabindex="-1" role="dialog" id="modalSend">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                        aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Перевод денег</h4>
            </div>
            <div class="modal-body">
                <form id="formOut">
                    <input type="hidden" id="recipient-billing_id" name="billing_id"
                        >

                    <div class="form-group recipient-field-address">
                        <label for="recipient-name" class="control-label">Кому:</label>
                        <input type="text" class="form-control" id="recipient-address" name="address"
                               style="font-family: Consolas, Courier New, monospace"
                            >

                        <p class="help-block help-block-error" style="display: none;"></p>
                    </div>
                    <div class="form-group recipient-field-amount">
                        <label for="recipient-name" class="control-label">Сколько:</label>

                        <div class="input-group">
                                <span class="input-group-addon" id="basic-addon1"><i class="glyphicon glyphicon-btc"
                                                                                     style="padding-right: 7px; padding-left: 2px;"></i></span>
                            <input
                                type="text"
                                class="form-control"
                                placeholder="0.00000001"
                                name="amount"
                                style="font-family: Consolas, Courier New, monospace"

                                aria-describedby="basic-addon1" autocomplete="off" id="recipient-amount">
                        </div>
                        <p class="help-block help-block-error" style="display: none;"></p>
                    </div>
                    <div class="form-group recipient-field-password">
                        <label for="recipient-name" class="control-label">Пароль:</label>
                        <input type="password" class="form-control" id="recipient-password" name="password">

                        <p class="help-block help-block-error" style="display: none;"></p>
                    </div>
                    <div class="form-group recipient-field-comment">
                        <label for="message-text" class="control-label">Комментарий:</label>
                        <textarea class="form-control" id="recipient-amount" name="comment"></textarea>

                        <p class="help-block help-block-error" style="display: none;"></p>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary buttonModalSend">Перевести</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div><!-- /.modal -->

<div class="modal fade" tabindex="-1" role="dialog" id="modalQrCode">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                        aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Принять на счет</h4>
            </div>
            <div class="modal-body">
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div><!-- /.modal -->

<div class="container" style="padding-bottom: 70px;">
<div class="row">
<div class="col-lg-12">
<h1 class="page-header text-center"><?= $this->title ?></h1>

<?php
$columns = [
    [
        'header'  => '',
        'content' => function ($item) {
            return
                Html::img(
                    '/images/controller/cabinet-bills/index/bitcoin2.jpg',
                    [
                        'width' => 50,
                    ]
                );
        }
    ],



];

$columns[] = Yii::$app->deviceDetect-> isMobile()? [
    'header'  => 'Счет',
    'content' => function ($item) {
        return
            Html::tag(
                'abbr',
                substr(\yii\helpers\ArrayHelper::getValue($item, 'address'), 0, 8) . '...',
                [
                    'class' => 'js-buttonTransactionInfo gsssTooltip',
                    'style' => 'font-family: "Courier New", Courier, monospace;',
                    'role'  => 'button',
                    'title' => 'Подробнее',
                    'data'  => [
                        'placement' => 'bottom',
                    ],
                ]
            );
    }
] : [
    'header'  => 'Счет',
    'content' => function ($item) {
        return
            Html::tag(
                'abbr',
                substr(\yii\helpers\ArrayHelper::getValue($item, 'address'), 0, 8) . '...',
                [
                    'class' => 'js-buttonTransactionInfo gsssTooltip',
                    'style' => 'font-family: "Courier New", Courier, monospace;',
                    'role'  => 'button',
                    'title' => 'Подробнее',
                    'data'  => [
                        'placement' => 'bottom',
                    ],
                ]
            );
    }
];

$columns = \yii\helpers\ArrayHelper::merge($columns, [ 'name:text:Назначение',
    [
        'header'         => 'Баланс',
        'contentOptions' => function ($item) {
            return [
                'class' => 'rowBill',
                'id'    => 'bill_confirmed_' . $item['id'],
                'data'  => ['id' => $item['id']],
                'style' => 'text-align: right;',
            ];
        },
        'content'        => function ($item) {
            return Html::img(
                Yii::$app->assetManager->getBundle('avatar\assets\App\Asset')->baseUrl . '/images/ajax-loader.gif'
            );
        }
    ],
    [
        'header'         => Html::tag('abbr', 'Неподтв. тр.', ['title' => 'Сумма неподтвержденные транзакции', 'data' => ['toggle' => 'tooltip']]),
        'contentOptions' => function ($item) {
            return [
                'class' => 'rowBill',
                'id'    => 'bill_unconfirmed_' . $item['id'],
                'data'  => ['id' => $item['id']],
                'style' => 'text-align: right;',
            ];
        },
        'content'        => function ($item) {
            return Html::img(
                Yii::$app->assetManager->getBundle('avatar\assets\App\Asset')->baseUrl . '/images/ajax-loader.gif'
            );
        }
    ],

    [
        'header'  => 'QR',
        'content' => function ($item) {
            return Html::button(Html::tag('i', '', ['class' => 'fa fa-qrcode']), [
                'class' => 'btn btn-default buttonQrCode',
                'data'  => [
                    'id'      => $item['id'],
                    'address' => $item['address'],
                ]
            ]);
        }
    ],
    [
        'header'  => 'Отправить',
        'content' => function ($item) {
            return Html::button('Отправить', [
                'class' => 'btn btn-default btn-xs buttonSend',
                'data'  => [
                    'id' => $item['id'],
                ]
            ]);
        }
    ],
]);

$ids = \common\models\avatar\UserBillSystem::find()->select('id')->column();
?>
<?= \yii\grid\GridView::widget([
    'dataProvider' => new \yii\data\ActiveDataProvider([
        'query' => \common\models\avatar\UserBill::find()
            ->where(['in', 'id', $ids]),
    ]),
    'tableOptions' => [
        'class' => 'table table-hover table-striped',
        'style' => 'width: auto;',
        'id'    => 'tableTransaction'
    ],
    'rowOptions'   => function ($item) {
        $data = [
            'data'  => ['id' => $item['id']],
            'role'  => 'button',
            'class' => 'rowTable'
        ];

        return $data;
    },
    'columns'      => $columns,
]) ?>

<div class="modal fade" id="modalInfo" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                        aria-hidden="true">&times;</span></button>
                <h4 class="modal-title text-center" id="myModalLabel">Информация</h4>
            </div>
            <div class="modal-body">

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" style="width: 100%;" data-dismiss="modal">Закрыть</button>
            </div>
        </div>
    </div>
</div>

<?php
$arrayBillsIds = json_encode($ids);

$this->registerJs(<<<JS
$('.buttonNew').click(function() {
    $('#modalNew').modal();
});

$('.buttonModalNew').click(function() {
    var b = $(this);
    b.off('click');
    b.attr('disabled','disabled');
    ajaxJson({
        url: '/admin-partner/new',
        data: {
            name: $('#field-name').val()
        },
        success: function(ret) {
            $('#modalNew').modal('hide');
            window.location.reload();
        }
    });
});
$('.buttonSend').click(function(e) {
    e.preventDefault();
    e.stopPropagation();
    $('#recipient-billing_id').val($(this).data('id'));
    $('#modalSend').modal();
    $('#recipient-address').focusin();
});
$('#formOut .form-control').on('focus', function() {
    var o = $(this);
    var p = o.parent();
    if (p.hasClass('input-group')) {
        p = p.parent();
    }
    p.removeClass('has-error');
    p.find('p.help-block-error').hide();
});
$('.buttonModalSend').click(function() {
    ajaxJson({
        url: '/admin-partner/send',
        data: $('#formOut').serializeArray(),
        success: function(ret) {
            $('#modalSend').modal('hide').on('hidden.bs.modal', function(e) {
                $('#modalInfo .modal-body')
                .append(
                    $('<p>').html('Транзакция успешно отправлена')
                )
                .append(
                    $('<p>').append(
                        $('<code>').html(ret.address)
                    )
                )
                ;
                $('#modalInfo').on('hidden.bs.modal', function(e) {
                    window.location.reload();
                });
                $('#modalInfo').modal();
            });


        },
        errorScript: function(ret) {
            switch (ret.id) {
                case 101:
                    $.each(ret.data,function(i,v) {
                        var name = v.name;
                        var value = v.value;
                        var o = $('#recipient-'+ name);
                        var t = $('.recipient-field-' + name);
                        t.addClass('has-error');
                        t.find('p.help-block-error').html(value.join('<br>')).show();
                    });

                    break;
            }
        }
    });

});
$('.buttonQrCode').click(function(e) {
    e.preventDefault();
    e.stopPropagation();
    var address = $(this).data('address');
    ajaxJson({
        url: '/cabinet/qr-code?address=' + address,
        success: function(ret) {
            var img = $('<p>', {class: 'text-center'}). html(
                $('<img>', {src: ret.src})
            );
            var p = $('<p>', {class: 'text-center'}).html(
                $('<code>').html(address)
            );
            $('#modalQrCode .modal-body').html('');
            $('#modalQrCode .modal-body')
            .append(img)
            .append(p)
            ;
            $('#modalQrCode').modal();
        }
    });
});

var i = 0;
var operationList = {$arrayBillsIds};

var functionCalc = function(i, arr) {
    var item = arr[i];
    var options = {
        url: '/admin-partner/get-balance',
        data: {
            id: item
        },
        success: function(ret) {
            i++;
            if (ret[4] == 1 || ret[4] == 2) {
                $('#bill_confirmed_'+ item).html(
                    ret[2]
                );
                $('#bill_unconfirmed_'+ item).html(
                    ret[3]
                );
            } else {
                $('#bill_confirmed_'+ item).html(
                    formatAsDecimal(ret[0], ',', 8)
                );
                $('#bill_unconfirmed_'+ item).html(
                    formatAsDecimal(ret[1], ',', 8)
                );
            }


            if (i < arr.length) {
                functionCalc(i, arr);
            }
        },
        errorScript: function(ret) {
            i++;
            if (i < arr.length) {
                functionCalc(i, arr);
            }
        }
    };
    if (typeof(item.beforeSend) != "undefined") {
        options.beforeSend = item.beforeSend;
    }
    if (typeof(item.data) != "undefined") {
        options.data = item.data();
    }

    ajaxJson(options);
};
functionCalc(i, operationList);



JS
);
?>
<button class="btn btn-success btn-lg buttonNew"><i class="glyphicon glyphicon-plus"></i> Создать</button>
</div>
</div>
</div>


