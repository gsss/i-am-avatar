<?php
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model \avatar\models\forms\GoogleTest2 */

$this->title = 'Вы успешно отписались от рассылки';


?>

<div class="container" style="padding-bottom: 70px;">
    <div class="col-lg-12">
        <h1 class="page-header text-center"><?= $this->title ?></h1>

        <?php if (Yii::$app->session->hasFlash('form')) { ?>
            <p class="alert alert-success">Удачно</p>
        <?php } else { ?>
            <?php $form = \yii\bootstrap\ActiveForm::begin([]) ?>
            <?= $form->field($model, 'code') ?>
            <hr>
            <?= Html::submitButton('Проверить', [
                'class' => 'btn btn-success',
            ]) ?>
            <?php \yii\bootstrap\ActiveForm::end() ?>
        <?php } ?>
    </div>
</div>
