<?php

use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */

/* @var $url string */
/* @var $secret string */

$this->title = 'Чат';

?>

<div class="container" style="padding-bottom: 70px;">
    <div class="col-lg-12">
        <h1 class="page-header text-center"><?= $this->title ?></h1>

        <?= \iAvatar777\services\DateRus\DateRus::format('день недели: f, день мес: C, день мес с ведущими нулями: E, номер месяца: J'); ?>
    </div>
</div>
