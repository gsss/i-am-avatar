<?php
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */

/* @var $url string */
/* @var $secret string */

$this->title = 'Вы успешно отписались от рассылки';

/** @var \common\models\UserAvatar $user */
$user = Yii::$app->user->identity;
$ret = $user->saveGoogleAuthCode();
$secret = $ret['secret'];
$url = $ret['url'];
?>

<div class="container" style="padding-bottom: 70px;">
    <div class="col-lg-12">
        <h1 class="page-header text-center"><?= $this->title ?></h1>

        <p>

            <?php
$content = (new \Endroid\QrCode\QrCode())
    ->setText($url)
    ->setSize(180)
    ->setPadding(20)
    ->setErrorCorrection('high')
    ->setForegroundColor(['r' => 0, 'g' => 0, 'b' => 0, 'a' => 0])
    ->setBackgroundColor(['r' => 255, 'g' => 255, 'b' => 255, 'a' => 0])
    ->setLabelFontSize(16)
    ->setImageType(\Endroid\QrCode\QrCode::IMAGE_TYPE_PNG)
    ->get('png');
$src = 'data:image/png;base64,' . base64_encode($content);
            ?>

            <img src="<?= $src ?>">
        </p>
        <p><?= $secret ?></p>
    </div>
</div>
