<?php
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */

/* @var $url string */
/* @var $secret string */

$this->title = 'Чат';

$this->registerJsFile('https://chat.i-am-avatar.com/socket.io/socket.io.js', ['depends' => ['yii\web\JqueryAsset']]);
$this->registerJs(<<<JS
var socket = io.connect('https://chat.i-am-avatar.com');

// Создаем текст сообщений для событий
var strings = {
    'connected': '[sys][time]%time%[/time]: Вы успешно соединились к сервером как [user]%name%[/user].[/sys]',
    'userJoined': '[sys][time]%time%[/time]: Пользователь [user]%name%[/user] присоединился к чату.[/sys]',
    'messageSent': '[out][time]%time%[/time]: [user]%name%[/user]: %text%[/out]',
    'messageReceived': '[in][time]%time%[/time]: [user]%name%[/user]: %text%[/in]',
    'userSplit': '[sys][time]%time%[/time]: Пользователь [user]%name%[/user] покинул чат.[/sys]'
};

socket.on('message', function (msg) {
    // Добавляем в лог сообщение, заменив время, имя и текст на полученные
    document.querySelector('#log').innerHTML += strings[msg.event].replace(/\[([a-z]+)\]/g, '<span class="$1">').replace(/\[\/[a-z]+\]/g, '</span>').replace(/\%time\%/, msg.time).replace(/\%name\%/, msg.name).replace(/\%text\%/, unescape(msg.text).replace('<', '&lt;').replace('>', '&gt;')) + '<br>';
    
    // Прокручиваем лог в конец
    document.querySelector('#log').scrollTop = document.querySelector('#log').scrollHeight;
});

// При нажатии <Enter> или кнопки отправляем текст
document.querySelector('#input').onkeypress = function(e) {
    console.log('onkeypress');

    if (e.which == '13') {
        // Отправляем содержимое input'а, закодированное в escape-последовательность
        socket.send(escape(document.querySelector('#input').value));
        // Очищаем input
        document.querySelector('#input').value = '';
    }
};
document.querySelector('#send').onclick = function() {
    console.log('send');
    socket.send(escape(document.querySelector('#input').value));
    document.querySelector('#input').value = '';
};


$('.buttonTest').click(function(e) {
    socket.emit('some-event', 2, 3);
});
JS
);
?>

<div class="container" style="padding-bottom: 70px;">
    <div class="col-lg-12">
        <h1 class="page-header text-center"><?= $this->title ?></h1>

        <style>
            #log {
                width: 590px;
                height: 290px;
                border: 1px solid rgb(192, 192, 192);
                padding: 5px;
                margin-bottom: 5px;
                font: 11pt 'Palatino Linotype';
                overflow: auto;
            }
            #input {
                width: 550px;
            }
            #send {
                width: 50px;
            }

            .in {
                color: rgb(0, 0, 0);
            }
            .out {
                color: rgb(0, 0, 0);
            }
            .time {
                color: rgb(144, 144, 144);
                font: 10pt 'Courier New';
            }
            .system {
                color: rgb(165, 42, 42);
            }
            .user {
                color: rgb(25, 25, 112);
            }
        </style>
        <div id="log"></div><br>
        <input type="text" id="input" autofocus><input type="submit" id="send" value="Send">

        <p><button class="btn btn-success buttonTest">Test</button> </p>
    </div>
</div>
