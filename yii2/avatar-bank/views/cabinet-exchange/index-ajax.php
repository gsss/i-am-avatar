<?php

use common\models\avatar\Currency;
use common\models\avatar\UserBill;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $curList array  пересечение */


$this->title = 'Обмен';

$ids = [];

foreach ($curList as $code) {
    $c = Currency::findOne(['code' => $code]);
    $ids[] = $c->id;
}

$this->registerJs(<<<JS
$('.rowTableTo').click(function(e) {
    $('#step1').on('hidden.bs.collapse', function () {
        $('#step2').collapse('show');
    });
    $('#step1').collapse('hide');
});
JS
);
?>

<?php
$columns = [
    [
        'header'  => '',
        'content' => function ($item) {
            $currency = \yii\helpers\ArrayHelper::getValue($item, 'currency', Currency::BTC);
            $title = '';
            switch ($currency) {
                case Currency::BTC:
                    $src = '/images/controller/cabinet-bills/transactions/btc.jpg';
                    $title = 'BitCoin (BTC)';
                    break;
                case Currency::ETH:
                    $src = '/images/controller/cabinet-bills/transactions/eth.png';
                    $title = 'Ethereum (ETH)';
                    break;
                default:
                    $currencyObject = Currency::findOne($currency);
                    if (is_null($currencyObject)) {
                        throw new \yii\base\Exception('Не указана валюта у счета');
                    }
                    $src = $currencyObject->image;
                    $title = $currencyObject->title . ' '. '(' . $currencyObject->code . ')';
                    break;
            }
            $imgOptions = [
                'width' => 50,
                'class' => 'img-circle',
                'data'  => [
                    'toggle' => 'tooltip',
                    'title'  => $title,
                ],
            ];

            $passwordType = \yii\helpers\ArrayHelper::getValue($item, 'password_type', UserBill::PASSWORD_TYPE_OPEN);
            if ($passwordType == UserBill::PASSWORD_TYPE_HIDE_CABINET) {
                /** @var \common\models\UserAvatar $user */
                $user = Yii::$app->user->identity;
                if ($user->wallets_is_locked) {
                    $imgOptions['style']['opacity'] = '0.3';
                }
            }

            if (isset($imgOptions['style'])) {
                if (is_array($imgOptions['style'])) {
                    $s = [];
                    foreach ($imgOptions['style'] as $key => $value) {
                        $s[] = $key . ':' . $value . ';';
                    }
                    $imgOptions['style'] = join(' ', $s);
                }
            }

            return Html::img($src, $imgOptions);
        },
    ],
];

$columns[] = Yii::$app->deviceDetect->isMobile() ? [
    'header'  => Yii::t('c.TsqnzVaJuC', 'Адрес'),
    'content' => function ($item) {
        return
            Html::tag(
                'span',
                substr($item['address'], 0, 8) . '...',
                [
                    'class' => 'js-buttonTransactionInfo textDecorated',
                    'style' => 'font-family: "Courier New", Courier, monospace;',
                    'role'  => 'button',
                    'title' => Yii::t('c.TsqnzVaJuC', 'Подробнее'),
                    'data' => [
                        'placement'      => 'bottom',
                        'toggle'         => 'tooltip',
                        'clipboard-text' => $item['address'],
                    ],
                ]
            );
    },
] : [
    'header'  => Yii::t('c.TsqnzVaJuC', 'Адрес'),
    'content' => function ($item) {
        return
            Html::tag(
                'span',
                substr($item['address'], 0, 8) . '...',
                [
                    'class' => 'js-buttonTransactionInfo textDecorated',
                    'style' => 'font-family: "Courier New", Courier, monospace;',
                    'role'  => 'button',
                    'data' => [
                        'placement'      => 'bottom',
                        'clipboard-text' => $item['address'],
                    ],
                ]
            );
    },
];

$columns = \yii\helpers\ArrayHelper::merge($columns, [
    [
        'header' => Yii::t('c.TsqnzVaJuC', 'Наименование'),
        'attribute' => 'name',
    ],
]);
?>
<?= \yii\grid\GridView::widget([
    'dataProvider' => new \yii\data\ActiveDataProvider([
        'query' => UserBill::find()->where([
            'user_id'      => Yii::$app->user->id,
            'mark_deleted' => 0,
            'currency' => $ids,
        ]),
    ]),
    'tableOptions' => [
        'class' => 'table table-hover table-striped',
        'style' => 'width: auto;',
        'id'    => 'tableTransaction',
    ],
    'summary' => '',
    'rowOptions'   => function (UserBill $item) {
        $data = [
            'data'  => [
                'id'       => $item['id'],
                'currency' => $item['currency'],
            ],
            'role'  => 'button',
            'class' => 'rowTableTo',
        ];

        if (!in_array($item['currency'], [Currency::ETH, Currency::BTC])) {
            $data['data']['address'] = $item['address'];
            $data['data']['contract'] = $item->getCurrencyObject()->getToken()->address;
        } else {
            if ($item['currency'] == Currency::ETH) {
                $data['data']['convert'] = Currency::convertBySettings(1, 'ETH');
                $data['data']['address'] = $item['address'];
            }
        }

        return $data;
    },
    'columns'      => $columns,
]) ?>