<?php

use common\models\avatar\Currency;
use common\models\avatar\UserBill;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $shapeShiftAvailable array */
/* @var $avatarCurrency array  \common\models\avatar\Currency[] */
/* @var $ret array  пересечение */


$this->title = 'Обмен';


$rows = [];
$ids = [];

foreach ($ret as $code) {
    /** @var Currency $c */
    foreach ($avatarCurrency as $c) {
        if ($c->code == $code) {
            $rows[] = $c;
            $ids[] = $c->id;
            break;
        }
    }
}

$str1 = Yii::t('c.TsqnzVaJuC', 'Вы уверены?');
$str2 = Yii::t('c.TsqnzVaJuC', 'Успешно завершено!');
$this->registerJs(<<<JS
$('.buttonClose').click(function(e) {
    e.preventDefault();
    e.stopPropagation();
    var b = $(this);
    if (confirm('{$str1}')) {
        ajaxJson({
            url: '/cabinet-bills/close',
            data: {
                id: b.data('id')
            },
            success: function(ret) {
                b.parent().parent().remove();
                $('#modalQrCode .modal-body').html('{$str2}');
                $('#modalQrCode').modal();
            }
        });
    }
});

/**
*
* @param val
* @param separator
* @param lessOne int кол-во символов после точки, если 0 то точка не выводится, добавляются задние нули работает пока только для 0..2
* @returns {string}
*/
function formatAsDecimal (val, separator, lessOne)
{
    if (typeof separator == 'undefined') separator = ',';
    if (typeof lessOne == 'undefined') lessOne = 0;
    var pStr = '';
    var original = val;
    val = parseInt(val);
    if (val >= 1000) {
        var t = parseInt(val/1000);
        var ost = (val - t * 1000);
        var ostStr = ost;
        if  (ost == 0) {
            ostStr = '000';
        } else {
            if (ost < 10) {
                ostStr = '00' + ost;
            } else if (ost < 100) {
                ostStr = '0' + ost;
            }
        }
        pStr = t + separator + ostStr;
    } else {
        pStr = val;
    }
    var oStr = '';
    if (lessOne > 0) {
        var d = original - parseInt(original);
        var i;
        d = d * Math.pow(10, lessOne);
        d = parseInt(d);
        if (d == 0) {
            for (i = 0; i < lessOne; i++) {
                oStr = oStr + '0';
            }
        } else {
            var p;
            // определить склько знаков в числе после заятой
            if (lessOne == 1) {
                oStr = d;
            } else {
                oStr = repeat('0', lessOne - getNums(d));
                oStr += d;
            }
        }
    }

    return pStr + '.' + oStr;
}

/**
* Возвращает кол-во цифр в числе
* Например в числе 150 = 3 чифры
* Например в числе 999 = 3 чифры
* Например в числе 9 = 1 чифра
*
* @param  d integer
*/
function getNums(d)
{
    var i = 0;
    while(true) {
        p = Math.pow(10, i);
        if (d - p < 0) return i;
        i++;
    }
}

/**
* 
* @param  s string
* @param  n integer
* 
* @returns {string}
*/
function repeat(s, n)
{
    var a = [];
    while(a.length < n){
        a.push(s);
    }
    return a.join('');
}





var i = 0;
var operationList = [];
$('#tableTransaction').find('tr').each(function(i,v) {
    if (typeof($(v).data('id')) != 'undefined') {
        operationList.push([
            $(v).data('id'),
            $(v).data('currency'),
            $(v).data('address'),
            $(v).data('contract'),
            $(v).data('convert')
        ]);
    } 
}); 

var functionCalc = function(i, arr) {
    var item = arr[i];
    var id = item[0];
    var currency = item[1];
    
    
    
    if (currency != 3 && currency != 4 && id != 1528 & id != 1522) {
        $.ajax({
            url: 'https://api.tokenbalance.com/token/' + item[3] + '/' + item[2],
            success: function(ret) {
                i++;
                $('#bill_confirmed_'+ id).html(
                    formatAsDecimal(ret.balance,',',2)
                );
                

                if (i < arr.length) {
                    functionCalc(i, arr);
                }
              
            }
        });
    } else if (currency == 4) {
        console.log(item);
        console.log(item[2]);
        $.ajax({
            url: 'https://api.tokenbalance.com/token/0x89205a3a3b2a69de6dbf7f01ed13b2108b2c43e7/' + item[2],
            success: function(ret) {
                console.log(ret);
                console.log(ret.eth_balance * item[4]);
                i++;
                $('#bill_confirmed_'+ id).html(
                    formatAsDecimal(ret.eth_balance * item[4],',',2)
                );
                

                if (i < arr.length) {
                    functionCalc(i, arr);
                }
            }
        });
    } else {
       ajaxJson({
            url: '/cabinet-bills/get-balance',
            data: { id: id },
            success: function(ret) {
                i++;
                $('#bill_confirmed_'+ id).html(
                    ret.confirmed
                );
                
    
                if (i < arr.length) {
                    functionCalc(i, arr);
                }
            },
            errorScript: function(ret) {
                i++;
                if (i < arr.length) {
                    functionCalc(i, arr);
                }
            }
        });
    }
    
    
};
functionCalc(i, operationList);

$('.rowTable').click(function(e) {
    ajaxJson({
        url: '/cabinet-exchange/index-ajax',
        data: {
            id: $(this).data('id')
        },
        success: function(ret) {
            $('#exchangeTo').html(ret.html);
        }
    });    
});


JS
);
?>
?>

<div class="container" style="padding-bottom: 70px;">
    <div class="col-lg-12">
        <h1 class="page-header text-center">
            <?= $this->title ?>
        </h1>
    </div>

    <div class="collapse in" id="step1">
        <div class="col-lg-6">
            <?php
            $columns = [
                [
                    'header'  => '',
                    'content' => function ($item) {
                        $currency = \yii\helpers\ArrayHelper::getValue($item, 'currency', Currency::BTC);
                        $title = '';
                        switch ($currency) {
                            case Currency::BTC:
                                $src = '/images/controller/cabinet-bills/transactions/btc.jpg';
                                $title = 'BitCoin (BTC)';
                                break;
                            case Currency::ETH:
                                $src = '/images/controller/cabinet-bills/transactions/eth.png';
                                $title = 'Ethereum (ETH)';
                                break;
                            default:
                                $currencyObject = Currency::findOne($currency);
                                if (is_null($currencyObject)) {
                                    throw new \yii\base\Exception('Не указана валюта у счета');
                                }
                                $src = $currencyObject->image;
                                $title = $currencyObject->title . ' '. '(' . $currencyObject->code . ')';
                                break;
                        }
                        $imgOptions = [
                            'width' => 50,
                            'class' => 'img-circle',
                            'data'  => [
                                'toggle' => 'tooltip',
                                'title'  => $title,
                            ],
                        ];

                        $passwordType = \yii\helpers\ArrayHelper::getValue($item, 'password_type', UserBill::PASSWORD_TYPE_OPEN);
                        if ($passwordType == UserBill::PASSWORD_TYPE_HIDE_CABINET) {
                            /** @var \common\models\UserAvatar $user */
                            $user = Yii::$app->user->identity;
                            if ($user->wallets_is_locked) {
                                $imgOptions['style']['opacity'] = '0.3';
                            }
                        }

                        if (isset($imgOptions['style'])) {
                            if (is_array($imgOptions['style'])) {
                                $s = [];
                                foreach ($imgOptions['style'] as $key => $value) {
                                    $s[] = $key . ':' . $value . ';';
                                }
                                $imgOptions['style'] = join(' ', $s);
                            }
                        }

                        return Html::img($src, $imgOptions);
                    },
                ],
            ];

            if (!YII_ENV_PROD) {
                $columns[] = [
                    'header'  => '',
                    'content' => function ($item) {
                        $passwordType = \yii\helpers\ArrayHelper::getValue($item, 'password_type', UserBill::PASSWORD_TYPE_OPEN);
                        if ($passwordType == UserBill::PASSWORD_TYPE_HIDE_CABINET) {
                            /** @var \common\models\UserAvatar $user */
                            $user = Yii::$app->user->identity;
                            if ($user->wallets_is_locked) {
                                $color = '#d9534f';
                                return Html::tag('i', null, [
                                    'class' => 'fa fa-key',
                                    'style' => 'color: ' . $color . ';',
                                    'title' => 'Пароль от кабинета сменен, доступ к кошелькам нужно восстановить',
                                    'data'  => ['toggle' => 'tooltip'],
                                ]);
                            }
                        }
                    },
                ];
            }

            $columns[] = Yii::$app->deviceDetect->isMobile() ? [
                'header'  => Yii::t('c.TsqnzVaJuC', 'Адрес'),
                'content' => function ($item) {
                    return
                        Html::tag(
                            'span',
                            substr($item['address'], 0, 8) . '...',
                            [
                                'class' => 'js-buttonTransactionInfo textDecorated',
                                'style' => 'font-family: "Courier New", Courier, monospace;',
                                'role'  => 'button',
                                'title' => Yii::t('c.TsqnzVaJuC', 'Подробнее'),
                                'data' => [
                                    'placement'      => 'bottom',
                                    'toggle'         => 'tooltip',
                                    'clipboard-text' => $item['address'],
                                ],
                            ]
                        );
                },
            ] : [
                'header'  => Yii::t('c.TsqnzVaJuC', 'Адрес'),
                'content' => function ($item) {
                    return
                        Html::tag(
                            'span',
                            substr($item['address'], 0, 8) . '...',
                            [
                                'class' => 'js-buttonTransactionInfo textDecorated',
                                'style' => 'font-family: "Courier New", Courier, monospace;',
                                'role'  => 'button',
                                'data' => [
                                    'placement'      => 'bottom',
                                    'clipboard-text' => $item['address'],
                                ],
                            ]
                        );
                },
            ];

            $columns = \yii\helpers\ArrayHelper::merge($columns, [
                [
                    'header' => Yii::t('c.TsqnzVaJuC', 'Наименование'),
                    'attribute' => 'name',
                ],
                [
                    'header'         => Yii::t('c.TsqnzVaJuC', 'Баланс'),
                    'contentOptions' => function ($item) {
                        return [
                            'class' => 'rowBill',
                            'id'    => 'bill_confirmed_' . $item['id'],
                            'data'  => ['id' => $item['id']],
                            'style' => 'text-align: right; font-family: Bender;',
                        ];
                    },
                    'content'        => function ($item) {
                        return Html::img(
                            Yii::$app->assetManager->getBundle('avatar\assets\App\Asset')->baseUrl . '/images/ajax-loader.gif'
                        );
                    },
                ],
            ]);
            ?>
            <?= \yii\grid\GridView::widget([
                'dataProvider' => new \yii\data\ActiveDataProvider([
                    'query' => UserBill::find()->where([
                        'user_id'      => Yii::$app->user->id,
                        'mark_deleted' => 0,
                        'currency' => $ids,
                    ]),
                ]),
                'tableOptions' => [
                    'class' => 'table table-hover table-striped',
                    'style' => 'width: auto;',
                    'id'    => 'tableTransaction',
                ],
                'summary' => '',
                'rowOptions'   => function (UserBill $item) {
                    $data = [
                        'data'  => [
                            'id'       => $item['id'],
                            'currency' => $item['currency'],
                        ],
                        'role'  => 'button',
                        'class' => 'rowTable',
                    ];

                    if (!in_array($item['currency'], [Currency::ETH, Currency::BTC])) {
                        $data['data']['address'] = $item['address'];
                        $data['data']['contract'] = $item->getCurrencyObject()->getToken()->address;
                    } else {
                        if ($item['currency'] == Currency::ETH) {
                            $data['data']['convert'] = Currency::convertBySettings(1, 'ETH');
                            $data['data']['address'] = $item['address'];
                        }
                    }

                    return $data;
                },
                'columns'      => $columns,
            ]) ?>

        </div>
        <div class="col-lg-6" id="exchangeTo">

        </div>
    </div>
    <div class="collapse" id="step2">
        <div class="col-lg-4 col-lg-offset-4">
            <p>Введите сумму:</p>
            <input class="form-control">
            <button class="btn btn-success" style="width: 100%;">Обменять</button>
        </div>
    </div>



</div>