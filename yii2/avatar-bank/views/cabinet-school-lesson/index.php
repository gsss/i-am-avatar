<?php

/** $this \yii\web\View  */
/** @var $kurs \common\models\school\Kurs */
/** @var $school \common\models\school\School */

use yii\helpers\Url;
use app\services\GsssHtml;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use app\services\GetArticle\Collection;

$this->title = 'Все уроки';


?>

<div class="container">

    <?php \avatar\widgets\SchoolMenu::begin(['school' => $school]) ?>
    <h1 class="page-header"><?= Html::encode($this->title) ?></h1>

    <p><a href="<?= Url::to(['cabinet-school-lesson/add', 'id' => $kurs->id]) ?>" class="btn btn-default">Добавить</a>
    </p>

    <?php \yii\widgets\Pjax::begin(); ?>
    <?php
    $this->registerJS(<<<JS
$('[data-toggle="tooltip"]').tooltip();
$('.buttonDelete').click(function (e) {
    e.preventDefault();
    e.stopPropagation();
    if (confirm('Подтвердите удаление')) {
        var button = $(this);
        var id = $(this).data('id');
        ajaxJson({
            url: '/cabinet-school-lesson/delete' + '?' + 'id' + '=' + id,
            success: function (ret) {
                $('#modalInfo').on('hidden.bs.modal', function() {
                    button.parent().parent().remove();
                }).modal();
            }
        });
    }
});
$('.buttonSubscription').click(function (e) {
    e.preventDefault();
    e.stopPropagation();
    if (confirm('Подтвердите рассылку')) {
        var button = $(this);
        var id = $(this).data('id');
        ajaxJson({
            url: '/cabinet-school-lesson/subscription' + '?' + 'id' + '=' + id,
            success: function (ret) {
                $('#modalInfo').on('hidden.bs.modal', function() {}).modal();
            }
        });
    }
});

$('.rowTable').click(function() {
    window.location = '/cabinet-school-lesson/edit' + '?' + 'id' + '=' + $(this).data('id');
});

JS
    );
    ?>
    <?= \yii\grid\GridView::widget([
        'dataProvider' => new \yii\data\ActiveDataProvider([
            'query'      => \common\models\school\Lesson::find()
                ->where(['kurs_id' => $kurs->id])
            ,
            'pagination' => [
                'pageSize' => 20,
            ],
        ]),
        'tableOptions' => [
            'class' => 'table table-striped table-hover',
        ],
        'rowOptions'   => function ($item) {
            $data = [
                'data'  => ['id' => $item['id']],
                'role'  => 'button',
                'class' => 'rowTable',
            ];

            return $data;
        },
        'columns'      => [
            'id',
            [
                'header'  => 'Картинка',
                'content' => function ($item) {
                    $i = ArrayHelper::getValue($item, 'image', '');
                    if ($i == '') return '';

                    return Html::img(
                        \common\widgets\FileUpload7\FileUpload::getFile($i, 'crop'),
                        [
                            'class'  => "thumbnail",
                            'width'  => 80,
                            'height' => 80,
                            'style'  => 'margin-bottom: 0px;',
                        ]);
                },
            ],
            'name',
            [
                'header'  => 'Тип',
                'content' => function ($item) {
                    $i = ArrayHelper::getValue($item, 'type_id', '');
                    if ($i == '') return '-';
                    $type = \common\models\school\LessonType::findOne($i);
                    if (is_null($type)) return '';

                    return $type->name;
                },
            ],
            [
                'header'  => 'Создано',
                'content' => function ($item) {
                    $v = \yii\helpers\ArrayHelper::getValue($item, 'created_at', 0);
                    if ($v == 0) return '';

                    return Html::tag('abbr', \cs\services\DatePeriod::back($v, ['isShort' => true]), ['data' => ['toggle' => 'tooltip'], 'title' => Yii::$app->formatter->asDatetime($v)]);
                },
            ],
            [
                'header'  => 'Рассылка',
                'content' => function ($item) {
                    return Html::button('Рассылка', [
                        'class' => 'btn btn-info btn-xs buttonSubscription',
                        'data'  => [
                            'id' => $item['id'],
                        ],
                    ]);
                },
            ],
            [
                'header'  => 'Удалить',
                'content' => function ($item) {
                    return Html::button('Удалить', [
                        'class' => 'btn btn-danger btn-xs buttonDelete',
                        'data'  => [
                            'id' => $item['id'],
                        ],
                    ]);
                },
            ],
        ],
    ]) ?>
    <?php \yii\widgets\Pjax::end(); ?>
    <?php \avatar\widgets\SchoolMenu::end() ?>


</div>

<div class="modal fade" id="modalInfo" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="myModalLabel">Информация</h4>
            </div>
            <div class="modal-body">
                Успешно!
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
            </div>
        </div>
    </div>
</div>