<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;
use app\models\UnionCategory;
use yii\db\Query;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/** @var $model \avatar\models\forms\school\Lesson */
/** @var $kurs \common\models\school\Kurs */
/** @var $school \common\models\school\School */

$this->title = $model->name;

?>
<div class="container">

    <?php \avatar\widgets\SchoolMenu::begin(['school' => $school]) ?>
    <h1 class="page-header"><?= Html::encode($this->title) ?></h1>

    <?php if (Yii::$app->session->hasFlash('form')) { ?>
        <div class="alert alert-success">
            Успешно обновлено.
        </div>

        <p>
            <a href="<?= \yii\helpers\Url::to(['cabinet-school-lesson/index', 'id' => $kurs->id]) ?>" class="btn btn-success">Все уроки</a>
        </p>

    <?php } else { ?>

        <div class="row">
            <div class="col-lg-8">
                <?php $form = ActiveForm::begin([
                    'id'      => 'contact-form',
                    'options' => ['enctype' => 'multipart/form-data'],
                ]); ?>
                <?= $model->field($form, 'name') ?>
                <?= $model->field($form, 'description')->textarea(['rows' => 10]) ?>
                <?= $model->field($form, 'image') ?>
                <?= $model->field($form, 'type_id')->dropDownList(
                    ArrayHelper::merge(
                        ['- Ничего не выбрано -'],
                        ArrayHelper::map(
                            \common\models\school\LessonType::find()->orderBy(['sort_index' => SORT_ASC])->all(),
                            'id',
                            'name'
                        )
                    )
                ) ?>

                <hr>
                <div class="form-group">
                    <?= Html::submitButton('Обновить', [
                        'class' => 'btn btn-default',
                        'name'  => 'contact-button',
                        'style' => 'width:100%',
                    ]) ?>
                </div>
                <?php ActiveForm::end(); ?>
            </div>
        </div>
        <?php if ($model->type_id == 4) { ?>
            <p>
                <a href="<?= \yii\helpers\Url::to(['cabinet-school-lesson/edit-place', 'id' => $model->id]) ?>">Редактировать
                    урок</a>
            </p>
        <?php } ?>
        <?php if ($model->type_id == 2) { ?>
            <p>
                <a href="<?= \yii\helpers\Url::to(['cabinet-school-lesson/edit-video', 'id' => $model->id]) ?>">Редактировать
                    урок</a>
            </p>
        <?php } ?>
        <?php if ($model->type_id == 5) { ?>
            <p>
                <a href="<?= \yii\helpers\Url::to(['cabinet-school-lesson/edit-page', 'id' => $model->id]) ?>">Редактировать
                    урок</a>
            </p>
        <?php } ?>
    <?php } ?>
    <?php \avatar\widgets\SchoolMenu::end() ?>


</div>
