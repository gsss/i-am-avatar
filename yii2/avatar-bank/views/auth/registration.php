<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \avatar\models\forms\Registration */

$this->title = Yii::t('c.0uMdJb0e0n', 'Регистрация');

$s = \common\models\school\School::get();
$logo = '/images/controller/auth/registration/avatarlogo1.png';
if (!\cs\Application::isEmpty($s->image)) {
    $logo = \iAvatar777\widgets\FileUpload7\FileUpload::getFile($s->image, 'crop');
}

?>

<div class="container">
    <div class="col-lg-4 col-lg-offset-4">
        <h1 class="page-header text-center"><?= Html::encode($this->title) ?></h1>


        <?php if (Yii::$app->session->hasFlash('contactFormSubmitted')): ?>
            <div class="alert alert-success">
                <?= \Yii::t('c.0uMdJb0e0n', 'Благодрим вас за регистрацию') ?>
            </div>
        <?php else: ?>
            <?php $form = ActiveForm::begin([
                'id'                   => 'contact-form',
                'enableAjaxValidation' => true,
            ]); ?>
            <?php $field = $form->field($model, 'email', ['inputOptions' => ['placeholder' => 'email']])->label('Введите ваш электронный адрес');
            $field->validateOnBlur = true;
            $field->validateOnChange = true;
            echo $field;
            ?>
            <?= $form->field($model, 'password1', ['inputOptions' => ['placeholder' => Yii::t('c.0uMdJb0e0n', 'Пароль')]])->passwordInput()->label(Yii::t('c.0uMdJb0e0n', 'Пароль'), ['class' => 'hide']) ?>
            <?= $form->field($model, 'password2', ['inputOptions' => ['placeholder' => Yii::t('c.0uMdJb0e0n', 'Повторите пароль')]])->passwordInput()->label(Yii::t('c.0uMdJb0e0n', 'Повторите пароль'), ['class' => 'hide']) ?>
            <?= $form->field($model, 'is_rules')->widget('\common\widgets\CheckBox2\CheckBox') ?>
            <p><a href="/files/lic.pdf" target="_blank">Лицензионный договор</a> </p>
            <hr>

            <?= $form->field($model, 'verificationCode')->widget('\cs\Widget\Google\reCaptcha\reCaptcha') ?>
            <hr>
            <div class="form-group">
                <?= Html::submitButton(Yii::t('c.0uMdJb0e0n', 'Зарегистрироваться'), [
                    'class' => 'btn btn-primary',
                    'name'  => 'contact-button',
                    'style' => 'width: 100%',
                ]) ?>
            </div>
            <?php ActiveForm::end(); ?>
        <?php endif; ?>
    </div>
</div>