<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \avatar\models\forms\Login */

$this->title = Yii::t('c.Q2vFnrwWB1', 'Вход в личный кабинет');


$logo = '/images/logo830.png';
$serverNameString = 'www.i-am-avatar.com';
$serverNameImage = '/images/domain-name.png';

$school = \common\models\school\School::get();
$url = $school->getUrl();
$serverNameString = $url;


$logo = '/images/controller/auth/login/1.jpg';
if ($school->image) {
    $logo = \iAvatar777\widgets\FileUpload7\FileUpload::getFile($school->image, 'crop');
}

$this->render('../blocks/share', [
    'image'       => Url::to('/images/controller/auth/login/1.jpg', true),
    'title'       => $this->title,
    'url'         => Url::current([], true),
    'description' => 'Вход на платформу',
])


?>
<div class="container">

    <div class="col-lg-12">
        <h1 class="page-header text-center"><?= Html::encode($this->title) ?></h1>
    </div>
    <div class="row">
        <div class="col-lg-4 col-lg-offset-4">
            <p class="text-center"><img src="<?= $logo ?>" class="thumbnail" width="100%"></p>
        </div>
    </div>
    <div class="col-lg-4 col-lg-offset-4">

        <p><span style="color: red"><?= \Yii::t('c.Q2vFnrwWB1', 'Важно') ?>
                !</span>: <?= \Yii::t('c.Q2vFnrwWB1', 'Убедитесь, что вы находитесь на сайте') ?>
            <?= $serverNameString ?></p>

        <p><?= \Yii::t('c.Q2vFnrwWB1', 'Пожалуйста заполните следующие поля для входа') ?>:</p>

        <?php $form = ActiveForm::begin(); ?>

        <?= $form->field($model, 'username', ['inputOptions' => ['placeholder' => 'Email']])->label('Email', ['class' => 'hide']) ?>
        <?= $form
            ->field(
                $model,
                'password',
                ['inputOptions' => ['placeholder' => Yii::t('c.0uMdJb0e0n', 'Пароль')]]
            )
            ->passwordInput()
            ->label(Yii::t('c.0uMdJb0e0n', 'Пароль'), ['class' => 'hide']) ?>

        <a href="/auth/password-recover"><?= \Yii::t('c.Q2vFnrwWB1', 'Восстановить пароль') ?></a>
        <hr>
        <div class="form-group">
            <?= Html::submitButton(Yii::t('c.Q2vFnrwWB1', 'Вход'), [
                'class' => 'btn btn-success',
                'name'  => 'login-button',
                'style' => 'width:100%',
            ]) ?>
        </div>
        <p><a href="/auth/registration" class="btn btn-default"
              style="width: 100%;"><?= \Yii::t('c.Q2vFnrwWB1', 'Регистрация') ?></a></p>
        <?php ActiveForm::end(); ?>
    </div>

</div>