<?php

/** @var $this \yii\web\View */
/** @var $school \common\models\school\School */

/** @var $request \common\models\school\SchoolSaleRequest */
/** @var $sale \common\models\school\SchoolSale */
/** @var $kurs \common\models\school\Kurs */
/** @var $potok \common\models\school\Potok */

use yii\helpers\Url;
use app\services\GsssHtml;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use app\services\GetArticle\Collection;

$this->title = 'Заявка #' . $request->id;

?>

<div class="container">

    <?php \avatar\widgets\SchoolMenu::begin(['school' => $school]) ?>
    <h1 class="page-header"><?= Html::encode($this->title) ?></h1>


    <?= \yii\widgets\DetailView::widget([
        'model' => $request,
        'attributes' => [
            [
                'label'          => 'id',
                'captionOptions' => [
                    'style' => 'width: 30%',
                ],
                'value'          => $request->id,
            ],
            'sale_id',
            'lid_id',
            'ps_config_id',
            'created_at:datetime:Время создания',
            'school_id',
            'is_paid',
        ],
    ]) ?>


    <?php \avatar\widgets\SchoolMenu::end() ?>


</div>

