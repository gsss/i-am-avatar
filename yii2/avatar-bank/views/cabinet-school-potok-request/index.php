<?php

/** @var $this \yii\web\View */
/** @var $school \common\models\school\School */
/** @var $kurs \common\models\school\Kurs */
/** @var $potok \common\models\school\Potok */

use yii\helpers\Url;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;

$this->title = 'Заявки';

?>

<div class="container">
    <?php \avatar\widgets\SchoolMenu::begin(['school' => $school]) ?>
    <h1 class="page-header"><?= Html::encode($this->title) ?></h1>



    <?php \yii\widgets\Pjax::begin(); ?>
    <?php
    $this->registerJS(<<<JS
$('[data-toggle="tooltip"]').tooltip();
$('.rowTable').click(function(e) {
    window.location = '/cabinet-school-potok-request/item' + '?' + 'id' + '=' + $(this).data('id');
});
$('.buttonLid').click(function(e) {
    e.preventDefault();
    e.stopPropagation();
    ajaxJson({
        url: '/cabinet-school-potok-request/lid',
        data: {id: $(this).data('id')},
        success: function(ret) {
            var m = $('#modalInfo');
            m.find('td.email').html(ret.email);
            m.find('td.name').html(ret.name);
            m.find('td.phone').html(ret.phone);
            m.modal();
        }
    });
    
});
$('.buttonAccept').click(function(e) {
    e.preventDefault();
    e.stopPropagation();
    if (confirm('Подтвердите оплату')) {
        ajaxJson({
            url: '/cabinet-school-potok-request/accept',
            data: {id: $(this).data('id')},
            success: function(ret) {
                window.location.reload();
            }
        });
    }
});

JS
    );

    $sort = new \yii\data\Sort([
        'defaultOrder' => ['created_at' => SORT_DESC],
    ])
    ?>
    <?= \yii\grid\GridView::widget([
        'dataProvider' => new \yii\data\ActiveDataProvider([
            'query'      => \common\models\school\SchoolSaleRequest::find()
                ->where(['school_id' => $school->id])
            ,
            'sort'       => $sort,
            'pagination' => [
                'pageSize' => 20,
            ],
        ]),
        'tableOptions' => [
            'class' => 'table table-striped table-hover',
        ],
        'rowOptions'   => function ($item) {
            $data = [
                'data'  => ['id' => $item['id']],
                'role'  => 'button',
                'class' => 'rowTable',
            ];
            return $data;
        },
        'columns'      => [
            'id',
            [
                'header'    => 'Продажа',
                'attribute' => 'sale_id',
                'content'   => function ($item) {
                    $s = \common\models\school\SchoolSale::findOne($item['sale_id']);

                    return $s->name;
                },
            ],
            [
                'header'    => 'LID',
                'attribute' => 'lid_id',
                'content'   => function ($item) {
                    return Html::button('LID', [
                        'class' => 'btn btn-default btn-xs buttonLid',
                        'data'  => [
                            'id' => $item['lid_id'],
                        ]
                    ]);
                },
            ],
            [
                'header'    => 'Платежная настройка',
                'attribute' => 'ps_config_id',
                'content'   => function ($item) {
                    $c = \common\models\PaySystemConfig::findOne($item['ps_config_id']);

                    return $c->name;
                },
            ],
            [
                'header'  => 'Создано',
                'content' => function ($item) {
                    $v = \yii\helpers\ArrayHelper::getValue($item, 'created_at', 0);
                    if ($v == 0) return '';

                    return Html::tag('abbr', \cs\services\DatePeriod::back($v, ['isShort' => true]), ['data' => ['toggle' => 'tooltip'], 'title' => Yii::$app->formatter->asDatetime($v)]);
                },
            ],
            [
                'header'    => 'Оплачен?',
                'attribute' => 'is_paid',
                'filter'    => [
                    0 => 'Нет',
                    1 => 'Да',
                ],
                'content'   => function ($item) {
                    $v = \yii\helpers\ArrayHelper::getValue($item, 'is_paid', 0);
                    if ($v == 0) return Html::tag('span', 'Нет', ['class' => 'label label-default']);

                    return Html::tag('span', 'Да', ['class' => 'label label-success']);
                },
            ],
            [
                'header'    => 'Подтвердить платеж',
                'content'   => function ($item) {
                    if ($item['is_paid'] == 1) {
                        return '';
                    }

                    return Html::button('Подтвердить платеж', [
                        'class' => 'btn btn-info btn-xs buttonAccept',
                        'data'  => [
                            'id' => $item['id'],
                        ]
                    ]);
                },
            ],
        ],
    ]) ?>
    <?php \yii\widgets\Pjax::end(); ?>

    <?php \avatar\widgets\SchoolMenu::end() ?>


</div>

<div class="modal fade" id="modalInfo" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="myModalLabel">Информация</h4>
            </div>
            <div class="modal-body">
                <table class="table table-striped table-hover">
                    <tr>
                        <td style="width: 30%;">
                            Email
                        </td>
                        <td class="email">

                        </td>
                    </tr>
                    <tr>
                        <td>
                            Имя
                        </td>
                        <td class="name">

                        </td>
                    </tr>
                    <tr>
                        <td>
                            Телефон
                        </td>
                        <td class="phone">

                        </td>
                    </tr>
                </table>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
            </div>
        </div>
    </div>
</div>
