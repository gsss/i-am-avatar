<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;
use app\models\UnionCategory;
use yii\db\Query;
use yii\helpers\ArrayHelper;

/** @var $this yii\web\View */
/** @var $model \avatar\models\forms\CabinetSettingsOneC */
/** @var $school \common\models\school\School */

$this->title = 'Настройки 1C API';

?>
<div class="container">


    <h1 class="page-header text-center"><?= Html::encode($this->title) ?></h1>

    <?php if (!is_null($id = Yii::$app->session->getFlash('form'))) : ?>

        <div class="alert alert-success">
            Успешно обновлено.
        </div>

        <p>
            <a href="<?= \yii\helpers\Url::to(['cabinet-school-settings/blog', 'id' => $school->id]) ?>" class="btn btn-success">Назад к настройкам</a>
        </p>

    <?php else: ?>

        <div class="row">
            <div class="col-lg-4 col-lg-offset-4">
                <p>
                    <img src="https://cloud1.cloud999.ru/upload/cloud/15898/87036_qhYKzXfOUi.jpg" width="100%" class="thumbnail">
                </p>

                <?php $form = \iAvatar777\services\FormAjax\ActiveForm::begin([
                    'model'   => $model,
                    'success' => <<<JS

function (ret) {
    $('#modalInfo').on('hidden.bs.modal', function() {
        
    }).modal();
}
JS


                ]); ?>

                <hr>
                <?= $form->field($model, 'onec_api_url') ?>
                <?= $form->field($model, 'onec_api_key')->passwordInput() ?>
                <hr>

                <?php \iAvatar777\services\FormAjax\ActiveForm::end(); ?>
            </div>
        </div>

    <?php endif; ?>



</div>


<div class="modal fade" id="modalInfo" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="myModalLabel">Информация</h4>
            </div>
            <div class="modal-body">
                Успешно!
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
            </div>
        </div>
    </div>
</div>
