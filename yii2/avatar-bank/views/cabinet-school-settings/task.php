<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;
use app\models\UnionCategory;
use yii\db\Query;
use yii\helpers\ArrayHelper;

/** @var $this yii\web\View */
/** @var $model \avatar\models\forms\SchoolDesign */
/** @var $school \common\models\school\School */

$this->title = 'Настройки задач';

?>
<div class="container">


    <h1 class="page-header"><?= Html::encode($this->title) ?></h1>

    <?php $form = \iAvatar777\services\FormAjax\ActiveForm::begin([
        'model' => $model,
        'success' => <<<JS
function (ret) {
    $('#modalInfo').on('hidden.bs.modal', function() {
    }).modal();
}
JS
,
    ]); ?>
        <?= $form->field($model, 'is_add_user_in_command_after_register') ?>
        <hr>
    <?php \iAvatar777\services\FormAjax\ActiveForm::end(); ?>





</div>

<div class="modal fade" id="modalInfo" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Информация</h4>
            </div>
            <div class="modal-body">
                Успешно!
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
            </div>
        </div>
    </div>
</div>