<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;
use app\models\UnionCategory;
use yii\db\Query;
use yii\helpers\ArrayHelper;

/** @var $this yii\web\View */
/** @var $model \common\models\school\SchoolDesign */
/** @var $school \common\models\school\School */

$this->title = 'Настройки компании';

?>
<div class="container">


    <h1 class="page-header"><?= Html::encode($this->title) ?></h1>

    <?php if (!is_null($id = Yii::$app->session->getFlash('form'))) : ?>

        <div class="alert alert-success">
            Успешно обновлено.
        </div>

        <p>
            <a href="<?= \yii\helpers\Url::to(['cabinet-school-settings/design-sub', 'id' => $school->id]) ?>" class="btn btn-success">Назад к настройкам</a>
        </p>

    <?php else: ?>

        <div class="row">
            <div class="col-lg-8">
                <?php $form = ActiveForm::begin([
                    'options' => ['enctype' => 'multipart/form-data'],
                ]); ?>

                <?= $form->field($model, 'logo_top_menu') ?>
                <?= $form->field($model, 'video_intro') ?>
                <?= $form->field($model, 'background_cabinet') ?>
                <?= $form->field($model, 'footer_site') ?>
                <?= $form->field($model, 'footer_mail') ?>
                <?= $form->field($model, 'registration_header_id') ?>
                <?= $form->field($model, 'registration_footer_id') ?>
                <?= $form->field($model, 'login_header_id') ?>
                <?= $form->field($model, 'login_footer_id') ?>
                <?= $form->field($model, 'cabinet_menu')->textarea(['rows' => 5]) ?>
                <?= $form->field($model, 'guest_frontend')->textarea(['rows' => 5]) ?>
                <?= $form->field($model, 'auth_frontend')->textarea(['rows' => 5]) ?>
                <?= $form->field($model, 'user_menu')->textarea(['rows' => 5]) ?>

                <hr>
                <div class="form-group">
                    <?= Html::submitButton('Обновить', [
                        'class' => 'btn btn-primary',
                        'name'  => 'contact-button',
                        'style' => 'width:100%',
                    ]) ?>
                </div>
                <?php ActiveForm::end(); ?>
            </div>
        </div>

    <?php endif; ?>




</div>
