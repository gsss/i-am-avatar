<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;
use app\models\UnionCategory;
use yii\db\Query;
use yii\helpers\ArrayHelper;

/** @var $this yii\web\View */
/** @var $model \common\models\school\Settings */
/** @var $school \common\models\school\School */

$this->title = 'Настройки монет';

?>
<div class="container">


    <h1 class="page-header"><?= Html::encode($this->title) ?></h1>

    <?php if (!is_null($id = Yii::$app->session->getFlash('form'))) : ?>

        <div class="alert alert-success">
            Успешно обновлено.
        </div>

        <p>
            <a href="<?= \yii\helpers\Url::to(['cabinet-school-settings/blog', 'id' => $school->id]) ?>" class="btn btn-success">Назад к настройкам</a>
        </p>

    <?php else: ?>

        <div class="row">
            <div class="col-lg-8">
                <?php $form = ActiveForm::begin([
                    'options' => ['enctype' => 'multipart/form-data'],
                ]); ?>
                <?= $form->field($model, 'blog_image_cut_id')->dropDownList([
                    0 => 'обрезать',
                    1 => 'вписывать',
                    2 => 'вписывать с фоном',
                ], ['prompt' => '- Ничего не выбрано -']) ?>

                <?= $form->field($model, 'blog_image_width') ?>
                <?= $form->field($model, 'blog_image_height') ?>

                <hr>
                <div class="form-group">
                    <?= Html::submitButton('Обновить', [
                        'class' => 'btn btn-default',
                        'name'  => 'contact-button',
                        'style' => 'width:100%',
                    ]) ?>
                </div>
                <?php ActiveForm::end(); ?>
                <p>
                    <img src="/images/controller/cabinet-school-settings/shop/s.png">
                </p>
            </div>
        </div>

    <?php endif; ?>




</div>
