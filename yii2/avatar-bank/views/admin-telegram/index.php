<?php

/** $this \yii\web\View  */

use yii\helpers\Url;
use app\services\GsssHtml;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use app\services\GetArticle\Collection;

$this->title = 'admin-telegram';


?>

<div class="container">
    <div class="col-lg-12">
        <h1 class="page-header"><?= Html::encode($this->title) ?></h1>

        <p>
            Задача по установке сертификата: <a href="https://www.i-am-avatar.com/cabinet-task-list/view?id=390">https://www.i-am-avatar.com/cabinet-task-list/view?id=390</a><br>
            Информация о сеотификатах в Telegram: <a href="https://tlgrm.ru/docs/bots/self-signed" target="_blank">https://tlgrm.ru/docs/bots/self-signed </a>
        </p>
        <p style="margin-top: 100px;">
            Ссылка:
        </p>
        <p style="margin-top: 10px;">
            <input type="text" class="form-control" id="url"/>
        </p>
        <p style="margin-top: 20px;">
            <button  class="btn btn-default buttonDelete">Удалить сертификат</button>
            <button  class="btn btn-default buttonSet">Установить сертификат</button>
            <button  class="btn btn-default buttonInfo">Получить инфо</button>
        </p>


        <form id="form_25">
            <div class="form-group required">
                <label class="control-label">Картинка</label>
                <?php $model = new \avatar\models\forms\Ax1(); ?>
                <?= \iAvatar777\widgets\FileUpload8\FileUpload::widget([
                    'model'     => $model,
                    'attribute' => 'file',
                    'settings'  => [
                        'controller'      => 'upload4',
                        'functionSuccess' => new \yii\web\JsExpression(<<<JS
function (response) {
    $('#fileUploadedPath').html(response.path);
}

JS
                        ),
                    ],
                ]) ?>
                <div id="fileUploadedPath"></div>
            </div>
        </form>

        <?php
        \avatar\assets\Notify::register($this);

        $this->registerJS(<<<JS
$('[data-toggle="tooltip"]').tooltip();
$('.buttonDelete').click(function (e) {
    e.preventDefault();
    e.stopPropagation();
    if (confirm('Подтвердите удаление')) {
        var button = $(this);
        var id = $(this).data('id');
        ajaxJson({
            url: '/admin-telegram/remove-call-back',
            success: function (ret) {
                console.log(ret);
                if (ret.ok) {
                    if (ret.result) {
                        new Noty({
                            timeout: 3000,
                            theme: 'sunset',
                            type: 'success',
                            layout: 'bottomLeft',
                            text: ret.description
                        }).show();
                    }
                }
            }
        });
    }
});
$('.buttonInfo').click(function (e) {
    e.preventDefault();
    e.stopPropagation();
    ajaxJson({
        url: '/admin-telegram/get-webhook-info',
        success: function (ret) {
            console.log(ret);
            if (ret.ok) {
                if (ret.result.has_custom_certificate) {
                    new Noty({
                        timeout: 3000,
                        theme: 'sunset',
                        type: 'success',
                        layout: 'bottomLeft',
                        text: ret.result.url
                    }).show();
                } else {
                    new Noty({
                        timeout: 3000,
                        theme: 'sunset',
                        type: 'error',
                        layout: 'bottomLeft',
                        text: 'has_custom_certificate: false'
                    }).show();
                }
            }
        }
    });
});
$('.buttonSet').click(function (e) {
    e.preventDefault();
    e.stopPropagation();
    if (confirm('Подтвердите установку')) {
        if ($('#fileUploadedPath').length == 0) {
            new Noty({
                timeout: 3000,
                theme: 'sunset',
                type: 'error',
                layout: 'bottomLeft',
                text: 'Не загружен файл'
            }).show();
        } else {
            ajaxJson({
                url: '/admin-telegram/set-call-back',
                data: {
                    path: $('#fileUploadedPath').html(),
                    url: $('#url').val()
                },
                success: function (ret) {
                    console.log(ret);
                    if (ret.ok) {
                        if (ret.result) {
                            new Noty({
                                timeout: 3000,
                                theme: 'sunset',
                                type: 'success',
                                layout: 'bottomLeft',
                                text: ret.description
                            }).show();
                        }
                    } else {
                        new Noty({
                            timeout: 3000,
                            theme: 'sunset',
                            type: 'error',
                            layout: 'bottomLeft',
                            text: ret.error_code + ': ' +ret.description
                        }).show();
                    }
                }
            });
        }
            
    }
});

JS
        );

        ?>

    </div>
</div>

<div class="modal fade" id="modalInfo" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Информация</h4>
            </div>
            <div class="modal-body">
                Успешно!
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
            </div>
        </div>
    </div>
</div>