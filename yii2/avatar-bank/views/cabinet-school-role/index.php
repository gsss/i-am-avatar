<?php

/** $this \yii\web\View  */
/** @var $school \common\models\school\School */

use yii\helpers\Url;
use app\services\GsssHtml;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use app\services\GetArticle\Collection;

$this->title = 'Все роли';
\avatar\assets\Notify::register($this);


?>

<div class="container">


    <h1 class="page-header"><?= Html::encode($this->title) ?></h1>



    <?php \yii\widgets\Pjax::begin(); ?>
    <?php
    $this->registerJS(<<<JS
$('[data-toggle="tooltip"]').tooltip();
$('.buttonDelete').click(function (e) {
    e.preventDefault();
    e.stopPropagation();
    if (confirm('Подтвердите удаление')) {
        var button = $(this);
        var id = $(this).data('id');
        ajaxJson({
            url: '/cabinet-school-role/delete',
            data: {id:id},
            success: function (ret) {
                $('#modalInfo').on('hidden.bs.modal', function() {
                    button.parent().parent().remove();
                }).modal();
            }
        });
    }
});

$('.rowTable').click(function() {
    // window.location = '/cabinet-school-role/edit' + '?' + 'id' + '=' + $(this).data('id');
});
JS
    );
    ?>

    <?= \yii\grid\GridView::widget([
        'dataProvider' => new \yii\data\ActiveDataProvider([
            'query'      => \common\models\school\Role::find()
            ,
            'pagination' => [
                'pageSize' => 20,
            ],
        ]),
        'tableOptions' => [
            'class' => 'table table-striped table-hover',
        ],
        'rowOptions'   => function ($item) {
            $data = [
                'data'  => ['id' => $item['id']],
                'role'  => 'button',
                'class' => 'rowTable',
            ];
            return $data;
        },
        'columns'      => [
            'id',
            'name',


        ],
    ]) ?>
    <?php \yii\widgets\Pjax::end(); ?>


    <?php \yii\widgets\Pjax::begin(); ?>
    <?php
    $this->registerJS(<<<JS
$('[data-toggle="tooltip"]').tooltip();

JS
    );
    $model = new \avatar\models\search\UserSchool();
    $provider = $model->search(Yii::$app->request->get(), [
        'user_school_link.school_id' => $school->id,
        'user_root.avatar_status'    => 2,
    ]);
    ?>
    <?= \yii\grid\GridView::widget([
        'dataProvider' => $provider,
        'filterModel'  => $model,
        'tableOptions' => [
            'class' => 'table table-striped table-hover',
        ],
        'rowOptions'   => function ($item) {
            $data = [
                'data'  => ['id' => $item['id']],
                'role'  => 'button',
                'class' => 'rowTable',
            ];
            return $data;
        },
        'columns'      => [
            [
                'attribute'     => 'id',
                'headerOptions' => [
                    'style' => 'width: 10%',
                ],
            ],
            [
                'header'        => 'Почта',
                'attribute'     => 'email',
                'headerOptions' => [
                    'style' => 'width: 75%',
                ],
            ],
        ],
    ]) ?>
    <?php \yii\widgets\Pjax::end(); ?>


</div>

<div class="modal fade" id="modalInfo" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="myModalLabel">Информация</h4>
            </div>
            <div class="modal-body">
                Успешно!
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
            </div>
        </div>
    </div>
</div>
