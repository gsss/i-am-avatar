<?php
/**
 * Created by PhpStorm.
 * User: god
 * Date: 18.02.2017
 * Time: 3:12
 */
/** @var $merchant \avatar\models\forms\UserBillMerchant */

?>

<form action="https://www.avatar-bank.com/merchant/index" method="post" target="_blank">
    <p>Комментарий:</p>
    <input type="text" name="title">
    <input type="hidden" name="key" value="<?= $merchant->code ?>">
    <input type="hidden" name="currency" value="BTC">
    <p>Сколько:</p>
    <input type="text" name="price">
    <p>Валюта:</p>
    <select name="currency">
        <option value="RUB">Рубль (RUB)</option>
        <option value="USD">Доллар (USD)</option>
        <option value="BTC">Bitcoin (BTC)</option>
        <option value="ETH">Эфир (ETH)</option>
        <option value="EUR">Евро (EUR)</option>
    </select>
    <button type="submit" name="pay-button">Оплатить</button>
</form>
