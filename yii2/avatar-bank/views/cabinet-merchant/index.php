<?php
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model \avatar\models\forms\UserBillMerchant */
/* @var $billing \common\models\avatar\UserBill */
/* @var $formCode string */
/* @var $formCode2 string */

$this->title = 'Мерчант для счета #' . $billing->id;


$merchantUrl = (new \cs\services\Url('https://www.avatar-bank.com/merchant'))->addParam('key', $model->code)->__toString();

?>

<div class="container" style="padding-bottom: 70px;">
    <div class="col-lg-12">
        <h1 class="page-header text-center">
            <?= $this->title ?>
        </h1>
    </div>
    <style>
        .code {
            font-family: "Courier New", Courier, monospace;
        }
    </style>
    <div class="col-lg-10 col-lg-offset-1">
        <?php if (Yii::$app->session->hasFlash('form')) { ?>
            <p class="alert alert-success">Успешно.</p>
        <?php } else { ?>
            <?php $form = \yii\bootstrap\ActiveForm::begin([
                'options' => ['enctype' => 'multipart/form-data', 'style' => 'margin-bottom: 100px;'],
                'layout'  => 'horizontal',
            ]) ?>
            <?= $model->field($form, 'name') ?>
            <?= $model->field($form, 'image_top') ?>
            <?= $model->field($form, 'confirmations') ?>
            <?= $form->field($model, 'code', ['inputOptions' => ['disabled' => 'disabled']]) ?>
            <h3 class="page-header">Уведомление по почте</h3>
            <?= $model->field($form, 'is_email') ?>
            <?= $model->field($form, 'is_email_email') ?>
            <h3 class="page-header">Уведомление по HTTP</h3>
            <?= $model->field($form, 'is_http') ?>
            <?= $model->field($form, 'is_http_address') ?>
            <?= $form->field($model, 'secret')->widget('\avatar\widgets\SecretKey') ?>
            <hr>
            <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success btn-lg', 'style' => 'width: 100%']) ?>
            <?php \yii\bootstrap\ActiveForm::end() ?>

            <h2 class="page-header text-center">Формирование ссылки</h2>
            <form

                    class="form-horizontal"
                    action="https://www.avatar-bank.com/merchant/index"
                    method="get"
            >
                <input type="hidden" class="form-control" name="key"
                       value="yBVTWeU0KNrXjBQsuFLk2X1UWRFtvAJRpypsvSgJ">


                <div class="form-group field-arayampa-title">
                    <div class="col-sm-12">
                        <a target="_blank" href="<?= $merchantUrl ?>" id="merchantLink"
                           style='font-family: Courier, "Courier new", monospace'><?= $merchantUrl ?></a>
                    </div>
                </div>
                <div class="form-group field-arayampa-title">
                    <label class="control-label col-sm-5" for="arayampa-title">Назначение
                        платежа</label>
                    <div class="col-sm-7">
                        <input type="text" id="arayampa-title" class="form-control" name="title"
                               value="">

                        <div class="help-block help-block-error "></div>
                    </div>
                </div>
                <div class="form-group field-arayampa-title">
                    <label class="control-label col-sm-5">Сумма</label>
                    <div class="col-sm-7">
                        <div class="input-group">
                            <input type="text"
                                   name="amount"
                                   class="form-control"
                                   aria-label="..."
                                   placeholder="0.00000001"
                                   autocomplete="off"
                                   id="recipient-amount"
                                   style="font-family: Consolas, Courier New, monospace">
                            <input type="hidden"
                                   name="currency"
                                   id="recipient-currency"
                                   value="BTC"
                            >
                            <div class="input-group-btn">
                                <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown"
                                        aria-haspopup="true"
                                        aria-expanded="false"><span class="buttonCurrency">BTC</span> <span
                                            class="caret"></span></button>
                                <ul class="dropdown-menu dropdown-menu-right">
                                    <?php
                                    $this->registerJs(<<<JS
var currency = 'BTC';
$('.itemCurrency').click(function(e) {
    $('#recipient-currency').val($(this).data('code'));
    currency = $(this).data('code');
    $('.buttonCurrency').html($(this).html());
    var i,s='',c=$(this).data('decimals');
    for(i = 0; i < c; i++) {
        s = s + '0';
    }
    $('#recipient-amount').attr('placeholder', '0.'+ s);
    updateLink();
});

var updateLink = function() {
   var url = '{$merchantUrl}';
   var urlHtml = '{$merchantUrl}';
   var o = $('#arayampa-title');
   
   if (o.val() != '') {
       url = url + '&amp;' + 'title' + '=' + encodeURIComponent(o.val())
       urlHtml = urlHtml + '&' + 'title' + '=' + encodeURIComponent(o.val())
   }
   var price = $('#recipient-amount').val();
   if (price != '') {
       url = url + '&amp;' + 'price' + '=' + encodeURIComponent(price);
       urlHtml = urlHtml + '&' + 'price' + '=' + encodeURIComponent(price);
       url = url + '&amp;' + 'currency' + '=' + encodeURIComponent(currency);
       urlHtml = urlHtml + '&' + 'currency' + '=' + encodeURIComponent(currency);
   }
   $('#merchantLink').html(url).attr('href', urlHtml);
};
$('#arayampa-title').on('input', updateLink);
$('#recipient-amount').on('input', updateLink);
JS
                                    );
                                    ?>
                                    <?php foreach (\common\models\avatar\Currency::find()->all() as $currency) { ?>
                                        <li>
                                            <a
                                                    href="javascript:void(0);"
                                                    class="itemCurrency"
                                                    data-code="<?= $currency->code ?>"
                                                    data-kurs="<?= $currency->kurs ?>"
                                                    data-decimals="<?= $currency->decimals ?>"
                                                    data-id="<?= $currency->id ?>">
                                                <?= $currency->code ?>
                                            </a>
                                        </li>
                                    <?php } ?>
                                </ul>
                            </div><!-- /btn-group -->
                        </div><!-- /input-group -->


                        <div class="help-block help-block-error "></div>
                    </div>
                </div>
            </form>


            <h2 class="page-header text-center">Код формы для сайта</h2>
            <style>
                textarea.form-control {
                    font-family: Courier, "Courier new", monospace;
                }
            </style>

            <div class="form-group">
                <p>Разместив этот код вы сможете собирать деньги с свой счет.</p>
                <label class="control-label" for="loginform-username">Код формы (bootstrap):</label>
                <textarea class="form-control" rows="8"><?= Html::encode($formCode) ?></textarea>
            </div>
            <div class="form-group">
                <label class="control-label" for="loginform-username">Код формы:</label>
                <textarea class="form-control" rows="8"><?= Html::encode($formCode2) ?></textarea>
            </div>


            <h2 class="page-header text-center">Счета</h2>
            <?php \yii\widgets\Pjax::begin() ?>
            <?php $sort = new \yii\data\Sort([
                'defaultOrder' => [
                    'id' => SORT_DESC,
                ],
            ]) ?>
            <?= \yii\grid\GridView::widget([
                'dataProvider' => new \yii\data\ActiveDataProvider([
                    'query' => \common\models\MerchantRequest::find()
                        ->select([
                            'merchant_request.id',
                            'merchant_request.billing_id',
                            'merchant_request.created_at',
                            'merchant_request.title',
                            'merchant_request.is_paid',
                            'merchant_request.price',
                            'payments_bit_coin.address',
                            'payments_bit_coin.confirmations',
                            'payments_bit_coin.transaction_hash',
                        ])
                        ->innerJoin('payments_bit_coin', 'payments_bit_coin.billing_id = merchant_request.billing_id')
                        ->where(['merchant_id' => $model->id])
                        ->asArray()
                    ,
                    'sort'  => $sort,
                ]),
                'tableOptions' => [
                    'class' => 'table table-striped table-hover',
                ],
                'columns'      => [
                    'billing_id',
                    [
                        'header'  => 'Создано',
                        'content' => function ($item) {
                            $v = \yii\helpers\ArrayHelper::getValue($item, 'created_at', 0);
                            if ($v == 0) return '';

                            return Html::tag('abbr', \cs\services\DatePeriod::back($v, ['isShort' => true]), ['data' => ['toggle' => 'tooltip'], 'title' => Yii::$app->formatter->asDatetime($v)]);
                        },
                    ],
                    'title:text:Назначение',
                    [
                        'header'    => 'Оплачен?',
                        'content'   => function ($item) {
                            $v = \yii\helpers\ArrayHelper::getValue($item, 'is_paid', 0);
                            if ($v == 0) return Html::tag('span', 'Нет', ['class' => 'label label-default']);

                            return Html::tag('span', 'Да', ['class' => 'label label-success']);
                        },
                        'attribute' => 'is_paid',
                    ],
                    [
                        'header'    => 'Адрес',
                        'content'   => function ($item) {
                            $v = \yii\helpers\ArrayHelper::getValue($item, 'address', '');
                            if ($v == '') return '';

                            return Html::tag('code', $v);
                        },
                        'attribute' => 'address',
                    ],
                    [
                        'header'    => 'txid',
                        'content'   => function ($item) {
                            $v = \yii\helpers\ArrayHelper::getValue($item, 'transaction_hash', '');
                            if ($v == '') return '';

                            return Html::tag('a', substr($v, 0, 8) . '...', [
                                'href'   => 'https://blockchain.info/tx/' . $v,
                                'target' => '_blank',
                                'style'  => 'font-family: Courier, "Courier new", monospace',
                            ]);
                        },
                        'attribute' => 'is_paid',
                    ],
                    [
                        'header'    => 'Подтверждений',
                        'content'   => function ($item) {
                            $confirmations = $item['confirmations'];
                            if ($confirmations == 0) {
                                $class = 'label label-danger';
                            } else
                                if ($confirmations < 6) {
                                    $class = 'label label-warning';
                                } else {
                                    $class = 'label label-success';
                                }
                            return
                                Html::tag(
                                    'span',
                                    Yii::$app->formatter->asDecimal($confirmations),
                                    ['class' => $class]
                                );
                        },
                        'attribute' => 'confirmations',
                    ],
                    [
                        'attribute' => 'price',
                        'format'    => ['decimal', 8],
                        'contentOptions' => [
                            'style' => 'text-align:right;'
                        ],
                        'headerOptions' => [
                            'style' => 'text-align:right;'
                        ],
                    ],
                ],
            ]) ?>
            <?php \yii\widgets\Pjax::end() ?>
        <?php } ?>
    </div>
</div>


