<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \avatar\models\forms\MerchantPassword */

$this->title = 'Инициализация мерчанта';
?>
<div class="container">

    <div class="site-contact">
        <h1 class="page-header text-center"><?= Html::encode($this->title) ?></h1>


        <div class="row">
            <div class="col-lg-4 col-lg-offset-4">
                <?php $form = ActiveForm::begin([
                    'id' => 'contact-form',
                ]); ?>
                <?= $form->field($model, 'password1')->passwordInput()->label('Пароль') ?>

                <hr>
                <div class="form-group">
                    <?= Html::submitButton('Подтвердить', [
                        'class' => 'btn btn-primary',
                        'name'  => 'contact-button',
                        'style' => 'width:100%;',
                    ]) ?>
                </div>
                <?php ActiveForm::end(); ?>
            </div>
        </div>

    </div>
</div>