<?php
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $merchant \avatar\models\forms\UserBillMerchant */

// Параметры id - идентификатор мерчанта, paysysytem_id - идентификатор платежной системы

$this->title = 'Мерчант для счета #' . $merchant->id;



?>

<div class="container" style="padding-bottom: 70px;">
    <div class="col-lg-12">
        <h1 class="page-header text-center">
            <?= $this->title ?>
        </h1>

    </div>
</div>


