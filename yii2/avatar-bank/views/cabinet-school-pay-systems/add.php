<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;
use app\models\UnionCategory;
use yii\db\Query;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \common\models\PaySystemConfig */

$this->title = 'Добавить платежную систему';

?>
<div class="container">

    <?php \avatar\widgets\SchoolMenu::begin(['school' => $school]) ?>
    <h1 class="page-header"><?= Html::encode($this->title) ?></h1>

    <?php if (Yii::$app->session->hasFlash('form')) : ?>

        <div class="alert alert-success">
            Успешно добавлено.
        </div>

        <p>
            <a href="<?= \yii\helpers\Url::to(['cabinet-school-pay-systems/index', 'id' => $school->id]) ?>" class="btn btn-success">Все настройки</a>
            <a href="<?= \yii\helpers\Url::to(['cabinet-school-pay-systems/settings', 'id' => Yii::$app->session->getFlash('form')]) ?>" class="btn btn-success">Настроить Платежную систему</a>
        </p>

    <?php else: ?>


        <div class="row">
            <div class="col-lg-4 col-lg-offset-4">
                <?php $form = ActiveForm::begin([
                    'id' => 'contact-form',
                    'options' => ['enctype' => 'multipart/form-data'],
                ]); ?>
                <?= $form->field($model, 'name')->label('Название настройки') ?>
                <?= $form->field($model, 'paysystem_id')->label('Платежная система')->dropDownList(
                    ArrayHelper::merge(
                        ['- Ничего не выбрано -'],
                        ArrayHelper::map(
                            \common\models\PaySystem::find()->all(),
                            'id',
                            function (\common\models\PaySystem $item) {
                                return $item->currency . ' / ' . $item->title;
                            }
                        )
                    )
                ) ?>

                <hr>
                <div class="form-group">
                    <?= Html::submitButton('Добавить', [
                        'class' => 'btn btn-default',
                        'name'  => 'contact-button',
                        'style' => 'width:100%',
                    ]) ?>
                </div>
                <?php ActiveForm::end(); ?>
            </div>
        </div>

    <?php endif; ?>
    <?php \avatar\widgets\SchoolMenu::end() ?>
</div>
