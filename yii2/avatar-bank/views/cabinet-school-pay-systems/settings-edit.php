<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;
use app\models\UnionCategory;
use yii\db\Query;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $view string '@avatar/modules/PaySystems/views/rub-modul-bank' */
/* @var $config \common\models\PaySystemConfig */
/* @var $paySystem \common\models\PaySystem */
/* @var $school \common\models\school\School */
/* @var $model  \avatar\modules\PaySystems\items\RubModulBank\model */

$this->title = $school->name . ' / ' . $paySystem->currency . ' / ' . $paySystem->title;

?>
<div class="container">

    <?php \avatar\widgets\SchoolMenu::begin(['school' => $school]) ?>
    <h1 class="page-header"><?= Html::encode($this->title) ?></h1>

    <?php if (Yii::$app->session->hasFlash('form')) : ?>

        <div class="alert alert-success">
            Успешно добавлено.
        </div>

        <p>
            <a href="<?= \yii\helpers\Url::to(['cabinet-school-pay-systems/index', 'id' => $school->id]) ?>"
               class="btn btn-success">Все настройки</a>
            <a href="<?= \yii\helpers\Url::to(['cabinet-school-pay-systems/settings', 'id' => $config->id]) ?>"
               class="btn btn-success">Настроить Платежную систему</a>
        </p>

    <?php else: ?>

        <div class="row">
            <div class="col-lg-4 col-lg-offset-4">
                <?php $form = ActiveForm::begin([
                    'id'      => 'contact-form',
                    'options' => ['enctype' => 'multipart/form-data'],
                ]); ?>

                <?= $this->render($view, [
                    'form'  => $form,
                    'model' => $model,
                ]); ?>

                <hr>
                <div class="form-group">
                    <?= Html::submitButton('Обновить', [
                        'class' => 'btn btn-default',
                        'name'  => 'contact-button',
                        'style' => 'width:100%',
                    ]) ?>
                </div>
                <?php ActiveForm::end(); ?>
            </div>
        </div>

    <?php endif; ?>

    <?php \avatar\widgets\SchoolMenu::end() ?>
</div>
