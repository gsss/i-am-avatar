<?php

/** @var $this \yii\web\View */


use yii\helpers\Url;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use kartik\grid\GridView;

$this->title = 'Все задачи';

$schoolList = \common\models\school\CommandLink::find()
    ->where([
        'user_id' => Yii::$app->user->id,
    ])
    ->select(['school_id'])
    ->column();

$id = Yii::$app->request->get('id');
$isTab = true;


/** @var $school \common\models\school\School */
$school = \common\models\school\School::findOne($id);


?>

<div class="container" style="padding-bottom: 10px;">
    <div class="col-lg-12">
        <h1 class="page-header"><?= Html::encode($this->title) ?></h1>
    </div>

    <div class="col-lg-12">
        <?php
        $rows = [];
        foreach ($schoolList as $id1) {
            $item = [
                'label' => \common\models\school\School::findOne($id1)->name,
                'url'   => '/cabinet-task-list/index?id=' . $id1,
            ];
            if ($id == $id1) {
                $item['active'] = true;
            }
            $rows[] = $item;
        }
        ?>




    </div>

</div>
<?= $this->render('index-list', [
    'school'     => $school,
    'schoolList' => $schoolList,
]) ?>

