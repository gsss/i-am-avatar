<?php
/**
 * Created by PhpStorm.
 * User: Ra-m-ha
 * Date: 02.11.2019
 * Time: 23:23
 */
use common\models\task\Session;

?>
<?php
$this->registerJs(<<<JS
var is_counter = 0;

function setTimer(i) {
    var mi = parseInt(i/60);
    var s = i - mi*60;
    
    var hi = parseInt(mi/60);
    var m = mi - hi*60;
    $('.timer_h').text(hi);

    var ss,ms,hs;
    
    if (s < 10) ss = '0' + s;
    else ss =  s;
    if (m < 10) ms = '0' + m;
    else ms =  m;

    $('.timer_s').text(ss);
    $('.timer_m').text(ms);

}

$('.buttonSessionStart').click(function(e) {
    ajaxJson({
        url: '/cabinet-task-list/session-start',
        data: {
            id: {$model->id}
        },
        success: function(ret) {
            is_counter = 1;
            $('.buttonSessionStart').attr('disabled', 'disabled');
            $('.buttonSessionFinish').removeAttr('disabled');
        }
    });
});
$('.buttonSessionFinish').click(function(e) {
    ajaxJson({
        url: '/cabinet-task-list/session-finish',
        data: {
            id: {$model->id}
        },
        success: function(ret) {
            is_counter = 0;
            $('.buttonSessionStart').removeAttr('disabled');
            $('.buttonSessionFinish').attr('disabled', 'disabled');
        }
    });
});
JS
);
?>
<?php
if ( $session = Session::find()->where(['task_id' => $model->id])->exists()) {
    /** @var Session $session */
    $session = Session::find()
        ->where(['task_id' => $model->id, 'is_finish' => 0])
        ->one();
    // если не найдены не закрытые сессии, то все закрыты и счетчик остановлен
    if (is_null($session)) {
        $all = Session::find()
            ->where([
                'task_id' => $model->id,
            ])
            ->select([
                'sum(finish - start) as ss',
            ])
            ->scalar();
        if ($all === false) $all = 0;
    } else {
        $all = Session::find()
            ->where(['task_id' => $model->id, 'is_finish' => 1])
            ->select([
                'sum(finish - start) as ss',
            ])
            ->scalar();

        $all += time() - $session->start;
        $this->registerJs(<<<JS
is_counter = 1;
$('.buttonSessionStart').attr('disabled', 'disabled');
$('.buttonSessionFinish').removeAttr('disabled');
JS
        );
    }
} else {
    $all = 0;
}

$this->registerJs(<<<JS
setTimer({$all});
var timer = {$all};
setInterval(function(e) {
    if (is_counter) {
        timer++;
        setTimer(timer);
    }
}, 1000);
JS
);

?>
<h1>
    <span class="timer_h"></span>:<span class="timer_m"></span>:<span class="timer_s"></span>
    <button class="btn btn-default buttonSessionStart">Старт</button>
    <button class="btn btn-default buttonSessionFinish" disabled="disabled">Стоп</button>
</h1>

<hr>
