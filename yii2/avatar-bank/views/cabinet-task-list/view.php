<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;
use app\models\UnionCategory;
use yii\db\Query;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \common\models\task\Task */
/* @var $school \common\models\school\School */

$this->title = $model->name;
$statusList = \common\models\task\Status::find()
    ->where(['school_id' => $school->id])
    ->orderBy(['sort_index' => SORT_ASC])
    ->select('id')
    ->column();

try {
    $Executer = $model->getExecuter();
} catch (Exception $e) {
    $Executer = null;
}

// Награда
if ($model->price > 0 && $model->getCurrency()) {
    $p = \iAvatar777\services\Processing\Currency::getValueFromAtom($model->price, $model->currency_id);
    $c = \iAvatar777\services\Processing\Currency::findOne($model->currency_id);
    $priceHtml = Yii::$app->formatter->asDecimal($p, $c->decimals) . ' ' . Html::tag('span', $model->getCurrency()->code, ['class' => 'label label-info']);
} else {
    $priceHtml = '';
}

?>
<div class="container">
    <div class="col-lg-12">
        <h1 class="page-header"><?= Html::encode($this->title) ?></h1>
    </div>

    <div class="col-lg-12">

        <p>

            <?php
            \avatar\assets\Notify::register($this);
            \common\assets\Clipboard\Asset::register($this);

            $this->registerJs(<<<JS

var clipboard = new Clipboard('.buttonCopy');
clipboard.on('success', function(e) {
    new Noty({
        timeout: 1000,
        theme: 'sunset',
        type: 'success',
        layout: 'bottomLeft',
        text: 'Скопировано'
    }).show();
});
JS
            );

            ?>
            <button class="btn btn-default buttonCopy" data-toggle="tooltip" data-clipboard-text="<?= \yii\helpers\Url::to(['cabinet-task-list/view', 'id' => $model->id], true) ?>" title="Нажми чтобы скопировать ссылку на эту задачу"><i class="glyphicon glyphicon-link"></i> Копировать ссылку</button>
        </p>
        <?php $c = \common\models\task\Category::findOne($model->category_id); ?>
        <?= \yii\widgets\DetailView::widget([
            'model'      => $model,
            'attributes' => [
                'id:text:ID',
                [
                    'label'          => 'Заголовок',
                    'captionOptions' => [
                        'style' => 'width: 30%'
                    ],
                    'value'          => $model->name,
                ],
                'created_at:datetime:Создано',
                'updated_at:datetime:Изменено',
                [
                    'label'  => 'Автор',
                    'format' => 'html',
                    'value'  => Html::img(
                        $model->getUser()->getAvatar(),
                        [
                            'class'       => 'img-circle',
                            'width'       => 30,
                            'data-toggle' => 'tooltip',
                            'title'       => \yii\helpers\Html::encode($model->getUser()->getName2())
                        ]
                    ),
                ],
                [
                    'label'  => 'Исполнитель',
                    'format' => 'html',
                    'value'  => (is_null($Executer)) ? '' : Html::img($model->getExecuter()->getAvatar(), ['class' => 'img-circle', 'width' => 30, 'data-toggle' => 'tooltip', 'title' => \yii\helpers\Html::encode($model->getExecuter()->getName2())]),
                ],
                [
                    'label'  => 'Награда',
                    'format' => 'html',
                    'value'  => $priceHtml,
                ],
                [
                    'label' => 'Категория',
                    'value' => (is_null($c)) ? '' : $c->name,
                ],
                [
                    'label'  => 'Содержание',
                    'format' => 'html',
                    'value'  => nl2br($model->content),
                ],
            ],
        ]) ?>
        <p>
            <?php if ($model->status == 0) { ?>
                <?php
                $this->registerJs(<<<JS
$('.buttonStart').click(function(e) {
    ajaxJson({
        url: '/cabinet-task-list/view-start',
        data: {
            id: $(this).data('id')
        },
        success: function(ret) {
            window.location.reload();
        }
    })
});
JS
                );
                ?>
                <button class="btn btn-default buttonStart" data-id="<?= $model->id ?>">Я сделаю это!</button>
            <?php } ?>

            <?php if ($model->status == $statusList[0]) { ?>
                <?php if ($model->executer_id == Yii::$app->user->id) { ?>
                    <?php
                    $this->registerJs(<<<JS
$('.buttonFinish').on('click', function(e) {
    ajaxJson({
        url: '/cabinet-task-list/view-finish',
        data: {
            id: $(this).data('id')
        },
        success: function(ret) {
            window.location.reload();
        }
    })
});
JS
                    );
                    $gender = Yii::$app->user->identity->gender;
                    $do = 'Я сделал(а)!';
                    switch ($gender) {
                        case \common\models\UserAvatar::GENDER_MALE:
                            $do = 'Я сделал!';
                            break;
                        case \common\models\UserAvatar::GENDER_FEMALE:
                            $do = 'Я сделала!';
                            break;
                    }
                    ?>
                    <button class="btn btn-success buttonFinish" data-id="<?= $model->id ?>"><i
                                class="glyphicon glyphicon-ok"></i> <?= $do ?>
                    </button>
                <?php } ?>
            <?php } ?>

            <?php if ($model->status == $statusList[1]) { ?>
                <?php if (Yii::$app->user->can('permission_project-manager')) { ?>
                    <?php
                    $this->registerJs(<<<JS
$('.buttonDone').on('click', function(e) {
    console.log(1);
    ajaxJson({
        url: '/cabinet-task-list/view-done',
        data: {
            id: $(this).data('id')
        },
        success: function(ret) {
            window.location.reload();
        }
    })
});
JS
                    );
                    ?>
                    <button class="btn btn-primary buttonDone" data-id="<?= $model->id ?>"><i
                                class="glyphicon glyphicon-ok"></i> Принять!
                    </button>
                <?php } ?>
            <?php } ?>
        </p>
        <hr>
        <?php if (!is_null($model->status) and !in_array($model->status, [$statusList[1], $statusList[2]])) { ?>
            <?= $this->render('view-counter', ['model' => $model]) ?>
        <?php } ?>
        <?= \avatar\modules\Comment\Module::getComments2(1, $model->id); ?>

    </div>
</div>
