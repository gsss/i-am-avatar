<?php
use common\widgets\FileUpload3\FileUpload;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $item \common\models\NewsItem */

$this->title = $item->name;

?>
<style>
    .contentBlock p, .contentBlock li {
        font-size: 130%;
    }
</style>
<div class="container" style="padding-bottom: 70px;">
    <div class="col-lg-12">
        <h1 class="page-header text-center"><?= $this->title ?></h1>
        <p style="color: #888;" class=" text-center"><?= Yii::$app->formatter->asDate($item->created_at, 'php:d.m.Y') ?></p>
    </div>
    <div class="col-lg-8 col-lg-offset-2 contentBlock">

        <p>
            <img src="<?= FileUpload::getOriginal($item->image) ?>" width="100%" class="thumbnail"/>
        </p>

        <?= $item->content ?>
        <hr>
        <?= $this->render('../blocks/fbLike') ?>

        <?= $this->render('../blocks/share', [
            'image'       => Url::to(FileUpload::getOriginal($item->image), true),
            'title'       => $this->title,
            'url'         => Url::current([], true),
            'description' => $item->description,

        ]) ?>
    </div>
</div>


