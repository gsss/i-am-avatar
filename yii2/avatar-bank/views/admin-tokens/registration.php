<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;
use app\models\UnionCategory;
use yii\db\Query;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \avatar\models\forms\TokenRegistration */

$this->title = 'Регистрация токена';

?>
<div class="container">
    <div class="col-lg-12">
        <h1 class="page-header"><?= Html::encode($this->title) ?></h1>

        <?php if (!is_null($id = Yii::$app->session->getFlash('form'))) : ?>

            <div class="alert alert-success">
                Успешно добавлено.
            </div>

        <?php else: ?>

            <div class="row">
                <div class="col-lg-6">
                    <?php $form = ActiveForm::begin([
                        'options' => ['enctype' => 'multipart/form-data']
                    ]); ?>
                    <?= $form->field($model, 'abi')->textarea(['rows' => 8]) ?>
                    <?= $form->field($model, 'address') ?>
                    <?= $form->field($model, 'title') ?>
                    <?= $form->field($model, 'code') ?>
                    <?= $form->field($model, 'decimals') ?>
                    <?= $form->field($model, 'image')->widget('\common\widgets\FileUpload31\FileUpload', [
                        'tableName' => 'currency',
                        'attribute' => 'image',
                    ]) ?>


                    <hr>
                    <div class="form-group">
                        <?= Html::submitButton('Зарегистрировать', [
                            'class' => 'btn btn-default',
                            'name'  => 'contact-button',
                            'style' => 'width:100%',
                        ]) ?>
                    </div>
                    <?php ActiveForm::end(); ?>
                </div>
            </div>

        <?php endif; ?>
    </div>
</div>
