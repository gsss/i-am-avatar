<?php

/** @var $this \yii\web\View */
/** @var $school \common\models\school\School */


use yii\helpers\Url;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use kartik\grid\GridView;

$this->title = 'СНТ';

function sec2time($diff)
{
    $sec = $diff % 60;
    $diffOstatok = $diff - $sec;

    if ($diffOstatok > 0) {
        $min = ($diffOstatok % (60*60)) / 60;
        $diffOstatok = $diffOstatok - $min * 60;
        if ($diffOstatok > 0) {
            $hour = ($diffOstatok % (60*60*24)) / (60*60);
            $diffOstatok = $diffOstatok - $hour * 60*60;
            if ($diffOstatok > 0) {
                $day = $diffOstatok;
            } else {
                $day = 0;
            }
        } else {
            $hour = 0;
            $day = 0;
        }
    } else {
        $min = 0;
        $hour = 0;
        $day = 0;
    }

    if ($day < 10) $day = '0' . $day;
    if ($hour < 10) $hour = '0' . $hour;
    if ($min < 10) $min = '0' . $min;
    if ($sec < 10) $sec = '0' . $sec;

    return $day . ':' . $hour . ':' . $min . ':' . $sec;
}
?>

<div class="container" style="padding-bottom: 10px;">
    <div class="col-lg-12">
        <h1 class="page-header"><?= Html::encode($this->title) ?></h1>
    </div>

    <div class="col-lg-12">



        <h2 class="page-header">Растраты</h2>
        <?php \yii\widgets\Pjax::begin(); ?>
        <?= \yii\grid\GridView::widget([
            'dataProvider' => new \yii\data\ActiveDataProvider([
                'query'      => \common\models\SchoolSntOut::find()
                ,
                'pagination' => [
                    'pageSize' => 20,
                ],
                'sort' => ['defaultOrder' => ['id' => SORT_DESC]]
            ]),
            'tableOptions' => [
                'class' => 'table table-striped table-hover',
            ],
            'rowOptions'   => function ($item) {
                return [
                    'data'  => ['id' => $item['id']],
                    'role'  => 'button',
                    'class' => 'rowTable',
                ];
            },
            'columns' => [
                'id',
                'name',
                'date:date:Дата',
                [
                    'header' => 'Сумма',
                    'headerOptions' => ['class' => 'text-right'],
                    'contentOptions' => ['class' => 'text-right'],
                    'content' => function($i) {
                        return Yii::$app->formatter->asDecimal($i['amount'] / 100, 2);
                    }
                ],
                [
                    'header' => 'Кто',
                    'content' => function($i) {
                        try {
                            $u = \common\models\UserAvatar::findOne($i['user_id']);
                            return $u->getName2();
                        } catch (Exception $e) {
                            return '';
                        }
                    }
                ]
            ]
        ]) ?>
        <?php \yii\widgets\Pjax::end(); ?>

        <h2 class="page-header">Походы</h2>
        <?php \yii\widgets\Pjax::begin(); ?>
        <?php
        $this->registerJS(<<<JS

$('.rowTable2').click(function(e) {
    window.location = '/cabinet-snt/pohod' + '?' + 'id' + '=' + $(this).data('id');
});

JS
        );
        ?>
        <?= \yii\grid\GridView::widget([
            'dataProvider' => new \yii\data\ActiveDataProvider([
                'query'      => \common\models\SchoolSntPohod::find()
                ,
                'pagination' => [
                    'pageSize' => 20,
                ],
                'sort' => ['defaultOrder' => ['id' => SORT_DESC]]
            ]),
            'tableOptions' => [
                'class' => 'table table-striped table-hover',
            ],
            'rowOptions'   => function ($item) {
                return [
                    'data'  => ['id' => $item['id']],
                    'role'  => 'button',
                    'class' => 'rowTable2',
                ];
            },
            'columns' => [
                'id',
                'date:date:Дата',
                'description',
            ]
        ]) ?>
        <?php \yii\widgets\Pjax::end(); ?>

        <h2 class="page-header">Труд</h2>
        <?php \yii\widgets\Pjax::begin(); ?>
        <?php
        $this->registerJS(<<<JS

$('.rowTable3').click(function(e) {
});

JS
        );
        ?>
        <?= \yii\grid\GridView::widget([
            'dataProvider' => new \yii\data\ActiveDataProvider([
                'query'      => \common\models\SchoolSntPohodUser::find()
                ,
                'pagination' => [
                    'pageSize' => 20,
                ],
                'sort' => ['defaultOrder' => ['id' => SORT_DESC]]
            ]),
            'tableOptions' => [
                'class' => 'table table-striped table-hover',
            ],
            'rowOptions'   => function ($item) {
                return [
                    'data'  => ['id' => $item['id']],
                    'role'  => 'button',
                    'class' => 'rowTable3',
                ];
            },
            'columns' => [
                'id',
                'tarif',
                'start:datetime:Старт',
                'end:datetime:Окончание',
                [
                    'header' => 'Сколько, ч',
                    'content' => function($i) {
                        $t = $i['end'] - $i['start'];
                        return sec2time($t);
                    }
                ],
                [
                    'header' => 'Сумма',
                    'headerOptions' => ['class' => 'text-right'],
                    'contentOptions' => ['class' => 'text-right'],
                    'content' => function($i) {
                        $t = $i['end'] - $i['start'];

                        return Yii::$app->formatter->asDecimal(($t/(60*60)) * $i['tarif'], 2);
                    }
                ],
                [
                    'header' => 'Кто',
                    'content' => function($i) {
                        try {
                            $u = \common\models\UserAvatar::findOne($i['user_id']);
                            return $u->getName2();
                        } catch (Exception $e) {
                            return '';
                        }
                    }
                ],
                [
                    'header' => 'pohod_id',
                    'content' => function($i) {
                        if (is_null($i['pohod_id'])) return '';
                        $u = \common\models\SchoolSntPohod::findOne($i['pohod_id']);

                        return $u->date;
                    }
                ]
            ]
        ]) ?>
        <?php \yii\widgets\Pjax::end(); ?>

    </div>



</div>


