<?php

/** @var $this \yii\web\View */
/** @var $model \avatar\models\forms\SchoolSntPohodFile */
/** @var $school \common\models\school\School */
/** @var $pohod \common\models\SchoolSntPohod */


use yii\helpers\Url;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use kartik\grid\GridView;

$this->title = 'СНТ файл похода добавить';




?>

<div class="container" style="padding-bottom: 10px;">
    <div class="col-lg-12">
        <h1 class="page-header"><?= Html::encode($this->title) ?></h1>
    </div>

    <div class="col-lg-12">

        <?php $form = \iAvatar777\services\FormAjax\ActiveForm::begin([
            'model'   => $model,
            'success' => <<<JS
function (ret) {
    window.location = '/cabinet-snt/pohod?id=' + {$pohod->id}
}
JS
            ,
        ]) ?>
        <?= $form->field($model, 'file') ?>
        <hr>
        <?php $form = \iAvatar777\services\FormAjax\ActiveForm::end() ?>
    </div>



</div>


