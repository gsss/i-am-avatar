<?php

/** @var $this \yii\web\View */
/** @var $model \avatar\models\forms\SchoolSntPohodUser */
/** @var $school \common\models\school\School */


use yii\helpers\Url;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use kartik\grid\GridView;

$this->title = 'СНТ Походы';




?>

<div class="container" style="padding-bottom: 10px;">
    <div class="col-lg-12">
        <h1 class="page-header"><?= Html::encode($this->title) ?></h1>
    </div>

    <div class="col-lg-12">

        <?php $form = \iAvatar777\services\FormAjax\ActiveForm::begin([
            'model'   => $model,
            'success' => <<<JS
function (ret) {
    window.location = '/cabinet-snt/index?id=' + {$school->id}
}
JS
,
        ]) ?>
        <?= $form->field($model, 'start')->widget('\kartik\datetime\DateTimePicker') ?>
        <?= $form->field($model, 'end')->widget('\kartik\datetime\DateTimePicker') ?>
        <?= $form->field($model, 'tarif') ?>

        <?= $form->field($model, 'user_id')->dropDownList([
            9    => 'Святослав',
            4620 => 'Алексей',
            4621 => 'Сергей Паленов',
            4622 => 'Алексей Барановский (солохаул)',
            4623 => 'Ефимов Сергей',
            4624 => 'Виктор Жеребцов',
            4625 => 'Евгений Андрющенко',
            4626 => 'Саша',
            4627 => 'Костя',
            4628 => 'Дмитрий',
            4629 => 'Скоробогатов',
        ], ['prompt' => '- Ничего не выбрано -']) ?>

        <?= $form->field($model, 'pohod_id')->dropDownList(ArrayHelper::map(
                \common\models\SchoolSntPohod::find()->all(),
                'id',
                'date'
        ), ['prompt' => '- Ничего не выбрано -']) ?>
        <hr>
        <?php $form = \iAvatar777\services\FormAjax\ActiveForm::end() ?>
    </div>



</div>


