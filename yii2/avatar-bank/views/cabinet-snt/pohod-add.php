<?php

/** @var $this \yii\web\View */
/** @var $model \avatar\models\forms\SchoolSntPohod */
/** @var $school \common\models\school\School */


use yii\helpers\Url;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use kartik\grid\GridView;

$this->title = 'СНТ Походы';




?>

<div class="container" style="padding-bottom: 10px;">
    <div class="col-lg-12">
        <h1 class="page-header"><?= Html::encode($this->title) ?></h1>
    </div>

    <div class="col-lg-12">

        <?php $form = \iAvatar777\services\FormAjax\ActiveForm::begin([
            'model'   => $model,
            'success' => <<<JS
function (ret) {
    window.location = '/cabinet-snt/index?id=' + {$school->id}
}
JS
,
        ]) ?>
        <?= $form->field($model, 'date') ?>
        <?= $form->field($model, 'description') ?>
        <hr>
        <?php $form = \iAvatar777\services\FormAjax\ActiveForm::end() ?>
    </div>



</div>


