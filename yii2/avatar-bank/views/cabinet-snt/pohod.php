<?php

/** @var $this \yii\web\View */
/** @var $model \common\models\SchoolSntPohod */


use yii\helpers\Url;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use kartik\grid\GridView;

$this->title = $model->date;


function sec2time($diff)
{
    $sec = $diff % 60;
    $diffOstatok = $diff - $sec;

    if ($diffOstatok > 0) {
        $min = ($diffOstatok % (60*60)) / 60;
        $diffOstatok = $diffOstatok - $min * 60;
        if ($diffOstatok > 0) {
            $hour = ($diffOstatok % (60*60*24)) / (60*60);
            $diffOstatok = $diffOstatok - $hour * 60*60;
            if ($diffOstatok > 0) {
                $day = $diffOstatok;
            } else {
                $day = 0;
            }
        } else {
            $hour = 0;
            $day = 0;
        }
    } else {
        $min = 0;
        $hour = 0;
        $day = 0;
    }

    if ($day < 10) $day = '0' . $day;
    if ($hour < 10) $hour = '0' . $hour;
    if ($min < 10) $min = '0' . $min;
    if ($sec < 10) $sec = '0' . $sec;

    return $day . ':' . $hour . ':' . $min . ':' . $sec;
}

?>

<div class="container" style="padding-bottom: 10px;">
    <div class="col-lg-12">
        <h1 class="page-header"><?= Html::encode($this->title) ?></h1>
    </div>

    <div class="col-lg-12">



        <?= \yii\widgets\DetailView::widget([
                'model' => $model
        ]) ?>

        <h2 class="page-header">Файлы</h2>
        <?php \yii\widgets\Pjax::begin(); ?>

        <?= \yii\grid\GridView::widget([
            'dataProvider' => new \yii\data\ActiveDataProvider([
                'query'      => \common\models\SchoolSntPohodFile::find()
                    ->where(['pohod_id' => $model->id])
                ,
                'pagination' => [
                    'pageSize' => 20,
                ],
            ]),
            'tableOptions' => [
                'class' => 'table table-striped table-hover',
            ],
            'rowOptions'   => function ($item) {
                return [
                    'data'  => ['id' => $item['id']],
                    'role'  => 'button',
                    'class' => 'rowTable2',
                ];
            },
            'columns' => [
                'id',
                'created_at:datetime:Дата',
                [
                   'header' => 'Файл',
                   'content' => function($i) {
                        return Html::a($i['file'], $i['file'], ['target' => '_blank']);
                   }
                ]
            ]
        ]) ?>
        <?php \yii\widgets\Pjax::end(); ?>



        <h2 class="page-header">Труд</h2>
        <?php \yii\widgets\Pjax::begin(); ?>
        <?php
        $this->registerJS(<<<JS

$('.rowTable3').click(function(e) {
});

JS
        );
        ?>
        <?= \yii\grid\GridView::widget([
            'dataProvider' => new \yii\data\ActiveDataProvider([
                'query'      => \common\models\SchoolSntPohodUser::find()
                    ->where(['pohod_id' => $model->id])
                ,
                'pagination' => [
                    'pageSize' => 20,
                ],
            ]),
            'tableOptions' => [
                'class' => 'table table-striped table-hover',
            ],
            'rowOptions'   => function ($item) {
                return [
                    'data'  => ['id' => $item['id']],
                    'role'  => 'button',
                    'class' => 'rowTable3',
                ];
            },
            'columns' => [
                'id',
                'tarif',
                'start:datetime:Старт',
                'end:datetime:Окончание',
                [
                    'header' => 'Сколько, сек',
                    'content' => function($i) {
                        $t = $i['end'] - $i['start'];
                        return sec2time($t);
                    }
                ],
                [
                    'header' => 'Сумма',
                    'headerOptions' => ['class' => 'text-right'],
                    'contentOptions' => ['class' => 'text-right'],
                    'content' => function($i) {
                        $t = $i['end'] - $i['start'];

                        return Yii::$app->formatter->asDecimal(($t/(60*60)) * $i['tarif'], 2);
                    }
                ],
                [
                    'header' => 'Кто',
                    'content' => function($i) {
                        try {
                            $u = \common\models\UserAvatar::findOne($i['user_id']);
                            return $u->getName2();
                        } catch (Exception $e) {
                            return '';
                        }
                    }
                ],
            ]
        ]) ?>
        <?php \yii\widgets\Pjax::end(); ?>

    </div>



</div>


