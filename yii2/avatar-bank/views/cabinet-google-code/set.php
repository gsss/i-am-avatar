<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;
use Endroid\QrCode\ErrorCorrectionLevel;
use Endroid\QrCode\LabelAlignment;
use Endroid\QrCode\QrCode;
use Endroid\QrCode\Response\QrCodeResponse;

/* @var $this yii\web\View */
/* @var $model \avatar\models\forms\GoogleAuth */

$this->title = Yii::t('c.DLo6pHtmzQ', 'Двухфакторная авторизация (2FA)');

/** @var $user \common\models\UserAvatar */
$user = Yii::$app->user->identity;


Yii::setAlias('@Endroid/QrCode', '@vendor/endroid/QrCode/src');
?>
<div class="container">
    <div class="col-lg-12">
        <h1 class="page-header"><?= Html::encode($this->title) ?></h1>

    </div>
    <div class="col-lg-8">

        <?php if ($user->google_auth_code) { ?>
            <p class="alert alert-success"><?= \Yii::t('c.DLo6pHtmzQ', 'Код установлен') ?></p>
            <?php
            $this->registerJs(<<<JS
$('.buttonReset').click(function(e) {
    ajaxJson({
        url: '/cabinet-google-code/reset',
        success: function(ret) {
            window.location.reload();
        }
    });
});
JS
            );
            ?>
            <p><button  class="btn btn-danger buttonReset"><?= \Yii::t('c.DLo6pHtmzQ', 'Сбросить') ?></button></p>
        <?php } else { ?>
            <p class="text-center"><img src="/images/controller/auth/google-confirm/ga2.png" width="150"></p>
            <p>
                <?php
                $secret = Yii::$app->session->get('googleKey');
                if (is_null($secret)) {
                    $file = Yii::getAlias('@vendor/chregu/GoogleAuthenticator.php/lib/GoogleAuthenticator.php');
                    require_once($file);

                    $ga = new \GoogleAuthenticator;
                    $secret = $ga->generateSecret();
                    Yii::$app->session->set('googleKey', $secret);
                }

                $url = sprintf("otpauth://totp/%s?secret=%s", $user->email . '@' . Yii::$app->params['google-authorisation-code']['server'], $secret);

               /// -----------------

               /// -----------------
                $qr = new \Endroid\QrCode\QrCode();
                $qr->setText($url);
                $qr->setSize(180);
                $qr->setMargin(20);
                $qr->setErrorCorrectionLevel(ErrorCorrectionLevel::HIGH());
                $qr->setForegroundColor(['r' => 0, 'g' => 0, 'b' => 0, 'a' => 0]);
                $qr->setBackgroundColor(['r' => 255, 'g' => 255, 'b' => 255, 'a' => 0]);
                $qr->setLabelFontSize(16);
                $content = $qr->writeString();
                $src = 'data:image/png;base64,' . base64_encode($content);

                ?>
            <p class="text-center"><img src="<?= $src ?>"></p>
            <div class="row">
                <div class="col-lg-6">
                    <p class="text-center"><a href="https://play.google.com/store/apps/details?id=com.google.android.apps.authenticator2" target="_blank"><img src="/images/mobile/android.gif"></a></p>
                </div>
                <div class="col-lg-6">
                    <p class="text-center"><a href="https://itunes.apple.com/ru/app/google-authenticator/id388497605?mt=8" target="_blank"><img src="/images/mobile/ios.gif"></a></p>
                </div>
                <div class="col-lg-12">
                    <p class="text-center"><?= \Yii::t('c.DLo6pHtmzQ', 'Если у вас нет телефона то пройдите по') ?> <a href="https://www.labnol.org/internet/google-authenticator-for-desktop/25341/"><?= \Yii::t('c.DLo6pHtmzQ', 'ссылке') ?></a></p>
                </div>
            </div>
            <p class="alert alert-success text-center"><?= \Yii::t('c.DLo6pHtmzQ', 'Сохраните код') ?>:<br><code style="font-size: 200%;"><?= $secret ?></code></p>
            <p class="text-center">
                <a
                        href="<?= Url::to(['cabinet-google-code/download', 'code' => $secret, 'email' => $user->email]) ?>"
                        class="btn btn-success"
                        style="width: 100%; max-width: 200px;text-align: center;"
                ><i class="glyphicon glyphicon-download"></i> <?= \Yii::t('c.DLo6pHtmzQ', 'Скачать PDF') ?></a>
            </p>

            <table align="center" width="200" border="0">
                <tr>
                    <td>
                        <?php $form = ActiveForm::begin([
                            'options' => ['style' => 'width: 100%; max-width: 200px;']
                        ]) ?>
                        <?= $form->field($model, 'pin', ['inputOptions' => ['placeholder' => '000000']])->label(Yii::t('c.DLo6pHtmzQ', 'Код с телефона')) ?>
                        <?= Html::hiddenInput(Html::getInputName($model, 'code'), $secret) ?>
                        <?= Html::submitButton(Yii::t('c.DLo6pHtmzQ', 'Включить'), ['class' => 'btn btn-success', 'style' => 'width:100%']) ?>
                        <?php ActiveForm::end() ?>
                    </td>
                </tr>
            </table>

        <?php } ?>
    </div>
    <div class="col-lg-4">
        <?= $this->render('../cabinet/_menu') ?>
    </div>
</div>