<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;
use app\models\UnionCategory;
use \Endroid\QrCode\ErrorCorrectionLevel;

/* @var $this yii\web\View */
/* @var $code string */
/* @var $email string */


$url = sprintf("otpauth://totp/%s?secret=%s", $email . '@' . Yii::$app->params['google-authorisation-code']['server'], $code);

$qr = new \Endroid\QrCode\QrCode();
$qr->setText($url);
$qr->setSize(180);
$qr->setMargin(20);
$qr->setErrorCorrectionLevel(ErrorCorrectionLevel::HIGH());
$qr->setForegroundColor(['r' => 0, 'g' => 0, 'b' => 0, 'a' => 0]);
$qr->setBackgroundColor(['r' => 255, 'g' => 255, 'b' => 255, 'a' => 0]);
$qr->setLabelFontSize(16);
$content = $qr->writeString();
$src = 'data:image/png;base64,' . base64_encode($content);

?>
<html>
<head>
    <title>Документ</title>
    <style>
        td {
            font-size: 150%;
            font-family: "Courier New", Courier, monospace;
        }
    </style>
</head>
<body>
<table width="100%">
    <tr>
        <td width="33%">&nbsp;</td>
        <td width="33%" style="text-align: center;"><img src="/images/pdf/lovo-avr-04.png" width="300"></td>
        <td width="33%">&nbsp;</td>
    </tr>
</table>
<hr>
<h4 style="text-align: center;font-weight: normal;">
    <span style="font-size: 250%;">Google Authorization<br>
</h4>
<table class="table table-hover table-striped" align="center">
    <tr>
        <td style="text-align: right; vertical-align: top;"><b>secret</b></td>
        <td style='font-family: "Courier New", Courier, monospace'><?= $code ?></td>
    </tr>
    <tr>
        <td style="text-align: right;vertical-align: top;"><b>QR</b></td>
        <td><img src="<?= $src ?>"></td>
    </tr>
    <tr>
        <td style="text-align: right;"><b>Email</b></td>
        <td><?= $email ?></td>
    </tr>
    <tr>
        <td style="text-align: right;"><b>Аккаунт</b></td>
        <td><?= $email . '@' . Yii::$app->params['google-authorisation-code']['server'] ?></td>
    </tr>
    <tr>
        <td style="text-align: right;"><b>Time</b></td>
        <td><?= Yii::$app->formatter->asDatetime(time(),'php:c') ?></td>
    </tr>
</table>

</body>
</html>



