<?php
use yii\helpers\Html;
use yii\bootstrap\ButtonDropdown;
use yii\helpers\Url;
use cs\services\Url as csUrl;
use cs\services\Str;
use yii\widgets\Breadcrumbs;

/** @var $this yii\web\View */
/** @var $item \common\models\HdGenKeys */

$this->title = 'Генный ключ '. $item->num . '. Дизайн Человека';

function cc($v)
{
    return $v;
}
?>
<div class="container">
    <div class="col-lg-12">
        <h1 class="page-header"><?= \yii\helpers\Html::encode($this->title) ?></h1>
        <?= \cs\Widget\BreadCrumbs\BreadCrumbs::widget([
            'items' => [
                [
                    'label' => 'Дизайн Человека',
                    'url'   => ['human-design/index'],
                ],
                $this->title
            ],
        ]) ?>
        <hr>
    </div>
    <div class="col-lg-8 col-lg-offset-2">
        <?php if (
            $item->num &&
            $item->ten &&
            $item->dar &&
            $item->siddhi &&
            $item->name &&
            $item->content_ten &&
            $item->content_dar &&
            $item->content_siddhi &&
            $item->codon &&
            $item->amin &&
            $item->patern &&
            $item->fiz
        ) { ?>
            <table class="table table-hover table-striped" style="width: auto;">
                <tr>
                    <td>
                        Кодон
                    </td>
                    <td>
                        <?php
                        $img = 'http://www.galaxysss.ru' . $item->image_codon;
                        if ($img) {
                            echo Html::img($img, ['width' => 100, 'height' => 100, 'class' => 'thumbnail', 'margin-bottom: 0px;']);
                        } else {
                            echo '';
                        }
                        ?>
                    </td>
                </tr>
                <tr>
                    <td>
                        Граф
                    </td>
                    <td>
                        <?php
                        $img = 'http://www.galaxysss.ru'.$item->image_graph;
                        if ($img) {
                            echo Html::img($img, ['width' => 100, 'height' => 100, 'class' => 'thumbnail', 'margin-bottom: 0px;']);
                        } else {
                            echo '';
                        }
                        ?>
                    </td>
                </tr>
                <tr>
                    <td>
                        Название
                    </td>
                    <td>
                        <?= $item->name  ?>
                    </td>
                </tr>
                <tr>
                    <td>
                        Тень
                    </td>
                    <td>
                        <?= $item->ten  ?>
                    </td>
                </tr>
                <tr>
                    <td>
                        Дар
                    </td>
                    <td>
                        <?= $item->dar  ?>
                    </td>
                </tr>
                <tr>
                    <td>
                        Сиддхи
                    </td>
                    <td>
                        <?= $item->siddhi  ?>
                    </td>
                </tr>
                <tr>
                    <td>
                        Кодоновое кольцо
                    </td>
                    <td>
                        <?php
                        $text = $item->codon;
                        $arr = explode('(', $text);
                        $text = trim($arr[0]);
                        $arr = explode(')', $arr[1]);
                        $arr = explode(',', $arr[0]);
                        $t = [];
                        foreach($arr as $i) {
                            $i = trim($i);
                            $t[] = Html::a($i, ['human-design/gen-keys-item', 'id' => $i], ['target' => '_blank']);
                        }
                        ?>

                        <?= $text . ' (' . join(', ', $t) . ')'  ?>
                    </td>
                </tr>
                <tr>
                    <td>
                        Физиология
                    </td>
                    <td>
                        <?= $item->fiz  ?>
                    </td>
                </tr>
                <tr>
                    <td>
                        Аминокислота
                    </td>
                    <td>
                        <?= $item->amin  ?>
                    </td>
                </tr>
                <tr>
                    <td>
                        Программный паттерн
                    </td>
                    <td>
                        <a href="<?= Url::to(['human-design/gen-keys-item', 'id' => $item->patern])?>" target="_blank"><?= $item->patern  ?></a>
                    </td>
                </tr>
            </table>
            <h2 class="page-header">Тень</h2>
            <?= cc($item->content_ten)  ?>

            <h2 class="page-header">Дар</h2>
            <?= cc($item->content_dar)  ?>

            <h2 class="page-header">Сиддхи</h2>
            <?= cc($item->content_siddhi)  ?>

        <?php } else { ?>
            <?= cc($item->content)  ?>
        <?php } ?>

        <hr>
        <?= $this->render('../blocks/share', [
            'image'       => \yii\helpers\Url::to('/images/new_earth/chakri/1444980126_XlhMUkP7z3.jpg', true),
            'url'         => Url::current([], true),
            'title'       => $this->title,
            'description' => trim(Str::sub(strip_tags($item->content), 0, 200)),
        ]) ?>


    </div>

</div>