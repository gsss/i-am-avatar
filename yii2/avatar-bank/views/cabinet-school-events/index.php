<?php

/** $this \yii\web\View  */
/** @var $school \common\models\school\School */

use yii\helpers\Url;
use app\services\GsssHtml;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use app\services\GetArticle\Collection;

$this->title = 'Все события';

?>

<div class="container">

    <?php \avatar\widgets\SchoolMenu::begin(['school' => $school]) ?>
    <h1 class="page-header"><?= Html::encode($this->title) ?></h1>

    <p>
        <a href="<?= Url::to(['cabinet-school-events/add', 'id' => $school->id]) ?>"
           class="btn btn-default">Добавить</a>
    </p>

    <?php \yii\widgets\Pjax::begin(); ?>
    <?php
    $this->registerJS(<<<JS
$('[data-toggle="tooltip"]').tooltip();
$('.buttonDelete').click(function (e) {
    e.preventDefault();
    e.stopPropagation();
    if (confirm('Подтвердите удаление')) {
        var button = $(this);
        var id = $(this).data('id');
        ajaxJson({
            url: '/cabinet-school-events/delete' + '?' + 'id' + '=' + id,
            success: function (ret) {
                $('#modalInfo').on('hidden.bs.modal', function() {
                    button.parent().parent().remove();
                }).modal();
            }
        });
    }
});

$('.rowTable').click(function() {
    // window.location = '/cabinet-school-events/edit' + '?' + 'id' + '=' + $(this).data('id');
});
$('.buttonEdit').click(function() {
    window.location = '/cabinet-school-events/edit' + '?' + 'id' + '=' + $(this).data('id');
});

JS
    );
    ?>
    <?= \yii\grid\GridView::widget([
        'dataProvider' => new \yii\data\ActiveDataProvider([
            'query'      => \common\models\school\Potok::find()
                ->innerJoin('school_kurs', 'school_kurs.id = school_potok.kurs_id')
                ->select([
                    'school_potok.*',
                    'school_kurs.image',
                ])
                ->where(['school_kurs.school_id' => $school->id])
                ->asArray()
            ,
            'pagination' => [
                'pageSize' => 20,
            ],
        ]),
        'tableOptions' => [
            'class' => 'table table-striped table-hover',
        ],
        'rowOptions'   => function ($item) {
            $data = [
                'data'  => ['id' => $item['id']],
                'role'  => 'button',
                'class' => 'rowTable',
            ];
            return $data;
        },
        'columns'      => [
            [
                'attribute' => 'image',
                'header'    => 'Картинка',
                'content'   => function ($item) {
                    $i = ArrayHelper::getValue($item, 'image', '');
                    if ($i == '') return '';
                    $size = 40;

                    return Html::img(\common\widgets\FileUpload7\FileUpload::getFile($i, 'crop'), [
                        'class'  => "img-circle",
                        'width'  => $size,
                        'height' => $size,
                        'style'  => 'margin-bottom: 0px;',
                    ]);
                },
            ],
            [
                'header'    => 'Ссылка',
                'content'   => function ($item) {
                    $i = ArrayHelper::getValue($item, 'page_id', '');
                    if (is_null($i)) return '';
                    $page = \common\models\school\Page::findOne($i);
                    if (is_null($page)) return '';

                    return Html::a($page->url, $page->getUrl(), ['target' => '_blank']);
                },
            ],
            [
                'attribute' => 'name',
            ],
            [
                'attribute' => 'date_start',
                'format'    => 'date',
            ],
            [
                'attribute' => 'date_end',
                'format'    => 'date',
            ],
            [
                'header'  => '',
                'content' => function ($item) {
                    return Html::button('Редактировать', [
                        'class' => 'btn btn-primary btn-xs buttonEdit',
                        'data'  => [
                            'id' => $item['id'],
                        ],
                    ]);
                },
            ],
        ],
    ]) ?>
    <?php \yii\widgets\Pjax::end(); ?>

    <?php \avatar\widgets\SchoolMenu::end() ?>
</div>

<div class="modal fade" id="modalInfo" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="myModalLabel">Информация</h4>
            </div>
            <div class="modal-body">
                Успешно!
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
            </div>
        </div>
    </div>
</div>