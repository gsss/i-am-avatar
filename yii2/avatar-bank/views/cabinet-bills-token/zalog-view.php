<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;
use yii\db\Query;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $billing    \common\models\avatar\UserBill */
/* @var $zalog      \common\models\UserZalog */

$this->title = 'Просмотр залога #' . $zalog->id;

$partner = \common\models\UserAvatar::findOne($zalog->partner_user_id);

?>
<div class="container">
    <div class="col-lg-12">

        <h1 class="page-header"><?= Html::encode($this->title) ?></h1>

        <?= \yii\widgets\DetailView::widget([
            'model'      => $zalog,
            'options'    => [
                'class' => 'table table-striped detail-view',
                'style' => 'width: auto;',

            ],
            'attributes' => [
                [
                    'label'          => 'ID',
                    'value'          => $zalog->id,
                    'captionOptions' => [
                        'style' => 'width:50%',
                    ],
                ],
                'created_at:datetime:Создана',
                'finish:datetime:Оканчивается',
                [
                    'label'  => 'Сумма',
                    'format' => 'html',
                    'value'  => Html::tag('code', Yii::$app->formatter->asDecimal($zalog->amount / pow(10, $billing->getCurrencyObject()->decimals), $billing->getCurrencyObject()->decimals)) . ' ' . Html::tag('span', $billing->getCurrencyObject()->code, ['class' => 'label label-info']),
                ],
                [
                    'label' => 'Партнер сделки',
                    'value' => $partner->email,
                ],
                [
                    'label'  => 'Статус',
                    'format' => 'html',
                    'value'  => ($zalog->status == 1) ? Html::tag('span', 'Токены возвращены, сделка завершена', ['class' => 'label label-success']) : Html::tag('span', 'Сделка активна', ['class' => 'label label-default']),
                ],
            ],
        ]) ?>

        <?php if ( $zalog->status == 0 ) { ?>
            <?php
            $this->registerJs(<<<JS
$('.buttonReturn').click(function(e) {
    $('#modalZalog').modal();
});
JS
            );
            ?>
            <p><button class="btn btn-primary buttonReturn">Вернуть</button></p>
        <?php } ?>


    </div>
</div>


<div class="modal fade" id="modalZalog" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">&times;</span></button>
                <h4 class="modal-title text-center" id="myModalLabel">Возврат токенов</h4>
            </div>
            <div class="modal-body">
                <form id="formZalog">
                    <input name="zalog_id" value="<?= $zalog->id ?>" type="hidden">
                    <div class="form-group recipient-field-password">
                        <label class="control-label">Пароль:</label>
                        <input type="password" class="form-control"  name="password" autocomplete="off">
                        <p class="help-block">Пароль от кабинета</p>
                        <p class="help-block help-block-error" style="display: none;"></p>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <?php
                $url = (new \avatar\modules\ETH\ServiceEtherScan())->getUrl('/tx/');
                $this->registerJs(<<<JS

var functionZalogClick = function(e) {
    var b = $(this);
    b.off('click');
    b.html($('<i>', {class: 'fa fa-spinner fa-spin fa-fw'}));
    b.attr('disabled', 'disabled');
    
    ajaxJson({
        url: '/cabinet-bills-token/zalog-return',
        data: $('#formZalog').serializeArray(),
        success: function(ret) {
            b.on('click', functionZalogClick);
            b.html('Вернуть');
            b.removeAttr('disabled');
            
            $('#modalZalog').on('hidden.bs.modal', function(e) {
                $('#modalInfo .modal-body')
                .html(
                    $('<p>', {class: 'text-center'}).html(
                        'Успешно'
                    )
                )
                .append(
                    $('<p>', {class: 'text-center'}).html(
                        $('<a>', {href: '{$url}' + ret.txid, target: '_blank'}).html(
                            $('<code>').html(
                                ret.txid
                            )
                        )
                    )
                )
                ; 
                $('#modalInfo').on('hidden.bs.modal', function(e) {
                    window.location.reload();
                }).modal();
            }).modal('hide');
        },
        errorScript: function(ret) {
            switch (ret.id) {
                case 101:
                case 102:
                    var f = $('#formZalog');
                    $.each(ret.data,function(i,v) {
                        var name = v.name;
                        if (name == 'zalog_id') name = 'password';
                        var value = v.value;
                        var t = f.find('.recipient-field-' + name);
                        t.addClass('has-error');
                        t.find('p.help-block-error').html(value.join('<br>')).show();
                    });

                    break;
            }
            b.on('click', functionZalogClick);
            b.html('Вернуть');
            b.removeAttr('disabled');
        }
    });
};

$('.buttonOpen').click(functionZalogClick);

$('#formZalog .form-control').on('focus', function() {
    var o = $(this);
    var p = o.parent();
    if (p.hasClass('input-group')) {
        p = p.parent();
    }
    p.removeClass('has-error');
    p.find('p.help-block-error').hide();
});

JS
                );
                ?>
                <button type="button" class="btn btn-primary buttonOpen" style="width: 100%;">Вернуть</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modalInfo" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button"
                        class="close"
                        data-dismiss="modal"
                        aria-label="Close"><span
                            aria-hidden="true">&times;</span></button>
                <h4 class="modal-title text-center" id="myModalLabel"><?= \Yii::t('c.TsqnzVaJuC', 'Информация') ?></h4>
            </div>
            <div class="modal-body text-center">
                Успешно
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" style="width: 100%;" data-dismiss="modal">
                    <?= \Yii::t('c.TsqnzVaJuC', 'Закрыть') ?>
                </button>
            </div>
        </div>
    </div>
</div>