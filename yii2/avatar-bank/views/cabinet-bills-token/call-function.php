<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;
use yii\db\Query;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $billing \common\models\avatar\UserBill */
/* @var $model \avatar\models\forms\CabinetBillsTokenContract */

$this->title = Yii::t('c.NRHKEjgSii', 'Исполнить контракт');

$abi = $billing->getWalletToken()->token->abi;
$abiObject = \yii\helpers\Json::decode($abi);
$functionList = [];
foreach ($abiObject as $item) {
    if (isset($item['type'])) {
        if ($item['type'] == 'function') {
            if ($item['constant'] == false) {
                $functionList[] = $item;
            }
        }
    }
}

$strCall = Yii::t('c.NRHKEjgSii', 'Вызвать');
$strConfirm = Yii::t('c.NRHKEjgSii', 'Подтвердить');
$provider = new \avatar\modules\ETH\ServiceEtherScan();
$url = $provider->getUrl('/tx/');

$this->registerJs(<<<JS

$('#cabinetbillstokencontract-functionname').on('change', function(e) {
    $('.collapseAll').collapse('hide');
    var o = $(this)[0];
    var i = o.selectedIndex;
    var item = o.options[i];
    var name = item.label;
    
    $('.fn-' + name).collapse('show');
});

var clickCalc = function(e) {
    var b = $(this);
    b.off('click');
    b.html($('<i>', {class: 'fa fa-spinner fa-spin fa-fw'}));
    b.attr('disabled', 'disabled');
    
    ajaxJson({
        url: '/cabinet-bills-token/call-function-ajax' + '?' + 'id' + '=' + $(this).data('id'),
        data: $('#contact-form').serializeArray(),
        success: function(ret) {
            $('#modalInfo').modal();
            for(var i=0; i < ret.length; i++) {
                var item = ret[i];
                $('#modalInfo .modal-' + item.name).html(item.value);
            }
            console.log(ret);
            b.on('click', clickCalc);
            b.html('{$strCall}');
            b.removeAttr('disabled');     
        },
        errorScript: function(ret) {
            console.log(ret);
            switch (ret.id) {
                case 103: // functionName password
                    var f = $('#contact-form');
                    $.each(ret.data,function(i,v) {
                        var name = v.name;
                        var value = v.value;
                        var o = f.find('#cabinetbillstokencontract-'+ name);
                        var t = f.find('.field-cabinetbillstokencontract-' + name);
                        t.addClass('has-error');
                        t.find('p.help-block-error').html(value.join('<br>')).show();
                    });
                    
                    break;
                case 102: // paramErrors
                    var f = $('#contact-form');
                    var fname = $('#cabinetbillstokencontract-functionname').val();
                    
                    for (var a = 0; a < ret.data.length; a++) {
                        var item = ret.data[a];
                        var t = f.find('.field-cabinetbillstokencontract-params-' + fname + '-' + item.name);
                        t.addClass('has-error');
                        t.find('p.help-block-error').html(item.message).show();
                    }
                    
                    break;
            }
            
            b.on('click', clickCalc);
            b.html('{$strCall}');
            b.removeAttr('disabled');      
        }        
    })
};

$('#contact-form .form-control').on('focus', function() {
    var o = $(this);
    var p = o.parent();
    if (p.hasClass('input-group')) {
        p = p.parent();
    }
    p.removeClass('has-error');
    p.find('p.help-block-error').hide();
});

var buttonConfirm = function(e) {
    var d = $(this);
    d.off('click');
    d.html($('<i>', {class: 'fa fa-spinner fa-spin fa-fw'}));
    d.attr('disabled', 'disabled');
    
    ajaxJson({
        url: '/cabinet-bills-token/call-function-execute-ajax' + '?' + 'id' + '=' + $(this).data('id'),
        data: $('#contact-form').serializeArray(),
        success: function(ret) {
            $('#modalInfo2 .modal-body').html(
                $('<p>', {class: 'text-center'}).html(
                    $('<code>').html(
                        $('<a>', {href: '$url' + ret.address, target: '_blank'}).html(ret.address)
                    )
                )
            );
            $('#modalInfo').on('hidden.bs.modal', function(e) {
                 $('#modalInfo2').on('hidden.bs.modal', function(e) {
                     window.location = '/cabinet-bills/transactions-token' + '?id=' + {$billing->id};
                }).modal();
            }).modal('hide');
        },
        errorScript: function(ret) {
            d.on('click', buttonConfirm);
            d.html('{$strConfirm}');
            d.removeAttr('disabled');      
        }        
    })
};

$('#buttonSend').click(clickCalc);
$('.buttonConfirm').click(buttonConfirm);
JS
);


$token = $billing->getWalletToken()->token;
$currencyObject = $token->getCurrency();
$currencyId = $currencyObject->id;

$options = [
    'class' => 'img-circle',
    'src'   => $currencyObject->image,
    'style' => 'width: 100%;max-width: 308px;',
    'data'  => [
        'toggle'   => 'tooltip',
        'placement' => 'bottom',
    ],
    'title' => $currencyObject->title . ' ' . '(' . $currencyObject->code . ')',
];
$provider = new \avatar\modules\ETH\ServiceEtherScan();
$url = $provider->getUrl('/token/' . $token->address);

?>
<div class="container">
    <div class="col-lg-12">
        <h1 class="page-header text-center"><?= Html::encode($this->title) ?></h1>

        <p class="text-center">
            <?= \yii\helpers\Html::tag('img', null, $options) ?>
        </p>
        <p class="text-center"
           title="<?= \Yii::t('c.oifM0YkVx7', 'Адрес токена') ?>"
           data-toggle="tooltip"
           data-placement="bottom"
        >
            <?= \yii\helpers\Html::a(\yii\helpers\Html::tag('code', $token->address), $url, ['target' => '_blank']) ?>
        </p>

        <div class="row">
            <div class="col-lg-4 col-lg-offset-4">
                <?php $form = ActiveForm::begin([
                    'id'      => 'contact-form',
                    'options' => ['enctype' => 'multipart/form-data'],
                ]); ?>

                <?= $form->field($model, 'functionName')
                    ->dropDownList(ArrayHelper::merge(
                        ['- '.Yii::t('c.NRHKEjgSii', 'Ничего не выбрано').' -'],
                        ArrayHelper::map($functionList, 'name', 'name')
                    )) ?>
                <hr>

                <?php $c = 1; ?>
                <?php foreach ($functionList as $item) { ?>
                    <?php $options = ['collapse', 'collapseAll', 'fn-' . $item['name']]; ?>
                    <div class="<?= join(' ', $options) ?>" >
                        <?php foreach ($item['inputs'] as $i) { ?>
                            <div class="form-group field-cabinetbillstokencontract-params-<?= $item['name'] ?>-<?= $i['name'] ?>">
                                <label class="control-label" for="cabinetbillstokencontract-functionname-<?= $item['name'] ?>-<?= $i['name'] ?>"><?= $i['name'] ?></label>
                                <input class="form-control"
                                       type="text"
                                       name="CabinetBillsTokenContract[params][<?= $item['name'] ?>][<?= $i['name'] ?>]"
                                       id="cabinetbillstokencontract-functionname-<?= $item['name'] ?>-<?= $i['name'] ?>"
                                >

                                <div class="help-block "><code><?= $i['type'] ?></code></div>
                                <p class="help-block help-block-error"></p>
                            </div>
                        <?php } ?>
                    </div>
                    <?php $c++; ?>
                <?php } ?>

                <hr>
                <?= $form->field($model, 'password')->passwordInput()->hint(Yii::t('c.NRHKEjgSii', 'Пароль от кабинета')) ?>


                <hr>
                <div class="form-group">
                    <?= Html::button(Yii::t('c.NRHKEjgSii', 'Вызвать'), [
                        'class' => 'btn btn-default',
                        'id'    => 'buttonSend',
                        'style' => 'width:100%',
                        'data'  => [
                            'id' => $billing->id,
                        ]
                    ]) ?>
                </div>
                <?php ActiveForm::end(); ?>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modalInfo" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">&times;</span></button>
                <h4 class="modal-title text-center" id="myModalLabel"><?= \Yii::t('c.NRHKEjgSii', 'Подтверждение') ?></h4>
            </div>
            <div class="modal-body text-center">
                <?php
                $params = [
                    'from',
                    'to',
                    'functionName',
                    'costEth',
                    'costCalc',
                    'gasPriceGwei',
                ];
                ?>
                <table class="table table-striped table-hover" id="tableModal">
                    <?php foreach ($params as $p) { ?>
                        <tr>
                            <td>
                                <?= $p ?>
                            </td>
                            <td class="modal-<?= $p ?>">

                            </td>
                        </tr>
                    <?php } ?>
                </table>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary buttonConfirm" style="width: 100%;" data-id="<?= $billing->id ?>">
                    <?= $strConfirm ?>
                </button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modalInfo2" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">&times;</span></button>
                <h4 class="modal-title text-center" id="myModalLabel"><?= \Yii::t('c.NRHKEjgSii', 'Завершено успешно') ?></h4>
            </div>
            <div class="modal-body text-center">

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" style="width: 100%;" data-dismiss="modal">
                    <?= \Yii::t('c.NRHKEjgSii', 'Закрыть') ?>
                </button>
            </div>
        </div>
    </div>
</div>