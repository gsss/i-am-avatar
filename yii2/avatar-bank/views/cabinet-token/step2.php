<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;
use app\models\UnionCategory;

/* @var $this yii\web\View */
/* @var $request \common\models\RequestTokenCreate */

$this->title = 'Выпуск токена. Шаг 2. Оплата';


$this->registerJs(<<<JS

var functionNew = function(e) {
    var b = $(this);
    b.off('click');
    b.html($('<i>', {class: 'fa fa-spinner fa-spin fa-fw'}));
    b.attr('disabled', 'disabled');
    ajaxJson({
        url: '/cabinet-token/step2-ajax',
        data: $('#formToken').serializeArray(),
        success: function(ret) {
            // удачно зарегистрирован, переместить человека на следующий шаг
            window.location = '/cabinet-token/step3?id=' + {$request->id}
        },
        errorScript: function(ret) {
            switch (ret.id) {
                case 102:
                    var f = $('#formToken');
                    $.each(ret.data,function(i,v) {
                        var name = v.name;
                        var value = v.value;
                        var o = f.find('#cabinettokenstep2-'+ name);
                        var t = f.find('.field-cabinettokenstep2-' + name);
                        t.addClass('has-error');
                        t.find('p.help-block-error').html(value.join('<br>')).show();
                    });
                    break;
            }
            b.on('click', functionNew);
            b.html('Создать');
            b.removeAttr('disabled');
        }
    });
};
$('#formToken .form-control').on('focus', function() {
    var o = $(this);
    var p = o.parent();
    if (p.hasClass('input-group')) {
        p = p.parent();
    }
    p.removeClass('has-error');
    p.find('p.help-block-error').hide();
});

$('.buttonPay').click(functionNew);
JS
);

$model = new \avatar\models\forms\CabinetTokenStep2(['request_id' => $request->id]);

?>
<div class="container">
    <div class="col-lg-12">
        <h1 class="page-header"><?= Html::encode($this->title) ?></h1>

    </div>
    <div class="col-lg-8">

        <p class="alert alert-success">Вам нужно заплатить 1 AVR за выпуск токена.</p>

        <?php $form = ActiveForm::begin([
            'id' => 'formToken'
        ]) ?>

        <?= Html
            ::activeHiddenInput($model, 'request_id') ?>
        <?= $form->field($model, 'password')->passwordInput() ?>
        <?= $form->field($model, 'billing_id')->dropDownList(
            \yii\helpers\ArrayHelper::map(
                    \common\models\avatar\UserBill::find()
                    ->where(['in', 'id', [1499,1500]])
                    ->all(),
                    'id',
                    'name'
            )
        ) ?>

        <hr>
        <?= Html::button('Заплатить', [
            'class' => 'btn btn-success buttonPay',
            'style' => 'width:100%',
        ]) ?>

        <?php ActiveForm::end() ?>

    </div>
    <div class="col-lg-4">
        <?= $this->render('../cabinet/_menu') ?>
    </div>
</div>



