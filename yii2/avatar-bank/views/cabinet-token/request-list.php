<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;
use app\models\UnionCategory;

/* @var $this yii\web\View */

$this->title = 'Заявки на выпуск токена';


$this->registerJs(<<<JS
$('.rowTable').click(function() {
    window.location = '/cabinet-token/view?' +
     'id' + '=' + $(this).data('id')
     ;
});
JS
);

$sort = new \yii\data\Sort([
    'attributes' => [
        'created_at',
    ],
    'defaultOrder' => [
        'created_at' => SORT_DESC,
    ],
]);
?>
<style>
    .textDecorated {
        text-decoration-line: underline;
        text-decoration-style: dotted;
    }
</style>
<div class="container">
    <div class="col-lg-12">
        <h1 class="page-header"><?= Html::encode($this->title) ?></h1>

    </div>
    <div class="col-lg-8">

        <p>
            <a href="/cabinet-token/new" class="btn btn-default">Выпустить токен</a>
        </p>

        <?= \yii\grid\GridView::widget([
            'dataProvider' => new \yii\data\ActiveDataProvider([
                'query' => \common\models\RequestTokenCreate::find()->where(['user_id' => Yii::$app->user->id]),
                'sort' => $sort,
            ]),
            'tableOptions' => [
                'class' => 'table table-hover table-striped',
                'style' => 'width: auto;',
                'id'    => 'tableTransaction',
            ],
            'rowOptions'   => function ($item) {
                $params = [
                    'data'  => [
                        'id' => $item['id'],
                    ],
                    'role'  => 'button',
                    'class' => 'rowTable'
                ];

                return $params;
            },
            'columns' => [
                'id',
                [
                    'header'  => 'Картинка',
                    'content' => function ($item) {
                        $v = \yii\helpers\ArrayHelper::getValue($item, 'image', '');

                        return Html::img($v, [
                            'width' => 50,
                            'class' => 'img-circle',
                        ]);
                    },
                ],
                'name',
                [
                    'attribute' => 'coin_emission',
                    'format'    => ['decimal', 0],
                    'headerOptions'  => [
                        'style' => 'text-align:right',
                    ],
                    'contentOptions' => [
                        'style' => 'text-align:right',
                    ],
                ],
                [
                    'header'    => 'Создано',
                    'attribute' => 'created_at',
                    'content'   => function ($item) {
                        $v = \yii\helpers\ArrayHelper::getValue($item, 'created_at', 0);
                        if ($v == 0) return '';

                        return Html::tag('span', \cs\services\DatePeriod::back($v, ['isShort' => true]), [
                            'data'  => [
                                'toggle'    => 'tooltip',
                                'placement' => 'bottom',
                            ],
                            'title' => Yii::$app->formatter->asDatetime($v),
                            'class' => 'textDecorated'
                        ]);
                    },
                ],
                [
                    'header'  => 'Зарегистрирован в системе?',
                    'content' => function ($item) {
                        if ($item['is_register_in_system'] == 1) {
                            return Html::tag('span', 'Да', ['class' => 'label label-success']);
                        }
                        return Html::tag('span', 'Нет', ['class' => 'label label-default']);
                    },
                ],
            ],
        ])?>

    </div>
    <div class="col-lg-4">
        <?= $this->render('../cabinet/_menu') ?>
    </div>
</div>



