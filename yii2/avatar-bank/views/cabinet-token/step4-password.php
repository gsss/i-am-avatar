<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;
use app\models\UnionCategory;

/* @var $this yii\web\View */
/* @var $request \common\models\RequestTokenCreate */

$this->title = 'Выпуск токена. Шаг 4. Регистрация контракта в блокчейне';


$this->registerJs(<<<JS

var functionSendAjax = function(e) {
    var b = $(this);
    b.off('click');
    b.html($('<i>', {class: 'fa fa-spinner fa-spin fa-fw'}));
    b.attr('disabled', 'disabled');
    $('#step2').collapse('show');
    ajaxJson({
        url: '/cabinet-token/step4-ajax',
        data: $('#formToken').serializeArray(),
        success: function(ret) {
            // удачно зарегистрирован, переместить человека на следующий шаг
            $('#modalInfo .modal-body').append(
                $('<p>').append(
                    $('<code>').append(ret.txid)
                )
            );
            $('#modalInfo').on('hidden.bs.modal', function(e) {
                window.location = '/cabinet-token/view?id=' + {$request->id}
            }).modal();
        },
        errorScript: function(ret) {
            switch (ret.id) {
                case 102:
                    var f = $('#formToken');
                    $.each(ret.data,function(i,v) {
                        var name = v.name;
                        var value = v.value;
                        var o = f.find('#cabinettokenstep3-'+ name);
                        var t = f.find('.field-cabinettokenstep3-' + name);
                        t.addClass('has-error');
                        t.find('p.help-block-error').html(value.join('<br>')).show();
                    });
                    break;
                case 103:
                    setTimeout(functionSendAjax, 3000);
                    return;
                    break;
                case 104:
                    alert('Не хватает денег на эфировском кошельке чтобы оплатить комиссию, пополните кошелек эфира и зайдите снова в заявку чтобы закончить выпуск токена');
                    break;
            }
            $('#step2').collapse('hide');
            b.on('click', functionSendAjax);
            b.html('Зарегистрировать');
            b.removeAttr('disabled');
        }
    });
};
$('#formToken .form-control').on('focus', function() {
    var o = $(this);
    var p = o.parent();
    if (p.hasClass('input-group')) {
        p = p.parent();
    }
    p.removeClass('has-error');
    p.find('p.help-block-error').hide();
});

$('.buttonPay').click(functionSendAjax);
JS
);

$model = new \avatar\models\forms\CabinetTokenStep4(['request_id' => $request->id]);

?>
<div class="container">
    <div class="col-lg-12">
        <h1 class="page-header"><?= Html::encode($this->title) ?></h1>

    </div>
    <div class="col-lg-8">

        <div class="row">
            <div class="col-lg-6">
                <?php $form = ActiveForm::begin([
                    'id' => 'formToken'
                ]) ?>

                <?= Html::activeHiddenInput($model, 'request_id') ?>

                <?= $form->field($model, 'password')->passwordInput() ?>
                <hr>
                <?= Html::button('Зарегистрировать', [
                    'class' => 'btn btn-success buttonPay',
                    'style' => 'width:100%',
                ]) ?>

                <?php ActiveForm::end() ?>
                <div class="collapse" id="step2">
                    <p class="alert alert-success">Начинается компиляция и регистрация контракта в сеть. Это может занять от 30 до 60 сек. Ожидайте пожалуйста.</p>
                </div>

            </div>
        </div>

    </div>
    <div class="col-lg-4">
        <?= $this->render('../cabinet/_menu') ?>
    </div>
</div>

<div class="modal fade" id="modalInfo" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">&times;</span></button>
                <h4 class="modal-title text-center" id="myModalLabel"><?= \Yii::t('c.TsqnzVaJuC', 'Информация') ?></h4>
            </div>
            <div class="modal-body text-center">

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" style="width: 100%;" data-dismiss="modal">
                    <?= \Yii::t('c.TsqnzVaJuC', 'Закрыть') ?>
                </button>
            </div>
        </div>
    </div>
</div>

