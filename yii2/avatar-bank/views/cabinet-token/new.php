<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;
use app\models\UnionCategory;

/* @var $this yii\web\View */

$this->title = 'Выпуск токена';

$model = new \avatar\models\validate\CabinetTokenControllerNew();

$this->registerJs(<<<JS
var c = 0;
var abi;

var functionNew = function(e) {
    var b = $(this);
    b.off('click');
    b.html($('<i>', {class: 'fa fa-spinner fa-spin fa-fw'}));
    b.attr('disabled', 'disabled');
    ajaxJson({
        url: '/cabinet-token/new-ajax',
        data: $('#formToken').serializeArray(),
        success: function(ret) {
            // удачно зарегистрирован, переместить человека на следующий шаг
            window.location = '/cabinet-token/step4?id=' + ret.id
        },
        errorScript: function(ret) {
            switch (ret.id) {
                case 102:
                    var f = $('#formToken');
                    $.each(ret.data,function(i,v) {
                        var name = v.name;
                        var value = v.value;
                        var o = f.find('#cabinettokencontrollernew-'+ name);
                        var t = f.find('.field-cabinettokencontrollernew-' + name);
                        t.addClass('has-error');
                        t.find('p.help-block-error').html(value.join('<br>')).show();
                    });
                    break;
            }
            b.on('click', functionNew);
            b.html('Создать');
            b.removeAttr('disabled');
        }
    });
};
$('#formToken .form-control').on('focus', function() {
    var o = $(this);
    var p = o.parent();
    if (p.hasClass('input-group')) {
        p = p.parent();
    }
    p.removeClass('has-error');
    p.find('p.help-block-error').hide();
});
$('.buttonCreate').click(functionNew);
JS
);

?>
<div class="container">
    <div class="col-lg-12">
        <h1 class="page-header"><?= Html::encode($this->title) ?></h1>

    </div>
    <div class="col-lg-8">

        <?php $form = ActiveForm::begin([
            'id' => 'formToken'
        ]) ?>

        <?= $form->field($model, 'name') ?>
        <?= $form->field($model, 'total') ?>
        <?= $form->field($model, 'decimals') ?>
        <?= $form->field($model, 'code') ?>

        <?= $form->field($model, 'is_add')->widget('\common\widgets\CheckBox2\CheckBox')->label('Нужна ли функция довыпуска') ?>
        <?= $form->field($model, 'is_burn')->widget('\common\widgets\CheckBox2\CheckBox')->label('Нужна ли функция сжигания токенов') ?>
        <?= $form->field($model, 'is_change_owner')->widget('\common\widgets\CheckBox2\CheckBox')->label('Нужна ли функция смены владельца контрата') ?>

        <?= $form->field($model, 'image')->widget('\common\widgets\FileUpload4\FileUploadMany') ?>

        <hr>
        <?= Html::button('Создать', [
            'class' => 'btn btn-success buttonCreate',
            'style' => 'width:100%',
        ]) ?>

        <?php ActiveForm::end() ?>

    </div>
    <div class="col-lg-4">
        <?= $this->render('../cabinet/_menu') ?>
    </div>
</div>



