<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;
use app\models\UnionCategory;

/* @var $this \yii\web\View */
/* @var $request \common\models\RequestTokenCreate */

$this->title = 'Заявка на выпуск токена #'.$request->id;

$p = new \avatar\modules\ETH\ServiceEtherScan();
$urlEtherScan = $p->getUrl();

$this->registerJs(<<<JS
$('.buttonTest').click(function (e) {
    var button = $(this);
    var id = $(this).data('id');
    ajaxJson({
        url: '/cabinet-token/test-tx' + '?' + 'id' + '=' + {$request->id},
        success: function (ret) {
            $('#modalInfo').on('hidden.bs.modal', function() {
                window.location.reload();
            }).modal();
        }, 
        errorScript: function(ret) {
            
        }
    });
});

JS
);

?>
<div class="container">
    <div class="col-lg-12">
        <h1 class="page-header"><?= Html::encode($this->title) ?></h1>
    </div>
    <div class="col-lg-8">

        <?php if (is_null($request->address)) { ?>
            <p class="alert alert-warning">Транзакция подтверждается в блокчейне. Ожидайте от нескольких секунд до нескольких минут. <i class="fa fa-spinner fa-spin fa-fw"></i></p>
        <?php } ?>
        <?= \yii\widgets\DetailView::widget([
            'model'      => $request,
            'attributes' => [
                'name',               // title attribute (in plain text)
                'code',               // title attribute (in plain text)
                'decimals',               // title attribute (in plain text)
                'coin_emission:decimal:Эмиттировано',               // title attribute (in plain text)
                [
                    'attribute' => 'image',
                    'format'    => ['image', ['class' => 'img-circle', 'style' => 'border: 1px solid;']],
                ],
            ],
        ]) ?>

        <p>Время создания заявки:</p>
        <p><code><?= Yii::$app->formatter->asDatetime($request->created_at) ?></code></p>

        <p>Адрес хозяина токена:</p>
        <?php
        try {
            $billing = \common\models\avatar\UserBill::findOne(['address' => $request->owner, 'currency' => \common\models\avatar\Currency::ETH]);
            $html = Html::a('Счет', ['cabinet-bills/transactions-eth', 'id' => $billing->id]);
        } catch (Exception $e) {
            $html = '';
        }
        ?>
        <p><a href="<?= $urlEtherScan . '/address/' . $request->owner ?>" target="_blank"><code><?= $request->owner ?></code></a> <?= $html ?></p>


        <p>Транзакция регистрации:</p>
        <p><a href="<?= $urlEtherScan . '/tx/' . $request->txid ?>" target="_blank"><code><?= $request->txid ?></code></a></p>


        <p>Адрес контракта:</p>

        <div class="progress" style="margin-top: 20px;display: none;">
            <div class="progress-bar" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%;" id="progressBar">
                <span class="sr-only">60% Complete</span>
            </div>
        </div>



        <?php if (is_null($request->address)) { ?>
            <?php

            $this->registerJs(<<<JS
var c = 0;
var abi;

var functionGet = function(t) {
    var dd = t;
    setTimeout(function() {
        c++;
        var val = c * 10;
        ajaxJson({
            url: '/cabinet-token/new-check-ajax',
            data: {
                txid: dd
            },
            success: function(ret) {
                if (ret[0]) {
                    
                    ajaxJson({
                        url: '/cabinet-token/registration2',
                        data: {
                            id: {$request->id},
                            address: ret[1]
                        },
                        success: function(ret1) {
                            $('#modalInfo .modal-body').append(
                                $('<p>').append(
                                    $('<code>').append(ret[1])
                                )
                            );
                            $('#modalInfo').on('hidden.bs.modal', function(e) {
                                window.location = '/cabinet-bills/transactions-token?id=' + ret1.wallet.id
                            }).modal();
                        }
                    });
                    
                } else {
                    functionGet(t);
                }
            }
        });
    }, 10000);
};

functionGet('{$request->txid}');

JS
            );
            ?>
            <p><code>На обработке</code></p>
        <?php } else { ?>
            <p><a href="<?= $urlEtherScan . '/token/' . $request->address ?>" target="_blank"><code><?= $request->address ?></code></a></p>
        <?php } ?>

        <p>Код контракта</p>
        <pre><?= $request->contract ?></pre>

        <p>Интерфейс контракта</p>
        <pre><?= $request->abi ?></pre>

        <p>Данные для инициализации контракта</p>
        <pre><?= $request->init ?></pre>

    </div>
    <div class="col-lg-4">
        <?= $this->render('../cabinet/_menu') ?>
    </div>
</div>



<div class="modal fade" id="modalInfo" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">&times;</span></button>
                <h4 class="modal-title text-center" id="myModalLabel"><?= \Yii::t('c.TsqnzVaJuC', 'Информация') ?></h4>
            </div>
            <div class="modal-body text-center">
                Успешно. Ваш токен выпущен
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" style="width: 100%;" data-dismiss="modal">
                    <?= \Yii::t('c.TsqnzVaJuC', 'Закрыть') ?>
                </button>
            </div>
        </div>
    </div>
</div>