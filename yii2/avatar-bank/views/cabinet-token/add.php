<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;
use app\models\UnionCategory;

/* @var $this yii\web\View */
/* @var $model  */

$this->title = 'Довыпустить токен';


?>
<div class="container">
    <div class="col-lg-12">
        <h1 class="page-header"><?= Html::encode($this->title) ?></h1>
    </div>
    <div class="col-lg-8">

        <?php if (Yii::$app->session->hasFlash('form')) { ?>
            <p class="alert alert-success">Готово <code><?= Yii::$app->session->getFlash('form') ?></code>.</p>
        <?php } else { ?>
            <?php $form = ActiveForm::begin([
                'id' => 'formToken'
            ]) ?>

            <?= $form->field($model, 'password')->passwordInput() ?>
            <?= $form->field($model, 'amount') ?>

            <hr>
            <?= Html::submitButton('Прибавить', [
                'class' => 'btn btn-success',
                'style' => 'width:100%',
            ]) ?>

            <?php ActiveForm::end() ?>
        <?php } ?>

    </div>
    <div class="col-lg-4">
        <?= $this->render('../cabinet/_menu') ?>
    </div>
</div>



