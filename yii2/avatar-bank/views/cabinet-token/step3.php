<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;
use app\models\UnionCategory;

/* @var $this yii\web\View */
/* @var $request \common\models\RequestTokenCreate */

$this->title = 'Выпуск токена. Шаг 3. Расчет стоимости регистрации контракта';


$this->registerJs(<<<JS

var functionSendAjax = function() {
    ajaxJson({
        url: '/cabinet-token/step3-ajax',
        data: $('#formToken').serializeArray(),
        success: function(ret) {
            console.log(ret);
            // удачно зарегистрирован, переместить человека на следующий шаг
            // $('#modalInfo .modal-body').append(
            //     $('<p>').append(
            //         $('<code>').append(ret.txid)
            //     )
            // );
            $('#modalInfo').on('hidden.bs.modal', function(e) {
                window.location = '/cabinet-token/view?id=' + {$request->id}
            }).modal();
        },
        errorScript: function(ret) {
            switch (ret.id) {
                case 102:
                    var f = $('#formToken');
                    $.each(ret.data,function(i,v) {
                        var name = v.name;
                        var value = v.value;
                        var o = f.find('#cabinettokenstep3-'+ name);
                        var t = f.find('.field-cabinettokenstep3-' + name);
                        t.addClass('has-error');
                        t.find('p.help-block-error').html(value.join('<br>')).show();
                    });
                    break;
                case 103:
                    setTimeout(functionSendAjax, 3000);
                    break;
                case 104:
                    alert('Не хватает денег на эфировском кошельке чтобы оплатить комиссию, пополните кошелек эфира и зайдите снова в заявку чтобы закончить выпуск токена');
                    break;
            }
        }
    });
};

functionSendAjax();

JS
);

$model = new \avatar\models\forms\CabinetTokenStep3(['request_id' => $request->id]);

?>
<div class="container">
    <div class="col-lg-12">
        <h1 class="page-header"><?= Html::encode($this->title) ?></h1>

    </div>
    <div class="col-lg-8">

        <p class="alert alert-warning">Расчитывается стоимость регистрации в блокчейне. Ожидайте около нескольких секунд. <i class="fa fa-spinner fa-spin fa-fw"></i></p>

        <?php $form = ActiveForm::begin(['id' => 'formToken']) ?>

        <?= Html::activeHiddenInput($model, 'request_id') ?>

        <?php ActiveForm::end() ?>

    </div>
    <div class="col-lg-4">
        <?= $this->render('../cabinet/_menu') ?>
    </div>
</div>



<div class="modal fade" id="modalInfo" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">&times;</span></button>
                <h4 class="modal-title text-center" id="myModalLabel"><?= \Yii::t('c.TsqnzVaJuC', 'Информация') ?></h4>
            </div>
            <div class="modal-body text-center">

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" style="width: 100%;" data-dismiss="modal">
                    <?= \Yii::t('c.TsqnzVaJuC', 'Закрыть') ?>
                </button>
            </div>
        </div>
    </div>
</div>