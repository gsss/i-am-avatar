<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;
use yii\db\Query;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/** @var $school \common\models\school\School */
/** @var $model \avatar\models\forms\school\Article */

$this->title = $model->name;

?>
<div class="container">

    <h1 class="page-header"><?= Html::encode($this->title) ?></h1>

    <?php if (Yii::$app->session->hasFlash('form')) { ?>
        <div class="alert alert-success">
            Успешно обновлено.
        </div>

    <?php } else { ?>

        <div class="row">
            <div class="col-lg-8">
                <?php $form = \iAvatar777\services\FormAjax\ActiveForm::begin([
                    'model'   => $model,
                    'success' => <<<JS
function (ret) {
    $('#modalInfo').on('hidden.bs.modal', function() {
        window.location = '/cabinet-school-blago-program/index?id='  + {$school->id};
    }).modal();
}
JS

                ]); ?>
                <?= $form->field($model, 'name') ?>
                <?= $form->field($model, 'success_url') ?>
                <?= $form->field($model, 'error_url') ?>


                <hr>
                <?php $form = \iAvatar777\services\FormAjax\ActiveForm::end([]); ?>
            </div>
        </div>
    <?php } ?>


</div>
<div class="modal fade" id="modalInfo" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Информация</h4>
            </div>
            <div class="modal-body">
                Успешно!
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
            </div>
        </div>
    </div>
</div>