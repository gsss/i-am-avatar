<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;
use app\models\UnionCategory;
use yii\db\Query;
use yii\helpers\ArrayHelper;

/** @var $this yii\web\View */
/** @var $form yii\bootstrap\ActiveForm */
/** @var $model \avatar\models\forms\BlagoProgram */

$this->title = $model->name;

?>
<div class="container">

    <h1 class="page-header"><?= Html::encode($this->title) ?></h1>

    <div class="row">
        <div class="col-lg-8">

            <p>Ссылка: <?= $school->getUrl('/blago-program?id=' . $model->id) ?></p>
            <p>Ссылка для возврата токенов: <?= $school->getUrl('/blago-program2?id=' . $model->id) ?></p>
            <?= \yii\widgets\DetailView::widget(['model' => $model]) ?>

            <h2>Взносы</h2>

            <?php \yii\widgets\Pjax::begin(); ?>
            <?php
            $this->registerJS(<<<JS
$('[data-toggle="tooltip"]').tooltip();

$('.rowTable').click(function() {
//    window.location = '/admin-blog/edit' + '?' + 'id' + '=' + $(this).data('id');
});
JS
            );
            ?>
            <?= \yii\grid\GridView::widget([
                'dataProvider' => new \yii\data\ActiveDataProvider([
                    'query'      => \common\models\SchoolBlagoInput::find()
                        ->where(['program_id' => $model->id])
                        ->select([
                                'school_blago_input.*',
                                'billing.is_paid',
                            ])
                        ->innerJoin('billing', 'billing.id = school_blago_input.billing_id')
                        ->asArray()
                    ,
                    'pagination' => [
                        'pageSize' => 20,
                    ],
                ]),
                'tableOptions' => [
                    'class' => 'table table-striped table-hover',
                ],
                'rowOptions'   => function ($item) {
                    $data = [
                        'data'  => ['id' => $item['id']],
                        'role'  => 'button',
                        'class' => 'rowTable'
                    ];
                    return $data;
                },
                'columns' => [
                        'id',
                        [
                            'header'  => 'сумма',
                            'contentOptions' => ['class' => 'text-right'],
                            'headerOptions' => ['class' => 'text-right'],
                            'content' => function ($item) {
                                $c = \common\models\piramida\Currency::findOne($item['currency_id']);
                                if (is_null($c)) return '';

                                return Yii::$app->formatter->asDecimal($item['amount'] / pow(10, $c->decimals), $c->decimals);
                            }
                        ],
                        [
                            'header'  => 'Валюта',
                            'content' => function ($item) {
                                $c = \common\models\piramida\Currency::findOne($item['currency_id']);
                                if (is_null($c)) return '';

                                return Html::tag('span', $c->code, ['class' => 'label label-info']);
                            }
                        ],
                        [
                            'header'  => 'Создано',
                            'content' => function ($item) {
                                $v = \yii\helpers\ArrayHelper::getValue($item, 'created_at', 0);
                                if ($v == 0) return '';

                                return Html::tag('abbr', \cs\services\DatePeriod::back($v, ['isShort' => true]), ['data' => ['toggle' => 'tooltip'], 'title' => Yii::$app->formatter->asDatetime($v)]);
                            }
                        ],
                        [
                            'header'  => 'Оплачено? (магазин)',
                            'content' => function ($item) {
                                $v = \yii\helpers\ArrayHelper::getValue($item, 'is_paid', 0);

                                if ($v) {
                                    $html = Html::tag('span', 'Да', ['class' => 'label label-success']);
                                } else {
                                    $html = Html::tag('span', 'Нет', ['class' => 'label label-default']);
                                }

                                return $html;
                            },
                        ],
                ],
            ]) ?>
            <?php \yii\widgets\Pjax::end(); ?>
        </div>
    </div>


</div>
<div class="modal fade" id="modalInfo" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Информация</h4>
            </div>
            <div class="modal-body">
                Успешно!
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
            </div>
        </div>
    </div>
</div>