<?php
/**
 * Created by PhpStorm.
 * User: Metatron
 * Date: 22.12.2018
 * Time: 5:44
 */
use yii\helpers\Html;

/** @var \yii\web\View $this */

$this->title = 'Кошельки';

\avatar\assets\Notify::register($this);
\avatar\assets\Clipboard::register($this);
$this->registerJS(<<<JS

var clipboard = new Clipboard('.buttonCopy');
clipboard.on('success', function(e) {

    new Noty({
        timeout: 1000,
        theme: 'sunset',
        type: 'success',
        layout: 'bottomLeft',
        text: 'Скопировано'
    }).show();

});

JS
);
\avatar\assets\Paralax::register($this);

?>
<div class="parallax-window" data-parallax="scroll"
     data-image-src="/images/controller/coin-explorer/index/blockchain-reaction-american-libraries.jpg"
     style="min-height: 300px; background: transparent;">
    <h1 class="text-center" style="padding-top: 100px;margin-top: 0px;"><?= Html::encode($this->title) ?></h1>

</div>
<div class="container">
    <div class="col-lg-12">
        <p>
            <a href="/coin-explorer/index" class="btn btn-default">Проводник</a>
            <a href="/coin-explorer/currency" class="btn btn-default">Список монет</a>
            <a href="/coin-explorer/transaction" class="btn btn-default">Список всех транзакций</a>
            <a href="/coin-explorer/operation" class="btn btn-default">Список всех операций</a>
            <a href="/coin-explorer/wallet" class="btn btn-default">Список всех кошельков</a>
        </p>
        <?php \yii\widgets\Pjax::begin(); ?>
        <?php
        $this->registerJS(<<<JS
$('[data-toggle="tooltip"]').tooltip();

$('.buttonView').click(function(e) {
    e.preventDefault();
    e.stopPropagation();
    window.location = '/coin-explorer/operation' + '?' + 'id' + '=' + $(this).data('id');
});
JS
        );
        $sort = new \yii\data\Sort([
            'defaultOrder' => ['id' => SORT_ASC],
        ]);
        ?>
        <?= \yii\grid\GridView::widget([
            'dataProvider' => new \yii\data\ActiveDataProvider([
                'query'      => \common\models\piramida\Wallet::find()
                    ->where(['is_deleted' => 0])
                ,
                'pagination' => [
                    'pageSize' => 20,
                ],
                'sort'       => $sort,
            ]),
            'tableOptions' => [
                'class' => 'table table-striped table-hover',
            ],
            'rowOptions'   => function ($item) {
                $data = [
                    'data'  => ['id' => $item['id']],
                    'role'  => 'button',
                    'class' => 'rowTable',
                ];
                return $data;
            },
            'columns'      => [
                [
                    'header'        => 'Адрес',
                    'headerOptions' => ['style' => 'width: 20%'],
                    'attribute'     => 'address',
                    'content'       => function (\common\models\piramida\Wallet $item) {
                        $address = $item->getAddress();
                        $options = [
                            'data'  => [
                                'toggle'         => 'tooltip',
                                'clipboard-text' => $address,
                            ],
                            'class' => 'buttonCopy',
                            'title' => 'Нажми чтобы скопировать',
                        ];

                        return Html::tag('code', $item->getAddressShort(), $options);
                    },
                ],
                [
                    'header'  => 'Монета',
                    'content' => function (\common\models\piramida\Wallet $item) {
                        $link = \common\models\avatar\CurrencyLink::findOne(['currency_int_id' => $item->currency_id]);
                        $currencyExt = \common\models\avatar\Currency::findOne($link->currency_ext_id);


                        return Html::img(\common\widgets\FileUpload7\FileUpload::getFile($currencyExt->image, 'crop'), [
                            'class'  => "img-circle",
                            'width'  => 50,
                            'height' => 50,
                            'style'  => 'margin-bottom: 0px;',
                            'data'  => [
                                'toggle'         => 'tooltip',
                            ],
                            'title' => $currencyExt->title . ' ['.$currencyExt->code.']',
                        ]);
                    },
                ],
                [
                    'header'        => 'Валюта',
                    'headerOptions' => ['style' => 'width: 20%'],
                    'attribute'     => 'currency_id',
                    'content'       => function ($item) {
                        $v = \yii\helpers\ArrayHelper::getValue($item, 'currency_id', 0);
                        $w = new \common\models\piramida\Currency(['id' => $v]);
                        $address = $w->getAddress();
                        $options = [
                            'data'  => [
                                'toggle'         => 'tooltip',
                                'clipboard-text' => $address,
                            ],
                            'class' => 'buttonCopy',
                            'title' => 'Нажми чтобы скопировать',
                        ];

                        return Html::tag('code', $w->getAddressShort(), $options);
                    },
                ],
                [
                    'header'         => 'Атомов',
                    'attribute'      => 'amount',
                    'headerOptions'  => ['class' => 'text-right'],
                    'contentOptions' => ['class' => 'text-right'],
                ],
                [
                    'header'        => 'Операции',
                    'headerOptions' => ['style' => 'width: 10%'],
                    'content'       => function ($item) {
                        return Html::button('Операции', ['class' => 'btn btn-default btn-xs buttonView', 'data' => ['id' => $item['id']]]);
                    },
                ],
            ],
        ]) ?>
        <?php \yii\widgets\Pjax::end(); ?>

    </div>
</div>



