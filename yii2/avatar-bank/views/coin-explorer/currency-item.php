<?php
/**
 * Created by PhpStorm.
 * User: Metatron
 * Date: 22.12.2018
 * Time: 5:44
 */
use yii\helpers\Html;

/** @var \yii\web\View $this */
/** @var \common\models\piramida\Currency $item */

$this->title = 'Монета ' . $item->name;

\avatar\assets\Notify::register($this);
\avatar\assets\Clipboard::register($this);

$this->registerJS(<<<JS

var clipboard = new Clipboard('.buttonCopy');
clipboard.on('success', function(e) {

    new Noty({
        timeout: 1000,
        theme: 'sunset',
        type: 'success',
        layout: 'bottomLeft',
        text: 'Скопировано'
    }).show();

});

JS
);
\avatar\assets\Paralax::register($this);

?>
<div class="parallax-window" data-parallax="scroll"
     data-image-src="/images/controller/coin-explorer/index/blockchain-reaction-american-libraries.jpg"
     style="min-height: 300px; background: transparent;">
    <h1 class="text-center" style="padding-top: 100px;margin-top: 0px;"><?= Html::encode($this->title) ?></h1>

</div>
<div class="container">
    <div class="col-lg-12">
        <p>
            <a href="/coin-explorer/index" class="btn btn-default">Проводник</a>
            <a href="/coin-explorer/currency" class="btn btn-default">Список монет</a>
            <a href="/coin-explorer/transaction" class="btn btn-default">Список всех транзакций</a>
            <a href="/coin-explorer/operation" class="btn btn-default">Список всех операций</a>
            <a href="/coin-explorer/wallet" class="btn btn-default">Список всех кошельков</a>
        </p>
        <?php
        $address = $item->getAddress();
        ?>
        <?= \yii\widgets\DetailView::widget([
            'model'      => $item,
            'attributes' => [
                [
                    'label'          => 'ID',
                    'value'          => $item->id,
                    'captionOptions' => ['style' => 'width: 30%'],
                ],
                [
                    'label' => 'Наименование',
                    'value' => $item->name,
                ],
                [
                    'label'  => 'Код',
                    'format' => 'html',
                    'value'  => Html::tag('span', $item->code, ['class' => 'label label-info']),
                ],
                [
                    'label'  => 'Адрес',
                    'format' => 'html',
                    'value'  => Html::tag('code', $address, ['title' => 'Нажми чтобы скопировать','class' => 'buttonCopy', 'data' => ['toggle' => 'tooltip', 'clipboard-text' => $address,]]),
                ],
                [
                    'label' => 'Зкаков после запятой',
                    'value' => $item->decimals,
                ],
                [
                    'label' => 'Атомов в обороте',
                    'value' => $item->amount,
                ],
            ],
        ]) ?>

    </div>
</div>



