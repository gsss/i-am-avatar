<?php
/**
 * Created by PhpStorm.
 * User: Metatron
 * Date: 22.12.2018
 * Time: 5:44
 */
use yii\helpers\Html;

/** @var \yii\web\View $this */

$this->title = 'Операции';
$where = null;
$id = Yii::$app->request->get('id');
if (!is_null($id)) $where = ['wallet_id' => $id];
$tid = Yii::$app->request->get('tid');
if (!is_null($tid)) $where = ['transaction_id' => $tid];
$cid = Yii::$app->request->get('cid');
$walletList = \common\models\piramida\Wallet::find()->where(['currency_id' => $cid])->select('id')->column();
if (!is_null($cid)) $where = ['wallet_id' => $walletList];


\avatar\assets\Notify::register($this);
\avatar\assets\Clipboard::register($this);
$this->registerJS(<<<JS

var clipboard = new Clipboard('.buttonCopy');
clipboard.on('success', function(e) {

    new Noty({
        timeout: 1000,
        theme: 'sunset',
        type: 'success',
        layout: 'bottomLeft',
        text: 'Скопировано'
    }).show();

});

JS
);
\avatar\assets\Paralax::register($this);

?>
<div class="parallax-window" data-parallax="scroll"
     data-image-src="/images/controller/coin-explorer/index/blockchain-reaction-american-libraries.jpg"
     style="min-height: 300px; background: transparent;">
    <h1 class="text-center" style="padding-top: 100px;margin-top: 0px;"><?= Html::encode($this->title) ?></h1>

</div>
<div class="container">
    <div class="col-lg-12">
        <p>
            <a href="/coin-explorer/index" class="btn btn-default">Проводник</a>
            <a href="/coin-explorer/currency" class="btn btn-default">Список монет</a>
            <a href="/coin-explorer/transaction" class="btn btn-default">Список всех транзакций</a>
            <a href="/coin-explorer/operation" class="btn btn-default">Список всех операций</a>
            <a href="/coin-explorer/wallet" class="btn btn-default">Список всех кошельков</a>
        </p>
        <?php \yii\widgets\Pjax::begin(); ?>
        <?php
        $this->registerJS(<<<JS
$('[data-toggle="tooltip"]').tooltip();



JS
        );
        $sort = new \yii\data\Sort([
            'defaultOrder' => ['datetime' => SORT_DESC],
        ]);
        ?>
        <?= \yii\grid\GridView::widget([
            'dataProvider' => new \yii\data\ActiveDataProvider([
                'query'      => \common\models\piramida\Operation::find()->where($where)
                ,
                'pagination' => [
                    'pageSize' => 20,
                ],
                'sort'       => $sort,
            ]),
            'tableOptions' => [
                'class' => 'table table-striped table-hover',
            ],
            'rowOptions'   => function ($item) {
                $data = [
                    'data'  => ['id' => $item['id']],
                    'role'  => 'button',
                    'class' => 'rowTable',
                ];
                return $data;
            },
            'columns'      => [
                [
                    'header'    => 'Адрес',
                    'attribute' => 'address',
                    'content'   => function (\common\models\piramida\Operation $item) {
                        $address = $item->getAddress();
                        $options = [
                            'data'  => [
                                'toggle'         => 'tooltip',
                                'clipboard-text' => $address,
                            ],
                            'class' => 'buttonCopy',
                            'title' => 'Нажми чтобы скопировать',
                        ];

                        return Html::tag('code', $item->getAddressShort(), $options);
                    },
                ],
                [
                    'header'  => 'Монета',
                    'content' => function ($item) {
                        $from = \yii\helpers\ArrayHelper::getValue($item, 'wallet_id', '');
                        $wallet = \common\models\piramida\Wallet::findOne($from);
                        $currencyInt = $wallet->getCurrency();

                        if (is_null($currencyInt)) return '';
                        $link = \common\models\avatar\CurrencyLink::findOne(['currency_int_id' => $currencyInt->id]);
                        $currencyExt = \common\models\avatar\Currency::findOne($link->currency_ext_id);


                        return Html::img(\common\widgets\FileUpload7\FileUpload::getFile($currencyExt->image, 'crop'), [
                            'class'  => "img-circle",
                            'width'  => 50,
                            'height' => 50,
                            'style'  => 'margin-bottom: 0px;',
                            'data'  => [
                                'toggle'         => 'tooltip',
                            ],
                            'title' => $currencyExt->title . ' ['.$currencyExt->code.']',
                        ]);
                    },
                ],
                [
                    'header'    => 'Кошелек',
                    'attribute' => 'wallet_id',
                    'content'   => function (\common\models\piramida\Operation $item) {
                        $v = \yii\helpers\ArrayHelper::getValue($item, 'wallet_id', 0);
                        $w = new \common\models\piramida\Wallet(['id' => $v]);
                        $address = $w->getAddress();
                        $options = [
                            'data'  => [
                                'toggle'         => 'tooltip',
                                'clipboard-text' => $address,
                            ],
                            'class' => 'buttonCopy',
                            'title' => 'Нажми чтобы скопировать',
                        ];

                        return Html::tag('code', $w->getAddressShort(), $options);
                    },
                ],
                [
                    'header'    => 'Транзакция',
                    'attribute' => 'transaction_id',
                    'content'   => function ($item) {
                        $v = \yii\helpers\ArrayHelper::getValue($item, 'transaction_id');
                        if (is_null($v)) return '';
                        $w = new \common\models\piramida\Transaction(['id' => $v]);
                        $address = $w->getAddress();
                        $options = [
                            'data'  => [
                                'toggle'         => 'tooltip',
                                'clipboard-text' => $address,
                            ],
                            'class' => 'buttonCopy',
                            'title' => 'Нажми чтобы скопировать',
                        ];

                        return Html::tag('code', $w->getAddressShort(), $options);
                    },
                ],
                [
                    'header'    => 'Тип',
                    'attribute' => 'type',
                    'content'   => function ($item) {
                        $v = \yii\helpers\ArrayHelper::getValue($item, 'type', 0);

                        if ($v == \common\models\piramida\Operation::TYPE_BURN) return Html::tag('span', 'Сжигание', ['class' => 'label label-danger']);
                        if ($v == \common\models\piramida\Operation::TYPE_OUT) return Html::tag('span', 'Расход', ['class' => 'label label-warning']);
                        if ($v == \common\models\piramida\Operation::TYPE_IN) return Html::tag('span', 'Приход', ['class' => 'label label-info']);
                        if ($v == \common\models\piramida\Operation::TYPE_EMISSION) return Html::tag('span', 'Эмиссия', ['class' => 'label label-success']);

                        return '';
                    },
                ],
                [
                    'header'         => 'Время',
                    'attribute'      => 'datetime',
                    'contentOptions' => ['nowrap' => 'nowrap'],
                    'format'         => 'datetime',
                ],
                [
                    'header'         => 'Атомов до операции',
                    'attribute'      => 'before',
                    'headerOptions'  => ['class' => 'text-right'],
                    'contentOptions' => ['class' => 'text-right'],
                ],
                [
                    'header'         => 'Атомов после операции',
                    'attribute'      => 'after',
                    'headerOptions'  => ['class' => 'text-right'],
                    'contentOptions' => ['class' => 'text-right'],
                ],
                [
                    'header'         => 'Атомов',
                    'attribute'      => 'amount',
                    'headerOptions'  => ['class' => 'text-right'],
                    'contentOptions' => ['class' => 'text-right'],
                ],
                [
                    'header'    => 'Коментарий',
                    'attribute' => 'comment',
                ],
                [
                    'header'    => 'Hash',
                    'format' => 'html',
                    'attribute' => 'hash',
                    'content' => function ($item) {
                        $id = $item['hash'];
                        $last4 = substr($id, strlen($id) - 8);
                        $options = [
                            'data'  => [
                                'toggle'         => 'tooltip',
                                'clipboard-text' => $item['hash'],
                            ],
                            'class' => 'buttonCopy',
                            'title' => 'Нажми чтобы скопировать',
                        ];
                        return Html::tag('code', '...' . $last4, $options);
                    },
                ],
            ],
        ]) ?>
        <?php \yii\widgets\Pjax::end(); ?>

    </div>
</div>



