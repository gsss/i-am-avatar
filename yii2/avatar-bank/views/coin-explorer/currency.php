<?php
/**
 * Created by PhpStorm.
 * User: Metatron
 * Date: 22.12.2018
 * Time: 5:44
 */
use yii\helpers\Html;

/** @var \yii\web\View $this */

$this->title = 'Монеты';

\avatar\assets\Notify::register($this);
\avatar\assets\Clipboard::register($this);

$this->registerJS(<<<JS

var clipboard = new Clipboard('.buttonCopy');
clipboard.on('success', function(e) {

    new Noty({
        timeout: 1000,
        theme: 'sunset',
        type: 'success',
        layout: 'bottomLeft',
        text: 'Скопировано'
    }).show();

});

JS
);
\avatar\assets\Paralax::register($this);

?>
<div class="parallax-window" data-parallax="scroll"
     data-image-src="/images/controller/coin-explorer/index/blockchain-reaction-american-libraries.jpg"
     style="min-height: 300px; background: transparent;">
    <h1 class="text-center" style="padding-top: 100px;margin-top: 0px;"><?= Html::encode($this->title) ?></h1>

</div>
<div class="container">
    <div class="col-lg-12">
        <p>
            <a href="/coin-explorer/index" class="btn btn-default">Проводник</a>
            <a href="/coin-explorer/currency" class="btn btn-default">Список монет</a>
            <a href="/coin-explorer/transaction" class="btn btn-default">Список всех транзакций</a>
            <a href="/coin-explorer/operation" class="btn btn-default">Список всех операций</a>
            <a href="/coin-explorer/wallet" class="btn btn-default">Список всех кошельков</a>
        </p>
        <?php \yii\widgets\Pjax::begin(); ?>
        <?php
        $this->registerJS(<<<JS
$('[data-toggle="tooltip"]').tooltip();

$('.buttonView').click(function(e) {
    e.preventDefault();
    e.stopPropagation();
    window.location = '/coin-explorer/currency-item' + '?' + 'id' + '=' + $(this).data('id');
});
$('.buttonOp').click(function(e) {
    e.preventDefault();
    e.stopPropagation();
    window.location = '/coin-explorer/operation' + '?' + 'cid' + '=' + $(this).data('id');
});


JS
        );

        ?>
        <?= \yii\grid\GridView::widget([
            'dataProvider' => new \yii\data\ActiveDataProvider([
                'query'      => \common\models\piramida\Currency::find()
                ,
                'pagination' => [
                    'pageSize' => 20,
                ],
            ]),
            'tableOptions' => [
                'class' => 'table table-striped table-hover',
            ],
            'rowOptions'   => function ($item) {
                $data = [
                    'data'  => ['id' => $item['id']],
                    'role'  => 'button',
                    'class' => 'rowTable',
                ];
                return $data;
            },
            'columns'      => [
                'id',
                [
                    'header'    => 'Наименование',
                    'attribute' => 'name',
                ],
                [
                    'header'    => 'Код',
                    'attribute' => 'code',
                ],
                [
                    'header'    => 'Адрес',
                    'attribute' => 'address',
                    'content'   => function (\common\models\piramida\Currency $item) {
                        $address = $item->getAddress();
                        $options = [
                            'data'  => [
                                'toggle'         => 'tooltip',
                                'clipboard-text' => $address,
                            ],
                            'class' => 'buttonCopy',
                            'title' => 'Нажми чтобы скопировать',
                        ];

                        return Html::tag('code', $address, $options);
                    },
                ],
                [
                    'header'    => 'Десятичных знаков',
                    'attribute' => 'decimals',
                ],
                [
                    'header'         => 'Атомов в обороте',
                    'attribute'      => 'amount',
                    'headerOptions'  => ['class' => 'text-right'],
                    'contentOptions' => ['class' => 'text-right'],
                ],
                [
                    'header'         => 'Монет в обороте',
                    'attribute'      => 'amount',
                    'headerOptions'  => ['class' => 'text-right'],
                    'contentOptions' => ['class' => 'text-right'],
                    'content'        => function (\common\models\piramida\Currency $item) {
                        return (int)($item->amount / pow(10, $item->decimals));
                    },
                ],

                [
                    'header'  => 'Просмотр',
                    'content' => function ($item) {
                        return Html::button('Просмотр', ['class' => 'btn btn-default btn-xs buttonView', 'data' => ['id' => $item['id']]]);
                    },
                ],
                [
                    'header'  => 'Операции',
                    'content' => function ($item) {
                        return Html::button('Операции', ['class' => 'btn btn-default btn-xs buttonOp', 'data' => ['id' => $item['id']]]);
                    },
                ],
            ],
        ]) ?>
        <?php \yii\widgets\Pjax::end(); ?>

    </div>
</div>



