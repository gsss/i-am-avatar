<?php
/**
 * Created by PhpStorm.
 * User: Metatron
 * Date: 22.12.2018
 * Time: 5:44
 */
use yii\helpers\Html;

/** @var \yii\web\View $this */

$this->title = 'Транзакции';
\avatar\assets\Notify::register($this);
\avatar\assets\Clipboard::register($this);
$this->registerJS(<<<JS

var clipboard = new Clipboard('.buttonCopy');
clipboard.on('success', function(e) {

    new Noty({
        timeout: 1000,
        theme: 'sunset',
        type: 'success',
        layout: 'bottomLeft',
        text: 'Скопировано'
    }).show();

});

JS
);
\avatar\assets\Paralax::register($this);

?>
<div class="parallax-window" data-parallax="scroll"
     data-image-src="/images/controller/coin-explorer/index/blockchain-reaction-american-libraries.jpg"
     style="min-height: 300px; background: transparent;">
    <h1 class="text-center" style="padding-top: 100px;margin-top: 0px;"><?= Html::encode($this->title) ?></h1>

</div>
<div class="container">
    <div class="col-lg-12">
        <p>
            <a href="/coin-explorer/index" class="btn btn-default">Проводник</a>
            <a href="/coin-explorer/currency" class="btn btn-default">Список монет</a>
            <a href="/coin-explorer/transaction" class="btn btn-default">Список всех транзакций</a>
            <a href="/coin-explorer/operation" class="btn btn-default">Список всех операций</a>
            <a href="/coin-explorer/wallet" class="btn btn-default">Список всех кошельков</a>
        </p>
        <?php \yii\widgets\Pjax::begin(); ?>
        <?php
        $this->registerJS(<<<JS
$('[data-toggle="tooltip"]').tooltip();


$('.buttonView').click(function(e) {
    e.preventDefault();
    e.stopPropagation();
    window.location = '/coin-explorer/operation' + '?' + 'tid' + '=' + $(this).data('id');
});
JS
        );
        $sort = new \yii\data\Sort([
            'defaultOrder' => ['datetime' => SORT_DESC],
        ]);
        ?>
        <?= \yii\grid\GridView::widget([
            'dataProvider' => new \yii\data\ActiveDataProvider([
                'query'      => \common\models\piramida\Transaction::find()
                ,
                'pagination' => [
                    'pageSize' => 20,
                ],
                'sort'       => $sort,
            ]),
            'tableOptions' => [
                'class' => 'table table-striped table-hover',
            ],
            'rowOptions'   => function ($item) {
                $data = [
                    'data'  => ['id' => $item['id']],
                    'role'  => 'button',
                    'class' => 'rowTable',
                ];
                return $data;
            },
            'columns'      => [
                [
                    'header'    => 'Адрес',
                    'attribute' => 'address',
                    'content'   => function (\common\models\piramida\Transaction $item) {
                        $address = $item->getAddress();
                        $options = [
                            'data'  => [
                                'toggle'         => 'tooltip',
                                'clipboard-text' => $address,
                            ],
                            'class' => 'buttonCopy',
                            'title' => 'Нажми чтобы скопировать',
                        ];

                        return Html::tag('code', $item->getAddressShort(), $options);
                    },
                ],
                [
                    'header'  => 'Монета',
                    'content' => function ($item) {
                        $from = \yii\helpers\ArrayHelper::getValue($item, 'from', '');
                        $wallet = \common\models\piramida\Wallet::findOne($from);
                        $currencyInt = $wallet->getCurrency();

                        if (is_null($currencyInt)) return '';
                        $link = \common\models\avatar\CurrencyLink::findOne(['currency_int_id' => $currencyInt->id]);
                        $currencyExt = \common\models\avatar\Currency::findOne($link->currency_ext_id);


                        return Html::img(\common\widgets\FileUpload7\FileUpload::getFile($currencyExt->image, 'crop'), [
                            'class'  => "img-circle",
                            'width'  => 50,
                            'height' => 50,
                            'style'  => 'margin-bottom: 0px;',
                            'data'  => [
                                'toggle'         => 'tooltip',
                            ],
                            'title' => $currencyExt->title . ' ['.$currencyExt->code.']',
                        ]);
                    },
                ],
                [
                    'header'    => 'Откуда',
                    'attribute' => 'from',
                    'content'   => function (\common\models\piramida\Transaction $item) {
                        $v = \yii\helpers\ArrayHelper::getValue($item, 'from', 0);
                        $w = new \common\models\piramida\Wallet(['id' => $v]);
                        $address = $w->getAddress();
                        $options = [
                            'data'  => [
                                'toggle'         => 'tooltip',
                                'clipboard-text' => $address,
                            ],
                            'class' => 'buttonCopy',
                            'title' => 'Нажми чтобы скопировать',
                        ];

                        return Html::tag('code', $w->getAddressShort(), $options);
                    },
                ],
                [
                    'header'    => 'Куда',
                    'attribute' => 'to',
                    'content'   => function (\common\models\piramida\Transaction $item) {
                        $v = \yii\helpers\ArrayHelper::getValue($item, 'to', 0);
                        $w = new \common\models\piramida\Wallet(['id' => $v]);
                        $address = $w->getAddress();
                        $options = [
                            'data'  => [
                                'toggle'         => 'tooltip',
                                'clipboard-text' => $address,
                            ],
                            'class' => 'buttonCopy',
                            'title' => 'Нажми чтобы скопировать',
                        ];

                        return Html::tag('code', $w->getAddressShort(), $options);
                    },
                ],
                [
                    'header'         => 'Момент',
                    'attribute'      => 'datetime',
                    'contentOptions' => ['nowrap' => 'nowrap'],
                    'format'         => 'datetime',
                ],
                [
                    'header'         => 'Атомов',
                    'attribute'      => 'amount',
                    'headerOptions'  => ['class' => 'text-right'],
                    'contentOptions' => ['class' => 'text-right'],
                ],
                [
                    'header'    => 'Коментарий',
                    'attribute' => 'comment',
                ],
                [
                    'header'    => 'Hash',
                    'format' => 'html',
                    'attribute' => 'hash',
                    'content' => function ($item) {
                        $id = $item['hash'];
                        $last4 = substr($id, strlen($id) - 8);
                        $options = [
                            'data'  => [
                                'toggle'         => 'tooltip',
                                'clipboard-text' => $item['hash'],
                            ],
                            'class' => 'buttonCopy',
                            'title' => 'Нажми чтобы скопировать',
                        ];
                        return Html::tag('code', '...' . $last4, $options) . Html::tag('span', Html::tag('i',null, ['class' => 'glyphicon glyphicon-ok']), ['class' => 'label label-success'] );
                    },
                ],

                [
                    'header'  => 'Операции',
                    'content' => function ($item) {
                        return Html::button('Операции', ['class' => 'btn btn-default btn-xs buttonView', 'data' => ['id' => $item['id']]]);
                    },
                ],
            ],
        ]) ?>
        <?php \yii\widgets\Pjax::end(); ?>

    </div>
</div>



