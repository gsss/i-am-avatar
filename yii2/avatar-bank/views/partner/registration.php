<?php
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this \yii\web\View */
/* @var $model \avatar\models\forms\QrEnter */

$this->title = 'Регистрация партнера';

?>

<div class="container" style="padding-bottom: 70px;">
    <div class="col-lg-12">
       <h1 class="page-header text-center"><?= $this->title ?></h1>
        <?php if (Yii::$app->session->hasFlash('form')) {?>
            <p class="alert alert-success">Успешно. Зайдите на почту и подтвердите свой Email.</p>
            <p><a href="/partner/choose-pay">Перейти к оплате</a></p>
        <?php } else { ?>
            <div class="row">
                <p class="text-center">
                    <img src="/images/controller/site/index/2016-10-22_01-23-38.png" style="width: 100%; max-width: 500px;"
                        >
                </p>
            </div>
            <div class="row">
                <div class="col-lg-4 col-lg-offset-4">
                    <?php $form = \yii\bootstrap\ActiveForm::begin([]) ?>
                    <?= $form->field($model, 'email', ['inputOptions' => ['placeholder' => 'email']])->label('Почта', ['class' => 'hide']) ?>
                    <?= $form->field($model, 'password1', ['inputOptions' => ['placeholder' => 'Пароль']])->label('Почта', ['class' => 'hide'])->passwordInput() ?>
                    <?= $form->field($model, 'password2', ['inputOptions' => ['placeholder' => 'Пароль еще раз']])->label('Почта', ['class' => 'hide'])->passwordInput() ?>
                    <hr>
                    <?= Html::submitButton('Зарегистрироваться', ['class' => 'btn btn-success btn-lg', 'style' => 'width: 100%']) ?>
                    <?php \yii\bootstrap\ActiveForm::end() ?>
                </div>
            </div>
        <?php } ?>
    </div>
</div>


