<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;
use yii\db\Query;

/**
 * Форма оплаты
 */

/* @var $this yii\web\View */
/* @var $request \common\models\piramida\InRequest */

$this->title = 'Оплата заказа #' . $request->id;

$billing = $request->getBilling();
$sid = $billing->source_id;
/** @var \common\models\PaySystemConfig $payConfig */
$payConfig = \common\models\PaySystemConfig::findOne(['paysystem_id' => $sid, 'parent_id' => 2]);
/** @var \common\models\piramida\WalletSourceInterface $source */
$source = $payConfig->getClass();
?>
<div class="container">
    <div class="page-header text-center">
        <h1>
            <?= Html::encode($this->title) ?>
        </h1>
    </div>

    <div class="row">
        <div class="col-lg-8 col-lg-offset-2">
            <?= $source->getForm($billing, 'Вклад #' . $request->id, 'Аватар Банк', $payConfig->config, [
                'action'  => 'avatarPartner.' . $request->id,
                'request' => $request,
            ]) ?>
        </div>
    </div>
</div>
