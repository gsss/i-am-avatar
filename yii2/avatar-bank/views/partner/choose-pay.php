<?php
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this \yii\web\View */
/* @var $model \avatar\models\forms\QrEnter */

$this->title = 'Выбор системы оплаты';

?>

<div class="container" style="padding-bottom: 70px;">
    <div class="col-lg-12">
        <h1 class="page-header text-center">
            <?= $this->title ?>
        </h1>

        <?php
        $this->registerJs(<<<JS
    $('.paymentType').click(function () {
        $('#paymentType').attr('value', $(this).data('value'));
        if (!$(this).hasClass('active')) {
            $('.paymentType').removeClass('active');
            $(this).addClass('active');
        }
    });
JS
        );
        ?>
        <h4 class="page-header">С помощью платежной системы</h4>

        <p>
            <?php $this->registerJs("$('[data-toggle=\"popup\"]').popover()"); ?>
            <img
                width="200"
                style="margin: 20px 0px 20px 64px"
                src="/images/controller/partner/pay/security-seal-2x.png"
                data-toggle="popup"
                data-html="enabled"
                data-placement="bottom"
                data-trigger="hover"
                class="gsssPopup"
                data-content="<b>Вся информация на этой странице защищена SSL. <br> Почему это так важно?</b><br><br>SSL (англ. Secure Sockets Layer — уровень защищенных сокетов) — это технология, которая обеспечивает безопасность обмена данными через интернет. Эта ссылка гарантирует, что все данные, передаваемые между веб-сервером и браузерами, защищены и останутся конфиденциальными. <br><br>SSL является отраслевым стандартом и используется миллионами сайтов для защиты онлайн-транзакций с клиентами."/>
        </p>

        <input type="hidden" value="" id="paymentType">
        <?php /** @var \common\models\PaySystem $paySystem */ ?>
        <?php
        $this->registerJs(<<<JS
$('.rowTable').click(function() {
    $('#paymentType').val($(this).data('id'));
    $(this).parent().find('.js-radio').prop('checked','');
    $(this).find('.js-radio').prop('checked', 'checked');
});
$('.buttonSuccess').click(function(e) {
    if ($('#paymentType').val() == '') {
        alert('нужно выбрать платежную систему');
        return;
    }
    ajaxJson({
        url: '/partner/choose-pay-ajax',
        data: {
            paymentType: $('#paymentType').val(),
            product: 1
        },
        success: function(ret) {
            window.location = '/partner/buy?id=' + ret.id;
        }
    });
});
JS
        );
        ?>
        <?= \yii\grid\GridView::widget([
            'showHeader'   => false,
            'summary'      => '',
            'dataProvider' => new \yii\data\ActiveDataProvider([
                'query' => \common\models\PaySystem::find()
                    ->select([
                        'paysystems_config.id',
                        'paysystems.code',
                        'paysystems.title',
                        'paysystems.image',
                    ])
                    ->innerJoin('paysystems_config', 'paysystems_config.paysystem_id = paysystems.id')
                    ->andWhere(['parent_id' => 2])
            ]),
            'tableOptions' => [
                'class' => 'table table-hover table-striped',
            ],
            'rowOptions'   => function ($item) {
                $data = [
                    'data'  => ['id' => $item['id']],
                    'role'  => 'button',
                    'class' => 'rowTable'
                ];

                return $data;
            },
            'columns'      => [
                [
                    'content' => function ($item) {
                        return Html::radio(null, false, [
                            'value' => \yii\helpers\ArrayHelper::getValue($item, 'id'),
                            'class' => 'js-radio',
                        ]);
                    }
                ],
                [
                    'attribute' => 'image',
                    'format'    => ['image', ['width' => 100, 'class' => 'thumbnail', 'style' => 'margin-bottom:0px;']]
                ],
                'title',
            ]
        ]) ?>

        <hr>
        <button class="btn btn-success buttonSuccess">Оплатить</button>
    </div>
</div>


