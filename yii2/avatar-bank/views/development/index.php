<?php
use common\services\UsersInCache;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;

/* @var $this yii\web\View */

$this->title = 'Разработка';


$servers = \common\models\Server::find()->all();
?>
<div class="container">
    <div class="col-lg-12">
        <h1 class="page-header text-center"><?= Html::encode($this->title) ?></h1>

        <table class="table table-striped table-hover">
            <tr>
                <th></th>
                <?php foreach ($servers as $server) { ?>
                    <th><?= $server['name'] ?></th>
                <?php } ?>
            </tr>
            <tr>
                <td>Активность</td>
                <?php foreach ($servers as $server) { ?>
                    <td>
                        <?php
                        $status = \common\models\statistic\PingStatus::find()
                            ->where(['server_id' => $server->id])
                            ->orderBy(['time' => SORT_DESC])
                            ->select('status')
                            ->scalar();
                        ?>
                        <?php if ($status) { ?>
                            <?= Html::tag('span','Да', ['class' => 'label label-success']) ?>
                        <?php } else { ?>
                            <?= Html::tag('span','Нет', ['class' => 'label label-danger'])  ?>
                        <?php } ?>
                    </td>
                <?php } ?>
            </tr>
            <tr>
                <?php $param = 'ip'; ?>
                <td><?= $param ?></td>
                <?php foreach ($servers as $server) { ?>
                    <td>
                        <?php if (isset($server[$param])) { ?>
                            <code><?= $server[$param] ?></code>
                        <?php } ?>
                    </td>
                <?php } ?>
            </tr>
            <tr>
                <?php $param = 'url'; ?>
                <td><?= $param ?></td>
                <?php foreach ($servers as $server) { ?>
                    <td>
                        <?php if (isset($server[$param])) { ?>
                            <?= Html::a($server[$param], $server[$param], ['target' => '_blank']) ?>
                        <?php } ?>
                    </td>
                <?php } ?>
            </tr>
            <tr>
                <?php $param = 'hosting'; ?>
                <td><?= $param ?></td>
                <?php foreach ($servers as $server) { ?>
                    <td>
                        <?php if (isset($server[$param])) { ?>
                            <?= $server[$param] ?>
                        <?php } ?>
                    </td>
                <?php } ?>
            </tr>
            <tr>
                <?php $param = 'config'; ?>
                <td><?= $param ?></td>
                <?php foreach ($servers as $server) { ?>
                    <td>
                        <?php if (isset($server[$param])) { ?>
                            <?= join('<br>', \yii\helpers\Json::decode($server[$param])) ?>
                        <?php } ?>
                    </td>
                <?php } ?>
            </tr>
            <tr>
                <?php $param = 'is_php'; ?>
                <td><?= $param ?></td>
                <?php foreach ($servers as $server) { ?>
                    <td>
                        <?php if (isset($server[$param])) { ?>
                            <?= ($server[$param]) ? Html::tag('span','Да', ['class' => 'label label-success']) : Html::tag('span','Нет', ['class' => 'label label-default']) ?>
                        <?php } else { ?>
                            <?= Html::tag('span','Нет', ['class' => 'label label-default']) ?>
                        <?php } ?>
                    </td>
                <?php } ?>
            </tr>
            <tr>
                <?php $param = 'is_mysql'; ?>
                <td><?= $param ?></td>
                <?php foreach ($servers as $server) { ?>
                    <td>
                        <?php if (isset($server[$param])) { ?>
                            <?= ($server[$param]) ? Html::tag('span','Да', ['class' => 'label label-success']) : Html::tag('span','Нет', ['class' => 'label label-default']) ?>
                        <?php } else { ?>
                            <?= Html::tag('span','Нет', ['class' => 'label label-default']) ?>
                        <?php } ?>
                    </td>
                <?php } ?>
            </tr>
            <tr>
                <?php $param = 'api'; ?>
                <td><?= $param ?></td>
                <?php foreach ($servers as $server) { ?>
                    <td>
                        <?php if (isset($server[$param])) { ?>
                            <?= Html::a($server[$param], $server[$param], ['target' => '_blank']) ?>
                        <?php } ?>
                    </td>
                <?php } ?>
            </tr>
            <tr>
                <?php $param = 'api'; ?>
                <td>log</td>
                <?php foreach ($servers as $server) { ?>
                    <td>
                        <?php if (isset($server[$param])) { ?>
                            <?php $url = $server[$param] . '/site/ethereum-log'; ?>
                            <?= Html::a(Html::tag('span', 'LOG', ['class' => 'label label-info']), $url, ['target' => '_blank']) ?>
                        <?php } ?>
                    </td>
                <?php } ?>
            </tr>
            <tr>
                <?php $param = 'used'; ?>
                <td><?= $param ?></td>
                <?php foreach ($servers as $server) { ?>
                    <td>
                        <p><code id="pieSizeUsed2_<?= $server['id'] ?>"></code></p>
                    </td>
                <?php } ?>
            </tr>
            <tr>
                <?php $param = 'disk'; ?>
                <td><?= $param ?></td>
                <?php foreach ($servers as $server) { ?>
                    <td>
                        <p><code id="pieSize2_<?= $server['id'] ?>"></code></p>
                    </td>
                <?php } ?>
            </tr>
            <tr>
                <?php $param = 'api'; ?>
                <td><?= $param ?></td>
                <?php foreach ($servers as $server) { ?>
                    <td>
                        <div id="pieSize_<?= $server['id'] ?>" style="height: 200px;width: 200px;">
                    </td>
                <?php } ?>
            </tr>

        </table>
<?php
$ids = \common\models\Server::find()->select(['id'])->column();
$idsJson = \yii\helpers\Json::encode($ids);

$this->registerJs(<<<JS

var i = 0;
var operationList = {$idsJson};
var operationList2 = {$idsJson};

var listEth = [];
var pie1 = [];

var functionFinish = function() {
    functionCalc2(i2, operationList2);
};

function draw(id, data) {
    console.log([id, data]);
    var elements = [
        {
            name: 'Used',
            y: data.all.Used
        },
        {
            name: 'Avail',
            y: data.all.Avail
        }
    ];
    
    
    $('#pieSize2_' + id).html(formatAsDecimal2(data.all.Size / 1000000000, ',', 2) + ' Gb');
    $('#pieSizeUsed2_' + id).html(formatAsDecimal2(data.all.Used / 1000000000, ',', 2) + ' Gb');
    window.chart = $('#pieSize_' + id).highcharts({
        "chart":{
            "plotBackgroundColor": null,
            "plotBorderWidth": null,
            "plotShadow": false,
            "type": "pie"
        },
        "title": {
            "text": ""
        },
        "tooltip": {
            "pointFormat": "{series.name}: <b>{point.percentage:.1f}%</b>"
        },
        "plotOptions": {
            "pie": {
                "allowPointSelect": true,
                "cursor": "pointer",
                "colors": [
                    '#bf4040', // Занято
                    '#40bf40' // Свободно
                ],
                "dataLabels": {
                    "enabled": true,
                    "format": "<b>{point.name}</b>: {point.percentage:.1f} %",
                    "style": {
                        "color":(Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                    }
                }
            }
        },
        "series": [
            {
                "name": "Size",
                "colorByPoint": true,
                "data": elements
            }
        ]
    });
}

var functionCalc = function(i, arr) {
    var id = arr[i];

    ajaxJson({
        url: '/development/get-size' + '?' + 'id' + '=' + id,
        success: function(ret) {
            i++;
            draw(id, ret.data);
            
            if (i < arr.length) {
                functionCalc(i, arr);
            } else { functionFinish(); }
        },
        error: function(ret) {
            i++;
            if (i < arr.length) {
                functionCalc(i, arr);
            } else { functionFinish(); }
        },
        errorScript: function(ret) {
            i++;
            if (i < arr.length) {
                functionCalc(i, arr);
            } else { functionFinish(); }
        }
    });
};

console.log(operationList);

functionCalc(i, operationList);

function formatAsDecimal2 (val, separator, lessOne)
{
    if (typeof separator == 'undefined') separator = ',';
    if (typeof lessOne == 'undefined') lessOne = 0;
    
    var pStr = '';
    var original = val;
    val = parseInt(val);
    var j = 1;
    var blocks = [];
    var valChacngable = val;
    while (true) {
        if (1000 <= valChacngable) { // 1000 < 1001
            // формирую ведущие нули 
            pStr = vedNullInt(valChacngable % 1000);
            blocks.push(pStr);
        } else { // 1000 > 52
            blocks.push(valChacngable);
        }
        if (valChacngable < 1000) break;
        valChacngable = parseInt(valChacngable / 1000);
        j++;
    }
    blocks = blocks.reverse();
    pStr = blocks.join(',');
    
    
    var oStr = '';
    if (lessOne > 0) {
        var d = original - parseInt(original);
        var i;
        d = d * Math.pow(10, lessOne);
        d = parseInt(d);
        if (d == 0) {
            for (i = 0; i < lessOne; i++) {
                oStr = oStr + '0';
            }
        } else {
            var p;
            // определить склько знаков в числе после заятой
            if (lessOne == 1) {
                oStr = d;
            } else {
                oStr = repeat('0', lessOne - getNums(d));
                oStr += d;
            }
        }
    }

    return pStr + '.' + oStr;
}

/**
* Возвращает число с ведущими нулями
* Например в числе 9 = 009 чифра
*
* @param  d integer
*/
function vedNullInt(d)
{
    var len = getNums(d);
    if (len == 3) return d;
    var prefix = repeat('0', 3 - len);
    
    return prefix + d;
}

/**
* Возвращает кол-во цифр в числе
* Например в числе 150 = 3 чифры
* Например в числе 999 = 3 чифры
* Например в числе 9 = 1 чифра
*
* @param  d integer
*/
function getNums(d)
{
    if (d == 0) return 1;
    var i = 0;
    while(true) {
        p = Math.pow(10, i);
        if (d - p < 0) return i;
        i++;
    }
}

/**
* 
* @param  s string
* @param  n integer
* 
* @returns {string}
*/
function repeat(s, n)
{
    var a = [];
    while(a.length < n){
        a.push(s);
    }
    return a.join('');
}


var i2 = 0;

var functionFinish2 = function() {
};

function draw2(id, data) {
    console.log([id, data]);
    console.log($('#lineServerStatus_' + id));
    var rows = data.rows;
    var newRows = [];
    for(i = 0; i < rows.length; i++)
    {
        var item = rows[i];
        newRows.push({
            x: new Date(item.time * 1000),
            y: item.status
        });
    }
    var server = data.server;
    
    window.chart = $('#lineServerStatus_' + id).highcharts({
        chart: {
            zoomType: "x",
            type     : "spline"
        },
        title : {
            text : server.name
        },
        subtitle : {
           text: server.url
        },
        xAxis: {
            type: "datetime"
        },
        yAxis : [
            {
                title : {
                    text : "Количество"
                }
            }
        ],
        legend : {
            enabled : true
        },
        tooltip : {
            crosshairs : true,
            shared : true
        },
        plotOptions : {
            series : {
                turboThreshold : 0
            }
        },
        series : [
            {
                type : "spline",
                name : 'PING',
                data : newRows
            }
        ]
    });
}

var functionCalc2 = function(i2, arr2) {
    var id = arr2[i2];

    ajaxJson({
        url: '/development/get-status' + '?' + 'id' + '=' + id,
        success: function(ret) {
            i2++;
            draw2(id, ret);

            if (i2 < arr2.length) {
                functionCalc2(i2, arr2);
            } else { functionFinish2(); }
        },
        error: function(ret) {
            i2++;
            if (i2 < arr2.length) {
                functionCalc2(i2, arr2);
            } else { functionFinish2(); }
        },
        errorScript: function(ret) {
            i2++;
            if (i2 < arr2.length) {
                functionCalc2(i2, arr2);
            } else { functionFinish2(); }
        }
    });
};


JS
);

\common\assets\HighCharts\HighChartsAsset::register($this);
?>

        <?php /** @var \common\models\Server $server */?>
        <?php foreach (\common\models\Server::find()->all() as $server) { ?>
            <h2 class="page-header"><?= $server->name ?></h2>
            <div id="lineServerStatus_<?= $server->id ?>" style="width: 100%; height: 200px;"></div>
        <?php } ?>


        <p>
            <a href="https://drive.google.com/file/d/0BzHYNoEyPNTXYW1UM2MtT19jY1U/view?usp=sharing" target="_blank">
                <img src="/images/controller/development/index/AvatarNetwork.png" width="100%"/>
            </a>
        </p>


        <p>
            <a href="https://drive.google.com/file/d/0BzHYNoEyPNTXY0ZzVXdhQk5Iejg/view?usp=sharing" target="_blank">
                <img src="/images/controller/development/index/servers.png" width="100%"/>
            </a>
        </p>
        <p><code>test.avatarnetwork.io</code> - TEST (Public)</p>
        <p><code>d7JN1ZvBolJRS1Oc5zS6.avatarnetwork.io</code> - STAGE</p>
        <p><code>z1iLsBIccBegpAzHSaDi.avatarnetwork.io</code> - TEST</p>
        <p><code>sudo nano /etc/nginx/sites-enabled/avatar-network</code> - конфиг nginx</p>
        <p><code>sudo service nginx restart</code> - перезагрузить nginx</p>
        <p><code>crontab -e</code> - cron <a href="https://losst.ru/nastrojka-cron" target="_blank">Описание</a></p>




        <p>База знаний. Администрирование. Права файлов</p>
        <ul>
            <li><a href="https://losst.ru/komanda-chmod-linux" target="_blank">КОМАНДА CHMOD LINUX</a></li>
            <li><a href="https://sheensay.ru/chown" target="_blank">КОМАНДА CHOWN</a></li>
        </ul>

        <p>База знаний. Администрирование. безопасность</p>
        <ul>
            <li><a href="https://www.kaspersky.ru/blog/https-does-not-mean-safe/19464/" target="_blank">HTTPS не значит «безопасность»</a></li>
            <li><a href="https://ubuntu-favorite-os.blogspot.ru/2011/08/ubuntu.html" target="_blank">Мониторинг в Ubuntu средствами терминала</a></li>
        </ul>

        <p>База знаний. NodeJS</p>
        <ul>
            <li><a href="https://hackerx.ru/mysql-node-js-connect-to-database/" target="_blank">MySQL и Node.js — Подключение к базе данных</a></li>
            <li><a href="https://archakov.im/post/telegram-bot-on-nodejs.html" target="_blank">Node.JS: Делаем своего Telegram бота</a></li>
        </ul>

        <p>База знаний. ZCash</p>
        <ul>
            <li><a href="https://pastebin.com/AJFwhfXp" target="_blank">Zcash RPC Commands</a></li>
            <li><a href="https://github.com/zcash/zcash/wiki/1.0-User-Guide" target="_blank">Zcash 1.0-User-Guide</a></li>
        </ul>
        <p>Такого вида запрос:</p>
        <pre>curl --user xxx:xxx --data-binary '{"jsonrpc":"1.0","id":"curltext","method":"getinfo","params":[]}' -H 'content-type:text/plain;' http://zen.servers.atlantida.io:18231</pre>

        <p>База знаний. MariaDB</p>
        <ul>
            <li><a href="http://idroot.net/linux/install-mariadb-ubuntu-16-04/" target="_blank">How To Install MariaDB on Ubuntu 16.04</a></li>
        </ul>

        <p>База знаний. HTML</p>
        <ul>
            <li><a href="http://plentz.github.io/jquery-maskmoney/" target="_blank">Jquery-maskMoney</a></li>
        </ul>

        <p>База знаний. Ethereum</p>
        <ul>
            <li><a href="http://ethereum-php.org" target="_blank">Ethereum PHP</a></li>
            <li><a href="https://www.ethereum.org/cli" target="_blank">Инсталяция клиента GETH</a></li>
            <li><a href="https://habrahabr.ru/post/336770/" target="_blank">Погружение в разработку на Ethereum. Часть 2: Web3.js и газ</a></li>
            <li><a href="https://github.com/ethereum/wiki/wiki/JavaScript-API" target="_blank">Web3 JavaScript app API for 0.2x.x</a></li>
            <li><a href="http://web3js.readthedocs.io/en/1.0/" target="_blank">web3.js - Ethereum JavaScript API</a></li>
            <li><a href="https://ethtools.com" target="_blank">https://ethtools.com</a></li>
            <li><a href="https://dev.oraclize.it/" target="_blank">https://dev.oraclize.it/</a></li>
            <li><a href="https://consensys.github.io/smart-contract-best-practices/recommendations/" target="_blank">Recommendations for Smart Contract Security in Solidity</a></li>
        </ul>

        <p>База знаний. Smart Contracts</p>
        <ul>
            <li><a href="https://github.com/bitclave/crowdsale" target="_blank">crowdsale</a></li>
            <li><a href="https://github.com/bitclave/Multiownable" target="_blank">Multiownable</a></li>
            <li><a href="https://habrahabr.ru/post/342200/.com[iz-pesochnitsy]-obnovlyaemye-smart-kontrak">Обновляемые смарт-контракты Ethereum</a></li>
        </ul>

        <p>База знаний. Ethereum Classic</p>
        <ul>
            <li><a href="http://mewapi.epool.io/" target="_blank">http://mewapi.epool.io/</a></li>
        </ul>

        <p>База знаний. BitShares</p>
        <ul>
            <li><a href="https://github.com/u-transnet/codebase/wiki/%D0%A0%D0%B0%D0%B7%D0%B2%D0%B5%D1%80%D1%82%D0%BA%D0%B0-witness-node-BitShares-%D0%B8-CLI-Wallet-%D0%BD%D0%B0-linux-%D0%BC%D0%B0%D1%88%D0%B8%D0%BD%D0%B5-%D0%B8%D0%B7-%D0%B8%D1%81%D1%85%D0%BE%D0%B4%D0%BD%D0%B8%D0%BA%D0%BE%D0%B2">Развертка witness node BitShares и CLI Wallet на linux машине из исходников</a></li>
        </ul>

        <p>База знаний. ZEN</p>
        <ul>
            <li><a href="https://github.com/ZencashOfficial/zencashjs" target="_blank">ZEN</a></li>
        </ul>


        <p><a href="/development-avatar-bank/index">AvatarNetwork</a></p>
        <ul>
            <li><a href="/development-avatar-bank/ar">Интерфейс дополненной реальности в блокчейне</a></li>
            <li><a href="/development-avatar-bank/install-eth">Инсталяция сервера ETH</a></li>
            <li><a href="/development-avatar-bank/subscribe-news">Рассылка дайджеста новостей</a></li>
            <li><a href="/development-avatar-bank/company-customize">Брендирование компаний</a></li>
        </ul>

        <p>ETH</p>
        <ul>

            <li><a href="/development-avatar-bank/eth-klaster">ETH кластер</a></li>
            <li><a href="/development-avatar-bank/eth-geth">Geth - интерфейс командной строки go-ethereum</a></li>
            <li><a href="/development-avatar-bank/eth-import-chain">ETH. Импортирование цепи</a></li>
            <li><a href="/development-avatar-bank/eth-contracts">ETH. Контракты</a></li>

        </ul>
        <p>API</p>
        <ul>
            <li><a href="/development-avatar-bank/api">API</a></li>
            <li><a href="/development-avatar-bank/api2">API2</a></li>
        </ul>
        <p>ICO</p>
        <ul>    <li><a href="/development-avatar-bank/ico">ICO</a></li>

            <ul>
                <li><a href="/development-avatar-bank/ico-item">ICO Страница проекта</a></li>
                <ul>
                    <li><a href="/development-avatar-bank/ico-path1">Маршрут 1</a></li>
                    <li><a href="/development-avatar-bank/ico-path2">Маршрут 2</a></li>
                    <li><a href="/development-avatar-bank/ico-path3">Маршрут 3</a></li>
                    <li><a href="/development-avatar-bank/ico-path4">Маршрут 4</a></li>
                </ul>
            </ul>

            <li><a href="/development-avatar-bank/langauages">Языки</a></li>
            <li>Страницы</li>
            <ul>
                <li><a href="/development-avatar-bank/page-cabinet-bills-index">Страница счетов</a></li>
                <li><a href="/development-avatar-bank/bills">Счета</a></li>
                <li><a href="/development-avatar-bank/transactions">Транзакции в счете</a></li>
            </ul>

            <li>Карты</li>
            <ul>
                <li><a href="/development-avatar-bank/card-number">Номер карты</a></li>
                <li><a href="/development-avatar-bank/qr-enter">Активация Карты Аватара</a></li>
            </ul>

            <li><a href="/development-avatar-bank/currency">Валюты</a></li>
            <li><a href="/development-avatar-bank/mobile">Мобильное приложение</a></li>
            <li><a href="/development-avatar-bank/auth">Авторизация</a></li>
            <li><a href="/development-avatar-bank/file-permission">Права файлов</a> <a href="https://losst.ru/prava-dostupa-k-fajlam-v-linux" target="_blank">Описание</a></li>

            <li><a href="/development-avatar-bank/cabinet-exchange">Обмен валют</a></li>
            <li><a href="/development-avatar-bank/cabinet-documents">Портфель документов</a></li>
            <li><a href="/development-avatar-bank/merchant">Мерчант</a></li>
            <li><a href="/development-avatar-bank/log">access log</a></li>
            <li><a href="/development-avatar-bank/koop">Кооперативы</a></li>
            <li><a href="/development-avatar-bank/subscribe">Рассылки</a></li>
            <li><b>Пользователи</b></li>
            <ul>
                <li><a href="/development-avatar-bank/user-data-base">Модель организации модели данных</a></li>
                <li><a href="/development-avatar-bank/user-status-list">Статусы</a></li>
            </ul>
            <li><a href="/development-avatar-bank/security">Безопасность</a></li>
            <li><a href="/development-avatar-bank/map">Географическая карта криптоматов</a></li>
            <li><a href="/development-avatar-bank/referals">Реферальная программа</a></li>
            <li><a href="/development-avatar-bank/pasport">Паспорт</a></li>
            <li><a href="/development-avatar-bank/support">Служба поддержки</a></li>
            <li><a href="/development-avatar-bank/tokens">Токены/Жетоны</a></li>
            <li><a href="/development-avatar-bank/monitoring">Мониторинг</a></li>
            <li><a href="/development-avatar-bank/binance">Binance</a></li>
            <li><a href="/development-avatar-bank/tokens-new">Выпуск токена</a></li>

            <li><a href="/development-avatar-bank/path">Пути пользователя в системе и маршруты</a></li>
            <ul>
                <li><a href="/development-avatar-bank/path-registration">Путь регистрации</a></li>
            </ul>
            <li><a href="/development-avatar-bank/pay-systems">Платежные системы</a></li>
            <ul>
                <li><a href="/development-avatar-bank/wallet">Общие сведения о кошельках и счет по умолчанию</a></li>
                <li><a href="/development-avatar-bank/wallet-password">Обработка паролей к кошелькам</a></li>
                <li><a href="/development-avatar-bank/wallet-send">Функция перевода денег</a></li>
                <li><a href="/development-avatar-bank/wallet-eth">Кошелек ETH</a></li>
                <li><a href="/development-avatar-bank/wallet-eth-send">Кошелек ETH. Отправить</a></li>
                <li><a href="/development-avatar-bank/eth-token-zalog">Кошелек Token. Залог</a></li>
                <ul>
                    <li><a href="/development-avatar-bank/wallet-eth-page">Транзакции</a></li>
                </ul>
                <li><a href="/development-avatar-bank/wallet-token">Кошелек Token</a></li>
                <li><a href="/development-avatar-bank/wallet-token-send">Кошелек Token. Отправить</a></li>
            </ul>
            <li><a href="/development-ussr/index">СССР</a></li>
            <ul>
                <li><a href="/development-ussr/minfin">Министерство финансов СССР</a></li>
                <li><a href="/development-ussr/prestuplenie">Регистрация преступления</a></li>
                <li><a href="/development-ussr/minust">Министерство юстиции СССР</a></li>
            </ul>
            <li><a href="/development-avatar-bank/kraud-fanding">Краудфандинг</a></li>
            <li><a href="/development-avatar-bank/gai">ГАИ</a></li>
        </ul>

        <p><a href="/development-sirius-aton/index">Сириус Атон API</a></p>
        <p><a href="/development-sirius-aton/php">Сириус B API</a></p>

        <p><a href="/development-eos/index">EOS</a></p>



        <h2 class="page-header">Контракты</h2>
        <p><a href="/development-contracts/accreditiv">AvatarPay</a></p>
        <p><a href="/development-contracts/documents">Документы</a></p>
        <p><a href="/development-contracts/registration">Регистрация 1.0</a></p>
        <p><a href="/development-contracts/registration-1-1">Регистрация 1.1</a></p>
        <p><a href="/development-contracts/registration-1-2">Регистрация 1.2</a></p>
        <p><a href="/development-contracts/avr-token">AvatarCoin</a></p>
        <p><a href="/development-contracts/tree-identy">Делегирование прав</a></p>
        <p><a href="/development-contracts/franshiza">Франшиза</a> - Контракт для выдачи разрешений продавать продукцию по франшизе</p>
        <p><a href="/development-contracts/koop">Кооператив</a></p>
        <p><a href="/development-contracts/rojd">Регистрация свидетельства о рождении</a></p>
        <p><a href="/development-contracts/zalog">Залог</a></p>

    </div>
</div>



