<?php

/** @var $this \yii\web\View */
/** @var $bill \common\models\avatar\UserBill */
/** @var $model \avatar\models\forms\SendAtom2 */

use common\models\avatar\UserBill;
use yii\helpers\Url;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;

$this->title = $bill->name;

\avatar\assets\Notify::register($this);
\avatar\assets\Clipboard::register($this);

?>
<?php
$this->registerJs(<<<JS

var clipboard = new Clipboard('.buttonCopy');
clipboard.on('success', function(e) {

    new Noty({
        timeout: 1000,
        theme: 'sunset',
        type: 'success',
        layout: 'bottomLeft',
        text: 'Скопировано'
    }).show();

});

JS
);
?>

<div class="container">
    <div class="col-lg-12">
        <h1 class="page-header text-center"><?= Html::encode($this->title) ?></h1>
    </div>

    <div class="col-lg-12">

        <?php
        /** @var  $currencyELXT \common\models\avatar\Currency */
        $currencyELXT = $bill->getCurrencyObject();

        $userWallet = $bill;
        $wallet = \common\models\piramida\Wallet::findOne($userWallet->address);
        $currency = $wallet->getCurrency();
        Yii::$app->session->set('currency.decimals', $currency->decimals);
        ?>

        <p class="text-center">
            <img src="<?= $currencyELXT->image ?>" width="300" style="margin-top: 30px;" class="img-circle">
        </p>
        <?php
        $address = $wallet->getAddress();
        $options = [
            'data'  => [
                'toggle'         => 'tooltip',
                'clipboard-text' => $address,
            ],
            'class' => 'buttonCopy',
            'title' => 'Адрес кошелька. Нажми чтобы скопировать',
        ];
        ?>
        <p class="text-center"><code <?= Html::renderTagAttributes($options) ?>><?= $address ?></code></p>
        <p class="text-center"><?= $currencyELXT->title ?></p>
        <h3 class="page-header text-center"><?= Yii::$app->formatter->asDecimal($wallet->getAmountWithDecimals(), $currency->decimals) ?></h3>
        <p class="text-center"><span class="label label-info"><?= $currencyELXT->code ?></span></p>

        <?php if (Yii::$app->session->hasFlash('form')) { ?>

            <?php $id = Yii::$app->session->getFlash('form'); ?>
            <div class="alert alert-success">
                Успешно отправлено. TXID = <code><?= $id ?></code>
            </div>

            <p>
                <a href="<?= \yii\helpers\Url::to(['cabinet-wallet/item', 'id' => $bill->id]) ?>" class="btn btn-success">Этот кошелек</a>
                <a href="<?= \yii\helpers\Url::to(['cabinet-wallet/index']) ?>" class="btn btn-success">Все кошельки</a>
            </p>

        <?php  } else { ?>


            <div class="row">
                <div class="col-lg-5">

                    <?php $form = \yii\bootstrap\ActiveForm::begin([
                        'options' => ['enctype' => 'multipart/form-data']
                    ]); ?>
                    <?= Html::hiddenInput(Html::getInputName($model, 'id'), $bill->id) ?>
                    <?= $form->field($model, 'user_email')->label('Почта пользователя') ?>
                    <?= $form->field($model, 'amount')->label('Монет для пересылки') ?>
                    <?= $form->field($model, 'comment')->label('Коментарий для отправки') ?>

                    <hr>
                    <div class="form-group">
                        <?= Html::submitButton('Отправить', [
                            'class' => 'btn btn-default',
                            'name'  => 'contact-button',
                            'style' => 'width:100%',
                        ]) ?>
                    </div>
                    <?php \yii\bootstrap\ActiveForm::end(); ?>
                </div>
            </div>

        <?php } ?>
    </div>
</div>
