<?php

use yii\helpers\Html;

$tableList = \console\controllers\LanguagesController::$tableList;

$languages = \common\models\Language::find()->select('code')->column();


/**
 * Выдает имя поля в зависимости от типа именования полей
 *
 * @param $tableConfig
 * @param $columnName
 * @param $language
 * @return string
 */
function getColumnName($tableConfig, $columnName, $language)
{
    if ($tableConfig['name_sufix'] == 2) {
        if ($language == 'ru') {
            return $columnName;
        }
    }

    return $columnName . '_' . $language;
}

?>

<h1>Табличные данные</h1>

<style>
    .pagination {
        margin-top: 20px;
    }
</style>
<div class="row">
    <?php \yii\widgets\Pjax::begin() ?>
    <div class="col-lg-1">
        <?php foreach ($tableList as $table => $config) { ?>
            <p>
                <?php if ($table == Yii::$app->request->get('table')) { ?>
                    <span class="label label-success">
                        <?= $table ?>
                    </span>
                <?php } else { ?>
                    <a href="?table=<?= $table ?>">
                        <?= $table ?>
                    </a>
                <?php } ?>

            </p>
        <?php } ?>
    </div>
    <div class="col-lg-11">
        <?php if (Yii::$app->request->get('table')) { ?>
            <?php
            $table = Yii::$app->request->get('table');
            $this->registerJs(<<<JS
var functionUpdate = function() {
    var b = $(this);
    var language1 = b.parent().parent().parent().data('language');
    var columnName = b.parent().parent().parent().data('column-name');
   
    var value = b.parent().parent().find('input').val();
    b.off('click');
    ajaxJson({
        url: '/languages/update-string-horizontal',
        data: {
            id: b.data('id'),
            table: '{$table}',
            value: value,
            columnName: columnName,
            language: language1
        },
        success: function(ret) {
            b.removeClass('btn-default').removeClass('btn-info').addClass('btn-success');
            b.parent().parent().removeClass('has-error');
            b.parent().parent().addClass('has-success');
            b.click(functionUpdate);
            window.setTimeout(function() {
                b.removeClass('btn-success').addClass('btn-default');
                b.parent().parent().removeClass('has-success');
            }, 1000);
        },
        errorScript: function(ret) {
            alert(ret.data.data);
        }
    });
};
            
$('.buttonUpdate').click(functionUpdate);

$('.buttonHover').on('mouseover', function() {
    var b = $(this);
    b.removeClass('btn-default').addClass('btn-info');
}).on('mouseout', function() {
    var b = $(this);
    b.removeClass('btn-info').removeClass('btn-success').addClass('btn-default');
});
JS
            );

            $config = $tableList[Yii::$app->request->get('table')];
            $class = $config['model'];
            if (isset($config['id'])) {
                $select[] = $config['id'] . ' as id';
            } else {
                $select[] = 'id';
            }
            $columns = ['id'];
            foreach ($languages as $language) {
                foreach ($config['column'] as $columnName => $columnConfig) {
                    $select[] = getColumnName($config, $columnName, $language);
                    $columns[] = [
                        'header'         => strtoupper($language) . ' ' . '(' . $columnName . ')',
                        'attribute'      => getColumnName($config, $columnName, $language),
                        'contentOptions' => [
                            'data' => [
                                'language'   => $language,
                                'column-name' => $columnName,
                            ],
                        ],
                        'content'        => function ($model, $key, $index, \yii\grid\DataColumn $column) {
                            $attribute = $column->attribute;
                            $t = $model[$attribute];
                            $arr = [];
                            $arr[] = Html::input('text', 'message', $t, [
                                'class'   => 'form-control',
                                'data-id' => $model['id'],
                                'style'   => 'width: 200px',
                                'id'      => $attribute . '',
                            ]);
                            $arr[] = Html::tag('span',
                                Html::button(
                                    Html::tag('span', null, [
                                        'class' => 'glyphicon glyphicon-ok',
                                    ])
                                    , [
                                    'class' => 'btn btn-default buttonUpdate buttonHover',
                                    'data'  => [
                                        'id'        => $model['id'],
                                        'attribute' => $attribute,
                                        'table'     => Yii::$app->request->get('table'),
                                    ],
                                ])
                                , [
                                    'class' => 'input-group-btn',
                                ]);

                            $value = Html::tag("div", join("\n", $arr), [
                                'class' => ($t == '') ? 'input-group has-error' : 'input-group',
                                'style' => 'width: 100%',
                            ]);

                            return $value;
                        },
                    ];
                }
            }

            ?>
            <h2>
                <?= $config['title'] ?>
            </h2>
            <?= \yii\grid\GridView::widget([
                'dataProvider' => new \yii\data\ActiveDataProvider([
                    'query' => $class::find()->select($select)->asArray(),
                ]),
                'tableOptions' => [
                    'class' => 'table table-hover table-striped',
                    'style' => 'width: auto;',
                    'id'    => 'tableMessages'
                ],
                'columns'      => $columns,
            ]) ?>
        <?php } ?>
    </div>
    <?php \yii\widgets\Pjax::end() ?>
</div>