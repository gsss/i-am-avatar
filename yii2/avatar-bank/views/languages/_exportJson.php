<?php
/**
 * Created by PhpStorm.
 * User: s.arhangelskiy
 * Date: 26.09.2016
 * Time: 15:51
 */

/** @var \yii\web\View $this */
/** @var array $ids идентификаторы строк для экспорта */
/** @var string $className */
/** @var array $categoryTree */

$ret = [];
$rows = \common\models\language\SourceMessage::find()->where(['in', 'id', $ids])->all();
/** @var \common\models\language\SourceMessage $message */
foreach ($rows as $message) {
    $row = \yii\helpers\ArrayHelper::toArray($message);
    $translations = \yii\helpers\ArrayHelper::map(\common\models\language\Message::find()
        ->where(['id' => $message->id])
        ->select([
            'language',
            'translation',
        ])
        ->asArray()
        ->all(), 'language', 'translation');
    if (!is_null($row['type'])) {
        if ($row['type'] == \common\models\language\Category::TYPE_IMAGE) {
            /**
             * @var array $translations
             * {
             * "cz": "/images/translation/cz/dM47ho2FAdHV2cy7rwmC9rq_D1uRquK5_b.jpg",
             * "de": "/images/translation/de/A2zFQWpjdFjcv2RziCa-x9dLLSekMIOz_b.jpg",
             * "en": "/images/translation/en/aMoHXyc6LqAKm1FmVczr2wjdJGpWWU0J_b.jpg",
             * "es": "/images/translation/es/ixOmbHYYzLM8ZE4rcx5es88gC9mtolgH_b.jpg",
             * "et": "/images/translation/et/qTbv6EJj6N0dx3GsFRCnAk9GMpIL4OQ8_b.jpg",
             * "fr": "/images/translation/fr/djTR2P4v2NqjaqXucrssyfIaUt3oXki__b.jpg",
             * "hu": "/images/translation/hu/S9fbogoPGK8ly7KPTxVISwAxqB4p6wFt_b.jpg",
             * "lt": "/images/translation/lt/0aAmRJFDtVGSHxYGBZLik93kOPWKayFb_b.jpg",
             * "ru": "/images/translation/ru/Tk24awD4sYOh8nn9dxjo98yrNarsSJZ__b.jpg",
             * "sk": "/images/translation/sk/rmwytNUeRHMainV5ObdYrMqMF-yT14RM_b.jpg"
             * }
             */
            $files = [];
            foreach ($translations as $language => $path) {
                $pathFull = Yii::getAlias(\backend\models\form\LanguageImageSave::$pathSave1 . $path);
                if (file_exists($pathFull)) {
                    $info = pathinfo($pathFull);
                    $pathFull_t =  $info['dirname'] . '/' . str_replace('_b.' . $info['extension'], '_t.' . $info['extension'], $info['basename']);
                    $files[$language] = [
                        'b' => getFileBase64($pathFull),
                        't' => getFileBase64($pathFull_t),
                    ];
                }
            }
            $row['files'] = $files;
        }
        if ($row['type'] == \common\models\language\Category::TYPE_FILE) {
            /**
             * @var array $translations
             * {
             * "cz": "/images/translation/cz/dM47ho2FAdHV2cy7rwmC9rq_D1uRquK5_b.jpg",
             * "de": "/images/translation/de/A2zFQWpjdFjcv2RziCa-x9dLLSekMIOz_b.jpg",
             * "en": "/images/translation/en/aMoHXyc6LqAKm1FmVczr2wjdJGpWWU0J_b.jpg",
             * "es": "/images/translation/es/ixOmbHYYzLM8ZE4rcx5es88gC9mtolgH_b.jpg",
             * "et": "/images/translation/et/qTbv6EJj6N0dx3GsFRCnAk9GMpIL4OQ8_b.jpg",
             * "fr": "/images/translation/fr/djTR2P4v2NqjaqXucrssyfIaUt3oXki__b.jpg",
             * "hu": "/images/translation/hu/S9fbogoPGK8ly7KPTxVISwAxqB4p6wFt_b.jpg",
             * "lt": "/images/translation/lt/0aAmRJFDtVGSHxYGBZLik93kOPWKayFb_b.jpg",
             * "ru": "/images/translation/ru/Tk24awD4sYOh8nn9dxjo98yrNarsSJZ__b.jpg",
             * "sk": "/images/translation/sk/rmwytNUeRHMainV5ObdYrMqMF-yT14RM_b.jpg"
             * }
             */
            $files = [];
            foreach ($translations as $language => $path) {
                $pathFull = Yii::getAlias(\backend\models\form\LanguageImageSave::$pathSave1 . $path);
                if (file_exists($pathFull)) {
                    $info = pathinfo($pathFull);
                    $files[$language] = getFileBase64($pathFull);
                }
            }
            $row['files'] = $files;
        }
    }
    $row['translations'] = $translations;
    $ret[] = $row;
}


/**
 * Выдает файл с контентом
 *
 * @param string $path полный путь к файлу
 *
 * @return string array
 * [
 *      'name': string - только имя
 *      'content': array (base64) - закодированный в Base64 данные файла разбитые на строки по 1000 символов
 * ]
 *
 */
function getFileBase64($path)
{
    if (!file_exists($path)) {
        return null;
    }
    $info = pathinfo($path);
    $filePath = $path;
    $content = base64_encode(file_get_contents($filePath));
    $chars = 100;
    $c = (int)((strlen($content) + ($chars - 1)) / $chars);
    $rows = [];
    for ($page = 0; $page < $c; $page++) {
        $offset = $page * $chars;
        $rows[] = substr($content, $offset, $chars);
    }

    return [
        'name'    => $info['basename'],
        'content' => $rows
    ];
}


if (isset($categoryTree)) {
    $ret = [
        'categoryTree' => $categoryTree,
        'messages'     => $ret,
    ];
} else {
    $ret = [
        'messages' => $ret,
    ];
}

echo(\yii\helpers\Json::encode($ret));
?>
