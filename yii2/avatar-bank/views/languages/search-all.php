<?php
/**
 * Created by PhpStorm.
 * User: s.arhangelskiy
 * Date: 17.02.2017
 * Time: 17:40
 */

/** @var $model \backend\models\form\SearchLanguage */

use yii\bootstrap\ActiveForm;

?>

<h1>Поиск</h1>
<style>
    .pagination {
        margin-top: 20px;
    }
</style>
<?php if (Yii::$app->session->hasFlash('form')) { ?>
    <p>Результат</p>

    <p>Вы искали: <?= \yii\helpers\Html::encode($model->term) ?></p>
    <p><a href="/languages/search-all" class="btn btn-default">Вернуться</a></p>

    <h2>В строках</h2>

    <?php \yii\widgets\Pjax::begin() ?>
    <?= \yii\grid\GridView::widget([
        'dataProvider' => new \yii\data\ActiveDataProvider([
            'query' => \common\models\language\Message::find()
                ->select([
                    'source_message.id',
                    'source_message.category',
                    'source_message.message',
                    'message.language',
                    'message.translation',
                    'languages_category_tree.id as cid',
                    'languages_category_tree.name',
                ])
                ->innerJoin('source_message', 'source_message.id = message.id')
                ->innerJoin('languages_category_tree', 'languages_category_tree.code = source_message.category')
                ->where(['like', 'message.translation', $model->term])
                ->asArray()
        ]),
        'columns'      => [
            [
                'attribute' => 'language',
                'header'    => 'язык'
            ],
            [
                'attribute' => 'translation',
                'header'    => 'перевод'
            ],
            [
                'attribute' => 'message',
                'header'    => 'KEY'
            ],
            'category',
            'name',
            'cid',
            [
                'header'  => 'редактировать',
                'content' => function ($item) {
                    return \yii\helpers\Html::a('редактировать', ['languages/category', 'id' => $item['cid'], 'MessageSearch' => ['id' => $item['id']]], ['data' => ['pjax' => 0]]);
                }
            ],
        ]

    ]) ?>
    <?php \yii\widgets\Pjax::end() ?>

    <h2>В таблицах</h2>

    <?php foreach (\console\controllers\LanguagesController::$tableList as $tableName => $tableConfig) { ?>
        <h3><a href="<?= \yii\helpers\Url::to(['languages/tables-horizont', 'table' => $tableName]) ?>"><?= $tableName ?></a></h3>
        <?php
        $select = [];
        $where = [];
        $languages = \common\models\Language::find()->select(['code'])->column();
        $type = $tableConfig['name_sufix'];
        foreach ($tableConfig['column'] as $column) {
            foreach ($languages as $language) {
                if ($language == 'ru') {
                    if ($type == 2) {
                        $select[] = $column['name'];
                    } else {
                        $select[] = $column['name'] . '_' . $language;
                    }
                } else {
                    $select[] = $column['name'] . '_' . $language;
                }
            }
        }
        foreach ($select as $field) {
            $where[] = ['like', $field, $model->term];
        }
        ?>
        <?= \yii\grid\GridView::widget([
            'dataProvider' => new \yii\data\ActiveDataProvider([
                'query'      => (new \yii\db\Query())->select($select)
                    ->from($tableName)
                    ->where(\yii\helpers\ArrayHelper::merge(['or'], $where)),
                'pagination' => [
                    'pageParam' => 'page-' . $tableName
                ]
            ]),
        ]) ?>
    <?php } ?>


<?php } else { ?>
    <?php $form = ActiveForm::begin(['method' => 'get']) ?>
    <?= $form->field($model, 'term') ?>
    <?= \yii\helpers\Html::submitButton('Найти', ['class' => 'btn btn-success']) ?>
    <?php ActiveForm::end() ?>
<?php } ?>
