<?php


/** @var \yii\web\View $this */

use common\models\Language;
use common\models\language\Category;
use yii\grid\GridView;
use yii\helpers\ArrayHelper;
use yii\widgets\Pjax;

$this->title = 'Переводы';

function getLink($params = [])
{
    $options = Yii::$app->request->get();
    $options[] = 'languages/index';
    ArrayHelper::merge($options, $params);

    return \yii\helpers\Url::to($options);
}

?>


<h2>Переводы</h2>

<div class="col-lg-2">
    <p>Категории</p>
    <ul>
        <?= $this->render('_tree', ['rows' => Category::getRows()]); ?>
    </ul>
    <?php foreach (Language::find()->all() as $item) { ?>
        <?php
        $class = ['btn'];
        if ($item->code == Yii::$app->language) {
            $class[] = 'btn-success';
        } else {
            $class[] = 'btn-default';
        }
        $class = join(' ', $class);
        ?>
        <button data-language="<?= $item->code ?>" class="<?= $class ?>"><?= $item->name ?></button>
    <?php } ?>
</div>
<div class="col-lg-10">

    <?php Pjax::begin(); ?>
    <?php if (Yii::$app->request->get('id')) { ?>
        <?php $category = Category::findOne(Yii::$app->request->get('id')) ?>
        <?php
        $this->registerJs(<<<JS
            var language = 1;
            var functionUpdate = function() {
                var b = $(this);
                var id = b.data('id');
                var value = $('#value_' + id).val();
                b.click(null);
                $.ajax({
                    url: '/languages/update-string',
                    data: {
                        id: id,
                        value: value,
                        language: language
                    },
                    type: 'post',
                    dataType: 'json',
                    success: function(ret) {
                        b.removeClass('btn-default').addClass('btn-success');
                        window.setTimeout(function() {
                            b.click(functionUpdate);
                            b.removeClass('btn-success').addClass('btn-default');
                        }, 2000);
                    }
                });
            };
            $('.buttonUpdate').click(functionUpdate);
JS
);
        ?>
        <?= GridView::widget([
            'dataProvider' => new \yii\data\ActiveDataProvider([
                'query'      => $category->getMessages()
                    ->innerJoin('message', 'message.id = source_message.id')
                    ->select([
                        'source_message.id',
                        'source_message.message',
                        'message.language',
                        'message.translation',
                    ])
                    ->andWhere(['message.language' => 'ru'])
                    ->asArray(),
                'pagination' => [
                    'pageSize' => 100,
                ],

            ]),
            'columns'      => [
                'id',
                'message',
                [
                    'header'  => 'Перевод',
                    'content' => function ($item) {
                        $arr = [];
                        $arr[] = \yii\helpers\Html::input('text', 'message', $item['translation'], [
                            'class' => 'form-control',
                            'id'    => 'value_' . $item['id'],
                        ]);
                        $arr[] = \yii\helpers\Html::tag('span',
                            \yii\helpers\Html::button('+', [
                                'class' => 'btn btn-default buttonUpdate',
                                'data-id' => $item['id'],
                            ])
                            , [
                                'class'   => 'input-group-btn',
                            ]);

                        return \yii\helpers\Html::tag("div", join("\n", $arr), ['class' => 'input-group']);
                    }
                ],
            ],
            'summary'      => '',
        ]); ?>
    <?php } ?>
    <?php Pjax::end(); ?>

    <button class="btn btn-default">Обновить</button>
</div>
