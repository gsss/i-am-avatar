<?php

/**
 * params:
 * [
 *      'rows'     => [
 *                       [
 *                            'id'                 => '524'
 *                            'name'               => '123456'
 *                            ...
 *                            'selected'           => bool
 *                        ], ...
 *                    ]
 * ]
 */

/** @var $rows array */
/** @var $options string */


?>


<?php foreach ($rows as $item) { ?>
    <div style="margin-top: 10px;">
    <?php if ($options['id'] != $item['id']) { ?>
        <a
            href="<?= \yii\helpers\Url::to(['languages/category', 'id' => $item['id']]) ?>"
            id="categoryName_<?= $item['id'] ?>"
            >
            <?= $item['name'] ?>
        </a> (<?= $item['code'] ?>)
    <?php } else { ?>
        <span class="label label-success" id="categoryName_<?= $item['id'] ?>">
            <?= $item['name'] ?>
        </span>
    <?php } ?><br/>
        <!-- Extra small button group -->
        <div class="btn-group">
            <button class="btn btn-default btn-xs dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                Действия <span class="caret"></span>
            </button>
            <ul class="dropdown-menu">
                <li><a class="buttonEdit" data-id="<?= $item['id'] ?>" data-value="<?= $item['name'] ?>">Редактировать</a></li>
                <li><a class="buttonCategoryDelete" data-id="<?= $item['id'] ?>" >Удалить</a></li>
                <li><a class="buttonCategoryExport" data-id="<?= $item['id'] ?>" >Экспортировать категорию</a></li>
                <li><a class="buttonCategoryExportJson" data-id="<?= $item['id'] ?>" >Экспортировать категорию в JSON</a></li>
<!--                <li><a class="buttonCategoryExportCsv" data-id="--><?//= $item['id'] ?><!--" >Экспортировать категорию в CSV</a></li>-->
            </ul>
        </div>
    <?php if (isset($item['nodes'])) { ?>
        <div style="margin-left: 20px; margin-top: 10px;">
            <?= $this->render('_tree', [
                'rows'    => $item['nodes'],
                'options' => $options,
            ]); ?>
        </div>
    <?php } ?>
    </div>

<?php } ?>



