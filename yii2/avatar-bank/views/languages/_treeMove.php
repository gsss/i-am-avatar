<?php

/**
 * params:
 * [
 *      'rows'     => [
 *                       [
 *                            'id'                 => '524'
 *                            'name'               => '123456'
 *                            ...
 *                            'selected'           => bool
 *                        ], ...
 *                    ]
 * ]
 */

/** @var $rows array */
/** @var $options string */


?>


<?php foreach ($rows as $item) { ?>
    <div style="margin-top: 10px;">
    <?php if ($options['id'] != $item['id']) { ?>
        <a
            href = "javascript:move(<?= $item['id'] ?>);"
            id = "categoryName_<?= $item['id'] ?>"
            >
            <?= $item['name'] ?>
        </a>
    <?php } else { ?>
        <span class="label label-success" id="categoryName_<?= $item['id'] ?>">
            <?= $item['name'] ?>
        </span>
    <?php } ?>

    <?php if (isset($item['nodes'])) { ?>
        <div style="margin-left: 20px; margin-top: 10px;">
            <?= $this->render('_treeMove', [
                'rows'    => $item['nodes'],
                'options' => $options,
            ]); ?>
        </div>
    <?php } ?>
    </div>

<?php } ?>



