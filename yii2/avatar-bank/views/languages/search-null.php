<?php
/**
 * Created by PhpStorm.
 * User: s.arhangelskiy
 * Date: 17.02.2017
 * Time: 17:40
 */

/** @var \yii\web\View $this */
/** @var $model \backend\models\form\LanguageNull */

use common\models\language\Category;
use yii\bootstrap\ActiveForm;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;


?>

<h1>Поиск</h1>
<style>
    .pagination {
        margin-top: 20px;
    }
</style>
<?php if (Yii::$app->session->hasFlash('form')) { ?>
    <div class="modal fade" id="modalEditString" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Редактирование</h4>
                </div>
                <div class="modal-body">
                    <div class="col-lg-12" style="margin: 40px 0px 40px 0px;">
                        <textarea class="form-control" rows="20"></textarea>
                    </div>
                </div>
                <div class="modal-footer">
                    <?php
                    $this->registerJs(<<<JS
$('.buttonSaveField').click(function(ret){
    var inputId = $('#modalEditString').data('input-id');
    $('#' + inputId).val(
        $('#modalEditString textarea').val()
    );
    
    var id = $('#modalEditString').data('id');
    var language = $('#modalEditString').data('language');
    var value = $('#modalEditString textarea').val();
    ajaxJson({
        url: '/languages/update-string',
        data: {
            id: id,
            value: value,
            language: language
        },
        success: function(ret) {
            $('#modalEditString').modal('hide');
        },
        errorScript: function(ret) {
            alert(ret.data.data);
        }
    });
});
JS
                    );
                    ?>
                    <button type="button" class="btn btn-success buttonSaveField">Сохранить</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
                </div>
            </div>
        </div>
    </div>

    <p>Результат</p>

    <p>Вы искали: <?= Html::encode($model->language) ?></p>
    <p>
        <a href="/languages/search-null" class="btn btn-default">Вернуться</a>
        <a href="<?= \yii\helpers\Url::to(['languages/search-null-export', 'language' => $model->language])?>" class="btn btn-default">CSV</a>
    </p>

    <?php \yii\widgets\Pjax::begin() ?>
    <?php
    $this->registerJs(<<<JS
$('.buttonHover').on('mouseover', function() {
    console.log(1);
    var b = $(this);
    b.removeClass('btn-default').addClass('btn-info');
}).on('mouseout', function() {
  var b = $(this);
    b.removeClass('btn-info').removeClass('btn-success').addClass('btn-default');
});

$('.buttonUpdateModal').click(function(e) {
    var o = $(this);
    var language = o.data('language');
    var id = o.data('id');
    $('#modalEditString').modal();
    $('#modalEditString textarea').val($('#value_' + language + '_' + id).val());
    $('#modalEditString').data('input-id', 'value_' + language + '_' + id);
    $('#modalEditString').data('language', language);
    $('#modalEditString').data('id', id);
});

var functionUpdate = function() {
    var b = $(this);
    var id = b.data('id');
    var language = b.data('language');
    var value = $('#value_' + language + '_' + id).val();
    b.off('click');
    ajaxJson({
        url: '/languages/update-string',
        data: {
            id: id,
            value: value,
            language: language
        },
        success: function(ret) {
            b.removeClass('btn-default').removeClass('btn-info').addClass('btn-success');
            b.parent().parent().removeClass('has-error');
            b.parent().parent().addClass('has-success');
            b.click(functionUpdate);
            window.setTimeout(function() {
                b.removeClass('btn-success').addClass('btn-default');
                b.parent().parent().removeClass('has-success');
            }, 1000);
        },
        errorScript: function(ret) {
            alert(ret.data.data);
        }
    });
};
$('.buttonUpdate').click(functionUpdate);

JS
    );
    $modelSearch = new \backend\models\search\MessageNull();
    $provider = $modelSearch->search($model->language, Yii::$app->request->get());

    ?>
    <?= \yii\grid\GridView::widget([
        'dataProvider' => $provider,
        'filterModel'  => $modelSearch,
        'tableOptions' => [
            'class' => 'table table-hover table-striped',
        ],
        'rowOptions'   => function ($item) {
            if ($item['is_locked'] or $item['is_to_delete']) {
                return [
                    'style' => 'opacity:0.5;',
                ];
            } else {
                return [];
            }
        },
        'columns'      => [
            [
                'attribute' => 'id',
                'header'    => 'sid'
            ],
            [
                'header'        => $model->language,
                'headerOptions' => [
                    'style' => 'width:400px;',
                    'role'  => 'button',
                    'data'  => [
                        'language-code' => $model->language,
                    ]
                ],
                'attribute'     => 'translation',
                'content'       => function ($item, $key, $index, \yii\grid\Column $column) {
                    $languageCode = $column->headerOptions['data']['language-code'];

                    $type = ArrayHelper::getValue($item, 'type', 1);
                    if (is_null($type)) $type = 1;
                    $t = (is_null($item['translation'])) ? '' : $item['translation'];
                    $isLocked = false;
                    if ($item['is_locked']) $isLocked = true;
                    $isToDelete = false;
                    if ($item['is_to_delete']) $isToDelete = true;

                    switch ($type) {
                        case Category::TYPE_STRING:
                            $arr = [];
                            $options = [
                                'class'   => 'form-control',
                                'id'      => 'value_' . $languageCode . '_' . $item['id'],
                                'data-id' => $item['id'],
                            ];
                            if ($isLocked || $isToDelete) $options['disabled'] = 'disabled';
                            $arr[] = Html::input('text', 'message', $t, $options);
                            if (($isLocked == false) && ($isToDelete == false)) {
                                $arr[] = Html::tag('span',
                                    Html::button(
                                        Html::tag('span', null, [
                                            'class' => 'glyphicon glyphicon-ok',
                                        ])
                                        , [
                                        'class' => 'btn btn-default buttonUpdate buttonHover',
                                        'data'  => [
                                            'language' => $languageCode,
                                            'id'       => $item['id'],
                                        ],
                                    ])
                                    , [
                                        'class' => 'input-group-btn',
                                    ]);
                                $arr[] = Html::tag('span',
                                    Html::button(
                                        Html::tag('span', null, [
                                            'class' => 'glyphicon glyphicon-comment',
                                        ])
                                        , [
                                        'class' => 'btn btn-default buttonUpdateModal buttonHover',
                                        'data'  => [
                                            'language' => $languageCode,
                                            'id'       => $item['id'],
                                        ],
                                    ])
                                    , [
                                        'class' => 'input-group-btn',
                                    ]);
                            }

                            $value = Html::tag("div", join("\n", $arr), [
                                'class' => 'input-group',
                                'style' => 'width: 100%',
                            ]);
                            break;
                        case Category::TYPE_IMAGE:
                            if ($isLocked || $isToDelete) {
                                return '';
                            } else {
                                $arr = [];
                                $category = Yii::$app->session->get('languages.cache.category');
                                $formBegin = Html::beginForm(\yii\helpers\Url::to(), 'post', ['enctype' => 'multipart/form-data']);
                                $formEnd = Html::endForm();

                                $arr[] = Html::hiddenInput('id', $item['id']);
                                $arr[] = Html::hiddenInput('language', $languageCode);
                                $arr[] = Html::fileInput('file');
                                $arr[] = Html::submitButton('Обновить', [
                                    'class' => 'btn btn-default btn-xs buttonHover',
                                    'style' => 'margin-top: 5px;',
                                    'name'  => 'action-upload-image',
                                ]);

                                return $formBegin . "\n" . join("\n", $arr) . "\n" . $formEnd;
                            }

                            break;
                    }

                    return $value;
                }
            ],
            [
                'header'        => 'ru',
                'headerOptions' => [
                    'style' => 'width:400px;',
                    'role'  => 'button',
                    'data'  => [
                        'language-code' => 'ru',
                    ]
                ],
                'content'       => function ($item, $key, $index, \yii\grid\Column $column) {
                    $languageCode = $column->headerOptions['data']['language-code'];
                    $sourceMessage = \common\models\language\SourceMessage::findOne($item['id']);
                    $messageObject = \common\models\language\Message::findOne(['id' => $item['id'], 'language' => $languageCode]);
                    $messageToDeleteObject = \common\models\language\MessageToDelete::findOne(['id' => $item['id'], 'language' => $languageCode]);

                    if (!is_null($messageToDeleteObject)) {
                        switch ($sourceMessage->type) {
                            case \common\models\language\SourceMessage::TYPE_STRING:
                                return $messageToDeleteObject->translation;
                                break;
                            case \common\models\language\SourceMessage::TYPE_IMAGE:
                                $info = pathinfo($messageToDeleteObject->translation);
                                $ext = $info['extension'];
                                $path = str_replace('_b.', '_t.', $messageToDeleteObject->translation);
                                $fullPath = Yii::getAlias(\backend\models\form\LanguageImageSave::$pathSave1 . $path);
                                if (!file_exists($fullPath)) {
                                    $src = 'data:image/' . $ext . ';base64,' . '';
                                } else {
                                    $src = 'data:image/' . $ext . ';base64,' . base64_encode(file_get_contents($fullPath));
                                }
                                return Html::tag('p',
                                    Html::a(Html::img($src, ['class' => 'thumbnail']), ['languages/download', 'id' => $item['id'], 'language' => 'ru'], ['target' => '_blank', 'data' => ['pjax' => 0]])
                                );
                                break;
                            default:
                                return $messageToDeleteObject->translation;
                                break;
                        }
                    }
                    if (!is_null($messageObject)) {
                        switch ($sourceMessage->type) {
                            case \common\models\language\SourceMessage::TYPE_STRING:
                                return $messageObject->translation;
                                break;
                            case \common\models\language\SourceMessage::TYPE_IMAGE:
                                $info = pathinfo($messageObject->translation);
                                $ext = $info['extension'];
                                $path = str_replace('_b.', '_t.', $messageObject->translation);
                                $fullPath = Yii::getAlias(\backend\models\form\LanguageImageSave::$pathSave1 . $path);
                                if (!file_exists($fullPath)) {
                                    $src = 'data:image/' . $ext . ';base64,' . '';
                                } else {
                                    $src = 'data:image/' . $ext . ';base64,' . base64_encode(file_get_contents($fullPath));
                                }
                                return Html::tag('p',
                                    Html::a(Html::img($src, ['class' => 'thumbnail']), ['languages/download', 'id' => $item['id'], 'language' => 'ru'], ['target' => '_blank', 'data' => ['pjax' => 0]])
                                );
                                break;
                            default:
                                return $messageObject->translation;
                                break;
                        }
                    }

                    return '';
                }
            ],
            [
                'header'        => 'en',
                'headerOptions' => [
                    'style' => 'width:400px;',
                    'role'  => 'button',
                    'data'  => [
                        'language-code' => 'en',
                    ]
                ],
                'content'       => function ($item, $key, $index, \yii\grid\Column $column) {
                    $languageCode = $column->headerOptions['data']['language-code'];
                    $sourceMessage = \common\models\language\SourceMessage::findOne($item['id']);
                    $messageObject = \common\models\language\Message::findOne(['id' => $item['id'], 'language' => $languageCode]);
                    $messageToDeleteObject = \common\models\language\MessageToDelete::findOne(['id' => $item['id'], 'language' => $languageCode]);

                    if (!is_null($messageToDeleteObject)) {
                        switch ($sourceMessage->type) {
                            case \common\models\language\SourceMessage::TYPE_STRING:
                                return Html::encode($messageToDeleteObject->translation);
                                break;
                            case \common\models\language\SourceMessage::TYPE_IMAGE:
                                $info = pathinfo($messageToDeleteObject->translation);
                                $ext = $info['extension'];
                                $path = str_replace('_b.', '_t.', $messageToDeleteObject->translation);
                                $fullPath = Yii::getAlias(\backend\models\form\LanguageImageSave::$pathSave1 . $path);
                                if (!file_exists($fullPath)) {
                                    $src = 'data:image/' . $ext . ';base64,' . '';
                                } else {
                                    $src = 'data:image/' . $ext . ';base64,' . base64_encode(file_get_contents($fullPath));
                                }
                                return Html::tag('p',
                                    Html::a(Html::img($src, ['class' => 'thumbnail']), ['languages/download', 'id' => $item['id'], 'language' => 'ru'], ['target' => '_blank', 'data' => ['pjax' => 0]])
                                );
                                break;
                            default:
                                return Html::encode($messageToDeleteObject->translation);
                                break;
                        }
                    }
                    if (!is_null($messageObject)) {
                        switch ($sourceMessage->type) {
                            case \common\models\language\SourceMessage::TYPE_STRING:
                                return Html::encode($messageObject->translation);
                                break;
                            case \common\models\language\SourceMessage::TYPE_IMAGE:
                                $info = pathinfo($messageObject->translation);
                                $ext = $info['extension'];
                                $path = str_replace('_b.', '_t.', $messageObject->translation);
                                $fullPath = Yii::getAlias(\backend\models\form\LanguageImageSave::$pathSave1 . $path);
                                if (!file_exists($fullPath)) {
                                    $src = 'data:image/' . $ext . ';base64,' . '';
                                } else {
                                    $src = 'data:image/' . $ext . ';base64,' . base64_encode(file_get_contents($fullPath));
                                }

                                return Html::tag('p',
                                    Html::a(Html::img($src, ['class' => 'thumbnail']), ['languages/download', 'id' => $item['id'], 'language' => 'ru'], ['target' => '_blank', 'data' => ['pjax' => 0]])
                                );
                                break;
                            default:
                                return Html::encode($messageObject->translation);
                                break;
                        }
                    }

                    return '';
                }
            ],
            [
                'attribute' => 'message',
                'header'    => 'KEY'
            ],
            'category',
            'name',
            [
                'header'  => 'редактировать',
                'content' => function ($item) {
                    return Html::a('редактировать', ['languages/category', 'id' => $item['cid'], 'MessageSearch' => ['id' => $item['id']]], ['data' => ['pjax' => 0]]);
                }
            ],
        ]

    ]) ?>

    <?php foreach (\console\controllers\LanguagesController::$tableList as $tableName => $tableConfig) { ?>
        <h2>
            <a href="<?= \yii\helpers\Url::to(['languages/tables-horizont', 'table' => $tableName]) ?>">
                <?= $tableName ?>
            </a>
        </h2>
        <?php
        $columns = [];
        $where = [];
        if (isset($tableConfig['id'])) {
            $columns[] = $tableConfig['id'];
        } else {
            $columns[] = 'id';
        }
        foreach ($tableConfig['column'] as $name => $config) {
            if ($model->language == 'ru' && $tableConfig['name_sufix'] == 2) {
                $columnName = $name;
            } else {
                $columnName = $name . '_' . $model->language;
            }
            $columns[] = $columnName;
            $where[] = ['or',
                [$columnName => null],
                [$columnName => ''],
            ];
        }
        $rows = (new \yii\db\Query())->select('*')->from($tableName)->where(ArrayHelper::merge(['or'], $where))->select($columns)->all();

        ?>
        <?= \yii\grid\GridView::widget([
            'dataProvider' => new \yii\data\ActiveDataProvider([
                'query'      => (new \yii\db\Query())
                    ->select('*')
                    ->from($tableName)
                    ->where(ArrayHelper::merge(['or'], $where))
                    ->select($columns),
                'pagination' => [
                    'pageParam' => 'page-' . $tableName
                ]
            ])
        ]) ?>
    <?php } ?>

<?php } else { ?>
    <p>Ищет ключи для для строк у которых нет перевода на указанный язык. И в строках "на удаление" то же нет перевода.</p>
    <?php $form = ActiveForm::begin(['method' => 'get']) ?>
    <?= $form->field($model, 'language')->dropDownList(ArrayHelper::merge(['- ничего не выбрано -'], ArrayHelper::map(
        \common\models\Language::find()->all(),
        'code',
        'name'
    ))) ?>
    <?= Html::submitButton('Найти', ['class' => 'btn btn-success']) ?>
    <?php ActiveForm::end() ?>
<?php } ?>
