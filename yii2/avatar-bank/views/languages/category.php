<?php


/** @var \yii\web\View $this */
/** @var integer $id */

use common\models\Language;
use common\models\language\Access;
use common\models\language\Category;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

\avatar\assets\Clipboard::register($this);

$this->title = 'Переводы';

function getLink($params = [])
{
    $options = Yii::$app->request->get();
    $options[] = 'languages/category';
    $options = ArrayHelper::merge($options, $params);

    return Url::to($options);
}


/**
 * В этом языке вывожу все данные
 */
$language = Yii::$app->request->get('language', Yii::$app->language);
$languageId = null;

$ids = Access::find()->where(['user_id' => Yii::$app->user->id])->select('language_id')->column();
$languages = Language::find()
    ->where([
        'and',
        ['not', ['code' => 'ru']],
        ['in', 'id', $ids],
    ])
    ->asArray()
    ->all();

$languageInfo = [
    'languages' => $languages,
    'default'   => 0,
    'count'     => count($languages),
];

Yii::$app->cache->set('languages.cache', $languageInfo);


?>

<style>
    a.asc:after {
        content: ' ↓';
        display: inline;
    }

    a.desc:after {
        content: ' ↑';
        display: inline;
    }
    td.input_id input {
        width:100px;
    }
</style>
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Добавление новой строки</h4>
            </div>
            <div class="modal-body">
                <div class="form-group recipient-field-address">
                    <p>Message:</p>
                    <input class="form-control" style="margin-bottom: 20px;">

                    <p class="label label-danger labelError" style="display: none;">Message:</p>
                </div>
                <div class="form-group recipient-field-address">

                    <p>Тип:</p>
                    <select class="form-control" id="field-type">
                        <option value="1">Строка</option>
                        <option value="2">Картинка</option>
                        <option value="4">Файл</option>
                        <option value="3">HTML</option>
                    </select>

                    <p class="label label-danger labelError" style="display: none;">Message:</p>
                </div>
            </div>
            <div class="modal-footer">
                <?php
                $this->registerJs(<<<JS
$('#myModal .modal-body input').on('focus', function(e) {
    $('.labelError').hide();
});
$('.buttonSave').click(function(e) {
    ajaxJson({
        url: '/languages/add-key',
        data: {
            value: $('#myModal .modal-body input').val(),
            category: $('#category').html(),
            type: $('#field-type').val()
        },
        success:function(ret) {
            var id = ret;
            window.location.reload();
        },
        errorScript: function(ret) {
            $('.labelError').html(ret.data).show();
        }
    });
});

JS
                );
                ?>
                <button type="button" class="btn btn-primary buttonSave">Сохранить</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Отмена</button>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="modalMove" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Перенос строк</h4>
            </div>
            <div class="modal-body">
                <div class="col-lg-12" style="margin: 40px;">
                    Выберите категорию нажмите на категорию в которую вы хотите переместить выбранные строки:
                    <?= $this->render('_treeMove', [
                        'rows'    => Category::getRows(),
                        'options' => ['id' => $id],
                    ]); ?>
                </div>
            </div>
            <div class="modal-footer">
            </div>
        </div>
    </div>
</div>



<?php if (Yii::$app->session->hasFlash('\backend\controllers\LanguagesController::actionCategory')) { ?>
<?php
    $this->registerJs(<<<JS
$('#modalAskImport').modal();
$('.buttonReplace').click(function() {
    ajaxJson({
        url: '/languages/import-replace',
        success: function(ret) {
            window.location = window.location;
        }
    });
});
$('.buttonNew').click(function() {
    ajaxJson({
        url: '/languages/import-new',
        success: function(ret) {
            window.location = window.location;
        }
    });
});
$('.buttonCancel').click(function() {
    ajaxJson({
        url: '/languages/import-cancel',
        success: function(ret) {
            window.location = window.location;
        }
    });
});
$('.buttonCancel').click(function() {
    $('#modalAskImport').modal('hide');
});
JS
    );
?>
    <div class="modal fade" id="modalAskImport" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Вопрос по импорту</h4>
                </div>
                <div class="modal-body">
                    <div class="col-lg-12" style="margin: 40px;">
                        <p>При импорте были обнаружены строки которые уже есть в Базе Данных.</p>
                        <p>
                            <?= join(', ', Yii::$app->cacheFile->get('\common\components\Migration::updateStringsIds')); ?>
                        </p>
                        <p>Что с ними делать?</p>
                    </div>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-default buttonReplace">Перезаписать</button>
                    <button class="btn btn-warning buttonNew">Загрузить только новые</button>
                    <button class="btn btn-danger buttonCancel">Ничего не загружать</button>
                </div>
            </div>
        </div>
    </div>

<?php } ?>

<?php if (Yii::$app->session->hasFlash('Error')) { ?>
<?php
    $error = Yii::$app->session->getFlash('Error');
    $this->registerJs(<<<JS
$('#modalErrorMaxFileSize .body').html('{$error}');
$('#modalErrorMaxFileSize').modal();
JS
    );
?>
    <div class="modal fade" id="modalErrorMaxFileSize" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Загрузка файла</h4>
                </div>
                <div class="modal-body">
                    <div class="col-lg-12 body" style="margin: 40px;">
                    </div>
                </div>
                <div class="modal-footer">
                </div>
            </div>
        </div>
    </div>

<?php } ?>

<div class="modal fade" id="modalInfo" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button
                        type="button"
                        class="close"
                        data-dismiss="modal"
                        aria-label="Close"
                ><span
                            aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Успешно</h4>
            </div>
            <div class="modal-body">
                <div class="col-lg-12" style="margin: 40px 0px 40px 0px;">
                    <p>Успешно скопировано в буфер.</p>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="modalEdit" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Успешно</h4>
            </div>
            <div class="modal-body">
                <div class="col-lg-12" style="margin: 40px 0px 40px 0px;">
                    <input class="form-control">
                </div>
            </div>
            <div class="modal-footer">
                <?php
                $this->registerJs(<<<JS
$('.buttonSaveCategory').click(function(ret){
    var id = $('#modalEdit').data('id');
    ajaxJson({
        url: '/languages/category-save',
        data: {
            id: id,
            name: $('#modalEdit .modal-body input').val()
        },
        success: function(ret) {
            $('#categoryName_' + id).html($('#modalEdit .modal-body input').val());
            $('#modalEdit').modal('hide');
        }
    });
});
JS
                );
                ?>
                <button type="button" class="btn btn-default buttonSaveCategory">Сохранить</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="modalEditString" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Редактирование</h4>
            </div>
            <div class="modal-body">
                <textarea class="form-control" rows="20"></textarea>
            </div>
            <div class="modal-footer">
                <?php
                $this->registerJs(<<<JS
$('.buttonSaveField').click(function(ret){
    var inputId = $('#modalEditString').data('input-id');
    $('#' + inputId).val(
        $('#modalEditString textarea').val()
    );
    
    var id = $('#modalEditString').data('id');
    var language = $('#modalEditString').data('language');
    var value = $('#modalEditString textarea').val();
    ajaxJson({
        url: '/languages/update-string',
        data: {
            id: id,
            value: value,
            language: language
        },
        success: function(ret) {
            $('#modalEditString').modal('hide');
        },
        errorScript: function(ret) {
            alert(ret.data.data);
        }
    });
});
JS
                );
                ?>
                <button type="button" class="btn btn-success buttonSaveField">Сохранить</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="modalEditComment" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Коментрарий</h4>
            </div>
            <div class="modal-body">
                <textarea class="form-control" rows="10"></textarea>

            </div>
            <div class="modal-footer">
                <?php
                $this->registerJs(<<<JS
$('.buttonSaveComment').click(function(ret){
    var comment = $('#modalEditComment textarea').val();
    var id = $('#modalEditComment').data('id');
    
    ajaxJson({
        url: '/languages/update-comment',
        data: {
            id: id,
            value: comment
        },
        success: function(ret) {
            $('#modalEditComment').modal('hide');
            $('#comment_' + id).html(comment);
        },
        errorScript: function(ret) {
            alert(ret.data.data);
        }
    });
});
$('.buttonUpdateJson').on('click', function(e) {
    var o = $(this);
    var form = o.parent();
    var val = form.find('input[type="file"]').val();
    console.log(val);
    if (val != '') {
        var arr = val.toLowerCase().split('.');
        if (!(arr[arr.length-1] == 'json')) {
            alert('Можно загрузить только JSON');
            return false;
        }
        o.parent().append($('<input>', {name: 'action-upload-json', type: 'hidden', value:1}));
        form.submit();
    } else {
        alert('Нужно выбрать файл');
    }
});

JS
                );
                ?>
                <button type="button" class="btn btn-success buttonSaveComment">Сохранить</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="modalError" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Ошибка</h4>
            </div>
            <div class="modal-body">
                <div class="col-lg-12" style="margin: 40px 0px 40px 0px;">
                    <p>Выберите строки для экспорта</p>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
            </div>
        </div>
    </div>
</div>



<div class="container" style="padding-bottom: 70px;">

        <div class="col-lg-12">

<h1 class="page-header">Переводы</h1>

<div class="row">

    <div class="col-lg-2">
        <?= $this->render('_tree', [
            'rows'    => Category::getRows(['selectFields' => 'id,name,code']),
            'options' => ['id' => $id],
        ]); ?>
        <p style="margin-top: 20px;">
            <a href="/languages/category-add" data-pjax="0" style="width: 100%;" class="btn btn-success">Добавить</a>
        </p>

        <?= Html::beginForm(null, 'post', ['enctype' => 'multipart/form-data', 'style' => 'margin-top:50px;']); ?>
        <?= Html::fileInput('file', null, ['accept' => 'application/json', 'style' => 'margin-bottom:20px;']) ?>
        <?= Html::button('Импортировать', ['class' => 'btn btn-success buttonUpdateJson', 'style' => 'width:100%;']) ?>
        <?= Html::endForm() ?>

    </div>


    <?php $id = Yii::$app->request->get('id'); ?>

    <?php if (!is_null($id)) { ?>
        <?php
        /**
         * в переменной хранится список языков
         * переменная default говорит о том какой язык нужно вывести в текущей рисуемой ячейке
         * после заканчивания рисования всех ячеек в одной строке я возвращаюсь в начало
         * русский выводится первым и с запросом
         * следовательно в массиве все языки кроме русского и индексы расставляются начиная с 0
         */
        $category = Category::findOne(Yii::$app->request->get('id'));
        Yii::$app->session->set('languages.cache.category', $category);
        ?>

        <div class="hide" id="category" data-id="<?= $category->id ?>"><?= $category->code ?></div>

        <?php \yii\widgets\Pjax::begin(); ?>
        <div class="col-lg-10">
            <?php
            $this->registerJs(<<<JS
$('.buttonEdit').click(function(e) {
    var id = $(this).data('id');

    $('#modalEdit input').val($('#categoryName_'+id).html().trim());
    $('#modalEdit').data('id', $(this).data('id')).modal();
});
$('.buttonCategoryDelete').click(function(e) {
    var o = $(this);
    if (confirm('Вы уверены?')) {
        ajaxJson({
            url: '/languages/category-delete',
            data: {
                id: o.data('id')
            },
            success: function(ret) {
                $('#categoryName_' + o.data('id')).parent().remove();
                alert('Успешно');
                window.location = '/languages/category';
            }
        });
    }
});
$('.buttonUpdateModal').click(function(e) {
    var o = $(this);
    var language = o.data('language');
    var id = o.data('id');
    $('#modalEditString').modal();
    $('#modalEditString textarea').val($('#value_' + language + '_' + id).val());
    $('#modalEditString').data('input-id', 'value_' + language + '_' + id);
    $('#modalEditString').data('language', language);
    $('#modalEditString').data('id', id);
});
$('.buttonComment').click(function(e) {
    var o = $(this);
    var comment = o.data('comment');
    var id = o.data('id');
    $('#modalEditComment textarea').val(comment);
    $('#modalEditComment').data('id', id);
    $('#modalEditComment').modal();
});
$('.buttonCategoryExport').click(function(e) {
    var o = $(this);
    window.location = '/languages/category-export?id=' + o.data('id');
});
$('.buttonCategoryExportJson').click(function(e) {
    var o = $(this);
    window.location = '/languages/category-export-json?id=' + o.data('id');
});
$('.buttonCategoryExportCsv').click(function(e) {
    var o = $(this);
    window.location = '/languages/category-export-csv?id=' + o.data('id');
});

JS
            );


            ?>


            <?php if (Yii::$app->request->get('id')) { ?>
                <?php
                $languageInfo['category'] = $category;
                Yii::$app->cache->set('languages.cache', $languageInfo);
                $this->registerJs(<<<JS
var clipboard = new Clipboard('.buttonCopy');
clipboard.on('success', function(e) {
    console.log(e);
    console.log(e.trigger);
});
JS
                );


                $this->registerJs(<<<JS
var language = 1;
var functionUpdate = function() {
    var b = $(this);
    var id = b.data('id');
    var language = b.data('language');
    var value = $('#value_' + language + '_' + id).val();
    b.off('click');
    ajaxJson({
        url: '/languages/update-string',
        data: {
            id: id,
            value: value,
            language: language
        },
        success: function(ret) {
            b.removeClass('btn-default').removeClass('btn-info').addClass('btn-success');
            b.parent().parent().removeClass('has-error');
            b.parent().parent().addClass('has-success');
            b.click(functionUpdate);
            window.setTimeout(function() {
                b.removeClass('btn-success').addClass('btn-default');
                b.parent().parent().removeClass('has-success');
            }, 1000);
        },
        errorScript: function(ret) {
            alert(ret.data.data);
        }
    });
};
$('.buttonUpdate').click(functionUpdate);
$('.buttonHover').on('mouseover', function() {
    var b = $(this);
    b.removeClass('btn-default').addClass('btn-info');
}).on('mouseout', function() {
  var b = $(this);
    b.removeClass('btn-info').removeClass('btn-success').addClass('btn-default');
});
JS
                );

                $sort = new \yii\data\Sort([
                    'attributes' => [
                        'id' => [
                            'default' => SORT_ASC,
                            'label'   => 'id',
                        ],
                        'message',
                        'translation',
                    ],
                ]);
                $searchModel = new \avatar\models\search\MessageSearch();
                $dataProvider = $searchModel->search($category, 'ru', $sort, Yii::$app->request->get());
                ?>
                <style>
                    .pagination {
                        margin: 20px 0px 20px 0px;
                    }
                </style>
                <?php
                \avatar\assets\ZeroClipboard\Asset::register($this);
                $pathAsset = Yii::$app->assetManager->getBundle('\avatar\assets\ZeroClipboard\Asset')->baseUrl;
                $this->registerJs(<<<JS

$('[data-toggle="tooltip"]').tooltip();

$('.buttonUpdate1').on('click', function(e) {
    var o = $(this);
    var form = o.parent();
    var val = form.find('input[type="file"]').val();
    if (val != '') {
        var arr = val.toLowerCase().split('.');
        if (arr[arr.length-1] != 'pdf') {
            alert('Можно загрузить только PDF'); 
            return false;
        }
        o.parent().append($('<input>', {name: 'action-upload-file', type: 'hidden', value:1}));
        form.submit();
    } else {
        alert('Нужно выбрать файл');
    }
});
$('.buttonDelete1').on('click', function(e) {
    var o = $(this);
    o.parent().append($('<input>', {name: 'action-delete-file', type: 'hidden', value:1}));
    o.parent().submit();
});
$('.buttonUpdate2').on('click', function(e) {
    var o = $(this);
    var form = o.parent();
    var val = form.find('input[type="file"]').val();
    console.log(val);
    if (val != '') {
        var arr = val.toLowerCase().split('.');
        if (!(arr[arr.length-1] == 'jpg' || arr[arr.length-1] == 'png')) {
            alert('Можно загрузить только JPG,PNG');
            return false;
        }
        o.parent().append($('<input>', {name: 'action-upload-image', type: 'hidden', value:1}));
        form.submit();
    } else {
        alert('Нужно выбрать файл');
    }
});
$('.buttonDelete2').on('click', function(e) {
    var o = $(this);
    o.parent().append($('<input>', {name: 'action-delete-image', type: 'hidden', value:1}));
    o.parent().submit();
});
$('.inputCopy').on('click', function(e) {
    var o = $(this);
    $(this).tooltip({
        title: 'Copied!',
        delay: { "show": 100, "hide": 500 },
        placement: 'bottom'
    })
});

JS
                );
                $columns = [
                    [
                        'class' => 'yii\grid\CheckboxColumn', // <-- here
                        // you may configure additional properties here
                    ],
                ];
                if (Yii::$app->user->can('languages-admin')) {
                    $this->registerJs(<<<JS
$('.buttonBlock').click(function(e) {
    var b = $(this);
    ajaxJson({
        url: '/languages/block',
        data: {
            id: b.data('id')
        },
        success: function(ret) {
            window.location.reload();
        }
    }); 
});
$('.buttonUnBlock').click(function(e) {
    var b = $(this);
    ajaxJson({
        url: '/languages/un-block',
        data: {
            id: b.data('id')
        },
        success: function(ret) {
            window.location.reload();
        }
    }); 
});
$('.buttonToDelete').click(function(e) {
    var b = $(this);
    ajaxJson({
        url: '/languages/to-delete',
        data: {
            id: b.data('id')
        },
        success: function(ret) {
            window.location.reload();
        }
    }); 
});
$('.buttonUnToDelete').click(function(e) {
    var b = $(this);
    ajaxJson({
        url: '/languages/un-to-delete',
        data: {
            id: b.data('id')
        },
        success: function(ret) {
            window.location.reload();
        }
    }); 
});
JS
                    );
                    $columns[] = [
                        'header'        => 'Блокировка',
                        'content'        => function ($item) {
                            $rows = [];
                            $is_locked = $item['is_locked'];
                            if ($is_locked) {
                                $rows[] = Html::button(Html::tag('i', null, ['class' => 'glyphicon glyphicon-remove']), ['class' => 'btn btn-danger buttonUnBlock', 'data' => ['id' => $item['id']], 'id' => 'buttonBlock' . $item['id'] ]);
                            } else {
                                $rows[] = Html::button(Html::tag('i', null, ['class' => 'glyphicon glyphicon-ok']), ['class' => 'btn btn-success buttonBlock', 'data' => ['id' => $item['id']], 'id' => 'buttonBlock' . $item['id']]);
                            }

                            return Html::tag("div", join("\n", $rows), [
                                'class' => 'input-group',
                            ]);
                        }
                    ];
                    $columns[] = [
                        'header'        => 'На удаление',
                        'content'        => function ($item) {
                            $rows = [];
                            $is_locked = $item['is_to_delete'];
                            if ($is_locked) {
                                $rows[] = Html::button(Html::tag('i',null,['class' => 'glyphicon glyphicon-remove']), ['class' => 'btn btn-danger buttonUnToDelete', 'data' => ['id' => $item['id']], 'id' => 'buttonBlock' . $item['id'] ]);
                            } else {
                                $rows[] = Html::button(Html::tag('i',null,['class' => 'glyphicon glyphicon-ok']), ['class' => 'btn btn-success buttonToDelete', 'data' => ['id' => $item['id']], 'id' => 'buttonBlock' . $item['id']]);
                            }

                            return Html::tag("div", join("\n", $rows), [
                                'class' => 'input-group',
                            ]);
                        }
                    ];
                }

                $columns2 = [
                    [
                        'header'        => $sort->link('id'),
                        'headerOptions' => [
                            'style' => 'width:200px;',
                            'role'  => 'button',
                        ],
                        'filterOptions' => [
                            'class'  => 'input_id',
                        ],
                        'attribute'     => 'id',
                    ],
                    [
                        'header'         => $sort->link('message'),
                        'headerOptions'  => [
                            'style' => 'width:300px;',
                            'role'  => 'button',
                        ],
                        'contentOptions' => [
                            'style' => 'width:200px;',
                        ],
                        'attribute'      => 'message',
                        'content'        => function ($item) {
                            $l = 30;
                            $message = ArrayHelper::getValue($item, 'message');
                            if (\cs\services\Str::length($message) > $l) {
                                $message = \cs\services\Str::sub($message, 0, $l) . ' ...';
                            }
                            return Html::tag(
                                'div'
                                ,
                                Html::encode($message)
                                ,
                                [
                                    'style' => 'width: 200px',
                                ]
                            );
                        }
                    ],

                    [
                        'header'  => 'KEY',
                        'content' => function ($item) {
                            $message = $item['message'];
                            $string = $message;

                            return Html::button('c', [
                                'class' => 'btn btn-default btn-xs buttonCopy',
                                'data'  => [
                                    'clipboard-text' => $string,
                                    'id'      => $item['id'],
                                ],
                                'title' => $string,
                            ]);
                        }
                    ],
                    [
                        'header'  => 'Yii',
                        'content' => function ($item) {
                            $category = $item['category'];
                            $message = $item['message'];
                            $message = \yii\helpers\VarDumper::export($message);
                            $string = "Yii::t('{$category}', {$message})";

                            return Html::button('Yii', [
                                'class' => 'btn btn-default btn-xs buttonCopy',
                                'data'  => [
                                    'clipboard-text' => $string,
                                    'id'      => $item['id'],
                                ],
                                'title' => $string,
                            ]);
                        }
                    ],
                    [
                        'header'  => '<>',
                        'content' => function ($item) {
                            $category = $item['category'];
                            $message = $item['message'];
                            $message = \yii\helpers\VarDumper::export($message);
                            $string = "<?= \\Yii::t('{$category}', {$message}) ?>";

                            return Html::button('<>', [
                                'class' => 'btn btn-default btn-xs buttonCopy',
                                'data'  => [
                                    'clipboard-text' => $string,
                                    'id'      => $item['id'],
                                ],
                                'title' => $string,
                            ]);
                        }
                    ],

                    [
                        'header'         => 'Русский',
                        'headerOptions'  => [
                            'style' => 'width:400px;',
                            'role'  => 'button',
                        ],
                        'attribute'      => 'translation',
                        'contentOptions' => function ($item) {
                            $type = ArrayHelper::getValue($item, 'type', 1);
                            if (is_null($type)) $type = 1;
                            $isToDelete = false;
                            if ($item['is_to_delete']) $isToDelete = true;

                            if ($isToDelete) {
                                $t = \common\models\language\MessageToDelete::findOne([
                                    'id'       => $item['id'],
                                    'language' => 'ru',
                                ]);
                                $t = (is_null($t)) ? '' : $t->translation;
                            } else {
                                $t = \common\models\language\Message::findOne([
                                    'id'       => $item['id'],
                                    'language' => 'ru',
                                ]);
                                $t = (is_null($t)) ? '' : $t->translation;
                            }
                            if ($t == '' && $type == 2) return [
                                'style' => 'background-color: #ffdddd;'
                            ];

                            return [];
                        },
                        'content'        => function ($item) {
                            $type = ArrayHelper::getValue($item, 'type', 1);
                            if (is_null($type)) $type = 1;
                            $t = (is_null($item['translation'])) ? '' : $item['translation'];
                            $isLocked = false;
                            if ($item['is_locked']) $isLocked = true;
                            $isToDelete = false;
                            if ($item['is_to_delete']) $isToDelete = true;

                            if ($isToDelete) {
                                $messageToDelete = \common\models\language\MessageToDelete::findOne(['id' => $item['id'], 'language' => 'ru']);
                                if (!is_null($messageToDelete)) {
                                    $t = $messageToDelete->translation;
                                }
                            }
                            switch ($type) {
                                case Category::TYPE_STRING:
                                    $arr = [];
                                    $options = [
                                        'class'   => 'form-control',
                                        'id'      => 'value_ru_' . $item['id'],
                                        'data-id' => $item['id'],
                                        'style'   => 'width: 200px',
                                    ];
                                    if ($isLocked || $isToDelete) $options['disabled'] = 'disabled';
                                    $arr[] = Html::input('text', 'message', $t, $options);
                                    if (($isLocked == false) && ($isToDelete == false)) {
                                        $arr[] = Html::tag('span',
                                            Html::button(
                                                Html::tag('span', null, [
                                                    'class' => 'glyphicon glyphicon-ok',
                                                ])
                                                , [
                                                'class' => 'btn btn-default buttonUpdate buttonHover',
                                                'data'  => [
                                                    'language' => 'ru',
                                                    'id'       => $item['id'],
                                                ],
                                            ])
                                            , [
                                                'class' => 'input-group-btn',
                                            ]);
                                        $arr[] = Html::tag('span',
                                            Html::button(
                                                Html::tag('span', null, [
                                                    'class' => 'glyphicon glyphicon-comment',
                                                ])
                                                , [
                                                'class' => 'btn btn-default buttonUpdateModal buttonHover',
                                                'data'  => [
                                                    'language' => 'ru',
                                                    'id'       => $item['id'],
                                                ],
                                            ])
                                            , [
                                                'class' => 'input-group-btn',
                                            ]);
                                    }

                                    $value = Html::tag("div", join("\n", $arr), [
                                        'class' => 'input-group',
                                        'style' => 'width: 100%',
                                    ]);
                                    break;
                                case Category::TYPE_IMAGE:
                                    $img = '';
                                    if ($t != '') {
                                        $info = pathinfo($t);
                                        $ext = $info['extension'];
                                        $path = str_replace('_b.', '_t.', $t);
                                        $fullPath = Yii::getAlias(\backend\models\form\LanguageImageSave::$pathSave1 . $path);
                                        if (!file_exists($fullPath)) {
                                            $src = 'data:image/' . $ext . ';base64,' . '';
                                        } else {
                                            $src = 'data:image/' . $ext . ';base64,' . base64_encode(file_get_contents($fullPath));
                                        }
                                        $img = Html::tag('p',
                                            Html::a(Html::img($src, ['class' => 'thumbnail']), ['languages/download', 'id' => $item['id'], 'language' => 'ru'], ['target' => '_blank', 'data' => ['pjax' => 0]])
                                        );
                                    }
                                    if ($isLocked || $isToDelete) {
                                        $value = $img;
                                    } else {
                                        $arr = [];
                                        $category = Yii::$app->session->get('languages.cache.category');
                                        $formBegin = Html::beginForm(null, 'post', ['enctype' => 'multipart/form-data']);
                                        $formEnd = Html::endForm();

                                        $arr[] = Html::hiddenInput('id', $item['id']);
                                        $arr[] = Html::hiddenInput('language', 'ru');
                                        $arr[] = Html::fileInput('file');
                                        $arr[] = Html::button('Обновить', [
                                            'class' => 'btn btn-default btn-xs buttonHover buttonUpdate2',
                                            'style' => 'margin-top: 5px;',
                                            'name'  => 'action-upload-image',
                                        ]);
                                        if ($img != '') {
                                            $arr[] = Html::button('Удалить', [
                                                'class' => 'btn btn-default btn-xs buttonHover buttonDelete2',
                                                'style' => 'margin-top: 5px;',
                                                'name'  => 'action-delete-image',
                                            ]);
                                        }

                                        $value = $img . $formBegin . "\n" . join("\n", $arr) . "\n" . $formEnd;
                                    }

                                    break;
                                case Category::TYPE_FILE:
                                    $img = '';
                                    if ($t != '') {
                                        $info = pathinfo($t);
                                        $img = Html::tag('p',
                                            Html::a(Html::img('/images/controller/languages/category/doc.png', ['width' => 50]), ['languages/download', 'id' => $item['id'], 'language' => 'ru'], ['target' => '_blank', 'data' => ['pjax' => 0]]) . '.' . strtoupper($info['extension'])
                                        );
                                    }
                                    if ($isLocked || $isToDelete) {
                                        $value = $img;
                                    } else {
                                        $arr = [];
                                        $formBegin = Html::beginForm(null, 'post', ['enctype' => 'multipart/form-data', 'class' => 'uploadForm']);
                                        $formEnd = Html::endForm();

                                        $arr[] = Html::hiddenInput('id', $item['id']);
                                        $arr[] = Html::hiddenInput('language', 'ru');
                                        $arr[] = Html::hiddenInput('MAX_FILE_SIZE', 50*1024*1024);
                                        $arr[] = Html::fileInput('file');
                                        $arr[] = Html::button('Обновить', [
                                            'class' => 'btn btn-default btn-xs buttonHover buttonUpdate1',
                                            'style' => 'margin-top: 5px;',
                                            'name'  => 'action-upload-file',
                                        ]);
                                        if ($img != '') {
                                            $arr[] = Html::button('Удалить', [
                                                'class' => 'btn btn-default btn-xs buttonHover buttonDelete1',
                                                'style' => 'margin-top: 5px;',
                                                'name'  => 'action-delete-file',
                                            ]);
                                        }

                                        $value = $img . $formBegin . "\n" . join("\n", $arr) . "\n" . $formEnd;
                                    }

                                    break;
                            }
                            return $value;
                        }
                    ],
                ];
                $columns = ArrayHelper::merge($columns, $columns2);

                /** @var Language $language */
                foreach ($languageInfo['languages'] as $language) {
                    $columns[] = [
                        'header'         => $language['name'] . ' ' . '(' . strtoupper( $language['code']) . ')',
                        'headerOptions'  => [
                            'style' => 'width:400px;',
                            'role'  => 'button',
                            'data'  => [
                                'language-code' => $language['code'],
                            ],
                        ],
                        'contentOptions' => function ($item, $key, $index, \yii\grid\Column $column) {
                            $languageCode = $column->headerOptions['data']['language-code'];

                            $type = ArrayHelper::getValue($item, 'type', 1);
                            if (is_null($type)) $type = 1;
                            $isToDelete = false;
                            if ($item['is_to_delete']) $isToDelete = true;

                            if ($isToDelete) {
                                $t = \common\models\language\MessageToDelete::findOne([
                                    'id'       => $item['id'],
                                    'language' => $languageCode,
                                ]);
                                $t = (is_null($t)) ? '' : $t->translation;
                            } else {
                                $t = \common\models\language\Message::findOne([
                                    'id'       => $item['id'],
                                    'language' => $languageCode,
                                ]);
                                $t = (is_null($t)) ? '' : $t->translation;
                            }
                            if ($t == '' && $type == 2) return [
                                'style' => 'background-color: #ffdddd;'
                            ];

                            return [];
                        },
                        'content'        => function ($item, $key, $index, \yii\grid\Column $column) {
                            $languageCode = $column->headerOptions['data']['language-code'];

                            $type = ArrayHelper::getValue($item, 'type', 1);
                            if (is_null($type)) $type = 1;
                            // перевод
                            {
                                $t = \common\models\language\Message::findOne([
                                    'id'       => $item['id'],
                                    'language' => $languageCode,
                                ]);
                                $t = (is_null($t)) ? '' : $t->translation;
                            }
                            $isLocked = false;
                            if ($item['is_locked']) $isLocked = true;
                            $isToDelete = false;
                            if ($item['is_to_delete']) $isToDelete = true;

                            if ($isToDelete) {
                                $messageToDelete = \common\models\language\MessageToDelete::findOne(['id' => $item['id'], 'language' => $languageCode]);
                                if (!is_null($messageToDelete)) {
                                    $t = $messageToDelete->translation;
                                }
                            }

                            switch ($type) {
                                case Category::TYPE_STRING:
                                    $arr = [];

                                    $options = [
                                        'class'   => 'form-control',
                                        'id'      => 'value_' . $languageCode . '_' . $item['id'],
                                        'data-id' => $item['id'],
                                        'style'   => 'width: 200px',
                                    ];
                                    if ($isLocked || $isToDelete) $options['disabled'] = 'disabled';
                                    $arr[] = Html::input('text', 'message', $t, $options);
                                    if (($isLocked == false) && ($isToDelete == false)) {
                                        $arr[] = Html::tag('span',
                                            Html::button(
                                                Html::tag('span', null, [
                                                    'class' => 'glyphicon glyphicon-ok',
                                                ])
                                                , [
                                                'class' => 'btn btn-default buttonUpdate buttonHover',
                                                'data'  => [
                                                    'language' => $languageCode,
                                                    'id'       => $item['id'],
                                                ],
                                            ])
                                            , [
                                                'class' => 'input-group-btn',
                                            ]);
                                        $arr[] = Html::tag('span',
                                            Html::button(
                                                Html::tag('span', null, [
                                                    'class' => 'glyphicon glyphicon-comment',
                                                ])
                                                , [
                                                'class' => 'btn btn-default buttonUpdateModal buttonHover',
                                                'data'  => [
                                                    'language' => $languageCode,
                                                    'id'       => $item['id'],
                                                ],
                                            ])
                                            , [
                                                'class' => 'input-group-btn',
                                            ]);
                                    }

                                    $value = Html::tag("div", join("\n", $arr), [
                                        'class' => ($t == '') ? 'input-group has-error' : 'input-group',
                                        'style' => 'width: 100%',
                                    ]);
                                    break;
                                case Category::TYPE_IMAGE:
                                    $img = '';
                                    if ($t != '') {
                                        $info = pathinfo($t);
                                        $ext = $info['extension'];
                                        $path = str_replace('_b.', '_t.', $t);
                                        $fullPath = Yii::getAlias(\backend\models\form\LanguageImageSave::$pathSave1 . $path);
                                        if (!file_exists($fullPath)) {
                                            $src = 'data:image/' . $ext . ';base64,' . '';
                                        } else {
                                            $src = 'data:image/' . $ext . ';base64,' . base64_encode(file_get_contents($fullPath));
                                        }
                                        $img = Html::tag('p',
                                            Html::a(Html::img($src, ['class' => 'thumbnail']), ['languages/download', 'id' => $item['id'], 'language' => $languageCode], ['target' => '_blank', 'data'=>['pjax' => 0]])
                                        );
                                    }
                                    if ($isLocked || $isToDelete) {
                                        $value = $img;
                                    } else {
                                        $arr = [];
                                        $category = Yii::$app->session->get('languages.cache.category');
                                        $formBegin = Html::beginForm(null, 'post', ['enctype' => 'multipart/form-data']);
                                        $formEnd = Html::endForm();

                                        $arr[] = Html::hiddenInput('id', $item['id']);
                                        $arr[] = Html::hiddenInput('language', $languageCode);
                                        $arr[] = Html::fileInput('file');
                                        $arr[] = Html::button('Обновить', [
                                            'class' => 'btn btn-default btn-xs buttonHover buttonUpdate2',
                                            'style' => 'margin-top: 5px;',
                                        ]);
                                        if ($img != '') {
                                            $arr[] = Html::button('Удалить', [
                                                'class' => 'btn btn-default btn-xs buttonHover buttonDelete2',
                                                'style' => 'margin-top: 5px;',
                                            ]);
                                        }

                                        $value = $img . $formBegin . "\n" . join("\n", $arr) . "\n" . $formEnd;
                                    }
                                    break;
                                case Category::TYPE_FILE:
                                    $img = '';
                                    if ($t != '') {
                                        $info = pathinfo($t);
                                        $img = Html::tag('p',
                                            Html::a(Html::img('/images/controller/languages/category/doc.png', ['width' => 50]), ['languages/download', 'id' => $item['id'], 'language' => $languageCode], ['target' => '_blank', 'data' => ['pjax' => 0]]) . '.' . strtoupper($info['extension'])
                                        );
                                    }
                                    if ($isLocked || $isToDelete) {
                                        $value = $img;
                                    } else {
                                        $arr = [];
                                        $formBegin = Html::beginForm(null, 'post', ['enctype' => 'multipart/form-data']);
                                        $formEnd = Html::endForm();

                                        $arr[] = Html::hiddenInput('id', $item['id']);
                                        $arr[] = Html::hiddenInput('language', $languageCode);
                                        $arr[] = Html::hiddenInput('MAX_FILE_SIZE', 50*1024*1024);
                                        $arr[] = Html::fileInput('file');
                                        $arr[] = Html::button('Обновить', [
                                            'class' => 'btn btn-default btn-xs buttonHover buttonUpdate1',
                                            'style' => 'margin-top: 5px;',
                                        ]);
                                        if ($img != '') {
                                            $arr[] = Html::button('Удалить', [
                                                'class' => 'btn btn-default btn-xs buttonHover buttonDelete1',
                                                'style' => 'margin-top: 5px;',
                                            ]);
                                        }

                                        $value = $img . $formBegin . "\n" . join("\n", $arr) . "\n" . $formEnd;
                                    }

                                    break;
                            }

                            return $value;
                        }
                    ];
                }
                $this->registerJs(<<<JS
$('.buttonDelete').click(function(e) {
    if (confirm('Вы уверены?')) {
        var b = $(this);
            ajaxJson({
                url: '/languages/delete',
                data: {id: b.data('id')},
                success: function(ret) {
                    b.parent().parent().remove();
                    alert('Успешно');
                }
            });
    }
});
JS
                );
                $columnComment = [
                    'header'  => 'Коментарий',
                    'content' => function ($item) {
                        $rows = [];
                        $commentOriginal = $item['comment'];
                        $comment = $commentOriginal;
                        if (is_null($comment)) {
                            $comment = '- не задан -';
                        }
                        if ($comment == '') {
                            $comment = '- не задан -';
                        }
                        $rows[] = Html::tag('div', $comment, ['class' => 'buttonComment', 'id' => 'comment' . '_' . $item['id'], 'data' => ['id' => $item['id'], 'comment' => $commentOriginal]]);

                        return join("\n", $rows);
                    }
                ];
                $columns[] = $columnComment;
                $columnDelete = [
                    'header'  => 'Удалить',
                    'content' => function ($item) {
                        return Html::button(
                            Html::tag(
                                'i',
                                null,
                                ['class' => 'glyphicon glyphicon-remove']
                            ),
                            [
                                'class' => 'btn btn-danger buttonDelete',
                                'data'  => [
                                    'id' => $item['id']
                                ]
                            ]
                        );
                    }
                ];
                $columns[] = $columnDelete;

                ?>
                <?= \yii\grid\GridView::widget([
                    'dataProvider' => $dataProvider,
                    'filterModel'  => $searchModel,
                    'rowOptions' => function($item) {
                        if ($item['is_locked'] or $item['is_to_delete']) {
                            return [
                                'style' => 'opacity:0.5;',
                            ];
                        } else {
                            return [];
                        }
                    },
                    'tableOptions' => [
                        'class' => 'table table-hover table-striped',
                        'style' => 'width: auto;',
                        'id'    => 'tableMessages'
                    ],
                    'columns'      => $columns,
                    'summary'      => '',
                ]); ?>
                <?php
                $this->registerJs(<<<JS
$('#buttonExport').click(function(e) {
    var ids = [];
    $('input[name="selection[]"]').each(function(i,v) {
        if ($(v).is(':checked')) {
            ids.push(parseInt($(v).attr('value')));
        }
    });
    if (ids.length == 0) {
        $('#modalError').modal();
        return false;
    }
    window.location = '/languages/export?ids=' + ids.join(',');
});
$('#buttonExportJson').click(function(e) {
    var ids = [];
    $('input[name="selection[]"]').each(function(i,v) {
        if ($(v).is(':checked')) {
            ids.push(parseInt($(v).attr('value')));
        }
    });
    if (ids.length == 0) {
        $('#modalError').modal();
        return false;
    }
    window.location = '/languages/export-json?ids=' + ids.join(',');
});
$('#buttonExportCsv').click(function(e) {
    var ids = [];
    $('input[name="selection[]"]').each(function(i,v) {
        if ($(v).is(':checked')) {
            ids.push(parseInt($(v).attr('value')));
        }
    });
    if (ids.length == 0) {
        $('#modalError').modal();
        return false;
    }
    window.location = '/languages/export-csv?ids=' + ids.join(',');
});
$('#buttonMove').click(function(e) {
    if (!$('input[name="selection[]"]').is(':checked')) {
        alert('Вам нужно выбрать хотябы одну строку');
        return;
    }
    $('#modalMove').modal();
});
function move(id)
{
    $('input[name="selection[]"]').each(function(i,v) {

    });
    ajaxJson({
        url: '/languages/move',
        data: {
            from: [],
            to: id
        },
        success: function(ret) {

        }
    });
}
JS
                );
                ?>
                <div class="btn-group">
                    <button type="button"  class="btn btn-default" id="buttonExport" >Экспортировать</button>
                    <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <span class="caret"></span>
                        <span class="sr-only">Toggle Dropdown</span>
                    </button>
                    <ul class="dropdown-menu">
                        <li><a id="buttonExportJson">Экспортировать в JSON</a></li>
                    </ul>
                </div>
                <div class="btn-group">
                    <button class="btn btn-default" data-toggle="modal" data-target="#myModal">
                        Добавить
                    </button>
                </div>


            <?php } ?>
        </div>

        <?php \yii\widgets\Pjax::end(); ?>
    <?php } ?>

</div>
</div>

</div>
