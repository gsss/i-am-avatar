<?php

/** $this \yii\web\View  */
/** @var $school \common\models\school\School */
/** @var $product \common\models\shop\Product */

use yii\helpers\Url;
use app\services\GsssHtml;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use app\services\GetArticle\Collection;

$this->title = 'Все картинки';


?>

<div class="container">

    <?php \avatar\widgets\SchoolMenu::begin(['school' => $school]) ?>
    <p>
        <a href="<?= Url::to(['cabinet-school/view', 'id' => $school->id]) ?>">Сообщество</a> /
        Интернет магазин /
        <a href="<?= Url::to(['cabinet-school-shop-goods/index', 'id' => $school->id]) ?>">Товары</a> /
        <a href="<?= Url::to(['cabinet-school-shop-goods/edit', 'id' => $product->id]) ?>">Товар <?= $product->id ?></a> /
        <a href="<?= Url::to(['cabinet-school-shop-goods-images/index', 'id' => $product->id]) ?>">Изображения</a>
    </p>

    <h1 class="page-header"><?= Html::encode($this->title) ?></h1>

    <p>
        <a href="<?= Url::to(['cabinet-school-shop-goods-images/add', 'id' => $product->id]) ?>" class="btn btn-default">Добавить</a>
    </p>

    <?php \yii\widgets\Pjax::begin(); ?>
    <?php
    $this->registerJS(<<<JS
$('[data-toggle="tooltip"]').tooltip();
$('.buttonDelete').click(function (e) {
    e.preventDefault();
    e.stopPropagation();
    if (confirm('Подтвердите удаление')) {
        var button = $(this);
        var id = $(this).data('id');
        ajaxJson({
            url: '/cabinet-school-shop-goods-images/delete' + '?' + 'id' + '=' + id,
            success: function (ret) {
                $('#modalInfo').on('hidden.bs.modal', function() {
                    button.parent().parent().remove();
                }).modal();
            }
        });
    }
});

$('.rowTable').click(function() {
    //window.location = '/cabinet-school-shop-goods-images/edit' + '?' + 'id' + '=' + $(this).data('id');
});



JS
    );
    ?>


    <?= \yii\grid\GridView::widget([
        'dataProvider' => new \yii\data\ActiveDataProvider([
            'query'      => \common\models\shop\ProductImage::find()
                ->where(['product_id' => $product->id])
            ,
            'pagination' => [
                'pageSize' => 20,
            ],
        ]),
        'tableOptions' => [
            'class' => 'table table-striped table-hover',
        ],
        'rowOptions'   => function ($item) {
            $data = [
                'data'  => ['id' => $item['id']],
                'class' => 'rowTable',
            ];
            return $data;
        },
        'columns'      => [
            'id',
            [
                'header'  => 'Картинка',
                'content' => function ($item) {
                    $i = ArrayHelper::getValue($item, 'image', '');
                    if ($i == '') return '';

                    $img = Html::img(\common\widgets\FileUpload7\FileUpload::getFile($i, 'crop'), [
                        'class'  => "thumbnail",
                        'width'  => 80,
                        'height' => 80,
                        'style'  => 'margin-bottom: 0px;',
                    ]);

                    return $img;
                },
            ],
            [
                'header'  => 'Наименование',
                'content' => function ($item) {
                    return $item['name'];
                },
            ],
            [
                'header'  => '',
                'content' => function ($item) {
                    $html = [];

                    $html[] =  Html::a('<span class="glyphicon glyphicon-pencil"></span>', [
                        'cabinet-school-shop-goods-images/edit',
                        'id' => $item['id'],
                    ], [
                        'class' => 'btn btn-primary btn-xs buttonEdit',
                        'data'  => [
                            'id' => $item['id'],
                        ],
                        'style' => 'margin-right: 10px;',
                    ]);

                    $html[] =  Html::button('<span class="glyphicon glyphicon-trash"></span>', [
                        'class' => 'btn btn-danger btn-xs buttonDelete',
                        'data'  => [
                            'id' => $item['id'],
                        ],
                    ]);

                    return join('', $html);
                },
            ],
        ],
    ]) ?>
    <?php \yii\widgets\Pjax::end(); ?>

    <?php \avatar\widgets\SchoolMenu::end() ?>
</div>

<div class="modal fade" id="modalInfo" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="myModalLabel">Информация</h4>
            </div>
            <div class="modal-body">
                Успешно!
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
            </div>
        </div>
    </div>
</div>