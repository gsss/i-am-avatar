<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;
use app\models\UnionCategory;
use yii\db\Query;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;

/** @var $this yii\web\View */
/** @var $school \common\models\school\School */
/** @var $model \avatar\models\forms\shop\Product */
/** @var $product \common\models\shop\Product */

$this->title = $model->name;

?>
<div class="container">

    <?php \avatar\widgets\SchoolMenu::begin(['school' => $school]) ?>
    <p>
        <a href="<?= Url::to(['cabinet-school/view', 'id' => $school->id]) ?>">Сообщество</a> /
        Интернет магазин /
        <a href="<?= Url::to(['cabinet-school-shop-goods/index', 'id' => $school->id]) ?>">Товары</a> /
        <a href="<?= Url::to(['cabinet-school-shop-goods/edit', 'id' => $product->id]) ?>">Товар <?= $product->id ?></a> /
        <a href="<?= Url::to(['cabinet-school-shop-goods-images/index', 'id' => $product->id]) ?>">Изображения</a>
    </p>

    <h1 class="page-header"><?= Html::encode($this->title) ?></h1>


    <div class="row">
        <div class="col-lg-8">
            <?php $form = \iAvatar777\services\FormAjax\ActiveForm::begin([
                'model' => $model,
                'success' => <<<JS
function (ret) {
    $('#modalInfo').on('hidden.bs.modal', function() {
        window.location = '/cabinet-school-shop-goods-images/index?id=' + {$product->id};
    }).modal();
}
JS

            ]); ?>
            <?= $form->field($model, 'name') ?>
            <?= $form->field($model, 'image') ?>

            <?php \iAvatar777\services\FormAjax\ActiveForm::end(); ?>
        </div>
    </div>

    <?php \avatar\widgets\SchoolMenu::end() ?>


</div>
<div class="modal fade" id="modalInfo" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="myModalLabel">Информация</h4>
            </div>
            <div class="modal-body">
                Успешно!
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
            </div>
        </div>
    </div>
</div>