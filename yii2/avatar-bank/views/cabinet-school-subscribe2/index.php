<?php

/** @var $this \yii\web\View */
/** @var $potok \common\models\school\Potok */
/** @var $school \common\models\school\School */

use yii\helpers\Url;
use app\services\GsssHtml;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use app\services\GetArticle\Collection;

$this->title = 'Все рассылки';

?>

<div class="container">

    <?php \avatar\widgets\SchoolMenu::begin(['school' => $school]) ?>
    <h1 class="page-header"><?= Html::encode($this->title) ?></h1>

    <p>
        <a href="<?= Url::to(['cabinet-school-subscribe2/add', 'id' => $school->id]) ?>"
          class="btn btn-default">Добавить</a>
    </p>

    <?php \yii\widgets\Pjax::begin(); ?>
    <?php
    $this->registerJS(<<<JS
$('.buttonOne').click(function(e) {
    e.preventDefault();
    e.stopPropagation();
    if (confirm('Подтвердите рассылку')) {
        var id = $(this).data('id');
        ajaxJson({
            url: '/cabinet-school-subscribe2/subscribe-one' + '?' + 'id' + '=' + id,
            success: function (ret) {
                $('#modalInfo').on('hidden.bs.modal', function() {
                }).modal();
            }
        });
    }
});
$('.buttonMe').click(function(e) {
    e.preventDefault();
    e.stopPropagation();
    if (confirm('Подтвердите рассылку')) {
        var id = $(this).data('id');
        ajaxJson({
            url: '/cabinet-school-subscribe2/subscribe-me' + '?' + 'id' + '=' + id,
            success: function (ret) {
                $('#modalInfo').on('hidden.bs.modal', function() {
                }).modal();
            }
        });
    }
});
$('.rowTable').click(function(e) {
    e.preventDefault();
    e.stopPropagation();
    window.location = '/cabinet-school-subscribe2/edit' + '?' + 'id' + '=' + $(this).data('id');
});
$('.buttonDelete').click(function (e) {
    e.preventDefault();
    e.stopPropagation();
    if (confirm('Подтвердите удаление')) {
        var button = $(this);
        var id = $(this).data('id');
        ajaxJson({
            url: '/cabinet-school-subscribe2/delete' + '?' + 'id' + '=' + id,
            success: function (ret) {
                $('#modalInfo').on('hidden.bs.modal', function() {
                    button.parent().parent().remove();
                }).modal();
            }
        });
    }
});
$('.buttonSubscribe').click(function (e) {
    e.preventDefault();
    e.stopPropagation();
    if (confirm('Подтвердите рассылку')) {
        var id = $(this).data('id');
        ajaxJson({
            url: '/cabinet-school-subscribe2/subscribe' + '?' + 'id' + '=' + id,
            success: function (ret) {
                $('#modalInfo').on('hidden.bs.modal', function() {
                }).modal();
            }
        });
    }
});
JS
    );
    ?>
    <?= \yii\grid\GridView::widget([
        'dataProvider' => new \yii\data\ActiveDataProvider([
            'query'      => \common\models\school\Subscribe2::find()
                ->where(['school_id' => $school->id])
            ,
            'pagination' => [
                'pageSize' => 20,
            ],
        ]),
        'tableOptions' => [
            'class' => 'table table-striped table-hover',
        ],
        'rowOptions'   => function ($item) {
            $data = [
                'data'  => ['id' => $item['id']],
                'role'  => 'button',
                'class' => 'rowTable',
            ];
            return $data;
        },
        'columns'      => [
            'id',
            'subject',
            [
                'header'  => 'Разослать',
                'content' => function ($item) {
                    return Html::button('Разослать', [
                        'class' => 'btn btn-success btn-xs buttonSubscribe',
                        'data'  => [
                            'id' => $item['id'],
                        ],
                    ]);
                },
            ],
            [
                'header'  => 'Тест',
                'content' => function ($item) {
                    return Html::button('Тест', [
                        'class' => 'btn btn-success btn-xs buttonOne',
                        'data'  => [
                            'id'     => $item['id'],
                            'toggle' => 'tooltip',
                        ],
                        'title' => 'Отправить хозяину школы',
                    ]);
                },
            ],
            [
                'header'  => 'Me',
                'content' => function ($item) {
                    return Html::button('Me', [
                        'class' => 'btn btn-success btn-xs buttonMe',
                        'data'  => [
                            'id'     => $item['id'],
                            'toggle' => 'tooltip',
                        ],
                        'title' => 'Отправить test',
                    ]);
                },
            ],
            [
                'header'  => 'Удалить',
                'content' => function ($item) {
                    return Html::button('Удалить', [
                        'class' => 'btn btn-danger btn-xs buttonDelete',
                        'data'  => [
                            'id' => $item['id'],
                        ],
                    ]);
                },
            ],
        ],
    ]) ?>
    <?php \yii\widgets\Pjax::end(); ?>
    <?php \avatar\widgets\SchoolMenu::end() ?>


</div>

<div class="modal fade" id="modalInfo" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="myModalLabel">Информация</h4>
            </div>
            <div class="modal-body">
                Успешно!
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
            </div>
        </div>
    </div>
</div>