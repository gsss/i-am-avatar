<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;
use app\models\UnionCategory;
use yii\db\Query;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/** @var $model \avatar\models\forms\school\Subscribe */
/* @var $school \common\models\school\School */

$this->title = 'Добавить рассылку';

?>
<div class="container">

    <?php \avatar\widgets\SchoolMenu::begin(['school' => $school]) ?>
    <h1 class="page-header"><?= Html::encode($this->title) ?></h1>

    <?php if (!is_null($id = Yii::$app->session->getFlash('form'))) : ?>

        <div class="alert alert-success">
            Успешно добавлено.
        </div>

        <p>
            <a href="<?= \yii\helpers\Url::to(['cabinet-school-subscribe2/index', 'id' => $school->id]) ?>" class="btn btn-success">Все рассылки</a>
        </p>

    <?php else: ?>


        <div class="row">
            <div class="col-lg-8">
                <?php $form = ActiveForm::begin([
                    'options' => ['enctype' => 'multipart/form-data'],
                ]); ?>
                <?= $model->field($form, 'subject') ?>
                <?= $model->field($form, 'content') ?>


                <hr>
                <div class="form-group">
                    <?= Html::submitButton('Добавить', [
                        'class' => 'btn btn-default',
                        'name'  => 'contact-button',
                        'style' => 'width:100%',
                    ]) ?>
                </div>
                <?php ActiveForm::end(); ?>
            </div>
        </div>

    <?php endif; ?>
    <?php \avatar\widgets\SchoolMenu::end() ?>


</div>
