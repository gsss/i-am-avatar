<?php
/**
 * Created by PhpStorm.
 * User: Бог-Творец
 * Date: 04.01.2017
 * Time: 18:54
 */
/* @var $this yii\web\View */
/* @var $file string */


$c = file_get_contents(__DIR__ . '/' . $file . '.md');

$rows = explode( "\n", $c);
if (\yii\helpers\StringHelper::startsWith($rows[0], '# ')) {
    $this->title = substr($rows[0], 2);
    array_shift($rows);
    $r = array_splice($rows, 1);
    $c = join("\n", $r);
} else {
    $this->title = 'Документ';
}

?>
<style>
    .content22 img {
        width: 100%;
    }
</style>
<div class="container">
    <div class="col-lg-12">
        <h1 class="page-header col-lg-offset-3"><?= \yii\helpers\Html::encode($this->title) ?></h1>
    </div>
    <div class="col-lg-3">
        <p>Главная</p>
        <p>Сообщество</p>
        <ul>
            <li><a href="/docs/school-dns">Доменное имя</a></li>
        </ul>
        <p>Интернет магазин</p>
        <ul>
            <li><a href="/docs/school-shop-goods">Товары</a></li>
            <li><a href="/docs/school-shop-goods-images">Картинки</a></li>
        </ul>
        <p>Кооператив</p>
        <ul>
            <li><a href="/docs/school-koop-index">Введение</a></li>
            <li><a href="/docs/school-koop-registration">Регистрация</a></li>
            <li><a href="/docs/school-koop-widget">Платежный виджет в кооперативе</a></li>
            <li><a href="/docs/school-koop-1c">API 1С</a></li>
            <li><a href="/docs/school-sms">СМС шлюз</a></li>
        </ul>
    </div>
    <div class="col-lg-9 content22">

        <?php
        $Parsedown = new Parsedown();
        echo $Parsedown->text($c);
        ?>
    </div>
</div>
