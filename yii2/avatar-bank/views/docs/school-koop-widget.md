# Платежный виджет в кооперативе

Предназначен для размещения на сайте кооператива у которого уже был сайт с каталогом товаров или услуг.

Что из себя представляет виджет?

Это еще один вид платежной системы посредством которой клиент может оплатить товар (услугу).

Выбирая платежный виджет кооператива клиент может сразу оплатить кооперативной картой или зарегистрироваться и оплатить.

Рассмотрим оба варианта

1) Клиент является участником кооператива
    - Клиент указывает номер карты
    - Вводит пин код
    - Подтверждает по СМС
    - Выводится сообщение об успешной сделке.
    - Со счета клиента списываются монеты.
    - На сайт кооператива отправляется уведомление об успешной покупке.
    
    Сделка завершена.

2) Клиент является гостем, то есть не является участником кооператива

    Клиент регистрируется в кооперативе и уже потом оплачивает товар (услугу) по схеме варианта 1.
    
    Регистрация:
    - Email и пароль
    - Числовая подпись
    - Подтвердить телефон
    - 2FA
    - Заполнение персональных данных и паспорта
    - Проверка со стороны членов совета и подпись
    - Подпись со стороны участника
    - Оплата членского взноса

![](https://cloud1.cloud999.ru/upload/cloud/15917/32254_l2gPdk4pNJ.png)

[Схема](https://app.diagrams.net/?title=%D0%9F%D0%BE%D0%BA%D1%83%D0%BF%D0%BA%D0%B0%20%D0%B2%20%D0%BA%D0%BE%D0%BE%D0%BF%D0%B5%D1%80%D0%B0%D1%82%D0%B8%D0%B2%D0%B5#R7V1bc5s4GP01ntk%2BZAdJgMlj4tjZnb3M7nS6bR6JkW222HgxTpz%2B%2BgV0QeJmxbHrzymdDNWVi6RzdKTvAw%2FIaLm7T%2Fz14o84oNEAW8FuQO4GGCMb40H%2BZwUvLMX1hixhnoQBL1QmfAy%2FUZ5o8dRtGNCNVjCN4ygN13riNF6t6DTV0vwkiZ%2F1YrM40q%2B69ue0lvBx6kf11M9hkC5YqoeHZfovNJwvxJWRe81ylr4ozJ9ks%2FCD%2BFlJIuMBGSVxnLLQcjeiUd54ol1YvUlLrryxhK5Skwr42b1ffgrmzm%2Fe30vH%2B5yO%2F%2Fl2hfndbtIX8cQ0yBqAR%2BMkXcTzeOVH4zL1drpNnmh%2BVpRFkni7CoqYlcXKCr%2FH8ZoX%2BZem6QvvWn%2BbxlnSIl1GPJfuwvSLEn7IT%2FWzw2N3O37mIvIiIqs0efmiRpRaebSsVsREvXqb8WbcxNtkSjsaSow9P5nTtKtBWbm8EZUL8B65p%2FGSZveTFUho5Kfhkz7KfD5Y57Jc2Z9ZgHfpK7qXn%2FfJj7b8SoOsHW5Qfry1iuP1IGt5Ly9VRG%2BK41g5ToqjUxSzRMWiSh72iiPm6dWxVA6OvHefF2FKP679oqGfM77QB0Jr5zzRJKW7zubkuTa%2FBU42SGDvuYSuTFsosPWsE3UAqbdJj68O3OzFFwGFL9yIr%2BtxARBbgEgCZ1QcbwWgsuOdAiJHYHCowPNGCQ%2BBoAxb4GCGao1yCTA7HC7EEC42KLiQZrjgAiIMFkgDDQfERMGNOvkwDLkKksZ8TsOsGBLwYuFKxRG%2F7rnxRFxoeEJwpq0SQw9KzrHxZBviyQGFJ7sZTyMFCnYxxokAUBYevQ%2Bp52BooPEOwMyJph3cPe9oSD0cNI4haFxQoIG64h2eSiuYdtMQVDc5B2oFrPCWpfCWJVSC5DNZRSW%2FisgAoQ%2BqVCcpbB%2FVycTjL3xcoCiCtq51DcGHYS1s3Ub03ZACEUMFI6qqVhezN%2B9DZbjegdCzTybNhz3yjJA3NEQeQqCQN2ye94gy1yFldhqBg4hczZ4PIoco8R8RImLo78cIMLNGg10Du1H2BLebtb%2FSut79b5tb2G6ncRQnA5LPS8n88ScH5RMUzhfDWvBDHs7b1JrFq%2FRq5i%2FD6IVVy87lL9dFJiF23t80eqJpOPWzzBXN7qRaQJ63q6pxHT8J%2Fai9%2FMZfba42NAln%2BiNsijGaPwCy1zuZl6E7vfKjcL5imRGdpTLz0Z9%2BnRcguKq0G3YcecVK%2BEPZ2Flonv8%2FXvphJJSAJwKTmkQf6%2FsTrCuzocF6k53r7ETn2eCIDupaFhzRmZqXECwZLu67JzrgRFdotElt3xUrixl1wWMrdgms7MpWDBS3SkVbOfkdVIpElgWNI6X7zWVx5Bu4ztQ2iGAZB1GDdbDnOqhcp%2FooTHQNJ3eCOlwcKntA0lQ7VqpYmigsd29ZijyVrW%2FvthVrVJ9Iv395M3L7CSjNIvdAmj2Z8UvS02XR7BmkqKmpGcGyNaMGY3NPz8eg5zYtWVkd11Wk8A3DOoPi%2Bs78uHY2wZG88LDFM01cGtdJWr2fikGtwrVjhacnNY%2Bc06z3Z2EUjdjwy%2BqS2Yy602k%2BVtMk%2FkqVnGB4%2FWh1QvoVvEwwOF7GF8nLb%2BBXU3M3guWWgBoM3j2%2FQpW%2FhsT2dmJ%2BjcUVtma1D7UTnc5rGI4p9TVejqVmLWXqg6ZSj65ZTb0YCDDN2uDG0HMqVE7tcMNSxeXetyOyv25dKylZ9U0hCklLdTvUpbCsaOnnV0W5wytqCniv67pkenWWsPa5rhNxlTc62QCdMdyqwc0x9XuzTjVlYDhTxveR09h0NxnD2k3G7bvJQfjUyPwNtMRr5Pw4yN8xFqvGsiBh%2F9Qka%2BZP9TPXJ4BRMWJlah7ltF2l6LZ7ap3Ausj8mHx9Q1opuGrWqmwnjDRuwhUqVzdhmRJuoSeZzLqnllz08zG4jM9%2Bd9MMPTQ5DrlVnQnO71iIe68pQ0o03cHFsNQwbt%2FB7QmuSZDWrfdI01DY%2BuvXP5skmP1azoLGTjU7%2FvnpiZCLpKc30IzpRibBsGimfSOzp5nTuA0plhYWZV%2F4YC%2BCZuFLZ6Oqufv8bGTbF8lG318sCXLa%2F6UCWJ6X4r7fJYu1LecqHjGMejxt%2FYa7X5mU%2B2FqlTOyT8UMHDjUC%2FLd0JoZ2MOPxHWPxFceOPVk9%2B45hnxlqrpEV0Hhq958fEGmDvMP2rDcycDUKnBRG%2FryxfazmYBtp%2BdFM170LlTHeb2O63XckXQcOb%2BOw2D4SndZkRT1KpcVdDK%2BEj2wf%2FfMA8VX4r57HXcBOo6bQFUJpr6mZ%2Fi5z7H2BV6mDTs27c4t4qqkCEDEwXHKAE6Kxp8NheUbbfevBv7YpPg%2BHdwwPCur3X9RwpBKTV2ibWD6suXLbu%2F2JwFqIDv%2Fh2Ltfk%2Bi35M4dDhXbeHIPt2ckUXL36cp8pRf%2BSHj%2FwE%3D)

В чем сложность?

Нужно для каждого сайта сделать платежный модуль, который будет формировать стоимость заказа, наименование и другие параметры и уже потом отправляет на платформу Аватар.

Если сделать так чтобы пользователь вносил номер заказа и его стоимость то можно сделать универсальный виджет и на стороне сайта доделывать ничего не надо.

## Получение уведомления об успешной оплате

Обычно на сайте ведется учет покупок и это означает что сайт должен получить уведомление об успешной оплате товара. Для этого на аватаре надо прописать куда отправить это уведомление.

## Картинки

![](https://cloud1.cloud999.ru/upload/cloud/15917/34629_f3cKhFOJkz.png)
