<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;
use app\models\UnionCategory;
use yii\db\Query;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \avatar\models\forms\shop\CatalogItem */
/* @var $school \common\models\school\School */

$this->title = $model->name;

?>
<div class="container">
    <div class="col-lg-12">
        <h1 class="page-header"><?= Html::encode($this->title) ?></h1>
    </div>

    <?php \avatar\widgets\SchoolMenu::begin(['school' => $school]) ?>
        <?php if (Yii::$app->session->hasFlash('form')) { ?>
            <div class="alert alert-success">
                Успешно обновлено.
            </div>

            <?php $id = Yii::$app->session->getFlash('form'); ?>
            <p>
                <a href="<?= \yii\helpers\Url::to(['cabinet-school-shop-catalog/index', 'id' => $school->id]) ?>" class="btn btn-default">Весь каталог</a>
                <a href="<?= \yii\helpers\Url::to(['cabinet-school-shop/index', 'id' => $school->id]) ?>" class="btn btn-default">Интернет магазин</a>
            </p>

        <?php  } else { ?>

            <div class="row">
                <div class="col-lg-5">
                    <?php $form = ActiveForm::begin([
                        'id'      => 'contact-form',
                        'options' => ['enctype' => 'multipart/form-data']
                    ]); ?>
                    <?= $form->field($model, 'name') ?>
                    <?= $form->field($model, 'pay_config_id')->dropDownList(
                        ArrayHelper::map(
                            \common\models\PaySystemConfig::find()
                                ->where(['school_id' => $school->id])
                                ->all(),
                            'id',
                            'name'
                        ), ['prompt' => '- Ничего не выбрано -']
                    ) ?>

                    <hr>
                    <div class="form-group">
                        <?= Html::submitButton('Обновить', [
                            'class' => 'btn btn-default',
                            'name'  => 'contact-button',
                            'style' => 'width:100%',
                        ]) ?>
                    </div>
                    <?php ActiveForm::end(); ?>
                </div>
            </div>

        <?php } ?>
    <?php \avatar\widgets\SchoolMenu::end() ?>

</div>
