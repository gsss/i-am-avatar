<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;
use app\models\UnionCategory;
use yii\db\Query;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \avatar\models\forms\shop\CatalogItem */
/* @var $school \common\models\school\School */

$this->title = 'Добавить в каталог';

?>
<div class="container">

    <?php \avatar\widgets\SchoolMenu::begin(['school' => $school]) ?>
    <h1 class="page-header"><?= Html::encode($this->title) ?></h1>

    <?php \yii\widgets\Pjax::begin(); ?>
    <?php if (Yii::$app->session->hasFlash('form')) { ?>

        <div class="alert alert-success">
            Успешно добавлено.
        </div>

        <?php $id = Yii::$app->session->getFlash('form'); ?>
        <p>
            <a href="<?= \yii\helpers\Url::to(['cabinet-school-shop-catalog/index', 'id' => $school->id]) ?>" class="btn btn-default" data-pjax="0">Весь каталог</a>
            <a href="<?= \yii\helpers\Url::to(['cabinet-school-shop/index', 'id' => $school->id]) ?>" class="btn btn-default" data-pjax="0">Интернет магазин</a>
        </p>

    <?php  } else { ?>


        <div class="row">
            <div class="col-lg-5">
                <?php $form = ActiveForm::begin([
                    'options' => ['enctype' => 'multipart/form-data','data-pjax' => true,]
                ]); ?>
                <?= $form->field($model, 'name') ?>
                <?= $form->field($model, 'pay_config_id')->dropDownList(
                        ArrayHelper::map(
                                \common\models\PaySystemConfig::find()
                                ->where(['school_id' => $school->id])
                                ->all(),
                                'id',
                                'name'
                        ), ['prompt' => '- Ничего не выбрано -']
                ) ?>


                <hr>
                <div class="form-group">
                    <?= Html::submitButton('Добавить', [
                        'class' => 'btn btn-default',
                        'name'  => 'contact-button',
                        'style' => 'width:100%',
                    ]) ?>
                </div>
                <?php ActiveForm::end(); ?>
            </div>
        </div>

    <?php } ?>
    <?php \yii\widgets\Pjax::end(); ?>

    <?php \avatar\widgets\SchoolMenu::end() ?>


</div>
