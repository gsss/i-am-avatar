<?php
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $user \common\models\UserAvatar */

$this->title = \yii\helpers\Html::encode($user->getName2());
?>

<div class="container" style="padding-bottom: 70px;">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header text-center">
                <?= $this->title ?>
            </h1>
            <p class="text-center">
                <img src="<?= $user->getAvatar() ?>" width="300" class="img-circle">
            </p>

        </div>
    </div>
</div>


