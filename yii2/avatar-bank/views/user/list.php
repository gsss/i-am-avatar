<?php
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $user \common\models\UserAvatar */

$this->title = 'Учителя';
?>

<div class="container" style="padding-bottom: 70px;">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header text-center">
                <?= $this->title ?>
            </h1>

            <?php \yii\widgets\Pjax::begin(); ?>
            <?php
            $this->registerJS(<<<JS
$('[data-toggle="tooltip"]').tooltip();

$('.rowTable').click(function() {
    window.location = '/user/' +  $(this).data('id');
});
JS
            );
            $sort = new \yii\data\Sort([
                'defaultOrder' => ['created_at' => SORT_DESC],
            ])
            ?>
            <?= \yii\grid\GridView::widget([
                'dataProvider' => new \yii\data\ActiveDataProvider([
                    'query'      => \common\models\UserAvatar::find()->where(['is_master' => 1])
                    ,
                    'pagination' => [
                        'pageSize' => 20,
                    ],
                    'sort' => $sort
                ]),
                'tableOptions' => [
                    'class' => 'table table-striped table-hover',
                ],
                'rowOptions'   => function ($item) {
                    $data = [
                        'data'  => ['id' => $item['id']],
                        'role'  => 'button',
                        'class' => 'rowTable'
                    ];
                    return $data;
                },
                'columns'      => [
                    'id',
                    [
                        'header'  => 'Фото',
                        'content' => function ($item) {
                            $i = ArrayHelper::getValue($item, 'avatar', '');
                            if ($i == '') return '';

                            return Html::img(\common\widgets\FileUpload7\FileUpload::getFile($i, 'crop'), [
                                'class'  => "img-circle",
                                'width'  => 80,
                                'height' => 80,
                                'style'  => 'margin-bottom: 0px;',
                            ]);
                        }
                    ],
                    [
                        'header'  => 'Имя',
                        'attribute'  => 'name_first',
                    ],
                    [
                        'header'  => 'Фамилия',
                        'attribute'  => 'name_last',
                    ],
                ],
            ]) ?>
            <?php \yii\widgets\Pjax::end(); ?>

        </div>
    </div>
</div>


