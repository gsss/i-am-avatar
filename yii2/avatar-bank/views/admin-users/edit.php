<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;
use app\models\UnionCategory;
use yii\db\Query;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model \avatar\models\forms\UserAvatar */

$this->title = $model->name_first;
?>
<div class="container">
    <div class="col-lg-12">
        <h1  class="page-header text-center"><?= Html::encode($this->title) ?></h1>

        <?php if (Yii::$app->session->hasFlash('form')): ?>

            <div class="alert alert-success">
                Успешно обновлено.
            </div>

        <?php else: ?>


            <div class="row">
                <div class="col-lg-6 col-lg-offset-3">
                    <?php $form = ActiveForm::begin([
                        'options' => ['enctype' => 'multipart/form-data']
                    ]); ?>
                    <?= $model->field($form, 'name_first') ?>
                    <?= $model->field($form, 'name_last') ?>
                    <?= $model->field($form, 'avatar') ?>
                    <?= $model->field($form, 'email') ?>
                    <?= $model->field($form, 'email_is_confirm') ?>
                    <?= $model->field($form, 'phone') ?>
                    <?= $model->field($form, 'phone_is_confirm') ?>

                    <hr>
                    <div class="form-group">
                        <?= Html::submitButton('Обновить', [
                            'class' => 'btn btn-default',
                            'name'  => 'contact-button',
                            'style' => 'width:100%',
                        ]) ?>
                    </div>
                    <?php ActiveForm::end(); ?>
                </div>
            </div>
        <?php endif; ?>
    </div>
</div>
