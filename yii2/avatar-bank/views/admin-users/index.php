<?php

/** $this \yii\web\View  */

use yii\helpers\Url;
use app\services\GsssHtml;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use app\services\GetArticle\Collection;

$this->title = 'Все пользователи';

?>
<style>
    .textDecorated {
        text-decoration-line: underline;
        text-decoration-style: dotted;
    }
</style>
<div class="container">
    <div class="col-lg-12">
        <h1 class="page-header"><?= Html::encode($this->title) ?></h1>

        <?php \yii\widgets\Pjax::begin(); ?>
        <?php
        $this->registerJS(<<<JS
$('.buttonDelete').click(function (e) {
    e.preventDefault();
    e.stopPropagation();
    if (confirm('Подтвердите удаление')) {
        var button = $(this);
        var id = $(this).data('id');
        ajaxJson({
            url: '/admin-users/delete' + '?' + 'id' + '=' + id,
            success: function (ret) {
                infoWindow('Успешно', function() {
                    button.parent().parent().remove();
                });
            }
        });
    }
});

$('.buttonRole').click(function (e) {
    e.preventDefault();
    e.stopPropagation();
    window.location = '/admin-users/permissions' + '?' + 'id' + '=' + $(this).data('id');
});

$('.buttonWalletList').click(function (e) {
    e.preventDefault();
    e.stopPropagation();
    window.location = '/admin-users/wallet-list' + '?' + 'id' + '=' + $(this).data('id');
});

$('[data-toggle="tooltip"]').tooltip();

$('.rowTable').click(function() {
    window.location = '/admin-users/edit' + '?' + 'id' + '=' + $(this).data('id');
});
JS
        );
        ?>
        <?php
        $model = new \avatar\models\search\UserAvatar();
        $provider = $model->search(Yii::$app->request->get());
        ?>
        <?= \yii\grid\GridView::widget([
            'dataProvider' => $provider,
            'filterModel'  => $model,
            'tableOptions' => [
                'class' => 'table table-striped table-hover',
            ],
            'rowOptions'   => function ($item) {
                $data = [
                    'data'  => ['id' => $item['id']],
                    'role'  => 'button',
                    'class' => 'rowTable',
                ];

                return $data;
            },
            'columns'      => [
                [
                    'attribute'     => 'id',
                    'headerOptions' => [
                        'style' => 'width: 8%;',
                    ],
                ],
                [
                    'header'        => 'Картинка',
                    'headerOptions' => [
                        'style' => 'width: 10%;',
                    ],
                    'content'       => function (\common\models\UserAvatar $item) {
                        $i = $item->getAvatar();
                        $params = [
                            'class'  => "img-circle",
                            'width'  => 80,
                            'height' => 80,
                            'style'  => ['margin-bottom' => '0px'],
                        ];
                        if (\yii\helpers\StringHelper::endsWith($i, '/images/iam.png')) {
                            $params['style']['opacity'] = '0.3';
                            $params['style']['border'] = '1px solid #888';
                        }
                        if (isset($params['style'])) {
                            if (is_array($params['style'])) {
                                $params['style'] = Html::cssStyleFromArray($params['style']);
                            }
                        }

                        return Html::img($i, $params);
                    },
                ],
                [
                    'attribute'     => 'email',
                    'headerOptions' => [
                        'style' => 'width: 24%;',
                    ],
                ],
                [
                    'attribute'     => 'name_first',
                    'header'        => 'Имя',
                    'headerOptions' => [
                        'style' => 'width: 20%;',
                    ],
                ],
                [
                    'attribute'     => 'name_last',
                    'header'        => 'Фамилия',
                    'headerOptions' => [
                        'style' => 'width: 20%;',
                    ],
                ],
                [
                    'header'        => 'Зарег.',
                    'headerOptions' => [
                        'style' => 'width: 6%;',
                    ],
                    'content'       => function ($item) {
                        $v = \yii\helpers\ArrayHelper::getValue($item, 'registered_ad', 0);
                        if ($v == 0) return '';

                        return Html::tag('span', \cs\services\DatePeriod::back($v, ['isShort' => true]), [
                            'data'  => ['toggle' => 'tooltip'],
                            'title' => Yii::$app->formatter->asDatetime($v),
                            'class' => 'textDecorated',
                        ]);
                    },
                ],
                [
                    'header'        => 'Создан',
                    'headerOptions' => [
                        'style' => 'width: 6%;',
                    ],
                    'content'       => function ($item) {
                        $v = \yii\helpers\ArrayHelper::getValue($item, 'created_at', 0);
                        if ($v == 0) return '';

                        return Html::tag('span', \cs\services\DatePeriod::back($v, ['isShort' => true]), [
                            'data'  => ['toggle' => 'tooltip'],
                            'title' => Yii::$app->formatter->asDatetime($v),
                            'class' => 'textDecorated',
                        ]);
                    },
                ],
                [
                    'header'        => 'Роли',
                    'headerOptions' => [
                        'style' => 'width: 6%;',
                    ],
                    'content'       => function ($item) {

                        return Html::button('Роли', [
                            'class' => 'btn btn-default btn-xs buttonRole',
                            'data'  => [
                                'id' => $item['id'],
                            ],
                        ]);
                    },
                ],
            ],
        ]) ?>
        <?php \yii\widgets\Pjax::end(); ?>
    </div>
</div>