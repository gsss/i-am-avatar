<?php

/** $this \yii\web\View  */
/** @var $school \common\models\school\School */

use yii\helpers\Url;
use app\services\GsssHtml;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use app\services\GetArticle\Collection;

$this->title = 'Все монеты';



?>

<div class="container">


    <h1 class="page-header"><?= Html::encode($this->title) ?></h1>

    <p>
        <a href="<?= Url::to(['add', 'id' => $school->id]) ?>" class="btn btn-default">Добавить</a>
    </p>

    <?php \yii\widgets\Pjax::begin(); ?>

    <?php
    $this->registerJS(<<<JS
$('[data-toggle="tooltip"]').tooltip();

$('.rowTable').click(function() {
    window.location = '/cabinet-company-coin/view' + '?' + 'id' + '=' + $(this).data('id');
});
JS
    );
    ?>

    <?= \yii\grid\GridView::widget([
        'dataProvider' => new \yii\data\ActiveDataProvider([
            'query'      => \common\models\SchoolCoin::find()
                ->where(['school_id' => $school->id])
            ,
            'pagination' => [
                'pageSize' => 20,
            ],
        ]),
        'tableOptions' => [
            'class' => 'table table-striped table-hover',
        ],
        'rowOptions'   => function ($item) {
            $data = [
                'data'  => ['id' => $item['id']],
                'role'  => 'button',
                'class' => 'rowTable',
            ];
            return $data;
        },
        'columns'      => [
            'id',
            [
                'header'  => 'Вознаграждение',
                'content' => function ($item) {
                    $c = \common\models\piramida\Currency::findOne($item['currency_id']);

                    return join('<br>', [
                        $c->name,
                        Html::tag('span', $c->code, ['class' => 'label label-info']),
                        'Знаков: '. $c->decimals,
                        'M1: '. \Yii::$app->formatter->asDecimal($c->amount, 0),
                        'Главный Кошелек: '. $item->wallet_id,
                    ]);
                },
            ],
            [
                'header'  => 'Бюджет',
                'content' => function ($item) {
                    $c = \common\models\piramida\Currency::findOne($item['budjet_currency_id']);
                    if (is_null($c)) return '';

                    return join('<br>', [
                        $c->name,
                        Html::tag('span', $c->code, ['class' => 'label label-info']),
                        'Знаков: '. $c->decimals,
                        'M1: '. $c->amount,
                    ]);
                },
            ],
        ],
    ]) ?>
    <?php \yii\widgets\Pjax::end(); ?>


</div>

<div class="modal fade" id="modalInfo" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="myModalLabel">Информация</h4>
            </div>
            <div class="modal-body">
                Успешно!
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
            </div>
        </div>
    </div>
</div>