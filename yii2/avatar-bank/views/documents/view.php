<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;
use app\models\UnionCategory;

/* @var $this yii\web\View */
/* @var $document \common\models\UserDocument */

$this->title = $document->name;

$v = \yii\helpers\ArrayHelper::getValue($document, 'file', 0);
$file = pathinfo($v);

$html = Html::img('/images/controller/cabinet/documents/file.png', ['width' => 50]) . '.' . strtoupper($file['extension']);
$provider = new \avatar\modules\ETH\ServiceEtherScan();

?>
<div class="container">
    <div class="col-lg-12">
        <h1 class="page-header text-center"><?= Html::encode($document->name) ?></h1>

        <table class="table table-hover table-striped">
            <tr>
                <td style="text-align: right; width: 50%;">ID</td>
                <td><?= $document->id ?></td>
            </tr>
            <tr>
                <td style="text-align: right;">Наименование</td>
                <td><?= $document->name ?></td>
            </tr>
            <tr>
                <td style="text-align: right;">Hash</td>
                <td><?= Html::tag('code', $document->hash) ?></td>
            </tr>
            <tr>
                <td style="text-align: right;">TXID</td>
                <td><?= Html::tag('code', Html::a($document->txid, $provider->getUrl('/tx/' . $document->txid), ['target' => '_blank'])) ?></td>
            </tr>
            <tr>
                <td style="text-align: right;">Data</td>
                <td><?= ($document->data) ? Html::tag('pre', $document->data) : '' ?></td>
            </tr>
            <tr>
                <td style="text-align: right;">Добавлен</td>
                <td><?= Yii::$app->formatter->asDatetime($document->created_at, 'php:c') ?></td>
            </tr>
            <tr>
                <td style="text-align: right;">Файл</td>
                <td><a href="<?= Url::to(['documents/download', 'id' => $document->id]) ?>"><?= $html ?></td>
            </tr>
            <tr>
                <td style="text-align: right;"></td>
                <td><img src="/images/controller/cabinet/documents-view/100pctauthentic_1446253057.jpg" width="200"
                         class="img-circle"></td>
            </tr>
            <tr>
                <td style="text-align: right;"></td>
                <td></td>
            </tr>
            <tr>
                <td style="text-align: right;"></td>
                <td><h3>Подписи</h3></td>
            </tr>
            <?php /** @var $signature \common\models\UserDocumentSignature */ ?>
            <?php foreach (\common\models\UserDocumentSignature::find()->where(['document_id' => $document->id])->all() as $signature) { ?>
                <tr>
                    <td>

                    </td>
                    <td>
                        <img src="/images/controller/cabinet/documents-view/blockchain-blog-620x347.png" width="100"
                             style="border-radius: 10px;"/>
                    </td>
                </tr>
                <tr>
                    <td style="text-align: right;">
                        Address
                    </td>
                    <td>
                        <?= Html::tag('code', Html::a($signature->member, $provider->getUrl('/address/' . $signature->member), ['target' => '_blank'])) ?>
                    </td>
                </tr>
                <tr>
                    <td style="text-align: right;">
                        TXID
                    </td>
                    <td>
                        <?= Html::tag('code', Html::a($signature->txid, $provider->getUrl('/tx/' . $signature->txid), ['target' => '_blank'])) ?>
                    </td>
                </tr>
                <tr>
                    <td style="text-align: right;">
                        Время
                    </td>
                    <td>
                        <?php
                        $v = \yii\helpers\ArrayHelper::getValue($signature, 'created_at', 0);
                        if ($v == 0) {
                            echo '';
                        } else {
                            echo Html::tag('abbr', \cs\services\DatePeriod::back($v, ['isShort' => true]), ['data' => ['toggle' => 'tooltip'], 'title' => Yii::$app->formatter->asDatetime($v, 'php:c')]);
                        }
                        ?>
                    </td>
                </tr>
                <tr>
                    <td>

                    </td>
                    <td>

                    </td>
                </tr>
            <?php } ?>
        </table>

        <center>
            <a href="<?= Url::to(['documents/download-pdf', 'id' => $document->id]) ?>" class="btn btn-primary">
                <i class="glyphicon glyphicon-download-alt"></i>
                Скачать</a>
        </center>
    </div>
</div>



