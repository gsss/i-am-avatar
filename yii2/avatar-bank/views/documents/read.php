<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;
use app\models\UnionCategory;

/* @var $this yii\web\View */
/* @var $model \avatar\models\forms\DocumentsRead */

$this->title = 'Чтение документа';

$this->registerJs(<<<JS
var functionRead = function(e) {
    var b = $(this);
    b.off('click');
    b.html($('<i>', {class: 'fa fa-spinner fa-spin fa-fw'}));
    b.attr('disabled', 'disabled');
    ajaxJson({
        url: '/documents/read-ajax',
        data: $('#contact-form').serializeArray(),
        success: function(ret) {
            $('#step1').collapse('hide');
            $('#result').html(ret.html);
        },
        errorScript: function(ret) {
            switch (ret.id) {
                case 101:
                    var f = $('#contact-form');
                    $.each(ret.data,function(i,v) {
                        var name = v.name;
                        var value = v.value;
                        var o = f.find('#documentsread-'+ name);
                        var t = f.find('.field-documentsread-' + name);
                        t.addClass('has-error');
                        t.find('p.help-block-error').html(value.join('<br>')).show();
                    });

                    break;
            }
            b.on('click', functionRead);
            b.html('contact-form');
            b.removeAttr('disabled');
        }
    });
};

$('#contact-button').click(functionRead);
JS
);

/**
 */
?>
<div class="container">
    <div class="col-lg-12">
        <h1 class="page-header"><?= Html::encode($this->title) ?></h1>

    </div>
    <div class="col-lg-8">

        <div class="collapse in" id="step1">
            <?php $form = ActiveForm::begin([
                'id'      => 'contact-form',
            ]); ?>
            <?= Html::activeHiddenInput($model, 'id') ?>
            <?= $form->field($model, 'password')->passwordInput()->label('Пароль')->hint('Пароль от кабинета') ?>


            <hr class="featurette-divider">
            <div class="form-group">
                <?= Html::button('Получить', [
                    'class' => 'btn btn-default',
                    'id'    => 'contact-button',
                    'style' => 'width: 100%;',
                ]) ?>
            </div>
            <?php ActiveForm::end(); ?>
        </div>

        <div id="result"></div>

    </div>
    <div class="col-lg-4">
        <?= $this->render('../cabinet/_menu') ?>
    </div>
</div>



