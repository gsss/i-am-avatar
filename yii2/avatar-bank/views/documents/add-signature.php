<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;
use app\models\UnionCategory;

/* @var $this yii\web\View */
/* @var $id int */

$this->title = 'Подпись документа';

$model = new \avatar\models\forms\UserDocumentAddSignature(['id' => $id]);

?>
<div class="container">
    <div class="col-lg-12">
        <h1 class="page-header"><?= Html::encode($this->title) ?></h1>

        <div class="collapse in" id="step1">
            <?php $form = ActiveForm::begin([
                'id'      => 'formSign',
            ]); ?>

            <?= Html::activeHiddenInput($model, 'id'); ?>
            <?=
            $form->field($model, 'password')
                ->passwordInput()
                ->hint('Пароль от кабинета')
            ?>
            <?php ActiveForm::end(); ?>

            <hr>
            <div class="form-group">
                <?= Html::button('Подписать', [
                    'class' => 'btn btn-default',
                    'id'    => 'buttonSign',
                    'style' => 'width: 100%;',
                ]) ?>
            </div>

            <?php
            $this->registerJs(<<<JS
var functionSign = function(e) {
    var b = $(this);
    b.off('click');
    b.html($('<i>', {class: 'fa fa-spinner fa-spin fa-fw'}));
    b.attr('disabled', 'disabled');
    ajaxJson({
        url: '/documents/add-signature-ajax',
        data: $('#formSign').serializeArray(),
        success: function(ret) {
            $('#step1').collapse('hide');
            $('#step2').append(
                $('<p>').html(
                    $('<code>').html(ret.txid)
                )
            );
            $('#step2').collapse('show');
        },
        errorScript: function(ret) {
            var f = $('#formSign');
            switch (ret.id) {
                case 101:
                    var o = f.find('#userdocumentaddsignature-' + 'password');
                    var t = f.find('.field-userdocumentaddsignature-' + 'password');
                    t.addClass('has-error');
                    t.find('p.help-block-error').html('Не загружены данные').show();
                    break;
                    
                case 102:
                    $.each(ret.data,function(i,v) {
                        var name = v.name;
                        var value = v.value;
                        var o = f.find('#userdocumentaddsignature-' + 'password');
                        var t = f.find('.field-userdocumentaddsignature-' + 'password');
                        t.addClass('has-error');
                        t.find('p.help-block-error').html(value.join('<br>')).show();
                    });

                    break;
            }
            b.on('click', functionSign);
            b.html('Подписать');
            b.removeAttr('disabled');
        }
    });
};
$('#buttonSign').click(functionSign);

$('#formSign .form-control').on('click', function() {
    var o = $(this);
    var p = o.parent();
    if (p.hasClass('input-group')) {
        p = p.parent();
    }
    p.removeClass('has-error');
    p.find('p.help-block-error').hide();
});
JS
            );
            ?>
        </div>
        <div class="collapse" id="step2">






        </div>
</div>



