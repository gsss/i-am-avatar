<?php

/** @var \yii\web\View $this */
/** @var int $id идентификатор комнаты */
/** @var \common\models\chat\Room $roomMain  */
/** @var array $rooms chat_room.*  */

\common\assets\SocketIO\Asset::register($this);

?>

<?php

/** @var \common\models\UserAvatar $user */
$user = Yii::$app->user->identity;
$user_id = Yii::$app->user->id;
$user_name = $user->getName2();
$user_avatar = $user->getAvatar();

// Список комнат к которым я подключаюсь
$roomsIds = \yii\helpers\ArrayHelper::getColumn($rooms, 'id');
$roomsIdsJson = \yii\helpers\Json::encode($roomsIds);
$server = \common\assets\SocketIO\Asset::getHost();


$tet_a_tet = ($roomMain->type_id == \common\models\chat\Room::TYPE_ID_TET_A_TET)? 1 : 0;
$this->registerJs(<<<JS

var socket = io.connect('{$server}');

function chatScroll() {
    var div = $('.chatContainerScroll');
    div.scrollTop(div.prop('scrollHeight'));
    console.log('chatScroll');
}

chatScroll();

var name;
let roomName = {$id};
let rooms = {$roomsIdsJson};

// объявляю чат сервису что я добавляюсь в комнату roomName
var i;
for (i = 0; i < rooms.length; i++) {
    socket.emit('room-new-user', rooms[i], '{$user_id}');
}

$('.buttonSend').click(function(e) {
    e.preventDefault();
    if ($('.textareaChat').val().length > 0) {
        appendMessage();
    }           
});

/**
* @param object data
* {
*     user: {id: int, name: str, avatar: str }
*     message: {}
*     room: int
* }
*/
socket.on('room-send-message', data => {
    console.log(data);
    if (data.room == roomName) {
        appendMessageGet(data.user, data.message);
        var tet_a_tet = {$tet_a_tet};
        if (tet_a_tet) {
            console.log(['room-confirm', roomName, data ]);
            console.log(socket.emit('room-send-message1', roomName, data));
        }
    } else {
        var o = $('.js-personChat[data-id=' + data.room + ']').find('.badgeMessages');
        var c = o.data('counter');
        c++;
        o.html(c);
        o.data('counter', c);
    }
});


socket.on('room-send-message1', (p1,p2) => {
    console.log(["room-send-message1", p1,p2]);
    console.log(["room-send-message1_1", p1.user.message.message.id,$('.js-chatMessage[data-id='+p1.user.message.message.id+']')]);
    var dd = $('.js-chatMessage[data-id='+ p1.user.message.message.id+']').find('div.chat-hour').find('span.icon-done');
    dd.removeClass('icon-done').addClass('icon-done_all');
});

socket.on('user-connected', name => {
    console.log("user-connected: " + name +' вошел')
});

socket.on('user-disconnected', name => {
    console.log("user-disconnected: " + name +' вышел')
});

/**
* 
* @param user
* @param data
* {
*     message: {}
*     html: ''
* }
*/
function appendMessageGet(user, data) {
    // генерирую HTML
    var liObject = $('<li>', {class:'chat-left js-chatMessage', "data-id": data.message.id});
    var divText = $('<div>', {class:'chat-text'});
    divText.append($('<p>').html(data.html));
    divText.append($('<div>', {class: 'chat-hour'}).html(data.message.timeFormatted));
    var divAvatar = $('<div>', {class:'chat-avatar'});
    divAvatar.append($('<img>', {src: user.avatar, alt: user.name}));
    liObject.append(divAvatar);
    liObject.append(divText);
    
    // Добавляю HTML в чат
    $('.chat-box').append(liObject);

    // прокручиваю чат вниз
    chatScroll();
}

function appendMessage() {
    if ($('.textareaChat').val().length > 0) {
        ajaxJson({
            url: '/chat/send',
            data: {
                text: $('.textareaChat').val(),
                room_id: {$id},
                user_id: '{$user_id}',
                files: $('.textareaChat').attr('data-files')
            },
            success: function(ret) {
                // отправляю сообщение на сервер
                socket.emit('room-send-message', roomName, {id: {$user_id}, name: '{$user_name}', avatar: '{$user_avatar}' }, ret);
                
                // генерирую HTML
                var liObject = $('<li>', {class:'chat-right js-chatMessage', "data-id": ret.message.id});
                var divText = $('<div>', {class:'chat-text'});
                divText.append($('<p>').html(ret.html));
                divText.append($('<div>', {class: 'chat-hour'}).html(ret.message.timeFormatted + ' <span class="icon-done"></span>'));
                var divAvatar = $('<div>', {class:'chat-avatar'});
                divAvatar.append($('<img>', {src: '{$user_avatar}', alt: '{$user_name}'}));
                liObject.append(divText);
                liObject.append(divAvatar);
                
                // Добавляю HTML в чат
                $('.chat-box').append(liObject);
                
                // очищаю поле сообщения
                $('.textareaChat').val('');
                $('.textareaChat').attr('data-files','[]');
                $('#upload1 .fileUploadedUrl').html('');

                // прокручиваю чат вниз
                chatScroll();

                // Если я пишу в чат который в списке чатов не наверху находится то переношу его вверх
                var i1 = 0;
                $('li.js-personChat').each(function(i,e) {
                    console.log([i,e]);
                    // если это выделенный чат?
                    if ($(e).data('id') == {$id}) {
                        // если выделенный чат не наверху? 
                        if (i > 0) {
                            // ставлю наверх
                            var first = $('.users')[0].firstChild;
                            var n1 = $(e)[0];
                            var list = $('.users')[0];
                            
                            list.insertBefore(n1, first);
                        }
                    }
                }); 
            }
        });
    }
}


JS
);
?>

