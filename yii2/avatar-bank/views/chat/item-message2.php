<?php
/** @var \common\models\chat\Message $m */

?>
<?php $user = \common\models\UserAvatar::findOne($m->user_id); ?>
<?php if ($m->user_id == Yii::$app->user->id) { ?>
    <li class='chat-right js-chatMessage' data-id="<?= $m->id ?>">
        <div class='chat-text'>
            <?= $this->render('item-message', ['message' => $m]); ?>
            <div class='chat-hour'><?= Yii::$app->formatter->asTime($m->created_at) ?></div>
        </div>
        <div class='chat-avatar'>
            <img src="<?= $user->getAvatar() ?>" alt="<?= $user->getName2() ?>" />
        </div>
    </li>
<?php } else { ?>
    <li class='chat-left js-chatMessage' data-id="<?= $m->id ?>">
        <div class='chat-avatar'>
            <img src="<?= $user->getAvatar() ?>" alt="<?= $user->getName2() ?>" />
        </div>
        <div class='chat-text'>
            <?= $this->render('item-message', ['message' => $m]); ?>
            <div class='chat-hour'><?= Yii::$app->formatter->asTime($m->created_at) ?></div>
        </div>
    </li>
<?php } ?>
