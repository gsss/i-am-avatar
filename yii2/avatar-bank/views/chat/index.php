<?php
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */

$this->title = 'Чат';

$rooms =\common\models\chat\ListUser::find()
    ->innerJoin('chat_room', 'chat_room.id = chat_list.room_id')
    ->select([
        'chat_room.*'
    ])
    ->where(['chat_list.user_id' => Yii::$app->user->id])
    ->orderBy(['chat_room.last_message' => SORT_DESC])
    ->asArray()
    ->all()
;

$this->registerJs(<<<JS
$('.js-personChat').click(function(e) {
    window.location = '/chat/item?id=' + $(this).data('id');
})
JS
);
?>


<style>
    .chat
    {
        list-style: none;
        margin: 0;
        padding: 0;
    }

    .chat li
    {
        margin-bottom: 10px;
        padding-bottom: 5px;
        border-bottom: 1px dotted #B3A9A9;
    }

    .chat li.left .chat-body
    {
        margin-left: 60px;
    }

    .chat li.right .chat-body
    {
        margin-right: 60px;
    }


    .chat li .chat-body p
    {
        margin: 0;
        color: #777777;
    }

    .panel .slidedown .glyphicon, .chat .glyphicon
    {
        margin-right: 5px;
    }

    .panel-body
    {
        overflow-y: scroll;
        height: 600px;
    }

    ::-webkit-scrollbar-track
    {
        -webkit-box-shadow: inset 0 0 6px rgba(0,0,0,0.3);
        background-color: #F5F5F5;
    }

    ::-webkit-scrollbar
    {
        width: 30px;
        background-color: #F5F5F5;
    }

    ::-webkit-scrollbar-thumb
    {
        -webkit-box-shadow: inset 0 0 6px rgba(0,0,0,.3);
        background-color: #555;
    }

    .panel-primary > .panel-heading {
        background-image: linear-gradient(to bottom, #750f0b 0%, #750f0b 100%) !important;
    }

    .panel-primary {
        border-color: #750f0b !important;
    }

    .btn-warning {
        background-image: linear-gradient(to bottom, #5cb85c 0%, #5cb85c 100%);
    }

    .btn-warning:hover, .btn-warning:focus {
        background-color: #5cb85c !important;
        background-position: 0 -15px;
    }

    .btn-warning:hover {
        color: #fff;
        background-color: #5cb85c !important;
        border-color: #5cb85c !important;
    }

</style>

<div class="main-container">


    <!-- Page header start -->
    <div class="page-header">
        <ol class="breadcrumb">
            <li class="breadcrumb-item">ДОМ</li>
            <li class="breadcrumb-item active">Чат</li>
        </ol>

        <ul class="app-actions">
            <li>
                <a href="#" id="reportrange">
                    <span class="range-text"></span>
                    <i class="icon-chevron-down"></i>
                </a>
            </li>
            <li>
                <a href="#" data-toggle="tooltip" data-placement="top" title="" data-original-title="Print">
                    <i class="icon-print"></i>
                </a>
            </li>
            <li>
                <a href="#" data-toggle="tooltip" data-placement="top" title="" data-original-title="Download CSV">
                    <i class="icon-cloud_download"></i>
                </a>
            </li>
        </ul>
    </div>
    <!-- Page header end -->


    <!-- Content wrapper start -->
    <div class="content-wrapper p-0">


        <!-- Row start -->
        <div class="row no-gutters">
            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                <div class="chat-section">
                    <!-- Row start -->
                    <div class="row no-gutters">
                        <div class="col-xl-3 col-lg-3 col-md-4 col-sm-2 col-3">
                            <div class="users-container">
                                <div class="chat-search-box">
                                    <div class="input-group">
                                        <input class="form-control" placeholder="Search" />
                                        <div class="input-group-btn">
                                            <button type="button" class="btn btn-primary">
                                                <i class="icon-magnifying-glass"></i>
                                            </button>
                                        </div>
                                    </div>
                                </div>
                                <div class="usersContainerScroll">
                                    <ul class="users">
                                        <?php /** @var  \common\models\chat\Room $room */ ?>
                                        <?php foreach ($rooms as $room) { ?>
                                            <?php
                                            if ($room['type_id'] == \common\models\chat\Room::TYPE_ID_GROUP) {
                                                $g = \common\models\chat\Group::findOne(['room_id' => $room['id']]);
                                                $name = $g->name;
                                                $avatar = \iAvatar777\widgets\FileUpload7\FileUpload::getFile($g->image, 'crop');
                                            } else {
                                                $tet = \common\models\chat\Tet::findOne(['room_id' => $room['id']]);
                                                if (Yii::$app->user->id == $tet['user1_id']) {
                                                    $user = \common\models\UserAvatar::findOne($tet['user2_id']);
                                                } else {
                                                    $user = \common\models\UserAvatar::findOne($tet['user1_id']);
                                                }
                                                $name = $user->getName2();
                                                $avatar = $user->getAvatar();
                                            }
                                            ?>
                                            <li class="person js-personChat" data-id="<?= $room['id'] ?>">
                                                <div class="user">
                                                    <img src="<?= $avatar ?>" alt="<?= $name ?>" />
                                                </div>
                                                <p class="name-time">
                                                    <span class="name"><?= $name ?></span>
                                                    <span class="time"><?= \cs\services\DatePeriod::back($room['last_message'], ['isShort' => true,]) ?></span>
                                                </p>
                                            </li>
                                        <?php } ?>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="col-xl-9 col-lg-9 col-md-8 col-sm-10 col-9">
                            <div class="active-user-chatting">
                                <div class="active-user-info">
                                    <img src="img/user.png" class="avatar" alt="avatar" />
                                    <div class="avatar-info">
                                        <h5>Amy Hood</h5>
                                        <div class="typing">Typing ...</div>
                                    </div>
                                </div>
                                <div class="chat-actions">
                                    <a href="#" data-toggle="modal" data-target="#videoCall">
                                        <i class="icon-video"></i>
                                    </a>
                                    <a href="#" data-toggle="modal" data-target="#audioCall">
                                        <i class="icon-phone1"></i>
                                    </a>
                                </div>

                                <!-- Audio Call Modal -->
                                <div class="modal fade" id="audioCall" tabindex="-1" role="dialog" aria-labelledby="audioCallLabel" aria-hidden="true">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                            <div class="modal-body">
                                                <div class="call-container">
                                                    <div class="current-user">
                                                        <img src="img/user24.png" alt="Avatar" >
                                                    </div>
                                                    <h5 class="calling-user-name">
                                                        Amy Hood <span class="calling">Calling...</span>
                                                    </h5>
                                                    <div class="calling-btns">
                                                        <button class="btn btn-secondary" data-dismiss="modal">
                                                            <i class="icon-x"></i>
                                                        </button>
                                                        <button class="btn btn-primary">
                                                            <i class="icon-phone1"></i>
                                                        </button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <!-- Video Call Modal -->
                                <div class="modal fade" id="videoCall" tabindex="-1" role="dialog" aria-labelledby="videoCallLabel" aria-hidden="true">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                            <div class="modal-body">
                                                <div class="call-container">
                                                    <div class="current-user">
                                                        <img src="img/user22.png" alt="Avatar" >
                                                    </div>
                                                    <h5 class="calling-user-name">
                                                        Zhenya Rynzhuk <span class="calling">Calling...</span>
                                                    </h5>
                                                    <div class="calling-btns">
                                                        <button class="btn btn-secondary" data-dismiss="modal">
                                                            <i class="icon-x"></i>
                                                        </button>
                                                        <button class="btn btn-primary">
                                                            <i class="icon-video"></i>
                                                        </button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                            <div class="chat-container">
                                <div class="chatContainerScroll">
                                    <ul class="chat-box">



                                    </ul>
                                </div>
                                <div class="chat-form">
                                    <div class="form-group">
                                        <textarea class="form-control" placeholder="Type your message here..."></textarea>
                                        <button class="btn btn-primary">
                                            <i class="icon-send"></i>
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Row end -->
                </div>
            </div>
        </div>
        <!-- Row end -->


    </div>
    <!-- Content wrapper end -->


</div>


