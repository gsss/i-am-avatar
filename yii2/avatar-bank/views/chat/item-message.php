<?php
/** @var \common\models\chat\Message $message */

$data = \yii\helpers\Json::decode($message->text);
$files = [];
if (isset($data['files'])) {
    $files = $data['files'];
}
if (!is_array($files)) {
    $files = \yii\helpers\Json::decode($files);
}

?>
<p>
    <?= nl2br(\yii\helpers\Json::decode($message->text)['text']) ?>
    <?php if (count($files) > 0) {?>
        <br>

    <?php }?>
    <?php foreach ($files as $file) { ?>
        <?php
        $pathinfo = pathinfo($file);
        if (in_array(strtolower($pathinfo['extension']), ['png', 'jpg', 'jpeg'])) {
            $crop = \iAvatar777\widgets\FileUpload7\FileUpload::getFile($file, 'crop');
            $img = \yii\helpers\Html::img($crop, ['width' => 200]);
        } else {
            $crop = 'https://image.flaticon.com/icons/svg/55/55025.svg';
            $img = \yii\helpers\Html::img($crop, ['width' => 200]);
        }
        ?>
        <a href="<?= $file ?>" target="_blank">
            <?= $img ?>
        </a>
    <?php } ?>
</p>
