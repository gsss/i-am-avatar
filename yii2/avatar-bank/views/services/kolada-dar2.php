<?php

use iAvatar777\widgets\KoladaDar1\KoladaDar1;
use yii\helpers\Html;
use yii\helpers\Url;
use iAvatar777\services\DateRus\DateRus;


/* @var $this \yii\web\View */
/* @var $model \avatar\models\forms\QrEnter */

$this->title = 'Календарь русский (КолядыДар)';


\avatar\assets\Notify::register($this);
\avatar\assets\Clipboard::register($this);


?>
<?php
$this->registerJs(<<<JS

var clipboard = new Clipboard('.buttonCopy');
clipboard.on('success', function(e) {

    new Noty({
        timeout: 1000,
        theme: 'relax',
        type: 'success',
        layout: 'bottomLeft',
        text: 'Скопировано'
    }).show();

});

JS
);


\iAvatar777\widgets\KalendarSlavi1\Asset::register($this);

$this->registerJs(<<<JS

var d1 = KrugoLet(new Date());
$('#day_' + d1.Mes + '_' + d1.Chislo).css('background-color','red');
console.log(d1);
JS
);
?>
<style>
    .kolada1 {
        margin-top: 30px;
    }
</style>

<div class="container" style="padding-bottom: 70px;">
    <div class="col-lg-12">
        <h1 class="page-header text-center">
            Календарь русский (КолядыДар)
        </h1>

        <?php
        $v = DateRus::format('j k b(Y)');
        ?>
        <p class="text-center lead">Сейчас <span style="font-size: 150%"><?= $v ?></span></p>

        <?php
        $v = DateRus::format('d.m.b/Y');
        ?>
    </div>
    <div class="col-lg-12">

        <h2 class="page-header" id="timeRus">00:000</h2>
        <?php
        $this->registerJs(<<<JS
setInterval(function() {
    var d2 = KrugoLet(new Date());
    var c = d1.Chas;
    if (c < 10) c = '0' + c; 
    $('#timeRus').html(c + ':' + d2.ChastiFormat);
}, 1000);

setInterval(function() {
    var Xmas95 = new Date();
    var hours = Xmas95.getHours();
    if (hours < 10) hours = '0' + hours;
    var m = Xmas95.getMinutes();
    if (m < 10) m = '0' + m;
    var s = Xmas95.getSeconds();
    if (s < 10) s = '0' + s;
    $('#timeGrig').html(hours + ':' + m + ':' + s);
}, 1000);
JS
        );
        ?>
        <?php
        $e = [
            1 => 'Земля (Чёрный)',
            2 => 'Звезда (Красный)',
            3 => 'Огонь (Алый)',
            4 => 'Солнце (Златый)',
            5 => 'Дерево (Зеленый)',
            6 => 'Свага (Небесный)',
            7 => 'Океан (Синий)',
            8 => 'Луна (ФиоЛѣтовый)',
            9 => 'Бог (Белый)',
        ];
        $i = [
            1  => 'Путь (странник)',
            2  => 'Жрец',
            3  => 'Жрица',
            4  => 'Мiр (Явь)',
            5  => 'Свиток',
            6  => 'Феникс',
            7  => 'Лис (Навь)',
            8  => 'Дракон',
            9  => 'Змей',
            10 => 'Орёл',
            11 => 'Дельфин',
            12 => 'Конь',
            13 => 'Пёс',
            14 => 'Тур (бык)',
            15 => 'Хоромы (дом)',
            16 => 'Капище (храм)',
        ];
        $model = [
            'leto'    => DateRus::format('b'),
            'element' => $e[DateRus::format('x')],
            'image'   => $i[DateRus::format('X')],
        ];
        ?>
        <?= \yii\widgets\DetailView::widget([
            'model'      => $model,
            'attributes' => [
                'leto:text:Лето',
                'element:text:Элемент',
                'image:text:Образ',
            ],
        ]) ?>

        <hr>

        <?= KoladaDar1::widget([
            'dayStart'         => 9,
            'optionsWeek'      => [
                1 => ['style' => 'background-color: #000000; color: #ffffff;'],
                2 => ['style' => 'background-color: #ff9395;'],
                3 => ['style' => 'background-color: #ffd092;'],
                4 => ['style' => 'background-color: #fffb92;'],
                5 => ['style' => 'background-color: #ace790;'],
                6 => ['style' => 'background-color: #a1e5fe;'],
                7 => ['style' => 'background-color: #909ffa;'],
                8 => ['style' => 'background-color: #b5a4e5;'],
                9 => ['style' => 'background-color: #ffffff;'],
            ],
            'optionsColumn'    => [
                1 => ['style' => 'width: 90px;'],
                2 => ['style' => 'width: 90px;'],
                3 => ['style' => 'width: 90px;'],
                4 => ['style' => 'width: 90px;'],
                5 => ['style' => 'width: 90px;'],
                6 => ['style' => 'width: 90px;'],
            ],
            'monthNames'        => [
                1 => '1 Рамхатъ',
                2 => '2 Айлѣтъ',
                3 => '3 Бейлѣтъ',
                4 => '4 Гэйлѣтъ',
                5 => '5 Дайлѣтъ',
                6 => '6 Элѣтъ',
                7 => '7 Вэйлѣтъ',
                8 => '8 Хейлѣтъ',
                9 => '9 Тайлѣтъ',
            ],
            'isDrawIds'        => true,
            'isDrawDateGrigor' => true,
            'DateGrigorClass'  => 'kal1',
            'DateGrigorFirst'  => '2020-09-21',
            'cellFormat'       => function (DateTime $d, $options) {
                $day = $options['day'];

                return $day . Html::tag('span', ' / ' . DateRus::format('j K', $d->format('U')), ['style' => 'color:#ccc; font-size:70%;']);
            }
        ]);
        ?>
        <hr>
        <p>Выделенный день означает что он соответствует от 18:00 предыдущего дня который указывается в подсказке по
            григорианскому календарю до 18:00 того дня который указывается в подсказке по григорианскому
            календарю</p>
        <p>День переходит на следующий в 18:00</p>
    </div>
    <div class="col-lg-12" style="margin-bottom: 60px;">
        <h2 class="page-header">Ссылки</h2>
        <p>Русский Славяно-Арийский Календарь <a href="http://energodar.net/ha-tha.php?str=vedy%2Fkalendar"
                                                 target="_blank">http://energodar.net/ha-tha.php?str=vedy%2Fkalendar</a>
        </p>
        <p>Виджет для Yii2 для рисования календаря Коляда Дар на лето: <a
                    href="https://github.com/i-avatar777/yii2-widget-kolada-dar1" target="_blank">https://github.com/i-avatar777/yii2-widget-kolada-dar1</a>
        </p>
        <p>Русский календарь на JavaScript: <a href="https://github.com/i-avatar777/kalendar_slavi" target="_blank">https://github.com/i-avatar777/kalendar_slavi</a>
        </p>
        <p>Сервис для форматирования дат с поддержкой русского календаря: <a
                    href="https://github.com/i-avatar777/service-date-rus" target="_blank">https://github.com/i-avatar777/service-date-rus</a>
        </p>
        <p>Компонент для Yii2 для форматирования расширяющий стандартный датами в русском календаре: <a
                    href="https://github.com/i-avatar777/yii2-component-formatter" target="_blank">https://github.com/i-avatar777/yii2-component-formatter</a>
        </p>
    </div>


</div>


