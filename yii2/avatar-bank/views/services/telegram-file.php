<?php

use iAvatar777\widgets\KoladaDar1\KoladaDar1;
use yii\helpers\Html;
use yii\helpers\Url;
use iAvatar777\services\DateRus\DateRus;
use yii\bootstrap\ActiveForm;


/* @var $this \yii\web\View */
/* @var $model \avatar\models\forms\QrEnter */

$this->title = 'Telegram файл';

\avatar\assets\Notify::register($this);
\avatar\assets\Clipboard::register($this);

?>
<?php
$this->registerJs(<<<JS

var clipboard = new Clipboard('.buttonCopy');
clipboard.on('success', function(e) {

    new Noty({
        timeout: 1000,
        theme: 'relax',
        type: 'success',
        layout: 'bottomLeft',
        text: 'Скопировано'
    }).show();

});

JS
);



$this->registerJs(<<<JS

$('.buttonGet').click(function(e) {
    ajaxJson({
        url: '/services/telegram-file-ajax',
        data: $('#formSign').serializeArray(),
        success: function(ret) {
            console.log(ret);
            if (ret.ok) {
                $('#buttonsAdd').collapse('show');
                $('#form1').collapse('hide');
                $('#downloadLink').attr('href', '/services/telegram-file-download?path=' + encodeURI(ret.result.file_path));
            } else {
                new Noty({
                    timeout: 2000,
                    theme: 'relax',
                    type: 'warning',
                    layout: 'bottomLeft',
                    text: JSON.stringify(ret)
                }).show();
            }
        }
    });
});

JS
);

$class1 = 'collapse in';
$class2 = 'collapse';
?>

<div class="container" style="padding-bottom: 70px;">
    <div class="col-lg-12">
        <h1 class="page-header text-center">
            <?= $this->title ?>
        </h1>

        <div class="<?= $class1 ?>" id="form1">
            <p>Чтобы получить файл введи идентификатор файла:</p>
            <p>Если ты не знаешь его, то зайди в бот <a href="tg://resolve?domain=iAvatar_bot" target="_blank">@iAvatar_bot</a> и кинь ему файл или перешли, он тебе выдаст <code>file_id</code></p>
            <?php
            $model = new \avatar\models\validate\ServicesTelegramFileAjax();
            $form = ActiveForm::begin([
                'id'      => 'formSign',
                'options' => ['enctype' => 'multipart/form-data']
            ]);
            ?>
            <?= $form->field($model, 'file_id') ?>
            <hr>

            <?php ActiveForm::end(); ?>

            <div class="form-group">
                <?= Html::button('Получить', [
                    'class' => 'btn btn-default buttonGet',
                    'name'  => 'contact-button',
                    'style' => 'width:100%',
                ]) ?>
            </div>
        </div>

        <div class="<?= $class2 ?>"  id="buttonsAdd">
            <div class="form-group" >
                <p><a href="/services/telegram-file-download" class="btn btn-primary" data-method="post" id="downloadLink">Скачать файл</a></p>
                <p><a href="/services/telegram-file" class="btn btn-default" >Назад</a></p>
            </div>
        </div>
    </div>



</div>


