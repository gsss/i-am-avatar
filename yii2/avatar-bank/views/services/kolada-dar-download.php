<?php

use iAvatar777\widgets\KoladaDar1\KoladaDar1;
use yii\helpers\Html;
use yii\helpers\Url;
use iAvatar777\services\DateRus\DateRus;

Html::encode(12);
?>

<html>
<head>

</head>
<body>

<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"
      integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

<!-- Optional theme -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css"
      integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">

<h1 class="page-header"><?= DateRus::format('b') ?> лето <small><?= (DateRus::format('Y') - 1) ?>
        - <?= DateRus::format('Y') ?></small></h1>

<?php
$e = [
    1 => 'Земля (Чёрный)',
    2 => 'Звезда (Красный)',
    3 => 'Огонь (Алый)',
    4 => 'Солнце (Златый)',
    6 => 'Свага (Небесный)',
    7 => 'Океан (Синий)',
    8 => 'Луна (ФиоЛѣтовый)',
    9 => 'Бог (Белый)',
];
$i = [
    1  => 'Путь (странник)',
    2  => 'Жрец',
    3  => 'Жрица',
    4  => 'Мiр (Явь)',
    5  => 'Свиток',
    6  => 'Феникс',
    7  => 'Лис (Навь)',
    8  => 'Дракон',
    9  => 'Змей',
    10 => 'Орёл',
    11 => 'Дельфин',
    12 => 'Конь',
    13 => 'Пёс',
    14 => 'Тур (бык)',
    15 => 'Хоромы (дом)',
    16 => 'Капище (храм)',
];
$model = [
    'leto'    => DateRus::format('b'),
    'element' => $e[DateRus::format('x')],
    'image'   => $i[DateRus::format('X')],
];
?>
<?= \yii\widgets\DetailView::widget([
    'model'      => $model,
    'attributes' => [
        'element:text:Элемент',
        'image:text:Образ',
    ],
]) ?>

<?= KoladaDar1::widget([
    'dayStart'         => 9,
    'optionsWeek'      => [
        1 => ['style' => 'background-color: #000000; color: #ffffff;'],
        2 => ['style' => 'background-color: #ff9395;'],
        3 => ['style' => 'background-color: #ffd092;'],
        4 => ['style' => 'background-color: #fffb92;'],
        5 => ['style' => 'background-color: #ace790;'],
        6 => ['style' => 'background-color: #a1e5fe;'],
        7 => ['style' => 'background-color: #909ffa;'],
        8 => ['style' => 'background-color: #b5a4e5;'],
        9 => ['style' => 'background-color: #ffffff;'],
    ],
    'optionsColumn'    => [
        1 => ['style' => 'width: 90px;'],
        2 => ['style' => 'width: 90px;'],
        3 => ['style' => 'width: 90px;'],
        4 => ['style' => 'width: 90px;'],
        5 => ['style' => 'width: 90px;'],
        6 => ['style' => 'width: 90px;'],
    ],
    'isDrawIds'        => true,
    'isDrawDateGrigor' => true,
    'DateGrigorClass'  => 'kal1',
    'cellFormat'       => function (DateTime $d, $options) {
        $day = $options['day'];

        return Html::tag(
            'span',
            $day . Html::tag('span', ' / ' . date('d.m', $d->format('U')), ['style' => 'color:#ccc']),
            ['style' => 'font-size:6pt;']
        );
    }
]); ?>

</body>
</html>