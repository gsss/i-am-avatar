<?php

use iAvatar777\widgets\KoladaDar1\KoladaDar1;
use yii\helpers\Html;
use yii\helpers\Url;
use iAvatar777\services\DateRus\DateRus;


/* @var $this \yii\web\View */
/* @var $model \avatar\models\forms\QrEnter */

$this->title = 'Календарь русский (КолядыДар)';


\avatar\assets\Notify::register($this);
\avatar\assets\Clipboard::register($this);


?>
<?php
$this->registerJs(<<<JS

var clipboard = new Clipboard('.buttonCopy');
clipboard.on('success', function(e) {

    new Noty({
        timeout: 1000,
        theme: 'relax',
        type: 'success',
        layout: 'bottomLeft',
        text: 'Скопировано'
    }).show();

});

JS
);


\avatar\assets\Kalendar::register($this);
$this->registerJs(<<<JS

var d1 = KrugoLet(new Date());
$('#day_' + d1.Mes + '_' + d1.Chislo).css('background-color','red');
console.log(d1);
JS
);
?>
<style>
    .kolada1 {
        margin-top: 30px;
    }
</style>

<div class="container" style="padding-bottom: 70px;">
    <div class="col-lg-12">
        <h1 class="page-header text-center">
            <?= $this->title ?>
        </h1>

        <?php
        $v = DateRus::format('j k b(Y)');
        ?>
        <p class="text-center lead">Сейчас <span style="font-size: 150%"><?= $v ?></span></p>

        <?php
        $v = DateRus::format('d.m.b/Y');
        ?>
    </div>
    <div class="col-lg-6">

        <h2 class="page-header" id="timeGrig">00:00:00</h2>

        <p><code>KOLADA_DAR_1</code></p>
        <div class="input-group">
            <input type="text" class="form-control" placeholder="" value="<?= $v ?>">
            <span class="input-group-btn">
                    <button class="btn btn-default buttonCopy" type="button" data-clipboard-text="<?= $v ?>">Копировать в буфер</button>
                  </span>
        </div><!-- /input-group -->
        <?php
        $v = DateRus::format('d.m.bл/Yг');
        ?>
        <p class="kolada1"><code>KOLADA_DAR_2</code></p>
        <div class="input-group">
            <input type="text" class="form-control" placeholder="" value="<?= $v ?>">
            <span class="input-group-btn">
                    <button class="btn btn-default buttonCopy" type="button" data-clipboard-text="<?= $v ?>">Копировать в буфер</button>
                  </span>
        </div><!-- /input-group -->
        <?php
        $v = DateRus::format('d.m.bл./Yг.');
        ?>
        <p class="kolada1"><code>KOLADA_DAR_3</code></p>
        <div class="input-group">
            <input type="text" class="form-control" placeholder="" value="<?= $v ?>">
            <span class="input-group-btn">
                    <button class="btn btn-default buttonCopy" type="button" data-clipboard-text="<?= $v ?>">Копировать в буфер</button>
                  </span>
        </div><!-- /input-group -->
        <?php
        $v = DateRus::format('d.m.b(Y)');
        ?>
        <p class="kolada1"><code>KOLADA_DAR_4</code></p>
        <div class="input-group">
            <input type="text" class="form-control" placeholder="" value="<?= $v ?>">
            <span class="input-group-btn">
                    <button class="btn btn-default buttonCopy" type="button" data-clipboard-text="<?= $v ?>">Копировать в буфер</button>
                  </span>
        </div><!-- /input-group -->
        <?php
        $v = DateRus::format('j k b(Y)');
        ?>
        <p class="kolada1"><code>KOLADA_DAR_5</code></p>
        <div class="input-group">
            <input type="text" class="form-control" placeholder="" value="<?= $v ?>">
            <span class="input-group-btn">
                    <button class="btn btn-default buttonCopy" type="button" data-clipboard-text="<?= $v ?>">Копировать в буфер</button>
                  </span>
        </div><!-- /input-group -->

        <?php
        $v = DateRus::format('j K b(Y)');
        ?>
        <p class="kolada1"><code>KOLADA_DAR_6</code></p>
        <div class="input-group">
            <input type="text" class="form-control" placeholder="" value="<?= $v ?>">
            <span class="input-group-btn">
                    <button class="btn btn-default buttonCopy" type="button" data-clipboard-text="<?= $v ?>">Копировать в буфер</button>
                  </span>
        </div><!-- /input-group -->

        <?php
        $v = DateRus::format('b-Y-m-d_H-i-s');
        ?>
        <p class="kolada1"><code>KOLADA_DAR_7</code></p>
        <div class="input-group">
            <input type="text" class="form-control" placeholder="" value="<?= $v ?>">
            <span class="input-group-btn">
                    <button class="btn btn-default buttonCopy" type="button" data-clipboard-text="<?= $v ?>">Копировать в буфер</button>
                  </span>
        </div><!-- /input-group -->

        <?php
        $v = DateRus::format('b-Y-m-d');
        ?>
        <p class="kolada1"><code>KOLADA_DAR_8</code></p>
        <div class="input-group">
            <input type="text" class="form-control" placeholder="" value="<?= $v ?>">
            <span class="input-group-btn">
                    <button class="btn btn-default buttonCopy" type="button" data-clipboard-text="<?= $v ?>">Копировать в буфер</button>
                  </span>
        </div><!-- /input-group -->

        <pre style="margin-top: 20px;">KOLADA_DAR_1
{dd}.{mm}.{llll}/{yyyy}

KOLADA_DAR_2
{dd}.{mm}.{llll}л/{yyyy}г

KOLADA_DAR_3
{dd}.{mm}.{llll}л./{yyyy}г.

KOLADA_DAR_4
{dd}.{mm}.{llll}({yyyy})

KOLADA_DAR_5
{d} {mstr} {llll}/{yyyy}

KOLADA_DAR_6
{d} {mstr3} {llll}/{yyyy}

KOLADA_DAR_7 - для файлов
{llll}-{yyyy}-{mm}-{dd}_{hh}-{min2}-{ss}

KOLADA_DAR_8 - для файлов
{llll}-{yyyy}-{mm}-{dd}

dd - день месяца с ведущими нулями от 01 до 31 по григориансокому календарю
mm - месяц с ведущими нулями от 01 до 12 по григориансокому календарю
llll - номер лета по русскому календарю
yyyy - номер года по григориансокому календарю
mstr - месяц по русски полный по григориансокому календарю
mstr3 - месяц по русски три символа по григориансокому календарю
d - день месяца по григориансокому календарю</pre>
    </div><!-- /.col-lg-6 -->
    <div class="col-lg-6">

        <h2 class="page-header" id="timeRus">00:000</h2>
        <?php
        $this->registerJs(<<<JS
setInterval(function() {
    var d2 = KrugoLet(new Date());
    var c = d1.Chas;
    if (c < 10) c = '0' + c; 
    $('#timeRus').html(c + ':' + d2.ChastiFormat);
}, 1000);

setInterval(function() {
    var Xmas95 = new Date();
    var hours = Xmas95.getHours();
    if (hours < 10) hours = '0' + hours;
    var m = Xmas95.getMinutes();
    if (m < 10) m = '0' + m;
    var s = Xmas95.getSeconds();
    if (s < 10) s = '0' + s;
    $('#timeGrig').html(hours + ':' + m + ':' + s);
}, 1000);
JS
        );
        ?>
        <?php
        $e = [
            1 => 'Земля (Чёрный)',
            2 => 'Звезда (Красный)',
            3 => 'Огонь (Алый)',
            4 => 'Солнце (Златый)',
            5 => 'Дерево (Зеленый)',
            6 => 'Свага (Небесный)',
            7 => 'Океан (Синий)',
            8 => 'Луна (ФиоЛѣтовый)',
            9 => 'Бог (Белый)',
        ];
        $i = [
            1  => 'Путь (странник)',
            2  => 'Жрец',
            3  => 'Жрица',
            4  => 'Мiр (Явь)',
            5  => 'Свиток',
            6  => 'Феникс',
            7  => 'Лис (Навь)',
            8  => 'Дракон',
            9  => 'Змей',
            10 => 'Орёл',
            11 => 'Дельфин',
            12 => 'Конь',
            13 => 'Пёс',
            14 => 'Тур (бык)',
            15 => 'Хоромы (дом)',
            16 => 'Капище (храм)',
        ];
        $model = [
            'leto'    => DateRus::format('b'),
            'element' => $e[DateRus::format('x')],
            'image'   => $i[DateRus::format('X')],
        ];
        ?>
        <?= \yii\widgets\DetailView::widget([
            'model'      => $model,
            'attributes' => [
                'leto:text:Лето',
                'element:text:Элемент',
                'image:text:Образ',
            ],
        ]) ?>

        <hr>

        <?= KoladaDar1::widget([
            'dayStart'         => 9,
            'optionsWeek'      => [
                1 => ['style' => 'background-color: #000000; color: #ffffff;'],
                2 => ['style' => 'background-color: #ff9395;'],
                3 => ['style' => 'background-color: #ffd092;'],
                4 => ['style' => 'background-color: #fffb92;'],
                5 => ['style' => 'background-color: #ace790;'],
                6 => ['style' => 'background-color: #a1e5fe;'],
                7 => ['style' => 'background-color: #909ffa;'],
                8 => ['style' => 'background-color: #b5a4e5;'],
                9 => ['style' => 'background-color: #ffffff;'],
            ],
            'isDrawIds'        => true,
            'isDrawDateGrigor' => true,
            'DateGrigorClass'  => 'kal1',
            'DateGrigorFirst'  => '2020-09-21',
        ]);
        ?>
        <hr>
        <p>Выделенный день означает что он соответствует от 18:00 предыдущего дня который указывается в подсказке по
            григорианскому календарю до 18:00 того дня который указывается в подсказке по григорианскому календарю</p>
        <p>День переходит на следующий в 18:00</p>
        <p><a href="/services/kolada-dar2" class="btn btn-success">Расширенный</a></p>
        <p><a href="/services/kolada-dar-download" class="btn btn-success">Скачать</a></p>
    </div>
    <div class="col-lg-12" style="margin-bottom: 60px;">
        <h2 class="page-header">Ссылки</h2>
        <p>Русский Славяно-Арийский Календарь <a href="http://energodar.net/ha-tha.php?str=vedy%2Fkalendar"
                                                 target="_blank">http://energodar.net/ha-tha.php?str=vedy%2Fkalendar</a>
        </p>
        <p>Виджет для Yii2 для рисования календаря Коляда Дар на лето: <a
                    href="https://github.com/i-avatar777/yii2-widget-kolada-dar1" target="_blank">https://github.com/i-avatar777/yii2-widget-kolada-dar1</a>
        </p>
        <p>Русский календарь на JavaScript: <a href="https://github.com/i-avatar777/kalendar_slavi" target="_blank">https://github.com/i-avatar777/kalendar_slavi</a>
        </p>
        <p>Сервис для форматирования дат с поддержкой русского календаря: <a
                    href="https://github.com/i-avatar777/service-date-rus" target="_blank">https://github.com/i-avatar777/service-date-rus</a>
        </p>
        <p>Компонент для Yii2 для форматирования расширяющий стандартный датами в русском календаре: <a
                    href="https://github.com/i-avatar777/yii2-component-formatter" target="_blank">https://github.com/i-avatar777/yii2-component-formatter</a>
        </p>
    </div>


</div>


