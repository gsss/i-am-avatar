<?php
use common\services\UsersInCache;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;

/* @var $this yii\web\View */

$this->title = 'Акционерное общество';



?>
<div class="container">
    <div class="col-lg-12">
        <h1 class="page-header text-center"><?= Html::encode($this->title) ?></h1>


        <?= \common\services\documentation\DbTable::widget([
            'name'      => 'rai_akcioneri',
            'columns' => [
                [
                    'name'          => 'id',
                    'type'          => 'int',
                    'isRequired'    => true,
                    'description'   => 'Идентификатор пользователя',
                ],
                [
                    'name'          => 'name_first',
                    'type'          => 'varchar(100)',
                    'isRequired'    => false,
                    'description'   => 'Имя',
                ],
                [
                    'name'          => 'name_last',
                    'type'          => 'varchar(100)',
                    'isRequired'    => false,
                    'description'   => 'Фамилия',
                ],
                [
                    'name'          => 'name_middle',
                    'type'          => 'varchar(100)',
                    'isRequired'    => false,
                    'description'   => 'Отчество',
                ],
                [
                    'name'          => 'projivanie',
                    'type'          => 'varchar(100)',
                    'isRequired'    => false,
                    'description'   => 'Место проживания',
                ],
                [
                    'name'          => 'foto',
                    'type'          => 'varchar(255)',
                    'isRequired'    => false,
                    'description'   => 'Фото человека',
                ],
                [
                    'name'          => 'contact_phone',
                    'type'          => 'varchar(100)',
                    'isRequired'    => false,
                    'description'   => 'Контактные данные Телефон',
                ],
                [
                    'name'          => 'contact_skype',
                    'type'          => 'varchar(100)',
                    'isRequired'    => false,
                    'description'   => 'Контактные данные Скайп',
                ],
                [
                    'name'          => 'contact_vk',
                    'type'          => 'varchar(255)',
                    'isRequired'    => false,
                    'description'   => 'Контактные данные VK',
                ],
                [
                    'name'          => 'contact_fb',
                    'type'          => 'varchar(255)',
                    'isRequired'    => false,
                    'description'   => 'Контактные данные FB',
                ],
                [
                    'name'          => 'country',
                    'type'          => 'varchar(2)',
                    'isRequired'    => false,
                    'description'   => 'Страна пребывания',
                ],
                [
                    'name'          => 'country_passport',
                    'type'          => 'varchar(2)',
                    'isRequired'    => false,
                    'description'   => 'Страна которая выдала паспорт',
                ],
                [
                    'name'          => 'utc_delta',
                    'type'          => 'int',
                    'isRequired'    => false,
                    'description'   => 'Смещение относительно гринвича',
                ],
                [
                    'name'          => 'time_zone',
                    'type'          => 'varchar(100)',
                    'isRequired'    => false,
                    'description'   => 'Название временной зоны по стандарту PHP',
                ],
                [
                    'name'          => 'is_show_profile',
                    'type'          => 'int',
                    'isRequired'    => false,
                    'description'   => 'Можно ли показывать профиль в списке акционеров?',
                ],
                [
                    'name'          => 'is_bill_exist',
                    'type'          => 'int',
                    'isRequired'    => false,
                    'description'   => 'Существует счет в АО?',
                ],
                [
                    'name'          => 'balance',
                    'type'          => 'int',
                    'isRequired'    => false,
                    'description'   => 'Баланс в акциях',
                ],

            ]
        ])?>
        <p>Паспорт РФ</p>
        <?= \common\services\documentation\DbTable::widget([
            'name'      => 'rai_akcioneri_passport_rus',
            'columns'   => [
                [
                    'name'          => 'id',
                    'type'          => 'int',
                    'isRequired'    => true,
                    'description'   => 'Идентификатор пользователя',
                ],
                [
                    'name'          => 'seria',
                    'type'          => 'int',
                    'isRequired'    => true,
                    'description'   => 'Паспорт серия',
                ],
                [
                    'name'          => 'number',
                    'type'          => 'int',
                    'isRequired'    => true,
                    'description'   => 'Паспорт номер',
                ],
                [
                    'name'          => 'kem',
                    'type'          => 'varchar(255)',
                    'isRequired'    => false,
                    'description'   => 'Кем выдан',
                ],
                [
                    'name'          => 'propiska',
                    'type'          => 'varchar(255)',
                    'isRequired'    => false,
                    'description'   => 'Кем выдан',
                ],
                [
                    'name'          => 'issue_date',
                    'type'          => 'date',
                    'isRequired'    => false,
                    'description'   => 'Паспорт дата выдачи',
                ],
                [
                    'name'          => 'rojd_date',
                    'type'          => 'date',
                    'isRequired'    => false,
                    'description'   => 'Паспорт дата рождения',
                ],
                [
                    'name'          => 'rojd_place',
                    'type'          => 'varchar(100)',
                    'isRequired'    => false,
                    'description'   => 'Паспорт место рождения',
                ],
                [
                    'name'          => 'image1',
                    'type'          => 'string(255)',
                    'isRequired'    => false,
                    'description'   => 'Паспорт скан1',
                ],
                [
                    'name'          => 'image2',
                    'type'          => 'string(255)',
                    'isRequired'    => false,
                    'description'   => 'Паспорт скан2. Прописка',
                ],
                [
                    'name'          => 'images',
                    'type'          => 'string(255)',
                    'isRequired'    => false,
                    'description'   => 'Паспорт скан2. Прописка',
                ],
            ]
        ])?>
        <p>Заявка на акцию:</p>
        <?= \common\services\documentation\DbTable::widget([
            'name'      => 'rai_request_akciya',
            'columns' => [
                [
                    'name'          => 'id',
                    'type'          => 'int',
                    'isRequired'    => true,
                    'description'   => 'Идентификатор пользователя',
                ],
                [
                    'name'          => 'count',
                    'type'          => 'int',
                    'isRequired'    => false,
                    'description'   => 'Кол-во акций',
                ],
                [
                    'name'          => 'create_at',
                    'type'          => 'int',
                    'isRequired'    => false,
                    'description'   => 'Время создания',
                ],
                [
                    'name'          => 'author_id',
                    'type'          => 'int',
                    'isRequired'    => false,
                    'description'   => 'Автор заявки, акционер',
                ],
                [
                    'name'          => 'status',
                    'type'          => 'int',
                    'isRequired'    => false,
                    'description'   => 'Статус заявки',
                ],
                [
                    'name'          => 'dogovor',
                    'type'          => 'varchar(255)',
                    'isRequired'    => false,
                    'description'   => 'Договор подписанный с двух сторон',
                ],
            ]
        ])?>


    </div>
</div>



