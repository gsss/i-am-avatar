<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;
use app\models\UnionCategory;

/* @var $this yii\web\View */

/* @var $userToken \common\models\UserToken */

$this->title = 'Изменение макета монеты';

$this->registerJs(<<<JS

JS
);
?>

<div class="container">
    <div class="col-lg-12">
        <h1 class="page-header"><?= Html::encode($this->title) ?></h1>

    </div>
    <div class="col-lg-8">

        <?php if (Yii::$app->session->hasFlash('form')) : ?>

            <div class="alert alert-success">
                Успешно добавлено.
            </div>

            <?php $id = Yii::$app->session->getFlash('form'); ?>
            <p>
                <a href="/cabinet-token-list/image?=<?= $id ?>" class="btn btn-default">Вернуться к редактированию</a>
                <a href="/cabinet-token-list/index" class="btn btn-default">Перейти к списку</a>
            </p>

        <?php else: ?>

            <div class="row">
                <div class="col-lg-5">
                    <?php $form = ActiveForm::begin([
                        'options' => ['enctype' => 'multipart/form-data']
                    ]); ?>
                    <?= $model->field($form, 'image') ?>

                    <hr>
                    <div class="form-group">
                        <?= Html::submitButton('Обновить', [
                            'class' => 'btn btn-primary',
                            'name'  => 'contact-button',
                            'style' => 'width:100%',
                        ]) ?>
                    </div>
                    <?php ActiveForm::end(); ?>
                </div>
            </div>

        <?php endif; ?>

    </div>
    <div class="col-lg-4">
        <?= $this->render('../cabinet/_menu') ?>
    </div>
</div>