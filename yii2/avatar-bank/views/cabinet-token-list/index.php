<?php

use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */

$this->title = \Yii::t('c.kTtfiHPqec', 'Мои токены');

$this->registerJs(<<<JS
$('.rowTable').click(function() {
    // window.location = '/cabinet-token-list/view?' +
    //  'id' + '=' + $(this).data('id')
    //  ;
});
$('.buttonImage').click(function(e) {
    e.preventDefault();
    e.stopPropagation();
    
    window.location = '/cabinet-token-list/image?' +
     'id' + '=' + $(this).data('id')
     ;
});
JS
);

?>
<div class="container">
    <div class="col-lg-12">
        <h1 class="page-header"><?= Html::encode($this->title) ?></h1>
    </div>
    <div class="col-lg-8">

        <?= \yii\grid\GridView::widget([
            'dataProvider' => new \yii\data\ActiveDataProvider([
                'query' => \common\models\UserToken::find()
                    ->where(['user_token.user_id' => Yii::$app->user->id])
                    ->innerJoin('token', 'token.id = user_token.token_id')
                    ->innerJoin('currency', 'currency.id = token.currency_id')
                    ->select([
                        'user_token.id',
                        'currency.image',
                        'currency.code',
                        'currency.title',
                        'token.address',
                    ])
                    ->asArray()
                ,
            ]),
            'tableOptions' => [
                'class' => 'table table-hover table-striped',
                'style' => 'width: auto;',
                'id'    => 'tableTransaction',
            ],
            'rowOptions'   => function ($item) {
                $params = [
                    'data'  => [
                        'id' => $item['id'],
                    ],
                    'role'  => 'button',
                    'class' => 'rowTable'
                ];
                return $params;
            },
            'columns' => [
                'id',
                [
                    'header'  => 'Картинка',
                    'content' => function ($item) {
                        $v = \yii\helpers\ArrayHelper::getValue($item, 'image', '');

                        return Html::img($v, [
                            'width' => 50,
                            'class' => 'img-circle',
                        ]);
                    },
                ],
                'title',
                'code',
                [
                    'header'  => 'address',
                    'content' => function ($item) {
                        $provider = new \avatar\modules\ETH\ServiceEtherScan();
                        $url = $provider->getUrl('/token/'.$item['address']);

                        return Html::tag('code', Html::a($item['address'], $url, ['target' => '_blank']));
                    },
                ],
                [
                    'header'  => 'Изменить картинку',
                    'content' => function ($item) {
                        return Html::button('Изменить картинку', [
                            'class' => 'btn btn-primary buttonImage',
                            'data' => [
                                'id' => $item['id'],
                            ],
                        ]);
                    },
                ],
            ],
        ])?>

    </div>
    <div class="col-lg-4">
        <?= $this->render('../cabinet/_menu') ?>
    </div>
</div>