<?php

/** @var $this \yii\web\View */
/** @var $model \avatar\models\forms\WalletAdd */
/** @var $wallet \common\models\piramida\Wallet */

use yii\bootstrap\ActiveForm;
use yii\helpers\Url;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;

$this->title = 'Эмиссия на ' . $wallet->id;


?>

<div class="container">
    <div class="col-lg-12">
        <h1 class="page-header"><?= Html::encode($this->title) ?></h1>

        <?php if (Yii::$app->session->hasFlash('form')) { ?>

            <?php $id = Yii::$app->session->getFlash('form'); ?>
            <div class="alert alert-success">
                Успешно отправлено. O_ID = <code><?= $id ?></code>
            </div>

            <p>
                <a href="<?= \yii\helpers\Url::to(['admin-wallets/index']) ?>" class="btn btn-success">Все кошельки</a>
            </p>

        <?php } else { ?>

            <div class="row">
                <div class="col-lg-5">

                    <?php $form = ActiveForm::begin([
                        'options' => ['enctype' => 'multipart/form-data'],
                    ]); ?>
                    <?= Html::hiddenInput(Html::getInputName($model, 'id'), $wallet->id) ?>
                    <?= $form->field($model, 'amount')->label('Атомов для эмиссии') ?>
                    <?= $form->field($model, 'comment')->label('Коментарий для эмиссии') ?>


                    <hr>
                    <div class="form-group">
                        <?= Html::submitButton('Эмитировать', [
                            'class' => 'btn btn-default',
                            'name'  => 'contact-button',
                            'style' => 'width:100%',
                        ]) ?>
                    </div>
                    <?php ActiveForm::end(); ?>
                </div>
            </div>

        <?php } ?>
    </div>
</div>

<div class="modal fade" id="modalInfo" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Информация</h4>
            </div>
            <div class="modal-body">
                Успешно!
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
            </div>
        </div>
    </div>
</div>
