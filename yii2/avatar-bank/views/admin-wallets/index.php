<?php

/** $this \yii\web\View  */

use yii\helpers\Url;
use app\services\GsssHtml;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use app\services\GetArticle\Collection;

$this->title = 'Все кошельки';


?>

<div class="container">
    <div class="col-lg-12">
        <h1 class="page-header"><?= Html::encode($this->title) ?></h1>

        <p><a href="test" class="btn btn-default">TEST</a></p>

        <?php \yii\widgets\Pjax::begin(); ?>
        <?php
        $this->registerJS(<<<JS
$('[data-toggle="tooltip"]').tooltip();

$('.rowTable').click(function(e) {
    window.location = '/admin-wallets/view' + '?' + 'id' + '=' + $(this).data('id');
});
$('.buttonAdd').click(function(e) {
    e.preventDefault();
    e.stopPropagation();
    window.location = '/admin-wallets/add' + '?' + 'id' + '=' + $(this).data('id');
});
$('.buttonSend').click(function(e) {
    e.preventDefault();
    e.stopPropagation();
    window.location = '/admin-wallets/send' + '?' + 'id' + '=' + $(this).data('id');
});
$('.buttonSub').click(function(e) {
    e.preventDefault();
    e.stopPropagation();
    window.location = '/admin-wallets/sub' + '?' + 'id' + '=' + $(this).data('id');
});

var functionAddExec = function (e) {
    e.preventDefault();
    e.stopPropagation();
    var b = $(this);
    var bText = b.html();
    b.off('click');
    b.html($('<i>', {class: 'fa fa-spinner fa-spin fa-fw'}));
    b.attr('disabled', 'disabled');
    
    var id = $('#modalAdd').data('id');
    ajaxJson({
        url: '/admin-wallets/add',
        data: {
            id: id,
            amount: $('#inputAdd').val()
        },
        success: function (ret) {
            b.on('click', functionAddExec);
            b.html(bText);
            b.removeAttr('disabled');
            
            window.location.reload();
        },
        errorScript: function(ret) {
            b.on('click', functionAddExec);
            b.html(bText);
            b.removeAttr('disabled');
        }
    });
};

$('.buttonAddExecute').click(functionAddExec);

var functionBurn = function (e) {
    e.preventDefault();
    e.stopPropagation();
    
    var b = $(this);
    var bText = b.html();
    b.off('click');
    b.html($('<i>', {class: 'fa fa-spinner fa-spin fa-fw'}));
    b.attr('disabled', 'disabled');
    
    var id = $('#modalSub').data('id');
    ajaxJson({
        url: '/admin-wallets/sub',
        data: {
            id: id,
            amount: $('#inputSub').val()
        },
        success: function (ret) {
            b.on('click', functionBurn);
            b.html(bText);
            b.removeAttr('disabled');
          
            window.location.reload();
        },
        errorScript: function(ret) {
            b.on('click', functionAddExec);
            b.html(bText);
            b.removeAttr('disabled');
        }
    });
};

$('.buttonSubExecute').click(functionBurn);
JS
        );
        $sort = new \yii\data\Sort([
            'attributes'   => [
                'code' => [
                    'asc'     => ['currency.code' => SORT_ASC],
                    'desc'    => ['currency.code' => SORT_DESC],
                    'default' => SORT_DESC,
                    'label'   => 'code',
                ]
            ],
            'defaultOrder' => ['code' => SORT_ASC]
        ]);
        ?>
        <?= \yii\grid\GridView::widget([
            'dataProvider' => new \yii\data\ActiveDataProvider([
                'query'      => \common\models\piramida\Wallet::find(),
                'pagination' => [
                    'pageSize' => 20,
                ],
            ]),
            'tableOptions' => [
                'class' => 'table table-striped table-hover',
            ],
            'rowOptions'   => function ($item) {
                $data = [
                    'data'  => ['id' => $item['id']],
                    'role'  => 'button',
                    'class' => 'rowTable'
                ];
                return $data;
            },
            'columns'      => [
                'id',
                'amount',
                [
                    'header'  => 'currency',
                    'content' => function ($item) {
                        $currency = \common\models\piramida\Currency::findOne($item->currency_id);

                        return Html::tag('code', $currency->code);
                    }
                ],
                [
                    'header'  => 'user',
                    'content' => function ($item) {
                        try {
                            $userBill = \common\models\avatar\UserBill::findOne(['address' => $item->id]);
                        } catch (Exception $e) {
                            return '';
                        }
                        if (!\cs\Application::isEmpty($userBill->user_id)) {
                            try {
                                return Html::encode($userBill->getUser()->getName2());
                            } catch (Exception $e) {
                                return '';
                            }
                        }

                        return '';
                    }
                ],
                'comment',
                [
                    'header'  => 'Отправить',
                    'content' => function ($item) {
                        return Html::button('Отправить', [
                            'class' => 'btn btn-default btn-xs buttonSend',
                            'data'  => [
                                'id' => $item['id'],
                            ]
                        ]);
                    }
                ],
                [
                    'header'  => 'Эмиссия',
                    'content' => function ($item) {
                        return Html::button('Эмиссия', [
                            'class' => 'btn btn-success btn-xs buttonAdd',
                            'data'  => [
                                'id' => $item['id'],
                            ]
                        ]);
                    }
                ],
                [
                    'header'  => 'Сжигание',
                    'content' => function ($item) {
                        return Html::button('Сжигание', [
                            'class' => 'btn btn-danger btn-xs buttonSub',
                            'data'  => [
                                'id' => $item['id'],
                            ]
                        ]);
                    }
                ],
            ],
        ]) ?>
        <?php \yii\widgets\Pjax::end(); ?>
    </div>
</div>

<div class="modal fade" id="modalInfo" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Информация</h4>
            </div>
            <div class="modal-body">
                Успешно!
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="modalAdd" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Эмиссия</h4>
            </div>
            <div class="modal-body">
                <input class="form-control" id="inputAdd">
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary buttonAddExecute">Выполнить</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="modalSub" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Сжигание</h4>
            </div>
            <div class="modal-body">
                <input class="form-control" id="inputSub">
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary buttonSubExecute">Выполнить</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
            </div>
        </div>
    </div>
</div>