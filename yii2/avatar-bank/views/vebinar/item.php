<?php

use yii\helpers\Html;
use yii\helpers\Url;


/** @var int $id идентификатор вебинара */


/** @var \yii\web\View $this */
$this->title = 'Вебинар '.$id;

?>


<div class="container" style="padding-bottom: 70px;">
    <div class="col-lg-12">
        <h1 class="page-header text-center">
            Вебинар
        </h1>


        <div id="chat">
            <?= $this->render('item-chat', ['id' => $id]) ?>
        </div>


    </div>



</div>
