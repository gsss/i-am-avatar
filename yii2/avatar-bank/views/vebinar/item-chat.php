<?php

/** @var \yii\web\View $this */
/** @var int $id идентификатор вебинара */

\common\assets\SocketIO\Asset::register($this);

?>

<?php

$user_id = Yii::$app->session->id;

$serverName = \common\assets\SocketIO\Asset::getHost();

$this->registerJs(<<<JS

var socket = io.connect('{$serverName}');
console.log(socket);
function chatScroll() {
    var chat = $(".panel-body");
    chat.scrollTop(chat.prop("scrollHeight"));
}

chatScroll();

var name;
const messageInput = document.getElementById('bt-input');
const buttonSend = document.getElementById('btn-chat');
let roomName = {$id};

// объявляю чат сервису что я добавляюсь в комнату roomName
socket.emit('room-new-user', roomName, '{$user_id}');

$("#btn-input").keyup(function(event) {
    
    if (event.keyCode == 13 && $('#btn-input').val().length > 0) {
     
        socket.emit('room-send-message', roomName, '{$user_id}', $('#btn-input').val());
        
        appendMessage();
    }
});

buttonSend.addEventListener('click', e => {
    if ($('#btn-input').val().length > 0) {
        e.preventDefault();
        
        socket.emit('room-send-message', roomName, '{$user_id}', $('#btn-input').val());
        
        appendMessage();
    }           
});
socket.on('room-send-message', data => {
    console.log(data);
    appendMessageGet(data.user, data.message);
});

socket.on('user-connected', name => {
    console.log("user-connected: " + name +' вошел')
});

socket.on('user-disconnected', name => {
    console.log("user-disconnected: " + name +' вышел')
});

function appendMessageGet(user, message) {
    var name = user, image = 'https://cloud1.cloud999.ru/upload/cloud/15732/92236_1DUw7WNREa_crop.jpg';
    
    var time = (new Date).toLocaleString();
    var liObject = $('<li>', {class:'left clearfix'});
    var spanObject = $('<span>', {class:'chat-img pull-left'});
    var imgObject = $('<img>', {src: image, class: 'img-circle', width: 50});
    spanObject.append(imgObject);
    liObject.append(spanObject);
    var div1Object = $('<div>', {class: 'chat-body clearfix'});
    var div2Object = $('<div>', {class: 'header2'});
    var strongObject = $('<strong>', {class: 'primary-font'}).html(name);
    var smallObject = $('<small>', {class: 'pull-right text-muted'});
    var spanTimeObject = $('<span>', {class: 'glyphicon glyphicon-time'});
    div2Object.append(strongObject);
    smallObject.append(spanTimeObject);
    smallObject.append(time);
    strongObject.append(smallObject);
    var pObject = $('<p>').html(message);
    div1Object.append(div2Object);
    div1Object.append(pObject);
    liObject.append(div1Object);
    $('.chat').append(liObject);
    chatScroll(); 
}

function appendMessage() {
    if ($('#btn-input').val().length>0) {
        ajaxJson({
            url: '/vebinar/send',
            data: {
                text: $('#btn-input').val(),
                vebinar_id: {$id},
                session_id: '{$user_id}'
            },
            success: function(ret) {
                var user = ret.message.session_id;
                var liObject = $('<li>', {class:'left clearfix', "data-id": ret.message.id});
                var spanObject = $('<span>', {class:'chat-img pull-left'});
                var imgObject = $('<img>', {src: 'https://cloud1.cloud999.ru/upload/cloud/15732/92236_1DUw7WNREa_crop.jpg', class: 'img-circle', width: 50});
                spanObject.append(imgObject);
                liObject.append(spanObject);
                var div1Object = $('<div>', {class: 'chat-body clearfix'});
                var div2Object = $('<div>', {class: 'header2'});

                var strongObject = $('<strong>', {class: 'primary-font'}).html(user);
                var smallObject = $('<small>', {class: 'pull-right text-muted'});
                var spanTimeObject = $('<span>', {class: 'glyphicon glyphicon-time'});
                div2Object.append(strongObject);
                smallObject.append(spanTimeObject);
                smallObject.append(ret.message.timeFormatted);
                strongObject.append(smallObject);
                var o = JSON.parse(ret.message.text);
                var pObject = $('<p>').html(o.text);
                div1Object.append(div2Object);
                div1Object.append(pObject);
                liObject.append(div1Object);
                $('.chat').append(liObject);
                $('#btn-input').val('');
                chatScroll();
            }
        });
    }
}


JS
);
?>

<style>
    .chat
    {
        list-style: none;
        margin: 0;
        padding: 0;
    }

    .chat li
    {
        margin-bottom: 10px;
        padding-bottom: 5px;
        border-bottom: 1px dotted #B3A9A9;
    }

    .chat li.left .chat-body
    {
        margin-left: 60px;
    }

    .chat li.right .chat-body
    {
        margin-right: 60px;
    }


    .chat li .chat-body p
    {
        margin: 0;
        color: #777777;
    }

    .panel .slidedown .glyphicon, .chat .glyphicon
    {
        margin-right: 5px;
    }

    .panel-body
    {
        overflow-y: scroll;
        height: 650px;
    }

    ::-webkit-scrollbar-track
    {
        -webkit-box-shadow: inset 0 0 6px rgba(0,0,0,0.3);
        background-color: #F5F5F5;
    }

    ::-webkit-scrollbar
    {
        width: 12px;
        background-color: #F5F5F5;
    }

    ::-webkit-scrollbar-thumb
    {
        -webkit-box-shadow: inset 0 0 6px rgba(0,0,0,.3);
        background-color: #555;
    }

    .panel-primary > .panel-heading {
        background-image: linear-gradient(to bottom, #750f0b 0%, #750f0b 100%) !important;
    }

    .panel-primary {
        border-color: #750f0b !important;
    }

    .btn-warning {
        background-image: linear-gradient(to bottom, #5cb85c 0%, #5cb85c 100%);
    }

    .btn-warning:hover, .btn-warning:focus {
        background-color: #5cb85c !important;
        background-position: 0 -15px;
    }

    .btn-warning:hover {
        color: #fff;
        background-color: #5cb85c !important;
        border-color: #5cb85c !important;
    }

</style>


<div class="container">
    <div class="row">
        <div class="col-md-5" style="padding-top: 100px;">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <span class="glyphicon glyphicon-comment"></span> Чат
                </div>
                <div class="panel-body">
                    <ul class="chat">
                        <?php /** @var \common\models\VebinarChatItem $message */ ?>
                        <?php foreach (\common\models\VebinarChatItem::find()->where(['vebinar_id' => $id])->all() as $message) { ?>
                            <li class="left clearfix" data-id="<?= $message->id ?>"><span class="chat-img pull-left">
                            <img width="50" src="https://cloud1.cloud999.ru/upload/cloud/15732/92236_1DUw7WNREa_crop.jpg" alt="" class="img-circle" />
                        </span>
                                <div class="chat-body clearfix">
                                    <div class="header2">
                                        <strong class="primary-font"><?= $message->session_id ?></strong> <small class="pull-right text-muted">
                                            <span class="glyphicon glyphicon-time"></span><?= Yii::$app->formatter->asDatetime($message->created_at, 'php:d.m.Y H:i:s') ?></small>
                                    </div>
                                    <p>
                                        <?= \yii\helpers\Json::decode($message->text)['text'] ?>
                                    </p>
                                </div>
                            </li>
                        <?php } ?>
                    </ul>
                </div>

                <div class="panel-footer">
                    <div class="input-group">
                        <input id="btn-input" type="text" class="form-control input-sm" placeholder="Напиши свое сообщение здесь..." />
                        <span class="input-group-btn">

                            <button class="btn btn-warning btn-sm" id="btn-chat">
                                Отправить</button>
                        </span>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
