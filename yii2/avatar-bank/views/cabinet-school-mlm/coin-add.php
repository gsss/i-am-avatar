<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;
use app\models\UnionCategory;
use yii\db\Query;
use yii\helpers\ArrayHelper;

/** @var $this yii\web\View */
/** @var $form yii\bootstrap\ActiveForm */
/** @var $model \avatar\models\forms\CoinAdd */
/** @var $school \common\models\school\School */

$this->title = 'Добавить монету';

?>
<div class="container">

    <?php \avatar\widgets\SchoolMenu::begin(['school' => $school]) ?>
    <h1 class="page-header"><?= Html::encode($this->title) ?></h1>

    <?php if (Yii::$app->session->hasFlash('form')) : ?>

        <?php $data = Yii::$app->session->getFlash('form'); ?>
        <div class="alert alert-success">
            Успешно добавлено. OID = <code><?= $data['operation']->getAddress() ?></code>
        </div>

        <p>
            <a href="<?= \yii\helpers\Url::to(['cabinet-school-mlm/index', 'id' => $school->id]) ?>" class="btn btn-success">Настройка партнеской программы</a>
        </p>

    <?php else: ?>


        <div class="row">
            <div class="col-lg-8">
                <?php $form = ActiveForm::begin([
                    'options' => ['enctype' => 'multipart/form-data'],
                ]); ?>
                <?= $form->field($model, 'code') ?>
                <?= $form->field($model, 'name') ?>
                <?= $form->field($model, 'decimals') ?>
                <?= $form->field($model, 'amount') ?>
                <?= $form->field($model, 'comment') ?>
                <?= $form->field($model, 'image')->widget('\common\widgets\FileUpload7\FileUpload', [
                    'update'    => \avatar\controllers\CabinetSchoolPagesConstructorController::getUpdate(),
                    'settings'  => \avatar\controllers\CabinetSchoolPagesConstructorController::getSettings($school->id, 16),
                    'events'    => [
                        'onDelete' => function (\avatar\models\forms\school\Article $item) {
                            $r = new \cs\services\Url($item->image);
                            $d = pathinfo($r->path);
                            $start = $d['dirname'] . '/' . $d['filename'];

                            \common\models\school\File::deleteAll(['like', 'file', $start]);
                        },
                    ],
                ]) ?>

                <hr>
                <div class="form-group">
                    <?= Html::submitButton('Добавить', [
                        'class' => 'btn btn-default',
                        'name'  => 'contact-button',
                        'style' => 'width:100%',
                    ]) ?>
                </div>
                <?php ActiveForm::end(); ?>
            </div>
        </div>

    <?php endif; ?>
    <?php \avatar\widgets\SchoolMenu::end() ?>


</div>
