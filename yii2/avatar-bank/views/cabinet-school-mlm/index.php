<?php

/** @var $this \yii\web\View */
/** @var $school \common\models\school\School */

use yii\helpers\Url;
use app\services\GsssHtml;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use app\services\GetArticle\Collection;

$this->title = 'Настройка партнеской программы';

\avatar\assets\Notify::register($this);
\avatar\assets\Clipboard::register($this);

$this->registerJs(<<<JS

var clipboard = new Clipboard('.buttonCopy');
clipboard.on('success', function(e) {

    new Noty({
        timeout: 1000,
        theme: 'sunset',
        type: 'success',
        layout: 'bottomLeft',
        text: 'Скопировано'
    }).show();

});

var functionDelete = function(e) {
    $(this).parent().parent().remove();
};

$('.buttonDelete').click(functionDelete);
$('.buttonAdd').click(function(e) {
    var length1 = $('#percent div.row').length;
    $('#percent').append(
        $('<div>', {class: 'row', style: 'margin-bottom: 10px;'})
        .append($('<div>', {class: 'col-sm-5'}).html(length1 + 1))
        .append($('<div>', {class: 'col-sm-5'}).append(
            $('<input>', {class: 'form-control', name: 'Levels[percent][]', type: 'text'})
        ))
        .append($('<div>', {class: 'col-sm-2'}).append(
            $('<button>', {class: 'btn btn-default buttonDelete', style: 'width: 100%;'}).html('-').click(functionDelete)
        ))
    );
});
$('.buttonSave').click(function(e) {
    ajaxJson({
        url: '/cabinet-school-mlm/save',
        data: $('#percent').serializeArray(),
        success: function(ret) {
            
        }
    });
});
JS
);
?>

<div class="container">

    <h1 class="page-header"><?= Html::encode($this->title) ?></h1>

    <div class="col-lg-6">
        <h2 class="page-header">Уровни</h2>

        <div class="row" style="margin-bottom: 10px;">
            <div class="col-sm-5">
                Уровень
            </div>
            <div class="col-sm-5">
                Проценты * 100
            </div>
        </div>
        <form id="percent">
            <input type="hidden" value="<?= $school->id ?>" name="Levels[school_id]">
            <?php $c = 1; ?>
            <?php $levelList = \common\models\school\ReferalLevel::find()->where(['school_id' => $school->id])->all();?>
            <?php ArrayHelper::multisort($levelList, 'level'); ?>
            <?php /** @var \common\models\school\ReferalLevel $level */ ?>
            <?php foreach ($levelList as $level) { ?>
                <div class="row" style="margin-bottom: 10px;">
                    <div class="col-sm-5">
                        <?= $c ?>
                    </div>
                    <div class="col-sm-5">
                        <input class="form-control" name="Levels[percent][]" type="text" value="<?= $level->percent ?>">
                    </div>
                    <div class="col-sm-2">
                        <button class="btn btn-default buttonDelete" style="width: 100%;">-</button>
                    </div>
                </div>
                <?php $c++; ?>
            <?php } ?>
        </form>
        <div class="row" style="margin-bottom: 10px;">
            <div class="col-sm-12">
                <button class="btn btn-default buttonAdd" style="width: 100%;">Добавить</button>
            </div>
        </div>
        <div class="row" style="margin-bottom: 10px;">
            <div class="col-sm-12">
                <button class="btn btn-success buttonSave" style="width: 100%;">Обновить</button>
            </div>
        </div>

    </div>
    <div class="col-lg-6">
        <h2 class="page-header text-center">Монета</h2>
        <?php if ($school->referal_wallet_id) { ?>
            <?php
            $wallet = \common\models\piramida\Wallet::findOne($school->referal_wallet_id);
            $currency = $wallet->getCurrency();
            $link = \common\models\avatar\CurrencyLink::findOne(['currency_int_id' => $currency->id]);
            $currencyExt = \common\models\avatar\Currency::findOne($link->currency_ext_id);

            ?>
            <p class="text-center"><img src="<?= $currencyExt->image ?>" class="img-circle" width="300"></p>
            <p class="text-center"><code title="Нажми чтобы скопировать" data-toggle="tooltip" class="buttonCopy"
                                         data-clipboard-text="<?= $wallet->getAddress() ?>"><?= $wallet->getAddress() ?></code>
            </p>
            <p class="text-center"><?= Html::a('Редактировать', ['cabinet-school-mlm/coin-edit', 'id' => $school->id], ['class' => 'btn btn-default',]) ?></p>

        <?php } else { ?>
            <p class="text-center"><?= Html::a('Добавить', ['cabinet-school-mlm/coin-add', 'id' => $school->id], [
                    'class' => 'btn btn-default',
                ]) ?></p>
        <?php } ?>

    </div>




</div>

<div class="modal fade" id="modalInfo" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="myModalLabel">Информация</h4>
            </div>
            <div class="modal-body">
                Успешно!
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
            </div>
        </div>
    </div>
</div>
