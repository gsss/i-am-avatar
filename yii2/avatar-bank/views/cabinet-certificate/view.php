<?php

/** @var $this \yii\web\View  */
/** @var $school \common\models\school\School  */
/* @var $item \avatar\models\forms\school\Certificate */

use yii\helpers\Url;
use app\services\GsssHtml;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use app\services\GetArticle\Collection;

$this->title = $school->name;


?>

<div class="container">
    <div class="col-lg-12">
        <h1 class="page-header"><?= Html::encode($this->title) ?></h1>
    </div>
    <div class="col-lg-4">
        <?= $this->render('../cabinet-school/_menu', ['school' => $school]) ?>
    </div>
    <div class="col-lg-8">
        test
    </div>
</div>
