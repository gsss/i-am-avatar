<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;
use app\models\UnionCategory;

/** @var $this yii\web\View */

$this->title = 'Заявки на вывод реферальных начислений';

\avatar\assets\Notify::register($this);
\avatar\assets\Clipboard::register($this);

$this->registerJs(<<<JS

var clipboard = new Clipboard('.buttonCopy');
clipboard.on('success', function(e) {

    new Noty({
        timeout: 1000,
        theme: 'sunset',
        type: 'success',
        layout: 'bottomLeft',
        text: 'Скопировано'
    }).show();

});

JS
);

$school = \common\models\school\School::get();
$query = \common\models\school\ReferalRequestOut::find()
    ->where(['user_id' => Yii::$app->user->id])
    ->andWhere(['school_id' => $school->id]);

$isExist = false;
if (!\cs\Application::isEmpty($school->referal_wallet_id)) {
    $isExist = true;
    $wallet_id = $school->referal_wallet_id;
    $wallet = \common\models\piramida\Wallet::findOne($wallet_id);
    $link = \common\models\avatar\CurrencyLink::findOne(['currency_int_id' => $wallet->currency_id]);
    $bill = \common\models\avatar\UserBill::getBillByCurrencyAvatarProcessing($wallet->currency_id, Yii::$app->user->id);
    $walletMe = \common\models\piramida\Wallet::findOne($bill->address);
    $c = \common\models\piramida\Currency::findOne($walletMe->currency_id);
}
?>
<div class="container">
    <div class="col-lg-12">
        <h1 class="page-header"><?= Html::encode($this->title) ?></h1>
    </div>
    <div class="col-lg-8">
        <?php if ($isExist) { ?>
            <p>Монет для вывода: <?= Yii::$app->formatter->asDecimal($walletMe->amount/pow(10, $c->decimals), $c->decimals) ?> <?= strtoupper($c->code) ?></p>

            <p><a href="<?= Url::to(['cabinet-referal-request/add']) ?>" class="btn btn-default">Добавить</a></p>

            <?php \yii\widgets\Pjax::begin(); ?>
            <?php
            $this->registerJS(<<<JS

$('.rowTable').click(function() {
   // window.location = '/admin-anketa/view?id=' + $(this).data('id');
});
JS
            );
            $sort = new \yii\data\Sort([
                'defaultOrder' => [
                    'created_at' => SORT_DESC,
                ],
            ]);
            ?>
            <?= \yii\grid\GridView::widget([
                'dataProvider' => new \yii\data\ActiveDataProvider([
                    'query' => $query
                    ,
                    'sort'  => $sort,
                ]),
                'tableOptions' => [
                    'class' => 'table table-striped table-hover',
                ],
                'rowOptions'   => function ($item) {
                    return [
                        'data'  => ['id' => $item['id']],
                        'role'  => 'button',
                        'class' => 'rowTable',
                    ];
                },
            ]) ?>
            <?php \yii\widgets\Pjax::end(); ?>
        <?php } else { ?>
            <p class="alert alert-success">В школе не задана реферальная система</p>
        <?php } ?>
    </div>
    <div class="col-lg-4">
        <?= $this->render('../cabinet/_menu') ?>
    </div>
</div>



