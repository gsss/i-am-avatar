<?php
/**
 * Created by PhpStorm.
 * User: s.arhangelskiy
 * Date: 13.04.2017
 * Time: 11:34
 */

/** @var $item                      \common\models\school\Subscribe */
/** @var $school_subscribe_image    string                              картинка с относительным путем или полным */
/** @var $sid                       int идентификатор                   school_potok_subscribe_item.id */
/** @var $potok                     \common\models\school\Potok         поток на который делается рассылка */

if (!isset($school_subscribe_image)) $school_subscribe_image = '/images/mail/new-lesson/logo_v2.png';
if (\yii\helpers\StringHelper::startsWith($school_subscribe_image, '/')) $school_subscribe_image = \yii\helpers\Url::to($school_subscribe_image, true);

if (isset($potok)) {
    $url = new \cs\services\Url(\yii\helpers\Url::to(['subscribe/unsubscribe', 'potok_id' => $potok->id, 'sid' => $sid], true));
    $url = '{{link}}' . $url->query . '?';
}

$school = $potok->getSchool();
?>


<p style="text-align: center">
    <img src="<?= $school_subscribe_image ?>" width="200">
</p>

<h1 style="text-align: center;font-weight: normal;">
    <?= $item->subject ?>
</h1>

<?= $item->content ?>

<?php if ($school->id == 2) { ?>
<p style="margin-top: 40px;">Присоединяйтесь к нашим каналам: <?php if (isset($sid)) { ?><img src="<?= \yii\helpers\Url::to(['subscribe/pixel', 'sid' => $sid], true) ?>" width="1"><?php } ?></p>
<p>
    <a target="_blank" href="https://vk.com/iavatar777"><img src="<?= \yii\helpers\Url::to('/images/mail/subscribe/social-net/vk.png', true) ?>" alt="VK" title="VK" ></a>
    <a target="_blank" href="https://www.i-am-avatar.com/telegram/group"><img src="<?= \yii\helpers\Url::to('/images/mail/subscribe/social-net/Telegram.png', true) ?>" alt="Telegram группа" title="Telegram группа" ></a>
    <a target="_blank" href="https://www.i-am-avatar.com/telegram/channel"><img src="<?= \yii\helpers\Url::to('/images/mail/subscribe/social-net/Telegram.png', true) ?>" alt="Telegram канал" title="Telegram канал" ></a>
    <a target="_blank" href="https://www.facebook.com/iAvatar777"><img src="<?= \yii\helpers\Url::to('/images/mail/subscribe/social-net/fb.png', true) ?>" alt="Facebook" title="Facebook" ></a>
    <a target="_blank" href="https://www.youtube.com/c/%D0%AF%D0%90%D0%B2%D0%B0%D1%82%D0%B0%D1%80"><img src="<?= \yii\helpers\Url::to('/images/mail/subscribe/social-net/media.png', true) ?>" alt="YouTube"  width="43" title="YouTube" ></a>
</p>
<?php } ?>

<?php if (isset($potok)) { ?>
    <hr>

    <p>Вы получили это письмо, потому что зарегистрировались на событие "<?= $potok->name ?>". Чтобы отписаться от этой рассылки нажмите
        <a href="<?= $url ?>">здесь</a>
    </p>
<?php } ?>
