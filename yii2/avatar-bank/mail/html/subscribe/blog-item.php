<?php
/**
 * Created by PhpStorm.
 * User: s.arhangelskiy
 * Date: 13.04.2017
 * Time: 11:34
 */

/** @var $newsItem  \common\models\BlogItem */

?>


<p style="text-align: center;">
    <a href="<?= $newsItem->getLink(true) ?>">
        <img src="<?= \common\widgets\FileUpload7\FileUpload::getFile($newsItem->image, 'crop') ?>" width="300" style="border-radius: 20px;"/>
    </a>
</p>
<p style="text-align: center;"><?= \avatar\services\Html::getMiniText($newsItem->content) ?></p>
<p style="text-align: center;"><a href="<?= $newsItem->getLink(true) ?>" style="
text-decoration: none;
            color: #fff;
            background-color: #337ab7;
            border-color: #2e6da4;
             display: inline-block;
            padding: 6px 12px;
            margin-bottom: 0;
            font-size: 14px;
            font-weight: normal;
            line-height: 1.42857143;
            text-align: center;
            white-space: nowrap;
            vertical-align: middle;
            -ms-touch-action: manipulation;
            touch-action: manipulation;
            cursor: pointer;
            -webkit-user-select: none;
            -moz-user-select: none;
            -ms-user-select: none;
            user-select: none;
            background-image: none;
            border: 1px solid transparent;
            border-radius: 4px;
">Подробнее</a></p>
