<?php
/**
 * @var \common\models\task\Task $task
 * @var string $reason
 */
/** @var $school_subscribe_image    string                              картинка с относительным путем или полным */

if (!isset($school_subscribe_image)) $school_subscribe_image = '/images/mail/new-lesson/logo_v2.png';
if (\yii\helpers\StringHelper::startsWith($school_subscribe_image, '/')) $school_subscribe_image = \yii\helpers\Url::to($school_subscribe_image, true);

$url = \yii\helpers\Url::to(['cabinet-task-list/view', 'id' => $task->id], true);

?>

<p style="text-align: center">
    <img src="<?= $school_subscribe_image ?>" width="200">
</p>
<p>Задача отклонена</p>
<p>ID: <?= $task->id ?>.</p>
<p>Задача: "<?= $task->name ?>".</p>
<hr>
<p>Причина:</p>
<p><?= nl2br($reason) ?></p>
<hr>
<p>Задачу можно посмотреть по ссылке <a href="<?= $url ?>"><?= $url ?></a></p>
