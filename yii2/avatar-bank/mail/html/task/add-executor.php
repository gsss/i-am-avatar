<?php
/**
 * Created by PhpStorm.
 * User: Ra-m-ha
 * Date: 06.01.2019
 * Time: 19:08
 */

/** @var \common\models\task\Task $task */
/** @var \common\models\UserAvatar $user */

$url = \yii\helpers\Url::to(['cabinet-task-list/view', 'id' => $task->id], true);
?>

<p>На тебя назначена задача.</p>
<p>ID: <?= $task->id ?>.</p>
<p>Задача: "<?= $task->name ?>".</p>
<p>Задачу можно посмотреть по ссылке <a href="<?= $url ?>"><?= $url ?></a></p>