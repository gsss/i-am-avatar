<?php
/**
 * @var \common\models\comment\Comment $comment
 */
?>

<p>Добавлен новый коментарий</p>
<p>Тип: <?= $comment->getList()->type_id ?></p>
<p>Идентификатор коментируемого обхекта: <?= $comment->getList()->object_id ?></p>
<p>Автор: <?= \yii\helpers\Html::encode($comment->getUser()->getName2()) ?></p>
<p>Время: <?= Yii::$app->formatter->asDatetime($comment->created_at) ?></p>
<hr>
<p><?= $comment->getHtml() ?></p>
