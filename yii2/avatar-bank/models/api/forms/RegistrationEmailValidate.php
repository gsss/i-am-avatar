<?php

namespace avatar\models\api\forms;

use common\models\SendLetter;
use common\models\UserAvatar;
use common\models\UserPhoneToken;
use common\models\UserRegistration;
use common\services\Mobile;
use common\services\Security;
use cs\Application;
use cs\services\Str;
use Yii;
use yii\base\Exception;
use yii\base\Model;
use yii\helpers\ArrayHelper;
use yii\helpers\VarDumper;

/**
 */
class RegistrationEmailValidate extends Model
{
    // @var string пин код переданный пользователем
    public $pin;

    // @var string ключ переданный с предыдущей функции
    public $key;

    // @var string пин код определенный из кеша по ключу $key
    private $pinInternal;

    // @var int Идентификатор пользователя который хочет логиниться
    private $userId;

    /**
     */
    public function rules()
    {
        return [
            [['key', 'pin'], 'required'],
            ['key', 'validateKey'],
            ['pin', 'trim'],
            ['pin', 'validatePin'],
        ];
    }

    public function validateKey($attribute, $params)
    {
        if (!$this->hasErrors()) {
            $value = Yii::$app->cache->get('registration-mail-' . $this->key);
            if (!$value) {
                $this->addError($attribute, 'Не найден ключ');
                return;
            }
            $this->pinInternal = $value['pin'];
            $this->userId = $value['user_id'];
        }
    }

    public function validatePin($attribute, $params)
    {
        if (!$this->hasErrors()) {
            if ($this->pinInternal != $this->pin) {
                $this->addError($attribute, 'Не верный pin');
                return;
            }
        }
    }

    /**
     * @return \common\models\UserPhoneToken
     */
    public function action()
    {
        $user = UserAvatar::findOne($this->userId);
        $user->activate();

        // периуд на который выдается токен, сек
        $period = 60 * 60 * 24 * 100;

        // проверяю нет ли уже такого токена
        $phoneToken = UserPhoneToken::findOne($user->id);
        $model = Mobile::getModel();

        if (is_null($phoneToken)) {
            $params = [
                'id'          => $user->id,
                'token'       => Security::generateRandomString(100),
                'finish_time' => time() + $period,
            ];
            if ($model) {
                $params['model'] = $model;
            }
            $phoneToken = new UserPhoneToken($params);
        } else {
            $phoneToken->token = Security::generateRandomString(100);
            $phoneToken->finish_time = time() + $period;
            if ($model) {
                $phoneToken->model = $model;
            }
        }
        $phoneToken->save();

        return $phoneToken;
    }

}
