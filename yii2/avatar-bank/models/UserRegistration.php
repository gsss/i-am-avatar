<?php

namespace avatar\models;

use cs\services\Security;
use Yii;
use yii\db\ActiveRecord;

/**
 * @property integer $id
 * @property integer parent_id
 * @property string code
 * @property integer date_finish
 */
class UserRegistration extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'user_register';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['parent_id', 'code', 'date_finish'], 'required'],
            [['code'], 'string', 'max' => 60],
            [['parent_id', 'date_finish'], 'integer'],
        ];
    }

    /**
     * @param int $user_id
     *
     * @return self
     */
    public static function add($user_id)
    {
        $fields = [
            'parent_id'   => $user_id,
            'code'        => Security::generateRandomString(30),
            'date_finish' => time() + 60 * 60 * 24 * 7,
        ];
        $item = new static($fields);
        $item->save();
        $item->id = self::getDb()->lastInsertID;

        return $item;
    }
}
