<?php
/**
 * Created by PhpStorm.
 * User: Бог-Творец
 * Date: 18.11.2016
 * Time: 3:55
 */

namespace avatar\models;


use avatar\modules\ETH\ServiceEtherScan;
use common\models\avatar\Currency;
use common\models\Token;
use common\services\Security;
use common\services\UsersInCache;
use cs\services\VarDumper;
use Yii;
use Blocktrail\SDK\WalletInterface;
use common\models\avatar\UserBill;
use common\models\avatar\UserBillAddress;
use common\models\avatar\UserBillOperation;
use common\models\piramida\BitCoinUserAddress;
use common\payment\BitCoinBlockTrailPayment;
use yii\base\Exception;

use yii\helpers\ArrayHelper;
use yii\helpers\Json;
use yii\helpers\StringHelper;
use yii\httpclient\Client;

/**
 * Класс для кошелька токена
 *
 * Class WalletToken
 *
 * @package avatar\models
 */
class WalletToken extends Object
{
    /** @var  \common\components\providers\Token */
    public $provider;

    /** @var  \common\models\avatar\UserBill */
    public $billing;

    /** @var  \common\models\Token */
    public $token;

    /**
     * Выдает баланс эфира на кошельке
     *
     * @return array
     * [
     * int          - сколько самых малых единиц
     * double       - сколько в реальных единицах токена
     * decimals     - сколько десятичных знаков после запятой
     * ]
     */
    public function getBalance()
    {
        $int = $this->provider->getBalance($this->billing->address);
        $currency = $this->token->getCurrency();
        $decimals = $currency->decimals;
        $double = $int / pow(10, $decimals);

        return [$int, $double, $decimals];
    }

    /**
     * Отправляет деньги с записыванием информации какие транзакции были сделаны с комментариями
     *
     * @param array $to
     * [
     *      <На какой адрес> => <сколько>
     * ]
     * - <На какой адрес> - string
     * - <сколько> - double | array - сколько монет
     * [
     *      - amount - string
     *      - comment - string | array
     * ]
     * если is_array($comment)
     * [
     *      - from - string
     *      - to - string
     * ]
     * если иначе предполагается
     * [
     *      - from - comment
     *      - to - comment
     * ]
     * @param null | string $password
     *
     * @return string txid
     *
     * @throws
     *
     * 'insufficient funds for gas * price + value'
     * 'Error: could not decrypt key with given passphrase'
     */
    public function pay($to, $password = null)
    {
        $password = $this->billing->getPassword($password);
        $optionsSend = [];

        foreach ($to as $destinationAddress => $amount) {
            // счет как строка
            $from = '';
            $to = '';
            if (is_array($amount)) {
                $options = $amount;
                $amount = $options['amount'];
                $comment = $options['comment'];
                if (is_array($comment)) {
                    $from = $comment['from'];
                    $to = $comment['to'];
                } else {
                    $from = $comment;
                    $to = $comment;
                }
                if (is_array($amount)) {
                    $options = $amount;
                    $amount = $options['amount'];
                    if (isset($options['gasPrice'])) $optionsSend['gasPrice'] = $options['gasPrice'];
                    if (isset($options['gasLimit'])) $optionsSend['gasLimit'] = $options['gasLimit'];
                }
            }

            Yii::trace(
                \yii\helpers\VarDumper::dumpAsString([
                    $this->billing->address,
                    Security::maskPassword($password),
                    $destinationAddress,
                    $amount,
                ]),
                'avatar\models\WalletToken::pay'
            );

            $decimals = $this->token->getCurrency()->decimals;

            // работаю с большими целыми числами, потому что контракт не воспринимает десятичные числа а только целые
            $pow = bcpow(10, $decimals);
            $val = bcmul($amount, $pow);

            $ret = $this->provider->send(
                $this->billing->address,
                $password,
                $destinationAddress,
                $val,
                $optionsSend
            );

            $transaction = $ret['transaction'];

            if ($from) {
                $fromOperation = UserBillOperation::add([
                    'bill_id'     => $this->billing->id,
                    'type'        => UserBillOperation::TYPE_OUT,
                    'transaction' => $transaction,
                    'message'     => $from,
                    'user_id'     => \Yii::$app->user->id,
                ]);
            }
            $walletETH = UserBill::findOne(['address' => $this->billing->address, 'currency' => Currency::ETH]);
            $f = UserBillOperation::add([
                'bill_id'     => $walletETH->id,
                'type'        => UserBillOperation::TYPE_OUT,
                'transaction' => $transaction,
                'message'     => 'Перевод токена: ' . $this->token->getCurrency()->title . ' address:' . $this->token->address . ' Монет: ' . $amount,
                'user_id'     => \Yii::$app->user->id,
            ]);

            if ($to != '') {
                // ищу куда переводятся деньги
                try {
                    $billingTo = UserBill::findOne(['address' => $destinationAddress, 'currency' => $this->token->currency_id, 'mark_deleted' => 0]);
                    if (isset($fromOperation)) {
                        $fromOperation->data = json_encode([
                            'user_id'    => $billingTo->user_id,
                            'billing_id' => $billingTo->id,
                        ]);
                        $fromOperation->save();
                    }
                    UserBillOperation::add([
                        'bill_id'     => $billingTo->id,
                        'type'        => UserBillOperation::TYPE_IN,
                        'transaction' => $transaction,
                        'message'     => $to,
                        'user_id'     => $billingTo->user_id,
                        'data'        => json_encode([
                            'user_id'    => \Yii::$app->user->id,
                            'billing_id' => $this->billing->id,
                        ]),
                    ]);
                } catch (\Exception $e) {
                    \Yii::warning('Не найден адрес назначения для ' . $destinationAddress, 'avatar/noAddressDestination');
                }
            }
            if ($from) {
                $fromOperation->save();
            }

            return $transaction;
        }
    }

    /**
     * Отправляет деньги с записыванием информации какие транзакции были сделаны с комментариями
     *
     * @param array $to
     * [
     *      <На какой адрес> => <сколько>
     * ]
     * - <На какой адрес> - string
     * - <сколько> - double | array - сколько монет
     * [
     *      - amount - string - копейки токена
     *      - comment - string | array
     * ]
     * если is_array($comment)
     * [
     *      - from - string
     *      - to - string
     * ]
     * если иначе предполагается
     * [
     *      - from - comment
     *      - to - comment
     * ]
     * @param string $password - пароль от кошелька Atlantida
     *
     * @return string txid
     *
     * @throws
     *
     * 'insufficient funds for gas * price + value'
     * 'Error: could not decrypt key with given passphrase'
     */
    public function send($to, $password)
    {
        $optionsSend = [];

        foreach ($to as $destinationAddress => $amount) {
            // счет как строка
            $from = '';
            $to = '';
            if (is_array($amount)) {
                $options = $amount;
                $amount = $options['amount'];
                $comment = $options['comment'];
                if (is_array($comment)) {
                    $from = $comment['from'];
                    $to = $comment['to'];
                } else {
                    $from = $comment;
                    $to = $comment;
                }
                if (is_array($amount)) {
                    $options = $amount;
                    $amount = $options['amount'];
                    if (isset($options['gasPrice'])) $optionsSend['gasPrice'] = $options['gasPrice'];
                    if (isset($options['gasLimit'])) $optionsSend['gasLimit'] = $options['gasLimit'];
                }
            }

            Yii::trace(
                \yii\helpers\VarDumper::dumpAsString([
                    $this->billing->address,
                    Security::maskPassword($password),
                    $destinationAddress,
                    $amount,
                ]),
                'avatar\models\WalletToken::pay'
            );

            $ret = $this->provider->send(
                $this->billing->address,
                $password,
                $destinationAddress,
                $amount,
                $optionsSend
            );

            $transaction = $ret['transaction'];

            if ($from) {
                $fromOperation = UserBillOperation::add([
                    'bill_id'     => $this->billing->id,
                    'type'        => UserBillOperation::TYPE_OUT,
                    'transaction' => $transaction,
                    'message'     => $from,
                    'user_id'     => \Yii::$app->user->id,
                ]);
            }
            $walletETH = UserBill::findOne(['address' => $this->billing->address, 'currency' => Currency::ETH]);
            $f = UserBillOperation::add([
                'bill_id'     => $walletETH->id,
                'type'        => UserBillOperation::TYPE_OUT,
                'transaction' => $transaction,
                'message'     => 'Перевод токена: ' . $this->token->getCurrency()->title . ' address:' . $this->token->address . ' Монет: ' . $amount,
                'user_id'     => \Yii::$app->user->id,
            ]);

            if ($to != '') {
                // ищу куда переводятся деньги
                try {
                    $billingTo = UserBill::findOne(['address' => $destinationAddress, 'currency' => $this->token->currency_id, 'mark_deleted' => 0]);
                    if (isset($fromOperation)) {
                        $fromOperation->data = json_encode([
                            'user_id'    => $billingTo->user_id,
                            'billing_id' => $billingTo->id,
                        ]);
                        $fromOperation->save();
                    }
                    UserBillOperation::add([
                        'bill_id'     => $billingTo->id,
                        'type'        => UserBillOperation::TYPE_IN,
                        'transaction' => $transaction,
                        'message'     => $to,
                        'user_id'     => $billingTo->user_id,
                        'data'        => json_encode([
                            'user_id'    => \Yii::$app->user->id,
                            'billing_id' => $this->billing->id,
                        ]),
                    ]);
                } catch (\Exception $e) {
                    \Yii::warning('Не найден адрес назначения для ' . $destinationAddress, 'avatar/noAddressDestination');
                }
            }
            if ($from) {
                $fromOperation->save();
            }

            return $transaction;
        }
    }

    /**
     * Расчитывает
     * Отправляет деньги с записыванием информации какие транзакции были сделаны с комментариями
     *
     * @param array $to
     * [
     *      <На какой адрес> => <сколько>
     * ]
     * - <На какой адрес> - string
     * - <сколько> - double | array - сколько монет
     * [
     *      - amount - string
     *      - comment - string | array
     * ]
     * если is_array($comment)
     * [
     *      - from - string
     *      - to - string
     * ]
     * если иначе предполагается
     * [
     *      - from - comment
     *      - to - comment
     * ]
     * @param null | string $password
     *
     * @return array
     * [
     *      'transaction'
     * ]
     *
     * @throws
     *
     * 'insufficient funds for gas * price + value'
     * 'Error: could not decrypt key with given passphrase'
     */
    public function calculate($to, $password = null)
    {
        $password = $this->billing->getPassword($password);

        foreach ($to as $destinationAddress => $amount) {
            // счет как строка
            $from = '';
            $to = '';
            if (is_array($amount)) {
                $options = $amount;
                $amount = $options['amount'];
                $comment = $options['comment'];
                if (is_array($comment)) {
                    $from = $comment['from'];
                    $to = $comment['to'];
                } else {
                    $from = $comment;
                    $to = $comment;
                }
            }

            Yii::trace(\yii\helpers\VarDumper::dumpAsString([$this->billing->address,
                $password,
                $destinationAddress,
                $amount]), 'avatar\models\WalletToken::calculate');

            $decimals = $this->token->getCurrency()->decimals;

            // работаю с большими целыми числами, потому что контракт не воспринимает десятичные числа а только целые
            $pow = bcpow(10, $decimals);
            $val = bcmul($amount, $pow);

            $ret = $this->provider->sendCalculate(
                $this->billing->address,
                $password,
                $destinationAddress,
                $val
            );
            Yii::info(\yii\helpers\VarDumper::dumpAsString($ret), 'avatar\models\WalletToken::calculate');

            return $ret;
        }
    }

    /**
     * Расчитывает
     * Отправляет деньги с записыванием информации какие транзакции были сделаны с комментариями
     *
     * @param array $to
     * [
     *      <На какой адрес> => <сколько>
     * ]
     * - <На какой адрес> - string
     * - <сколько> - double | array - сколько монет
     * [
     *      - amount - string
     *      - comment - string | array
     * ]
     * если is_array($comment)
     * [
     *      - from - string
     *      - to - string
     * ]
     * если иначе предполагается
     * [
     *      - from - comment
     *      - to - comment
     * ]
     * @param null | string $password
     * @param string        $functionName
     * @param array         $params
     *
     * @return array
     * [
     *      'cost'
     *      'gasPrice'
     * ]
     * @throws
     *
     * 'insufficient funds for gas * price + value'
     * 'Error: could not decrypt key with given passphrase'
     */
    public function calculateFunction($functionName, $params = [], $password = null)
    {
        $password = $this->billing->getPassword($password);

        Yii::info(\yii\helpers\VarDumper::dumpAsString([
            $this->billing->address,
            $password,
            $this->token->address,
            $this->token->abi,
            $functionName,
            $params
        ]), 'avatar\common\components\providers\WalletToken::contractCalculate()');

        $ret = $this->provider->contractCalculate(
            $this->billing->address,
            $password,
            $this->token->address,
            $this->token->abi,
            $functionName,
            $params
        );
        Yii::info(\yii\helpers\VarDumper::dumpAsString($ret), 'avatar\models\WalletToken::calculateFunction');

        return $ret;
    }

    /**
     * Расчитывает
     * Отправляет деньги с записыванием информации какие транзакции были сделаны с комментариями
     *
     * @param array $to
     *
     * @param null | string $password
     * @param string        $functionName
     * @param array         $params
     *
     * @return array
     * [
     *      'cost'
     *      'gasPrice'
     * ]
     * @throws
     *
     * 'insufficient funds for gas * price + value'
     * 'Error: could not decrypt key with given passphrase'
     */
    public function contract($functionName, $params = [], $password = null)
    {
        $password = $this->billing->getPassword($password);

        $ret = $this->provider->contract(
            $this->billing->address,
            $password,
            $this->token->address,
            $this->token->abi,
            $functionName,
            $params
        );
        Yii::info(\yii\helpers\VarDumper::dumpAsString($ret), 'avatar\models\WalletToken::contract');

        return $ret;
    }

    /**
     * Возвращает список транзакций, выводит по 20 транцакций, без комментариев
     *
     * @param int $page страница для вывода
     *
     * @return array сортируются транзакции по дате, первые - молодые
     * [
         * {
             * "timestamp": 1507394270,
             * "transactionHash": "0xa593a273b3c6fdc151849fb8864d656172f79e8d52014d2ab4a536387670ed60",
             * "tokenInfo": {
                 * "address": "0x86fa049857e0209aa7d9e616f7eb3b3b78ecfdb0",
                 * "name": "EOS",
                 * "decimals": 18,
                 * "symbol": "EOS",
                 * "totalSupply": "1000000000000000000000000000",
                 * "owner": "0xd0a6e6c54dbc68db5db3a091b171a77407ff7ccf",
                 * "txsCount": 624071,
                 * "transfersCount": 733467,
                 * "lastUpdated": 1510883995,
                 * "totalIn": 2.5964967036019e+27,
                 * "totalOut": 2.5964967036019e+27,
                 * "issuancesCount": 0,
                 * "holdersCount": 108009,
                 * "description": "https://eos.io/",
                 * "price": {
                     * "rate": "1.65861",
                     * "diff": 3.14,
                     * "diff7d": 43.49,
                     * "ts": "1510884263",
                     * "marketCapUsd": "797326417.0",
                     * "availableSupply": "480719649.0",
                     * "volume24h": "67217100.0",
                     * "currency": "USD"
                 * }
             * },
             * "type": "transfer",
             * "value": "1500000000000000000",
             * "from": "0x324f0b05cb1e2c586e07fa7da14312f0fced9f7e",
             * "to": "0x2ed7ea0efc43f19cce01903d43f85e04fa92d855"
         * }, ...
     * ]
     */
    private function transactionList($page = 1)
    {
        $data = $this->provider->getTransactions($this->billing->address, $page);

        return $data;
    }

    /**
     *
     * @param int $page
     *
     * @return array
     */
    private function transactionList2($page = 1)
    {
        /**
         * Я могу вычислить только те транзакции которые исходящие
         * но не могу вычислить входящие в кошелекЮ потому что они были отправлены с другого кошелька
         */
        $data = $this->provider->getTransactions($this->billing->address, $page);

        return $data;
    }

    /**
     * Возвращает список транзакций, выводит по 20 транцакций, с комментариями
     *
     * @param int $page страница для вывода
     *
     * @return array
     * [
     *      [
     *          'txid'      => '0xdf57c61e66bfa574625ad7dc4b61f8a8261a9564ee953d13d5a4e1f71caf3193'
     *          'time'      => '1501851527'
     *          'from'      => '0x64f9a89cf477fa58cb4c439515368f328260c072'
     *          'to'        => '0x214aa8a188691a942e268595674b17e2d38ac5de'
     *          'direction' => -1
     *          'value'     => '10000' // измеряется в монетах
     *          'comment'   => 'Перевод от клиента №'
     *          "data": "{}"
     *      ],
     *      //...
     * ]
     */
    public function transactionListComments($page = 1)
    {
        $list = $this->transactionList($page);
        $list = $this->concatComments($this->billing, $list);
//        $list = $this->changeRateComments($list);

        return $list;
    }

    /**
     * Возвращает список транзакций, выводит по 20 транцакций, с комментариями
     *
     * @param int $page страница для вывода
     *
     * @return array
     * [
     *      [
     *          'txid'      => '0xdf57c61e66bfa574625ad7dc4b61f8a8261a9564ee953d13d5a4e1f71caf3193'
     *          'time'      => '1501851527'
     *          'from'      => '0x64f9a89cf477fa58cb4c439515368f328260c072'
     *          'to'        => '0x214aa8a188691a942e268595674b17e2d38ac5de'
     *          'direction' => -1
     *          'value'     => '10000' // измеряется в монетах
     *          'comment'   => 'Перевод от клиента №'
     *          "data": "{}"
     *      ],
     *      //...
     * ]
     */
    public function transactionListComments2($page = 1)
    {
        $list = $this->transactionList2($page);
        $list = $this->concatComments($this->billing, $list);
//        $list = $this->changeRateComments($list);

        return $list;
    }

    /**
     * Присоединяет колонку комментарий для транзакций
     *
     * @param \common\models\avatar\UserBill $billing
     * @param array $data транзакции кошелька
     * [
     *      [
     *          'txid'      => '0xdf57c61e66bfa574625ad7dc4b61f8a8261a9564ee953d13d5a4e1f71caf3193'
     *          'time'      => '1501851527'
     *          'from'      => '0x64f9a89cf477fa58cb4c439515368f328260c072'
     *          'to'        => '0x214aa8a188691a942e268595674b17e2d38ac5de'
     *          'direction' => -1
     *          'value'     => '10000'
     *      ]
     *      //...
     * ]
     *
     * @return array
     * [
     *      [
     *          //...
     *          'comment' => 'Перевод от клиента №'
     *          'type' => 1
     *      ],
     *      //...
     * ]
     */
    private function concatComments($billing, $data, $itemsPerPage = 20)
    {
        $comments = \common\models\avatar\UserBillOperation::find()
            ->where(['bill_id' => $billing->id])
            ->orderBy(['created_at' => SORT_DESC])
            ->all();
        ;

        $rows = [];
        foreach($data as $rowOld) {
            $rowNew = $rowOld;
            $comment = $this->getTransaction($rowOld['transactionHash'], $comments);
            if (!is_null($comment)) {
                $rowNew['comment'] = $comment->message;
                $rowNew['type'] = $comment->type;
                $rowNew['data'] = Json::decode($comment->data);
                if (isset($rowNew['data']['user_id'])) {
                    $rowNew['data']['user'] = UsersInCache::find($rowNew['data']['user_id']);
                    if (isset($rowNew['data']['user']['avatar'])) $rowNew['data']['user']['avatar'] = 'https://avatarnetwork.io' . $rowNew['data']['user']['avatar'];
                }
                if (isset($rowNew['data']['billing_id'])) {
                    $billing = UserBill::findOne($rowNew['data']['billing_id']);
                    $rowNew['data']['billing'] = [
                        'id'    => $billing->id,
                        'name'  => $billing->name,
                    ];
                }
            } else {
                $rowNew['comment'] = '';
            }

            if ($rowNew['from'] == $this->billing->address) {
                $rowNew['direction'] = -1;
            }
            if ($rowNew['to'] == $this->billing->address) {
                $rowNew['direction'] = 1;
            }

            $rows[] = $rowNew;
        }

        return $rows;
    }


    /**
     * Если необхдимо то конвертирует валюту
     * Если `$user->currency_view` > 0, то добавляются два параметра `value_converted` и `fee_converted`
     *
     * @param array $transactionList
     *
     * @return array
     */
    private function changeRateComments($transactionList)
    {
        /** @var \common\models\UserAvatar $user */
        $user = Yii::$app->user->identity;
        $currencyView = $user->currency_view;

        if (!is_null($currencyView)) {
            if ($currencyView > 0) {
                for ($i = 0; $i < count($transactionList); $i++) {
                    $item = $transactionList[$i];
                    $feeWei = $item['value'];
                    $feeEth = $feeWei / 1000000000000000000;
                    $transactionList[$i]['value_converted'] = \common\models\avatar\Currency::convertBySettings($feeEth, 'ETH', true);

                    $gasPrice = $item['gasPrice'];
                    $gasUsed = $item['gasUsed'];
                    $feeWei = $gasPrice * $gasUsed;
                    $feeEth = $feeWei / 1000000000000000000;

                    $transactionList[$i]['fee_converted'] = \common\models\avatar\Currency::convertBySettings($feeEth, 'ETH', true);
                }
            }
        }

        return $transactionList;
    }

    /**
     * Ищет определенную транзакцию в списке коментариев и возвращает ее
     *
     * @param int $hash
     * @param \common\models\avatar\UserBillOperation[] $array
     * @return \common\models\avatar\UserBillOperation | null
     */
    private function getTransaction($hash, $array)
    {
        foreach($array as $row) {
            $t = ArrayHelper::getValue($row, 'transaction', '');
            if ($t) {
                if ($t == $hash) {
                    return $row;
                }
            }
        }
        return null;
    }


    /**
     * Создает кошелек для токена
     *
     * Создает счет \common\models\avatar\UserBill
     *
     * @param \common\models\avatar\UserBill | int  $billing        счет ETH для которого нужно сделать кошелек
     * @param \common\models\Token | int            $token          идентификатор токена или объект для которого нужно создать кошелек
     * @param string                                $name           назвние счета
     * @param string                                $password       пароль от кабинета
     * @param int | null                            $userId         пользователь к которому будет прикреплен счет
     * @param int                                   $passwordType   вид хранения пароля
     *
     * @return \avatar\models\WalletToken
     *
     * @throws
     */
    public static function create($billing, $token, $name, $password, $userId = null, $passwordType = UserBill::PASSWORD_TYPE_HIDE_CABINET)
    {
        if (!($billing instanceof UserBill)) {
            $billing = UserBill::findOne($billing);
        }
        if (!($token instanceof \common\models\Token)) {
            $token = \common\models\Token::findOne($token);
        }

        $passwordWallet = $billing->getPassword($password);
        switch ($passwordType) {
            case UserBill::PASSWORD_TYPE_HIDE_CABINET:
            case UserBill::PASSWORD_TYPE_HIDE_PRIVATE:
                $key = $password;
                $passwordHash = UserBill::passwordEnСript($passwordWallet, $key);
                break;
            case UserBill::PASSWORD_TYPE_OPEN:
                $passwordHash = $passwordWallet;
                break;
            case UserBill::PASSWORD_TYPE_OPEN_CRYPT:
                $key = Yii::$app->params['unlockKey'];
                $passwordHash = UserBill::passwordEnСript($passwordWallet, $key);
                break;
            default:
                throw new \Exception('Unknown $passwordType');
        }

        /** @var \common\models\avatar\UserBill $billing */
        $address = $billing->address;

        Yii::info(\yii\helpers\VarDumper::dumpAsString([$passwordHash, $passwordWallet, $address, $passwordType]), 'avatar\models\WalletToken::create:1');

        $t = Yii::$app->db->beginTransaction();
        try {
            // Сохраняю счет
            $billing = \common\models\avatar\UserBill::add([
                'user_id'       => $userId,
                'address'       => $address,
                'name'          => $name,
                'password'      => $passwordHash,
                'password_type' => $passwordType,
                'currency'      => $token->currency_id,
            ]);

            $walletAvatar = new self([
                'billing' => $billing,
            ]);

            $t->commit();

            return $walletAvatar;
        } catch (\Exception $e) {
            Yii::warning($e->getMessage(), 'avatar\common\components\providers\Token::create');
            $t->rollBack();

            return null;
        }
    }

    /**
     * Создает кошелек для токена
     *
     * Создает счет \common\models\avatar\UserBill
     *
     * @param \common\models\avatar\UserBill | int  $billing        счет ETH для которого нужно сделать кошелек
     * @param \common\models\Token | int            $token          идентификатор токена или объект для которого нужно создать кошелек
     * @param string                                $name           назвние счета
     * @param int | null                            $userId         пользователь к которому будет прикреплен счет
     *
     * @return \avatar\models\WalletToken
     *
     * @throws \yii\db\Exception
     */
    public static function createFromEth($billing, $token, $name, $userId = null)
    {
        if (!($billing instanceof UserBill)) {
            $billing = UserBill::findOne($billing);
        }
        if (!($token instanceof \common\models\Token)) {
            $token = \common\models\Token::findOne($token);
        }

        /** @var \common\models\avatar\UserBill $billing */
        $address = $billing->address;

        $t = Yii::$app->db->beginTransaction();
        try {
            // Сохраняю счет
            $billing = \common\models\avatar\UserBill::add([
                'user_id'       => $userId,
                'address'       => $address,
                'name'          => $name,
                'password'      => $billing->password,
                'password_type' => $billing->password_type,
                'currency'      => $token->currency_id,
            ]);

            $walletToken = new self([
                'billing' => $billing,
            ]);

            $t->commit();

            return $walletToken;

        } catch (\Exception $e) {
            Yii::warning($e->getMessage(), 'avatar\\common\\components\\providers\\Token::create');
            $t->rollBack();

            return null;
        }
    }
}