<?php
/**
 * Created by PhpStorm.
 * User: Ra-m-ha
 * Date: 10.01.2019
 * Time: 19:17
 */

namespace avatar\models;


use yii\base\Model;

class M1 extends Model
{
    /**
     * @var array
     */
    public $user_roles;
    public $all_roles;

    public function __get($name)
    {
        foreach ($this->user_roles as $role) {
            if (md5($role) == $name) return true;
        }
        return false;
    }
}