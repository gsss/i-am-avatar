<?php

namespace avatar\models;

use cs\services\Security;
use Yii;
use yii\db\ActiveRecord;

/**
 * @property integer id
 * @property integer status
 * @property integer school_id
 * @property integer created_at
 *
 * @property integer place_type_id
 * @property string  code
 * @property string  whatsapp
 * @property string  phone
 * @property string  address
 * @property string  place
 *
 * @property integer user_id
 * @property string  pay_id
 * @property string  is_payd
 * @property string  passport_scan1
 * @property string  passport_scan2
 * @property string  address_gruz
 * @property string  address_pochta
 * @property string  name_first
 * @property string  name_last
 * @property string  name_middle
 *
 * @property string  passport_ser
 * @property int     passport_number
 * @property string  passport_vidan_kem
 * @property date    passport_vidan_date
 * @property string  passport_vidan_num
 * @property date    passport_born_date
 * @property string  passport_born_place
 *
 * @property integer   user1_id
 * @property integer   user2_id
 * @property string    public_key1
 * @property string    public_key2
 * @property string    hash
 * @property string    signature1
 * @property string    signature2
 * @property string    file

 */
class UserEnter extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'user_enter';
    }

    public function successShop()
    {

    }
}
