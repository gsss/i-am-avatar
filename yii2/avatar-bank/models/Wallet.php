<?php
/**
 * Created by PhpStorm.
 * User: Бог-Творец
 * Date: 18.11.2016
 * Time: 3:55
 */

namespace avatar\models;


use Blocktrail\SDK\WalletInterface;
use common\components\providers\BTC;
use common\models\avatar\Currency;
use common\models\avatar\UserBill;
use common\models\avatar\UserBillAddress;
use common\models\avatar\UserBillOperation;
use common\models\piramida\BitCoinUserAddress;
use common\payment\BitCoinBlockTrailPayment;
use common\services\Security;
use common\services\UsersInCache;
use cs\services\VarDumper;
use Yii;

use yii\helpers\ArrayHelper;
use yii\helpers\Json;

class Wallet extends Object
{
    /** @var  \Blocktrail\SDK\WalletInterface */
    public $blockTrailWallet;

    /** @var  \common\models\avatar\UserBill */
    public $billing;

    /**
     * Отправляет деньги с записыванием информации какие транзакции были сделаны с комментариями
     *
     * @param array $to
     * [
     *      <На какой адрес> => <сколько>
     * ]
     * - <На какой адрес> - string | \common\models\avatar\UserBill -
     * - <сколько> - double | array - сколько BTC
     * [
     *      - amount - string
     *      - comment - string | array
     * ]
     * если is_array($comment)
     * [
     *      - from - string
     *      - to - string
     * ]
     * если иначе предполагается
     * [
     *      - from - comment
     *      - to - comment
     * ]
     * @param string $feeStrategy стратегия расчета комиссии
     *                                \Blocktrail\SDK\WalletInterface::FEE_STRATEGY_OPTIMAL - оптимальная цена комиссии гарантирующая 75% вероятности попадания транзакции в следущий блок
     *                                \Blocktrail\SDK\WalletInterface::FEE_STRATEGY_BASE_FEE - стандартная цена за транзакцию 0,0001 за кб
     *                                \Blocktrail\SDK\WalletInterface::FEE_STRATEGY_LOW_PRIORITY - пониженная цена за транзакцию 0,00005 за кб
     *
     * @return string
     */
    public function pay($to, $feeStrategy = WalletInterface::FEE_STRATEGY_OPTIMAL)
    {
        if (count($to) == 1) {
            return $this->payOne($to, $feeStrategy);
        } else {
            if (isset($to['transaction'])) {
                return $this->payManyTransaction($to);
            } else {
                return $this->payMany($to);
            }
        }
    }

    /**
     * Производит множественные переводы
     *
     * @param array $to
     * [
     * 'address1' => $amount1,
     * 'address3' => $amount2,
     * 'address4' => $amount3,
     * 'address5' => $amount4,
     * ]
     * @return string транзакция
     */
    private function payMany($to)
    {
        /**
         * [
         *  '<address>' => 'comment',
         *  // ...
         * ]
         */
        $commentList = [];
        /** @var array $bitCoinAddressList сюда сохраняются списое адресов для отправки туда денег */
        $bitCoinAddressList = [];
        foreach ($to as $address => $amount) {
            if (is_array($amount)) {
                $commentList[$address] = $amount['comment'];
                $amount = $amount['amount'];
                $amount = (int)($amount * 100000000);
            } else {
                $amount = (int)($amount * 100000000);
            }
            $bitCoinAddressList[$address] = $amount;
            $transaction = $this->blockTrailWallet->pay($bitCoinAddressList);
            $operationFrom = [];
            foreach ($commentList as $address => $comment) {
                // ищу куда переводятся деньги
                try {
                    $addressObject = UserBillAddress::findOne(['address' => $address]);
                    $billingTo = $addressObject->getBilling();
                    $operationFrom[] = [
                        'user_id'    => $billingTo->user_id,
                        'billing_id' => $billingTo->id,
                    ];
                    UserBillOperation::add([
                        'bill_id'     => $billingTo->id,
                        'type'        => UserBillOperation::TYPE_IN,
                        'transaction' => $transaction,
                        'message'     => $comment,
                        'user_id'     => $billingTo->user_id,
                        'data'        => json_encode([
                            'user_id'    => \Yii::$app->user->id,
                            'billing_id' => $this->billing->id,
                        ]),
                    ]);
                } catch (\Exception $e) {
                    \Yii::warning('Не найден адрес назначения для ' . $address, 'avatar//noAddressDestination');
                }

            }
            if (isset($fromOperation)) {
                $fromOperation->data = json_encode([
                    'destionations' => $operationFrom
                ]);
                $fromOperation->save();
            }

        }
        return '';
    }

    /**
     * Производит множественные переводы
     *
     * @param array $to
     * [
     *      'comment'     => '', // комметарий для транзакции кошелька из которой производится перевод
     *      'transaction' => [
     *          'address' => [
     *              'amount'  => $amount,
     *              'comment' => '', // комметарий для транзакции кошелька в которой производится перевод
     *          ],
     *          'address' => [
     *              'amount'  => $amount,
     *              'comment' => '', // комметарий для транзакции кошелька в которой производится перевод
     *          ],
     *          //..
     *      ]
     * ]
     * @return string транзакция
     */
    private function payManyTransaction($to)
    {
        return '';
    }

    /**
     * Производит один перевод на один адрес
     *
     * @param array $to массив из одного элемента
     * @param string $feeStrategy стратегия расчета комиссии
     *                                \Blocktrail\SDK\WalletInterface::FEE_STRATEGY_OPTIMAL - оптимальная цена комиссии гарантирующая 75% вероятности попадания транзакции в следущий блок
     *                                \Blocktrail\SDK\WalletInterface::FEE_STRATEGY_BASE_FEE - стандартная цена за транзакцию 0,0001 за кб
     *                                \Blocktrail\SDK\WalletInterface::FEE_STRATEGY_LOW_PRIORITY - пониженная цена за транзакцию 0,00005 за кб
     *
     * @return string транзакция
     */
    private function payOne($to, $feeStrategy = WalletInterface::FEE_STRATEGY_OPTIMAL)
    {
        foreach ($to as $address => $amount) {
            // счет как строка
            $from = '';
            $to = '';
            if (is_array($amount)) {
                $options = $amount;
                $amount = $options['amount'];
                $comment = $options['comment'];
                if (is_array($comment)) {
                    $from = $comment['from'];
                    $to = $comment['to'];
                } else {
                    $from = $comment;
                    $to = $comment;
                }
            }
            $amount = (int)($amount * 100000000);
            try {
                $transaction = $this->blockTrailWallet->pay([$address => $amount], null, false, true, $feeStrategy);
            } catch (\Exception $e) {
                Yii::error($e->getMessage(), 'avatar\models\Wallet::payOne');
                throw $e;
            }
            if ($from) {
                $fromOperation = UserBillOperation::add([
                    'bill_id'     => $this->billing->id,
                    'type'        => UserBillOperation::TYPE_OUT,
                    'transaction' => $transaction,
                    'message'     => $from,
                    'user_id'     => \Yii::$app->user->id,
                ]);
            }

            if ($to != '') {
                // ищу куда переводятся деньги
                try {
                    $addressObject = UserBillAddress::findOne(['address' => $address]);
                    $billingTo = $addressObject->getBilling();
                    if (isset($fromOperation)) {
                        $fromOperation->data = json_encode([
                            'user_id'    => $billingTo->user_id,
                            'billing_id' => $billingTo->id,
                        ]);
                        $fromOperation->save();
                    }
                    UserBillOperation::add([
                        'bill_id'     => $billingTo->id,
                        'type'        => UserBillOperation::TYPE_IN,
                        'transaction' => $transaction,
                        'message'     => $to,
                        'user_id'     => $billingTo->user_id,
                        'data'        => json_encode([
                            'user_id'    => \Yii::$app->user->id,
                            'billing_id' => $this->billing->id,
                        ]),
                    ]);
                } catch (\Exception $e) {
                    \Yii::warning('Не найден адрес назначения для ' . $address, 'avatar//noAddressDestination');
                }
            }

            return $transaction;
        }
    }


    /**
     * @return array
     * [
     *      <confirmed>, <unconfirmed>
     * ]
     *
     * @throws \Exception
     */
    public function getBalance()
    {
        $balance = $this->blockTrailWallet->getBalance();

        return [
            $balance[0] / 100000000,
            $balance[1] / 100000000,
        ];

    }


    /**
     * Возвращает список транзакций, выводит по 20 транцакций, без комментариев
     *
     * @param int $page страница для вывода
     *
     * @return array сортируются транзакции по дате, первые - молодые
     */
    public function transactionList($page = 1)
    {
        $list = $this->blockTrailWallet->transactions($page, 20, 'desc');

        return $list;
    }

    /**
     * Возвращает список транзакций, выводит по 20 транцакций, с комментариями
     *
     * @param int $page страница для вывода
     *
     * @return array сортируются транзакции по дате, первые - молодые
     */
    public function transactionListComments($page = 1)
    {
        $list = $this->transactionList($page);
        $list = $this->concatComments($this->billing, $list);

        return $list;
    }


    /**
     * Присоединяет колонку комментарий для транзакций
     *
     * @param \common\models\avatar\UserBill $billing
     * @param array $data транзакции кошелька
     * [
     *      [
     *          //...
     *      ],
     *      //...
     * ]
     *
     * @return array
     * [
     *      [
     *          //...
     *          'comment' => 'Перевод от клиента №'
     *          'type' => 1
     *      ],
     *      //...
     * ]
     */
    private function concatComments($billing, $data, $itemsPerPage = 20)
    {
        $firstTime = $data[0]['timeStamp'];

        $comments = \common\models\avatar\UserBillOperation::find()
            ->where([
                'and',
                ['bill_id' => $billing->id],
                ['<=', 'created_at', $firstTime + 60],
            ])
            ->limit($itemsPerPage + 10)
            ->orderBy(['created_at' => SORT_DESC])
            ->all()
        ;
        $unset = [
//            'is_coinbase',
//            'total_input_value',
//            'inputs',
//            'outputs',
//            'wallet',
//            'addresses',
//            'total_output_value',
        ];
        $rows = [];
        foreach($data as $rowOld) {
            $rowNew = $rowOld;
            $comment = $this->getTransaction($rowOld['hash'], $comments);
            if (!is_null($comment)) {
                $rowNew['comment'] = $comment->message;
                $rowNew['type'] = $comment->type;
                $rowNew['data'] = Json::decode($comment->data);
                if (isset($rowNew['data']['user_id'])) {
                    $rowNew['data']['user'] = UsersInCache::find($rowNew['data']['user_id']);
                    if (isset($rowNew['data']['user']['avatar'])) $rowNew['data']['user']['avatar'] = 'https://www.avatar-bank.com' . $rowNew['data']['user']['avatar'];
                }
                if (isset($rowNew['data']['billing_id'])) {
                    $billing = UserBill::findOne($rowNew['data']['billing_id']);
                    $rowNew['data']['billing'] = [
                        'id'    => $billing->id,
                        'name'  => $billing->name,
                    ];
                }
            } else {
                $rowNew['comment'] = '';
                $rowNew['type'] = null;
            }
            $v = $rowNew['wallet_value_change'];
            if ($v < 0) {
                $rowNew['wallet_value_change'] += $rowNew['total_fee'];
            }

            foreach ($unset as $name) {
                unset($rowNew[$name]);
            }
            // time
            $t = new \DateTime($rowNew['time']);
            $rowNew['time'] = $t->format('U');

            $rows[] = $rowNew;
        }

        return $rows;
    }

    /**
     * Ищет определенную транзакцию в списке коментариев и возвращает ее
     *
     * @param int $hash
     * @param \common\models\avatar\UserBillOperation[] $array
     * @return \common\models\avatar\UserBillOperation | null
     */
    public function getTransaction($hash, $array)
    {
        foreach($array as $row) {
            $t = ArrayHelper::getValue($row, 'transaction', '');
            if ($t) {
                if ($t == $hash) {
                    return $row;
                }
            }
        }
        return null;
    }


    /**
     * @return string
     * @throws \Exception
     */
    public function getNewAddress()
    {
        $address = $this->blockTrailWallet->getNewAddress();
        (new \common\models\avatar\UserBillAddress([
            'bill_id'    => $this->billing->id,
            'address'    => $address,
            'created_at' => time(),
        ]))->save();

        return $address;
    }

    /**
     * Создается кошелек
     *
     * @param string        $name
     * @param int | null    $userId
     *
     * @return null | \avatar\models\Wallet
     * @throws \yii\db\Exception
     */
    public static function create($name, $password, $userId = null, $passwordType = \common\models\avatar\UserBill::PASSWORD_TYPE_OPEN_CRYPT)
    {
        $passwordData = UserBill::createPassword($password, $passwordType);
        Yii::info(\yii\helpers\VarDumper::dumpAsString($passwordData), 'avatar\models\Wallet::create:1');
        $passwordWallet = $passwordData['passwordWallet'];
        $passwordHash = $passwordData['passwordHash'];

        $identity = 'bill_' . Security::generateRandomString();
        $walletInfo = [];
        if (YII_ENV != 'dev') {
            try {
                /** @var \common\components\providers\BTC $btc */
                $btc = Yii::$app->BTC;
                $client = $btc->getProvider()->getClient();
                $walletInfo = $client->createNewWallet([
                    'identifier' => $identity,
                    'password'   => $passwordWallet,
                ]);
            } catch (\Exception $e) {
                Yii::warning($e->getTrace(), 'avatar\\common\\components\\providers\\BTC::create');
                return null;
            }
        }

        $t = Yii::$app->db->beginTransaction();
        try {
            /** @var \Blocktrail\SDK\WalletInterface $wallet */
            $wallet = $walletInfo[0];
            if (!YII_ENV_DEV) {
                $address = $wallet->getNewAddress();
            } else {
                $address = '4' . Security::generateRandomString();
            }
            // Сохраняю счет
            $billing = new \common\models\avatar\UserBill([
                'user_id'       => $userId,
                'address'       => $address,
                'identity'      => $identity,
                'password'      => $passwordHash,
                'password_type' => $passwordType,
                'name'          => $name,
                'currency'      => Currency::BTC,
                'mark_deleted'  => 0,
                'created_at'    => time(),
            ]);
            $ret = $billing->save();
            $billing->id = \common\models\avatar\UserBill::getDb()->lastInsertID;
            (new \common\models\avatar\UserBillConfig([
                'id'       => $billing->id,
                'pubKeys'  => Json::encode(YII_ENV_DEV ? '' : $wallet->getBlocktrailPublicKeys()),
                'config'   => \yii\helpers\VarDumper::dumpAsString($walletInfo),
//                'config'   => Security\AES::encrypt256CBC(\yii\helpers\VarDumper::dumpAsString($walletInfo), UserBill::passwordToKey32($password)),
            ]))->save();
            Yii::info(\yii\helpers\VarDumper::dumpAsString($walletInfo), 'avatar\models\Wallet::create:2');
            Yii::info(\yii\helpers\VarDumper::dumpAsString($walletInfo[1]), 'avatar\models\Wallet::create:3');
            Yii::info(\yii\helpers\VarDumper::dumpAsString($walletInfo[1]), 'avatar\models\Wallet::create:3');

            $walletAvatar = new Wallet([
                'billing'          => $billing,
                'blockTrailWallet' => $wallet,
            ]);

            // Сохраняю адрес в списке адресов счета
            (new UserBillAddress([
                'bill_id'    => $billing->id,
                'address'    => $address,
                'created_at' => time(),
            ]))->save();

            $t->commit();

            return $walletAvatar;
        } catch (\Exception $e) {
            Yii::info($e->getTrace(), 'avatar\\common\\components\\providers\\BTC::create');
            $t->rollBack();

            return null;
        }
    }

} 