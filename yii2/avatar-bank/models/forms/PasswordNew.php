<?php

namespace avatar\models\forms;

use app\models\User;
use common\models\UserAvatar;
use cs\web\Exception;
use Yii;
use yii\base\Model;
use yii\helpers\Url;

/**
 *
 */
class PasswordNew extends Model
{
    public $passwordOld;
    public $password1;
    public $password2;

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            [
                [
                    'passwordOld',
                    'password1',
                    'password2'
                ],
                'required',
                'message' => 'Это поле должно быть заполнено обязательно'
            ],
            ['password2', 'compare', 'compareAttribute' => 'password1', 'operator' => '==', 'message' => 'Пароли должны совпадать'],
            ['passwordOld', 'validatePassword'],
        ];
    }

    public function validatePassword($attribute, $params)
    {
        if (!$this->hasErrors()) {
            /** @var \common\models\UserAvatar $user */
            $user = Yii::$app->user->identity;
            if (!$user->validatePassword($this->passwordOld)) {
                $this->addError($attribute, 'Не верный пароль');
            }
        }
    }

    /**
     * Устанавливает
     *
     * @param  \common\models\UserAvatar
     *
     * @return boolean
     *
     * @throws \cs\web\Exception
     */
    public function action(\common\models\UserAvatar $user)
    {
        $user->changePasswordInBills($this->passwordOld, $this->password1);

        return $user->setPassword($this->password1);
    }
}
