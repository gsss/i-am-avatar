<?php
namespace avatar\models\forms;

use common\models\school\File;
use cs\base\FormActiveRecord;
use cs\services\Url;
use cs\services\VarDumper;
use Yii;
use yii\base\Model;
use yii\base\NotSupportedException;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\web\IdentityInterface;

/**
 */
class UserMap extends Model
{
    public $place;
    public $placeLat;
    public $placeLng;
    public $type_id;

    /** @var \common\models\shop\UserMap */
    public $row;

    public function init()
    {
        $row = \common\models\shop\UserMap::findOne(['user_id' => Yii::$app->user->id]);
        if (is_null($row)) {
            $row = new \common\models\shop\UserMap(['user_id' => Yii::$app->user->id]);
        } else {
            $this->placeLat = $row->lat / 1000000000;
            $this->placeLng = $row->lng / 1000000000;
            $this->place = [
                'lat'   => $this->placeLat,
                'lng'   => $this->placeLng,
                'place' => '',
            ];
            $this->type_id = $row->type_id;
        }
        $this->row = $row;
    }

    public function rules()
    {
        return [
            ['place', 'required'],
            ['place', 'string'],
            ['type_id', 'integer'],
        ];
    }

    /**
     * @return \common\models\shop\UserMap
     */
    public function action()
    {
        $UserMap = Yii::$app->request->post('UserMap');
        $this->placeLat = $UserMap['place-lat'];
        $this->placeLng = $UserMap['place-lng'];


        $this->row->lat = (int) (1000000000 * $this->placeLat);
        $this->row->lng = (int) (1000000000 * $this->placeLng);
        $this->row->type_id = $this->type_id;
        $this->row->save();

        return $this->row;
    }
}
