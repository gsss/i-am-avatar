<?php
namespace avatar\models\forms;

use cs\base\FormActiveRecord;
use Yii;
use yii\base\Model;
use yii\base\NotSupportedException;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\web\IdentityInterface;

/**
 * PaySystem model
 *
 * @property integer id
 * @property string  image
 * @property string  hash
 * @property string  data
 * @property integer created_at
 */
class Security extends Model
{

    public $is_protect_cabinet_browser;

    public function rules()
    {
        return [['is_protect_cabinet_browser', 'integer']];
    }
}
