<?php

namespace avatar\models\forms;

use common\models\avatar\UserBill;
use common\models\SendLetter;
use common\models\UserAvatar;
use common\models\UserSeed;
use common\services\Security\AES;
use cs\Application;
use cs\services\Str;
use Yii;
use yii\base\Model;
use yii\helpers\ArrayHelper;
use yii\helpers\VarDumper;

/**
 */
class RepareWallets extends Model
{
    /** @var  string SEEDS */
    public $seeds;

    /** @var  string пароль от кабинета */
    public $password;

    /** @var  string пароль от кабинета по которому были зашифрованы старые коешельки (устанавливается в tryToUnlock) */
    public $_passwordCabinetOld;

    /** @var  string key32 из tryToUnlock */
    public $_key32;

    /** @var  \common\models\UserSeed  (устанавливается в tryToUnlock) */
    public $_hashSeedsObject;

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            [['seeds', 'password'], 'required'],
            ['seeds', 'string'],
            ['seeds', 'normalizeSeeds'],
            ['seeds', 'tryToUnlock'], // устанавливает $_passwordCabinetOld, $_hashSeedsObject, $_key32
            ['password', 'string'],
            ['password', 'validatePassword'],
        ];
    }

    public function normalizeSeeds($attribute, $params)
    {
        if (!$this->hasErrors()) {
            $ret = [];
            $arr = explode("\n", $this->seeds);
            foreach ($arr as $row) {
                $arr2 = explode(" ", $row);
                foreach ($arr2 as $word) {
                    $ret[] = trim($word);
                }
            }
            $this->seeds = join(' ', $ret);
        }
    }

    public function tryToUnlock($attribute, $params)
    {
        if (!$this->hasErrors()) {
            // распаковываю пароль от кабинета старый
            $sha256 = hash('sha256', $this->seeds);
            $key32 = substr($sha256, 0, 32);
            $hashSeedsObject = UserSeed::findOne(Yii::$app->user->id);
            $userCabinetPassword = \common\services\Security\AES::decrypt256CBC($hashSeedsObject->seeds, $key32);
            if ($userCabinetPassword == '') {
                $this->addError($attribute, 'Не верный SEEDS');
                return;
            }
            $this->_passwordCabinetOld = $userCabinetPassword;
            $this->_hashSeedsObject = $hashSeedsObject;
            $this->_key32 = $key32;
        }
    }

    public function validatePassword($attribute, $params)
    {
        if (!$this->hasErrors()) {
            /** @var \common\models\UserAvatar $user */
            $user = Yii::$app->user->identity;
            if (!$user->validatePassword($this->password)) {
                $this->addError($attribute, 'Пароль не верный');
            }
        }
    }


    /**
     */
    public function action()
    {
        // меняю ключ доступа к кошелькам на новый "пароль от кабинета"
        /** @var \common\models\UserAvatar $user */
        $user = Yii::$app->user->identity;
        try {
            $user->changePasswordInBills($this->_passwordCabinetOld, $this->password);
        } catch (\Exception $e) {
            return false;
        }

        $user->wallets_is_locked = 0;
        $user->save();

        // Сохраняю в "ячейку хранения" ($hashSeedsObject->seeds) новый пароль чтобы потом восстановить можно было пароль
        $newHashSeeds = \common\services\Security\AES::encrypt256CBC($this->password, $this->_key32);
        $this->_hashSeedsObject->seeds = $newHashSeeds;
        $this->_hashSeedsObject->save();

        return true;
    }
}
