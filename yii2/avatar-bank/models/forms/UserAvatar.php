<?php
namespace avatar\models\forms;

use cs\base\FormActiveRecord;
use Yii;
use yii\base\NotSupportedException;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\web\IdentityInterface;

/**
 * @property integer $id
 * @property string name_first
 * @property string name_last
 */
class UserAvatar extends FormActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'user';
    }

    public function formAttributes()
    {
        return [
            [
                'name_first',
                'Имя',
                1,
                'string',
                ['max' => 255],
            ],
            [
                'name_last',
                'Фамилия',
                0,
                'string',
                ['max' => 255],
            ],
            [
                'phone',
                'Телефон',
                0,
                'string',
                ['max' => 255],
            ],
            [
                'email',
                'Email',
                0,
                'string',
                ['max' => 255],
            ],
            [
                'avatar',
                'Картинка',
                0,
                '\common\widgets\FileUpload3\Validator',
                'widget' => [
                    '\common\widgets\FileUpload3\FileUpload',
                    [
                        'options' => [
                            'small' => \avatar\services\Html::$formatIcon
                        ]
                    ]
                ]
            ],
            [
                'email_is_confirm',
                'Email подтвержден?',
                0,
                '\common\widgets\CheckBox2\Validator',
                'widget' => [
                    '\common\widgets\CheckBox2\CheckBox'
                ],
            ],
            [
                'phone_is_confirm',
                'Телефон подтвержден?',
                0,
                '\common\widgets\CheckBox2\Validator',
                'widget' => [
                    '\common\widgets\CheckBox2\CheckBox'
                ],
            ],
        ];
    }
}
