<?php

namespace avatar\models\forms;

use common\components\providers\ETH;
use common\models\avatar\UserBill;
use common\models\SendLetter;
use common\models\UserSeed;
use common\models\UserZalog;
use common\services\Security;
use cs\Application;
use cs\services\Str;
use Yii;
use yii\base\Model;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;
use yii\helpers\VarDumper;

/**
 */
class CabinetZalogRecall extends Model
{
    /** @var  int */
    public $zalog_id;

    /** @var  \common\models\UserZalog */
    private $zalog;

    /** @var  string ключ разблокировки */
    public $password;

    /** @var  string пароль от кошелька $this->zalog->owner_billing_id */
    private $walletPassword;

    /** @var  string идентификатор транзакции */
    private $txid;

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            ['zalog_id', 'required'],
            ['zalog_id', 'integer'],
            ['zalog_id', 'validateZalog'],

            ['password', 'required'],
            ['password', 'trim'],
            ['password', 'string', 'length' => 32],
            ['password', 'validatePassword'],
            ['password', 'tryToSend'],
        ];
    }

    public function validatePassword($attribute, $params)
    {
        if (!$this->hasErrors()) {
            $walletPassword = Security\AES::decrypt256CBC($this->zalog->hash, UserBill::passwordToKey32($this->password));
            if ($walletPassword == '') {
                $this->addError($attribute, 'Ключ не верный');
                return;
            }
            $this->walletPassword = $walletPassword;

        }
    }

    public function validateZalog($attribute, $params)
    {
        if (!$this->hasErrors()) {
            $zalog = UserZalog::findOne($this->zalog_id);
            if (is_null($zalog)) {
                $this->addError($attribute, 'Не найден залог');
                return;
            }

            if ($zalog->partner_user_id != Yii::$app->user->id) {
                $this->addError($attribute, 'Это не ваш залог');
                return;
            }
            $this->zalog = $zalog;
        }
    }

    /**
     * Пытается отправить
     * @param $attribute
     * @param $params
     */
    public function tryToSend($attribute, $params)
    {
        if (!$this->hasErrors()) {
            $userPartner = \common\models\UserAvatar::findOne($this->zalog->partner_user_id);
            try {
                $billingPartner = $userPartner->getPiramidaBilling(\common\models\avatar\Currency::ETH);
            } catch (\Exception $e) {
                $this->addError($attribute, $e->getMessage());
                return;
            }

            $billingOwner = UserBill::findOne($this->zalog->owner_billing_id);
            try {
                $txid = $billingOwner
                    ->getWalletToken()
                    ->send(
                        [$billingPartner->address => $this->zalog->amount],
                        $this->walletPassword
                    );
            } catch (\Exception $e) {
                $this->addError($attribute, $e->getMessage());
                return;
            }

            $this->txid = $txid;
        }
    }

    /**
     * Конвертирует массив ошибок от формы чтобы их можно было выдать через JSON
     *
     * @param array $params
     *      [
     *      'amount' => ['Не верное число',...],
     *      ]
     *
     * @return array
     * [
     *      'name'  => 'amount',
     *      'value' => ['Не верное число',...],
     * ]
     */
    public function convert($params)
    {
        $ret = [];
        foreach ($params as $name => $arr) {
            $ret[] = [
                'name'  => $name,
                'value' => $arr,
            ];
        }
        return $ret;
    }

    /**
     *
     * @return string $txid
     * @throws
     */
    public function action()
    {
        $this->zalog->status = UserZalog::STATUS_RECALL;
        $this->zalog->save();
        $owner = \common\models\UserAvatar::findOne($this->zalog->owner_user_id);

        $ret = \avatar\base\Application::mail($owner->email, 'Токены отозваны по залогу #' . $this->zalog->id,'zalog_recall', [
            'zalog' => $this->zalog,
            'txid'  => $this->txid,
        ]);

        return $this->txid;
    }
}
