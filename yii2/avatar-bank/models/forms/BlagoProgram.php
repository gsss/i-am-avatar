<?php
namespace avatar\models\forms;

use common\models\school\File;
use cs\base\FormActiveRecord;
use cs\services\Url;
use cs\services\VarDumper;
use iAvatar777\services\Processing\Wallet;
use Yii;
use yii\base\NotSupportedException;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\web\IdentityInterface;

/**
 * @property integer id
 * @property integer school_id
 * @property string  name
 */
class BlagoProgram extends \iAvatar777\services\FormAjax\ActiveRecord
{
    public static function tableName()
    {
        return 'school_blago_program';
    }

    public function rules()
    {
        return [
            ['school_id', 'integer'],

            ['name', 'string'],

            ['success_url', 'string'],
            ['error_url', 'string'],
            ['currency_id', 'integer'],
            ['currency_wallet_id', 'integer'],
            ['currency_wallet_id', 'validateWallet'],
        ];
    }

    public function validateWallet($a, $p)
    {
        if (!$this->hasErrors()) {
            $w = Wallet::findOne($this->currency_wallet_id);
            if (is_null($w)) {
                $this->addError($a, 'Такой кошелек не найден');
                return;
            }
            if ($w->currency_id != $this->currency_id) {
                $this->addError($a, 'Этот кошелек не той валюты');
                return;
            }
        }
    }

    public function attributeLabels()
    {
        return [
            'name'               => 'Наименование',
            'success_url'        => 'Страница поздравления',
            'error_url'          => 'Страница ошибки',
            'currency_id'        => 'Учетная единица',
            'currency_wallet_id' => 'Счет',
        ];
    }
}
