<?php

namespace avatar\models\forms;

use common\components\providers\ETH;
use common\models\SendLetter;
use common\models\UserSeed;
use common\services\Security;
use cs\Application;
use cs\services\Str;
use Yii;
use yii\base\Model;
use yii\helpers\ArrayHelper;
use yii\helpers\FileHelper;
use yii\helpers\Json;
use yii\helpers\VarDumper;

/**
 */
class CabinetBillsEthExportJson extends Model
{
    /** @var  \common\models\avatar\UserBill */
    public $billing;

    /** @var  string */
    public $password;

    /** @var  string */
    public $pin;

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        /** @var \common\models\UserAvatar $user */
        $user = Yii::$app->user->identity;

        if (!empty($user->google_auth_code)) {
            return [
                ['password', 'required'],
                ['password', 'string'],
                ['password', 'validatePassword'],

                ['pin', 'validateRequired', 'skipOnEmpty' => false],
                ['pin', 'integer'],
                ['pin', 'validatePin'],
            ];
        } else {
            return [
                ['password', 'required'],
                ['password', 'string'],
                ['password', 'validatePassword'],
            ];
        }
    }

    public function validateRequired($attribute, $params)
    {
        $user = Yii::$app->user->identity;
        if (!empty($user->google_auth_code)) {
            if (empty($this->pin)) {
                $this->addError($attribute, 'PIN должен быть заполнен обязательно');
            }
        }
    }

    public function validatePassword($attribute, $params)
    {
        if (!$this->hasErrors()) {
            /** @var \common\models\UserAvatar $user */
            $user = Yii::$app->user->identity;
            if (!$user->validatePassword($this->password)) {
                $this->addError($attribute, 'Пароль не верный');
            }
        }
    }

    public function validatePin($attribute, $params)
    {
        if (!$this->hasErrors()) {
            /** @var \common\models\UserAvatar $user */
            $user = Yii::$app->user->identity;
            if (!$user->validateGoogleAuthCode($this->pin)) {
                $this->addError($attribute, 'Код не верный');
            }
        }
    }

    /**
     * Экспортирует ключ
     *
     * @return string SEEDs
     */
    public function action()
    {
        // Генерирую временную папку
        $directoryPath = '@runtime/export/' . time() . '_' . Security::generateRandomString(10);
        $directoryPathFull = Yii::getAlias($directoryPath);
        $directoryTemp = FileHelper::createDirectory($directoryPathFull);

        // Генерирую файл JSON
        $data = $this->billing->getWalletETH()->getPrivateJsonLocal($this->password);
        $fileContent = Json::encode($data);
        $fileName1 = $this->billing->address . '.json';
        $directoryPathFull = Yii::getAlias($directoryPath);
        $filePath1 = $directoryPathFull . DIRECTORY_SEPARATOR . $fileName1;
        file_put_contents($filePath1, $fileContent);

        // Генерирую файл password.txt
        $fileContent = $this->billing->getPassword($this->password);
        $fileName2 = 'password.txt';
        $directoryPathFull = Yii::getAlias($directoryPath);
        $filePath2 = $directoryPathFull . DIRECTORY_SEPARATOR . $fileName2;
        file_put_contents($filePath2, $fileContent);

        // Генерирую файл password.txt
        $fileName3 = $this->billing->address . '.zip';
        $directoryPathFull = Yii::getAlias($directoryPath);
        $filePath3 = $directoryPathFull . DIRECTORY_SEPARATOR . $fileName3;

        $zip = new \ZipArchive();
        $zip->open($filePath3, \ZipArchive::CREATE);
        $zip->addFile($filePath1, $fileName1);
        $zip->addFile($filePath2, $fileName2);
        $zip->close();

        $fileContent = file_get_contents($filePath3);
        unlink($filePath1);
        unlink($filePath2);
        unlink($filePath3);

        return Yii::$app->response->sendContentAsFile($fileContent, $fileName3);
    }
}
