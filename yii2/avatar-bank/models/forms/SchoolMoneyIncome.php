<?php

namespace avatar\models\forms;


use common\models\school\File;
use yii\db\ActiveRecord;

/**
 *
 * @property int            id
 * @property int            user_id
 * @property int            school_id
 * @property int            created_at
 * @property int            amount
 * @property int            currency_id dbWallet.currency.id
 * @property string         date
 * @property string         description
 * @property string         image
 *
 */
class SchoolMoneyIncome extends \iAvatar777\services\FormAjax\ActiveRecord
{
    public function rules()
    {
        return [
            ['school_id', 'integer'],
            ['user_id', 'integer'],
            ['created_at', 'integer'],
            ['amount', 'integer'],
            ['currency_id', 'integer'],
            ['date',   '\iAvatar777\widgets\DateTime\Validator'],
            ['description', 'string'],
            ['image', 'string'],
        ];
    }

    public function attributeWidgets()
    {
        return [
            'date'  => ['class' => '\iAvatar777\widgets\DateTime\DateTime', 'dateFormat' => 'php:d.m.Y',],
            'amount' => [
                'class'    => '\avatar\widgets\Price',
                'currency' => \common\models\piramida\Currency::findOne(\common\models\piramida\Currency::RUB),
            ],
            'image' => [
                'class'    => '\iAvatar777\widgets\FileUpload7\FileUpload',
                'update'   => \avatar\controllers\CabinetSchoolPagesConstructorController::getUpdate(),
                'settings' => \avatar\controllers\CabinetSchoolPagesConstructorController::getSettingsLibrary($this->school_id, 27),
                'events'   => [
                    'onDelete' => function ($item) {
                        $r = new \cs\services\Url($item->image);
                        $d = pathinfo($r->path);
                        $start = $d['dirname'] . '/' . $d['filename'];

                        File::deleteAll(['like', 'file', $start]);
                    },
                ],
            ],

        ];
    }

    public function behaviors()
    {
        return [
            [
                'class'      => '\yii\behaviors\TimestampBehavior',
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at'],
                ],
                // если вместо метки времени UNIX используется datetime:
                // 'value' => new Expression('NOW()'),
            ],
        ];
    }
}