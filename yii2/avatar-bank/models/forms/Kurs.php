<?php
namespace avatar\models\forms;

use common\models\school\PotokUserLink;
use common\models\User2;

/**
 */
class Kurs extends \yii\base\Model
{
    public $email;
    public $name;
    public $phone;

    public function rules()
    {
        return [
            ['email', 'required'],
            ['email', 'email'],
            ['name', 'string'],
            ['phone', 'string'],
        ];
    }

    public function action($potokId)
    {
        \Yii::info([$_POST, $_GET], 'avatar\models\forms\Kurs::action');

        $email = strtolower($this->email);
        /** @var \common\models\User2 $row */
        $row = User2::find()->where(['email' => $email])->one();
        if (is_null($row)) {
            $user = User2::add([
                'email' => $email,
                'name'  => $this->name,
                'phone' => $this->phone,
            ]);
            PotokUserLink::add([
                'user_id'  => $user->id,
                'potok_id' => $potokId,
            ]);
        } else {
            $UserId = $row->id;

            if (!PotokUserLink::find()->where(['user_id'  => $UserId, 'potok_id' => $potokId])->exists()) {
                PotokUserLink::add([
                    'user_id'  => $UserId,
                    'potok_id' => $potokId,
                ]);
            }
        }
    }
}
