<?php
namespace avatar\models\forms;

use cs\base\FormActiveRecord;
use Yii;
use yii\base\Model;
use yii\base\NotSupportedException;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\web\IdentityInterface;

/**
 *
 * @property int    id                  Идентификатор проекта
 * @property int    billing_id          Идентификатор счета на котором лежат все монеты
 * @property int    currency_id         Идентификатор валюты по таблице currency
 * @property int    remained_tokens     Сколько осталось токенов
 * @property int    all_tokens          Сколько токенов выставленное на продажу
 *
 */
class ProjectIco extends Model
{
    public $id;
    public $billing_id;
    public $remained_tokens;


    public function save()
    {

    }

    public function formAttributes()
    {
        return [
            [
                'billing_id',
                'Счет токенов',
                0,
                'integer',
            ],
            [
                'remained_tokens',
                'Кол-во токенов выставленные на продажу',
                0,
                'integer',
            ],
        ];
    }
}
