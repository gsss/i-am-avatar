<?php

namespace avatar\models\forms;

use common\components\providers\ETH;
use common\models\avatar\UserBinance;
use common\models\SendLetter;
use common\models\UserSeed;
use common\services\Security;
use cs\Application;
use cs\services\Str;
use Yii;
use yii\base\Model;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;
use yii\helpers\VarDumper;

/**
 */
class CabinetTradeBinanceApiSet extends Model
{
    /** @var  string */
    public $api_key;

    /** @var  string */
    public $api_secret;

    public function init()
    {
        /** @var \common\models\UserAvatar $user */
        $user = Yii::$app->user->identity;
        $userBinance = UserBinance::findOne($user->id);
        if (!is_null($userBinance)) {
            $this->api_key = $userBinance->api_key;
            $this->api_secret = $userBinance->api_secret;
        }
    }

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            ['api_key', 'required'],
            ['api_key', 'string', 'length' => 64],

            ['api_secret', 'required'],
            ['api_secret', 'string', 'length' => 64],
        ];
    }

    /**
     * Сохраняет значения в БД
     *
     * @return string SEEDs
     */
    public function action()
    {
        $userBinance = UserBinance::findOne(Yii::$app->user->id);
        if (is_null($userBinance)) {
            $userBinance = new UserBinance([
                'id'         => Yii::$app->user->id,
                'api_key'    => $this->api_key,
                'api_secret' => $this->api_secret,
            ]);
        } else {
            $userBinance->api_key = $this->api_key;
            $userBinance->api_secret = $this->api_secret;
        }

        return $userBinance->save();
    }
}
