<?php

namespace avatar\models\forms;

use common\models\avatar\UserBill;
use common\models\RequestTokenCreate;
use common\models\SendLetter;
use common\models\UserSeed;
use common\services\Security;
use cs\Application;
use cs\services\Str;
use Yii;
use yii\base\Model;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;
use yii\helpers\VarDumper;

/**
 */
class CabinetTokenStep4 extends Model
{
    /** @var  string пароль от счета (кабинета) */
    public $password;

    /** @var  int идентификатор заявки на создание токена */
    public $request_id;

    /** @var  \common\models\RequestTokenCreate заявка на создание токена */
    public $request;

    /** @var  array ответ от registerContract */
    private $response;

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            ['password', 'validatePassword'],

            ['request_id', 'required'],
            ['request_id', 'integer'],
            ['request_id', 'validateRequest'],
            ['request_id', 'tryToRegisterContract'],
        ];
    }

    /**
     * @return array customized attribute labels
     */
    public function attributeLabels()
    {
        return [
            'password' => 'Пароль',
        ];
    }

    /**
     * Конвертирует массив ошибок от формы чтобы их можно было выдать через JSON
     *
     * @param array $params
     *      [
     *      'amount' => ['Не верное число',...],
     *      ]
     *
     * @return array
     * [
     *      'name'  => 'amount',
     *      'value' => ['Не верное число',...],
     * ]
     */
    public function convert($params)
    {
        $ret = [];
        foreach ($params as $name => $arr) {
            $ret[] = [
                'name'  => $name,
                'value' => $arr,
            ];
        }
        return $ret;
    }

    public function validatePassword($attribute, $params)
    {
        if (!$this->hasErrors()) {
            if (empty($this->password)) {
                $password = Yii::$app->cache->get(['session_id' => Yii::$app->session->id, 'name' => 'createToken']);
                if ($password === false) {
                    $this->addError($attribute, 'Не задан пароль');
                    return;
                }
                $this->password = $password;
            }

            /** @var \common\models\UserAvatar $user */
            $user = Yii::$app->user->identity;
            if (!$user->validatePassword($this->password)) {
                $this->addError($attribute, 'Пароль не верный');
            }
        }
    }

    public function validateRequest($attribute, $params)
    {
        if (!$this->hasErrors()) {
            $request = RequestTokenCreate::findOne($this->request_id);
            if (is_null($request)) {
                $this->addError($attribute, 'Не найдена заявка');
                return;
            }
            if ($request->user_id != Yii::$app->user->id) {
                $this->addError($attribute, 'Это не ваша заявка');
                return;
            }
            if ($request->step != 3) {
                $this->addError($attribute, 'Вы находитесь не на верном шаге выпуска токена');
                return;
            }
            $this->request = $request;
        }
    }

    public function tryToRegisterContract($attribute, $params)
    {
        if (!$this->hasErrors()) {
            /** @var \common\models\UserAvatar $user */
            $user = Yii::$app->user->identity;

            // работаю с большими целыми числами
            $pow = bcpow(10, $this->request->decimals);
            $all = bcmul($this->request->coin_emission, $pow);

            try {
                $data = $user->getWalletEthPassport()->registerContract(
                    $this->password,
                    $this->request->contract,
                    $this->request->name,
                    [
                        $all,
                        $this->request->name,
                        $this->request->decimals,
                        $this->request->code,
                    ]
                );
            } catch (\Exception $e) {
                $this->addError($attribute, $e->getMessage());
                return;
            }

            $this->response = $data;
        }
    }

    /**
     * Устанавливает для заявки 4 шаг
     *
     * @return string идентификатор транзакции
     */
    public function action()
    {
        $this->request->step = 4;
        $data = $this->response;
        $this->request->txid = $data['transactionHash'];
        $this->request->abi = Json::encode($data['abi']);
        $this->request->is_register_in_system = 1;
        $this->request->save();

        return $this->request->txid;
    }
}
