<?php

namespace avatar\models\forms;

use common\models\BillingMain;
use common\models\BillingMainClass;
use common\models\Config;
use common\models\school\School;
use common\models\Tarif;
use cs\Application;
use cs\services\Security;
use cs\services\VarDumper;
use iAvatar777\services\FormAjax\Model;
use Yii;
use yii\base\Exception;
use yii\db\ActiveRecord;
use yii\helpers\Json;

/**
 * name{id}_month
 */
class CabinetSchoolPay extends Model
{
    /**
     * @var array
     * [
     *  [
     *      id
     *      in
     *      out
     *  ]
     * ]
     */
    public $params = [];

    public $pay_id;

    /** @var \common\models\school\School */
    public $School;

    public $confirm;

    public function attributeWidgets()
    {
        return [
            'pay_id' => [
                'class'    => '\avatar\widgets\PaySystemList',
                'rows'     => \common\models\PaySystemConfig::find()->where(['school_id' => 50]),
            ],
            'confirm' => '\common\widgets\CheckBox3\CheckBox',
        ];
    }

    public function rules()
    {
        $currencyList = \common\models\Tarif::find()->select('id')->column();
        $rules = [];
        $fields = [];

        foreach ($currencyList as $cid) {
            $fields[] = 'name' . $cid . '_month';
        }
        $rules[] = [$fields, 'integer', 'min' => 0];

        return $rules;
    }

    public function attributeLabels()
    {
        return [
            'confirm' => 'Согласие с условиями договора предоставления сервисов',
            'pay_id'  => 'Платежная система',
        ];
    }

    public function __get($name)
    {
        if ($name == 'attributes') {
            $attributes = [];
            foreach ($this->params as $p) {
                $attributes['name' . $p['id'] . '_month'] = $p['month'];
            }
            return $attributes;
        }
        if ($name == 'errors') {
            return parent::__get($name);
        }
        $arr = explode('_', $name);
        try {
            $param = $arr[1];
        } catch (\Exception $e) {
            return parent::__get($name);
        }

        $id = substr($arr[0], 4);
        foreach ($this->params as $p) {
            if ($id == $p['id']) {
                return $p[$param];
            }
        }

        return 0;
    }

    public function __set($name, $value)
    {
        $arr = explode('_', $name);
        $param = $arr[1];
        $id = substr($arr[0], 4);
        $c = count ($this->params);
        for ($i = 0; $i < $c; $i++) {
            if ($id == $this->params[$i]['id']) {
                $this->params[$i][$param] = $value;
                return true;
            }
        }
        $this->params[] = [
            'id'    => $id,
            $param  => $value,
        ];
        return true;
    }


    public function save($runValidation = true, $attributeNames = null)
    {
        if (false) {
            $sum = 0;
            foreach ($this->params as $param) {
                $tarif = Tarif::findOne($param['id']);
                $sum += $tarif->price;
            }

            // Создаю счет на оплату
            $BillingMainClass = BillingMainClass::getByName('\common\models\TarifRequest');

            $bill = BillingMain::add([
                'sum_before'  => $sum,
                'sum_after'   => $sum,
                'class_id'    => $BillingMainClass->id,
                'successUrl'  => '',
                'failUrl'     => '',
                'action'      => '',
                'description' => 'Оплата тарифа за сообщество',
                'destination' => '',
                'source_id'   => '',
                'config_id'   => '',
            ]);

            \common\models\TarifRequest::add([
                'company_id'  => $this->School->id,
                'price'       => $sum,
                'billing_id'  => $bill->id,
                'currency_id' => $bill->id,
            ]);

            VarDumper::dump($this->params);
        }

        return 1;
    }
}
