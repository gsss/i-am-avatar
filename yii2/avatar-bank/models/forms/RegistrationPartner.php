<?php

namespace avatar\models\forms;

use app\models\User;
use common\models\UserAvatar;
use cs\Application;
use cs\web\Exception;
use Yii;
use yii\base\Model;
use yii\helpers\Url;

/**
 *
 */
class RegistrationPartner extends Model
{
    public $email;
    public $password1;
    public $password2;
    public $code;

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            [['email', 'password1', 'password2'], 'required', 'message' => 'Это поле должно быть заполнено обязательно'],
            ['email', 'trim'],
            ['email', 'email'],
            ['email', 'unique', 'targetAttribute' => 'email', 'targetClass' => '\common\models\UserAvatar', 'message' => 'Такая почта уже есть'],
            ['password2', 'compare', 'compareAttribute' => 'password1', 'operator' => '==', 'message' => 'Пароли должны совпадать'],
        ];
    }

    /**
     * Активирует карту
     *
     * @param int $partnerId идентификатор параметра
     *
     * @return bool
     */
    public function activate($partnerId)
    {
        // Создаю пользователя
        $data = UserAvatar::registration($this->email, $this->password1);

        /** @var \common\models\UserAvatar $user */
        $user = $data['user'];
        $user->parent_id = $partnerId;
        $user->save();
        Yii::$app->user->login($user);

        return true;
    }
}
