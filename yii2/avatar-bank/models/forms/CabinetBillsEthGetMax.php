<?php

namespace avatar\models\forms;

use common\components\providers\ETH;
use common\models\avatar\UserBill;
use common\models\SendLetter;
use common\models\UserSeed;
use common\services\Security;
use cs\Application;
use cs\services\Str;
use Yii;
use yii\base\Model;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;
use yii\helpers\VarDumper;

/**
 */
class CabinetBillsEthGetMax extends Model
{
    /** @var  int */
    public $billing_id;

    /** @var  \common\models\avatar\UserBill */
    public $billing;

    /** @var  int */
    public $gasLimit;

    /** @var  int Gwei */
    public $gasPrice;

    /** @var  int wei кол-во макс wei кот можно отправить */
    public $max;

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            ['billing_id', 'required'],
            ['billing_id', 'integer'],
            ['billing_id', 'validateBilling'],
            ['billing_id', 'calculateSend'],
            ['gasLimit', 'integer', 'min' => 21000, 'max' => 4000000],
            ['gasPrice', 'integer', 'min' => 21, 'max' => 50],
        ];
    }

    public function validateBilling($attribute, $params)
    {
        if (!$this->hasErrors()) {
            $billing = UserBill::findOne($this->billing_id);
            if (is_null($billing)) {
                $this->addError($attribute, 'Счет не найден');
                return;
            }
            if ($billing->user_id != Yii::$app->user->id) {
                $this->addError($attribute, 'Не ваш счет');
                return;
            }
            $this->billing = $billing;
        }
    }

    /**
     * Вычисляет максимальную сумму для перевода и если она отрицательная то выдает ошибку
     *
     * @param $attribute
     * @param $params
     */
    public function calculateSend($attribute, $params)
    {
        if (!$this->hasErrors()) {
            $walletETH = $this->billing->getWalletETH();

            try {
                $balanceWie = $walletETH->getBalanceWei2();
            } catch (\Exception $e) {
                $this->addError('amount', 'Не удалось получить баланс, попробуйте еще раз.');
                return;
            }

            Yii::info($balanceWie, '\avatar\models\forms\CabinetBillsEthGetMax::calculateSend');
            $gasPrice = 21;
            if (!empty($this->gasPrice)) $gasPrice = $this->gasPrice;
            $gasPrice = $gasPrice * pow(10, 9);
            $gasLimit = 21000;
            if (!empty($this->gasLimit)) $gasLimit = $this->gasLimit;
            $max = $balanceWie - ($gasPrice * $gasLimit);

            if ($max < 0) {
                if (!empty($this->gasPrice) and !empty($this->gasLimit)) {
                    $this->addError('gasPrice', 'У вас недостаточно Эфира чтобы оплатить комисиию');
                    return;
                } else {
                    if (!empty($this->gasPrice)) {
                        $this->addError('gasPrice', 'У вас недостаточно Эфира чтобы оплатить комисиию');
                        return;
                    } else if (!empty($this->gasLimit)) {
                        $this->addError('gasLimit', 'У вас недостаточно Эфира чтобы оплатить комисиию');
                        return;
                    } else {
                        $this->addError('amount', 'У вас недостаточно Эфира чтобы оплатить комисиию');
                        return;
                    }
                }
            }

            $this->max = $max;
        }
    }

    /**
     * @return array
     * [
     *   'wei' => $max,
     * ];
     */
    public function action()
    {
        $max = $this->max;
        bcscale(18);
        $eth = bcdiv($this->max, pow(10, 18));

        return [
            'wei' => $max,
            'eth' => $eth,
//            'eth' => $max / pow(10, 18),
        // Ошибка, не сходятся цифры после деления в PHP, Поэтому не используем деление в PHP для эфира
        ];
    }


    /**
     * Конвертирует массив ошибок от формы чтобы их можно было выдать через JSON
     *
     * @param array $params
     *      [
     *      'amount' => ['Не верное число',...],
     *      ]
     *
     * @return array
     * [
     *      'name'  => 'amount',
     *      'value' => ['Не верное число',...],
     * ]
     */
    public function convert($params)
    {
        $ret = [];
        foreach ($params as $name => $arr) {
            $ret[] = [
                'name'  => $name,
                'value' => $arr,
            ];
        }
        return $ret;
    }
}
