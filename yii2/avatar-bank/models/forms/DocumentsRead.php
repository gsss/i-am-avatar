<?php

namespace avatar\models\forms;

use common\components\providers\ETH;
use common\models\SendLetter;
use common\models\UserSeed;
use common\services\Security;
use cs\Application;
use cs\services\Str;
use Yii;
use yii\base\Model;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;
use yii\helpers\VarDumper;

/**
 */
class DocumentsRead extends Model
{
    /** @var  int */
    public $id;

    /** @var  string */
    public $password;

    /** @var  \common\models\UserDocument */
    public $document;

    /** @var  int */
    public $blockchainId;

    /** @var  array */
    public $response;

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            ['password', 'required'],
            ['password', 'string'],
            ['password', 'validatePassword'],

            ['id', 'integer'],
            ['id', 'validateDocument'],
            ['id', 'tryToRead'],
        ];
    }


    public function validateDocument($attribute, $params)
    {
        if (!$this->hasErrors()) {
            $doc = \common\models\UserDocument::findOne($this->id);
            if (is_null($doc)) {
                $this->addError('password', 'Файл не найден');
                return;
            }
            $this->document = $doc;
        }
    }


    public function tryToRead($attribute, $params)
    {
        if (!$this->hasErrors()) {
            /** @var \common\models\UserAvatar $user */
            $user = \Yii::$app->user->identity;

            $passportWalletEth = $user->getWalletEthPassport();
            try {
                $id = $this->getIdFromTxHash2($this->document->txid);
            } catch (\Exception $e) {
                $this->addError('password', $e->getMessage());
                return;
            }
            $this->blockchainId = $id;

            $contract = '0x52ed3c202c4652f952a1561ac0c030f1ed9460ff';
            if (!YII_ENV_PROD) {
                $contract = '0xfF6C78BFedF1bdb2D85071A9040798F8057A0A36';
            }

            try {
                $data = $passportWalletEth->contract(
                    $this->password,
                    $contract,
                    '[{"constant":true,"inputs":[],"name":"getDocumentsCount","outputs":[{"name":"","type":"uint256"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":false,"inputs":[{"name":"hash","type":"string"},{"name":"link","type":"string"},{"name":"data","type":"string"}],"name":"registerDocument","outputs":[],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":false,"inputs":[{"name":"id","type":"uint256"}],"name":"addSignature","outputs":[],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":true,"inputs":[{"name":"id","type":"uint256"}],"name":"getDocument","outputs":[{"name":"hash","type":"string"},{"name":"link","type":"string"},{"name":"data","type":"string"},{"name":"creator","type":"address"},{"name":"date","type":"uint256"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":false,"inputs":[],"name":"kill","outputs":[],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":true,"inputs":[{"name":"","type":"uint256"}],"name":"documentsIds","outputs":[{"name":"hash","type":"string"},{"name":"link","type":"string"},{"name":"data","type":"string"},{"name":"creator","type":"address"},{"name":"date","type":"uint256"},{"name":"signsCount","type":"uint256"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":true,"inputs":[{"name":"id","type":"uint256"}],"name":"getDocumentSignsCount","outputs":[{"name":"","type":"uint256"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":false,"inputs":[{"name":"newOwner","type":"address"}],"name":"changeOwner","outputs":[],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":true,"inputs":[{"name":"id","type":"uint256"},{"name":"index","type":"uint256"}],"name":"getDocumentSign","outputs":[{"name":"member","type":"address"},{"name":"date","type":"uint256"}],"payable":false,"stateMutability":"view","type":"function"},{"inputs":[],"payable":false,"stateMutability":"nonpayable","type":"constructor"},{"anonymous":false,"inputs":[{"indexed":false,"name":"id","type":"uint256"},{"indexed":false,"name":"member","type":"address"}],"name":"DocumentSigned","type":"event"},{"anonymous":false,"inputs":[{"indexed":false,"name":"id","type":"uint256"},{"indexed":false,"name":"hash","type":"string"}],"name":"DocumentRegistered","type":"event"}]',
                    'getDocument',
                    [$id],
                    'false'
                );
                $this->response = $data;

            } catch (\Exception $e) {
                $this->addError('password', $e->getMessage());
                return;
            }
        }
    }


    /**
     * Получает идентификатор документа через хеш транзакции по которой был зарегистрирован документ
     *
     * @param $hash
     *
     * @return int
     * @throws
     */
    private function getIdFromTxHash2($hash)
    {
        $provider = new \avatar\modules\ETH\ServiceInfura();
        $data = $provider->_method('eth_getTransactionReceipt', [$hash]);

        $log = $data['logs'][0]['data'];
        $hex = substr($log, 0, 66);
        $id = (int)hexdec($hex);

        return $id;
    }

    public function validatePassword($attribute, $params)
    {
        if (!$this->hasErrors()) {
            /** @var \common\models\UserAvatar $user */
            $user = Yii::$app->user->identity;
            if (!$user->validatePassword($this->password)) {
                $this->addError($attribute, 'Пароль не верный');
            }
        }
    }


    /**
     * Конвертирует массив ошибок от формы чтобы их можно было выдать через JSON
     *
     * @param array $params
     *      [
     *      'amount' => ['Не верное число',...],
     *      ]
     *
     * @return array
     * [
     *      'name'  => 'amount',
     *      'value' => ['Не верное число',...],
     * ]
     */
    public function convert($params)
    {
        $ret = [];
        foreach ($params as $name => $arr) {
            $ret[] = [
                'name'  => $name,
                'value' => $arr,
            ];
        }
        return $ret;
    }

    public function action()
    {
        $data = $this->response;
        $data[2] = Json::decode($data[2]);
        $data[4] = date('c', $data[4]);
        $data['id'] = $this->blockchainId;

        return [
            'id' => $this->blockchainId,
            'hash' => $data[0],
            'link' => $data[1],
            'author' => $data[3],
            'date' => $data[4],
            'data' => $data[2],
        ];
    }
}
