<?php
/**
 * Created by PhpStorm.
 * User: Бог-Творец
 * Date: 22.11.2016
 * Time: 20:20
 */

namespace avatar\models\forms;


use avatar\modules\UniSender\UniSender;
use avatar\services\Html;
use common\widgets\FileUpload3\FileUpload;
use cs\base\FormActiveRecord;
use cs\services\VarDumper;
use yii\base\Exception;
use yii\base\Model;
use yii\widgets\ActiveForm;

class Subscribe2 extends Model
{
    public $is_blog;
    public $is_news;

    private $key = 'avatar\models\forms\Subscribe2';

    public function rules()
    {
        return [
            ['is_blog', 'integer'],
        ];
    }

    public function init()
    {
        $provider = new UniSender();
        try {
            $result = $provider->_call('exportContacts', ['email' => \Yii::$app->user->identity->email, 'field_names' => ['email_list_ids']]);
            $ids = explode(',', $result['data'][0][0]);
//            $this->is_news = in_array(\common\models\UserAvatar::SUBSCRIBE_UNISERDER_NEWS, $ids);
            $this->is_blog = in_array(\common\models\UserAvatar::SUBSCRIBE_UNISERDER_BLOG, $ids);

            \Yii::$app->session->set($this->key, $ids);
        } catch (\Exception $e) {
            \Yii::error($e->getMessage(), 'avatar\common\models\UserAvatar::activate');
        }
    }

    public function attributeLabels()
    {
        return [
            'is_blog' => \Yii::t('c.UDkssceBqi', 'Блог'),
            'is_news' => \Yii::t('c.UDkssceBqi', 'Новости'),
        ];
    }

    public function attributeHints()
    {
        return [
            'is_blog' => \Yii::t('c.UDkssceBqi', 'Статьи и обзоры рынка криптовалют и финансов'),
            'is_news' => \Yii::t('c.UDkssceBqi', 'Новости нашей платформы'),
        ];
    }

    public function action()
    {
        $ids = \Yii::$app->session->get($this->key);
        $add = [];
        $remove = [];
        $provider = new UniSender();
        $this->is_blog = (int)$this->is_blog;

        if ($this->is_blog) { // установился
            // до этого небыл
            if (!in_array(\common\models\UserAvatar::SUBSCRIBE_UNISERDER_BLOG, $ids)) {
                // установить новости
                $add[] = \common\models\UserAvatar::SUBSCRIBE_UNISERDER_BLOG;
            }
        } else { // снялся
            // до этого был
            if (in_array(\common\models\UserAvatar::SUBSCRIBE_UNISERDER_BLOG, $ids)) {
                // снять новости
                $remove[] = \common\models\UserAvatar::SUBSCRIBE_UNISERDER_BLOG;
            }
        }
        $res1 = null;
        $res2 = null;
        if (count($add) > 0) {
            try {
                $res1 = $provider->subscribe($add, ['email' => \Yii::$app->user->identity->email], [
                    'confirm_ip'   => \Yii::$app->request->getUserIP(),
                    'request_ip'   => \Yii::$app->request->getUserIP(),
                    'double_optin' => UniSender::DOUBLE_OPTIN_3,
                ]);
            } catch (\Exception $e) {

            }
        }
        if (count($remove) > 0) {
            try {
                $res2 = $provider->_call('exclude', [
                    'contact_type' => 'email',
                    'contact'      => \Yii::$app->user->identity->email,
                    'list_ids'     => join(',', $remove),
                ]);
            } catch (\Exception $e) {

            }
        }
    }

} 