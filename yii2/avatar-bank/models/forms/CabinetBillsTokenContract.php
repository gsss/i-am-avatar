<?php

namespace avatar\models\forms;

use common\models\avatar\UserBill;
use common\models\avatar\UserBillOperation;
use common\models\SendLetter;
use common\models\UserSeed;
use common\services\Security;
use cs\Application;
use cs\services\Str;
use function Sodium\add;
use Yii;
use yii\base\Model;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\StringHelper;
use yii\helpers\VarDumper;

/**
 */
class CabinetBillsTokenContract extends Model
{
    /** @var  \common\models\avatar\UserBill */
    public $billing;

    /** @var string */
    public $password;

    /** @var  string */
    public $functionName;

    public $functionList;

    /** @var  array */
    public $params;

    public $paramErrors = [];

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            ['functionName', 'required', 'message' => Yii::t('c.NRHKEjgSii', 'Это поле обязательно к заполнению')],
            ['functionName', 'validateFunctionName'],

            ['password', 'required'],
            ['password', 'validatePassword'],

            ['params', 'validateParams'],
        ];
    }

    public function validatePassword($attribute, $params)
    {
        if (!$this->hasErrors()) {
            /** @var \common\models\UserAvatar $user */
            $user = Yii::$app->user->identity;
            if (!$user->validatePassword($this->password)) {
                $this->addError($attribute, Yii::t('c.NRHKEjgSii', 'Пароль не верный'));
            }
        }
    }

    public function validateFunctionName($attribute, $params)
    {
        if (!$this->hasErrors()) {
            $billing = $this->billing;
            $abi = $billing->getWalletToken()->token->abi;
            $abiObject = \yii\helpers\Json::decode($abi);
            $functionList = [];
            foreach ($abiObject as $item) {
                if (isset($item['type'])) {
                    if ($item['type'] == 'function') {
                        if ($item['constant'] == false) {
                            $functionList[] = $item['name'];
                        }
                    }
                }
            }
            if (!in_array($this->functionName, $functionList)) {
                $this->addError($attribute, Yii::t('c.NRHKEjgSii', 'Нет такой функции на вызов'));
                return;
            }
        }
    }

    public function validateParams($attribute, $params)
    {
        if (!$this->hasErrors()) {
            $billing = $this->billing;
            $abi = $billing->getWalletToken()->token->abi;
            $abiObject = \yii\helpers\Json::decode($abi);
            $functionList = [];
            $functionParams = [];
            foreach ($abiObject as $item) {
                if (isset($item['type'])) {
                    if ($item['type'] == 'function') {
                        if ($item['constant'] == false) {
                            $functionList[] = $item['name'];
                            $functionParams[$item['name']] = $item;
                        }
                    }
                }
            }

            if (isset($this->params[$this->functionName])) {
                foreach ($this->params[$this->functionName] as $k => $v) {
                    if (strlen($v) == 0) {
                        $this->paramErrors[] = [
                            'name'    => $k,
                            'message' => Yii::t('c.NRHKEjgSii', 'Это поле обязательно к заполнению'),
                        ];
                    }

                    // если у переменной тип целое?
                    if ($this->isInt($functionParams[$this->functionName]['inputs'], $k)) {
                        if (!Application::isInteger($v)) {
                            $this->paramErrors[] = [
                                'name'    => $k,
                                'message' => Yii::t('c.NRHKEjgSii', 'Это поле должно содержать только цифры'),
                            ];
                        }
                    }
                    // если у переменной тип address?
                    if ($this->isAddress($functionParams[$this->functionName]['inputs'], $k)) {
                        if (!StringHelper::startsWith($v, '0x')) {
                            $this->paramErrors[] = [
                                'name'    => $k,
                                'message' => Yii::t('c.NRHKEjgSii', 'Адрес должен начинаться с 0x'),
                            ];
                        }
                    }
                }
                if (count( $this->paramErrors) > 0) {
                    $this->addError($attribute, Yii::t('c.NRHKEjgSii', 'Ошибка в параметрах'));
                    return;
                }
            }

        }
    }

    /**
     * Отвечает на вопрос является ли переменная $name целым
     *
     * @param $inputs
     * @param $name
     * @return bool
     */
    private function isInt($inputs, $name)
    {
        foreach ($inputs as $item) {
            if ($item['name'] == $name) {
                if (in_array($item['type'], ['uint256'])) {
                    return true;
                }
            }
        }

        return false;
    }

    /**
     * Отвечает на вопрос является ли переменная $name address
     *
     * @param $inputs
     * @param $name
     * @return bool
     */
    private function isAddress($inputs, $name)
    {
        foreach ($inputs as $item) {
            if ($item['name'] == $name) {
                if (in_array($item['type'], ['address'])) {
                    return true;
                }
            }
        }

        return false;
    }


    /**
     * Конвертирует массив ошибок от формы чтобы их можно было выдать через JSON
     *
     * @param array $params
     *      [
     *      'amount' => ['Не верное число',...],
     *      ]
     *
     * @return array
     * [
     *      'name'  => 'amount',
     *      'value' => ['Не верное число',...],
     * ]
     */
    public function convert($params)
    {
        $ret = [];
        foreach ($params as $name => $arr) {
            $ret[] = [
                'name'  => strtolower($name),
                'value' => $arr,
            ];
        }
        return $ret;
    }

    /**
     * Расчитывает
     *
     * @return array
     * [
     *      'cost'
     *      'gasPrice'
     * ]
     */
    public function action()
    {
        if (isset($this->params[$this->functionName])) {
            $params = array_values($this->params[$this->functionName]);
        } else {
            $params = [];
        }

        $walletToken = $this->billing->getWalletToken();
        $data = $walletToken->calculateFunction(
            $this->functionName,
            $params,
            $this->password
        );

        $data2 = [
            'from'         => Html::tag('code', $walletToken->billing->address),
            'to'           => Html::tag('code', $walletToken->token->address),
            'functionName' => Html::tag('span', $this->functionName, ['class' => 'label label-info']),
            'costEth'      => Yii::$app->formatter->asDecimal($data['cost'] / (1000000000 * 1000000000), 18) . ' ' . Html::tag('span', 'ETH', ['class' => 'label label-success']),
            'costCalc'     => '?',
            'gasPriceGwei' => Yii::$app->formatter->asDecimal($data['gasPrice'] / 1000000000, 0) . ' ' . Html::tag('span', 'Gwei', ['class' => 'label label-success']),
        ];

        $currency_view = Yii::$app->user->identity->currency_view;
        if (!is_null($currency_view)) {
            if ($currency_view > 0) {
                $currency = \common\models\avatar\Currency::findOne($currency_view);
                $data2['costCalc'] = \common\models\avatar\Currency::convert($data['cost'] / (1000000000 * 1000000000), 'ETH', $currency->code);
                $data2['costCalc'] = Yii::$app->formatter->asDecimal($data2['costCalc'], $currency->decimals) . ' ' . Html::tag('span', $currency->code, ['class' => 'label label-success']);
            }
        }

        $re = [];
        foreach ($data2 as $k => $v) {
            $re[] = [
                'name' => $k,
                'value' => $v,
            ];
        }

        return $re;
    }

    /**
     * Исполняет
     *
     * @return array
     * [
     *      'cost'
     *      'gasPrice'
     * ]
     */
    public function execute()
    {
        if (isset($this->params[$this->functionName])) {
            $params = array_values($this->params[$this->functionName]);
        } else {
            $params = [];
        }

        $walletToken = $this->billing->getWalletToken();
        $txid = $walletToken->contract(
            $this->functionName,
            $params,
            $this->password
        );

        $operation = UserBillOperation::add([
            'transaction' => $txid,
            'message'     => Yii::t('c.NRHKEjgSii', 'Вызов функции {functionName} контракта {contractAddress}', [
                'functionName' => $this->functionName,
                'contractAddress' => $walletToken->token->address,
            ]),
            'bill_id'     => UserBill::findOne(['address' => $this->billing->address, 'currency' => \common\models\avatar\Currency::ETH])->id,
        ]);

        return ['address' => $txid];
    }
}
