<?php
namespace avatar\models\forms;

use cs\base\FormActiveRecord;
use Yii;
use yii\base\NotSupportedException;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\web\IdentityInterface;

/**
 * Akcioner model
 *
 * @property integer id
 * @property string  foto
 */
class Akcioner extends FormActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'rai_akcioneri';
    }

    public function formAttributes()
    {
        return [
            [
                'name_first',
                'Имя',
                0,
                'string',
            ],
            [
                'name_last',
                'Фамилия',
                0,
                'string',
            ],
            [
                'name_middle',
                'Отчество',
                0,
                'string',
            ],
            [
                'projivanie',
                'Место проживания',
                0,
                'string',
                ['max' => 100]
            ],
            [
                'contact_phone',
                'Контактные данные Телефон',
                0,
                'string',
                ['max' => 100]
            ],
            [
                'contact_skype',
                'Контактные данные Скайп',
                0,
                'string',
                ['max' => 100]
            ],
            [
                'contact_email',
                'Контактные данные Email',
                0,
                'email',
            ],
            [
                'contact_vk',
                'Контактные данные VK',
                0,
                'string',
                ['max' => 100]
            ],
            [
                'contact_fb',
                'Контактные данные FB',
                0,
                'string',
                ['max' => 100]
            ],
            [
                'country',
                'Страна пребывания',
                0,
                'string',
                ['length' => 2]
            ],
            [
                'country_passport',
                'Страна которая выдала паспорт',
                0,
                'string',
                ['length' => 2]
            ],
            [
                'utc_delta',
                'Смещение относительно Гринвича',
                0,
                'integer',
            ],
            [
                'time_zone',
                'Название временной зоны по стандарту PHP',
                0,
                'string',
            ],
            [
                'is_show_profile',
                'Показывать в списке акционеров?',
                0,
                '\common\widgets\CheckBox2\Validator',
                'widget' => [
                    '\common\widgets\CheckBox2\CheckBox'
                ],
            ],
            [
                'is_bill_exist',
                'Открыт счет в АО?',
                0,
                '\common\widgets\CheckBox2\Validator',
                'widget' => [
                    '\common\widgets\CheckBox2\CheckBox'
                ],
            ],
            [
                'balance',
                'Баланс на счету АО',
                0,
                'double',
            ],
            [
                'foto',
                'Картинка',
                0,
                '\common\widgets\FileUpload3\Validator',
                'widget' => [
                    '\common\widgets\FileUpload3\FileUpload',
                    [
                        'options' => [
                            'small' => \avatar\services\Html::$formatIcon
                        ]
                    ]
                ]
            ],
        ];
    }
}
