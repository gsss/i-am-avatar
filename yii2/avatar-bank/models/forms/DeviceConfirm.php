<?php
namespace avatar\models\forms;

use common\models\UserAvatar;
use cs\base\FormActiveRecord;
use cs\services\VarDumper;
use Yii;
use yii\base\Model;
use yii\base\NotSupportedException;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\web\IdentityInterface;

/**
 * PaySystem model
 *
 * @property integer id
 * @property string  image
 * @property string  hash
 * @property string  data
 * @property integer created_at
 */
class DeviceConfirm extends Model
{
    public $code;

    private $_user;

    public function rules()
    {
        return [
            ['code', 'required'],
            ['code', 'string', 'length' => 8],
            ['code', 'validateCode'],
        ];
    }

    public function validateCode()
    {
        if (!$this->hasErrors()) {
            /**
             * @var array $code
             * [
             *      'code'
             *      'user'
             * ]
             */
            $code = Yii::$app->session->get('enter');
            if (is_null($code)) {
                $this->addError('code', Yii::t('c.HS07bGa1P0', 'Код не задан'));
                return;
            }
            if ($this->code != $code['code']) {
                $this->addError('code', Yii::t('c.HS07bGa1P0', 'Код не верный'));
                return;
            }
            $this->_user = UserAvatar::findOne($code['user_id']);
        }
    }

    public function getUser()
    {
        return $this->_user;
    }
}
