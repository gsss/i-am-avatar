<?php
namespace avatar\models\forms;

use common\models\Config;
use cs\base\FormActiveRecord;
use Yii;
use yii\base\Model;
use yii\base\NotSupportedException;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\helpers\Json;
use yii\helpers\StringHelper;
use yii\web\IdentityInterface;

/**
 * PaySystem model
 *
 */
class EthereumWalletRead extends Model
{
    private $key = 'eth.main.wallet.contract.read';

    public $address;
    public $password;

    public function rules()
    {
        return [
            [['address', 'password'], 'required'],
            [['address'], 'validateAddress'],
        ];
    }

    public function validateAddress($attribute, $params)
    {
        if (!$this->hasErrors()) {
            if (!StringHelper::startsWith($this->address, '0x')) {
                $this->addError($attribute, 'Адрес должен начинаться с 0x');
            }
        }
    }

    public function init()
    {
        $json = Config::get($this->key);
        if (!is_null($json)) {
            try {
                $data = Json::decode($json);
                $this->address = $data[0];
                $this->password = $data[1];
            } catch (\Exception $e) {
                // ничего
            }
        }
    }

    public function action()
    {
        $json = Json::encode([$this->address, $this->password]);
        Config::set($this->key, $json);

        return true;
    }
}
