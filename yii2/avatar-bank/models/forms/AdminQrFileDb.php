<?php

namespace avatar\models\forms;

use common\models\SendLetter;
use common\models\UserSeed;
use common\services\Security;
use cs\Application;
use cs\services\Str;
use Yii;
use yii\base\Model;
use yii\helpers\ArrayHelper;
use yii\helpers\VarDumper;
use yii\web\UploadedFile;

/**
 */
class AdminQrFileDb extends Model
{
    /** @var  \yii\web\UploadedFile */
    public $file;

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            ['file', 'file'],
            ['file', 'validateFile', 'skipOnEmpty' => false],
        ];
    }

    /**
     * @return array customized attribute labels
     */
    public function attributeLabels()
    {
        return [
            'file' => 'Файл БД',
        ];
    }

    public function validateFile($attribute, $params)
    {
        if (!$this->hasErrors()) {
            $m = UploadedFile::getInstance($this, 'file');
            if (is_null($m)) {
                $this->addError($attribute, 'Файл должен быть заполнен');
                return;
            }
            $this->file = $m;
        }
    }

    /**
     * @return string полный путь к скачанному файлу
     */
    public function action()
    {
        $p = pathinfo($this->file->name);
        $file = Yii::getAlias('@runtime/' . time() . '_' . Security::generateRandomString(10) . '.' . $p['extension']);
        $this->file->saveAs($file);

        return $file;
    }
}
