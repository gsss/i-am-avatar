<?php

namespace avatar\models\forms;

use BitcoinPHP\BitcoinECDSA\BitcoinECDSA;
use common\models\avatar\UserBill;
use common\models\school\Cloud;
use common\models\school\File;
use common\models\UserDigitalSign;
use common\payment\BitCoinBlockTrailPayment;
use common\services\AvatarCloud;
use cs\base\FormActiveRecord;
use cs\services\BitMask;
use cs\services\Url;
use iAvatar777\services\FormAjax\ActiveForm;
use yii\base\Exception;
use yii\db\ActiveRecord;
use yii\db\Query;
use yii\helpers\VarDumper;

/**
 * @property int     id
 * @property int     user_id
 * @property int     type_id
 * @property int     school_id
 * @property string  name
 * @property string  txid
 * @property string  file
 * @property string  hash
 * @property string  data
 * @property string  link
 * @property string  signature
 * @property int     created_at
 *
 */
class SchoolDocument extends \iAvatar777\services\FormAjax\ActiveRecord
{
    public static function tableName()
    {
        return 'user_documents';
    }

    /**
     * @return \common\services\AvatarCloud
     */
    private function getCloud()
    {
        $one = Cloud::findOne(['school_id' => 2, 'is_active' => 1]);
        if (is_null($one)) {
            $c = \Yii::$app->AvatarCloud;
        } else {
            $c = new AvatarCloud([
                'key' => $one->key,
                'url' => $one->url,
            ]);
        }

        return $c;
    }

    public function attributeWidgets()
    {
        $s = [
            'maxSize'         => 20 * 1000,
            'accept'          => '*/*',
//            'controller'      => 'upload4',
            'functionSuccess' => new \yii\web\JsExpression(<<<JS
function (response) {
    // Вызываю AJAX для записи в school_file
    ajaxJson({
        url: '/cabinet/file-upload7-save',
        data: {
            file: response.url,
            school_id: 2,
            type_id: 25,
            size: response.size, // Размер файла в байтах
            update: response.update
        },
        success: function (ret) {
            
        }
    });
}

JS
            ),
        ];

        $one = Cloud::findOne(['school_id' => 2, 'is_active' => 1]);
        if (is_null($one)) {
            $s['server'] = \Yii::$app->AvatarCloud->url;
        } else {
            $s['server'] = $one->url;
        }

        return [
            'file' => [
                'class'    => '\iAvatar777\widgets\FileUpload8\FileUpload',
                'update'   => \avatar\controllers\CabinetSchoolPagesConstructorController::getUpdate(),
                'settings' => $s,
                'events'   => [
                    'onDelete' => function ($item) {
                        $r = new \cs\services\Url($item->image);
                        $d = pathinfo($r->path);
                        $start = $d['dirname'] . '/' . $d['filename'];

                        File::deleteAll(['like', 'file', $start]);
                    },
                ],
            ],
        ];
    }

    public function rules()
    {
        return [
            ['name', 'required'],
            ['name', 'string'],

            ['data', 'string'],

            ['file', 'required'],
            ['file', 'string'],

            ['type_id', 'integer'],

            ['user_id', 'integer'],

            ['school_id', 'integer'],

            ['created_at', 'integer'],

            ['hash', 'string'],
        ];
    }

    public function save($runValidation = true, $attributeNames = null)
    {
        $options = $this->attributeWidgets();
        $server = $options['file']['settings']['server'];

        $this->created_at = time();
        $u = new Url($this->file);
        $cloud = $this->getCloud();
        $ret = $cloud->post(null, 'upload/sha256', ['file' => $u->path]);
        $this->hash = $ret['data']['hash'];
        $this->user_id = \Yii::$app->user->id;
        $this->type_id = \common\models\UserDocument::TYPE_AVATAR_CHAIN;

        parent::save(false);

        return [
            'document'       => $this,
        ];
    }
}