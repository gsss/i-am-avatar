<?php

namespace avatar\models\forms;

use avatar\models\UserLogin;
use common\models\UserAvatar;
use cs\base\BaseForm;
use cs\base\FormActiveRecord;
use cs\services\Str;
use Yii;
use yii\base\Model;
use yii\helpers\ArrayHelper;

/**
 */
class PhoneLogin extends Model
{
    public $phone;
    public $password;

    private $_user = false;

    public function rules()
    {
        return [
            ['phone', 'string'],
            ['phone', 'trim'],
            ['phone', 'validatePhone'],
            ['phone', 'validateUser'],

            ['password', 'string', 'min' => 3],
            ['password', 'validatePassword'],
        ];
    }

    public function validatePhone($attribute, $params)
    {
        if (!$this->hasErrors()) {
            $phone = $this->phone;
            $phone = str_replace('+', '', $phone);
            $phone = str_replace('(', '', $phone);
            $phone = str_replace(')', '', $phone);
            $phone = str_replace('-', '', $phone);

            $symbols = Str::getChars($phone);
            foreach ($symbols as $char) {
                if (!in_array($char, ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '.'])) {
                    $this->addError('phone', 'Содержит запрещенные символы');
                }
            }
            $this->phone = $phone;
        }
    }

    public function validateUser($attribute, $params)
    {
        if (!$this->hasErrors()) {

            $user = $this->getUser();

            if (is_null($user)) {
                $this->addError($attribute, 'Пользователя нет');
                return;
            }
            if ($user->password == '') {
                $this->addError($attribute, 'Вы  не завели себе пароль для аккаунта. Зайдите в восстановление пароля');
                return;
            }
            if ($user->phone_is_confirm != 1) {
                $this->addError($attribute, 'Пользователь не активирован');
                return;
            }
            if ($user->mark_deleted == 1) {
                $this->addError($attribute, 'Пользователь заблокирован');
                return;
            }
        }
    }

    /**
     * Validates the password.
     * This method serves as the inline validation for password.
     *
     * @param string $attribute the attribute currently being validated
     * @param array $params the additional name-value pairs given in the rule
     */
    public function validatePassword($attribute, $params)
    {
        if (!$this->hasErrors()) {
            $user = $this->getUser();

            if (is_null($user)) {
                return;
            }

            if ($user->validatePassword($this->password) == false) {
                $this->addError($attribute, 'Не верный пароль');
            }
        }
    }

    /**
     * Logs in a user using the provided username and password.
     * @return boolean whether the user is logged in successfully
     */
    public function login()
    {
        return Yii::$app->user->login($this->getUser());
    }

    /**
     * Finds user by [[username]]
     *
     * @return \common\models\UserAvatar | null
     */
    public function getUser()
    {
        if ($this->_user === false) {
            $this->_user = UserAvatar::findOne(['phone' => $this->phone]);
        }

        return $this->_user;
    }
}
