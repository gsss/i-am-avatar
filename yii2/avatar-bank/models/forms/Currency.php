<?php
namespace avatar\models\forms;

use common\models\school\File;
use cs\base\FormActiveRecord;
use Yii;
use yii\base\NotSupportedException;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\web\IdentityInterface;

/**
 * Currency model
 *
 */
class Currency extends FormActiveRecord
{
    public $_school_id;
    public $_type_id;


    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'currency';
    }

    public function formAttributes()
    {
        return [
            [
                'code',
                'Код валюты',
                1,
                'string',
                ['max' => 10],
            ],
            [
                'title',
                'Наименование валюты',
                0,
                'string',
                ['max' => 255],
            ],
            [
                'code_coinmarketcap',
                'code_coinmarketcap',
                0,
                'string',
                ['max' => 6],
            ],
            [
                'id_coinmarketcap',
                'id_coinmarketcap',
                0,
                'string',
                ['max' => 32],
            ],
            [
                'decimals',
                'Кол-во символов после запятой',
                0,
                'integer',
                ['min' => 0],
            ],
            [
                'kurs',
                'Курс относительно рубля',
                0,
                'double',
            ],
            [
                'is_crypto',
                'is_crypto',
                0,
                '\common\widgets\CheckBox2\Validator',
                'widget' => [
                    '\common\widgets\CheckBox2\CheckBox'
                ],
            ],
            [
                'is_view',
                'is_view',
                0,
                '\common\widgets\CheckBox2\Validator',
                'widget' => [
                    '\common\widgets\CheckBox2\CheckBox'
                ],
            ],
            [
                'is_merchant',
                'is_merchant',
                0,
                '\common\widgets\CheckBox2\Validator',
                'widget' => [
                    '\common\widgets\CheckBox2\CheckBox'
                ],
            ],
            [
                'is_avr_wallet',
                'is_avr_wallet',
                0,
                '\common\widgets\CheckBox2\Validator',
                'widget' => [
                    '\common\widgets\CheckBox2\CheckBox'
                ],
            ],
            [
                'image',
                'Картинка',
                0,
                '\common\widgets\FileUpload7\Validator',
                'widget' => [
                    '\common\widgets\FileUpload7\FileUpload',
                    [
                        'update'    => \avatar\controllers\CabinetSchoolPagesConstructorController::getUpdate(),
                        'settings'  => \avatar\controllers\CabinetSchoolPagesConstructorController::getSettings($this->_school_id, $this->_type_id),
                        'events'    => [
                            'onDelete' => function (BlogItem $item) {
                                $r = new \cs\services\Url($item->image);
                                $d = pathinfo($r->path);
                                $start = $d['dirname'] . '/' . $d['filename'];

                                File::deleteAll(['like', 'file', $start]);
                            },
                        ],
                    ],
                ],
            ],
        ];
    }
}
