<?php

namespace avatar\models\forms;

use avatar\modules\Piramida\PaySystems\Base\Model;
use common\models\avatar\UserBill;
use common\models\avatar\UserBillOperation;
use common\models\UserDocument;
use common\payment\BitCoinBlockTrailPayment;
use cs\base\FormActiveRecord;
use cs\services\BitMask;
use yii\base\Exception;
use yii\db\ActiveRecord;
use yii\db\Query;
use yii\helpers\Json;
use yii\helpers\VarDumper;

/**
 */
class SchoolSntPohodUser extends \iAvatar777\services\FormAjax\ActiveRecord
{
    /** @var  \common\models\school\School */
    public $school;

    public function rules()
    {
        return [
            ['pohod_id', 'integer'],

            ['user_id', 'required'],
            ['user_id', 'integer'],

            ['start', 'required'],
            ['start', 'default'],

            ['end', 'required'],
            ['end', 'default'],

            ['tarif', 'required'],
            ['tarif', 'integer'],
        ];
    }
}