<?php
namespace avatar\models\forms;

use common\components\providers\Token;
use common\models\avatar\Currency;
use cs\base\FormActiveRecord;
use cs\services\VarDumper;
use Yii;
use yii\base\Model;
use yii\base\NotSupportedException;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;
use yii\web\IdentityInterface;

/**
 * TokenAdd model
 *
 */
class TokenAdd extends Model
{
    /** @var  string */
    public $code;

    /** @var  int */
    public $initialCoins;

    /** @var  string */
    public $title;

    /** @var  int */
    public $decimals;

    /** @var  string */
    public $image;

    public $optionsImage = [
        'image',
        'image',
        'image',
        'widget' => [
            '\common\widgets\FileUpload31\FileUpload',
            [
                'tableName' => 'currency',
                'options'   => [
                    'small' => [
                        300,
                        300,
                        \common\widgets\FileUpload3\FileUpload::MODE_THUMBNAIL_CUT
                    ],
                ]
            ]
        ],
    ];


    public function rules()
    {
        return [
            [['code', 'initialCoins', 'title', 'decimals', 'image'], 'safe'],
        ];
    }

    public function load($data, $formName = null)
    {
        $ret = parent::load($data, $formName);

        if ($ret) {
            $class = new \common\widgets\FileUpload31\FileUpload([
                'tableName' => 'currency',
                'model'     => $this,
                'attribute' => 'image',
                'value'     => $this->image,
            ]);
            $class->onLoad([]);
        }

        return $ret;
    }


    public function action()
    {
        // добавление currency
        $currency = new Currency([
            'code'     => $this->code,
            'title'    => $this->title,
            'decimals' => $this->decimals,
        ]);
        $currency->save();
        $currency->id = $currency::getDb()->lastInsertID;

        $class = new \common\widgets\FileUpload31\FileUpload([
            'tableName' => 'currency',
            'model'     => $this,
            'attribute' => 'image',
            'value'     => $this->image,
            'id'        => $currency->id,
        ]);
        $class->onUpdate($this->optionsImage);
        $currency->image = $this->image;
        $currency->save();

        // регистрация контракта

        // добавление token
        $token = new \common\models\Token([
            'address'     => '0x',
            'abi'         => '[{"constant":true,"inputs":[],"name":"name","outputs":[{"name":"","type":"string"}],"payable":false,"type":"function"},{"constant":false,"inputs":[{"name":"_spender","type":"address"},{"name":"_value","type":"uint256"}],"name":"approve","outputs":[{"name":"success","type":"bool"}],"payable":false,"type":"function"},{"constant":true,"inputs":[],"name":"totalSupply","outputs":[{"name":"","type":"uint256"}],"payable":false,"type":"function"},{"constant":false,"inputs":[{"name":"_from","type":"address"},{"name":"_to","type":"address"},{"name":"_value","type":"uint256"}],"name":"transferFrom","outputs":[{"name":"success","type":"bool"}],"payable":false,"type":"function"},{"constant":true,"inputs":[],"name":"decimals","outputs":[{"name":"","type":"uint8"}],"payable":false,"type":"function"},{"constant":true,"inputs":[],"name":"version","outputs":[{"name":"","type":"string"}],"payable":false,"type":"function"},{"constant":true,"inputs":[{"name":"_owner","type":"address"}],"name":"balanceOf","outputs":[{"name":"balance","type":"uint256"}],"payable":false,"type":"function"},{"constant":true,"inputs":[],"name":"symbol","outputs":[{"name":"","type":"string"}],"payable":false,"type":"function"},{"constant":false,"inputs":[{"name":"_to","type":"address"},{"name":"_value","type":"uint256"}],"name":"transfer","outputs":[{"name":"success","type":"bool"}],"payable":false,"type":"function"},{"constant":false,"inputs":[{"name":"_spender","type":"address"},{"name":"_value","type":"uint256"},{"name":"_extraData","type":"bytes"}],"name":"approveAndCall","outputs":[{"name":"success","type":"bool"}],"payable":false,"type":"function"},{"constant":true,"inputs":[{"name":"_owner","type":"address"},{"name":"_spender","type":"address"}],"name":"allowance","outputs":[{"name":"remaining","type":"uint256"}],"payable":false,"type":"function"},{"inputs":[{"name":"_initialAmount","type":"uint256"},{"name":"_tokenName","type":"string"},{"name":"_decimalUnits","type":"uint8"},{"name":"_tokenSymbol","type":"string"}],"payable":false,"type":"constructor"},{"payable":false,"type":"fallback"},{"anonymous":false,"inputs":[{"indexed":true,"name":"_from","type":"address"},{"indexed":true,"name":"_to","type":"address"},{"indexed":false,"name":"_value","type":"uint256"}],"name":"Transfer","type":"event"},{"anonymous":false,"inputs":[{"indexed":true,"name":"_owner","type":"address"},{"indexed":true,"name":"_spender","type":"address"},{"indexed":false,"name":"_value","type":"uint256"}],"name":"Approval","type":"event"}]',
            'currency_id' => $currency->id,
        ]);
        $token->save();
    }
}
