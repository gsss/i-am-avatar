<?php

namespace avatar\models\forms;

use common\models\SendLetter;
use common\models\UserSeed;
use common\services\Security;
use cs\Application;
use cs\services\Str;
use Yii;
use yii\base\Model;
use yii\helpers\ArrayHelper;
use yii\helpers\VarDumper;

/**
 * ContactForm is the model behind the contact form.
 */
class CabinetBillsSaveSeeds extends Model
{
    public $password;

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            [['password'], 'required', 'message' => 'Это поле должно быть заполнено обязательно'],
            ['password', 'validatePassword'],
        ];
    }

    /**
     * @return array customized attribute labels
     */
    public function attributeLabels()
    {
        return [
            'password' => 'Пароль',
        ];
    }

    public function validatePassword($attribute, $params)
    {
        if (!$this->hasErrors()) {
            /** @var \common\models\UserAvatar $user */
            $user = Yii::$app->user->identity;
            if (!$user->validatePassword($this->password)) {
                $this->addError($attribute, 'Пароль не верный');
            }
        }
    }


    /**
     * Задает пароль и создает первый кошелек
     *
     * @return string SEEDs
     */
    public function action()
    {
        $userCabinet = $this->password;

        // генерирует SEEDs и сохраняет
        $seeds = Security::getSeeds();
        $sha256 = hash('sha256', $seeds);
        $key32 = substr($sha256, 0, 32);
        $hash = Security\AES::encrypt256CBC($userCabinet, $key32);
        $seedsHashObject = UserSeed::add($hash);

        return $seeds;
    }
}
