<?php

namespace avatar\models\forms;

use common\models\avatar\CurrencyLink;
use common\models\piramida\Wallet;
use common\models\school\File;
use cs\base\FormActiveRecord;
use cs\services\Url;
use cs\services\VarDumper;
use Yii;
use yii\base\Model;
use yii\base\NotSupportedException;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\web\IdentityInterface;

/**
 */
class CoinAdd extends Model
{
    public $code;
    public $name;
    public $image;
    public $decimals;
    public $amount;
    public $comment;

    /** @var  \common\models\avatar\Currency */
    public $currencyExt;

    /** @var  \common\models\piramida\Currency */
    public $currencyInt;

    public function rules()
    {
        return [
            ['code', 'required'],
            ['code', 'string'],

            ['name', 'required'],
            ['name', 'string'],

            ['image', 'string'],
            ['image', 'url'],

            ['decimals', 'required'],
            ['decimals', 'integer', 'min' => 0, 'max' => 18],

            ['amount', 'required'],
            ['amount', 'integer', 'min' => 0, 'max' => pow(2, 64)],

            ['comment', 'string', 'min' => 0, 'max' => 255],
        ];
    }

    public function attributeLabels()
    {
        return [
            'code'     => 'Код валюты',
            'name'     => 'Название валюты',
            'image'    => 'Картинка валюты',
            'decimals' => 'Картинка валюты',
            'amount'   => 'Атомов на эмиссию',
            'comment'  => 'Коментарий к эмиссии',
        ];
    }

    public function attributeHints()
    {
        return [
            'code'     => 'Обычно англ большие буквы 0-10 шт',
            'decimals' => 'от 0 до 18, обычно 2',
        ];
    }

    /**
     * @return array
     * [
     * 'wallet'    => $wallet,
     * 'operation' => $operation,
     * 'link'      => $link,
     * ];
     */
    public function add()
    {
        $this->currencyExt = \common\models\avatar\Currency::add([
            'code'     => $this->code,
            'decimals' => $this->decimals,
            'title'    => $this->name,
            'image'    => $this->image,
        ]);

        $this->currencyInt = \common\models\piramida\Currency::add([
            'code'     => $this->code,
            'decimals' => $this->decimals,
            'name'     => $this->name,
        ]);

        $wallet = Wallet::addNew(['currency_id' => $this->currencyInt->id]);

        $operation = $wallet->in($this->amount, $this->comment);

        $link = CurrencyLink::add([
            'currency_int_id' => $this->currencyInt->id,
            'currency_ext_id' => $this->currencyExt->id,
        ]);

        return [
            'wallet'    => $wallet,
            'operation' => $operation,
            'link'      => $link,
        ];
    }
}
