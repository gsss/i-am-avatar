<?php

namespace avatar\models\forms;

use app\models\Piramida\Piramida;
use app\models\Piramida\Wallet;
use app\models\User;
use common\models\UserAvatar;
use Yii;
use yii\base\Model;
use yii\db\Query;


/**
 * Registration is the model behind the contact form.
 */
class SetPassword extends Model
{
    public $password1;
    public $password2;

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            ['password1', 'required', 'message' => Yii::t('c.0uMdJb0e0n', 'Это поле должно быть заполнено обязательно')],
            ['password2', 'required', 'message' => Yii::t('c.0uMdJb0e0n', 'Это поле должно быть заполнено обязательно')],
            ['password2', 'compare', 'compareAttribute' => 'password1', 'operator' => '==', 'message' => Yii::t('c.0uMdJb0e0n', 'Пароли должны совпадать')],
        ];
    }

    /**
     * @param \common\models\UserRoot $userRoot
     *
     * @return UserAvatar
     * @throws
     */
    public function action($userRoot)
    {
        $user =  UserAvatar::createUserFromRoot($userRoot, $this->password1);
        Yii::$app->user->login($user);

        // делаю update для ранее зарегистрированых пользователей
        \common\models\school\PotokUser3Link::updateAll(['is_avatar' => 1], ['user_root_id' => $userRoot->id]);

        return $user;
    }
}
