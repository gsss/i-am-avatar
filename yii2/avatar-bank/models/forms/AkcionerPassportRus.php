<?php
namespace avatar\models\forms;

use cs\base\FormActiveRecord;
use Yii;
use yii\base\NotSupportedException;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\web\IdentityInterface;

/**
 * Akcioner model
 *
 * @property integer id
 * @property string  foto
 */
class AkcionerPassportRus extends FormActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'rai_akcioneri_passport_rus';
    }

    public function formAttributes()
    {
        return [
            [
                'seria',
                'Серия',
                0,
                'integer',
                ['min' => 1000, 'max' => 9999]
            ],
            [
                'number',
                'Номер',
                0,
                'integer',
                ['min' => 100000, 'max' => 999999]
            ],
            [
                'kem',
                'Кем выдан',
                0,
                'string',
            ],
            [
                'propiska',
                'Прописка',
                0,
                'string',
            ],
            [
                'issue_date',
                'Дата выдачи',
                0,
                'string',
            ],
            [
                'rojd_date',
                'Дата рождения',
                0,
                'string',
            ],
            [
                'rojd_place',
                'Место рождения',
                0,
                'string',
                ['max' => 100]
            ],
            [
                'image1',
                'Главный скан',
                0,
                '\common\widgets\FileUpload3\Validator',
                'widget' => [
                    '\common\widgets\FileUpload3\FileUpload',
                    [
                        'options' => [
                            'small' => \avatar\services\Html::$formatIcon
                        ]
                    ]
                ]
            ],
            [
                'image2',
                'Главный скан',
                0,
                '\common\widgets\FileUpload3\Validator',
                'widget' => [
                    '\common\widgets\FileUpload3\FileUpload',
                    [
                        'options' => [
                            'small' => \avatar\services\Html::$formatIcon
                        ]
                    ]
                ]
            ],
        ];
    }
}
