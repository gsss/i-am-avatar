<?php

namespace avatar\models\forms;

use cs\base\FormActiveRecord;
use Yii;
use yii\base\NotSupportedException;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\helpers\Json;
use yii\web\IdentityInterface;

/**
 * PaySystem model
 *
 * @property integer id
 * @property string file
 */
class Test extends FormActiveRecord
{
    public $_school_id;
    public $_type_id;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'test';
    }

    public function formAttributes()
    {
        return [
            [
                'file',
                'Картинка',
                0,
                '\common\widgets\FileUpload3\Validator',
                'widget' => [
                    '\common\widgets\FileUpload7\FileUpload',
                    [
                        'school_id' => $this->_school_id,
                        'type_id'   => $this->_type_id,
                        'settings'  => [
                            'maxSize' => 2000,
                        ],
                        'update'    => [
                            [
                                'function' => 'crop',
                                'index'    => 'crop',
                                'options'  => [
                                    'width'  => '300',
                                    'height' => '300',
                                    'mode'   => 'MODE_THUMBNAIL_CUT',
                                ],
                            ],
                        ],
                    ],
                ],
            ],
        ];
    }
}
