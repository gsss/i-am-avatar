<?php

namespace avatar\models\forms;

use common\models\SendLetter;
use cs\Application;
use cs\services\Str;
use Yii;
use yii\base\Model;
use yii\helpers\ArrayHelper;
use yii\helpers\VarDumper;

/**
 * ContactForm is the model behind the contact form.
 */
class CardNumber extends Model
{
    public $number;

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            [['number'], 'required'],
            [['number'], 'verifyNumber'],
        ];
    }

    /**
     * @return array customized attribute labels
     */
    public function attributeLabels()
    {
        return [
            'number' => 'Номер',
        ];
    }

    /**
     * @return array customized attribute labels
     */
    public function verifyNumber($attribute, $params)
    {
        if (!$this->hasErrors()) {

        }
    }

    /**
     * @return boolean whether the model passes validation
     */
    public function action()
    {
        return true;
    }
}
