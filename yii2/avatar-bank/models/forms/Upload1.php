<?php

namespace avatar\models\forms;

use common\models\SendLetter;
use common\models\UserSeed;
use common\services\Security;
use cs\Application;
use cs\services\Str;
use Yii;
use yii\base\Model;
use yii\helpers\ArrayHelper;
use yii\helpers\VarDumper;
use yii\web\UploadedFile;

/**
 */
class Upload1 extends Model
{
    /** @var  string */
    public $upload1;

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            ['upload1', 'string'],
        ];
    }

}
