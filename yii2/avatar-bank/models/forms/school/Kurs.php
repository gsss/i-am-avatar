<?php

namespace avatar\models\forms\school;

use common\models\school\File;
use cs\base\FormActiveRecord;
use cs\services\VarDumper;
use Yii;
use yii\base\NotSupportedException;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\helpers\Url;
use yii\web\IdentityInterface;
use yii\web\JsExpression;

/**
 *
 * @property integer    id
 * @property string     name
 * @property string     image
 * @property int        school_id
 * @property int        created_at
 * @property int        updated_at
 * @property int        is_show
 * @property string     for_who
 * @property string     daet
 */
class Kurs extends FormActiveRecord
{
    public $_school_id;
    public $_type_id;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'school_kurs';
    }

    public function formAttributes()
    {
        return [
            [
                'name',
                'Название',
                1,
                'string',
                ['max' => 100],
            ],
            [
                'for_who',
                'Для кого?',
                0,
                'string',
            ],
            [
                'daet',
                'Что курс дает?',
                0,
                'string',
            ],
            [
                'status',
                'Статус',
                0,
                'integer',
            ],
            [
                'is_show',
                'Показывать в каталоге курсов?',
                0,
                '\common\widgets\CheckBox2\Validator',
                'widget' => [
                    '\common\widgets\CheckBox2\CheckBox'
                ],
            ],
            [
                'image',
                'Картинка',
                0,
                '\common\widgets\FileUpload7\Validator',
                'widget' => [
                    '\common\widgets\FileUpload7\FileUpload',
                    [
                        'update'    => \avatar\controllers\CabinetSchoolPagesConstructorController::getUpdate(),
                        'settings'  => \avatar\controllers\CabinetSchoolPagesConstructorController::getSettings($this->_school_id, $this->_type_id),
                        'events'    => [
                            'onDelete' => function (\avatar\models\forms\school\Article $item) {
                                $r = new \cs\services\Url($item->image);
                                $d = pathinfo($r->path);
                                $start = $d['dirname'] . '/' . $d['filename'];

                                File::deleteAll(['like', 'file', $start]);
                            },
                        ],
                    ],
                ],
            ],
            [
                'social_net_title',
                'Репост для соц сети. Заголовок',
                0,
                'string',
            ],
            [
                'social_net_description',
                'Репост для соц сети. Описание',
                0,
                'string',
            ],
            [
                'social_net_image',
                'Репост для соц сети. Картинка',
                0,
                '\common\widgets\FileUpload7\Validator',
                'widget' => [
                    '\common\widgets\FileUpload7\FileUpload',
                    [
                        'update'    => \avatar\controllers\CabinetSchoolPagesConstructorController::getUpdate(),
                        'settings'  => \avatar\controllers\CabinetSchoolPagesConstructorController::getSettings($this->_school_id, $this->_type_id),
                        'events'    => [
                            'onDelete' => function (\avatar\models\forms\school\Article $item) {
                                $r = new \cs\services\Url($item->image);
                                $d = pathinfo($r->path);
                                $start = $d['dirname'] . '/' . $d['filename'];

                                File::deleteAll(['like', 'file', $start]);
                            },
                        ],
                    ],
                ],
            ],
        ];
    }
}
