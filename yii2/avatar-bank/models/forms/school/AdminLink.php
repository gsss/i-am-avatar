<?php
namespace avatar\models\forms\school;

use cs\base\FormActiveRecord;
use Yii;
use yii\base\NotSupportedException;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\web\IdentityInterface;

/**
 * @property integer id
 * @property int    school_id
 * @property int    user_id
 */
class AdminLink extends FormActiveRecord
{
    public $_school_id;


    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'school_admin_link';
    }

    public function rules()
    {
        return [
            ['school_id', 'integer'],
            ['user_id', 'integer'],
            ['user_id', 'required'],
        ];
    }

    public function formAttributes()
    {
        return [
            [
                'user_id',
                'Пользователь',
                0,
                '\common\widgets\FileUpload3\Validator',
                'widget' => [
                    '\common\widgets\Autocomplete\Autocomplete',
                    [
                        'clientOptions' => [
                            'source' => new \yii\web\JsExpression(<<<JS
function( request, response ) {
    ajaxJson({
        url: '/cabinet-school-admins/search-ajax?id=' + {$this->_school_id},
        data: {
            term: request.term
        },
        success: function(ret) {
            response(ret);
        }
    });
     }
JS
                            ),
                        ],
                        'options'       => [
                            'placeholder' => 'Искать по почте...',
                        ],
                    ]
                ]
            ],
        ];
    }
}
