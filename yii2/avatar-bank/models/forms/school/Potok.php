<?php

namespace avatar\models\forms\school;

use avatar\modules\HumanDesign\calculate\HumanDesignAmericaCom;
use common\models\HD;
use common\models\HDtown;
use cs\base\BaseForm;
use cs\base\FormActiveRecord;
use cs\services\SitePath;
use cs\services\VarDumper;
use cs\web\Exception;
use Yii;
use yii\base\Model;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use cs\Widget\FileUpload2\FileUpload;
use yii\widgets\ActiveForm;

/**
 * @property int kurs_id
 */
class Potok extends FormActiveRecord
{

    public static function tableName()
    {
        return 'school_potok';
    }

    public function formAttributes()
    {
        return [
            [
                'name',
                'Название',
                1,
                'string'
            ],
            [
                'date_start',
                'Дата старта',
                0,
                'cs\Widget\DatePicker\Validator',
                'widget' => [
                    '\common\widgets\DatePicker\DatePicker',
                    [
                        'dateFormat' => 'php:d.m.Y',
                    ],
                ],
            ],
            [
                'date_end',
                'Дата финиша',
                0,
                'cs\Widget\DatePicker\Validator',
                'widget' => [
                    '\common\widgets\DatePicker\DatePicker',
                    [
                        'dateFormat' => 'php:d.m.Y',
                    ],
                ],
            ],
        ];
    }
}