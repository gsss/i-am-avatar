<?php
namespace avatar\models\forms\school;

use common\models\school\PotokUser3Link;
use common\models\school\PotokUserExt;
use common\models\school\UserLink;
use common\models\UserRoot;
use Yii;
use yii\base\Model;
use yii\base\NotSupportedException;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\web\IdentityInterface;

/**
 * @property string email
 * @property string name
 * @property string phone
 */
class Lid extends Model
{
    /** @var  int инициализируется при создании */
    public $potok_id;


    public $email;
    public $name;
    public $phone;

    public function rules()
    {
        return [
            ['email', 'string'],
            ['email', 'required'],
            ['email', 'email'],
            ['email', 'verifyEmail'],

            ['name', 'string'],

            ['phone', 'string'],
        ];
    }

    public function verifyEmail($attribute, $params)
    {
        if (!$this->hasErrors()) {
            // ищу email в user_root
            $user_root = UserRoot::findOne(['email' => $this->email]);
            if (!is_null($user_root)) {
                // ищу есть ли он уже в потоке
                $link = PotokUser3Link::findOne([
                    'user_root_id' => $user_root->id,
                    'potok_id'     => $this->potok_id,
                ]);
                if (!is_null($link)) {
                    $this->addError($attribute, 'Есть такой EMAIL в потоке');
                }
            }
        }
    }

    /**
     * Документ для понимания
     * https://docs.google.com/spreadsheets/d/1U-2ud9ecXdaeaGcCFc2uowgvGBGBG5KxcICRiFEIeGo/edit?usp=sharing
     *
     * @param $potok_id
     * @param $school_id
     *
     * @return array
     * [
     *  'code' => int
     *              101 - пользователь с таким email уже есть в этом потоке
     *              1 - пользователя не было в user_root он добавлен
     *              2 - пользователь был в user_root и user и добавлен в school_potok_user_root_link и если надо добавляет в user_school_link
     *              3 - пользователь был в каком то потоке в виде лида, и он был добавлен в этот поток
     * ]
     */
    public function action($potok_id, $school_id)
    {
        // ищу email в user_root
        $user_root = UserRoot::findOne(['email' => $this->email]);
        if (is_null($user_root)) {
            $user_root = UserRoot::add(['email' => $this->email]);
            $link = PotokUser3Link::add([
                'user_root_id' => $user_root->id,
                'potok_id'     => $potok_id,
                'is_avatar'    => 0,
            ]);
            $ext = PotokUserExt::add([
                'link_id' => $link->id,
                'phone'   => $this->phone,
                'name'    => $this->name,
            ]);
            $linkSchool = UserLink::add(['user_root_id' => $user_root->id, 'school_id' => $school_id]);

            return [1];
        } else {
            // ищу есть ли он уже в потоке
            $link = PotokUser3Link::findOne([
                'user_root_id' => $user_root->id,
                'potok_id'     => $potok_id,
            ]);
            if (!is_null($link)) return ['code' => 101];

            if (is_null(UserLink::findOne(['user_root_id' => $user_root->id, 'school_id' => $school_id]))) {
                $linkSchool = UserLink::add(['user_root_id' => $user_root->id, 'school_id' => $school_id]);
            }

            if ($user_root->avatar_status == UserRoot::STATUS_REGISTERED) {
                $link = PotokUser3Link::add([
                    'user_root_id' => $user_root->id,
                    'potok_id'     => $potok_id,
                    'is_avatar'    => 1,
                ]);

                // но это не обязательно потому что данные будут браться из user. То есть эти данные уже избыточные.
                $ext = PotokUserExt::add([
                    'link_id' => $link->id,
                    'phone'   => $this->phone,
                    'name'    => $this->name,
                ]);
                return [2];
            } else {
                $link = PotokUser3Link::add([
                    'user_root_id' => $user_root->id,
                    'potok_id'     => $potok_id,
                    'is_avatar'    => 0,
                ]);
                $ext = PotokUserExt::add([
                    'link_id' => $link->id,
                    'phone'   => $this->phone,
                    'name'    => $this->name,
                ]);
                return [3];
            }
        }
    }
}
