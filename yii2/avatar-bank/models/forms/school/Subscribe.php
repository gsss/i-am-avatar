<?php
namespace avatar\models\forms\school;

use cs\base\FormActiveRecord;
use Yii;
use yii\base\NotSupportedException;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\web\IdentityInterface;

/**
 * @property integer id
 * @property integer potok_id
 * @property string  subject
 * @property string  content
 * @property integer created_at
 */
class Subscribe extends FormActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'school_potok_subscribe';
    }

    public function formAttributes()
    {
        return [
            [
                'subject',
                'Тема',
                1,
                'string',
                ['max' => 255],
            ],
            [
                'content',
                'Содержание',
                0,
                'string',
                'widget' => [ '\common\widgets\HtmlContent\HtmlContent' ]
            ],
        ];
    }
}
