<?php

namespace avatar\models\forms\school;

use common\models\school\File;
use cs\base\FormActiveRecord;
use cs\services\VarDumper;
use Yii;
use yii\base\NotSupportedException;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\helpers\Url;
use yii\web\IdentityInterface;
use yii\web\JsExpression;

/**
 *
 * @property integer id
 * @property string name
 * @property string image
 * @property int    school_id
 * @property int    created_at
 * @property string date
 * @property string id_string
 * @property string content
 * @property string link
 */
class NewsItem extends FormActiveRecord
{
    public $_school_id;
    public $_type_id;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'school_news_article';
    }

    public function formAttributes()
    {
        $update = [];

        return [
            [
                'name',
                'Название',
                1,
                'string',
                ['max' => 255],
            ],
            [
                'image',
                'Картинка',
                0,
                '\common\widgets\FileUpload7\Validator',
                'widget' => [
                    '\common\widgets\FileUpload7\FileUpload',
                    [
                        'update'    => \avatar\controllers\CabinetSchoolSettingsController::getUpdateNewsImage($this->_school_id),
                        'settings'  => \avatar\controllers\CabinetSchoolPagesConstructorController::getSettings($this->_school_id, $this->_type_id),
                        'events'    => [
                            'onDelete' => function ($item) {
                                $r = new \cs\services\Url($item->image);
                                $d = pathinfo($r->path);
                                $start = $d['dirname'] . '/' . $d['filename'];

                                File::deleteAll(['like', 'file', $start]);
                            },
                        ],
                    ],
                ],
            ],
            [
                'content',
                'Полный текст',
                0,
                'string',
                'widget' => [
                    '\common\widgets\HtmlContent\HtmlContent',
                ],
            ],
        ];
    }
}
