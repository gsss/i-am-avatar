<?php
namespace avatar\models\forms\school;

use cs\base\FormActiveRecord;
use Yii;
use yii\base\NotSupportedException;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;
use yii\web\IdentityInterface;

/**
 * PageBlock model
 *
 * @property integer id
 * @property integer category_id
 * @property integer sort_index
 * @property string  name
 * @property string  asset
 * @property string  image
 * @property string  init
 */
class PageBlock extends FormActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'school_page_block';
    }

    public function formAttributes()
    {
        return [
            [
                'name',
                'Название',
                1,
                'string',
                ['max' => 100],
            ],
            [
                'asset',
                'asset',
                0,
                'string',
                ['max' => 100],
            ],
            [
                'init',
                'init',
                0,
                'string',
            ],
            [
                'image',
                'Картинка',
                0,
                '\common\widgets\FileUpload3\Validator',
                'widget' => [
                    '\common\widgets\FileUpload3\FileUpload',
                    [
                        'options' => [
                            'small' => [
                                600,
                                300,
                                \common\widgets\FileUpload3\FileUpload::MODE_THUMBNAIL_CUT
                            ]
                        ]
                    ]
                ]
            ],
        ];
    }

    public function rules()
    {
        $r = parent::rules();
        $new = [
            ['init', 'jsonCheck'],
        ];

        return ArrayHelper::merge($r, $new);
    }

    public function jsonCheck($attribute, $params)
    {
        if (!$this->hasErrors()) {
            try {
                $j = Json::decode($this->init);
            } catch (\Exception $e) {
                $this->addError($attribute, 'Ошибка в JSON данных, проверьте пожалуйста');
            }
        }
    }
}
