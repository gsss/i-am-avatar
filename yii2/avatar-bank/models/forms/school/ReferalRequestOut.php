<?php
namespace avatar\models\forms\school;

use cs\base\FormActiveRecord;
use Yii;
use yii\base\NotSupportedException;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\web\IdentityInterface;

/**
 * @property integer id
 * @property integer school_id
 * @property integer user_id
 * @property integer currency_id    db.currency.id
 * @property string  to             Адрес кошелька для вывода
 * @property integer amount         Кол-во атомов для вывода
 * @property integer status         Статус заявки, 0 - создана, 1 - выведено успешно, 2 - отвергнута
 * @property integer transaction_id
 * @property integer created_at
 */
class ReferalRequestOut extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'school_referal_out_request';
    }

    public function rules()
    {
        return [
            ['to', 'required'],
            ['to', 'string'],
            ['amount', 'required'],
            ['amount', 'double'],
        ];
    }


    public function behaviors()
    {
        return [
            [
                'class'      => '\yii\behaviors\TimestampBehavior',
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at'],
                ],
                // если вместо метки времени UNIX используется datetime:
                // 'value' => new Expression('NOW()'),
            ],
        ];
    }
}
