<?php

namespace avatar\models\forms\school;

use common\models\school\PotokUser3Link;
use common\models\school\PotokUserExt;
use common\models\school\TildaPlugin;
use common\models\school\UserLink;
use common\models\UserRoot;
use Yii;
use yii\base\InvalidConfigException;
use yii\base\Model;
use yii\base\NotSupportedException;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\web\IdentityInterface;

/**
 * @property string email
 * @property string name
 * @property string phone
 */
class Tilda extends Model
{
    /** @var  \common\models\school\School */
    public $school;

    /** @var  \common\models\school\TildaPlugin */
    public $settings;

    public $publickey;
    public $secretkey;

    public function init()
    {
        if (is_null($this->school)) throw new InvalidConfigException('school required');
        $settings = TildaPlugin::findOne(['school_id' => $this->school->id]);
        if (!is_null($settings)) {
            $this->publickey = $settings->publickey;
            $this->secretkey = $settings->secretkey;
        }
        $this->settings = $settings;
    }

    public function rules()
    {
        return [
            ['publickey', 'required'],
            ['publickey', 'string'],

            ['secretkey', 'required'],
            ['secretkey', 'string'],
        ];
    }

    /**
     *
     */
    public function action()
    {
        if (is_null($this->settings)) {
            $settings = TildaPlugin::add(
                [
                    'school_id' => $this->school->id,
                    'publickey' => $this->publickey,
                    'secretkey' => $this->secretkey,
                ]
            );
        } else {
            $this->settings->publickey = $this->publickey;
            $this->settings->secretkey = $this->secretkey;
            $this->settings->save();
        }
    }
}
