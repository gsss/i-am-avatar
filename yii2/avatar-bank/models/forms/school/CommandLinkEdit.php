<?php
namespace avatar\models\forms\school;

use common\models\piramida\Currency;
use cs\base\FormActiveRecord;
use Yii;
use yii\base\NotSupportedException;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\web\IdentityInterface;

/**
 * @property integer id
 * @property int    school_id
 * @property int    user_id
 * @property int    hour_price
 * @property int    hour_price_currency_id
 */
class CommandLinkEdit extends \iAvatar777\services\FormAjax\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'school_command_link';
    }

    public function rules()
    {
        return [
            ['school_id', 'integer'],

            ['user_id', 'required'],
            ['user_id', 'integer'],

            ['hour_price', 'integer'],

            ['hour_price_currency_id', 'integer'],
        ];
    }

    public function attributeWidgets()
    {
        return [
            'hour_price' => [
                'class'    => '\avatar\widgets\Price',
                'currency' => Currency::findOne(Currency::RUB),
            ],
        ];
    }
}
