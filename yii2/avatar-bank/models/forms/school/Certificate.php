<?php
namespace avatar\models\forms\school;

use common\models\school\File;
use cs\base\FormActiveRecord;
use Yii;
use yii\base\NotSupportedException;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\web\IdentityInterface;

/**
 * PaySystem model
 *
 * @property integer id
 * @property string  name
 * @property string  link
 * @property string  image
 * @property string  description
 * @property string  content
 */
class Certificate extends FormActiveRecord
{
    public $_school_id;
    public $_type_id;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'school_user_sertificate';
    }

    public function formAttributes()
    {
        return [
            [
                'name',
                'Название',
                1,
                'string',
                ['max' => 100],
            ],
            [
                'school_id',
                'school_id',
                0,
                'integer',
            ],
            [
                'description',
                'description',
                0,
                'string',
                'widget' => [
                    '\common\widgets\HtmlContent\HtmlContent',
                ]
            ],

            [
                'image',
                'Картинка',
                0,
                '\common\widgets\FileUpload7\Validator',
                'widget' => [
                    '\common\widgets\FileUpload7\FileUpload',
                    [
                        'update'    => \avatar\controllers\CabinetSchoolPagesConstructorController::getUpdate(),
                        'settings'  => \avatar\controllers\CabinetSchoolPagesConstructorController::getSettings($this->_school_id, $this->_type_id),
                        'events'    => [
                            'onDelete' => function ($item) {
                                $r = new \cs\services\Url($item->image);
                                $d = pathinfo($r->path);
                                $start = $d['dirname'] . '/' . $d['filename'];

                                File::deleteAll(['like', 'file', $start]);
                            },
                        ],
                    ],
                ],
            ],
        ];
    }
}
