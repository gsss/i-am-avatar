<?php
namespace avatar\models\forms\school;

use common\models\school\PotokUser3Link;
use common\models\UserAvatar;
use cs\base\FormActiveRecord;
use Yii;
use yii\base\Model;
use yii\base\NotSupportedException;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\helpers\VarDumper;
use yii\web\IdentityInterface;

/**
 * @property string text
 */
class AdminTelegram extends Model
{
    public $text;

    public function rules()
    {
        return [
            ['text', 'required'],
            ['text', 'string'],
        ];
    }

    public function action()
    {
        $rows = PotokUser3Link::find()->where(['potok_id' => 2, 'is_unsubscribed_telegram' => 0])->select('user_root_id')->column();
        $ids = UserAvatar::find()->where(['in', 'user_root_id', $rows])->andWhere(['not', ['telegram_chat_id' => null]])->select('telegram_chat_id')->column();

        /** @var \aki\telegram\Telegram $telegram */
        $telegram = Yii::$app->telegram;

        foreach ($ids as $id) {
            $telegram->sendMessage(['chat_id' => $id, 'text' => $this->text]);
        }
    }
}
