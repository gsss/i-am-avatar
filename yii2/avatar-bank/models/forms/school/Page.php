<?php
namespace avatar\models\forms\school;

use cs\base\FormActiveRecord;
use Yii;
use yii\base\NotSupportedException;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\web\IdentityInterface;

/**
 * Page model
 *
 * @property integer id
 * @property integer school_id
 * @property string  facebook_image
 * @property string  facebook_title
 * @property string  facebook_description
 */
class Page extends FormActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'school_page';
    }

    public function formAttributes()
    {
        return [
            [
                'facebook_title',
                'title',
                0,
                'string',
                ['max' => 255],
            ],
            [
                'facebook_description',
                'description',
                0,
                'string',
                ['max' => 2000],
            ],
            [
                'facebook_image',
                'Картинка',
                0,
                '\common\widgets\FileUpload3\Validator',
                'widget' => [
                    '\common\widgets\FileUpload3\FileUpload',
                    [
                        'options' => [
                            'small' => [
                                600,
                                300,
                                \common\widgets\FileUpload3\FileUpload::MODE_THUMBNAIL_CUT
                            ]
                        ]
                    ]
                ]
            ],
            [
                'favicon',
                'Favicon PNG квадратная',
                0,
                '\common\widgets\FileUpload3\Validator',
                'widget' => [
                    '\common\widgets\FileUpload3\FileUpload',
                    [
                        'options' => [
                            'small' => [
                                64,
                                64,
                                \common\widgets\FileUpload3\FileUpload::MODE_THUMBNAIL_CUT
                            ]
                        ]
                    ]
                ]
            ],
        ];
    }
}
