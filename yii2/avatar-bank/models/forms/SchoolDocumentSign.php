<?php

namespace avatar\models\forms;

use BitcoinPHP\BitcoinECDSA\BitcoinECDSA;
use common\models\avatar\UserBill;
use common\models\school\File;
use common\models\UserDigitalSign;
use common\models\UserDocumentSignature;
use common\payment\BitCoinBlockTrailPayment;
use cs\base\FormActiveRecord;
use cs\services\BitMask;
use iAvatar777\services\FormAjax\ActiveForm;
use yii\base\Exception;
use yii\db\ActiveRecord;
use yii\db\Query;
use yii\helpers\VarDumper;

/**
 * @property int     id
 * @property int     user_id
 * @property int     type_id
 * @property string  name
 * @property string  txid
 * @property string  file
 * @property string  hash
 * @property string  data
 * @property string  link
 * @property string  signature
 * @property int     created_at
 *
 */
class SchoolDocumentSign extends \iAvatar777\services\FormAjax\Model
{
    public $id;

    /** @var \common\models\UserDocument */
    public $document;

    public function rules()
    {
        return [
            ['id', 'required'],
            ['id', 'integer'],
            ['id', 'validateDocument'],
        ];
    }

    public function validateDocument($a, $p)
    {
        if (!$this->hasErrors()) {
            $doc = \common\models\UserDocument::findOne($this->id);
            if (is_null($doc)) {
                $this->addError($a, 'Не найден документ');
                return;
            }
            $this->document = $doc;

            if ($doc->user_id != \Yii::$app->user->id) {
                $this->addError($a, 'Это не ваш документ');
                return;
            }
        }
    }

    public function save($runValidation = true, $attributeNames = null)
    {
        $bitcoinECDSA = new BitcoinECDSA();
        $bitcoinECDSA->setPrivateKey(UserDigitalSign::getPK(\Yii::$app->user->id));
        $signature = $bitcoinECDSA->signMessage($this->document->hash, true);
        $this->document->signature = $signature;
        $this->document->save();

        UserDocumentSignature::add([
            'document_id' => $this->document->id,
            'user_id'     => $this->document->user_id,
            'created_at'  => time(),
            'member'      => $bitcoinECDSA->getAddress(),
            'signature'   => $this->document->signature,
        ]);

        return 1;
    }
}