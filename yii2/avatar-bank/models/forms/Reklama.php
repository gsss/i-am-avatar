<?php
namespace avatar\models\forms;

use cs\base\FormActiveRecord;
use Yii;
use yii\base\NotSupportedException;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\web\IdentityInterface;

/**
 * @property int id
 * @property int created_at
 * @property string name
 * @property string image
 * @property string file
 * @property string content
 */
class Reklama extends FormActiveRecord
{
    public $_school_id;
    public $_type_id;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'reklama';
    }

    public function formAttributes()
    {
        return [
            [
                'name',
                'Название',
                1,
                'string',
                ['max' => 255],
            ],

            [
                'content',
                'Полный текст',
                0,
                'string',
                'widget' => [
                    '\common\widgets\HtmlContent3\HtmlContent',
                ]
            ],
            [
                'file',
                'Файл',
                0,
                '\common\widgets\FileUpload3\Validator',
                'widget' => [
                    '\common\widgets\FileUpload8\FileUpload',
                    [
                        'school_id' => $this->_school_id,
                        'settings' => [
                            'maxSize'           => 50000,
                        ],
                    ]
                ]
            ],
            [
                'image',
                'Картинка',
                0,
                '\common\widgets\FileUpload7\Validator',
                'widget' => [
                    '\common\widgets\FileUpload7\FileUpload',
                    [
                        'school_id' => $this->_school_id,
                        'type_id'   => $this->_type_id,
                        'settings' => [
                            'maxSize'           => 2000,
                        ],
                        'update'    => [
                            [
                                'function' => 'crop',
                                'index'    => 'crop',
                                'options'  => [
                                    'width'  => '300',
                                    'height' => '300',
                                    'mode'   => 'MODE_THUMBNAIL_CUT',
                                ],
                            ],
                        ],
                    ]
                ]
            ],
        ];
    }
}
