<?php

namespace avatar\models\forms;

use common\models\avatar\UserBill;
use common\models\RequestTokenCreate;
use common\models\SendLetter;
use common\models\UserSeed;
use common\services\Security;
use cs\Application;
use cs\services\Str;
use Yii;
use yii\base\Model;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;
use yii\helpers\VarDumper;

/**
 */
class CabinetTokenAdd extends Model
{
    /** @var  string */
    public $password;

    /** @var  float */
    public $amount;

    /** @var  int идентификатор заявки на создание токена */
    public $request_id;

    /** @var  \common\models\RequestTokenCreate заявка на создание токена */
    public $request;

    /** @var  array ответ от add */
    private $response;

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            ['amount', 'required'],
            ['amount', 'double', 'min' => 0],

            ['password', 'required'],
            ['password', 'validatePassword'],

            ['request_id', 'required'],
            ['request_id', 'integer'],
            ['request_id', 'validateRequest'],
            ['request_id', 'tryToAction'],
        ];
    }

    /**
     * @return array customized attribute labels
     */
    public function attributeLabels()
    {
        return [
            'password' => 'Пароль',
        ];
    }

    /**
     * Конвертирует массив ошибок от формы чтобы их можно было выдать через JSON
     *
     * @param array $params
     *      [
     *      'amount' => ['Не верное число',...],
     *      ]
     *
     * @return array
     * [
     *      'name'  => 'amount',
     *      'value' => ['Не верное число',...],
     * ]
     */
    public function convert($params)
    {
        $ret = [];
        foreach ($params as $name => $arr) {
            $ret[] = [
                'name'  => $name,
                'value' => $arr,
            ];
        }
        return $ret;
    }

    public function validatePassword($attribute, $params)
    {
        if (!$this->hasErrors()) {
            /** @var \common\models\UserAvatar $user */
            $user = Yii::$app->user->identity;
            if (!$user->validatePassword($this->password)) {
                $this->addError($attribute, 'Пароль не верный');
            }
        }
    }

    public function validateRequest($attribute, $params)
    {
        if (!$this->hasErrors()) {
            $request = RequestTokenCreate::findOne($this->request_id);
            if (is_null($request)) {
                $this->addError($attribute, 'Не найдена заявка');
                return;
            }
            if ($request->user_id != Yii::$app->user->id) {
                $this->addError($attribute, 'Это не ваша заявка');
                return;
            }
            $this->request = $request;
        }
    }

    public function tryToAction($attribute, $params)
    {
        if (!$this->hasErrors()) {
            /** @var \common\models\UserAvatar $user */
            $user = Yii::$app->user->identity;

            // работаю с большими целыми числами
            $pow = bcpow(10, $this->request->decimals);
            $all = bcmul($this->amount, $pow);

            try {
                $data = $user->getWalletEthPassport()->contract(
                    $this->password,
                    $this->request->address,
                    $this->request->abi,
                    'add',
                    [
                        $all,
                    ]
                );
            } catch (\Exception $e) {
                $this->addError($attribute, $e->getMessage());
                return;
            }

            $this->response = $data;
        }
    }

    /**
     * @return string идентификатор транзакции
     */
    public function action()
    {
        return $this->response;
    }
}
