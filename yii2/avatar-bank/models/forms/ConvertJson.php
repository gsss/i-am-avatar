<?php
namespace avatar\models\forms;

use cs\base\FormActiveRecord;
use Yii;
use yii\base\Model;
use yii\base\NotSupportedException;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\helpers\Json;
use yii\web\IdentityInterface;

/**
 */
class ConvertJson extends Model
{
    public $text;

    public function rules()
    {
        return [
            ['text', 'string'],
        ];
    }

    public function action()
    {
        return Json::encode($this->text);
    }

}
