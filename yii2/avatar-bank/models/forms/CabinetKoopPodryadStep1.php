<?php

namespace avatar\models\forms;


use common\models\school\File;
use yii\db\ActiveRecord;

/**
 *
 * @property int            id
 * @property string         agent
 * @property string         dogovor_podryada
 *
 */
class CabinetKoopPodryadStep1 extends \iAvatar777\services\FormAjax\ActiveRecord
{

    public static function tableName()
    {
        return 'koop_podryad';
    }

    public function rules()
    {
        return [
            ['school_id', 'integer'],

            ['agent', 'required'],
            ['agent', 'string'],

            ['dogovor_podryada', 'required'],
            ['dogovor_podryada', 'string'],
        ];
    }

    public function attributeWidgets()
    {
        return [
            'dogovor_podryada' => [
                'class'    => '\iAvatar777\widgets\FileUpload8\FileUpload',
                'update'   => \avatar\controllers\CabinetSchoolPagesConstructorController::getUpdate(),
                'settings' => \avatar\controllers\CabinetSchoolPagesConstructorController::getSettingsLibrary($this->school_id, 27),
                'events'   => [
                    'onDelete' => function ($item) {
                        $r = new \cs\services\Url($item->image);
                        $d = pathinfo($r->path);
                        $start = $d['dirname'] . '/' . $d['filename'];

                        File::deleteAll(['like', 'file', $start]);
                    },
                ],
            ],
        ];
    }

    public function behaviors()
    {
        return [
            [
                'class'      => '\yii\behaviors\TimestampBehavior',
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at'],
                ],
                // если вместо метки времени UNIX используется datetime:
                // 'value' => new Expression('NOW()'),
            ],
        ];
    }
}