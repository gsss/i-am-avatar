<?php
namespace avatar\models\forms;

use common\models\UserAvatar;
use cs\base\FormActiveRecord;
use cs\services\VarDumper;
use Yii;
use yii\base\Model;
use yii\base\NotSupportedException;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\web\IdentityInterface;

/**
 * GoogleTest2 model
 *
 */
class GoogleTest2 extends Model
{
    public $code;

    public function rules()
    {
        return [
            ['code', 'required'],
            ['code', 'string', 'length' => 6],
            ['code', 'validateCode'],
        ];
    }

    public function validateCode($attribute, $params)
    {
        if (!$this->hasErrors()) {
            /** @var \common\models\UserAvatar $user */
            $user = Yii::$app->user->identity;

            if (!$user->validateGoogleAuthCode($this->code)) {
                $this->addError($attribute, 'Не верный код');
            }
        }
    }
}
