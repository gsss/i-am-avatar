<?php

namespace avatar\models\forms;

use common\components\providers\ETH;
use common\models\avatar\UserBill;
use common\models\SendLetter;
use common\models\UserSeed;
use common\services\Security;
use cs\Application;
use cs\services\Str;
use Yii;
use yii\base\Model;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;
use yii\helpers\VarDumper;

/**
 */
class CommandIsTask extends Model
{
    public $is_only_my;
}
