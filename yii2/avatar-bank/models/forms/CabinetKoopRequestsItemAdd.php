<?php

namespace avatar\models\forms;

use common\models\avatar\UserBill;
use common\models\koop\Kooperative;
use common\models\RequestTokenCreate;
use common\models\SendLetter;
use common\models\UserSeed;
use common\services\Security;
use cs\Application;
use cs\services\Str;
use Yii;
use yii\base\Model;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;
use yii\helpers\VarDumper;

/**
 */
class CabinetKoopRequestsItemAdd extends Model
{
    /** @var  string */
    public $password;

    /** @var  float */
    public $amount;


    /** @var  \common\models\koop\RequestPayIn заявка на внесение пая */
    public $request;

    /** @var  \common\models\Token */
    public $token;

    /** @var  \common\models\avatar\Currency */
    public $currency;

    /** @var  string ответ от add */
    private $response;

    public function init()
    {
        $product = $this->request->getProduct();
        $this->amount = $product->price * $product->count;
    }

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            ['amount', 'required'],
            ['amount', 'double', 'min' => 0],

            ['password', 'required'],
            ['password', 'validatePassword'],

            ['amount', 'validateRequest'], // устанавливает Token
            ['amount', 'tryToAction'],
        ];
    }

    /**
     * @return array customized attribute labels
     */
    public function attributeLabels()
    {
        return [
            'password' => 'Пароль',
        ];
    }

    /**
     * Конвертирует массив ошибок от формы чтобы их можно было выдать через JSON
     *
     * @param array $params
     *      [
     *      'amount' => ['Не верное число',...],
     *      ]
     *
     * @return array
     * [
     *      'name'  => 'amount',
     *      'value' => ['Не верное число',...],
     * ]
     */
    public function convert($params)
    {
        $ret = [];
        foreach ($params as $name => $arr) {
            $ret[] = [
                'name'  => $name,
                'value' => $arr,
            ];
        }
        return $ret;
    }

    public function validatePassword($attribute, $params)
    {
        if (!$this->hasErrors()) {
            /** @var \common\models\UserAvatar $user */
            $user = Yii::$app->user->identity;
            if (!$user->validatePassword($this->password)) {
                $this->addError($attribute, 'Пароль не верный');
            }
        }
    }

    public function validateRequest($attribute, $params)
    {
        if (!$this->hasErrors()) {
            $request = $this->request;
            if (is_null($request)) {
                $this->addError($attribute, 'Нет такой заявки');
                return;
            }
            $company = $request->getProduct()->getCompany();
            if ($request->getProduct()->getCompany()->user_id != \Yii::$app->user->id) {
                $this->addError($attribute, 'Нет такой заявки');
                return;
            }
            $koop = Kooperative::findOne($request->koop_id);
            $currency = \common\models\avatar\Currency::findOne($koop->currency_id);
            $token = $currency->getToken();
            $this->token = $token;
            $this->currency = $currency;
        }
    }

    public function tryToAction($attribute, $params)
    {
        if (!$this->hasErrors()) {
            /** @var \common\models\UserAvatar $user */
            $user = Yii::$app->user->identity;

            // работаю с большими целыми числами
            $pow = bcpow(10, $this->currency->decimals);
            $all = bcmul($this->amount, $pow);

            try {
                $data = $user->getWalletEthPassport()->contract(
                    $this->password,
                    $this->token->address,
                    $this->token->abi,
                    'add',
                    [
                        $all,
                    ]
                );
            } catch (\Exception $e) {
                $this->addError($attribute, $e->getMessage());
                return;
            }

            $this->response = $data;
        }
    }

    /**
     * @return string идентификатор транзакции
     */
    public function action()
    {
        $product = $this->request->getProduct();
        $product->koop_id = $this->request->koop_id;
        $product->save();

        $this->request->is_done = 1;
        $this->request->save();

        return $this->response;
    }
}
