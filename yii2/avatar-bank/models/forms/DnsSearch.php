<?php
namespace avatar\models\forms;

use cs\base\FormActiveRecord;
use Yii;
use yii\base\Model;
use yii\base\NotSupportedException;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;
use yii\web\IdentityInterface;

/**
 */
class DnsSearch extends Model
{
    public $name;

    public function rules()
    {
        return [
            ['name', 'search'],
        ];
    }


    public function validateNameExist($attribute, $params)
    {
        if (!$this->hasErrors()) {
            if (self::find()->where(['name' => $this->name, 'is_dns_verified' => [0, 1]])->exists()) {
                $this->addError($attribute, 'Этот домен уже занят');
                return;
            }
        }
    }

    public function action()
    {

    }

}
