<?php
namespace avatar\models\forms;

use common\components\providers\ETH;
use cs\base\FormActiveRecord;
use Yii;
use yii\base\Model;
use yii\base\NotSupportedException;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\web\IdentityInterface;

/**
 */
class ContractRegistration extends Model
{
    public $code;
    public $name;
    public $password;

    public function rules()
    {
        return [
            [[
                'code',
            ], 'safe'],
            ['name', 'required'],
            ['code', 'required'],
            ['password', 'required'],
            ['password', 'validatePassword'],
        ];
    }


    /**
     * Validates the password.
     * This method serves as the inline validation for password.
     *
     * @param string $attribute the attribute currently being validated
     * @param array $params the additional name-value pairs given in the rule
     */
    public function validatePassword($attribute, $params)
    {
        if (!$this->hasErrors()) {
            /** @var \common\models\UserAvatar $user */
            $user = Yii::$app->user->identity;
            $passportWalletEth = $user->getWalletEthPassport();

            if ($passportWalletEth->billing->validatePassword($this->password) == false) {
                $this->addError($attribute, 'Не верный пароль');
            }
        }
    }

    public function action()
    {
        /** @var \common\models\UserAvatar $user */
        $user = Yii::$app->user->identity;
        $passportWalletEth = $user->getWalletEthPassport();
        $txid = $passportWalletEth->registerContract(
            $this->password,
            $this->code,
            $this->name
        );

        return $txid;
    }
}
