<?php

namespace avatar\models\forms;

use app\models\User;
use avatar\models\UserRecover;
use common\models\avatar\UserBill;
use common\models\UserAvatar;
use common\models\UserRoot;
use common\models\UserSeed;
use cs\web\Exception;
use Yii;
use yii\base\Model;
use yii\db\Query;
use yii\helpers\Url;

/**
 *
 */
class PasswordNewRecover extends Model
{
    public $password1;
    public $password2;
    public $code;

    public $seeds;

    /** @var  \common\models\UserAvatar */
    public $_user;

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            [
                [
                    'password1',
                    'password2'
                ],
                'required',
                'message' => 'Это поле должно быть заполнено обязательно'
            ],
            ['password2', 'compare', 'compareAttribute' => 'password1', 'operator' => '==', 'message' => 'Пароли должны совпадать'],
            ['code', 'string'],
            ['code', 'validateCode'], // устанавливает $_user
        ];
    }

    public function validateCode($attribute, $params)
    {
        if (!$this->hasErrors()) {
            $row = UserRecover::findOne(['code' => $this->code]);
            if (is_null($row)) {
                $this->addError($attribute, 'Не верный code');
                return;
            }
            try {
                $user = UserAvatar::findOne($row->parent_id);
            } catch (\Exception $e) {
                $this->addError($attribute, 'Не найден пользователь');
                return;
            }
            $this->_user = $user;
        }
    }

    /**
     * Устанавливает
     *
     * @param  \common\models\UserAvatar $user
     *
     * @return boolean
     *
     * @throws
     */
    public function action($user)
    {
        $user->setPassword($this->password1);

        $userRoot = UserRoot::findOne($user->user_root_id);
        $userRoot->avatar_status = 2;
        $userRoot->save();

        // делаю update для ранее зарегистрированых пользователей
        (new Query())->createCommand()->update(\common\models\school\PotokUser3Link::tableName(), ['is_avatar' => 1], ['user_root_id' => $userRoot->id])->execute();

        return true;
    }
}
