<?php

namespace avatar\models\forms;

use app\models\SiteContentInterface;
use app\models\SiteUpdateItem;
use app\models\SubscribeItem;
use common\models\avatar\UserBill;
use common\payment\BitCoinBlockTrailPayment;
use cs\base\FormActiveRecord;
use cs\services\BitMask;
use yii\base\Exception;
use yii\db\ActiveRecord;
use yii\db\Query;
use app\services\Subscribe;
use yii\helpers\VarDumper;

/**
 * @property int    id
 * @property string is_sms_phone
 * @property string is_email_email
 * @property string is_http_address
 * @property string is_http_config
 * @property string code
 * @property string name
 * @property string secret
 * @property int    is_sms
 * @property int    is_email
 * @property int    is_http
 * @property int    confirmations кол-во подтверждений
 *
 */
class UserBillMerchant extends FormActiveRecord
{
    public static function tableName()
    {
        return 'bill_merchant';
    }

    public function formAttributes()
    {
        return [
            [
                'confirmations',
                'Кол-во подтверждений',
                0,
                'integer',
                ['min' => 0, 'max' => 6],
                'Кол-во подтверждений в Биткойн сети для транзакции после которых отсылаются уведомления, 0 - не подтверждена, 1 - записана в блок (подтверждена)',
            ],
            [
                'is_sms',
                'СМС?',
                0,
                '\common\widgets\CheckBox2\Validator',
                [],
                'widget' => [
                    '\common\widgets\CheckBox2\CheckBox'
                ],
            ],
            [
                'image_top',
                'Картинка верхняя',
                0,
                '\common\widgets\FileUpload3\Validator',
                [],
                '1600*500 px',
                'widget' => [
                    '\common\widgets\FileUpload3\FileUpload',
                    [
                        'options' => [
                            'small' => [
                                1600,
                                500,
                                \common\widgets\FileUpload3\FileUpload::MODE_THUMBNAIL_CUT
                            ]
                        ]
                    ]
                ],
            ],
            [
                'is_sms_phone',
                'Телефон',
                0,
                'string',
                [],
                'На этот телефон будет отправляться уведомление об оплате'
            ],
            [
                'secret',
                'Секретный код',
                0,
                'string',
                [],
                'Этот код будет посылаться нашим мерчантом как ключ для вашей "входной точки" приема уведомлений об оплаченных заказах.'
            ],
            [
                'code',
                'KEY',
                0,
                'string',
            ],
            [
                'name',
                'Заголовок',
                1,
                'string',
            ],
            [
                'is_email',
                'Отсылать уведомление на Email?',
                0,
                '\common\widgets\CheckBox2\Validator',
                [],
                'widget' => [
                    '\common\widgets\CheckBox2\CheckBox'
                ],
            ],
            [
                'is_email_email',
                'Email адрес',
                0,
                'string',
                [],
                'На этот адрес будет отправляться уведомление об оплате'
            ],
            [
                'is_http',
                'Отсылать уведомление на HTTP?',
                0,
                '\common\widgets\CheckBox2\Validator',
                [],
                'widget' => [
                    '\common\widgets\CheckBox2\CheckBox'
                ],
            ],
            [
                'is_http_address',
                'Адрес Callback',
                0,
                'url',
                [],
                'На этот адрес будет отправляться уведомление об оплате'
            ],
        ];
    }

    /**
     * @return \common\models\avatar\UserBill
     *
     */
    public function getBilling()
    {
        return UserBill::findOne($this->id);
    }
}