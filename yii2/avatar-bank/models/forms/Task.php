<?php


namespace avatar\models\forms;


use iAvatar777\services\FormAjax\ActiveRecord;

/**
 * @property int    id
 * @property int    school_id
 * @property int    category_id
 * @property int    parent_id
 * @property int    user_id
 * @property int    executer_id
 * @property int    created_at
 * @property int    updated_at
 * @property int    status
 * @property int    price
 * @property int    currency_id         dbWallet.currency.id
 * @property int    is_hide
 * @property int    sort_index
 * @property int    wallet_type3_user_id
 * @property int    wallet_type         1 - Брать из кошелька школы, 2 - Брать с кошелька постановщика
 * @property int    last_comment_time   Мгновение когда был добавлен новый коментарий
 * @property string name
 * @property string content
 *
 * Class Task
 *
 * @package avatar\models\forms
 */
class Task extends ActiveRecord
{
    public static function tableName()
    {
        return 'school_task';
    }

    public function rules()
    {
        return [
            ['wallet_type', 'integer'],

            ['school_id', 'integer'],

            ['is_hide', 'integer'],
            ['wallet_type3_user_id', 'integer'],
            ['user_id', 'integer'],
            ['executer_id', 'integer'],
            ['created_at', 'integer'],
            ['updated_at', 'integer'],
            ['status', 'integer'],
            ['price', 'integer'],
            ['currency_id', 'integer'],
            ['category_id', 'integer'],
            ['last_comment_time', 'integer'],
            ['name', 'string'],
            ['content', 'string'],
        ];
    }
}