<?php

namespace avatar\models\forms;

use app\models\User;
use common\components\Card;
use common\models\avatar\UserBill;
use common\models\avatar\UserBillDefault;
use common\models\UserAvatar;
use common\models\UserPassword;
use common\models\UserRegistration;
use common\models\UserRoot;
use cs\Application;
use cs\web\Exception;
use Endroid\QrCode\QrCode;
use Yii;
use yii\base\Model;
use yii\db\Query;
use yii\helpers\Url;

/**
 *
 */
class QrEnter extends Model
{
    /** @var string email */
    public $email;

    /** @var string пароль1 */
    public $password1;

    /** @var string пароль2 */
    public $password2;

    /** @var string номер карты */
    public $code;

    /** @var  \common\models\Card */
    public $card;

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        if (Yii::$app->user->isGuest) {
            return [
                ['email', 'required', 'message' => Yii::t('c.RGQ3VcUnI0', 'Это поле должно быть заполнено обязательно')],
                ['email', 'trim'],
                ['email', 'email', 'checkDNS' => true, 'message' => Yii::t('c.RGQ3VcUnI0', 'Email должен быть верным')],
                ['email', 'unique', 'targetAttribute' => 'email', 'targetClass' => '\common\models\UserAvatar', 'message' => 'Вы уже зарегистрированы в системе и вам надо внутри кабинете активировать карту'],

                ['password1', 'string'],
                ['password2', 'compare', 'compareAttribute' => 'password1', 'operator' => '==', 'message' => Yii::t('c.0uMdJb0e0n', 'Пароли должны совпадать')],

                ['code', 'required', 'message' => Yii::t('c.RGQ3VcUnI0', 'Это поле должно быть заполнено обязательно')],
                ['code', 'trim'],
                ['code', 'string', 'max' => 20, 'min' => 16],
                ['code', 'normalize'],
                ['code', 'validateCardNumber'],
                ['code', 'joinBilling'],
            ];

        } else {
            return [
                ['code', 'required', 'message' => Yii::t('c.RGQ3VcUnI0', 'Это поле должно быть заполнено обязательно')],
                ['code', 'trim'],
                ['code', 'string', 'max' => 20, 'min' => 16],
                ['code', 'normalize'],
                ['code', 'validateCardNumber'],
                ['code', 'joinBilling'],
            ];

        }
    }

    public function normalize($attribute, $params)
    {
        if (!$this->hasErrors()) {
            $this->code = str_replace(' ', '', $this->code);
        }
    }

    public function comparePassword($attribute, $params)
    {
        if (!$this->hasErrors()) {
            if ($this->password1 != $this->password2) {
                $this->addError($attribute, Yii::t('c.0uMdJb0e0n', 'Пароли должны совпадать'));
            }
        }
    }

    /**
     * Проверяет номер карты на валидность
     * Как входное значене уже приходит номер без пробелов
     *
     * @param $attribute
     * @param $params
     */
    public function validateCardNumber($attribute, $params)
    {
        if (!$this->hasErrors()) {
            $card_number = $this->code;
            if (strlen($card_number) != 16) {
                $this->addError($attribute, Yii::t('c.RGQ3VcUnI0', 'В номере карты должно быть 16 цифр'));
                return;
            }
            if (preg_match('/\D/', $card_number) != 0) {
                $this->addError($attribute, Yii::t('c.RGQ3VcUnI0', 'В номере карты должны быть только цифры'));
                return;
            }
            if (!Card::checkSum($card_number)) {
                $this->addError($attribute, Yii::t('c.RGQ3VcUnI0', 'Контрольная сумма в номере карты не совпадает'));
                return;
            }
        }
    }

    public function joinBilling($attribute, $params)
    {
        if (!$this->hasErrors()) {
            $card = \common\models\Card::findOne(['number' => $this->code]);
            if (is_null($card)) {
                $this->addError($attribute, Yii::t('c.RGQ3VcUnI0', 'Карта не найдена'));
                return;
            }
            $this->card = $card;

            // проверка: может карта уже присоединена комуто?
            if (!empty($card->user_id)) {
                $this->addError($attribute, 'Карта уже принадлежит кому-то');
                return;
            }
        }
    }

    /**
     * Активирует карту, отправляет письмо
     *
     */
    public function activate()
    {
        if (!Yii::$app->user->isGuest) {
            $this->card->user_id = Yii::$app->user->id;
            $this->card->save();
        } else {
            // Создаю пользователя если надо
            $userRoot = UserRoot::findOne(['email' => $this->email]);
            if (is_null($userRoot)) {
                $userRoot = UserRoot::add(['email' => $this->email]);
            }

            $hash = \cs\services\Security::generateRandomString(32);
            \common\services\Subscribe::sendArray([$this->email], 'Уведомление о подтверждении регистрации и активации карты', 'registration-activate-card', [
                'url' => Url::to(['auth/registration-activate-card', 'hash' => $hash], true),
            ]);

            \common\models\UserRegistrationCard::deleteAll(['user_root_id' => $userRoot->id]);
            \common\models\UserRegistrationCard::add([
                'password_hash' => \common\models\UserAvatar::hashPassword($this->password1),
                'user_root_id'  => $userRoot->id,
                'hash'          => $hash,
                'card'          => $this->code,
            ]);
        }


        return true;
    }
}
