<?php

namespace avatar\models\forms;

use common\models\avatar\UserBill;
use common\models\koop\Kooperative;
use common\models\RequestTokenCreate;
use common\models\SendLetter;
use common\models\UserSeed;
use common\services\Security;
use cs\Application;
use cs\services\Str;
use iAvatar777\services\FormAjax\ActiveRecord;
use Yii;
use yii\base\Model;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;
use yii\helpers\VarDumper;

/**
 */
class CabinetKoopAimAdd extends ActiveRecord
{
    public static function tableName()
    {
        return 'school_koop_program';
    }

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            ['school_id', 'required'],
            ['school_id', 'integer'],

            ['name', 'required'],
            ['name', 'string'],

            ['link', 'required'],
            ['link', 'string'],

            ['document', 'required'],
            ['document', 'string'],
        ];
    }

    public function attributeWidgets()
    {
        return [
            'document'      => [
                'class'    => '\iAvatar777\widgets\FileUpload8\FileUpload',
                'update'   => \avatar\controllers\CabinetSchoolPagesConstructorController::getUpdate(),
                'settings' => \avatar\controllers\CabinetSchoolPagesConstructorController::getSettingsLibrary($this->school_id, 21),
            ],
        ];
    }

    public function attributeLabels()
    {
        return [
            'name'     => 'Наименование',
            'link'     => 'Ссылка',
            'document' => 'Документ',
        ];
    }
}
