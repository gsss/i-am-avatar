<?php
namespace avatar\models\forms;

use cs\base\FormActiveRecord;
use cs\services\Str;
use Yii;
use yii\base\Model;
use yii\base\NotSupportedException;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\helpers\StringHelper;
use yii\web\IdentityInterface;

/**
 */
class Ico extends Model
{
    /** @var  int */
    public $icoId;

    /** @var  \common\models\investment\Project */
    public $ico;

    /** @var  float */
    public $tokens;

    /** @var  string */
    public $email;

    /** @var  string */
    public $address;


    public function rules()
    {
        return [
            ['icoId', 'required'],
            ['icoId', 'validateIco'],

            ['tokens', 'required'],
            ['tokens', 'double'],
            ['tokens', 'compare', 'operator' => '>', 'type' => 'number', 'compareValue' => 0],
            ['tokens', 'validateTokensMax'], // Проверить на максимум (сколько осталось)

            ['email', 'validateRequired', 'skipOnEmpty' => false],
            ['email', 'email'],
            ['email', 'toLower'],
            ['email', 'validateEmail'],

            ['address', 'string'],
            ['address', 'validateAddress'],
        ];
    }

    /**
     * Конвертирует массив ошибок от формы чтобы их можно было выдать через JSON
     *
     * @param array $params
     *      [
     *      'amount' => ['Не верное число',...],
     *      ]
     *
     * @return array
     * [
     *      'name'  => 'amount',
     *      'value' => ['Не верное число',...],
     * ]
     */
    public function convert($params)
    {
        $ret = [];
        foreach ($params as $name => $arr) {
            $ret[] = [
                'name'  => $name,
                'value' => $arr,
            ];
        }
        return $ret;
    }

    public function validateAddress($attribute, $params)
    {
        if (!$this->hasErrors()) {
            if (!StringHelper::startsWith($this->address, '0x')) {
                $this->addError($attribute, 'Адрес должен начинаться с 0x');
                return;
            }
            if (!$this->validateAddressHex($this->address)) {
                $this->addError($attribute, 'Адрес должен содержать только шестнадцатеричеые символы');
                return;
            }

        }
    }


    /**
     * Проверка адреса на валидные символы
     *
     * @param string $address
     * @return bool
     */
    private function validateAddressHex($address)
    {
        $address = strtolower($address);
        $address = substr($address, 2);
        $symbols = Str::getChars($address);
        foreach ($symbols as $char) {
            if (!in_array($char, ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f'])) return false;
        }
        return true;
    }

    /**
     * Если пользователь не аторизован то поле проверяется на обязательность
     * Если пользователь авторизован то поле не проверяется
     *
     * @param $attribute
     * @param $params
     */
    public function validateRequired($attribute, $params)
    {
        if (!$this->hasErrors()) {
            if (Yii::$app->user->isGuest) {
                if (is_null($this->email)) {
                    $this->addError($attribute, 'Этот элемент обязательный');
                    return;
                } else {
                    if ($this->email == '') {
                        $this->addError($attribute, 'Этот элемент обязательный');
                        return;
                    }
                }
            } else {
                // ничего не делать
                return;
            }
        }
    }

    public function validateIco($attribute, $params)
    {
        if (!$this->hasErrors()) {
            $project = \common\models\investment\Project::findOne($this->icoId);
            if (is_null($project)) {
                $this->addError($attribute, 'Такой проект не найден');
                return;
            }

            $this->ico = $project;
        }
    }

    public function validateTokensMax($attribute, $params)
    {
        if (!$this->hasErrors()) {
            if ($this->tokens > $this->ico->getIco()->remained_tokens) {
                $this->addError($attribute, 'Недостаточно токенов, осталось всего ' . $this->ico->getIco()->remained_tokens);
                return;
            }
        }
    }

    public function validateEmail($attribute, $params)
    {
        if (!$this->hasErrors()) {
            if (\common\models\UserAvatar::find()->where(['email' => $this->email])->exists()) {
                $this->addError($attribute, 'Такой пользователь уже существует, вам нужно авторизоваться');
            }
        }
    }

    public function toLower($attribute, $params)
    {
        if (!$this->hasErrors()) {
            $this->email = strtolower($this->email);
        }
    }
}
