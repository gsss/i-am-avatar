<?php

namespace app\models\Shop;

use app\models\Piramida\Billing;
use app\models\Piramida\ReferalBonus;
use app\models\Piramida\Transaction;
use app\models\Shop;
use app\models\Union;
use app\models\User;
use common\models\shop\DostavkaItem;
use cs\services\VarDumper;
use cs\Widget\FileUploadMany\FileUploadMany;
use yii\db\Exception;
use yii\db\Query;
use yii\helpers\ArrayHelper;

class Request extends \cs\base\DbRecord
{
    const TABLE = 'gs_users_shop_requests';

    const STATUS_USER_NOT_CONFIRMED = 1; // Пользователь заказал с регистрацией пользователя, но не подтвердил свою почту еще

    const STATUS_SEND_TO_SHOP = 2;       // заказ отправлен в магазин
    const STATUS_ORDER_DOSTAVKA = 3;     // клиенту выставлен счет с учетом доставки
    const STATUS_PAID_CLIENT = 5;        // заказ оплачен со стороны клиента
    const STATUS_PAID_SHOP = 6;          // оплата подтверждена магазином
    const STATUS_REJECTED = 17;          // Заказ отменен

    // статусы для доставки
    const STATUS_DOSTAVKA_ADDRESS_PREPARE = 8;            // заказ формируется для отправки клиенту
    const STATUS_DOSTAVKA_ADDRESS_PREPARE_DONE = 9;       // заказ сформирован для отправки клиенту
    const STATUS_DOSTAVKA_ADDRESS_SEND_TO_USER = 7;       // заказ отправлен клиенту
    const STATUS_DOSTAVKA_ADDRESS_FINISH_CLIENT = 10;     // заказ исполнен, как сообщил клиент
    const STATUS_DOSTAVKA_ADDRESS_FINISH_SHOP = 11;       // заказ исполнен, магазин сам указал этот статус по своим данным

    // статусы для самовывоза
    const STATUS_DOSTAVKA_SAMOVIVOZ_WAIT = 13;             // заказ ожидает клиента
    const STATUS_DOSTAVKA_SAMOVIVOZ_FINISH_CLIENT = 14;    // клиент сообщил что получил товар
    const STATUS_DOSTAVKA_SAMOVIVOZ_FINISH_SHOP = 15;      // заказ исполнен, магазин сам указал этот статус по своим данным

    // статусы для электронного товара
    const STATUS_DOSTAVKA_ELECTRON_DONE = 16;              // заказ принимает этот статус если в заказе все товары электронные, после факта оплаты заказа

    // статусы для наложного платежа
    const STATUS_DOSTAVKA_NALOZH_PREPARE = 21;            // заказ формируется для отправки клиенту
    const STATUS_DOSTAVKA_NALOZH_PREPARE_DONE = 22;       // заказ сформирован для отправки клиенту
    const STATUS_DOSTAVKA_NALOZH_SEND_TO_USER = 23;       // заказ отправлен клиенту
    const STATUS_DOSTAVKA_NALOZH_PIAD = 24;               // заказ исполнен, деньги были получены
    const STATUS_DOSTAVKA_NALOZH_RETURN = 25;             // заказ вернулся, отменен

    const DIRECTION_TO_CLIENT = 1;
    const DIRECTION_TO_SHOP = 2;

    private $_sumPiramida;

    public static $statusList = [
        self::STATUS_DOSTAVKA_NALOZH_PREPARE               => [
            'client'   => 'заказ формируется для отправки клиенту',
            'shop'     => 'заказ формируется для отправки клиенту',
            'timeLine' => [
                'icon'  => 'glyphicon-plane',
                'color' => 'default',
            ],
        ],
        self::STATUS_DOSTAVKA_NALOZH_PREPARE_DONE               => [
            'client'   => 'заказ сформирован для отправки клиенту',
            'shop'     => 'заказ сформирован для отправки клиенту',
            'timeLine' => [
                'icon'  => 'glyphicon-plane',
                'color' => 'warning',
            ],
        ],
        self::STATUS_DOSTAVKA_NALOZH_SEND_TO_USER               => [
            'client'   => 'заказ отправлен клиенту',
            'shop'     => 'заказ отправлен клиенту',
            'timeLine' => [
                'icon'  => 'glyphicon-plane',
                'color' => 'success',
            ],
        ],
        self::STATUS_DOSTAVKA_NALOZH_PIAD               => [
            'client'   => 'заказ исполнен, деньги были получены',
            'shop'     => 'заказ исполнен, деньги были получены',
            'timeLine' => [
                'icon'  => 'glyphicon-thumbs-up',
                'color' => 'success',
            ],
        ],
        self::STATUS_DOSTAVKA_NALOZH_RETURN               => [
            'client'   => 'заказ вернулся, отменен',
            'shop'     => 'заказ вернулся, отменен',
            'timeLine' => [
                'icon'  => 'glyphicon-remove',
                'color' => 'danger',
            ],
        ],


        self::STATUS_REJECTED               => [
            'client'   => 'Заказ отменен',
            'shop'     => 'Пользователь отменил заказ',
            'timeLine' => [
                'icon'  => 'glyphicon-remove',
                'color' => 'danger',
            ],
        ],
        self::STATUS_USER_NOT_CONFIRMED               => [
            'client'   => 'Пользователь не подтвердил почту',
            'shop'     => 'Пользователь не подтвердил почту',
            'timeLine' => [
                'icon'  => 'glyphicon-minus',
                'color' => 'default',
            ],
        ],
        self::STATUS_SEND_TO_SHOP                     => [
            'client'   => 'Отпрален в магазин',
            'shop'     => 'Пользователь отправил заказ',
            'timeLine' => [
                'icon'  => 'glyphicon-ok',
                'color' => 'default',
            ],
        ],
        self::STATUS_ORDER_DOSTAVKA                   => [
            'client'   => 'Выставлен счет с учетом доставки',
            'shop'     => 'Выставлен счет с учетом доставки',
            'timeLine' => [
                'icon'  => 'glyphicon-credit-card',
                'color' => 'default',
            ],
        ],
        self::STATUS_PAID_CLIENT                      => [
            'client'   => 'Оплата сделана',
            'shop'     => 'Оплата сделана',
            'timeLine' => [
                'icon'  => 'glyphicon-credit-card',
                'color' => 'warning',
            ],
        ],
        self::STATUS_PAID_SHOP                        => [
            'client'   => 'Оплата подтверждена',
            'shop'     => 'Оплата подтверждена',
            'timeLine' => [
                'icon'  => 'glyphicon-credit-card',
                'color' => 'success',
            ],
        ],
        self::STATUS_DOSTAVKA_ADDRESS_PREPARE         => [
            'client'   => 'Заказ формируется для отправки клиенту',
            'shop'     => 'Заказ формируется для отправки клиенту',
            'timeLine' => [
                'icon'  => 'glyphicon-plane',
                'color' => 'default',
            ],
        ],


        self::STATUS_DOSTAVKA_ADDRESS_PREPARE_DONE    => [
            'client'   => 'Заказ сформирован для отправки клиенту',
            'shop'     => 'Заказ сформирован для отправки клиенту',
            'timeLine' => [
                'icon'  => 'glyphicon-plane',
                'color' => 'warning',
            ],
        ],
        self::STATUS_DOSTAVKA_ADDRESS_SEND_TO_USER    => [
            'client'   => 'Заказ отправлен клиенту',
            'shop'     => 'Заказ отправлен клиенту',
            'timeLine' => [
                'icon'  => 'glyphicon-plane',
                'color' => 'success',
            ],
        ],
        self::STATUS_DOSTAVKA_ADDRESS_FINISH_CLIENT   => [
            'client'   => 'Заказ исполнен, как сообщил клиент',
            'shop'     => 'Заказ исполнен, как сообщил клиент',
            'timeLine' => [
                'icon'  => 'glyphicon-thumbs-up',
                'color' => 'success',
            ],
        ],
        self::STATUS_DOSTAVKA_ADDRESS_FINISH_SHOP     => [
            'client'   => 'Заказ выполнен',
            'shop'     => 'Заказ выполнен',
            'timeLine' => [
                'icon'  => 'glyphicon-thumbs-up',
                'color' => 'success',
            ],
        ],
        self::STATUS_DOSTAVKA_SAMOVIVOZ_WAIT          => [
            'client'   => 'Заказ ожидает клиента',
            'shop'     => 'Заказ ожидает клиента',
            'timeLine' => [
                'icon'  => 'glyphicon-plane',
                'color' => 'warning',
            ],
        ],
        self::STATUS_DOSTAVKA_SAMOVIVOZ_FINISH_CLIENT => [
            'client'   => 'Клиент сообщил что получил товар',
            'shop'     => 'Клиент сообщил что получил товар',
            'timeLine' => [
                'icon'  => 'glyphicon-thumbs-up',
                'color' => 'success',
            ],
        ],
        self::STATUS_DOSTAVKA_SAMOVIVOZ_FINISH_SHOP   => [
            'client'   => 'Заказ исполнен',
            'shop'     => 'Заказ исполнен',
            'timeLine' => [
                'icon'  => 'glyphicon-thumbs-up',
                'color' => 'success',
            ],
        ],
        self::STATUS_DOSTAVKA_ELECTRON_DONE           => [
            'client'   => 'Заказ отправлен по почте',
            'shop'     => 'Заказ отправлен по почте',
            'timeLine' => [
                'icon'  => 'glyphicon-thumbs-up',
                'color' => 'success',
            ],
        ],
    ];

    /**
     * Отвечает на вопрос "Заказ содержит только электронные товары?"
     * @return bool
     * true - да все товары электроннеы
     * true - один из товаром может быть не электронным
     */
    public function onlyElectronic()
    {
        return $this->getProductList()->andWhere(['gs_unions_shop_product.is_electron' => 1])->exists();
    }


    /**
     * @return \app\models\Piramida\Billing
     * @throws \yii\base\Exception
     */
    public function getBilling()
    {
        $bid = $this->get('billing_id');
        if (is_null($bid)) return null;
        $b = Billing::findOne($bid);
        if (is_null($b)) return null;

        return $b;
    }

    /**
     * Заказ был исполнен
     */
    public function success()
    {
        \Yii::info('request.success action begin', 'gs\\order\\success::actionBegin');
        $shop = $this->getUnion()->getShop();

        $this->update(['is_paid' => 1]);

        // привязываю пригласителя к приглашенному (покупателю)
        if ($this->get('parent_id')) {
            $user = $this->getUser();
            if (is_null($user->get('parent_id'))) {
                $user->update(['parent_id' => $this->get('parent_id')]);
            }
        }
        // высылаю эелектронные товары если таковые есть
        $this->sendElectron();

        // делаю реферальные отчисления
        $this->sendReferal();

        // прибавляю счетчика покупок для товаров
        $this->incrementSalesCount();

        $shop->mailToMe('Оплата подтверждена по заказу #' . $this->getId(), 'shop/request_to_shop_answer_pay', [
            'request' => $this,
            'text'    => '',
        ]);

        return true;
    }

    public function beforeSuccess()
    {
        // Ставлю статусы о том что платеж был совершен клиентом, если такого статуса не установлено
        $this->addPaidClient();
        $this->addStatusToClient(self::STATUS_PAID_SHOP); // магазин подтвердил оплату
        if (!$this->onlyElectronic()) {
            $dostavka = $this->getDostavkaItem();
            if ($dostavka->getField('is_need_address', 0) == 1) {
                // если есть адрес
                $this->addStatusToClient(self::STATUS_DOSTAVKA_ADDRESS_PREPARE);
            } else {
                $this->addStatusToClient(self::STATUS_DOSTAVKA_SAMOVIVOZ_WAIT);
            }
        } else {
            $this->addStatusToClient(self::STATUS_DOSTAVKA_ELECTRON_DONE);
        }
    }

    /**
     * Устанавливает статус для заказа "Оплата подтверждена магазином"
     * Если в
     *
     * @param \app\models\Piramida\WalletSourceInterface $transaction
     *
     * @return bool
     */
    public function paid($transaction)
    {
        \Yii::info('request.paid action begin', 'gs\\order\\success::actionBegin');
        // создаю запись об переводе денег из внешней системы в магазин
        \Yii::info('$transaction='.\yii\helpers\VarDumper::dumpAsString($transaction), 'gs\\paid');
        $this->addTransactionPaidPS($transaction);

        // подтверждаю исполнения заказа
        return $this->success();
    }

    private function incrementSalesCount()
    {
        $productList = $this->getProductList()->select([
            'gs_unions_shop_product.id',
            'gs_users_shop_requests_products.count',
        ])->all();
        foreach($productList as $product) {
            $id = $product['id'];
            $count = $product['count'];
            (new Query())->createCommand()->update(Product::TABLE, ['sales_count' => new \yii\db\Expression('`sales_count` + ' . $count)], ['id' => $id])->execute();
        }
    }


    /**
     * Добавляет статус self::STATUS_PAID_CLIENT если его нет
     */
    private function addPaidClient()
    {
        $isExist = RequestMessage::query([
            'request_id' => $this->getId(),
            'status'     => self::STATUS_PAID_CLIENT,
        ])->exists();
        if (!$isExist) $this->addStatusToShop(self::STATUS_PAID_CLIENT); // клиент совершил оплату
    }

    /**
     * Возвращает статус с уровнями
     *
     * @return array
     * ['levels' => explode(',', $shop->piramida)]
     */
    protected function getMaxStatus()
    {
        $unionId = $this->get('union_id');
        $shop = \common\models\shop\Shop::findOne(['union_id' => $unionId]);
        $levels = explode(',', $shop->piramida);
        $rows = [];
        for($i = 1; $i <= count($levels); $i++) {
            $rows[$i] = $levels[$i-1];
        }

        return ['levels' => $rows];
    }

    /**
     * Делает реферальные начисления
     */
    protected function sendReferal()
    {
        $maxStatus  = $this->getMaxStatus();
        $maxLevel = count($maxStatus['levels']);
        $userRequest = $this->getUser();

        // сумма от заказа от которой надо распределить реферальные отчисления
        $sumPiramida = $this->getSumPiramida();
        if ($sumPiramida > 0) {
            $user = $userRequest;
            $requestTransaction = [];
            $referalBonus = [];

            for ($level = 1; $level <= $maxLevel; $level++) {
                $parent_id = $user->get('parent_id');
                if (is_null($parent_id)) break;
                $userParent = User::find($parent_id);
                $status = new \app\models\Piramida\Status($maxStatus);
                if (count($status->levels) >= $level) {
                    /** @var \app\models\Piramida\Wallet $piramidaWallet */
                    $piramidaWallet = \Yii::$app->piramida->getWallet();
                    $percent = $status->levels[$level];
                    $sum = $sumPiramida * ($percent/100);

                    // делаю перевод
                    $walletB = $userParent->getWallet('B');
                    $t = $piramidaWallet->move($walletB, $sum, 'Бонус от продажи опекуна (' . $userRequest->getEmail() . ') по заказу ' . $this->getId(), Transaction::OPERATION_BONUS);

                    // делаю запись в заказ что были сделаны отчисления
                    $requestTransaction[] = [
                        $this->getId(),
                        $t->id,
                        ($level == 1) ? Shop\RequestTransaction::TYPE_REFERAL1 : Shop\RequestTransaction::TYPE_REFERAL,
                        $sum
                    ];
                    $referalBonus[] = [
                        $userRequest->getId(),
                        $userParent->getId(),
                        $level,
                        $walletB->id,
                        $sum,
                        $percent,
                        $this->getId(),
                        $t->id,
                        microtime(true)
                    ];
                }
                $user = $userParent;
            }
            if (count($requestTransaction) > 0){
                Shop\RequestTransaction::batchInsert([
                    'request_id',
                    'transaction_id',
                    'type',
                    'sum',
                ], $requestTransaction);
                ReferalBonus::batchInsert([
                    'from',
                    'to',
                    'level',
                    'to_wallet',
                    'sum',
                    'percent',
                    'request_id',
                    'transaction_id',
                    'datetime',
                ], $referalBonus);
            }
        }
    }

    /**
     * Выдает сумму цены товаров входящих в пирамиду
     *
     * @return double
     */
    public function getSumPiramida()
    {
        if (is_null($this->_sumPiramida)) {
            $this->_sumPiramida = $this->_getSumPiramida();
        }

        return $this->_sumPiramida;
    }

    /**
     * Возвращает значение с которого будет происходить расчет реферальных отчислений
     *
     * @return float
     */
    private function _getSumPiramida()
    {
        $sum = $this->get('sum', 0);

        return $sum;
    }

    /**
     * Добавляет запись о том что была оплата заказа из платежной системы
     *
     * @param \app\models\Piramida\WalletSourceInterface $transaction
     *
     * @throws \Exception
     */
    public function addTransactionPaidPS($transaction)
    {
        /** @var \app\common\components\Piramida $piramida */
        $piramida = \Yii::$app->piramida;
        {
            $billing_id = $this->get('billing_id');
            $billing = Billing::findOne($billing_id);

            $sum = $billing->sum_after;
            // Добавляю транзакцию о том что была проплата из внешней системы
            $t = $piramida->getWallet()->in($sum, 'Оплата заказа ' . $this->getId(), Transaction::OPERATION_PAY);
            \Yii::info('$t=' . \yii\helpers\VarDumper::dumpAsString($t),'gs\\order\\addTransactionPaidPS');
            \Yii::info('$piramida->getWallet()='.\yii\helpers\VarDumper::dumpAsString($piramida->getWallet()),'gs\\order\\addTransactionPaidPS');
            // Добавляю запись о том какая транзакция была произведена для оплаты заказа
            Shop\RequestTransaction::add([
                'request_id'     => $this->getId(),
                'transaction_id' => $t->id,
                'type'           => Shop\RequestTransaction::TYPE_PAID_PAYSYSTEM,
                'sum'            => $billing->sum_after,
            ]);

            $billing->success($t, $transaction);
        }
    }

    /**
     * Высылает электронные товары по эл почте
     */
    protected function sendElectron()
    {
        // Отсылаю оплаченные заказы, если они электронные
        $shop = $this->getUnion()->getShop();
        $productList = $this->getProductList()->all();
        if ($productList) {
            foreach($productList as $product) {
                if ($product['is_electron'] == 1) {
                    /**
                     * @var array $files
                     * [[
                     *    'id'        => int
                     *    'file_path' => str
                     *    'file_name' => str
                     *    'datetime'  => str
                     * ], ...]
                     */
                    $files = FileUploadMany::getFiles(Product::TABLE, 'attached_files', $product['id']);
                    $attaches = [];
                    foreach($files as $file) {
                        $path = new \cs\services\SitePath($file['file_path']);
                        $attaches[$path->getPathFull()] = $file['file_name'];
                    }
                    $shop->mail($this->getUser()->get('email'), 'Ваш товар #' . $product['id'], 'shop/electron', [
                        'text' => $product['electron_text'],
                    ], $attaches);
                }
            }
        }

    }

    /**
     * Возвращает тип доставки текстом
     * @return string
     */
    public function getDostavkaText()
    {
        $item = $this->getDostavkaItem();
        if (is_null($item)) return '';

        return $item->getField('name');
    }

    /**
     * Возвращает тип доставки текстом
     * @return \app\models\Shop\DostavkaItem | null
     */
    public function getDostavkaItem()
    {
        return \app\models\Shop\DostavkaItem::find($this->getField('dostavka'));
    }

    /**
     * Возвращает тип доставки текстом
     *
     * @return \common\models\shop\DostavkaItem
     * @throws \yii\base\Exception
     */
    public function getDostavka()
    {
        $item = DostavkaItem::findOne($this->getField('dostavka'));
        if (is_null($item)) {
           throw new \yii\base\Exception('Не указана доставка');
        }

        return $item;
    }

    /**
     * @param array $fields
     * @return \app\models\Shop\Request
     */
    public static function insert($fields = [])
    {
        if (!isset($fields['user_id'])) {
            $fields['user_id'] = \Yii::$app->user->id;
        }
        $fields['date_create'] = time();

        return parent::insert($fields);
    }

    /**
     * Добавить статус
     *
     * @param int | array $status статус self::STATUS_* или массив со статусом и сообщением
     * @param int $direction направление сообщения self::DIRECTION_*
     *
     * @return \app\models\Shop\RequestMessage
     */
    public function addStatus($status, $direction)
    {
        if (!is_array($status)) {
            $fields = [
                'status' => $status,
            ];
        } else {
            $fields = $status;
        }

        return $this->addMessageItem($fields, $direction);
    }

    /**
     * Добавить статус к клиенту
     *
     * @param int | array $status статус self::STATUS_* или массив со статусом и сообщением
     *
     * @return \app\models\Shop\RequestMessage
     */
    public function addStatusToClient($status)
    {
        if (!is_array($status)) {
            $fields = [
                'status' => $status,
            ];
        } else {
            $fields = $status;
        }

        return $this->addMessageItem($fields, self::DIRECTION_TO_CLIENT);
    }

    /**
     * Добавить статус в магазин
     *
     * @param int | array $status статус self::STATUS_* или массив со статусом и сообщением
     *
     * @return \app\models\Shop\RequestMessage
     */
    public function addStatusToShop($status)
    {
        if (!is_array($status)) {
            $fields = [
                'status' => $status,
            ];
        } else {
            $fields = $status;
        }

        return $this->addMessageItem($fields, self::DIRECTION_TO_SHOP);
    }

    /**
     * Добавить сообщение
     *
     * @param string $message сообщение
     * @param int $direction направление сообщения self::DIRECTION_*
     *
     * @return \app\models\Shop\RequestMessage
     */
    public function addMessage($message, $direction)
    {
        return $this->addMessageItem([
            'message' => $message,
        ], $direction);
    }

    /**
     * Добавить сообщение для клиента
     *
     * @param string $message сообщение
     *
     * @return \app\models\Shop\RequestMessage
     */
    public function addMessageToClient($message)
    {
        return $this->addMessage($message, self::DIRECTION_TO_CLIENT);
    }

    /**
     * Добавить сообщение для клиента
     *
     * @param string $message сообщение
     *
     * @return \app\models\Shop\RequestMessage
     */
    public function addMessageToShop($message)
    {
        return $this->addMessage($message, self::DIRECTION_TO_SHOP);
    }

    /**
     * Добавить сообщение или статус
     *
     * @param array $fields поля для сообщения
     * @param int $direction направление сообщения self::DIRECTION_*
     *
     * @return \app\models\Shop\RequestMessage
     */
    public function addMessageItem($fields, $direction)
    {
        $fieldsRequest = [
            'is_answer_from_shop'   => ($direction == self::DIRECTION_TO_CLIENT) ? 1 : 0,
            'is_answer_from_client' => ($direction == self::DIRECTION_TO_CLIENT) ? 0 : 1,
            'last_message_time'     => time(),
        ];
        if (isset($fields['status'])) {
            $fieldsRequest['status'] = $fields['status'];
        }
        if ($direction == self::DIRECTION_TO_SHOP) {
            $fieldsRequest['is_hide'] = null;
        }
        $this->update($fieldsRequest);

        return RequestMessage::insert(ArrayHelper::merge($fields, [
            'request_id' => $this->getId(),
            'direction'  => $direction,
            'datetime'   => time(),
        ]));
    }

    /**
     * Добавляет заказ с продуктами
     *
     * @param $fields array поля заказа
     * - sum - float - сумма заказа без учета доставки
     * + price - float - сумма заказа с учетом доставки
     * - dostavka - int - тип доставки
     * - address - string - адрес доставки
     *
     * @param $productList array список продуктов в заказе
     * [
     *    [
     *       'id'     => int,
     *       'count'  => int,
     *    ], ...
     * ]
     * @return self
     */
    public static function add($fields, $productList, $status = self::STATUS_SEND_TO_SHOP)
    {
        if (!ArrayHelper::keyExists('sum', $fields)) {
            $sum = Product::query(['in', 'id', ArrayHelper::getColumn($productList, 'id')])
                ->select('sum(price)')
                ->scalar();
            if ($sum === false) {
                $sum = 0;
            }
            $fields['sum'] = $sum;
        }
        $request = self::insert($fields);
        foreach ($productList as $item) {
            RequestProduct::insert([
                'request_id' => $request->getId(),
                'product_id' => $item['id'],
                'count'      => $item['count'],
            ]);
        }
        $message = $request->addStatus($status, self::DIRECTION_TO_SHOP);

        return $request;
    }

    /**
     * Возвращает заготовку запроса для списка товаров для заказа
     * gs_unions_shop_product.*
     *
     * @return \yii\db\Query
     */
    public function getProductList()
    {
        return RequestProduct::query(['request_id' => $this->getId()])
            ->select([
                'gs_unions_shop_product.*',
                'gs_users_shop_requests_products.count',
            ])
            ->innerJoin('gs_unions_shop_product', 'gs_unions_shop_product.id = gs_users_shop_requests_products.product_id');
    }

    /**
     * Возвращает заготовку запроса для сообщений для заказа
     * gs_users_shop_requests_messages.*
     *
     * @return \yii\db\Query
     */
    public function getMessages()
    {
        return RequestMessage::query(['request_id' => $this->getId()]);
    }

    /**
     * Возвращает объединение на которое оформлен заказ
     *
     * @return \app\models\Union
     */
    public function getUnion()
    {
        return Union::find($this->getField('union_id'));
    }

    /**
     * Возвращает объект клиента
     *
     * @return \app\models\User | null
     */
    public function getUser()
    {
        return User::find($this->getField('user_id'));
    }

    /**
     * Возвращает объект клиента
     *
     * @return \app\models\User | null
     */
    public function getClient()
    {
        return $this->getUser();
    }

    public function getStatus()
    {
        return $this->getField('status');
    }
}