<?php

namespace app\models\Shop;

use app\services\Subscribe;
use cs\services\BitMask;
use yii\db\ActiveRecord;
use yii\db\Query;
use yii\helpers\ArrayHelper;

/**
 * Ссылки на наложный платеж
 *
 * Class Nalojnii
 * @package app\models\Shop
 *
 * @property int $id
 * @property string $link
 * @property string $address
 * @property string $phone
 * @property int $time
 * @property int $price
 * @property int $is_done - 0 - еще активен, 1 - заказ выполнен, получены деньги
 * @property int $is_hide - 0 - виден, 1 - скрыт из показа
 * @property int $status 0 - в обработке, 1 - получил я, 2 - пропало
 *
 */
class Nalojnii extends ActiveRecord
{
    public static function tableName()
    {
        return 'gs_users_shop_nalojnii';
    }

    public function rules()
    {
        return [
            [['link'], 'string', 'max' => 255],
            [['address'], 'string', 'max' => 255],
            [['phone'], 'string', 'max' => 20],
            [['time'], 'integer'],
            [['price'], 'integer'],
            [['status'], 'integer'],
        ];
    }
}