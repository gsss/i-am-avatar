<?php

namespace avatar\models\forms\shop;

use common\models\school\UserLink;
use common\models\SendLetter;
use common\models\UserSeed;
use common\services\Security;
use cs\Application;
use cs\services\Str;
use Yii;
use yii\base\Model;
use yii\helpers\ArrayHelper;
use yii\helpers\VarDumper;
use yii\web\UploadedFile;

/**
 */
class ShopLogin extends \iAvatar777\services\FormAjax\Model
{
    public $login;
    public $password;

    /** @var  int */
    public $school_id;

    /** @var  \common\models\UserAvatar */
    private $user;

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            ['school_id', 'required'],
            ['school_id', 'integer'],

            ['login', 'required'],
            ['login', 'string'],
            ['login', 'email'],
            ['login', 'validateUser'],

            ['password', 'required'],
            ['password', 'string'],
            ['password', 'validatePassword'],

        ];
    }

    /**
     */
    public function validateUser($attribute, $params)
    {
        if (!$this->hasErrors()) {
            try {
                $user = \common\models\UserAvatar::findOne(['email' => $this->login]);
            } catch (\Exception $e) {
                $this->addError($attribute, 'Пользователь не найден');
                return;
            }

            if ($user->mark_deleted == 1) {
                $this->addError($attribute, 'Пользователь удален');
                return;
            }

            // Если пользователь не присоединен к школе, что делать? Я предлагаю молча добавлять потому что он же клиент школы становится. Хотя такая ситуация минимальна. ТО есть сложно представить что пользователь зарегистрировался в другой школе или аватаре а потом зашел на незнакомый сайт и он скорее будет регистрироваться. А вот здесь уже еще одна ситуация возникает.
            $link = UserLink::findOne(['school_id' => $this->school_id, 'user_root_id' => $user->user_root_id]);
            if (is_null($link)) {
                $link = UserLink::add(['school_id' => $this->school_id, 'user_root_id' => $user->user_root_id]);
            }
            $this->user = $user;
        }
    }

    /**
     * Validates the password.
     * This method serves as the inline validation for password.
     *
     * @param string $attribute the attribute currently being validated
     * @param array $params the additional name-value pairs given in the rule
     */
    public function validatePassword($attribute, $params)
    {
        if (!$this->hasErrors()) {
            $user = $this->user;

            if (Application::isEmpty($user->password)) {
                $this->addError($attribute, 'Вы не установили себе пароль');
                return;
            }

            if ($user->validatePassword($this->password) == false) {
                $this->addError($attribute, Yii::t('c.Q2vFnrwWB1', 'Не верный пароль'));
                return;
            }
        }
    }

    public function save($runValidation = true, $attributeNames = null)
    {
        Yii::$app->user->login($this->user, 2592000);
    }
}
