<?php

namespace app\models\Shop;

use app\models\Union;
use app\services\Subscribe;
use cs\services\BitMask;
use yii\db\ActiveRecord;
use yii\db\Query;
use yii\helpers\Url;

/**
 * Cписок модификации товара
 *
 * Class Modification
 * @package app\models\Shop
 *
 * @property int union_id идентификатор объединения
 * @property string name наименование модификации
 * @property int sort_index сортоировочный индекс
 *
 */
class Modification extends ActiveRecord
{
    public static function tableName()
    {
        return 'gs_unions_shop_modification_list';
    }
}