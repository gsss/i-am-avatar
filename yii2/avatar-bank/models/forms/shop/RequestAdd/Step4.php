<?php

namespace avatar\models\forms\shop\RequestAdd;

use common\models\school\TeacherLink;
use common\models\SendLetter;
use common\models\UserSeed;
use common\services\Security;
use cs\Application;
use cs\services\Str;
use Yii;
use yii\base\Model;
use yii\helpers\ArrayHelper;
use yii\helpers\VarDumper;
use yii\web\UploadedFile;

/**
 */
class Step4 extends StepBase
{
    /** @var  string например '9,458' */
    public $masters;

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            ['masters', 'required', 'message' => 'Нужно выбрать хотябы одного преподавателя'],
            ['masters', 'string'],
            ['masters', 'validateMasters'],
        ];
    }

    public function validateMasters($attribute, $params)
    {
        if (!$this->hasErrors()) {
            $ids = explode(',', $this->masters);
            foreach ($ids as $id) {
                $exists = TeacherLink::find()->where(['user_id' => $id, 'school_id' => $this->school_id])->exists();
                if (!$exists) {
                    $this->addError($attribute, 'Пользователя ' . $id . ' нет в учителях школы');
                    return;
                }
            }
        }
    }

    /**
     * @return array customized attribute labels
     */
    public function attributeLabels()
    {
        return [
            'masters'        => 'Преподаватели',
        ];
    }

    public function action()
    {

    }
}
