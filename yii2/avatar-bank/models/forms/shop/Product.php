<?php

namespace avatar\models\forms\shop;

use app\models\Form\Shop\ProductImage;
use app\models\Union;
use app\services\Subscribe;
use common\models\school\File;
use cs\services\BitMask;
use yii\db\Query;
use yii\helpers\Url;

/**
 * Class Product
 *
 * @property int school_id
 * @property int price
 * @property int tree_node_id
 *
 * @package avatar\models\forms
 */
class Product extends \cs\base\FormActiveRecord
{
    public $_school_id;
    public $_type_id;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'gs_unions_shop_product';
    }

    public function formAttributes()
    {
        return [
            [
                'name',
                'Имя',
                1,
                'string',
            ],
            [
                'price',
                'Цена',
                0,
                'integer',
            ],
            [
                'currency_id',
                'Валюта',
                0,
                'integer',
            ],
            [
                'tree_node_id',
                'Каталог',
                0,
                'integer',
            ],
            [
                'image',
                'Картинка',
                0,
                '\common\widgets\FileUpload7\Validator',
                'widget' => [
                    '\common\widgets\FileUpload7\FileUpload',
                    [
                        'update'    => \avatar\controllers\CabinetSchoolSettingsController::getUpdateShopImage($this->_school_id),
                        'settings'  => \avatar\controllers\CabinetSchoolPagesConstructorController::getSettings($this->_school_id, $this->_type_id),
                        'events'    => [
                            'onDelete' => function (Product $item) {
                                $r = new \cs\services\Url($item->image);
                                $d = pathinfo($r->path);
                                $start = $d['dirname'] . '/' . $d['filename'];

                                File::deleteAll(['like', 'file', $start]);
                            },
                        ],
                    ],
                ],
            ],
            [
                'is_electron',
                'Электронынй?',
                0,
                '\common\widgets\CheckBox2\Validator',
                'widget' => [
                    '\common\widgets\CheckBox2\CheckBox'
                ],
            ],
            [
                'content',
                'Полный текст',
                0,
                'string',
                'widget' => [
                    '\common\widgets\HtmlContent\HtmlContent',
                ]
            ],



        ];
    }
}