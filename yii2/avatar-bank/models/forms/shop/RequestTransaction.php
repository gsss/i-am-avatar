<?php

namespace app\models\Shop;

use app\models\Shop;
use app\models\Union;
use app\models\User;
use app\services\Subscribe;
use cs\Application;
use cs\services\BitMask;
use cs\web\Exception;
use cs\Widget\FileUploadMany\FileUploadMany;
use yii\db\ActiveRecord;
use yii\db\Query;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;

/**
 * Class RequestTransaction
 * @package app\models\Shop
 * 
 * @property int    id
 * @property int    request_id
 * @property int    transaction_id
 * @property int    type
 * @property double sum
 * 
 */
class RequestTransaction extends ActiveRecord
{
    const TYPE_PAID_WALLET    = 1; // Оплачено с кошелька
    const TYPE_PAID_PAYSYSTEM = 2; // Оплачено с внешней платежной системы
    const TYPE_REFERAL1       = 3; // Реферальные отчисления 1 уровня
    const TYPE_REFERAL        = 4; // Реферальные отчисления всех уровней кроме первого

    public static function tableName()
    {
        return 'gs_users_shop_requests_transactions';
    }

    public function rules()
    {
        return [
            [[
                'request_id',
                'transaction_id',
                'type',
                'sum',
            ], 'required'],
            [[
                'request_id',
                'transaction_id',
                'type',
            ], 'integer'],
            [[
                'sum',
            ], 'double'],
        ];
    }

    public static function add($fields)
    {
        $iam = new self($fields);
        $iam->save();
        $iam->id = \Yii::$app->db->lastInsertID;

        return $iam;
    }

    public static function batchInsert($columns, $rows)
    {
        (new Query())->createCommand()->batchInsert(self::tableName(), $columns, $rows)->execute();
    }
}