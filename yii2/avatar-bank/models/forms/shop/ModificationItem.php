<?php

namespace app\models\Shop;

use app\models\Union;
use app\services\Subscribe;
use cs\services\BitMask;
use yii\db\ActiveRecord;
use yii\db\Query;
use yii\helpers\Url;

/**
 * Элемент списка модификации товара
 *
 * Class ModificationItem
 * @package app\models\Shop
 *
 * @property int list_id идентификатор объединения
 * @property string name наименование модификации
 * @property int sort_index сортировочный индекс
 * @property float price_add
 * @property float price_multiply
 *
 */
class ModificationItem extends ActiveRecord
{
    public static function tableName()
    {
        return 'gs_unions_shop_modification_item';
    }
}