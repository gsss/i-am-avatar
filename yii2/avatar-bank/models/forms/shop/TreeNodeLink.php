<?php

namespace app\models\Shop;

use app\services\Subscribe;
use cs\services\BitMask;
use yii\db\ActiveRecord;
use yii\db\Query;
use yii\helpers\ArrayHelper;

/**
 * Class TreeNodeLink
 * @package app\models\Shop
 *
 * @property int $tree_node_id
 * @property int $product_id
 *
 */
class TreeNodeLink extends ActiveRecord
{
    public static function tableName()
    {
        return 'gs_shop_tree_products_link';
    }

    public function rules()
    {
        return [
            [['tree_node_id', 'product_id'], 'required'],
            [['tree_node_id', 'product_id'], 'integer'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'tree_node_id' => 'Идентификатор ветки',
            'product_id'   => 'Идентификатор продукта',
        ];
    }

    /**
     * Возвращает дерево
     *
     * @param int $parentId
     *
     * @return array
     * [[
     *  'id' =>
     *  'name' =>
     *  'nodes' => array
     * ], ... ]
     */
    public static function getTree($parentId = null)
    {
        $rows = self::find()
            ->select('id, name, id_string')
            ->where(['parent_id' => $parentId])
            ->orderBy(['sort_index' => SORT_ASC])
            ->asArray()
            ->all();
        for ($i = 0; $i < count($rows); $i++) {
            $item = &$rows[$i];
            $rows2 = self::getTree($item['id']);
            if (count($rows2) > 0) {
                $item['nodes'] = $rows2;
            }
        }

        return $rows;
    }

    /**
     * Возвращает массив идентификаторов которые входяв в эту ветку
     *
     * @return array
     * [<int>, ... ]
     */
    public function getAllChilds()
    {
        $ids = self::find()
            ->select('id')
            ->where(['parent_id' => $this->id])
            ->asArray()
            ->column();
        $new = [];
        foreach ($ids as $id) {
            $new[] = (int)$id;
        }
        $ids = $new;
        foreach ($ids as $id) {
            $new = TreeNodeGeneral::findOne($id)->getAllChilds();
            $ids = ArrayHelper::merge($ids, $new);
        }

        return $ids;
    }
}