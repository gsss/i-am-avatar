<?php

namespace app\models\Shop;

use app\services\Subscribe;
use cs\base\DbRecord;
use cs\services\BitMask;
use yii\db\ActiveRecord;
use yii\db\Query;
use yii\helpers\Url;


/**
 * Class DostavkaItem
 * @package app\models\Shop
 *
 * @property integer id
 * @property string name
 * @property integer description
 * @property integer content
 * @property integer union_id
 * @property integer price
 * @property integer is_need_address
 * @property integer type
 */
class DostavkaItem extends DbRecord
{
    const TABLE = 'gs_unions_shop_dostavka';

    public static function tableName()
    {
        return 'gs_unions_shop_dostavka';
    }
}