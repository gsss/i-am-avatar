<?php

namespace avatar\models\forms\shop\EventAdd;

use common\models\school\TeacherLink;
use common\models\SendLetter;
use common\models\UserSeed;
use common\services\Security;
use cs\Application;
use cs\services\Str;
use Yii;
use yii\base\Model;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;
use yii\helpers\VarDumper;
use yii\web\UploadedFile;

/**
 * \avatar\widgets\Anketa\Widget
 */
class Step5 extends StepBase
{
    /** @var  string as JSON например '["Адрес проживания","Ссыдка на FB"]' */
    public $fields;

    /** @var  array преобразованные данные из $fields в функции validateJSON */
    public $data;

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            ['fields', 'string'],
            ['fields', 'validateJSON'],
            ['fields', 'validateFields'],
        ];
    }

    public function validateJSON($attribute, $params)
    {
        if (!$this->hasErrors()) {
            try {
                $data = Json::decode($this->fields);
            } catch (\Exception $e) {
                $this->addError($attribute, 'Не верный JSON');
                return;
            }
            $this->data = $data;
        }
    }

    public function validateFields($attribute, $params)
    {
        if (!$this->hasErrors()) {
            $fieldsArray = $this->data;
        }
    }

    /**
     * @return array customized attribute labels
     */
    public function attributeLabels()
    {
        return [
            'fields'        => 'Поля',
        ];
    }

    public function action()
    {

    }
}
