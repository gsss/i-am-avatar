<?php

namespace avatar\models\forms\shop\EventAdd;

use common\models\school\School;
use common\models\school\TeacherLink;
use common\models\SendLetter;
use common\models\UserSeed;
use common\services\Security;
use cs\Application;
use cs\services\Str;
use Yii;
use yii\base\Model;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;
use yii\helpers\VarDumper;
use yii\web\UploadedFile;

/**
 */
class Step7 extends StepBase
{
    /** @var  string */
    public $url;

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            ['url', 'string'],
            ['url', 'validateUrl'],
        ];
    }

    public function validateUrl($a,$p)
    {
        if (!$this->hasErrors()) {
            $school = School::findOne($this->school_id);
            $result = $school->validateUrl($this->url);
            if ($result['code'] > 0) {
                $this->addError($a, $result['message']);
                return;
            }
        }
    }

    /**
     * @return array customized attribute labels
     */
    public function attributeLabels()
    {
        return [
            'url' => 'Ссылка на страницу',
        ];
    }

    /**
     * @return array customized attribute labels
     */
    public function attributeHints()
    {
        return [
            'url' => 'Начинается co слеша',
        ];
    }

    public function action()
    {

    }
}
