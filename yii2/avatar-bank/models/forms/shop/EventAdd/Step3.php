<?php

namespace avatar\models\forms\shop\EventAdd;

use common\models\SendLetter;
use common\models\UserSeed;
use common\services\Security;
use cs\Application;
use cs\services\Str;
use Yii;
use yii\base\Model;
use yii\helpers\ArrayHelper;
use yii\helpers\VarDumper;
use yii\web\UploadedFile;

/**
 */
class Step3 extends StepBase
{
    public $date_start;
    public $time_start;
    public $date_end;
    public $time_end;
    public $time_zone;

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            ['date_start', 'required'],
            ['date_start', 'date', 'format' => 'php:d.m.Y'],
            ['time_start', 'string'],

            ['date_end', 'string'],
            ['date_end', 'date', 'format' => 'php:d.m.Y'],
            ['date_end', 'validateDate'],
            ['time_end', 'string'],
            ['time_zone', 'string'],
        ];
    }

    /**
     * @return array customized attribute labels
     */
    public function attributeLabels()
    {
        return [
            'date_start' => 'Дата начала',
            'time_start' => 'Время начала',
            'date_end'   => 'Дата окончания',
            'time_end'   => 'Время окончания',
            'time_zone'   => 'Временная зона',
        ];
    }

    /**
     * Проверяет чтобы дата окончания не была раньше даты начала
     */
    public function validateDate($attribute, $params)
    {
        if (!$this->hasErrors()) {
            $date1 = substr($this->date_start, 6) . '-' . substr($this->date_start, 3 ,2) . '-' . substr($this->date_start, 0 ,2);
            $date2 = substr($this->date_end, 6) . '-' . substr($this->date_end, 3 ,2) . '-' . substr($this->date_end, 0 ,2);
            $date1O = new \DateTime($date1);
            $date2O = new \DateTime($date2);
            if ($date2O->format('U') < $date1O->format('U') ) {
                $this->addError($attribute, 'Дата окончания должны быть больше даты начала или равна');
                return;
            }
        }
    }

    /**
     * @return string полный путь к скачанному файлу
     */
    public function action()
    {

    }
}
