<?php

namespace avatar\models\forms\shop\EventAdd;

use common\models\SendLetter;
use common\models\UserSeed;
use common\services\Security;
use cs\Application;
use cs\services\Str;
use Yii;
use yii\base\Model;
use yii\helpers\ArrayHelper;
use yii\helpers\VarDumper;
use yii\web\UploadedFile;

/**
 */
class Step1 extends StepBase
{
    /** @var  \yii\web\UploadedFile */
    public $name;
    public $content_short;
    public $content;
    public $image;

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            ['name', 'required'],
            ['name', 'string'],

            ['content_short', 'string'],

            ['content', 'string'],

            ['image', 'string'],
        ];
    }

    /**
     * @return array customized attribute labels
     */
    public function attributeLabels()
    {
        return [
            'name'          => 'Наименование',
            'content'       => 'Описание',
            'content_short' => 'Описание короткое',
            'image'         => 'Картинка/иконка',
        ];
    }

    public function validateFile($attribute, $params)
    {
        if (!$this->hasErrors()) {
            $m = UploadedFile::getInstance($this, 'file');
            if (is_null($m)) {
                $this->addError($attribute, 'Файл должен быть заполнен');
                return;
            }
            $this->file = $m;
        }
    }

    /**
     */
    public function action()
    {

    }
}
