<?php

namespace avatar\models\forms\shop\EventAdd;

use common\models\SendLetter;
use common\models\UserSeed;
use common\services\Security;
use cs\Application;
use cs\services\Str;
use cs\services\Url;
use Yii;
use yii\base\Model;
use yii\helpers\ArrayHelper;
use yii\helpers\VarDumper;
use yii\web\UploadedFile;

/**
 */
class Step2 extends StepBase
{
    /** @var  integer */
    public $is_online;

    /** @var  string */
    public $place;

    /** @var  string */
    public $place_name;

    /** @var  string */
    public $online_url;

    /** @var  string Идентификатор видео */
    public $v;

    public function init()
    {
        $this->is_online = true;
    }

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            ['place_name', 'string'],
            ['place', 'string'],
            ['place', 'validatePlace', 'skipOnEmpty' => false,],

            ['is_online', 'integer'],

            ['online_url', 'string'],
            ['online_url', 'url'],
            ['online_url', 'validateUrl', 'skipOnEmpty' => false],
        ];
    }

    /**
     * @return array customized attribute labels
     */
    public function attributeLabels()
    {
        return [
            'place'      => 'Адрес',
            'place_name' => 'Название места',
            'is_online'  => 'Это онлайн событие?',
            'online_url' => 'Ссылка на трансляцию YouTube',
        ];
    }

    public function validatePlace($attribute, $params)
    {
        if (!$this->hasErrors()) {
            if ($this->is_online == 0) {
                if (Application::isEmpty($this->place)) {
                    $this->addError($attribute, 'Для офлайн события адрес обязателен');
                    return;
                }
            }
        }
    }

    public function validateUrl($attribute, $params)
    {
        if (!$this->hasErrors()) {
            if ($this->is_online == 1) {
                if (Application::isEmpty($this->online_url)) {
                    $this->addError($attribute, 'Для онлайн события ссылка обязательна');
                    return;
                }
                $this->online_url = strtolower($this->online_url);
                $urlObject = new Url($this->online_url);

                if (!in_array($urlObject->host, ['www.youtube.com', 'youtube.com', 'youtu.be'])) {
                    $this->addError($attribute, 'сайт должент быть www.youtube.com, youtube.com, youtu.be');
                    return;
                }

                if (in_array($urlObject->host, ['www.youtube.com', 'youtube.com'])) {
                    if (!isset($urlObject->params['v'])) {
                        $this->addError($attribute, 'Должен быть параметр v');
                        return;
                    }
                    $this->v = $urlObject->params['v'];
                }
                if (in_array($urlObject->host, ['youtu.be'])) {
                    $this->v = $urlObject->path;
                }
            }
        }
    }

    /**
     */
    public function action()
    {

    }
}
