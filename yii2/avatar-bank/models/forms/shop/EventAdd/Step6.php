<?php

namespace avatar\models\forms\shop\EventAdd;

use common\models\school\TeacherLink;
use common\models\SendLetter;
use common\models\UserSeed;
use common\services\Security;
use cs\Application;
use cs\services\Str;
use Yii;
use yii\base\Model;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;
use yii\helpers\VarDumper;
use yii\web\UploadedFile;

/**
 */
class Step6 extends StepBase
{
    public $vebinar_tab;

    /** @var  integer */
    public $vebinar_is_password;

    /** @var  integer */
    public $vebinar_max;

    /** @var  string */
    public $vebinar_password;

    /** @var  string JSON */
    public $vebinar_both_tickets;
    public $vebinar_buy_tickets;

    public $offline_tab;

    /** @var  integer */
    public $offline_max;

    /** @var  string JSON */
    public $offline_both_tickets;
    public $offline_buy_tickets;

    /** @var  array данные из $tickets расшифрованные из JSON */
    public $data = [];

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            ['vebinar_tab', 'required'],
            ['vebinar_tab', 'integer'],

            ['vebinar_is_password', 'integer'],
            ['vebinar_password', 'string'],
            ['vebinar_max', 'integer'],
            ['vebinar_both_tickets', 'string'],
            ['vebinar_both_tickets', 'validateJSON'],
            ['vebinar_both_tickets', 'validateFields'],

            ['vebinar_buy_tickets', 'string'],
            ['vebinar_buy_tickets', 'validateJSON'],
            ['vebinar_buy_tickets', 'validateFields'],

            ['offline_tab', 'integer'],
            ['offline_max', 'integer'],
            ['offline_both_tickets', 'string'],
            ['offline_both_tickets', 'validateJSON'],
            ['offline_both_tickets', 'validateFields'],
            ['offline_buy_tickets', 'string'],
            ['offline_buy_tickets', 'validateJSON'],
            ['offline_buy_tickets', 'validateFields'],
        ];
    }

    public function validateJSON($attribute, $params)
    {
        if (!$this->hasErrors()) {
            try {
                $data = Json::decode($this->$attribute);
            } catch (\Exception $e) {
                $this->addError($attribute, 'Не верный JSON');
                return;
            }
            $this->data[$attribute] = $data;
        }
    }

    public function validateFields($attribute, $params)
    {
        if (!$this->hasErrors()) {
            $fieldsArray = $this->data;
        }
    }

    /**
     * @return array customized attribute labels
     */
    public function attributeLabels()
    {
        return [
            'vebinar_is_password'  => 'Нужен пароль?',
            'vebinar_password'     => 'Пароль',
            'vebinar_max'          => 'Лимит пользователей',
            'vebinar_both_tickets' => 'Билеты',
            'vebinar_buy_tickets'  => 'Билеты',
            'offline_max'  => 'Лимит пользователей',
            'offline_both_tickets'  => 'Билеты',
            'offline_buy_tickets'  => 'Билеты',
        ];
    }

    public function action()
    {

    }
}
