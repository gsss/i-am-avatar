<?php

namespace avatar\models\forms\shop;

use app\services\Subscribe;
use common\models\school\File;
use cs\services\BitMask;
use iAvatar777\services\FormAjax\ActiveRecord;
use yii\db\Query;
use yii\helpers\Url;

class ProductImage extends ActiveRecord
{
    public $_school_id;
    public $_type_id;

    public static function tableName()
    {
        return 'gs_unions_shop_product_images';
    }

    public function rules()
    {
        return [
            ['name', 'string'],

            ['image', 'required'],
            ['image', 'string'],
        ];
    }

    public function attributeWidgets()
    {
        return [
            'image' => [
                'class'    => '\iAvatar777\widgets\FileUpload7\FileUpload',
                'update'   => \avatar\controllers\CabinetSchoolPagesConstructorController::getUpdate(),
                'settings' => \avatar\controllers\CabinetSchoolPagesConstructorController::getSettingsLibrary($this->_school_id, $this->_type_id),
                'events'   => [
                    'onDelete' => function ($item) {
                        $r = new \cs\services\Url($item->image);
                        $d = pathinfo($r->path);
                        $start = $d['dirname'] . '/' . $d['filename'];

                        File::deleteAll(['like', 'file', $start]);
                    },
                ],
            ],
        ];
    }
}