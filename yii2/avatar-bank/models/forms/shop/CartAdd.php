<?php

namespace avatar\models\forms\shop;

use common\models\avatar\Currency;
use common\models\avatar\UserBill;
use common\models\BillingMain;
use common\models\PaySystemConfig;
use common\models\piramida\Wallet;
use common\models\shop\Basket;
use common\models\shop\Product;
use common\models\shop\Request;
use school\modules\sroutes\models\Deal;
use school\modules\sroutes\models\DealStatus;
use school\modules\sroutes\models\Offer;
use cs\base\FormActiveRecord;
use cs\services\VarDumper;
use Yii;
use yii\base\Model;
use yii\base\NotSupportedException;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\helpers\Json;
use yii\web\IdentityInterface;

/**
 *
 */
class CartAdd extends Model
{
    public $id;

    /** @var  \common\models\shop\Product */
    public $product;

    public function rules()
    {
        return [
            ['id', 'required'],
            ['id', 'integer'],
            ['id', 'validateProduct'],
        ];
    }

    public function validateProduct($a, $p)
    {
        if (!$this->hasErrors()) {
            $product = Product::findOne($this->id);
            if (is_null($product)) {
                $this->addError($a, 'Продукт не найден');
                return;
            }
            $this->product = $product;
        }
    }

    public function action()
    {
        return Basket::add($this->product);
    }
}
