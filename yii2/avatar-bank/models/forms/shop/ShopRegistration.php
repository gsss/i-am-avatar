<?php

namespace avatar\models\forms\shop;

use common\models\school\UserLink;
use common\models\SendLetter;
use common\models\UserAvatar;
use common\models\UserRoot;
use common\models\UserSeed;
use common\services\Security;
use cs\Application;
use cs\services\Str;
use Yii;
use yii\base\Model;
use yii\helpers\ArrayHelper;
use yii\helpers\VarDumper;
use yii\web\UploadedFile;
use \avatar\models\UserRegistration;
use \common\models\UserPassword;
use \common\services\Subscribe;
use \yii\helpers\Url;


/**
 */
class ShopRegistration extends \iAvatar777\services\FormAjax\Model
{
    public $login;
    public $password;

    /** @var  int */
    public $school_id;

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            ['school_id', 'required'],
            ['school_id', 'integer'],

            ['login', 'required'],
            ['login', 'string'],
            ['login', 'trim'],
            ['login', 'toLower'],
            ['login', 'email'],
            ['login', 'validateUser'],
            ['login', 'validateUserRoot'],

            ['password', 'required'],
            ['password', 'string'],

        ];
    }

    /**
     */
    public function validateUser($attribute, $params)
    {
        if (!$this->hasErrors()) {
            try {
                $user = \common\models\UserAvatar::findOne(['email' => $this->login]);
                $this->addError($attribute, 'Такой пользователь уже есть, придумайте другой или если это ваш логин, то войдите');
                return;
            } catch (\Exception $e) {

            }
        }
    }

    /**
     */
    public function toLower($attribute, $params)
    {
        if (!$this->hasErrors()) {
            $this->login = strtolower($this->login);
        }
    }

    /**
     */
    public function validateUserRoot($attribute, $params)
    {
        if (!$this->hasErrors()) {
            $userRoot = UserRoot::findOne(['email' => $this->login]);
            if (is_null($userRoot)) {
                $userRoot = UserRoot::add(['email' => $this->login]);
                $link = UserLink::add(['user_root_id' => $userRoot->id, 'school_id' => $this->school_id]);
            } else {
                $link = UserLink::findOne(['user_root_id' => $userRoot->id, 'school_id' => $this->school_id]);
                if (is_null($link)) {
                    $link = UserLink::add(['user_root_id' => $userRoot->id, 'school_id' => $this->school_id]);
                }
            }
        }
    }

    public function save($runValidation = true, $attributeNames = null)
    {
        $email = $this->login;
        $password = $this->password;

        $userRoot = UserRoot::findOne(['email' => $email]);
        if (is_null($userRoot)) {
            $userRoot = UserRoot::add(['email' => $email]);
        }

        UserRegistration::deleteAll(['parent_id' => $userRoot->id]);
        $userRegistration = UserRegistration::add($userRoot->id);

        Subscribe::sendArray([$email], 'Подтверждение регистрации', 'registration', [
            'url'      => Url::to([
                'auth/registration-shop-activate',
                'code' => $userRegistration->code
            ], true),
            'user'     => $userRoot,
            'datetime' => \Yii::$app->formatter->asDatetime($userRegistration->date_finish)
        ]);

        // Создаю пользователя
        $user = \common\models\UserAvatar::registrationCreateUser($email, $password, [
            'user_root_id'     => $userRoot->id,
            'email_is_confirm' => 0,
        ]);

        // Логиню пользователя
        Yii::$app->user->login($user);

        return $user;
    }
}
