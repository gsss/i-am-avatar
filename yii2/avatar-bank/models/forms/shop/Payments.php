<?php

namespace app\models\Shop;

use app\models\Union;
use app\services\Subscribe;
use cs\services\BitMask;
use yii\db\ActiveRecord;
use yii\db\Query;
use yii\helpers\Url;

class Payments extends ActiveRecord
{
    public static function tableName()
    {
        return 'bog_shop_payments';
    }
}