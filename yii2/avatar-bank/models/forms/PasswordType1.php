<?php

namespace avatar\models\forms;

use common\models\avatar\UserBill;
use common\models\SendLetter;
use common\models\UserAvatar;
use common\models\UserSeed;
use common\services\Security\AES;
use cs\Application;
use cs\services\Str;
use Yii;
use yii\base\Model;
use yii\helpers\ArrayHelper;
use yii\helpers\VarDumper;

/**
 */
class PasswordType1 extends Model
{
    /** @var  string пароль от кабинета */
    public $password;
    public $seeds;
    public $isAgree;

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            ['password', 'required'],
            ['password', 'string'],
            ['password', 'validatePassword'],

            ['seeds', 'required'],
            ['seeds', 'string'],
            ['seeds', 'normalizeSeeds'],
            ['seeds', 'validateSeeds', 'message' => 'Вы ввели не верный ключ'],

            ['isAgree', 'required'],
            ['isAgree', 'integer'],
            ['isAgree', 'compare', 'compareValue' => 1, 'message' => 'Нужно принять условия соглашения'],
        ];
    }

    public function validatePassword($attribute, $params)
    {
        if (!$this->hasErrors()) {
            /** @var \common\models\UserAvatar $user */
            $user = Yii::$app->user->identity;
            if (!$user->validatePassword($this->password)) {
                $this->addError($attribute, 'Пароль не верный');
            }
        }
    }

    public function normalizeSeeds($attribute, $params)
    {
        if (!$this->hasErrors()) {
            $ret = [];
            $arr = explode("\n", $this->seeds);
            foreach ($arr as $row) {
                $arr2 = explode(" ", $row);
                foreach ($arr2 as $word) {
                    if (trim($word) != '') {
                        $ret[] = trim($word);
                    }
                }
            }
            $this->seeds = join(' ', $ret);
        }
    }

    public function validateSeeds($attribute, $params)
    {
        if (!$this->hasErrors()) {
            if (Yii::$app->session->get('seeds-start') != $this->seeds) {
                $this->addError($attribute, 'Не верный SEEDS');
            }
        }
    }


    /**
     */
    public function action()
    {
        $this->setPassword($this->password);

        /** @var \common\models\UserAvatar $user */
        $user = Yii::$app->user->identity;
        $user->password_save_type = UserBill::PASSWORD_TYPE_HIDE_CABINET;
        $user->wallets_is_locked = 0;
        $user->save();

        $seeds = $this->seeds;
        $sha256 = hash('sha256', $seeds);
        $key32 = substr($sha256, 0, 32);
        $hash = \common\services\Security\AES::encrypt256CBC($this->password, $key32);
        $seedsHashObject = UserSeed::add($hash);
        $seedsHashObject->seeds = $hash;
        $seedsHashObject->save();

        Yii::$app->session->remove('seeds-start');
        Yii::$app->session->remove('seeds');

        return true;
    }

    /**
     * Устанавливает новый пароль на кошельки
     *
     * @param string $to новый пароль
     *
     * @throws \Exception
     */
    private function setPassword($to)
    {
        $billingList = UserBill::find()
            ->where([
                'user_id'      => Yii::$app->user->id,
                'mark_deleted' => 0,
                'is_merchant'  => 0,
            ])
            ->all()
        ;
        $t = \Yii::$app->db->beginTransaction();

        try {
            /** @var \common\models\avatar\UserBill $billing */
            foreach ($billingList as $billing) {
                $passwordHash = $billing->password;
                $keyTo = UserBill::passwordToKey32($to);
                $walletPassword = $passwordHash;
                $passwordHashNew = AES::encrypt256CBC($walletPassword, $keyTo);
                $billing->password_type = UserBill::PASSWORD_TYPE_HIDE_CABINET;
                $billing->password = $passwordHashNew;
                $ret = $billing->save();
            }
            $t->commit();
        } catch (\Exception $e) {
            $t->rollBack();
            throw $e;
        }
    }
}
