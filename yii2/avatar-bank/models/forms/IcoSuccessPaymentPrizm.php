<?php
namespace avatar\models\forms;

use avatar\base\Application;
use common\models\BillingMain;
use common\models\Config;
use common\models\investment\IcoRequest;
use cs\base\FormActiveRecord;
use cs\services\VarDumper;
use Yii;
use yii\base\Model;
use yii\base\NotSupportedException;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\helpers\Json;
use yii\helpers\StringHelper;
use yii\web\IdentityInterface;

/**
 *
 */
class IcoSuccessPaymentPrizm extends Model
{
    public $id;

    /** @var  \common\models\investment\IcoRequest */
    public $request;

    public $txid;

    public function rules()
    {
        return [
            ['id', 'required'],
            ['id', 'integer'],
            ['id', 'validateBilling'],

            ['txid', 'required'],
            ['txid', 'string'],
        ];
    }

    public function validateBilling($attribute, $params)
    {
        if (!$this->hasErrors()) {
            $billing = BillingMain::findOne($this->id);
            if (is_null($billing)) {
                $this->addError($attribute,'Не найден счет');
                return;
            }
            $request = IcoRequest::findOne(['billing_id' => $billing->id]);
            if (is_null($request)) {
                $this->addError($attribute,'Не найдена заявка');
                return;
            }
            if ($request->user_id != Yii::$app->user->id) {
                $this->addError($attribute,'Это не ваш заказ');
                return;
            }

            $this->request = $request;
        }
    }

    /**
     * Конвертирует массив ошибок от формы чтобы их можно было выдать через JSON
     *
     * @param array $params
     * [
     *      'amount' => ['Не верное число',...],
     * ]
     *
     * @return array
     * [
     *  [
     *      'name'  => 'amount',
     *      'value' => ['Не верное число',...],
     *  ], ...
     * ]
     */
    public function convert($params = null)
    {
        if (is_null($params)) {
            $params = $this->errors;
        }
        if (count($params) == 0) {
            return [];
        }
        $ret = [];
        foreach ($params as $name => $arr) {
            $ret[] = [
                'name'  => $name,
                'value' => $arr,
            ];
        }
        return $ret;
    }

    public function action()
    {
        // установка статуса
        $this->request->addStatusToShop([
            'status'  => IcoRequest::STATUS_USER_CONFIRM,
            'message' => 'txid = ' . $this->txid,
        ]);

        // установка транзакции
        $payment = $this->request->getPaymentI();
        $payment->saveTransaction($this->txid);

        // отправка письма
        $mail = $this->request->getIco()->getProject()->getUser()->email;
        $ret = Application::mail(
            $mail,
            'Оплата произведена по заказу #' . $this->request->id,
            'ico_client_confirm_to_shop',
            [
                'request' => $this->request,
                'txid'    => $this->txid,
            ]
        );

        return true;
    }
}
