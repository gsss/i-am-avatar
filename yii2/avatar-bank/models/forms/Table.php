<?php
namespace avatar\models\forms;

use common\models\avatar\UserBill;
use common\models\piramida\Billing;
use common\models\statistic\StatisticDb;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;

/**
 * @property int id
 * @property int school_id
 * @property string name
 */
class Table extends ActiveRecord
{
    public static function tableName()
    {
        return 'school_task_table';
    }

    public function rules()
    {
        return [
            ['name', 'required'],
            ['name', 'string'],

            ['school_id', 'integer'],
        ];
    }

    /**
     * @param $fields
     *
     * @return static
     */
    public static function add($fields)
    {
        $i = new static($fields);
        $i->save();
        $i->id = self::getDb()->lastInsertID;

        return $i;
    }
}