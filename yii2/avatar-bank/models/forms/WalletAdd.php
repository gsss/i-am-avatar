<?php

namespace avatar\models\forms;

use common\models\avatar\CurrencyLink;
use common\models\avatar\UserBill;
use common\models\piramida\Wallet;
use cs\Application;
use cs\base\FormActiveRecord;
use Yii;
use yii\base\Model;
use yii\base\NotSupportedException;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\web\IdentityInterface;

/**
 */
class WalletAdd extends Model
{
    /** @var  int Идентификатор кошелька откуда отправляются монеты */
    public $id;

    public $amount;

    public $comment;

    /** @var  \common\models\piramida\Wallet */
    public $walletOut;

    public function rules()
    {
        return [
            ['id', 'required'],
            ['id', 'integer'],
            ['id', 'exist', 'targetClass' => '\common\models\piramida\Wallet'],
            ['id', 'setItem'],

            ['amount', 'required'],
            ['amount', 'integer'],

            ['comment', 'string'],
            ['comment', 'default', 'value' => 'Отправление'],
        ];
    }

    public function setItem($attribute, $params)
    {
        if (!$this->hasErrors()) {
            $walletOut = Wallet::findOne($this->id);
            $this->walletOut = $walletOut;
        }
    }

    /**
     * @return \common\models\Piramida\Operation
     */
    public function action()
    {
        return $this->walletOut->in(
            $this->amount,
            $this->comment
        );
    }
}
