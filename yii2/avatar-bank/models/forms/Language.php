<?php
namespace avatar\models\forms;

use cs\base\FormActiveRecord;
use Yii;
use yii\base\NotSupportedException;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\web\IdentityInterface;

/**
 * PaySystem model
 *
 * @property integer id
 * @property string  code
 * @property string  name
 * @property string  image
 */
class Language extends FormActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'languages';
    }

    public function formAttributes()
    {
        return [
            [
                'name',
                'Название',
                1,
                'string',
                ['max' => 255],
            ],
            [
                'code',
                'Описание',
                1,
                'string',
                ['max' => 2],
            ],
            [
                'image',
                'Картинка',
                0,
                '\common\widgets\FileUpload3\Validator',
                'widget' => [
                    '\common\widgets\FileUpload3\FileUpload',
                    [
                        'options' => [
                            'small' => \avatar\services\Html::$formatIcon
                        ]
                    ]
                ]
            ],
        ];
    }
}
