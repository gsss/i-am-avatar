<?php

namespace avatar\models\forms;

use app\models\User;
use avatar\models\UserRecover;
use common\models\UserAvatar;
use cs\web\Exception;
use Yii;
use yii\base\Model;
use yii\helpers\Url;
use yii\helpers\VarDumper;

/**
 */
class PasswordRecover extends Model
{
    public $email;
    public $verifyCode;

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            [
                'email',
                'required',
                'message' => 'Это поле должно быть заполнено обязательно'
            ],
            [
                'email',
                'trim',
            ],
            [
                'email',
                'email',
                'message' => 'Email должен быть верным'
            ],
            [
                'email',
                'validateEmail'
            ],
            ['verifyCode', '\cs\Widget\Google\reCaptcha\Validator', 'skipOnEmpty' => false],
        ];
    }

    public function scenarios()
    {
        return [
            'insert' => [
                'email',
                'verifyCode'
            ],
            'ajax'   => ['email'],
        ];
    }

    /**
     * @return array customized attribute labels
     */
    public function attributeLabels()
    {
        return [
            'verifyCode' => 'Проверочный код',
        ];
    }

    public function validateEmail($attribute, $params)
    {
        $email = strtolower($this->email);
        try {
            $user = UserAvatar::findOne(['email' => $email]);
        } catch (\Exception $e) {
            $this->addError($attribute, 'Такой пользователь не найден');
            return;
        }

        if ($user->mark_deleted == 1) {
            $this->addError($attribute, 'Пользователь заблокирован');
            return;
        }
    }

    /**
     * @return \common\models\subscribe\Subscribe
     *
     * @throws
     */
    public function send()
    {
        $user = UserAvatar::findOne(['email' => $this->email]);
        $userRecover = UserRecover::add($user->id);

        $school = \common\models\school\School::get();

        $ret = \common\services\Subscribe::sendArraySchool($school, [$this->email], 'Восстановление пароля', 'password_recover', [
            'user'     => $user,
            'url'      => Url::to([
                'auth/password-recover-activate',
                'code' => $userRecover->code
            ], true),
            'datetime' => Yii::$app->formatter->asDatetime($userRecover->date_finish),
        ]);

        return $ret;
    }
}
