<?php

namespace avatar\models\forms;

use app\models\ActionsRange;
use common\components\DbMessageSourceSky;
use common\components\EmailVerifier;
use common\models\Config;
use common\models\Language;
use common\models\language\Access;
use common\models\school\AdminLink;
use common\models\school\ReferalLevel;
use common\services\Debugger;
use cs\Application;
use GuzzleHttp\Query;
use yii\base\Model;
use Yii;
use yii\helpers\ArrayHelper;
use yii\helpers\VarDumper;

class Levels extends Model
{
    /**
     * @var array
     */
    public $percent;

    /** @var  int */
    public $school_id;

    public function rules()
    {
        return [
            ['school_id', 'required'],
            ['school_id', 'integer'],
            ['school_id', 'validateSchool'],

            ['percent', 'safe'],
            ['percent', 'isArray'], // Проверка на массив
            ['percent', 'clearEmpty'], // удаляет пустые значения
            ['percent', 'validateInteger'], // Проверяет на целые
        ];
    }

    public function isArray($attribute, $params)
    {
        if (!$this->hasErrors()) {
            if (!is_array($this->percent)) {
                $this->addError($attribute, 'Элемент должен быть массивом');
                return;
            };
        }
    }

    public function clearEmpty($attribute, $params)
    {
        if (!$this->hasErrors()) {
            $i = 0;
            // индексы пустых элементов
            $empty = [];
            foreach ($this->percent as $item) {
                if (Application::isEmpty($item)) {
                    $empty[] = $i;
                }
                $i++;
            }
            if (count($empty) > 0) {
                $new = [];
                $i = 0;
                foreach ($this->percent as $item) {
                    if (!in_array($i, $empty)) {
                        $new[] = $item;
                    }
                }
                $this->percent = $new;
            }
        }
    }

    public function validateInteger($attribute, $params)
    {
        if (!$this->hasErrors()) {
            foreach ($this->percent as $i) {
                if (!Application::isInteger($i)) {
                    $this->addError($attribute, 'Все элементы массива должны быть целыми');
                    return;
                }
            }
        }
    }

    public function validateSchool($attribute, $params)
    {
        if (!$this->hasErrors()) {
            $result = AdminLink::find()
                ->where([
                    'school_id' => $this->school_id,
                    'user_id'   => Yii::$app->user->id,
                ])
                ->exists();

            if (!$result) {
                $this->addError($attribute, 'Вам запрещен доступ к чужой школе');
                return;
            };
        }
    }

    /**
     */
    public function action()
    {
        /** @var \common\models\school\ReferalLevel[] $levelList Это то что есть */
        $levelListOld = ReferalLevel::find()->where(['school_id' => $this->school_id])->all();
        ArrayHelper::multisort($levelListOld, 'level');
        $levelListOld = ArrayHelper::getColumn($levelListOld, 'percent');
        $levelListNew = $this->percent;
        if (count($levelListNew) > count($levelListOld)) {
            // Создаю новые записи
            for ($i = 0; $i < count($levelListNew) - count($levelListOld); $i++) {
                ReferalLevel::add([
                    'percent'   => $levelListNew[$i + count($levelListOld)],
                    'school_id' => $this->school_id,
                    'level'     => $i + count($levelListOld) + 1,
                ]);
            }
            // Обновляю существующие записи
            for ($i = 0; $i < count($levelListOld); $i++) {
                if ($levelListNew[$i] != $levelListOld[$i]) {
                    ReferalLevel::updateAll(['percent' => $levelListNew[$i]], ['school_id' => $this->school_id, 'level' => $i + 1]);
                }
            }
        } else if (count($levelListNew) < count($levelListOld)) {
            // Удаляю старые записи
            $delete = [];
            for ($level = count($levelListNew) + 1; $level <= count($levelListOld); $level++) {
                $delete[] = $level;
            }
            ReferalLevel::deleteAll([
                'and',
                ['school_id' => $this->school_id],
                ['in', 'level', $delete],
            ]);
            // Обновляю существующие записи
            for ($i = 0; $i < count($levelListNew); $i++) {
                if ($levelListNew[$i] != $levelListOld[$i]) {
                    ReferalLevel::updateAll(['percent' => $levelListNew[$i]], ['school_id' => $this->school_id, 'level' => $i + 1]);
                }
            }
        } else {
            // Обновляю существующие записи
            for ($i = 0; $i < count($levelListOld); $i++) {
                if ($levelListNew[$i] != $levelListOld[$i]) {
                    ReferalLevel::updateAll(['percent' => $levelListNew[$i]], ['school_id' => $this->school_id, 'level' => $i + 1]);
                }
            }
        }

        return true;
    }

}