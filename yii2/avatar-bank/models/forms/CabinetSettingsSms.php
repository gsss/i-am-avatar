<?php
/**
 * Created by PhpStorm.
 * User: god
 * Date: 28.02.2017
 * Time: 23:49
 */

namespace avatar\models\forms;

use common\components\sms\IqSms;
use common\models\UserAvatar;
use common\services\OneC;
use cs\services\VarDumper;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;
use yii\helpers\StringHelper;
use yii\web\HttpException;

/**
 * @property integer    id
 * @property integer    shop_image_cut_id   Конфигурация вида обрезки изображений товаров. 0 - обрезать 1 - вписывать 2 - вписывать с фоном. По умолчанию - 0
 * @property integer    blog_image_cut_id   Конфигурация вида обрезки изображений статьи. 0 - обрезать 1 - вписывать 2 - вписывать с фоном. По умолчанию - 0
 * @property integer    blog_image_width    Ширина
 * @property integer    blog_image_height   Высота
 * @property integer    news_image_cut_id   Конфигурация вида обрезки изображений статьи. 0 - обрезать 1 - вписывать 2 - вписывать с фоном. По умолчанию - 0
 * @property integer    news_image_width    Ширина
 * @property integer    news_image_height   Высота
 * @property integer    onec_api_url
 * @property integer    onec_api_key
 * @property integer    sms_login
 * @property integer    sms_password
 * @property integer    school_id
 */
class CabinetSettingsSms extends \iAvatar777\services\FormAjax\ActiveRecord
{
    public static function tableName()
    {
        return 'school_settings';
    }

    /**
     * @return \common\models\school\School
     */
    public function getSchool()
    {
        return School::findone($this->school_id);
    }

    /**
     * @return \common\components\sms\IqSms
     */
    public function getSmsGate()
    {
        return new IqSms(['login' => $this->sms_login, 'password' => $this->sms_password, 'gate' => 'gate.iqsms.ru']);
    }

    /**
     * @return \common\services\OneC
     */
    public function getOneC()
    {
        return new OneC(['url' => $this->onec_api_url, 'key' => $this->onec_api_key]);
    }

    public function rules()
    {
        return [
            ['school_id', 'integer'],

            ['shop_image_cut_id', 'integer'],
            ['blog_image_cut_id', 'integer'],
            ['blog_image_width', 'integer'],
            ['blog_image_height', 'integer'],
            ['news_image_cut_id', 'integer'],
            ['news_image_width', 'integer'],
            ['news_image_height', 'integer'],

            ['onec_api_url', 'string'],
            ['onec_api_url', 'url'],

            ['onec_api_key', 'string'],

            ['sms_login', 'string'],
            ['sms_password', 'string'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'shop_image_cut_id' => 'Вид формирования квадратного изображения товара',
            'blog_image_cut_id' => 'Вид формирования квадратного изображения картинки статьи',
            'blog_image_width'  => 'Ширина',
            'blog_image_height' => 'Высота',
            'news_image_cut_id' => 'Вид формирования квадратного изображения картинки новости',
            'news_image_width'  => 'Ширина',
            'news_image_height' => 'Высота',
            'onec_api_url'      => 'URL',
            'onec_api_key'      => 'Ключ',
            'sms_login'         => 'Логин',
            'sms_password'      => 'Пароль',
        ];
    }

}