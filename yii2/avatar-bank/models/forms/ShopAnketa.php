<?php

namespace avatar\models\forms;

use common\models\SendLetter;
use common\models\UserSeed;
use common\services\Security;
use cs\Application;
use cs\services\Str;
use Yii;
use yii\base\Model;
use yii\helpers\ArrayHelper;
use yii\helpers\VarDumper;
use yii\web\UploadedFile;

/**
 */
class ShopAnketa extends Model
{

    /** @var  int */
    public $anketa_id;

    /** @var  \common\models\school\AnketaShop */
    public $anketa;

    /**
     * @var array
     */
    public $fields;

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        $data = [
            ['anketa_id', 'required'],
            ['anketa_id', 'integer'],
        ];
        if ($this->anketa) {

            $fields = \common\models\school\AnketaField::find()->where(['setting_id' => $this->anketa->id])->all();
            foreach ($fields as $field) {
                $data[] = ['field_' . $field->id, 'string'];
            }
        }

        return $data;
    }

    public function __get($name)
    {
        if (!isset($this->fields[$name])) return null;

        return $this->fields[$name];
    }

    public function __set($name, $value)
    {
        $this->fields[$name] = $value;
    }

    /**
     */
    public function action()
    {
        $request = Yii::$app->session->get('request', []);
        $data = [];
        foreach ($this->fields as $n => $v) {
            $data[$n] = $v;
        }
        $request['anketa'] = [
            'id'     => $this->anketa->id,
            'fields' => $data,
        ];
        Yii::$app->session->set('request', $request);

        return $data;
    }
}
