<?php

namespace avatar\models\forms;

use avatar\models\UserEnter;
use BitcoinPHP\BitcoinECDSA\BitcoinECDSA;
use common\models\SendLetter;
use common\models\UserDigitalSign;
use common\models\UserSeed;
use common\services\Security;
use cs\Application;
use cs\services\Str;
use Yii;
use yii\base\Model;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;
use yii\helpers\VarDumper;
use yii\web\UploadedFile;

/**
 */
class KoopSign extends Model
{
    public $id;

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            ['id', 'required'],
            ['id', 'integer'],
            ['id', 'exist', 'targetClass' => '\avatar\models\UserEnter'],
        ];
    }

    /**
     * @return array
     * @throws \Exception
     */
    public function action()
    {
        $pk1Object = new BitcoinECDSA();

        $uSign = UserDigitalSign::getPK(Yii::$app->user->id);
        $pk1Object->setPrivateKey($uSign);

        $UserEnter = \avatar\models\UserEnter::findOne($this->id);
        $data = [
            $UserEnter->id,
            $UserEnter->user1_id,
            $UserEnter->user2_id,
            $UserEnter->public_key1,
            $UserEnter->public_key2,
            $UserEnter->created_at,
            $UserEnter->file,
            $UserEnter->name_first,
            $UserEnter->name_last,
            $UserEnter->name_middle,
            $UserEnter->address_pochta,
            $UserEnter->phone,
            $UserEnter->passport_ser,
            $UserEnter->passport_number,
            $UserEnter->passport_vidan_kem,
            $UserEnter->passport_vidan_date,
            $UserEnter->passport_vidan_num,
            $UserEnter->passport_born_date,
            $UserEnter->passport_born_place,
            $UserEnter->passport_scan1,
            $UserEnter->passport_scan2,
            $UserEnter->school_id,
        ];
        $message = join('', $data);
        $hash = hash('sha256', $message);
        $signedMessage1 = $pk1Object->signMessage($hash, true);
        $UserEnter->hash = $hash;
        $UserEnter->user1_id = Yii::$app->user->id;
        $UserEnter->public_key1 = $pk1Object->getAddress();
        $UserEnter->signature1 = $signedMessage1;
        $UserEnter->save();

        return ['sign' => $UserEnter->signature1];
    }

}
