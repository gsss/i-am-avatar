<?php

namespace avatar\models\forms;

use common\models\SendLetter;
use common\models\UserSeed;
use common\services\Security;
use cs\Application;
use cs\services\Str;
use Yii;
use yii\base\Model;
use yii\helpers\ArrayHelper;
use yii\helpers\VarDumper;
use yii\web\UploadedFile;

/**
 */
class AdminQrFileDb2 extends Model
{
    /** @var  \yii\web\UploadedFile */
    public $file;

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            ['file', 'file'],
            ['file', 'validateFile', 'skipOnEmpty' => false],
        ];
    }

    /**
     * @return array customized attribute labels
     */
    public function attributeLabels()
    {
        return [
            'file' => 'Файл БД',
        ];
    }

    public function validateFile($attribute, $params)
    {
        if (!$this->hasErrors()) {
            $m = UploadedFile::getInstance($this, 'file');
            if (is_null($m)) {
                $this->addError($attribute, 'Файл должен быть заполнен');
                return;
            }
            $this->file = $m;
        }
    }

    /**
     * @return \yii\web\Response
     */
    public function action()
    {
        $content = file_get_contents($this->file->tempName);
        $rows = explode("\n", $content);
        $rows2 = [];
        foreach ($rows as $row) {
            $row = explode("\r", $row);
            $rows2[] = explode(';', $row[0]);
        }

        $rows3 = [];
        foreach ($rows2 as $row) {
            $v = substr($row[4], 2);
            $v = substr($v, 0, strlen($v)-1);
            $nums = [];
            $nums[] = substr($v,0,4);
            $nums[] = substr($v,4,4);
            $nums[] = substr($v,8,4);
            $nums[] = substr($v,12,4);
            $v = join(' ', $nums);
            $row[4] = '="' . $v . '"';
            $rows3[] = join(';', $row);
        }
        $content = join("\n", $rows3);

        return Yii::$app->response->sendContentAsFile($content, '1.csv');
    }
}
