<?php

namespace avatar\models\forms;

use avatar\models\WalletETH;
use avatar\models\WalletToken;
use common\models\avatar\UserBill;
use common\models\BillingMain;
use common\models\investment\IcoRequest;
use common\models\PaySystemConfig;
use common\models\SendLetter;
use common\models\UserSeed;
use common\services\Security;
use Yii;
use yii\base\Model;
use yii\helpers\ArrayHelper;
use yii\helpers\VarDumper;
use yii\web\UploadedFile;

/**
 */
class IcoPath4CreateWallet extends Model
{
    /** @var  float */
    public $tokens;

    /** @var  string */
    public $address;

    /** @var  int */
    public $currency;

    /** @var  int */
    private $icoId;

    /** @var  \common\models\investment\Project */
    private $ico;

    /** @var  \common\models\Token токен который продается на ICO */
    private $tokenObject;

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            ['tokens', 'required'],
            ['tokens', 'double'],

            ['currency', 'required'],
            ['currency', 'integer'],

            ['address', 'required'],
            ['address', 'string'],
            ['address', 'toLower'],
            ['address', 'setICO'],
        ];
    }

    public function setICO($attribute, $params)
    {
        if (!$this->hasErrors()) {
            $this->icoId = \Yii::$app->session->get('ico')['id'];
            $this->ico = \common\models\investment\Project::findOne($this->icoId);
            $this->tokenObject = $this->ico->getIco()->getCurrency()->getToken();
        }
    }

    /**
     * Конвертирует массив ошибок от формы чтобы их можно было выдать через JSON
     *
     * @param array $params
     *      [
     *      'amount' => ['Не верное число',...],
     *      ]
     *
     * @return array
     * [[
     *      'name'  => 'amount',
     *      'value' => ['Не верное число',...],
     * ]]
     */
    public function convert($params = null)
    {
        if (is_null($params)) {
            $params = $this->errors;
        }
        if (count($params) == 0) {
            return [];
        }
        $ret = [];
        foreach ($params as $name => $arr) {
            $ret[] = [
                'name'  => $name,
                'value' => $arr,
            ];
        }
        return $ret;
    }

    public function toLower($attribute, $params)
    {
        $this->address = strtolower($this->address);
    }

    /**
     *
     *
     * @return \common\models\investment\IcoRequest
     */
    public function action()
    {
        $request = $this->createRequest();
        $this->setNewStatus($request);

        return $request;
    }

    /**
     * Создает счет и заказ
     *
     * @return IcoRequest
     */
    private function createRequest()
    {
        $c = \common\models\avatar\Currency::findOne($this->currency);

        $id = PaySystemConfig::find()
            ->select(['paysystems_config.id'])
            ->innerJoin('paysystems', 'paysystems.id = paysystems_config.paysystem_id')
            ->where(['paysystems_config.parent_id' => $this->ico->payments_list_id])
            ->andWhere(['paysystems.currency' => $c->code])
            ->scalar();

        $config = PaySystemConfig::findOne($id);
        $paySystem = $config->getPaySystem();

        $tokens = $this->tokens;
        $address = $this->address;
        $currency = $paySystem->getCurrencyObject();
        $sumAfter = $this->convertTokens($tokens, $currency);
        $sumBefore = $sumAfter;

        $billing = BillingMain::add([
            'sum_before'  => $sumBefore,
            'sum_after'   => $sumAfter,
            'source_id'   => $config->paysystem_id,
            'currency_id' => $currency->id,
        ]);

        $request = IcoRequest::add([
            'paysystem_config_id' => $config->id,
            'billing_id'          => $billing->id,
            'tokens'              => $tokens,
            'address'             => $address,
            'user_id'             => Yii::$app->user->id,
            'ico_id'              => $this->icoId,
        ]);

        return $request;
    }

    /**
     * Устанавливает статус "Заказ создан"
     *
     * @param \common\models\investment\IcoRequest $IcoRequest
     */
    public function setNewStatus($IcoRequest)
    {
        $IcoRequest->addStatusToShop([
            'status'  => IcoRequest::STATUS_CREATED,
            'message' => 'Создан заказ',
        ]);
    }

    /**
     * Переводит кол-во токенов в валюту
     *
     * @param float $tokens
     * @param \common\models\avatar\Currency $currency
     *
     * @return float кол-во единиц
     * @throws
     */
    private function convertTokens($tokens, $currency)
    {
        $ico = $this->ico->getIco();

        /** @var \avatar\models\forms\Currency $currencyIco */
        $currencyIco = Currency::findOne($ico->kurs_currency_id);
        if (is_null($currencyIco)) {
            throw new \Exception('Не найдена валюта');
        }

        return \common\models\avatar\Currency::convert($tokens * $ico->kurs, $currencyIco->code, $currency->code);
    }

}

