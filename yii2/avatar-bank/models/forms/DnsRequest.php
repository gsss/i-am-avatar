<?php
namespace avatar\models\forms;

use cs\base\FormActiveRecord;
use Yii;
use yii\base\NotSupportedException;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;
use yii\web\IdentityInterface;

/**
 */
class DnsRequest extends \common\models\school\DnsRequest
{
    public function rules()
    {
        $rules = parent::rules();
        $rules[] = ['name', 'validateNameExist'];

        return $rules;
    }


    public function validateNameExist($attribute, $params)
    {
        if (!$this->hasErrors()) {
            if (self::find()->where(['name' => $this->name, 'is_dns_verified' => [0, 1]])->exists()) {
                $this->addError($attribute, 'Этот домен уже занят');
                return;
            }
        }
    }

}
