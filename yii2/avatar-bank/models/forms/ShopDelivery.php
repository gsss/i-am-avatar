<?php

namespace avatar\models\forms;

use common\models\SendLetter;
use common\models\shop\DeliveryItem;
use common\models\UserSeed;
use common\services\Security;
use cs\Application;
use cs\services\Str;
use Yii;
use yii\base\Model;
use yii\helpers\ArrayHelper;
use yii\helpers\VarDumper;
use yii\web\UploadedFile;

/**
 */
class ShopDelivery extends Model
{
    public $id;

    /** @var  \common\models\shop\DeliveryItem */
    public $delivery;
    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            ['id', 'required'],
            ['id', 'integer'],
            ['id', 'verifyDeliveryItem'],
//            ['id', 'verifyCurrency'],
        ];
    }

    public function verifyDeliveryItem($attribute, $params)
    {
        if (!$this->hasErrors()) {
            $delivery = DeliveryItem::findOne($this->id);
            if (is_null($delivery)) {
                $this->addError($attribute, 'Не найдена доставка');
                return;
            }
            $this->delivery = $delivery;
        }
    }

    /** Если валюта доставки отличается от заказа то будет вызвана ошибка 103 */
    public function verifyCurrency($attribute, $params)
    {
        if (!$this->hasErrors()) {
            $request = Yii::$app->session->get('request');

            if ($request['currency_id'] != $this->delivery->currency_id) {
                $this->addError($attribute, 'валюта доставки отличается от заказа');
                return;
            }
        }
    }

    /**
     */
    public function action()
    {
        $order = Yii::$app->session->get('request', []);
        $order['delivery']['id'] = $this->id;
        $order['delivery']['type'] = $this->delivery->type;

        Yii::$app->session->set('request', $order);

        return $this->delivery;
    }
}
