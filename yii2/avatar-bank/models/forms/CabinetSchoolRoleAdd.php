<?php

namespace avatar\models\forms;

use common\models\avatar\UserBill;
use common\models\koop\Kooperative;
use common\models\RequestTokenCreate;
use common\models\SendLetter;
use common\models\UserSeed;
use common\services\FormAjax\ActiveRecord;
use common\services\Security;
use cs\Application;
use cs\services\Str;
use Yii;
use yii\base\Model;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;
use yii\helpers\VarDumper;

/**
 * @property int id
 * @property int school_id
 * @property string name
 */
class CabinetSchoolRoleAdd extends ActiveRecord
{
    public static function tableName()
    {
        return 'school_role';
    }

    public function rules()
    {
        return [
            ['name', 'required'],
            ['name', 'string'],

            ['school_id', 'required'],
            ['school_id', 'integer'],

            ['id', 'integer'],
        ];
    }
}
