<?php

namespace avatar\models\forms;

use common\models\SendLetter;
use common\models\UserSeed;
use common\services\Security;
use cs\Application;
use cs\services\Str;
use Yii;
use yii\base\Model;
use yii\helpers\ArrayHelper;
use yii\helpers\VarDumper;
use yii\web\UploadedFile;

/**
 */
class CardAction extends Model
{
    /** @var  integer */
    public $card_action;

    public function init()
    {
        /** @var \common\models\UserAvatar $user */
        $user = Yii::$app->user->identity;
        $this->card_action = $user->card_action;
    }

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            ['card_action', 'integer'],
        ];
    }

    /**
     * @return array customized attribute labels
     */
    public function attributeLabels()
    {
        return [
            'card_action' => 'Модель поведения после сканирования QR кода вашей карты',
        ];
    }

    /**
     * @return bool
     */
    public function action()
    {
        /** @var \common\models\UserAvatar $user */
        $user = Yii::$app->user->identity;
        $user->card_action = $this->card_action;
        $user->save();

        return true;
    }
}
