<?php

namespace avatar\models\forms;

use common\components\providers\ETH;
use common\models\avatar\UserBill;
use common\models\school\AdminLink;
use common\models\school\Page;
use common\models\school\PageBlock;
use common\models\school\PageBlockContent;
use common\models\SendLetter;
use common\models\UserSeed;
use common\services\Security;
use cs\Application;
use cs\services\Str;
use Yii;
use yii\base\InvalidConfigException;
use yii\base\Model;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;
use yii\helpers\StringHelper;
use yii\helpers\VarDumper;

/**
 */
class CabinetSchoolPagesMove extends Model
{
    /** @var  int Идентификатор страницы */
    public $id;

    /** @var  Page */
    public $page;

    /** @var  int идентификатор школы куда переместить страницу */
    public $to;

    /** @var  \common\models\school\School Школа куда переместить страницу */
    public $toSchool;

    public function init()
    {
        $page = Page::findOne($this->id);
        if (is_null($page)) {
            throw new InvalidConfigException('Не найдена страница');
        }
        $this->page = $page;
    }

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            ['to', 'required'],
            ['to', 'integer'],
            ['to', 'validateSchool'],
        ];
    }

    public function validateSchool($attribute, $params)
    {
        if (!$this->hasErrors()) {
            $s = \common\models\school\School::findOne($this->to);
            if (is_null($s)) {
                $this->addError($attribute, 'Нет такой школы');
                return;
            }
            $isMy = false;
            if ($s->user_id == Yii::$app->user->id) {
                $isMy = true;
            }

            $isAdmin = false;
            $link = AdminLink::findOne(['school_id' => $this->to, 'user_id' => Yii::$app->user->id]);
            if (!is_null($link)) {
                $isAdmin = true;
            }
            if ($isAdmin == false and $isMy == false) {
                $this->addError($attribute, 'Нельзя перемещать не в свою школу');
                return;
            }
            $this->toSchool = $s;
        }
    }

    /**
     * Переносит страницу
     *
     * @return bool
     */
    public function action()
    {
        $this->page->school_id = $this->to;
        $this->page->header_id = null;
        $this->page->footer_id = null;
        $this->page->save();

        return true;
    }
}
