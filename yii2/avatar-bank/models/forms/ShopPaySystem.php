<?php

namespace avatar\models\forms;

use common\models\BillingMain;
use common\models\BillingMainClass;
use common\models\PaySystemConfig;
use common\models\piramida\Billing;
use common\models\SendLetter;
use common\models\shop\DeliveryItem;
use common\models\UserSeed;
use common\services\Security;
use cs\Application;
use cs\services\Str;
use Yii;
use yii\base\Model;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;
use yii\helpers\VarDumper;
use yii\web\Request;
use yii\web\UploadedFile;

/**
 */
class ShopPaySystem extends Model
{
    public $item;

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            ['item', 'required'],
            ['item', 'integer'],
        ];
    }

    /**
     */
    public function action()
    {
        $request = Yii::$app->session->get('request');

        $delivery = DeliveryItem::findOne($request['delivery']['id']);
        $PaySystemConfig = PaySystemConfig::findOne($this->item);
        $BillingMainClass = BillingMainClass::findOne(['name' => '\common\models\shop\Request']);

        // Создаю счет на оплату
        $billing = BillingMain::add([
            'sum_before'  => $delivery->price + $request['price'],
            'sum_after'   => $delivery->price + $request['price'],
            'source_id'   => $PaySystemConfig->paysystem_id,
            'class_id'    => $BillingMainClass->id,
            'currency_id' => $request['currency_id'],
            'config_id'   => $PaySystemConfig->id,
        ]);

        // получаю реферала
        $parent_id = null;
        $v = Yii::$app->session->get('ReferalProgram');
        if (!is_null($v)) {
            $parent_id = $v['parent_id'];
        }

        // Создаю заявку
        $requestClass = \common\models\shop\Request::add([
            'user_id'     => Yii::$app->user->id,
            'school_id'   => $request['school_id'],
            'address'     => $request['address']['index'] . ', ' . $request['address']['address'] . ', ' . $request['address']['fio'],
            'comment'     => $request['address']['comment'],
            'phone'       => $request['address']['phone'],
            'dostavka_id' => $request['delivery']['id'],
            'price'       => $request['price'],
            'currency_id' => $request['currency_id'],
            'billing_id'  => $billing->id,
            'sum'         => $delivery->price + $request['price'],
            'parent_id'   => $parent_id,
        ]);

        // Добавляю статус: Заказ отправлен в магазин
        $requestClass->addStatusToShop(\common\models\shop\Request::STATUS_SEND_TO_SHOP);

        // Добавляю статус: Выставлен счет на оплату клиенту
        {
            $currency = \common\models\avatar\Currency::findOne($requestClass->currency_id);
            $delivery = DeliveryItem::findOne($requestClass->dostavka_id);
            $requestClass->addStatusToClient([
                'status' => \common\models\shop\Request::STATUS_ORDER_DOSTAVKA,
                'message' => join("\n", [
                    'Стоимость с учетом доставки: ' . Yii::$app->formatter->asDecimal($requestClass->sum/pow(10, $currency->decimals), $currency->decimals),
                    'Доставка: ' . $delivery->name,
                    'Адрес: ' . $requestClass->address,
                ]),
            ]);
        }

        // Записываю данные анкеты, если есть
        if (isset($request['anketa'])) {
            $anketa = \common\models\school\AnketaShop::findOne($request['anketa']['id']);
            \common\models\school\AnketaShopData::add([
                'request_id' => $requestClass->id,
                'anketa_id'  => $anketa->id,
                'data'       => Json::encode($request['anketa']['fields']),
            ]);
        }

        // Добавляю товары в заказ
        foreach ($request['productList'] as $id => $count) {
            \common\models\shop\RequestProduct::add([
                'request_id' => $requestClass->id,
                'product_id' => $id,
                'count'      => $count,
            ]);
        }

        return [
            'request'         => $requestClass,
            'billing'         => $billing,
            'PaySystemConfig' => $PaySystemConfig->id,
            'delivery'        => $delivery,
        ];
    }
}
