<?php
namespace avatar\models\forms;

use common\models\UserAvatar;
use cs\base\FormActiveRecord;
use cs\services\VarDumper;
use Yii;
use yii\base\Model;
use yii\base\NotSupportedException;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\web\IdentityInterface;

/**
 * @property integer id
 * @property string  code
 */
class GoogleConfirm extends Model
{
    public $code;
    /** @var  \common\models\UserAvatar */
    public $_user;

    public function rules()
    {
        return [
            ['code', 'required'],
            ['code', 'trim'],
            ['code', 'removeSpace'],
            ['code', 'string', 'length' => 6],
            ['code', 'validateCode'],
        ];
    }

    public function removeSpace()
    {
        if (!$this->hasErrors()) {
            $this->code = str_replace(' ','', $this->code);
        }
    }

    public function validateCode()
    {
        if (!$this->hasErrors()) {
            $id = Yii::$app->session->get('userLogin');
            if (is_null($id)) {
                $this->addError('code', Yii::t('c.HS07bGa1P0', 'Время сессии истекло, зайдите заново'));
                return;
            }
            $user = UserAvatar::findOne($id);
            $this->_user = $user;

            if (!$user->validateGoogleAuthCode($this->code)) {
                $this->addError('code', Yii::t('c.HS07bGa1P0', 'Код не верный'));
                return;
            }
        }
    }

    /**
     * @return \common\models\UserAvatar
     */
    public function getUser()
    {
        return $this->_user;
    }
}
