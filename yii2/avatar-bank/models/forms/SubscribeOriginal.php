<?php
/**
 * Created by PhpStorm.
 * User: Бог-Творец
 * Date: 22.11.2016
 * Time: 20:20
 */

namespace avatar\models\forms;


use avatar\services\Html;
use common\widgets\FileUpload3\FileUpload;
use cs\base\FormActiveRecord;
use yii\base\Exception;
use yii\widgets\ActiveForm;

class SubscribeOriginal extends FormActiveRecord
{
    public static function tableName()
    {
        return 'gs_subscribe_original';
    }


    function formAttributes()
    {
        return [

            [
                'subject',
                'Описание',
                1,
                'string',
                ['max' => 255],
            ],
            [
                'html',
                'Полный текст',
                0,
                'string',
                'widget' => [
                    '\common\widgets\HtmlContent\HtmlContent',
                ]
            ],
        ];
    }
} 