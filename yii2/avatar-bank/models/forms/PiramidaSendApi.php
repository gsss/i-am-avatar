<?php

namespace avatar\models\forms;

use common\models\avatar\UserBill;
use common\models\avatar\UserBillOperation;
use cs\services\Str;
use cs\services\VarDumper;
use Yii;
use yii\base\Exception;
use yii\base\Model;
use cs\Widget\FileUpload3\FileUpload;
use yii\db\Query;
use yii\helpers\Html;
use yii\helpers\StringHelper;

/**
 */
class PiramidaSendApi extends Model
{
    public $billing_id;

    public $address;

    public $amount;

    public $comment;

    public $password;

    public function rules()
    {
        return [
            [['billing_id', 'address', 'amount'], 'required'],
            [['address'], 'normalizeAddress'],
            [['amount'], 'normalize'],
            [['address', 'comment'], 'string'],
            [['billing_id'], 'integer'],
        ];
    }

    public function normalize($attribute, $params)
    {
        if (!$this->hasErrors()) {
            $this->amount = str_replace(',', '.', $this->amount);
            if (!preg_match('/[0-9-.]/', $this->amount)) {
                $this->addError($attribute, 'Не верные символы');
                return;
            }
        }
    }

    public function normalizeAddress($attribute, $params)
    {
        if (!$this->hasErrors()) {
            $address = $this->address;
            if (StringHelper::startsWith($address, 'bitcoin')) {
                $address = substr($address,8);
            }
            $arr = explode('?', $address);
            if (count($arr) > 1) {
                $address = $arr[0];
            }
            $this->address = $address;
        }
    }

    /**
     * Отправляет деньги на счет
     *
     * @return bool|string
     * @throws Exception
     */
    public function send()
    {
        $billing = UserBill::findOne($this->billing_id);
        $wallet = $billing->getWalletAvatar();
        try {
            $transaction = $wallet->pay([$this->address => [
                'amount'  => $this->amount,
                'comment' => $this->comment,
            ]]);
        } catch (\Exception $e) {
            $this->setError($e->getMessage());
            return false;
        }

        return $transaction;
    }

    /**
     * Устанавливает ошибку
     *
     * {"code":0,"msg":"No usable unspent outputs [2]"} // не хватает денег на счету
     *
     * @param $message
     */
    public function setError($message)
    {
        if (StringHelper::startsWith($message, 'Values should be more than dust')) {
            $arr = explode('(', $message);
            $v = 0;
            if (count($arr) > 1) {
                $v = substr($arr[1], 0, strlen($arr[1]) - 1);
            }
            $this->addError('amount', 'Значение должно быть больше чем ' . \Yii::$app->formatter->asDecimal($v / 100000000, 8));
        } else if (StringHelper::startsWith($message, 'A Server Error has occurred')) {
            if (strpos($message, 'No usable unspent outputs') !== false) {
                $this->addError('amount', 'Недостаточно денег в кошельке или они еще не подтверждены');
            }
        } else {
            if ($message == 'Wallet balance too low') {
                $this->addError('amount', 'Слишком мало денег на счету');
            } else {
                if (StringHelper::startsWith($message, 'Invalid address')) {
                    $this->addError('address', 'Не верный адрес');
                } else {
                    if (StringHelper::startsWith($message, 'Wallet balance too low to pay the fee')) {
                        $arr = explode('[', $message);
                        $v = 0;
                        if (count($arr) > 1) {
                            $v = substr($arr[1], 0, strlen($arr[1]) - 1);
                        }
                        $this->addError('amount', 'Баланс слишком мал чтобы заплатить комиссию ' . Yii::$app->formatter->asDecimal($v / 100000000, 8));
                    } else {
                        $this->addError('amount', $message);
                    }
                }
            }
        }
    }

}
