<?php

namespace avatar\models\forms;

use avatar\modules\Piramida\PaySystems\Base\Model;
use common\models\avatar\UserBill;
use common\models\avatar\UserBillOperation;
use common\models\UserDocument;
use common\payment\BitCoinBlockTrailPayment;
use cs\base\FormActiveRecord;
use cs\services\BitMask;
use yii\base\Exception;
use yii\db\ActiveRecord;
use yii\db\Query;
use yii\helpers\Json;
use yii\helpers\VarDumper;

/**
 */
class UserDocumentStep2 extends Model
{
    public $password;

    /** @var  \common\models\UserDocument */
    public $_doc;

    public function rules()
    {
        return [
            ['password', 'required'],
            ['password', 'validatePassword'],
        ];
    }

    public function init()
    {
        $id = \Yii::$app->request->get('id');
        $this->_doc = UserDocument::findOne($id);
    }

    public function validatePassword($attribute, $params)
    {
        if (!$this->hasErrors()) {
            /** @var \common\models\UserAvatar $user */
            $user = \Yii::$app->user->identity;

            $passportWalletEth = $user->getWalletEthPassport();
            if (!$passportWalletEth->billing->validatePassword($this->password)) {
                $this->addError($attribute, 'Не верный пароль');
            }
        }
    }

    public function action()
    {
        /** @var \common\models\UserAvatar $user */
        $user = \Yii::$app->user->identity;

        $passportWalletEth = $user->getWalletEthPassport();

        $contract = '0x52ed3c202c4652f952a1561ac0c030f1ed9460ff';
        if (!YII_ENV_PROD) {
            $contract = '0xfF6C78BFedF1bdb2D85071A9040798F8057A0A36';
        }

        $txid = $passportWalletEth->contract(
            $this->password,
            $contract,
            '[{"constant":true,"inputs":[],"name":"getDocumentsCount","outputs":[{"name":"","type":"uint256"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":false,"inputs":[{"name":"hash","type":"string"},{"name":"link","type":"string"},{"name":"data","type":"string"}],"name":"registerDocument","outputs":[],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":false,"inputs":[{"name":"id","type":"uint256"}],"name":"addSignature","outputs":[],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":true,"inputs":[{"name":"id","type":"uint256"}],"name":"getDocument","outputs":[{"name":"hash","type":"string"},{"name":"link","type":"string"},{"name":"data","type":"string"},{"name":"creator","type":"address"},{"name":"date","type":"uint256"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":false,"inputs":[],"name":"kill","outputs":[],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":true,"inputs":[{"name":"","type":"uint256"}],"name":"documentsIds","outputs":[{"name":"hash","type":"string"},{"name":"link","type":"string"},{"name":"data","type":"string"},{"name":"creator","type":"address"},{"name":"date","type":"uint256"},{"name":"signsCount","type":"uint256"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":true,"inputs":[{"name":"id","type":"uint256"}],"name":"getDocumentSignsCount","outputs":[{"name":"","type":"uint256"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":false,"inputs":[{"name":"newOwner","type":"address"}],"name":"changeOwner","outputs":[],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":true,"inputs":[{"name":"id","type":"uint256"},{"name":"index","type":"uint256"}],"name":"getDocumentSign","outputs":[{"name":"member","type":"address"},{"name":"date","type":"uint256"}],"payable":false,"stateMutability":"view","type":"function"},{"inputs":[],"payable":false,"stateMutability":"nonpayable","type":"constructor"},{"anonymous":false,"inputs":[{"indexed":false,"name":"id","type":"uint256"},{"indexed":false,"name":"member","type":"address"}],"name":"DocumentSigned","type":"event"},{"anonymous":false,"inputs":[{"indexed":false,"name":"id","type":"uint256"},{"indexed":false,"name":"hash","type":"string"}],"name":"DocumentRegistered","type":"event"}]',
            'registerDocument',
            [
                $this->_doc->hash,
                'https://avatarnetwork.io/documents' . '?' . 'id' . '=' . $this->_doc->id,
                Json::encode([
                    'id'             => $this->_doc->id,
                    'linkAvatarBank' => 'https://avatarnetwork.io/documents' . '?' . 'id' . '=' . $this->_doc->id,
                    'data'           => $this->_doc->data,
                ]),
            ],
            'true'
        );

        $this->_doc->txid = $txid;
        $this->_doc->save();

        // добавляю операцию в кошелек Эфировский
        $operation = new UserBillOperation([
            'transaction' => $txid,
            'message'     => 'Регистрация документа',
            'bill_id'     => $passportWalletEth->billing->id,
            'type'        => UserBillOperation::TYPE_OUT,
            'created_at'  => time(),
        ]);
        $operation->save();
    }
}