<?php

namespace avatar\models\forms;

use app\models\ActionsRange;
use common\components\DbMessageSourceSky;
use common\components\EmailVerifier;
use common\models\Config;
use common\models\Language;
use common\models\language\Access;
use common\services\Debugger;
use GuzzleHttp\Query;
use yii\base\Model;
use Yii;
use yii\helpers\ArrayHelper;
use yii\helpers\VarDumper;

class LanguageSettings extends Model
{
    /**
     * @var int стратегия для перевода \common\components\DbMessageSourceSky::STRATEGY_TRANSLATE_*
     */
    public $strategy;

    public function init()
    {
        $this->strategy = Config::get(DbMessageSourceSky::CONFIG_KEY);
    }

    public function rules()
    {
        return [
            [['strategy'], 'integer'],
        ];
    }

    /**
     * Сохраняет настройку в конфиг `\common\models\Config`
     *
     * @return bool
     */
    public function save()
    {
        Config::set(DbMessageSourceSky::CONFIG_KEY, $this->strategy);

        return true;
    }

    public function attributeLabels()
    {
        return [
            'strategy' => 'Стратегия обработки потерянных переводов',
        ];
    }
}