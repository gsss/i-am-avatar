<?php

namespace avatar\models\forms;


use common\models\school\File;
use cs\services\Url;
use cs\services\VarDumper;
use yii\db\ActiveRecord;
use yii\helpers\Json;

/**
 *
 * @property int            id
 * @property int            school_id
 * @property string         tz
 * @property string         tz_hash
 *
 */
class CabinetKoopPodryadStep3 extends \iAvatar777\services\FormAjax\ActiveRecord
{
    public static function tableName()
    {
        return 'koop_podryad';
    }

    public function rules()
    {
        return [
            ['tz', 'required'],
            ['tz', 'string'],

            ['tz_hash', 'required'],
            ['tz_hash', 'string'],
        ];
    }

    public function attributeWidgets()
    {
        return [
            'tz' => [
                'class'    => '\iAvatar777\widgets\FileUpload8\FileUpload',
                'update'   => \avatar\controllers\CabinetSchoolPagesConstructorController::getUpdate(),
                'settings' => \avatar\controllers\CabinetSchoolPagesConstructorController::getSettingsLibrary($this->school_id, 30),
                'events'   => [
                    'onDelete' => function ($item) {
                        $r = new \cs\services\Url($item->image);
                        $d = pathinfo($r->path);
                        $start = $d['dirname'] . '/' . $d['filename'];

                        File::deleteAll(['like', 'file', $start]);
                    },
                ],
            ],
        ];
    }

    public function save($runValidation = true, $attributeNames = null)
    {
        // получаю хеш файла
        $file = $this->tz;
        $s = \common\models\school\School::findOne($this->school_id);
        $url = new Url($file);

        $cloud = $s->getCloud();
        if (is_null($cloud)) {
            /** @var \common\services\AvatarCloud $cloud2 */
            $cloud2 = \Yii::$app->AvatarCloud;
            $response = $cloud2->_post(null, 'upload/sha256', ['file' => $url->path]);
        } else {
            $response = $cloud->call('upload/sha256', ['file' => $url->path], []);
        }
        $data = Json::decode($response->content);
        $this->tz_hash = $data['data']['hash'];

        // сохраняю
        parent::save($runValidation, $attributeNames);
    }
}