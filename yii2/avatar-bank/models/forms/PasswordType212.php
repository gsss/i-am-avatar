<?php

namespace avatar\models\forms;

use common\models\avatar\UserBill;
use common\models\SendLetter;
use common\models\UserAvatar;
use common\models\UserSeed;
use common\services\Security\AES;
use cs\Application;
use cs\services\Str;
use Yii;
use yii\base\Model;
use yii\helpers\ArrayHelper;
use yii\helpers\VarDumper;

/**
 */
class PasswordType212 extends Model
{
    /** @var  string SEEDS */
    public $seeds;

    /** @var  string пароль от кабинета по которому были зашифрованы старые коешельки (устанавливается в tryToUnlock) */
    public $_passwordCabinetOld;

    /** @var  string key32 из tryToUnlock */
    public $_key32;

    /** @var  \common\models\UserSeed  (устанавливается в tryToUnlock) */
    public $_hashSeedsObject;

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            [['seeds'], 'required'],
            ['seeds', 'string'],
            ['seeds', 'normalizeSeeds'],
            ['seeds', 'tryToUnlock'], // устанавливает $_passwordCabinetOld, $_hashSeedsObject, $_key32
        ];
    }

    public function normalizeSeeds($attribute, $params)
    {
        if (!$this->hasErrors()) {
            $ret = [];
            $arr = explode("\n", $this->seeds);
            foreach ($arr as $row) {
                $arr2 = explode(" ", $row);
                foreach ($arr2 as $word) {
                    if (trim($word) != '') {
                        $ret[] = trim($word);
                    }
                }
            }
            $this->seeds = join(' ', $ret);
        }
    }

    public function tryToUnlock($attribute, $params)
    {
        if (!$this->hasErrors()) {
            // распаковываю пароль от кабинета старый
            $sha256 = hash('sha256', $this->seeds);
            $key32 = substr($sha256, 0, 32);
            $hashSeedsObject = UserSeed::findOne(Yii::$app->user->id);
            $userCabinetPassword = \common\services\Security\AES::decrypt256CBC($hashSeedsObject->seeds, $key32);
            if ($userCabinetPassword == '') {
                $this->addError($attribute, 'Не верный SEEDS');
                return;
            }
            $this->_passwordCabinetOld = $userCabinetPassword;
            Yii::info(\yii\helpers\VarDumper::dumpAsString([$this->seeds, $userCabinetPassword]), 'avatar\models\forms\PasswordType212::tryToUnlock');
            Yii::info(\yii\helpers\VarDumper::dumpAsString([$this->seeds, $userCabinetPassword]), 'info\avatar\models\forms\PasswordType212::tryToUnlock');
            $this->_hashSeedsObject = $hashSeedsObject;
            $this->_key32 = $key32;
        }
    }

    /**
     */
    public function action()
    {
        $this->clearPassword();

        /** @var \common\models\UserAvatar $user */
        $user = Yii::$app->user->identity;

        $user->password_save_type = UserBill::PASSWORD_TYPE_OPEN_CRYPT;
        $user->wallets_is_locked = 0;
        $user->save();

        // удаляю seeds
        $this->_hashSeedsObject->delete();

        return true;
    }


    /**
     * @throws \Exception
     */
    private function clearPassword()
    {
        $billingList = UserBill::find()
            ->where([
                'user_id'       => Yii::$app->user->id,
                'mark_deleted'  => 0,
                'password_type' => UserBill::PASSWORD_TYPE_HIDE_CABINET,
            ])
            ->all();

        $t = \Yii::$app->db->beginTransaction();
        try {
            /** @var \common\models\avatar\UserBill $billing */
            foreach ($billingList as $billing) {
                $passwordWallet = $billing->getPassword($this->_passwordCabinetOld);
                if ($passwordWallet == '') {
                    \Yii::warning('Не верный пароль к кошельку '.$billing->id, 'avatar\common\models\avatar\UserBill::changePassword');
                    throw new \Exception('Не верный пароль к кошельку ' . $billing->id);
                }
                $ret = $billing->setPasswordOpenCrypt($passwordWallet);
            }
            $t->commit();
        } catch (\Exception $e) {
            $t->rollBack();
            throw $e;
        }
    }
}
