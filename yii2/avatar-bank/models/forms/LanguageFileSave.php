<?php

namespace avatar\models\forms;

use app\models\ActionsRange;
use common\components\EmailVerifier;
use common\models\Config;
use common\models\Language;
use common\models\language\Access;
use common\models\language\Message;
use common\models\language\SourceMessage;
use common\services\Debugger;
use GuzzleHttp\Query;
use Imagine\Image\Box;
use yii\base\Model;
use Yii;
use yii\debug\models\search\Debug;
use yii\helpers\ArrayHelper;
use yii\helpers\FileHelper;
use yii\helpers\VarDumper;
use yii\imagine\Image;
use yii\web\UploadedFile;

class LanguageFileSave extends Model
{
    const MAX_FILE_SIZE = 50000000;

    /** @var \yii\web\UploadedFile устанавливается в `validateFile` */
    public $file;

    /** @var  int идентификатор строки source_message.id */
    public $id;

    /** @var  string код языка */
    public $language;

    public function rules()
    {
        return [
            [['id', 'language'], 'required'],
            [['id'], 'integer'],
            [['language'], 'string', 'max' => 2],
            [['file'], 'validateFile', 'skipOnEmpty' => false],
        ];
    }

    public function validateFile($attribute, $params)
    {
        if (!$this->hasErrors()) {
            $file = UploadedFile::getInstanceByName('file');
            if (is_null($file)) {
                $this->addError($attribute, 'Файл не был загружен');
                return;
            }
            $file = UploadedFile::getInstanceByName('file');
            if ($file->error == 2) {
                $this->addError($attribute, 'Нельзя загружать файл больше 50MB');
                Yii::$app->session->setFlash('Error', 'Нельзя загружать файл больше 50MB');
                return;
            } else if ($file->size > self::MAX_FILE_SIZE) {
                $this->addError($attribute, 'Нельзя загружать файл больше 50MB');
                Yii::$app->session->setFlash('Error', 'Нельзя загружать файл больше 50MB');
                return;
            }
            $info = pathinfo($file->name);
            if (strtolower($info['extension']) != 'pdf') {
                $this->addError($attribute, 'Можно загрузить только файлы PDF');
                Yii::$app->session->setFlash('Error', 'Можно загрузить только файлы PDF');
                return;
            }
            $this->file = $file;
        }
    }

    /**
     * Сохраняет файл
     *
     * @return bool
     */
    public function action()
    {
        $file = $this->file;
        $info = pathinfo($file->name);
        $ext = strtolower($info['extension']);

        // формирую имя файла
        $hash = Yii::$app->security->generateRandomString();
        $b = $hash . '_b' . '.' . $ext;
        $folder = Yii::getAlias(\backend\models\form\LanguageImageSave::$pathSave1 . '/images/translation/' . $this->language);
        if (!file_exists($folder)) FileHelper::createDirectory($folder);
        $path_b = '/images/translation/' . $this->language . '/' . $b;
        $fullPath_b = Yii::getAlias(\backend\models\form\LanguageImageSave::$pathSave1 . $path_b);

        // удаляю старые файлы
        if (file_exists($fullPath_b)) {
            unlink($fullPath_b);
        }

        // сохраняю файл
        $file->saveAs($fullPath_b);

        // сохраняю в БД
        $messageObject = Message::findOne([
            'id'       => $this->id,
            'language' => $this->language,
        ]);
        if (is_null($messageObject)) {
            $messageObject = new Message([
                'id'       => $this->id,
                'language' => $this->language,
            ]);
        }
        $messageObject->translation = $path_b;
        $messageObject->save();

        // сбрасываю кеш
        Yii::$app->cacheLanguages->flush();

        return true;
    }
}