<?php

namespace avatar\models\forms;


use common\models\school\File;
use cs\services\Url;
use yii\db\ActiveRecord;
use yii\helpers\Json;

/**
 *
 * @property int            id
 * @property int            school_id
 * @property string         report
 *
 */
class CabinetKoopPodryadStep7 extends \iAvatar777\services\FormAjax\ActiveRecord
{
    public static function tableName()
    {
        return 'koop_podryad';
    }

    public function rules()
    {
        return [
            ['report', 'required'],
            ['report', 'string'],
        ];
    }

    public function attributeWidgets()
    {
        return [
            'report' => [
                'class'    => '\iAvatar777\widgets\FileUpload8\FileUpload',
                'update'   => \avatar\controllers\CabinetSchoolPagesConstructorController::getUpdate(),
                'settings' => \avatar\controllers\CabinetSchoolPagesConstructorController::getSettingsLibrary($this->school_id, 29),
                'events'   => [
                    'onDelete' => function ($item) {
                        $r = new \cs\services\Url($item->image);
                        $d = pathinfo($r->path);
                        $start = $d['dirname'] . '/' . $d['filename'];

                        File::deleteAll(['like', 'file', $start]);
                    },
                ],
            ],
        ];
    }
}