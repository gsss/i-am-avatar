<?php

namespace backend\models\form;

use app\models\ActionsRange;
use cabinet\base\Str;
use cabinet\models\NewsText;
use common\components\EmailVerifier;
use common\models\Config;
use common\models\Language;
use common\models\language\Message;
use common\services\Debugger;
use yii\base\Model;
use Yii;

class SearchLanguage extends Model
{
    public $term;

    /** @var  \common\models\language\Message[] */
    public $messages;

    public function rules()
    {
        $columns = ['term'];

        return [
            [$columns, 'string'],
        ];
    }

    /**
     * Сохраняет все языки в БД
     *
     * @return bool
     */
    public function search()
    {
        $messages = Message::find()->where(['translation' => $this->term])->all();
        $this->messages = $messages;
    }

    public function attributeLabels()
    {
        return [
            'term' => 'Запрос',
        ];
    }

}