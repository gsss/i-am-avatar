<?php

namespace avatar\models\forms;

use common\models\avatar\CurrencyLink;
use common\models\school\File;
use cs\base\FormActiveRecord;
use cs\services\Url;
use cs\services\VarDumper;
use Yii;
use yii\base\Model;
use yii\base\NotSupportedException;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\web\IdentityInterface;

/**
 */
class CoinEdit extends Model
{
    public $code;
    public $name;
    public $image;
    public $decimals;

    /** @var  \common\models\avatar\Currency */
    public $currencyExt;

    /** @var  \common\models\piramida\Currency */
    public $currencyInt;

    public function rules()
    {
        return [
            ['code', 'required'],
            ['code', 'string'],

            ['name', 'required'],
            ['name', 'string'],

            ['image', 'string'],
            ['image', 'url'],

            ['decimals', 'required'],
            ['decimals', 'integer', 'min' => 0, 'max' => 18],
        ];
    }

    public function attributeLabels()
    {
        return [
            'code'     => 'Код валюты',
            'name'     => 'Название валюты',
            'image'    => 'Картинка валюты',
            'decimals' => 'Картинка валюты',
            'amount'   => 'Атомов на эмиссию',
        ];
    }

    public function attributeHints()
    {
        return [
            'code'     => 'Обычно англ большие буквы 0-10 шт',
            'decimals' => 'от 0 до 18, обычно 2',
        ];
    }

    public function init2(\common\models\piramida\Currency $currency)
    {
        $this->currencyInt = $currency;
        $link = CurrencyLink::findOne(['currency_int_id' => $currency->id]);
        $currencyExt = \common\models\avatar\Currency::findOne($link->currency_ext_id);
        $this->currencyExt = $currencyExt;

        $this->decimals = $currency->decimals;
        $this->code = $currency->code;
        $this->name = $currencyExt->title;
        $this->image = $currencyExt->image;
    }

    public function edit()
    {
        $this->currencyExt->code = $this->code;
        $this->currencyExt->decimals = $this->decimals;
        $this->currencyExt->title = $this->name;
        $this->currencyExt->image = $this->image;
        $this->currencyExt->save();

        $this->currencyInt->code = $this->code;
        $this->currencyInt->decimals = $this->decimals;
        $this->currencyInt->name = $this->name;
        $this->currencyInt->save();
    }
}
