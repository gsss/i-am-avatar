<?php

namespace avatar\models\forms;

use avatar\modules\HumanDesign\calculate\HumanDesignAmericaCom;
use common\models\HD;
use common\models\HDtown;
use cs\base\BaseForm;
use cs\services\SitePath;
use cs\services\VarDumper;
use cs\web\Exception;
use Yii;
use yii\base\Model;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use cs\Widget\FileUpload2\FileUpload;

/**
 *
 */
class ProfileHumanDesignCalc extends BaseForm
{
    /** @var  \DateTime */
    public $date;

    /** @var  string */
    public $time;

    public $point;

    /** @var  int страна */
    public $country;

    /** @var  int город */
    public $town;

    public function init()
    {
        if (!Yii::$app->user->isGuest) {
            /** @var \common\models\UserAvatar $user */
            $user = Yii::$app->user->identity;
            $birth_date = $user->birth_date;
            if ($birth_date) {
                $this->date = new \DateTime($birth_date);
            }
            $birth_time = $user->birth_time;
            if ($birth_time) {
                $this->time = substr($birth_time, 0, 5);
            }
            $birth_lat = $user->birth_lat;
            $birth_lng = $user->birth_lng;
            if (!is_null($birth_lat) and !is_null($birth_lng)) {
                $this->point = [
                    'lat' => $birth_lat,
                    'lng' => $birth_lng,
                ];
            }
        }
    }

    function __construct($fields = [])
    {
        static::$fields = [
            ['date', 'Дата рождения', 1, 'cs\Widget\DatePicker\Validator',
                'widget' => [
                    'cs\Widget\DatePicker\DatePicker',
                    [
                        'dateFormat' => 'php:d.m.Y'
                    ]
                ]
            ],
            ['time', 'Время рождения', 1, 'cs\Widget\TimePiker\Validator', [], 'чч:мм',
                'widget' => ['cs\Widget\TimePiker\TimePiker']
            ],
            ['country', 'Страна', 1, 'integer'],
            ['town', 'Город', 0, 'integer'],
        ];
        parent::__construct($fields);
    }
}