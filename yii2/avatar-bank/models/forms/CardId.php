<?php
namespace avatar\models\forms;

use common\models\school\File;
use cs\base\FormActiveRecord;
use cs\services\Url;
use cs\services\VarDumper;
use Yii;
use yii\base\NotSupportedException;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\web\IdentityInterface;

/**
 */
class CardId extends FormActiveRecord
{
    public $_school_id;
    public $_type_id;

    public function behaviors()
    {
        return [
            [
                'class' => '\yii\behaviors\TimestampBehavior',
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at'],
                ],
                // если вместо метки времени UNIX используется datetime:
                // 'value' => new Expression('NOW()'),
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'card_id';
    }

    public function formAttributes()
    {
        return [
            [
                'name_first',
                'Имя',
                1,
                'string',
                ['max' => 100],
            ],
            [
                'card_num',
                'Имя',
                0,
                'integer',
            ],
            [
                'name_patronymic',
                'Отчество',
                0,
                'string',
                ['max' => 100],
            ],
            [
                'name_last',
                'Фамилия',
                0,
                'string',
                ['max' => 100],
            ],
            [
                'date_born',
                'Дата рождения',
                0,
                'cs\Widget\DatePicker\Validator',
                'widget' => [
                    '\common\widgets\DatePicker\DatePicker',
                    [
                        'dateFormat' => 'php:d.m.Y'
                    ]
                ]
            ],
            [
                'date_get',
                'Дата получения',
                0,
                'cs\Widget\DatePicker\Validator',
                'widget' => [
                    '\common\widgets\DatePicker\DatePicker',
                    [
                        'dateFormat' => 'php:d.m.Y'
                    ]
                ]
            ],
            [
                'place_born',
                'Место рождения',
                0,
                'string',
                ['max' => 100],
            ],
            [
                'image_top',
                'Изображение переднее',
                0,
                '\common\widgets\FileUpload7\Validator',
                'widget' => [
                    '\common\widgets\FileUpload7\FileUpload',
                    [
                        'update'    => [
                            [
                                'function' => 'crop',
                                'index'    => 'crop',
                                'options'  => [
                                    'width'  => 1000,
                                    'height' => 600,
                                    'mode'   => 'MODE_THUMBNAIL_CUT',
                                ],
                            ],
                        ],
                        'settings'  => \avatar\controllers\CabinetSchoolPagesConstructorController::getSettings($this->_school_id, 19),
                        'events'    => [
                            'onDelete' => function (CardId $item) {
                                $r = new \cs\services\Url($item->image);
                                $d = pathinfo($r->path);
                                $start = $d['dirname'] . '/' . $d['filename'];

                                File::deleteAll(['like', 'file', $start]);
                            },
                        ],
                    ],
                ],
            ],
            [
                'image_back',
                'Изображение переднее',
                0,
                '\common\widgets\FileUpload7\Validator',
                'widget' => [
                    '\common\widgets\FileUpload7\FileUpload',
                    [
                        'update'    => [
                            [
                                'function' => 'crop',
                                'index'    => 'crop',
                                'options'  => [
                                    'width'  => 1000,
                                    'height' => 600,
                                    'mode'   => 'MODE_THUMBNAIL_CUT',
                                ],
                            ],
                        ],
                        'settings'  => \avatar\controllers\CabinetSchoolPagesConstructorController::getSettings($this->_school_id, 20),
                        'events'    => [
                            'onDelete' => function (CardId $item) {
                                $r = new \cs\services\Url($item->image);
                                $d = pathinfo($r->path);
                                $start = $d['dirname'] . '/' . $d['filename'];

                                File::deleteAll(['like', 'file', $start]);
                            },
                        ],
                    ],
                ],
            ],

        ];
    }
}
