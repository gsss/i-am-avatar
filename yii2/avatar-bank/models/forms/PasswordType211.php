<?php

namespace avatar\models\forms;

use common\models\avatar\UserBill;
use common\models\SendLetter;
use common\models\UserAvatar;
use common\models\UserSeed;
use common\services\Security\AES;
use cs\Application;
use cs\services\Str;
use Yii;
use yii\base\Model;
use yii\helpers\ArrayHelper;
use yii\helpers\VarDumper;
use yii\web\User;

/**
 */
class PasswordType211 extends Model
{
    /** @var  string SEEDS */
    public $seeds;

    /** @var  string пароль от кабинета */
    public $password;

    /** @var  string пароль от кабинета по которому были зашифрованы старые коешельки (устанавливается в tryToUnlock) */
    public $_passwordCabinetOld;

    /** @var  string key32 из tryToUnlock */
    public $_key32;

    /** @var  \common\models\UserSeed  (устанавливается в tryToUnlock) */
    public $_hashSeedsObject;

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            [['seeds', 'password'], 'required'],
            ['seeds', 'string'],
            ['seeds', 'normalizeSeeds'],
            ['seeds', 'tryToUnlock'], // устанавливает $_passwordCabinetOld, $_hashSeedsObject, $_key32
            ['password', 'string'],
            ['password', 'validatePassword'],
        ];
    }

    public function normalizeSeeds($attribute, $params)
    {
        if (!$this->hasErrors()) {
            $ret = [];
            $arr = explode("\n", $this->seeds);
            foreach ($arr as $row) {
                $arr2 = explode(" ", $row);
                foreach ($arr2 as $word) {
                    if (trim($word) != '') {
                        $ret[] = trim($word);
                    }
                }
            }
            $this->seeds = join(' ', $ret);
        }
    }

    public function tryToUnlock($attribute, $params)
    {

        if (!$this->hasErrors()) {
            // распаковываю пароль от кабинета старый
            $sha256 = hash('sha256', $this->seeds);
            $key32 = substr($sha256, 0, 32);
            $hashSeedsObject = UserSeed::findOne(Yii::$app->user->id);
            $userCabinetPassword = \common\services\Security\AES::decrypt256CBC($hashSeedsObject->seeds, $key32);
            if ($userCabinetPassword == '') {
                $this->addError($attribute, 'Не верный SEEDS');
                return;
            }
            $this->_passwordCabinetOld = $userCabinetPassword;
            $this->_hashSeedsObject = $hashSeedsObject;
            $this->_key32 = $key32;
        }
    }

    public function validatePassword($attribute, $params)
    {
        if (!$this->hasErrors()) {
            /** @var \common\models\UserAvatar $user */
            $user = Yii::$app->user->identity;
            if (!$user->validatePassword($this->password)) {
                $this->addError($attribute, 'Пароль не верный');
            }
        }
    }


    /**
     */
    public function action()
    {
        $this->changePassword($this->_passwordCabinetOld, $this->password);

        /** @var \common\models\UserAvatar $user */
        $user = Yii::$app->user->identity;
        $user->wallets_is_locked = 0;
        $user->save();
        Yii::info(\yii\helpers\VarDumper::dumpAsString([$this->seeds]), 'avatar\models\forms\PasswordType211::action');
        Yii::info(\yii\helpers\VarDumper::dumpAsString([$this->seeds]), 'info\avatar\models\forms\PasswordType211::action');

        // устанавливаю новый seeds
        $sha256 = hash('sha256', $this->seeds);
        $key32 = substr($sha256, 0, 32);
        $seedsHash = \common\services\Security\AES::encrypt256CBC($this->password, $key32);
        $hashSeedsObject = UserSeed::findOne(Yii::$app->user->id);
        $hashSeedsObject->seeds = $seedsHash;
        $hashSeedsObject->save();

        return true;
    }


    /**
     * @param string $from
     * @param string $to
     *
     * @throws \Exception
     */
    private function changePassword($from, $to)
    {
        $billingList = UserBill::find()
            ->where([
                'user_id'       => Yii::$app->user->id,
                'mark_deleted'  => 0,
                'password_type' => UserBill::PASSWORD_TYPE_HIDE_CABINET,
            ])
            ->all();

        $t = \Yii::$app->db->beginTransaction();
        try {
            /** @var \common\models\avatar\UserBill $billing */
            foreach ($billingList as $billing) {
                $walletPassword = $billing->getPassword($from);
                if ($walletPassword == '') {
                    \Yii::warning('Не верный пароль к кошельку '.$billing->id, 'avatar\common\models\avatar\UserBill::changePassword');
                    throw new \Exception('Не верный пароль к кошельку ' . $billing->id);
                }
                $billing->password = UserBill::passwordEnСript($walletPassword, $to);
                $billing->password_type = UserBill::PASSWORD_TYPE_HIDE_CABINET;
                $ret = $billing->save();

                Yii::info(\yii\helpers\VarDumper::dumpAsString([$billing->address, $walletPassword, $to, $billing->password, $billing->password_type]), 'avatar\models\forms\PasswordType211::changePassword');
                Yii::info(\yii\helpers\VarDumper::dumpAsString([$billing->address, $walletPassword, $to, $billing->password, $billing->password_type]), 'info\avatar\models\forms\PasswordType211::changePassword');
                \Yii::trace(\yii\helpers\VarDumper::dumpAsString($ret), 'avatar\common\models\avatar\UserBill::changePassword');
            }
            $t->commit();
        } catch (\Exception $e) {
            $t->rollBack();
            throw $e;
        }
    }
}
