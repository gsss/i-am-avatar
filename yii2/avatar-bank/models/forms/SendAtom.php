<?php

namespace avatar\models\forms;

use common\models\avatar\CurrencyLink;
use common\models\avatar\UserBill;
use common\models\piramida\Wallet;
use cs\Application;
use cs\base\FormActiveRecord;
use Yii;
use yii\base\Model;
use yii\base\NotSupportedException;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\web\IdentityInterface;

/**
 */
class SendAtom extends Model
{
    /** @var  int Идентификатор кошелька откуда отправляются монеты */
    public $id;

    public $amount;

    public $comment;

    public $user_email;

    public $user_id;

    /** @var  int Идентификатор кошелька куда отправляются монеты */
    public $wallet_id;

    /** @var  \common\models\piramida\Wallet */
    public $walletIn;

    /** @var  \common\models\piramida\Wallet */
    public $walletOut;

    public function rules()
    {
        return [
            ['id', 'required'],
            ['id', 'integer'],
            ['id', 'exist', 'targetClass' => '\common\models\piramida\Wallet'],
            ['id', 'setItem'],

            ['amount', 'required'],
            ['amount', 'integer'],
            ['amount', 'validateInWallet'],

            ['comment', 'string'],
            ['comment', 'default', 'value' => 'Отправление'],

            ['user_email', 'string'],
            ['user_email', 'exist', 'targetAttribute' => 'email', 'targetClass' => '\common\models\UserAvatar'],

            ['user_id', 'integer'],
            ['user_id', 'exist', 'targetAttribute' => 'id', 'targetClass' => '\common\models\UserAvatar'],

            ['wallet_id', 'integer'],
            ['wallet_id', 'exist', 'targetAttribute' => 'id', 'targetClass' => '\common\models\piramida\Wallet'],
            ['wallet_id', 'validateCurrency'],

            ['wallet_id', 'validateAll', 'skipOnEmpty' => false],
        ];
    }

    public function setItem($attribute, $params)
    {
        if (!$this->hasErrors()) {
            $walletOut = Wallet::findOne($this->id);
            $this->walletOut = $walletOut;
        }
    }

    public function validateInWallet($attribute, $params)
    {
        if (!$this->hasErrors()) {
            if ($this->walletOut->amount < $this->amount) {
                $this->addError($attribute, 'Недостаточно денег в кошельке');
                return;
            }
        }
    }

    public function validateCurrency($attribute, $params)
    {
        if (!$this->hasErrors()) {
            $walletIn = Wallet::findOne($this->wallet_id);

            if ($this->walletOut->currency_id != $walletIn->currency_id) {
                $this->addError($attribute, 'Валюта не соответствует с кошельком отправителя');
                return;
            }

            $this->walletIn = $walletIn;
        }
    }

    public function validateAll($attribute, $params)
    {
        if (!$this->hasErrors()) {
            if (Application::isEmpty($this->user_email) and Application::isEmpty($this->user_id) and Application::isEmpty($this->wallet_id)) {
                $this->addError($attribute, 'Нужно заполнить хотябы одно поле');
            }
        }
    }

    /**
     * @return \common\models\Piramida\Transaction
     */
    public function action()
    {
        /** @var string $function 'wallet' | 'user'*/
        $function = 'wallet';
        if (!Application::isEmpty($this->user_email)) {
            $user = \common\models\UserAvatar::findOne(['email' => $this->user_email]);
            $function = 'user';
        } else if (!Application::isEmpty($this->user_id)) {
            $user = \common\models\UserAvatar::findOne($this->user_id);
            $function = 'user';
        }

        if ($function == 'user') {
            $currency = \common\models\piramida\Currency::findOne($this->walletOut->currency_id);
            $link = CurrencyLink::findOne(['currency_int_id' => $this->walletOut->currency_id]);
            try {
                $bill = UserBill::findOne([
                    'user_id'    => $user->id,
                    'currency'   => $link->currency_ext_id,
                    'is_default' => 1,
                ]);
                $wallet = Wallet::findOne($bill->address);
            } catch (\Exception $e) {
                // Создаю кошелек
                $wallet = Wallet::addNew(['currency_id' => $this->walletOut->currency_id]);
                $bill = UserBill::add([
                    'user_id'    => $user->id,
                    'name'       => $currency->name,
                    'currency'   => $link->currency_ext_id,
                    'is_default' => 1,
                    'address'    => $wallet->id,
                ]);
            }
            $this->walletIn = $wallet;
        }

        $t = $this->walletOut->move(
            $this->walletIn,
            $this->amount,
            $this->comment
        );

        return $t;
    }
}
