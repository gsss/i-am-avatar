<?php
namespace avatar\models\forms;

use cs\base\FormActiveRecord;
use Yii;
use yii\base\NotSupportedException;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\web\IdentityInterface;

/**
 * PaySystem model
 *
 * @property integer    id
 * @property string     address
 * @property string     image
 * @property string     name
 * @property string     abi
 */
class Contract extends FormActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'contract';
    }

    public function formAttributes()
    {
        return [
            [
                'name',
                'Название',
                1,
                'string',
                ['max' => 255],
            ],
            [
                'address',
                'Адрес',
                1,
                'string',
                ['max' => 255],
            ],
            [
                'name',
                'Название',
                0,
                'string',
                ['max' => 255],
            ],
            [
                'abi',
                'Интерфейс',
                1,
                'string',
            ],
            [
                'image',
                'Картинка',
                0,
                '\common\widgets\FileUpload3\Validator',
                'widget' => [
                    '\common\widgets\FileUpload3\FileUpload',
                    [
                        'options' => [
                            'small' => \avatar\services\Html::$formatIcon
                        ]
                    ]
                ]
            ],
        ];
    }
}
