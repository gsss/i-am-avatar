<?php
namespace avatar\models\forms;

use cs\base\FormActiveRecord;
use Yii;
use yii\base\NotSupportedException;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\web\IdentityInterface;

/**
 *
 * @property integer id
 * @property integer user_id
 */
class UserMaster extends FormActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'user_master';
    }

    public function formAttributes()
    {
        return [
            [
                'contact_vk',
                'Ссылка на страницу VK',
                0,
                'string',
                ['max' => 100],
            ],
            [
                'contact_fb',
                'Ссылка на страницу FB',
                0,
                'string',
                ['max' => 100],
            ],
            [
                'contact_youtube',
                'Ссылка на страницу youtube',
                0,
                'string',
                ['max' => 100],
            ],
            [
                'contact_instagram',
                'Ссылка на страницу Instagram',
                0,
                'string',
                ['max' => 100],
            ],
            [
                'contact_tweeter',
                'Ссылка на страницу Tweeter',
                0,
                'string',
                ['max' => 100],
            ],
            [
                'contact_telegram',
                'Аккаунт telegram или номер телефона',
                0,
                'string',
                ['max' => 100],
            ],
            [
                'contact_whatsapp',
                'Аккаунт-телефон whatsapp',
                0,
                'string',
                ['max' => 100],
            ],
            [
                'contact_email',
                'Почта для связи',
                0,
                'string',
            ],
            [
                'contact_phone',
                'Телефон для связи',
                0,
                'string',
                ['max' => 100],
            ],
            [
                'contact_viber',
                'Телефон viber',
                0,
                'string',
                ['max' => 100],
            ],
            [
                'share_title',
                'Заголовок',
                0,
                'string',
                ['max' => 100],
            ],
            [
                'share_description',
                'Описание',
                0,
                'string',
                ['max' => 1000],
            ],
            [
                'share_image',
                'Изображение',
                0,
                '\common\widgets\FileUpload3\Validator', [],
                'Реккомендуемый размер 1200 x 630 px',
                'widget' => [
                    '\common\widgets\FileUpload3\FileUpload',
                    [
                        'options' => [
                            'small' => \avatar\services\Html::$formatIcon
                        ]
                    ]
                ],
            ],

            [
                'bio',
                'Биография',
                0,
                'string',
                'widget' => [
                    '\common\widgets\HtmlContent\HtmlContent',
                ]
            ],
            [
                'vozm',
                'Возможности',
                0,
                'string',
                'widget' => [
                    '\common\widgets\HtmlContent\HtmlContent',
                ]
            ],
            [
                'is_sert',
                'Отображать сертификаты?',
                0,
                '\common\widgets\CheckBox2\Validator',
                'widget' => [
                    '\common\widgets\CheckBox2\CheckBox'
                ],
            ],
            [
                'is_design',
                'Отображать дизайн аватара?',
                0,
                '\common\widgets\CheckBox2\Validator',
                'widget' => [
                    '\common\widgets\CheckBox2\CheckBox'
                ],
            ],

        ];


    }
}
