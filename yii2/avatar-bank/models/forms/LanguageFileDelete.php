<?php

namespace avatar\models\forms;

use app\models\ActionsRange;
use common\components\EmailVerifier;
use common\models\Config;
use common\models\Language;
use common\models\language\Access;
use common\models\language\Message;
use common\models\language\SourceMessage;
use common\services\Debugger;
use GuzzleHttp\Query;
use Imagine\Image\Box;
use yii\base\Model;
use Yii;
use yii\helpers\ArrayHelper;
use yii\helpers\FileHelper;
use yii\helpers\VarDumper;
use yii\imagine\Image;
use yii\web\UploadedFile;

class LanguageFileDelete extends Model
{
    public $file;

    /** @var  int идентификатор строки source_message.id */
    public $id;

    /** @var  string код языка */
    public $language;

    public function rules()
    {
        return [
            [['id', 'language'], 'required'],
            [['id'], 'integer'],
            [['language'], 'string', 'max' => 2],
        ];
    }


    /**
     * Сохраняет файл
     *
     * @return bool
     */
    public function action()
    {
        $messageObject = Message::findOne(['id' => $this->id, 'language' => $this->language]);
        $translation = $messageObject->translation;
        $path_b = $translation;
        $fullPath_b = Yii::getAlias(\backend\models\form\LanguageImageSave::$pathSave1 . $path_b);

        // удаляю старые файлы
        if (file_exists($fullPath_b)) {
            unlink($fullPath_b);
        }

        $messageObject->delete();

        // сбрасываю кеш
        Yii::$app->cacheLanguages->flush();

        return true;
    }
}