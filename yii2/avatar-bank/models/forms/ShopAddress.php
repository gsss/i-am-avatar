<?php

namespace avatar\models\forms;

use common\models\SendLetter;
use common\models\UserSeed;
use common\services\Security;
use cs\Application;
use cs\services\Str;
use Yii;
use yii\base\Model;
use yii\helpers\ArrayHelper;
use yii\helpers\VarDumper;
use yii\web\UploadedFile;

/**
 */
class ShopAddress extends Model
{
    public $index;
    public $address;
    public $fio;
    public $comment;
    public $phone;

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        $request = Yii::$app->session->get('request', []);
        if (isset($request['delivery'])) {
            if ($request['delivery']['type'] == 1) {
                return [
                    ['comment', 'string', 'max' => 255],
                    ['phone', 'string', 'max' => 20],
                ];
            }
        }
        return [
            ['index', 'required'],
            ['index', 'string', 'max' => 20],

            ['address', 'required'],
            ['address', 'string', 'max' => 200],

            ['fio', 'required'],
            ['fio', 'string', 'max' => 100],

            ['comment', 'string', 'max' => 255],

            ['phone', 'string', 'max' => 20],
        ];
    }

    public function attributeLabels()
    {
        return [
            'index'   => 'Индекс',
            'address' => 'Адрес доставки',
            'fio'     => 'ИОФ получателя',
            'comment' => 'Комментарий к заказу',
            'phone'   => 'Телефон',
        ];
    }

    /**
     */
    public function action()
    {
        $request = Yii::$app->session->get('request', []);
        $request['address']['index'] = $this->index;
        $request['address']['address'] = $this->address;
        $request['address']['fio'] = $this->fio;
        $request['address']['comment'] = $this->comment;
        $request['address']['phone'] = $this->phone;

        Yii::$app->session->set('request', $request);

        return true;
    }
}
