<?php

namespace avatar\models\forms;

use avatar\models\WalletToken;
use common\models\avatar\UserBill;
use common\models\Token;
use Yii;
use yii\helpers\ArrayHelper;

/**
 */
class IcoPath3Index2Ajax extends IcoPath3Base
{
    /** @var  int */
    public $listEth;

    /** @var  string */
    public $password;

    /** @var  \common\models\avatar\UserBill Кошелек эфира от которого будет создан кошелек токена */
    private $billing;

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        $rules = [
            ['listEth', 'required'],
            ['listEth', 'integer'],
            ['listEth', 'tryToCreate'], // попытка создать кошелек ETH

            ['password', 'string'],
            ['password', 'validatePassword'],
        ];

        return ArrayHelper::merge(parent::rules(), $rules);
    }

    /**
     * Пытается создать кошелек ETH
     * Устанавливает $billing
     *
     * @param $attribute
     * @param $params
     */
    public function tryToCreate($attribute, $params)
    {
        if (!$this->hasErrors()) {
            // Если надо создаю кошелек эфира
            if ($this->listEth == -1) {
                /** @var \common\models\UserAvatar $user */
                $user = Yii::$app->user->identity;

                /** @var \common\components\providers\ETH $ETH */
                $ETH = Yii::$app->eth;
                try {
                    $wallet = $ETH->create('ETH', $this->password, Yii::$app->user->id, $user->password_save_type);
                    $this->billing = $wallet->billing;

                } catch (\Exception $e) {
                    $this->addError($attribute, $e->getMessage());
                    return;
                }
            } else {
                try {
                    $billing = UserBill::findOne([
                        'id'           => $this->listEth,
                        'currency'     => \common\models\avatar\Currency::ETH,
                        'user_id'      => Yii::$app->user->id,
                        'mark_deleted' => 0,
                    ]);
                    $this->billing = $billing;
                } catch (\Exception $e) {
                    $this->addError($attribute, $e->getMessage());
                    return;
                }
            }
        }
    }

    /**
     * Создает кошелек токена
     *
     * @return \common\models\investment\IcoRequest
     */
    public function action()
    {
        $token = Token::findOne($this->token);

        $wallet = WalletToken::createFromEth($this->billing, $token, $token->getCurrency()->title, Yii::$app->user->id);
        $this->address = $wallet->billing->address;

        return $this->createRequest();
    }
}
