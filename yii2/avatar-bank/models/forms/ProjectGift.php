<?php
namespace avatar\models\forms;

use cs\base\FormActiveRecord;
use Yii;
use yii\base\NotSupportedException;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\web\IdentityInterface;

/**
 * Project model
 *
 * @property int    id            Идентификатор
 * @property int    project_id       Идентификатор проекта
 * @property string name             Наименование подарка
 * @property string content          Текст описания подпрка
 * @property string image            Картинка для проекта. 300*300 обрезается
 * @property int    currency         Идентификатор валюты по таблице currency
 * @property double sum              Сумма вложений
 */
class ProjectGift extends FormActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'projects_gifts';
    }

    public function formAttributes()
    {
        return [
            [
                'name',
                'Название',
                1,
                'string',
                ['max' => 255],
            ],
            [
                'link',
                'Ссылка',
                0,
                'string',
                ['max' => 255],
            ],
            [
                'description',
                'Описание',
                0,
                'string',
                ['max' => 2000],
            ],
            [
                'content',
                'Полный текст',
                0,
                'string',
                'widget' => [
                    '\common\widgets\HtmlContent\HtmlContent',
                ]
            ],

            [
                'image',
                'Картинка',
                0,
                '\common\widgets\FileUpload3\Validator',
                'widget' => [
                    '\common\widgets\FileUpload3\FileUpload',
                    [
                        'options' => [
                            'small' => \avatar\services\Html::$formatIcon
                        ]
                    ]
                ]
            ],
        ];
    }
}
