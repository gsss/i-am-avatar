<?php

namespace avatar\models\forms;

use common\models\Event;
use cs\base\FormActiveRecord;
use cs\services\VarDumper;
use cs\web\Exception;
use Yii;
use yii\base\Model;
use yii\db\ActiveRecord;
use yii\helpers\Url;

/**
 */
class ProfilePhone extends ActiveRecord
{
    public function init()
    {
        \yii\base\Event::on('\avatar\models\forms\ProfilePhone', \yii\db\BaseActiveRecord::EVENT_AFTER_FIND, function($event) {
            /** @var \avatar\models\forms\ProfilePhone $user */
            $user = $event->sender;
            if ($this->phone_is_confirm) {
                $phone = '+' . substr($this->phone, 0, 1);
                $phone .= '(' . substr($this->phone, 1, 3) . ')-';
                $phone .= substr($this->phone, 4, 3) . '-' . substr($this->phone, 7, 2) . '-' . substr($this->phone, 9, 2);
                $this->phone = $phone;
            }
        });
    }

    public static function tableName()
    {
        return 'user';
    }

    public function rules()
    {
        return [['phone', 'string']];
    }

    public function attributeLabels()
    {
        return [
            'phone' => 'Телефон',
        ];
    }
}
