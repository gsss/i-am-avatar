<?php

namespace avatar\models\forms;

use app\models\ActionsRange;
use common\components\EmailVerifier;
use common\models\Config;
use common\models\Language;
use common\models\language\Access;
use common\models\language\Message;
use common\services\Debugger;
use GuzzleHttp\Query;
use Imagine\Image\Box;
use yii\base\Model;
use Yii;
use yii\helpers\ArrayHelper;
use yii\helpers\FileHelper;
use yii\helpers\Json;
use yii\helpers\VarDumper;
use yii\imagine\Image;
use yii\web\UploadedFile;

class LanguageImportFile extends Model
{
    public $file;

    public function rules()
    {
        return [
            [['file'], 'file', 'extensions' => ['json']],
        ];
    }

    /**
     * Сохраняет файл
     *
     * @return bool | null
     * Если появился вопрос о том что загружать то выдается null
     * иначе выдается true
     */
    public function action()
    {
        $file = UploadedFile::getInstanceByName('file');
        $content = file_get_contents($file->tempName);
        $dataArray = ArrayHelper::toArray(Json::decode($content));
        $migration = new \common\components\Migration(['isLog' => false]);

        return $migration->updateStrings($dataArray);
    }

}