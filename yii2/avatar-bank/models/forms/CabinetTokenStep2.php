<?php

namespace avatar\models\forms;

use common\models\avatar\UserBill;
use common\models\RequestTokenCreate;
use common\models\SendLetter;
use common\models\UserSeed;
use common\services\Security;
use cs\Application;
use cs\services\Str;
use Yii;
use yii\base\Model;
use yii\helpers\ArrayHelper;
use yii\helpers\VarDumper;

/**
 */
class CabinetTokenStep2 extends Model
{
    /** @var  string пароль от счета (кабинета) */
    public $password;

    /** @var  int идентификатор счета AVR с которого производится оплата */
    public $billing_id;

    /** @var  \common\models\avatar\UserBill счет AVR с которого производится оплата */
    private $billing;

    /** @var  int идентификатор заявки на создание токена */
    public $request_id;

    /** @var  \common\models\RequestTokenCreate заявка на создание токена */
    public $request;

    /** @var  string идентификатор транзакции */
    private $txid;

    /** @var  double стоимость услуги в AVR */
    private $price = 1;

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            [['password'], 'required', 'message' => 'Это поле должно быть заполнено обязательно'],
            ['password', 'validatePassword'],

            ['request_id', 'required'],
            ['request_id', 'integer'],
            ['request_id', 'validateRequest'],

            ['billing_id', 'required'],
            ['billing_id', 'integer'],
            ['billing_id', 'validateBilling'],
            ['billing_id', 'validateBalance'], // проверяет баланс на счете перед списанием
            ['billing_id', 'tryToPay'],
        ];
    }

    /**
     * @return array customized attribute labels
     */
    public function attributeLabels()
    {
        return [
            'password' => 'Пароль',
        ];
    }

    /**
     * Конвертирует массив ошибок от формы чтобы их можно было выдать через JSON
     *
     * @param array $params
     *      [
     *      'amount' => ['Не верное число',...],
     *      ]
     *
     * @return array
     * [
     *      'name'  => 'amount',
     *      'value' => ['Не верное число',...],
     * ]
     */
    public function convert($params)
    {
        $ret = [];
        foreach ($params as $name => $arr) {
            $ret[] = [
                'name'  => $name,
                'value' => $arr,
            ];
        }
        return $ret;
    }

    public function validatePassword($attribute, $params)
    {
        if (!$this->hasErrors()) {
            /** @var \common\models\UserAvatar $user */
            $user = Yii::$app->user->identity;
            if (!$user->validatePassword($this->password)) {
                $this->addError($attribute, 'Пароль не верный');
            }
        }
    }

    public function validateRequest($attribute, $params)
    {
        if (!$this->hasErrors()) {
            $request = RequestTokenCreate::findOne($this->request_id);
            if (is_null($request)) {
                $this->addError($attribute, 'Не найдена заявка');
                return;
            }
            if ($request->user_id != Yii::$app->user->id) {
                $this->addError($attribute, 'Это не ваша заявка');
                return;
            }
            if ($request->step != 2) {
                $this->addError($attribute, 'Вы находитесь не на верном шаге выпуска токена');
                return;
            }
            $this->request = $request;
        }
    }

    public function validateBilling($attribute, $params)
    {
        if (!$this->hasErrors()) {
            try {
                $billing = UserBill::findOne($this->billing_id);
            } catch (\Exception $e) {
                $this->addError($attribute, 'Не найден счет');
                return;
            }
            if ($billing->user_id != Yii::$app->user->id) {
                $this->addError($attribute, 'Это не ваш счет');
                return;
            }
            $this->billing = $billing;
        }
    }

    /**
     * Проверяет баланс
     * Так как при списании недостаточного кол-ва монет все равно выдастся удачная транзакция, но деньги не спишутся
     *
     * @param $attribute
     * @param $params
     */
    public function validateBalance($attribute, $params)
    {
        if (!$this->hasErrors()) {
            $balance = $this->billing->getWalletToken()->getBalance();
            if ($balance[1] < $this->price) {
                $this->addError($attribute, 'У вас не хватает денег на счету');
                return;
            }
        }
    }

    /**
     * Списывает деньги со счета
     *
     * @param $attribute
     * @param $params
     */
    public function tryToPay($attribute, $params)
    {
        if (!$this->hasErrors()) {
            $destinationAddress = Yii::$app->params['createTokenWalletPayment'];

            $tokenWallet = $this->billing->getWalletToken();
            try {
                $txid = $tokenWallet->pay([$destinationAddress => [
                    'amount'  => 1,
                    'comment' => [
                        'from' => 'Оплата за выпуск токена',
                        'to'   => 'Оплата за выпуск токена от user_id=' . $this->billing->user_id,
                    ],
                ]], $this->password);
            } catch (\Exception $e) {
                $this->addError($attribute, $e->getMessage());
                return;
            }

            $this->txid = $txid;
        }
    }

    /**
     * Сохраняет пароль в кеш
     * Устанавливает для заявки 3 шаг
     *
     * @return string идентификатор транзакции
     */
    public function action()
    {
        Yii::$app->cache->set(['session_id' => Yii::$app->session->id, 'name' => 'createToken'], $this->password);
        $this->request->step = 3;
        $this->request->is_paid_avatars = 1;
        $this->request->save();

        return $this->txid;
    }
}
