<?php
namespace avatar\models\forms;

use cs\base\FormActiveRecord;
use Yii;
use yii\base\NotSupportedException;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\web\IdentityInterface;

/**
 *
 */
class SchoolSettings extends FormActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'school';
    }

    public function formAttributes()
    {
        return [
            [
                'subscribe_from_name',
                'Обратный адрес в рассылке, имя',
                0,
                'string',
                ['max' => 100],
            ],
            [
                'subscribe_from_email',
                'Обратный адрес в рассылке, почта',
                0,
                'string',
                ['max' => 100],
            ],
            [
                'subscribe_image',
                'Картинка для письма в рассылке',
                0,
                '\common\widgets\FileUpload3\Validator',
                'widget' => [
                    '\common\widgets\FileUpload3\FileUpload',
                    [
                        'options' => [
                            'small' => \avatar\services\Html::$formatIcon
                        ]
                    ]
                ]
            ],
        ];
    }
}
