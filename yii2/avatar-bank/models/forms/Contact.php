<?php

namespace avatar\models\forms;

use common\models\SendLetter;
use cs\Application;
use cs\services\Str;
use Yii;
use yii\base\Model;
use yii\helpers\ArrayHelper;
use yii\helpers\VarDumper;

/**
 * ContactForm is the model behind the contact form.
 */
class Contact extends Model
{
    public $name;
    public $email;
    public $subject;
    public $body;
    public $verifyCode;

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            // name, email, subject and body are required
            [['name', 'email', 'subject', 'body'], 'required'],
            // email has to be a valid email address
            ['email', 'email'],
            // verifyCode needs to be entered correctly
            ['verifyCode', 'captcha'],
        ];
    }

    /**
     * @return array customized attribute labels
     */
    public function attributeLabels()
    {
        return [
            'verifyCode' => 'Проверочный код',
        ];
    }

    /**
     * Sends an email to the specified email address using the information collected by this model.
     * @param  string  $email the target email address
     * @return boolean whether the model passes validation
     */
    public function contact($email)
    {
        Yii::trace(VarDumper::dumpAsString([
            'name'  => $this->name,
            'email' => $this->email,
            'text'  => $this->body,
            'to'    => $email,
        ]), 'avatar\models\forms\Contact::contact');

        // добавляю в БД
        \common\models\avatar\SendLetter::add([
            'name'  => $this->name,
            'email' => $this->email,
            'text'  => $this->body,
        ]);
        \common\services\Subscribe::sendArray([$email], 'Письмо с сайта ' . $this->getDomainName() . ': ' .$this->subject, 'contact', [
            'text' => $this->body,
            'from' => [
                'email' => $this->email,
                'name'  => $this->name,
            ],
        ]);

        return true;
    }

    /**
     * Возвращает имя домена на котором располагается сайт
     *
     * @return string
     */
    private function getDomainName()
    {
        return $_SERVER['HTTP_HOST'];
    }
}
