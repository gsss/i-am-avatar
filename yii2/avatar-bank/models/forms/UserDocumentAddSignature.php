<?php

namespace avatar\models\forms;

use avatar\modules\Piramida\PaySystems\Base\Model;
use common\models\avatar\UserBill;
use common\models\avatar\UserBillOperation;
use common\models\UserDocument;
use common\models\UserDocumentSignature;
use common\payment\BitCoinBlockTrailPayment;
use cs\base\FormActiveRecord;
use cs\services\BitMask;
use function PHPSTORM_META\elementType;
use yii\base\Exception;
use yii\db\ActiveRecord;
use yii\db\Query;
use yii\helpers\Json;
use yii\helpers\VarDumper;
use yii\httpclient\Client;

/**
 */
class UserDocumentAddSignature extends Model
{
    public $id;
    public $password;

    /** @var  \common\models\UserDocument */
    public $_doc;

    private $txid;

    public function rules()
    {
        return [
            ['id', 'required'],
            ['id', 'integer'],
            ['id', 'assignDoc'],
            ['password', 'required'],
            ['password', 'validatePassword'],
            ['password', 'tryToSign'],
        ];
    }

    /**
     * Конвертирует массив ошибок от формы чтобы их можно было выдать через JSON
     *
     * @param array $params
     *      [
     *      'amount' => ['Не верное число',...],
     *      ]
     *
     * @return array
     * [
     *      'name'  => 'amount',
     *      'value' => ['Не верное число',...],
     * ]
     */
    public function convert($params)
    {
        $ret = [];
        foreach ($params as $name => $arr) {
            $ret[] = [
                'name'  => $name,
                'value' => $arr,
            ];
        }
        return $ret;
    }


    public function validatePassword($attribute, $params)
    {
        if (!$this->hasErrors()) {
            /** @var \common\models\UserAvatar $user */
            $user = \Yii::$app->user->identity;

            try {
                $passportWalletEth = $user->getWalletEthPassport();
            } catch (\Exception $e) {
                $this->addError($attribute, $e->getMessage());
                return;
            }

            if (!$passportWalletEth->billing->validatePassword($this->password)) {
                $this->addError($attribute, 'Не верный пароль');
            }
        }
    }

    public function assignDoc($attribute, $params)
    {
        if (!$this->hasErrors()) {
            $doc = UserDocument::findOne($this->id);
            if (is_null($doc)) {
                $this->addError($attribute, 'Не найден документ');
                return;
            }
            $this->_doc = $doc;
        }
    }

    public function tryToSign($attribute, $params)
    {
        if (!$this->hasErrors()) {
            /** @var \common\models\UserAvatar $user */
            $user = \Yii::$app->user->identity;

            $passportWalletEth = $user->getWalletEthPassport();
            try {
                $id = $this->getIdFromTxHash2($this->_doc->txid);
            } catch (\Exception $e) {
                if ($e->getMessage() == 'Service unavaliable') {
                    $this->addError($attribute, 'Сервис временно не доступен. Попробуйте позже.');
                    return;
                }
            }

            $contract = '0x52ed3c202c4652f952a1561ac0c030f1ed9460ff';
            if (!YII_ENV_PROD) {
                $contract = '0xfF6C78BFedF1bdb2D85071A9040798F8057A0A36';
            }

            try {
                $txid = $passportWalletEth->contract(
                    $this->password,
                    $contract,
                    '[{"constant":true,"inputs":[],"name":"getDocumentsCount","outputs":[{"name":"","type":"uint256"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":false,"inputs":[{"name":"hash","type":"string"},{"name":"link","type":"string"},{"name":"data","type":"string"}],"name":"registerDocument","outputs":[],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":false,"inputs":[{"name":"id","type":"uint256"}],"name":"addSignature","outputs":[],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":true,"inputs":[{"name":"id","type":"uint256"}],"name":"getDocument","outputs":[{"name":"hash","type":"string"},{"name":"link","type":"string"},{"name":"data","type":"string"},{"name":"creator","type":"address"},{"name":"date","type":"uint256"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":false,"inputs":[],"name":"kill","outputs":[],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":true,"inputs":[{"name":"","type":"uint256"}],"name":"documentsIds","outputs":[{"name":"hash","type":"string"},{"name":"link","type":"string"},{"name":"data","type":"string"},{"name":"creator","type":"address"},{"name":"date","type":"uint256"},{"name":"signsCount","type":"uint256"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":true,"inputs":[{"name":"id","type":"uint256"}],"name":"getDocumentSignsCount","outputs":[{"name":"","type":"uint256"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":false,"inputs":[{"name":"newOwner","type":"address"}],"name":"changeOwner","outputs":[],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":true,"inputs":[{"name":"id","type":"uint256"},{"name":"index","type":"uint256"}],"name":"getDocumentSign","outputs":[{"name":"member","type":"address"},{"name":"date","type":"uint256"}],"payable":false,"stateMutability":"view","type":"function"},{"inputs":[],"payable":false,"stateMutability":"nonpayable","type":"constructor"},{"anonymous":false,"inputs":[{"indexed":false,"name":"id","type":"uint256"},{"indexed":false,"name":"member","type":"address"}],"name":"DocumentSigned","type":"event"},{"anonymous":false,"inputs":[{"indexed":false,"name":"id","type":"uint256"},{"indexed":false,"name":"hash","type":"string"}],"name":"DocumentRegistered","type":"event"}]',
                    'addSignature',
                    [$id],
                    'true'
                );
                \Yii::trace($txid, '\avatar\models\forms\UserDocumentAddSignature::action()');
                $this->txid = $txid;

            } catch (\Exception $e) {
                if ($e->getCode() == 502) {
                    $this->addError($attribute, 'Сервер не отвечает');
                } else {
                    $this->addError($attribute, $e->getMessage());
                }

                return;
            }
        }
    }

    public function action()
    {
        /** @var \common\models\UserAvatar $user */
        $user = \Yii::$app->user->identity;

        $passportWalletEth = $user->getWalletEthPassport();

        // добавляю операцию в кошелек Эфировский
        $operation = new UserBillOperation([
            'transaction' => $this->txid,
            'message'     => 'Подпись документа #' . $this->id,
            'bill_id'     => $passportWalletEth->billing->id,
            'type'        => UserBillOperation::TYPE_OUT,
            'created_at'  => time(),
        ]);
        $operation->save();

        // добавляю подпись в свою БД
        $signature = UserDocumentSignature::add([
            'document_id' => $this->_doc->id,
            'member'      => $passportWalletEth->billing->address,
            'txid'        => $this->txid,
        ]);

        return $this->txid;
    }

    /**
     * Получает идентификатор документа через хеш транзакции по которой был зарегистрирован документ
     *
     * @param $hash
     *
     * @return int
     * @throws
     */
    private function getIdFromTxHash2($hash)
    {
        $provider = new \avatar\modules\ETH\ServiceInfura();
        $data = $provider->_method('eth_getTransactionReceipt', [$hash]);

        $log = $data['logs'][0]['data'];
        $hex = substr($log, 0, 66);
        $id = (int)hexdec($hex);

        return $id;
    }


}