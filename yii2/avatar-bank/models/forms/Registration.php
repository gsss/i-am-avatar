<?php

namespace avatar\models\forms;

use app\models\Piramida\Piramida;
use app\models\Piramida\Wallet;
use app\models\User;
use common\models\UserAvatar;
use cs\Application;
use Yii;
use yii\base\Model;


/**
 * Registration is the model behind the contact form.
 */
class Registration extends Model
{
    public $email;
    public $password1;
    public $password2;
    public $is_rules;
    public $verificationCode;

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            ['email', 'required', 'message' => Yii::t('c.0uMdJb0e0n', 'Это поле должно быть заполнено обязательно')],
            ['email', 'trim'],
            ['email', 'toLower'],
            ['email', 'email', 'checkDNS' => true, 'message' => Yii::t('c.0uMdJb0e0n', 'Email должен быть с корректным доменным именем')],
            ['email', 'validateEmail'],

            ['is_rules', 'integer'],
            ['is_rules', 'validateRules', 'skipOnEmpty' => false],

            ['password1', 'required', 'message' => Yii::t('c.0uMdJb0e0n', 'Это поле должно быть заполнено обязательно')],

            ['password2', 'required', 'message' => Yii::t('c.0uMdJb0e0n', 'Это поле должно быть заполнено обязательно')],
            ['password2', 'compare', 'compareAttribute' => 'password1', 'operator' => '==', 'message' => Yii::t('c.0uMdJb0e0n', 'Пароли должны совпадать')],

            ['verificationCode', '\cs\Widget\Google\reCaptcha\Validator', 'skipOnEmpty' => false],
        ];
    }

    public function validateRules($attribute, $params)
    {
        if (Application::isEmpty($this->is_rules)) {
            $this->addError($attribute, Yii::t('c.0uMdJb0e0n', 'Условия должны быть приняты обязательно'));
            return;
        } else {
            if ($this->is_rules != 1) {
                $this->addError($attribute, Yii::t('c.0uMdJb0e0n', 'Условия должны быть приняты обязательно'));
                return;
            }
        }
    }

    public function scenarios()
    {
        return [
            'insert' => ['is_rules', 'email', 'password1', 'password2', 'verificationCode'],
            'ajax'   => ['is_rules', 'email', 'password1', 'password2'],
        ];
    }

    /**
     * @return array customized attribute labels
     */
    public function attributeLabels()
    {
        return [
            'verificationCode'  => Yii::t('c.0uMdJb0e0n', 'Проверочный код'),
            'is_rules'          => 'Условия лицензионного договора',
        ];
    }

    public function validateEmail($attribute, $params)
    {
        if (!$this->hasErrors()) {
            if (UserAvatar::find()->where(['email' => $this->email])->exists()) {
                $this->addError($attribute, Yii::t('c.0uMdJb0e0n', 'Такой пользователь уже есть'));
            }
        }
    }

    public function toLower($attribute, $params)
    {
        if (!$this->hasErrors()) {
           $this->email = strtolower($this->email);
        }
    }

    /**
     * @return boolean whether the model passes validation
     */
    public function register()
    {
        $email = $this->email;

        return is_null(\common\models\UserAvatar::registration($email, $this->password1));
    }
}
