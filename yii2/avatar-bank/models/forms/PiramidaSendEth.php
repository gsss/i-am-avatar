<?php

namespace avatar\models\forms;

use avatar\models\WalletToken;
use common\models\avatar\Currency;
use common\models\avatar\UserBill;
use common\models\avatar\UserBillAddress;
use common\models\avatar\UserBillOperation;
use common\models\Card;
use common\models\Token;
use common\models\UserAvatar;
use common\services\Security;
use cs\services\Str;
use cs\services\VarDumper;
use Yii;
use yii\base\Exception;
use yii\base\Model;
use cs\Widget\FileUpload3\FileUpload;
use yii\db\Query;
use yii\helpers\Html;
use yii\helpers\StringHelper;

/**
 * ContactForm is the model behind the contact form.
 */
class PiramidaSendEth extends Model
{
    /** @var string можно задать адрес, телефон, почта, номер карты с пробелами */
    public $address;

    /** @var \common\models\avatar\UserBill счет отправителя  */
    public $billingFrom;

    /** @var  string(3) код валюты, валюта в которой пользователь указывал переводимые средства */
    public $currency;

    /** @var  \common\models\avatar\Currency валюта в которой пользователь указывал переводимые средства устанавливается в normalizeCurrency */
    public $_currencyObject;

    /** @var string пароль откабинета */
    public $password;

    /** @var string кол-во монет для отправки */
    public $amount;

    /** @var string комментарий к транзакции */
    public $comment;

    /** @var int Идентификатор счета откуда отправляются деньги */
    public $billing_id;

    /** @var int Кол-во газа для исполнения транзакции */
    public $gasLimit;

    /** @var int цена газа в Gwei */
    public $gasPrice;

    public function rules()
    {
        return [
            [['address', 'password', 'amount', 'billing_id'], 'required'],
            ['currency', 'normalizeCurrency'],
            ['billing_id', 'integer'],
            ['billing_id', 'validateBilling'],

            ['address', 'trim'],
            ['address', 'normalizeAddress'],

            ['amount', 'normalizeAmount'],

            [['address', 'password', 'comment'], 'string'],
            ['password', 'validatePassword'],

            ['gasLimit', 'integer', 'min' => 21000, 'max' => 4000000],
            ['gasPrice', 'integer', 'min' => 0, 'max' => 50],
        ];
    }

    /**
     * Преобразовывает запятую в точку если есть
     * Если есть запрещенные символы то выводит сообщение об ошибке
     * Если валюта отлична от BTC то производит конвертацию
     *
     * @param $attribute
     * @param $params
     *
     * @throws
     */
    public function normalizeAmount($attribute, $params)
    {
        if (!$this->hasErrors()) {
            $this->amount = str_replace(',', '.', $this->amount);
            if (!$this->validateAmount($this->amount)) {
                $this->addError($attribute, 'Не верные символы');
                return;
            }
            $to = 'ETH';

            if ($this->currency != $to) {
                $this->amount = Currency::convert($this->amount, $this->currency, $to);
                $this->currency = $to;
            }
        }
    }

    /**
     * Преобразовывает ВТС в \common\models\avatar\Currency
     *
     * @param $attribute
     * @param $params
     *
     * @throws
     */
    public function normalizeCurrency($attribute, $params)
    {
        if (!$this->hasErrors()) {
            $c = Currency::findOne(['code' => $this->currency]);
            if (is_null($c)) {
                $this->addError($attribute, 'Не найдена валюта');
            }
            $this->_currencyObject = $c;
        }
    }

    private function validateAmount($amount)
    {
        $symbols = Str::getChars($amount);
        foreach ($symbols as $char) {
            if (!in_array($char, ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '.'])) return false;
        }
        return true;
    }

    /**
     * Если адрес задан как номер карты, номер телефона, или логин то приводит его к адресу по умолчанию
     *
     * @param $attribute
     * @param $params
     */
    public function normalizeAddress($attribute, $params)
    {
        if (!$this->hasErrors()) {
            $address = $this->address;
            if (StringHelper::startsWith($address, 'bitcoin')) {
                $address = substr($address, 8);
                $arr = explode('?', $address);
                if (count($arr) > 1) {
                    $address = $arr[0];
                }
            } else if (StringHelper::startsWith($address, 'ethereum')) {
                $address = substr($address, 9);
                $arr = explode('?', $address);
                if (count($arr) > 1) {
                    $address = $arr[0];
                }
            } else if (filter_var($address, FILTER_VALIDATE_EMAIL)) {
                // email
                $email = $address;
                try {
                    $user = UserAvatar::findByUsername($email);
                } catch (\Exception $e) {
                    $this->addError($attribute, 'Не найдена почта');
                    return;
                }

                try {
                    $billing = $user->getPiramidaBilling($this->billingFrom->currency);
                } catch (\Exception $e) {
                    $this->addError($attribute, $e->getMessage());
                    return;
                }

                $address = $billing->address;
            } else if ($this->isPhone($address)) {
                // phone
                $phone = $address;
                $phone = str_replace('+', '', $phone);
                $phone = str_replace('-', '', $phone);
                $phone = str_replace('(', '', $phone);
                $phone = str_replace(')', '', $phone);
                $phone = str_replace(' ', '', $phone);
                try {
                    $user = UserAvatar::findOne(['phone' => $phone]);
                } catch (\Exception $e) {
                    $this->addError($attribute, 'Не найден телефон');
                    return;
                }

                try {
                    $billing = $user->getPiramidaBilling($this->billingFrom->currency);
                } catch (\Exception $e) {
                    $this->addError($attribute, $e->getMessage());
                    return;
                }
                $address = $billing->address;

            } else if ($this->isCardNumber($address)) {
                $address = str_replace(' ', '', $address);

                try {
                    $card = Card::findOne(['number' => $address]);
                    $billing = UserBill::findOne(['card_id' => $card->id, 'currency' => $this->billingFrom->currency]);
                } catch (\Exception $e) {
                    // TODO Здесь можно создать кошелек если не стоит высокая защита
                    $this->addError($attribute, 'Не найден счет для валюты ETH на карте');
                    return;
                }
                $address = $billing->address;
            } else {
                // Подразумевается что это адрес
                if (!StringHelper::startsWith($this->address, '0x')) {
                    $this->addError($attribute, 'Адрес должен начинаться с 0x');
                    return;
                }
            }

            $this->address = $address;
        }
    }

    private function isPhone($address)
    {
        return StringHelper::startsWith($address, '+');
    }

    private function isCardNumber($address)
    {
        $address = str_replace(' ', '', $address);
        if (strlen($address) == 16) {
            return preg_match('/\D/', $address) == 0;
        } else {
            return false;
        }
    }

    /**
     * Проверяет пароль для счета
     * Сравнивает с паролем кабинета
     *
     * @param $attribute
     * @param $params
     */
    public function validatePassword($attribute, $params)
    {
        if (!$this->hasErrors()) {
            if (!$this->billingFrom->validatePassword($this->password)) {
                $this->addError('password', 'Не верный пароль');
                return;
            }
        }
    }

    /**
     * Проверяет что счет существует и принадлежит пользователю
     * и присоединяет объект billing
     *
     * @param $attribute
     * @param $params
     */
    public function validateBilling($attribute, $params)
    {
        if (!$this->hasErrors()) {
            /** @var \common\models\UserAvatar $user */
            try {
                $billing = UserBill::findOne($this->billing_id);
            } catch (\Exception $e) {
                $this->addError('address', 'Не найден счет');
                return;
            }
            if ($billing->user_id != Yii::$app->user->id) {
                $this->addError('address', 'Это не ваш счет. Не красиво так делать');
                return;
            }
            if ($billing->address == $this->address) {
                $this->addError('address', 'Нельзя отправить деньги на счет отправителя');
                return;
            }
            $this->billingFrom = $billing;
        }
    }

    /**
     * Отправляет деньги на счет
     *
     * @return bool|string
     * @throws Exception
     */
    public function send()
    {
        $wallet = $this->billingFrom->getWalletETH();
        try {
            if (!empty($this->gasLimit) || !empty($this->gasPrice)) {
                $amount = [];
                $amount['amount'] = $this->amount;
                if (!empty($this->gasLimit)) $amount['gasLimit'] = $this->gasLimit;
                if (!empty($this->gasPrice)) $amount['gasPrice'] = $this->gasPrice * pow(10,9);
            } else {
                $amount = $this->amount;
            }
            $transaction = $wallet->pay([$this->address => [
                'amount'  => $amount,
                'comment' => $this->comment,
            ]], $this->password);
        } catch (\Exception $e) {
            switch ($e->getCode()) {
                case 400:
                    if (StringHelper::startsWith($e->getMessage(), 'insufficient funds for gas * price + value')) {
                        $this->addError('amount', 'Не хватило денег для оплаты комиссии');
                        break;
                    }
                    if (StringHelper::startsWith(strtolower($e->getMessage()), 'insufficient')) {
                        $this->addError('amount', 'Не хватило денег для оплаты комиссии');
                        break;
                    }
                    $this->addError('amount', 'Другая ошибка от API: '. $e->getMessage());
                    break;
                case 401:
                    $this->addError('amount', 'Не верная авторизация API на сервере');
                    break;
                case 502:
                    $this->addError('amount', 'Сервер не отвечает');
                    break;
                case 402:
                    $this->addError('amount', 'Не верный формат JSON от сервера');
                    break;
                default:
                    $this->addError('amount', $e->getMessage());
                    break;
            }

            return false;
        }

        return $transaction;
    }

    /**
     * Подготовка для отправки, ищет счет пользователя, user billing не обязательно
     *
     * @return array
     * [
     *  'address' => '<string>',
     *  'user' => [
     *      'id' =>
     *      'name2' =>
     *      'avatar' =>
     *  ],
     *  'billing' => [
     *      'id' =>
     *      'currency' => 'BTC'
     *      'image' => '/images/btc.png' (квадратная)
     *  ],
     * ]
     *
     * @throws Exception
     */
    public function prepare()
    {
        try {
            $address = UserBillAddress::findOne(['address' => $this->address]);
            $billing = $address->getBilling();
        } catch (\Exception $e) {
            try {
                $billing = UserBill::findOne(['address' => $this->address, 'currency' => $this->billingFrom->currency]);
            } catch (\Exception $e) {
                return [
                    'address'  => $this->address,
                    'amount'   => $this->amount,
                    'currency' => $this->currency,
                    'hasUser'  => false,
                ];
            }
        }
        $user = $billing->getUser();

        return [
            'address'  => $this->address,
            'amount'   => $this->amount,
            'currency' => $this->currency,
            'hasUser'  => true,
            'user'     => [
                'id'     => $user->id,
                'name2'  => $user->getName2(),
                'avatar' => $user->getAvatar(),
            ],
            'billing'  => [
                'id'       => $billing->id,
                'address'  => $billing->address,
                'name'     => $billing->name,
                'currency' => 'ETH',
                'image'    => '/images/controller/cabinet-bills/index/bitcoin2.jpg',
            ],
        ];
    }
}
