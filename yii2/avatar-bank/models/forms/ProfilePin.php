<?php
/**
 * Created by PhpStorm.
 * User: Бог-Творец
 * Date: 22.11.2016
 * Time: 20:20
 */

namespace avatar\models\forms;


use avatar\services\Html;
use common\models\UserAvatar;
use common\widgets\FileUpload3\FileUpload;
use cs\base\FormActiveRecord;
use yii\base\Exception;
use yii\base\Model;
use yii\widgets\ActiveForm;

class ProfilePin extends Model
{
    public $pin;

    public function rules()
    {
        return [
            ['pin', 'string', 'length' => 5],
        ];
    }

    public function save()
    {
        /** @var \common\models\UserAvatar $user */
        $user = \Yii::$app->user->identity;
        $user->pin = password_hash($this->pin, PASSWORD_DEFAULT);
        $user->save();

        return true;
    }

    public function attributeLabels()
    {
        return [
            'pin' => 'Пин код',
        ];
    }

    public function attributeHints()
    {
        return [
            'pin' => 'Устанавливает новый',
        ];
    }
}