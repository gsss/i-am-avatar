<?php

namespace avatar\models\forms;

use common\components\providers\ETH;
use common\models\avatar\UserBill;
use common\models\school\Page;
use common\models\school\PageBlock;
use common\models\school\PageBlockContent;
use common\models\SendLetter;
use common\models\UserSeed;
use common\services\Security;
use cs\Application;
use cs\services\Str;
use Yii;
use yii\base\InvalidConfigException;
use yii\base\Model;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;
use yii\helpers\StringHelper;
use yii\helpers\VarDumper;

/**
 */
class CabinetSchoolPagesCopy extends Model
{
    /** @var  int Идентификатор страницы */
    public $id;

    /** @var  Page */
    public $page;

    public $name;

    public $link;

    public function init()
    {
        $page = Page::findOne($this->id);
        if (is_null($page)) {
            throw new InvalidConfigException('Не найдена страница');
        }
        $this->page = $page;
        $this->name = $page->name;
        $this->link = substr($page->url, 1);
    }

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            ['name', 'required'],
            ['name', 'string'],

            ['link', 'required'],
            ['link', 'string'],
            ['link', 'trim'],
            ['link', 'validateLink'],
        ];
    }


    /**
     * Проверяет ссылку
     * первый символ не должен быть /
     * только латиница, дефис и подчерк и цифры
     * максимум два вложения, тоесть максимум примерно так /root/page
     * Не должна повторять предыдущие ссылки
     * Не должна пересекаться с резервом
     *
     * @param $attribute
     * @param $params
     */
    public function validateLink($attribute, $params)
    {
        if (!$this->hasErrors()) {
            if (StringHelper::startsWith($this->link, '/')) {
                $this->addError($attribute, 'Ссылка не должна начинаться с символа /');
                return;
            }
            // максимум два вложения, тоесть максимум примерно так /root/page
            // разбиваю на /
            $arr = explode('/', $this->link);
            if (count($arr) > 2) {
                $this->addError($attribute, 'Максимум два вложения, слишком много /');
                return;
            }
            // только латиница, дефис и подчерк и цифры
            $avaliable = [
                'a',
                'b',
                'c',
                'd',
                'e',
                'f',
                'g',
                'h',
                'i',
                'j',
                'k',
                'l',
                'm',
                'n',
                'o',
                'p',
                'q',
                'r',
                's',
                't',
                'u',
                'v',
                'w',
                'x',
                'y',
                'z',
                'A',
                'B',
                'C',
                'D',
                'E',
                'F',
                'G',
                'H',
                'I',
                'J',
                'K',
                'L',
                'M',
                'N',
                'O',
                'P',
                'Q',
                'R',
                'S',
                'T',
                'U',
                'V',
                'W',
                'X',
                'Y',
                'Z',
                '1',
                '2',
                '3',
                '4',
                '5',
                '6',
                '7',
                '8',
                '9',
                '0',
                '-',
                '_',
            ];
            foreach ($arr as $item) {
                $chars = Str::getChars($item);
                foreach ($chars as $c) {
                    if (!in_array($c, $avaliable)) {
                        $this->addError($attribute, 'Символ '. $c . ' не допустим');
                        return;
                    }
                }
            }
            // Не должна повторять предыдущие ссылки
            if (Page::find()->where(['url' => '/' . $this->link, 'school_id' => $this->page->school_id])->exists()) {
                $this->addError($attribute, 'Такая страница уже есть');
                return;
            }
            // Не должна пересекаться с резервом
            foreach (Page::$rezervUrlList as $i) {
                if ($i == $this->link) {
                    $this->addError($attribute, 'Эта ссылка зарещервирована');
                    return;
                }
            }
        }
    }

    /**
     * Копирует страницу
     *
     * @return array
     *
     * [$newPage, $items]
     */
    public function action()
    {
        $fields = $this->page->attributes;
        $fields['name'] = $this->name;
        $fields['url'] = '/' . $this->link;
        unset($fields['id']);
        $newPage = new Page($fields);
        $newPage->save();
        $newPage->id = $newPage::getDb()->lastInsertID;

        $items = [];
        /** @var \common\models\school\PageBlockContent $c */
        foreach (PageBlockContent::find()->where(['page_id' => $this->page->id])->all() as $c) {
            $fields = $c->attributes;
            $fields['page_id'] = $newPage->id;
            unset($fields['id']);
            $newContent = new PageBlockContent($fields);
            $newContent->save();
            $items[] = $newContent;
        }

        return [$newPage, $items];
    }
}
