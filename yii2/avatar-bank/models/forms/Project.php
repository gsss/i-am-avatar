<?php
namespace avatar\models\forms;

use avatar\modules\Piramida\PaySystems\Base\Model;
use cs\base\FormActiveRecord;
use cs\services\VarDumper;
use Yii;
use yii\base\NotSupportedException;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\web\IdentityInterface;

/**
 * Project model
 * Хранится проекты ICO и краудфандинг
 *
 * @property int id            Идентификатор
 * @property string name          Наименование проекта
 * @property string key           Идентификатор проекта, случайная строка из 10 символов
 * @property string content       Текст описания проекта
 * @property string image         Картинка
 * @property int user_id       Идентификатор пользователя кому принадлежит проект
 * @property int status        Флаг. Прошел врификацию? 0 - не прошел верификацию и не отправил на верификацию (черновик)
 *                                  2 - отправил на верификацию
 *                                  3 - верифицировано
 * @property int time_start    Время начала краудфандинга
 * @property int time_end      Время окончания краудфандинга
 * @property int currency      Идентификатор валюты по таблице currency
 * @property int created_at    Время добавления
 *
 */
class Project extends \yii\base\Model
{
    public $id;
    public $name;
    public $content;
    public $image;
    public $time_start_date;
    public $time_end_date;
    public $time_start_time;
    public $time_end_time;
    public $is_hide_client;

    public static $imageOptions;

    public function init()
    {
        self::$imageOptions = [
            'widget' => [
                '\common\widgets\FileUpload3\FileUpload', [
                    'options' => [
                        'small' => \avatar\services\Html::$formatIcon
                    ]
                ]
            ]
        ];
    }

    /**
     * @param \common\models\investment\Project $project
     *
     */
    public function set($project)
    {
        $this->id = $project->id;
        $this->name = $project->name;
        $this->content = $project->content;
        $this->is_hide_client = $project->is_hide_client;
        $this->image = $project->image;
        $i = new \common\widgets\FileUpload3\FileUpload([
            'model'     => $this,
            'attribute' => 'image',
            'value'     => $this->image,
        ]);
        $i->onLoadDb(\avatar\models\forms\Project::$imageOptions);
        $start = new \DateTime('@' . $project->time_start);
        $this->time_start_date = $start->format('d.m.Y');
        $this->time_start_time = $start->format('H:i:s');
        $end = new \DateTime('@' . $project->time_end);
        $this->time_end_date = $end->format('d.m.Y');
        $this->time_end_time = $end->format('H:i:s');
    }

    public function rules()
    {
        return [
            [[
                'name',
                'content',
                'image',

                'time_start_date',
                'time_start_time',

                'time_end_date',
                'time_end_time',
            ], 'safe'],

            ['name', 'required'],
            ['content', 'required'],
            ['time_start_date', 'required'],
            ['time_start_time', 'required'],
            ['time_end_date', 'required'],
            ['time_end_time', 'required'],

            ['is_hide_client', 'integer'],
        ];
    }

    public static function tableName()
    {
        return 'projects';
    }

    public function beforeSave($project = null)
    {
        if (!is_null($project)) {
            $model = $project;
        } else {
            $model = $this;
        }
        $i = new \common\widgets\FileUpload3\FileUpload([
            'model'     => $model,
            'attribute' => 'image',
            'value'     => $model->image,
        ]);
        $i->onLoad(\avatar\models\forms\Project::$imageOptions);
        $i->onUpdate(\avatar\models\forms\Project::$imageOptions);

        $i = new \common\widgets\DatePicker\DatePicker([
            'model'      => $this,
            'attribute'  => 'time_start_date',
            'value'      => $this->time_start_date,
            'dateFormat' => 'php:d.m.Y'
        ]);
        $i->onLoad(['widget' => [0, 'dateFormat' => 'php:d.m.Y']]);
        $i->onUpdate(['dateFormat' => 'php:d.m.Y']);

        $i = new \common\widgets\DatePicker\DatePicker([
            'model'      => $this,
            'attribute'  => 'time_end_date',
            'value'      => $this->time_end_date,
            'dateFormat' => 'php:d.m.Y'
        ]);
        $i->onLoad(['widget' => [0, 'dateFormat' => 'php:d.m.Y']]);
        $i->onUpdate(['dateFormat' => 'php:d.m.Y']);

        $i = new \common\widgets\HtmlContent\HtmlContent([
            'model'      => $this,
            'attribute'  => 'content',
            'value'      => $this->content,
        ]);
        $i->onUpdate();
    }

    public function action()
    {
        $p = new \common\models\investment\Project();
        $p->name = $this->name;
        $p->save();
        $this->id = $p::getDb()->lastInsertID;
        $this->beforeSave();
        $p->content = $this->content;
        $p->image = $this->image;
        $p->is_hide_client = $this->is_hide_client;
        $p->time_start = \DateTime::createFromFormat('Ymd H:i:s', $this->time_start_date . ' ' . $this->time_start_time, new \DateTimeZone('UTC'))->format('U');
        $p->time_end = \DateTime::createFromFormat('Ymd H:i:s', $this->time_end_date . ' ' . $this->time_end_time, new \DateTimeZone('UTC'))->format('U');

        $i1 = new \common\models\PaySystemListItem(['name' => 'ICO '. $this->name, 'created_at' => time()]);
        $i1->save();
        $i1->id = $i1::getDb()->lastInsertID;

        $i = $p;
        $i->payments_list_id = $i1->id;
        $i->created_at = time();
        $i->user_id = Yii::$app->user->id;
        $i->status = 0;
        $i->type = 1;
        $i->save();
    }

    /**
     * @param \common\models\investment\Project $project
     */
    public function actionUpdate($project)
    {
        $this->beforeSave($project);
        $project->content = $this->content;
        $project->name = $this->name;
        $project->is_hide_client = $this->is_hide_client;
        $project->time_start = \DateTime::createFromFormat('Ymd H:i:s', $this->time_start_date . ' ' . $this->time_start_time, new \DateTimeZone('UTC'))->format('U');
        $project->time_end = \DateTime::createFromFormat('Ymd H:i:s', $this->time_end_date . ' ' . $this->time_end_time, new \DateTimeZone('UTC'))->format('U');
        $project->save();
    }

}
