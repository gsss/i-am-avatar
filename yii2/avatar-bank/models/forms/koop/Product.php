<?php
namespace avatar\models\forms\koop;

use cs\base\FormActiveRecord;
use Yii;
use yii\base\NotSupportedException;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;
use yii\web\IdentityInterface;

/**
 * @property integer        id
 * @property integer        company_id
 * @property string(20)     code
 * @property string(200)    image
 * @property string(255)    title
 * @property double         price
 * @property integer        currency
 * @property integer        count
 * @property integer        created_at
 * @property integer        is_balance_koop     0 - на балансе компании, 1 - на балансе кооператива
 * @property integer        koop_id             идентификатор на балансе какого кооператива числится данный товар.
 */
class Product extends FormActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'koop_products';
    }

    public function formAttributes()
    {
        return [
            [
                'code',
                'Код товара',
                0,
                'string',
                ['max' => 200],
            ],
            [
                'title',
                'Наименование',
                1,
                'string',
                ['max' => 255],
            ],
            [
                'price',
                'Цена',
                0,
                'double',
                ['min' => 0],
            ],
            [
                'count',
                'Кол-во единиц',
                1,
                'integer',
            ],
            [
                'image',
                'Картинка',
                0,
                '\common\widgets\FileUpload3\Validator',
                'widget' => [
                    '\common\widgets\FileUpload3\FileUpload',
                    [
                        'options' => [
                            'small' => \avatar\services\Html::$formatIcon
                        ]
                    ]
                ]
            ],
        ];
    }
}
