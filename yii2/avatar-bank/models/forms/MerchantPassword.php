<?php
namespace avatar\models\forms;

use common\models\avatar\UserBill;
use common\services\Security\AES;
use cs\base\FormActiveRecord;
use Yii;
use yii\base\Model;
use yii\base\NotSupportedException;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\web\IdentityInterface;

/**
 */
class MerchantPassword extends Model
{
    public $password1;

    public function rules()
    {
        return [
            ['password1', 'required'],
            ['password1', 'validatePassword'],
        ];
    }

    public function validatePassword($attribute, $params)
    {
        if (!$this->hasErrors()) {
            /** @var \common\models\UserAvatar $user */
            $user = Yii::$app->user->identity;
            if (!$user->validatePassword($this->password1)) {
                $this->addError($attribute, 'Не верный пароль');
            }
        }
    }

    /**
     * Расшифровывавет пароль
     *
     * @param \common\models\avatar\UserBill $billing
     */
    public function action($billing)
    {
        $key32 = UserBill::passwordToKey32($this->password1);
        $password = AES::decrypt256CBC($billing->password, $key32);
        $billing->password = $password;
        $billing->password_type = UserBill::PASSWORD_TYPE_OPEN;
        $billing->save();
    }
}
