<?php

namespace avatar\models\forms;

use common\models\avatar\UserBill;
use common\models\UserDocument;
use common\payment\BitCoinBlockTrailPayment;
use cs\base\FormActiveRecord;
use cs\services\BitMask;
use yii\base\Exception;
use yii\db\ActiveRecord;
use yii\db\Query;
use yii\helpers\Json;
use yii\helpers\VarDumper;

/**
 */
class GoogleAuth extends \yii\base\Model
{
    public $code;
    public $pin;

    public function rules()
    {
        return [
            ['code', 'required'],

            ['pin', 'required'],
            ['pin', 'trim'],
            ['pin', 'removeSpace'],
            ['pin', 'validatePassword'],
        ];
    }

    public function removeSpace()
    {
        if (!$this->hasErrors()) {
            $this->pin = str_replace(' ','', $this->pin);
        }
    }

    public function validatePassword($attribute, $params)
    {
        if (!$this->hasErrors()) {
            $secret = $this->code;
            $pinExternal = $this->pin;
            $file = \Yii::getAlias('@vendor/chregu/GoogleAuthenticator.php/lib/GoogleAuthenticator.php');
            require_once($file);
            $ga = new \GoogleAuthenticator;
            $pinInternal = $ga->getCode($secret);
            if ($pinInternal != $pinExternal) {
                $this->addError($attribute, 'Пин код не верный');
                return;
            }
        }
    }

    public function action()
    {
        /** @var \common\models\UserAvatar $user */
        $user = \Yii::$app->user->identity;
        $user->google_auth_code = $this->code;
        $user->save();
    }
}