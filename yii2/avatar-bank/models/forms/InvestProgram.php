<?php
namespace avatar\models\forms;

use common\models\school\File;
use cs\base\FormActiveRecord;
use cs\services\Url;
use cs\services\VarDumper;
use Yii;
use yii\base\NotSupportedException;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\web\IdentityInterface;

/**
 * @property integer id
 * @property integer school_id
 * @property string  name
 */
class InvestProgram extends \iAvatar777\services\FormAjax\ActiveRecord
{
    public static function tableName()
    {
        return 'school_invest_program';
    }

    public function rules()
    {
        return [
            ['project_id', 'integer'],

            ['name', 'string'],

            ['success_url', 'string'],
            ['error_url', 'string'],
        ];
    }

    public function attributeLabels()
    {
        return ['name' => 'Наименование'];
    }
}
