<?php
namespace avatar\models\forms;

use common\models\avatar\Currency;
use common\models\Token;
use cs\services\VarDumper;
use yii\base\Model;
use yii\helpers\StringHelper;

/**
 * TokenAdd model
 *
 */
class TokenRegistration extends Model
{
    /** @var  string */
    public $abi;

    /** @var  string */
    public $address;

    /** @var  string */
    public $title;

    /** @var  string */
    public $code;

    /** @var  int */
    public $decimals;

    /** @var  string */
    public $image;

    public $optionsImage = [
        'image',
        'image',
        'image',
        'widget' => [
            '\common\widgets\FileUpload31\FileUpload',
            [
                'tableName' => 'currency',
                'options'   => [
                    'small' => [
                        300,
                        300,
                        \common\widgets\FileUpload3\FileUpload::MODE_THUMBNAIL_CUT,
                    ],
                ]
            ]
        ],
    ];

    /**
     * @param int $id идентификатор токена
     */
    public function set($id)
    {
        $token = Token::findOne($id);
        $this->abi = $token->abi;
        $this->address = $token->address;
        $currency = Currency::findOne($token->currency_id);
        $this->code = $currency->code;
        $this->decimals = $currency->decimals;
        $this->title = $currency->title;
        $this->image = $currency->image;
        $class = new \common\widgets\FileUpload31\FileUpload([
            'tableName' => 'currency',
            'model'     => $this,
            'attribute' => 'image',
            'value'     => $this->image,
        ]);
        $class->onLoadDb([]);
    }

    public function rules()
    {
        return [
            [['abi', 'address', 'title', 'code', 'decimals', 'image'], 'safe'],
            ['address', 'validateAddress'],
            ['abi', 'default', 'value' => '[{"constant":true,"inputs":[],"name":"name","outputs":[{"name":"","type":"string"}],"payable":false,"type":"function"},{"constant":false,"inputs":[{"name":"_spender","type":"address"},{"name":"_value","type":"uint256"}],"name":"approve","outputs":[{"name":"success","type":"bool"}],"payable":false,"type":"function"},{"constant":true,"inputs":[],"name":"totalSupply","outputs":[{"name":"","type":"uint256"}],"payable":false,"type":"function"},{"constant":false,"inputs":[{"name":"_from","type":"address"},{"name":"_to","type":"address"},{"name":"_value","type":"uint256"}],"name":"transferFrom","outputs":[{"name":"success","type":"bool"}],"payable":false,"type":"function"},{"constant":true,"inputs":[],"name":"decimals","outputs":[{"name":"","type":"uint8"}],"payable":false,"type":"function"},{"constant":true,"inputs":[],"name":"version","outputs":[{"name":"","type":"string"}],"payable":false,"type":"function"},{"constant":true,"inputs":[{"name":"_owner","type":"address"}],"name":"balanceOf","outputs":[{"name":"balance","type":"uint256"}],"payable":false,"type":"function"},{"constant":true,"inputs":[],"name":"symbol","outputs":[{"name":"","type":"string"}],"payable":false,"type":"function"},{"constant":false,"inputs":[{"name":"_to","type":"address"},{"name":"_value","type":"uint256"}],"name":"transfer","outputs":[{"name":"success","type":"bool"}],"payable":false,"type":"function"},{"constant":false,"inputs":[{"name":"_spender","type":"address"},{"name":"_value","type":"uint256"},{"name":"_extraData","type":"bytes"}],"name":"approveAndCall","outputs":[{"name":"success","type":"bool"}],"payable":false,"type":"function"},{"constant":true,"inputs":[{"name":"_owner","type":"address"},{"name":"_spender","type":"address"}],"name":"allowance","outputs":[{"name":"remaining","type":"uint256"}],"payable":false,"type":"function"},{"inputs":[{"name":"_initialAmount","type":"uint256"},{"name":"_tokenName","type":"string"},{"name":"_decimalUnits","type":"uint8"},{"name":"_tokenSymbol","type":"string"}],"payable":false,"type":"constructor"},{"payable":false,"type":"fallback"},{"anonymous":false,"inputs":[{"indexed":true,"name":"_from","type":"address"},{"indexed":true,"name":"_to","type":"address"},{"indexed":false,"name":"_value","type":"uint256"}],"name":"Transfer","type":"event"},{"anonymous":false,"inputs":[{"indexed":true,"name":"_owner","type":"address"},{"indexed":true,"name":"_spender","type":"address"},{"indexed":false,"name":"_value","type":"uint256"}],"name":"Approval","type":"event"}]'],
        ];
    }

    public function validateAddress($attribute, $params)
    {
        if (!$this->hasErrors()) {
            if (!StringHelper::startsWith($this->address, '0x')) {
                $this->addError($attribute, 'Адрес должен начинаться с 0x');
                return;
            }
            $t = Token::findOne(['address' => $this->address]);
            if (!is_null($t)) {
                $this->addError($attribute, 'Такой токен уже зарегистрирован id='. $t->id);
                return;
            }
        }
    }

    public function load($data, $formName = null)
    {
        $ret = parent::load($data, $formName);

        if ($ret) {
            $class = new \common\widgets\FileUpload31\FileUpload([
                'tableName' => 'currency',
                'model'     => $this,
                'attribute' => 'image',
                'value'     => $this->image,
            ]);
            $class->onLoad($this->optionsImage);
        }

        return $ret;
    }

    public function action()
    {
        // добавление currency
        $currency = new Currency([
            'code'     => $this->code,
            'title'    => $this->title,
            'decimals' => $this->decimals,
        ]);
        $currency->save();
        $currency->id = $currency::getDb()->lastInsertID;

        $class = new \common\widgets\FileUpload31\FileUpload([
            'tableName' => 'currency',
            'model'     => $this,
            'attribute' => 'image',
            'value'     => $this->image,
            'id'        => $currency->id,
        ]);
        $class->onUpdate($this->optionsImage);
        $currency->image = $this->image;
        $currency->save();

        // добавление token
        $token = new \common\models\Token([
            'address'     => strtolower($this->address),
            'abi'         => $this->abi,
            'currency_id' => $currency->id,
        ]);
        $token->save();
    }

    /**
     * @param int $id идентификатор токена
     *
     * @return bool
     */
    public function save($id)
    {

        $class = new \common\widgets\FileUpload31\FileUpload([
            'tableName' => 'currency',
            'model'     => $this,
            'attribute' => 'image',
            'value'     => $this->image,
        ]);
        $class->onUpdate($this->optionsImage);

        $token = \common\models\Token::findOne($id);
        $token->abi = $this->abi;
        $token->address = $this->address;
        $token->save();

        $currency = Currency::findOne($token->currency_id);
        $currency->image = $this->image;
        $currency->code = $this->code;
        $currency->title = $this->title;
        $currency->decimals = $this->decimals;
        $currency->save();

        return true;
    }
}
