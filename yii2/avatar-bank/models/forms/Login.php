<?php

namespace avatar\models\forms;

use avatar\models\UserLogin;
use common\models\UserAvatar;
use cs\base\BaseForm;
use cs\base\FormActiveRecord;
use cs\services\Str;
use Yii;
use yii\base\Model;
use yii\helpers\ArrayHelper;
use yii\helpers\StringHelper;

/**
 */
class Login extends Model
{
    /** @var  string email или телефон */
    public $username;

    public $password;

    private $_user = false;

    public function rules()
    {
        return [
            ['password', 'string', 'min' => 3],
            ['username', 'validateUser'],
            ['password', 'validatePassword'],
        ];
    }

    private function validatePhone($amount)
    {
        $symbols = Str::getChars($amount);
        foreach ($symbols as $char) {
            if (!in_array($char, ['0','1','2','3','4','5','6','7','8','9'])) return false;
        }
        return true;
    }

    public function validateUser($attribute, $params)
    {
        if (!$this->hasErrors()) {
            if (StringHelper::startsWith($this->username, '+7')) {
                // Это телефон
                // +79252374501
                if (strlen($this->username) != 12) {
                    $this->addError($attribute, Yii::t('c.Q2vFnrwWB1', 'В телефоне должно быть 12 символов и никаких спецсимволов'));
                    return;
                }
                if (!$this->validatePhone(substr($this->username, 1))) {
                    $this->addError($attribute, Yii::t('c.Q2vFnrwWB1', 'В телефоне должны быть только цифры'));
                    return;
                }
                try {
                    $user = $this->getUserPhone();
                } catch (\Exception $e) {
                    $user = null;
                }
            } else {
                if (strpos($this->username, '@')) {
                    if (!filter_var($this->username, FILTER_VALIDATE_EMAIL)) {
                        $this->addError($attribute, Yii::t('c.Q2vFnrwWB1', 'Не верный формат email'));
                        return;
                    }
                } else {
                    $this->addError($attribute, Yii::t('c.Q2vFnrwWB1', 'Email должен содержать').' @');
                    return;
                }
                try {
                    $user = $this->getUser();
                } catch (\Exception $e) {
                    $user = null;
                }
            }

            if (is_null($user)) {
                $this->addError($attribute, Yii::t('c.Q2vFnrwWB1', 'Пользователя нет'));
                return;
            }
            if ($user->password == '') {
                $this->addError($attribute, Yii::t('c.Q2vFnrwWB1', 'Вы  не завели себе пароль'));
                return;
            }
            if ($user->mark_deleted == 1) {
                $this->addError($attribute, Yii::t('c.Q2vFnrwWB1', 'Пользователь заблокирован'));
                return;
            }
        }
    }

    /**
     * Validates the password.
     * This method serves as the inline validation for password.
     *
     * @param string $attribute the attribute currently being validated
     * @param array $params the additional name-value pairs given in the rule
     */
    public function validatePassword($attribute, $params)
    {
        if (!$this->hasErrors()) {
            $user = $this->getUser();

            if (is_null($user)) {
                return;
            }

            if ($user->validatePassword($this->password) == false) {
                $this->addError($attribute, Yii::t('c.Q2vFnrwWB1', 'Не верный пароль'));
            }
        }
    }

    /**
     * Logs in a user using the provided username and password.
     * @return boolean whether the user is logged in successfully
     */
    public function login($timeOut = 0)
    {
        return Yii::$app->user->login($this->getUser(), $timeOut);
    }

    /**
     * Finds user by [email]]
     *
     * @return \common\models\UserAvatar | null
     */
    public function getUser()
    {
        if ($this->_user === false) {
            $this->_user = UserAvatar::findByUsername($this->username);
        }

        return $this->_user;
    }

    /**
     * Finds user by [[phone]]
     *
     * @return \common\models\UserAvatar | null
     */
    public function getUserPhone()
    {
        if ($this->_user === false) {
            $this->_user = UserAvatar::findOne(['phone' => substr($this->username,1)]);
        }

        return $this->_user;
    }
}
