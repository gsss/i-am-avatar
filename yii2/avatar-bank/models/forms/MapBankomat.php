<?php
namespace avatar\models\forms;

use cs\base\FormActiveRecord;
use Yii;
use yii\base\NotSupportedException;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\web\IdentityInterface;

/**
 * PaySystem model
 *
 * @property integer id
 * @property string  name
 * @property string  image
 * @property string  content
 * @property string  description
 * @property integer created_at
 * @property integer user_id
 * @property integer btc
 * @property integer eth
 * @property integer enc
 * @property float   lat
 * @property float   lng
 */
class MapBankomat extends FormActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'map_bankomat';
    }

    public function formAttributes()
    {
        return [
            [
                'name',
                'Название',
                0,
                'string',
            ],
            [
                'address',
                'Адрес',
                0,
                'string', [], 'Где расположен, Город, улица, дом и точка дислокации'
            ],
            [
                'description',
                'Описание',
                0,
                'string',
            ],
            [
                'content',
                'Полный текст',
                0,
                'string',
                'widget' => [
                    '\common\widgets\HtmlContent\HtmlContent',
                ]
            ],
            [
                'point',
                'Точка',
                0,
                '\common\widgets\FileUpload3\Validator',
                'widget' => [
                    '\common\widgets\PlaceMapYandex\PlaceMap',
                ]
            ],
            [
                'image',
                'Картинка',
                0,
                '\common\widgets\FileUpload3\Validator',
                'widget' => [
                    '\common\widgets\FileUpload3\FileUpload',
                    [
                        'options' => [
                            'small' => \avatar\services\Html::$formatIcon
                        ]
                    ]
                ]
            ],
        ];
    }
}
