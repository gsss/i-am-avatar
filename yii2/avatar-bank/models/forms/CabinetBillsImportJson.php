<?php

namespace avatar\models\forms;

use common\components\providers\ETH;
use common\models\avatar\UserBill;
use common\models\RequestTokenCreate;
use common\models\SendLetter;
use common\models\UserSeed;
use common\services\Security;
use cs\Application;
use cs\services\Str;
use Yii;
use yii\base\Model;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;
use yii\helpers\VarDumper;
use yii\web\UploadedFile;

/**
 */
class CabinetBillsImportJson extends Model
{
    /** @var  string Пароль от JSON файла */
    public $password;

    /** @var  string Пароль от кабинета */
    public $userPassword;

    /** @var  string файл */
    public $json;

    /** @var  string содержимое файла $json, устанавливается в validatePasswordJson */
    private $contentJson;

    /** @var  string адрес кошелька начинающийся на 0x и малыми буквами, устанавливается в validatePasswordJson  */
    private $address;

    /** @var  string */
    public $name;

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            ['json', 'file'],

            ['name', 'required'],
            ['name', 'string'],

            ['password', 'required'],
            ['password', 'validatePasswordJson'], // устанавливает $contentJson

            ['userPassword', 'required'],
            ['userPassword', 'validatePassword'],
        ];
    }

    /**
     * @return array customized attribute labels
     */
    public function attributeLabels()
    {
        return [
            'password' => 'Пароль',
            'json'     => 'JSON файл',
        ];
    }

    /**
     * Конвертирует массив ошибок от формы чтобы их можно было выдать через JSON
     *
     * @param array $params
     *      [
     *      'amount' => ['Не верное число',...],
     *      ]
     *
     * @return array
     * [
     *      'name'  => 'amount',
     *      'value' => ['Не верное число',...],
     * ]
     */
    public function convert($params)
    {
        $ret = [];
        foreach ($params as $name => $arr) {
            $ret[] = [
                'name'  => $name,
                'value' => $arr,
            ];
        }
        return $ret;
    }

    public function validatePassword($attribute, $params)
    {
        if (!$this->hasErrors()) {
            /** @var \common\models\UserAvatar $user */
            $user = Yii::$app->user->identity;
            if (!$user->validatePassword($this->userPassword)) {
                $this->addError($attribute, 'Пароль не верный');
            }
        }
    }

    public function validatePasswordJson($attribute, $params)
    {
        if (!$this->hasErrors()) {
            $file = UploadedFile::getInstance($this, 'json');
            $path = $file->tempName;

            // получаю содержимое файла JSON
            $contentJson = file_get_contents($path);
            $this->contentJson = $contentJson;

            // Получаю адрес кошелька
            $address = $this->getAddress($contentJson);
            $this->address = $address;

            // Проверяю пароль от JSON
            {
                /** @var \common\components\providers\ETH $provider */
                $provider = Yii::$app->eth;
                try {
                    $result = $provider->testPassword($address, $this->password);
                } catch (\Exception $e) {
                    $this->addError($attribute, $e->getMessage());
                }
            }
        }
    }

    /**
     * Импортирует кошелек и создает для него счет
     *
     * @return \common\models\avatar\UserBill объект созданного счета
     */
    public function action()
    {
        // Импортирую кошелек на сервер
        {
            /** @var \common\components\providers\ETH $provider */
            // $provider = Yii::$app->eth;
            // $address = $provider->importJsonFile($contentJson, $this->password);
        }

        $fields = [
            'name'          => $this->name,
            'address'       => $this->address,
            'currency'      => \common\models\avatar\Currency::ETH,
            'user_id'       => Yii::$app->user->id,
        ];
        $fields = ArrayHelper::merge($fields, $this->getPasswordFields($this->password, $this->userPassword));
        $billing = UserBill::add($fields);

        return $billing;
    }

    /**
     * Выдает адрес кошелька из файла JSON
     *
     * @param string $contentJson
     *
     * @return string адрес кошелька начинающийся с '0x'
     */
    private function getAddress($contentJson)
    {
        $data = Json::decode($contentJson);

        return '0x' . $data['address'];
    }

    /**
     * Создает для \common\models\avatar\UserBill поля password и password_type в зависимомти от типа установленного у пользователя
     *
     * @param string $walletPassword
     * @param string $userPassword
     *
     * @return array
     * [
     *      'password_type'
     *      'password'
     * ]
     */
    private function getPasswordFields($walletPassword, $userPassword)
    {
        /** @var \common\models\UserAvatar $user */
        $user = Yii::$app->user->identity;
        if ($user->password_save_type == UserBill::PASSWORD_TYPE_OPEN) {
            return [
                'password_type' => UserBill::PASSWORD_TYPE_OPEN,
                'password'      => $walletPassword,
            ];
        } else {
            return [
                'password_type' => $user->password_save_type,
                'password'      => UserBill::passwordEnСript($walletPassword, $userPassword),
            ];
        }
    }
}
