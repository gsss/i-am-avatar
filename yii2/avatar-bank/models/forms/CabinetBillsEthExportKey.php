<?php

namespace avatar\models\forms;

use common\components\providers\ETH;
use common\models\SendLetter;
use common\models\UserSeed;
use common\services\Security;
use cs\Application;
use cs\services\Str;
use Yii;
use yii\base\Model;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;
use yii\helpers\VarDumper;

/**
 */
class CabinetBillsEthExportKey extends Model
{
    /** @var  \common\models\avatar\UserBill */
    public $billing;

    /** @var  string */
    public $password;

    /** @var  string */
    public $pin;

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        /** @var \common\models\UserAvatar $user */
        $user = Yii::$app->user->identity;

        if (!empty($user->google_auth_code)) {
            return [
                ['password', 'required'],
                ['password', 'string'],
                ['password', 'validatePassword'],

                ['pin', 'validateRequired', 'skipOnEmpty' => false],
                ['pin', 'integer'],
                ['pin', 'validatePin'],
            ];
        } else {
            return [
                ['password', 'required'],
                ['password', 'string'],
                ['password', 'validatePassword'],
            ];
        }
    }

    public function validateRequired($attribute, $params)
    {
        $user = Yii::$app->user->identity;
        if (!empty($user->google_auth_code)) {
            if (empty($this->pin)) {
                $this->addError($attribute, 'PIN должен быть заполнен обязательно');
            }
        }
    }

    public function validatePassword($attribute, $params)
    {
        if (!$this->hasErrors()) {
            /** @var \common\models\UserAvatar $user */
            $user = Yii::$app->user->identity;
            if (!$user->validatePassword($this->password)) {
                $this->addError($attribute, 'Пароль не верный');
            }
        }
    }

    public function validatePin($attribute, $params)
    {
        if (!$this->hasErrors()) {
            /** @var \common\models\UserAvatar $user */
            $user = Yii::$app->user->identity;
            if (!$user->validateGoogleAuthCode($this->pin)) {
                $this->addError($attribute, 'Код не верный');
            }
        }
    }

    /**
     * Экспортирует ключ
     *
     * @return string SEEDs
     */
    public function action()
    {
        $data = $this->billing->getWalletETH()->getPrivateJson($this->password);

        return Yii::$app->response->sendContentAsFile(Json::encode($data), $this->billing->address . '.json');
    }
}
