<?php

namespace avatar\models\forms;

use common\models\avatar\CurrencyLink;
use common\models\avatar\UserBill;
use common\models\piramida\Wallet;
use cs\Application;
use cs\base\FormActiveRecord;
use Yii;
use yii\base\Model;
use yii\base\NotSupportedException;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\web\IdentityInterface;

/**
 */
class SendAtom2 extends Model
{
    /** @var  int Идентификатор счета user_bill.id */
    public $id;

    /** @var  int Кол-во монет */
    public $amount;

    /** @var  int Кол-во атомов для отправки */
    public $amountToSend;

    public $comment;

    public $user_email;

    /** @var  \common\models\UserAvatar */
    public $user;

    /** @var  \common\models\piramida\Wallet */
    public $walletIn;

    /** @var  \common\models\piramida\Wallet */
    public $walletOut;
    /** @var  \common\models\avatar\CurrencyLink */
    public $link;

    /** @var  \common\models\avatar\UserBill */
    public $billOut;

    public function rules()
    {
        return [
            ['id', 'required'],
            ['id', 'integer'],
            ['id', 'exist', 'targetClass' => '\common\models\avatar\UserBill'],
            ['id', 'setItem'],

            ['amount', 'required'],
            ['amount', 'integer'],
            ['amount', 'convertToAtom'],
            ['amount', 'validateOutWallet'],

            ['comment', 'string'],
            ['comment', 'default', 'value' => 'Отправление'],

            ['user_email', 'string'],
            ['user_email', 'exist', 'targetAttribute' => 'email', 'targetClass' => '\common\models\UserAvatar'],
            ['user_email', 'setUser'],
            ['user_email', 'setWalletIn'],

        ];
    }

    public function setItem($attribute, $params)
    {
        if (!$this->hasErrors()) {
            try {
                $billOut = UserBill::findOne($this->id);
            } catch (\Exception $e) {
                $this->addError($attribute, 'Счет не найден');
                return;
            }
            if ($billOut->mark_deleted) {
                $this->addError($attribute, 'Счет удален');
                return;
            }
            $link = CurrencyLink::findOne(['currency_ext_id' => $billOut->currency]);
            if (is_null($link)) {
                $this->addError($attribute, 'Эта монета не предназначена для перевода');
                return;
            }
            $walletOut = Wallet::findOne($billOut->address);
            $this->link = $link;
            $this->walletOut = $walletOut;
            $this->billOut = $billOut;
        }
    }

    public function convertToAtom($attribute, $params)
    {
        if (!$this->hasErrors()) {
            $currency = \common\models\piramida\Currency::findOne($this->link->currency_int_id);
            $this->amountToSend = $this->amount * pow(10, $currency->decimals);
        }
    }

    public function validateOutWallet($attribute, $params)
    {
        if (!$this->hasErrors()) {
            if ($this->walletOut->amount < $this->amountToSend) {
                $this->addError($attribute, 'Недостаточно денег в кошельке');
                return;
            }
        }
    }

    public function setUser($attribute, $params)
    {
        if (!$this->hasErrors()) {
            $user = \common\models\UserAvatar::findOne(['email' => $this->user_email]);

            $this->user = $user;
        }
    }

    /**
     * Устанавливает входящий кошелек. Выбирает кошелек по умолчанию, если нет, то создает
     */
    public function setWalletIn($attribute, $params)
    {
        if (!$this->hasErrors()) {
            $bill = UserBill::getBillByCurrencyAvatarProcessing($this->link->currency_int_id, $this->user->id);
            $wallet = Wallet::findOne($bill->address);

            $this->walletIn = $wallet;
        }
    }

    /**
     * @return \common\models\Piramida\Transaction
     */
    public function action()
    {
        $t = $this->walletOut->move(
            $this->walletIn,
            $this->amountToSend,
            $this->comment
        );

        return $t;
    }
}
