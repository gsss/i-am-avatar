<?php

namespace avatar\models\forms;

use common\models\SendLetter;
use common\models\UserSeed;
use common\services\Security;
use cs\Application;
use cs\services\Str;
use Yii;
use yii\base\Model;
use yii\helpers\ArrayHelper;
use yii\helpers\VarDumper;

/**
 */
class RegistrationActivate extends Model
{
    public $password1;
    public $password2;

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            [['password1', 'password2'], 'required', 'message' => 'Это поле должно быть заполнено обязательно'],
            ['password2', 'compare', 'compareAttribute' => 'password1', 'operator' => '==', 'message' => 'Пароли должны совпадать'],
        ];
    }

    /**
     * @return array customized attribute labels
     */
    public function attributeLabels()
    {
        return [
            'password1' => 'Пароль',
            'password2' => 'Пароль еще раз',
        ];
    }

    /**
     * Задает пароль и создает первый кошелек
     *
     * @return string SEEDs
     */
    public function action()
    {
        $userCabinet = $this->password1;
        // сохраняет кошелек
        /** @var \common\models\UserAvatar $user */
        $user = Yii::$app->user->identity;
        $user->setPassword($userCabinet);

        // генерирует SEEDs и сохраняет
        $seeds = Security::getSeeds();
        $sha256 = hash('sha256', $seeds);
        $key32 = substr($sha256, 0, 32);
        $hash = Security\AES::encrypt256CBC($userCabinet, $key32);
        $seedsHashObject = UserSeed::add($hash);

        return $seeds;
    }
}
