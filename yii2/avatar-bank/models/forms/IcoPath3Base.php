<?php

namespace avatar\models\forms;

use avatar\models\WalletToken;
use common\models\avatar\UserBill;
use common\models\BillingMain;
use common\models\investment\IcoRequest;
use common\models\PaySystemConfig;
use common\models\Token;
use Yii;
use yii\base\Model;

/**
 * Базовый класс для третьего маршрута
 *
 */
class IcoPath3Base extends Model
{
    /** @var  float кол-во заказанных токенов */
    public $tokens;

    /** @var  int идентификатор валюты */
    public $currency;

    /** @var  int */
    protected $icoId;

    /** @var  \common\models\investment\Project */
    protected $ico;

    /** @var  \common\models\PaySystemConfig */
    protected $config;

    /** @var  \common\models\PaySystem */
    protected $paySystem;

    /** @var  \common\models\avatar\Currency */
    protected $currencyObject;

    /** @var string */
    protected $address;

    public function init()
    {
        $this->icoId = \Yii::$app->session->get('ico')['id'];
        $this->ico = \common\models\investment\Project::findOne($this->icoId);

        $this->currencyObject = Currency::findOne($this->currency);
        $id = PaySystemConfig::find()
            ->select(['paysystems_config.id'])
            ->innerJoin('paysystems', 'paysystems.id = paysystems_config.paysystem_id')
            ->where(['paysystems_config.parent_id' => $this->ico->payments_list_id])
            ->andWhere(['paysystems.currency' => $this->currencyObject->code])
            ->scalar();
        if ($id === false) {
            return [false, 103, 'Для платежной системы $currency=' . $this->currency . ' нет платежной системы'];
        }
        $this->config = PaySystemConfig::findOne($id);
        $this->paySystem = $this->config->getPaySystem();
    }

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            ['tokens', 'required'],
            ['tokens', 'double'],

            ['currency', 'required'],
            ['currency', 'integer'],
        ];
    }

    public function validatePassword($attribute, $params)
    {
        if (!$this->hasErrors()) {
            /** @var \common\models\UserAvatar $user */
            $user = Yii::$app->user->identity;
            if (!$user->validatePassword($this->password)) {
                $this->addError($attribute, 'Пароль не верный');
            }
        }
    }

    /**
     * Конвертирует массив ошибок от формы чтобы их можно было выдать через JSON
     *
     * @param array $params
     *      [
     *      'amount' => ['Не верное число',...],
     *      ]
     *
     * @return array
     * [[
     *      'name'  => 'amount',
     *      'value' => ['Не верное число',...],
     * ]]
     */
    public function convert($params = null)
    {
        if (is_null($params)) {
            $params = $this->errors;
        }
        if (count($params) == 0) {
            return [];
        }
        $ret = [];
        foreach ($params as $name => $arr) {
            $ret[] = [
                'name'  => $name,
                'value' => $arr,
            ];
        }
        return $ret;
    }

    /**
     * Создает кошелек токена
     *
     * @return \common\models\investment\IcoRequest
     */
    public function createRequest()
    {
        $tokens = $this->tokens;
        $config = $this->config;
        $paySystem = $this->paySystem;
        $paySystemWallet = $config->getClass();
        $currency = $paySystem->getCurrencyObject();
        $sumAfter = $this->convertTokens($tokens, $currency);
        $sumBefore = $paySystemWallet->getPriceWithTax($sumAfter);

        $billing = BillingMain::add([
            'sum_before'  => $sumBefore,
            'sum_after'   => $sumAfter,
            'source_id'   => $config->paysystem_id,
            'currency_id' => $currency->id,
        ]);

        /** @var \common\models\UserAvatar $user */
        $user = \Yii::$app->user->identity;
        $fields = [
            'paysystem_config_id' => $config->id,
            'billing_id'          => $billing->id,
            'tokens'              => $tokens,
            'address'             => $this->address,
            'user_id'             => $user->id,
            'ico_id'              => $this->icoId,
        ];
        $request = IcoRequest::add($fields);

        return $request;
    }

    /**
     * Переводит кол-во токенов в валюту
     *
     * @param float $tokens
     * @param \common\models\avatar\Currency $currency
     *
     * @return float кол-во единиц
     */
    private function convertTokens($tokens, $currency)
    {
        $ico = $this->ico->getIco();
        $currencyKurs = \common\models\avatar\Currency::findOne($ico->kurs_currency_id);

        return \common\models\avatar\Currency::convert($ico->kurs * $tokens, $currencyKurs, $currency->code);
    }
}
