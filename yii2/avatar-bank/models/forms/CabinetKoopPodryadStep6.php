<?php

namespace avatar\models\forms;


use common\models\school\File;
use cs\services\Url;
use yii\db\ActiveRecord;
use yii\helpers\Json;

/**
 *
 * @property int            id
 * @property int            school_id
 * @property string         akt_pp_prod
 *
 */
class CabinetKoopPodryadStep6 extends \iAvatar777\services\FormAjax\ActiveRecord
{
    public static function tableName()
    {
        return 'koop_podryad';
    }

    public function rules()
    {
        return [
            ['akt_pp_prod', 'required'],
            ['akt_pp_prod', 'string'],
        ];
    }

    public function attributeWidgets()
    {
        return [
            'akt_pp_prod' => [
                'class'    => '\iAvatar777\widgets\FileUpload8\FileUpload',
                'update'   => \avatar\controllers\CabinetSchoolPagesConstructorController::getUpdate(),
                'settings' => \avatar\controllers\CabinetSchoolPagesConstructorController::getSettingsLibrary($this->school_id, 27),
                'events'   => [
                    'onDelete' => function ($item) {
                        $r = new \cs\services\Url($item->image);
                        $d = pathinfo($r->path);
                        $start = $d['dirname'] . '/' . $d['filename'];

                        File::deleteAll(['like', 'file', $start]);
                    },
                ],
            ],
        ];
    }
}