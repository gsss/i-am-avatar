<?php
namespace avatar\models\forms;

use common\models\school\File;
use cs\base\FormActiveRecord;
use Yii;
use yii\base\NotSupportedException;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\web\IdentityInterface;

/**
 * @property integer id
 * @property string  name
 * @property string  dns
 * @property string  image
 * @property string  about
 * @property string  description
 */
class School extends FormActiveRecord
{
    public $_school_id;
    public $_type_id;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'school';
    }

    public function formAttributes()
    {
        return [
            [
                'name',
                'Название',
                1,
                'string',
                ['max' => 100],
            ],
            [
                'dns',
                'Ссылка',
                0,
                'string',
                ['max' => 100],
            ],
            [
                'description',
                'Описание',
                0,
                'string',
                ['max' => 1000],
            ],
            [
                'about',
                'О сообществе',
                0,
                'string',
                ['max' => 64000],
            ],
            [
                'image',
                'Картинка',
                0,
                '\iAvatar777\widgets\FileUpload7\Validator',
                'widget' => [
                    '\iAvatar777\widgets\FileUpload7\FileUpload',
                    [
                        'update'    => \avatar\controllers\CabinetSchoolPagesConstructorController::getUpdate(),
                        'settings'  => \avatar\controllers\CabinetSchoolPagesConstructorController::getSettingsLibrary($this->_school_id, $this->_type_id),
                        'events'    => [
                            'onDelete' => function (BlogItem $item) {
                                $r = new \cs\services\Url($item->image);
                                $d = pathinfo($r->path);
                                $start = $d['dirname'] . '/' . $d['filename'];

                                File::deleteAll(['like', 'file', $start]);
                            },
                        ],
                    ],
                ],
            ],
            [
                'favicon',
                'Favicon',
                0,
                '\iAvatar777\widgets\FileUpload7\Validator',
                'widget' => [
                    '\iAvatar777\widgets\FileUpload7\FileUpload',
                    [
                        'update'    => \avatar\controllers\CabinetSchoolPagesConstructorController::getUpdate(),
                        'settings'  => \avatar\controllers\CabinetSchoolPagesConstructorController::getSettingsLibrary($this->_school_id, $this->_type_id),
                        'events'    => [
                            'onDelete' => function (BlogItem $item) {
                                $r = new \cs\services\Url($item->image);
                                $d = pathinfo($r->path);
                                $start = $d['dirname'] . '/' . $d['filename'];

                                File::deleteAll(['like', 'file', $start]);
                            },
                        ],
                    ],
                ],
            ],
        ];
    }
}
