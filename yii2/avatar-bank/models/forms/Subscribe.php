<?php
/**
 * Created by PhpStorm.
 * User: Бог-Творец
 * Date: 22.11.2016
 * Time: 20:20
 */

namespace avatar\models\forms;


use avatar\services\Html;
use common\widgets\FileUpload3\FileUpload;
use cs\base\FormActiveRecord;
use yii\base\Exception;
use yii\widgets\ActiveForm;

class Subscribe extends FormActiveRecord
{
    public static function tableName()
    {
        return 'user';
    }

    /**
     * Ищт профиль в БД
     * @param mixed $condition
     * @return \avatar\models\forms\Profile
     * @throws
     */
    public static function findOne($condition)
    {
        $i = parent::findOne($condition);
        if (is_null($i)) {
            throw new Exception('Не найден профиль');
        }

        return $i;
    }


    function formAttributes()
    {
        return [

            [
                'subscribe_is_news',
                'Новости',
                0,
                '\common\widgets\CheckBox2\Validator',
                [],
                'Новости нашего банка',
                'widget' => [
                    '\common\widgets\CheckBox2\CheckBox'
                ],
            ],

            [
                'subscribe_is_blog',
                'Блог',
                0,
                '\common\widgets\CheckBox2\Validator',
                [],
                'Статьи и обзоры рынка криптовалют и финансов',
                'widget' => [
                    '\common\widgets\CheckBox2\CheckBox'
                ],
            ],
        ];
    }
} 