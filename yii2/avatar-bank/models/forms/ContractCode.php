<?php
namespace avatar\models\forms;

use common\components\providers\ETH;
use cs\base\FormActiveRecord;
use Yii;
use yii\base\Model;
use yii\base\NotSupportedException;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\helpers\Json;
use yii\web\IdentityInterface;

/**
 */
class ContractCode extends Model
{
    public $code;

    public function rules()
    {
        return [
            [[
                'code',
            ], 'safe'],
            ['code', 'required'],
        ];
    }


    public function action()
    {
        $data = [
            'solidity' => $this->code,
            'params' => [],
        ];

        $provider = new \avatar\modules\ETH\ServiceBlockCypher();
        $requestContent = Json::encode($data);

        $url = $provider->apiUrl . '/' . 'eth/main/contracts' . '?' . 'token' . '=' . $provider->token;
        $curl = curl_init($url);
        curl_setopt($curl, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/45.0.2454.85 Safari/537.36');
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl, CURLOPT_HEADER, 0);
        curl_setopt($curl, CURLOPT_CONNECTTIMEOUT, 10);
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $requestContent);
        $body = curl_exec($curl);

        $result = new \StdClass();
        $result->headers = curl_getinfo($curl);
        $result->body = $body;
        curl_close($curl);

        $data = Json::decode($body);

        return $data;
    }
}
