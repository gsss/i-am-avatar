<?php
namespace avatar\models\forms;

use avatar\base\Application;
use cs\base\FormActiveRecord;
use Yii;
use yii\base\Model;
use yii\base\NotSupportedException;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\web\IdentityInterface;

/**
 */
class LandingSend extends Model
{
    public $name;
    public $email;
    public $text;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['name', 'required'],
            ['name', 'string'],
            ['name', 'trim'],

            ['email', 'required'],
            ['email', 'string'],
            ['email', 'trim'],
            ['email', 'email'],

            ['text', 'required'],
            ['text', 'string'],
        ];
    }

    public function action()
    {
        Application::mail(Yii::$app->params['landingEmail'], 'Письмо с лендинга', 'landing', [
            'name'  => $this->name,
            'email' => $this->email,
            'text'  => $this->text,
        ]);

        return true;
    }

    /**
     * Конвертирует массив ошибок от формы чтобы их можно было выдать через JSON
     *
     * @param array $params
     *      [
     *      'amount' => ['Не верное число',...],
     *      ]
     *
     * @return array
     * [
     *      'name'  => 'amount',
     *      'value' => ['Не верное число',...],
     * ]
     */
    public function convert($params)
    {
        $ret = [];
        foreach ($params as $name => $arr) {
            $ret[] = [
                'name'  => $name,
                'value' => $arr,
            ];
        }
        return $ret;
    }

}
