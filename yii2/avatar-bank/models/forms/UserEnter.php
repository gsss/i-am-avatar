<?php

namespace avatar\models\forms;

use common\models\school\File;
use iAvatar777\services\FormAjax\ActiveRecord;

class UserEnter extends ActiveRecord
{
    public function attributeWidgets()
    {
        return [
            'passport_scan1' => [
                '\iAvatar777\widgets\FileUpload7\FileUpload',
                [
                    'update'    => \avatar\controllers\CabinetSchoolPagesConstructorController::getUpdate(),
                    'settings'  => \avatar\controllers\CabinetSchoolPagesConstructorController::getSettingsLibrary($this->_school_id, 21),
                    'events'    => [
                        'onDelete' => function ($item) {
                            $r = new \cs\services\Url($item->image);
                            $d = pathinfo($r->path);
                            $start = $d['dirname'] . '/' . $d['filename'];

                            File::deleteAll(['like', 'file', $start]);
                        },
                    ],
                ],
            ],
            'passport_scan2' => [
                '\iAvatar777\widgets\FileUpload7\FileUpload',
                [
                    'update'    => \avatar\controllers\CabinetSchoolPagesConstructorController::getUpdate(),
                    'settings'  => \avatar\controllers\CabinetSchoolPagesConstructorController::getSettingsLibrary($this->_school_id, 21),
                    'events'    => [
                        'onDelete' => function ($item) {
                            $r = new \cs\services\Url($item->image);
                            $d = pathinfo($r->path);
                            $start = $d['dirname'] . '/' . $d['filename'];

                            File::deleteAll(['like', 'file', $start]);
                        },
                    ],
                ],
            ],
        ];
    }
}