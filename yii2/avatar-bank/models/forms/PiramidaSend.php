<?php

namespace avatar\models\forms;

use avatar\models\WalletToken;
use common\models\avatar\Currency;
use common\models\avatar\UserBill;
use common\models\avatar\UserBillAddress;
use common\models\avatar\UserBillOperation;
use common\models\Card;
use common\models\Token;
use common\models\UserAvatar;
use common\services\Security;
use cs\services\Str;
use cs\services\VarDumper;
use Yii;
use yii\base\Exception;
use yii\base\Model;
use cs\Widget\FileUpload3\FileUpload;
use yii\db\Query;
use yii\helpers\Html;
use yii\helpers\StringHelper;

/**
 * ContactForm is the model behind the contact form.
 */
class PiramidaSend extends Model
{
    /** @var string можно задать адрес, телефон, почта, номер карты с пробелами */
    public $address;

    /** @var \common\models\avatar\UserBill счет отправителя  */
    public $billingFrom;

    /** @var  string(3) код валюты, валюта в которой пользователь указывал переводимые средства */
    public $currency;

    /** @var  \common\models\avatar\Currency валюта в которой пользователь указывал переводимые средства устанавливается в normalizeCurrency */
    public $_currencyObject;

    /** @var string пароль откабинета */
    public $password;

    /** @var string кол-во монет для отправки */
    public $amount;

    /** @var string комментарий к транзакции */
    public $comment;

    /**
     * @var string стратегия расчета комиссии
     *             0 | 1
     */
    public $feeStrategy;

    /** @var int Идентификатор счета откуда отправляются деньги */
    public $billing_id;

    public function rules()
    {
        return [
            [['address', 'password', 'amount', 'billing_id'], 'required'],
            ['currency', 'normalizeCurrency'],
            ['billing_id', 'integer'],
            ['billing_id', 'validateBilling'],

            ['address', 'trim'],
            ['address', 'normalizeAddress'],

            ['amount', 'normalizeAmount'],
            ['feeStrategy', 'in', 'range' => [0, 1]],
            ['feeStrategy', 'default', 'value' => 1],

            [['address', 'password', 'comment'], 'string'],
            ['password', 'validatePassword'],
        ];
    }

    /**
     * Преобразовывает запятую в точку если есть
     * Если есть запрещенные символы то выводит сообщение об ошибке
     * Если валюта отлична от BTC то производит конвертацию
     *
     * @param $attribute
     * @param $params
     *
     * @throws
     */
    public function normalizeAmount($attribute, $params)
    {
        if (!$this->hasErrors()) {
            $this->amount = str_replace(',', '.', $this->amount);
            if (!$this->validateAmount($this->amount)) {
                $this->addError($attribute, 'Не верные символы');
                return;
            }
            $to = 'BTC';
            switch ($this->billingFrom->currency) {
                case Currency::ETH:
                    $to = 'ETH';
                    break;
                case Currency::ETC:
                    $to = 'ETC';
                    break;
                case Currency::BTC:
                    $to = 'BTC';
                    break;
                default:
                    $to = $this->currency;
                    // для токенов. Если указана валюта отличная от валюты токена то вызывается ошибка
                    if ($this->billingFrom->getCurrencyObject()->code != $this->currency) {
                        $this->addError($attribute, 'Должна быть установлена валюта токена');
                        return;
                    }
                    break;
            }
            if ($this->currency != $to) {
                $this->amount = Currency::convert($this->amount, $this->currency, $to);
                $this->currency = $to;
            }
        }
    }

    /**
     * Преобразовывает ВТС в \common\models\avatar\Currency
     *
     * @param $attribute
     * @param $params
     *
     * @throws
     */
    public function normalizeCurrency($attribute, $params)
    {
        if (!$this->hasErrors()) {
            $c = Currency::findOne(['code' => $this->currency]);
            if (is_null($c)) {
                $this->addError($attribute, 'Не найдена валюта');
            }
            $this->_currencyObject = $c;
        }
    }

    private function validateAmount($amount)
    {
        $symbols = Str::getChars($amount);
        foreach ($symbols as $char) {
            if (!in_array($char, ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '.'])) return false;
        }
        return true;
    }

    /**
     * Если адрес задан как номер карты, номер телефона, или логин то приводит его к адресу по умолчанию
     *
     * @param $attribute
     * @param $params
     */
    public function normalizeAddress($attribute, $params)
    {
        if (!$this->hasErrors()) {
            $address = $this->address;
            if (StringHelper::startsWith($address, 'bitcoin')) {
                $address = substr($address, 8);
                $arr = explode('?', $address);
                if (count($arr) > 1) {
                    $address = $arr[0];
                }
            } else if (StringHelper::startsWith($address, 'ethereum')) {
                $address = substr($address, 9);
                $arr = explode('?', $address);
                if (count($arr) > 1) {
                    $address = $arr[0];
                }
            } else if (filter_var($address, FILTER_VALIDATE_EMAIL)) {
                // email
                $email = $address;
                try {
                    $user = UserAvatar::findByUsername($email);
                } catch (\Exception $e) {
                    $this->addError($attribute, 'Не найдена почта');
                    return;
                }

                // счет == [BTC | ETH]
                if (in_array($this->billingFrom->currency, [Currency::BTC, Currency::ETH])) {
                    try {
                        $billing = $user->getPiramidaBilling($this->billingFrom->currency);
                    } catch (\Exception $e) {
                        $this->addError($attribute, $e->getMessage());
                        return;
                    }
                } else {
                    try {
                        // Ищу кошелек токена
                        $billing = UserBill::findOne(['user_id' => $user->id, 'currency' => $this->billingFrom->currency, 'mark_deleted' => 0]);
                    } catch (\Exception $e) {
                        // создаю кошелек токена

                        // Получаю кошелек эфира
                        $billing = UserBill::findOne(['user_id' => $user->id, 'currency' => Currency::ETH, 'mark_deleted' => 0]);
                        $tokenObject = Token::findOne(['currency_id' => $this->billingFrom->currency]);
                        $tokenBilling = WalletToken::createFromEth($billing, $tokenObject, $tokenObject->getCurrency()->title, $user->id);
                        $billing = $tokenBilling->billing;
                    }
                }

                $address = $billing->address;
            } else if ($this->isPhone($address)) {
                // phone
                $phone = $address;
                $phone = str_replace('+', '', $phone);
                $phone = str_replace('-', '', $phone);
                $phone = str_replace('(', '', $phone);
                $phone = str_replace(')', '', $phone);
                $phone = str_replace(' ', '', $phone);
                try {
                    $user = UserAvatar::findOne(['phone' => $phone]);
                } catch (\Exception $e) {
                    $this->addError($attribute, 'Не найден телефон');
                    return;
                }

                // счет == [BTC | ETH | ETC]
                if (in_array($this->billingFrom->currency, [Currency::BTC, Currency::ETH, Currency::ETC])) {
                    try {
                        $billing = $user->getPiramidaBilling($this->billingFrom->currency);
                    } catch (\Exception $e) {
                        $this->addError($attribute, $e->getMessage());
                        return;
                    }
                } else {
                    try {
                        // Ищу кошелек токена
                        $billing = UserBill::findOne(['user_id' => $user->id, 'currency' => $this->billingFrom->currency, 'mark_deleted' => 0]);
                    } catch (\Exception $e) {
                        // Ищу кошелек ETH
                        try {
                            $billing = UserBill::findOne(['user_id' => $user->id, 'currency' => Currency::ETH, 'mark_deleted' => 0]);
                        } catch (\Exception $e) {
                            $this->addError($attribute, 'У получателя нет кошелька ETH');
                            return;
                        }
                        // создаю кошелек токена

                        // Получаю кошелек эфира
                        $billing = UserBill::findOne(['user_id' => $user->id, 'currency' => Currency::ETH, 'mark_deleted' => 0]);
                        $tokenObject = Token::findOne(['currency_id' => $this->billingFrom->currency]);
                        $tokenBilling = WalletToken::createFromEth($billing, $tokenObject, $tokenObject->getCurrency()->title, $user->id);
                        $billing = $tokenBilling->billing;
                    }
                }
                $address = $billing->address;

            } else if ($this->isCardNumber($address)) {
                $address = str_replace(' ', '', $address);

                try {
                    $card = Card::findOne(['number' => $address]);
                    $billing = UserBill::findOne(['card_id' => $card->id, 'currency' => $this->billingFrom->currency]);
                } catch (\Exception $e) {
                    $currency = Currency::findOne($this->billingFrom->currency);
                    $this->addError($attribute, 'Не найден счет для валюты ' . $currency->code . ' на карте');
                    return;
                }
                $address = $billing->address;
            } else {
                // если это перевод токена
                if (!in_array($this->billingFrom->currency, [Currency::BTC, Currency::ETH])) {
                    try {
                        // ищу есть ли счет токена у клиента
                        $billingTo = UserBill::findOne(['address' => $address, 'currency' => $this->billingFrom->currency, 'mark_deleted' => 0]);
                    } catch (\Exception $e) {
                        // Пробую найти кошелек эфира у получателя

                        try {
                            $billing = UserBill::findOne(['address' => $address, 'currency' => Currency::ETH, 'mark_deleted' => 0]);
                            $tokenObject = Token::findOne(['currency_id' => $this->billingFrom->currency]);
                            $tokenBilling = WalletToken::createFromEth($billing, $tokenObject, $tokenObject->getCurrency()->title, $billing->user_id);

                        } catch (\Exception $e) {
                            // создаю кошелек токена
                             $this->addError($attribute, 'Произошла ошибка во время создания и поиска кошелька');
                             return;
                        }
                    }
                }
            }

            $this->address = $address;
        }
    }

    private function isPhone($address)
    {
        return StringHelper::startsWith($address, '+');
    }

    private function isCardNumber($address)
    {
        $address = str_replace(' ', '', $address);
        if (strlen($address) == 16) {
            return preg_match('/\D/', $address) == 0;
        } else {
            return false;
        }
    }

    /**
     * Проверяет пароль для счета
     * Сравнивает с паролем кабинета
     *
     * @param $attribute
     * @param $params
     */
    public function validatePassword($attribute, $params)
    {
        if (!$this->hasErrors()) {
            if (!$this->billingFrom->validatePassword($this->password)) {
                $this->addError('password', 'Не верный пароль');
                return;
            }
        }
    }

    /**
     * Проверяет что счет существует и принадлежит пользователю
     * и присоединяет объект billing
     *
     * @param $attribute
     * @param $params
     */
    public function validateBilling($attribute, $params)
    {
        if (!$this->hasErrors()) {
            /** @var \common\models\UserAvatar $user */
            try {
                $billing = UserBill::findOne($this->billing_id);
            } catch (\Exception $e) {
                $this->addError('address', 'Не найден счет');
                return;
            }
            if ($billing->user_id != Yii::$app->user->id) {
                $this->addError('address', 'Это не ваш счет. Не красиво так делать');
                return;
            }
            if ($billing->address == $this->address) {
                $this->addError('address', 'Нельзя отправить деньги на счет отправителя');
                return;
            }
            $this->billingFrom = $billing;
        }
    }

    /**
     * Отправляет деньги на счет
     *
     * @return bool|string
     * @throws Exception
     */
    public function send()
    {
        switch ($this->currency) {
            case 'BTC':
                $wallet = $this->billingFrom->getWalletBTC($this->password);
                try {
                    if ($this->feeStrategy) {
                        $fee = \Blocktrail\SDK\WalletInterface::FEE_STRATEGY_OPTIMAL;
                    } else {
                        $fee = \Blocktrail\SDK\WalletInterface::FEE_STRATEGY_BASE_FEE;
                    }
                    $transaction = $wallet->pay([$this->address => [
                        'amount'  => $this->amount,
                        'comment' => $this->comment,
                    ]], $fee);
                } catch (\Exception $e) {
                    Yii::warning($e->getMessage(), 'avatar\models\forms\PiramidaSend::send');
                    $this->setError($e->getMessage());
                    return false;
                }
                return $transaction;
                break;
            case 'ETH':
                $wallet = $this->billingFrom->getWalletETH();
                try {
                    $transaction = $wallet->pay([$this->address => [
                        'amount'  => $this->amount,
                        'comment' => $this->comment,
                    ]], $this->password);
                } catch (\Exception $e) {
                    if ($e->getMessage() == 'insufficient funds for gas * price + value') {
                        $this->addError('amount', 'Не хватило денег для оплаты комиссии');
                    } else {
                        $this->addError('amount', $e->getMessage());
                    }
                    return false;
                }
                return $transaction;
                break;
            case 'ETС':
                $wallet = $this->billingFrom->getWalletETC();
                try {
                    $transaction = $wallet->pay([$this->address => [
                        'amount'  => $this->amount,
                        'comment' => $this->comment,
                    ]], $this->password);
                } catch (\Exception $e) {
                    if ($e->getMessage() == 'insufficient funds for gas * price + value') {
                        $this->addError('amount', 'Не хватило денег для оплаты комиссии');
                    } else {
                        $this->addError('amount', $e->getMessage());
                    }
                    return false;
                }
                return $transaction;
                break;
            default:
                // token
                $wallet = $this->billingFrom->getWalletToken();
                try {
                    $transaction = $wallet->pay([$this->address => [
                        'amount'  => $this->amount,
                        'comment' => $this->comment,
                    ]], $this->password);
                } catch (\Exception $e) {
                    if (StringHelper::startsWith($e->getMessage(), 'Error: insufficient funds for gas * price + value')) {
                        $this->addError('amount', 'Не хватило денег для оплаты комиссии');
                    } else if (StringHelper::startsWith($e->getMessage(),'Error: could not decrypt key with given passphrase')) {
                        $this->addError('amount', 'Не верный пароль от кошелька на сервере');
                    } else {
                        $this->addError('amount', $e->getMessage());
                    }
                    return false;
                }
                return $transaction;
                break;
        }

    }

    /**
     * Подготовка для отправки, ищет счет пользователя, user billing не обязательно
     *
     * @return array
     * [
     *  'address' => '<string>',
     *  'user' => [
     *      'id' =>
     *      'name2' =>
     *      'avatar' =>
     *  ],
     *  'billing' => [
     *      'id' =>
     *      'currency' => 'BTC'
     *      'image' => '/images/btc.png' (квадратная)
     *  ],
     * ]
     *
     * @throws Exception
     */
    public function prepare()
    {
        try {
            $address = UserBillAddress::findOne(['address' => $this->address]);
            $billing = $address->getBilling();
        } catch (\Exception $e) {
            try {
                $billing = UserBill::findOne(['address' => $this->address, 'currency' => $this->billingFrom->currency]);
            } catch (\Exception $e) {
                return [
                    'address'  => $this->address,
                    'amount'   => $this->amount,
                    'currency' => $this->currency,
                    'hasUser'  => false,
                ];
            }
        }
        $user = $billing->getUser();
        $data = $this->calculateEstimateCost();


        return [
            'address'  => $this->address,
            'amount'   => $this->amount,
            'currency' => $this->currency,
            'hasUser'  => true,
            'calculate' => $data,
            'user'     => [
                'id'     => $user->id,
                'name2'  => $user->getName2(),
                'avatar' => $user->getAvatar(),
            ],
            'billing'  => [
                'id'       => $billing->id,
                'address'  => $billing->address,
                'name'     => $billing->name,
                'currency' => $billing->currency == Currency::BTC ? 'BTC' : 'ETH',
                'image'    => '/images/controller/cabinet-bills/index/bitcoin2.jpg',
            ],
        ];
    }

    private function calculateEstimateCost()
    {
        if (!in_array($this->currency, ['BTC', 'ETH'])) {
            // token
            $wallet = $this->billingFrom->getWalletToken();
            $transaction = $wallet->calculate([$this->address => [
                'amount'  => $this->amount,
                'comment' => $this->comment,
            ]], $this->password);

            return $transaction;
        }

    }


    /**
     * Устанавливает ошибку
     *
     * {"code":0,"msg":"No usable unspent outputs [2]"} // не хватает денег на счету
     *
     * @param $message
     */
    public function setError($message)
    {
        if (StringHelper::startsWith($message, 'Values should be more than dust')) {
            $arr = explode('(', $message);
            $v = 0;
            if (count($arr) > 1) {
                $v = substr($arr[1], 0, strlen($arr[1]) - 1);
            }
            $this->addError('amount', 'Значение должно быть больше чем ' . \Yii::$app->formatter->asDecimal($v / 100000000, 8));
        } else if (StringHelper::startsWith($message, 'A Server Error has occurred')) {
            if (strpos($message, 'No usable unspent outputs') !== false) {
                $this->addError('amount', 'Недостаточно денег в кошельке или они еще не подтверждены');
            }
        } else {
            if ($message == 'Wallet balance too low') {
                $this->addError('amount', 'Слишком мало денег на счету');
            } else {
                if (StringHelper::startsWith($message, 'Invalid address')) {
                    $this->addError('address', 'Не верный адрес');
                } else {
                    if (StringHelper::startsWith($message, 'Wallet balance too low to pay the fee')) {
                        $arr = explode('[', $message);
                        $v = 0;
                        if (count($arr) > 1) {
                            $v = substr($arr[1], 0, strlen($arr[1]) - 1);
                        }
                        $this->addError('amount', 'Баланс слишком мал чтобы заплатить комиссию ' . Yii::$app->formatter->asDecimal($v / 100000000, 8));
                    } else {
                        $this->addError('amount', $message);
                    }
                }
            }
        }
    }
}
