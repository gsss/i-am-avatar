<?php
namespace avatar\models\forms;

use common\models\school\File;
use cs\base\FormActiveRecord;
use Yii;
use yii\base\NotSupportedException;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\web\IdentityInterface;

/**
 * PaySystem model
 *
 * @property integer $id
 * @property string  $code
 * @property string  $title
 * @property string  $class_name
 * @property string  $image
 */
class PaySystem extends FormActiveRecord
{
    public $_school_id;
    public $_type_id;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'paysystems';
    }

    public function formAttributes()
    {
        return [
            [
                'code',
                'Код',
                0,
                'string',
                ['max' => 20],
                'Малые английские буквы или тире'
            ],
            [
                'title',
                'Название',
                1,
                'string',
                ['max' => 60],
            ],
            [
                'currency',
                'Валюта',
                1,
                'string',
                ['max' => 8],
                'Код валюты, большие буквы'
            ],
            [
                'class_name',
                'Название класса',
                0,
                'string',
                ['max' => 20],
                'app\models\Piramida\WalletSource'
            ],
            [
                'image',
                'Картинка',
                0,
                '\common\widgets\FileUpload7\Validator',
                'widget' => [
                    '\common\widgets\FileUpload7\FileUpload',
                    [
                        'update'    => \avatar\controllers\CabinetSchoolPagesConstructorController::getUpdate(),
                        'settings'  => \avatar\controllers\CabinetSchoolPagesConstructorController::getSettings($this->_school_id, $this->_type_id),
                        'events'    => [
                            'onDelete' => function (\avatar\models\forms\PaySystem $item) {
                                $r = new \cs\services\Url($item->image);
                                $d = pathinfo($r->path);
                                $start = $d['dirname'] . '/' . $d['filename'];

                                File::deleteAll(['like', 'file', $start]);
                            },
                        ],
                    ],
                ],
            ],
        ];
    }
}
