<?php
/**
 * Created by PhpStorm.
 * User: s.arhangelskiy
 * Date: 31.07.2017
 * Time: 10:50
 */

namespace avatar\models\validate;


use common\models\avatar\Currency;
use common\models\avatar\UserBill;
use common\models\BlogItem;
use common\models\school\SubscribeItem;
use common\models\Token;
use common\models\UserRoot;
use common\models\VebinarChatItem;
use cs\services\VarDumper;
use yii\helpers\Json;

class VebinarSend extends \yii\base\Model
{
    public $vebinar_id;
    public $text;
    public $session_id;

    public function rules()
    {
        return [
            ['vebinar_id', 'required'],
            ['vebinar_id', 'integer'],

            ['text', 'required'],
            ['text', 'string'],

            ['session_id', 'required'],
            ['session_id', 'string'],
        ];
    }

    public function action()
    {
        $n = new VebinarChatItem([
            'session_id' => $this->session_id,
            'text'       => Json::encode(['text' => $this->text]),
            'vebinar_id' => $this->vebinar_id,
        ]);
        $n->save();
        $f = $n->attributes;
        $f['timeFormatted'] = \Yii::$app->formatter->asDatetime($n->created_at, 'php:H:i:s');

        return ['message' => $f];
    }
}