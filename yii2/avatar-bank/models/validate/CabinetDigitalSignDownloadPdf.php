<?php

namespace avatar\models\validate;

use common\components\providers\ETH;
use common\models\avatar\Currency;
use common\models\avatar\UserBill;
use common\models\avatar\UserBillOperation;
use common\models\PassportLink;
use common\models\PaySystem;
use common\models\RequestTokenCreate;
use common\models\school\AdminLink;
use common\models\UserAvatar;
use common\models\UserDigitalSign;
use cs\Application;
use cs\services\File;
use cs\services\Str;
use cs\services\UploadFolderDispatcher;
use cs\services\Url;
use cs\services\VarDumper;
use Imagine\Image\Box;
use Yii;
use yii\base\Model;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;
use yii\imagine\Image;
use BitcoinPHP\BitcoinECDSA\BitcoinECDSA;

/**
 *
 */
class CabinetDigitalSignDownloadPdf extends Model
{
    public $random;

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            ['random', 'required'],
            ['random', 'string'],
        ];
    }

    /**
     * Если удачно будет удаление на сервере то удаляет локально в таблице school_file
     *
     * Иначе вызываю исключение
     */
    public function action()
    {
        $nonce = '1234567890123456';

        /** @var \common\models\UserDigitalSign $row */
        $row = UserDigitalSign::findOne(['user_id' => \Yii::$app->user->id]);
        if (is_null($row)) {
            $bitcoinECDSA = new BitcoinECDSA();
            $bitcoinECDSA->generateRandomPrivateKey($this->random); //generate new random private key
            $address = $bitcoinECDSA->getAddress();
            $PrivateKey = $bitcoinECDSA->getPrivateKey();
            $getUncompressedAddress = $bitcoinECDSA->getUncompressedAddress();
            $getUncompressedPubKey = $bitcoinECDSA->getUncompressedPubKey();
            $getWif = $bitcoinECDSA->getWif();

            $unlockKey = \Yii::$app->params['unlockKey'];

            $key = UserBill::passwordToKey32($unlockKey);
            $ciphertext = openssl_encrypt($PrivateKey, 'AES-256-CTR', $key, true, $nonce); // OpenSSL
            $base64PrivateKey = base64_encode($ciphertext);

            $row = UserDigitalSign::add([
                'user_id'                 => \Yii::$app->user->id,
                'type_id'                 => 1,
                'address'                 => $address,
                'private_key'             => $base64PrivateKey,
                'address_uncompressed'    => $getUncompressedAddress,
                'public_key_uncompressed' => $getUncompressedPubKey,
            ]);

        } else {
            $unlockKey = \Yii::$app->params['unlockKey'];

            $key = UserBill::passwordToKey32($unlockKey);
            $ciphertext2 = base64_decode($row->private_key);
            $PrivateKey = openssl_decrypt($ciphertext2, 'AES-256-CTR', $key, true, $nonce); // OpenSSL

            $address = $row->address;
            $getUncompressedAddress = $row->address_uncompressed;
            $getUncompressedPubKey = $row->public_key_uncompressed;
            $getWif = '';
        }

        return [
            'address'               => $address,
            'PrivateKey'            => $PrivateKey,
            'UncompressedAddress'   => $getUncompressedAddress,
            'UncompressedPubKey'    => $getUncompressedPubKey,
            'Wif'                   => $getWif,
        ];
    }
}
