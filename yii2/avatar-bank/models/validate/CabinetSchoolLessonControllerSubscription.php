<?php

namespace avatar\models\validate;

use common\components\providers\ETH;
use common\models\avatar\Currency;
use common\models\avatar\UserBill;
use common\models\avatar\UserBillOperation;
use common\models\PassportLink;
use common\models\RequestTokenCreate;
use common\models\school\AdminLink;
use common\models\school\Kurs;
use common\models\school\Lesson;
use common\models\school\Potok;
use common\models\school\PotokUser3Link;
use common\models\school\SubscribeItem;
use common\models\UserAvatar;
use common\models\UserRoot;
use common\models\UserRootExt;
use cs\services\File;
use cs\services\Str;
use cs\services\UploadFolderDispatcher;
use cs\services\VarDumper;
use Imagine\Image\Box;
use Yii;
use yii\base\Model;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;
use yii\imagine\Image;

/**
 */
class CabinetSchoolLessonControllerSubscription extends Model
{
    /** @var  int school_kurs_lesson.id */
    public $id;

    /** @var  \common\models\school\Lesson */
    public $lesson;

    /** @var  \common\models\school\Kurs */
    public $kurs;

    /** @var  \common\models\school\School */
    public $school;

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            ['id', 'required'],
            ['id', 'integer'],
            ['id', 'validateLid'],
        ];
    }

    public function validateLid($attr, $params)
    {
        if (!$this->hasErrors()) {
            $lesson = Lesson::findOne($this->id);
            if (is_null($lesson)) {
                $this->addError($attr, 'Не найден урок');
                return;
            }
            $this->lesson = $lesson;

            $Kurs = Kurs::findOne($lesson->kurs_id);
            $this->kurs = $Kurs;
            $school = $Kurs->getSchool();
            $this->school = $school;

            $result = AdminLink::find()->where([
                'school_id' => $school->id,
                'user_id'   => Yii::$app->user->id,
            ])->exists();

            if (!$result) {
                $this->addError($attr, 'Вам запрещен доступ к чужой школе');
                return;
            }
        }
    }

    /**
     */
    public function action()
    {
        $potok_list = Potok::find()->where(['kurs_id' => $this->kurs->id])->all();
        /** @var Potok $Potok */
        foreach ($potok_list as $Potok) {

            $Subscribe = \common\models\school\Subscribe::add([
                'potok_id' => $Potok->id,
                'subject'  => 'Добавлен новый урок',
                'content'  => \Yii::$app->view->renderFile('@avatar/mail/html/subscribe/new-lesson.php', [
                    'lesson' => $this->lesson,
                ]),
            ]);
            $model = new \avatar\models\validate\CabinetSchoolSubscribeControllerSubscribe(['id' => $Subscribe->id]);
            $model->validate();

            $model->action();
        }

        return;
    }
}
