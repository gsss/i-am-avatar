<?php

namespace avatar\models\validate;

use common\components\Card;
use common\models\avatar\QrCode;
use common\models\avatar\UserBill;
use common\models\avatar\UserBillMerchant;
use common\models\investment\IcoRequest;
use common\models\investment\IcoRequestAddress;
use common\models\investment\IcoRequestSuccess;
use common\models\SendLetter;
use common\models\UserAvatar;
use cs\Application;
use cs\services\Str;
use Yii;
use yii\base\Model;
use yii\helpers\ArrayHelper;
use yii\helpers\VarDumper;

/**
 */
class RequestSend extends Model
{
    /** @var int ico_request.id */
    public $id;

    /** @var  string */
    public $password;

    /** @var  string адрес куда отправлять токены */
    public $address;

    /** @var  \common\models\investment\IcoRequest */
    private $request;

    /** @var  \common\models\investment\ProjectIco */
    private $ico;

    /** @var  \common\models\avatar\UserBill */
    private $billing;


    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            [['id', 'password'], 'required'],
            [['id'], 'integer'],
            [['id'], 'validateRequest'],
            [['id'], 'validateAddress'],
            [['password'], 'validatePassword'],
        ];
    }

    /**
     * Проверяет Заказ
     *
     * @param $attribute
     * @param $params
     */
    public function validateRequest($attribute, $params)
    {
        if (!$this->hasErrors()) {
            $request = IcoRequest::findOne($this->id);
            if (is_null($request)) {
                $this->addError('id', 'Не найден заказ');
                return;
            }
            $this->request = $request;
        }
    }

    /**
     * Проверяет наличие адреса для отправки токенов
     *
     * @param $attribute
     * @param $params
     */
    public function validateAddress($attribute, $params)
    {
        if (!$this->hasErrors()) {
            $request = $this->request;
            if ($request->address) {
                $address = $request->address;
            } else {
                // на самом деле здесь должна быть ошибка, потому что для заказа не установлен адрес и не записан IcoRequestAddress
                $this->addError('id', 'Нет адреса для отправки токенов');
                return;
            }
            $this->address = $address;
        }
    }

    /**
     * Проверяет Заказ
     *
     * @param $attribute
     * @param $params
     */
    public function validatePassword($attribute, $params)
    {
        if (!$this->hasErrors()) {
            $request = $this->request;
            $this->ico = $request->getIco();
            $this->billing = $this->ico->getBilling();
            if (!$this->billing->validatePassword($this->password)) {
                $this->addError('password', 'Не верный пароль');
            }
        }
    }


    /**
     * Выыдает массив ошибок от формы чтобы их можно было выдать через JSON
     *
     * @return array
     * [
     *      'name'  => 'amount',
     *      'value' => ['Не верное число',...],
     * ]
     */
    public function convertErrors()
    {
        $ret = [];
        foreach ($this->errors as $name => $arr) {
            $ret[] = [
                'name'  => $name,
                'value' => $arr,
            ];
        }
        return $ret;
    }

    /**
     * Отправляет токены
     *
     * Ставит флаг что заказ оплачен
     * Ставит статус IcoRequest::STATUS_SHOP_SEND
     * Отправляет письмо
     *
     * @return string идентификатор транзакции
     *
     * @throws
     */
    public function action()
    {
        $request = $this->request;
        $ico = $this->ico;
        $billing = $this->billing;
        $walletToken = $billing->getWalletToken();
        $token = $walletToken->token;
        $user = $request->getUser();
        $address = $this->address;

        // добаляю статус "Магазин подтвердил оплату"
        $request->addStatusToClient(IcoRequest::STATUS_SHOP_CONFIRM);

        // отправляю токены
        $txid = $walletToken->pay([$address => [
            'amount'  => $request->tokens,
            'comment' => [
                'to'   => $this->replace('Отправлены токены: {tokens} {token} по заказу #{rid} по ICO: {name}', [
                    '{tokens}' => Yii::$app->formatter->asDecimal($request->tokens, 2),
                    '{token}'  => $token->getCurrency()->code,
                    '{rid}'    => $request->id,
                    '{name}'   => $ico->getProject()->name,
                ]),
                'from' => $this->replace('Отправлены токены: {tokens} {token} по заказу #{rid} для: {email} [{uid}] на адрес: {address}', [
                    '{tokens}'  => Yii::$app->formatter->asDecimal($request->tokens, 2),
                    '{token}'   => $token->getCurrency()->code,
                    '{rid}'     => $request->id,
                    '{email}'   => $user->email,
                    '{uid}'     => $user->id,
                    '{address}' => $address,
                ]),
            ]
        ]], $this->password);

        // добавляю запись об отправленных токенах (транзакцию)
        $txObject = IcoRequestSuccess::add([
            'txid' => $txid,
            'id'   => $request->id,
        ]);

        // добаляю статус "Магазин выслал токены" и указываю транзакцию
        $request->addStatusToClient([
            'status'    => IcoRequest::STATUS_SHOP_SEND,
            'message'   => 'Токены высланы, txid: ' . $txid
        ]);

        // помечаю заказ как исполненный
        $request->success();

        // вычитаю кол-во отправленых токенов чтобы кол-во оставшихся токенов было актуальным
        $ico->remained_tokens -= $request->tokens;
        $ico->save();

        // отправляю письмо
        Application::mail($user->email, 'Вам отправлены токены по заказу #' . $request->id, 'ico_request_send_token', [
            'user'    => $user,
            'txid'    => $txid,
            'request' => $request,
        ]);

        return $txid;
    }

    private function replace($string, $params)
    {
        foreach ($params as $k => $v) {
            $string = str_replace($k,$v,$string);
        }

        return $string;
    }
}

