<?php

namespace avatar\models\validate;

use BitcoinPHP\BitcoinECDSA\BitcoinECDSA;
use common\components\Card;
use common\models\avatar\QrCode;
use common\models\avatar\UserBill;
use common\models\avatar\UserBillMerchant;
use common\models\SendLetter;
use common\models\UserDigitalSignList;
use common\models\UserDigitalSignPersonalData;
use cs\Application;
use cs\services\Str;
use Yii;
use yii\base\Model;
use yii\db\Query;
use yii\helpers\ArrayHelper;
use yii\helpers\VarDumper;

/**
 */
class CabinetDigitalSignVer2 extends \iAvatar777\services\FormAjax\Model
{
    /** @var  string адрес */
    public $address;

    /** @var  string подпись */
    public $message;

    /** @var  string подпись */
    public $sign;


    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            ['address', 'required'],
            ['address', 'trim'],
            ['address', 'string'],

            ['message', 'required'],
            ['message', 'trim'],
            ['message', 'string'],

            ['sign', 'required'],
            ['sign', 'trim'],
            ['sign', 'string'],
            ['sign', 'validateCardNumber'],
        ];
    }

    /**
     * @param $attribute
     * @param $params
     */
    public function validateCardNumber($attribute, $params)
    {
        if (!$this->hasErrors()) {
            $bitcoinECDSA = new BitcoinECDSA();

            if (!$bitcoinECDSA->checkSignatureForMessage($this->address, $this->sign, $this->message)) {
                $this->addError($attribute, 'Не верен адрес или подпись');
                return;
            }
        }
    }

    public function save($runValidation = true, $attributeNames = null)
    {
        \common\models\UserDigitalSign::add([
            'user_id' => Yii::$app->user->id,
            'type_id' => \common\models\UserDigitalSign::TYPE_VER2,
            'address' => $this->address,
        ]);
        UserDigitalSignList::add([
            'user_id' => Yii::$app->user->id,
            'address' => $this->address,
        ]);
        UserDigitalSignPersonalData::add([
            'user_id'    => Yii::$app->user->id,
            'address'    => $this->address,
            'sign'       => $this->sign,
            'name_first' => Yii::$app->user->identity->name_first,
            'name_last'  => Yii::$app->user->identity->name_last,
            'avatar'     => Yii::$app->user->identity->getAvatar(),
            'hash'       => $this->message,
        ]);

        return 1;
    }

}
