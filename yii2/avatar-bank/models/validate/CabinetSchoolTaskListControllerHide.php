<?php

namespace avatar\models\validate;

use avatar\models\forms\school\ProjectManagerLink;
use common\components\providers\ETH;
use common\models\avatar\Currency;
use common\models\avatar\UserBill;
use common\models\avatar\UserBillOperation;
use common\models\PassportLink;
use common\models\PaySystem;
use common\models\RequestTokenCreate;
use common\models\school\AdminLink;
use common\models\task\Task;
use common\models\UserAvatar;
use cs\services\File;
use cs\services\Str;
use cs\services\UploadFolderDispatcher;
use cs\services\Url;
use cs\services\VarDumper;
use Imagine\Image\Box;
use Yii;
use yii\base\Model;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;
use yii\imagine\Image;

/**
 *
 */
class CabinetSchoolTaskListControllerHide extends Model
{
    /** @var int */
    public $id;

    /** @var  Task */
    public $task;

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            ['id', 'required'],
            ['id', 'string'],
            ['id', 'validateTask'],
        ];
    }

    /**
     * Проверяет на наличие задачи
     * Проверяет на доступ к школе
     */
    public function validateTask($attribute, $params)
    {
        if (!$this->hasErrors()) {
            $task = Task::findOne($this->id);
            if (is_null($task)) {
                $this->addError($attribute, 'Не такой задачи');
                return;
            }
            $isAdmin = AdminLink::find()->where([
                'school_id' => $task->school_id,
                'user_id'   => Yii::$app->user->id,
            ])->exists();
            $isProjectManager = ProjectManagerLink::find()->where([
                'school_id' => $task->school_id,
                'user_id'   => Yii::$app->user->id,
            ])->exists();

            if (!($isAdmin || $isProjectManager)) {
                $this->addError($attribute, 'Нет доступа к школе');
                return;
            }

            $this->task = $task;
        }
    }

    /**
     */
    public function action()
    {
        $this->task->is_hide = 1;
        $this->task->save();
    }
}
