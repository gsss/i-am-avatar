<?php

namespace avatar\models\validate;

use common\components\providers\ETH;
use common\models\avatar\Currency;
use common\models\avatar\UserBill;
use common\models\avatar\UserBillOperation;
use common\models\PassportLink;
use common\models\RequestTokenCreate;
use cs\services\File;
use cs\services\Str;
use cs\services\UploadFolderDispatcher;
use cs\services\VarDumper;
use Imagine\Image\Box;
use Yii;
use yii\base\Model;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;
use yii\imagine\Image;

/**
 */
class CabinetTokenControllerNew extends Model
{
    public $name;
    public $total;
    public $decimals;
    public $code;
    public $image;

    /** @var  int нужна ли функция довыпуска 0 - нет, 1 - да */
    public $is_add = 0;

    /** @var  int нужна ли функция сжигания токенов 0 - нет, 1 - да */
    public $is_burn = 0;

    /** @var  int нужна ли функция смены владельца контрата 0 - нет, 1 - да */
    public $is_change_owner = 0;

    public $billing_id;

    /** @var  \common\models\avatar\UserBill */
    public $billing;

    public $password;

    public $model;
    public $attribute = 'image';

    public $optionsImage = [
        'image',
        'image',
        'image',
        'widget' => [
            '\common\widgets\FileUpload31\FileUpload',
            [
                'tableName' => 'requests_token_create',
                'options'   => [
                    'small' => [
                        300,
                        300,
                        \common\widgets\FileUpload3\FileUpload::MODE_THUMBNAIL_CUT
                    ],
                ]
            ]
        ],
    ];

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            ['name', 'required'],
            ['name', 'string'],
            ['name', 'trim'],
            ['name', 'validateName'],

            ['total', 'required'],
            ['total', 'integer'],

            ['decimals', 'required'],
            ['decimals', 'integer', 'max' => 24],

            ['code', 'required'],
            ['code', 'trim'],
            ['code', 'string'],

            [['is_add', 'is_burn', 'is_change_owner'], 'integer'],

            ['billing_id', 'integer'],
            ['billing_id', 'validateBilling', 'skipOnEmpty' => false],

            ['image', 'validateImage', 'skipOnEmpty' => false],
        ];
    }

    public function attributeLabels()
    {
        return [
            'name'     => 'Название',
            'total'    => 'Кол-во монет',
            'decimals' => 'Кол-во десятичных знаков',
            'code'     => 'Код валюты',
        ];
    }

    /**
     *
     */
    public function validateBilling($attribute, $params)
    {
        if (!$this->hasErrors()) {
            if (is_null($this->billing_id)) {
                $link = PassportLink::findOne(['user_id' => Yii::$app->user->id]);
                $this->billing_id = $link->billing_id;
            }
            try {
                $billing = UserBill::findOne($this->billing_id);
            } catch (\Exception $e) {
                $this->addError($attribute, 'Не найден счет');
                return;
            }
            $this->billing = $billing;
        }
    }

    /**
     *
     */
    public function validateImage($attribute, $params)
    {
        if (!$this->hasErrors()) {
            $data = Yii::$app->request->post();
            $v = ArrayHelper::getValue($data, $this->formName() . '.image-files', '[]');

            if ($v == '[]') {
                $this->addError($attribute, 'Картинка обязательна');
                return;
            }
        }
    }

    /**
     *
     */
    public function validateName($attribute, $params)
    {
        if (!$this->hasErrors()) {
            $chars = Str::getChars($this->name);
            if (in_array(' ', $chars)) {
                $this->addError($attribute, 'Не допустимо использование пробела');
                return;
            }
            $result = true;
            foreach ($chars as $char) {
                if (!in_array(strtolower($char), [
                    'a',
                    'b',
                    'c',
                    'd',
                    'e',
                    'f',
                    'g',
                    'h',
                    'i',
                    'j',
                    'k',
                    'l',
                    'm',
                    'n',
                    'o',
                    'p',
                    'q',
                    'r',
                    's',
                    't',
                    'u',
                    'v',
                    'w',
                    'x',
                    'y',
                    'z',
                    '1',
                    '2',
                    '3',
                    '4',
                    '5',
                    '6',
                    '7',
                    '8',
                    '9',
                    '0',
                ])) {
                    $result = false;
                    break;
                }
            }
            if ($result == false) {
                $this->addError($attribute, 'Допустимо использовать только символы латиницы и цифры');
                return;
            }
        }
    }

    /**
     * Производит регистрацию контракта
     *
     * @return array
     * [
     *      'contract' - код контракта
     *      'id'       - идентификатор заявки
     * ]
     */
    public function action()
    {
        // работаю с большими целыми числами
        $pow = bcpow(10, $this->decimals);
        $all = bcmul($this->total, $pow);

        $contractCode = $this->getContractCode([
            '{totalSupply}'  => $all,
            '{tokenName}'    => $this->name,
            '{decimals}'     => $this->decimals,
            '{tokenSymbol}'  => $this->code,
        ]);

        Yii::$app->cache->set([
            Yii::$app->session->id,
            'createToken'
        ], $this->password);

        $response['init'] = '';
        $response['contract'] = $contractCode;
        $fields = [
            'init'          => '',
            'contract'      => $contractCode,
            'owner'         => $this->billing->address,
            'name'          => $this->name,
            'code'          => $this->code,
            'decimals'      => $this->decimals,
            'coin_emission' => $this->total,
            'user_id'       => \Yii::$app->user->id,
        ];
        Yii::info(\yii\helpers\VarDumper::dumpAsString($fields), 'avatar\models\validate\CabinetTokenControllerNew::action');
        $request = RequestTokenCreate::add($fields);
        $response['id'] = $request->id;

        $file = Json::decode(ArrayHelper::getValue(Yii::$app->request->post(), 'CabinetTokenControllerNew.image-files'))[0][2];
        $fileObject = File::path(Yii::getAlias('@webroot' . $file));
        $info = pathinfo($file);
        Yii::info(\yii\helpers\VarDumper::dumpAsString($info), 'avatar\models\validate\CabinetTokenControllerNew::$info');
        $this->model = $request;
        $request->image = $this->save($fileObject, 'png', $this->optionsImage);
        Yii::info(\yii\helpers\VarDumper::dumpAsString($request), 'avatar\models\validate\CabinetTokenControllerNew::$request');

//        $request->step = 2;
        $request->step = 3;
        $request->is_paid_avatars = 1;
        $request->save();

        Yii::info(\yii\helpers\VarDumper::dumpAsString([$this]), 'avatar\models\validate\CabinetTokenControllerNew::action');

        return $response;
    }

    /**
     * Формирует код контракта
     *
     * @param array $replace замена
     * [
     *      'поиск' => 'заменить',
     *      'поиск' => 'заменить',
     * ]
     *
     * @return string
     */
    private function getContractCode($replace = [])
    {
        $file = \Yii::getAlias('@avatar/contracts/create-token-v1/main.txt');
        $contractCode = file_get_contents($file);
        $contractCode = str_replace('{ContractName}', $this->name, $contractCode);
        if ($this->is_add) {
            $file1 = \Yii::getAlias('@avatar/contracts/create-token-v1/main.add.txt');
            $contractCode = str_replace('{add}', file_get_contents($file1), $contractCode);
        } else {
            $contractCode = str_replace('{add}', '', $contractCode);
        }

        if ($this->is_burn) {
            $file1 = \Yii::getAlias('@avatar/contracts/create-token-v1/main.burn.txt');
            $contractCode = str_replace('{burn}', file_get_contents($file1), $contractCode);
        } else {
            $contractCode = str_replace('{burn}', '', $contractCode);
        }

        if ($this->is_change_owner) {
            $file1 = \Yii::getAlias('@avatar/contracts/create-token-v1/main.changeOwner.txt');
            $contractCode = str_replace('{changeOwner}', file_get_contents($file1), $contractCode);
        } else {
            $contractCode = str_replace('{changeOwner}', '', $contractCode);
        }

        foreach ($replace as $from => $to) {
            $contractCode = str_replace($from, $to, $contractCode);
        }

        return $contractCode;
    }


    /**
     * Сохраняет файл
     *
     * @param \cs\services\File $file
     * @param string            $extension
     * @param array             $field
     *
     * @return bool
     */
    public function save($file, $extension, $field)
    {
        /** @var \cs\base\FormActiveRecord $model */
        $model = $this->model;
        $attribute = $this->attribute;
        $fieldName = $attribute;

        $path = $this->getFolderPath($field, $model);

        $folderSmall = $path->create('small');
        $folderOriginal = $path->create('original');
        $fileName = $fieldName . '.' . $extension;
        $folderSmall->add($fileName)->deleteFile();
        $folderOriginal->add($fileName)->deleteFile();

        $smallFormat = ArrayHelper::getValue($field, 'widget.1.options.small', false);
        $originalFormat = ArrayHelper::getValue($field, 'widget.1.options.original', false);
        $this->saveImage($file, $folderOriginal, $originalFormat, $field);

        if ($smallFormat === false) {
            Yii::info(\yii\helpers\VarDumper::dumpAsString($folderOriginal), 'avatar\models\validate\CabinetTokenControllerNew::save');

            return $folderOriginal->getPath();
        } else {
            $this->saveImage(File::path($folderOriginal->getPathFull()), $folderSmall, $smallFormat, $field);
            Yii::info(\yii\helpers\VarDumper::dumpAsString($folderSmall), 'avatar\models\validate\CabinetTokenControllerNew::save2');

            return $folderSmall->getPath();
        }

    }

    /**
     * Сохраняет картинку по формату
     *
     * @param \cs\services\File     $file
     * @param \cs\services\SitePath $destination
     * @param array $field
     * @param array | false $format => [
     *           3000,
     *           3000,
     *           FileUpload::MODE_THUMBNAIL_OUTBOUND
     *          'isExpandSmall' => true,
     *      ] ,
     *
     * @return \cs\services\SitePath
     */
    private function saveImage($file, $destination, $format, $field)
    {
        if ($format === false || is_null($format)) {
            $file->save($destination->getPathFull());
            return $destination;
        }

        $widthFormat = 1;
        $heightFormat = 1;
        if (is_numeric($format)) {
            // Обрезать квадрат
            $widthFormat = $format;
            $heightFormat = $format;
        }
        else if (is_array($format)) {
            $widthFormat = $format[0];
            $heightFormat = $format[1];
        }

        // generate a thumbnail image
        $mode = ArrayHelper::getValue($format, 2, \common\widgets\FileUpload3\FileUpload::MODE_THUMBNAIL_CUT);
        if ($file->isContent()) {
            $image = Image::getImagine()->load($file->content);
        } else {
            $image = Image::getImagine()->open($file->path);
        }
        if (ArrayHelper::getValue($format, 'isExpandSmall', true)) {
            $image = self::expandImage($image, $widthFormat, $heightFormat, $mode);
        }
        $quality = ArrayHelper::getValue($field, 'widget.1.options.quality', 80);
        $options = ['quality' => $quality];
        $image->thumbnail(new Box($widthFormat, $heightFormat), $mode)->save($destination->getPathFull(), $options);

        return $destination;
    }

    /**
     * Расширяет маленькую картинку по заданной стратегии
     *
     * @param \Imagine\Image\ImageInterface $image
     * @param int $widthFormat
     * @param int $heightFormat
     * @param int $mode
     *
     * @return \Imagine\Image\ImageInterface
     */
    protected static function expandImage($image, $widthFormat, $heightFormat, $mode)
    {
        $size = $image->getSize();
        $width = $size->getWidth();
        $height = $size->getHeight();
        if ($width < $widthFormat || $height < $heightFormat) {
            // расширяю картинку
            if ($mode == \common\widgets\FileUpload3\FileUpload::MODE_THUMBNAIL_CUT) {
                if ($width < $widthFormat && $height >= $heightFormat) {
                    $size = $size->widen($widthFormat);
                } else if ($width >= $widthFormat && $height < $heightFormat) {
                    $size = $size->heighten($heightFormat);
                } else if ($width < $widthFormat && $height < $heightFormat) {
                    // определяю как расширять по ширине или по высоте
                    if ($width / $widthFormat < $height / $heightFormat) {
                        $size = $size->widen($widthFormat);
                    }
                    else {
                        $size = $size->heighten($heightFormat);
                    }
                }
                $image->resize($size);
            } else {
                if ($width < $widthFormat && $height >= $heightFormat) {
                    $size = $size->heighten($heightFormat);
                } else if ($width >= $widthFormat && $height < $heightFormat) {
                    $size = $size->widen($widthFormat);
                } else if ($width < $widthFormat && $height < $heightFormat) {
                    // определяю как расширять по ширине или по высоте
                    if ($width / $widthFormat < $height / $heightFormat) {
                        $size = $size->heighten($heightFormat);
                    }
                    else {
                        $size = $size->widen($widthFormat);
                    }
                }
                $image->resize($size);
            }
        }

        return $image;
    }


    /**
     * Создает папку для загрузки
     *
     * @param array             $field
     *
     * @return \cs\services\SitePath
     */
    protected function getFolderPath($field)
    {
        $model = $this->model;
        $folder = ArrayHelper::getValue($field, 'type.1.folder', 'requests_token_create');

        return UploadFolderDispatcher::createFolder('FileUpload3', $folder, $model->id);
    }

}
