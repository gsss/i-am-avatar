<?php

namespace avatar\models\validate;

use avatar\models\forms\school\CommandLink;
use common\components\providers\ETH;
use common\models\avatar\Currency;
use common\models\avatar\UserBill;
use common\models\avatar\UserBillOperation;
use common\models\PassportLink;
use common\models\RequestTokenCreate;
use common\models\school\AdminLink;
use common\models\school\Kurs;
use common\models\school\Potok;
use common\models\school\PotokUser3Link;
use common\models\school\School;
use common\models\school\SubscribeItem;
use common\models\school\UserLink;
use common\models\task\Helper;
use common\models\task\Task;
use common\models\UserAvatar;
use common\models\UserRoot;
use common\models\UserRootExt;
use cs\services\File;
use cs\services\Str;
use cs\services\UploadFolderDispatcher;
use cs\services\VarDumper;
use Imagine\Image\Box;
use Yii;
use yii\base\Model;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Json;
use yii\imagine\Image;

/**
 */
class CabinetSchoolTaskListHelperSearchAjax extends Model
{
    /** @var  int идентиифкатор школы */
    public $school_id;

    /** @var School */
    public $school;

    /** @var  string */
    public $term;

    /** @var  int идентиифкатор задачи */
    public $task_id;

    /** @var  Task */
    public $task;

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            ['school_id', 'required'],
            ['school_id', 'integer'],
            ['school_id', 'validateSchool'],

            ['task_id', 'required'],
            ['task_id', 'integer'],
            ['task_id', 'validateTask'],

            ['term', 'string'],
        ];
    }

    public function validateTask($attr, $params)
    {
        if (!$this->hasErrors()) {
            $task = Task::findOne($this->task_id);
            if (is_null($task)) {
                $this->addError($attr, 'Не найдена Задача');
                return;
            }
            $this->task = $task;
        }
    }

    public function validateSchool($attr, $params)
    {
        if (!$this->hasErrors()) {
            $s = School::findOne($this->school_id);
            if (is_null($s)) {
                $this->addError($attr, 'Не найдена школа');
                return;
            }

            $this->school = $s;
        }
    }

    /**
     * @return array;
     */
    public function action()
    {
        $term = $this->term;
        $id = $this->school_id;

        $query = \common\models\UserAvatar::find()
            ->select([
                'id',
                'email',
                'name_first',
                'name_last',
            ])
            ->where([
                'or',
                ['like', 'email', $term],
                ['like', 'name_first', $term],
                ['like', 'name_last', $term],
            ])
            ->andWhere(['not', ['id' => $this->task->executer_id]])
        ;
        // Ограничиваю область поиска командой школы
        $items = CommandLink::find()->where(['school_id' => $id])->select(['user_id'])->column();
        if (count($items) > 0) $query->andWhere(['id' => $items]);

        // Если назначен исполнитель то исключаю его из поиска
        if ($this->task->executer_id) $query->andWhere(['not', ['id' => $this->task->executer_id]]);

        // исключаю уже назначенных помощников
        $items = \common\models\task\Helper::find()->where(['task_id' => $this->task->id])->select('user_id')->column();
        if (count($items) > 0) $query->andWhere(['not', ['id' => $items]]);

        $rows = $query
            ->asArray()
            ->limit(10)
            ->all();

        $rows2 = [];
        foreach ($rows as $item) {
            $value = ($item['name_first'] . $item['name_last'] . '' == '') ? $item['email'] : $item['name_first'] . ' ' . $item['name_last'] . ' (' . $item['email'] . ')';
            $value = Html::encode($value);
            $rows2[] = [
                'id'    => $item['id'],
                'value' => $value,
            ];
        }

        return $rows2;
    }
}
