<?php

namespace avatar\models\validate;

use common\components\providers\ETH;
use common\models\avatar\Currency;
use common\models\avatar\UserBill;
use common\models\avatar\UserBillOperation;
use common\models\PassportLink;
use common\models\RequestTokenCreate;
use common\models\school\AdminLink;
use common\models\school\Kurs;
use common\models\school\Potok;
use common\models\school\PotokUser3Link;
use common\models\school\SubscribeItem;
use common\models\school\UserLink;
use common\models\task\Helper;
use common\models\task\Task;
use common\models\UserAvatar;
use common\models\UserRoot;
use common\models\UserRootExt;
use common\services\Subscribe;
use cs\Application;
use cs\services\File;
use cs\services\Str;
use cs\services\UploadFolderDispatcher;
use cs\services\Url;
use cs\services\VarDumper;
use Imagine\Image\Box;
use Yii;
use yii\base\Model;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;
use yii\helpers\StringHelper;
use yii\imagine\Image;

/**
 */
class CabinetSchoolTaskListExport extends Model
{
    /** @var  int идентиифкатор пользователя который добавляется */
    public $id;

    /** @var  string */
    public $search;


    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            ['id', 'required'],
            ['id', 'integer'],

            ['search', 'required'],
            ['search', 'string'],
        ];
    }

    public function validateTask($attr, $params)
    {
        if (!$this->hasErrors()) {
            $task = Task::findOne($this->task_id);
            if (is_null($task)) {
                $this->addError($attr, 'Не найдена Задача');
                return;
            }
            $this->task = $task;

            $result = UserLink::find()
                ->where([
                    'school_id'    => $task->school_id,
                    'user_root_id' => $this->user->user_root_id,
                ])
                ->exists();

            if (!$result) {
                $this->addError($attr, 'Пользователя нет в школе');
                return;
            }
        }
    }

    public function validateUser($attr, $params)
    {
        if (!$this->hasErrors()) {
            $user = UserAvatar::findOne($this->id);
            if (is_null($user)) {
                $this->addError($attr, 'Не найден пользователь');
                return;
            }

            $this->user = $user;
        }
    }

    /**
     * @return
     */
    public function action()
    {
        $s = substr($this->search, 1);

        $params = explode('&', $s);
        $params2 = [];
        foreach ($params as $p1) {
            $arr = explode('=', $p1);
            $params2[] = [
                'name' => urldecode($arr[0]),
                'value' => urldecode($arr[1]),
            ];
        }

        $task = [];
        foreach ($params2 as $p) {
            if (StringHelper::startsWith($p['name'], 'Task')) {
                $name = substr($p['name'],5, strlen($p['name']) - 5 - 1);
                $task[$name] = $p['value'];
            }
        }

        $statusList = \common\models\task\Status::find()
            ->where(['school_id' => $this->id])
            ->orderBy(['sort_index' => SORT_ASC])
            ->select('id')
            ->column();

        $sort = new \yii\data\Sort([
            'attributes'   => [
                'id'                => ['label' => 'ID'],
                'status'            => [
                    'label' => 'Прогресс',
                    'asc'   => ['status' => SORT_ASC, 'category_id' => SORT_ASC],
                    'desc'  => ['status' => SORT_DESC, 'category_id' => SORT_DESC],
                ],
                'price'             => [
                    'label'   => 'Награда',
                    'default' => SORT_DESC,
                ],
                'created_at'        => ['label' => 'Созд', 'default' => SORT_DESC],
                'name'              => ['label' => 'Наименование'],
                'last_comment_time' => ['label' => 'Обн', 'default' => SORT_DESC],
                'category_id'       => ['label' => 'category_id'],
                'currency_id '      => ['label' => 'Валюта'],
            ],
            'defaultOrder' => [
                'category_id' => SORT_ASC,
            ],
        ]);
        $model = new \avatar\models\search\Task();
        $provider = $model->search($sort, ['Task' => $task], $statusList, [
            'school_task.school_id' => $this->id,
            'is_hide'               => 0,
        ]);

        $statusList = [
            1 => 'Выполняется',
            2 => 'Проверяется',
            3 => 'Выполнено',
        ];

        $rows = [];
        $row = [];
        $row[] = '"' .  'ID' . '"';
        $row[] = '"' .  'Наименование' . '"';
        $row[] = '"' .  'Автор' . '"';
        $row[] = '"' .  'Исполнитель' . '"';
        $row[] = '"' .  'Награда' . '"';
        $row[] = '"' .  'Награда2' . '"';
        $row[] = '"' .  'Статус' . '"';
        $row[] = '"' .  'Создано' . '"';
        $row[] = '"' .  'Обновлено' . '"';
        $rows[] = join(';', $row);
        /** @var \avatar\models\search\Task $item */
        foreach ($provider->query->all() as $item) {
            $row = [];
            $row[] = $item['id'];
            $row[] = '"' .  $item['name'] . '"';
            if (!Application::isEmpty($item['user_id'])) {
                $author = UserAvatar::findOne($item['user_id']);
                $v = '"' .  $author->getName2() . '"';
            } else {
                $v = '"' . '"';
            }
            $row[] = $v;
            if (!Application::isEmpty($item['executer_id'])) {
                $author = UserAvatar::findOne($item['executer_id']);
                $v = '"' .  $author->getName2() . '"';
            } else {
                $v = '"' . '"';
            }
            $row[] = $v;
            $Currency = \common\models\piramida\Currency::findOne($item['currency_id']);
            if (is_null($Currency)) {
                $c = '';
            } else {
                $c = $Currency->code;
            }
            $row[] = '"' .  Yii::$app->formatter->asDecimal($item['price']/100, 2) . ' ' . $c . '"';
            $row[] = $item['price']/100;
            if (is_null($item['type_id'])) {
                $s = '';
            } else {
                $s = $statusList[$item['type_id']];
            }
            $row[] = '"' .  $s . '"';
            $row[] = '"' .  Yii::$app->formatter->asDatetime($item['created_at']) . '"';
            $row[] = '"' .  Yii::$app->formatter->asDatetime($item['updated_at']) . '"';
            $rows[] = join(';', $row);
        }

        return Yii::$app->response->sendContentAsFile(join("\n", $rows),'file1.csv');
    }
}
