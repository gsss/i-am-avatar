<?php

namespace avatar\models\validate;

use BitcoinPHP\BitcoinECDSA\BitcoinECDSA;
use common\components\Card;
use common\models\avatar\QrCode;
use common\models\avatar\UserBill;
use common\models\avatar\UserBillMerchant;
use common\models\SendLetter;
use common\models\UserDigitalSignList;
use common\models\UserDigitalSignPersonalData;
use common\models\UserDocumentSignature;
use cs\Application;
use cs\services\Str;
use Yii;
use yii\base\Model;
use yii\db\Query;
use yii\helpers\ArrayHelper;
use yii\helpers\VarDumper;

/**
 */
class CabinetDocumentSignVer2 extends \iAvatar777\services\FormAjax\Model
{
    /** @var  string адрес */
    public $address;

    /** @var  string подпись */
    public $message;

    /** @var  int документ */
    public $id;

    /** @var  string подпись */
    public $sign;


    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            ['id', 'required'],
            ['id', 'trim'],
            ['id', 'integer'],

            ['message', 'required'],
            ['message', 'trim'],
            ['message', 'string'],

            ['sign', 'required'],
            ['sign', 'trim'],
            ['sign', 'string'],
            ['sign', 'validateCardNumber'],
        ];
    }

    public function init()
    {
        $ds = \common\models\UserDigitalSign::findOne(['user_id' => Yii::$app->user->id]);
        $this->address = $ds->address;
    }

    /**
     * @param $attribute
     * @param $params
     */
    public function validateCardNumber($attribute, $params)
    {
        if (!$this->hasErrors()) {
            $bitcoinECDSA = new BitcoinECDSA();

            if (!$bitcoinECDSA->checkSignatureForMessage($this->address, $this->sign, $this->message)) {
                $this->addError($attribute, 'Не верен адрес или подпись');
                return;
            }
        }
    }

    public function save($runValidation = true, $attributeNames = null)
    {
        $sign = UserDocumentSignature::add([
            'document_id' => $this->id,
            'member'      => $this->address,
            'signature'   => $this->sign,
            'user_id'     => Yii::$app->user->id,
        ]);

        return $sign;
    }

}
