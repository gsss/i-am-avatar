<?php

namespace avatar\models\validate;

use common\components\Card;
use common\components\providers\ETH;
use common\models\avatar\Currency;
use common\models\avatar\CurrencyLink;
use common\models\avatar\UserBill;
use common\models\avatar\UserBillOperation;
use common\models\PassportLink;
use common\models\PaySystem;
use common\models\RequestTokenCreate;
use common\models\school\AdminLink;
use common\models\UserAvatar;
use common\models\UserDigitalSign;
use common\models\UserDigitalSignList;
use cs\Application;
use cs\services\File;
use cs\services\Str;
use cs\services\UploadFolderDispatcher;
use cs\services\Url;
use cs\services\VarDumper;
use Imagine\Image\Box;
use Yii;
use yii\base\Model;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;
use yii\imagine\Image;
use BitcoinPHP\BitcoinECDSA\BitcoinECDSA;

/**
 *AJAX
 * Генерирует один новый код
 *
 * REQUEST:
 * - secret_num - string | integer - три тимвола в виде цифр которые отображены сзади карты, если целое передается то добавляются ведущие нули. Еси число более 999 то оно будет проигнорировано
 * - design_id - integer - идентификатор дизайна карты
 * - finish - string - дата окончания действия карты, формат 'MM/YYYY'
 *
 * @return string JSON array
 * [
 *      'billing' => [
 *          'id'      => $wallet->billing->id,
 *          'address' => $wallet->billing->address,
 *      ],
 *      'code'    => [
 *          'id'   => $qrCode->id,
 *          'code' => $qrCode->code,
 *      ],
 * ]
 */
class AdminQrCodeNewEthereum extends Model
{
    public $design_id;
    public $secret_num;
    public $currency_id;
    public $is_elexir;

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            ['design_id', 'required'],
            ['design_id', 'integer'],

            ['secret_num', 'required'],
            ['secret_num', 'integer'],

            ['currency_id', 'integer'],

            ['is_elexir', 'integer'],
        ];
    }

    /**
     * Если удачно будет удаление на сервере то удаляет локально в таблице school_file
     *
     * Иначе вызываю исключение
     */
    public function action()
    {
        $params = [
            'number'          => Card::generateNumber('8999', '09'),
            'design_id'       => $this->design_id,
            'is_address_work' => 1,
            'secret_num'      => $this->secret_num,
        ];

        $card = \common\models\Card::add($params);
        $walletElxGold = \common\models\piramida\Wallet::addNew(['currency_id' => 1]);
        if ($this->is_elexir == 1) {
            $billELXGOLD = UserBill::add([
                'address'    => (string)$walletElxGold->id,
                'currency'   => 1,
                'card_id'    => $card->id,
                'is_default' => 1,
                'name'       => $walletElxGold->getCurrency()->name,
            ]);
            $walletElxSky = \common\models\piramida\Wallet::addNew(['currency_id' => 2]);
            $billELXSKY = UserBill::add([
                'address'    => (string)$walletElxSky->id,
                'currency'   => 2,
                'card_id'    => $card->id,
                'is_default' => 1,
                'name'       => $walletElxSky->getCurrency()->name,
            ]);
        }
        if ($this->currency_id != '-1') {
            $link = CurrencyLink::findOne(['currency_ext_id' => $this->currency_id]);
            $walletMy = \common\models\piramida\Wallet::addNew(['currency_id' => $link->currency_int_id]);
            $billMy = UserBill::add([
                'address'    => (string)$walletMy->id,
                'currency'   => $this->currency_id,
                'card_id'    => $card->id,
                'is_default' => 1,
                'name'       => $walletMy->getCurrency()->name,
            ]);
        }

        return [
            'card'    => [
                'id'         => $card->id,
                'number'     => $card->number,
                'address'    => $card->address,
                'finish'     => $card->finish,
            ],
        ];
    }
}
