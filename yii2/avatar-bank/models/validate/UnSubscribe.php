<?php

namespace avatar\models\validate;

use common\components\Card;
use common\models\avatar\QrCode;
use common\models\avatar\UserBill;
use common\models\avatar\UserBillMerchant;
use common\models\SendLetter;
use common\models\school\Potok;
use common\models\school\PotokUser3Link;
use common\models\school\SubscribeItem;
use common\models\UserAvatar;
use common\models\UserRoot;
use cs\Application;
use cs\services\Str;
use Yii;
use yii\base\Model;
use yii\helpers\ArrayHelper;
use yii\helpers\VarDumper;

/**
 */
class UnSubscribe extends Model
{
    public $email;
    public $potok_id;
    public $hash;
    public $sid;

    /** @var  \common\models\school\PotokUser3Link */
    private $link;

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            ['email', 'required'],
            ['email', 'email'],
            ['email', 'exist', 'targetClass' => '\common\models\UserRoot'],

            ['potok_id', 'required'],
            ['potok_id', 'integer'],
            ['potok_id', 'exist', 'targetClass' => '\common\models\school\Potok', 'targetAttribute' => 'id'],

            ['sid', 'integer'],
            ['sid', 'exist', 'targetClass' => '\common\models\school\SubscribeItem', 'targetAttribute' => 'id'],

            ['hash', 'required'],
            ['hash', 'validateHash'],
        ];
    }

    /**
     * Проверяет Хеш
     *
     * @param $attribute
     * @param $params
     */
    public function validateHash($attribute, $params)
    {
        if (!$this->hasErrors()) {
            $user_root = UserRoot::findOne(['email' => $this->email]);
            $link = PotokUser3Link::findOne(['user_root_id' => $user_root->id, 'potok_id' => $this->potok_id]);
            if (is_null($link)) {
                $this->addError($attribute, 'Не найдена подписка');
                return;
            }
            $this->link = $link;

            if (!password_verify('iAvatar777', $this->hash)) {
                $this->addError($attribute, 'Не верный hash');
                return;
            }
        }
    }

    /**
     * @return bool
     */
    public function action()
    {
        $this->link->is_unsubscribed = 1;
        $this->link->save();
        $s = SubscribeItem::findOne($this->sid);
        $s->unsubscribe_count++;
        $s->save();

        return true;
    }
}
