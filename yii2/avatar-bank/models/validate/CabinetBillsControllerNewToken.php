<?php

namespace avatar\models\validate;

use avatar\models\WalletToken;
use common\components\providers\ETH;
use common\models\avatar\Currency;
use common\models\avatar\UserBill;
use common\models\Token;
use Yii;
use yii\authclient\clients\Yandex;
use yii\base\Model;

/**
 */
class CabinetBillsControllerNewToken extends Model
{
    /** @var  string название счета */
    public $name;

    /** @var  int идентификатор токена */
    public $token_id;

    /** @var  int идентификатор счета ETH Для которого будет привязан токен */
    public $billing_id;

    /** @var  \common\models\avatar\UserBill счет ETH, устанавливается в validateBilling  */
    public $billing;

    /** @var  string  */
    public $password;

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            ['name', 'required'],
            ['name', 'string'],

            ['billing_id', 'required', 'message' => Yii::t('c.tvITsCQPKS', 'Нужно обязательно выбрать счет')],
            ['billing_id', 'integer'],
            ['billing_id', 'validateBilling'],

            ['token_id', 'required', 'message' => Yii::t('c.tvITsCQPKS', 'Нужно обязательно выбрать один из токенов')],
            ['token_id', 'integer'],
            ['token_id', 'exist', 'targetAttribute' => 'id', 'targetClass' => '\common\models\Token'],
            ['billing_id', 'validateToken'],

            ['password', 'string'],
            ['password', 'validatePassword'],
        ];
    }

    /**
     * @param $attribute
     * @param $params
     */
    public function validatePassword($attribute, $params)
    {
        if (!$this->hasErrors()) {
            /** @var \common\models\UserAvatar $user */
            $user = Yii::$app->user->identity;
            if (!$user->validatePassword($this->password)) {
                $this->addError($attribute, Yii::t('c.tvITsCQPKS', 'Пароль от кабинета не верный'));
                return;
            }
        }
    }

    /**
     * @param $attribute
     * @param $params
     */
    public function validateBilling($attribute, $params)
    {
        if (!$this->hasErrors()) {
            $billing = UserBill::findOne($this->billing_id);
            if (is_null($billing)) {
                $this->addError($attribute, Yii::t('c.tvITsCQPKS', 'Нет такого счета'));
                return;
            }
            if ($billing->currency != Currency::ETH) {
                $this->addError($attribute, Yii::t('c.tvITsCQPKS', 'Счет должен быть с валютой ETH'));
                return;
            }
            $this->billing = $billing;
        }
    }

    /**
     * Проверяет на ситуацию:
     * Если у человека уже есть токен на этот же адрес то создать второй нельзя
     *
     * @param $attribute
     * @param $params
     */
    public function validateToken($attribute, $params)
    {
        if (!$this->hasErrors()) {
            $token = Token::findOne($this->token_id);

            $isExist = UserBill::find()->where([
                'address'      => $this->billing->address,
                'mark_deleted' => 0,
                'currency'     => $token->currency_id,
            ])->exists();
            if ($isExist) {
                $this->addError($attribute, Yii::t('c.tvITsCQPKS', 'У вас уже есть такой токен'));
                return;
            }
        }
    }

    /**
     * Создает счет для токена
     *
     * @return \avatar\models\WalletToken
     */
    public function action()
    {
        /** @var \common\models\UserAvatar $user */
        $user = \Yii::$app->user->identity;
        $wallet = WalletToken::create(
            $this->billing,
            $this->token_id,
            $this->name,
            $this->password,
            $user->id,
            $user->password_save_type
        );

        return $wallet;
    }
}
