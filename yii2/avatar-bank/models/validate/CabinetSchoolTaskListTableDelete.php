<?php

namespace avatar\models\validate;

use common\models\task\Table;
use yii\base\Model;

/**
 *
 */
class CabinetSchoolTaskListTableDelete extends Model
{
    /** @var int */
    public $id;

    /** @var  \common\models\task\Table */
    public $table;

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            ['id', 'required'],
            ['id', 'string'],
            ['id', 'validateTable'],
        ];
    }

    /**
     */
    public function validateTable($attribute, $params)
    {
        if (!$this->hasErrors()) {
            $Table = Table::findOne($this->id);
            if (is_null($Table)) {
                $this->addError($attribute, 'Не такой доски');
                return;
            }

            $this->table = $Table;
        }
    }

    /**
     */
    public function action()
    {
        $this->table->delete();
    }
}
