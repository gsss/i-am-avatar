<?php

namespace avatar\models\validate;

use common\components\providers\ETH;
use common\models\avatar\Currency;
use common\models\avatar\UserBill;
use common\models\avatar\UserBillOperation;
use common\models\KoopPodryad;
use common\models\KoopPodryadCommandUser;
use common\models\PassportLink;
use common\models\RequestTokenCreate;
use common\models\school\AdminLink;
use common\models\school\Kurs;
use common\models\school\Potok;
use common\models\school\PotokUser3Link;
use common\models\school\SubscribeItem;
use common\models\school\UserLink;
use common\models\task\Helper;
use common\models\task\Task;
use common\models\UserAvatar;
use common\models\UserRoot;
use common\models\UserRootExt;
use common\services\Subscribe;
use cs\services\File;
use cs\services\Str;
use cs\services\UploadFolderDispatcher;
use cs\services\VarDumper;
use Imagine\Image\Box;
use Yii;
use yii\base\Model;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;
use yii\imagine\Image;

/**
 */
class CabinetKoopPodryadStep2Finish extends Model
{
    /** @var  int идентиифкатор задачи */
    public $task_id;

    /** @var  KoopPodryad */
    public $task;

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            ['task_id', 'required'],
            ['task_id', 'integer'],
            ['task_id', 'validateTask'],
        ];
    }

    public function validateTask($attr, $params)
    {
        if (!$this->hasErrors()) {
            $task = KoopPodryad::findOne($this->task_id);
            if (is_null($task)) {
                $this->addError($attr, 'Не найдена Задача');
                return;
            }
            $this->task = $task;
        }
    }


    /**
     * @return
     */
    public function action()
    {
        // назначаю исполнителя
        $this->task->status = 2;
        $this->task->save();

        return 1;
    }
}
