<?php

namespace avatar\models\validate;

use avatar\models\forms\school\Article;
use common\components\providers\ETH;
use common\models\avatar\Currency;
use common\models\avatar\UserBill;
use common\models\avatar\UserBillOperation;
use common\models\BlogItem;
use common\models\PassportLink;
use common\models\RequestTokenCreate;
use common\models\school\AdminLink;
use common\models\school\Kurs;
use common\models\school\Potok;
use common\models\school\PotokUser3Link;
use common\models\school\School;
use common\models\school\SubscribeItem;
use common\models\shop\Request;
use common\models\UserAvatar;
use common\models\UserDocument;
use common\models\UserRoot;
use common\models\UserRootExt;
use common\services\Security;
use cs\Application;
use cs\services\File;
use cs\services\Str;
use cs\services\UploadFolderDispatcher;
use cs\services\Url;
use cs\services\VarDumper;
use Imagine\Image\Box;
use Yii;
use yii\base\Model;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;
use yii\helpers\StringHelper;
use yii\imagine\Image;

/**
 */
class CabinetSchoolBlogSign extends Model
{
    /** @var int идентификатор статьи блога */
    public $id;

    /** @var  School */
    public $school;

    /** @var \common\models\blog\Article */
    public $article;

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            ['id', 'required'],
            ['id', 'integer'],
            ['id', 'validateSchool'],
        ];
    }

    public function validateSchool($attr, $params)
    {
        if (!$this->hasErrors()) {
            $article = \common\models\blog\Article::findOne($this->id);
            if (is_null($article)) {
                $this->addError($attr, 'Школа не найдена');
                return;
            }
            $s = School::findOne($article->school_id);

            $this->school = $s;
            $this->article = $article;
        }
    }

    /**
     *
     */
    public function action()
    {
        // Файл идентификации статьи и ее содержимого по стандарту БОСТ, который я регистририую
        $file = Json::encode([
            'bost-version'   => '1.0.0',
            'name'           => $this->article->name,
            'content'        => [
                'format' => 'html',
                'data'   => $this->article->content,
            ],
            'created_at'     => $this->article->created_at,
            'created_at_utc' => date(DATE_ATOM, $this->article->created_at),
            'author'         => [
                'avatar-id' => Yii::$app->user->id,
            ],
        ]);
        $sha256 = hash('sha256', $file);
        $fileData = $this->uploadFile('article.json', $file);
        $url = $fileData['url'];

        return [
            'name' => $this->article->name,
            'url'  => $url,
            'hash' => $sha256,
        ];
    }

    /**
     * @param $name
     * @param $content
     *
     * @return array
     * [
     * 'url'  => 'http://clo...',
     * ];
     * @throws
     */
    public function uploadFile($name, $content)
    {
        /** @var \common\services\AvatarCloud $cloud */
        $cloud = Yii::$app->AvatarCloud;

        $folder = UploadFolderDispatcher::createFolder('sign/' . Security::generateRandomString(16));
        $fullpath = $folder->add($name)->getPathFull();
        file_put_contents($fullpath, $content);
        $file = \cs\services\File::path($fullpath);
        /**
         *[
         * 'fileList' => [
         * 'file'   => '$newFileName',
         * 'url'    => 'http://cloud1.cloud999.ru/asa',
         * 'size'   => '12324565 в байтах',
         * 'update' => [
         * [
         * 'url'  => 'string',
         * 'size' => 'int',
         * ],
         * ],
         * ],
         * ];
         */
        $data = $cloud->post(null, 'upload/image', [], ['files' => [$file]]);
        if ($data['success']) {
            $fileList = $data['data']['fileList'];
        }

        return [
            'url'  => $fileList[0]['url'],
        ];
    }
}
