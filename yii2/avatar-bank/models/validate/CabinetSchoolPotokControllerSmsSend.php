<?php

namespace avatar\models\validate;

use avatar\models\forms\School;
use common\components\providers\ETH;
use common\models\avatar\Currency;
use common\models\avatar\UserBill;
use common\models\avatar\UserBillOperation;
use common\models\PassportLink;
use common\models\RequestTokenCreate;
use common\models\school\AdminLink;
use common\models\school\Kurs;
use common\models\school\Lesson;
use common\models\school\Potok;
use common\models\school\PotokUser3Link;
use common\models\school\PotokUserExt;
use common\models\school\SubscribeItem;
use common\models\UserAvatar;
use common\models\UserRoot;
use common\models\UserRootExt;
use cs\services\File;
use cs\services\Str;
use cs\services\UploadFolderDispatcher;
use cs\services\VarDumper;
use Imagine\Image\Box;
use Yii;
use yii\base\Model;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;
use yii\imagine\Image;

/**
 */
class CabinetSchoolPotokControllerSmsSend extends Model
{
    /** @var  int school_kurs_lesson.id */
    public $id;

    /** @var  Potok */
    public $potok;

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            ['id', 'required'],
            ['id', 'integer'],
            ['id', 'validatePotok'],
        ];
    }

    public function validatePotok($attr, $params)
    {
        if (!$this->hasErrors()) {
            $p = Potok::findOne($this->id);
            if (is_null($p)) {
                $this->addError($attr, 'Не найден поток');
                return;
            }
            if (!AdminLink::find()->where([
                'school_id' => $p->getSchool()->id,
                'user_id'   => Yii::$app->user->id,
            ])->exists()) {
                $this->addError($attr, 'Вам отказано в доступе');
                return;
            }
            $this->potok = $p;
        }
    }

    /**
     */
    public function action()
    {
        // определить список телефонов
        $users = [];
        $list = PotokUser3Link::find()
            ->where(['potok_id' => $this->id])
            ->all();
        /** @var \common\models\school\PotokUser3Link $item */
        foreach ($list as $item) {
            if ($item->is_avatar == \common\models\school\PotokUser3Link::IS_AVATAR_USER) {
                try {
                    $userAvatar = UserAvatar::findOne(['user_root_id' => $item->user_root_id]);
                    if ($userAvatar->phone) {
                        $users[] = $userAvatar->phone;
                    }
                } catch (\Exception $e) {

                }
            }
            if ($item->is_avatar == \common\models\school\PotokUser3Link::IS_AVATAR_GUEST) {
                $user = PotokUserExt::findOne(['link_id' => $item->id]);
                if (!is_null($user)) {
                    if ($user->phone) {
                        $users[] = $user->phone;
                    }
                }
            }
        }

        // нормализация телефонов
        $rows = [];
        foreach ($users as $tel) {
            $rows[] = $this->clearTel($tel);
        }
        // фильтр
        $rows2 = [];
        foreach ($rows as $tel) {
            if ($this->isFilter($tel)) {
                $rows2[] = $tel;
            }
        }
        // рассылка
        $result = $this->send($rows2);

        return $result;
    }

    /**
     * Делает рассылку СМС
     * @param array $list
     * @return array
     * [
     * [
     * 'tel'    => $tel,
     * 'result' => $result,
     * ], ...
     * ]
     */
    private function send($list)
    {
        $text = 'Здравия! В 20:00 МСК состоится трансляция видеопередачи Новая Земля по ссылке https://youtu.be/WPtVqypgMgo';

        $subscribe = \common\models\school\SubscribeSms::add([
            'school_id' => $this->potok->getSchool()->id,
            'potok_id'  => $this->potok->id,
            'text'      => $text,
        ]);
        /** @var \common\components\sms\IqSms $smsProvider */
        $smsProvider = Yii::$app->sms;
        $rows = [];
        foreach ($list as $tel) {
            $result = $smsProvider->send($tel, $text);
            $subscribe = \common\models\school\SubscribeSmsResult::add([
                'subscribe_id' => $subscribe->id,
                'phone'        => $tel,
                'result'       => $result,
            ]);
            $rows[] = [
                'tel'    => $tel,
                'result' => $result,
            ];
        }

        return $rows;
    }

    private function clearTel($phone)
    {
        $phone = str_replace('+','', $phone);
        $phone = str_replace('(','', $phone);
        $phone = str_replace(')','', $phone);
        $phone = str_replace('-','', $phone);
        $phone = str_replace(' ','', $phone);

        return $phone;
    }

    private function isFilter($phone)
    {
        if (in_array(substr($phone,0, 1), ['7','8'])) {
            if (strlen($phone) != 11) return false;
            return true;
        }

        return false;
    }
}
