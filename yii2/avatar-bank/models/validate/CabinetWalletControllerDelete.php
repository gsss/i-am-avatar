<?php

namespace avatar\models\validate;

use common\models\avatar\CurrencyLink;
use common\models\avatar\UserBill;
use common\models\piramida\Wallet;
use common\models\SendLetter;
use cs\Application;
use cs\services\Str;
use Yii;
use yii\base\Model;
use yii\helpers\ArrayHelper;
use yii\helpers\VarDumper;

/**
 * ContactForm is the model behind the contact form.
 */
class CabinetWalletControllerDelete extends Model
{
    /** @var  int */
    public $id;

    /** @var \common\models\avatar\UserBill */
    public $billing;

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            ['id', 'required'],
            ['id', 'integer'],
            ['id', 'verifyID'],
        ];
    }

    /**
     *
     */
    public function verifyID($attribute, $params)
    {
        if (!$this->hasErrors()) {
            try {
                $billing = UserBill::findOne($this->id);
                $this->billing = $billing;
            } catch (\Exception $e) {
                $this->addError($attribute, 'нет такого счета');
                return;
            }
        }
    }

    /**
     */
    public function action()
    {
        // Проверить является ли счет процессингом Аватара
        $link = CurrencyLink::findOne(['currency_ext_id' => $this->billing->currency]);
        if (!is_null($link)) {
            $wallet = Wallet::findOne($this->billing->address);
            if ($wallet->amount > 0) {
                return false;
            }

            if ($this->billing->is_default) {
                $billList = UserBill::find()
                    ->where([
                        'user_id'      => Yii::$app->user->id,
                        'mark_deleted' => 0,
                        'currency'     => $this->billing->currency,
                    ])
                    ->andWhere(['not', ['id' => $this->billing->id]])
                    ->all();
                if (count($billList) > 0) {
                    /** @var \common\models\avatar\UserBill $b */
                    $b = $billList[0];
                    $b->is_default = 1;
                    $b->save();
                }
            }
            $wallet->delete();
            $this->billing->delete();
        }

        return true;
    }

}
