<?php

namespace avatar\models\validate;

use common\components\Card;
use common\models\avatar\QrCode;
use common\models\avatar\UserBill;
use common\models\avatar\UserBillMerchant;
use common\models\SendLetter;
use common\models\UserAvatar;
use cs\Application;
use cs\services\Str;
use Yii;
use yii\base\Model;
use yii\helpers\ArrayHelper;
use yii\helpers\VarDumper;

/**
 */
class UnSubscribe2 extends Model
{
    public $mail;
    public $type;
    public $hash;

    /** @var  \common\models\UserAvatar */
    public $_user;

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            [['mail', 'type', 'hash'], 'required'],
            [['mail'], 'email'],
            [['type'], 'integer'],
            [['hash'], 'validateHash'],
            [['mail'], 'attachUser'],
        ];
    }

    /**
     * Проверяет Хеш
     *
     * @param $attribute
     * @param $params
     */
    public function validateHash($attribute, $params)
    {
        if (!$this->hasErrors()) {
            if (md5($this->mail . '_' . $this->type) != $this->hash) {
                $this->addError($attribute, 'Hash не верный');
                return;
            }
        }
    }

    /**
     * присоединяет пользователя
     *
     * @param $attribute
     * @param $params
     */
    public function attachUser($attribute, $params)
    {
        if (!$this->hasErrors()) {
            try {
                $user = \common\models\UserAvatar::findOne(['email' => $this->mail]);
            } catch (\Exception $e) {
                $this->addError($attribute, 'Не найден пользователь');
                return;
            }
            $this->_user = $user;
        }
    }

    /**
     * снимает флаг рассылки в зависимости от type
     *
     * @return bool
     */
    public function action()
    {
        switch ($this->type) {
            case UserAvatar::SUBSCRIBE_NEWS:
                $this->_user->subscribe_is_news = 0;
                $this->_user->save();
                break;
            case UserAvatar::SUBSCRIBE_BLOG:
                $this->_user->subscribe_is_blog = 0;
                $this->_user->save();
                break;
        }

        return true;
    }
}
