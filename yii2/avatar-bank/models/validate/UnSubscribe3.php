<?php

namespace avatar\models\validate;

use common\components\Card;
use common\models\avatar\QrCode;
use common\models\avatar\UserBill;
use common\models\avatar\UserBillMerchant;
use common\models\school\PotokUser3Link;
use common\models\school\Subscribe;
use common\models\school\SubscribeItem;
use common\models\SendLetter;
use common\models\UserAvatar;
use common\models\UserRoot;
use cs\Application;
use cs\services\Str;
use Yii;
use yii\base\Model;
use yii\helpers\ArrayHelper;
use yii\helpers\VarDumper;

/**
 */
class UnSubscribe3 extends Model
{
    public $email;
    public $sid;
    public $hash;

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            ['email', 'required'],
            ['email', 'email'],
            ['email', 'exist', 'targetClass' => '\common\models\UserRoot'],

            ['sid', 'required'],
            ['sid', 'integer'],
            ['sid', 'exist', 'targetClass' => '\common\models\school\SubscribeItem', 'targetAttribute' => 'id'],

            ['hash', 'required'],
            ['hash', 'validateHash'],

        ];
    }

    /**
     * Проверяет Хеш
     *
     * @param $attribute
     * @param $params
     */
    public function validateHash($attribute, $params)
    {
        if (!$this->hasErrors()) {
            $pass = \Yii::$app->params['subscribe_unsubscribe_pass'];
            $query = 'sid' . '=' . $this->sid . '&' . 'email' . '=' . urlencode($this->email) . '&' . 'password' . '=' . urlencode($pass);
            $hash = md5($query);

            if ($hash != $this->hash) {
                $this->addError($attribute, 'Hash не верный');
                return;
            }
        }
    }

    /**
     * @return bool
     */
    public function action()
    {
        $userRoot = UserRoot::findOne(['email' => $this->email]);
        $sid = \common\models\school\SubscribeItem::findOne($this->sid);
        $s = Subscribe::findOne($sid->subscribe_id);

        $link = PotokUser3Link::findOne(['user_root_id' => $userRoot->id, 'potok_id' => $s->potok_id]);
        if ($link->is_unsubscribed == 0) {
            $link->is_unsubscribed = 1;
            $link->save();

            $sid->unsubscribe_count++;
            $sid->save();
        }

        return true;
    }
}
