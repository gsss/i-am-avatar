<?php

namespace avatar\models\validate;

use common\components\providers\ETH;
use common\models\avatar\Currency;
use common\models\avatar\UserBill;
use common\models\avatar\UserBillOperation;
use common\models\PassportLink;
use common\models\PaySystem;
use common\models\RequestTokenCreate;
use common\models\school\AdminLink;
use common\models\UserAvatar;
use cs\Application;
use cs\services\File;
use cs\services\Str;
use cs\services\UploadFolderDispatcher;
use cs\services\Url;
use cs\services\VarDumper;
use Imagine\Image\Box;
use Yii;
use yii\base\Model;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;
use yii\imagine\Image;

/**
 *
 */
class CabinetSchoolCommandControllerSet extends Model
{
    public $id;
    /** @var  \common\models\school\CommandLink */
    public $link;
    public $value;

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            ['id', 'required'],
            ['id', 'integer'],
            ['id', 'validateID'],

            ['value', 'required'],
            ['value', 'integer'],
            ['value', 'in', 'range' => [0, 1]],
        ];
    }

    /**
     * Проверяет на наличие файла в БД
     */
    public function validateID($attribute, $params)
    {
        if (!$this->hasErrors()) {
            $link = \common\models\school\CommandLink::findOne($this->id);
            if (is_null($link)) {
                $this->addError($attribute, 'Не найден ID');
                return;
            }
            $this->link = $link;
       }
    }

    /**
     * Если удачно будет удаление на сервере то удаляет локально в таблице school_file
     *
     * Иначе вызываю исключение
     */
    public function action()
    {
        $this->link->is_only_my = $this->value;
        $this->link->save();
    }
}
