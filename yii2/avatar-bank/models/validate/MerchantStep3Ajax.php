<?php

namespace avatar\models\validate;

use common\components\Card;
use common\models\avatar\UserBill;
use common\models\avatar\UserBillMerchant;
use common\models\SendLetter;
use cs\Application;
use cs\services\Str;
use Yii;
use yii\base\Model;
use yii\helpers\ArrayHelper;
use yii\helpers\VarDumper;

/**
 */
class MerchantStep3Ajax extends Model
{
    public $card_number;
    public $merchant_id;
    public $request_id;
    public $amount;

    /** @var  \common\models\avatar\UserBill */
    public $billing;

    /** @var  \common\models\avatar\UserBillMerchant */
    public $merchant;

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            [['card_number', 'merchant_id', 'request_id', 'amount', ], 'required'],
            [['card_number'], 'string'],
            [['card_number'], 'normalize'],
            [['card_number'], 'validateCardNumber'],
            [['card_number'], 'joinBilling'],
            [['amount'], 'double'],
            [['merchant_id', 'request_id'], 'integer'],
            [['merchant_id'], 'joinMerchant'],
        ];
    }

    /**
     * Проверяет номер карты на валидность
     * Как входное значене уже приходит номер без пробелов
     *
     * @param $attribute
     * @param $params
     */
    public function validateCardNumber($attribute, $params)
    {
        if (!$this->hasErrors()) {
            $card_number = $this->card_number;
            if (strlen($card_number) == 16) {
                if (preg_match('/\D/',$card_number) != 0) {
                    $this->addError($attribute, 'В номере карты есть запрещенные символы');
                    return;
                }
                if (!Card::checkSum($card_number)) {
                    $this->addError($attribute, 'Контрольная сумма в номере карты не совпадает');
                    return;
                }
            } else {
                $this->addError($attribute, 'В номере карты должно быть 16 цифр');
                return;
            }
        }
    }

    public function joinBilling($attribute, $params)
    {
        if (!$this->hasErrors()) {
            try {
                $this->billing = UserBill::findOne(['card_number' => $this->card_number]);
            } catch (\Exception $e) {
                $this->addError($attribute, 'Карта не найдена');
            }
        }
    }

    public function joinMerchant($attribute, $params)
    {
        if (!$this->hasErrors()) {
            try {
                $this->merchant = UserBillMerchant::findOne($this->merchant_id);
            } catch (\Exception $e) {
                $this->addError($attribute, 'Мерчант не найден');
            }
        }
    }

    public function normalize($attribute, $params)
    {
        if (!$this->hasErrors()) {
            $this->card_number = str_replace(' ', '', $this->card_number);
        }
    }
}
