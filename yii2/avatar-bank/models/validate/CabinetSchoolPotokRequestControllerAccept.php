<?php

namespace avatar\models\validate;

use common\components\providers\ETH;
use common\models\avatar\Currency;
use common\models\avatar\UserBill;
use common\models\avatar\UserBillOperation;
use common\models\PassportLink;
use common\models\RequestTokenCreate;
use cs\services\File;
use cs\services\Str;
use cs\services\UploadFolderDispatcher;
use cs\services\VarDumper;
use Imagine\Image\Box;
use Yii;
use yii\base\Model;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;
use yii\imagine\Image;

/**
 */
class CabinetSchoolPotokRequestControllerAccept extends Model
{
    public $id;

    /** @var  \common\models\school\SchoolSaleRequest */
    public $request;

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            ['id', 'required'],
            ['id', 'integer'],
            ['id', 'validateRequest'],
        ];
    }

    public function validateRequest($attr, $params)
    {
        if (!$this->hasErrors()) {
            $request = \common\models\school\SchoolSaleRequest::findOne($this->id);
            if (is_null($request)) {
                $this->addError($attr, 'Не найден заказ');
                return;
            }
            $this->request = $request;
        }
    }
    /**
     *
     */
    public function action()
    {
        $this->request->success();
    }
}
