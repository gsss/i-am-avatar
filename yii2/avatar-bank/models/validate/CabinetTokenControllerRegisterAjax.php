<?php

namespace avatar\models\validate;

use common\components\providers\ETH;
use common\models\avatar\Currency;
use common\models\avatar\UserBill;
use common\models\avatar\UserBillOperation;
use common\models\PassportLink;
use common\models\RequestTokenCreate;
use common\models\UserAvatar;
use cs\services\File;
use cs\services\Str;
use cs\services\UploadFolderDispatcher;
use cs\services\VarDumper;
use Imagine\Image\Box;
use Yii;
use yii\base\Model;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;
use yii\imagine\Image;

/**
 */
class CabinetTokenControllerRegisterAjax extends Model
{
    public $id;

    /** @var  \common\models\RequestTokenCreate */
    private $request;

    private $password;

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            ['id', 'required'],
            ['id', 'integer'],
            ['id', 'validateRequest'],
            ['id', 'validatePassword'],
        ];
    }

    /**
     *
     */
    public function validateRequest($attribute, $params)
    {
        if (!$this->hasErrors()) {
            $request = RequestTokenCreate::findOne($this->id);
            if (is_null($request)) {
                $this->addError($attribute, 'Не найдена заявка');
                return;
            }
            if ($request->user_id != Yii::$app->user->id) {
                $this->addError($attribute, 'Это не ваша заявка');
                return;
            }
            $this->request = $request;
        }
    }

    /**
     *
     */
    public function validatePassword($attribute, $params)
    {
        if (!$this->hasErrors()) {
            $password = Yii::$app->cache->get([
                Yii::$app->session->id,
                'createToken'
            ]);
            if ($password === false) {
                $this->addError($attribute, 'Не задан пароль');
                return;
            }
            $this->password = $password;
        }
    }

    /**
     * Производит регистрацию контракта
     *
     * @return array
     * [
     *      'transactionHash'
     *      'abi' - интерфейс
     *      'init' - инициализационные параметры
     *      'contract' - код контракта
     * ]
     */
    public function action()
    {
        // работаю с большими целыми числами
        $pow = bcpow(10, $this->request->decimals);
        $all = bcmul($this->request->coin_emission, $pow);

        /** @var \common\models\UserAvatar $user */
        $user = Yii::$app->user->identity;
        $passport = $user->getWalletEthPassport();

        $response = $passport->registerContract(
            $this->password,
            $this->request->contract,
            $this->request->name,
            [
                $all,
                $this->request->name,
                $this->request->decimals,
                $this->request->code,
            ]
        );
        $response = Json::decode($response);

        $transactionHash = $response['transactionHash'];
        $abi = $response['abi'];

        $this->request->txid = $transactionHash;
        $this->request->abi = $abi;
        $this->request->save();

        $f = UserBillOperation::add([
            'bill_id'     => $passport->billing->id,
            'type'        => UserBillOperation::TYPE_OUT,
            'transaction' => $transactionHash,
            'message'     => 'Регистрация токена ' . $this->request->name,
            'user_id'     => \Yii::$app->user->id,
        ]);

    }
}
