<?php

namespace avatar\models\validate;

use avatar\models\forms\school\Article;
use common\components\providers\ETH;
use common\models\avatar\Currency;
use common\models\avatar\UserBill;
use common\models\avatar\UserBillOperation;
use common\models\BlogItem;
use common\models\PassportLink;
use common\models\RequestTokenCreate;
use common\models\school\AdminLink;
use common\models\school\Kurs;
use common\models\school\Potok;
use common\models\school\PotokUser3Link;
use common\models\school\School;
use common\models\school\SubscribeItem;
use common\models\shop\Request;
use common\models\UserAvatar;
use common\models\UserDocument;
use common\models\UserRoot;
use common\models\UserRootExt;
use common\services\Security;
use cs\Application;
use cs\services\File;
use cs\services\Str;
use cs\services\UploadFolderDispatcher;
use cs\services\Url;
use cs\services\VarDumper;
use Imagine\Image\Box;
use Yii;
use yii\base\Model;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;
use yii\helpers\StringHelper;
use yii\imagine\Image;

/**
 */
class CabinetSchoolNewsRegister extends Model
{
    public $txid;
    public $name;
    public $hash;
    public $url;
    public $id;

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            ['txid', 'required'],
            ['txid', 'string'],

            ['name', 'required'],
            ['name', 'string'],

            ['hash', 'required'],
            ['hash', 'string'],

            ['url', 'required'],
            ['url', 'string'],
            ['url', 'url'],

            ['id', 'required'],
            ['id', 'integer'],
        ];
    }

    /**
     *
     */
    public function action()
    {
        $document = UserDocument::add([
            'user_id'   => Yii::$app->user->id,
            'name'      => $this->name,
            'txid'      => $this->txid,
            'hash'      => $this->hash,
            'link'      => $this->url,
        ]);

        $article = \common\models\blog\Article::findOne($this->id);
        $article->is_signed = 1;
        $article->document_id = $document->id;
        $article->save();

    }

}
