<?php

namespace avatar\models\validate;

use common\components\providers\ETH;
use common\models\avatar\Currency;
use common\models\avatar\UserBill;
use common\models\avatar\UserBillOperation;
use common\models\PassportLink;
use common\models\PaySystem;
use common\models\RequestTokenCreate;
use common\models\school\AdminLink;
use common\models\UserAvatar;
use cs\services\File;
use cs\services\Str;
use cs\services\UploadFolderDispatcher;
use cs\services\Url;
use cs\services\VarDumper;
use Imagine\Image\Box;
use Yii;
use yii\base\Model;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;
use yii\imagine\Image;

/**
 *
 */
class CabinetSchoolFilesControllerDelete extends Model
{
    /** @var int */
    public $id;

    /** @var  \common\models\school\File */
    public $file;

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            ['id', 'required'],
            ['id', 'string'],
            ['id', 'validateFile'],
        ];
    }

    /**
     * Проверяет на наличие файла в БД
     * Проверяет на доступ к школе
     */
    public function validateFile($attribute, $params)
    {
        if (!$this->hasErrors()) {
            $file = \common\models\school\File::findOne($this->id);
            if (is_null($file)) {
                $this->addError($attribute, 'Не такого файла');
                return;
            }
            $result = AdminLink::find()->where([
                'school_id' => $file->school_id,
                'user_id'   => Yii::$app->user->id,
            ])->exists();

            if (!$result) {
                $this->addError($attribute, 'Нет доступа к школе');
                return;
            }

            $this->file = $file;
       }
    }

    /**
     */
    public function action()
    {
        /** @var \common\services\AvatarCloud $cloud */
        $cloud = Yii::$app->AvatarCloud;

        $url = new Url($this->file->file);
        $response = $cloud->_post($url->scheme . '://' . $url->host, 'update/delete', ['file' => $this->file->file]);

    }
}
