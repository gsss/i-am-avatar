<?php

namespace avatar\models\validate;

use common\components\providers\ETH;
use common\models\avatar\Currency;
use common\models\avatar\UserBill;
use common\models\avatar\UserBillOperation;
use common\models\Card;
use common\models\CardSchoolLink;
use common\models\PassportLink;
use common\models\PaySystem;
use common\models\RequestTokenCreate;
use common\models\school\School;
use common\models\UserAvatar;
use cs\services\File;
use cs\services\Str;
use cs\services\UploadFolderDispatcher;
use cs\services\VarDumper;
use Imagine\Image\Box;
use Yii;
use yii\base\Model;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;
use yii\imagine\Image;

/**
 *
 */
class CabinetCardsControllerActionPinAjax extends Model
{
    /** @var string */
    public $id;


    public $pin;

    /** @var  \common\models\Card */
    public $card;

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            ['id', 'required'],
            ['id', 'integer'],
            ['id', 'validateCardNumber'],
            ['pin', 'required'],
            ['pin', 'string', 'length' => 4],
        ];
    }


    /**
     * Проверяет номер карты на валидность
     * Как входное значене уже приходит номер без пробелов
     *
     * @param $attribute
     * @param $params
     */
    public function validateCardNumber($attribute, $params)
    {
        if (!$this->hasErrors()) {
            $card = Card::findOne($this->id);
            if (is_null($card)) {
                $this->addError($attribute, 'Карта не найдена');
                return;
            }
            if ($card->user_id != Yii::$app->user->id) {
                $this->addError($attribute, 'Это не ваша карта');
                return;
            }
            $this->card = $card;
        }
    }


    /**
     */
    public function action()
    {
        // присоединяю владельца карты
        $this->card->pin = UserAvatar::hashPassword($this->pin);
        $this->card->save();
    }


}
