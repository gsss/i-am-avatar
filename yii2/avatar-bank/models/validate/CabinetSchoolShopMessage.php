<?php

namespace avatar\models\validate;

use common\components\providers\ETH;
use common\models\avatar\Currency;
use common\models\avatar\UserBill;
use common\models\avatar\UserBillOperation;
use common\models\PassportLink;
use common\models\RequestTokenCreate;
use common\models\school\AdminLink;
use common\models\school\Kurs;
use common\models\school\Potok;
use common\models\school\PotokUser3Link;
use common\models\school\School;
use common\models\school\SubscribeItem;
use common\models\shop\Request;
use common\models\UserAvatar;
use common\models\UserRoot;
use common\models\UserRootExt;
use cs\Application;
use cs\services\File;
use cs\services\Str;
use cs\services\UploadFolderDispatcher;
use cs\services\VarDumper;
use Imagine\Image\Box;
use Yii;
use yii\base\Model;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;
use yii\imagine\Image;

/**
 */
class CabinetSchoolShopMessage extends Model
{
    public $id;

    /** @var  Request */
    public $request;

    public $text;

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            ['id', 'required'],
            ['id', 'integer'],
            ['id', 'validateRequest'],

            ['text', 'required'],
        ];
    }

    public function validateRequest($attr, $params)
    {
        if (!$this->hasErrors()) {
            $request = Request::findOne($this->id);
            if (!$request->getSchool()->isAccess()) {
                $this->addError($attr, 'Доступ запрещен к школе');
                return;
            }

            $this->request = $request;
        }
    }

    /**
     */
    public function action()
    {
        $t = $this->request->addMessageToClient($this->text);

        // отправляю письмо

//        $shop->mail($request->getUser()->getEmail(), 'Заказ #'.$request->getId().'. Новое сообщение', 'shop/request_to_client_message', [
//            'request' => $request,
//            'text'    => $text,
//        ]);

        return $t;
    }
}
