<?php

namespace avatar\models\validate;

use common\components\providers\ETH;
use common\models\avatar\Currency;
use common\models\avatar\UserBill;
use common\models\avatar\UserBillOperation;
use common\models\PassportLink;
use common\models\RequestTokenCreate;
use common\models\school\AdminLink;
use common\models\school\Kurs;
use common\models\school\Potok;
use common\models\school\PotokUser3Link;
use common\models\school\School;
use common\models\school\SubscribeItem;
use common\models\school\UserLink;
use common\models\task\Helper;
use common\models\task\Session;
use common\models\task\Task;
use common\models\UserAvatar;
use common\models\UserRoot;
use common\models\UserRootExt;
use common\services\Subscribe;
use cs\Application;
use cs\services\File;
use cs\services\Str;
use cs\services\UploadFolderDispatcher;
use cs\services\Url;
use cs\services\VarDumper;
use Imagine\Image\Box;
use Yii;
use yii\base\Model;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;
use yii\helpers\StringHelper;
use yii\imagine\Image;

/**
 */
class CabinetSchoolTaskListViewAjax extends Model
{
    /** @var  int идентиифкатор пользователя который добавляется */
    public $id;

    public $task;

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            ['id', 'required'],
            ['id', 'integer'],
            ['id', 'validateTask'],
        ];
    }

    public function validateTask($attr, $params)
    {
        if (!$this->hasErrors()) {
            $task = Task::findOne($this->id);
            if (is_null($task)) {
                $this->addError($attr, 'Не найдена Задача');
                return;
            }
            $this->task = $task;

        }
    }

    /**
     * @return
     */
    public function action()
    {
        $task = Task::findOne($this->id);
        $s = School::findOne($task->school_id);

        $is_counter = 0;
        if ( $session = Session::find()->where(['task_id' => $this->id])->exists()) {
            /** @var Session $session */
            $session = Session::find()
                ->where(['task_id' => $this->id, 'is_finish' => 0])
                ->one();

            // если не найдены не закрытые сессии, то все закрыты и счетчик остановлен
            if (is_null($session)) {
                $all = Session::find()
                    ->where([
                        'task_id' => $this->id,
                    ])
                    ->select([
                        'sum(finish - start) as ss',
                    ])
                    ->scalar();
                if ($all === false) $all = 0;
            } else {
                $all = Session::find()
                    ->where(['task_id' => $this->id, 'is_finish' => 1])
                    ->select([
                        'sum(finish - start) as ss',
                    ])
                    ->scalar();

                $all += time() - $session->start;
                $is_counter = 1;

            }
        } else {
            $all = 0;
        }

        return [
            'is_counter' => $is_counter,
            'time' => $all,
            'task' => $task,
            'html' => Yii::$app->view->renderFile('@avatar/views/cabinet-school-task-list/view-desk-item.php', ['model' => $task, 'school' => $s])
        ];
    }
}
