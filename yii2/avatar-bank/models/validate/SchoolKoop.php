<?php

namespace avatar\models\validate;

use common\components\Card;
use common\models\avatar\QrCode;
use common\models\avatar\UserBill;
use common\models\avatar\UserBillMerchant;
use common\models\SendLetter;
use cs\Application;
use cs\services\Str;
use iAvatar777\services\FormAjax\ActiveRecord;
use Yii;
use yii\base\Model;
use yii\db\Query;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\VarDumper;

/**
 */
class SchoolKoop extends ActiveRecord
{
    public static function tableName()
    {
        return 'school_koop';
    }

    public function attributeLabels()
    {
        return [
            'name'                         => 'Наименование',
            'inn'                          => 'ИНН',
            'address_isp'                  => 'Адрес исп.органа',
            'phone'                        => 'Номер телефона',
            'email'                        => 'Адрес эл.почты',
            'registration_is_video_verify' => 'Нужна видео верификация при регистрации?',
            'okved'                        => 'Основной ОКВЭД',
            'okved_dop'                    => 'Дополнительные ОКВЭД, через запятую',
            'input_amount'                 => 'Размер вступительного взноса',
        ];
    }

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            ['name', 'string'],

            ['inn', 'integer'],

            ['address_isp', 'string'],

            ['phone', 'string'],

            ['email', 'string'],
            ['email', 'email'],

            ['registration_is_video_verify', 'integer'],

            ['okved', 'string'],
            ['okved_dop', 'string'],

            ['input_amount', 'integer'],
        ];
    }

}
