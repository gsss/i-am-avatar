<?php

namespace avatar\models\validate;

use common\components\providers\ETH;
use common\models\avatar\Currency;
use common\models\avatar\UserBill;
use common\models\avatar\UserBillOperation;
use common\models\comment\Comment;
use common\models\comment\CommentList;
use common\models\PassportLink;
use common\models\RequestTokenCreate;
use common\models\school\AdminLink;
use common\models\school\Kurs;
use common\models\school\Potok;
use common\models\school\PotokUser3Link;
use common\models\school\ProjectManagerLink;
use common\models\school\School;
use common\models\school\SubscribeItem;
use common\models\school\UserLink;
use common\models\task\Helper;
use common\models\task\Session;
use common\models\task\Task;
use common\models\UserAvatar;
use common\models\UserRoot;
use common\models\UserRootExt;
use common\services\Subscribe;
use cs\services\File;
use cs\services\Str;
use cs\services\UploadFolderDispatcher;
use cs\services\VarDumper;
use Imagine\Image\Box;
use Yii;
use yii\base\Model;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;
use yii\imagine\Image;

/**
 */
class CabinetSchoolTaskListControllerViewCloseSessionStart extends Model
{
    /** @var  int идентиифкатор задачи */
    public $id;

    /** @var  Task */
    public $task;

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            ['id', 'required'],
            ['id', 'integer'],
            ['id', 'validateTask'],
        ];
    }

    public function validateTask($attr, $params)
    {
        if (!$this->hasErrors()) {
            $task = Task::findOne($this->id);
            if (is_null($task)) {
                $this->addError($attr, 'Не найдена Задача');
                return;
            }
            $this->task = $task;
        }
    }

    /**
     * @return \common\models\comment\Comment
     */
    public function action()
    {
        $task = $this->task;
        $statusList = \common\models\task\Status::find()
            ->where(['school_id' => $task->school_id])
            ->orderBy(['sort_index' => SORT_ASC])
            ->select('id')
            ->column();

        $task->status = $statusList[0];
        $task->executer_id = Yii::$app->user->id;
        $task->save();
        $list = CommentList::get(1, $task->id);

        $comment = Comment::add([
            'text'    => 'Я сделаю это!',
            'list_id' => $list->id,
            'user_id' => Yii::$app->user->id,
        ]);

        // Начинаю новую сессию
        Session::add(['task_id' => $task->id, 'start' => time()]);

        // Сообщаю PM что кто-то взял задачу
        $pmList = ProjectManagerLink::find()->where(['school_id' => $task->school_id])->select('user_id')->column();
        $pmListString = \common\models\UserAvatar::find()->where(['in', 'id', $pmList])->select('email')->column();
        Subscribe::sendArraySchool(School::findOne($task->school_id), $pmListString, 'Исполнитель взял задачу', 'task/start', ['task' => $task], 'layouts/html/task');
        $task->trigger(Task::EVENT_TASK_START);

        return $comment;
    }
}
