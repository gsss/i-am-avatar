<?php
/**
 * Created by PhpStorm.
 * User: s.arhangelskiy
 * Date: 31.07.2017
 * Time: 10:50
 */

namespace avatar\models\validate;


use common\models\avatar\Currency;
use common\models\avatar\UserBill;
use common\models\BlogItem;
use common\models\school\SubscribeItem;
use common\models\Token;
use common\models\UserRoot;
use cs\services\VarDumper;

class AdminBlogControllerSubscribe extends \yii\base\Model
{
    public $id;

    public function rules()
    {
        return [
            ['id', 'required'],
            ['id', 'integer'],
            ['id', 'exist', 'targetClass' => '\common\models\BlogItem'],
        ];
    }

    /**
     * @return \common\models\school\SubscribeItem
     */
    public function action()
    {
        $item = \common\models\BlogItem::findOne($this->id);

        // добавляю в рассылку потока
        $Potok = \common\models\school\Potok::findOne(2);
        $Subscribe = \common\models\school\Subscribe::add([
            'potok_id' => $Potok->id,
            'subject'  => $item->name,
            'content'  => \Yii::$app->view->renderFile('@avatar/mail/html/subscribe/blog-item.php', [
                'newsItem' => $item,
            ]),
        ]);
        $model = new \avatar\models\validate\CabinetSchoolSubscribeControllerSubscribe(['id' => $Subscribe->id]);
        $model->validate();
        $item->is_send = 1;
        $item->save();

        return $model->action();
    }
}