<?php

namespace avatar\models\validate;

use common\components\providers\ETH;
use common\models\avatar\Currency;
use common\models\avatar\UserBill;
use common\models\avatar\UserBillOperation;
use common\models\PassportLink;
use common\models\RequestTokenCreate;
use common\models\school\AdminLink;
use common\models\school\Kurs;
use common\models\school\Potok;
use common\models\school\PotokUser3Link;
use common\models\school\School;
use common\models\school\SubscribeItem;
use common\models\shop\Request;
use common\models\UserAvatar;
use common\models\UserRoot;
use common\models\UserRootExt;
use cs\Application;
use cs\services\File;
use cs\services\Str;
use cs\services\UploadFolderDispatcher;
use cs\services\VarDumper;
use Imagine\Image\Box;
use Yii;
use yii\base\Model;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;
use yii\helpers\StringHelper;
use yii\imagine\Image;

/**
 */
class CabinetSchoolDnsReject extends Model
{
    /** @var int идентификатор школы */
    public $id;

    /** @var  School */
    public $school;

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            ['id', 'required'],
            ['id', 'integer'],
            ['id', 'validateSchool'],
        ];
    }

    public function validateSchool($attr, $params)
    {
        if (!$this->hasErrors()) {
            $s = School::findOne($this->id);
            if (is_null($s)) {
                $this->addError($attr, 'Школа не найдена');
                return;
            }

            $this->school = $s;
        }
    }

    /**
     *
     * @url https://api.cloudflare.com/#dns-records-for-a-zone-delete-dns-record
     */
    public function action()
    {
        if (!StringHelper::startsWith($this->school->dns, 'http')) {
            $this->deleteFromCloudFlare($this->school);
        }

        // Удаляю ссылку в школе
        $this->school->dns = '';
        $this->school->save();

        return ['status' => 1];
    }

    /**
     * @param \common\models\school\School $s
     *
     * @return bool
     */
    public function deleteFromCloudFlare($s)
    {
        $zone = '061a8247a405dcf405ebbbf7544e359c';

        /** @var \common\services\CloudFlare $CloudFlare */
        $CloudFlare = Yii::$app->CloudFlare;

        // Ищу ID
        $response = $CloudFlare->_get('zones/' . $zone . '/dns_records', ['name' => $s->dns.'.i-am-avatar.com']);
        $data = Json::decode($response->content);
        if (count($data['result']) == 1) {
            $id = $data['result'][0]['id'];

            // Посылаю запрос на удаление
            $response = $CloudFlare->_delete('zones/' . $zone . '/dns_records/' . $id);
            $data = Json::decode($response->content);
            if (isset($data['result']['id'])) {
                return true;
            }
        }

        return false;
    }
}
