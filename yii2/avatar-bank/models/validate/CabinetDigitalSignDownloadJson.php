<?php

namespace avatar\models\validate;

use common\components\providers\ETH;
use common\models\avatar\Currency;
use common\models\avatar\UserBill;
use common\models\avatar\UserBillOperation;
use common\models\PassportLink;
use common\models\PaySystem;
use common\models\RequestTokenCreate;
use common\models\school\AdminLink;
use common\models\UserAvatar;
use common\models\UserDigitalSign;
use cs\Application;
use cs\services\File;
use cs\services\Str;
use cs\services\UploadFolderDispatcher;
use cs\services\Url;
use cs\services\VarDumper;
use Imagine\Image\Box;
use Yii;
use yii\base\Model;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;
use yii\imagine\Image;
use BitcoinPHP\BitcoinECDSA\BitcoinECDSA;

/**
 *
 */
class CabinetDigitalSignDownloadJson extends Model
{
    public $wif;

    /** @var \BitcoinPHP\BitcoinECDSA\BitcoinECDSA */
    public $bitcoinECDSA;

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            ['wif', 'required'],
            ['wif', 'string'],
            ['wif', 'validateWif'],
        ];
    }

    public function validateWif($attribute, $params)
    {
        if (!$this->hasErrors()) {
            $bitcoinECDSA = new BitcoinECDSA();
            $this->bitcoinECDSA = $bitcoinECDSA;
            if (!$bitcoinECDSA->validateWifKey($this->wif)) {
                $this->addError($attribute, 'Не верный код');
                return;
            }
        }
    }

    /**
     * Если удачно будет удаление на сервере то удаляет локально в таблице school_file
     *
     * Иначе вызываю исключение
     */
    public function action()
    {
        $bitcoinECDSA = $this->bitcoinECDSA;
        $bitcoinECDSA->setPrivateKeyWithWif($this->wif);

        $address = $bitcoinECDSA->getAddress();
        $PrivateKey = $bitcoinECDSA->getPrivateKey();
        $getUncompressedAddress = $bitcoinECDSA->getUncompressedAddress();
        $getUncompressedPubKey = $bitcoinECDSA->getUncompressedPubKey();

        $data = [
            'version' => '1.0.0',
            'type'    => 'avatar-id-key',
            'data'    => [
                'address'             => $address,
                'PrivateKey'          => $PrivateKey,
                'UncompressedAddress' => $getUncompressedAddress,
                'UncompressedPubKey'  => $getUncompressedPubKey,
                'wif'                 => $this->wif,
            ],
        ];

        return Yii::$app->response->sendContentAsFile(Json::encode($data), 'AVATAR_' . $address . '.' . 'json', ['mimeType' => 'application/json']);
    }
}
