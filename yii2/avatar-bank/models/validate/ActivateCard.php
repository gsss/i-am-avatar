<?php

namespace avatar\models\validate;

use common\components\Card;
use common\models\avatar\QrCode;
use common\models\avatar\UserBill;
use common\models\avatar\UserBillMerchant;
use common\models\SendLetter;
use cs\Application;
use cs\services\Str;
use Yii;
use yii\base\Model;
use yii\db\Query;
use yii\helpers\ArrayHelper;
use yii\helpers\VarDumper;

/**
 * \avatar\controllers\CabinetBillsController::actionActivateCard
 */
class ActivateCard extends Model
{
    /** @var  string номер карты */
    public $card_number;

    /** @var  string Название карты */
    public $name;

    /** @var  \common\models\avatar\UserBill */
    public $billing;

    /** @var  \common\models\Card */
    public $card;

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            [['card_number', 'name'], 'required'],

            ['card_number', 'string'],
            ['card_number', 'normalize'],
            ['card_number', 'validateCardNumber'],
            ['card_number', 'joinBilling'],

            ['name', 'string', 'min' => 3, 'max' => 100],
        ];
    }

    /**
     * Проверяет номер карты на валидность
     * Как входное значене уже приходит номер без пробелов
     *
     * @param $attribute
     * @param $params
     */
    public function validateCardNumber($attribute, $params)
    {
        if (!$this->hasErrors()) {
            $card_number = $this->card_number;
            if (strlen($card_number) == 16) {
                if (preg_match('/\D/',$card_number) != 0) {
                    $this->addError($attribute, 'В номере карты есть запрещенные символы');
                    return;
                }
                if (!Card::checkSum($card_number)) {
                    $this->addError($attribute, 'Контрольная сумма в номере карты не совпадает');
                    return;
                }
            } else {
                $this->addError($attribute, 'В номере карты должно быть 16 цифр');
                return;
            }
        }
    }

    public function joinBilling($attribute, $params)
    {
        if (!$this->hasErrors()) {
            try {
                $card = \common\models\Card::findOne(['number' => $this->card_number]);
            } catch (\Exception $e) {
                $this->addError($attribute, Yii::t('c.RGQ3VcUnI0', 'Карта не найдена'));
                return;
            }
            $this->card = $card;
        }
    }

    public function normalize($attribute, $params)
    {
        if (!$this->hasErrors()) {
            $this->card_number = str_replace(' ', '', $this->card_number);
        }
    }

    /**
     * Производит активацию карты
     *
     * @return null
     * @throws
     */
    public function action()
    {
        (new Query())->createCommand(UserBill::getDb())->update(UserBill::tableName(), ['user_id' => Yii::$app->user->id], ['card_id' => $this->card->id])->execute();
        (new Query())->createCommand(UserBill::getDb())->update(UserBill::tableName(), ['name' => $this->name], ['card_id' => $this->card->id])->execute();

        $codeObject = \common\models\avatar\QrCode::findOne(['card_id' => $this->card->id]);
        $codeObject->is_used = 1;
        $codeObject->save();

        return null;
    }
}
