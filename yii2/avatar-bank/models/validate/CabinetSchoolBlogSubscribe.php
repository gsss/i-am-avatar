<?php

namespace avatar\models\validate;

use avatar\models\forms\school\Article;
use common\components\providers\ETH;
use common\models\avatar\Currency;
use common\models\avatar\UserBill;
use common\models\avatar\UserBillOperation;
use common\models\BlogItem;
use common\models\PassportLink;
use common\models\RequestTokenCreate;
use common\models\school\AdminLink;
use common\models\school\Kurs;
use common\models\school\Potok;
use common\models\school\PotokUser3Link;
use common\models\school\School;
use common\models\school\SubscribeItem;
use common\models\shop\Request;
use common\models\UserAvatar;
use common\models\UserDocument;
use common\models\UserRoot;
use common\models\UserRootExt;
use common\services\Security;
use cs\Application;
use cs\services\File;
use cs\services\Str;
use cs\services\UploadFolderDispatcher;
use cs\services\Url;
use cs\services\VarDumper;
use Imagine\Image\Box;
use Yii;
use yii\base\Model;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;
use yii\helpers\StringHelper;
use yii\imagine\Image;

/**
 */
class CabinetSchoolBlogSubscribe extends Model
{
    /** @var int идентификатор статьи блога */
    public $id;

    /** @var int идентификатор потока */
    public $potok_id;

    /** @var  School */
    public $school;

    /** @var \common\models\blog\Article */
    public $article;

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            ['id', 'required'],
            ['id', 'integer'],
            ['id', 'validateSchool'],

            ['potok_id', 'required'],
            ['potok_id', 'integer'],
        ];
    }

    public function validateSchool($attr, $params)
    {
        if (!$this->hasErrors()) {
            $article = \common\models\blog\Article::findOne($this->id);
            if (is_null($article)) {
                $this->addError($attr, 'Школа не найдена');
                return;
            }
            $s = School::findOne($article->school_id);

            $this->school = $s;
            $this->article = $article;
        }
    }

    /**
     *
     */
    public function save()
    {
        $item = $this->article;

        // добавляю в рассылку потока
        $Potok = \common\models\school\Potok::findOne($this->potok_id);
        $Subscribe = \common\models\school\Subscribe::add([
            'potok_id' => $Potok->id,
            'subject'  => $item->name,
            'content'  => \Yii::$app->view->renderFile('@avatar/mail/html/subscribe/blog-item.php', [
                'newsItem' => $item,
            ]),
        ]);

        // добавляю текст рассылки в очередь
        $model = new \avatar\models\validate\CabinetSchoolSubscribeControllerSubscribe(['id' => $Subscribe->id]);
        $model->validate();
        $item->is_subscribe = 1;
        $item->save();

        return $model->action();
    }
}
