<?php

namespace avatar\models\validate;

use common\components\providers\ETH;
use common\models\avatar\Currency;
use common\models\avatar\UserBill;
use common\models\avatar\UserBillOperation;
use common\models\Card;
use common\models\CardSchoolLink;
use common\models\PassportLink;
use common\models\PaySystem;
use common\models\RequestTokenCreate;
use common\models\school\School;
use common\models\UserAvatar;
use cs\services\File;
use cs\services\Str;
use cs\services\UploadFolderDispatcher;
use cs\services\VarDumper;
use Imagine\Image\Box;
use Yii;
use yii\base\Model;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;
use yii\imagine\Image;

/**
 *
 */
class CabinetCardsControllerActionAddCard extends Model
{
    /** @var string */
    public $number;

    /** @var  \common\models\Card */
    public $card;

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            ['number', 'required', 'message' => Yii::t('c.RGQ3VcUnI0', 'Это поле должно быть заполнено обязательно')],
            ['number', 'trim'],
            ['number', 'string', 'max' => 20, 'min' => 16],
            ['number', 'normalize'],
            ['number', 'validateCardNumber'],
            ['number', 'joinBilling'],
        ];
    }

    public function normalize($attribute, $params)
    {
        if (!$this->hasErrors()) {
            $this->number = str_replace(' ', '', $this->number);
        }
    }

    /**
     * Проверяет номер карты на валидность
     * Как входное значене уже приходит номер без пробелов
     *
     * @param $attribute
     * @param $params
     */
    public function validateCardNumber($attribute, $params)
    {
        if (!$this->hasErrors()) {
            $card_number = $this->number;
            if (strlen($card_number) != 16) {
                $this->addError($attribute, Yii::t('c.RGQ3VcUnI0', 'В номере карты должно быть 16 цифр'));
                return;
            }
            if (preg_match('/\D/', $card_number) != 0) {
                $this->addError($attribute, Yii::t('c.RGQ3VcUnI0', 'В номере карты должны быть только цифры'));
                return;
            }
            if (!\common\components\Card::checkSum($card_number)) {
                $this->addError($attribute, Yii::t('c.RGQ3VcUnI0', 'Контрольная сумма в номере карты не совпадает'));
                return;
            }
        }
    }

    public function joinBilling($attribute, $params)
    {
        if (!$this->hasErrors()) {
            $card = \common\models\Card::findOne(['number' => $this->number]);
            if (is_null($card)) {
                $this->addError($attribute, Yii::t('c.RGQ3VcUnI0', 'Карта не найдена'));
                return;
            }
            $this->card = $card;

            // проверка: может карта уже присоединена комуто?
            if (!empty($card->user_id)) {
                $this->addError($attribute, 'Карта уже принадлежит кому-то');
                return;
            }
        }
    }

    /**
     */
    public function action()
    {
        // присоединяю владельца карты
        $this->card->user_id = Yii::$app->user->id;
        $this->card->save();

        // установка счета по умолчанию
        // выбираю все счета карты
        /** @var \common\models\avatar\UserBill $bill */
        foreach (UserBill::find()->where(['card_id' => $this->card->id])->all() as $bill) {
            // Отсутствует счет такойже валюты уже?
            if (!UserBill::find()->where(['currency' => $bill->currency, 'user_id' => Yii::$app->user->id])->exists()) {
                $bill->is_default = 1;
                $bill->save();
            }
        }

        // присоединяю счета карты
        UserBill::updateAll(['user_id' => Yii::$app->user->id], ['card_id' => $this->card->id]);

        // присоединяю карту к школе
        if (!School::isRoot()) {
            $s = School::get();
            CardSchoolLink::add(['card_id' => $this->card->id, 'school_id' => $s->id]);
        }
    }

    /**
     * Конвертирует массив ошибок от формы чтобы их можно было выдать через JSON
     *
     * @param array $params
     *      [
     *      'amount' => ['Не верное число',...],
     *      ]
     *
     * @return array
     * [
     *      'name'  => 'amount',
     *      'value' => ['Не верное число',...],
     * ]
     */
    public function getErrors2()
    {
        $ret = [];
        foreach ($this->errors as $name => $arr) {
            $ret[] = [
                'name'  => $name,
                'value' => $arr,
            ];
        }
        return $ret;
    }

}
