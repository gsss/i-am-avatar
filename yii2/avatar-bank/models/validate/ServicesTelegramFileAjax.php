<?php

namespace avatar\models\validate;

use common\components\providers\ETH;
use common\models\avatar\Currency;
use common\models\avatar\UserBill;
use common\models\avatar\UserBillOperation;
use common\models\PassportLink;
use common\models\RequestTokenCreate;
use cs\services\File;
use cs\services\Str;
use cs\services\UploadFolderDispatcher;
use cs\services\VarDumper;
use Imagine\Image\Box;
use Yii;
use yii\base\Model;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;
use yii\imagine\Image;

/**
 */
class ServicesTelegramFileAjax extends Model
{
    public $file_id;

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            ['file_id', 'required'],
            ['file_id', 'trim'],
            ['file_id', 'string'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'file_id'     => 'file_id',
        ];
    }

    /**
     * Производит регистрацию контракта
     *
     * @return array
     * [
     *      'contract' - код контракта
     *      'id'       - идентификатор заявки
     * ]
     */
    public function action()
    {
        /** @var \aki\telegram\Telegram $telegram */
        $telegram = Yii::$app->telegram;

        $f = $telegram->getFile(['file_id' => $this->file_id]);
        VarDumper::dump($f);

        return $f;
    }


}
