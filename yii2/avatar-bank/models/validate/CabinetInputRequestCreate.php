<?php

namespace avatar\models\validate;

use common\components\providers\ETH;
use common\models\avatar\Currency;
use common\models\avatar\UserBill;
use common\models\avatar\UserBillOperation;
use common\models\BillingMain;
use common\models\BillingMainClass;
use common\models\PassportLink;
use common\models\PaySystem;
use common\models\PaySystemConfig;
use common\models\RequestTokenCreate;
use common\models\school\AdminLink;
use common\models\school\RequestInput;
use common\models\UserAvatar;
use cs\Application;
use cs\services\File;
use cs\services\Str;
use cs\services\UploadFolderDispatcher;
use cs\services\Url;
use cs\services\VarDumper;
use Imagine\Image\Box;
use Yii;
use yii\base\Model;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;
use yii\imagine\Image;

/**
 *
 */
class CabinetInputRequestCreate extends Model
{
    /** @var int */
    public $amount;

    /** @var int */
    public $ps_config_id;

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            ['amount', 'required'],
            ['amount', 'double'],

            ['ps_config_id', 'integer'],
            ['ps_config_id', 'required'],
        ];
    }

    /**
     * Если удачно будет удаление на сервере то удаляет локально в таблице school_file
     *
     * Иначе вызываю исключение
     */
    public function action()
    {
        $amountNormalize = (int)($this->amount * 100);
        $PayConfig = PaySystemConfig::findOne($this->ps_config_id);

        $class = BillingMainClass::findOne(['name' => '\common\models\school\RequestInput']);
        if (is_null($class)) {
            $class = BillingMainClass::add(['name' => '\common\models\school\RequestInput']);
        }
        $currency_id = 6;

        $billing = BillingMain::add([
            'sum_before'  => $amountNormalize,
            'sum_after'   => $amountNormalize,
            'source_id'   => $PayConfig->paysystem_id,
            'currency_id' => $currency_id,
            'class_id'    => $class->id,
            'config_id'   => $this->ps_config_id,
        ]);

        $r = RequestInput::add([
            'user_id'     => Yii::$app->user->id,
            'amount'      => $amountNormalize,
            'billing_id'  => $billing->id,
            'currency_id' => $currency_id,
        ]);

        return $r;
    }
}
