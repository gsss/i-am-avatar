<?php

namespace avatar\models\validate;

use common\components\providers\ETH;
use common\models\avatar\Currency;
use common\models\avatar\UserBill;
use common\models\UserSeed;
use common\services\Security;
use common\services\Security\AES;
use Yii;
use yii\base\Model;
use yii\helpers\VarDumper;

/**
 */
class CabinetBillsControllerActionPasswordType2 extends Model
{
    public $password;

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            ['password', 'string'],
            ['password', 'validatePassword'],
        ];
    }

    /**
     * @param $attribute
     * @param $params
     */
    public function validatePassword($attribute, $params)
    {
        if (!$this->hasErrors()) {
            /** @var \common\models\UserAvatar $user */
            $user = Yii::$app->user->identity;
            if (!$user->validatePassword($this->password)) {
                $this->addError($attribute, 'Пароль от кабинета не верный');
                return;
            }
        }
    }

    /**
     * Производит шифрование кошельков
     *
     * @return array
     * [
     *      bool,
     *      $seeds,
     *      $seedsHashObject
     * ]
     */
    public function action()
    {
        $billingList = UserBill::find()
            ->where([
                'user_id' => Yii::$app->user->id,
            ])
            ->andWhere(['not', ['currency' => Currency::BTC]])
            ->andWhere([
                'or',
                ['password_type' => null],
                ['password_type' => 1],
            ])
            ->all()
        ;

        /** @var \common\models\avatar\UserBill $billing */
        foreach ($billingList as $billing) {
            $passwordHash = UserBill::passwordEnСript($billing->password, $this->password);
            Yii::info(VarDumper::dumpAsString([$billing->password,$billing->address, $passwordHash, $this->password]), 'avatar\models\validate\CabinetBillsControllerActionPasswordType2::action:1');
            $billing->password = $passwordHash;
            $billing->password_type = UserBill::PASSWORD_TYPE_HIDE_CABINET;
            $billing->save();
        }

        $is_seeds_exist = 0;
        if (UserSeed::has()) {
            $is_seeds_exist = 1;
            return [1];
        } else {
            $userCabinet = $this->password;

            // генерирует SEEDs и сохраняет
            $seeds = Security::getSeeds();
            $sha256 = hash('sha256', $seeds);
            $key32 = substr($sha256, 0, 32);
            $hash = Security\AES::encrypt256CBC($userCabinet, $key32);
            $seedsHashObject = UserSeed::add($hash);

            return [0, $seeds, $seedsHashObject];
        }
    }
}
