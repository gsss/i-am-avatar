<?php

namespace avatar\models\validate;

use common\components\providers\ETH;
use common\models\avatar\Currency;
use common\models\avatar\UserBill;
use common\models\avatar\UserBillOperation;
use common\models\PassportLink;
use common\models\RequestTokenCreate;
use common\models\school\AdminLink;
use common\models\school\Cloud;
use common\models\school\Kurs;
use common\models\school\Potok;
use common\models\school\PotokUser3Link;
use common\models\school\School;
use common\models\school\SubscribeItem;
use common\models\shop\Request;
use common\models\UserAvatar;
use common\models\UserRoot;
use common\models\UserRootExt;
use cs\Application;
use cs\services\File;
use cs\services\Str;
use cs\services\UploadFolderDispatcher;
use cs\services\VarDumper;
use Imagine\Image\Box;
use Yii;
use yii\base\Model;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;
use yii\helpers\StringHelper;
use yii\imagine\Image;

/**
 */
class CabinetSchoolFilesCloudSave extends \iAvatar777\services\FormAjax\Model
{
    /** @var int идентификатор школы */
    public $id;
    public $url;
    public $key;

    /** @var  School */
    public $school;

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            ['id', 'required'],
            ['id', 'integer'],
            ['id', 'validateSchool'],
            ['url', 'required'],

            ['url', 'string'],
            ['url', 'url'],

            ['key', 'required'],
            ['key', 'string'],
        ];
    }

    public function validateSchool($attr, $params)
    {
        if (!$this->hasErrors()) {
            $s = School::findOne($this->id);
            if (is_null($s)) {
                $this->addError($attr, 'Школа не найдена');
                return;
            }

            $this->school = $s;
        }
    }

    public function save($runValidation = true, $attributeNames = null)
    {
        $item = Cloud::findOne(['school_id' => $this->id, 'is_active' => 1]);
        if (is_null($item)) {
            $item = Cloud::add([
                'school_id' => $this->school->id,
                'url'       => $this->url,
                'key'       => $this->key,
                'is_active' => 1,
            ]);
        } else {
            $item->url = $this->url;
            $item->key = $this->key;
            $item->save();
        }
    }

}
