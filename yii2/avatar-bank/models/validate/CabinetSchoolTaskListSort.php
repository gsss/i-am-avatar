<?php

namespace avatar\models\validate;

use common\components\providers\ETH;
use common\models\avatar\Currency;
use common\models\avatar\UserBill;
use common\models\avatar\UserBillOperation;
use common\models\PassportLink;
use common\models\RequestTokenCreate;
use common\models\school\AdminLink;
use common\models\school\Kurs;
use common\models\school\Potok;
use common\models\school\PotokUser3Link;
use common\models\school\School;
use common\models\school\SubscribeItem;
use common\models\school\UserLink;
use common\models\task\Helper;
use common\models\task\Task;
use common\models\UserAvatar;
use common\models\UserRoot;
use common\models\UserRootExt;
use common\services\Subscribe;
use cs\Application;
use cs\services\File;
use cs\services\Str;
use cs\services\UploadFolderDispatcher;
use cs\services\Url;
use cs\services\VarDumper;
use Imagine\Image\Box;
use Yii;
use yii\base\Model;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;
use yii\helpers\StringHelper;
use yii\imagine\Image;

/**
 */
class CabinetSchoolTaskListSort extends Model
{
    public $school_id;
    public $list_id;
    public $list;
    public $task_id;

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            ['task_id', 'required'],
            ['task_id', 'integer'],

            ['school_id', 'required'],
            ['school_id', 'integer'],

            ['list_id', 'required'],
            ['list_id', 'integer'],
            ['list_id', 'validateListID'],

            ['list', 'required'],
        ];
    }

    public function validateListID($a,$p)
    {
        if (!$this->hasErrors()) {
            $task = Task::findOne($this->task_id);
            if ($task->status != $this->list_id) {
                $this->addError($a, 'Изменение статуса не допустимо');
                return;
            }
            if (!in_array($this->task_id, $this->list)) {
                $this->addError($a, 'Задача должна быть в списке');
                return;
            }
        }
    }

    /**
     * @return
     */
    public function action()
    {
        $c = 0;
        foreach ($this->list as $id) {
            $task = Task::findOne($id);
            $task->sort_index = $c;
            $task->save();
            $c++;
        }

        return 1;
    }
}
