<?php

namespace avatar\models\validate;

use common\components\providers\ETH;
use common\models\avatar\Currency;
use common\models\avatar\UserBill;
use cs\services\VarDumper;
use GuzzleHttp\Client;
use Yii;
use yii\base\Model;
use yii\helpers\Json;
use yii\helpers\StringHelper;

/**
 */
class CabinetTokenControllerNewCheck extends Model
{
    public $txid;

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            ['txid', 'required'],
            ['txid', 'string'],
        ];
    }

    /**
     * Производит проверку транзакции
     *
     * @return array
     * [
     *      bool,
     *      'addressContract'
     * ]
     *
     * если транзакция подтверждена то будет возвращено [true, address]
     * если не подтверждена то будет возвращено [false]
     */
    public function action()
    {
        $provider = new \avatar\modules\ETH\ServiceInfura();
        $data = $provider->_method('eth_getTransactionReceipt', [$this->txid]);
        if (isset($data['contractAddress'])) {
            if ($data['contractAddress'] != '') {
                return [
                    true,
                    $data['contractAddress']
                ];
            }
        }

        return [false];
    }
}
