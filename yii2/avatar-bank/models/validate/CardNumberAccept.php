<?php

namespace avatar\models\validate;

use common\models\avatar\UserBill;
use common\models\SendLetter;
use cs\Application;
use cs\services\Str;
use Yii;
use yii\base\Model;
use yii\helpers\ArrayHelper;
use yii\helpers\VarDumper;

/**
 * ContactForm is the model behind the contact form.
 */
class CardNumberAccept extends Model
{
    /** @var  string */
    public $number;

    /** @var  int */
    public $id;

    /** @var \common\models\avatar\UserBill  */
    public $billing;

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            [['number', 'id'], 'required'],
            [['number'], 'verifyNumber'],
            [['id'], 'verifyID'],
        ];
    }

    /**
     * @return array customized attribute labels
     */
    public function attributeLabels()
    {
        return [
            'number' => 'Номер',
            'id'     => 'ID',
        ];
    }

    /**
     *
     *
     * @return array customized attribute labels
     */
    public function verifyNumber($attribute, $params)
    {
        if (!$this->hasErrors()) {
            $this->number = str_replace(' ', '', $this->number);
            if (strlen($this->number) != 16) {
                $this->addError($attribute, 'Длина должна быть равна 16 цифр');
            }
        }
    }

    /**
     * @return array customized attribute labels
     */
    public function verifyID($attribute, $params)
    {
        if (!$this->hasErrors()) {
            $billing = UserBill::findOne($this->id);
            if (is_null($billing)) {
                $this->addError($attribute, 'Счет не найден');
                return;
            }
            if ($billing->user_id != Yii::$app->user->id) {
                $this->addError($attribute, 'Это не ваш счет');
                return;
            }
            $this->billing = $billing;
        }
    }

}
