<?php
/**
 * Created by PhpStorm.
 * User: s.arhangelskiy
 * Date: 31.07.2017
 * Time: 10:50
 */

namespace avatar\models\validate;



use common\models\avatar\Currency;
use common\models\avatar\UserBill;
use common\models\Token;

class ApiTokenControllerTokenNew extends \yii\base\Model
{
    /** @var  string */
    public $name;

    /** @var  string */
    public $password;

    /** @var  int */
    public $currency;

    /** @var  int */
    public $billing_id;

    /** @var  \common\models\avatar\UserBill счет $billing_id, устанавливается в validateBilling */
    public $billing;

    /** @var \common\models\Token токен соответствующий $currency, устанавливается в validateCurrency */
    public $token;

    public function rules()
    {
        return [
            [['name', 'password', 'currency', 'billing_id'], 'required'],
            ['billing_id', 'validateBilling'],
            ['password', 'validatePassword'],
            ['currency', 'validateCurrency'],
        ];
    }

    public function validateBilling($attribute, $params)
    {
        if (!$this->hasErrors()) {
            try {
                $billing = UserBill::findOne($this->billing_id);
            } catch (\Exception $e) {
                $this->addError($attribute, 'Не найден счет');
                return;
            }
            if ($billing->currency != Currency::ETH) {
                $this->addError($attribute, 'Должен быть счет ETH');
                return;
            }
            $this->billing = $billing;
        }
    }

    /**
     * @param $attribute
     * @param $params
     */
    public function validatePassword($attribute, $params)
    {
        if (!$this->hasErrors()) {
            if (!$this->billing->validatePassword($this->password)) {
                $this->addError($attribute, 'Пароль от кабинета не верный');
                return;
            }
        }
    }

    /**
     * @param $attribute
     * @param $params
     */
    public function validateCurrency($attribute, $params)
    {
        if (!$this->hasErrors()) {
            $token = Token::findOne(['currency_id' => $this->currency]);
            if (is_null($token)) {
                $this->addError($attribute, 'Не верный токен, нет его');
                return;
            }
            $this->token = $token;
        }
    }

    /**
     * @return \avatar\models\WalletToken
     */
    public function action()
    {
        return \avatar\models\WalletToken::create($this->billing, $this->token, $this->name, $this->password, \Yii::$app->user->id);
    }
}