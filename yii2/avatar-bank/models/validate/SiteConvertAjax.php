<?php
/**
 * Created by PhpStorm.
 * User: s.arhangelskiy
 * Date: 23.06.2016
 * Time: 11:48
 */

namespace avatar\models\validate;


use common\models\avatar\Currency;
use yii\base\Model;
use yii\helpers\ArrayHelper;

class SiteConvertAjax extends Model
{
    /** @var  float */
    public $value;

    /** @var  string */
    public $from;
    
    /** @var  string */
    public $to;

    private $key = 'SiteConvertAjax::convert';

    public function rules()
    {
        return [
            ['value', 'required'],
            ['value', 'trim'],
            ['value', 'double'],

            ['from', 'required'],
            ['from', 'string'],
            ['from', 'validateCurrency'],

            ['to', 'required'],
            ['to', 'string'],
            ['to', 'validateCurrency'],
        ];
    }
    
    public function validateCurrency($attribute, $params)
    {
        if (!$this->hasErrors()) {
            $currencyList = $this->getCurrencyList();
            if (!in_array($this->$attribute, $currencyList)) {
                $this->addError($attribute, 'Не варный код');
                return;
            }
        }
    }

    /**
     * @return array
     * [
     *  'RUB',
     *  'USD',
     *   ... ,
     * ]
     */
    private function getCurrencyList()
    {
        $data = \Yii::$app->session->get($this->key);
        if (is_null($data)) {
            $data = $this->_getCurrencyList();
        }

        return $data;
    }

    /**
     * @return array
     * [
     *  'RUB',
     *  'USD',
     *   ... ,
     * ]
     */
    private function _getCurrencyList()
    {
        return Currency::find()->select('code')->column();
    }


    public function action()
    {
        return Currency::convert($this->value, $this->from, $this->to);
    }
}