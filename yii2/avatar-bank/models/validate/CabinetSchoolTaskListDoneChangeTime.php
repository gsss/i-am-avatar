<?php

namespace avatar\models\validate;

use avatar\models\forms\school\CommandLink;
use common\components\providers\ETH;
use common\models\avatar\Currency;
use common\models\avatar\UserBill;
use common\models\avatar\UserBillOperation;
use common\models\PassportLink;
use common\models\RequestTokenCreate;
use common\models\school\AdminLink;
use common\models\school\Kurs;
use common\models\school\Potok;
use common\models\school\PotokUser3Link;
use common\models\school\School;
use common\models\school\SubscribeItem;
use common\models\school\UserLink;
use common\models\task\Helper;
use common\models\task\Task;
use common\models\UserAvatar;
use common\models\UserRoot;
use common\models\UserRootExt;
use cs\services\File;
use cs\services\Str;
use cs\services\UploadFolderDispatcher;
use cs\services\VarDumper;
use Imagine\Image\Box;
use Yii;
use yii\base\Model;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Json;
use yii\imagine\Image;
use common\models\BillingMain;
use common\models\comment\Comment;
use common\models\comment\CommentList;
use common\models\investment\IcoRequest;
use common\models\PaySystemConfig;
use common\models\piramida\Wallet;
use common\models\SchoolCoin;
use common\models\Token;
use common\models\UserWallet;
use common\services\Subscribe;

/**
 */
class CabinetSchoolTaskListDoneChangeTime extends Model
{
    /** @var School */
    public $school;

    /** @var  string */
    public $hour;
    public $min;
    public $id;

    /** @var  Task */
    public $task;

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            ['id', 'required'],
            ['id', 'integer'],
            ['id', 'validateTask'],

            ['hour', 'required'],
            ['hour', 'integer'],

            ['min', 'required'],
            ['min', 'integer'],
        ];
    }

    public function validateTask($attr, $params)
    {
        if (!$this->hasErrors()) {
            $task = Task::findOne($this->id);
            if (is_null($task)) {
                $this->addError($attr, 'Не найдена Задача');
                return;
            }
            $this->task = $task;
            $this->school = $task->getSchool();
        }
    }


    /**
     * @return array;
     */
    public function action()
    {
        $task = $this->task;

        $statusList = \common\models\task\Status::find()
            ->where(['school_id' => $task->school_id])
            ->orderBy(['sort_index' => SORT_ASC])
            ->select('id')
            ->column();

        $task->status = $statusList[2];
        $task->save();

        // расчитать награду за задачу
        // умножаю кол-во времени на трудочас
        /** @var \avatar\models\forms\school\CommandLink $c */
        $c = CommandLink::findOne(['user_id' => $task->executer_id, 'school_id' => $this->school->id]);
        if (!is_null($c)) {
            if (!is_null($c->hour_price)) {
                if ($c->hour_price > 0) {
                    $hour_price = \common\models\piramida\Currency::getValueFromAtom($c->hour_price, $c->hour_price_currency_id);
                    $hour_all = $this->hour + ($this->min / 60);

                    $price = $hour_price * $hour_all;
                    $sc = SchoolCoin::findOne(['school_id' => $this->school->id]);
                    if (!is_null($sc)) {
                        $task->price = \common\models\piramida\Currency::getAtomFromValue($price, $sc->currency_id);
                        $task->currency_id = $sc->currency_id;
                        $task->save();

                        \avatar\controllers\actions\CabinetSchoolTaskListController\actionViewDone::doMoney($this->task, $this->school);
                    }
                }
            }
        }


        $comment = 'Задача #' . $task->id . ' принята';

        // добавляю коментарий
        $commentObject = Comment::add([
            'text'    => $comment,
            'list_id' => CommentList::get(1, $task->id)->id,
            'user_id' => Yii::$app->user->id,
        ]);

        // Высылаю на почту исполнителю
        Subscribe::sendArraySchool(
            $task->getSchool(),
            [
                $task->getExecuter()->email,
            ],
            'Задача принята',
            'task/done',
            [
                'task' => $task,
                'text' => $comment,
            ],
            'layouts/html/task'
        );

        return 1;
    }
}
