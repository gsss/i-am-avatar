<?php

namespace avatar\models\validate;

use common\components\providers\ETH;
use common\models\avatar\Currency;
use common\models\avatar\UserBill;
use common\models\avatar\UserBillOperation;
use common\models\PassportLink;
use common\models\RequestTokenCreate;
use common\models\school\PotokUser3Link;
use common\models\UserAvatar;
use common\models\UserRoot;
use common\models\UserRootExt;
use cs\services\File;
use cs\services\Str;
use cs\services\UploadFolderDispatcher;
use cs\services\VarDumper;
use Imagine\Image\Box;
use Yii;
use yii\base\Model;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;
use yii\imagine\Image;

/**
 */
class CabinetSchoolPotokRequestControllerLid extends Model
{
    public $id;

    /** @var  PotokUser3Link */
    public $link;

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            ['id', 'required'],
            ['id', 'integer'],
            ['id', 'validateLid'],
        ];
    }

    public function validateLid($attr, $params)
    {
        if (!$this->hasErrors()) {
            $link = \common\models\school\PotokUser3Link::findOne($this->id);
            if (is_null($link)) {
                $this->addError($attr, 'Не найдена ссылка');
                return;
            }
            $this->link = $link;
        }
    }

    /**
     *
     */
    public function action()
    {
        $userRoot = UserRoot::findOne($this->link->user_root_id);
        $data = ['email' => $userRoot->email];

        if ($userRoot->avatar_status == UserRoot::STATUS_REGISTERED) {
            $u = UserAvatar::findOne(['user_root_id' => $userRoot->id]);
            $data['name'] = $u->getName2();
        } else {
            $u = UserRootExt::findOne(['user_root_id' => $userRoot->id]);
            if (!is_null($u)) {
                $data['name'] = $u->name;
                $data['phone'] = $u->phone;
            } else {
                $data['name'] = '';
                $data['phone'] = '';
            }
        }

        return $data;
    }
}
