<?php

namespace avatar\models\validate;

use common\components\providers\ETH;
use common\models\avatar\Currency;
use common\models\avatar\UserBill;
use common\models\avatar\UserBillOperation;
use common\models\PassportLink;
use common\models\RequestTokenCreate;
use common\models\school\AdminLink;
use common\models\school\Kurs;
use common\models\school\Potok;
use common\models\school\PotokUser3Link;
use common\models\school\SubscribeItem;
use common\models\UserAvatar;
use common\models\UserRoot;
use common\models\UserRootExt;
use cs\services\File;
use cs\services\Str;
use cs\services\UploadFolderDispatcher;
use cs\services\VarDumper;
use Imagine\Image\Box;
use Yii;
use yii\base\Model;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;
use yii\imagine\Image;

/**
 */
class CabinetSchoolSubscribeControllerSubscribe extends Model
{
    public $id;

    public $s;
    public $potok;
    public $school;

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            ['id', 'required'],
            ['id', 'integer'],
            ['id', 'validateLid'],
        ];
    }

    public function validateLid($attr, $params)
    {
        if (!$this->hasErrors()) {
            $original = \common\models\school\Subscribe::findOne($this->id);
            if (is_null($original)) {
                $this->addError($attr, 'Не найдена рассылка');
                return;
            }
            $this->s = $original;
            $Potok = Potok::findOne($original->potok_id);
            $Kurs = Kurs::findOne($Potok->kurs_id);
            $school = $Kurs->getSchool();
            $this->potok = $Potok;
            $this->school = $school;

            $result = AdminLink::find()->where([
                'school_id' => $school->id,
                'user_id'   => Yii::$app->user->id,
            ])->exists();

            if (!$result) {
                $this->addError($attr, 'Вам запрещен доступ к чужой школе');
                return;
            }
        }
    }

    /**
     * @return \common\models\school\SubscribeItem
     */
    public function action()
    {
        $school = $this->school;
        $original = $this->s;
        $Potok = $this->potok;

        $emailList = PotokUser3Link::find()
            ->innerJoin('user_root', 'user_root.id = school_potok_user_root_link.user_root_id')
            ->where(['school_potok_user_root_link.potok_id' => $original->potok_id])
            ->andWhere(['school_potok_user_root_link.is_unsubscribed' => 0])
            ->select(['user_root.email'])
            ->column();

        $sItem = SubscribeItem::add([
            'subscribe_id' => $original->id,
            'email_count'  => count($emailList),
        ]);

        $subscribeProcess = \common\services\Subscribe::sendArraySchool(
            $school,
            $emailList,
            $original->subject,
            'subscribe/manual2',
            [
                'item'  => $original,
                'potok' => $Potok,
                'sid'   => $sItem->id,
            ],
            'layouts/html/subscribe'
        );

        $sItem->gs_subscribe_id = $subscribeProcess->id;
        $sItem->save();

        return $sItem;
    }
}
