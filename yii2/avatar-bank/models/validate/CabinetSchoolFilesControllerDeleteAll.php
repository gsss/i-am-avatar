<?php

namespace avatar\models\validate;

use common\components\providers\ETH;
use common\models\avatar\Currency;
use common\models\avatar\UserBill;
use common\models\avatar\UserBillOperation;
use common\models\PassportLink;
use common\models\PaySystem;
use common\models\RequestTokenCreate;
use common\models\school\AdminLink;
use common\models\UserAvatar;
use cs\Application;
use cs\services\File;
use cs\services\Str;
use cs\services\UploadFolderDispatcher;
use cs\services\Url;
use cs\services\VarDumper;
use Imagine\Image\Box;
use Yii;
use yii\base\Model;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;
use yii\imagine\Image;

/**
 *
 */
class CabinetSchoolFilesControllerDeleteAll extends Model
{
    /** @var string */
    public $ids;

    /** @var  \common\models\school\File[] */
    public $fileList;

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            ['ids', 'required'],
            ['ids', 'string'],
            ['ids', 'validateFile'],
        ];
    }

    /**
     * Проверяет на наличие файла в БД
     * Проверяет на доступ к школе
     */
    public function validateFile($attribute, $params)
    {
        if (!$this->hasErrors()) {
            $ids = explode(',', $this->ids);
            foreach ($ids as $id) {
                if (!Application::isInteger($id)) {
                    $this->addError($attribute, 'Должны быть только целые числа');
                    return;
                }
                $file = \common\models\school\File::findOne($id);
                if (is_null($file)) {
                    $this->addError($attribute, 'Не такого файла #' . $id);
                    return;
                }
                $this->fileList[] = $file;
            }
       }
    }

    /**
     * Если удачно будет удаление на сервере то удаляет локально в таблице school_file
     *
     * Иначе вызываю исключение
     */
    public function action()
    {
        /** @var array $fileArray относительные пути */
        $fileArray = [];
        /** @var \common\models\school\File $file */
        foreach ($this->fileList as $file) {
            $url = new Url($file->file);
            $fileArray[] = $url->path;
        }

        /** @var \common\services\AvatarCloud $cloud */
        $cloud = Yii::$app->AvatarCloud;

        $data = $cloud->post(null, 'upload/delete-many-index', ['fileList' => $fileArray]);
        if (!$data['success']) {
            throw new \Exception(\yii\helpers\VarDumper::dumpAsString($data['data']));
        }
        \common\models\school\File::deleteAll(['in', 'id', ArrayHelper::getColumn($this->fileList,'id')]);
    }
}
