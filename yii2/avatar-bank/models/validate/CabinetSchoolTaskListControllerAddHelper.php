<?php

namespace avatar\models\validate;

use common\components\providers\ETH;
use common\models\avatar\Currency;
use common\models\avatar\UserBill;
use common\models\avatar\UserBillOperation;
use common\models\PassportLink;
use common\models\RequestTokenCreate;
use common\models\school\AdminLink;
use common\models\school\Kurs;
use common\models\school\Potok;
use common\models\school\PotokUser3Link;
use common\models\school\SubscribeItem;
use common\models\school\UserLink;
use common\models\task\Helper;
use common\models\task\Task;
use common\models\UserAvatar;
use common\models\UserRoot;
use common\models\UserRootExt;
use cs\services\File;
use cs\services\Str;
use cs\services\UploadFolderDispatcher;
use cs\services\VarDumper;
use Imagine\Image\Box;
use Yii;
use yii\base\Model;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;
use yii\imagine\Image;

/**
 */
class CabinetSchoolTaskListControllerAddHelper extends Model
{
    /** @var  int идентиифкатор пользователя который добавляется */
    public $id;

    /** @var  UserAvatar */
    public $user;

    /** @var  int идентиифкатор задачи */
    public $task_id;

    /** @var  Task */
    public $task;

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            ['id', 'required'],
            ['id', 'integer'],
            ['id', 'validateUser'],

            ['task_id', 'required'],
            ['task_id', 'integer'],
            ['task_id', 'validateTask'],

            ['id', 'validateHelperExist'], // Проверяю Есть ли такой пользователь уже в списке помощников?
            ['id', 'validateHelperExecuter'], // Проверяю Является пользователь исполнителем?
        ];
    }

    public function validateTask($attr, $params)
    {
        if (!$this->hasErrors()) {
            $task = Task::findOne($this->task_id);
            if (is_null($task)) {
                $this->addError($attr, 'Не найдена Задача');
                return;
            }
            $this->task = $task;

            $result = UserLink::find()
                ->where([
                    'school_id'    => $task->school_id,
                    'user_root_id' => $this->user->user_root_id,
                ])
                ->exists();

            if (!$result) {
                $this->addError($attr, 'Пользователя нет в школе');
                return;
            }
        }
    }

    /**
     * Проверяю Есть ли такой пользователь уже в списке помощников?
     * @param $attr
     * @param $params
     */
    public function validateHelperExist($attr, $params)
    {
        if (!$this->hasErrors()) {
            if (Helper::find()->where(['task_id' => $this->task_id, 'user_id' => $this->id])->exists()) {
                $this->addError($attr, 'Этот пользователь уже есть в помощниках');
                return;
            }
        }
    }

    /**
     * Проверяю Является пользователь исполнителем?
     * @param $attr
     * @param $params
     */
    public function validateHelperExecuter($attr, $params)
    {
        if (!$this->hasErrors()) {
            if ($this->id == $this->task->executer_id) {
                $this->addError($attr, 'Этот пользователь является исполнителем');
                return;
            }
        }
    }

    public function validateUser($attr, $params)
    {
        if (!$this->hasErrors()) {
            $user = UserAvatar::findOne($this->id);
            if (is_null($user)) {
                $this->addError($attr, 'Не найден пользователь');
                return;
            }

            $this->user = $user;
        }
    }

    /**
     * @return \common\models\task\Helper
     */
    public function action()
    {
        return Helper::add([
            'task_id'   => $this->task_id,
            'user_id'   => $this->id,
        ]);
    }
}
