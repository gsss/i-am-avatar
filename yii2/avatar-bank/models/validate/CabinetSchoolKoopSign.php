<?php

namespace avatar\models\validate;

use BitcoinPHP\BitcoinECDSA\BitcoinECDSA;
use common\components\providers\ETH;
use common\models\avatar\Currency;
use common\models\avatar\UserBill;
use common\models\avatar\UserBillOperation;
use common\models\PassportLink;
use common\models\RequestTokenCreate;
use common\models\school\AdminLink;
use common\models\school\Kurs;
use common\models\school\Potok;
use common\models\school\PotokUser3Link;
use common\models\school\School;
use common\models\school\SubscribeItem;
use common\models\school\UserLink;
use common\models\task\Helper;
use common\models\task\Task;
use common\models\UserAvatar;
use common\models\UserDigitalSign;
use common\models\UserEnter;
use common\models\UserRoot;
use common\models\UserRootExt;
use common\services\Subscribe;
use cs\services\File;
use cs\services\Str;
use cs\services\UploadFolderDispatcher;
use cs\services\VarDumper;
use Imagine\Image\Box;
use Yii;
use yii\base\Model;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;
use yii\imagine\Image;

/**
 */
class CabinetSchoolKoopSign extends \iAvatar777\services\FormAjax\Model
{
    /** @var  int идентиифкатор \avatar\models\UserEnter */
    public $id;
    public $signedMessage1;

    /**
     * @var \BitcoinPHP\BitcoinECDSA\BitcoinECDSA
     */
    public $pk1Object;

    /** @var string */
    public $hash;

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            ['id', 'required'],
            ['id', 'integer'],
            ['id', 'exist', 'targetClass' => '\avatar\models\UserEnter'],

        ];
    }

    public function action()
    {
        $pk1Object = new BitcoinECDSA();

        $pk1Object->setPrivateKey(UserDigitalSign::getPK(Yii::$app->user->id));

        $UserEnter = \avatar\models\UserEnter::findOne($this->id);
        $data = [
            $UserEnter->id,
            $UserEnter->user1_id,
            $UserEnter->user2_id,
            $UserEnter->public_key1,
            $UserEnter->public_key2,
            $UserEnter->created_at,
            $UserEnter->file,
            $UserEnter->name_first,
            $UserEnter->name_last,
            $UserEnter->name_middle,
            $UserEnter->address_pochta,
            $UserEnter->phone,
            $UserEnter->passport_ser,
            $UserEnter->passport_number,
            $UserEnter->passport_vidan_kem,
            $UserEnter->passport_vidan_date,
            $UserEnter->passport_vidan_num,
            $UserEnter->passport_born_date,
            $UserEnter->passport_born_place,
            $UserEnter->passport_scan1,
            $UserEnter->passport_scan2,
            $UserEnter->school_id,
        ];
        $message = join('', $data);
        $hash = hash('sha256', $message);
        $signedMessage1 = $pk1Object->signMessage($hash, true);

        $UserEnter = UserEnter::findOne($this->id);
        $UserEnter->hash = $hash;
        $UserEnter->user1_id = Yii::$app->user->id;
        $UserEnter->public_key1 = $pk1Object->getAddress();
        $UserEnter->signature1 = $signedMessage1;
        $UserEnter->status = UserEnter::STATUS_AFTER_SIGN1;
        $UserEnter->save();

        // Отправить уведомление пользователю user2
        $u = UserAvatar::findOne($UserEnter->user2_id);
        Subscribe::sendArraySchool(School::findOne($UserEnter->school_id), [$u->email], 'Ваша заявка одобрена', 'koop/request_enter_accept', [
            'request' => $UserEnter,
        ]);

        return ['sign' => $signedMessage1];
    }
}
