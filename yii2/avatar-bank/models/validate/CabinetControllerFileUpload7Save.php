<?php

namespace avatar\models\validate;

use common\components\providers\ETH;
use common\models\avatar\Currency;
use common\models\avatar\UserBill;
use common\models\avatar\UserBillOperation;
use common\models\Card;
use common\models\PassportLink;
use common\models\PaySystem;
use common\models\RequestTokenCreate;
use common\models\UserAvatar;
use cs\Application;
use cs\services\File;
use cs\services\Str;
use cs\services\UploadFolderDispatcher;
use cs\services\VarDumper;
use Imagine\Image\Box;
use Yii;
use yii\base\Model;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;
use yii\imagine\Image;

/**
 *
 */
class CabinetControllerFileUpload7Save extends Model
{
    /** @var string полный путь к файлу с доменом и c протоколом */
    public $file;

    /** @var int */
    public $school_id;

    /** @var int */
    public $type_id;

    /** @var int */
    public $size;


    /** @var array
     *  [
     *          crop => [
     *              url =>
     *              size =>
     *          ]
     *      ]
     */
    public $update;

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            ['file', 'required'],
            ['file', 'string'],
            ['file', 'url'],

            ['school_id', 'required'],
            ['school_id', 'integer'],

            ['type_id', 'required'],
            ['type_id', 'integer'],

            ['size', 'required'],
            ['size', 'integer'],

            ['update', 'safe'],
        ];
    }

    public function urlValidate($attribute, $params)
    {
        if (!$this->hasErrors()) {
            if (strpos($this->file, '//') !== 0) {
                $this->addError($attribute, 'Путь должен начинаться с //');
            }
        }
    }

    /**
     * @return \common\models\school\File
     */
    public function action()
    {
        if (!Application::isEmpty($this->update)) {
            /** @var array $file */
            foreach ($this->update as $index => $file) {
                $object = \common\models\school\File::add([
                    'file'      => $file['url'],
                    'school_id' => $this->school_id,
                    'type_id'   => $this->type_id,
                    'size'      => $file['size'],
                ]);
            }
        }

        return \common\models\school\File::add([
            'file'      => $this->file,
            'school_id' => $this->school_id,
            'type_id'   => $this->type_id,
            'size'      => $this->size,
            'is_main'   => 1,
        ]);
    }

}
