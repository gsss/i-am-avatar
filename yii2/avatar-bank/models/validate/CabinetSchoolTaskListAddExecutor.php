<?php

namespace avatar\models\validate;

use common\components\providers\ETH;
use common\models\avatar\Currency;
use common\models\avatar\UserBill;
use common\models\avatar\UserBillOperation;
use common\models\PassportLink;
use common\models\RequestTokenCreate;
use common\models\school\AdminLink;
use common\models\school\Kurs;
use common\models\school\Potok;
use common\models\school\PotokUser3Link;
use common\models\school\SubscribeItem;
use common\models\school\UserLink;
use common\models\task\Helper;
use common\models\task\Task;
use common\models\UserAvatar;
use common\models\UserRoot;
use common\models\UserRootExt;
use common\services\Subscribe;
use cs\services\File;
use cs\services\Str;
use cs\services\UploadFolderDispatcher;
use cs\services\VarDumper;
use Imagine\Image\Box;
use Yii;
use yii\base\Model;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;
use yii\imagine\Image;

/**
 */
class CabinetSchoolTaskListAddExecutor extends Model
{
    /** @var  int идентиифкатор пользователя который добавляется */
    public $id;

    /** @var  UserAvatar */
    public $user;

    /** @var  int идентиифкатор задачи */
    public $task_id;

    /** @var  Task */
    public $task;

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            ['id', 'required'],
            ['id', 'integer'],
            ['id', 'validateUser'],

            ['task_id', 'required'],
            ['task_id', 'integer'],
            ['task_id', 'validateTask'],
        ];
    }

    public function validateTask($attr, $params)
    {
        if (!$this->hasErrors()) {
            $task = Task::findOne($this->task_id);
            if (is_null($task)) {
                $this->addError($attr, 'Не найдена Задача');
                return;
            }
            $this->task = $task;

            $result = UserLink::find()
                ->where([
                    'school_id'    => $task->school_id,
                    'user_root_id' => $this->user->user_root_id,
                ])
                ->exists();

            if (!$result) {
                $this->addError($attr, 'Пользователя нет в школе');
                return;
            }
        }
    }

    public function validateUser($attr, $params)
    {
        if (!$this->hasErrors()) {
            $user = UserAvatar::findOne($this->id);
            if (is_null($user)) {
                $this->addError($attr, 'Не найден пользователь');
                return;
            }

            $this->user = $user;
        }
    }

    /**
     * @return
     */
    public function action()
    {
        // назначаю исполнителя
        $this->task->executer_id = $this->id;

        $school = $this->task->getSchool();
        $statusList = \common\models\task\Status::find()
            ->where(['school_id' => $school->id])
            ->orderBy(['sort_index' => SORT_ASC])
            ->select('id')
            ->column();

        // Устанавливаю статус
        $this->task->status = $statusList[0];
        $this->task->save();

        // отправляю уведомление по почте
        Subscribe::sendArraySchool($school, [$this->user->email], 'На вас назначена задача', 'task/add-executor', [
            'task' => $this->task,
            'user' => $this->user,
        ]);

        // отправляю уведомление в телеграм
        if ($this->user->telegram_chat_id) {
            /** @var \aki\telegram\Telegram $telegram */
            $telegram = Yii::$app->telegram;
            $url = \yii\helpers\Url::to(['cabinet-task-list/view', 'id' => $this->task->id], true);
            $telegram->sendMessage([
                'chat_id' => $this->user->telegram_chat_id,
                'text'    => join("\n", [
                        'На тебя назначена задача.',
                        'ID: ' . $this->task->id,
                        'Задача: ' . $this->task->name,
                        'Задачу можно посмотреть по ссылке: ' . $url,
                    ]
                ),
            ]);
        }
    }
}
