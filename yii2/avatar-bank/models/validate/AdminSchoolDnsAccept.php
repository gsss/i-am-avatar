<?php
/**
 * Created by PhpStorm.
 * User: s.arhangelskiy
 * Date: 31.07.2017
 * Time: 10:50
 */

namespace avatar\models\validate;

use common\models\avatar\Currency;
use common\models\avatar\UserBill;
use common\models\BlogItem;
use common\models\school\DnsRequest;
use common\models\school\Page;
use common\models\school\PageBlockContent;
use common\models\school\School;
use common\models\school\SubscribeItem;
use common\models\Token;
use common\models\UserRoot;
use cs\services\VarDumper;
use Yii;
use yii\helpers\Json;

class AdminSchoolDnsAccept extends \yii\base\Model
{
    /** @var int Идентификатор заявки */
    public $id;

    /** @var DnsRequest */
    public $request;

    public function rules()
    {
        return [
            ['id', 'required'],
            ['id', 'integer'],
            ['id', 'validateRequest'],
        ];
    }

    public function validateRequest($a, $p)
    {
        if (!$this->hasErrors()) {
            $request = DnsRequest::findOne($this->id);
            if (is_null($request)) {
                $this->addError($a, 'Заявка не найдена');
                return;
            }
            if (in_array($request->is_dns_verified, [-1, 1])) {
                $this->addError($a, 'Заявка уже имеет статус');
                return;
            }
            $this->request = $request;
        }
    }

    /**
     * @return \common\models\school\DnsRequest
     */
    public function action()
    {
        $request = $this->request;
        $school = $request->getSchool();

        if (YII_ENV_DEV) {
            if (strpos($request->name, '.') === false) {
                $school->dns = 'http://' . $request->name . '.' . env('CLOUD_FLARE_ZONE_NAME');
            } else {
                $school->dns = 'http://' . $request->name;
            }
        } else {
            // Создаю DNS запись
            // Если название домена это домен третьего уровня Аватара то
            if (strpos($request->name, '.') === false) {
                $zone = env('CLOUD_FLARE_ZONE_ID');

                /** @var \common\services\CloudFlare $CloudFlare */
                $CloudFlare = Yii::$app->CloudFlare;
                $d = $CloudFlare->_post('zones/' . $zone . '/dns_records', [
                    'name'    => $request->name,
                    'type'    => 'A',
                    'content' => Yii::$app->params['schoolIp'],
                ]);
                $data = $d->content;

                // Устанавливаю домен
                $school->dns = 'http://' . $request->name . '.' . env('CLOUD_FLARE_ZONE_NAME');

                // Если название домена это домен не аватара
            } else {
                // Устанавливаю домен
                // Удаляю 'www'
                $arr = explode('.', $request->name);
                if ($arr[0] == 'www') {
                    $items = [];
                    for ($i = 1; $i < count($arr); $i++) {
                        $items[] = $arr[$i];
                    }
                    $school->dns = 'http://' . join('.', $items);
                } else {
                    $school->dns = 'http://' . $request->name;
                }
            }
        }

        $school->save();

        // Устанавливаю статус
        $request->is_dns_verified = 1;
        $request->save();

        // Отправляю письмо создателю школы
        \common\services\Subscribe::sendArraySchool(
            $school,
            [$school->getUser()->email],
            'Заявка на подключение домена одобрена',
            'school/dns-accept',
            [
                'request' => $request,
            ]
        );

        // Создаю главную страницу
        $this->generateFirstPage($school);

        return $request;
    }

    /**
     * Формирует первую страницу для школы, если ее нет. Если есть страницы то ничего не будет создано.
     * Создает шапку и главную страницу.
     *
     * @param School $school
     *
     * @return int
     *            1 - уже есть страницы.
     *            0 - успешно сделано
     */
    private function generateFirstPage($school)
    {
        if (Page::find()->where(['school_id' => $school->id])->exists()) {
            return 1;
        }

        // Формирую шапку
        $pageHeader = Page::add([
            'name'      => 'Шапка',
            'school_id' => $school->id,
        ]);

        $block = PageBlockContent::add([
            'type_id' => 51, // Меню с входом
            'page_id' => $pageHeader->id,
            'content' => Json::encode([]),
        ]);

        // Формирую Главную страницу
        $page = Page::add([
            'name'      => 'Главная',
            'url'       => '/',
            'school_id' => $school->id,
            'header_id' => $pageHeader->id,
        ]);

        $block = PageBlockContent::add([
            'type_id' => 1, // Заглавная
            'page_id' => $page->id,
            'content' => Json::encode([
                'image'   => 'https://cloud1.cloud999.ru/upload/cloud/15609/46797_HR6Ov29Cgz.jpg',
                'content' => '<p class="text-center" style="padding-top: 250px;">&nbsp;</p><p class="text-center lead" style="font-size:200%;color:white;text-shadow: -1px 0 1px white, 0 -1px 1px white, 0 1px 1px white, 1px 0 1px white, 0 0 8px white, 0 0 8px white, 0 0 8px white, 2px 2px 3px black;">'.$school->name.'</p>',
            ]),
        ]);

        return 0;
    }
}