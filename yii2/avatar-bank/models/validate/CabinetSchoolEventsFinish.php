<?php

namespace avatar\models\validate;

use common\components\providers\ETH;
use common\models\avatar\Currency;
use common\models\avatar\UserBill;
use common\models\avatar\UserBillOperation;
use common\models\PassportLink;
use common\models\RequestTokenCreate;
use common\models\school\AdminLink;
use common\models\school\Event;
use common\models\school\Kurs;
use common\models\school\Lesson;
use common\models\school\LessonVebinar;
use common\models\school\Page;
use common\models\school\PageBlockContent;
use common\models\school\Potok;
use common\models\school\PotokUser3Link;
use common\models\school\School;
use common\models\school\SchoolSale;
use common\models\school\SubscribeItem;
use common\models\shop\Request;
use common\models\UserAvatar;
use common\models\UserRoot;
use common\models\UserRootExt;
use cs\Application;
use cs\services\File;
use cs\services\Str;
use cs\services\UploadFolderDispatcher;
use cs\services\Url;
use cs\services\VarDumper;
use Imagine\Image\Box;
use Yii;
use yii\base\Model;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;
use yii\imagine\Image;

/**
 */
class CabinetSchoolEventsFinish extends Model
{
    public $id;

    /** @var \common\models\school\School */
    public $school;

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            ['id', 'required'],
            ['id', 'integer'],
            ['id', 'validateSchool'],
        ];
    }

    public function validateSchool($attr, $params)
    {
        if (!$this->hasErrors()) {
            $s = School::findOne($this->id);
            if (is_null($s)) {
                $this->addError($attr, 'Школа не найдена');
                return;
            }
            $this->school = $s;
        }
    }

    /**
     */
    public function action()
    {
        /**
         * [
         * 1 => [
         * 'name' => '1'
         * 'content_short' => ''
         * 'content' => ''
         * 'image' => ''
         * ]
         * 2 => [
         * 'date_start' => '01.10.2019'
         * 'date_end' => ''
         * ]
         * 3 => [
         * 'is_online' => '0'
         * 'place' => '2'
         * 'online_url' => ''
         * ]
         * 4 => [
         * 'masters' => '9'
         * ]
         * 5 => [
         * 'fields' => '[]'
         * ]
         * 6 => [
         * 'vebinar_is_password' => '1'
         * 'vebinar_password' => '12341'
         * 'vebinar_max' => ''
         * 'vebinar_both_tickets' => '[]'
         * 'vebinar_buy_tickets' => '[]'
         * 'vebinar_tab' => '1'
         * ]
         * 7 => [
         * 'url' => 'dsa'
         * ]
         * ]
         */
        $data = Yii::$app->session->get('event');

        $school_id = $this->id;
        // записываю данные в событие
        $e = Event::add([
            'school_id' => $school_id,
            'config'    => Json::encode($data),
        ]);

        // создаю страницу
        {
            if ($data['Step2']['is_online'] == 1) {
                if ($data['Step6']['vebinar_tab'] == 1) {
                    $page = $this->createPageOnlineFree($school_id, $data);
                } else if ($data['Step6']['vebinar_tab'] == 3) {
                    $page = $this->createPageOnlinePay($school_id, $data);
                }
            } else {
                if ($data['Step6']['offline_tab'] == 1) {
                    $page = $this->createPageOfflineFree($school_id, $data);
                } else if ($data['Step6']['offline_tab'] == 3) {
                    $page = $this->createPageOfflinePay($school_id, $data);
                }
            }
        }

        // создаю курс
        {
            $kurs = Kurs::add([
                'school_id' => $school_id,
                'status'    => 1,
                'name'      => $data['Step1']['name'],
            ]);
        }

        // создаю поток
        {
            $dateEnd = null;
            if (!Application::isEmpty($data['Step3']['date_end'])) {
                $dateEnd = substr($data['Step3']['date_end'], 6) . '-' . substr($data['Step3']['date_end'], 3, 2) . '-' . substr($data['Step3']['date_end'], 0, 2);
            }
            $potok = Potok::add([
                'kurs_id'    => $kurs->id,
                'name'       => $data['Step1']['name'],
                'date_start' => substr($data['Step3']['date_start'], 6) . '-' . substr($data['Step3']['date_start'], 3, 2) . '-' . substr($data['Step3']['date_start'], 0, 2),
                'date_end'   => $dateEnd,
                'page_id'    => $page->id,
            ]);
            $e->potok_id = $potok->id;
            $e->save();
        }

        if ($data['Step2']['is_online'] == 1) {
            // Добавляю урок
            $lesson = Lesson::add([
                'name'       => 'Вебинар',
                'kurs_id'    => $kurs->id,
                'type_id'    => 6, // вебинар
                'created_at' => time(),
            ]);
            $url = $data['Step2']['online_url'];
            $urlObject = new Url($url);
            $vebinar = LessonVebinar::add([
                'video_id'  => $urlObject->getParam('v'),
                'lesson_id' => $lesson->id,
            ]);
        }


        // создаю продажи
        {
            $tickets = '[]';
            if ($data['Step2']['is_online'] == 1) {
                if (count($data['Step6']['offline_tab']) == 3) {
                    $tickets = $data['Step6']['offline_buy_tickets'];
                }
            } else {
                if (count($data['Step6']['vebinar_tab']) == 3) {
                    $tickets = $data['Step6']['vebinar_buy_tickets'];
                }
            }
            $t = Json::decode($tickets);
            // Если билетов больше 0 то
            if (count($t) > 0) {
                foreach ($t as $i) {
                    SchoolSale::add([
                        'school_id'   => $school_id,
                        'potok_id'    => $potok->id,
                        'name'        => $i['name'],
                        'currency_id' => 6, // RUB,
                        'price'       => $i['price'] * 100,
                    ]);
                }
            }
        }

        $href = $data['Step7']['url'];

        return [
            'href' => $this->school->getUrl($href),
        ];
    }

    /**
     * @param int $school_id
     * @param array $data
     */
    private function createPageOnlineFree($school_id, $data)
    {
        $page = Page::add([
            'school_id' => $school_id,
            'name'      => $data['Step1']['name'],
            'url'       => $data['Step7']['url'],
            'header_id' => -2,
        ]);

        PageBlockContent::add([
            'page_id' => $page->id,
            'type_id' => 47,
            'content' => Json::encode([
                'name' => $data['Step1']['name'],
                'date' => $data['Step3']['date_start'],
                'time' => $data['Step3']['time_start'],
                'image' => '//www.i-am-avatar.com/upload/cloud/1558619166_IHnxVpJft9.jpg'
            ]),
        ]);

        PageBlockContent::add([
            'page_id' => $page->id,
            'type_id' => 28,
            'content' => Json::encode([
                'content' => '<h1 class=\"text-center page-header\">Вебинар ведет</h1>',
            ]),
        ]);

        PageBlockContent::add([
            'page_id' => $page->id,
            'type_id' => 17,
            'content' => Json::encode([
                'imageBg' => '//www.i-am-avatar.com/upload/cloud/1554939548_ahsnc4tHmA.jpg',
                'image'   => '//www.i-am-avatar.com/upload/cloud/1550696138_CtFMbxq5SI.jpg',
                'header'  => 'Святослав Архангельский',
                'text'    => 'Святослав является медиумом новой Золотой Расы, практиком, исследователем, разработчиком и архитектором систем, моделей, проектов, матриц смыслов.',
            ]),
        ]);

        // Видео
        PageBlockContent::add([
            'page_id' => $page->id,
            'type_id' => 2,
            'content' => Json::encode([
                'id' => \avatar\services\YouTubeHelper::getID($data['Step2']['online_url']),
            ]),
        ]);

        // Форма
        PageBlockContent::add([
            'page_id' => $page->id,
            'type_id' => 10,
            'content' => Json::encode([
                'header' => 'Зарегистрируйтесь на событие',
                'button' => [
                    'text' => 'Зарегистрироваться'
                ],
                'items'  => [
                    [
                        'name'        => 'email',
                        'type'        => 'email',
                        'placeholder' => 'E-mail',
                    ],
                    [
                        'name'        => 'name',
                        'type'        => 'name',
                        'placeholder' => 'Имя',
                    ],
                    [
                        'name'        => 'phone',
                        'type'        => 'phone',
                        'placeholder' => 'Телефон',
                    ],
                ]
            ]),
        ]);

        return $page;
    }

    /**
     * @param int $school_id
     * @param array $data
     */
    private function createPageOnlinePay($school_id, $data)
    {

    }

    /**
     * @param int $school_id
     * @param array $data
     */
    private function createPageOfflineFree($school_id, $data)
    {

    }

    /**
     * @param int $school_id
     * @param array $data
     */
    private function createPageOfflinePay($school_id, $data)
    {

    }
}
