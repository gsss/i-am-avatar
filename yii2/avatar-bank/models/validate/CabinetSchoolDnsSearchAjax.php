<?php

namespace avatar\models\validate;

use common\components\providers\ETH;
use common\models\avatar\Currency;
use common\models\avatar\UserBill;
use common\models\avatar\UserBillOperation;
use common\models\PassportLink;
use common\models\piramida\Wallet;
use common\models\RequestTokenCreate;
use common\models\school\AdminLink;
use common\models\school\Kurs;
use common\models\school\Potok;
use common\models\school\PotokUser3Link;
use common\models\school\School;
use common\models\school\SubscribeItem;
use common\models\shop\Request;
use common\models\UserAvatar;
use common\models\UserRoot;
use common\models\UserRootExt;
use common\services\DynadotApi;
use cs\Application;
use cs\services\File;
use cs\services\Str;
use cs\services\UploadFolderDispatcher;
use cs\services\VarDumper;
use Yii;
use yii\base\Model;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;
use yii\helpers\StringHelper;
use yii\imagine\Image;

/**
 */
class CabinetSchoolDnsSearchAjax extends Model
{
    /** @var int идентификатор школы */
    public $id;
    public $term;

    /** @var  School */
    public $school;

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            ['id', 'required'],
            ['id', 'integer'],
            ['id', 'validateSchool'],

            ['term', 'required'],
            ['term', 'string'],
        ];
    }

    public function validateSchool($attr, $params)
    {
        if (!$this->hasErrors()) {
            $s = School::findOne($this->id);
            if (is_null($s)) {
                $this->addError($attr, 'Школа не найдена');
                return;
            }

            $this->school = $s;
        }
    }

    /**
     *
     */
    public function action()
    {
        /** @var DynadotApi $DynadotApi */
        $DynadotApi = Yii::$app->DynadotApi;
        $result = $DynadotApi->command('search', ['domain0' => $this->term, 'show_price' => 1]);
        $results = explode("\n", $result);
        $s = explode(',', $results[2]);
        if ($s[3] == 'yes') {
            $p = explode(' ', $s[4]);
            $price = -1;
            $options = [
                'result' => false
            ];
            if ($p[2] == 'USD') {
                $options['result'] = true;
                $c = Currency::findOne(['code' => 'USD']);
                $m = 2; // множитель
                $price = round($p[0] * $c->price_rub * $m, 2);
                $options['price'] = $price;
                $options['priceFormatted'] = Yii::$app->formatter->asDecimal($price, 2);

                $bill = UserBill::getBillByCurrencyAvatarProcessing(\common\models\piramida\Currency::ELXGOLD, Yii::$app->user->id);
                $wallet = Wallet::findOne($bill->address);
                $options['isElixir'] = ($wallet->amount >= $price * 100);
            }

            return $options;
        } else {
            return ['result' => false];
        }
    }
}
