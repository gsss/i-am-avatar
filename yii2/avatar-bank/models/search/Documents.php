<?php

namespace avatar\models\search;

use common\models\language\Message;
use common\services\Debugger;
use cs\services\VarDumper;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\db\Query;
use yii\helpers\Html;

/**
 */
class Documents extends Message
{
    public $id;
    public $txid;
    public $hash;
    public $created_at;

    public function rules()
    {
        return [
            [['id'], 'integer'],
            [['txid','hash','created_at'], 'string'],
            [['created_at'], 'date', 'format' => 'php:d.m.Y', 'message' => 'Дата должна быть в формате ДД.ММ.ГГГГ'],
        ];
    }

    /**
     * @param array $params
     * @param null $where
     * @return ActiveDataProvider
     */
    public function search($sort, $params, $where = null)
    {
        $query = \common\models\UserDocument::find()
            ->select([
                'count(user_documents_signatures.id) as counter',
                'user_documents.*',
            ])
            ->leftJoin('user_documents_signatures', 'user_documents_signatures.document_id = user_documents.id')
            ->groupBy('user_documents.id')
            ->asArray()
        ;
        if (!is_null($where)) $query->andWhere($where);

        $dataProvider = new ActiveDataProvider([
            'query'      => $query,
            'pagination' => [
                'pageSize' => 20,
            ],
        ]);
        if (!is_null($sort)) $dataProvider->sort = $sort;

        // load the search form data and validate
        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }
        // adjust the query by adding the filters
        if ($this->id) $query->andFilterWhere(['user_documents.id' => $this->id]);
        if ($this->hash) $query->andFilterWhere(['like', 'user_documents.hash', $this->hash]);
        if ($this->txid) $query->andFilterWhere(['like', 'user_documents.hash', $this->txid]);
        if ($this->created_at) {
            $start = substr($this->created_at, 6, 4) . '-' . substr($this->created_at, 3, 2) . '-' . substr($this->created_at, 0, 2) . ' 00:00:00';
            $end = new \DateTime($start, new \DateTimeZone(Yii::$app->timeZone));
            $start = (int)$end->format('U');
            $end = $start + (60 * 60 * 24);
            $dataProvider->query->andWhere(['between', 'user_documents.created_at', $start, $end]);
        }

        return $dataProvider;
    }
}
