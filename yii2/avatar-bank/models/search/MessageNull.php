<?php
/**
 * Created by PhpStorm.
 * User: s.arhangelskiy
 * Date: 26.04.2016
 * Time: 17:20
 */

namespace backend\models\search;

use common\models\Countries;
use common\models\User;
use common\services\Debugger;use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\db\Query;
use yii\helpers\ArrayHelper;
use yii\helpers\VarDumper;

class MessageNull extends Model
{
    public $id;
    public $translation;
    public $category;
    public $name;
    public $cid;

    public function rules()
    {
        return [
            [
               [
                   'id',
                   'translation',
                   'category',
                   'name',
                   'cid',
               ], 'safe'
            ]
        ];
    }

    /**
     * @param string $language
     * @param $sort
     * @param array | null $where
     * @param $params
     * @return ActiveDataProvider
     */
    public function search($language, $params, $where = null, $sort = null)
    {
        $ids = \common\models\language\SourceMessage::find()
            ->select([
                'source_message.id',
            ])
            ->leftJoin('message_to_delete', 'source_message.id = message_to_delete.id and message_to_delete.language=:language', [':language' => $language])
            ->innerJoin('languages_category_tree', 'languages_category_tree.code = source_message.category')
            ->where(['not', ['message_to_delete.translation' => null]])
            ->column();

        $dataProvider = new \yii\data\ActiveDataProvider([
            'query' => \common\models\language\SourceMessage::find()
                ->select([
                    'source_message.id',
                    'source_message.is_locked',
                    'source_message.is_to_delete',
                    'source_message.type',
                    'source_message.category',
                    'source_message.message',
                    'message.language',
                    'message.translation',
                    'languages_category_tree.id as cid',
                    'languages_category_tree.name',
                ])
                ->leftJoin('message', 'source_message.id = message.id and message.language=:language', [':language' => $language])
                ->innerJoin('languages_category_tree', 'languages_category_tree.code = source_message.category')
                ->where(['or', ['message.translation' => null], ['message.translation' => '']])
                ->andWhere(['not', ['in', 'source_message.id', $ids]])
                ->asArray()
        ]);
        if ($sort) {
            $dataProvider->sort = $sort;
        }
        if ($where) {
            $dataProvider->query->where($where);
        }

        // Загружаю данные
        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        // применяю фильтры
        if ($this->id) {
            $dataProvider->query->andWhere(['source_message.id' => $this->id]);
        }
        if ($this->translation) {
            $dataProvider->query->andWhere(['like', 'message.translation', $this->translation]);
        }
        if ($this->category) {
            $dataProvider->query->andWhere(['like', 'source_message.category', $this->category]);
        }
        if ($this->name) {
            $dataProvider->query->andWhere(['like', 'languages_category_tree.name', $this->name]);
        }
        if ($this->cid) {
            $dataProvider->query->andWhere(['languages_category_tree.id' => $this->cid]);
        }


        return $dataProvider;
    }
}