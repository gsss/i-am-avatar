<?php

namespace avatar\models\search;

use common\models\language\Message;
use common\services\Debugger;
use cs\services\VarDumper;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\db\Query;
use yii\helpers\Html;

/**
 */
class Page extends Message
{
    public $id;
    public $name;
    public $url;

    public function rules()
    {
        return [
            ['id', 'integer'],
            ['name', 'string'],
            ['url', 'string'],
        ];
    }

    /**
     * @param array $params
     * @param null $where
     * @return ActiveDataProvider
     */
    public function search($params, $where = null, $sort = null)
    {
        $query = \common\models\school\Page::find()
        ;
        if (!is_null($where)) $query->andWhere($where);

        $dataProvider = new ActiveDataProvider([
            'query'      => $query,
            'pagination' => [
                'pageSize' => 100,
            ],
        ]);
        if (!is_null($sort)) $dataProvider->sort = $sort;

        // load the search form data and validate
        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }
        // adjust the query by adding the filters
        if ($this->id) $query->andFilterWhere(['id' => $this->id]);
        if ($this->name) $query->andFilterWhere(['like', 'name', $this->name]);
        if ($this->url) $query->andFilterWhere(['like', 'url', $this->url]);

        return $dataProvider;
    }
}
