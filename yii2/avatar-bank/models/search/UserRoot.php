<?php

namespace avatar\models\search;

use common\models\language\Message;
use common\services\Debugger;
use cs\services\VarDumper;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\data\Sort;
use yii\db\Query;
use yii\helpers\Html;

/**
 */
class UserRoot extends Message
{
    public $email;
    public $avatar_status;
    public $id;

    public function rules()
    {
        return [
            ['email', 'string'],
            ['id', 'integer'],
            ['avatar_status', 'integer'],
        ];
    }

    /**
     * @param Sort $sort
     * @param array $params
     * @param null  $where
     *
     * @return ActiveDataProvider
     */
    public function search($sort, $params, $where = null)
    {
        $query = \common\models\UserRoot::find();
        if (!is_null($where)) $query->andWhere($where);

        $dataProvider = new ActiveDataProvider([
            'query'      => $query,
            'pagination' => [
                'pageSize' => 100,
            ],
        ]);
        if (!is_null($sort)) $dataProvider->sort = $sort;

        // load the search form data and validate
        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        // adjust the query by adding the filters
        if ($this->id) $query->andFilterWhere(['id' => $this->id]);
        if (!\cs\Application::isEmpty($this->avatar_status)) $query->andFilterWhere(['avatar_status' => $this->avatar_status]);
        if ($this->email) $query->andFilterWhere(['like', 'email', $this->email]);

        return $dataProvider;
    }
}
