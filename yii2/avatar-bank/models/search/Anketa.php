<?php
/**
 * Created by PhpStorm.
 * User: Ramha
 * Date: 25.10.2018
 * Time: 0:37
 */

namespace avatar\models\search;


use cs\Application;
use cs\services\VarDumper;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\data\Sort;
use yii\db\ActiveRecord;

class Anketa extends Model
{
    public $id;
    public $name_first;
    public $created_at;

    public function rules()
    {
        return [
            ['id', 'integer'],
            ['name_first', 'string'],
            ['created_at', 'string'],
        ];
    }

    /**
     * @param array $params
     * @param array $where
     * @param \yii\data\Sort $sort
     *
     * @return \yii\data\ActiveDataProvider
     */
    public function search(array $params, array $where = null, Sort $sort = null)
    {
        $query = \common\models\Anketa::find();
        if (!is_null($where)) $query->andWhere($where);

        $dataProvider = new ActiveDataProvider([
            'query'      => $query,
            'pagination' => [
                'pageSize' => 20,
            ],
        ]);
        if (!is_null($sort)) $dataProvider->sort = $sort;

        // загружаю данные
        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        // добавляю фильтры
        if ($this->id) {
            $query->andWhere(['id' => $this->id]);
        }
        if ($this->name_first) {
            $query->andWhere(['like', 'name_first', $this->name_first]);
        }
        if ($this->created_at) {
            $start = substr($this->created_at, 6, 4) . '-' . substr($this->created_at, 3, 2) . '-' . substr($this->created_at, 0, 2) . ' 00:00:00';
            $end = new \DateTime($start, new \DateTimeZone(\Yii::$app->timeZone));
            $start = (int)$end->format('U');
            $end = $start + (60 * 60 * 24);
            $query->andWhere(['between', 'date_created', $start, $end]);
        }

        return $dataProvider;
    }
}