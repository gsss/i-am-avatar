<?php

namespace avatar\models\search;

use avatar\models\Log;
use common\models\api\LogAvatar;
use cs\Application;
use cs\services\Str;
use cs\services\VarDumper;
use Yii;
use yii\base\Model;
use cs\Widget\FileUpload2\FileUpload;
use yii\data\ActiveDataProvider;
use yii\data\Sort;
use yii\db\ActiveQuery;
use yii\db\Query;
use yii\helpers\Html;

/**
 */
class LogSchool extends Model
{
    /** @var  string 'INFO' / 'ERROR' / 'WARNING' / 'PROFILE' */
    public $level;

    /** @var  string */
    public $category;

    /** @var  int */
    public $user_id;

    public function rules()
    {
        return [
            [['level', 'category', 'user_id'], 'default'],
            [[ 'user_id',], 'integer'],
            [['level','category'], 'string'],
        ];
    }

    /** 
     * @param array $params
     * @param array $where
     * @param \yii\data\Sort $sort
     *
     * @return \yii\data\ActiveDataProvider
     */
    public function search2(array $params, array $where = null, Sort $sort = null)
    {
        /** @var \yii\db\ActiveQuery $query */
        $query = \common\models\log\LogSchool::find();


        if (!is_null($where)) $query->andWhere($where);

        $dataProvider = new ActiveDataProvider([
            'query'      => $query,
            'pagination' => [
                'pageSize' => 20,
            ],
        ]);
        if (!is_null($sort)) $dataProvider->sort = $sort;

        // загружаю данные
        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        // добавляю фильтры
        if ($this->level) {
            switch ($this->level) {
                case 'INFO':
                    $type = \yii\log\Logger::LEVEL_INFO;
                    break;
                case 'TRACE':
                    $type = \yii\log\Logger::LEVEL_TRACE;
                    break;
                case 'ERROR':
                    $type = \yii\log\Logger::LEVEL_ERROR;
                    break;
                case 'WARNING':
                    $type = \yii\log\Logger::LEVEL_WARNING;
                    break;
                case 'PROFILE':
                    $type = \yii\log\Logger::LEVEL_PROFILE;
                    break;
                default:
                    $type = null;
                    break;
            }
            if ($type) {
                $query->andWhere(['level' => $type]);
            }
        }
        if ($this->user_id) {
            $query->andWhere(['user_id' => $this->user_id]);
        }
        if ($this->category) {
            $query->andWhere(['like', 'category', $this->category]);
        }

        return $dataProvider;
    }
}
