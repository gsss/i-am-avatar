<?php
/**
 * Created by PhpStorm.
 * User: s.arhangelskiy
 * Date: 26.04.2016
 * Time: 17:20
 */

namespace avatar\models\search;

use common\models\Countries;
use common\models\User;
use common\services\Debugger;
use cs\Application;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\db\Query;
use yii\helpers\ArrayHelper;
use yii\helpers\VarDumper;

class UserPotok extends Model
{
    public $id;
    public $email;
    public $name;
    public $phone;
    public $avatar_status;

    public function rules()
    {
        return [
            ['id', 'integer'],
            ['avatar_status', 'integer'],
            ['email', 'string'],
            ['name', 'string'],
            ['phone', 'string'],
        ];
    }

    /**
     * @param $sort
     * @param array | null $where
     * @param $params
     * @return ActiveDataProvider
     */
    public function search($params, $where = null, $sort = null)
    {
        $dataProvider = new \yii\data\ActiveDataProvider([
            'query' => \common\models\school\PotokUser3Link::find()
                ->innerJoin('user_root', 'user_root.id = school_potok_user_root_link.user_root_id')
                ->leftJoin('school_potok_user_ext', 'school_potok_user_ext.link_id = school_potok_user_root_link.id')
                ->select([
                    'school_potok_user_root_link.id',
                    'school_potok_user_root_link.is_avatar',
                    'school_potok_user_root_link.created_at',
                    'user_root.email',
                    'user_root.avatar_status',
                    'school_potok_user_ext.name',
                    'school_potok_user_ext.phone',
                ])
                ->asArray()
        ]);
        if ($sort) {
            $dataProvider->sort = $sort;
        }
        if ($where) {
            $dataProvider->query->where($where);
        }

        // Загружаю данные
        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        // применяю фильтры
        if ($this->id) {
            $dataProvider->query->andWhere(['school_potok_user_root_link.id' => $this->id]);
        }
        if (!Application::isEmpty($this->avatar_status)) {
            $dataProvider->query->andWhere(['user_root.avatar_status' => $this->avatar_status]);
        }
        if ($this->email) {
            $dataProvider->query->andWhere(['like', 'user_root.email', $this->email]);
        }
        if ($this->name) {
            $dataProvider->query->andWhere(['like', 'school_potok_user_ext.name', $this->name]);
        }
        if ($this->phone) {
            $dataProvider->query->andWhere(['like', 'school_potok_user_ext.phone', $this->phone]);
        }


        return $dataProvider;
    }
}