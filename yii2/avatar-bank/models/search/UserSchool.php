<?php
/**
 * Created by PhpStorm.
 * User: s.arhangelskiy
 * Date: 26.04.2016
 * Time: 17:20
 */

namespace avatar\models\search;

use common\models\Countries;
use common\models\User;
use common\services\Debugger;
use cs\Application;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\db\Query;
use yii\helpers\ArrayHelper;
use yii\helpers\VarDumper;

class UserSchool extends Model
{
    public $id;
    public $email;
    public $avatar_status;

    public function rules()
    {
        return [
            ['id', 'integer'],
            ['avatar_status', 'integer'],
            ['email', 'string'],
        ];
    }

    /**
     * @param $sort
     * @param array | null $where
     * @param $params
     * @return ActiveDataProvider
     */
    public function search($params, $where = null, $sort = null)
    {
        $dataProvider = new \yii\data\ActiveDataProvider([
            'query' => \common\models\UserRoot::find()
                ->innerJoin('user_school_link', 'user_school_link.user_root_id = user_root.id')
                ->select([
                    'user_root.id',
                    'user_root.email',
                    'user_root.avatar_status',
                ])
                ->asArray()
        ]);
        if ($sort) {
            $dataProvider->sort = $sort;
        }
        if ($where) {
            $dataProvider->query->where($where);
        }

        // Загружаю данные
        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        // применяю фильтры
        if ($this->id) {
            $dataProvider->query->andWhere(['user_root.id' => $this->id]);
        }
        if (!Application::isEmpty($this->avatar_status)) {
            $dataProvider->query->andWhere(['user_root.avatar_status' => $this->avatar_status]);
        }
        if ($this->email) {
            $dataProvider->query->andWhere(['like', 'user_root.email', $this->email]);
        }

        return $dataProvider;
    }
}