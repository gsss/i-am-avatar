<?php
/**
 * Created by PhpStorm.
 * User: s.arhangelskiy
 * Date: 26.04.2016
 * Time: 17:20
 */

namespace avatar\models\search;

use common\models\Countries;
use common\models\User;
use common\services\Debugger;use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\db\Query;
use yii\helpers\ArrayHelper;
use yii\helpers\VarDumper;

class StatisticCurrency extends Model
{
    public $code;
    public $title;
    public $chanche_1h;
    public $chanche_1d;
    public $chanche_1w;
    public $market_cap_usd;
    public $available_supply;
    public $total_supply;
    public $max_supply;

    public function rules()
    {
        return [
            ['code', 'string'],
            ['title', 'string'],
            ['chanche_1h', 'double'],
            ['chanche_1d', 'double'],
            ['chanche_1w', 'double'],
            ['market_cap_usd', 'double'],
            ['available_supply', 'double'],
            ['total_supply', 'double'],
            ['max_supply', 'double'],
        ];
    }

    /**
     * @param $sort
     * @param array | null $where
     * @param $params
     * @return ActiveDataProvider
     */
    public function search($params, $where = null, $sort = null)
    {
        $kurs = \common\models\avatar\Currency::convert(1, 'BTC', 'USD');
        $dataProvider = new \yii\data\ActiveDataProvider([
            'query' => \common\models\avatar\Currency::find()
                ->select([
                    '*',
                    '`day_volume_usd` / ' . $kurs . ' as `day_volume_btc`'
                ])
                ->where(['is_crypto' => 1])
                ->andWhere(['not', ['code_coinmarketcap' => '']])
                ->andWhere(['not in', 'id', [8,9]])
                ->asArray()
            ,
            'pagination' => [
                'pageSize' => 100
            ]
        ]);
        if ($sort) {
            $dataProvider->sort = $sort;
        }
        if ($where) {
            $dataProvider->query->where($where);
        }

        // Загружаю данные
        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        // применяю фильтры
        if ($this->code) {
            $dataProvider->query->andWhere(['like', 'code', $this->code]);
        }
        if ($this->title) {
            $dataProvider->query->andWhere(['like', 'title', $this->title]);
        }
        if ($this->chanche_1h) {
            $dataProvider->query->andWhere(['>', 'chanche_1h', $this->chanche_1h]);
        }
        if ($this->chanche_1d) {
            $dataProvider->query->andWhere(['>', 'chanche_1d', $this->chanche_1d]);
        }
        if ($this->chanche_1w) {
            $dataProvider->query->andWhere(['>', 'chanche_1w', $this->chanche_1w]);
        }


        return $dataProvider;
    }
}