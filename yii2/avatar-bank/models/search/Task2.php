<?php

namespace avatar\models\search;

use common\models\language\Message;
use common\services\Debugger;
use cs\Application;
use cs\services\VarDumper;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\db\Query;
use yii\helpers\Html;

/**
 */
class Task2 extends Message
{
    const EXECUTER_ID_MY = 1;
    const EXECUTER_ID_NO_EXECUTOR = 2;
    const EXECUTER_ID_YES_EXECUTOR = 3;
    const EXECUTER_ID_NOT_MY = 4;

    public static $list = [
        self::EXECUTER_ID_MY           => 'Только мои',
        self::EXECUTER_ID_NO_EXECUTOR  => 'Без исполнитля',
        self::EXECUTER_ID_YES_EXECUTOR => 'С исполнитлем',
        self::EXECUTER_ID_NOT_MY       => 'Не мои задачи',
    ];

    public static $listUserID = [
        1 => 'Я', // Я поставил задачу
    ];

    public $id;
    public $name;
    public $executer_id;
    public $user_id;
    public $price;
    public $status;
    public $school_id;

    public function rules()
    {
        return [
            ['id', 'string'],
            ['name', 'string'],
            ['executer_id', 'integer'],
            ['user_id', 'integer'],
            ['status', 'integer'],
            ['school_id', 'integer'],
            ['price', 'string'],
        ];
    }

    /**
     * @param array $params
     * @param array $statusList
     * @param null  $where
     *
     * @return ActiveDataProvider
     */
    public function search($sort, $params, $statusList, $where = null)
    {
        $query = \common\models\task\Task::find()
            ->leftJoin('school_task_status', 'school_task_status.id = school_task.status')
            ->select([
                'school_task.*',
                'school_task_status.type_id',
            ])

            ->asArray()
        ;

        if (!is_null($where)) {
            if (isset($where['school_id'])) {
                if (is_array($where['school_id'])) {
                    unset($where['school_id']);
                }
            }
            $query->andWhere($where);
        }

        $dataProvider = new ActiveDataProvider([
            'query'      => $query,
            'pagination' => [
                'pageSize' => 200,
            ],
        ]);
        if (!is_null($sort)) $dataProvider->sort = $sort;

        // load the search form data and validate
        if (!($this->load($params) && $this->validate())) {

            return $dataProvider;
        }

        // adjust the query by adding the filters
        if ($this->id) {
            // Если это строка с перечислением
            if (strpos($this->id, ',') !== false) {
                $arr = explode(',', $this->id);
                $rows = [];
                foreach ($arr as $id) {
                    if (Application::isInteger(trim($id))) {
                        $rows[] = trim($id);
                    }
                }
                $query->andFilterWhere(['in', 'school_task.id', $rows]);
            } else {
                if (Application::isInteger(trim($this->id))) {
                    $query->andFilterWhere(['school_task.id' => $this->id]);
                }
            }
        }
        if ($this->name) $query->andFilterWhere(['like', 'school_task.name', $this->name]);
        if ($this->executer_id) {
            switch ($this->executer_id) {
                case 1:
                    $query->andWhere(['executer_id' => Yii::$app->user->id]);
                    $query->andWhere(['not', ['executer_id' => null]]);
                    break;
                case 2;
                    $query->andWhere(['executer_id' => null]);
                    break;
                case 3:
                    $query->andWhere(['not', ['executer_id' => null]]);
                    break;
                case 4:
                    $query->andWhere([
                        'and',
                        ['not', ['executer_id' => Yii::$app->user->id]],
                        ['not', ['executer_id' => null]],
                    ]);
                    break;
                default:
                    break;
            }

        }
        if ($this->user_id) {
            switch ($this->user_id) {
                case 1:
                    $query->andWhere(['user_id' => Yii::$app->user->id]);
                    break;
                default:
                    break;
            }

        }
        if ($this->school_id) {
            $query->andWhere(['school_task.school_id' => $this->school_id]);
        }
        if (!Application::isEmpty($this->status)) {
            switch ($this->status) {
                case 1: // 1 => 'Показать Выполненные',
                    $query
                        ->andWhere([
                        'or',
                        ['school_task_status.type_id' => [1,2,3]],
                        ['school_task.status' => null],
                    ]);
                    break;

                case 2: // 2 => 'Скрыть Выполненные',
                    $query
                        ->andWhere([
                        'or',
                        ['school_task_status.type_id' => [1,2]],
                        ['school_task.status' => null],
                    ]);
                    break;
                default: // По умолчанию => 'Скрыть Выполненные',
                    $query
                        ->andWhere([
                        'or',
                        ['school_task_status.type_id' => [1,2]],
                        ['school_task.status' => null],
                    ]);
                    break;
            }
        } else {
            $query
                ->andWhere([
                'or',
                ['school_task_status.type_id' => [1,2]],
                ['school_task.status' => null],
            ]);
        }

        return $dataProvider;
    }
}
