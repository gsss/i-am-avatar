<?php

namespace backend\models;

use common\models\language\Message;
use common\models\language\SourceMessage;
use common\services\Debugger;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\db\Query;
use yii\helpers\Html;

/**
 */
class MessageSearch extends Message
{
    public $id;
    public $message;
    public $translation;
    public $category;

    public function rules()
    {
        return [
            ['id', 'integer'],
            [
                ['message', 'translation'], 'default'
            ],
            [
                ['message', 'translation', 'category'], 'string'
            ],
        ];
    }

    /**
     *
     *
     * @param \common\models\language\Category $category
     * @param array $params
     * @param null $where
     * @return ActiveDataProvider
     */
    public function search($category, $language, $sort, $params, $where = null)
    {
        $query = $category->getMessages()
            ->leftJoin('message', 'message.id = source_message.id and message.language = :language', [':language' => $language])
            ->select([
                'source_message.id',
                'source_message.message',
                'source_message.type',
                'source_message.category',
                'source_message.is_locked',
                'source_message.is_to_delete',
                'source_message.comment',
                'message.language',
                'message.translation',
            ])
            ->asArray()
        ;
        if (!is_null($where)) $query->andWhere($where);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort'  => $sort
        ]);

        // load the search form data and validate
        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }
        // adjust the query by adding the filters
        if ($this->id) $query->andFilterWhere(['source_message.id' => $this->id]);
        if ($this->message) $query->andFilterWhere(['like', 'source_message.message', $this->message]);
        if ($this->translation) $query->andFilterWhere(['like', 'message.translation', $this->translation]);

        return $dataProvider;
    }

    /**
     *
     *
     * @param \common\models\language\Category $category
     * @param array $params
     * @param null $where
     * @return ActiveDataProvider
     */
    public function searchAll($language, $sort, $params, $where = null)
    {
        $query = SourceMessage::find()
            ->leftJoin('message', 'message.id = source_message.id and message.language = :language', [':language' => $language])
            ->select([
                'source_message.id',
                'source_message.message',
                'source_message.type',
                'source_message.category',
                'source_message.is_locked',
                'source_message.is_to_delete',
                'source_message.comment',
                'message.language',
                'message.translation',
            ])
            ->asArray()
        ;

        if (!is_null($where)) $query->andWhere($where);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort'  => $sort
        ]);

        // load the search form data and validate
        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }
        // adjust the query by adding the filters
        if ($this->id) $query->andFilterWhere(['source_message.id' => $this->id]);
        if ($this->message) $query->andFilterWhere(['like', 'source_message.message', $this->message]);
        if ($this->category) $query->andFilterWhere(['like', 'source_message.category', $this->category]);
        if ($this->translation) $query->andFilterWhere(['like', 'message.translation', $this->translation]);

        return $dataProvider;
    }
}
