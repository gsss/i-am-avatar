<?php
/**
 * Created by PhpStorm.
 * User: Бог-Творец
 * Date: 18.11.2016
 * Time: 3:55
 */

namespace avatar\models;


use avatar\modules\ETH\ServiceEtherScan;
use avatar\modules\ETH\ServiceInfura;
use avatar\modules\ETH\ServiceSiriusB;
use common\components\providers\ETH;
use common\models\avatar\Currency;
use common\services\Security;
use common\services\UsersInCache;
use cs\services\Str;
use cs\services\VarDumper;
use Yii;
use Blocktrail\SDK\WalletInterface;
use common\models\avatar\UserBill;
use common\models\avatar\UserBillAddress;
use common\models\avatar\UserBillOperation;
use common\models\piramida\BitCoinUserAddress;
use common\payment\BitCoinBlockTrailPayment;
use yii\base\Exception;

use yii\helpers\ArrayHelper;
use yii\helpers\Json;
use yii\helpers\StringHelper;
use yii\httpclient\Client;

/**
 * Класс для кошелька ETH
 *
 * Class WalletETH
 *
 * @package avatar\models
 */
class WalletETH extends Object
{
    /** @var  \common\components\providers\ETH */
    public $provider;

    /** @var  \common\models\avatar\UserBill */
    public $billing;

    /**
     * Выдает баланс эфира на кошельке. Использует ServiceEtherScan
     *
     * @return double as string
     */
    public function getBalance()
    {
        $provider = new ServiceEtherScan();
        $responseData = $provider->getBalance($this->billing->address);

        return $responseData[0]['balance'] / pow(10, 18);
    }

    /**
     * Выдает баланс эфира на кошельке. Использует ServiceEtherScan
     *
     * @return string кол-во wei
     */
    public function getBalanceWei()
    {
        $provider = new ServiceEtherScan();
        $responseData = $provider->getBalance($this->billing->address);

        return $responseData[0]['balance'];
    }

    /**
     * Выдает баланс эфира на кошельке. Использует ServiceInfura
     *
     * @return double as string
     */
    public function getBalance2()
    {
//        $provider = new ServiceInfura();
//        $result = $provider->_method('eth_getBalance', [
//            $this->billing->address,
//            'latest',
//        ]);

        $url = 'https://mainnet.infura.io';
        if (!YII_ENV_PROD) {
            $url = 'https://ropsten.infura.io';
        }
        $client = new Client(['baseUrl' => $url]);
        $res = $client->post('eth_getBalance', Json::encode([
            'jsonrpc' => '2.0',
            'id'      => 1,
            'method'  => 'eth_getBalance',
            'params'  => [
                $this->billing->address,
                'latest',
            ],
        ]))->send();
        $data = $res->content;
        $dataArray = Json::decode($data);

        return $this->hex2dec($dataArray['result']) / pow(10, 18);
    }

    /**
     * Выдает баланс wei на кошельке. Использует ServiceInfura
     *
     * @return string
     */
    public function getBalanceWei2()
    {
//        $provider = new ServiceInfura();
//        $result = $provider->_method('eth_getBalance', [
//            $this->billing->address,
//            'latest',
//        ]);

        $url = 'https://mainnet.infura.io';
        if (!YII_ENV_PROD) {
            $url = 'https://ropsten.infura.io';
        }
        $client = new Client(['baseUrl' => $url]);
        $res = $client->post('eth_getBalance', Json::encode([
            'jsonrpc' => '2.0',
            'id'      => 1,
            'method'  => 'eth_getBalance',
            'params'  => [
                $this->billing->address,
                'latest',
            ],
        ]), [
            'Content-Type'  => 'application/json',
            'Accept'        => 'application/json',
        ])->send();
        if ($res->headers['http-code'] != 200) {
            throw new \Exception('code != 200');
        }
        $data = $res->content;
        try {
            $dataArray = Json::decode($data);
        } catch (\Exception $e) {
            VarDumper::dump([$res->headers, $data]);
            throw new \Exception($data);
        }

        return $this->hex2dec($dataArray['result']);
    }

    /**
     * @param null | string $password
     *
     * @return array
     */
    public function getPrivateJson($password)
    {
        $password = $this->billing->getPassword($password);

        /** @var \avatar\modules\ETH\ServiceSiriusB $provider */
        $provider = Yii::$app->ServiceSiriusB;
        $response = $provider->call('site/export', [
            'address'   => $this->billing->address,
            'password'  => $password,
        ], 'get');

        return $response['content'];
    }

    /**
     * Возвращает ключ JSON. Содержимое файла
     *
     * @param null | string $password
     *
     * @return array
     */
    public function getPrivateJsonLocal($password)
    {
        $address = $this->billing->address;
        $address = strtolower($address);
        $filePath = $this->getPrivateJsonLocalGetFile($address);

        return Json::decode(file_get_contents($filePath));
    }

    /**
     * @param string $address 0x
     * @return bool|null|string
     */
    private function getPrivateJsonLocalGetFile($address)
    {
        $address = substr($address, 2);
        $path = Yii::getAlias('@avatar/../k');
        $d = dir($path);

        while (($file = $d->read()) !== false){
            if (!in_array($file, ['.', '..'])) {
                if (StringHelper::endsWith($file, $address)) {
                    $d->close();
                    return Yii::getAlias('@avatar/../k/' . $file);
                }
            }
        }

        $d->close();
        return null;
    }

    /**
     * Конвертирует HEX в DEC
     *
     * @param string $value начинается с 0x например 0x61fe62a9b4f3086
     *
     * @return int
     */
    private function hex2dec($value)
    {
        $chars = Str::getChars($value);
        $multi = 1;
        $sum = 0;
        for ($i = count($chars) - 1; $i > 1; $i--) {
            $char = $chars[$i];
            switch ($char) {
                case 'a': $d = 10; break;
                case 'b': $d = 11; break;
                case 'c': $d = 12; break;
                case 'd': $d = 13; break;
                case 'e': $d = 14; break;
                case 'f': $d = 15; break;
                default: $d = (int) $char; break;
            }
            $sum += $d * $multi;
            $multi = $multi * 16;
        }

        return $sum;
    }

    /**
     * Отправляет деньги с записыванием информации какие транзакции были сделаны с комментариями
     *
     * @param array $to
     * [
     *      <На какой адрес> => <сколько>
     * ]
     * - <На какой адрес> - string | \common\models\avatar\UserBill -
     * - <сколько> - double | array - сколько BTC
     * [
     *      - amount - string | array
     *      - comment - string | array
     * ]
     * если is_array($amount)
     * [
     *      + amount - string
     *      - gasPrice - int wei
     *      - gasLimit - int
     * ]
     * если is_array($comment)
     * [
     *      - from - string
     *      - to - string
     * ]
     * если иначе предполагается
     * [
     *      - from - comment
     *      - to - comment
     * ]
     * @param null | string $password
     *
     * @return string txid
     * @throws
     * 'insufficient funds for gas * price + value'
     */
    public function pay($to, $password = null)
    {
        $password = $this->billing->getPassword($password);
        $optionsSend = [];

        foreach ($to as $destinationAddress => $amount) {
            // счет как строка
            $from = '';
            $to = '';
            if (is_array($amount)) {
                $options = $amount;
                $amount = $options['amount'];
                $comment = $options['comment'];
                if (is_array($comment)) {
                    $from = $comment['from'];
                    $to = $comment['to'];
                } else {
                    $from = $comment;
                    $to = $comment;
                }
                if (is_array($amount)) {
                    $options = $amount;
                    $amount = $options['amount'];
                    if (isset($options['gasPrice'])) $optionsSend['gasPrice'] = $options['gasPrice'];
                    if (isset($options['gasLimit'])) $optionsSend['gasLimit'] = $options['gasLimit'];
                }
            }

            Yii::trace(
                \yii\helpers\VarDumper::dumpAsString([
                    $this->billing->address,
                    $this->billing->password,
                    $destinationAddress,
                    $amount
                ]),
                'avatar\models\WalletETH::pay'
            );
            $ret = $this->provider->send(
                $this->billing->address,
                $password,
                $destinationAddress,
                $amount,
                $optionsSend
            );

            Yii::info($ret, 'avatar\models\WalletETH::pay');
            $transaction = $ret['transaction'];

            if ($from) {
                $fromOperation = UserBillOperation::add([
                    'bill_id'     => $this->billing->id,
                    'type'        => UserBillOperation::TYPE_OUT,
                    'transaction' => $transaction,
                    'message'     => $from,
                    'user_id'     => $this->billing->user_id,
                ]);
            }

            if ($to != '') {
                // ищу куда переводятся деньги
                try {
                    $billingTo = UserBill::findOne(['address' => $destinationAddress, 'currency' => Currency::ETH]);
                    if (isset($fromOperation)) {
                        $fromOperation->data = Json::encode([
                            'user_id'    => $billingTo->user_id,
                            'billing_id' => $billingTo->id,
                        ]);
                        $fromOperation->save();
                    }
                    UserBillOperation::add([
                        'bill_id'     => $billingTo->id,
                        'type'        => UserBillOperation::TYPE_IN,
                        'transaction' => $transaction,
                        'message'     => $to,
                        'user_id'     => $billingTo->user_id,
                        'data'        => Json::encode([
                            'user_id'    => \Yii::$app->user->id,
                            'billing_id' => $this->billing->id,
                        ]),
                    ]);
                } catch (\Exception $e) {
                    \Yii::warning($e->getMessage(), 'avatar/noAddressDestination');
                }
            }

            return $transaction;
        }
    }

    /**
     * Возвращает список транзакций, выводит по 20 транцакций, без комментариев
     *
     * @param int $page страница для вывода
     *
     * @return array сортируются транзакции по дате, первые - молодые
     * {
     * "blockNumber": "65204",
     * "timeStamp": "1439232889",
     * "hash": "0x98beb27135aa0a25650557005ad962919d6a278c4b3dde7f4f6a3a1e65aa746c",
     * "nonce": "0",
     * "blockHash": "0x373d339e45a701447367d7b9c7cef84aab79c2b2714271b908cda0ab3ad0849b",
     * "transactionIndex": "0",
     * "from": "0x3fb1cd2cd96c6d5c0b5eb3322d807b34482481d4",
     * "to": "0xde0b295669a9fd93d5f28d9ec85e40f4cb697bae",
     * "value": "0",
     * "gas": "122261",
     * "gasPrice": "50000000000",
     * "isError": "0",
     * "input": "0xf00d4b5d000000000000000000000000036c8cecce8d8bbf0831d840d7f29c9e3ddefa63000000000000000000000000c5a96db085dda36ffbe390f455315d30d6d3dc52",
     * "contractAddress": "",
     * "cumulativeGasUsed": "122207",
     * "gasUsed": "122207",
     * "confirmations": "3745410"
     * }
     */
    public function transactionList($page = 1)
    {
        $provider = new ServiceEtherScan();
        try {
            $data = $provider->transactionList($this->billing->address, $page);
        } catch (\Exception $e) {
            if ($e->getMessage() == 'No transactions found') {
                return [];
            }
        }

        return $data;
    }


    /**
     * Вызывает исполнение контракта
     *
     * @param string | null $password если = null, то значит пароль берется из счета и его тип = UserBill::PASSWORD_TYPE_OPEN
     *                                иначе это пароль пользователя от кабинета
     * @param string        $contract адрес
     * @param string        $abi
     * @param string        $functionName
     * @param array         $params
     * @param string        $write   'true' | 'false'
     *
     * @return string
     * @throws \Exception
     * 502 - Сервер не отвечает
     */
    public function contract($password = null, $contract, $abi, $functionName, $params = [], $write = 'true')
    {
        $user = $this->billing->address;
        $password = $this->billing->getPassword($password);

        $data = $this->provider->contract(
            $user,
            $password,
            $contract,
            $abi,
            $functionName,
            $params,
            $write
        );

        return $data;
    }

    /**
     * Вызывает исполнение контракта
     *
     * @param string | null $password
     *
     * @return string
     * @throws \Exception
     * 502 - Сервер не отвечает
     */
    public function testPassword($password)
    {
        $user = $this->billing->address;
        $password = $this->billing->getPassword($password);

        $data = $this->provider->testPassword(
            $user,
            $password
        );

        return $data;
    }

    /**
     * Вызывает исполнение контракта
     *
     * @param string | null $password если = null, то значит пароль берется из счета и его тип = UserBill::PASSWORD_TYPE_OPEN
     *                                иначе это пароль пользователя от кабинета
     * @param string        $contract код
     * @param string        $contractName
     * @param array         $params
     *
     * @return array
     * @throws \Exception
     */
    public function registerContract($password = null, $contract, $contractName, $params = [])
    {
        $user = $this->billing->address;
        $password = $this->billing->getPassword($password);

        $data = $this->provider->registerContract(
            $user,
            $password,
            $contract,
            $contractName,
            $params
        );

        $operation = UserBillOperation::add([
            'bill_id'     => $this->billing->id,
            'type'        => UserBillOperation::TYPE_OUT,
            'transaction' => $data['transactionHash'],
            'message'     => 'Регистрация контракта токена',
            'user_id'     => $this->billing->user_id,
        ]);

        return $data;
    }

    /**
     * Расчитывает регистрацию контракта
     *
     * @param string | null $password если = null, то значит пароль берется из счета и его тип = UserBill::PASSWORD_TYPE_OPEN
     *                                иначе это пароль пользователя от кабинета
     * @param string        $contract код
     * @param string        $contractName
     * @param array         $params
     *
     * @return array
     * @throws \Exception
     */
    public function calculateRegisterContract($password = null, $contract, $contractName, $params = [])
    {
        $user = $this->billing->address;
        $password = $this->billing->getPassword($password);

        $data = $this->provider->calculateRegisterContract(
            $user,
            $password,
            $contract,
            $contractName,
            $params
        );

        return $data;
    }

    /**
     * Возвращает список транзакций, выводит по 20 транцакций, с комментариями
     *
     * @param int $page страница для вывода
     *
     * @return array сортируются транзакции по дате, первые - молодые
     * {
     * "blockNumber": "65204",
     * "timeStamp": "1439232889",
     * "hash": "0x98beb27135aa0a25650557005ad962919d6a278c4b3dde7f4f6a3a1e65aa746c",
     * "nonce": "0",
     * "blockHash": "0x373d339e45a701447367d7b9c7cef84aab79c2b2714271b908cda0ab3ad0849b",
     * "transactionIndex": "0",
     * "from": "0x3fb1cd2cd96c6d5c0b5eb3322d807b34482481d4",
     * "to": "0xde0b295669a9fd93d5f28d9ec85e40f4cb697bae",
     * "value": "0",
     * "gas": "122261",
     * "gasPrice": "50000000000",
     * "isError": "0",
     * "input": "0xf00d4b5d000000000000000000000000036c8cecce8d8bbf0831d840d7f29c9e3ddefa63000000000000000000000000c5a96db085dda36ffbe390f455315d30d6d3dc52",
     * "contractAddress": "",
     * "cumulativeGasUsed": "122207",
     * "gasUsed": "122207",
     * "confirmations": "3745410"
     *
     * "comment": "1"
     * "type": "1"
     * "data": "{}"
     *
     * "value_converted" - float - значение сконвентированное Если `$user->currency_view` > 0
     * "fee_converted"   - float - значение сконвентированное Если `$user->currency_view` > 0
     * }
     */
    public function transactionListComments($page = 1)
    {
        $list = $this->transactionList($page);
        $list = $this->concatComments($this->billing, $list);
        $list = $this->changeRateComments($list);

        return $list;
    }

    /**
     * Присоединяет колонку комментарий для транзакций
     *
     * @param \common\models\avatar\UserBill $billing
     * @param array $data транзакции кошелька
     * [
     *      [
     *          //...
     *      ],
     *      //...
     * ]
     *
     * @return array
     * [
     *      [
     *          //...
     *          'comment' => 'Перевод от клиента №'
     *          'type' => 1
     *      ],
     *      //...
     * ]
     */
    private function concatComments($billing, $data, $itemsPerPage = 20)
    {
        /** @var array $unset Перечень параметров в одномерном массиве индексы которых нужно удалить */
        $unset = [];

        if (count($data) == 0) {
            return $data;
        }

        $firstTime = $data[0]['timeStamp'];

        $comments = \common\models\avatar\UserBillOperation::find()
            ->where([
                'and',
                ['bill_id' => $billing->id],
                ['<=', 'created_at', $firstTime + 60],
            ])
            ->limit($itemsPerPage + 10)
            ->orderBy(['created_at' => SORT_DESC])
            ->all();
        ;
        $rows = [];
        foreach($data as $rowOld) {
            $rowNew = $rowOld;
            $comment = $this->getTransaction($rowOld['hash'], $comments);
            if (!is_null($comment)) {
                $rowNew['comment'] = $comment->message;
                $rowNew['type'] = $comment->type;
                $rowNew['data'] = Json::decode($comment->data);
                if (isset($rowNew['data']['user_id'])) {
                    $rowNew['data']['user'] = UsersInCache::find($rowNew['data']['user_id']);
                    if (isset($rowNew['data']['user']['avatar'])) $rowNew['data']['user']['avatar'] = 'https://www.avatar-bank.com' . $rowNew['data']['user']['avatar'];
                }
                if (isset($rowNew['data']['billing_id'])) {
                    $billing = UserBill::findOne($rowNew['data']['billing_id']);
                    $rowNew['data']['billing'] = [
                        'id'    => $billing->id,
                        'name'  => $billing->name,
                    ];
                }
            } else {
                $rowNew['comment'] = '';
            }
            if (!isset($rowNew['type'])) {
                if ($rowNew['from'] == $this->billing->address) {
                    $rowNew['type'] = -1;
                    $rowNew['direction'] = -1;
                }
                if ($rowNew['to'] == $this->billing->address) {
                    $rowNew['type'] = 1;
                    $rowNew['direction'] = 1;
                }
            }

            foreach ($unset as $name) {
                unset($rowNew[$name]);
            }

            // time
            $rowNew['time'] = $rowNew['timeStamp'];

            // тип транзакции
            {
                if ($rowNew['contractAddress'] == '' && $rowNew['input'] == '0x' && $rowNew['value'] != 0) {
                    $rowNew['t'] = 1;
                } else if ($rowNew['contractAddress'] == '' && $rowNew['input'] == '0x' && $rowNew['value'] == 0) {
                    $rowNew['t'] = 2;
                } else if ($rowNew['contractAddress'] != '' && strlen($rowNew['input']) > 2 && $rowNew['to'] == '') {
                    // контракт
                    $rowNew['t'] = 3;
                }
            }

            $rows[] = $rowNew;
        }
        return $rows;

    }


    /**
     * Расчитывает стоимость вызова контракта
     *
     * @param string        $contract
     * @param string        $abi
     * @param string        $functionName
     * @param array         $params
     * @param string        $password
     *
     * @return array
     * [
     *      'cost'
     *      'gasPrice'
     * ]
     * @throws
     *
     * 'insufficient funds for gas * price + value'
     * 'Error: could not decrypt key with given passphrase'
     */
    public function calculateFunction($contract, $abi, $functionName, $params = [], $password = null)
    {
        $password = $this->billing->getPassword($password);

        Yii::info(\yii\helpers\VarDumper::dumpAsString([
            $this->billing->address,
            $password,
            $contract,
            $abi,
            $functionName,
            $params
        ]), 'avatar\common\components\providers\WalletETH::contractCalculate()');

        $ret = $this->provider->contractCalculate(
            $this->billing->address,
            $password,
            $contract,
            $abi,
            $functionName,
            $params
        );
        Yii::info(\yii\helpers\VarDumper::dumpAsString($ret), 'avatar\models\WalletETH::calculateFunction');

        return $ret;
    }

    /**
     * Если необхдимо то конвертирует валюту
     * Если `$user->currency_view` > 0, то добавляются два параметра `value_converted` и `fee_converted`
     *
     * @param array $transactionList
     *
     * @return array
     */
    private function changeRateComments($transactionList)
    {
        /** @var \common\models\UserAvatar $user */
        $user = Yii::$app->user->identity;
        $currencyView = $user->currency_view;

        if (!is_null($currencyView)) {
            if ($currencyView > 0) {
                for ($i = 0; $i < count($transactionList); $i++) {
                    $item = $transactionList[$i];
                    $feeWei = $item['value'];
                    $feeEth = $feeWei / pow(10, 18);
                    $transactionList[$i]['value_converted'] = \common\models\avatar\Currency::convertBySettings($feeEth, 'ETH', true);

                    $gasPrice = $item['gasPrice'];
                    $gasUsed = $item['gasUsed'];
                    $feeWei = $gasPrice * $gasUsed;
                    $feeEth = $feeWei / pow(10, 18);

                    $transactionList[$i]['fee_converted'] = \common\models\avatar\Currency::convertBySettings($feeEth, 'ETH', true);
                }
            }
        }

        return $transactionList;
    }

    /**
     * Ищет определенную транзакцию в списке коментариев и возвращает ее
     *
     * @param int $hash
     * @param \common\models\avatar\UserBillOperation[] $array
     * @return \common\models\avatar\UserBillOperation | null
     */
    public function getTransaction($hash, $array)
    {
        foreach($array as $row) {
            $t = ArrayHelper::getValue($row, 'transaction', '');
            if ($t) {
                if ($t == $hash) {
                    return $row;
                }
            }
        }
        return null;
    }


    /**
     * Создает кошелек
     *
     * Создает счет \common\models\avatar\UserBill
     * Создает адресн новый \common\models\avatar\UserBillAddress
     *
     * @param string        $name
     * @param string        $password пароль от кабинета
     * @param int | null    $userId
     * @param int           $passwordType
     *
     * @return \avatar\models\WalletETH
     *
     * @throws
     */
    public static function create($name, $password, $userId = null, $passwordType = \common\models\avatar\UserBill::PASSWORD_TYPE_OPEN_CRYPT)
    {
        $passwordData = UserBill::createPassword($password, $passwordType);
        Yii::info(\yii\helpers\VarDumper::dumpAsString($passwordData), 'avatar\models\WalletETH::create:1');
        Yii::info(\yii\helpers\VarDumper::dumpAsString($passwordData), 'info\avatar\models\WalletETH::create:1');
        $passwordWallet = $passwordData['passwordWallet'];
        $passwordHash = $passwordData['passwordHash'];

        /** @var \common\components\providers\ETH $provider */
        $provider = \Yii::$app->eth;
        $ret = $provider->_callPost('new', [
            'password' => $passwordWallet,
        ]);
        try {
            $data = Json::decode($ret->content);
        } catch (\Exception $e) {
            Yii::error($e->getMessage() . ' ' . \yii\helpers\VarDumper::dumpAsString($ret), 'avatar\common\components\providers\ETH::create');
            throw $e;
        }
        Yii::info(\yii\helpers\VarDumper::dumpAsString($data), 'avatar\common\components\providers\ETH::create:2');
        Yii::info(\yii\helpers\VarDumper::dumpAsString($data), 'info\avatar\models\WalletETH::create:2');
        if (isset($data['error'])) throw new Exception($data['error']);

        $t = Yii::$app->db->beginTransaction();
        try {
            $address = $data['address'];

            // Сохраняю счет
            $billing = \common\models\avatar\UserBill::add([
                'user_id'       => $userId,
                'address'       => $address,
                'name'          => $name,
                'password'      => $passwordHash,
                'password_type' => $passwordType,
                'currency'      => Currency::ETH,
            ]);

            $walletAvatar = new self([
                'billing' => $billing,
            ]);

            $t->commit();

            return $walletAvatar;

        } catch (\Exception $e) {
            Yii::warning($e->getMessage(), 'avatar\\common\\components\\providers\\ETH::create');
            $t->rollBack();

            throw $e;
        }
    }
}