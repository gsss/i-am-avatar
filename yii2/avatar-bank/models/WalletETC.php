<?php
/**
 * Created by PhpStorm.
 * User: Бог-Творец
 * Date: 18.11.2016
 * Time: 3:55
 */

namespace avatar\models;


use avatar\modules\ETC\Epool;
use avatar\modules\ETH\ServiceEtherScan;
use common\models\avatar\Currency;
use common\services\UsersInCache;
use cs\services\Str;
use cs\services\VarDumper;
use Yii;
use Blocktrail\SDK\WalletInterface;
use common\models\avatar\UserBill;
use common\models\avatar\UserBillAddress;
use common\models\avatar\UserBillOperation;
use common\models\piramida\BitCoinUserAddress;
use common\payment\BitCoinBlockTrailPayment;
use yii\base\Exception;

use yii\base\Security;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;
use yii\helpers\StringHelper;
use yii\httpclient\Client;

/**
 * Класс для кошелька ETC
 *
 * Class WalletETH
 *
 * @package avatar\models
 */
class WalletETC extends Object
{
    /** @var  \common\components\providers\ETC */
    public $provider;

    /** @var  \common\models\avatar\UserBill */
    public $billing;

    /**
     * Выдает баланс эфира на кошельке в ETC
     *
     * @return double as string
     */
    public function getBalance()
    {
        // TODO
        $provider = new \avatar\modules\ETC\Epool();
        $responseData = $provider->method('eth_getBalance', [
            $this->billing->address,
//            'pending',
            'latest',
        ]);

        return $this->hex2dec($responseData) / 1000000000000000000;
    }

    /**
     * @param null | string $password
     *
     * @return array
     * [
     *      'fileContent'
     *      'password'
     * ]
     */
    public function getPrivateJson($password = null)
    {
        // TODO
        $passwordHash = $this->billing->password;
        $passwordType = $this->billing->password_type;
        switch ($passwordType) {
            case UserBill::PASSWORD_TYPE_HIDE_CABINET:
            case UserBill::PASSWORD_TYPE_HIDE_PRIVATE:
                $key = UserBill::passwordToKey32($password);
                $password = \common\services\Security\AES::decrypt256CBC($passwordHash, $key);
                break;

            case UserBill::PASSWORD_TYPE_OPEN:
            default:
                $password = $passwordHash;
                break;
        }

        $data = $this->provider->_callPost('exportKeyJson', [
            'address'  => $this->billing->address,
            'password' => $password,
        ]);
        VarDumper::dump($data);

        $dataObject = Json::decode($data->content);

        return $dataObject;
    }

    /**
     * Конвертирует HEX в DEC
     *
     * @param string $value начинается с 0x например 0x61fe62a9b4f3086
     *
     * @return int
     */
    private function hex2dec($value)
    {
        $chars = Str::getChars($value);
        $multi = 1;
        $sum = 0;
        for ($i = count($chars) - 1; $i > 1; $i--) {
            $char = $chars[$i];
            switch ($char) {
                case 'a': $d = 10; break;
                case 'b': $d = 11; break;
                case 'c': $d = 12; break;
                case 'd': $d = 13; break;
                case 'e': $d = 14; break;
                case 'f': $d = 15; break;
                default: $d = (int) $char; break;
            }
            $sum += $d * $multi;
            $multi = $multi * 16;
        }

        return $sum;
    }

    /**
     * Отправляет деньги с записыванием информации какие транзакции были сделаны с комментариями
     *
     * @param array $to
     * [
     *      <На какой адрес> => <сколько>
     * ]
     * - <На какой адрес> - string | \common\models\avatar\UserBill -
     * - <сколько> - double | array - сколько BTC
     * [
     *      - amount - string
     *      - comment - string | array
     * ]
     * если is_array($comment)
     * [
     *      - from - string
     *      - to - string
     * ]
     * если иначе предполагается
     * [
     *      - from - comment
     *      - to - comment
     * ]
     * @param null | string $password
     *
     * @return string txid
     * @throws
     * 'insufficient funds for gas * price + value'
     */
    public function pay($to, $password = null)
    {
        // TODO
        $passwordHash = $this->billing->password;
        $passwordType = $this->billing->password_type;
        switch ($passwordType) {
            case UserBill::PASSWORD_TYPE_HIDE_CABINET:
            case UserBill::PASSWORD_TYPE_HIDE_PRIVATE:
                $key = UserBill::passwordToKey32($password);
                $password = \common\services\Security\AES::decrypt256CBC($passwordHash, $key);
                break;

            case UserBill::PASSWORD_TYPE_OPEN:
            default:
                $password = $passwordHash;
                break;
        }

        foreach ($to as $destinationAddress => $amount) {
            // счет как строка
            $from = '';
            $to = '';
            if (is_array($amount)) {
                $options = $amount;
                $amount = $options['amount'];
                $comment = $options['comment'];
                if (is_array($comment)) {
                    $from = $comment['from'];
                    $to = $comment['to'];
                } else {
                    $from = $comment;
                    $to = $comment;
                }
            }

            Yii::info(
                \yii\helpers\VarDumper::dumpAsString([
                    $this->billing->address,
                    $this->billing->password,
                    $destinationAddress,
                    $amount
                ]),
                'avatar\models\WalletETС::pay'
            );

            $ret = $this->provider->send(
                $this->billing->address,
                $password,
                $destinationAddress,
                $amount
            );

            Yii::info($ret, 'avatar\models\WalletETC::pay');
            $transaction = $ret['transaction'];

            if ($from) {
                $fromOperation = UserBillOperation::add([
                    'bill_id'     => $this->billing->id,
                    'type'        => UserBillOperation::TYPE_OUT,
                    'transaction' => $transaction,
                    'message'     => $from,
                    'user_id'     => \Yii::$app->user->id,
                ]);
            }

            if ($to != '') {
                // ищу куда переводятся деньги
                try {
                    $billingTo = UserBill::findOne(['address' => $destinationAddress, 'currency' => Currency::ETH]);
                    if (isset($fromOperation)) {
                        $fromOperation->data = json_encode([
                            'user_id'    => $billingTo->user_id,
                            'billing_id' => $billingTo->id,
                        ]);
                        $fromOperation->save();
                    }
                    UserBillOperation::add([
                        'bill_id'     => $billingTo->id,
                        'type'        => UserBillOperation::TYPE_IN,
                        'transaction' => $transaction,
                        'message'     => $to,
                        'user_id'     => $billingTo->user_id,
                        'data'        => json_encode([
                            'user_id'    => \Yii::$app->user->id,
                            'billing_id' => $this->billing->id,
                        ]),
                    ]);
                } catch (\Exception $e) {
                    \Yii::warning('Не найден адрес назначения для ' . $destinationAddress, 'avatar/noAddressDestination');
                }
            }

            return $transaction;
        }
    }

    /**
     * Возвращает список транзакций, без комментариев
     *
     * @param int $page страница для вывода
     *
     * @return array сортируются транзакции по дате, первые - молодые
     * {
     * confirms: 3022
     * failed: false
     * from: "0x3f5CE5FBFe3E9af3971dD833D26bA9b5C936f0bE"
     * hash: "0xceb2ba8854d3a2551ee5beefd03bcfa2fc429b0d6c81047acb6edf9b95c01fdf"
     * height: 5104307
     * internal: false
     * isSend: false
     * timestamp: "2017-12-29T01:07:10"
     * to: "0x913C2Ef0Fe908F41f53ceE0b52677B001297b576"
     * value:{
     *      ether: 0.01
     *      hex: "0x2386f26fc10000"
     *      wei: "10000000000000000"
     * }
     * }
     */
    public function transactionList($page = 1)
    {
        return (new Epool())->addressTransationList($this->billing->address, $page)['items'];
    }


    /**
     * Вызывает исполнение контракта
     *
     * @param string | null $password если = null, то значит пароль берется из счета и его тип = UserBill::PASSWORD_TYPE_OPEN
     *                                иначе это пароль пользователя от кабинета
     * @param string        $contract адрес
     * @param string        $abi
     * @param string        $functionName
     * @param array         $params
     * @param string        $write   'true' | 'false'
     *
     * @return string
     * @throws \Exception
     */
    public function contract($password = null, $contract, $abi, $functionName, $params = [], $write = 'true')
    {
        // TODO
        $user = $this->billing->address;
        if (is_null($password)) {
            if ($this->billing->password_type != UserBill::PASSWORD_TYPE_OPEN) {
                throw new Exception('Не задан пароль');
            }
            $password = $this->billing->password;
        } else {
            if ($this->billing->password_type == UserBill::PASSWORD_TYPE_OPEN) {
                $password = $this->billing->password;
            } else {
                $password = $this->billing->passwordDeСript($password);
            }
        }

        $data = $this->provider->contract(
            $user,
            $password,
            $contract,
            $abi,
            $functionName,
            $params,
            $write
        );

        return $data;
    }

    /**
     * Вызывает исполнение контракта
     *
     * @param string | null $password если = null, то значит пароль берется из счета и его тип = UserBill::PASSWORD_TYPE_OPEN
     *                                иначе это пароль пользователя от кабинета
     * @param string        $contract код
     * @param string        $contractName
     * @param array         $params
     *
     * @return array
     * @throws \Exception
     */
    public function registerContract($password = null, $contract, $contractName, $params = [])
    {
        // TODO
        $user = $this->billing->address;
        if (is_null($password)) {
            if ($this->billing->password_type != UserBill::PASSWORD_TYPE_OPEN) {
                throw new Exception('Не задан пароль');
            }
            $password = $this->billing->password;
        } else {
            if ($this->billing->password_type == UserBill::PASSWORD_TYPE_HIDE_CABINET) {
                $password = $this->billing->passwordDeСript($password);
            } else {
                $password = $this->billing->password;
            }
        }
        $data = $this->provider->registerContract(
            $user,
            $password,
            $contract,
            $contractName,
            $params
        );

        $operation = UserBillOperation::add([
            'bill_id'     => $this->billing->id,
            'type'        => UserBillOperation::TYPE_OUT,
            'transaction' => $data['transactionHash'],
            'message'     => 'Регистрация контракта токена',
            'user_id'     => $this->billing->user_id,
        ]);

        return $data;
    }

    /**
     * Расчитывает регистрацию контракта
     *
     * @param string | null $password если = null, то значит пароль берется из счета и его тип = UserBill::PASSWORD_TYPE_OPEN
     *                                иначе это пароль пользователя от кабинета
     * @param string        $contract код
     * @param string        $contractName
     * @param array         $params
     *
     * @return array
     * @throws \Exception
     */
    public function calculateRegisterContract($password = null, $contract, $contractName, $params = [])
    {
        // TODO
        $user = $this->billing->address;
        if (is_null($password)) {
            if ($this->billing->password_type != UserBill::PASSWORD_TYPE_OPEN) {
                throw new Exception('Не задан пароль');
            }
            $password = $this->billing->password;
        } else {
            if ($this->billing->password_type == UserBill::PASSWORD_TYPE_HIDE_CABINET) {
                $password = $this->billing->passwordDeСript($password);
            } else {
                $password = $this->billing->password;
            }
        }
        $data = $this->provider->calculateRegisterContract(
            $user,
            $password,
            $contract,
            $contractName,
            $params
        );

        return $data;
    }

    /**
     * Возвращает список транзакций, выводит по 20 транцакций, с комментариями
     *
     * @param int $page страница для вывода
     *
     * @return array сортируются транзакции по дате, первые - молодые
     * {
     * confirms: 3022
     * failed: false
     * from: "0x3f5CE5FBFe3E9af3971dD833D26bA9b5C936f0bE"
     * hash: "0xceb2ba8854d3a2551ee5beefd03bcfa2fc429b0d6c81047acb6edf9b95c01fdf"
     * height: 5104307
     * internal: false
     * isSend: false
     * timestamp: "2017-12-29T01:07:10"
     * to: "0x913C2Ef0Fe908F41f53ceE0b52677B001297b576"
     * value:{
     *      ether: 0.01
     *      hex: "0x2386f26fc10000"
     *      wei: "10000000000000000"
     * }
     * }
     */
    public function transactionListComments($page = 1)
    {
        // TODO
        $list = $this->transactionList($page);
        $list = $this->concatComments($this->billing, $list);
        $list = $this->changeRateComments($list);

        return $list;
    }

    /**
     * Присоединяет колонку комментарий для транзакций
     *
     * @param \common\models\avatar\UserBill $billing
     * @param array $data транзакции кошелька
     * [
     *      {
     *          confirms: 3022
     *          failed: false
     *          from: "0x3f5CE5FBFe3E9af3971dD833D26bA9b5C936f0bE"
     *          hash: "0xceb2ba8854d3a2551ee5beefd03bcfa2fc429b0d6c81047acb6edf9b95c01fdf"
     *          height: 5104307
     *          internal: false
     *          isSend: false
     *          timestamp: "2017-12-29T01:07:10"
     *          to: "0x913C2Ef0Fe908F41f53ceE0b52677B001297b576"
     *          value:{
     *              ether: 0.01
     *              hex: "0x2386f26fc10000"
     *              wei: "10000000000000000"
     *          }
     *      }
     *      //...
     * ]
     *
     * @return array
     * [
     *      [
     *          //...
     *          'comment' => 'Перевод от клиента №'
     *          'type' => 1
     *      ],
     *      //...
     * ]
     */
    private function concatComments($billing, $data, $itemsPerPage = 20)
    {
        // TODO
        /** @var array $unset Перечень параметров в одномерном массиве индексы которых нужно удалить */
        $unset = [];

        if (count($data) == 0) {
            return $data;
        }

        // timestamp: "2017-12-29T01:07:10"
        $firstTime = \DateTime::createFromFormat('c', $data[0]['timestamp']);

        $comments = \common\models\avatar\UserBillOperation::find()
            ->where([
                'and',
                ['bill_id' => $billing->id],
                ['<=', 'created_at', $firstTime + 60],
            ])
            ->limit($itemsPerPage + 10)
            ->orderBy(['created_at' => SORT_DESC])
            ->all();
        ;
        $rows = [];
        foreach($data as $rowOld) {
            $rowNew = $rowOld;
            $comment = $this->getTransaction($rowOld['hash'], $comments);
            if (!is_null($comment)) {
                $rowNew['comment'] = $comment->message;
                $rowNew['type'] = $comment->type;
                $rowNew['data'] = Json::decode($comment->data);
                if (isset($rowNew['data']['user_id'])) {
                    $rowNew['data']['user'] = UsersInCache::find($rowNew['data']['user_id']);
                    if (isset($rowNew['data']['user']['avatar'])) $rowNew['data']['user']['avatar'] = 'https://www.avatar-bank.com' . $rowNew['data']['user']['avatar'];
                }
                if (isset($rowNew['data']['billing_id'])) {
                    $billing = UserBill::findOne($rowNew['data']['billing_id']);
                    $rowNew['data']['billing'] = [
                        'id'    => $billing->id,
                        'name'  => $billing->name,
                    ];
                }
            } else {
                $rowNew['comment'] = '';
            }
            if (!isset($rowNew['type'])) {
                if (strtolower($rowNew['from']) == $this->billing->address) {
                    $rowNew['type'] = -1;
                    $rowNew['direction'] = -1;
                }
                if (strtolower($rowNew['to']) == $this->billing->address) {
                    $rowNew['type'] = 1;
                    $rowNew['direction'] = 1;
                }
            }

            foreach ($unset as $name) {
                unset($rowNew[$name]);
            }

            // time
            $rowNew['time'] = \DateTime::createFromFormat('c', $rowNew['timestamp']);

            // тип транзакции
            if (false) {
                if ($rowNew['contractAddress'] == '' && $rowNew['input'] == '0x' && $rowNew['value'] != 0) {
                    $rowNew['t'] = 1;
                } else if ($rowNew['contractAddress'] == '' && $rowNew['input'] == '0x' && $rowNew['value'] == 0) {
                    $rowNew['t'] = 2;
                } else if ($rowNew['contractAddress'] != '' && strlen($rowNew['input']) > 2 && $rowNew['to'] == '') {
                    // контракт
                    $rowNew['t'] = 3;
                }
            }

            $rows[] = $rowNew;
        }
        return $rows;

    }


    /**
     * Если необхдимо то конвертирует валюту
     * Если `$user->currency_view` > 0, то добавляются два параметра `value_converted` и `fee_converted`
     *
     * @param array $transactionList
     * [
     *      {
     *          confirms: 3022
     *          failed: false
     *          from: "0x3f5CE5FBFe3E9af3971dD833D26bA9b5C936f0bE"
     *          hash: "0xceb2ba8854d3a2551ee5beefd03bcfa2fc429b0d6c81047acb6edf9b95c01fdf"
     *          height: 5104307
     *          internal: false
     *          isSend: false
     *          timestamp: "2017-12-29T01:07:10"
     *          to: "0x913C2Ef0Fe908F41f53ceE0b52677B001297b576"
     *          value:{
     *              ether: 0.01
     *              hex: "0x2386f26fc10000"
     *              wei: "10000000000000000"
     *          }
     *      }
     *      //...
     * ]
     *
     * @return array
     */
    private function changeRateComments($transactionList)
    {
        // TODO
        /** @var \common\models\UserAvatar $user */
        $user = Yii::$app->user->identity;
        $currencyView = $user->currency_view;

        if (!is_null($currencyView)) {
            if ($currencyView > 0) {
                for ($i = 0; $i < count($transactionList); $i++) {
                    $item = $transactionList[$i];
                    $feeWei = $item['value']['wei'];
                    $feeEth = $feeWei / 1000000000000000000;
                    $transactionList[$i]['value_converted'] = \common\models\avatar\Currency::convertBySettings($feeEth, 'ETC', true);

//                    $gasPrice = $item['gasPrice'];
//                    $gasUsed = $item['gasUsed'];
//                    $feeWei = $gasPrice * $gasUsed;
//                    $feeEth = $feeWei / 1000000000000000000;
//
//                    $transactionList[$i]['fee_converted'] = \common\models\avatar\Currency::convertBySettings($feeEth, 'ETC', true);
                }
            }
        }

        return $transactionList;
    }

    /**
     * Ищет определенную транзакцию в списке коментариев и возвращает ее
     *
     * @param int $hash
     * @param \common\models\avatar\UserBillOperation[] $array
     * @return \common\models\avatar\UserBillOperation | null
     */
    public function getTransaction($hash, $array)
    {
        // TODO
        foreach($array as $row) {
            $t = ArrayHelper::getValue($row, 'transaction', '');
            if ($t) {
                if ($t == $hash) {
                    return $row;
                }
            }
        }
        return null;
    }


    /**
     * Создает кошелек
     *
     * Создает счет \common\models\avatar\UserBill
     * Создает адресн новый \common\models\avatar\UserBillAddress
     *
     * @param string $name
     * @param string $password
     * @param int | null $userId
     * @param int $passwordType
     *
     * @return \common\models\avatar\UserBill
     *
     * @throws
     */
    public static function create($name, $password, $userId = null, $passwordType = \common\models\avatar\UserBill::PASSWORD_TYPE_OPEN)
    {
        $passwordWallet = \common\services\Security::generateRandomString();
        if ($passwordType != \common\models\avatar\UserBill::PASSWORD_TYPE_OPEN) {
            $passwordHash = UserBill::passwordEnСript($passwordWallet, $password);
        } else {
            $passwordHash = $passwordWallet;
        }
        /** @var \common\components\providers\ETC $etc */
        $etc = Yii::$app->etc;
        $ret = $etc->_callPost('new', [
            'password' => $passwordWallet,
        ]);
        try {
            $data = Json::decode($ret->content);
        } catch (\Exception $e) {
            Yii::error($e->getMessage() . ' ' . \yii\helpers\VarDumper::dumpAsString($ret), 'avatar\common\components\providers\ETC::create');
            throw $e;
        }
        Yii::info(\yii\helpers\VarDumper::dumpAsString($data), 'avatar\common\components\providers\ETC::create');
        if (isset($data['error'])) throw new Exception($data['error']);

        $t = Yii::$app->db->beginTransaction();
        try {
            $address = $data['address'];

            // Сохраняю счет
            $billing = \common\models\avatar\UserBill::add([
                'user_id'       => $userId,
                'address'       => $address,
                'name'          => $name,
                'password'      => $passwordHash,
                'password_type' => $passwordType,
                'currency'      => Currency::ETC,
            ]);

            $t->commit();

            return $billing;

        } catch (\Exception $e) {
            Yii::warning($e->getMessage(), 'avatar\\common\\components\\providers\\ETC::create');
            $t->rollBack();

            return null;
        }
    }
}