<?php


namespace avatar\services;

use cs\services\Str;
use cs\services\VarDumper;
use yii\base\Widget;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\Html as yiiHtml;

class Params extends Widget
{
    public $options;

    /**
     * @var array
     * [
     *      'name'
     *      'description'
     *      'isRequired' bool по умолчанию false
     *      'type' string по умолчанию 'string'
     * ]
     */
    public $params;

    /** @var bool | null - Показывать нумерацию строк? true - да, false - нет. По умолчанию если не установлено = true */
    public $hasNums = true;

    /**
     * Печатает все объединения категории
     *
     * @return string HTML
     */
    public function run()
    {
        $headers = ['Название', 'Обязательный?', 'Тип данных', 'Описание'];
        if ($this->hasNums) {
            $headers = ArrayHelper::merge(['#'], $headers);
        }

        $r = [];
        foreach ($headers as $header) {
            $r[] = Html::tag('th', $header);
        }
        $rows = [Html::tag('tr', join('', $r))];

        $index = 1;
        foreach ($this->params as $param) {
            $rows[] = $this->tr($param, $index);
            $index++;
        }

        return Html::tag('table', join('', $rows), ['class' => 'table table-hover table-striped', 'style' => 'width:auto;']);
    }

    private function tr($param, $index)
    {
        $td = [];
        if ($this->hasNums) {
            $td[] = Html::tag('td', $index);
        }
        $td[] = Html::tag('td', Html::tag('code', $param['name']));
        $isRequired = ArrayHelper::getValue($param, 'isRequired', false);
        if ($isRequired) {
            $html = Html::tag('span', 'Да', ['class' => 'label label-success']);
        } else {
            $html = Html::tag('span', 'Нет', ['class' => 'label label-default']);
        }
        $td[] = Html::tag('td', $html);
        $type = ArrayHelper::getValue($param, 'type', 'string');
        $td[] = Html::tag('td', $type);
        $td[] = Html::tag('td', $param['description']);

        return Html::tag('tr', join('', $td));
    }
}