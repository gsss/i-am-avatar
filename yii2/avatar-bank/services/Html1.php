<?php
/**
 * Created by PhpStorm.
 * User: Ra-m-ha
 * Date: 04.04.2019
 * Time: 21:10
 */

namespace avatar\services;


use yii\helpers\Html;
use Yii;

/**
 * Class Html1
 * @package avatar\services
 * Сервисный класс который обеспечивает функции меню
 */
class Html1
{
    public static function liMenu12($route, $name = null, $options = [])
    {
        if (is_array($route)) {
            $arr = [];
            foreach ($route as $route1 => $name1) {
                $arr[] = self::liMenu12($route1, $name1);
            }
            return join("\n", $arr);
        }
        if (\Yii::$app->requestedRoute == $route) {
            $options['class'] = 'active';
        }

        return Html::tag('li', \common\helpers\Html::a($name, [$route]), $options);
    }

    /**
     * @param $item
     * @param $route
     * @return bool
     */
    public static function hasRoute20190404($item, $route)
    {
        if (isset($item['route'])) {
            if ($item['route'] == $route) return true;
        }

        if (isset($item['items'])) {
            foreach($item['items'] as $i) {
                if ($i['route'] == $route) {
                    return true;
                }
            }
        }

        if (\yii\helpers\ArrayHelper::keyExists('urlList', $item)) {
            foreach($item['urlList'] as $i) {
                switch($i[0]) {
                    case 'startsWith':
                        if (\yii\helpers\StringHelper::startsWith($route, $i[1])) return true;
                        break;
                    case 'route':
                        if ($i[1] == $route) return true;
                        break;
                    case 'controller':
                        $arr = explode('/', $route);
                        if ($arr[0] == $i[1]) return true;
                        break;
                }
            }
        }

        return false;
    }

    /**
     * Выводит элеемнты для меню выбора языка
     * @param $items
     * @return string
     */
    public static function getItems2($items)
    {
        $rows = [];
        foreach ($items as $k => $item) {
            $icon = Html::tag('i', Html::img('//www.i-am-avatar.com' . $item->image, [
                'height' => 20,
                'class'  => 'img-circle',
                'style'  => 'border: 1px solid #888',
            ]), ['style' => 'margin-right: 15px;']);
            $a = Html::a($icon . $item['name'], 'javascript:void(0)', ['class' => 'buttonLanguageSet', 'data' => ['code' => $item->code]]);
            $li = Html::tag('li', $a);
            $rows[] = $li;
        }

        return join("\n", $rows);
    }

    public static function isSelected20190516($item)
    {
        if (isset($item['route'])) {
            if ($item['route'] == Yii::$app->requestedRoute) return true;
        }

        if (isset($item['items'])) {
            foreach ($item['items'] as $i) {
                if ($i['route'] == Yii::$app->requestedRoute) {
                    return true;
                }
            }
        }

        if (\yii\helpers\ArrayHelper::keyExists('urlList', $item)) {
            foreach ($item['urlList'] as $ii) {
                $i = $ii[0];
                $v = $ii[1];
                switch ($i) {
                    case 'startsWith':
                        if (\yii\helpers\StringHelper::startsWith(Yii::$app->request->getUrl(), $v)) return true;
                        break;
                    case 'controller':
                        $arr = explode('/', Yii::$app->requestedRoute);
                        if ($arr[0] == $v) return true;
                        break;
                }
            }
        }

        return false;
    }

    public static function liMenu20190516($route, $name = null, $options = [])
    {
        if (is_array($route)) {
            $arr = [];
            foreach ($route as $route1 => $name1) {
                $arr[] = liMenu($route1, $name1);
            }
            return join("\n", $arr);
        }
        if (Yii::$app->requestedRoute == $route) {
            $options['class'] = 'active';
        }

        return \yii\helpers\Html::tag('li', \cs\helpers\Html::a($name, [$route]), $options);
    }
}