<?php


namespace avatar\services;


use yii\helpers\StringHelper;

class YouTubeHelper
{
    /**
     * Нормализует идентификатор видео с ютуба, то есть во входной переменной может быть как ссылка на видео так и сам идентификатор,
     * на выходе только идентификатор
     *
     * @param string $id
     * @return mixed
     * @throws
     */
    public static function getID($id)
    {
        if (StringHelper::startsWith($id, 'http')) {
            $url = new \cs\services\Url($id);
            if ($url->host == 'youtu.be') {
                $v = substr($url->path, 1);
            } else if ($url->host == 'www.youtube.com') {
                $v = $url->getParam('v');
                if ($v == '') {
                    throw new \Exception('Параметр v пустой');
                }
            } else {
                throw new \Exception('Адрес сайта должен быть www.youtube.com или youtu.be');
            }
        } else {
            $v = $id;
        }

        return $v;
    }
}