<?php


namespace avatar\services;

use common\models\HdGenKeys;
use cs\services\Str;
use cs\services\VarDumper;
use yii\base\Widget;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\StringHelper;
use yii\helpers\Url;
use yii\helpers\Html as yiiHtml;

class HumanDesign extends Widget
{
    /**
     * @var array
     * [
     *      1 => солнце - генный ключ
     *      2 => земля - генный ключ
     * ]
     */
    public $design;

    /**
     * @var array
     * [
     *      1 => солнце - генный ключ
     *      2 => земля - генный ключ
     * ]
     */
    public $personality;

    public function init()
    {
        $this->design[1] = HdGenKeys::findOne(['num' => $this->design[1]]);
        $this->design[2] = HdGenKeys::findOne(['num' => $this->design[2]]);
        $this->personality[1] = HdGenKeys::findOne(['num' => $this->personality[1]]);
        $this->personality[2] = HdGenKeys::findOne(['num' => $this->personality[2]]);
    }

    /**
     * Печатает все объединения категории
     *
     * @return string HTML
     */
    public function run()
    {
        $rows = [];
        $rows[] = Html::tag('tr', join('',[
            Html::tag('td', 'Дизайн', ['colspan' => 2,'style' => 'text-align:center;']),
            Html::tag('td', 'Личность', ['colspan' => 2,'style' => 'text-align:center;']),
        ]));
        $rows[] = Html::tag('tr', join('', [
            Html::tag('td', Html::img('/images/controller/user/index/photos.gif', ['width' => 70, 'class' => 'img-circle', 'data' => ['toggle' => 'tooltip'], 'title' => 'Земля']), ['style' => 'text-align:center;vertical-align:middle;']),
            Html::tag('td', Html::img('/images/controller/user/index/solnce.jpg', ['width' => 70, 'class' => 'img-circle', 'data' => ['toggle' => 'tooltip'], 'title' => 'Солнце']), ['style' => 'text-align:center;vertical-align:middle;']),
            Html::tag('td', Html::img('/images/controller/user/index/solnce.jpg', ['width' => 70, 'class' => 'img-circle', 'data' => ['toggle' => 'tooltip'], 'title' => 'Солнце']), ['style' => 'text-align:center;vertical-align:middle;']),
            Html::tag('td', Html::img('/images/controller/user/index/photos.gif', ['width' => 70, 'class' => 'img-circle', 'data' => ['toggle' => 'tooltip'], 'title' => 'Земля']), ['style' => 'text-align:center;vertical-align:middle;']),
        ]));
        $rows[] = $this->trGraph(
            [
                $this->tdGraph($this->design[2]),
                $this->tdGraph($this->design[1]),
                $this->tdGraph($this->personality[1]),
                $this->tdGraph($this->personality[2]),
            ]
        );
        $rows[] = $this->trCodon(
            [
                $this->tdCodon($this->design[2]),
                $this->tdCodon($this->design[1]),
                $this->tdCodon($this->personality[1]),
                $this->tdCodon($this->personality[2]),
            ]
        );
        $rows[] = $this->trNum(
            [
                $this->tdNum($this->design[2]),
                $this->tdNum($this->design[1]),
                $this->tdNum($this->personality[1]),
                $this->tdNum($this->personality[2]),
            ]
        );
        $rows[] = $this->trName(
            [
                $this->tdName($this->design[2]),
                $this->tdName($this->design[1]),
                $this->tdName($this->personality[1]),
                $this->tdName($this->personality[2]),
            ]
        );
        $rows[] = $this->trDar(
            [
                $this->tdDar($this->design[2]),
                $this->tdDar($this->design[1]),
                $this->tdDar($this->personality[1]),
                $this->tdDar($this->personality[2]),
            ]
        );
        $rows[] = $this->trTen(
            [
                $this->tdTen($this->design[2]),
                $this->tdTen($this->design[1]),
                $this->tdTen($this->personality[1]),
                $this->tdTen($this->personality[2]),
            ]
        );
        $rows[] = $this->trSiddhi(
            [
                $this->tdSiddhi($this->design[2]),
                $this->tdSiddhi($this->design[1]),
                $this->tdSiddhi($this->personality[1]),
                $this->tdSiddhi($this->personality[2]),
            ]
        );

        return Html::tag('table', join('', $rows), ['class' => 'table table-hover table-striped', 'style' => 'width:auto;', 'align' => 'center']);
    }

    /**
     * @param \common\models\HdGenKeys $genKey
     * @param array $options
     * @return string
     */
    private function tdImage($genKey, $options = [])
    {
        return Html::tag('td', Html::tag('p', $genKey->ten, ['data' => ['toggle' => 'tooltip'], 'title' => 'Тень', 'class'=> 'text-center']), $options);
    }

    /**
     * @param \common\models\HdGenKeys $genKey
     * @param array $options
     * @return string
     */
    private function tdNum($genKey, $options = [])
    {
        return Html::tag(
            'td',
            Html::tag(
                'p',
                Html::a($genKey->num, ['human-design/gen-keys-item', 'id' => $genKey->num]),
                ['data' => ['toggle' => 'tooltip'], 'title' => 'Номер', 'class' => 'text-center']
            ),
            $options
        );
    }

    /**
     * @param \common\models\HdGenKeys $genKey
     * @param array $options
     * @return string
     */
    private function tdTen($genKey, $options = [])
    {
        return Html::tag(
            'td',
            Html::tag(
                'p',
                Html::a($genKey->ten, ['human-design/gen-keys-item', 'id' => $genKey->num]),
                ['data' => ['toggle' => 'tooltip'], 'title' => 'Тень', 'class'=> 'text-center']
            ),
            $options
        );
    }

    /**
     * @param \common\models\HdGenKeys $genKey
     * @param array $options
     * @return string
     */
    private function tdDar($genKey, $options = [])
    {
        return Html::tag(
            'td',
            Html::tag(
                'p',
                Html::a($genKey->dar, ['human-design/gen-keys-item', 'id' => $genKey->num]),
                ['data' => ['toggle' => 'tooltip'], 'title' => 'Дар', 'class'=> 'text-center']
            ),
            $options
        );
    }

    /**
     * @param \common\models\HdGenKeys $genKey
     * @param array $options
     * @return string
     */
    private function tdSiddhi($genKey, $options = [])
    {
        return Html::tag(
            'td',
            Html::tag(
                'p',
                Html::a($genKey->siddhi, ['human-design/gen-keys-item', 'id' => $genKey->num]),
                ['data' => ['toggle' => 'tooltip'], 'title' => 'Совершенство', 'class'=> 'text-center']
            ),
            $options
        );
    }

    /**
     * @param \common\models\HdGenKeys $genKey
     * @param array $options
     * @return string
     */
    private function tdName($genKey, $options = [])
    {
        return Html::tag('td', Html::tag('p', $genKey->name, ['data' => ['toggle' => 'tooltip'], 'title' => 'Название', 'class'=> 'text-center']), $options);
    }

    /**
     * @param \common\models\HdGenKeys $genKey
     * @param array $options
     * @return string
     */
    private function tdGraph($genKey, $options = [])
    {
        if (isset($options['style'])) {
            if (!StringHelper::endsWith(trim($options['style']), ';')) {
                $options['style'] .= ';';
            }
            $options['style'] .= 'width:200px;';
        } else {
            $options['style'] = 'width:200px;';
        }
        return Html::tag(
            'td',
            Html::tag(
                'p',
                Html::img('http://www.galaxysss.ru' . $genKey->image_graph, ['width' => 50, 'class' => 'img-circle']),
                ['class' => 'text-center']
            ),
            $options
        );
    }

    /**
     * @param \common\models\HdGenKeys $genKey
     * @param array $options
     * @return string
     */
    private function tdCodon($genKey, $options = [])
    {
        return Html::tag('td',
            Html::tag(
                'p',
                Html::img('http://www.galaxysss.ru' . $genKey->image_codon, ['width' => 50, 'class' => 'img-circle']),
                ['class'=> 'text-center']
            ),
            $options);
    }

    private function trTen($content = null, $options = [])
    {
        if (is_array($content)) {
            $content = join('', $content);
        }

        return Html::tag('tr', $content, $options);
    }

    private function trNum($content = null, $options = [])
    {
        if (is_array($content)) {
            $content = join('', $content);
        }

        return Html::tag('tr', $content, $options);
    }

    private function trDar($content = null, $options = [])
    {
        if (is_array($content)) {
            $content = join('', $content);
        }

        return Html::tag('tr', $content, $options);
    }

    private function trSiddhi($content = null, $options = [])
    {
        if (is_array($content)) {
            $content = join('', $content);
        }

        return Html::tag('tr', $content, $options);
    }

    private function trName($content = null, $options = [])
    {
        if (is_array($content)) {
            $content = join('', $content);
        }

        return Html::tag('tr', $content, $options);
    }

    private function trCodon($content = null, $options = [])
    {
        if (is_array($content)) {
            $content = join('', $content);
        }

        return Html::tag('tr', $content, $options);
    }

    private function trGraph($content = null, $options = [])
    {
        if (is_array($content)) {
            $content = join('', $content);
        }

        return Html::tag('tr', $content, $options);
    }
}