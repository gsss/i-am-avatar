<?php

namespace avatar\services;

use cs\services\VarDumper;
use Yii;
use Detection\MobileDetect;
use yii\helpers\Json;

/**
 * CustomizeService
 * Предназначен для брендирования
 */
class CustomizeService extends \yii\base\Component
{
    const PREFIX_MEMCACHE = 'CompanyCustomize';

    const ALGORITM_DEFAULT  = 'default';
    const ALGORITM_EMPTY    = 'empty';

    public $mainDomain = ['avatarnetwork.io', 'avatar-network.org', 'www.avatar-network.org'];

    /**
     * Возвращает значение параметра из массива конфигурации
     *
     * @param string $name название параметра
     * @param string $algoritm Алгоритм дествия при отсутствии значения в конфигурации
     *
     * @return mixed
     */
    public function getParam1($name, $algoritm = self::ALGORITM_EMPTY)
    {
        /**
         * @var $data array
         * [
         *     'cabinet.new-earth.space' => [
         *          'id' => 1,
         *          'domain-name' => 'cabinet.new-earth.space'
         *          //...
         *      ],
         * ]
         */
        $data = self::getAll();
        $value = $data['avatarnetwork.io'][$name];

        $serverName = $_SERVER['HTTP_HOST'];
        if (isset($data[$serverName])) {
            if (isset($data[$serverName][$name])) {
                $value = $data[$serverName][$name];
            } else {
                if ($algoritm == self::ALGORITM_EMPTY) {
                    $value = null;
                }
            }
        } else {
            if ($algoritm == self::ALGORITM_EMPTY) {
                $value = null;
            }
        }

        return $value;
    }

    /**
     * Возвращает значение параметра из массива конфигурации второго уровня вложенности
     *
     * @param string $name название параметра
     * @param string $name2 название параметра 2
     * @param string $algoritm Алгоритм дествия при отсутствии значения в конфигурации
     *
     * @return mixed
     */
    public function getParam2($name, $name2, $algoritm = self::ALGORITM_EMPTY)
    {
        /**
         * @var $data array
         * [
         *     'cabinet.new-earth.space' => [
         *          'id' => 1,
         *          'domain-name' => 'cabinet.new-earth.space'
         *          //...
         *      ],
         * ]
         */
        $data = self::getAll();
        $value = $data['avatarnetwork.io'][$name][$name2];

        $serverName = $_SERVER['HTTP_HOST'];
        if (isset($data[$serverName])) {
            if (isset($data[$serverName][$name][$name2])) {
                $value = $data[$serverName][$name][$name2];
            } else {
                if ($algoritm == self::ALGORITM_EMPTY) {
                    $value = null;
                }
            }
        } else {
            if ($algoritm == self::ALGORITM_EMPTY) {
                $value = null;
            }
        }

        return $value;
    }

    /**
     * Выдает ответ на вопрос: Я нахожусь на главном сайте?
     *
     * @return bool
     */
    public function isMain()
    {
        $serverName = $_SERVER['HTTP_HOST'];
        return in_array($serverName, $this->mainDomain);
    }

    /**
     * Выдает данные из кеша
     *
     * @return array
     * [
     *     'cabinet.new-earth.space' => [
     *          'id' => 1,
     *          'domain-name' => 'cabinet.new-earth.space'
     *          //...
     *      ],
     * ]
     * @throws
     */
    public static function getAll()
    {
        $value = \Yii::$app->cache->get(self::PREFIX_MEMCACHE);
        if ($value === false) {
            $value = self::_getAll();
        }

        return $value;
    }

    /**
     * Выдает из БД данные
     *
     * @return array
     * [
     *     'cabinet.new-earth.space' => [
     *          'id' => 1,
     *          'domain-name' => 'cabinet.new-earth.space'
     *          //...
     *      ],
     * ]
     * @throws
     */
    private static function _getAll()
    {
        $rows = \common\models\CompanyCustomizeItem::find()->all();
        $data = [];
        /** @var \common\models\CompanyCustomizeItem $row */
        foreach ($rows as $row) {
            $item = Json::decode($row->config);
            $item['id'] = $row->id;
            if (isset($item['domain-name'])) {
                $data[$item['domain-name']] = $item;
            }
        }

        return $data;
    }

}
