<?php


namespace avatar\services;

use app\models\Service;
use cs\services\Str;
use cs\services\VarDumper;
use yii\helpers\ArrayHelper;
use yii\helpers\StringHelper;
use yii\helpers\Url;
use yii\helpers\Html as yiiHtml;
use app\models\UnionCategory;
use app\models\Union;

class Html
{
    public static $formatIcon = [
        370,
        370,
        \common\widgets\FileUpload3\FileUpload::MODE_THUMBNAIL_CUT
    ];

    /**
     * Печатает все объединения категории
     *
     * @param int $id идентификатор категории
     *
     * @return string HTML
     */
    public static function unionCategoryItems($id)
    {
        $html = [];
        $category = UnionCategory::find($id);
        if (is_null($category)) return '';
        foreach ($category->getUnions() as $item) {
            $html[] = self::unionItem($item);
        }

        return join('', $html);
    }

    public static function unionItem($row)
    {
        return self::unityItem($row);
    }

    public static function unityItem($row)
    {
        if (!is_array($row)) {
            $id = $row;
            $item = Union::find($id);
            if (is_null($item)) return '';
            $row = $item->getFields();
        }
        $header = (isset($row['header'])) ? $row['header'] : $row['name'];
        // Заголовок
        $html[] = yiiyiiHtml::tag('div', yiiyiiHtml::tag('h3', $header), ['class' => 'header']);
        // подзаголовок
        if (ArrayHelper::keyExists('sub_header', $row)) $html[] = yiiyiiHtml::tag('p', $row['sub_header']);
        // картинка с ссылкой
        $html[] = yiiyiiHtml::tag('p', yiiyiiHtml::a(yiiyiiHtml::img($row['img'], ['width' => '100%', 'class' => 'thumbnail']), self::getUnionUrl($row)));
        // Описание
        $content = $row['description'];
        $content = self::getMiniText($content);
        $html[] = yiiyiiHtml::tag('p', $content);

        return yiiyiiHtml::tag('div', join('', $html), ['class' => 'col-lg-4 unityItem']);
    }

    /**
     * Возвращает путь к карточки объединения
     * @param array $row
     *
     * @return string
     */
    private static function getUnionUrl($row)
    {
        $url = Url::current([], true);
        $oUrl = new \cs\services\Url($url);
        $arr = explode('/', $oUrl->path);
        if ($arr[1] == 'category') {
            $url = '/category/' . $arr[2] . '/' . $row['id'];
        } else {
            $url = '/category/' . $arr[1] . '/' . $row['id'];
        }

        return $url;
    }

    public static function unityCategoryItem($row)
    {
        if (!is_array($row)) {
            $id = $row;
            $item = UnionCategory::find($id);
            if (is_null($item)) return '';
            $row = $item->getFields();
        }
        // Заголовок
        $html[] = yiiyiiHtml::tag('div', yiiyiiHtml::tag('h2', $row['header']), ['class' => 'header']);
        // подзаголовок
        if (ArrayHelper::keyExists('sub_header', $row)) $html[] = yiiyiiHtml::tag('p', $row['sub_header']);
        // картинка с ссылкой
        $html[] = yiiyiiHtml::tag(
            'p',
            yiiyiiHtml::a(
                yiiyiiHtml::img(
                    $row['image'],
                    ['width' => '100%', 'class' => 'thumbnail']
                ),
                Url::to(['page/category', 'id' => $row['id_string']])
            )
        );
        // Описание
        $html[] = yiiyiiHtml::tag('p', self::getMiniText($row['description']));

        return yiiyiiHtml::tag('div', join('', $html), ['class' => 'col-lg-4 unityCategoryItem']);
    }

    /**
     * Рисует услугу
     *
     * @param integer|array $row
     *
     * @return string
     */
    public static function serviceItem($row)
    {
        if (!is_array($row)) {
            $id = $row;
            $item = Service::find($id);
            if (is_null($item)) return '';
            $row = $item->getFields();
        }
        $header = (isset($row['header'])) ? $row['header'] : $row['name'];
        // Заголовок
        $html[] = yiiHtml::tag('div', yiiHtml::tag('h2', $header), ['class' => 'header']);
        // Ссылка
        $l = ['page/services_item', 'id' => $row['id']];
        $html[] = yiiHtml::tag('p', yiiHtml::a(yiiHtml::img($row['image'], ['width' => '100%', 'class' => 'thumbnail']), $l));
        // Описание
        $html[] = $row['description'];

        return yiiHtml::tag('div', join('', $html), ['class' => 'col-lg-4 serviceItem']);
    }

    public static function newsItem($row)
    {
        if (!is_array($row)) {
            $id = $row;
            $item = \app\models\NewsItem::find($id);
            if (is_null($item)) return '';
            $row = $item->getFields();
        }
        // Заголовок
        $html[] = yiiHtml::tag('div', yiiHtml::tag('h4', $row['header']), ['class' => 'header']);
        // Дата
        $html[] = yiiHtml::tag('p', self::dateString($row['date']), yiiHtml::css([
            'font-size' => '70%',
            'color'     => '#808080',
        ]));
        // картинка с ссылкой
        $html[] = yiiHtml::tag('p', yiiHtml::a(yiiHtml::img($row['img'], ['width' => '100%', 'class' => 'thumbnail']), self::getNewsUrl($row)));
        // Описание
        $content = $row['description'];
        if ($content . '' == '') {
            $content = \cs\services\Str::sub(strip_tags($row['content']), 0, 200) . ' ...';
        }
        $html[] = yiiHtml::tag('p', $content);

        return yiiHtml::tag('div', join('', $html), ['class' => 'col-lg-4 newsItem']);
    }

    /**
     * @param array $row gs_praktice
     *
     * @return string
     */
    public static function prakticeItem($row)
    {
        if (!is_array($row)) {
            $id = $row;
            $item = \app\models\Praktice::find($id);
            if (is_null($item)) return '';
            $row = $item->getFields();
        }
        // Заголовок
        $html[] = yiiHtml::tag('div', yiiHtml::tag('h4', $row['header']), ['class' => 'header']);
        // картинка с ссылкой
        $html[] = yiiHtml::tag('p', yiiHtml::a(yiiHtml::img($row['image'], ['width' => '100%', 'class' => 'thumbnail']), self::getPrakticeUrl($row)));
        // Описание
        $html[] = yiiHtml::tag('p', $row['description']);

        return yiiHtml::tag('div', join('', $html), ['class' => 'col-lg-4 prakticeItem']);
    }

    public static function getPrakticeUrl($row, $isFull = false)
    {
        $year = substr($row['date'], 0, 4);
        $month = substr($row['date'], 5, 2);
        $day = substr($row['date'], 8, 2);

        return Url::to(['page/praktice_item', 'year' => $year, 'month' => $month, 'day' => $day, 'id' => $row['id_string']], $isFull);
    }

    public static function getNewsUrl($row, $isFull = false)
    {
        $year = substr($row['date'], 0, 4);
        $month = substr($row['date'], 5, 2);
        $day = substr($row['date'], 8, 2);

        return Url::to(['news/item', 'year' => $year, 'month' => $month, 'day' => $day, 'id' => $row['id_string']], $isFull);
    }

    public static function chennelingItem($row)
    {
        if (!is_array($row)) {
            $id = $row;
            $item = \app\models\Chenneling::find($id);
            if (is_null($item)) return '';
            $row = $item->getFields();
        }
        // Заголовок
        $html[] = yiiHtml::tag('div', yiiHtml::tag('h4', $row['header']), ['class' => 'header']);
        // Дата
        $html[] = yiiHtml::tag('p', self::dateString($row['date']), yiiHtml::css([
            'font-size' => '70%',
            'color'     => '#808080',
        ]));
        // автор
        $author = '&nbsp;';
        if (($uid = ArrayHelper::getValue($row, 'user_id', '')) != '') {
            $user = UsersInCache::find($uid);
            $author = yiiHtml::a($user['name'], '/user/' . $uid);
        } else {
            if (($a = ArrayHelper::getValue($row, 'author_name', '')) != '') {
                $author = $a;
            }
            if (($a = ArrayHelper::getValue($row, 'author', '')) != '') {
                $author = $a;
            }
        }
        $html[] = yiiHtml::tag('p', 'Автор: ' . $author, ['class' => 'gsss3dot', 'style' => 'margin: 0px; font-size:80%;']);
        // силы
        $html[] = yiiHtml::tag('p', 'Высшие Силы: ' . ((ArrayHelper::getValue($row, 'power_name', '') != '')? $row['power_name'] : '&nbsp;'), ['class' => 'gsss3dot', 'style' => 'margin: 0px 0px 10px 0px; font-size:80%;']);

        // картинка с ссылкой
        $year = substr($row['date'], 0, 4);
        $month = substr($row['date'], 5, 2);
        $day = substr($row['date'], 8, 2);
        $html[] = yiiHtml::tag('p', yiiHtml::a(yiiHtml::img($row['img'], ['width' => '100%', 'class' => 'thumbnail']), "/chenneling/{$year}/{$month}/{$day}/{$row['id_string']}"));
        // Описание
        $html[] = yiiHtml::tag('p', $row['description'], ['style' => 'height: 150px;']);

        return yiiHtml::tag('div', join('', $html), ['class' => 'col-sm-4 chennelingItem']);
    }

    public static function articleItem($row, $category)
    {
        if (!is_array($row)) {
            $id = $row;
            $item = \app\models\Article::find($id);
            if (is_null($item)) return '';
            $row = $item->getFields();
        }
        // Заголовок
        $html[] = yiiHtml::tag('div', yiiHtml::tag('h4', $row['header']), ['class' => 'header']);
        // ссылка
        $item2 = [
            'id'       => $row['id_string'],
            'year'     => substr($row['date_insert'], 0, 4),
            'month'    => substr($row['date_insert'], 5, 2),
            'day'      => substr($row['date_insert'], 8, 2),
            'category' => $category,
        ];
        $link = "/category/{$item2['category']}/article/{$item2['year']}/{$item2['month']}/{$item2['day']}/{$item2['id']}";

        // картинка с ссылкой
        $html[] = yiiHtml::tag('p', yiiHtml::a(yiiHtml::img($row['image'], [
            'width' => '100%',
            'class' => 'thumbnail'
        ]), $link));
        // Описание
        $content = $row['description'];
        if ($content . '' == '') {
            $content = self::getMiniText($row['content']);
        } else {
            $content = self::getMiniText($content);
        }
        $html[] = yiiHtml::tag('p', $content);

        return yiiHtml::tag('div', join('', $html), ['class' => 'col-lg-4 articleItem']);
    }

    /**
     * Возвращает дату в формате "d M Y г." например "1 Дек 2015 г."
     *
     * @param $date 'yyyy-mm-dd'
     *
     * @return string
     */
    public static function dateString($date)
    {
        if (is_null($date)) return '';
        if ($date == '') return '';
        if (!Str::isContain($date, '-')) $date = date('Y-m-d', $date);

        $year = substr($date, 0, 4);
        $month = substr($date, 5, 2);
        $day = substr($date, 8, 2);
        if (substr($month, 0, 1) == '0') $month = substr($month, 1, 1);
        if (substr($day, 0, 1) == '0') $day = substr($day, 1, 1);
        $monthList = [
            1  => 'Янв',
            2  => 'Фев',
            3  => 'Мар',
            4  => 'Апр',
            5  => 'Май',
            6  => 'Июн',
            7  => 'Июл',
            8  => 'Авг',
            9  => 'Сен',
            10 => 'Окт',
            11 => 'Ноя',
            12 => 'Дек',
        ];
        $month = $monthList[$month];

        return "{$day} {$month} {$year} г.";
    }

    /**
     * Создает краткое содержимое текста, которое может быть также и HTML.
     * Удаляет все теги, заменяет спец символы &...; на символы текстовые и обрезает до нужной длины строку.
     * Если она превышает `$len` то в конце возвращаемой строки будет добавлено многоточие '...'
     *
     * @param string $text
     * @param int $len
     *
     * @return string plain text
     */
    public static function getMiniText($text, $len = 200)
    {
        $htmlSpecial = [
            '&nbsp;'  => ' ',
            '&#8212;' => '—',
            '&ndash;' => '—',
            '&mdash;' => '—',
            '&laquo;' => '«',
            '&raquo;' => '»',
        ];
        $strip = strip_tags($text);
        foreach ($htmlSpecial as $code => $replaceChar) {
            $strip = str_replace($code, $replaceChar, $strip);
        }
        $strip = trim($strip);
        if (Str::length($strip) > $len) {
            return Str::sub($strip, 0, $len) . ' ...';
        }

        return $strip;
    }

    /**
     * Создает краткое содержимое текста
     *
     * @param string $text
     * @param int $len
     *
     * @return string plain text
     */
    public static function getFirstText($text, $len = 200)
    {
        $strip = trim($text);
        if (Str::length($strip) > $len) {
            return Str::sub($strip, 0, $len) . ' ...';
        }

        return $strip;
    }

    public static function getHtml($text)
    {
        $rows = explode("\n", $text);
        $rows2 = [];
        foreach ($rows as $row) {
            $arr = explode(' ', $row);
            $arr2 = [];
            foreach ($arr as $word) {
                $w = trim($word);
                if (StringHelper::startsWith($w, 'https://') or StringHelper::startsWith($w, 'http://') or StringHelper::startsWith($w, 'blob:http')) {
                    $w = \yii\helpers\Html::a($w, $w, ['target' => '_blank']);
                    $arr2[] = $w;
                } else {
                    $arr2[] = \yii\helpers\Html::encode($w);
                }
            }
            $rows2[] = join(' ', $arr2);
        }

        return join('<br>', $rows2);
    }
}