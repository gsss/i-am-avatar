<?php

/**
 * Правило хорошего тона создания ролей и правил (разрешений)
 * Разрешение создается с символом '2' в конце
 * а название роли повторяет его и содержит только одно разрешение, это простая роль
 *
 * составные роли отдельно пишутся в конце и состоят из других ролей
 */
return [

    // разрешения (permission)
    'permission_admin'         => [
        'type'        => 2,
        'description' => 'admin',
    ],
    'permission_admin_command'         => [
        'type'        => 2,
        'description' => 'Управление анкетами команды',
    ],
    'permission_partner-admin' => [
        'type'        => 2,
        'description' => 'admin Партнерской программы',
    ],
    'permission_qr-code-admin' => [
        'type'        => 2,
        'description' => 'Управлене кодами',
    ],
    'permission_developer' => [
        'type'        => 2,
        'description' => 'Архитектор матрицы',
    ],
    'permission_languages-admin' => [
        'type'        => 2,
        'description' => 'Администратор языков',
    ],
    'permission_languages' => [
        'type'        => 2,
        'description' => 'Редактор языков',
    ],
    'permission_team' => [
        'type'        => 2,
        'description' => 'Команда',
    ],
    'permission_project-manager' => [
        'type'        => 2,
        'description' => 'Project Manager',
    ],


    // простыре роли
    'role_project-manager'               => [
        'type'        => 1,
        'description' => 'Project Manager',
        'children'    => [
            'permission_project-manager',
        ],
    ],
    'role_admin'               => [
        'type'        => 1,
        'description' => 'admin',
        'children'    => [
            'permission_admin',
        ],
    ],
    'role_admin_command'               => [
        'type'        => 1,
        'description' => 'Управление анкетами команды',
        'children'    => [
            'permission_admin_command',
        ],
    ],
    'role_partner-admin'       => [
        'type'        => 1,
        'description' => 'admin Партнерской программы',
        'children'    => [
            'permission_partner-admin',
        ],
    ],
    'role_qr-code-admin'       => [
        'type'        => 1,
        'description' => 'Управлене кодами',
        'children'    => [
            'permission_qr-code-admin',
        ],
    ],
    'role_languages-admin'       => [
        'type'        => 1,
        'description' => 'Администратор языков',
        'children'    => [
            'permission_languages-admin',
        ],
    ],
    'role_languages'       => [
        'type'        => 1,
        'description' => 'Редактор языков',
        'children'    => [
            'permission_languages',
        ],
    ],
    'role_developer'       => [
        'type'        => 1,
        'description' => 'Архитектор матрицы',
        'children'    => [
            'permission_developer',
        ],
    ],
    'role_team'       => [
        'type'        => 1,
        'description' => 'Команда',
        'children'    => [
            'permission_team',
        ],
    ],
];
