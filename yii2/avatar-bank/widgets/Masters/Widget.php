<?php
/**
 * Created by PhpStorm.
 * User: s.arhangelskiy
 * Date: 27.09.2016
 * Time: 17:24
 */

namespace avatar\widgets\Masters;

use cs\services\VarDumper;
use yii\base\Event;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\web\View;

/**
 * Class Widget
 *
 * @url     https://www.i-am-avatar.com/admin-developer/widget-masters
 * @package avatar\widgets\Masters
 */
class Widget extends \yii\widgets\InputWidget
{
    /** @var  \common\models\school\School */
    public $school;

    public function run()
    {
        $attribute = $this->attribute;
        $html = \Yii::$app->view->renderFile('@avatar/widgets/Masters/view.php', ['school' => $this->school,'model' => $this->model, 'attribute' => $attribute]);

        return $html;
    }
}