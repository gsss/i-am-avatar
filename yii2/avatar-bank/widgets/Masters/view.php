<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use avatar\assets\App\Asset;

/* @var $school \common\models\school\School */
/* @var $model \avatar\models\forms\shop\EventAdd\Step4 */
/* @var $attribute string */


$idAvaliable = Html::getInputId($model, $attribute) . '-avaliable';
$idChoosen = Html::getInputId($model, $attribute) . '-choosen';
$idInput = Html::getInputId($model, $attribute);

$this->registerJs(<<<JS

var widgetMastersFunctionRemove = function(e) {
    var id = $(this).data('id');
    var object = $('#{$idChoosen} .widgetMastersUser[data-id='+id+']');

    // ищу объект для перемещения
    object.find('.buttonChoose').html('Выбрать').off('click').on('click', widgetMastersFunctionAdd);
    // Добавляю
    $('#{$idAvaliable}').append(object);
    // Удаляю
    $('#{$idChoosen} .widgetMastersUser[data-id='+id+']').remove();

    // Выставляю поле
    var ids = $('#{$idInput}').val();
    var idsArray = ids.split(',');
    console.log(idsArray);
    var newIds = [];
    for(i=0;i<idsArray.length;i++) {
        if (idsArray[i] != id) {
            newIds.push(idsArray[i]);
        }
    }
    $('#{$idInput}').val(newIds.join(','));
};
var widgetMastersFunctionAdd = function(e) {
    var id = $(this).data('id');
    var object = $('#{$idAvaliable} .widgetMastersUser[data-id='+id+']');

    // снимаю ошибку если она есть
    var group = object.closest('.form-group');
    group.removeClass('has-error');
    group.find('.help-block-error').hide();

    // ищу объект для перемещения
    object.find('.buttonChoose').html('Удалить').off('click').on('click', widgetMastersFunctionRemove);
    // Добавляю
    $('#{$idChoosen}').append(object);
    // Удаляю
    $('#{$idAvaliable} .widgetMastersUser[data-id='+id+']').remove();

    // Выставляю поле
    var ids = $('#{$idInput}').val();
    if (ids == '') ids = id;
    else ids = ids + ',' + id;
    $('#{$idInput}').val(ids);
};

$('#{$idAvaliable} .buttonChoose').click(widgetMastersFunctionAdd);
JS
);

?>
<div class="row">
    <div class="col-lg-5">
        <p class="media-heading">Возможные</p>
        <div class="media" id="<?= $idAvaliable ?>">
            <?php /** @var \common\models\school\TeacherLink $link */ ?>
            <?php foreach (\common\models\school\TeacherLink::find()->where(['school_id' => $school->id])->all() as $link) { ?>
                <?php $user = \common\models\UserAvatar::findOne($link->user_id); ?>

                <div class="widgetMastersUser" data-id="<?= $user->id ?>">
                    <div class="media-left">
                        <a href="#">
                            <img class="media-object img-circle" src="<?= $user->getAvatar() ?>" alt="..." width="50" title="<?= $user->getName2() ?>" data-toggle="tooltip">
                        </a>
                    </div>
                    <div class="media-body">
                        <h4 class="media-heading"><?= $user->getName2() ?></h4>
                        <p>
                            <a href="javascript: void(0);" class="btn btn-default buttonChoose" style="width: 100%;" data-id="<?= $user->id ?>">Выбрать</a>
                        </p>
                    </div>
                </div>
            <?php } ?>
        </div>
    </div>
    <div class="col-lg-2">
        <p><span class="glyphicon glyphicon-chevron-right"></span></p>
    </div>
    <div class="col-lg-5" id="<?= $idChoosen ?>">
        <p class="media-heading">Выбранные</p>
        <div class="media" id="<?= $idChoosen ?>">

        </div>
    </div>
</div>
<?= Html::hiddenInput(Html::getInputName($model, $attribute), null, ['id' => $idInput]) ?>