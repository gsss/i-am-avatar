<?php
/**
 * Created by PhpStorm.
 * User: s.arhangelskiy
 * Date: 27.09.2016
 * Time: 17:24
 */

namespace avatar\widgets;

use common\widgets\FileUpload7\FileUpload;
use cs\services\VarDumper;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;

class PaySystemList extends \yii\widgets\InputWidget
{
    /** @var  \common\models\PaySystemConfig[] */
    public $rows;

    public function run()
    {
        $attribute = $this->attribute;
        \Yii::$app->session->set('\avatar\widgets\PaySystemList', Html::getInputName($this->model, $attribute));

        $id = Html::getInputId($this->model, $attribute);
        \Yii::$app->view->registerJS(<<<JS
        
// Если в таблице одна строка то выделяю ее сразу    
if ($('.field-'+'{$id}'+' .rowTable').length == 1) {
    var i = $('.field-'+'{$id}'+' .rowTable')[0];
    $(i).parent().find('input').removeProp('checked');
    $(i).find('input').prop('checked', 'checked');
};

// На клик вешаю событие включение RADIOBUTTON    
$('.field-'+'{$id}'+' .rowTable').click(function() {
    var id =  $(this).data('id');
    $(this).parent().find('input').removeProp('checked');
    $(this).find('input').prop('checked', 'checked');
});
JS
        );

        return \yii\grid\GridView::widget([
            'dataProvider' => new \yii\data\ActiveDataProvider([
                'query'      => $this->rows
                ,
                'pagination' => [
                    'pageSize' => 20,
                ],
            ]),
            'tableOptions' => [
                'class' => 'table table-striped table-hover',
            ],
            'summary' => '',
            'rowOptions'   => function ($item) {
                $data = [
                    'data'  => ['id' => $item['id']],
                    'role'  => 'button',
                    'class' => 'rowTable'
                ];
                return $data;
            },
            'columns'      => [
                [
                    'header'  => '',
                    'content' => function (\common\models\PaySystemConfig $item) {
                        return Html::radio(\Yii::$app->session->get('\avatar\widgets\PaySystemList'), false, ['value' => $item->id]);
                    }
                ],
                [
                    'header'  => 'Картинка',
                    'content' => function (\common\models\PaySystemConfig $item) {
                        $ps = $item->getPaySystem();
                        $i = $ps->image;
                        if ($i == '') return '';

                        return Html::img(FileUpload::getFile($i, 'crop'), [
                            'class'  => "thumbnail",
                            'width'  => 80,
                            'height' => 80,
                            'style'  => 'margin-bottom: 0px;',
                        ]);
                    }
                ],

                [
                    'header'  => 'Валюта',
                    'content' => function (\common\models\PaySystemConfig $item) {
                        $ps = $item->getPaySystem();
                        $currency = $ps->getCurrencyObject();
                        if (is_null($currency)) return '';

                        return Html::tag('span', $currency->code, ['class' => 'label label-info']);
                    },
                ],
                [
                    'header'  => 'title',
                    'content' => function (\common\models\PaySystemConfig $item) {
                        $ps = $item->getPaySystem();

                        return $ps->title;
                    },
                ],
            ],
        ]);
    }
}