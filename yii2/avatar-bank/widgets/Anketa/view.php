<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use avatar\assets\App\Asset;

/* @var $school \common\models\school\School */
/* @var $model \avatar\models\forms\shop\EventAdd\Step4 */
/* @var $attribute string */


$idInput = Html::getInputId($model, $attribute);

$this->registerJs(<<<JS

var functionWidgetAnketaDelete = function (e) {
    
    $(this).parent().remove();
   
    // Выставляю поле
    var data = [];
    $('.list-group-item[data-text]').each(function(i,o) {
        data.push($(o).data('text'));
    }, []);
    jData = JSON.stringify(data);
    $('#{$idInput}').val(jData);
};

$('.field-{$idInput} .widgetAnketaButtonAdd').click(function(e) {
    var o = $(this).closest('.col-lg-6').find('.form-control');
    var text = o.val();
    var oLi = $('<li>', {class: 'list-group-item', 'data-text': text});
    oLi.append(text);
    oLi.append($('<a>', {
            href: 'javascript: void(0);',
            class: 'btn btn-default btn-xs',
            style: 'margin-left: 20px;'
            }).on('click', functionWidgetAnketaDelete).html('Убрать')
        );
    $('.field-{$idInput} .widgetAnketaList').append(oLi);
    
    // Очищаю значение
    o.val('');
    
    // Выставляю поле
    var data = [];
    $('.list-group-item[data-text]').each(function(i,o) {
        data.push($(o).data('text'));
    }, []);
    jData = JSON.stringify(data);
    $('#{$idInput}').val(jData);
})
JS
);

?>
<div class="row">
    <div class="col-lg-6">
        <p class="media-heading">Список</p>
        <div class="input-group">
            <input type="text" class="form-control" placeholder="Добавьте название поля...">
            <span class="input-group-btn">

            <a href="javascript: void(0);" class="btn btn-default widgetAnketaButtonAdd">Добавить</a>
      </span>
        </div><!-- /input-group -->
    </div>
    <div class="col-lg-6">
        <p class="media-heading">Список</p>
        <ul class="list-group widgetAnketaList">
            <li class="list-group-item">Имя</li>
            <li class="list-group-item">Почта</li>
            <li class="list-group-item">Телефон</li>
        </ul>
    </div>
</div>
<?= Html::hiddenInput(Html::getInputName($model, $attribute), '[]', ['id' => $idInput]) ?>