<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use avatar\assets\App\Asset;

/* @var $school \common\models\school\School */
/* @var $model \avatar\models\forms\shop\EventAdd\Step4 */
/* @var $attribute string */


$idInput = Html::getInputId($model, $attribute);



$this->registerJs(<<<JS

var functionWidgetAnketaDelete = function (e) {
    
    $(this).parent().remove();
   
    // Выставляю поле
    var data = [];
    $('.list-group-item[data-text]').each(function(i,o) {
        data.push($(o).data('text'));
    }, []);
    var jData = JSON.stringify(data);
    $('#{$idInput}').val(jData);
};

$('.field-{$idInput} .widgetTicketsButtonAdd').click(function(e) {
    var oCount = $(this).closest('.col-lg-6').find('.fieldCount');
    var oDescription = $(this).closest('.col-lg-6').find('.fieldDescription');
    var oName = $(this).closest('.col-lg-6').find('.fieldName');
    var oPrice = $(this).closest('.col-lg-6').find('.fieldPrice');
    var count = '';
    var name = oName.val();
    var description = oDescription.val();
    var price = oPrice.val();
    var oLi = $('<li>', {class: 'list-group-item', 'data-count': count, 'data-price': price});
    var text = '';
    if (count == '') {
        text = price + ' руб.';
    } else {
        text = count + ' шт * ' + price + ' руб.';
    }
    oLi.append(text);
    
    oLi.append($('<a>', {
            href: 'javascript: void(0);',
            class: 'btn btn-default btn-xs',
            style: 'margin-left: 20px;'
            }).on('click', functionWidgetAnketaDelete).html('Убрать')
        );
    $('.field-{$idInput} .widgetAnketaList').append(oLi);
    
    // Очищаю значение
    oName.val('');
    oDescription.val('');
    oPrice.val('');
    
    // Выставляю поле
    var data = [];
    $('.field-{$idInput} .list-group-item').each(function(i,o) {
        data.push({
        'name': name, 
        'description': description, 
        'price': price
        });
    }, []);
    var jData = JSON.stringify(data);
    $('#{$idInput}').val(jData);
})
JS
);

?>
<div class="row">
    <div class="col-lg-6">
        <p class="media-heading">Добавление нового</p>

<!--        <div class="form-group field-step1-name required">-->
<!--            <input type="text" class="form-control fieldCount" placeholder="Кол-во...">-->
<!--            <p>Если кол-во не ограничено, то оставьте поле пустым</p>-->
<!--        </div>-->
        <div class="form-group field-step1-name required">
            <input type="text" class="form-control fieldName" placeholder="Наименование...">
        </div>
        <div class="form-group field-step1-description required">
            <textarea type="text" class="form-control fieldDescription" placeholder="Описание..." rows="5"></textarea>
        </div>

        <div class="input-group">
            <input type="text" class="form-control fieldPrice" placeholder="Стоимость...">
            <span class="input-group-btn">

            <a href="javascript: void(0);" class="btn btn-default widgetTicketsButtonAdd">Добавить</a>
            </span>
        </div><!-- /input-group -->
        <p>Введите целое число</p>
    </div>
    <div class="col-lg-6">
        <p class="media-heading">Список</p>
        <ul class="list-group widgetAnketaList">
        </ul>
    </div>
</div>
<?= Html::hiddenInput(Html::getInputName($model, $attribute), '[]', ['id' => $idInput]) ?>