<?php
/**
 * Created by PhpStorm.
 * User: s.arhangelskiy
 * Date: 27.09.2016
 * Time: 17:24
 */

namespace avatar\widgets;

use cs\services\VarDumper;
use Yii;
use yii\base\Widget;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;

class SchoolMenu extends Widget
{
    public $school;

    public function init()
    {
        parent::init();
        ob_start();
    }

    public function run()
    {
        $content = ob_get_clean();
        $menu = $this->render('@avatar/views/cabinet-school/_menu2', ['school' => $this->school,'content' => $content]);

        return $menu;
    }


    /**
     * @param array $item Текущая на вывод элемент списка
     *
     * @return bool
     */
    public static function isActive3($item)
    {
        if (isset($item['items'])) {
            foreach ($item['items'] as $i) {
                if (isActive3($i)) {
                    return true;
                }
            }
        }
        $route = Yii::$app->requestedRoute;
        $uriCurrent = Url::current();
        if (\yii\helpers\ArrayHelper::keyExists('urlList', $item)) {
            foreach ($item['urlList'] as $i) {
                switch ($i[0]) {
                    case 'route':
                        if ($route == $i[1]) return true;
                        break;
                    case 'startsWith':
                        if (\yii\helpers\StringHelper::startsWith($uriCurrent, $i[1])) return true;
                        break;
                    case 'controller':
                        $arr = explode('/', $route);
                        if ($arr[0] == $i[1]) return true;
                        break;
                }
            }
        }

        return false;
    }
}