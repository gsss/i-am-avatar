<?php
/**
 * Created by PhpStorm.
 * User: s.arhangelskiy
 * Date: 27.09.2016
 * Time: 17:24
 */

namespace avatar\widgets\TaskTable;

use cs\services\VarDumper;
use yii\base\Event;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\web\View;

/**
 */
class TaskTable extends \yii\base\Widget
{
    /** @var  \common\models\school\School */
    public $school;

    public function run()
    {
        $attribute = $this->attribute;
        $html = \Yii::$app->view->renderFile('@avatar/widgets/Masters/view.php', [
            'school'    => $this->school,
            'model'     => $this->model,
            'attribute' => $attribute,
        ]);

        return $html;
    }
}