<?php

namespace avatar\widgets;

use common\models\piramida\Currency;
use cs\Application;
use cs\services\File;
use cs\services\SitePath;

use Imagine\Image\Box;
use Yii;
use yii\base\InvalidConfigException;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\helpers\Html;
use yii\helpers\Json;
use yii\helpers\VarDumper;
use yii\httpclient\Client;
use yii\widgets\InputWidget;
use yii\web\UploadedFile;
use yii\imagine\Image;
use Imagine\Image\ManipulatorInterface;
use cs\base\BaseForm;
use cs\services\UploadFolderDispatcher;
use \yii\helpers\FormatConverter;
use \yii\jui\JuiAsset;

/**
 */
class Price extends \yii\jui\InputWidget
{
    /** @var Currency */
    public $currency;

    public function run()
    {
        return Html::activeTextInput($this->model, $this->attribute, ['class' => 'form-control']);
    }

    public function onAfterLoadDb($field)
    {
        $model = $this->model;
        $attribute = $this->attribute;
        $model->$attribute = Currency::getValueFromAtom($model->$attribute, $this->currency->id);
    }

    public function onBeforeUpdate($field)
    {
        $model = $this->model;
        $attribute = $this->attribute;
        $model->$attribute = Currency::getAtomFromValue($model->$attribute, $this->currency->id);
    }

    public function onBeforeInsert($field)
    {
        $model = $this->model;
        $attribute = $this->attribute;
        $model->$attribute = Currency::getAtomFromValue($model->$attribute, $this->currency->id);
    }
}
