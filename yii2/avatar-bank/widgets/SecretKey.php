<?php
/**
 * Created by PhpStorm.
 * User: s.arhangelskiy
 * Date: 27.09.2016
 * Time: 17:24
 */

namespace avatar\widgets;

use cs\services\VarDumper;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;

class SecretKey extends \yii\widgets\InputWidget
{
    public function run()
    {
        $attribute = $this->attribute;
        $strLen = strlen($this->model->$attribute);

        $rows = [];

        $rows[] = Html::hiddenInput(Html::getInputName($this->model, $this->attribute), $this->model->$attribute, ['id' => Html::getInputId($this->model, $this->attribute)]);
        $rows[] = Html::input('text', null, str_repeat('*', $strLen), [
            'class'     => 'form-control',
            'id'        => Html::getInputId($this->model, $this->attribute) . '-text',
            'data'  => [
                'value'  => $this->model->$attribute,
                'hidden' => str_repeat('*', $strLen),
            ],
        ]);
        $rows[] = Html::tag(
            'span',
            Html::button(Html::tag('i', null, ['class' => 'glyphicon glyphicon-eye-open']), ['class' => 'btn btn-default', 'id' => Html::getInputId($this->model, $this->attribute) . '-button']),
            ['class' => 'input-group-btn']
        );
        $id0 = Html::getInputId($this->model, $this->attribute);
        $id1 = Html::getInputId($this->model, $this->attribute) . '-text';
        $id2 = Html::getInputId($this->model, $this->attribute) . '-button';
        \Yii::$app->view->registerJs(<<<JS
$('#{$id2}').click(function(e) {
    var i0 = $('#{$id0}');
    var i1 = $('#{$id1}');
    var i2 = $(this).find('i');
    if (i2.hasClass('glyphicon-eye-open')) {
        i2.attr('class', 'glyphicon glyphicon-eye-close');
        i1.val(i0.val());
    } else {
        i2.attr('class', 'glyphicon glyphicon-eye-open');
        i1.val('***');
    }
});
$('#{$id1}').on('input', function(e) {
    var i0 = $('#{$id0}');
    i0.val($(this).val());
});
JS
        );

        return Html::tag('div', join('', $rows), ['class' => 'input-group']);
    }
}