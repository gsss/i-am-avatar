<?php
require(__DIR__ . '/../../helpers/helpers.php');
require(__DIR__ . '/../../common/config/bootstrap.php');

$dotenv = Dotenv\Dotenv::createImmutable(__DIR__ . '/../../');
$dotenv->load();

$config = \yii\helpers\ArrayHelper::merge(
    require(__DIR__ . '/../../common/config/main.php'),
    require(__DIR__ . '/main.php')
);

return $config;
