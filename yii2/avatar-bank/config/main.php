<?php

$params = \yii\helpers\ArrayHelper::merge(
    require(__DIR__ . '/../../common/config/params.php'),
    require(__DIR__ . '/params.php')
);

$config = [
    'id'                                              => 'avatar-root',
    'basePath'                                        => dirname(__DIR__),
    'bootstrap'                                       => ['log'],
    'controllerNamespace'                             => 'avatar\controllers',
    'vendorPath'                                      => dirname(dirname(__DIR__)) . '/vendor',
    'language'                                        => 'ru',
    'sourceLanguage'                                  => 'ru',
    'timeZone'                                        => 'Etc/GMT-3',
    'aliases'                                         => [
        '@csRoot'  => __DIR__ . '/../../common/app',
        '@cs'      => __DIR__ . '/../../common/app',
        '@webroot' => __DIR__ . '/../../public_html',
    ],
    'components'                                      => [
        'session'         => [
            'timeout' => 600,
            'class'   => 'yii\web\CacheSession',
            'cache'   => 'cacheSession',
        ],
        'urlManager' => [
            'rules' => [
                '/'                                                        => 'site/index',
                'telegram'                                                 => 'telegram/index',
                'partner'                                                  => 'partner/index',
                'ru'                                                       => 'ru/index',
                'en'                                                       => 'en/index',
                'documents'                                                => 'documents/index',
                'chat/<action>'                                            => 'chat/site/<action>',

                // блог
                'blog'                                                     => 'blog/index',
                'blog/<year:\\d{4}>/<month:\\d{2}>/<day:\\d{2}>/<id:\\w+>' => 'blog/item',
                'blog/<action>'                                            => 'blog/<action>',

                // карточка пользователей
                'user/<id:\\d+>'                                           => 'user/item',

                // карточка школы
                'school/<id:\\d+>'                                         => 'school/item',
                'kurs/<id:\\d+>'                                           => 'kurs/item',

                'upload/HtmlContent2_common'       => 'upload/upload',
                'upload3/upload'                   => 'upload3/upload',
                'upload4/<action>'                 => 'upload4/<action>',
                'partner/registration/<code:\\d+>' => 'partner/registration',

                'page/<controller>'              => 'page/controller',
                'page/<controller>/<action>'     => 'page/controller-action',
                'gridview/<controller>/<action>' => 'gridview/<controller>/<action>',


                ['class' => '\avatar\services\CompanyUrlRule'],
//                '<controller>/<action>'           => '<controller>/<action>',
//                '<module>/<controller>/<action>' => '<module>/<controller>/<action>',
            ],

        ],
        'languageManager' => [
            'class'    => 'common\components\LanguageManager',
            'strategy' => [
                'ru' => ['ru', 'be', 'uk'],
                'en' => ['en', 'fr', 'de'],
            ],
            'default' => 'ru',
        ],
        'authManager'     => [
            'class' => 'yii\rbac\PhpManager',
        ],
        'assetManager' => [
            'appendTimestamp' => true,
            'basePath'        => '@avatar/../public_html/assets',
            'bundles' => [
                'yii\web\JqueryAsset' => [
                    'sourcePath' => null,
                    'js'         => ['/admin/vendors/jquery/dist/jquery.min.js'],
//                    'jsOptions' => ['position' => \yii\web\View::POS_HEAD],
                ],
                '\yii\web\JqueryAsset' => [
                    'sourcePath' => null,
                    'js'         => ['/admin/vendors/jquery/dist/jquery.min.js'],
//                    'jsOptions' => ['position' => \yii\web\View::POS_HEAD],
                ],
            ],
        ],
        'request'         => [
            // !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
            'cookieValidationKey'    => '',
            'enableCookieValidation' => false,
        ],
        'deviceDetect'    => [
            'class'     => 'avatar\services\DeviceDetect',
            'setParams' => 'false',
        ],
        'user'            => [
            'identityClass'   => 'common\models\UserAvatar',
            'authTimeout'     => 60*60*24*30,
            'enableAutoLogin' => true,
            'loginUrl'        => ['auth/login'],
        ],
        'errorHandler'    => [
            'errorAction' => 'site/error',
            'errorView'   => '@avatar/views/site/error.php',
        ],
        'formatter'       => [
            'class'             => '\iAvatar777\components\FormatterRus\FormatterRus',
            'dateFormat'        => 'php:d.m.Y/b',
            'timeFormat'        => 'php:H:i:s',
            'datetimeFormat'    => 'php:d.m.Y/b H:i',
            'decimalSeparator'  => '.',
            'thousandSeparator' => ',',
            'currencyCode'      => 'RUB',
            'locale'            => 'ru',
            'nullDisplay'       => '',
        ],
        'log'              => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets'    => [
                [
                    'class'       => '\yii\log\FileTarget',
                    'levels'      => [
                        'warning',
                        'error',
                    ],
                    'except'      => [
                        'yii\web\HttpException:404',
                        'yii\web\HttpException:403',
                        'cs\web\Exception',
                    ],
                ],
                [
                    'class'       => 'common\services\DbTarget',
                    'levels'      => [
                        'warning',
                        'error',
                    ],
                    'except'      => [
                        'yii\web\HttpException:404',
                        'yii\web\HttpException:403',
                        'cs\web\Exception',
                    ],

                    'db'         => 'dbStatistic',
                    'logTable'   => 'log2_avatar',
                ],

                [
                    'class'      => 'common\services\DbTarget',
                    'categories' => ['avatar\\*'],
                    'levels'     => [
                        'info',
                        'trace',
                    ],

                    'db'         => 'dbStatistic',
                    'logTable'   => 'log2_avatar',
                ],
            ],
        ],
    ],
    'modules' => [
        'gridview' =>  [
            'class' => '\kartik\grid\Module'
            // enter optional module parameters below - only if you need to
            // use your own export download action or custom translation
            // message source
            // 'downloadAction' => 'gridview/export/download',
            // 'i18n' => []
        ],
        'chat' =>  [
            'class' => '\avatar\modules\chat\Module'
        ],
        'treemanager' =>  [
            'class' => '\kartik\tree\Module',
            // other module settings, refer detailed documentation
        ]
    ],
    'params'                                          => $params,
    'controllerMap'                                   => [
        'upload'  => '\common\widgets\HtmlContent\Controller',
        'upload3' => '\common\widgets\FileUploadMany2\UploadController',
        'upload4' => '\iAvatar777\assets\JqueryUpload1\Upload2Controller',
    ],
    'on beforeRequest'                                => function ($event) {
        Yii::$app->languageManager->onBeforeRequest();
        Yii::$app->onlineManager->onBeforeRequest();
        Yii::$app->monitoring->onBeforeRequest();
        if (!YII_ENV_DEV) \common\models\statistic\ServerRequest::add();

        // сохраняю параметр parent_id для партнерки
        \avatar\modules\Shop\Shop::onBeforeRequest();

//        $s = \common\models\school\School::get();
//        if ($s->id != 2) {
//            Yii::$app->urlManager->hostInfo = $s->getUrl();
//        }
    },
    'on ' . \yii\web\Application::EVENT_AFTER_REQUEST => function ($event) {
        Yii::$app->monitoring->onAfterRequest();
    }
];


if (env('MEMCACHE_ENABLED', false)) {
    $config['components']['cacheSession'] = [
        'keyPrefix'    => 'session',
        'class'        => 'yii\caching\MemCache',
        'useMemcached' => true,
        'servers'      => [
            [
                'host' => env('MEMCACHE_HOST', 'localhost'),
                'port' => env('MEMCACHE_PORT', 11211),
            ],
        ],
    ];
} else {
    $config['components']['cacheSession'] = [
        'keyPrefix'    => 'session',
        'class'        => 'yii\caching\FileCache',
    ];
}

if (YII_DEBUG) {
    // configuration adjustments for 'dev' environment
    $config['bootstrap'][] = 'debug';
    $config['modules']['debug'] = [
        'class'      => 'yii\debug\Module',
        'allowedIPs' => ['*'],
    ];
    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = [
        'class' => 'yii\gii\Module',
    ];
}

return $config;
