<?php
/**
 * Created by PhpStorm.
 * User: Ramha
 * Date: 09.06.2018
 * Time: 19:52
 */

return [
    'cabinet.new-earth.space' => [
        'domain-name' => 'cabinet.new-earth.space',
        'logo'        => [
            'small' => '/images/controller/landing-new-earth/index/transparent.png',
            'big'   => '/images/controller/landing-new-earth/index/transparent.png',
        ],
        'contact'     => [
            'mail' => ['Проект «РАЙ»' => 'sacred-home@new-earth.space'],
        ],
        'is-blog'     => 1,
        'is-news'     => 1,
        'landing'     => [
            'layout' => 'landing-new-earth',
            'view'   => '/landing-new-earth/index',
        ],
        'footer'      => [
            'mail'        => 'sacred-home@new-earth.space',
            'name'        => 'Проект «РАЙ»',
            'social-nets' => [
                [
                    'type' => 'telegram',
                    'url'  => 'https://t.me/cityoflight2018',
                ],
                [
                    'type' => 'facebook',
                    'url'  => 'https://www.facebook.com/paradiseuniverse999/',
                ],
                [
                    'type' => 'vk',
                    'url'  => 'https://vk.com/public166290693',
                ],
                [
                    'type' => 'skype',
                    'url'  => 'https://join.skype.com/eH2TP4IwEZwX',
                ],
            ],
        ],
    ],
];