<?php

namespace avatar\controllers;

use avatar\models\forms\UserAvatar;
use avatar\models\validate\CardNumberAccept;
use avatar\modules\ETH\ServiceEtherScan;
use avatar\modules\ETH\ServiceEthPlorer;
use common\components\Card;
use common\components\providers\BTC;
use common\components\providers\ETH;
use common\models\avatar\Currency;
use common\models\avatar\UserBill;
use common\models\Config;
use common\models\school\Kurs;
use common\models\school\Lesson;
use common\models\school\Master;
use common\models\school\School;
use common\models\UserSeed;
use common\payment\BitCoinBlockTrailPayment;
use common\services\Security;
use console\controllers\MoneyRateController;
use cs\services\VarDumper;
use cs\web\Exception;
use cs\Widget\FileUpload4\ModelFields;
use Yii;
use yii\helpers\FileHelper;
use yii\helpers\Json;
use yii\helpers\StringHelper;
use yii\httpclient\Client;
use yii\imagine\Image;
use yii\web\Response;

class CabinetSchoolPlugOutController extends CabinetSchoolBaseController
{

    /**
     * @param int $id идентификатор школы
     *
     * @return string
     */
    public function actionIndex($id)
    {
        $school = School::findOne($id);

        return $this->render(['school' => $school]);
    }

    /**
     * @param int $id идентификатор школы
     *
     * @return string
     */
    public function actionAdd($id)
    {
        $model = new \common\models\school\PluginExportSettings();
        $school = School::findOne($id);

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            Yii::$app->session->setFlash('form', 1);
            $model->school_id = $school->id;
            $model->save();
        }

        return $this->render([
            'model' => $model,'school' => $school
        ]);
    }


    /**
     * @param int $id school_out_plugin_settings.id
     *
     * @return string
     */
    public function actionEdit($id)
    {
        $model = \common\models\school\PluginExportSettings::findOne($id);
        $school = School::findOne($model->school_id);

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $model->save();
            Yii::$app->session->setFlash('form', 1);
        }

        return $this->render([
            'model' => $model, 'school' => $school
        ]);
    }


    public function actionDelete($id)
    {
        \common\models\school\PluginExportSettings::findOne($id)->delete();

        return self::jsonSuccess();
    }
}
