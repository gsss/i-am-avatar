<?php

namespace avatar\controllers;

use avatar\models\forms\Contact;
use avatar\models\validate\CabinetCardsControllerActionAddCard;
use avatar\modules\ETH\ServiceEtherScan;
use avatar\services\LogReader;
use common\components\providers\BTC;
use common\components\providers\ETH;
use common\components\sms\IqSms;
use common\models\avatar\Currency;
use common\models\avatar\UserBill;
use common\models\Card;
use common\models\PaymentBitCoin;
use common\models\UserAvatar;
use common\models\UserRegistration;
use common\models\UserSeed;
use common\payment\BitCoinBlockTrailPayment;
use common\services\Security;
use cs\Application;
use cs\base\BaseController;
use cs\services\Str;
use cs\services\UploadFolderDispatcher;
use cs\services\VarDumper;
use cs\web\Exception;
use Yii;
use yii\base\UserException;
use yii\db\Connection;
use yii\db\Query;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use yii\helpers\FileHelper;
use yii\helpers\Json;
use yii\helpers\Url;
use yii\httpclient\Client;
use yii\web\Response;
use Blocktrail\SDK\Services\BlocktrailBatchUnspentOutputFinder;
use Blocktrail\SDK\WalletV1Sweeper;

class CabinetCardsController extends \avatar\controllers\CabinetBaseController
{
    /**
     */
    public function actionIndex()
    {
        return $this->render('@avatar/views/' . $this->id . '/' . $this->action->id);
    }

    public function actions()
    {
        return [
            'pin-ajax' => [
                'class' => '\avatar\controllers\actions\DefaultAjax',
                'model' => '\avatar\models\validate\CabinetCardsControllerActionPinAjax',
            ],
        ];
    }

    /**
     * Выводит карту и ее счета
     *
     * @param int $id идентификатор карты
     *
     * @return string
     * @throws
     */
    public function actionPin($id)
    {
        $card = Card::findOne($id);
        if ($card->user_id != Yii::$app->user->id) {
            throw new Exception('Это не ваша карта');
        }

        return $this->render('@avatar/views/' . $this->id . '/' . $this->action->id, ['card' => $card]);
    }

    /**
     * Выводит карту и ее счета
     *
     * @param int $id идентификатор карты
     *
     * @return string
     * @throws
     */
    public function actionCard($id)
    {
        $card = Card::findOne($id);
        if ($card->user_id != Yii::$app->user->id) {
            throw new Exception('Это не ваша карта');
        }

        return $this->render('@avatar/views/' . $this->id . '/' . $this->action->id, ['card' => $card]);
    }

    /**
     * Выводит карту и ее счета
     *
     * @param int $id идентификатор карты
     *
     * @return string
     * @throws
     */
    public function actionAdd($id)
    {
        $card = Card::findOne($id);
        if (!UserBill::find()->where(['card_id' => $card->id, 'user_id' => Yii::$app->user->id])->exists()) {
            throw new Exception('Это не ваша карта');
        }

        return $this->render('@avatar/views/'.$this->id.'/'.$this->action->id, ['card' => $card]);
    }

    /**
     * присоединяет карту
     * REQUEST:
     * - number - string
     *
     * @return string
     * @throws
     */
    public function actionAddCard()
    {
        $number = self::getParam('number');
        $model = new CabinetCardsControllerActionAddCard(['number' => $number]);
        if (!$model->validate()) return self::jsonErrorId(102, $model->getErrors2());
        $model->action();

        return self::jsonSuccess();
    }

    /**
     * Отсоединяет счет от карты
     *
     * REQUEST:
     * + id int $id идентификатор счета user_bill.id
     *
     * @return string
     * 101, 'Нужен параметр ID'
     * 102, 'Не найден счет'
     * 103, 'Это не ваш счет'
     *
     * @throws
     */
    public function actionUnlink()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        $id = self::getParam('id');
        if (is_null($id)) {
            return self::jsonErrorId(101, 'Нужен параметр ID');
        }
        try {
            $billing = UserBill::findOne($id);
        } catch (\Exception $e) {
            return self::jsonErrorId(102, 'Не найден счет');
        }
        if ($billing->user_id != Yii::$app->user->id) {
            return self::jsonErrorId(103, 'Это не ваш счет');
        }
        $billing->card_id = null;
        $billing->save();

        return self::jsonSuccess();
    }

    /**
     * Присоединяет счет от карты
     *
     * REQUEST:
     * + billing_id int $id идентификатор счета user_bill.id
     * + card_id int $id идентификатор карты card.id
     *
     * @return string
     * 101, 'Нужен параметр billing_id'
     * 102, 'Нужен параметр card_id'
     * 103, 'Не найден счет'
     * 104, 'Это не ваш счет'
     *
     * @throws
     */
    public function actionLink()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        $billing_id = self::getParam('billing_id');
        if (is_null($billing_id)) {
            return self::jsonErrorId(101, 'Нужен параметр billing_id');
        }
        $card_id = self::getParam('card_id');
        if (is_null($card_id)) {
            return self::jsonErrorId(102, 'Нужен параметр card_id');
        }
        try {
            $billing = UserBill::findOne($billing_id);
        } catch (\Exception $e) {
            return self::jsonErrorId(103, 'Не найден счет');
        }
        if ($billing->user_id != Yii::$app->user->id) {
            return self::jsonErrorId(104, 'Это не ваш счет');
        }

        $billing->card_id = $card_id;
        $billing->save();

        return self::jsonSuccess();
    }
}
