<?php

namespace avatar\controllers;

use avatar\models\forms\UserAvatar;
use avatar\models\validate\CardNumberAccept;
use avatar\modules\ETH\ServiceEtherScan;
use avatar\modules\ETH\ServiceEthPlorer;
use common\components\Card;
use common\components\providers\BTC;
use common\components\providers\ETH;
use common\models\avatar\Currency;
use common\models\avatar\UserBill;
use common\models\Config;
use common\models\school\AdminLink;
use common\models\school\Kurs;
use common\models\school\Lesson;
use common\models\school\LessonPage;
use common\models\school\LessonPlace;
use common\models\school\LessonPotokState;
use common\models\school\LessonVideo;
use common\models\school\Master;
use common\models\school\Potok;
use common\models\school\School;
use common\models\school\Subscribe;
use common\models\school\SubscribeItem;
use common\models\subscribe\SubscribeMail;
use common\models\task\Task;
use common\models\UserRoot;
use common\models\UserSeed;
use common\payment\BitCoinBlockTrailPayment;
use common\services\Security;
use console\controllers\MoneyRateController;
use cs\Application;
use cs\services\VarDumper;
use cs\web\Exception;
use cs\Widget\FileUpload4\ModelFields;
use Yii;
use yii\helpers\FileHelper;
use yii\helpers\Json;
use yii\helpers\StringHelper;
use yii\httpclient\Client;
use yii\imagine\Image;
use yii\web\NotFoundHttpException;
use yii\web\Response;

class CabinetSchoolLessonController extends CabinetSchoolBaseController
{

    public $type_id = 7;

    public function behaviors()
    {
        return [
            'access' => [
                'class' => '\yii\filters\AccessControl',
                'rules' => [
                    [
                        'allow'         => true,
                        'matchCallback' => function ($rule, $action) {
                            if (in_array($action->id, ['add', 'index'])) {
                                $id = Yii::$app->request->get('id');
                                $kurs = Kurs::findOne($id);
                                $school_id = $kurs->school_id;
                            } else if (in_array($action->id, ['delete', 'edit'])) {
                                $id = Yii::$app->request->get('id');
                                $lesson = Lesson::findOne($id);
                                $kurs = Kurs::findOne($lesson->kurs_id);
                                $school_id = $kurs->school_id;
                            } else {
                                return true;
                            }
                            if (!Application::isInteger($school_id)) throw new \Exception('$school_id не целое');

                            $isAdmin = AdminLink::find()->where([
                                'user_id'   => Yii::$app->user->id,
                                'school_id' => $school_id,
                            ])->exists();

                            return $isAdmin;
                        },
                    ],
                ],
            ],
        ];
    }

    /**
     * @return string
     */
    public function actionIndex($id)
    {
        $kurs = Kurs::findOne($id);
        $school = School::findOne($kurs->school_id);

        return $this->render(['kurs' => $kurs, 'school' => $school]);
    }

    /**
     * @param int $id school_kurs.id
     *
     * @return string
     */
    public function actionAdd($id)
    {
        $kurs = Kurs::findOne($id);
        $model = new \avatar\models\forms\school\Lesson(['_school_id' => $kurs->school_id, '_type_id' => $this->type_id]);
        $school = School::findOne($kurs->school_id);

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $model->save();
            $i = Lesson::findOne($model->id);
            $i->created_at = time();
            $i->kurs_id = $id;
            $i->save();

            /** @var \common\models\school\Potok $p */
            foreach (Potok::find()->where(['kurs_id' => $id])->all() as $p) {
                LessonPotokState::add([
                    'lesson_id' => $i->id,
                    'potok_id'  => $p->id,
                    'status'    => 0,
                    'is_hide'   => 0,
                ]);
            }
            Yii::$app->session->setFlash('form', $model->id);
        }

        return $this->render([
            'model' => $model, 'kurs' => $kurs, 'school' => $school,
        ]);
    }


    public function actionEdit($id)
    {
        $model = \avatar\models\forms\school\Lesson::findOne($id);
        $kurs = Kurs::findOne($model->kurs_id);
        $model->_school_id = $kurs->school_id;
        $model->_type_id = $this->type_id;
        $school = School::findOne($kurs->school_id);

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $model->save();
            Yii::$app->session->setFlash('form', 1);
        }

        return $this->render([
            'model'  => $model,
            'kurs'   => $kurs,
            'school' => $school,
        ]);
    }

    public function actionEditVideo($id)
    {
        $model = LessonVideo::findOne(['lesson_id' => $id]);
        if (is_null($model)) $model = new LessonVideo();
        /** @var \avatar\models\forms\school\Lesson $lesson */
        $lesson = \avatar\models\forms\school\Lesson::findOne($id);
        $kurs = Kurs::findOne($lesson->kurs_id);
        $this->isAccess($kurs->school_id);
        $school = School::findOne($kurs->school_id);

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $model->lesson_id = $id;
            $model->save();
            Yii::$app->session->setFlash('form', 1);
        }

        return $this->render([
            'model'  => $model,
            'kurs'   => $kurs,
            'school' => $school,
            'lesson' => $lesson,
        ]);
    }

    /**
     * @param int $id идентификатор урока страницы school_kurs_lesson_page.id
     *
     * @return string
     */
    public function actionEditPage($id)
    {
        $model = LessonPage::findOne(['lesson_id' => $id]);
        if (is_null($model)) $model = new LessonPage();

        /** @var \avatar\models\forms\school\Lesson $lesson */
        $lesson = \avatar\models\forms\school\Lesson::findOne($id);
        $kurs = Kurs::findOne($lesson->kurs_id);
        $this->isAccess($kurs->school_id);
        $school = School::findOne($kurs->school_id);

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->isNewRecord) $model->lesson_id = $id;
            $model->save();
            Yii::$app->session->setFlash('form', 1);
        }

        return $this->render([
            'model'  => $model,
            'kurs'   => $kurs,
            'school' => $school,
            'lesson' => $lesson,
        ]);
    }

    public function actionEditPlace($id)
    {
        $model = LessonPlace::findOne(['lesson_id' => $id]);
        if (is_null($model)) $model = new LessonPlace();
        /** @var \avatar\models\forms\school\Lesson $lesson */
        $lesson = \avatar\models\forms\school\Lesson::findOne($id);
        $kurs = Kurs::findOne($lesson->kurs_id);
        $this->isAccess($kurs->school_id);
        $school = School::findOne($kurs->school_id);

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $model->lesson_id = $id;
            $model->save();
            Yii::$app->session->setFlash('form', 1);
        }

        return $this->render([
            'model'  => $model,
            'kurs'   => $kurs,
            'school' => $school,
            'lesson' => $lesson,
        ]);
    }


    /**
     * REQUEST
     * - $id int school_kurs_lesson.id
     *
     * @return Response
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function actionSubscription()
    {
        $model = new \avatar\models\validate\CabinetSchoolLessonControllerSubscription();

        if (!$model->load(Yii::$app->request->get(), '')) {
            return self::jsonErrorId(400, 'Не загружены данные');
        }
        if (!$model->validate()) {
            return self::jsonErrorId(102, $model->errors);
        }
        $data = $model->action();

        return self::jsonSuccess(['item' => $data]);
    }

    /**
     * Удаляет урок
     *
     * @param int $id идентификатор урока
     *
     * @return Response
     *
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     * @throws \yii\web\ForbiddenHttpException
     */
    public function actionDelete($id)
    {
        $lesson = Lesson::findOne($id);

        // Удаляю статусы уроков в потоках
        /** @var \common\models\school\Potok $p */
        foreach (Potok::find()->where(['kurs_id' => $lesson->kurs_id])->all() as $p) {
            LessonPotokState::deleteAll([
                'lesson_id' => $lesson->id,
                'potok_id'  => $p->id,
            ]);
        }

        // Удаляю урок
        $lesson->delete();

        // Удаляю комментарии


        return self::jsonSuccess();
    }
}
