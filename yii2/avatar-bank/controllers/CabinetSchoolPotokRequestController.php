<?php

namespace avatar\controllers;

use avatar\models\forms\UserAvatar;
use avatar\models\validate\CardNumberAccept;
use avatar\modules\ETH\ServiceEtherScan;
use avatar\modules\ETH\ServiceEthPlorer;
use common\components\Card;
use common\components\providers\BTC;
use common\components\providers\ETH;
use common\models\avatar\Currency;
use common\models\avatar\UserBill;
use common\models\Config;
use common\models\school\Kurs;
use common\models\school\Potok;
use common\models\school\PotokUser3Link;
use common\models\school\PotokUserExt;
use common\models\school\School;
use common\models\school\SchoolSale;
use common\models\school\UserLink;
use common\models\UserRoot;
use common\models\UserSeed;
use common\payment\BitCoinBlockTrailPayment;
use common\services\Security;
use console\controllers\MoneyRateController;
use cs\services\VarDumper;
use cs\web\Exception;
use cs\Widget\FileUpload4\ModelFields;
use Yii;
use yii\helpers\FileHelper;
use yii\helpers\Json;
use yii\helpers\StringHelper;
use yii\httpclient\Client;
use yii\imagine\Image;
use yii\web\Response;

class CabinetSchoolPotokRequestController extends CabinetSchoolBaseController
{

    /**
     * @param int $id идентификатор потока
     *
     * @return string
     * @throws
     */
    public function actionIndex($id)
    {
        $potok = Potok::findOne($id);
        if (is_null($potok)) {
            throw new Exception('Не найден поток');
        }
        $kurs = Kurs::findOne($potok->kurs_id);
        if (is_null($kurs)) {
            throw new Exception('Не найден курс');
        }
        $this->isAccess($kurs->school_id);
        $school = School::findOne($kurs->school_id);

        return $this->render([
            'school' => $school,
            'kurs'   => $kurs,
            'potok'  => $potok,
        ]);
    }

    /**
     * @param int $id идентификатор заявки school_kurs_sale_request.id
     *
     * @return string
     * @throws
     */
    public function actionItem($id)
    {
        $request = \common\models\school\SchoolSaleRequest::findOne($id);
        if (is_null($request)) {
            throw new Exception('Не найдена заявка');
        }
        $sale = SchoolSale::findOne($request->sale_id);
        $this->isAccess($sale->school_id);
        $school = School::findOne($sale->school_id);

        $potok = Potok::findOne($sale->potok_id);
        if (is_null($potok)) {
            throw new Exception('Не найден поток');
        }
        $kurs = Kurs::findOne($potok->kurs_id);
        if (is_null($kurs)) {
            throw new Exception('Не найден курс');
        }

        return $this->render([
            'school'  => $school,
            'sale'    => $sale,
            'request' => $request,
            'kurs'    => $kurs,
            'potok'   => $potok,
        ]);
    }

    /**
     *
     * @return string
     * @throws
     */
    public function actionAccept()
    {
        $model = new \avatar\models\validate\CabinetSchoolPotokRequestControllerAccept();

        if (!$model->load(Yii::$app->request->post(), '')) {
            return self::jsonErrorId(400, 'Не загружены данные');
        }
        if (!$model->validate()) {
            return self::jsonErrorId(102, $model->errors);
        }
        $model->action();

        return self::jsonSuccess();
    }

    /**
     *
     * @return string
     * @throws
     */
    public function actionLid()
    {
        $model = new \avatar\models\validate\CabinetSchoolPotokRequestControllerLid();

        if (!$model->load(Yii::$app->request->post(), '')) {
            return self::jsonErrorId(400, 'Не загружены данные');
        }
        if (!$model->validate()) {
            return self::jsonErrorId(102, $model->errors);
        }

        return self::jsonSuccess($model->action());
    }
}
