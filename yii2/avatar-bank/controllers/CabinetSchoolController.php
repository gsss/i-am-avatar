<?php

namespace avatar\controllers;

use avatar\models\forms\UserAvatar;
use avatar\models\validate\CardNumberAccept;
use avatar\modules\ETH\ServiceEtherScan;
use avatar\modules\ETH\ServiceEthPlorer;
use common\components\Card;
use common\components\providers\BTC;
use common\components\providers\ETH;
use common\models\avatar\Currency;
use common\models\avatar\UserBill;
use common\models\Config;
use common\models\piramida\Wallet;
use common\models\school\School;
use common\models\school\UserLink;
use common\models\UserSeed;
use common\payment\BitCoinBlockTrailPayment;
use common\services\Security;
use common\services\Subscribe;
use console\controllers\MoneyRateController;
use cs\services\Url;
use cs\services\VarDumper;
use cs\web\Exception;
use cs\Widget\FileUpload4\ModelFields;
use Yii;
use yii\bootstrap\ActiveForm;
use yii\helpers\FileHelper;
use yii\helpers\Json;
use yii\helpers\StringHelper;
use yii\httpclient\Client;
use yii\imagine\Image;
use yii\web\Response;

class CabinetSchoolController extends CabinetSchoolBaseController
{
    public $type_id = 17;

    /**
     * @return string
     */
    public function actionIndex()
    {
        return $this->render();
    }

    public function actionAdd()
    {
        $model = new \avatar\models\forms\School();

        if ($model->load(Yii::$app->request->post()) && $model->insert()) {
            Yii::$app->session->setFlash('form', $model->id);

            // Добавляю кошелек QL
            $c = \common\models\piramida\Currency::findOne(['code' => 'QL']);
            $w = Wallet::addNew(['currency_id' => $c->id]);

            $i = School::findOne($model->id);
            $i->created_at = time();
            $i->user_id = Yii::$app->user->id;
            $i->pay_wallet_id = $w->id;
            $i->save();

            // Добавляю админа
            \common\models\school\AdminLink::add([
                'user_id'   => Yii::$app->user->id,
                'school_id' => $i->id,
            ]);

            // Добавляю Пользователя в школу
            UserLink::add([
                'user_root_id' => Yii::$app->user->identity->user_root_id,
                'school_id'    => $i->id,
            ]);

            UserLink::add([
                'user_root_id' => Yii::$app->user->identity->user_root_id,
                'school_id'    => $i->id,
            ]);

            // Добавляю статусы задач
            $statusList[] = \common\models\task\Status::add(['name' => 'Исполняется', 'school_id' => $i->id, 'sort_index' => 1, 'type_id' => 1]);
            $statusList[] = \common\models\task\Status::add(['name' => 'Проверяется', 'school_id' => $i->id, 'sort_index' => 2, 'type_id' => 2]);
            $statusList[] = \common\models\task\Status::add(['name' => 'Выполнено', 'school_id' => $i->id, 'sort_index' => 3, 'type_id' => 3]);
        }

        return $this->render([
            'model' => $model,
        ]);
    }


    public function actionEdit($id)
    {
        $this->isAccess($id);
        $model = \avatar\models\forms\School::findOne($id);
        $model->_school_id = $model->id;
        $model->_type_id = $this->type_id;

        if ($model->load(Yii::$app->request->post()) && $model->update()) {
            Yii::$app->session->setFlash('form', 1);
        }

        return $this->render([
            'model' => $model,
        ]);
    }

    public function actionView($id)
    {
        $this->isAccess($id);
        $school = School::findOne($id);

        return $this->render([
            'school' => $school,
        ]);
    }

    public function actionFileSha256($id)
    {
        $this->isAccess($id);
        $school = School::findOne($id);

        return $this->render([
            'school' => $school,
        ]);
    }

    public function actionFileSha256Cloud()
    {
        $file = self::getParam('file');
        $url = new Url($file);
        /** @var \common\services\AvatarCloud $cloud */
        $cloud = Yii::$app->AvatarCloud;
        $response = $cloud->_post(null, 'upload/sha256', ['file' => $url->path]);
        $data = Json::decode($response->content);

        self::jsonSuccess(['hash' => $data['data']['hash']]);
    }

    public function actionPay($id)
    {
        $this->isAccess($id);
        $School = School::findOne($id);
        $model = new \avatar\models\forms\CabinetSchoolPay(['School' => $School]);

        if (Yii::$app->request->isPost) {
            if ($model->load(Yii::$app->request->post()) && $model->validate()) {
                $s = $model->save();
                return self::jsonSuccess();
            } else {
                return self::jsonErrorId(102, $model->getErrors102());
            }
        }

        return $this->render([
            'model'  => $model,
            'school' => $School,
        ]);
    }

    public function actionCatalogOn($id)
    {
        $this->isAccess($id);
        $school = School::findOne($id);
        $school->is_catalog = 1;
        $school->save();

        return self::jsonSuccess();
    }

    public function actionHide($id)
    {
        $this->isAccess($id);
        $school = School::findOne($id);
        $school->is_hide = 1;
        $school->save();

        return self::jsonSuccess();
    }

    public function actionCatalogOff($id)
    {
        $this->isAccess($id);
        $school = School::findOne($id);
        $school->is_catalog = 0;
        $school->save();

        return self::jsonSuccess();
    }
}
