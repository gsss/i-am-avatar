<?php

namespace avatar\controllers;

use common\models\school\PageBlockContent;
use common\models\school\PluginExport;
use common\models\school\PluginExportSettings;
use common\models\school\Potok;
use common\models\school\PotokUser3Link;
use common\models\school\PotokUserExt;
use common\models\school\PotokUserLink;
use common\models\User2;
use common\models\UserRegisterVebinar;
use common\models\UserRegistration;
use common\models\UserRoot;
use common\services\Subscribe;
use cs\Application;
use cs\services\VarDumper;
use yii\helpers\Json;
use Yii;
use yii\helpers\Url;

class PagesController extends \avatar\base\BaseController
{

    /**
     * POST:
     * - block_id - int - идентификатор блока
     * - potok_id - int - идентификатор потока
     *
     * Ошибки
     * 101, Не найдена форма page_id - что делать? сообщить что ошибка
     *
     * @return string JSON array
     *                result - int - 1 - отправлено подтверждение на почту
     *                               2 - отправлено поздравление на почту
     *                               3 - не связана страница потоком, обработаны actions, ничего не отправлено
     */
    public function actionForm()
    {
        $block_id = self::getParam('block_id');
        $potok_id = self::getParam('potok_id');
        if (!is_null($potok_id)) {
            return $this->actionForm2();
        }
        $block = PageBlockContent::findOne($block_id);
        if (is_null($block)) {
            return self::jsonErrorId(101, 'Не найдена страница');
        }
        $object = Json::decode($block->content);
        $form = $object['items'];

        $errors = [];
        foreach ($form as $field) {
            $value = \Yii::$app->request->post($field['name']);
            if (isset($field['is_required'])) {
                if ($field['is_required']) {
                    if (strlen($value) == 0) {
                        $errors[$field['name']][] = 'Это поле должно быть заполнено обязательно';
                    }
                }
            }
            if ($field['type'] == 'email') {
                if (!filter_var($value, FILTER_VALIDATE_EMAIL)) {
                    $errors[$field['name']][] = 'Нужно ввести корректный Email';
                }
            }
        }

        if (count($errors) > 0) return self::jsonErrorId(102, $this->convert($errors));

        // успешно
        if (isset($object['action'])) {
            foreach ($object['action'] as $id) {
                $plugin = PluginExportSettings::findOne($id);
                $plugin->export($this->getFields($object));
            }
        }

        $result = 3;
        $potok = Potok::findOne(['page_id' => $block->page_id]);
        if (is_null($potok)) {
            return self::jsonErrorId(103, 'Не найден поток');
        }

        if (!is_null($potok)) {
            $fields = $this->getFields($object);
            $result = $this->tildaForm2($potok, $fields);
        }

        return self::jsonSuccess(['result' => $result]);
    }

    /**
     * POST:
     * - potok_id - int - идентификатор потока
     *
     * Ошибки
     * 101, Не найдена форма page_id - что делать? сообщить что ошибка
     *
     * @return string JSON array
     *                result - int - 1 - отправлено подтверждение на почту
     *                               2 - отправлено поздравление на почту
     *                               3 - не связана страница потоком, обработаны actions, ничего не отправлено
     */
    public function actionForm2()
    {
        $potok_id = self::getParam('potok_id');

        $form = [
            [
                "name"        => "email",
                "type"        => "email",
                "placeholder" => "E-mail",
            ],
            [
                "name"        => "name",
                "type"        => "name",
                "placeholder" => "Имя",
            ],
            [
                "name"        => "phone",
                "type"        => "phone",
                "placeholder" => "Телефон",
            ],
        ];

        $errors = [];
        foreach ($form as $field) {
            $value = \Yii::$app->request->post($field['name']);
            if (isset($field['is_required'])) {
                if ($field['is_required']) {
                    if (strlen($value) == 0) {
                        $errors[$field['name']][] = 'Это поле должно быть заполнено обязательно';
                    }
                }
            }
            if ($field['type'] == 'email') {
                if (!filter_var($value, FILTER_VALIDATE_EMAIL)) {
                    $errors[$field['name']][] = 'Нужно ввести корректный Email';
                }
            }
        }

        if (count($errors) > 0) return self::jsonErrorId(102, $this->convert($errors));

        $result = 3;
        $potok = Potok::findOne($potok_id);
        if (is_null($potok)) {
            return self::jsonErrorId(103, 'Не найден поток');
        }

        if (!is_null($potok)) {
            $fields = $this->getFields(['items' => $form]);
            $result = $this->tildaForm2($potok, $fields);
        }

        return self::jsonSuccess(['result' => $result]);
    }


    /**
     * Подготавливает данные из формы POST в стандартный набор полей для отправки
     *
     * @param array $object
     *
     * @return array ассоциированный массив
     */
    private function getFields($object)
    {
        $rows = [];
        foreach ($object['items'] as $field) {
            $rows[$field['type']] = Yii::$app->request->post($field['name']);
        }

        return $rows;
    }

    /**
     * Записывает данные в БД
     *
     * @param \common\models\school\Potok $potok
     * @param array $fields
     *
     * @return int  результат выполнения
     *              1 - отправлено подтверждение на почту
     *              2 - отправлено поздравление на почту
     */
    public function tildaForm2(Potok $potok, $fields)
    {
        $email = strtolower($fields['email']);
        $u = UserRoot::findOne(['email' => $email]);
        Yii::info(\yii\helpers\VarDumper::dumpAsString($fields), '\avatar\controllers\PagesController::tildaForm2');
        if (is_null($u)) {
            // вообще новый пользователь
            $u = UserRoot::add(['email' => $email]);
            $link = \common\models\school\UserLink::add([
                'user_root_id' => $u->id,
                'school_id'    => $potok->getKurs()->school_id,
            ]);
            $potokLink = PotokUser3Link::add([
                'user_root_id' => $u->id,
                'potok_id'     => $potok->id,
                'is_avatar'    => ($u->avatar_status == 2) ? 1 : 0,
            ]);
            $userExt = PotokUserExt::add([
                'link_id' => $potokLink->id,
                'name'    => $fields['name'],
                'phone'   => $fields['phone'],
            ]);
            UserRegisterVebinar::add([
                'potok_id'     => $potok->id,
                'user_root_id' => $u->id,
            ]);

            // Надо отправить письмо на подтверждение почты
            $this->sendConfirm($potok, $u);
            return 1;
        } else {
            // пользователь такой уже есть, но может он еще в школе не состоит
            {
                $link = \common\models\school\UserLink::findOne([
                    'user_root_id' => $u->id,
                    'school_id'    => $potok->getKurs()->school_id,
                ]);
                if (is_null($link)) {
                    $link = \common\models\school\UserLink::add([
                        'user_root_id' => $u->id,
                        'school_id'    => $potok->getKurs()->school_id,
                    ]);
                }
            }
            // Ищу пользователя в потоке, может он уже записывался
            $potokLink = PotokUser3Link::findOne([
                'user_root_id' => $u->id,
                'potok_id'     => $potok->id,
            ]);
            if (is_null($potokLink)) {
                // в потоке его нет, добавляю
                $potokLink = PotokUser3Link::add([
                    'user_root_id' => $u->id,
                    'is_avatar'    => ($u->avatar_status == 2) ? 1 : 0,
                    'potok_id'     => $potok->id,
                ]);
                Yii::info(\yii\helpers\VarDumper::dumpAsString($u), '\avatar\school\controllers\PagesController::tildaForm2');
                if ($u->avatar_status != UserRoot::STATUS_REGISTERED) {
                    $userExt = PotokUserExt::add([
                        'link_id' => $potokLink->id,
                        'name'    => $fields['name'],
                        'phone'   => $fields['phone'],
                    ]);
                }
                // проверяю а подтвержден ли его email
                {
                    if ($u->avatar_status == UserRoot::STATUS_UNCONFIRMED) {
                        // пользователь не подтвержден, высылаю запрос на подтверждение email
                        $this->sendConfirm($potok, $u);
                        return 1;
                    } else {
                        // пользователь подтвержден, высылаю поздравления об успешной подписке
                        $this->sendCongratulations($potok, $u);
                        return 2;
                    }
                }
            } else {
                // в потоке пользователь уже есть

                // проверяю а подтвержден ли его email
                {
                    if ($u->avatar_status == UserRoot::STATUS_UNCONFIRMED) {
                        // пользователь не подтвержден, высылаю запрос на подтверждение email
                        $this->sendConfirm($potok, $u);
                        return 1;
                    } else {
                        // пользователь подтвержден, высылаю поздравления об успешной подписке
                        $this->sendCongratulations($potok, $u);
                        return 2;
                    }
                }
            }
        }
    }

    /**
     * @param \common\models\school\Potok $potok
     * @param \common\models\UserRoot $u
     */
    private function sendCongratulations($potok, $u)
    {
        $school = $potok->getSchool();
        Subscribe::sendArraySchool($school, [$u->email], 'Уведомление об успешной регистрации на событие', 'congratulations', [
            'user'  => $u,
            'potok' => $potok,
            'kurs'  => $potok->getKurs(),
        ], 'layouts/html/subscribe');
    }

    /**
     * @param \common\models\school\Potok $potok
     * @param \common\models\UserRoot $u
     */
    private function sendConfirm($potok, $u)
    {
        $school = $potok->getSchool();
        $userRegister = \avatar\models\UserRegistration::add($u->id);
        $kurs = $potok->getKurs();
        if ($kurs->status == \common\models\school\Kurs::STATUS_CONFIRM) {
            $result = Subscribe::sendArraySchool($school, [$u->email], 'Подтверждение на событие', 'registration_form_1', [
                'user'  => $u,
                'potok' => $potok,
                'kurs'  => $kurs,
                'url'   => 'https://www.i-am-avatar.com/auth/confirm?code=' . $userRegister->code,
            ], 'layouts/html/subscribe');
        }
        if ($kurs->status == \common\models\school\Kurs::STATUS_CONFIRM_AND_REGISTRATION) {
            $result = Subscribe::sendArraySchool($school, [$u->email], 'Подтверждение на событие', 'registration_form_2', [
                'user'  => $u,
                'potok' => $potok,
                'kurs'  => $kurs,
                'url'   => 'https://www.i-am-avatar.com/auth/confirm-and-registration?code=' . $userRegister->code,
            ], 'layouts/html/subscribe');
        }
    }

    private function convert($errors)
    {
        $rows = [];
        foreach ($errors as $name => $value) {
            $rows[] = [
                'name'  => $name,
                'value' => $value,
            ];
        }

        return $rows;
    }
}
