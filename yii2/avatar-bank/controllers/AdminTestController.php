<?php

namespace avatar\controllers;

use avatar\models\forms\Test;
use common\models\Anketa;
use common\models\task\Task;
use cs\services\VarDumper;
use Yii;
use yii\filters\AccessControl;
use yii\web\Response;

class AdminTestController extends AdminBaseController
{
    public function actionAdd()
    {
        $model = new Test(['_school_id' => 2, '_type_id' => 555]);

        if ($model->load(Yii::$app->request->post()) && $model->insert()) {
            Yii::$app->session->setFlash('form', $model->id);
        }

        return $this->render([
            'model' => $model,
        ]);
    }
    public function actionEdit($id)
    {
        $model = Test::findOne($id);
        $model->_school_id = 2;

        if ($model->load(Yii::$app->request->post()) && $model->update()) {
            Yii::$app->session->setFlash('form', $model->id);
        }

        return $this->render([
            'model' => $model,
        ]);
    }

}
