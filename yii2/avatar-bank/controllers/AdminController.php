<?php

namespace avatar\controllers;

use app\models\Article;
use app\models\SiteUpdate;
use app\services\Subscribe;
use common\models\avatar\UserBill;
use cs\base\BaseController;
use cs\services\UploadFolderDispatcher;
use cs\services\VarDumper;
use cs\web\Exception;
use Yii;
use yii\base\UserException;
use yii\bootstrap\ActiveForm;
use yii\db\Connection;
use yii\db\Query;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use yii\helpers\FileHelper;
use yii\web\Response;

class AdminController extends AdminBaseController
{
    /**
     */
    public function actionIndex()
    {
        return $this->render();
    }

    /**
     */
    public function actionSetEthereumWalletRead()
    {
        $model = new \avatar\models\forms\EthereumWalletRead();

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            Yii::$app->session->setFlash('form', 1);
            $model->action();

            return $this->refresh();
        } else {
            return $this->render('set-ethereum-wallet-read', [
                'model' => $model,
            ]);
        }
    }

    /**
     */
    public function actionMailTest()
    {
        $model = new \avatar\models\forms\MailTest();

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $model->action();
            Yii::$app->session->setFlash('form', 1);
        }

        return $this->render([
            'model' => $model,
        ]);
    }

}
