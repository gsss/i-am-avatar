<?php

namespace avatar\controllers;

use app\common\components\Piramida;
use app\models\Article;
use app\models\Form\Shop\Order;
use app\models\Piramida\WalletSource\Custom;
use app\models\Piramida\WalletSource\Pochta;
use app\models\Piramida\WalletSource\Yandex;
use app\models\Shop;
use app\models\Shop\Product;
use app\models\Shop\Request;
use app\models\Shop\RequestMessage;
use app\models\Shop\RequestProduct;
use app\models\Union;
use app\models\User;
use app\modules\Shop\services\Basket;
use common\models\school\School;
use cs\Application;
use cs\services\VarDumper;
use cs\web\Exception;
use Yii;
use yii\base\UserException;
use yii\bootstrap\Html;
use yii\db\Query;


/**
 */
class CabinetSchoolShopCatalogController extends \avatar\controllers\CabinetSchoolBaseController
{
    /**
     *
     * @param int $id school.id
     * @return string
     */
    public function actionIndex($id)
    {
        $this->isAccess($id);
        $school = School::findOne($id);

        return $this->render(['school' => $school]);
    }

    public function actionAdd($id)
    {
        $model = new \avatar\models\forms\shop\CatalogItem();
        $school = School::findOne($id);

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $model->school_id = $id;
            Yii::$app->session->setFlash('form', 1);
            $model->save();
        }

        return $this->render([
            'model' => $model,
            'school' => $school,
        ]);
    }

    public function actionEdit($id)
    {
        $model = \avatar\models\forms\shop\CatalogItem::findOne($id);
        $school = School::findOne($model->school_id);

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $model->save();
            Yii::$app->session->setFlash('form', $id);
        }

        return $this->render([
            'model' => $model,
            'school' => $school,
        ]);
    }

    public function actionDelete($id)
    {
        \avatar\models\forms\shop\CatalogItem::findOne($id)->delete();

        return self::jsonSuccess();
    }
}
