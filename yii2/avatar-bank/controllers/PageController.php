<?php

namespace avatar\controllers;

use common\models\school\Potok;
use common\models\school\School;
use cs\services\Url;
use cs\services\VarDumper;
use yii\helpers\StringHelper;
use yii\web\HttpException;

class PageController extends \avatar\base\BaseController
{

    public $layout = 'blank';

    /**
     */
    public function actionController()
    {
        $url = \Yii::$app->request->getUrl();
        $u = explode('?', $url);
        if (count($u) > 1) {
            $url = $u[0];
        }
        $url = str_replace('/page', '', $url);
        $school = School::findOne(2);
        $page = $school->getPage($url);

        if (is_null($page)) {
            // страница не найдена
            throw  new HttpException(404, 'Page not found');
        }
        if (count($page->getBlocks()) == 0) {
            // страница не найдена
            throw  new HttpException(404, 'Page not found');
        }
        $page->view();

        return $this->render([
            'page'   => $page,
            'school' => $school,
        ]);
    }

    /**
     */
    public function actionPage($url)
    {
        $u = explode('?', $url);
        if (count($u) > 1) {
            $url = $u[0];
        }
        $school = School::get();
        $page = $school->getPage($url);

        if (is_null($page)) {
            // страница не найдена
            throw  new HttpException(404, 'Page not found');
        }
        if (count($page->getBlocks()) == 0) {
            // страница не найдена
            throw new HttpException(404, 'Страница не имеет блоков');
        }
        $page->view();

        return $this->render([
            'page'   => $page,
            'school' => $school,
        ]);
    }

    /**
     */
    public function actionControllerAction()
    {
        $url = \Yii::$app->request->getUrl();
        $u = explode('?', $url);
        if (count($u) > 1) {
            $url = $u[0];
        }
        $url = substr($url, 5);

        $school = School::findOne(2);
        $page = $school->getPage($url);
        if (is_null($page)) {
            // страница не найдена
            throw  new HttpException(404, 'Page not found');
        }
        if (count($page->getBlocks()) == 0) {
            // страница не найдена
            throw  new HttpException(404, 'Page not found');
        }
        $page->view();

        return $this->render('controller', [
            'page'   => $page,
            'school' => $school,
        ]);
    }
}
