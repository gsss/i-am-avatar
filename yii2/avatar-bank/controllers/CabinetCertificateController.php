<?php

namespace avatar\controllers;

use avatar\models\forms\UserAvatar;
use avatar\models\validate\CardNumberAccept;
use avatar\modules\ETH\ServiceEtherScan;
use avatar\modules\ETH\ServiceEthPlorer;
use common\components\Card;
use common\components\providers\BTC;
use common\components\providers\ETH;
use common\models\avatar\Currency;
use common\models\avatar\UserBill;
use common\models\Config;
use common\models\school\School;
use common\models\school\Sertificate;
use common\models\UserSeed;
use common\payment\BitCoinBlockTrailPayment;
use common\services\Security;
use console\controllers\MoneyRateController;
use cs\services\VarDumper;
use cs\web\Exception;
use cs\Widget\FileUpload4\ModelFields;
use Yii;
use yii\helpers\FileHelper;
use yii\helpers\Json;
use yii\helpers\StringHelper;
use yii\httpclient\Client;
use yii\imagine\Image;
use yii\web\Response;

class CabinetCertificateController extends \avatar\controllers\CabinetBaseController
{
    public $type_id = 11;

    /**
     * @return string
     */
    public function actionIndex()
    {
        return $this->render();
    }

    public function actionAdd()
    {
        $model = new \avatar\models\forms\school\Certificate(['_school_id' => 2, '_type_id' => $this->type_id]);

        if ($model->load(Yii::$app->request->post()) && $model->insert()) {
            Yii::$app->session->setFlash('form', 1);
            $i = \common\models\school\Sertificate::findOne($model->id);
            $i->user_id = Yii::$app->user->id;
            $i->save();
        }

        return $this->render([
            'model' => $model,
        ]);
    }


    public function actionEdit($id)
    {
        $model = \avatar\models\forms\school\Certificate::findOne($id);
        $model->_school_id = 2;
        $model->_type_id = $this->type_id;

        if ($model->load(Yii::$app->request->post()) && $model->update()) {
            Yii::$app->session->setFlash('form', 1);
        }

        return $this->render([
            'model' => $model,
        ]);
    }

    public function actionView($id)
    {
        $Certificate = \avatar\models\forms\school\Certificate::findOne($id);

        return $this->render([
            'item' => $Certificate,
        ]);
    }


    public function actionDelete($id)
    {
        \avatar\models\forms\school\Certificate::findOne($id)->delete();

        return self::jsonSuccess();
    }
}
