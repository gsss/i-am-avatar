<?php

namespace avatar\controllers;

use app\models\Article;
use app\models\SiteUpdate;
use app\services\Subscribe;
use avatar\models\forms\Contact;
use avatar\services\LogReader;
use common\components\sms\IqSms;
use common\models\avatar\UserBill;
use common\models\PaymentBitCoin;
use common\models\UserAvatar;
use common\models\UserMaster;
use common\models\UserRegistration;
use common\payment\BitCoinBlockTrailPayment;
use cs\Application;
use cs\base\BaseController;
use cs\services\UploadFolderDispatcher;
use cs\services\VarDumper;
use cs\web\Exception;
use Yii;
use yii\base\UserException;
use yii\db\Connection;
use yii\db\Query;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use yii\helpers\FileHelper;
use yii\helpers\Url;
use yii\web\Response;

class UserController extends \avatar\base\BaseController
{
    /**
     * Выводит профиль пользователя
     *
     * @param int $id идентификатор пользователя
     *
     * @return string Response
     */
    public function actionIndex($id)
    {
        $user = UserAvatar::findOne($id);

        return $this->render([
            'user' => $user,
        ]);
    }

    /**
     * Показывает счет
     */
    public function actionBilling($id)
    {
        $billing = UserBill::findOne($id);


        return $this->render([
            'billing' => $billing,
        ]);
    }

    /**
     */
    public function actionList()
    {
        return $this->render([]);
    }

    /**
     */
    public function actionItem($id)
    {
        try {
            $user = UserAvatar::findOne($id);
        } catch (\Exception $e) {
            throw new Exception('Не найден пользователь');
        }
        if ($user->is_master == 0) {
            throw new Exception('Учитель на желает участвовать в каталоге');
        }
        $userMaster = UserMaster::findOne(['user_id' => $user->id]);
        if (is_null($userMaster)) {
            throw new Exception('Учитель не заполнил анкету');
        }

        return $this->render([
            'user'       => $user,
            'userMaster' => $userMaster,
        ]);
    }
}
