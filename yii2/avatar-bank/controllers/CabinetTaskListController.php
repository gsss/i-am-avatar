<?php

namespace avatar\controllers;

use avatar\models\forms\school\CommandLink;
use avatar\models\forms\UserAvatar;
use avatar\models\validate\CardNumberAccept;
use common\components\Card;
use common\components\providers\BTC;
use common\components\providers\ETH;
use common\models\avatar\Currency;
use common\models\avatar\UserBill;
use common\models\comment\Comment;
use common\models\comment\CommentList;
use common\models\Config;
use common\models\piramida\Wallet;
use common\models\school\AdminLink;
use common\models\school\ProjectManagerLink;
use common\models\school\School;
use common\models\task\Task;
use common\models\UserSeed;
use common\models\UserWallet;
use common\payment\BitCoinBlockTrailPayment;
use common\services\Security;
use common\services\Subscribe;
use console\controllers\MoneyRateController;
use cs\Application;
use cs\services\VarDumper;
use cs\web\Exception;
use cs\Widget\FileUpload4\ModelFields;
use Yii;
use yii\base\Event;
use yii\filters\AccessControl;
use yii\helpers\FileHelper;
use yii\helpers\Json;
use yii\helpers\StringHelper;
use yii\httpclient\Client;
use yii\imagine\Image;
use yii\web\Response;

class CabinetTaskListController extends \avatar\controllers\CabinetBaseController
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => '\yii\filters\AccessControl',
                'rules' => [
                    [
                        'allow'         => true,
                        'matchCallback' => function ($rule, $action) {
                            if (in_array($action->id, ['index'])) {
                                $id = Yii::$app->request->get('id');
                                $school_id = null;
                                if (is_null($id)) {
                                    $schoolList = \common\models\school\CommandLink::find()
                                        ->where([
                                            'user_id' => Yii::$app->user->id,
                                        ])
                                        ->select(['school_id'])
                                        ->column();

                                    // не указано какую школу выводить
                                    // если школа одна то
                                    if (count($schoolList) == 0) {
                                        return false;
                                    } else {
                                        $school_id = $schoolList[0];
                                    }
                                } else {
                                    $school_id = $id;
                                }
                            }
                            if (in_array($action->id, ['view', 'view-done', 'view-start', 'view-finish', 'session-start', 'session-finish'])) {
                                $id = Yii::$app->request->get('id') . Yii::$app->request->post('id');
                                $task = Task::findOne($id);
                                $school_id = $task->school_id;
                            }
                            if (in_array($action->id, ['add'])) {
                                $school_id = Yii::$app->request->get('id');
                            }
                            if (in_array($action->id, ['budjet-search-ajax'])) {
                                $school_id = Yii::$app->request->post('company_id');
                            }
                            if (in_array($action->id, ['executor-search-ajax2'])) {
                                $school_id = Yii::$app->request->post('school_id');
                            }
                            if (!Application::isInteger($school_id)) throw new \Exception('$school_id не целое');

                            $isCommand = CommandLink::find()->where([
                                'user_id'   => Yii::$app->user->id,
                                'school_id' => $school_id,
                            ])->exists();
                            $isAdmin = AdminLink::find()->where([
                                'user_id'   => Yii::$app->user->id,
                                'school_id' => $school_id,
                            ])->exists();

                            return $isAdmin || $isCommand;
                        },
                    ],
                ],
            ],
        ];
    }

    public function actions()
    {
        return [
            'view-start' => [
                'class' => '\avatar\controllers\actions\CabinetSchoolTaskListController\actionViewStart',
            ],
            'view-done' => [
                'class' => '\avatar\controllers\actions\CabinetSchoolTaskListController\actionViewDone',
            ],
            'view-reject' => [
                'class' => '\avatar\controllers\actions\CabinetSchoolTaskListController\actionViewReject',
            ],
            'view-finish' => [
                'class' => '\avatar\controllers\actions\CabinetSchoolTaskListController\actionViewFinish',
            ],
            'view-finish-reject' => [
                'class' => '\avatar\controllers\actions\CabinetSchoolTaskListController\actionViewFinishReject',
            ],
            'session-start' => [
                'class' => '\avatar\controllers\actions\CabinetSchoolTaskListController\actionSessionStart',
            ],
            'session-finish' => [
                'class' => '\avatar\controllers\actions\CabinetSchoolTaskListController\actionSessionFinish',
            ],


            'budjet-search-ajax' => [
                'class' => '\avatar\controllers\actions\DefaultAjax',
                'model' => '\avatar\models\validate\CabinetSchoolTaskListBudjetSearchAjax',
            ],

            'executor-search-ajax' => [
                'class' => '\avatar\controllers\actions\DefaultAjax',
                'model' => '\avatar\models\validate\CabinetSchoolTaskListExecutorSearchAjax',
            ],
            'executor-search-ajax2' => [
                'class' => '\avatar\controllers\actions\DefaultAjax',
                'model' => '\avatar\models\validate\CabinetSchoolTaskListExecutorSearchAjax2',
            ],
            'executor-add'         => [
                'class' => '\avatar\controllers\actions\DefaultAjax',
                'model' => '\avatar\models\validate\CabinetSchoolTaskListAddExecutor',
            ],

            'helper-search-ajax' => [
                'class' => '\avatar\controllers\actions\DefaultAjax',
                'model' => '\avatar\models\validate\CabinetSchoolTaskListHelperSearchAjax',
            ],
            'helper-add'         => [
                'class' => '\avatar\controllers\actions\DefaultAjax',
                'model' => '\avatar\models\validate\CabinetSchoolTaskListHelperAdd',
            ],
        ];
    }

    public function actionAdd($id)
    {
        $model = new \avatar\models\forms\Task(['school_id' => $id, 'user_id' => Yii::$app->user->id]);
        $school = School::findOne($id);

        if (Yii::$app->request->isPost) {
            if ($model->load(Yii::$app->request->post()) && $model->validate()) {
                $model->created_at = time();
                if (!Application::isEmpty($model->currency_id)) {
                    $model->price = \iAvatar777\services\Processing\Currency::getAtomFromValue($model->price, $model->currency_id);
                }
                $model->last_comment_time = time();
                $model->school_id = $id;
                $model->is_hide = 0;
                $model->user_id = Yii::$app->user->id;
                $model->executer_id = Yii::$app->user->id;
                $s = $model->save();

                return self::jsonSuccess($s);
            } else {
                return self::jsonErrorId(102, $model->getErrors102());
            }
        }

        return $this->render([
            'model' => $model,
            'school' => $school,
        ]);
    }

    /**
     * @return string
     */
    public function actionIndex()
    {
        $school = School::get();
        return $this->render(['school' => $school]);
    }

    public function actionView($id)
    {
        $model = Task::findOne($id);
        $school = School::findOne($model->school_id);

        return $this->render('@avatar/views/cabinet-task-list/view', [
            'model' => $model,
            'school' => $school,
        ]);
    }


    /**
     * @return string
     */
    public function actionAjail($id)
    {
        $school = School::findOne($id);
        $cid = Yii::$app->request->get('cid');
        $category = null;
        if (!Application::isEmpty($cid)) {
            $category = \common\models\task\Category::findOne($cid);
        }

        $aid = Yii::$app->request->get('aid');
        $author = null;
        if (!Application::isEmpty($aid)) {
            $author = UserAvatar::findOne($aid);
        }

        $eid = Yii::$app->request->get('eid');
        $executor = null;
        if (!Application::isEmpty($eid)) {
            $executor = UserAvatar::findOne($eid);
        }

        return $this->render([
            'school'   => $school,
            'category' => $category,
            'author'   => $author,
            'executor' => $executor,
        ]);
    }

}
