<?php

namespace avatar\controllers;

use app\common\components\Piramida;
use app\models\Article;
use app\models\Form\Shop\Order;
use app\models\Piramida\WalletSource\Custom;
use app\models\Piramida\WalletSource\Pochta;
use app\models\Piramida\WalletSource\Yandex;
use app\models\Shop;
use app\models\Shop\Product;
use app\models\Shop\Request;
use app\models\Shop\RequestMessage;
use app\models\Shop\RequestProduct;
use app\models\Union;
use app\models\User;
use app\modules\Shop\services\Basket;
use common\models\school\School;
use cs\Application;
use cs\services\VarDumper;
use cs\web\Exception;
use Yii;
use yii\base\UserException;
use yii\bootstrap\Html;
use yii\db\Query;


/**
 */
class CabinetSchoolShopRequestsController extends \avatar\controllers\CabinetSchoolBaseController
{
    /**
     * @param int $id school.id
     */
    public function actionIndex($id)
    {
        $this->isAccess($id);
        $school = School::findOne($id);

        return $this->render(['school' => $school]);
    }

    /**
     */
    public function actionAdd($id)
    {
        $school = School::findOne($id);
        $this->isAccess($id);

        return $this->render([
            'school' => $school,
        ]);
    }

    /**
     */
    public function actionView($id)
    {
        $request = \common\models\shop\Request::findOne($id);
        $school = School::findOne($request->school_id);
        $this->isAccess($school->id);

        return $this->render([
            'school'  => $school,
            'request' => $request,
        ]);
    }

    /**
     */
    public function actionPaid($id)
    {
        $request = \common\models\shop\Request::findOne($id);
        $school = School::findOne($request->school_id);
        $this->isAccess($school->id);
        $request->successShop();

        return self::jsonSuccess();
    }

    public function actionMessage()
    {

        $model = new \avatar\models\validate\CabinetSchoolShopMessage();

        if ($model->load(Yii::$app->request->post(), '') && $model->validate()) {
            return self::jsonErrorId(102, $model->errors);
        }

        return self::jsonSuccess($model->action());
    }

}
