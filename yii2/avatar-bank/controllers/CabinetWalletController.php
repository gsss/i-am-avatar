<?php

namespace avatar\controllers;

use app\services\Subscribe;
use avatar\controllers\AdminBaseController;
use avatar\models\forms\BlogItem;
use avatar\models\Wallet;
use common\models\avatar\UserBill;
use common\models\investment\IcoRequest;
use common\models\investment\IcoRequestAddress;
use common\models\investment\IcoRequestSuccess;
use common\models\investment\Project;
use common\models\investment\ProjectIco;
use common\models\MapBankomat;
use common\models\PaySystemConfig;
use common\models\PaySystemListItem;
use common\models\UserAvatar;
use cs\Application;
use cs\services\VarDumper;
use cs\web\Exception;
use Yii;
use yii\base\UserException;
use yii\filters\AccessControl;
use yii\helpers\StringHelper;
use yii\helpers\Url;
use yii\web\Controller;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use app\models\NewsItem;
use app\models\Chenneling;
use cs\base\BaseController;
use yii\web\Response;
use yii\widgets\ActiveForm;
use app\models\SiteUpdate;

class CabinetWalletController extends \avatar\controllers\CabinetBaseController
{
    public function actionIndex()
    {
        return $this->render('@avatar/views/' . $this->id . '/' . $this->action->id);
    }

    public function actionItem($id)
    {
        try {
            $bill = UserBill::findOne($id);
            if ($bill->user_id != Yii::$app->user->id) {
                return $this->render('@avatar/views/site/error', ['name' => 'Ошибка', 'message' => 'Это не ваш счет']);
            }
        } catch (\Exception $e) {
            throw new Exception('Счет не найден');
        }

        return $this->render('@avatar/views/' . $this->id . '/' . $this->action->id, ['bill' => $bill]);
    }

    public function actionSend($id)
    {
        try {
            $bill = UserBill::findOne($id);
            if ($bill->user_id != Yii::$app->user->id) {
                return $this->render('@avatar/views/site/error', ['name' => 'Ошибка', 'message' => 'Это не ваш счет']);
            }
        } catch (\Exception $e) {
            throw new Exception('Счет не найден');
        }

        $model = new \avatar\models\forms\SendAtom2();

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $t = $model->action();
            Yii::$app->session->setFlash('form', $t->getAddress());
        }

        return $this->render('@avatar/views/' . $this->id . '/' . $this->action->id, [
            'bill'   => $bill,
            'model'  => $model,
        ]);
    }

    public function actionAdd()
    {
        $model = new \avatar\models\validate\CabinetWalletControllerActionAdd();

        if (!$model->load(Yii::$app->request->post(), '')) {
            return self::jsonErrorId(400, 'Не загружены данные');
        }
        if (!$model->validate()) {
            return self::jsonErrorId(102, $model->convert($model->errors));
        }
        $userBill = $model->action();

        return self::jsonSuccess($userBill);
    }

    public function actionDelete()
    {
        $model = new \avatar\models\validate\CabinetWalletControllerDelete();

        if (!$model->load(Yii::$app->request->post(), '')) {
            return self::jsonErrorId(400, 'Не загружены данные');
        }
        if (!$model->validate()) {
            return self::jsonErrorId(103, $model->errors);
        }
        $result = $model->action();
        if (!$result) return self::jsonErrorId(2, 'Кошелек не пустой');

        return self::jsonSuccess();
    }

    public function actionDeleteAgree()
    {
        $model = new \avatar\models\validate\CabinetWalletControllerDeleteAgree();

        if (!$model->load(Yii::$app->request->post(), '')) {
            return self::jsonErrorId(400, 'Не загружены данные');
        }
        if (!$model->validate()) {
            return self::jsonErrorId(103, $model->errors);
        }
        $model->action();

        return self::jsonSuccess();
    }

}
