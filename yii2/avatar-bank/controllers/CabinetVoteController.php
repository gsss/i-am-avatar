<?php

namespace avatar\controllers;

use app\services\Subscribe;
use avatar\controllers\AdminBaseController;
use avatar\models\forms\BlogItem;
use avatar\models\forms\UserAvatar;
use avatar\modules\UniSender\UniSender;
use common\models\school\School;
use common\models\subscribe\SubscribeMail;
use common\models\VoteAnswer;
use common\models\VoteItem;
use common\models\VoteList;
use cs\models\Tables\Client;
use cs\services\VarDumper;
use cs\web\Exception;
use Yii;
use yii\base\UserException;
use yii\filters\AccessControl;
use yii\helpers\Json;
use yii\helpers\Url;
use yii\httpclient\Client as ClientHttp;
use yii\web\Controller;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use app\models\NewsItem;
use app\models\Chenneling;
use cs\base\BaseController;
use yii\web\Response;
use yii\widgets\ActiveForm;
use app\models\SiteUpdate;

class CabinetVoteController extends \avatar\controllers\CabinetBaseController
{
    public function actionIndex($id)
    {
        $school = School::findOne($id);

        return $this->render(['school' => $school]);
    }

    public function actionAdd($id)
    {
        $school = School::findOne($id);
        $model = new VoteList(['school_id' => $id]);

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            Yii::$app->session->setFlash('form', 1);
            $model->created_at = time();
            $model->user_id = Yii::$app->user->id;
            $model->save();
        }

        return $this->render([
            'model' => $model,
            'school' => $school,
        ]);
    }


    public function actionEdit($id)
    {
        $model = VoteList::findOne($id);
        $school = School::findOne($model->school_id);

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $model->save();
            Yii::$app->session->setFlash('form', $id);
        }

        return $this->render([
            'model' => $model,
            'school' => $school,
        ]);
    }

    public function actionDelete($id)
    {
        $item = VoteList::findOne($id);

        /** @var \common\models\VoteAnswer $i */
        foreach (VoteAnswer::find()->where(['list_id' => $item->id])->all() as $i) {
            $i->delete();
        }

        /** @var \common\models\VoteItem $i */
        foreach (VoteItem::find()->where(['list_id' => $item->id])->all() as $i) {
            $i->delete();
        }
        $item->delete();

        return self::jsonSuccess();
    }
}
