<?php

namespace avatar\controllers;

use avatar\models\forms\UserAvatar;
use avatar\models\validate\CardNumberAccept;
use avatar\modules\ETH\ServiceEtherScan;
use avatar\modules\ETH\ServiceEthPlorer;
use common\components\Card;
use common\components\providers\BTC;
use common\components\providers\ETH;
use common\models\avatar\Currency;
use common\models\avatar\UserBill;
use common\models\Config;
use common\models\school\AdminLink;
use common\models\school\School;
use common\models\UserSeed;
use common\payment\BitCoinBlockTrailPayment;
use common\services\Security;
use common\services\Subscribe;
use console\controllers\MoneyRateController;
use cs\services\VarDumper;
use cs\web\Exception;
use cs\Widget\FileUpload4\ModelFields;
use Yii;
use yii\bootstrap\ActiveForm;
use yii\filters\AccessControl;
use yii\helpers\FileHelper;
use yii\helpers\Json;
use yii\helpers\StringHelper;
use yii\httpclient\Client;
use yii\imagine\Image;
use yii\web\ForbiddenHttpException;
use yii\web\Response;

class CabinetSchoolBaseController extends \avatar\controllers\CabinetBaseController
{
    /**
     * @param int $school_id
     *
     * @throws ForbiddenHttpException
     */
    public function isAccess($school_id)
    {
        $result = AdminLink::find()->where([
            'school_id' => $school_id,
            'user_id'   => Yii::$app->user->id,
        ])->exists();

        $isCommand = \common\models\school\CommandLink::find()->where([
            'user_id'   => Yii::$app->user->id,
            'school_id' => $school_id,
        ])->exists();

        if (!($result || $isCommand)) throw new ForbiddenHttpException('Вам запрещен доступ к чужой школе');
    }
}
