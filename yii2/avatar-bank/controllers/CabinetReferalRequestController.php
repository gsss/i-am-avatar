<?php

namespace avatar\controllers;

use app\models\Article;
use app\models\SiteUpdate;
use app\services\Subscribe;
use avatar\models\forms\BlogItem;
use common\components\providers\ETH;
use common\models\avatar\UserBill;
use common\models\avatar\UserBillDefault;
use common\models\HD;
use common\models\HDtown;
use common\models\PassportLink;
use common\models\piramida\Currency;
use common\models\school\AdminLink;
use common\models\school\School;
use common\models\UserAvatar;
use common\models\UserDocument;
use cs\base\BaseController;
use cs\services\SitePath;
use cs\services\UploadFolderDispatcher;
use cs\services\VarDumper;
use cs\web\Exception;
use Yii;
use yii\base\UserException;
use yii\bootstrap\ActiveForm;
use yii\db\Connection;
use yii\db\Query;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use yii\helpers\FileHelper;
use yii\helpers\Json;
use yii\web\Response;

class CabinetReferalRequestController extends \avatar\controllers\CabinetBaseController
{


    /**
     */
    public function actionIndex()
    {
        return $this->render('@avatar/views/'.$this->id.'/'.$this->action->id);
    }

    /**
     */
    public function actionAdd()
    {
        $model = new \avatar\models\forms\school\ReferalRequestOut();

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $model->user_id = Yii::$app->user->id;
            $school = School::get();

            $wallet_id = $school->referal_wallet_id;
            $wallet = \common\models\piramida\Wallet::findOne($wallet_id);
            $link = \common\models\avatar\CurrencyLink::findOne(['currency_int_id' => $wallet->currency_id]);
            $c = Currency::findOne($wallet->currency_id);

            $model->school_id = $school->id;
            $model->currency_id = $link->currency_ext_id;
            $model->amount =  (int)($model->amount * pow(10, $c->decimals));
            $model->status = 0;
            $model->save();
            $model->id = $model::getDb()->lastInsertID;

            $adminList = AdminLink::find()->where([
                'school_id' => $school->id,
            ])->select('user_id')->column();
            $rows = [];
            foreach ($adminList as $id) {
                $rows[] = UserAvatar::findOne($id)->email;
            }
            \common\services\Subscribe::sendArraySchool($school,$rows,'Поступила заявка на вывод','referal_request_out', [
                'request_id' => $model->id,
            ]);

            Yii::$app->session->setFlash('form', $model->id);
        }

        return $this->render('@avatar/views/'.$this->id.'/'.$this->action->id, [
            'model' => $model,
        ]);
    }


}
