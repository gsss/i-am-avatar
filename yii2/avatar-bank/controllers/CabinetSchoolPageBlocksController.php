<?php

namespace avatar\controllers;

use avatar\models\forms\UserAvatar;
use avatar\models\validate\CardNumberAccept;
use avatar\modules\ETH\ServiceEtherScan;
use avatar\modules\ETH\ServiceEthPlorer;
use common\components\Card;
use common\components\providers\BTC;
use common\components\providers\ETH;
use common\models\avatar\Currency;
use common\models\avatar\UserBill;
use common\models\Config;
use common\models\school\Kurs;
use common\models\school\Lesson;
use common\models\school\Master;
use common\models\school\Page;
use common\models\school\PageBlockContent;
use common\models\school\School;
use common\models\UserSeed;
use common\payment\BitCoinBlockTrailPayment;
use common\services\Security;
use console\controllers\MoneyRateController;
use cs\services\VarDumper;
use cs\web\Exception;
use cs\Widget\FileUpload4\ModelFields;
use Yii;
use yii\helpers\FileHelper;
use yii\helpers\Json;
use yii\helpers\StringHelper;
use yii\httpclient\Client;
use yii\imagine\Image;
use yii\web\Response;

class CabinetSchoolPageBlocksController extends CabinetSchoolBaseController
{

    /**
     * @param int $id страница
     *
     * @return string
     */
    public function actionIndex($id)
    {
        $page   = Page::findOne($id);
        $this->isAccess($page->school_id);
        $school = School::findOne($page->school_id);

        return $this->render(['page' => $page, 'school' => $school]);
    }

    /**
     * @param int $id страница school_page.id
     *
     * @return string
     */
    public function actionAdd($id)
    {
        $model  = new PageBlockContent();
        $page   = Page::findOne($id);
        $this->isAccess($page->school_id);
        $school = School::findOne($page->school_id);

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            Yii::$app->session->setFlash('form', 1);
            $model->page_id = $id;
            $model->save();
        }

        return $this->render([
            'model' => $model, 'school' => $school,
        ]);
    }


    public function actionEdit($id)
    {
        $model  = PageBlockContent::findOne($id);
        $page   = Page::findOne($model->page_id);
        $this->isAccess($page->school_id);
        $school = School::findOne($page->school_id);

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $model->save();
            Yii::$app->session->setFlash('form', 1);
        }

        return $this->render([
            'model'  => $model,
            'school' => $school,
            'page'   => $page,
        ]);
    }

    /**
     * @param int $id страница
     *
     * @return string
     */
    public function actionSort($id)
    {
        $page   = Page::findOne($id);
        $this->isAccess($page->school_id);
        $school = School::findOne($page->school_id);

        return $this->render([
            'school' => $school,
            'page'   => $page,
        ]);
    }

    /**
     * @param int $id страница
     * @param array $ids \common\models\school\PageBlockContent::id
     *
     * @return string
     */
    public function actionSortAjax($id)
    {
        $page   = Page::findOne($id);
        $this->isAccess($page->school_id);
        $ids = self::getParam('ids');

        $c = 0;
        foreach ($ids as $id) {
            $blockContent = PageBlockContent::findOne($id);
            $blockContent->sort_index = $c;
            $blockContent->save();
            $c++;
        }

        return self::jsonSuccess();
    }


    public function actionDelete($id)
    {
        $page   = Page::findOne($id);
        $this->isAccess($page->school_id);
        $page->delete();

        return self::jsonSuccess();
    }
}
