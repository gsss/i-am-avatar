<?php

namespace avatar\controllers;

use aki\telegram\Telegram;
use app\models\Article;
use app\models\SiteUpdate;
use avatar\models\forms\BlogItem;
use avatar\models\forms\Contact;
use avatar\services\LogReader;
use common\components\providers\ETH;
use common\components\sms\IqSms;
use common\models\avatar\Currency;
use common\models\avatar\UserBill;
use common\models\CompanyCustomizeItem;
use common\models\NewsItem;
use common\models\PaymentBitCoin;
use common\models\school\PotokUser3Link;
use common\models\UserAvatar;
use common\models\UserRegistration;
use common\models\UserTelegramConnect;
use common\models\UserTelegramTemp;
use common\payment\BitCoinBlockTrailPayment;
use cs\Application;
use cs\base\BaseController;
use cs\services\Security;
use cs\services\UploadFolderDispatcher;
use cs\services\VarDumper;
use cs\web\Exception;
use Yii;
use yii\base\UserException;
use yii\db\Connection;
use yii\db\Query;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use yii\helpers\FileHelper;
use yii\helpers\Json;
use yii\helpers\Url;
use yii\httpclient\Client;
use yii\web\ForbiddenHttpException;
use yii\web\Response;

class DbController extends \avatar\base\BaseController
{
    public $enableCsrfValidation = false;

    /**
     * curl -o s_routes_prod_main.sql -d "file=s_routes_prod_main&key=***" -X POST https://s-routes.com/db/get
     */
    public function actionGet()
    {
        $file = Yii::$app->request->post('file');
        $key = Yii::$app->request->post('key');
        if ($key != Yii::$app->params['PROD_DB_TOKEN']) throw new ForbiddenHttpException();
        Yii::$app->response->content = file_get_contents(Yii::getAlias('@avatar/../db/dumps/day/0000/' . $file . '.sql.gz'));
        Yii::$app->response->headers->add('Content-type', 'application/x-gzip');

        return Yii::$app->response;
    }
}
