<?php

namespace avatar\controllers;

use app\services\Subscribe;
use common\components\providers\ETH;
use common\models\avatar\Currency;
use common\models\avatar\UserBill;
use common\models\avatar\UserBillDefault;
use common\models\HD;
use common\models\HDtown;
use common\models\UserAvatar;
use common\models\UserDocument;
use common\services\Security\AES;
use cs\base\BaseController;
use cs\services\SitePath;
use cs\services\UploadFolderDispatcher;
use cs\services\VarDumper;
use cs\web\Exception;
use Yii;
use yii\base\UserException;
use yii\bootstrap\ActiveForm;
use yii\db\Connection;
use yii\db\Query;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use yii\helpers\FileHelper;
use yii\web\Response;
use Dompdf\Dompdf;

class CabinetGoogleCodeController extends \avatar\controllers\CabinetBaseController
{
    /**
     */
    public function actionSet()
    {
        $model = new \avatar\models\forms\GoogleAuth();

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $model->action();
        }

        return $this->render('@avatar/views/cabinet-google-code/set.php', ['model' => $model]);
    }

    /**
     * Сбрасывает код
     *
     * AJAX
     */
    public function actionReset()
    {
        /** @var \common\models\UserAvatar $user */
        $user = Yii::$app->user->identity;
        $user->google_auth_code = null;
        $user->save();
        Yii::$app->session->remove('googleKey');

        return self::jsonSuccess();
    }

    /**
     * Выдает на скачивание PDF с секретным кодом
     *
     * @param string $code
     * @param string $email
     *
     * @return \yii\web\Response
     * @throws
     */
    public function actionDownload($code, $email)
    {
        // готовлю HTML для PDF
        $filePath = \Yii::getAlias('@avatar/views/cabinet-google-code/_qr-pdf.php');
        $contentHtml = \Yii::$app->view->renderFile($filePath, [
            'code' => $code,
            'email' => $email,
        ]);

        $dompdf = new Dompdf();
        $dompdf->loadHtml($contentHtml);
        $dompdf->setPaper('A4', 'portrait');
        $dompdf->render();
        $dompdf->stream();
    }


}
