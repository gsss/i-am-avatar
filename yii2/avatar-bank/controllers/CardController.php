<?php

namespace avatar\controllers;

use app\models\Article;
use app\models\SiteUpdate;
use app\services\Subscribe;
use common\components\sms\IqSms;
use cs\base\BaseController;
use cs\services\UploadFolderDispatcher;
use cs\services\VarDumper;
use cs\web\Exception;
use Yii;
use yii\base\UserException;
use yii\db\Connection;
use yii\db\Query;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use yii\helpers\FileHelper;
use yii\web\Response;

class CardController extends \avatar\base\BaseController
{

    /**
     */
    public function actionIndex()
    {
        return $this->render();
    }

    /**
     */
    public function actionAsk($card)
    {
        return $this->render(['card' => $card]);
    }

    /**
     */
    public function actionAskQuick($card)
    {
        return $this->render(['card' => $card]);
    }


}
