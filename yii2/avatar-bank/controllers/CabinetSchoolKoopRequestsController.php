<?php

namespace avatar\controllers;

use avatar\models\forms\UserAvatar;
use avatar\models\UserEnter;
use common\models\school\AdminLink;
use common\models\school\School;
use common\services\FormAjax\ActiveForm;
use cs\Application;
use cs\services\VarDumper;
use iAvatar777\services\FormAjax\DefaultFormAjax;
use iAvatar777\services\FormAjax\DefaultFormDelete;
use Yii;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;

class CabinetSchoolKoopRequestsController extends CabinetSchoolBaseController
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => '\yii\filters\AccessControl',
                'rules' => [
                    [
                        'allow'         => true,
                        'matchCallback' => function ($rule, $action) {
                            if (in_array($action->id, [
                                'index',
                            ])) {

                                $school_id = Yii::$app->request->get('id');

                            } else if (in_array($action->id, [
                                'view',
                            ])) {

                                $id = Yii::$app->request->get('id');
                                $u = UserEnter::findOne($id);
                                $school_id = $u->school_id;

                            } else if (in_array($action->id, [
                                'sign-ajax',
                                'delete',
                            ])) {

                                $id = ArrayHelper::getValue(Yii::$app->request->post(),'id');
                                $u = UserEnter::findOne($id);
                                $school_id = $u->school_id;

                            } else if (in_array($action->id, [
                                'sign2-ajax',
                            ])) {

                                $id = ArrayHelper::getValue(Yii::$app->request->post(),'CabinetSchoolKoopSign2.id');
                                $u = UserEnter::findOne($id);
                                $school_id = $u->school_id;

                            }
                            if (!Application::isInteger($school_id)) throw new \Exception('$school_id не целое');

                            $isAdmin = AdminLink::find()
                                ->where([
                                    'user_id'   => Yii::$app->user->id,
                                    'school_id' => $school_id,
                                ])
                                ->exists();

                            return $isAdmin;
                        },
                    ],
                ],
            ],
        ];
    }

    public function actions()
    {
        return [
            'delete' => [
                'class'    => '\avatar\controllers\actions\DefaultFormDelete',
                'model'    => '\avatar\models\UserEnter',
            ],
            'sign-ajax' => [
                'class'    => '\avatar\controllers\actions\DefaultAjax',
                'model'    => '\avatar\models\validate\CabinetSchoolKoopSign',
            ],
            'sign2-ajax' => [
                'class'    => '\iAvatar777\services\FormAjax\DefaultFormAjax',
                'model'    => '\avatar\models\validate\CabinetSchoolKoopSign2',
            ],
        ];
    }

    /**
     * @return string
     */
    public function actionIndex($id)
    {
        $school = School::findOne($id);

        return $this->render(['school' => $school]);
    }

    /**
     * @return string
     */
    public function actionView($id)
    {
        $item = UserEnter::findOne($id);
        $school = School::findOne($item->school_id);

        return $this->render([
            'item'   => $item,
            'school' => $school,
        ]);
    }

}
