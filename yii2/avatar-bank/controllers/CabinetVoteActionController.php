<?php

namespace avatar\controllers;

use common\models\school\School;
use common\models\subscribe\SubscribeMail;
use common\models\VoteAnswer;
use common\models\VoteItem;
use common\models\VoteList;
use Yii;

class CabinetVoteActionController extends \avatar\controllers\CabinetBaseController
{
    public function actions()
    {
        return [
            'sign' => [
                'class' => '\avatar\controllers\actions\DefaultAjax',
                'model' => '\avatar\models\forms\CabinetVoteSignVer1',
            ],
            'sign2' => [
                'class' => '\iAvatar777\services\FormAjax\DefaultFormAjax',
                'model' => '\avatar\models\validate\CabinetVoteSignVer2',
            ],
        ];
    }

    public function actionIndex($id)
    {
        $school = School::findOne($id);

        return $this->renderFrontend(['school' => $school]);
    }

    /**
     */
    public function actionItem($id)
    {
        $item = VoteList::findOne($id);
        $school = School::findOne($item->school_id);

        return $this->renderFrontend([
            'item' => $item,
            'school' => $school,
        ]);
    }

    /**
     */
    public function actionAnswer($id)
    {
        $VoteAnswer = VoteAnswer::findOne($id);

        // Проверяю голосовал ли уже
        if (VoteItem::find()->where(['user_id' => Yii::$app->user->id, 'answer_id'  => $id])->exists()) {
            return self::jsonErrorId(1, 'Голос уже отдан');
        }

        // Записываю голос
        $item = new VoteItem([
            'comment'    => self::getParam('comment'),
            'user_id'    => Yii::$app->user->id,
            'answer_id'  => $id,
            'created_at' => time(),
            'list_id'    => $VoteAnswer->list_id,
        ]);
        $item->hash = $item->hash();
        $item->save();

        return self::jsonSuccess();
    }
}
