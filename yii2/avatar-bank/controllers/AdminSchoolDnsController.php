<?php

namespace avatar\controllers;

use app\services\Subscribe;
use avatar\controllers\AdminBaseController;
use avatar\models\forms\BlogItem;
use avatar\models\forms\UserAvatar;
use avatar\modules\UniSender\UniSender;
use common\models\school\DnsRequest;
use common\models\school\PageBlock;
use common\models\school\PageBlockCategory;
use cs\Application;
use cs\models\Tables\Client;
use cs\services\VarDumper;
use cs\web\Exception;
use Yii;
use yii\base\UserException;
use yii\filters\AccessControl;
use yii\helpers\Json;
use yii\helpers\Url;
use yii\httpclient\Client as ClientHttp;
use yii\web\Controller;
use yii\filters\VerbFilter;
use cs\base\BaseController;
use yii\web\Response;
use yii\widgets\ActiveForm;

class AdminSchoolDnsController extends AdminBaseController
{
    public function actionIndex()
    {
        return $this->render();
    }

    public function actions()
    {
        return [
            'accept' => [
                'class'    => '\avatar\controllers\actions\DefaultAjax',
                'model'    => '\avatar\models\validate\AdminSchoolDnsAccept',
            ],
        ];
    }

    /**
     * REQUEST:
     * - id - int - идентификатор заявки
     * - text - string - причина отказа
     *
     * @return Response
     * error
     * 101 - Заявка уже имеет статус
     */
    public function actionReject()
    {
        $id = self::getParam('id');
        $request = DnsRequest::findOne($id);
        if (in_array($request->is_dns_verified, [-1, 1])) {
            return self::jsonErrorId(101, 'Заявка уже имеет статус');
        }
        $request->is_dns_verified = -1;
        $ret = $request->save();
        if (!$ret) VarDumper::dump($request->errors);

        // Отправляю письмо создателю школы
        \common\services\Subscribe::sendArray(
            [$request->getSchool()->getUser()->email],
            'Заявка на подключение домена отклонена',
            'school/dns-reject',
            [
                'request' => $request,
                'reason'  => self::getParam('text'),
            ]
        );

        return self::jsonSuccess();
    }

}
