<?php

namespace avatar\controllers;

use avatar\models\forms\UserAvatar;
use avatar\models\validate\CardNumberAccept;
use avatar\modules\ETH\ServiceEtherScan;
use avatar\modules\ETH\ServiceEthPlorer;
use common\components\Card;
use common\components\providers\BTC;
use common\components\providers\ETH;
use common\models\avatar\Currency;
use common\models\avatar\UserBill;
use common\models\Config;
use common\models\school\AdminLink;
use common\models\school\School;
use common\models\UserSeed;
use common\payment\BitCoinBlockTrailPayment;
use common\services\Security;
use common\services\Subscribe;
use console\controllers\MoneyRateController;
use cs\services\VarDumper;
use cs\web\Exception;
use cs\Widget\FileUpload4\ModelFields;
use Yii;
use yii\bootstrap\ActiveForm;
use yii\helpers\FileHelper;
use yii\helpers\Json;
use yii\helpers\StringHelper;
use yii\httpclient\Client;
use yii\imagine\Image;
use yii\web\Response;

class CabinetSchoolAdminsController extends CabinetSchoolBaseController
{

    /**
     * @return string
     */
    public function actionIndex($id)
    {
        $school = School::findOne($id);

        return $this->render(['school' => $school]);
    }

    public function actionAdd($id)
    {
        $model = new \avatar\models\forms\school\AdminLink(['_school_id' => $id]);
        $school = School::findOne($id);

        if ($model->load(Yii::$app->request->post()) && $model->insert()) {
            Yii::$app->session->setFlash('form', 1);
            $i = AdminLink::findOne($model->id);
            $i->school_id = $id;
            $i->save();
        }

        return $this->render([
            'model'  => $model,
            'school' => $school,
        ]);
    }


    public function actionDelete($id)
    {
        $link = AdminLink::findOne($id);
        $this->isAccess($link->school_id);

        AdminLink::findOne($id)->delete();

        return self::jsonSuccess();
    }

    public function actionSearchAjax($id)
    {
        $this->isAccess($id);
        $term = self::getParam('term');
        $items = AdminLink::find()->where(['school_id' => $id])->select(['user_id'])->column();
        $rows2 = [];
        $query = \common\models\UserAvatar::find()
            ->select([
                'id',
                'email',
                'name_first',
                'name_last',
            ])
            ->where(['like', 'email', $term])
            ;
        if (count($items) > 0) $query->andWhere(['not', ['id' => $items]]);

        $rows = $query
            ->asArray()
            ->limit(10)
            ->all();

        foreach ($rows as $item) {
            $rows2[] = [
                'id'    => $item['id'],
                'value' => ($item['name_first'] . $item['name_last'] . '' == '') ? $item['email'] : $item['name_first'] . ' ' . $item['name_last'] . ' (' . $item['email'] . ')',
            ];
        }

        return self::jsonSuccess($rows2);
    }
}
