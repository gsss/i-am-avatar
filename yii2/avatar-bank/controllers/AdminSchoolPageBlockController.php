<?php

namespace avatar\controllers;

use app\services\Subscribe;
use avatar\controllers\AdminBaseController;
use avatar\models\forms\BlogItem;
use avatar\models\forms\UserAvatar;
use avatar\modules\UniSender\UniSender;
use common\models\school\PageBlock;
use common\models\school\PageBlockCategory;
use cs\models\Tables\Client;
use cs\services\VarDumper;
use cs\web\Exception;
use Yii;
use yii\base\UserException;
use yii\filters\AccessControl;
use yii\helpers\Json;
use yii\helpers\Url;
use yii\httpclient\Client as ClientHttp;
use yii\web\Controller;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use app\models\NewsItem;
use app\models\Chenneling;
use cs\base\BaseController;
use yii\web\Response;
use yii\widgets\ActiveForm;
use app\models\SiteUpdate;

class AdminSchoolPageBlockController extends AdminBaseController
{
    public function actionIndex($id)
    {
        $category = PageBlockCategory::findOne($id);

        return $this->render(['category' => $category]);
    }

    /**
     * @param int $id school_page_block_category.id
     *
     * @return string
     */
    public function actionAdd($id)
    {
        $model = new \avatar\models\forms\school\PageBlock();
        $category = PageBlockCategory::findOne($id);

        if ($model->load(Yii::$app->request->post()) && $model->insert()) {
            Yii::$app->session->setFlash('form', $model->id);
            $i = PageBlock::findOne($model->id);
            $i->category_id = $id;
            $i->save();
        }

        return $this->render([
            'model'    => $model,
            'category' => $category,
        ]);
    }


    public function actionEdit($id)
    {
        $model = \avatar\models\forms\school\PageBlock::findOne($id);
        $category = PageBlockCategory::findOne($model->category_id);

        if ($model->load(Yii::$app->request->post()) && $model->update()) {
            Yii::$app->session->setFlash('form', $model->id);
        }

        return $this->render([
            'model'    => $model,
            'category' => $category,
        ]);
    }

    public function actionDelete($id)
    {
        \avatar\models\forms\school\PageBlock::findOne($id)->delete();

        return self::jsonSuccess();
    }

    public function actionSortAjax($id)
    {
        $ids = self::getParam('ids');

        $c = 0;
        foreach ($ids as $id) {
            $category = PageBlock::findOne($id);
            $category->sort_index = $c;
            $category->save();
            $c++;
        }

        return self::jsonSuccess();
    }

    public function actionSort()
    {

        return $this->render();
    }
}
