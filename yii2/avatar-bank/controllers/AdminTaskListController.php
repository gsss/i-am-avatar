<?php

namespace avatar\controllers;

use avatar\models\forms\UserAvatar;
use avatar\models\validate\CardNumberAccept;
use avatar\modules\ETH\ServiceEtherScan;
use avatar\modules\ETH\ServiceEthPlorer;
use common\components\Card;
use common\components\providers\BTC;
use common\components\providers\ETH;
use common\models\avatar\Currency;
use common\models\avatar\UserBill;
use common\models\Config;
use common\models\school\Master;
use common\models\school\School;
use common\models\task\Category;
use common\models\task\Task;
use common\models\UserSeed;
use common\payment\BitCoinBlockTrailPayment;
use common\services\Security;
use console\controllers\MoneyRateController;
use cs\services\VarDumper;
use cs\web\Exception;
use cs\Widget\FileUpload4\ModelFields;
use Yii;
use yii\base\Event;
use yii\helpers\FileHelper;
use yii\helpers\Json;
use yii\helpers\StringHelper;
use yii\httpclient\Client;
use yii\imagine\Image;
use yii\web\Response;

class AdminTaskListController extends \avatar\controllers\AdminBaseController
{

    /**
     * @return string
     */
    public function actionIndex($id)
    {
        $school = School::findOne($id);
        return $this->render(['school' => $school]);
    }

    /**
     * @return string
     */
    public function actionAjail($id)
    {
        $school = School::findOne($id);

        return $this->render(['school' => $school]);
    }

    /**
     * - item_id - идентификатор задачи
     * - list_id - идентификатор статуса
     *
     * @return string
     */
    public function actionSet()
    {
        $item_id = self::getParam('item_id');
        $list_id = self::getParam('list_id');
        $Task = Task::findOne($item_id);
        $Task->status = $list_id;
        $Task->save();

        return self::jsonSuccess();
    }

    public function actionAdd($id)
    {
        $model = new Task();
        Event::on(Task::className(), 'EVENT_BEFORE_INSERT', function (Task $item) {
            $item->price = $item->price * 100;
        });
        $school = School::findOne($id);

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            Yii::$app->session->setFlash('form', 1);
            $model->user_id = Yii::$app->user->id;
            $model->school_id = $id;
            $model->save();
        }

        return $this->render([
            'model' => $model,'school' => $school
        ]);
    }


    public function actionEdit($id)
    {
        $model = Task::findOne($id);
        $school = School::findOne($model->school_id);

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $model->save();
            Yii::$app->session->setFlash('form', 1);
        }

        return $this->render([
            'model' => $model,'school' => $school
        ]);
    }


    public function actionDelete($id)
    {
        Task::findOne($id)->delete();

        return self::jsonSuccess();
    }
}
