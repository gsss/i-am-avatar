<?php

namespace avatar\controllers;

use avatar\models\forms\school\CommandLink;
use avatar\models\forms\UserAvatar;
use avatar\models\validate\CabinetSchoolTaskListControllerHide;
use avatar\models\validate\CardNumberAccept;
use avatar\modules\ETH\ServiceEtherScan;
use avatar\modules\ETH\ServiceEthPlorer;
use common\components\Card;
use common\components\providers\BTC;
use common\components\providers\ETH;
use common\models\avatar\Currency;
use common\models\avatar\UserBill;
use common\models\comment\Comment;
use common\models\comment\CommentList;
use common\models\Config;
use common\models\piramida\Wallet;
use common\models\school\AdminLink;
use common\models\school\Kurs;
use common\models\school\Lesson;
use common\models\school\Page;
use common\models\school\PageBlockContent;
use common\models\school\ProjectManagerLink;
use common\models\school\School;
use common\models\task\Category;
use common\models\task\Session;
use common\models\task\Table;
use common\models\task\Task;
use common\models\UserSeed;
use common\models\UserWallet;
use common\payment\BitCoinBlockTrailPayment;
use common\services\Security;
use common\services\Subscribe;
use console\controllers\MoneyRateController;
use cs\Application;
use cs\services\VarDumper;
use cs\web\Exception;
use cs\Widget\FileUpload4\ModelFields;
use Yii;
use yii\base\Event;
use yii\helpers\ArrayHelper;
use yii\helpers\FileHelper;
use yii\helpers\Html;
use yii\helpers\Json;
use yii\helpers\StringHelper;
use yii\httpclient\Client;
use yii\imagine\Image;
use yii\web\ForbiddenHttpException;
use yii\web\NotFoundHttpException;
use yii\web\Request;
use yii\web\Response;

class CabinetSchoolTaskListTableController extends CabinetSchoolBaseController
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => '\yii\filters\AccessControl',
                'rules' => [
                    [
                        'allow'         => true,
                        'matchCallback' => function ($rule, $action) {
                            if (in_array($action->id, [
                                'index',
                                'add',
                                ])) {

                                $school_id = Yii::$app->request->get('id');

                            } else if (in_array($action->id, [
                                'edit',
                                ])) {
                                $id = Yii::$app->request->get('id');
                                $Table = Table::findOne($id);
                                $school_id = $Table->school_id;

                            } else if (in_array($action->id, [
                                'view',
                                ])) {
                                $id = Yii::$app->request->get('id');
                                $Table = Table::findOne($id);
                                $school_id = $Table->school_id;
                            } else if (in_array($action->id, [
                                'delete',
                                ])) {
                                $id = Yii::$app->request->post('id');
                                $Table = Table::findOne($id);
                                $school_id = $Table->school_id;
                            }
                            if (!Application::isInteger($school_id)) throw new \Exception('$school_id не целое');

                            $isAdmin = AdminLink::find()->where([
                                'user_id'   => Yii::$app->user->id,
                                'school_id' => $school_id,
                            ])->exists();

                            return $isAdmin;
                        },
                    ],
                ],
            ],
        ];
    }

    public function actions()
    {
        return [
            'delete' => [
                'class'    => '\avatar\controllers\actions\DefaultAjax',
                'model'    => '\avatar\models\validate\CabinetSchoolTaskListTableDelete',
                'formName' => '',
            ],
        ];
    }

    /**
     * @return string
     */
    public function actionIndex($id)
    {
        $school = School::findOne($id);

        return $this->render(['school' => $school]);
    }

    /**
     * @param int $id \common\models\task\Table.id
     *
     * @return string
     * @throws ForbiddenHttpException
     * @throws NotFoundHttpException
     */
    public function actionView($id)
    {
        $model = \common\models\task\Table::findOne($id);
        $school = School::findOne($model->school_id);

        return $this->render([
            'model'  => $model,
            'school' => $school,
        ]);
    }

    public function actionAdd($id)
    {
        $model = new \avatar\models\forms\Table();
        $school = School::findOne($id);

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $model->school_id = $id;
            $model->save();
            $model->id = $model::getDb()->lastInsertID;
            Yii::$app->session->setFlash('form', $model->id);
        }

        return $this->render([
            'model'  => $model,
            'school' => $school,
        ]);
    }

    /**
     * Редаkктирование задачи, выводит форму и обрабатывает ее
     *
     * @param $id
     *
     * @return string
     * @throws ForbiddenHttpException
     * @throws NotFoundHttpException
     */
    public function actionEdit($id)
    {
        $model = \avatar\models\forms\Table::findOne($id);
        if (is_null($model)) {
            throw new NotFoundHttpException('Не найдена доска');
        }
        $school = School::findOne($model->school_id);

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $model->save();
            Yii::$app->session->setFlash('form', $model->id);
        }

        return $this->render([
            'model'  => $model,
            'school' => $school,
        ]);
    }

}
