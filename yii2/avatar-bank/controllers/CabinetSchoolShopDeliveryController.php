<?php

namespace avatar\controllers;

use app\common\components\Piramida;
use app\models\Article;
use app\models\Form\Shop\Order;
use app\models\Piramida\WalletSource\Custom;
use app\models\Piramida\WalletSource\Pochta;
use app\models\Piramida\WalletSource\Yandex;
use app\models\Shop;
use app\models\Shop\Product;
use app\models\Shop\Request;
use app\models\Shop\RequestMessage;
use app\models\Shop\RequestProduct;
use app\models\Union;
use app\models\User;
use app\modules\Shop\services\Basket;
use common\models\avatar\Currency;
use common\models\school\School;
use cs\Application;
use cs\services\VarDumper;
use cs\web\Exception;
use Yii;
use yii\base\UserException;
use yii\bootstrap\Html;
use yii\db\Query;


/**
 * Обслуживает действия магазина
 *
 * @package app\controllers
 */
class CabinetSchoolShopDeliveryController extends \avatar\controllers\CabinetSchoolBaseController
{
    /**
     *
     * @param int $id school.id
     */
    public function actionIndex($id)
    {
        $this->isAccess($id);
        $school = School::findOne($id);

        return $this->render(['school' => $school]);
    }


    /**
     * Добавление Доставки
     * @param int $id идентификатор объединения gs_unions.id
     * @return string|\yii\web\Response
     * @throws \cs\web\Exception
     */
    public function actionAdd($id)
    {
        $this->isAccess($id);
        $model = new \common\models\shop\DeliveryItem();
        $school = School::findOne($id);

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            Yii::$app->session->setFlash('form');
            $model->school_id = $id;
            if ($model->price) {
                if ($model->currency_id) {
                    $c = Currency::findOne($model->currency_id);
                    $model->price = $model->price * pow(10, $c->decimals);
                }
            }
            $model->save();
        }

        return $this->render([
            'model'  => $model,
            'school' => $school,
        ]);
    }

    /**
     * Редактирование товара
     *
     * @param int $id идентификатор товара gs_unions_shop_product.id
     *
     * @return string|\yii\web\Response
     *
     * @throws \cs\web\Exception
     */
    public function actionEdit($id)
    {
        $model = \common\models\shop\DeliveryItem::findOne($id);
        $school = School::findOne($model->school_id);
        $this->isAccess($model->school_id);
        if ($model->currency_id) {
            $c = Currency::findOne($model->currency_id);
            $model->price = (int)($model->price / pow(10, $c->decimals));
        }

        if ($model->load(Yii::$app->request->post()) && $model->validate($id)) {
            if ($model->price) {
                if ($model->currency_id) {
                    $c = Currency::findOne($model->currency_id);
                    $model->price = $model->price * pow(10, $c->decimals);
                }
            }
            $model->save();
            Yii::$app->session->setFlash('form');
        }

        return $this->render([
            'model' => $model,
            'school' => $school,
        ]);
    }

    public function actionDelete($id)
    {
        $model = \common\models\shop\DeliveryItem::findOne($id);
        $this->isAccess($model->school_id);

        $model->delete();

        return self::jsonSuccess();
    }
}
