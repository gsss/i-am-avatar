<?php

namespace avatar\controllers;

use avatar\controllers\AdminBaseController;
use avatar\models\forms\BlogItem;
use avatar\models\forms\UserAvatar;
use avatar\modules\UniSender\UniSender;
use common\models\school\SubscribeItem;
use common\models\UserRoot;
use cs\models\Tables\Client;
use cs\services\VarDumper;
use cs\web\Exception;
use Yii;
use yii\base\UserException;
use yii\filters\AccessControl;
use yii\helpers\Json;
use yii\helpers\Url;
use yii\httpclient\Client as ClientHttp;
use yii\web\Controller;
use yii\filters\VerbFilter;
use cs\base\BaseController;
use yii\web\Response;
use yii\widgets\ActiveForm;

class AdminBlogController extends AdminBaseController
{
    public $type_id = 8;

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['role_content'],
                    ],
                ],
            ],
        ];
    }

    public function actionIndex()
    {
        return $this->render();
    }

    public function actionAdd()
    {
        $model = new BlogItem(['_school_id' => 2, '_type_id' => $this->type_id]);

        if ($model->load(Yii::$app->request->post()) && $model->insert()) {
            Yii::$app->session->setFlash('form', $model->id);

            $i = \common\models\BlogItem::findOne($model->id);

            /** @var \avatar\services\CustomizeService $CustomizeService */
            $CustomizeService = Yii::$app->CustomizeService;
            $i->company_id = $CustomizeService->getParam1('id');

            $i->created_at = time();
            $i->save();

            return $this->refresh();
        } else {
            return $this->render([
                'model' => $model,
            ]);
        }
    }


    public function actionEdit($id)
    {
        $model = BlogItem::findOne($id);
        $model->_school_id = 2;
        $model->_type_id = $this->type_id;

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $model->update();
            Yii::$app->session->setFlash('form', $id);
        }

        return $this->render([
            'model' => $model,
        ]);
    }

    /**
     * AJAX
     * Формирует данные для рассылки
     *
     * @param integer $id - идентификатор послания
     *
     * @return string
     */
    public function actionSubscribe($id)
    {
        $model = new \avatar\models\validate\AdminBlogControllerSubscribe();

        if (!$model->load(Yii::$app->request->get(), '')) {
            return self::jsonErrorId(400, 'Не загружены данные');
        }
        if (!$model->validate()) {
            return self::jsonErrorId(102, $model->errors);
        }
        $data = $model->action();

        return self::jsonSuccess(['item' => $data]);
    }

    /**
     * AJAX
     * Формирует данные для рассылки
     *
     * @param integer $id - идентификатор послания
     *
     * @return string
     */
    public function actionSubscribeOne($id)
    {
        $item = \common\models\BlogItem::findOne($id);
        if (is_null($item)) {
            return self::jsonErrorId(101, 'Не найдена статья');
        }

        $emailList = ['dram1008@yandex.ru'];

        $subscribeProcess = \common\services\Subscribe::sendArray(
            $emailList,
            $item->name,
            'subscribe/blog-item',
            [
                'newsItem' => $item
            ],
            'layouts/html/subscribe'
        );

        return self::jsonSuccess();
    }

    public function actionDelete($id)
    {
        BlogItem::findOne($id)->delete();

        return self::jsonSuccess();
    }
}
