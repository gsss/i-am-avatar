<?php

namespace avatar\controllers;

use avatar\models\forms\UserAvatar;
use avatar\models\validate\CardNumberAccept;
use avatar\modules\ETH\ServiceEtherScan;
use avatar\modules\ETH\ServiceEthPlorer;
use common\components\Card;
use common\components\providers\BTC;
use common\components\providers\ETH;
use common\models\avatar\Currency;
use common\models\avatar\UserBill;
use common\models\Config;
use common\models\school\Kurs;
use common\models\school\Potok;
use common\models\school\PotokUser3Link;
use common\models\school\PotokUserExt;
use common\models\school\School;
use common\models\school\UserLink;
use common\models\UserRoot;
use common\models\UserSeed;
use common\payment\BitCoinBlockTrailPayment;
use common\services\Security;
use console\controllers\MoneyRateController;
use cs\services\VarDumper;
use cs\web\Exception;
use cs\Widget\FileUpload4\ModelFields;
use Yii;
use yii\helpers\FileHelper;
use yii\helpers\Json;
use yii\helpers\StringHelper;
use yii\httpclient\Client;
use yii\imagine\Image;
use yii\web\Response;

class CabinetSchoolPotokController extends CabinetSchoolBaseController
{
    public function actions()
    {
        return [
            'sms-send' => [
                'class'    => '\avatar\controllers\actions\DefaultAjax',
                'model'    => '\avatar\models\validate\CabinetSchoolPotokControllerSmsSend',
                'formName' => '',
            ],
        ];
    }

    /**
     * @param int $id идентификатор курса
     *
     * @return string
     */
    public function actionIndex($id)
    {
        $kurs = Kurs::findOne($id);
        $this->isAccess($kurs->school_id);
        $school = School::findOne($kurs->school_id);

        return $this->render(['school' => $school, 'kurs' => $kurs]);
    }

    /**
     * @param int $id идентификатор потока
     *
     * @return string
     */
    public function actionView($id)
    {
        $potok = Potok::findOne($id);
        $this->isAccess($potok->getKurs()->school_id);
        $school = School::findOne($potok->getKurs()->school_id);

        return $this->render([
            'school' => $school,
            'potok'  => $potok,
        ]);
    }

    /**
     * @param int $id идентификатор потока
     *
     * @return string
     */
    public function actionUsers($id)
    {
        $potok = Potok::findOne($id);
        $kurs = Kurs::findOne($potok->kurs_id);
        $this->isAccess($kurs->school_id);
        $school = School::findOne($kurs->school_id);

        return $this->render([
            'school' => $school,
            'kurs'   => $kurs,
            'potok'  => $potok,
        ]);
    }

    /**
     * @param int $id идентификатор потока
     *
     * @return string
     */
    public function actionUsersAdd($id)
    {
        $potok = Potok::findOne($id);
        $kurs = Kurs::findOne($potok->kurs_id);
        $this->isAccess($kurs->school_id);
        $school = School::findOne($kurs->school_id);
        $model = new \avatar\models\forms\school\Lid(['potok_id' => $potok->id]);

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            Yii::$app->session->setFlash('form', 1);
            $model->action($potok->id, $school->id);
        }

        return $this->render([
            'model'  => $model,
            'school' => $school,
            'kurs'   => $kurs,
            'potok'  => $potok,
        ]);
    }

    /**
     * @param int $id идентификатор school_potok_user_root_link
     *
     * @return string
     * @throws
     */
    public function actionUsersDelete($id)
    {
        $link = PotokUser3Link::findOne($id);
        // проверка доступа
        $potok = Potok::findOne($link->potok_id);
        $kurs = Kurs::findOne($potok->kurs_id);
        $this->isAccess($kurs->school_id);

        $ext = PotokUserExt::findOne(['link_id' => $id]);
        if (!is_null($ext)) $ext->delete();
        $isAvatar = $link->is_avatar;
        $user_root_id = $link->user_root_id;
        $link->delete();

        // удаление user_school_link
        if (!$isAvatar) {
            // проверяю состоит ли user_root_id в других потоках
            if (!PotokUser3Link::find()->where(['user_root_id' => $user_root_id])->exists()) {
                // если не существует то удаляю user_school_link
                $link = UserLink::findOne(['user_root_id' => $user_root_id]);
                if (!is_null($link)) {
                    $link->delete();
                }
            }
        }

        return self::jsonSuccess();
    }

    public function actionAdd($id)
    {
        $model = new \avatar\models\forms\school\Potok();
        $Kurs = Kurs::findOne($id);
        $this->isAccess($Kurs->school_id);
        $school = $Kurs->getSchool();

        if ($model->load(Yii::$app->request->post()) && $model->insert()) {
            Yii::$app->session->setFlash('form', 1);
            $i = Potok::findOne($model->id);
            $i->kurs_id = $id;
            $i->save();

        }

        return $this->render([
            'model'  => $model,
            'school' => $school,
            'kurs'   => $Kurs,
        ]);
    }


    public function actionEdit($id)
    {
        $model = \avatar\models\forms\school\Potok::findOne($id);
        $Potok = Potok::findOne($id);
        $Kurs = Kurs::findOne($Potok->kurs_id);
        $school = $Potok->getSchool();
        $this->isAccess($school->id);

        if ($model->load(Yii::$app->request->post()) && $model->update()) {
            Yii::$app->session->setFlash('form', 1);
        }

        return $this->render([
            'model'  => $model,
            'kurs'   => $Kurs,
            'school' => $school,
        ]);
    }


    public function actionDelete($id)
    {
        /** @var \avatar\models\forms\school\Potok $potok */
        $potok = \avatar\models\forms\school\Potok::findOne($id);
        if (is_null($potok)) throw new \Exception('Не найден $potok');
        $Kurs = Kurs::findOne($potok->kurs_id);
        $this->isAccess($Kurs->school_id);

        $potok->delete();

        return self::jsonSuccess();
    }
}
