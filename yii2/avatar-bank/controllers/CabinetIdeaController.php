<?php

namespace avatar\controllers;

use avatar\models\forms\UserAvatar;
use avatar\models\validate\CardNumberAccept;
use avatar\modules\ETH\ServiceEtherScan;
use avatar\modules\ETH\ServiceEthPlorer;
use common\components\Card;
use common\components\providers\BTC;
use common\components\providers\ETH;
use common\models\avatar\Currency;
use common\models\avatar\UserBill;
use common\models\Config;
use common\models\school\Kurs;
use common\models\school\Lesson;
use common\models\school\Master;
use common\models\school\Page;
use common\models\school\School;
use common\models\UserIdea;
use common\models\UserSeed;
use common\payment\BitCoinBlockTrailPayment;
use common\services\Security;
use console\controllers\MoneyRateController;
use cs\services\VarDumper;
use cs\web\Exception;
use cs\Widget\FileUpload4\ModelFields;
use Yii;
use yii\helpers\FileHelper;
use yii\helpers\Json;
use yii\helpers\StringHelper;
use yii\httpclient\Client;
use yii\imagine\Image;
use yii\web\Response;

class CabinetIdeaController extends \avatar\controllers\CabinetBaseController
{

    /**
     * @return string
     */
    public function actionIndex()
    {
        return $this->render();
    }

    public function actionAdd()
    {
        $model = new UserIdea();

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            Yii::$app->session->setFlash('form', 1);
            $model->user_id = Yii::$app->user->id;
            $model->save();
        }

        return $this->render([
            'model' => $model,
        ]);
    }

    public function actionEdit($id)
    {
        $model = UserIdea::findOne($id);

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $model->save();
            Yii::$app->session->setFlash('form', 1);
        }

        return $this->render([
            'model' => $model,
        ]);
    }


    public function actionView($id)
    {
        $model = UserIdea::findOne($id);

        return $this->render([
            'model' => $model,
        ]);
    }


    public function actionDelete($id)
    {
        UserIdea::findOne($id)->delete();

        return self::jsonSuccess();
    }
}
