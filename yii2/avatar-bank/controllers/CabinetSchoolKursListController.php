<?php

namespace avatar\controllers;

use avatar\models\forms\UserAvatar;
use avatar\models\validate\CardNumberAccept;
use avatar\modules\ETH\ServiceEtherScan;
use avatar\modules\ETH\ServiceEthPlorer;
use common\components\Card;
use common\components\providers\BTC;
use common\components\providers\ETH;
use common\models\avatar\Currency;
use common\models\avatar\UserBill;
use common\models\comment\Comment;
use common\models\comment\CommentList;
use common\models\Config;
use common\models\school\AdminLink;
use common\models\school\Kurs;
use common\models\school\Lesson;
use common\models\school\LessonPage;
use common\models\school\LessonVebinar;
use common\models\school\LessonVideo;
use common\models\school\Page;
use common\models\school\PageBlockContent;
use common\models\school\PageStat;
use common\models\school\Potok;
use common\models\school\PotokUser3Link;
use common\models\school\School;
use common\models\school\SchoolSale;
use common\models\school\SchoolSaleRequest;
use common\models\school\Subscribe;
use common\models\school\SubscribeItem;
use common\models\school\SubscribeSms;
use common\models\school\SubscribeSmsResult;
use common\models\task\Task;
use common\models\UserSeed;
use common\payment\BitCoinBlockTrailPayment;
use common\services\Security;
use console\controllers\MoneyRateController;
use cs\Application;
use cs\services\VarDumper;
use cs\web\Exception;
use cs\Widget\FileUpload4\ModelFields;
use Yii;
use yii\filters\AccessControl;
use yii\helpers\FileHelper;
use yii\helpers\Json;
use yii\helpers\StringHelper;
use yii\httpclient\Client;
use yii\imagine\Image;
use yii\web\NotFoundHttpException;
use yii\web\Response;

class CabinetSchoolKursListController extends CabinetSchoolBaseController
{
    public $type_id = 15;

    public function behaviors()
    {
        return [
            'access' => [
                'class' => '\yii\filters\AccessControl',
                'rules' => [
                    [
                        'allow'         => true,
                        'matchCallback' => function ($rule, $action) {
                            $isRunAction = false;
                            if (in_array($action->id, ['delete'])) {
                                $isRunAction = true;
                                $kurs_id = Yii::$app->request->get('id');
                                $k = Kurs::findOne($kurs_id);
                                if (is_null($k)) {
                                    throw new NotFoundHttpException();
                                }
                                $school_id = $k->school_id;
                            }
                            if (!$isRunAction) return true;

                            if (!Application::isInteger($school_id)) throw new \Exception('$school_id не целое');

                            $isAdmin = AdminLink::find()->where([
                                'user_id'   => Yii::$app->user->id,
                                'school_id' => $school_id,
                            ])->exists();

                            return $isAdmin;
                        },
                    ],
                ],
            ],
        ];
    }

    public function actions()
    {
        return [
            'action' => '\avatar\controllers\actions\CabinetSchoolKursListController\actionDelete'
        ];
    }

    /**
     * @return string
     */
    public function actionIndex($id)
    {
        $this->isAccess($id);
        $school = School::findOne($id);

        return $this->render(['school' => $school]);
    }

    /**
     * @param int $id
     * @return string
     */
    public function actionPotok($id)
    {
        $kurs = Kurs::findOne($id);
        $this->isAccess($kurs->school_id);
        $school = School::findOne($kurs->school_id);

        return $this->render(['school' => $school, '$kurs' => $kurs]);
    }

    public function actionAdd($id)
    {
        $this->isAccess($id);
        $school = School::findOne($id);
        $model = new \avatar\models\forms\school\Kurs(['_school_id' => $school->id, '_type_id' => $this->type_id]);

        if ($model->load(Yii::$app->request->post()) && $model->insert()) {
            Yii::$app->session->setFlash('form', $model->id);
            $i = Kurs::findOne($model->id);
            $i->school_id = $id;
            $i->save();
        }

        return $this->render([
            'model'  => $model,
            'school' => $school,
        ]);
    }


    public function actionEdit($id)
    {
        $model = \avatar\models\forms\school\Kurs::findOne($id);
        $this->isAccess($model->school_id);
        $school = School::findOne($model->school_id);
        $model->_school_id = $school->id;
        $model->_type_id = $this->type_id;

        if ($model->load(Yii::$app->request->post()) && $model->update()) {
            Yii::$app->session->setFlash('form', $model->id);
        }

        return $this->render([
            'model' => $model,'school' => $school
        ]);
    }


}
