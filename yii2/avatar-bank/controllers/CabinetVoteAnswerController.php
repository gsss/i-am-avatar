<?php

namespace avatar\controllers;

use app\services\Subscribe;
use avatar\controllers\AdminBaseController;
use avatar\models\forms\BlogItem;
use avatar\models\forms\UserAvatar;
use avatar\modules\UniSender\UniSender;
use common\models\school\School;
use common\models\subscribe\SubscribeMail;
use common\models\VoteAnswer;
use common\models\VoteItem;
use common\models\VoteList;
use cs\models\Tables\Client;
use cs\services\VarDumper;
use cs\web\Exception;
use Yii;
use yii\base\UserException;
use yii\filters\AccessControl;
use yii\helpers\Json;
use yii\helpers\Url;
use yii\httpclient\Client as ClientHttp;
use yii\web\Controller;
use yii\filters\VerbFilter;
use cs\base\BaseController;
use yii\web\Response;
use yii\widgets\ActiveForm;

class CabinetVoteAnswerController extends \avatar\controllers\CabinetBaseController
{
    public function actionIndex($id)
    {
        $list = \common\models\VoteList::findOne($id);
        $school = School::findOne($list->school_id);

        return $this->render([
            'school' => $school,
            'list' => $list,
        ]);
    }

    public function actionAdd($id)
    {
        $model = new VoteAnswer();
        $list = VoteList::findOne($id);
        $school = School::findOne($list->school_id);

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            Yii::$app->session->setFlash('form', 1);
            $model->list_id = $id;
            $model->save();
        }

        return $this->render([
            'model' => $model,
            'list' => $list,
            'school' => $school,
        ]);
    }


    public function actionEdit($id)
    {
        $model = VoteAnswer::findOne($id);
        $list = VoteList::findOne($model->list_id);
        $school = School::findOne($list->school_id);

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $model->save();
            Yii::$app->session->setFlash('form', $id);
        }

        return $this->render([
            'model' => $model,
            'list' => $list,
            'school' => $school,
        ]);
    }

    public function actionDelete($id)
    {
        $item = VoteAnswer::findOne($id);
        $item->delete();

        return self::jsonSuccess();
    }
}
