<?php

namespace avatar\controllers;

use avatar\models\forms\UserAvatar;
use avatar\models\validate\CardNumberAccept;
use avatar\modules\ETH\ServiceEtherScan;
use avatar\modules\ETH\ServiceEthPlorer;
use common\components\Card;
use common\components\providers\BTC;
use common\components\providers\ETH;
use common\models\avatar\Currency;
use common\models\avatar\UserBill;
use common\models\Config;
use common\models\school\AdminLink;
use common\models\school\Kurs;
use common\models\school\Lesson;
use common\models\school\Page;
use common\models\school\PageBlockContent;
use common\models\school\School;
use common\models\SchoolSntPohod;
use common\models\task\Task;
use common\models\UserSeed;
use common\payment\BitCoinBlockTrailPayment;
use common\services\Security;
use common\services\Subscribe;
use console\controllers\MoneyRateController;
use cs\Application;
use cs\services\VarDumper;
use cs\web\Exception;
use cs\Widget\FileUpload4\ModelFields;
use Yii;
use yii\bootstrap\ActiveForm;
use yii\helpers\FileHelper;
use yii\helpers\Json;
use yii\helpers\StringHelper;
use yii\httpclient\Client;
use yii\imagine\Image;
use yii\web\NotFoundHttpException;
use yii\web\Response;

class CabinetSntController extends CabinetSchoolBaseController
{

    public function actionIndex($id)
    {
        $school = School::findOne($id);

        return $this->render([
            'school' => $school,
        ]);
    }

    public function actionPohodAdd($id)
    {
        $school = School::findOne($id);
        $model = new \avatar\models\forms\SchoolSntPohod(['school' => $school]);

        if (Yii::$app->request->isPost) {
            if ($model->load(Yii::$app->request->post()) && $model->validate()) {
                $s = $model->save();
                return self::jsonSuccess($s);
            } else {
                return self::jsonErrorId(102, $model->getErrors102());
            }
        }

        return $this->render([
            'school' => $school,
            'model' => $model,
        ]);
    }

    public function actionTrudAdd($id)
    {
        $school = School::findOne($id);
        $model = new \avatar\models\forms\SchoolSntPohodUser(['school' => $school]);

        if (Yii::$app->request->isPost) {
            if ($model->load(Yii::$app->request->post()) && $this->onAfterLoad($model) && $model->validate()) {
                $this->onBeforeUpdate($model);
                $model->created_at = time();
                $model->school_id = $id;
                $s = $model->save();

                return self::jsonSuccess($s);
            } else {
                return self::jsonErrorId(102, $model->getErrors102());
            }
        }

        return $this->render([
            'school' => $school,
            'model'  => $model,
        ]);
    }

    public function actionTrudEdit($id)
    {
        $model = \avatar\models\forms\SchoolSntPohodUser::findOne($id);
        $this->onAfterLoadDb($model);
        $school = School::findOne($model->school_id);

        if (Yii::$app->request->isPost) {
            if ($model->load(Yii::$app->request->post()) && $this->onAfterLoad($model) && $model->validate()) {
                $this->onBeforeUpdate($model);
                $s = $model->save();

                return self::jsonSuccess($s);
            } else {
                return self::jsonErrorId(102, $model->getErrors102());
            }
        }

        return $this->render([
            'school' => $school,
            'model'  => $model,
        ]);
    }

    public function onAfterLoad($model)
    {
        $model->start = \DateTime::createFromFormat('Y-m-d H:i', $model->start);
        $model->end = \DateTime::createFromFormat('Y-m-d H:i', $model->end);

        return true;
    }

    public function onAfterLoadDb($model)
    {

        if (!Application::isEmpty($model->start)) {
            $model->start = Yii::$app->formatter->asDatetime($model->start, 'php:Y-m-d H:i');
        }
        if (!Application::isEmpty($model->end)) {
            $model->end = Yii::$app->formatter->asDatetime($model->end, 'php:Y-m-d H:i');
        }

        return true;
    }

    public function onBeforeUpdate($model)
    {
        $model->start = $model->start->format('U');
        $model->end = $model->end->format('U');
        return true;
    }

    public function actionPohodFileAdd($id)
    {
        $pohod = SchoolSntPohod::findOne($id);
        $school = School::findOne($pohod->school_id);
        $model = new \avatar\models\forms\SchoolSntPohodFile(['pohod' => $pohod]);

        if (Yii::$app->request->isPost) {
            if ($model->load(Yii::$app->request->post()) && $model->validate()) {
                $s = $model->save();
                return self::jsonSuccess($s);
            } else {
                return self::jsonErrorId(102, $model->getErrors102());
            }
        }

        return $this->render([
            'pohod'  => $pohod,
            'school' => $school,
            'model'  => $model,
        ]);
    }

    public function actionPohod($id)
    {
        $model = SchoolSntPohod::findOne($id);
        $school = School::findOne($model->school_id);

        return $this->render([
            'school' => $school,
            'model'  => $model,
        ]);
    }

}
