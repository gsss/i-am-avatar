<?php

namespace avatar\controllers;

use avatar\models\forms\UserAvatar;
use avatar\models\validate\CardNumberAccept;
use common\components\Card;
use common\components\providers\BTC;
use common\components\providers\ETH;
use common\models\avatar\Currency;
use common\models\avatar\UserBill;
use common\models\Config;
use common\models\UserSeed;
use common\payment\BitCoinBlockTrailPayment;
use common\services\Security;
use console\controllers\MoneyRateController;
use cs\services\VarDumper;
use cs\web\Exception;
use Yii;
use yii\helpers\FileHelper;
use yii\helpers\Json;
use yii\helpers\StringHelper;
use yii\imagine\Image;
use yii\web\Response;

class CabinetBillsBtcController extends \avatar\controllers\CabinetBaseController
{

    /**
     * Вызывает функцию в контракте
     *
     * @param int $id идентификатор счета
     *
     * @return string
     */
    public function actionExportKey($id)
    {
        $billing = $this->getBill($id);
        $model = new \avatar\models\forms\CabinetBillsBtcExportKey(['billing' => $billing]);

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            return $model->action();
        }

        return $this->render([
            'billing' => $billing,
            'model'   => $model,
        ]);
    }

    /**
     * Выдает курс BTC
     *
     * @param int $id идентификатор валюты
     *
     * @return string
     */
    public function actionKurs()
    {
        $rows = \common\models\ChartPoint::find()
            ->select([
                'usd',
                'time',
            ])
            ->where(['>', 'time', time() - (60 * 60 * 24 * 30)])
            ->andWhere(['currency_id' => \common\models\avatar\Currency::BTC])
            ->all()
        ;

        return self::jsonSuccess([
            'rows'     => $rows,
            'currency' => [
                'code' => 'USD',
            ],
        ]);
    }

    /**
     * Получает счет ID
     * Делает проверку на хозяина
     *
     * @return \common\models\avatar\UserBill
     * @throws \yii\base\Exception
     */
    public function getBill($id = null)
    {
        if (is_null($id)) {
            $id = self::getParam('id');
        }
        $bill = UserBill::findOne($id);
        if ($bill->user_id != Yii::$app->user->id) {
            throw new \yii\base\Exception('Не ваш счет');
        }

        return $bill;
    }

}
