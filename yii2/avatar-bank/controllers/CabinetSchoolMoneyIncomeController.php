<?php

namespace avatar\controllers;

use avatar\models\forms\UserAvatar;
use avatar\models\UserEnter;
use common\models\piramida\Currency;
use common\models\school\AdminLink;
use common\models\school\School;
use common\models\SchoolKoop;
use common\models\SchoolKoopWallet;
use common\services\FormAjax\ActiveForm;
use cs\Application;
use cs\services\VarDumper;
use iAvatar777\services\FormAjax\DefaultFormAjax;
use Yii;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;

class CabinetSchoolMoneyIncomeController extends CabinetSchoolBaseController
{
    public function actions()
    {
        return [
//            'view-sign' => [
//                'class'    => '\iAvatar777\services\FormAjax\DefaultFormAjax',
//                'model'    => '\avatar\models\validate\CabinetSchoolKoopSign',
//            ],
        ];
    }

    /**
     * @return string
     */
    public function actionIndex($id)
    {
        $school = School::findOne($id);

        return $this->render(['school' => $school]);
    }


    /**
     * @return string
     */
    public function actionAdd($id)
    {
        $school = School::findOne($id);
        $model = new \avatar\models\forms\SchoolMoneyIncome([
            'school_id'   => $id,
            'user_id'     => Yii::$app->user->id,
            'currency_id' => Currency::RUB,
        ]);

        if (Yii::$app->request->isPost) {
            if ($model->load(Yii::$app->request->post()) && $model->validate()) {
                $s = $model->save();
                return self::jsonSuccess($s);
            } else {
                return self::jsonErrorId(102, $model->getErrors102());
            }
        }

        return $this->render([
            'model'  => $model,
            'school' => $school,
        ]);
    }

    /**
     * @return string
     */
    public function actionEdit($id)
    {
        $model = \avatar\models\forms\SchoolMoneyIncome::findOne($id);
        $school = School::findOne($model->school_id);

        if (Yii::$app->request->isPost) {
            if ($model->load(Yii::$app->request->post()) && $model->validate()) {
                $s = $model->save();
                return self::jsonSuccess($s);
            } else {
                return self::jsonErrorId(102, $model->getErrors102());
            }
        }

        return $this->render([
            'model'  => $model,
            'school' => $school,
        ]);
    }

}
