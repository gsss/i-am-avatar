<?php

namespace avatar\controllers;

use common\models\avatar\UserBill;
use common\models\avatar\UserBillMerchant;
use common\models\PaySystemConfig;
use common\services\Security;
use cs\base\BaseController;
use cs\services\UploadFolderDispatcher;
use cs\services\VarDumper;
use cs\web\Exception;
use Yii;
use yii\base\UserException;
use yii\db\Connection;
use yii\db\Query;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use yii\helpers\FileHelper;
use yii\web\ErrorAction;
use yii\web\Response;

class CabinetMerchantController extends \avatar\controllers\CabinetBaseController
{
    /**
     * Редактирует настройки мерчанта
     * REQUEST:
     * - id - int - идентификатор счета
     */
    public function actionIndex()
    {
        $billing = self::getBill();
        if (!\avatar\models\forms\UserBillMerchant::find()->where(['id' => $billing->id])->exists()) {
            $merchant = new UserBillMerchant([
                'id'      => $billing->id,
                'user_id' => $billing->user_id,
                'code'    => Security::generateRandomString(40),
                'secret'  => Security::generateRandomString(40),
            ]);
            $merchant->save();
        }
        $merchant = \avatar\models\forms\UserBillMerchant::findOne($billing->id);

        if ($merchant->load(Yii::$app->request->post()) && $merchant->validate()) {
            $ret = $merchant->update();
            $billing->is_merchant = 1;
            $billing->save();
            Yii::$app->session->setFlash('form', 1);
        }

        return $this->render([
            'model'     => $merchant,
            'billing'   => $billing,
            'formCode'  => $this->renderPartial('@avatar/views/cabinet-merchant/_form.php', [
                'merchant' => $merchant,
            ]),
            'formCode2' => $this->renderPartial('@avatar/views/cabinet-merchant/_form2.php', [
                'merchant' => $merchant,
            ]),
        ]);
    }

    /**
     * Переквалификация пароля с 2 типа на 1
     *
     * @param int $id идентификатор счета
     *
     * @return string
     *
     * @throws
     */
    public function actionInit($id)
    {
        $model = new \avatar\models\forms\MerchantPassword();
        try {
            $billing = UserBill::findOne($id);
        } catch (\Exception $e) {
            throw new Exception('Не найден счет');
        }
        if ($billing->user_id != Yii::$app->user->id) {
            throw new Exception('Это не ваш счет');
        }
        if ($billing->password_type == UserBill::PASSWORD_TYPE_OPEN) {
            throw new Exception('Это не тот тип счета');
        }

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $model->action($billing);

            return $this->redirect(['index', 'id' => $id]);
        }

        return $this->render([
            'model' => $model,
        ]);
    }
   /**
     * Редактирует настройки платежной системы и показывает счета выставленные на эту платежную систему
     *
     * REQUEST:
     * + id - int - идентификатор мерчанта
     * + paysysytem_id - int - идентификатор мерчанта
     */
   public function actionPaysystem()
   {
        $billing = self::getBill();
        $merchant = \avatar\models\forms\UserBillMerchant::findOne($billing->id);
        $paysysytem_id = self::getParam('paysysytem_id');
        $config = PaySystemConfig::findOne($paysysytem_id);


        return $this->render([
           'merchant'   => $merchant,
           'config'     => $config,
        ]);
    }

    /**
     * Получает счет ID
     * Делает проверку на хозяина
     *
     * @return \common\models\avatar\UserBill
     * @throws \yii\base\Exception
     */
    private function getBill($id = null)
    {
        if (is_null($id)) {
            $id = self::getParam('id');
        }
        $bill = UserBill::findOne($id);
        if ($bill->user_id != Yii::$app->user->id) {
            throw new \yii\base\Exception('Не ваш счет');
        }

        return $bill;
    }

}
