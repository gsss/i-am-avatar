<?php

namespace avatar\controllers;

use avatar\models\forms\UserAvatar;
use avatar\models\validate\CardNumberAccept;
use avatar\modules\ETH\ServiceEtherScan;
use avatar\modules\ETH\ServiceEthPlorer;
use common\components\Card;
use common\components\providers\BTC;
use common\components\providers\ETH;
use common\models\avatar\Currency;
use common\models\avatar\UserBill;
use common\models\Config;
use common\models\school\Cloud;
use common\models\school\Kurs;
use common\models\school\Lesson;
use common\models\school\Page;
use common\models\school\PageBlock;
use common\models\school\PageBlockCategory;
use common\models\school\PageBlockContent;
use common\models\school\School;
use common\models\UserSeed;
use common\payment\BitCoinBlockTrailPayment;
use common\services\Security;
use console\controllers\MoneyRateController;
use cs\Application;
use cs\services\VarDumper;
use cs\web\Exception;
use cs\Widget\FileUpload4\ModelFields;
use Yii;
use yii\helpers\ArrayHelper;
use yii\helpers\FileHelper;
use yii\helpers\Json;
use yii\helpers\StringHelper;
use yii\helpers\Url;
use yii\httpclient\Client;
use yii\imagine\Image;
use yii\web\Response;

class CabinetSchoolPagesConstructorController extends CabinetSchoolBaseController
{

    public function actions()
    {
        $parent = parent::actions();
        $t = [
            'b11-save' => '\avatar\modules\Constructor\actions\B11Save',
            'b2-save'  => '\avatar\modules\Constructor\actions\B2Save',
        ];

        return ArrayHelper::merge($parent, $t);
    }

    public static function getSettings($school_id, $type_id)
    {
        $s = [
            'maxSize'         => 2 * 1000,
            'functionSuccess' => new \yii\web\JsExpression(<<<JS
function (response) {
    // Вызываю AJAX для записи в school_file
    ajaxJson({
        url: '/cabinet/file-upload7-save',
        data: {
            file: response.url,
            school_id: {$school_id},
            type_id: {$type_id},
            size: response.size, // Размер файла в байтах
            update: response.update
        },
        success: function (ret) {
            
        }
    });
}

JS
            ),
        ];

        $one = Cloud::findOne(['school_id' => $school_id, 'is_active' => 1]);
        if (is_null($one)) {
            $s['server'] = Yii::$app->AvatarCloud->url;
        } else {
            $s['server'] = $one->url;
        }

        return $s;
    }

    public static function getSettingsLibrary($school_id, $type_id)
    {
        $config = [
            'maxSize'         => 20 * 1000,
            'functionSuccess' => new \yii\web\JsExpression(<<<JS
function (response) {
    // Вызываю AJAX для записи в school_file
    ajaxJson({
        url: '/cabinet/file-upload7-save',
        data: {
            file: response.url,
            school_id: {$school_id},
            type_id: {$type_id},
            size: response.size, // Размер файла в байтах
            update: response.update
        },
        success: function (ret) {
            
        }
    });
}

JS
            ),
        ];

        $one = Cloud::findOne(['school_id' => $school_id, 'is_active' => 1]);
        if (is_null($one)) {
            $config['server'] = Yii::$app->AvatarCloud->url;
        } else {
            $config['server'] = $one->url;
        }
//        if (YII_ENV_DEV) {
//            $config['server'] = 'http://localhost:3081';
//            $config['controller'] = 'upload4';
//        }

        return $config;
    }

    public static function getUpdate()
    {
        return [
            [
                'function' => 'crop',
                'index'    => 'crop',
                'options'  => [
                    'width'  => '300',
                    'height' => '300',
                    'mode'   => 'MODE_THUMBNAIL_CUT',
                ],
            ],
        ];
    }

    /**
     * @param PageBlockContent $block
     *
     * @return string
     */
    public static function getBlockId($block)
    {

        return 'block' . $block->id;
    }

    /**
     * @return string
     */
    public function actionBlock00001Get()
    {
        $id = self::getParam('id');
        $block = PageBlockContent::findOne($id);


        return self::jsonSuccess(Json::decode($block->content));
    }

    /**
     * @return string
     */
    public function actionBlockGet()
    {
        $id = self::getParam('id');
        $block = PageBlockContent::findOne($id);


        return self::jsonSuccess(Json::decode($block->content));
    }

    /**
     * @return string
     */
    public function actionBlock00018Save()
    {
        $id = self::getParam('id');
        $block = PageBlockContent::findOne($id);
        $block->content = Json::encode([
            'image'   => self::getParam('image'),
            'content' => self::getParam('content'),
            'form'    => Json::decode($block->content)['form'],
        ]);
        $block->save();

        return self::jsonSuccess();
    }

    /**
     * @return string
     */
    public function actionBlock00027Save()
    {
        $id = self::getParam('id');
        $block = PageBlockContent::findOne($id);

        // получаю content
        try {
            {
                $content = self::getParam('form')['content'];
                require_once(Yii::getAlias('@common/app/services/simplehtmldom_1_5/simple_html_dom.php'));
                $content = str_get_html($content);
                foreach ($content->find('img') as $element) {
                    $src = $element->attr['src'];
                    if (StringHelper::startsWith($src, 'http') == false) {
                        $element->attr['src'] = Url::to($src, true);
                    }
                }
                $content = $content->root->outertext();
            }
        } catch (\Exception $e) {

        }

        $block->content = Json::encode([
            'content' => $content,
        ]);
        $block->save();

        return self::jsonSuccess();
    }

    /**
     * @return string
     */
    public function actionBlockSave()
    {
        $id = self::getParam('id');
        $form = self::getParam('form');
        $block = PageBlockContent::findOne($id);
        $block->content = Json::encode($form);
        $block->save();

        return self::jsonSuccess();
    }

    /**
     * @return string
     * @throws
     */
    public function actionBlockDelete()
    {
        $id = self::getParam('id');
        $block = PageBlockContent::findOne($id);
        $block->delete();

        return self::jsonSuccess();
    }

    /**
     * @return string
     * @throws
     */
    public function actionBlockUp()
    {
        $id = self::getParam('id');
        $block = PageBlockContent::findOne($id);
        $ids = PageBlockContent::find()->where(['page_id' => $block->page_id])->select(['id'])->orderBy(['sort_index' => SORT_ASC])->column();

        // если кол-во блоков равно один или 0 то ничего не делаю
        if (count($ids) < 2) self::jsonSuccess();

        // ищу порядковый номер блока в массиве
        $index = $this->findBlock($ids, $id);

        // если блок который нужно подвинуть является первый то ничего не делаю
        if ($ids[0] == $id) self::jsonSuccess();

        // новый порядок блоков
        $new = [];

        // завожу все верхние элементы не нуждающиеся в перестановке в массив $new
        $end = $index - 2;
        for ($i = 0; $i <= $end; $i++) {
            $new[] = $ids[$i];
        }

        // здесь меняю порядок
        $new[] = $ids[$index];
        $new[] = $ids[$index - 1];

        // здесь дополняю оставшиеся
        if ((count($ids) - 1) > $index) {
            for ($i = $index + 1; $i < count($ids); $i++) {
                $new[] = $ids[$i];
            }
        }

        // обновляю индексы
        $c = 0;
        for($i=0;$i<count($new);$i++) {
            $block = PageBlockContent::findOne($new[$i]);
            $block->sort_index = $c;
            $block->save();
            $c++;
        }

        return self::jsonSuccess();
    }

    /**
     * @return string
     * @throws
     */
    public function actionBlockDown()
    {
        $id = self::getParam('id');
        $block = PageBlockContent::findOne($id);
        $ids = PageBlockContent::find()->where(['page_id' => $block->page_id])->select(['id'])->orderBy(['sort_index' => SORT_ASC])->column();

        // если кол-во блоков равно один или 0 то ничего не делаю
        if (count($ids) < 2) self::jsonSuccess();

        // ищу порядковый номер блока в массиве
        $index = $this->findBlock($ids, $id);

        // если блок который нужно подвинуть является последний то ничего не делаю
        if ($ids[count($ids)-1] == $id) self::jsonSuccess();

        // новый порядок блоков
        $new = [];

        // завожу все верхние элементы не нуждающиеся в перестановке в массив $new
        $end = $index - 1;
        for ($i = 0; $i <= $end; $i++) {
            $new[] = $ids[$i];
        }

        // здесь меняю порядок
        $new[] = $ids[$index + 1];
        $new[] = $ids[$index];

        // здесь дополняю оставшиеся
        if ((count($ids) - 1) > ($index+1)) {
            for ($i = $index + 2; $i < count($ids); $i++) {
                $new[] = $ids[$i];
            }
        }

        // обновляю индексы
        $c = 0;
        for($i=0;$i<count($new);$i++) {
            $block = PageBlockContent::findOne($new[$i]);
            $block->sort_index = $c;
            $block->save();
            $c++;
        }

        return self::jsonSuccess();
    }

    /**
     * Ищет порядковый номер блока в массиве
     *
     * @param array $ids
     * @param int $id
     *
     * @return int | null Если найден то возвращаю порядковый номер блока начиная от 0, если не найден блок, то
     *             возвращаю null
     */
    private function findBlock($ids, $id)
    {
        for ($i = 0; $i < count($ids); $i++) {
            if ($ids[$i] == $id) return $i;
        }

        return null;
    }

    /**
     * - id
     *
     * @return string
     */
    public function actionBlockAdd()
    {
        $id = self::getParam('id');
        $query = PageBlock::find()
            ->where(['category_id' => $id]);
        if (!Yii::$app->user->can('permission_admin')) {
            $query->andWhere(['is_prod' => 1]);
        }

        return self::jsonSuccess(
            [
                'html' => Yii::$app->view->renderFile('@avatar/views/cabinet-school-pages/block-add.php', [
                        'items' => $query->all(),
                    ]
                ),
            ]
        );
    }

    /**
     * - id
     *
     * @return string
     */
    public function actionBlockCopy()
    {
        $id = self::getParam('id');
        $block = PageBlockContent::findOne($id);
        $clipBoard = [
            'content' => $block->content,
            'type_id' => $block->type_id,
        ];
        Yii::$app->session->set('clipboard', $clipBoard);

        return self::jsonSuccess();
    }

    /**
     * Вставляет блок
     * - id - идентификатор блока контанта выше которого добавляется блок
     *
     * @return string
     */
    public function actionBlockPaste()
    {
        $id = self::getParam('id');
        $clipBoard = Yii::$app->session->get('clipboard');

        if (is_null($clipBoard)) {
            return self::jsonErrorId(101, 'Нет данных в буфере');
        }

        $blockPoint = PageBlockContent::findOne($id);
        $page = Page::findOne($blockPoint->page_id);
        $rows = $page->getBlocks();

        // Добавляю новый блок
        $blockAdd = new PageBlockContent([
            'page_id' => $page->id,
            'type_id' => $clipBoard['type_id'],
            'content' => $clipBoard['content'],
        ]);
        $blockAdd->save();
        $blockAdd->id = $blockAdd::getDb()->lastInsertID;

        // Собираю новую последовательность
        $items = [];
        /** @var \common\models\school\PageBlockContent $blockContent */
        foreach ($rows as $blockContent) {
            if ($blockContent->id == $id) {
                $items[] = $blockAdd->id;
            }
            $items[] = $blockContent->id;
        }

        // Записываю новую последовательность
        $c = 0;
        foreach ($items as $id1) {
            $b = PageBlockContent::findOne($id1);
            $b->sort_index = $c;
            $b->save();
            $c++;
        }

        return self::jsonSuccess();
    }

    /**
     * Добавляет блок
     * - id - идентификатор типа блока который добавляется
     * - block_id - идентификатор блока контанта выше которого добавляется блок
     *
     * @return string
     */
    public function actionBlockAddExecute()
    {
        $id = self::getParam('id');
        $block_id = self::getParam('block_id');

        $blockPoint = PageBlockContent::findOne($block_id);
        $page = Page::findOne($blockPoint->page_id);
        $rows = $page->getBlocks();
        $blockOriginal = PageBlock::findOne($id);

        // Добавляю новый блок
        $blockAdd = new PageBlockContent([
            'page_id' => $page->id,
            'type_id' => $id,
            'content' => $blockOriginal->init,
        ]);
        $blockAdd->save();
        $blockAdd->id = $blockAdd::getDb()->lastInsertID;

        // Собираю новую последовательность
        $items = [];
        /** @var \common\models\school\PageBlockContent $blockContent */
        foreach ($rows as $blockContent) {
            if ($blockContent->id == $block_id) {
                $items[] = $blockAdd->id;
            }
            $items[] = $blockContent->id;
        }

        // Записываю новую последовательность
        $c = 0;
        foreach ($items as $id) {
            $b = PageBlockContent::findOne($id);
            $b->sort_index = $c;
            $b->save();
            $c++;
        }


        return self::jsonSuccess();
    }
}
