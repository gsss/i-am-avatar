<?php

namespace avatar\controllers;

use avatar\models\forms\UserAvatar;
use avatar\models\validate\CardNumberAccept;
use avatar\modules\ETH\ServiceEtherScan;
use avatar\modules\ETH\ServiceEthPlorer;
use common\components\Card;
use common\components\providers\BTC;
use common\components\providers\ETH;
use common\models\avatar\Currency;
use common\models\avatar\UserBill;
use common\models\Config;
use common\models\school\Kurs;
use common\models\school\Lesson;
use common\models\school\Master;
use common\models\school\Page;
use common\models\school\PageBlock;
use common\models\school\PageBlockContent;
use common\models\school\Potok;
use common\models\school\School;
use common\models\UserSeed;
use common\payment\BitCoinBlockTrailPayment;
use common\services\Security;
use console\controllers\MoneyRateController;
use cs\services\VarDumper;
use cs\web\Exception;
use cs\Widget\FileUpload4\ModelFields;
use Yii;
use yii\helpers\FileHelper;
use yii\helpers\Json;
use yii\helpers\StringHelper;
use yii\httpclient\Client;
use yii\imagine\Image;
use yii\web\Response;

class CabinetSchoolPagesController extends CabinetSchoolBaseController
{
    /**
     * @return string
     */
    public function actionIndex($id)
    {
        $this->isAccess($id);
        $school = School::findOne($id);

        return $this->render(['school' => $school]);
    }

    public function actionAdd($id)
    {
        $this->isAccess($id);
        $model = new Page();
        $school = School::findOne($id);
        $potok_id = self::getParam('potok_id');
        if ($potok_id) {
            $potok = Potok::findOne($potok_id);
            if (is_null($potok)) throw new Exception('Не найден potok_id');
            // проверяю что параметр potok_id входит в школу
            $this->isAccess($potok->getKurs()->school_id);
        }

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            Yii::$app->session->setFlash('form', 1);
            $model->school_id = $id;
            $model->save();
            $model->id = $model::getDb()->lastInsertID;
            $block = new PageBlockContent([
                'content' => file_get_contents(Yii::getAlias('@avatar/views/cabinet-school-pages/init.json')),
                'page_id' => $model->id,
                'type_id' => 1,
            ]);
            $block->save();
            if ($potok_id) {
                $potok = Potok::findOne($potok_id);
                $potok->page_id = $model->id;
                $potok->save();
            }
        }

        return $this->render([
            'model' => $model, 'school' => $school,
        ]);
    }

    /**
     * @param int $id страница
     *
     * @return string
     */
    public function actionConstructor($id)
    {
        $this->layout = 'blank';

        $page = Page::findOne($id);
        $this->isAccess($page->school_id);
        $school = School::findOne($page->school_id);

        return $this->render([
            'page'   => $page,
            'school' => $school,
        ]);
    }


    public function actionEdit($id)
    {
        $model = Page::findOne($id);
        $this->isAccess($model->school_id);
        $school = School::findOne($model->school_id);

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $model->save();
            Yii::$app->session->setFlash('form', 1);
        }

        return $this->render([
            'model'  => $model,
            'school' => $school,
        ]);
    }

    public function actionCopy($id)
    {
        $page = Page::findOne($id);
        $model = new \avatar\models\forms\CabinetSchoolPagesCopy(['id' => $id]);
        $this->isAccess($page->school_id);
        $school = School::findOne($page->school_id);

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $model->action();
            Yii::$app->session->setFlash('form', 1);
        }

        return $this->render([
            'model'  => $model,
            'page'   => $page,
            'school' => $school,
        ]);
    }

    public function actionMove($id)
    {
        $page = Page::findOne($id);
        $model = new \avatar\models\forms\CabinetSchoolPagesMove(['id' => $id]);
        $this->isAccess($page->school_id);
        $school = School::findOne($page->school_id);

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $model->action();
            Yii::$app->session->setFlash('form', 1);
        }

        return $this->render([
            'model'  => $model,
            'page'   => $page,
            'school' => $school,
        ]);
    }

    /**
     * @param int $id school_page.id
     *
     * @return string
     */
    public function actionView($id)
    {
        $page = Page::findOne($id);
        $this->isAccess($page->school_id);
        $school = School::findOne($page->school_id);

        return $this->render([
            'page'   => $page,
            'school' => $school,
        ]);
    }

    public function actionSeo($id)
    {
        $model = Page::findOne($id);
        $this->isAccess($model->school_id);
        $school = School::findOne($model->school_id);

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $model->save();
            Yii::$app->session->setFlash('form', 1);
        }

        return $this->render([
            'model'  => $model,
            'school' => $school,
        ]);
    }

    public function actionStat($id)
    {
        $model = Page::findOne($id);
        $this->isAccess($model->school_id);
        $school = School::findOne($model->school_id);

        return $this->render([
            'model'  => $model,
            'school' => $school,
        ]);
    }

    public function actionBody($id)
    {
        $model = Page::findOne($id);
        $this->isAccess($model->school_id);
        $school = School::findOne($model->school_id);

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $model->save();
            Yii::$app->session->setFlash('form', 1);
        }

        return $this->render([
            'model'  => $model,
            'school' => $school,
        ]);
    }

    public function actionSocialNet($id)
    {
        $model = \avatar\models\forms\school\Page::findOne($id);
        $this->isAccess($model->school_id);
        $school = School::findOne($model->school_id);

        if ($model->load(Yii::$app->request->post()) && $model->update()) {
            Yii::$app->session->setFlash('form', 1);
        }

        return $this->render([
            'model' => $model, 'school' => $school,
        ]);
    }

    public function actionHeaderFooter($id)
    {
        $this->isAccess($id);
        $model = School::findOne($id);

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $model->save();
            Yii::$app->session->setFlash('form', 1);
        }

        return $this->render([
            'model'  => $model,
            'school' => $model,
        ]);
    }

    public function actionIndexHeaderFooter($id)
    {
        $model = Page::findOne($id);
        $this->isAccess($model->school_id);
        $school = School::findOne($model->school_id);

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $model->save();
            Yii::$app->session->setFlash('form', 1);
        }

        return $this->render([
            'model'  => $model,
            'school' => $school,
        ]);
    }

    public function actionFavicon($id)
    {
        $model = \avatar\models\forms\school\Page::findOne($id);
        $this->isAccess($model->school_id);
        $school = School::findOne($model->school_id);

        if ($model->load(Yii::$app->request->post()) && $model->update()) {
            Yii::$app->session->setFlash('form', 1);
        }

        return $this->render([
            'model' => $model, 'school' => $school,
        ]);
    }


    public function actionDelete($id)
    {
        $model = Page::findOne($id);
        $this->isAccess($model->school_id);

        $rows = PageBlockContent::find()->where(['page_id' => $id])->all();
        /** @var \common\models\school\PageBlockContent $item */
        foreach ($rows as $item) {
            $item->delete();
        }
        Page::findOne($id)->delete();

        return self::jsonSuccess();
    }
}
