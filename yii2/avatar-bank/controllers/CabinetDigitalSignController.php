<?php

namespace avatar\controllers;

use avatar\models\forms\UserAvatar;
use avatar\models\validate\CardNumberAccept;
use avatar\modules\ETH\ServiceEtherScan;
use avatar\modules\ETH\ServiceEthPlorer;
use BitcoinPHP\BitcoinECDSA\BitcoinECDSA;
use common\components\Card;
use common\components\providers\BTC;
use common\components\providers\ETH;
use common\models\avatar\Currency;
use common\models\avatar\UserBill;
use common\models\Config;
use common\models\school\Kurs;
use common\models\school\Lesson;
use common\models\school\Potok;
use common\models\school\PotokUser3Link;
use common\models\school\PotokUserExt;
use common\models\school\School;
use common\models\UserSeed;
use common\payment\BitCoinBlockTrailPayment;
use common\services\Security;
use console\controllers\MoneyRateController;
use cs\services\VarDumper;
use cs\web\Exception;
use cs\Widget\FileUpload4\ModelFields;
use Yii;
use yii\db\Query;
use yii\helpers\FileHelper;
use yii\helpers\Json;
use yii\helpers\StringHelper;
use yii\httpclient\Client;
use yii\imagine\Image;
use yii\web\Response;

class CabinetDigitalSignController extends \avatar\controllers\CabinetBaseController
{

    public function actions()
    {
        return [
            'get' => [
                'class'     => '\avatar\controllers\actions\DefaultAjax',
                'model'     => '\avatar\models\validate\CabinetDigitalSignGet',
                'formName'  => 'CabinetDigitalSignGet',
            ],

            'download-pdf' => [
                'class'     => '\avatar\controllers\actions\DefaultAjax',
                'model'     => '\avatar\models\validate\CabinetDigitalSignDownloadPdf',
                'formName'  => '',
            ],
        ];
    }

    /**
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('@avatar/views/' . $this->id . '/' . $this->action->id);
    }

    public function actionDownloadJson()
    {
        $wif = Yii::$app->session->get('wif');
        $bitcoinECDSA = new BitcoinECDSA();
        $bitcoinECDSA->setPrivateKeyWithWif($wif);

        $address = $bitcoinECDSA->getAddress();
        $PrivateKey = $bitcoinECDSA->getPrivateKey();
        $getUncompressedAddress = $bitcoinECDSA->getUncompressedAddress();
        $getUncompressedPubKey = $bitcoinECDSA->getUncompressedPubKey();

        $data = [
            'version' => '1.0.0',
            'type'    => 'avatar-id-key',
            'data'    => [
                'address'             => $address,
                'PrivateKey'          => $PrivateKey,
                'UncompressedAddress' => $getUncompressedAddress,
                'UncompressedPubKey'  => $getUncompressedPubKey,
                'wif'                 => $wif,
            ],
        ];

        return Yii::$app->response->sendContentAsFile(Json::encode($data), 'AVATAR_' . $address . '.' . 'json', ['mimeType' => 'application/json']);
    }

    /**
     */
    public function actionStep2()
    {
        return $this->render('@avatar/views/' . $this->id . '/' . $this->action->id);
    }

    /**
     */
    public function actionVer2()
    {
        $model = new \avatar\models\validate\CabinetDigitalSignVer2();

        if (Yii::$app->request->isPost) {
            if ($model->load(Yii::$app->request->post()) && $model->validate()) {
                $s = $model->save();
                return self::jsonSuccess($s);
            } else {
                return self::jsonErrorId(102, $model->getErrors102());
            }
        }

        return $this->render('@avatar/views/' . $this->id . '/' . $this->action->id);
    }

    /**
     */
    public function actionIndex2()
    {
        return $this->render('@avatar/views/' . $this->id . '/' . $this->action->id);
    }

    /**
     */
    public function actionReset()
    {
        $row = \common\models\UserDigitalSign::findOne(['user_id' => Yii::$app->user->id]);
        if (!is_null($row)) {
            $row->delete();
        }

        return self::jsonSuccess();
    }
}
