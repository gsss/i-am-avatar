<?php

namespace avatar\controllers;

use avatar\models\forms\UserAvatar;
use avatar\models\UserEnter;
use common\models\school\AdminLink;
use common\models\school\School;
use common\models\SchoolKoop;
use common\models\SchoolKoopProgram;
use common\models\SchoolKoopProject;
use common\services\FormAjax\ActiveForm;
use cs\Application;
use cs\services\VarDumper;
use iAvatar777\services\FormAjax\DefaultFormAjax;
use iAvatar777\services\FormAjax\DefaultFormDelete;
use Yii;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;

class CabinetSchoolKoopProjectsController extends CabinetSchoolBaseController
{

    public function actions()
    {

        return [
            'delete' => [
                'class' => '\iAvatar777\services\FormAjax\DefaultFormDelete',
                'model' => '\avatar\models\forms\CabinetKoopProject',
            ]
        ];
    }

    /**
     * @return string
     */
    public function actionIndex($id)
    {
        $program = SchoolKoopProgram::findOne($id);
        $school = School::findOne($program->school_id);
        $koop = SchoolKoop::findOne(['school_id' => $program->school_id]);

        return $this->render([
            'school'  => $school,
            'koop'    => $koop,
            'program' => $program,
        ]);
    }

    /**
     * @param int $id school.id
     * @return string
     */
    public function actionAdd($id)
    {
        $program = SchoolKoopProgram::findOne($id);
        $school = School::findOne($program->school_id);
        $model = new \avatar\models\forms\CabinetKoopProject([
            'program_id' => $program->id,
            'author_id'  => Yii::$app->user->id,
            'school_id'  => $program->school_id,
        ]);

        if (Yii::$app->request->isPost) {
            if ($model->load(Yii::$app->request->post()) && $model->validate()) {
                $s = $model->save();
                return self::jsonSuccess($s);
            } else {
                return self::jsonErrorId(102, $model->getErrors102());
            }
        }

        return $this->render([
            'model'   => $model,
            'school'  => $school,
            'program' => $program,
        ]);
    }

    /**
     * @param int $id school.id
     * @return string
     */
    public function actionEdit($id)
    {
        $model = \avatar\models\forms\CabinetKoopProject::findOne($id);
        $program = SchoolKoopProgram::findOne($model->program_id);
        $school = School::findOne($program->school_id);

        if (Yii::$app->request->isPost) {
            if ($model->load(Yii::$app->request->post()) && $model->validate()) {
                $s = $model->save(false);
                return self::jsonSuccess($s);
            } else {
                return self::jsonErrorId(102, $model->getErrors102());
            }
        }

        return $this->render([
            'model'  => $model,
            'school' => $school,
        ]);
    }

    /**
     * @return string
     */
    public function actionSettings($id)
    {
        $project = SchoolKoopProject::findOne($id);
        switch ($project->type_id) {
            case SchoolKoopProject::TYPE_BLAGO_PROGRAM:
                return $this->redirect(['cabinet-school-blago-program/setting', 'project_id' => $project->id]);
            case SchoolKoopProject::TYPE_INVEST_PROGRAM:
                return $this->redirect(['cabinet-school-invest-program/setting', 'project_id' => $project->id]);
            default:
                throw new \Exception('Недопустимое значение');
        }
    }

    /**
     * @return string
     */
    public function actionSettingsView($id)
    {
        $project = SchoolKoopProject::findOne($id);
        switch ($project->type_id) {
            case SchoolKoopProject::TYPE_BLAGO_PROGRAM:
                return $this->redirect(['cabinet-school-blago-program/view', 'project_id' => $project->id]);
            case SchoolKoopProject::TYPE_INVEST_PROGRAM:
                return $this->redirect(['cabinet-school-invest-program/view', 'project_id' => $project->id]);
            default:
                throw new \Exception('Недопустимое значение');
        }
    }

}
