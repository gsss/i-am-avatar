<?php

namespace avatar\controllers;

use app\models\Article;
use app\models\Blog;
use app\models\Praktice;
use app\models\rai\Anketa;
use app\models\Service;
use app\models\Union;
use app\models\UnionCategory;
use app\services\HumanDesign2;
use common\services\Subscribe;
use cs\Application;
use cs\services\Str;
use cs\services\VarDumper;
use cs\web\Exception;
use Yii;
use yii\base\UserException;
use yii\filters\AccessControl;
use yii\grid\DataColumn;
use yii\helpers\StringHelper;
use yii\helpers\Url;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use app\models\NewsItem;
use app\models\Chenneling;
use cs\base\BaseController;
use yii\web\HttpException;


class AnketaController extends BaseController
{
    public function actionIndex()
    {
        return $this->render();
    }

    public function actionNew()
    {
        $model = new \common\models\Anketa();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $model->created_at = time();
            $model->save();
            Yii::$app->session->setFlash('anketa');
            Subscribe::sendArray('role_admin_command', 'Новый участник в команду', 'new_anketa', ['item' => $model]);
        }

        return $this->render([
            'model' => $model,
        ]);
    }

    public function actionNewSchool()
    {
        $model = new \common\models\AnketaSchool();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $model->created_at = time();
            $model->save();
            Yii::$app->session->setFlash('anketa');
            Subscribe::sendArray('role_admin', 'Новая школа на подключение', 'new_anketa-school', ['item' => $model]);
        }

        return $this->render('new-school', [
            'model' => $model,
        ]);
    }
}
