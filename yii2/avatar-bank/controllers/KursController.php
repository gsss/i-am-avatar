<?php

namespace avatar\controllers;

use avatar\models\forms\Contact;
use avatar\services\LogReader;
use common\components\sms\IqSms;
use common\models\avatar\UserBill;
use common\models\PaymentBitCoin;
use common\models\school\Kurs;
use common\models\UserAvatar;
use common\models\UserMaster;
use common\models\UserRegistration;
use common\payment\BitCoinBlockTrailPayment;
use cs\Application;
use cs\base\BaseController;
use cs\services\UploadFolderDispatcher;
use cs\services\VarDumper;
use cs\web\Exception;
use Yii;
use yii\base\UserException;
use yii\db\Connection;
use yii\db\Query;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use yii\helpers\FileHelper;
use yii\helpers\Url;
use yii\web\Response;

class KursController extends \avatar\base\BaseController
{
    /**
     *
     * @return string Response
     */
    public function actionIndex()
    {
        return $this->render([]);
    }

    /**
     * Выводит страницу успешной оплаты
     *
     * @return string Response
     */
    public function actionSuccess()
    {
        return $this->render('@avatar/views/kurs/success');
    }

    /**
     * @param int $id
     * @return string Response
     * @throws
     */
    public function actionItem($id)
    {
        $kurs = Kurs::findOne($id);
        if (is_null($kurs)) {
            throw new Exception('Курс не найден');
        }

        return $this->render(['item' => $kurs]);
    }

    /**
     * Оформление заявки на платный курс
     *
     * @param int $id идентификатор продажи school_kurs_sale.id
     *
     * @return string Response
     */
    public function actionOrder($id)
    {
        $model = new \avatar\models\forms\school\KursSale(['id' => $id]);

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $data = $model->action();

            /** @var \common\models\school\SchoolSaleRequest $request */
            $request = $data['request'];
            Yii::info($request->id, 'avatar\\$request->id');
            Yii::$app->session->setFlash('form', $request->id);
        }

        return $this->render('@avatar/views/kurs/order', [
            'model' => $model,
        ]);
    }

    /**
     * Оплата заявки
     *
     * @param int $id идентификатор заявки school_kurs_sale_request.id
     *
     * @return string Response
     * @throws
     */
    public function actionPay($id)
    {
        $request = \common\models\school\SchoolSaleRequest::findOne($id);
        if (is_null($request)) {
            throw new \Exception('Не найдена заявка');
        }

        return $this->render('@avatar/views/kurs/pay', [
            'request' => $request,
        ]);
    }

}
