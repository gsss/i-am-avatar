<?php

namespace avatar\controllers;

use app\common\components\Piramida;
use app\models\Article;
use app\models\Form\Shop\Order;
use app\models\Piramida\WalletSource\Custom;
use app\models\Piramida\WalletSource\Pochta;
use app\models\Piramida\WalletSource\Yandex;
use app\models\Shop;
use app\models\Shop\Product;
use app\models\Shop\Request;
use app\models\Shop\RequestMessage;
use app\models\Shop\RequestProduct;
use app\models\Union;
use app\models\User;
use app\modules\Shop\services\Basket;
use common\models\PaySystem;
use common\models\PaySystemConfig;
use common\models\school\School;
use cs\Application;
use cs\services\VarDumper;
use cs\web\Exception;
use Yii;
use yii\base\UserException;
use yii\bootstrap\Html;
use yii\db\Query;


/**
 */
class CabinetSchoolPaySystemsController extends CabinetSchoolBaseController
{
    /**
     *
     * @param int $id school.id
     */
    public function actionIndex($id)
    {
        $this->isAccess($id);
        $school = School::findOne($id);

        return $this->render(['school' => $school]);
    }


    /**
     * @param int $id school.id
     * @return string
     */
    public function actionAdd($id)
    {
        $this->isAccess($id);
        $model = new PaySystemConfig();
        $school = School::findOne($id);

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $model->school_id = $id;
            $model->save();
            Yii::$app->session->setFlash('form', $model::getDb()->lastInsertID);
        }

        return $this->render([
            'model'  => $model,
            'school' => $school,
        ]);
    }

    /**
     * @param int $id school_blog_article.id
     * @return string
     */
    public function actionEdit($id)
    {
        $model = PaySystemConfig::findOne($id);
        $this->isAccess($model->school_id);
        $school = School::findOne($model->school_id);

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $model->save();
            Yii::$app->session->setFlash('form', $model->id);
        }

        return $this->render([
            'model' => $model,
            'school' => $school,
        ]);
    }


    public function actionDelete($id)
    {
        $article = PaySystemConfig::findOne($id);
        if (is_null($article)) return self::jsonErrorId(404, 'Не найдена статья');
        $this->isAccess($article->school_id);
        $article->delete();

        return self::jsonSuccess();
    }

    /**
     * Делает редирект на настройку ПС
     *
     * @param int $id paysystems_config.id
     *
     * @return \yii\web\Response
     */
    public function actionSettings($id)
    {
        $article = PaySystemConfig::findOne($id);
        $ps = $article->_getPayment();

        return $this->redirect([$ps->code, 'id' => $id]);
    }

    /**
     * Выдает обработчики для настройки Платежных систем
     *
     * @return array
     */
    public function actions()
    {
        $rows = PaySystem::find()->all();
        $new = [];

        /** @var \common\models\PaySystem $row */
        foreach ($rows as $row) {
//            $new[$row->code] = '\avatar\controllers\actions\CabinetSchoolPaySystemsController\action' . $row->class_name;
            $new[$row->code] = 'avatar\\modules\\PaySystems\\items\\' . $row->class_name . '\action';
        }

        return $new;
    }
}
