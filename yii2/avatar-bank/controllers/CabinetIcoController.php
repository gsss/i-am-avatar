<?php

namespace avatar\controllers;

use common\models\avatar\UserBill;
use common\models\avatar\UserBillMerchant;
use common\models\BillingMain;
use common\models\investment\IcoRequest;
use common\models\investment\IcoRequestSuccess;
use common\models\PaySystemConfig;
use common\models\piramida\Billing;
use common\models\UserAvatar;
use common\services\Security;
use cs\Application;
use cs\base\BaseController;
use cs\services\UploadFolderDispatcher;
use cs\services\VarDumper;
use cs\web\Exception;
use Yii;
use yii\base\UserException;
use yii\db\Connection;
use yii\db\Query;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use yii\helpers\FileHelper;
use yii\web\ErrorAction;
use yii\web\Response;

class CabinetIcoController extends \avatar\controllers\CabinetBaseController
{
    /**
     */
    public function actionIndex()
    {
        return $this->render([]);
    }

    /**
     * Показывает заявку
     *
     * @param int $id
     *
     * @return string
     *
     * @throws \cs\web\Exception
     */
    public function actionRequest($id)
    {
        $request = IcoRequest::findOne($id);
        if (is_null($request)) {
            throw new Exception('Не найдена заявка');
        }
        if ($request->user_id != Yii::$app->user->id) {
            throw new Exception('Это не ваша заявка');
        }

        return $this->render([
            'request' => $request,
        ]);
    }

    /**
     * Указывает транзакцию на заявку
     * Высылает письмо
     *
     * REQUEST:
     * - txid - string
     * - id - int - идентификатор заявки
     *
     * @return string
     *
     * @throws \cs\web\Exception
     */
    public function actionConfirm()
    {
        $txid = self::getParam('txid');
        $id = self::getParam('id');

        if (is_null($id)) {
            return self::jsonErrorId(101, 'Не указан параметр $id');
        }
        if (is_null($txid)) {
            return self::jsonErrorId(101, 'Не указан параметр $txid');
        }
        $request = IcoRequest::findOne($id);
        if (is_null($request)) {
            return self::jsonErrorId(102, 'Не найден заказ');
        }
        if ($request->user_id != Yii::$app->user->id) {
            return self::jsonErrorId(103, 'Это не ваша заявка');
        }
        $payment = $request->getBilling()->getPayment();
        $payment->saveTransaction($txid);

        $request->addStatusToShop([
            'status'  => IcoRequest::STATUS_USER_CONFIRM,
            'message' => $txid,
        ]);

        $user = UserAvatar::findOne($request->getProject()->user_id);

        Application::mail($user->email, 'Укзана транзакция по заказу #' . $id, 'ico_request_tx', [
            'request' => $request,
            'txid'    => $txid,
            'payment' => $payment,
        ]);

        return self::jsonSuccess();
    }

    /**
     * Указывает транзакцию на заявку
     * Высылает письмо
     *
     * REQUEST:
     * - txid - string
     * - id - int - идентификатор счета
     *
     * @return string JSON
     * [
     *      'requestId' => int // идентификатор заказа
     * ]
     *
     * @throws \cs\web\Exception
     */
    public function actionConfirmBilling()
    {
        $txid = self::getParam('txid');
        $id = self::getParam('id');

        if (is_null($id)) {
            return self::jsonErrorId(101, 'Не указан параметр $id');
        }
        if (is_null($txid)) {
            return self::jsonErrorId(101, 'Не указан параметр $txid');
        }
        $request = IcoRequest::findOne(['billing_id' => $id]);
        if (is_null($request)) {
            return self::jsonErrorId(102, 'Не найден заказ');
        }
        if ($request->user_id != Yii::$app->user->id) {
            return self::jsonErrorId(103, 'Это не ваша заявка');
        }
        $payment = $request->getPaymentI();
        $payment->saveTransaction($txid);
        $request->addStatusToShop([
            'status'  => IcoRequest::STATUS_USER_CONFIRM,
            'message' => $txid,
        ]);

        $user = UserAvatar::findOne($request->getProject()->user_id);

        Application::mail($user->email, 'Укзана транзакция по заказу #' . $id, 'ico_request_tx', [
            'request' => $request,
            'txid'    => $txid,
            'payment' => $payment,
        ]);

        return self::jsonSuccess([
            'requestId' => $request->id
        ]);
    }
}