<?php

namespace avatar\controllers;

use common\models\Anketa;
use common\models\task\Task;
use Yii;
use yii\filters\AccessControl;
use yii\web\Response;


class AdminAnketaController extends AdminBaseController
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => '\yii\filters\AccessControl',
                'rules' => [
                    [
                        'allow'   => true,
                        'roles'   => ['role_admin_command'],
                    ],
                ],
            ],
        ];
    }

    public function actionIndex()
    {
        return $this->render([]);
    }

    public function actionView($id)
    {
        $model = Anketa::findOne($id);

        return $this->render([
            'model' => $model,
        ]);
    }

    /**
     * - item_id - идентификатор задачи
     * - list_id - идентификатор статуса
     *
     * @return string
     */
    public function actionSet()
    {
        $item_id = self::getParam('item_id');
        $list_id = self::getParam('list_id');
        $Task = Anketa::findOne($item_id);
        $Task->status = $list_id;
        $Task->save();

        return self::jsonSuccess();
    }



    public function actionDelete($id)
    {
        $model = Anketa::findOne($id);
        $model->delete();

        return self::jsonSuccess();
    }

    public function actionAjail()
    {
        return $this->render([]);
    }
}
