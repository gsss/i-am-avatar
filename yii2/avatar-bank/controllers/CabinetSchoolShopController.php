<?php

namespace avatar\controllers;

use app\common\components\Piramida;
use app\models\Article;
use app\models\Form\Shop\Order;
use app\models\Piramida\WalletSource\Custom;
use app\models\Piramida\WalletSource\Pochta;
use app\models\Piramida\WalletSource\Yandex;
use app\models\Shop;
use app\models\Shop\Product;
use app\models\Shop\Request;
use app\models\Shop\RequestMessage;
use app\models\Shop\RequestProduct;
use app\models\Union;
use app\models\User;
use app\modules\Shop\services\Basket;
use common\models\school\School;
use cs\Application;
use cs\services\VarDumper;
use cs\web\Exception;
use Yii;
use yii\base\UserException;
use yii\bootstrap\Html;
use yii\db\Query;


/**
 * Обслуживает действия магазина
 *
 * Class Cabinet_shop_shopController
 * @package app\controllers
 */
class CabinetSchoolShopController extends CabinetSchoolBaseController
{
    /**
     *
     * @param int $id school.id
     */
    public function actionIndex($id)
    {
        $this->isAccess($id);
        $school = School::findOne($id);

        return $this->render(['school' => $school]);
    }
    /**
     * Редактирование магазина
     * @param int $id идентификатор объединения gs_unions.id
     * @return string|\yii\web\Response
     */
    public function actionSettings($id)
    {
        $union = Union::find($id);
        $model = \app\models\Form\Shop::find(['union_id' => $id]);
        if (is_null($model)) $model = new \app\models\Form\Shop();
        if ($model->load(Yii::$app->request->post()) && $model->update2($id)) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        } else {
            return $this->render([
                'model' => $model,
                'union' => $union,
            ]);
        }
    }

    /**
     * Добавление товара
     * @param int $id идентификатор объединения gs_unions.id
     * @return string|\yii\web\Response
     * @throws \cs\web\Exception
     */
    public function actionProduct_list_add($id)
    {
        $model = new \app\models\Form\Shop\Product([
            'union_id' => $id,
        ]);

        $union = \app\models\Union::find($id);
        if (is_null($union)) {
            throw new Exception('Не найдено такое объединение');
        }
        if ($union->getField('user_id') != Yii::$app->user->id) {
            throw new Exception('Вы не можете редактировать товары чужого магазина');
        }

        if ($model->load(Yii::$app->request->post()) && $model->insert2($id)) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        } else {
            return $this->render([
                'model' => $model,
                'union' => $union,
            ]);
        }
    }

    /**
     * AJAX
     * Отправка продукиа на модерацию
     *
     * @param int $id идентификатор товара
     *
     * @return string json
     */
    public function actionProduct_list_send_moderation($id)
    {
        $product = Product::find($id);
        $union = Union::find($product->getField('union_id'));
        if ($union->getField('user_id') != Yii::$app->user->id) {
            return self::jsonErrorId(103, 'Товар принадлежит не вашему объединению');
        }
        $shop = Shop::find(['union_id' => $product->getField('union_id')]);
        // нужна модерация
        $product->update(['moderation_status' => Product::MODERATION_STATUS_SEND]);
        foreach(User::getQueryByRole(User::USER_ROLE_MODERATOR)
                    ->select(['email'])
                    ->column() as $moderatorMail) {
            Application::mail($moderatorMail, 'Добавлен товар', 'shop/moderation_product_add', [
                'shop'    => $shop,
                'product' => $product,
            ]);
        }

        return self::jsonSuccess();
    }

    /**
     * редактирование товара
     *
     * @param int $id идентификатор товара gs_unions_shop_product.id
     *
     * @return string|\yii\web\Response
     *
     * @throws \cs\web\Exception
     */
    public function actionProduct_list_edit($id)
    {
        $model = \app\models\Form\Shop\Product::find($id);
        $union = \app\models\Union::find($model->union_id);
        if (is_null($union)) {
            throw new Exception('Не найдено такое объединение');
        }
        if ($union->getField('user_id') != Yii::$app->user->id) {
            throw new Exception('Вы не можете редактировать товары чужого магазина');
        }

        if ($model->load(Yii::$app->request->post()) && $model->update($id)) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        } else {
            return $this->render([
                'model' => $model,
                'union' => $union,
            ]);
        }
    }

    /**
     * AJAX
     * удаление товара
     *
     * @param int $id идентификатор товара gs_unions_shop_product.id
     *
     * @return string|\yii\web\Response
     *
     * @throws \cs\web\Exception
     */
    public function actionProduct_list_delete($id)
    {
        $model = \app\models\Form\Shop\Product::find($id);
        $union = \app\models\Union::find($model->union_id);
        if (is_null($union)) {
            return self::jsonErrorId(101, 'Не найдено такое объединение');
        }
        if ($union->getField('user_id') != Yii::$app->user->id) {
            return self::jsonErrorId(102, 'Вы не можете удалять товары чужого магазина');
        }
        if (RequestProduct::query(['product_id' => $id])->exists()) {
            return self::jsonErrorId(103, 'Этот товар уже закреплен за одним из заказов');
        }
        $model->delete();

        return self::jsonSuccess();
    }

    /**
     * @param int $id идентификатор объединения
     * @return string
     * @throws \cs\web\Exception
     */
    public function actionProduct_list($id)
    {
        $union = \app\models\Union::find($id);
        if (is_null($union)) {
            throw new Exception('Не найдено такое объединение');
        }
        if ($union->getField('user_id') != Yii::$app->user->id) {
            throw new Exception('Вы не можете редактировать товары чужого магазина');
        }

        return $this->render([
            'union_id' => $id,
            'union'    => $union,
        ]);
    }

    /**
     * Выводит список доставки
     *
     * @param int $id идентификатор объединения
     * @return string
     *
     * @throws \cs\web\Exception
     */
    public function actionDostavka_list($id)
    {
        $union = \app\models\Union::find($id);
        if (is_null($union)) {
            throw new Exception('Не найдено такое объединение');
        }
        if ($union->getField('user_id') != Yii::$app->user->id) {
            throw new Exception('Вы не можете редактировать товары чужого магазина');
        }

        return $this->render([
            'union_id' => $id,
            'union'    => $union,
        ]);
    }

    /**
     * AJAX
     * Удаляет заказ
     *
     * @param int $id идентификатор заказа
     * @return string
     */
    public function actionRequest_list_delete($id)
    {
        $request = Request::find($id);
        if (is_null($request)) {
            return self::jsonErrorId(101, 'Не найден заказ');
        }
        if ($request->getUnion()->getField('user_id') != Yii::$app->user->id) {
            return self::jsonErrorId(102, 'Вы не можете удаялять заказы чужого магазина');
        }
        (new Query())->createCommand()->delete(RequestMessage::TABLE, ['request_id' => $request->getId()])->execute();
        (new Query())->createCommand()->delete(RequestProduct::TABLE, ['request_id' => $request->getId()])->execute();
        $request->delete();

        return self::jsonSuccess();
    }

    /**
     * AJAX
     * Скрывает заказ
     *
     * @param int $id идентификатор заказа gs_users_shop_requests.id
     *
     * @return string|\yii\web\Response
     *
     * @throws \cs\web\Exception
     */
    public function actionRequest_list_hide($id)
    {
        $request = Request::find($id);
        if (is_null($request)) {
            return self::jsonErrorId(101, 'Не найден заказ');
        }
        if ($request->getUnion()->getField('user_id') != Yii::$app->user->id) {
            return self::jsonErrorId(102, 'Вы не можете скрывать заказы чужого магазина');
        }
        $request->update(['is_hide' => 1]);

        return self::jsonSuccess();
    }



    /**
     * Добавление доставки
     * @param int $id идентификатор объединения gs_unions.id
     * @return string|\yii\web\Response
     * @throws \cs\web\Exception
     */
    public function actionDostavka_list_add($id)
    {
        $model = new \app\models\Form\Shop\DostavkaItem([
            'union_id' => $id,
        ]);

        $union = \app\models\Union::find($id);
        if (is_null($union)) {
            throw new Exception('Не найдено такое объединение');
        }
        if ($union->getField('user_id') != Yii::$app->user->id) {
            throw new Exception('Вы не можете редактировать товары чужого магазина');
        }

        if ($model->load(Yii::$app->request->post()) && $model->insert2($id)) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        } else {
            return $this->render([
                'model' => $model,
                'union' => $union,
            ]);
        }
    }

    /**
     * Получение адреса
     *
     * @param int $id идентификатор объединения gs_unions.id
     * @return string|\yii\web\Response
     * @throws \cs\web\Exception
     */
    public function actionRequestListItemAddress($id)
    {
        $request = Request::find($id);
        if (is_null($request)) {
            throw new Exception('Нет такого заказа');
        }
        $this->layout ='blank';

        return $this->render('request-list-item-address', [
            'request' => $request,
        ]);
    }

    /**
     * Ставит статус заказ подготовлен
     *
     * @param int $id идентификатор заказа
     * @return string|\yii\web\Response
     * @throws \cs\web\Exception
     */
    public function actionRequestListItemNalozhPrepared($id)
    {
        $request = Request::find($id);
        if (is_null($request)) {
            throw new Exception('Нет такого заказа');
        }
        $request->addStatusToClient(Request::STATUS_DOSTAVKA_NALOZH_PREPARE_DONE);

        return self::jsonSuccess();
    }

    /**
     * Ставит статус заказ отправлен
     * REQUEST:
     * - number - string - идентификатор отслеживания
     * - text - string - сообщение клиенту
     *
     * @param int $id идентификатор заказа
     * @return string|\yii\web\Response
     * @throws \cs\web\Exception
     */
    public function actionRequestListItemNalozhSended($id)
    {
        $request = Request::find($id);
        if (is_null($request)) {
            throw new Exception('Нет такого заказа');
        }
        $exist = RequestMessage::query(['request_id' => $request->getId(), 'status' => Request::STATUS_DOSTAVKA_NALOZH_PREPARE_DONE])->exists();
        if (!$exist) {
            $request->addStatusToClient(Request::STATUS_DOSTAVKA_NALOZH_PREPARE_DONE);
        }
        $number = self::getParam('number');
        $message = self::getParam('text');
        if (is_null($number)) {
            return self::jsonErrorId(102, 'Нет обязательного параметра number');
        }
        if (is_null($number)) {
            return self::jsonErrorId(102, 'Нет обязательного параметра text');
        }
        $messageArray = [
            'Идентификатор отслеживания: ' . $number,
            'Отслеживать вы можете на сайте: ' . Html::a('http://www.pochta.ru/Tracking#' . $number, 'http://www.pochta.ru/Tracking#'.$number, ['target' => '_blank']),
        ];
        if ($message) {
            $messageArray[] = $message;
        }
        $request->addStatusToClient([
            'status'  => Request::STATUS_DOSTAVKA_NALOZH_SEND_TO_USER,
            'message' => join("\n", $messageArray),
        ]);
        $request->getUnion()->getShop()->mail($request->getUser()->getEmail(), 'Заказ отправлен', 'shop/request_to_client_send', [
            'request' => $request,
            'text'    => join("\n", $messageArray),
        ]);

        return self::jsonSuccess();
    }

    /**
     * AJAX
     * Ставит статус заказ оплачен
     * REQUEST:
     * - text - string - информация о транзакции
     *
     * @param int $id идентификатор заказа
     * @return string|\yii\web\Response
     * @throws \cs\web\Exception
     */
    public function actionRequestListItemNalozhPaid($id)
    {
        $request = Request::find($id);
        if (is_null($request)) {
            return self::jsonErrorId(101, 'Нет такого заказа');
        }
        $message = self::getParam('text');
        if (is_null($message)) {
            return self::jsonErrorId(102, 'Нет обязательного параметра text');
        }
        $request->addStatusToClient([
            'status'  => Request::STATUS_DOSTAVKA_NALOZH_PIAD,
            'message' => $message,
        ]);
        $request->paid(new Pochta(['transaction' => ['comment' => $message]]));

        return self::jsonSuccess();
    }

    /**
     * Ставит статус заказ отменен
     *
     * @param int $id идентификатор заказа
     * @return string|\yii\web\Response
     * @throws \cs\web\Exception
     */
    public function actionRequestListItemNalozhCanceled($id)
    {
        $request = Request::find($id);
        if (is_null($request)) {
            throw new Exception('Нет такого заказа');
        }
        $request->addStatusToClient(Request::STATUS_DOSTAVKA_NALOZH_RETURN);

        return self::jsonSuccess();
    }

    /**
     * редактирование товара
     *
     * @param int $id идентификатор товара gs_unions_shop_product.id
     *
     * @return string|\yii\web\Response
     *
     * @throws \cs\web\Exception
     */
    public function actionDostavka_list_edit($id)
    {
        $model = \app\models\Form\Shop\DostavkaItem::find($id);
        $union = \app\models\Union::find($model->union_id);
        if (is_null($union)) {
            throw new Exception('Не найдено такое объединение');
        }
        if ($union->getField('user_id') != Yii::$app->user->id) {
            throw new Exception('Вы не можете редактировать товары чужого магазина');
        }

        if ($model->load(Yii::$app->request->post()) && $model->update($id)) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        } else {
            return $this->render([
                'model' => $model,
                'union' => $union,
            ]);
        }
    }

    /**
     * @param int $id идентификатор объединения
     * @return string
     * @throws \cs\web\Exception
     */
    public function actionRequest_list($id)
    {
        $union = \app\models\Union::find($id);
        if (is_null($union)) {
            throw new Exception('Не найдено такое объединение');
        }
        if ($union->getField('user_id') != Yii::$app->user->id) {
            throw new Exception('Вы не можете редактировать товары чужого магазина');
        }

        return $this->render([
            'union_id' => $id,
            'union'    => $union,
        ]);
    }

    /**
     * Показывает все заказы, в том числе исполненные и скрытые
     * @param int $id идентификатор объединения
     * @return string
     * @throws \cs\web\Exception
     */
    public function actionRequest_list_all($id)
    {
        $union = \app\models\Union::find($id);
        if (is_null($union)) {
            throw new Exception('Не найдено такое объединение');
        }
        if ($union->getField('user_id') != Yii::$app->user->id) {
            throw new Exception('Вы не можете редактировать товары чужого магазина');
        }

        return $this->render([
            'union_id' => $id,
            'union'    => $union,
        ]);
    }

    public function actionRequest_list_item($id)
    {
        return $this->render([
            'request' => Request::find($id),
        ]);
    }

    /**
     * AJAX
     * Отправляет сообщение к заказу для клиента
     *
     * REQUEST:
     * - text - string - текст сообщения
     *
     * @param int $id идентификатор заказа gs_users_shop_requests.id
     *
     * @return \yii\web\Response json
     */
    public function actionRequest_list_item_message($id)
    {
        $text = self::getParam('text');
        $request = Request::find($id);
        if (is_null($request)) {
            return self::jsonErrorId(102, 'Не найден заказ');
        }
        $union = $request->getUnion();
        if ($union->getField('user_id') != Yii::$app->user->id) {
            return self::jsonErrorId(201, 'Этот заказ принадлежит объединению которое принадлежит не вам');
        }
        $request->addMessageToClient($text);
        $shop = $union->getShop();

        // отправляю письмо
        $shop->mail($request->getUser()->getEmail(), 'Заказ #'.$request->getId().'. Новое сообщение', 'shop/request_to_client_message', [
            'request' => $request,
            'text'    => $text,
        ]);

        return self::jsonSuccess();
    }

    /**
     * AJAX
     * Отправляет сообщение для клиента: Оплата подтверждена
     *
     * REQUEST:
     * - text - string - текст сообщения
     *
     * @param int $id идентификатор заказа gs_users_shop_requests.id
     *
     * @return \yii\web\Response json
     */
    public function actionRequest_list_item_answer_pay($id)
    {
        return $this->sendStatus($id, Request::STATUS_PAID_SHOP, function (Request $request, $text) {
            $request->beforeSuccess();
            $request->paid(new Yandex(['transaction' => [
                'withdraw_amount' => 1000,
                'amount'          => 1000,
            ]]));

            // отправляю письмо
            $request->getUnion()->getShop()->mail(
                $request->getUser()->getEmail(),
                'Заказ #' . $request->getId() . '. Оплата подтверждена',
                'shop/request_to_client_answer_pay',
                [
                    'request' => $request,
                    'text'    => $text,
                ]
            );
        });
    }

    /**
     * AJAX
     * Оплачивает заказ со счетов клиента. Только для админов
     * Добавляет транзакции для заказа. Оплачено с кошелька
     * Делает перевод денег приоритетно с бонусного кошелька
     * Отпраляет писбмо пользователю
     *
     * @param int $id идентификатор заказа gs_users_shop_requests.id
     *
     * @return \yii\web\Response json
     */
    public function actionRequestListItemPayClient($id)
    {
        if (!Yii::$app->user->identity->isAdmin()) {
            return self::jsonErrorId(103, 'Эту функцию могут использовать только админ');
        }
        $request = Request::find($id);
        if (is_null($request)) {
            return self::jsonErrorId(102, 'Не найден заказ');
        }
        if ($request->getUnion()->getField('user_id') != Yii::$app->user->id) {
            return self::jsonErrorId(201, 'Этот заказ принадлежит объединению которое принадлежит не вам');
        }
        // оплата
        {
            /** @var \app\common\components\Piramida $piramida */
            $piramida = Yii::$app->piramida;
            $walletP = $piramida->getWallet();
            $user = $request->getUser();
            $walletA = $user->getWallet('A');
            $walletB = $user->getWallet('B');
            $price = $request->get('price');
            if ($walletA->value + $walletB->value < $price) {
                return self::jsonErrorId(101, 'У пользователя недостаточно денег на счету');
            }
            if ($walletB->value > 0) {
                if ($walletB->value > $price) {
                    $t = $walletB->move($walletP, $price, 'Оплата заказа '.$request->getId().' с кошелька B');
                    Shop\RequestTransaction::add([
                        'request_id'     => $request->getId(),
                        'transaction_id' => $t->id,
                        'type'           => Shop\RequestTransaction::TYPE_PAID_WALLET,
                        'sum'            => $price,
                    ]);
                } else {
                    $b = $walletB->value;
                    $t = $walletB->move($walletP, $walletB->value, 'Оплата заказа '.$request->getId().' с кошелька B');
                    Shop\RequestTransaction::add([
                        'request_id'     => $request->getId(),
                        'transaction_id' => $t->id,
                        'type'           => Shop\RequestTransaction::TYPE_PAID_WALLET,
                        'sum'            => $b,
                    ]);
                    $a = $price - $b;
                    $t = $walletA->move($walletP, $a, 'Оплата заказа '.$request->getId().' с кошелька A');
                    Shop\RequestTransaction::add([
                        'request_id'     => $request->getId(),
                        'transaction_id' => $t->id,
                        'type'           => Shop\RequestTransaction::TYPE_PAID_WALLET,
                        'sum'            => $a,
                    ]);
                }
            } else {
                $t = $walletA->move($walletP, $price, 'Оплата заказа '.$request->getId().' с кошелька A');
                Shop\RequestTransaction::add([
                    'request_id'     => $request->getId(),
                    'transaction_id' => $t->id,
                    'type'           => Shop\RequestTransaction::TYPE_PAID_WALLET,
                    'sum'            => $price,
                ]);
            }
        }

        $request->success();
        // отправляю письмо клиенту
        $request->getUnion()->getShop()->mail(
            $request->getUser()->getEmail(),
            'Заказ #' . $request->getId() . '. Оплата подтверждена',
            'shop/request_to_client_answer_pay',
            [
                'request' => $request,
                'text'    => 'Оплата была произведена с кошельков',
            ]
        );

        return self::jsonSuccess();
    }

    /**
     * AJAX
     * Устанавливает статус для заказа: Клиенту вручен заказ
     *
     * REQUEST:
     * - text - string - текст сообщения
     *
     * @param int $id идентификатор заказа gs_users_shop_requests.id
     *
     * @return \yii\web\Response json
     */
    public function actionRequest_list_item_samovivoz_done($id)
    {
        return $this->sendStatus($id, Request::STATUS_DOSTAVKA_SAMOVIVOZ_FINISH_SHOP);
    }

    /**
     * AJAX
     * Отправляет сообщение для клиента: Заказ выполнен
     *
     * REQUEST:
     * - text - string - текст сообщения
     *
     * @param int $id идентификатор заказа gs_users_shop_requests.id
     *
     * @return \yii\web\Response json
     */
    public function actionRequest_list_item_send($id)
    {
        $request = Request::find($id);
        if ($request->getStatus() == Request::STATUS_DOSTAVKA_ADDRESS_PREPARE) {
            $request->addStatusToClient(Request::STATUS_DOSTAVKA_ADDRESS_PREPARE_DONE);
        }

        return $this->sendStatus($id, Request::STATUS_DOSTAVKA_ADDRESS_SEND_TO_USER, function (Request $request, $text) {
            // отправляю письмо
            if ($request->getUser()->getEmail()) {
                Shop::mail($request->getUser()->getEmail(), 'Заказ #' . $request->getId().'. Товар отправлен клиенту', 'shop/request_to_client_send', [
                    'request' => $request,
                    'text'    => $text,
                ]);
            }
        });
    }

    /**
     * AJAX
     * Отправляет сообщение для клиента: Заказ выполнен
     *
     * REQUEST:
     * - text - string - текст сообщения
     *
     * @param int $id идентификатор заказа gs_users_shop_requests.id
     *
     * @return \yii\web\Response json
     */
    public function actionRequest_list_item_prepared($id)
    {
        return $this->sendStatus($id, Request::STATUS_DOSTAVKA_ADDRESS_PREPARE_DONE, function (Request $request, $text) {
            // отправляю письмо
            if ($request->getUser()->getEmail()) {
                Shop::mail($request->getUser()->getEmail(), 'Заказ #' . $request->getId() . '. Товар подготовлен к отправке', 'shop/request_to_client_prepared', [
                    'request' => $request,
                    'text'    => $text,
                ]);
            }
        });
    }

    /**
     * Заготовка для отправки статуса с сообщением
     *
     * REQUEST:
     * - text - string - текст сообщения
     *
     * @param int $id идентификатор заказа gs_users_shop_requests.id
     * @param int $status статус
     *
     * @return \yii\web\Response json
     */
    private function sendStatus($id, $status, \Closure $callback = null)
    {
        $text = self::getParam('text');
        $request = Request::find($id);
        if (is_null($request)) {
            return self::jsonErrorId(102, 'Не найден заказ');
        }
        if ($request->getUnion()->getField('user_id') != Yii::$app->user->id) {
            return self::jsonErrorId(201, 'Этот заказ принадлежит объединению которое принадлежит не вам');
        }
        $request->addStatusToClient([
            'message' => $text,
            'status'  => $status,
        ]);
        if ($callback) {
            $callback($request, $text);
        }

        return self::jsonSuccess();
    }
}
