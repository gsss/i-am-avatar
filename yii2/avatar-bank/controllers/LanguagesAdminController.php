<?php

namespace avatar\controllers;

use common\models\Language;
use common\models\language\Access;
use common\models\rbac\AuthAssignment;
use common\models\User;
use common\services\Debugger;
use Yii;
use yii\base\UserException;

class LanguagesAdminController extends AdminBaseController
{

    public function actionLanguages()
    {
        return $this->render('languages');
    }

    /**
     * Конфигурация настроуе для переводов
     */
    public function actionConfig()
    {
        $model = new \backend\models\form\LanguageSettings();

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $model->save();
            Yii::$app->session->setFlash('form', 1);

            return $this->refresh();
        } else {
            return $this->render('config', [
                'model' => $model
            ]);
        }
    }

    public function actionLanguagesEdit()
    {
        $id = self::getParam('id');
        if (is_null($id)) {
            throw new UserException('Не указан обязательный параметр id');
        }
        $model = Language::findOne($id);
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->session->setFlash('form', 1);

            return Yii::$app->response->refresh();
        } else {
            return $this->render('languages-edit', [
                'model' => $model,
            ]);
        }
    }

    public function actionExport()
    {
        $languages = \common\models\Language::find()->asArray()->all();
        $rows = \common\models\language\SourceMessage::find()->all();
        $lines = [];
        $headers = [
            'id',
            'category',
            'index',
        ];
        foreach($languages as $language) {
            $value = $language['code'];
            $headers[] = $value;
        }
        $lines[] = join(';', $headers);

        foreach($rows as $sourceMessage) {
            $row = [];
            $row[] = $sourceMessage['id'];
            $row[] = $sourceMessage['category'];
            $row[] = $this->csvString($sourceMessage['message']);
            foreach($languages as $language) {
                $t = \common\models\language\Message::findOne([
                    'id'       => $sourceMessage['id'],
                    'language' => $language['code'],
                ]);
                if (is_null($t)) {
                    $value = '';
                } else {
                    $value = $this->csvString($t->translation);
                }
                $row[] = $value;
            }
            $lines[] = join(';', $row);
        }
        $content = join("\n", $lines);

        return Yii::$app->response->sendContentAsFile($content, 'export.csv');
    }
    public function actionExport2()
    {
        $languages = \common\models\Language::find()->asArray()->all();
        $rows = \common\models\language\SourceMessage::find()->all();
        $lines = [];
        $headers = [
            'id',
            'category',
            'index',
        ];
        foreach($languages as $language) {
            $value = $language['code'];
            $headers[] = $value;
        }
        $lines[] = join(';', $headers);

        foreach($rows as $sourceMessage) {
            $row = [];
            $row[] = $sourceMessage['id'];
            $row[] = $sourceMessage['category'];
            $row[] = $this->csvString2($sourceMessage['message']);
            foreach($languages as $language) {
                $t = \common\models\language\Message::findOne([
                    'id'       => $sourceMessage['id'],
                    'language' => $language['code'],
                ]);
                if (is_null($t)) {
                    $value = '';
                } else {
                    $value = $this->csvString2($t->translation);
                }
                $row[] = $value;
            }
            $lines[] = join(';', $row);
        }
        $content = join("\n", $lines);

        return Yii::$app->response->sendContentAsFile($content, 'export.csv');
    }

    /**
     * Выдает Значение для CSV файла
     *
     * @param $value
     * @return string
     */
    function csvString2($value)
    {
        $value = iconv('utf-8', 'windows-1251', $value);
        $value = str_replace('"', '""', $value);
        $value = str_replace("\n", '\\n', $value);
        $value = str_replace("\r", '\\n', $value);
        $value = str_replace("\\n\\n", '\\n', $value);

        return '"' . $value . '"';
    }

    /**
     * Выдает Значение для CSV файла
     *
     * @param $value
     * @return string
     */
    function csvString($value)
    {
        $value = iconv('utf-8', 'windows-1251', $value);
        $value = str_replace('"', '""', $value);
        $value = str_replace("\n", '\\n', $value);
        $value = str_replace("\r", '\\n', $value);
        $value = str_replace("\\n\\n", '\\n', $value);

        return '"' . $value . '"';
    }

    public function actionGroup()
    {
        $model = new \backend\models\form\LanguageAccess();

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $model->do1();
            Yii::$app->session->addFlash('form', 1);

            return $this->refresh();
        } else {
            return $this->render('group', [
                'model' => $model,
            ]);
        }
    }

    /**
     * AJAX
     * REQUEST:
     * - id - int - идентификатор пользователя
     *
     * @return string|\yii\web\Response
     */
    public function actionGroupDelete()
    {
        $id = self::getParam('id');
        AuthAssignment::deleteAll([
            'user_id'   => $id,
            'item_name' => 'languages',
        ]);
        AuthAssignment::deleteAll([
            'user_id'   => $id,
            'item_name' => 'admin-panel',
        ]);
        Access::deleteAll(['user_id' => $id]);

        return self::jsonSuccess();
    }

    /**
     * AJAX
     * REQUEST:
     * - mail - string - почта
     *
     * @return string|\yii\web\Response
     */
    public function actionGroupAdd()
    {
        $email = self::getParam('mail');
        $user = User::findOne(['username' => $email]);
        if (is_null($user)) {
            return self::jsonErrorId(101, 'Не найден пользователь');
        }
        // если пользователь уже имеет роль переводчика выдаю ошибку
        if (AuthAssignment::find()->where([
            'item_name' => 'languages',
            'user_id'   => $user->id,
        ])->exists()
        ) {
            return self::jsonErrorId(102, 'Пользователь уже имеет роль переводчика');
        }
        if (!AuthAssignment::find()->where([
            'item_name' => 'admin-panel',
            'user_id'   => $user->id,
        ])->exists()
        ) {
            (new AuthAssignment([
                'item_name'  => 'admin-panel',
                'user_id'    => $user->id,
                'created_at' => time(),
            ]))->save();
        }
        $m = new AuthAssignment([
            'item_name'  => 'languages',
            'user_id'    => $user->id,
            'created_at' => time(),
        ]);
        $ret = $m->save();
        if (!$ret) {
            Debugger::dump($m->errors);
        }

        return self::jsonSuccess([
            'id'    => $user->id,
            'name'  => $user->firstname,
            'email' => $user->email,
        ]);
    }
}
