<?php

namespace avatar\controllers;

use app\models\Article;
use app\models\SiteUpdate;
use app\services\Subscribe;
use avatar\models\forms\BlogItem;
use avatar\models\forms\Contact;
use avatar\services\LogReader;
use common\components\sms\IqSms;
use common\models\avatar\UserBill;
use common\models\NewsItem;
use common\models\PaymentBitCoin;
use common\models\UserAvatar;
use common\models\UserRegistration;
use common\payment\BitCoinBlockTrailPayment;
use cs\Application;
use cs\base\BaseController;
use cs\services\UploadFolderDispatcher;
use cs\services\VarDumper;
use cs\web\Exception;
use Yii;
use yii\base\UserException;
use yii\db\Connection;
use yii\db\Query;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use yii\helpers\FileHelper;
use yii\helpers\Url;
use yii\web\NotFoundHttpException;
use yii\web\Response;

class BlogController extends \avatar\base\BaseController
{
    /**
     */
    public function actionIndex()
    {
        $page = self::getParam('page', 1);

        return $this->render(['page' => $page]);
    }

    /**
     */
    public function actionItem($id)
    {
        $item = \common\models\blog\Article::findOne($id);

        // Если запись не найдена
        if (is_null($item)) {
            throw new NotFoundHttpException('Статья не найдена');
        }

        $item->views_counter++;
        $item->save();

        return $this->render([
            'item' => $item,
        ]);
    }
}
