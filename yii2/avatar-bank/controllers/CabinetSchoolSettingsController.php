<?php

namespace avatar\controllers;

use avatar\models\forms\UserAvatar;
use avatar\models\validate\CardNumberAccept;
use avatar\modules\ETH\ServiceEtherScan;
use avatar\modules\ETH\ServiceEthPlorer;
use common\components\Card;
use common\components\providers\BTC;
use common\components\providers\ETH;
use common\models\avatar\Currency;
use common\models\avatar\UserBill;
use common\models\blog\Article;
use common\models\Config;
use common\models\school\AdminLink;
use common\models\school\Kurs;
use common\models\school\Lesson;
use common\models\school\LessonPlace;
use common\models\school\LessonVideo;
use common\models\school\Master;
use common\models\school\School;
use common\models\school\SchoolDesign;
use common\models\school\SchoolDesignMain;
use common\models\school\Settings;
use common\models\task\Task;
use common\models\UserSeed;
use common\payment\BitCoinBlockTrailPayment;
use common\services\Security;
use common\widgets\FileUpload7\FileUpload;
use console\controllers\MoneyRateController;
use cs\Application;
use cs\services\Str;
use cs\services\VarDumper;
use cs\web\Exception;
use cs\Widget\FileUpload4\ModelFields;
use iAvatar777\services\FormAjax\ActiveRecord;
use Yii;
use yii\base\Model;
use yii\helpers\ArrayHelper;
use yii\helpers\FileHelper;
use yii\helpers\Json;
use yii\helpers\StringHelper;
use yii\httpclient\Client;
use yii\imagine\Image;
use yii\web\NotFoundHttpException;
use yii\web\Response;

class CabinetSchoolSettingsController extends CabinetSchoolBaseController
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => '\yii\filters\AccessControl',
                'rules' => [
                    [
                        'allow'         => true,
                        'matchCallback' => function ($rule, $action) {
                            $school_id = Yii::$app->request->get('id');

                            $isAdmin = AdminLink::find()->where([
                                'user_id'   => Yii::$app->user->id,
                                'school_id' => $school_id,
                            ])->exists();

                            return $isAdmin;
                        },
                    ],
                ],
            ],
        ];
    }

    public function actionOnec($id)
    {
        $class = '\avatar\models\forms\CabinetSettingsOneC';
        /** @var Model | ActiveRecord $model */
        $model = $class::findOne(['school_id' => $id]);
        if (is_null($model)) {
            $model = new $class(['school_id' => $id]);
        }
        $school = School::findOne($model->school_id);

        if (Yii::$app->request->isPost) {
            if ($model->load(Yii::$app->request->post()) && $model->validate()) {
                $s = $model->save();

                return self::jsonSuccess($s);
            } else {
                return self::jsonErrorId(102, $model->getErrors102());
            }
        }

        return $this->render(['model' => $model, 'school' => $school]);
    }

    public function actionCoin($id)
    {
        $class = '\avatar\models\forms\CabinetSettingsOneC';

        /** @var Model | ActiveRecord $model */
        $model = $class::findOne(['school_id' => $id]);
        if (is_null($model)) {
            $model = new $class(['school_id' => $id]);
        }
        $school = School::findOne($model->school_id);

        if (Yii::$app->request->isPost) {
            if ($model->load(Yii::$app->request->post()) && $model->validate()) {
                $s = $model->save();

                return self::jsonSuccess($s);
            } else {
                return self::jsonErrorId(102, $model->getErrors102());
            }
        }

        return $this->render(['model' => $model, 'school' => $school]);
    }

    public function actionSms($id)
    {
        $class = '\avatar\models\forms\CabinetSettingsSms';
        /** @var Model | ActiveRecord $model */
        $model = $class::findOne(['school_id' => $id]);
        if (is_null($model)) {
            $model = new $class(['school_id' => $id]);
        }
        $school = School::findOne($model->school_id);

        if (Yii::$app->request->isPost) {
            if ($model->load(Yii::$app->request->post()) && $model->validate()) {
                $s = $model->save();

                return self::jsonSuccess($s);
            } else {
                return self::jsonErrorId(102, $model->getErrors102());
            }
        }

        return $this->render(['model' => $model, 'school' => $school]);
    }

    /**
     * @param int $id school.id
     * @return string
     * @throws
     */
    public function actionIndex($id)
    {
        $model = \avatar\models\forms\SchoolSettings::findOne($id);
        $school = School::findOne($id);

        if ($model->load(Yii::$app->request->post()) && $model->update()) {
            Yii::$app->session->setFlash('form', 1);
        }

        return $this->render([
            'model'  => $model,
            'school' => $school,
        ]);
    }

    /**
     * @param int $id school.id
     * @return string
     * @throws
     */
    public function actionShop($id)
    {
        $model = Settings::findOne(['school_id' => $id]);
        if (is_null($model)) {
            $model = new Settings(['school_id' => $id]);
        }
        $school = School::findOne($id);

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $model->save();
            Yii::$app->session->setFlash('form', 1);
        }

        return $this->render([
            'model'  => $model,
            'school' => $school,
        ]);
    }

    /**
     * @param int $id school.id
     * @return string
     * @throws
     */
    public function actionBlog($id)
    {
        $model = Settings::findOne(['school_id' => $id]);
        if (is_null($model)) {
            $model = new Settings(['school_id' => $id]);
        }
        $school = School::findOne($id);

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $model->save();
            Yii::$app->session->setFlash('form', 1);
        }

        return $this->render([
            'model'  => $model,
            'school' => $school,
        ]);
    }

    /**
     * Форма настройки компании
     *
     * @param int $id school.id
     * @return string
     * @throws
     */
    public function actionDesignSub($id)
    {
        $model = \avatar\models\forms\SchoolDesign::findOne(['school_id' => $id]);
        if (is_null($model)) {
            $model = new \avatar\models\forms\SchoolDesign(['school_id' => $id]);
        }
        $school = School::findOne($id);

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $model->save();
            Yii::$app->session->setFlash('form', 1);
        }

        return $this->render([
            'model'  => $model,
            'school' => $school,
        ]);
    }

    /**
     * Форма настройки компании
     *
     * @param int $id school.id
     * @return string
     * @throws
     */
    public function actionDesignMain($id)
    {
        $model = SchoolDesignMain::findOne(['school_id' => $id]);
        if (is_null($model)) {
            $model = new SchoolDesignMain(['school_id' => $id]);
        }
        $school = School::findOne($id);

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $model->save();
            Yii::$app->session->setFlash('form', 1);
        }

        return $this->render([
            'model'  => $model,
            'school' => $school,
        ]);
    }

    /**
     * Форма настройки компании
     *
     * @param int $id school.id
     * @return string
     * @throws
     */
    public function actionTask($id)
    {
        $model = \avatar\models\forms\SchoolDesign::findOne(['school_id' => $id]);
        if (is_null($model)) {
            $model = new \avatar\models\forms\SchoolDesign(['school_id' => $id]);
        }
        $school = School::findOne($id);

        if (Yii::$app->request->isPost) {
            if ($model->load(Yii::$app->request->post()) && $model->validate()) {
                $s = $model->save();
                return self::jsonSuccess();
            } else {
                return self::jsonErrorId(102, $model->getErrors102());
            }
        }

        return $this->render([
            'model'  => $model,
            'school' => $school,
        ]);
    }

    /**
     * @param int $id school.id
     * @return string
     * @throws
     */
    public function actionNews($id)
    {
        $model = Settings::findOne(['school_id' => $id]);
        if (is_null($model)) {
            $model = new Settings(['school_id' => $id]);
        }
        $school = School::findOne($id);

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $model->save();
            Yii::$app->session->setFlash('form', 1);
        }

        return $this->render([
            'model'  => $model,
            'school' => $school,
        ]);
    }


    /**
     * @param int $id идентификатор школы
     *
     * @return array
     */
    public static function getUpdateShopImage($id)
    {
        $settings = Settings::findOne(['school_id' => $id]);
        if (is_null($settings)) {
            $set = 0;
        } else {
            $set = $settings->shop_image_cut_id;
        }
        $mode = '';
        if ($set == 0) $mode = 'MODE_THUMBNAIL_CUT';
        if ($set == 1) $mode = 'MODE_THUMBNAIL_FIELDS';
        if ($set == 2) $mode = 'MODE_THUMBNAIL_WHITE';

        return [
            [
                'function' => 'crop',
                'index'    => 'crop',
                'options'  => [
                    'width'  => '300',
                    'height' => '300',
                    'mode'   => $mode,
                ],
            ],
        ];
    }

    /**
     * @param int $id идентификатор школы
     *
     * @return array
     */
    public static function getUpdateBlogImage($id)
    {
        $settings = Settings::findOne(['school_id' => $id]);
        if (is_null($settings)) {
            $set = 0;
            $w = 300;
            $h = 300;
        } else {
            $set = $settings->blog_image_cut_id;
            $w = (Application::isEmpty($settings->blog_image_width))? 300 : $settings->blog_image_width;
            $h = (Application::isEmpty($settings->blog_image_height))? 300 : $settings->blog_image_height;
        }
        $mode = '';
        if ($set == 0) $mode = 'MODE_THUMBNAIL_CUT';
        if ($set == 1) $mode = 'MODE_THUMBNAIL_FIELDS';
        if ($set == 2) $mode = 'MODE_THUMBNAIL_WHITE';

        return [
            [
                'function' => 'crop',
                'index'    => 'crop',
                'options'  => [
                    'width'  => $w,
                    'height' => $h,
                    'mode'   => $mode,
                ],
            ],
        ];
    }

    /**
     * @param int $id идентификатор школы
     *
     * @return array
     */
    public static function getUpdateNewsImage($id)
    {
        $settings = Settings::findOne(['school_id' => $id]);
        if (is_null($settings)) {
            $set = 0;
            $w = 300;
            $h = 300;
        } else {
            $set = $settings->news_image_cut_id;
            $w = (Application::isEmpty($settings->news_image_width))? 300 : $settings->news_image_width;
            $h = (Application::isEmpty($settings->news_image_height))? 300 : $settings->news_image_height;
        }
        $mode = '';
        if ($set == 0) $mode = 'MODE_THUMBNAIL_CUT';
        if ($set == 1) $mode = 'MODE_THUMBNAIL_FIELDS';
        if ($set == 2) $mode = 'MODE_THUMBNAIL_WHITE';

        return [
            [
                'function' => 'crop',
                'index'    => 'crop',
                'options'  => [
                    'width'  => $w,
                    'height' => $h,
                    'mode'   => $mode,
                ],
            ],
        ];
    }
}
