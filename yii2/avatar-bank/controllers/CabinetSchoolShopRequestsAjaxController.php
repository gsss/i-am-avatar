<?php

namespace avatar\controllers;

use avatar\models\forms\UserAvatar;
use avatar\models\validate\CardNumberAccept;
use avatar\modules\ETH\ServiceEtherScan;
use avatar\modules\ETH\ServiceEthPlorer;
use common\components\Card;
use common\components\providers\BTC;
use common\components\providers\ETH;
use common\models\avatar\Currency;
use common\models\avatar\UserBill;
use common\models\Config;
use common\models\school\Event;
use common\models\school\Kurs;
use common\models\school\Lesson;
use common\models\school\Page;
use common\models\school\PageBlockContent;
use common\models\school\Potok;
use common\models\school\PotokUser3Link;
use common\models\school\PotokUserExt;
use common\models\school\School;
use common\models\school\SchoolSale;
use common\models\UserSeed;
use common\payment\BitCoinBlockTrailPayment;
use common\services\Security;
use console\controllers\MoneyRateController;
use cs\services\Url;
use cs\services\VarDumper;
use cs\web\Exception;
use cs\Widget\FileUpload4\ModelFields;
use Yii;
use yii\bootstrap\ActiveForm;
use yii\db\Query;
use yii\helpers\FileHelper;
use yii\helpers\Json;
use yii\helpers\StringHelper;
use yii\httpclient\Client;
use yii\imagine\Image;
use yii\web\Response;

class CabinetSchoolShopRequestsAjaxController extends CabinetSchoolBaseController
{
    public static $count = 10;

    public static $list = [
        'Выбор пользователя',
        'Вариант доставки',
        'Поля для доставки',
        'Платежная система',
    ];

    public function actions()
    {
        $items = [];
        $count = self::$count;

        for ($k = 1; $k <= $count; $k++) {
            $items['step' . $k] = [
                'class'     => '\avatar\controllers\actions\CabinetSchoolShopRequestsAjax\actionStep',
                'num'       => $k,
            ];
        }

        return $items;
    }

    /**
     * Публикация события
     *
     * @return Response
     */
    public function actionFinish()
    {
        $model = new \avatar\models\validate\CabinetSchoolEventsFinish();

        if (!$model->load(Yii::$app->request->get(), '')) {
            return self::jsonErrorId(400, 'Не загружены данные');
        }
        if (!$model->validate()) {
            return self::jsonErrorId(102, $model->errors);
        }

        return self::jsonSuccess($model->action());
    }

}
