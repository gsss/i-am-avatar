<?php

namespace avatar\controllers;

use avatar\models\forms\UserAvatar;
use avatar\models\validate\CardNumberAccept;
use avatar\modules\ETH\ServiceEtherScan;
use avatar\modules\ETH\ServiceEthPlorer;
use common\components\Card;
use common\components\providers\BTC;
use common\components\providers\ETH;
use common\models\avatar\Currency;
use common\models\avatar\UserBill;
use common\models\blog\Article;
use common\models\Config;
use common\models\school\Kurs;
use common\models\school\Lesson;
use common\models\school\LessonPlace;
use common\models\school\LessonVideo;
use common\models\school\School;
use common\models\SchoolBlagoProgram;
use common\models\SchoolInvestProgram;
use common\models\SchoolKoopProgram;
use common\models\SchoolKoopProject;
use common\models\UserSeed;
use common\payment\BitCoinBlockTrailPayment;
use common\services\Security;
use console\controllers\MoneyRateController;
use cs\services\Str;
use cs\services\VarDumper;
use cs\web\Exception;
use cs\Widget\FileUpload4\ModelFields;
use Yii;
use yii\helpers\FileHelper;
use yii\helpers\Json;
use yii\helpers\StringHelper;
use yii\httpclient\Client;
use yii\imagine\Image;
use yii\web\Response;

class CabinetSchoolInvestProgramController extends CabinetSchoolBaseController
{
    /**
     * @param int $id school.id
     *
     * @return string
     */
    public function actionIndex($id)
    {
        $this->isAccess($id);
        $school = School::findOne($id);

        return $this->render(['school' => $school]);
    }

    /**
     * @param int $id school_invest_program.id
     *
     * @return string
     */
    public function actionView($project_id)
    {
        $project = SchoolKoopProject::findOne($project_id);
        $this->isAccess($project->school_id);
        $school = School::findOne($project->school_id);
        $model = SchoolInvestProgram::findOne(['project_id' => $project->id]);

        return $this->render([
            'model'  => $model,
            'school' => $school,
        ]);
    }

    /**
     * @param int $id school_blog_article.id
     *
     * @return string
     */
    public function actionSetting($project_id)
    {
        $project = SchoolKoopProject::findOne($project_id);
        $program = SchoolKoopProgram::findOne($project->program_id);
        $this->isAccess($program->school_id);
        $school = School::findOne($program->school_id);

        $model = \avatar\models\forms\InvestProgram::findOne(['project_id' => $project->id]);
        if (is_null($model)) {
            $model = new \avatar\models\forms\InvestProgram([
                'project_id' => $project->id,
//                'school_id'  => $school->id,
            ]);
        }

        if (Yii::$app->request->isPost) {
            if ($model->load(Yii::$app->request->post()) && $model->validate()) {
                $s = $model->save();
                return self::jsonSuccess($s);
            } else {
                return self::jsonErrorId(102, $model->getErrors102());
            }
        }

        return $this->render([
            'model'   => $model,
            'school'  => $school,
            'program' => $program,
            'project' => $project,
        ]);
    }


}
