<?php

namespace avatar\controllers;

use yii\filters\AccessControl;

class DevelopmentAvatarBankController extends \avatar\base\BaseController
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow'   => true,
                        'roles'   => ['role_developer'],
                    ],
                ],
            ],
        ];
    }

    /**
     */
    public function actionIndex()
    {
        return $this->render();
    }

    /**
     */
    public function actionEthGeth()
    {
        return $this->render();
    }

    /**
     */
    public function actionCompanyCustomize()
    {
        return $this->render();
    }

    /**
     */
    public function actionEthContracts()
    {
        return $this->render();
    }

    /**
     */
    public function actionEthImportChain()
    {
        return $this->render();
    }

    /**
     */
    public function actionEthTokenZalog()
    {
        return $this->render();
    }

    /**
     */
    public function actionUserDataBase()
    {
        return $this->render();
    }

    /**
     */
    public function actionCurrency()
    {
        return $this->render();
    }

    /**
     */
    public function actionInstallEth()
    {
        return $this->render();
    }

    /**
     */
    public function actionEthKlaster()
    {
        return $this->render();
    }

    /**
     */
    public function actionBinance()
    {
        return $this->render();
    }

    /**
     */
    public function actionPageCabinetBillsIndex()
    {
        return $this->render();
    }

    /**
     */
    public function actionWalletImport()
    {
        return $this->render();
    }

    /**
     */
    public function actionReferals()
    {
        return $this->render();
    }

    /**
     */
    public function actionMobile()
    {
        return $this->render();
    }

    /**
     */
    public function actionMonitoring()
    {
        return $this->render();
    }

    /**
     */
    public function actionInstall()
    {
        return $this->render();
    }

    /**
     */
    public function actionAr()
    {
        return $this->render();
    }

    /**
     */
    public function actionLangauages()
    {
        return $this->render();
    }

    /**
     */
    public function actionCabinetDocuments()
    {
        return $this->render();
    }

    /**
     */
    public function actionCabinetExchange()
    {
        return $this->render();
    }

    /**
     */
    public function actionProjects()
    {
        return $this->render();
    }


    /**
     */
    public function actionIco()
    {
        return $this->render();
    }

    /**
     */
    public function actionIcoItem()
    {
        return $this->render();
    }

    /**
     */
    public function actionIcoPath1()
    {
        return $this->render();
    }

    /**
     */
    public function actionFilePermission()
    {
        return $this->render();
    }

    /**
     */
    public function actionIcoPath2()
    {
        return $this->render();
    }

    /**
     */
    public function actionIcoPath3()
    {
        return $this->render();
    }

    /**
     */
    public function actionIcoPath4()
    {
        return $this->render();
    }

    /**
     */
    public function actionPathRegistration()
    {
        return $this->render('path-registration');
    }

    /**
     */
    public function actionPath()
    {
        return $this->render();
    }

    /**
     */
    public function actionPasport()
    {
        return $this->render();
    }

    /**
     */
    public function actionKoop()
    {
        return $this->render();
    }

    /**
     */
    public function actionTokens()
    {
        return $this->render();
    }

    /**
     */
    public function actionTokensNew()
    {
        return $this->render();
    }

    /**
     */
    public function actionLog()
    {
        return $this->render();
    }

    /**
     */
    public function actionKraudFanding()
    {
        return $this->render('kraud-fanding');
    }

    /**
     */
    public function actionMap()
    {
        return $this->render();
    }

    /**
     */
    public function actionMerchant()
    {
        return $this->render();
    }

    /**
     */
    public function actionGai()
    {
        return $this->render();
    }

    /**
     */
    public function actionUserStatusList()
    {
        return $this->render('user-status-list');
    }

    /**
     */
    public function actionWalletSend()
    {
        return $this->render('wallet-send');
    }

    /**
     */
    public function actionWallet()
    {
        return $this->render('wallet');
    }

    /**
     */
    public function actionWalletEth()
    {
        return $this->render();
    }

    /**
     */
    public function actionWalletEthSend()
    {
        return $this->render();
    }

    /**
     */
    public function actionWalletEthPage()
    {
        return $this->render();
    }

    /**
     */
    public function actionWalletToken()
    {
        return $this->render();
    }

    /**
     */
    public function actionWalletTokenSend()
    {
        return $this->render();
    }

    /**
     */
    public function actionWalletPassword()
    {
        return $this->render();
    }

    /**
     */
    public function actionApi()
    {
        return $this->render();
    }

    /**
     */
    public function actionApi2()
    {
        return $this->render();
    }

    /**
     */
    public function actionSupport()
    {
        return $this->render();
    }

    /**
     */
    public function actionEth()
    {
        return $this->render('eth');

    }

    /**
     */
    public function actionCardNumber()
    {
        return $this->render('card-number');
    }

    /**
     */
    public function actionPaySystems()
    {
        return $this->render('pay-systems');
    }

    /**
     */
    public function actionTransactions()
    {
        return $this->render();
    }

    /**
     */
    public function actionAuth()
    {
        return $this->render();
    }

    /**
     */
    public function actionBills()
    {
        return $this->render();
    }

    /**
     */
    public function actionSecurity()
    {
        return $this->render();
    }

    /**
     */
    public function actionQrEnter()
    {
        return $this->render('qr-enter');
    }

    /**
     */
    public function actionSubscribe()
    {
        return $this->render();
    }
}
