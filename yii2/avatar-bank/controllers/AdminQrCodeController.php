<?php

namespace avatar\controllers;

use avatar\base\Application;
use avatar\models\Wallet;
use avatar\models\WalletETH;
use common\components\Card;
use common\models\avatar\CurrencyLink;
use common\models\avatar\QrCode;
use common\models\avatar\UserBill;
use common\models\piramida\Billing;
use common\services\Security;
use Yii;
use yii\base\Exception;
use yii\filters\AccessControl;
use yii\helpers\FileHelper;
use yii\helpers\VarDumper;
use yii\imagine\Image;

class AdminQrCodeController extends \avatar\base\BaseController
{
    public static $isStatistic = false;

    public $layout = 'cabinet';

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['role_admin', 'role_qr-code-admin'],
                    ],
                ],
            ],
        ];
    }

    /**
     * Выводит список всех кодов
     *
     */
    public function actionIndex()
    {
        return $this->render();
    }

    /**
     * AJAX
     * Генерирует один новый код
     *
     * @return string JSON array
     * [
     * 'billing' => [
     * 'id'      => $wallet->billing->id,
     * 'address' => $wallet->billing->address,
     * ],
     * 'code'    => [
     * 'id'   => $qrCode->id,
     * 'code' => $qrCode->code,
     * ]
     * ]
     */
    public function actionNew()
    {
        $wallet = Wallet::create('Мой кошелек');
        if (is_null($wallet)) return self::jsonErrorId(102, 'Не получилось создать кошелек');
        $qrCode = QrCode::add([
            'code'       => Security::generateRandomString(10),
            'is_used'    => 0,
            'billing_id' => $wallet->billing->id,
        ]);
        $wallet->billing->card_number = Card::generateNumber();
        $wallet->billing->save();

        self::jsonSuccess([
            'billing' => [
                'id'      => $wallet->billing->id,
                'address' => $wallet->billing->address,
            ],
            'code'    => [
                'id'   => $qrCode->id,
                'code' => $qrCode->code,
            ],
        ]);
    }


    public function actions()
    {

        return [
            'new-ethereum' => [
                'class' => '\avatar\controllers\actions\DefaultAjax',
                'model' => '\avatar\models\validate\AdminQrCodeNewEthereum',
            ],
        ];
    }

    /**
     * выдает на скачивание список
     *
     * @param string $ids идентификаторы card.id через запятую
     *
     * @return string файл на скачивание
     */
    public function actionDownload($ids)
    {
        $ids = explode(',', $ids);
        $rows = \common\models\Card::find()->where(['in', 'id', $ids])->all();
        $ret = [];
        $c = 0;
        foreach ($rows as $billing) {
            $ret[] = join(';', [
                '=' . '"' . $billing['secret_num'] . '"',
                '=' . '"' . $billing['number'] . '"',
                '=' . '"' . \common\models\Card::convertNumberToSpace($billing['number']) . '"',
            ]);
            $c++;
        }
        $content = join("\r\n", $ret);

        return Yii::$app->response->sendContentAsFile($content, 'data' . date('YmdHis') . '.csv');
    }

    /**
     * AJAX
     * Отсоединяет пользователя от карты
     *
     * REQUEST:
     * - id - int - bill_codes.id
     *
     * @return string файл на скачивание
     */
    public function actionDisconnect()
    {
        $id = self::getParam('id');
        $qrCode = QrCode::findOne($id);
        $qrCode->is_used = 0;
        $qrCode->save();
        $billingId = $qrCode->billing_id;
        $billing = UserBill::findOne($billingId);
        $cardId = $billing->card_id;
        UserBill::find()->createCommand()->update(UserBill::tableName(), ['card_id' => null], ['card_id' => $cardId])->execute();
        $billing->user_id = null;
        $billing->save();

        return self::jsonSuccess();
    }

    /**
     * AJAX
     * Ставит флаг на карте что она для перевыпуска
     *
     * REQUEST:
     * - id - int - bill_codes.id
     *
     * @return string файл на скачивание
     */
    public function actionMark()
    {
        $id = self::getParam('id');
        $qrCode = QrCode::findOne($id);
        $billingId = $qrCode->billing_id;
        $billing = UserBill::findOne($billingId);
        $cardId = $billing->card_id;
        $card = \common\models\Card::findOne($cardId);
        $card->is_new = 1;
        $card->save();

        return self::jsonSuccess();
    }

    /**
     * AJAX
     * выдает на скачивание список
     *
     * REQUEST:
     * - ids
     *
     * @return string файл на скачивание
     */
    public function actionDownloadArchive()
    {
        $ids = self::getParam('ids');
        $ids = explode(',', $ids);
        $rows = UserBill::find()->where(['in', 'bill_codes.id', $ids])
            ->innerJoin('bill_codes', 'bill_codes.billing_id = user_bill.id')
            ->select([
                'bill_codes.code',
                'user_bill.address',
                'user_bill.card_number',
            ])
            ->asArray()->all();
        $ret = [];
        $p = '@runtime/qr/' . time();
        FileHelper::createDirectory(Yii::getAlias($p));
        foreach ($rows as $billing) {
            $content = (new \Endroid\QrCode\QrCode())
                ->setText('bitcoin:' . $billing['address'] . '?i-am-avatar=true')
                ->setSize(200)
                ->setPadding(10)
                ->setErrorCorrection('high')
                ->setForegroundColor(['r' => 0, 'g' => 0, 'b' => 0, 'a' => 0])
                ->setBackgroundColor(['r' => 255, 'g' => 255, 'b' => 255, 'a' => 0])
                ->setLabelFontSize(16)
                ->setImageType(\Endroid\QrCode\QrCode::IMAGE_TYPE_PNG)
                ->get('png');
            $path = Yii::getAlias($p . '/' . $billing['card_number'] . '.png');
            file_put_contents($path, $content);
            $ret[] = [
                'path' => $path,
                'name' => $billing['card_number'] . '.png',
            ];
        }

        $zip = new \ZipArchive();
        $zip->open(Yii::getAlias($p . '/all.zip'), \ZipArchive::CREATE);
        foreach ($ret as $file) {
            $zip->addFile($file['path'], $file['name']);
        }
        $zip->close();
        $content = file_get_contents(Yii::getAlias($p . '/all.zip'));

        return Yii::$app->response->sendContentAsFile($content, 'all.zip');
    }


    /**
     * Показывает карты которые можно скачать
     *
     */
    public function actionCards()
    {
        return $this->render(['billing' => $this->getBill()]);
    }

    /**
     * Получает счет ID
     * Делает проверку на хозяина
     *
     * @return \common\models\avatar\UserBill
     * @throws \yii\base\Exception
     */
    private function getBill($id = null)
    {
        if (is_null($id)) {
            $id = self::getParam('id');
        }
        $bill = UserBill::findOne($id);

        return $bill;
    }

    public function actionCards2()
    {
        return $this->render();
    }

    public function actionCards3()
    {
        $model = new \avatar\models\forms\AdminQrFileDb();

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $file = $model->action();
            Yii::$app->session->setFlash('form', $file);
        }

        return $this->render([
            'model' => $model,
        ]);
    }

    public function actionCards4()
    {
        $model = new \avatar\models\forms\AdminQrFileDb();

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $file = $model->action();
            Yii::$app->session->setFlash('form', $file);
        }

        return $this->render([
            'model' => $model,
        ]);
    }

    /**
     * Скачивает карту с установленным QR кодом
     * REQUEST:
     * - billing_id - int - идентификатор счета user_bill.id
     * - card - int - идентификатор картинки
     *
     * @return string Response файл для скачивания
     */
    public function actionCardDownload()
    {
        ini_set("memory_limit", "1000M");

        $billing = self::getBill(self::getParam('billing_id'));
        $card = self::getParam('card');
        $cardObject = \avatar\controllers\CabinetBillsController::$cardList[$card];
        if (!isset($cardObject['qr'])) {
            return Yii::$app->response->sendContentAsFile(file_get_contents(Yii::getAlias($cardObject['original'])), 'card_' . $billing->address . '.' . 'png');
        }

        $file = $cardObject['original'];
        $qr = $cardObject['qr'];
        $filePath = Yii::getAlias($file);

        $devImage = file_get_contents(Yii::getAlias('@webroot/images/controller/cabinet-bills/cards/transaction.png'));

        $image = $this->watermark(
            $filePath,
            [
                [
                    'file'  => [
                        'format'  => 'png',
                        'content' => (YII_ENV_DEV) ? $devImage : (new \Endroid\QrCode\QrCode())
                            ->setText('bitcoin:' . $billing->address . '?i-am-avatar=true')
                            ->setSize($qr['size'])
                            ->setPadding($qr['padding'])
                            ->setErrorCorrection('high')
                            ->setForegroundColor(['r' => 0, 'g' => 0, 'b' => 0, 'a' => 0])
                            ->setBackgroundColor(['r' => 255, 'g' => 255, 'b' => 255, 'a' => 0])
                            ->setLabelFontSize(16)
                            ->setImageType(\Endroid\QrCode\QrCode::IMAGE_TYPE_PNG)
                            ->get('png'),
                    ],
                    'start' => $qr['start'],
                ],
            ]
        );

        return Yii::$app->response->sendContentAsFile(file_get_contents($image['path']), 'card_' . $billing->address . '.' . 'png');
    }

    /**
     * Формирует страницу по 10 шт с QR кодом
     *
     * @param string $file полный путь к файлу
     *
     * REQUEST:
     * + page - int - номер страницы, начиная от 1
     * - db - string - файл с файлом номеров
     *
     * @return string Response JSON
     * [
     *      'file' => path_full
     * ]
     */
    public function actionPage()
    {
        ini_set("memory_limit", "1500M");
        ini_set('max_execution_time', 500);

        $db = self::getParam('db');
        if (is_null($db)) {
            $db = Yii::getAlias('@avatar/data20180509234338.csv');
        }

        $content = file_get_contents($db);
        $rows = explode("\n", $content);
        $page = self::getParam('page');
        $offset = (($page - 1) * 10);
        $rows2 = [];
        foreach ($rows as $row) {
            $row = explode("\r", $row);
            $rows2[] = explode(';', $row[0]);
        }
        $rows2 = $this->clearRows($rows2);
        $pages = $this->preparePages($rows2);
        $file = $this->getPage($pages[$page - 1]);

        return self::jsonSuccess(['file' => $file]);
    }

    /**
     * Формирует страницу по 10 шт с QR кодом
     * REQUEST:
     * + page - int - номер страницы, начиная от 1
     * + file - string - файл от первого прогона actionPage
     * - db - string - файл с файлом номеров
     *
     * @return string Response JSON
     * [
     *      'file' => path_full
     * ]
     */
    public function actionPage2()
    {
        ini_set("memory_limit", "1500M");
        ini_set('max_execution_time', 500);

        $db = self::getParam('db');
        if (is_null($db)) {
            $db = Yii::getAlias('@avatar/data20180509234338.csv');
        }

        $content = file_get_contents($db);
        $rows = explode("\n", $content);
        $page = self::getParam('page');
        $file = self::getParam('file');
        $offset = (($page - 1) * 10);
        $rows2 = [];
        foreach ($rows as $row) {
            $row = explode("\r", $row);
            $rows2[] = explode(';', $row[0]);
        }
        $rows2 = $this->clearRows($rows2);
        $pages = $this->preparePages($rows2);

        $file = $this->getPage($pages[$page - 1], $file);
        $tempFilePath = \Yii::getAlias('@runtime/watermarkDone');
        FileHelper::createDirectory($tempFilePath);
        $fileDest = $tempFilePath . DIRECTORY_SEPARATOR . time() . '_' . Security::generateRandomString(10) . '.' . 'png';
        copy($file, $fileDest);

        return self::jsonSuccess(['file' => $fileDest]);
    }

    /**
     * Подготавливает номера по страницам по алгоритму
     * Алгоритм:
     * Например я печатаю 100 карт
     * делю пополам
     * На карте будут отображаться номера
     * 1,2,3,4,5,51,52,53,54,55
     * 6,7,8,9,10,56,57,58,59,60
     *
     * @param array $rows стоки от 1 до 100 (кратно 10)
     *
     * @return array
     * [
     *      [ // страница
     *          [] // row 1
     *          [] // row 2
     *          [] // row 3
     *          [] // row 4
     *          [] // row 5
     *          [] // row 51
     *          [] // row 52
     *          [] // row 53
     *          [] // row 54
     *          [] // row 55
     *      ], ...
     * ]
     */
    private function preparePages($rows)
    {
        // вычисляю общее кол-во страниц
        $pagesCount = count($rows) / 10;
        $pagesCount = (int)$pagesCount;
        $pages = [];

        // счетчик указывающий на порядковый номер из $rows для верхнего левого угла
        $i = 1;

        // счетчик указывающий на порядковый номер из $rows для верхней карты из правого столбца
        $j = (count($rows) / 2) + 1;

        // $q - изменяющийся номер страницы
        for ($q = 0; $q < $pagesCount; $q++) {
            $pageRows = [];
            for ($c = 0; $c < 5; $c++) {
                $pageRows[] = $rows[($i - 1) + $c];
            }
            for ($c = 0; $c < 5; $c++) {
                $pageRows[] = $rows[($j - 1) + $c];
            }
            $pages[] = $pageRows;
            $i += 5;
            $j += 5;
        }

        return $pages;
    }

    /**
     * Очищает строки CSV от зкнаков = "
     * @param array $rows
     * [
     *      [
     *         "000"
     *         12
     *         "sd""sds"
     *         ="000sd"
     *      ]
     * ]
     * @return array
     */
    private function clearRows($rows)
    {
        $rows2 = [];
        foreach ($rows as $row) {
            $newRow = [];
            foreach ($row as $item) {
                $newRow[] = $this->clearItem($item);
            }
            $rows2[] = $newRow;
        }

        return $rows2;
    }

    /**
     * Делает очищение от символов = "
     * и если обернуто в кавычки то заменяет двойные на одинарные
     *
     * @param string $item
     *
     * @return string
     */
    private function clearItem($item)
    {
       $firstChar = substr($item, 0, 1);
       if ($firstChar == '=') {
           $item = substr($item, 1);
           $firstChar = substr($item, 0, 1);
       }
       if ($firstChar == '"') {
           $len = strlen($item);
           $item = substr($item, 1, $len - 2);
           $item = str_replace('""', '"', $item);
       }

       return $item;
    }

    /**
     * @param array  $rows - список строк которые нужно разместить на странице
     *                     1 - правый верхний угол потом вниз и потом второй столбик сверху вниз
     * [
     * [
     *  '0' => code
     *  '3' => address
     * ]
     * ]
     * @param string $file - файл с готовым первым рядом, если вызов осуществляется второй раз
     *
     * @return string путь к файлу
     */
    private function getPage($rows, $file = '')
    {
        $a = 806;
        $b = 195;
        $w = 2126;
        $h = 1346;
        $h2 = 1;
        $c = 272;
        $d = 135;
        $j = 187;
        $k = 34;
        $m = 46 - 18;
        $r = 518;
        $q = $r - ($k * 2);
        $n = 191;
        $p = 460 + 40 - 8;
        $x = $c + (($file == '') ? 0 : ($w + $j));
        $y = $d;
        $i2 = 1;

        if ($file != '') {
            $filePath = $file;
            $i2 = 2;
        } else {
            $filePath = \Yii::getAlias('@avatar/1511215584_3psRmWmLsZ1.png');
        }

        for ($i = 1; $i <= 5; $i++) {
            $address = $rows[$i - 1 + (($i2 - 1) * 5)][2];
            // накладываю QR код
            $image = $this->watermark(
                $filePath,
                [
                    [
                        'file'  => [
                            'format'  => 'png',
                            'content' => (new \Endroid\QrCode\QrCode())
                                ->setText($address)
                                ->setSize($q)
                                ->setPadding(0)
                                ->setErrorCorrection('high')
                                ->setForegroundColor(['r' => 0, 'g' => 0, 'b' => 0, 'a' => 0])
                                ->setBackgroundColor(['r' => 255, 'g' => 255, 'b' => 255, 'a' => 0])
                                ->setLabelFontSize(16)
                                ->setImageType(\Endroid\QrCode\QrCode::IMAGE_TYPE_PNG)
                                ->get('png'),
                        ],
                        'start' => [$x + $a + $k, $y + $b + $m],
                    ],
                ]);
            $filePath = $image['path'];

            // накладываю text
            $text = $rows[$i - 1 + (($i2 - 1) * 5)][0];
            $image = $this->text(
                $filePath,
                [
                    [
                        'text'        => $text,
                        'start'       => [$x + $a + $n, $y + $b + $p],
                        'fontFile'    => Yii::getAlias('@avatar/../public_html/images/controller/cabinet-bills/cards/Verdana.ttf'),
                        'fontOptions' => ['color' => '000', 'size' => 54],
                    ],
                ]);
            $filePath = $image['path'];

            $y += $h + $h2;
        }

        return $filePath;
    }


    /**
     * @param string $file полный путь к файлу
     * @param array  $watermark
     *                     [[
     *                     'file' => string - полный путь | array [
     *                     'format'  => string - расширение возможно jpg png
     *                     'content' => string - содержимое
     *                     ]
     *                     'start' => array [x, y] - точка от верхнего левого угла оригинальной картинки
     *                     ],...]
     *
     * @return array
     * [
     *      'img'  => \Imagine\Image\ImageInterface
     *      'path' => string - полный путь к файлу png
     * ]
     *
     * @throws \yii\base\Exception
     */
    private function watermark($file, $watermark)
    {
        if (count($watermark) == 0) {
            throw new Exception('Не задан массив $watermark');
        }
        $tempFilePath = \Yii::getAlias('@runtime/watermark');
        FileHelper::createDirectory($tempFilePath);
        $background = $file;
        $image = null;
        foreach ($watermark as $watermarkConfig) {
            if (is_array($watermarkConfig['file'])) {
                $temp = $tempFilePath . DIRECTORY_SEPARATOR . time() . '_' . Security::generateRandomString(10) . '.' . $watermarkConfig['file']['format'];
                file_put_contents($temp, $watermarkConfig['file']['content']);
            } else {
                $temp = $watermarkConfig['file'];
            }
            $image = Image::watermark($background, $temp, $watermarkConfig['start']);
            $background = $tempFilePath . DIRECTORY_SEPARATOR . time() . '_' . Security::generateRandomString(10) . '.' . 'png';
            file_put_contents($background, $image->get('png'));
        }

        return [
            'img'  => $image,
            'path' => $background,
        ];
    }

    /**
     * @param string $file полный путь к файлу
     * @param array  $watermark
     *                     [[
     *                     'text'     => string
     *                     'start'    => array [x, y] - точка от верхнего левого угла оригинальной картинки
     *                     'fontOptions' => string
     *                     'fontFile' => string
     *                     ],...]
     *
     * @return array
     * [
     *      'img'  => \Imagine\Image\ImageInterface
     *      'path' => string - полный путь к файлу png
     * ]
     *
     * @throws \yii\base\Exception
     */
    private function text($file, $watermark)
    {
        if (count($watermark) == 0) {
            throw new Exception('Не задан массив $watermark');
        }
        $tempFilePath = Yii::getAlias('@runtime/watermark');
        FileHelper::createDirectory($tempFilePath);
        $background = $file;
        $image = null;
        foreach ($watermark as $watermarkConfig) {
            if (!isset($watermarkConfig['fontOptions'])) {
                $watermarkConfig['fontOptions'] = [];
            }
            if (!isset($watermarkConfig['fontOptions'])) {
                $watermarkConfig['start'] = [0, 0];
            }

            $image = Image::text($background, $watermarkConfig['text'], $watermarkConfig['fontFile'], $watermarkConfig['start'], $watermarkConfig['fontOptions']);
            $background = $tempFilePath . DIRECTORY_SEPARATOR . time() . '_' . Security::generateRandomString(10) . '.' . 'png';
            file_put_contents($background, $image->get('png'));
        }

        return [
            'img'  => $image,
            'path' => $background,
        ];
    }


    /**
     * Выдает QR код
     *
     * REQUEST:
     * - id - int идентификатор счета
     *
     * @return string Response FILE
     */
    public function actionQr()
    {
        $id = self::getParam('id');
        $billing = UserBill::findOne($id);
        $content = (new \Endroid\QrCode\QrCode())
            ->setText('bitcoin:' . $billing->address . '?i-am-avatar=true')
            ->setSize(300)
            ->setPadding(10)
            ->setErrorCorrection('high')
            ->setForegroundColor(['r' => 0, 'g' => 0, 'b' => 0, 'a' => 0])
            ->setBackgroundColor(['r' => 255, 'g' => 255, 'b' => 255, 'a' => 0])
            ->setLabelFontSize(16)
            ->setImageType(\Endroid\QrCode\QrCode::IMAGE_TYPE_PNG)
            ->get('png');

        return Yii::$app->response->sendContentAsFile($content, $billing->address . '.' . 'png');
    }


}
