<?php

namespace avatar\controllers;

use app\models\Article;
use app\models\SiteUpdate;
use app\services\Subscribe;
use avatar\modules\UniSender\UniSender;
use common\components\providers\ETH;
use common\models\avatar\Currency;
use common\models\avatar\UserBill;
use common\models\avatar\UserBillDefault;
use common\models\comment\Comment;
use common\models\comment\CommentList;
use common\models\comment\Type;
use common\models\HD;
use common\models\HDtown;
use common\models\UserAvatar;
use common\models\UserDocument;
use common\services\Security\AES;
use cs\base\BaseController;
use cs\services\SitePath;
use cs\services\UploadFolderDispatcher;
use cs\services\VarDumper;
use cs\web\Exception;
use iAvatar777\services\FormAjax\DefaultFormAjax;
use Yii;
use yii\base\UserException;
use yii\bootstrap\ActiveForm;
use yii\db\Connection;
use yii\db\Query;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use yii\helpers\FileHelper;
use yii\web\Response;

class CommentsController extends \avatar\controllers\CabinetBaseController
{
    public function actions()
    {
        return [
            'add-ajax' => [
                'class' => '\iAvatar777\services\FormAjax\DefaultFormAjax',
                'model' => '\avatar\modules\Comment\Model',
            ],
        ];
    }

    /**
     */
    public function actionAdd()
    {
        $text = self::getParam('text');
        $list_id = self::getParam('list_id');
        $parent_id = self::getParam('parent_id');

        $c = Comment::add([
            'list_id'   => $list_id,
            'user_id'   => Yii::$app->user->id,
            'parent_id' => $parent_id,
            'text'      => $text,
        ]);

        // выполняю AFTER_INSERT
        $list = CommentList::findOne($list_id);
        $type = Type::findOne($list->type_id);
        if ($type->after_insert) {
            /** @var \common\models\comment\iCommentAfterInsert $class */
            $class = new $type->after_insert;
            $class->afterInsert($c);
        }

        return self::jsonSuccess($c);
    }

    /**
     */
    public function actionDelete()
    {
        $id = self::getParam('id');
        $c = Comment::findOne($id);
        if ($c->user_id != Yii::$app->user->id) {
            return self::jsonErrorId(101, 'Не ваша запись');
        }
        $c->delete();

        return self::jsonSuccess();
    }

}
