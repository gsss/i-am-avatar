<?php

namespace avatar\controllers;

use app\services\Subscribe;
use avatar\controllers\AdminBaseController;
use avatar\models\forms\BlogItem;
use avatar\models\forms\UserAvatar;
use common\models\investment\IcoRequest;
use common\models\investment\Project;
use cs\Application;
use cs\models\Tables\Client;
use cs\services\VarDumper;
use cs\web\Exception;
use Yii;
use yii\base\UserException;
use yii\filters\AccessControl;
use yii\helpers\Json;
use yii\helpers\Url;
use yii\httpclient\Client as ClientHttp;
use yii\web\Controller;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use app\models\NewsItem;
use app\models\Chenneling;
use cs\base\BaseController;
use yii\web\Response;
use yii\widgets\ActiveForm;
use app\models\SiteUpdate;

class AdminProjectsController extends AdminBaseController
{


    public function actionIndex()
    {
        return $this->render();
    }

    /**
     * Платежные реквизиты проекта
     *
     * @param int $id
     *
     * @return string
     *
     * @throws
     */
    public function actionPayments($id)
    {
        $project = Project::findOne($id);

        return $this->render(['project' => $project]);
    }

    /**
     *
     * @param int $id идентификатор проекта
     * @return string
     * @throws
     */
    public function actionView($id)
    {
        $project = Project::findOne($id);

        return $this->render(['project' => $project]);
    }

    /**
     * отправляет на модерацию
     *
     * @return string
     */
    public function actionSendSuccess()
    {
        $id = self::getParam('id');
        $project = Project::findOne($id);
        $project->status = Project::STATUS_ASSEPTED;
        $project->save();

        Application::mail($project->getUser()->email, 'Проект одобрен', 'ico_moderation_send_assept', [
            'project' => $project
        ]);

        return self::jsonSuccess();
    }

    /**
     * отправляет на модерацию
     *
     * @return string
     */
    public function actionSendReject()
    {
        $id = self::getParam('id');
        $project = Project::findOne($id);
        $project->status = Project::STATUS_REJECTED;
        $project->save();
        Application::mail($project->getUser()->email, 'Проект отклонен', 'ico_moderation_send_reject', [
            'project' => $project
        ]);

        return self::jsonSuccess();
    }

    /**
     * Удаляет конфиг ПС
     *
     * REQUEST:
     * - id - int - идентификатор конфига ПС
     *
     * @return string
     *
     * @throws
     */
    public function actionPaymentsRemove()
    {
        $id = self::getParam('id');
        if (is_null($id)) {
            return self::jsonErrorId(101, 'Не передан параметр');
        }
        if (!Application::isInteger($id)) {
            return self::jsonErrorId(102, 'Не верные данные');
        }
        $config = \common\models\PaySystemConfig::findOne($id);
        $project = Project::findOne($config->parent_id);
        if (is_null($project)) {
            return self::jsonErrorId(104, 'Не найден проект');
        }
        if ($project->user_id != Yii::$app->user->id) {
            return self::jsonErrorId(103, 'Это не ваш проект');
        }
        $config->delete();

        return self::jsonSuccess();
    }

    /**
     * Заказы проекта
     *
     * @param int $id - идентификатор проекта
     *
     * @return string
     */
    public function actionRequests($id)
    {
        $project = Project::findOne($id);

        return $this->render(['project' => $project]);
    }

    /**
     * Заказ проекта
     *
     * @param int $id - идентификатор заказа ico_request
     *
     * @return string
     */
    public function actionRequest($id)
    {
        $request = IcoRequest::findOne($id);

        return $this->render(['request' => $request]);
    }

    public function actionEdit($id)
    {
        $model = \avatar\models\forms\Project::findOne($id);
        if ($model->load(Yii::$app->request->post()) && $model->update()) {
            Yii::$app->session->setFlash('form', 1);

            return $this->refresh();
        } else {
            return $this->render([
                'model' => $model,
            ]);
        }
    }

    public function actionDelete($id)
    {
        \avatar\models\forms\Project::findOne($id)->delete();

        return self::jsonSuccess();
    }
}
