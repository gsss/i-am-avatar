<?php

namespace avatar\controllers;

use avatar\base\Application;
use avatar\models\search\UserAvatar;
use avatar\modules\ETH\ServiceEtherScan;
use avatar\modules\ETH\ServiceEthPlorer;
use common\models\avatar\Currency;
use common\models\avatar\UserBill;
use common\models\avatar\UserBillSystem;
use common\payment\BitCoinBlockTrailPayment;
use console\controllers\MoneyRateController;
use cs\web\Exception;
use Yii;
use yii\filters\AccessControl;
use yii\helpers\FileHelper;
use yii\helpers\Html;
use yii\web\Response;

class AdminUsersWalletListController extends \avatar\controllers\AdminBaseController
{
    public static $isStatistic = false;


    /**
     * Возвращает баланс в счете
     * AJAX
     * REQUEST:
     * - id - int - идентификатор счета
     *
     * @return string json array
     * [
     *      '<confirmed>',   (BTC)
     *      '<unconfirmed>', (BTC)
     *      '<confirmed>',   (валюта установленная в качетсве просмотра)
     *      '<unconfirmed>', (валюта установленная в качетсве просмотра)
     *      '<currencyView>', установленный параметр просмотра валюты, если 0 значит нет и 3 и 4 параметры можно не
     *      учитывать
     * ]
     */
    public function actionGetBalance()
    {
        $id = self::getParam('id');
        $bill = $this->getBill($id);
        switch ($bill->currency) {
            case Currency::BTC:
                return $this->actionBalanceInternal($id);
            case Currency::ETH:
                return $this->actionBalanceInternalEth($id);
            case Currency::ETC:
                return $this->actionBalanceInternalEtc($id);
            default:
                return $this->actionBalanceInternalToken($id);
        }
    }


    /**
     * Выдает кол-во денег на счету для токена
     *
     * AJAX
     * REQUEST:
     *
     * @return string json response
     * [
     *      'confirmed'             => string кол-во монет токенов с десятичными
     *      'confirmedConverted'    => string              - не обязательное, если currencyView > 0 то присутствует
     *      'currencyView'          => int
     *      'currencyString'        => string например 'ETH'/'RUB'
     * ]
     */
    public function actionBalanceInternalToken($id)
    {
        if (YII_ENV_DEV) return self::jsonSuccess([
            'confirmedBalance' => 0,
        ]);
        $billing = $this->getBill($id);
        $currency = $billing->currency;
        $currencyObject = Currency::findOne($currency);

        $balanceInt = $currencyObject->getToken()->getProvider()->getBalance($billing->address);
        $decimals = pow(10, $currencyObject->decimals);
        $confirmedBalance = $balanceInt / $decimals;

        /** @var \common\models\UserAvatar $user */
        $user = Yii::$app->user->identity;
        $currencyView = $user->currency_view;
        $return = [
            'confirmed'          => Yii::$app->formatter->asDecimal($confirmedBalance, 2),
            'confirmedConverted' => Yii::$app->formatter->asDecimal($confirmedBalance, $currencyObject->decimals),
            'currencyView'       => $currencyView,
            'currencyString'     => $currencyObject->code,
            'unconfirmed'        => 0,
        ];

        return self::jsonSuccess($return);
    }


    /**
     * AJAX
     * REQUEST:
     *
     * @return string json response
     * [
     *      'confirmedBalance'   => BTC
     *      'unconfirmedBalance' => BTC
     *      'all'                => mBTC
     * ]
     */
    public function actionBalanceInternal($id)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        if (YII_ENV_DEV) return self::jsonSuccess([
            'confirmedBalance'   => 0,
            'unconfirmedBalance' => 0,
            'all'                => 0,
        ]);
        $provider = new BitCoinBlockTrailPayment();
        $client = $provider->getClient();
        $billing = $this->getBill($id);
        $w = $client->getWalletBalance($billing->identity);
        $confirmedBalance = $w['confirmed'];

        /** @var \common\models\UserAvatar $user */
        $user = Yii::$app->user->identity;
        $currencyView = $user->currency_view;
        if (is_null($currencyView)) $currencyView = 0;
        $confirmed = $confirmedBalance / 100000000;

        $params = [
            'confirmedBalance'   => $confirmedBalance / 100000000,
            'confirmed'          => $confirmed,
            'currencyView'       => $currencyView,
            'currencyString'     => 'BTC',

            'coins'              => $confirmedBalance  / 100000000,
            'coinsFormated'      => Yii::$app->formatter->asDecimal($confirmedBalance / 100000000, 8),
        ];

        switch ($currencyView) {
            case 1: // RUB
                $confirmedConverted = Currency::convertBySettings($confirmedBalance / 100000000, 'BTC');

                $confirmed = Yii::$app->formatter->asDecimal($confirmedConverted, 2);

                $params['currencyString'] = 'RUB';
                $params['convert'] = $confirmedConverted;
                $params['convertFormated'] = $confirmed;
                break;
            case 2: // USD
                $confirmedConverted = Currency::convertBySettings($confirmedBalance / 100000000, 'BTC');

                $confirmed = Yii::$app->formatter->asDecimal($confirmedConverted, 2);

                $params['currencyString'] = 'USD';
                $params['convert'] = $confirmedConverted;
                $params['convertFormated'] = $confirmed;
                break;
        }

        return self::jsonSuccess($params);
    }

    /**
     * Выдает кол-во денег на счету
     *
     * AJAX
     * REQUEST:
     *
     * @return string json response
     * [
     *      'confirmed'             => string ETH
     *      'confirmedConverted'    => string              - не обязательное, если currencyView > 0 то присутствует
     *      'currencyView'          => int
     *      'currencyString'        => string например 'ETH'/'RUB'
     * ]
     */
    public function actionBalanceInternalEth($id)
    {
        if (YII_ENV_DEV) return self::jsonSuccess([
            'confirmedBalance' => 0,
        ]);
        if (!YII_ENV_PROD) {
            return $this->actionBalanceInternalEthTest($id);
        } else {
            return $this->actionBalanceInternalEthProd($id);
        }
    }

    /**
     * Выдает кол-во денег на счету
     *
     * AJAX
     * REQUEST:
     *
     * @return string json response
     * [
     *      'confirmed'             => string ETH
     *      'confirmedConverted'    => string              - не обязательное, если currencyView > 0 то присутствует
     *      'currencyView'          => int
     *      'currencyString'        => string например 'ETH'/'RUB'
     * ]
     */
    public function actionBalanceInternalEthProd($id)
    {
        if (YII_ENV_DEV) return self::jsonSuccess([
            'confirmedBalance' => 0,
        ]);

        $billing = $this->getBill($id);
        $provider = new ServiceEthPlorer();
        /**
         * Возвращает баланс в счете ETH
         * AJAX
         * REQUEST:
         * - id - int - идентификатор счета
         *
         * @return string json array
         * {
         * address: # address,
         * ETH: {   # ETH specific information
         * balance:  # ETH balance
         * totalIn:  # Total incoming ETH value
         * totalOut: # Total outgoing ETH value
         * },
         * contractInfo: {  # exists if specified address is a contract
         * creatorAddress:  # contract creator address,
         * transactionHash: # contract creation transaction hash,
         * timestamp:       # contract creation timestamp
         * },
         * tokenInfo:  # exists if specified address is a token contract address (same format as token info),
         * tokens: [   # exists if specified address has any token balances
         * {
         * tokenInfo: # token data (same format as token info),
         * balance:   # token balance (as is, not reduced to a floating point value),
         * totalIn:   # total incoming token value
         * totalOut:  # total outgoing token value
         * },
         * ...
         * ],
         * countTxs:    # Total count of incoming and outcoming transactions (including creation one),
         * }
         */
        $response = $provider->get('getAddressInfo/' . $billing->address);

        /** @var \avatar\models\forms\UserAvatar $user */
        $user = Yii::$app->user->identity;
        $currencyView = $user->currency_view;
        if (is_null($currencyView)) $currencyView = 0;
        $confirmedBalance = $response['ETH']['balance'];

        if ($currencyView > 0) {
            $currency = Currency::findOne($currencyView);
            $currencyString = $currency->code;
            $confirmedConverted = Currency::convertBySettings($confirmedBalance, 'ETH', false);

            $return = [
                'confirmed'          => $confirmedBalance,
                'currencyView'       => $currencyView,
                'currencyString'     => $currencyString,
                'unconfirmed'        => 0,
                'confirmedConverted' => $confirmedConverted,

                'coins'           => $confirmedBalance,
                'coinsFormated'   => Yii::$app->formatter->asDecimal($confirmedBalance, 8),
                'convert'         => $confirmedConverted,
                'convertFormated' => Yii::$app->formatter->asDecimal($confirmedConverted, 2),
            ];
        } else {
            $return = [
                'confirmed'      => Yii::$app->formatter->asDecimal($confirmedBalance, 18),
                'currencyView'   => $currencyView,
                'currencyString' => 'ETH',
                'unconfirmed'    => 0,

                'coins'         => $confirmedBalance,
                'coinsFormated' => Yii::$app->formatter->asDecimal($confirmedBalance, 8),
            ];
        }
        $response['avatarNetwork'] = $return;

        return self::jsonSuccess($response);
    }

    /**
     * Выдает кол-во денег на счету
     *
     * AJAX
     * REQUEST:
     *
     * @return string json response
     * [
     *      'confirmed'             => string ETH
     *      'confirmedConverted'    => string              - не обязательное, если currencyView > 0 то присутствует
     *      'currencyView'          => int
     *      'currencyString'        => string например 'ETH'/'RUB'
     * ]
     */
    public function actionBalanceInternalEthTest($id)
    {
        $billing = $this->getBill($id);
        $provider = new ServiceEtherScan();
        $data = $provider->getBalance($billing->address);

        $balance = $data[0]['balance'] / pow(10, 18);

        /** @var \avatar\models\forms\UserAvatar $user */
        $user = Yii::$app->user->identity;
        $currencyView = $user->currency_view;
        if (is_null($currencyView)) $currencyView = 0;
        $confirmedBalance = $balance;

        if ($currencyView > 0) {
            $currency = Currency::findOne($currencyView);
            $currencyString = $currency->code;
            $confirmedConverted = Currency::convertBySettings($confirmedBalance, 'ETH', false);

            $return = [
                'confirmed'          => $confirmedBalance,
                'currencyView'       => $currencyView,
                'currencyString'     => $currencyString,
                'unconfirmed'        => 0,
                'confirmedConverted' => $confirmedConverted,

                'coins'           => $confirmedBalance,
                'coinsFormated'   => Yii::$app->formatter->asDecimal($confirmedBalance, 8),
                'convert'         => $confirmedConverted,
                'convertFormated' => Yii::$app->formatter->asDecimal($confirmedConverted, 2),
            ];
        } else {
            $return = [
                'confirmed'      => Yii::$app->formatter->asDecimal($confirmedBalance, 18),
                'currencyView'   => $currencyView,
                'currencyString' => 'ETH',
                'unconfirmed'    => 0,

                'coins'         => $confirmedBalance,
                'coinsFormated' => Yii::$app->formatter->asDecimal($confirmedBalance, 8),
            ];
        }
        $response['avatarNetwork'] = $return;

        return self::jsonSuccess($response);
    }

    /**
     * Выдает кол-во денег на счету
     *
     * AJAX
     * REQUEST:
     *
     * @return string json response
     * [
     *      'confirmed'             => string ETH
     *      'confirmedConverted'    => string              - не обязательное, если currencyView > 0 то присутствует
     *      'currencyView'          => int
     *      'currencyString'        => string например 'ETC','RUB'
     * ]
     */
    public function actionBalanceInternalEtc($id)
    {
        if (YII_ENV_DEV) return self::jsonSuccess([
            'confirmedBalance' => 0,
        ]);
        $billing = $this->getBill($id);
        $walletEtc = $billing->getWalletETC();
        $confirmedBalance = $walletEtc->getBalance();

        /** @var \avatar\models\forms\UserAvatar $user */
        $user = Yii::$app->user->identity;
        $currencyView = $user->currency_view;
        if (is_null($currencyView)) $currencyView = 0;

        if ($currencyView > 0) {
            $currency = Currency::findOne($currencyView);
            $currencyString = $currency->code;

            $return = [
                'confirmed'      => $confirmedBalance,
                'currencyView'   => $currencyView,
                'currencyString' => $currencyString,
                'unconfirmed'    => 0,
            ];

            $return['confirmedConverted'] = Currency::convertBySettings($confirmedBalance, 'ETC', true);
            $return['confirmed'] = $return['confirmedConverted'];
        } else {
            $return = [
                'confirmed'      => $confirmedBalance,
                'currencyView'   => $currencyView,
                'currencyString' => 'ETC',
                'unconfirmed'    => 0,
            ];

            $return['confirmed'] = Yii::$app->formatter->asDecimal($confirmedBalance, 18);
        }

        return self::jsonSuccess($return);
    }

    /**
     * Получает счет ID
     * Делает проверку на хозяина
     *
     * @return \common\models\avatar\UserBill
     * @throws \yii\base\Exception
     */
    public function getBill($id = null)
    {
        if (is_null($id)) {
            $id = self::getParam('id');
        }
        $bill = UserBill::findOne($id);

        return $bill;
    }

}
