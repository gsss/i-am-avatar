<?php

namespace avatar\controllers;

use backend\models\form\SearchLanguage;
use common\models\Language;
use common\models\language\Category;
use common\models\language\Message;
use common\models\language\MessageToDelete;
use common\models\language\SourceMessage;
use Yii;
use yii\base\Exception;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;

class LanguagesController extends AdminBaseController
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow'   => true,
                        'roles'   => ['role_languages'],
                    ],
                ],
            ],
        ];
    }

    public function actions()
    {
        return [
            'category-delete' => [
                'class' => '\backend\controllers\action\languages\actionCategoryDelete',
            ],
        ];
    }

    public function actionIndex()
    {
        return $this->render('index');
    }

    public function actionTablesHorizont()
    {
        return $this->render('tables-horizont');
    }

    public function actionCategoryAdd()
    {
        $model = new Category();
        $code = \Yii::$app->security->generateRandomString(30);
        $code = str_replace('-', '', $code);
        $code = str_replace('_', '', $code);
        $code = substr($code, 0, 10);
        $code = 'c' . '.' . $code;

        if ($model->load(Yii::$app->request->post())) {
            if (empty($model->code)) {
                $model->code = $code;
            }
            if ($model->save()) {
                Yii::$app->session->setFlash('form', 1);
                return $this->refresh();
            }
        } else {
            if (empty($model->code)) {
                $model->code = $code;
            }
        }

        return $this->render('category-add', [
            'model' => $model
        ]);
    }

    /**
     * AJAX
     * REQUEST:
     * - int $id - идентификатор категории languages_category_tree.id
     *
     * @return string|\yii\web\Response
     */
    public function actionCategoryExport()
    {
        $id = self::getParam('id');
        $category = Category::findOne($id);
        if (is_null($category)) {
            return self::jsonErrorId(101, 'Не найдена категория');
        }

        $ids = $category->getMessages(true)->select('source_message.id')->column();

        $file = Yii::getAlias('@avatar/views/languages/_migration.php');
        $className = 'm' . date('ymd_His') . '_lang';
        $file = Yii::$app->view->renderFile($file, [
            'className'    => $className,
            'ids'          => $ids,
            'categoryTree' => [
                'id'    => $id,
                'name'  => $category->name,
                'code'  => $category->code,
                'nodes' => Category::getRows(['parentId' => $id, 'selectFields' => 'id, name, code']),
                'path'  => $category->getPath(),
            ],
        ]);

        return Yii::$app->response->sendContentAsFile($file, $className . '.php');
    }


    /**
     * AJAX
     * REQUEST:
     * - int $id - идентификатор категории languages_category_tree.id
     *
     * @return string|\yii\web\Response
     */
    public function actionCategoryExportJson()
    {
        ini_set('memory_limit', -1);
        $id = self::getParam('id');
        $category = Category::findOne($id);
        if (is_null($category)) {
            return self::jsonErrorId(101, 'Не найдена категория');
        }

        $ids = $category->getMessages(true)->select('source_message.id')->column();

        $file = Yii::getAlias('@avatar/views/languages/_exportJson.php');
        $className = 'm' . date('ymd_His') . '_lang';
        $file = Yii::$app->view->renderFile($file, [
            'className'    => $className,
            'ids'          => $ids,
            'categoryTree' => [
                'id'    => $id,
                'name'  => $category->name,
                'code'  => $category->code,
                'nodes' => Category::getRows(['parentId' => $id, 'selectFields' => 'id, name, code']),
                'path'  => $category->getPath(),
            ],
        ]);

        return Yii::$app->response->sendContentAsFile($file, $className . '.json');
    }

    /**
     * AJAX
     * REQUEST:
     * - int $id - идентификатор категории languages_category_tree.id
     *
     * @return string|\yii\web\Response
     */
    public function actionCategoryExportCsv()
    {
        $id = self::getParam('id');
        $category = Category::findOne($id);
        if (is_null($category)) {
            return self::jsonErrorId(101, 'Не найдена категория');
        }

        $ids = $category->getMessages(true)->select('source_message.id')->column();

        $file = Yii::getAlias('@avatar/views/languages/_exportCsv.php');
        $className = 'm' . date('ymd_His') . '_lang';
        $file = Yii::$app->view->renderFile($file, [
            'className'    => $className,
            'ids'          => $ids,
            'categoryTree' => [
                'id'    => $id,
                'name'  => $category->name,
                'code'  => $category->code,
                'nodes' => Category::getRows(['parentId' => $id, 'selectFields' => 'id, name, code']),
                'path'  => $category->getPath(),
            ],
        ]);

        return Yii::$app->response->sendContentAsFile($file, $className . '.csv');
    }


    /**
     * Делает экспорт строк
     * REQUEST:
     * - ids - string - массив для экспорта source_message.id разделенные запятыми
     *
     * @return \yii\web\Response файл на скачивание
     */
    public function actionExport()
    {
        ini_set('max_execution_time', 300);
        ini_set('memory_limit', -1);
        $ids = self::getParam('ids');
        $ids = explode(',', $ids);

        $file = Yii::getAlias('@avatar/views/languages/_migration.php');
        $className = 'm' . date('ymd_His') . '_lang';
        $file = Yii::$app->view->renderFile($file, [
            'className' => $className,
            'ids'       => $ids,
        ]);

        return Yii::$app->response->sendContentAsFile($file, $className . '.php');
    }

    /**
     * Делает экспорт строк в JSON
     * REQUEST:
     * - ids - array - массив для экспорта source_message.id
     *
     * @return \yii\web\Response файл на скачивание
     */
    public function actionExportJson()
    {
        ini_set('memory_limit', -1);
        $ids = self::getParam('ids');
        $ids = explode(',', $ids);

        $file = Yii::getAlias('@avatar/views/languages/_exportJson.php');
        $className = 'm' . date('ymd_His') . '_lang';
        $file = Yii::$app->view->renderFile($file, [
            'className' => $className,
            'ids'       => $ids,
        ]);

        return Yii::$app->response->sendContentAsFile($file, $className . '.json');
    }

    /**
     * AJAX
     * Добавляет ключ (новую строку)
     * REQUEST:
     * - value - string - эта строка будет добавлена source_message.message
     * - category - string - languages_category_tree.code = source_message.category
     * - type - int - тип строки
     *
     * @return string json
     * [
     *      'status' => bool,
     *      'data'   => int - идентификатор строки добавленной,
     * ]
     */
    public function actionAddKey()
    {
        $value = self::getParam('value');
        $category = self::getParam('category');
        $type = self::getParam('type');
        if (is_null($category)) {
            return self::jsonErrorId(101, 'Нет параметра category');
        }
        if (is_null($value)) {
            return self::jsonErrorId(102, 'Нет параметра value');
        }
        if (is_null($type)) {
            return self::jsonErrorId(102, 'Нет параметра type');
        }
        if (!Category::find()->where(['code' => $category])->exists()) {
            return self::jsonErrorId(103, 'Нет такой категории');
        }
        if (SourceMessage::find()->where(['category' => $category, 'message' => $value])->exists()) {
            return self::jsonErrorId(104, 'Такой ключ уже есть');
        }

        $item = new SourceMessage([
            'category' => $category,
            'message'  => $value,
            'type'     => $type,
        ]);
        $ret = $item->save();
        if (!$ret) {
            return self::jsonErrorId(104, 'Не удалось сохранить');
        }

        return self::jsonSuccess($item->getDb()->getLastInsertID());
    }

    /**
     * AJAX
     * Удаляет строку
     * REQUEST:
     * - id - int - идентификатор строки исходной source_message.id
     *
     * @return string json
     * [
     *      'status' => bool,
     * ]
     */
    public function actionDelete()
    {
        $id = self::getParam('id');
        $message = SourceMessage::findOne($id);
        if (is_null($message)) {
            return self::jsonErrorId(101, 'Нет такой строки');
        }
        if ($message->type == SourceMessage::TYPE_IMAGE) {
            $rows = Message::find()->where(['id' => $message->id])->all();
            /** @var \common\models\language\Message $messageObject */
            foreach ($rows as $messageObject) {
                $pathPrefixLength = 55;
                $translation = $messageObject->translation;
                $path_b = $translation;
                $info = pathinfo($translation);
                $ext = strtolower($info['extension']);
                $path_t = substr($messageObject->translation, 0, $pathPrefixLength) . '_t' . '.' . $ext;
                $fullPath_b = Yii::getAlias(\backend\models\form\LanguageImageSave::$pathSave1 . $path_b);
                $fullPath_t = Yii::getAlias(\backend\models\form\LanguageImageSave::$pathSave1 . $path_t);

                // удаляю старые файлы
                if (file_exists($fullPath_b)) {
                    unlink($fullPath_b);
                }
                if (file_exists($fullPath_t)) {
                    unlink($fullPath_t);
                }
            }
        }
        if ($message->type == SourceMessage::TYPE_FILE) {
            $rows = Message::find()->where(['id' => $message->id])->all();
            /** @var \common\models\language\Message $messageObject */
            foreach ($rows as $messageObject) {
                $translation = $messageObject->translation;
                $path_b = $translation;
                $fullPath_b = Yii::getAlias(\backend\models\form\LanguageImageSave::$pathSave1 . $path_b);

                // удаляю старые файлы
                if (file_exists($fullPath_b)) {
                    unlink($fullPath_b);
                }
            }
        }
        Message::deleteAll(['id' => $message->id]);
        $message->delete();

        return self::jsonSuccess();
    }

    public function actionCategory()
    {
        if (Yii::$app->request->isPost) {
            if (in_array('action-upload-image', array_keys(Yii::$app->request->post()))) {
                $model = new \avatar\models\forms\LanguageImageSave();
                if ($model->load(Yii::$app->request->post(), '')) {
                    if (!$model->validate()) Debugger::dump($model->errors);
                    $model->action();
                }
            }
            if (in_array('action-upload-file', array_keys(Yii::$app->request->post()))) {
                $model = new \avatar\models\forms\LanguageFileSave();
                if ($model->load(Yii::$app->request->post(), '')) {
                    if ($model->validate()) {
                        $model->action();
                    }
                }
            }
            if (in_array('action-delete-file', array_keys(Yii::$app->request->post()))) {
                $model = new \avatar\models\forms\LanguageFileDelete();
                if ($model->load(Yii::$app->request->post(), '')) {
                    if (!$model->validate()) Debugger::dump($model->errors);
                    $model->action();
                }
            }
            if (in_array('action-delete-image', array_keys(Yii::$app->request->post()))) {
                $model = new \avatar\models\forms\LanguageImageDelete();
                if ($model->load(Yii::$app->request->post(), '')) {
                    if (!$model->validate()) Debugger::dump($model->errors);
                    $model->action();
                }
            }
            if (in_array('action-upload-json', array_keys(Yii::$app->request->post()))) {
                $model = new \avatar\models\forms\LanguageImportFile();
                ini_set("memory_limit", "1000M");
                if ($model->load(Yii::$app->request->post(), '')) {
                    if (!$model->validate()) Debugger::dump($model->errors);
                    $result = $model->action();
                    if (is_null($result)) {
                        Yii::$app->session->setFlash('\backend\controllers\LanguagesController::actionCategory', 1);
                    }
                }
            }
        }

        return $this->render('category', [
            'id' => Yii::$app->request->get('id')
        ]);
    }

    public function actionImportReplace()
    {
        ini_set("memory_limit", "1000M");
        $migration = new \common\components\Migration(['isLog' => false]);
        $ret = $migration->replaceStrings(Yii::$app->cacheFile->get('\common\components\Migration::updateStrings'));
        Yii::$app->cacheFile->delete('\common\components\Migration::updateStrings');
        Yii::$app->cacheFile->delete('\common\components\Migration::updateStringsIds');
        Yii::$app->session->removeFlash('\backend\controllers\LanguagesController::actionCategory');

        return self::jsonSuccess();
    }

    public function actionImportNew()
    {
        $migration = new \common\components\Migration(['isLog' => false]);
        $ret = $migration->newStrings(Yii::$app->cacheFile->get('\common\components\Migration::updateStrings'));
        Yii::$app->cacheFile->delete('\common\components\Migration::updateStrings');
        Yii::$app->cacheFile->delete('\common\components\Migration::updateStringsIds');
        Yii::$app->session->removeFlash('\backend\controllers\LanguagesController::actionCategory');

        return self::jsonSuccess();
    }

    public function actionImportCancel()
    {
        Yii::$app->cacheFile->delete('\common\components\Migration::updateStrings');
        Yii::$app->cacheFile->delete('\common\components\Migration::updateStringsIds');
        Yii::$app->session->removeFlash('\backend\controllers\LanguagesController::actionCategory');

        return self::jsonSuccess();
    }

    public function actionSearch()
    {
        if (Yii::$app->request->isPost) {
            if (in_array('action-upload-image', array_keys(Yii::$app->request->post()))) {
                $model = new \backend\models\form\LanguageImageSave();
                if ($model->load(Yii::$app->request->post(), '')) {
                    if (!$model->validate()) Debugger::dump($model->errors);
                    $model->action();
                }
            }
            if (in_array('action-delete-image', array_keys(Yii::$app->request->post()))) {
                $model = new \backend\models\form\LanguageImageDelete();
                if ($model->load(Yii::$app->request->post(), '')) {
                    if (!$model->validate()) Debugger::dump($model->errors);
                    $model->action();
                }
            }
        }

        return $this->render('search');
    }

    /**
     * Выводит страницу "Поиск везде"
     *
     * @return string|\yii\web\Response
     */
    public function actionSearchAll()
    {
        $model = new SearchLanguage();

        if ($model->load(Yii::$app->request->get()) && $model->validate()) {
            $model->search();
            Yii::$app->session->setFlash('form', 1);
        }

        return $this->render('search-all', [
            'model' => $model,
        ]);
    }

    /**
     * Выводит страницу "Пустые строки"
     *
     * @return string|\yii\web\Response
     */
    public function actionSearchNull()
    {
        $model = new \backend\models\form\LanguageNull();

        if (Yii::$app->request->isPost) {
            if (in_array('action-upload-image', array_keys(Yii::$app->request->post()))) {
                $model2 = new \backend\models\form\LanguageImageSave();
                if ($model2->load(Yii::$app->request->post(), '')) {
                    if (!$model2->validate()) Debugger::dump($model2->errors);
                    $model2->action();
                }
            }
        }

        if ($model->load(\Yii::$app->request->get()) && $model->validate()) {
            $model->search();
            Yii::$app->session->setFlash('form', 1);
        }

        return $this->render('search-null', [
            'model' => $model,
        ]);
    }

    /**
     * GET:
     * + language - string(2) - язык
     *
     * @return string|\yii\web\Response
     */
    public function actionSearchNullExport()
    {
        $language = self::getParam('language');

        $query = \common\models\language\SourceMessage::find()
            ->select([
                'source_message.id',
                'source_message.is_locked',
                'source_message.is_to_delete',
                'source_message.type',
                'source_message.category',
                'source_message.message',
                'languages_category_tree.id as cid',
                'languages_category_tree.name',
            ])
            ->leftJoin('message', 'source_message.id = message.id and message.language=:language', [':language' => $language])
            ->innerJoin('languages_category_tree', 'languages_category_tree.code = source_message.category')
            ->where(['message.translation' => null, 'source_message.is_to_delete' => 0])
            ->andWhere([
                'or',
                ['source_message.type' => 1],
                ['source_message.type' => null],
            ])
            ->asArray();

        $rows = [
            join(';', [
                'id',
                'ru',
                'en',
                'key',
                'category',
                $language,
            ])
        ];
        foreach ($query->all() as $row) {
            $r = [];
            $r[] = $row['id'];
            $sourceMessage = \common\models\language\SourceMessage::findOne($row['id']);
            $messageObject = \common\models\language\Message::findOne(['id' => $row['id'], 'language' => 'ru']);
            $r[] = $this->convertToCsv($messageObject->translation);
            $messageObject = \common\models\language\Message::findOne(['id' => $row['id'], 'language' => 'en']);
            $r[] = $this->convertToCsv($messageObject->translation);
            $r[] = $this->convertToCsv($sourceMessage->message);
            $r[] = $this->convertToCsv($row['category']);
            $rows[] = join(';', $r);
        }

        return Yii::$app->response->sendContentAsFile(join("\n", $rows), $language.'.csv');
    }

    private function convertToCsv($value)
    {
        if (is_null($value)) return '=' . '"' . '' . '"';

        $value = iconv('utf-8', 'windows-1251', $value);
        $value = str_replace('"', '""', $value);

        return '"' . $value . '"';
    }

    /**
     * Выдает файл на скачивание
     *
     * REQUEST:
     * - id - int - source_message.id
     * - language - string -
     *
     * @return string
     */
    public function actionDownload()
    {
        $id = self::getParam('id');
        $language = self::getParam('language');

        $item = SourceMessage::findOne($id);
        if (is_null($item)) return '';

        $isToDelete = false;
        if ($item['is_to_delete']) $isToDelete = true;
        if ($isToDelete) {
            $message = MessageToDelete::findOne(['id' => $id, 'language' => $language]);
        } else {
            $message = Message::findOne(['id' => $id, 'language' => $language]);
        }

        if (is_null($message)) {
            throw new \Exception('Нет строки');
        }
        $path = Yii::getAlias(\backend\models\form\LanguageImageSave::$pathSave1 . $message->translation);
        $info = pathinfo($path);
        if (!file_exists($path)) {
            throw new \Exception('Нет файла');
        }
        $content = file_get_contents($path);

        return Yii::$app->response->sendContentAsFile($content, $info['basename']);
    }

    /**
     * AJAX
     * Обновляет строку
     * REQUEST:
     * - id - int - идентификатор строки `message.id`
     * - language - string - идентификатор `languages.code`
     * - value - string - новое значение для строки
     */
    public function actionUpdateString()
    {
        Yii::$app->response->format = 'json';

        $id = Yii::$app->request->post('id');
        if (is_null($id)) {
            return self::jsonErrorId(101, 'Не найдена строка');
        }
        $language = Yii::$app->request->post('language');
        if (is_null($language)) {
            return self::jsonErrorId(102, 'Не указан язык');
        }
        $value = Yii::$app->request->post('value');
        if (is_null($value)) {
            return self::jsonErrorId(103, 'Не указана строка');
        }
        $languageObject = Language::findOne(['code' => $language]);
        if ($value == '') {
            $message = Message::findOne([
                'id'       => $id,
                'language' => $languageObject->code,
            ]);
            if (!is_null($message)) {
                $message->delete();
            }
        } else {
            $message = Message::findOne([
                'id'       => $id,
                'language' => $languageObject->code,
            ]);
            if (is_null($message)) $message = new Message([
                'id'       => $id,
                'language' => $languageObject->code,
            ]);
            $message->translation = $value;
            $message->save();
        }

        // сбрасываю кеш
        {
            $sourceMessage = SourceMessage::findOne($id);
            $key = [
                'yii\\i18n\\DbMessageSource',
                $sourceMessage->category,
                $languageObject->code,
            ];
            Yii::$app->cacheLanguages->flush();
        }


        return self::jsonSuccess();
    }

    /**
     * AJAX
     * Обновляет строку
     * REQUEST:
     * - id - int - таблица
     * - table - string - таблица
     * - columnName - string - название колонки
     * - language - string - идентификатор `languages.code`
     * - value - string - новое значение для строки
     */
    public function actionUpdateStringHorizontal()
    {
        Yii::$app->response->format = 'json';

        $id = Yii::$app->request->post('id');
        if (is_null($id)) {
            return self::jsonErrorId(101, 'Не указан главный параметр $id');
        }
        $table = Yii::$app->request->post('table');
        if (is_null($table)) {
            return self::jsonErrorId(101, 'Не указан главный параметр $table');
        }
        $language = Yii::$app->request->post('language');
        if (is_null($language)) {
            return self::jsonErrorId(102, 'Не указан язык');
        }
        $columnName = Yii::$app->request->post('columnName');
        if (is_null($columnName)) {
            return self::jsonErrorId(101, 'Не указан главный параметр $columnName');
        }
        $value = Yii::$app->request->post('value');
        if (is_null($value)) {
            return self::jsonErrorId(101, 'Не указан главный параметр $value');
        }

        $tableList = \console\controllers\LanguagesController::$tableList;
        $tableObject = $tableList[$table];
        $columnObject = $tableObject['column'][$columnName];
        $columnNameDb = $tableObject['name_sufix'] == 2 ? (($language == 'ru')? $columnName : $columnName . '_' . $language) : $columnName . '_' . $language;
        /** @var \yii\db\ActiveRecord $model */
        $model = $tableObject['model'];
        /** @var \yii\db\ActiveRecord $modelObject */
        $modelObject = $model::findOne($id);
        $modelObject->$columnNameDb = $value;
        $modelObject->save();

        return self::jsonSuccess();
    }

    /**
     * AJAX
     *
     * Обновляет коментраий
     *
     * REQUEST:
     * - id - int - spurce_message.id
     * - value - string - новое значение для коментария
     */
    public function actionUpdateComment()
    {
        Yii::$app->response->format = 'json';

        $id = Yii::$app->request->post('id');
        if (is_null($id)) {
            return self::jsonErrorId(101, 'Не указан главный параметр $id');
        }
        $value = Yii::$app->request->post('value');
        if (is_null($value)) {
            return self::jsonErrorId(101, 'Не указан главный параметр $value');
        }

        $sourceMessage = SourceMessage::findOne($id);
        $sourceMessage->comment = $value;
        $sourceMessage->save();

        return self::jsonSuccess();
    }

    /**
     * AJAX
     * Обновляет коментраий
     * REQUEST:
     * - id - int - source_message.id
     */
    public function actionBlock()
    {
        Yii::$app->response->format = 'json';

        $id = Yii::$app->request->post('id');
        if (is_null($id)) {
            return self::jsonErrorId(101, 'Не указан главный параметр $id');
        }

        $sourceMessage = SourceMessage::findOne($id);
        $sourceMessage->is_locked = 1;
        $sourceMessage->save();

        return self::jsonSuccess();
    }

    /**
     * AJAX
     * Обновляет коментраий
     * REQUEST:
     * - id - int - source_message.id
     */
    public function actionUnBlock()
    {
        Yii::$app->response->format = 'json';

        $id = Yii::$app->request->post('id');
        if (is_null($id)) {
            return self::jsonErrorId(101, 'Не указан главный параметр $id');
        }

        $sourceMessage = SourceMessage::findOne($id);
        $sourceMessage->is_locked = 0;
        $sourceMessage->save();

        return self::jsonSuccess();
    }

    /**
     * AJAX
     * Ставит переменную как "на удаление"
     * Переносит строки в таблицу message_to_delete
     *
     * REQUEST:
     * - id - int - source_message.id
     */
    public function actionToDelete()
    {
        Yii::$app->response->format = 'json';

        $id = Yii::$app->request->post('id');
        if (is_null($id)) {
            return self::jsonErrorId(101, 'Не указан главный параметр $id');
        }

        $sourceMessage = SourceMessage::findOne($id);
        $sourceMessage->is_to_delete = 1;
        $sourceMessage->save();

        // переношу в таблицу message_to_delete
        $messages = Message::find()->where(['id' => $id])->all();
        /** @var \common\models\language\Message $message */
        foreach ($messages as $message) {
            $item = new MessageToDelete($message->getAttributes());
            $item->save();
        }
        Message::deleteAll(['id' => $id]);
        Yii::$app->cacheLanguages->flush();

        return self::jsonSuccess();
    }

    /**
     * AJAX
     * Снимает переменную как "на удаление"
     * Переносит строки в таблицу message
     *
     * REQUEST:
     * - id - int - source_message.id
     */
    public function actionUnToDelete()
    {
        Yii::$app->response->format = 'json';

        $id = Yii::$app->request->post('id');
        if (is_null($id)) {
            return self::jsonErrorId(101, 'Не указан главный параметр $id');
        }

        $sourceMessage = SourceMessage::findOne($id);
        $sourceMessage->is_to_delete = 0;
        $sourceMessage->save();

        // переношу в таблицу message_to_delete
        $messages = MessageToDelete::find()->where(['id' => $id])->all();
        /** @var \common\models\language\Message $message */
        foreach ($messages as $message) {
            $item = new Message($message->getAttributes());
            $item->save();
        }
        MessageToDelete::deleteAll(['id' => $id]);
        Yii::$app->cacheLanguages->flush();

        return self::jsonSuccess();
    }

    /**
     * AJAX
     * Обновляет категорию
     *
     * REQUEST:
     * - id - integer - идентификатор категории languages_category_tree.id
     * - name - string - название languages_category_tree.name
     */
    public function actionCategorySave()
    {
        Yii::$app->response->format = 'json';

        $id = Yii::$app->request->post('id');
        if (is_null($id)) {
            return self::jsonErrorId(101, 'Нет обязательного параметра id');
        }
        $name = Yii::$app->request->post('name');
        if (is_null($name)) {
            return self::jsonErrorId(101, 'Нет обязательного параметра name');
        }

        $category = Category::findOne($id);
        if (is_null($category)) {
            return self::jsonErrorId(102, 'Не найдена категория');
        }
        $category->name = $name;
        $category->save();

        return self::jsonSuccess();
    }
}
