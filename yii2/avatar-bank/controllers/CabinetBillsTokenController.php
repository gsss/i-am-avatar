<?php

namespace avatar\controllers;

use avatar\models\forms\UserAvatar;
use avatar\models\validate\CardNumberAccept;
use common\components\Card;
use common\components\providers\BTC;
use common\components\providers\ETH;
use common\models\avatar\Currency;
use common\models\avatar\UserBill;
use common\models\Config;
use common\models\Token;
use common\models\UserSeed;
use common\models\UserZalog;
use common\payment\BitCoinBlockTrailPayment;
use common\services\Security;
use console\controllers\MoneyRateController;
use cs\services\VarDumper;
use cs\web\Exception;
use Yii;
use yii\helpers\FileHelper;
use yii\helpers\Json;
use yii\helpers\StringHelper;
use yii\imagine\Image;
use yii\web\Response;

class CabinetBillsTokenController extends \avatar\controllers\CabinetBaseController
{

    public function actions()
    {
        return [
            'transactions-list-token' => [
                'class' => 'avatar\controllers\actions\CabinetBills\TransactionsListToken',
            ],
        ];
    }

    public function ajaxActions()
    {
        return [
            'zalog',
            'send',
            'send-confirm',
            'call-function-execute-ajax',
            'call-function-ajax',
            'transactions-list-token',
            'zalog-return',
        ];
    }


    /**
     * Выдает курс токена
     *
     * REQUEST:
     * - token_id - int - идентификатор токена
     *
     * @return string
     */
    public function actionKurs()
    {
        $id = self::getParam('token_id');
        if (is_null($id)) {
            return self::jsonErrorId(101, 'Не найден ID');
        }
        $token = Token::findOne($id);
        if (is_null($token)) {
            return self::jsonErrorId(101, 'Не найден $token');
        }

        $rows = \common\models\ChartPoint::find()
            ->select([
                'usd',
                'time',
            ])
            ->where(['>', 'time', time() - (60 * 60 * 24 * 30)])
            ->andWhere(['currency_id' => $token->getCurrency()->id])
            ->all()
        ;

        return self::jsonSuccess([
            'rows'     => $rows,
            'currency' => [
                'code' => 'USD',
            ],
        ]);
    }

    /**
     * AJAX
     * REQUEST:
     * + zalog_id - int
     * + password - string - ключ разблокировки
     */
    public function actionZalogReturn()
    {
        $model = new \avatar\models\forms\CabinetBillsTokenZalogReturn();

        if (!$model->load(Yii::$app->request->post(), '')) {
            return self::jsonErrorId(101, 'Данные не загружены');
        }
        if (!$model->validate()) {
            return self::jsonErrorId(102, $model->convert($model->errors));
        }

        return self::jsonSuccess(['txid' => $model->action()]);
    }

    /**
     * Вызывает функцию в контракте
     *
     * @param int $id идентификатор счета
     *
     * @return string
     */
    public function actionCallFunction($id)
    {
        $billing = $this->getBill($id);
        $model = new \avatar\models\forms\CabinetBillsTokenContract();

        return $this->render([
            'billing' => $billing,
            'model'   => $model,
        ]);
    }

    /**
     * Исполняет функцию в контракте
     * @param int $id идентификатор счета
     *
     * @return string
     */
    public function actionCallFunctionAjax($id)
    {
        $billing = $this->getBill($id);
        $model = new \avatar\models\forms\CabinetBillsTokenContract(['billing' => $billing]);

        if (!$model->load(Yii::$app->request->post())) {
            return self::jsonErrorId(101, 'Не загружены данные');
        }
        if (!$model->validate()) {
            if (isset($model->errors['params'])) {
                return self::jsonErrorId(102, $model->paramErrors);
            }

            return self::jsonErrorId(103, $model->convert($model->errors));
        }
        $data = $model->action();

        return self::jsonSuccess($data);
    }

    /**
     * Исполняет функцию в контракте
     * @param int $id идентификатор счета
     *
     * @return string
     */
    public function actionCallFunctionExecuteAjax($id)
    {
        $billing = $this->getBill($id);
        $model = new \avatar\models\forms\CabinetBillsTokenContract(['billing' => $billing]);

        if (!$model->load(Yii::$app->request->post())) {
            return self::jsonErrorId(101, 'Не загружены данные');
        }
        if (!$model->validate()) {
            if (isset($model->errors['params'])) {
                return self::jsonErrorId(102, $model->errors);
            }

            return self::jsonErrorId(103, [
                'functionName' => $model->functionName,
                'params'       => $model->params,
            ]);
        }
        $data = $model->execute();

        return self::jsonSuccess($data);
    }

    /**
     * Формирует заявку на залог
     *
     * @return string
     */
    public function actionZalog()
    {
        $model = new \avatar\models\forms\CabinetBillsTokenZalog();

        if (!$model->load(Yii::$app->request->post(), '')) {
            return self::jsonErrorId(101, 'Не загружены данные');
        }
        if (!$model->validate()) {
            return self::jsonErrorId(102, $model->convert($model->errors));
        }
        $data = $model->action();

        return self::jsonSuccess($data);
    }

    /**
     * Показывает залог
     *
     * @param int $id идентификатор залога
     *
     * @return string
     *
     * @throws
     */
    public function actionZalogView($id)
    {
        $zalog = UserZalog::findOne($id);
        if (is_null($zalog)) {
            throw new Exception('Не найден залог');
        }
        if ($zalog->owner_user_id != Yii::$app->user->id) {
            throw new Exception('Не ваш залог');
        }
        try {
            $billing = UserBill::findOne($zalog->owner_billing_id);
        } catch (\Exception $e) {
            throw new Exception('Не найден счет залога');
        }

        return $this->render([
            'zalog'     => $zalog,
            'billing'   => $billing,
        ]);
    }

    /**
     * Получает счет ID
     * Делает проверку на хозяина
     *
     * @return \common\models\avatar\UserBill
     * @throws \yii\base\Exception
     */
    public function getBill($id = null)
    {
        if (is_null($id)) {
            $id = self::getParam('id');
        }
        $bill = UserBill::findOne($id);
        if ($bill->user_id != Yii::$app->user->id) {
            throw new \yii\base\Exception('Не ваш счет');
        }

        return $bill;
    }

    /**
     *
     * AJAX
     *
     * Отправляет деньги
     *
     * REQUEST:
     * + address - string - адрес может быть или целым адресом или в формате bitcoin:rtyertyertytr?i-am-avatar=
     * + amount - string - сколько (разделитель - точка)
     * - comment - string
     * + password - string
     * + billing_id - int - идентификатор счета отправителя
     *
     * @return Response
     */
    public function actionSend()
    {
        $model = new \avatar\models\forms\PiramidaSendEthToken();
        $model->load(Yii::$app->request->post(), '');
        if (!$model->validate()) {
            return self::jsonErrorId(101, $this->convert($model->errors));
        }
        $data = $model->prepare();
        if ($data === false) {
            return self::jsonErrorId(102, $this->convert($model->errors));
        }

        return self::jsonSuccess($data);
    }


    /**
     * AJAX
     *
     * Отправляет деньги
     *
     * REQUEST:
     * - address - string - биткойн адрес
     * - amount - string - сколько (разделитель - точка)
     * - comment - string
     * - password - string
     * - billing_id - int - идентификатор счета отправителя
     *
     * @return Response
     */
    public function actionSendConfirm()
    {
        $model = new \avatar\models\forms\PiramidaSendEthToken();
        $model->load(Yii::$app->request->post(), '');
        if (!$model->validate()) {
            return self::jsonErrorId(101, $this->convert($model->errors));
        }
        $transaction = $model->send();
        if ($transaction === false) {
            return self::jsonErrorId(102, $this->convert($model->errors));
        }

        return self::jsonSuccess([
            'address' => $transaction,
        ]);
    }

    /**
     * Конвертирует массив ошибок от формы чтобы их можно было выдать через JSON
     *
     * @param array $params
     *      [
     *      'amount' => ['Не верное число',...],
     *      ]
     *
     * @return array
     * [
     *      'name'  => 'amount',
     *      'value' => ['Не верное число',...],
     * ]
     */
    private function convert($params)
    {
        $ret = [];
        foreach ($params as $name => $arr) {
            $ret[] = [
                'name'  => $name,
                'value' => $arr,
            ];
        }
        return $ret;
    }

}
