<?php

namespace avatar\controllers;

use app\services\Subscribe;
use avatar\controllers\AdminBaseController;
use avatar\models\forms\BlogItem;
use avatar\models\forms\UserAvatar;
use cs\models\Tables\Client;
use cs\services\VarDumper;
use cs\web\Exception;
use Yii;
use yii\base\UserException;
use yii\filters\AccessControl;
use yii\helpers\Json;
use yii\helpers\Url;
use yii\httpclient\Client as ClientHttp;
use yii\web\Controller;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use app\models\NewsItem;
use app\models\Chenneling;
use cs\base\BaseController;
use yii\web\Response;
use yii\widgets\ActiveForm;
use app\models\SiteUpdate;

class AdminCurrencyController extends AdminBaseController
{
    public $type_id = 16;

    public function actionIndex()
    {
        return $this->render();
    }

    public function actionAdd()
    {
        $model = new \avatar\models\forms\Currency(['_school_id' => 2, '_type_id' => $this->type_id]);

        if ($model->load(Yii::$app->request->post()) && $model->insert()) {
            Yii::$app->session->setFlash('form', $model->id);
        }

        return $this->render([
            'model' => $model,
        ]);
    }


    public function actionEdit($id)
    {
        $model = \avatar\models\forms\Currency::findOne($id);
        $model->_school_id = 2;
        $model->_type_id = $this->type_id;

        if ($model->load(Yii::$app->request->post()) && $model->update()) {
            Yii::$app->session->setFlash('form', 1);
        }
        return $this->render([
            'model' => $model,
        ]);

    }

    public function actionDelete($id)
    {
        \avatar\models\forms\Currency::findOne($id)->delete();

        return self::jsonSuccess();
    }
}
