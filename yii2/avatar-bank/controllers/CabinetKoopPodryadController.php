<?php

namespace avatar\controllers;

use app\services\Subscribe;
use avatar\controllers\actions\DefaultAjax;
use avatar\controllers\AdminBaseController;
use avatar\models\forms\BlogItem;
use common\models\avatar\UserBill;
use common\models\investment\IcoRequest;
use common\models\investment\IcoRequestAddress;
use common\models\investment\IcoRequestSuccess;
use common\models\investment\Project;
use common\models\investment\ProjectIco;
use common\models\KoopPodryad;
use common\models\MapBankomat;
use common\models\PaySystemConfig;
use common\models\PaySystemListItem;
use common\models\school\School;
use cs\Application;
use cs\services\VarDumper;
use cs\web\Exception;
use Yii;
use yii\base\UserException;
use yii\filters\AccessControl;
use yii\helpers\StringHelper;
use yii\helpers\Url;
use yii\web\Controller;
use yii\filters\VerbFilter;
use cs\base\BaseController;
use yii\web\Response;
use yii\widgets\ActiveForm;

class CabinetKoopPodryadController extends CabinetSchoolBaseController
{
    public function actions()
    {
        return [

            'executor-search-ajax' => [
                'class' => '\avatar\controllers\actions\DefaultAjax',
                'model' => '\avatar\models\validate\CabinetKoopPodryadExecutorSearchAjax',
            ],
            'executor-add'         => [
                'class' => '\avatar\controllers\actions\DefaultAjax',
                'model' => '\avatar\models\validate\CabinetKoopPodryadExecutorAdd',
            ],
            'step2-finish'         => [
                'class' => '\avatar\controllers\actions\DefaultAjax',
                'model' => '\avatar\models\validate\CabinetKoopPodryadStep2Finish',
            ],

        ];
    }

    public function actionIndex($id)
    {
        $this->isAccess($id);
        $school = School::findOne($id);

        return $this->render(['school' => $school]);
    }

    public function actionStep1($id)
    {
        $this->isAccess($id);
        $school = School::findOne($id);
        $model = new \avatar\models\forms\CabinetKoopPodryadStep1([
            'school_id'   => $id,
        ]);

        if (Yii::$app->request->isPost) {
            if ($model->load(Yii::$app->request->post()) && $model->validate()) {
                $s = $model->save();
                return self::jsonSuccess($s);
            } else {
                return self::jsonErrorId(102, $model->getErrors102());
            }
        }

        return $this->render([
            'model'  => $model,
            'school' => $school,
        ]);
    }

    public function actionStep3($id)
    {
        $item = KoopPodryad::findOne($id);
        $this->isAccess($item->school_id);
        $school = School::findOne($item->school_id);

        $model = \avatar\models\forms\CabinetKoopPodryadStep3::findOne($id);
        $model->tz_hash = '1';

        if (Yii::$app->request->isPost) {
            if ($model->load(Yii::$app->request->post()) && $model->validate()) {
                $s = $model->save();
                return self::jsonSuccess($s);
            } else {
                return self::jsonErrorId(102, $model->getErrors102());
            }
        }

        return $this->render([
            'item'   => $item,
            'model'  => $model,
            'school' => $school,
        ]);
    }

    public function actionStep5($id)
    {
        $item = KoopPodryad::findOne($id);
        $this->isAccess($item->school_id);
        $school = School::findOne($item->school_id);

        $model = \avatar\models\forms\CabinetKoopPodryadStep5::findOne($id);

        if (Yii::$app->request->isPost) {
            if ($model->load(Yii::$app->request->post()) && $model->validate()) {
                $s = $model->save();
                return self::jsonSuccess($s);
            } else {
                return self::jsonErrorId(102, $model->getErrors102());
            }
        }

        return $this->render([
            'item'   => $item,
            'model'  => $model,
            'school' => $school,
        ]);
    }

    public function actionStep6($id)
    {
        $item = KoopPodryad::findOne($id);
        $this->isAccess($item->school_id);
        $school = School::findOne($item->school_id);

        $model = \avatar\models\forms\CabinetKoopPodryadStep6::findOne($id);

        if (Yii::$app->request->isPost) {
            if ($model->load(Yii::$app->request->post()) && $model->validate()) {
                $s = $model->save();
                return self::jsonSuccess($s);
            } else {
                return self::jsonErrorId(102, $model->getErrors102());
            }
        }

        return $this->render([
            'item'   => $item,
            'model'  => $model,
            'school' => $school,
        ]);
    }

    public function actionStep7($id)
    {
        $item = KoopPodryad::findOne($id);
        $this->isAccess($item->school_id);
        $school = School::findOne($item->school_id);

        $model = \avatar\models\forms\CabinetKoopPodryadStep7::findOne($id);

        if (Yii::$app->request->isPost) {
            if ($model->load(Yii::$app->request->post()) && $model->validate()) {
                $s = $model->save();
                return self::jsonSuccess($s);
            } else {
                return self::jsonErrorId(102, $model->getErrors102());
            }
        }

        return $this->render([
            'item'   => $item,
            'model'  => $model,
            'school' => $school,
        ]);
    }

    public function actionStep8($id)
    {
        $item = KoopPodryad::findOne($id);
        $this->isAccess($item->school_id);
        $school = School::findOne($item->school_id);

        $model = \avatar\models\forms\CabinetKoopPodryadStep8::findOne($id);

        if (Yii::$app->request->isPost) {
            if ($model->load(Yii::$app->request->post()) && $model->validate()) {
                $s = $model->save();
                return self::jsonSuccess($s);
            } else {
                return self::jsonErrorId(102, $model->getErrors102());
            }
        }

        return $this->render([
            'item'   => $item,
            'model'  => $model,
            'school' => $school,
        ]);
    }

    public function actionView($id)
    {
        $item = KoopPodryad::findOne($id);
        $this->isAccess($item->school_id);
        $school = School::findOne($item->school_id);

        return $this->render([
            'item'   => $item,
            'school' => $school,
        ]);
    }

    public function actionStep2($id)
    {
        $item = KoopPodryad::findOne($id);
        $this->isAccess($item->school_id);
        $school = School::findOne($item->school_id);

        return $this->render([
            'item'   => $item,
            'school' => $school,
        ]);
    }

    public function actionStep4($id)
    {
        $item = KoopPodryad::findOne($id);
        $this->isAccess($item->school_id);
        $school = School::findOne($item->school_id);

        return $this->render([
            'item'   => $item,
            'school' => $school,
        ]);
    }

    public function actionStep9($id)
    {
        $item = KoopPodryad::findOne($id);
        $this->isAccess($item->school_id);
        $school = School::findOne($item->school_id);

        return $this->render([
            'item'   => $item,
            'school' => $school,
        ]);
    }

}
