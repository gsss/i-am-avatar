<?php

namespace avatar\controllers;

use app\models\Article;
use app\models\SiteUpdate;
use app\services\Subscribe;
use common\components\sms\IqSms;
use common\models\Card;
use cs\base\BaseController;
use cs\services\UploadFolderDispatcher;
use cs\services\VarDumper;
use cs\web\Exception;
use Yii;
use yii\base\UserException;
use yii\db\Connection;
use yii\db\Query;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use yii\helpers\FileHelper;
use yii\web\Response;

class QrController extends \avatar\base\BaseController
{

    /**
     */
    public function actionEnter()
    {
        $model = new \avatar\models\forms\QrEnter();

        // Если передан параметр card то сразу устанавливается параметр карты
        $card = self::getParam('card');
        if (!is_null($card)) {
            /** @var \common\models\Card $cardObject */
            $cardObject = Card::findOne(['number' => $card]);
            if (!is_null($cardObject)) {
                if ($cardObject->user_id) {
                    throw new \cs\web\Exception('Карта уже привязана другому пользователю');
                } else {
                    $model->code = Card::convertNumberToSpace($cardObject->number);
                }
            }
        }

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $model->activate();
            Yii::$app->session->setFlash('form', 1);
        }

        return $this->render('@avatar/views/qr/enter', [
            'model' => $model,
        ]);
    }

}
