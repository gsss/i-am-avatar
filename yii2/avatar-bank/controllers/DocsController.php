<?php

namespace avatar\controllers;

use app\models\Article;
use app\models\SiteUpdate;
use app\services\Subscribe;
use common\models\avatar\UserBill;
use cs\base\BaseController;
use cs\services\UploadFolderDispatcher;
use cs\services\VarDumper;
use cs\web\Exception;
use Yii;
use yii\base\UserException;
use yii\db\Connection;
use yii\db\Query;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use yii\helpers\FileHelper;
use yii\web\Response;

class DocsController extends \avatar\base\BaseController
{
    public function actions()
    {
        $rows = [];
        $path = dir(Yii::getAlias('@avatar/views/docs'));
        while (($file = $path->read()) !== false){
            if (!in_array($file, ['.', '..'])) {
                $i = pathinfo($file);
                if ($i['extension'] == 'md') {
                    $rows[$i['filename']] = '\avatar\controllers\actions\DocsAction';
                }
            }
        }
        $path->close();
        return $rows;
    }
}
