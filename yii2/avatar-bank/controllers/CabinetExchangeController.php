<?php

namespace avatar\controllers;

use app\services\Subscribe;
use avatar\controllers\AdminBaseController;
use avatar\models\forms\BlogItem;
use common\models\avatar\Currency;
use common\models\avatar\UserBill;
use common\models\investment\IcoRequest;
use common\models\investment\IcoRequestAddress;
use common\models\investment\IcoRequestSuccess;
use common\models\investment\Project;
use common\models\investment\ProjectIco;
use common\models\MapBankomat;
use common\models\PaySystemConfig;
use common\models\PaySystemListItem;
use cs\Application;
use cs\services\VarDumper;
use cs\web\Exception;
use Yii;
use yii\base\UserException;
use yii\filters\AccessControl;
use yii\helpers\Json;
use yii\helpers\StringHelper;
use yii\helpers\Url;
use yii\httpclient\Client;
use yii\web\Controller;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use app\models\NewsItem;
use app\models\Chenneling;
use cs\base\BaseController;
use yii\web\Response;
use yii\widgets\ActiveForm;
use app\models\SiteUpdate;

class CabinetExchangeController extends \avatar\controllers\CabinetBaseController
{
    public function actionIndex()
    {
        $client = new Client(['baseUrl' => 'https://shapeshift.io']);
        $response = $client->get('getcoins')->send();
        $data = Json::decode($response->content);
        $shapeShiftAvailable = [];

        /**
         * {
         * "name": "FirstBlood",
         * "symbol": "1ST",
         * "image": "https://shapeshift.io/images/coins/firstblood.png",
         * "imageSmall": "https://shapeshift.io/images/coins-sm/firstblood.png",
         * "status": "available"
         * }
         */
        foreach ($data as $code => $currencyObject) {
            if ($currencyObject['status'] == 'available') $shapeShiftAvailable[$currencyObject['symbol']] = $currencyObject;
        }
        $avatarCurrency = Currency::find()->where(['is_avr_wallet' => 1])->all();

        $ret = [];
        foreach ($shapeShiftAvailable as $currencyObject) {
            if ($this->isExist($avatarCurrency, $currencyObject['symbol'])) $ret[] = $currencyObject['symbol'];
        }

        return $this->render([
            'shapeShiftAvailable' => $shapeShiftAvailable,
            'avatarCurrency'      => $avatarCurrency,
            'ret'                 => $ret,
        ]);
    }

    /**
     * Ищет
     *
     * @param \common\models\avatar\Currency[] $array
     * @param string $item код валюты
     *
     * @return bool
     */
    private function isExist($array, $item)
    {
        /** @var \common\models\avatar\Currency $currency */
        foreach ($array as $currency) {
            if ($currency->code == $item) return true;
        }

        return false;
    }

    public function actionIndexAjax()
    {
        /** @var int $id  идентификатор счета с которого будет происходить обмен */
        $id = self::getParam('id');
        $billing = UserBill::findOne($id);
        if ($billing->user_id != Yii::$app->user->id) {
            return self::jsonErrorId(101, 'Это не ваш счет');
        }

        $client = new Client(['baseUrl' => 'https://shapeshift.io']);
        $response = $client->get('getcoins')->send();
        $data = Json::decode($response->content);
        $shapeShiftAvailable = [];

        /**
         * {
         * "name": "FirstBlood",
         * "symbol": "1ST",
         * "image": "https://shapeshift.io/images/coins/firstblood.png",
         * "imageSmall": "https://shapeshift.io/images/coins-sm/firstblood.png",
         * "status": "available"
         * }
         */
        foreach ($data as $code => $currencyObject) {
            if ($currencyObject['status'] == 'available') $shapeShiftAvailable[$currencyObject['symbol']] = $currencyObject;
        }
        $avatarCurrency = Currency::find()->where(['is_avr_wallet' => 1])->all();

        $ret = [];
        foreach ($shapeShiftAvailable as $currencyObject) {
            if ($this->isExist($avatarCurrency, $currencyObject['symbol'])) $ret[] = $currencyObject['symbol'];
        }

        // выичтаю только те счета валюта которых равна на выбранном счете
        $selectedCurrencyCode = $billing->getCurrencyObject()->code;
        $curList = [];
        foreach ($ret as $code) {
            if ($code != $selectedCurrencyCode) $curList[] = $code;
        }

        return self::jsonSuccess(['html' => $this->renderAjax('index-ajax', ['curList' => $curList])]);
    }

}
