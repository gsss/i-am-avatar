<?php

namespace avatar\controllers;

use avatar\models\forms\UserAvatar;
use avatar\models\validate\CardNumberAccept;
use avatar\modules\ETH\ServiceEtherScan;
use avatar\modules\ETH\ServiceEthPlorer;
use common\components\Card;
use common\components\providers\BTC;
use common\components\providers\ETH;
use common\models\avatar\Currency;
use common\models\avatar\UserBill;
use common\models\blog\Article;
use common\models\Config;
use common\models\school\AdminLink;
use common\models\school\Kurs;
use common\models\school\Lesson;
use common\models\school\LessonPlace;
use common\models\school\LessonVideo;
use common\models\school\Master;
use common\models\school\NewsItem;
use common\models\school\School;
use common\models\task\Task;
use common\models\UserSeed;
use common\payment\BitCoinBlockTrailPayment;
use common\services\Security;
use console\controllers\MoneyRateController;
use cs\Application;
use cs\services\Str;
use cs\services\VarDumper;
use cs\web\Exception;
use cs\Widget\FileUpload4\ModelFields;
use Yii;
use yii\helpers\FileHelper;
use yii\helpers\Json;
use yii\helpers\StringHelper;
use yii\httpclient\Client;
use yii\imagine\Image;
use yii\web\NotFoundHttpException;
use yii\web\Response;

class CabinetSchoolNewsController extends CabinetSchoolBaseController
{
    public $type_id = 22;

    /** @var \common\models\school\NewsItem */
    public $item;

    public function behaviors()
    {
        return [
            'access' => [
                'class' => '\yii\filters\AccessControl',
                'rules' => [
                    [
                        'allow'         => true,
                        'matchCallback' => function ($rule, $action) {
                            if (in_array($action->id, ['add', 'index', 'header-footer'])) {
                                $school_id = Yii::$app->request->get('id');
                            } else if (in_array($action->id, ['edit', 'delete'])) {
                                $id = Yii::$app->request->get('id');
                                $i = NewsItem::findOne($id);
                                $this->item = $i;
                                $school_id = $i->school_id;
                            } else if (in_array($action->id, ['sign', 'register'])) {
                                $id = Yii::$app->request->post('id');
                                $i = NewsItem::findOne($id);
                                $this->item = $i;
                                $school_id = $i->school_id;
                            }
                            if (!Application::isInteger($school_id)) throw new \Exception('$school_id не целое');

                            $isAdmin = AdminLink::find()->where([
                                'user_id'   => Yii::$app->user->id,
                                'school_id' => $school_id,
                            ])->exists();

                            return $isAdmin;
                        },
                    ],
                ],
            ],
        ];
    }

    public function actions()
    {
        return [
            'sign' => [
                'class'    => '\avatar\controllers\actions\DefaultAjax',
                'model'    => '\avatar\models\validate\CabinetSchoolNewsSign',
                'formName' => '',
            ],
            'register' => [
                'class'    => '\avatar\controllers\actions\DefaultAjax',
                'model'    => '\avatar\models\validate\CabinetSchoolNewsRegister',
                'formName' => '',
            ],
        ];
    }

    /**
     * @param int $id school.id
     *
     * @return string
     */
    public function actionIndex($id)
    {
        $school = School::findOne($id);

        return $this->render(['school' => $school]);
    }

    /**
     * @param int $id school.id
     *
     * @return string
     */
    public function actionAdd($id)
    {
        $model = new \avatar\models\forms\school\NewsItem(['_school_id' => $id, '_type_id' => $this->type_id]);
        $school = School::findOne($id);

        if ($model->load(Yii::$app->request->post()) && $model->insert()) {
            Yii::$app->session->setFlash('form', 1);

            $i = NewsItem::findOne($model->id);
            $i->created_at = time();
            $i->school_id = $id;
            $i->date = date('Y-m-d');
            $i->id_string = Str::rus2translit($i->name);
            $i->save();
        }

        return $this->render([
            'model' => $model, 'school' => $school,
        ]);
    }

    /**
     * @param int $id school_news_article.id
     *
     * @return string
     */
    public function actionEdit($id)
    {
        $a = $this->item;
        $model = \avatar\models\forms\school\NewsItem::findOneWithInit($id, [
            '_school_id' => $a->school_id,
            '_type_id'   => $this->type_id,
        ]);
        $school = School::findOne($model->school_id);

        if ($model->load(Yii::$app->request->post()) && $model->update()) {
            Yii::$app->session->setFlash('form', 1);
        }

        return $this->render([
            'model' => $model, 'school' => $school,
        ]);
    }

    public function actionHeaderFooter($id)
    {
        $model = School::findOne($id);
        $school = $model;

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $model->save();
            Yii::$app->session->setFlash('form', 1);
        }

        return $this->render([
            'model'  => $model,
            'school' => $school,
        ]);
    }

    public function actionDelete($id)
    {
        $article = \avatar\models\forms\school\NewsItem::findOne($id);
        $article->_school_id = $article->school_id;
        $article->_type_id = $this->type_id;

        if (is_null($article)) return self::jsonErrorId(103, 'Не найдена статья');
        $article->delete();

        return self::jsonSuccess();
    }

}
