<?php

namespace avatar\controllers;

use app\models\Article;
use app\models\SiteUpdate;
use app\services\Subscribe;
use common\components\providers\ETH;
use common\models\avatar\UserBill;
use common\models\avatar\UserBillDefault;
use common\models\HD;
use common\models\HDtown;
use common\models\PassportLink;
use common\models\UserAvatar;
use common\models\UserDocument;
use cs\base\BaseController;
use cs\services\SitePath;
use cs\services\UploadFolderDispatcher;
use cs\services\VarDumper;
use cs\web\Exception;
use Yii;
use yii\base\UserException;
use yii\bootstrap\ActiveForm;
use yii\db\Connection;
use yii\db\Query;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use yii\helpers\FileHelper;
use yii\helpers\Json;
use yii\web\Response;

class CabinetMapController extends \avatar\controllers\CabinetBaseController
{


    /**
     */
    public function actionIndex()
    {
        $model = new \avatar\models\forms\UserMap();

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $row = $model->action();

            Yii::$app->session->setFlash('form', 1);
        }

        return $this->render([
            'model' => $model,
        ]);
    }


}
