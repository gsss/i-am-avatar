<?php

namespace avatar\controllers;

use avatar\models\forms\UserAvatar;
use avatar\models\validate\CardNumberAccept;
use avatar\modules\ETH\ServiceEtherScan;
use avatar\modules\ETH\ServiceEthPlorer;
use common\components\Card;
use common\components\providers\BTC;
use common\components\providers\ETH;
use common\models\avatar\Currency;
use common\models\avatar\UserBill;
use common\models\Config;
use common\models\UserSeed;
use common\payment\BitCoinBlockTrailPayment;
use common\services\Security;
use console\controllers\MoneyRateController;
use cs\services\VarDumper;
use cs\web\Exception;
use cs\Widget\FileUpload4\ModelFields;
use Yii;
use yii\helpers\FileHelper;
use yii\helpers\Json;
use yii\helpers\StringHelper;
use yii\httpclient\Client;
use yii\imagine\Image;
use yii\web\Response;

class CabinetBillsController extends \avatar\controllers\CabinetBaseController
{
    public static $cardList = [
        1  => [
            'preview'  => '@webroot/images/controller/cabinet-bills/cards/base/01.jpg',
            'original' => '@webroot/images/controller/cabinet-bills/cards/base/01.png',
            'qr'       => [
                'start'   => [798, 424],
                'size'    => 400,
                'padding' => 50,
            ],
        ],
        2  => [
            'preview'  => '@webroot/images/controller/cabinet-bills/cards/base/02.jpg',
            'original' => '@webroot/images/controller/cabinet-bills/cards/base/02.png',
            'qr'       => [
                'start'   => [756, 383],
                'size'    => 470,
                'padding' => 55,
            ],
        ],
        3  => [
            'preview'  => '@webroot/images/controller/cabinet-bills/cards/base/03.jpg',
            'original' => '@webroot/images/controller/cabinet-bills/cards/base/03.png',
        ],
        4  => [
            'preview'  => '@webroot/images/controller/cabinet-bills/cards/base/04.jpg',
            'original' => '@webroot/images/controller/cabinet-bills/cards/base/04.png',
            'qr'       => [
                'start'   => [260, 508],
                'size'    => 270,
                'padding' => 33,
            ],
        ],
        5  => [
            'preview'  => '@webroot/images/controller/cabinet-bills/cards/base/05.jpg',
            'original' => '@webroot/images/controller/cabinet-bills/cards/base/05.png',
        ],
        6  => [
            'preview'  => '@webroot/images/controller/cabinet-bills/cards/base/06.jpg',
            'original' => '@webroot/images/controller/cabinet-bills/cards/base/06.png',
            'qr'       => [
                'start'   => [144, 467],
                'size'    => 330,
                'padding' => 40,
            ],
        ],
        7  => [
            'preview'  => '@webroot/images/controller/cabinet-bills/cards/base/07.jpg',
            'original' => '@webroot/images/controller/cabinet-bills/cards/base/07.png',
            'qr'       => [
                'start'   => [146, 469],
                'size'    => 324,
                'padding' => 40,
            ],
        ],
        8  => [
            'preview'  => '@webroot/images/controller/cabinet-bills/cards/base/08.jpg',
            'original' => '@webroot/images/controller/cabinet-bills/cards/base/08.png',
        ],
        9  => [
            'preview'  => '@webroot/images/controller/cabinet-bills/cards/base/09.jpg',
            'original' => '@webroot/images/controller/cabinet-bills/cards/base/09.png',
            'qr'       => [
                'start'   => [144, 467],
                'size'    => 330,
                'padding' => 40,
            ],
        ],
        10 => [
            'preview'  => '@webroot/images/controller/cabinet-bills/cards/base/10.jpg',
            'original' => '@webroot/images/controller/cabinet-bills/cards/base/10.png',
            'qr'       => [
                'start'   => [260, 607],
                'size'    => 356,
                'padding' => 20,
            ],
        ],
    ];

    public function actions()
    {
        return [
            'transactions-list'       => [
                'class' => 'avatar\controllers\actions\CabinetBills\TransactionsList',
            ],
            'transactions-list-eth'   => [
                'class' => 'avatar\controllers\actions\CabinetBills\TransactionsListEth',
            ],
            'transactions-list-etc'   => [
                'class' => 'avatar\controllers\actions\CabinetBills\TransactionsListEtc',
            ],
            'transactions-list-token' => [
                'class' => 'avatar\controllers\actions\CabinetBills\TransactionsListToken',
            ],
        ];
    }

    /**
     * @return string
     */
    public function actionCardsGenerate()
    {
        return $this->render('cards-generate');
    }

    /**
     */
    public function actionIndex()
    {
        return $this->render();
    }

    /**
     * Показывает настройки счета
     *
     * REQUEST:
     * + id - int - идентификатор счета
     */
    public function actionSettings()
    {
        $billing = self::getBill();

        if ($billing->load(Yii::$app->request->post()) && $billing->validate()) {
            $billing->save();
            Yii::$app->session->setFlash('form', 1);

            return $this->refresh();
        } else {
            return $this->render([
                'model' => $billing,
            ]);
        }
    }

    /**
     * Выводит Страницу "Номер карты привязанной к счету"
     */
    public function actionCardNumber()
    {
        $billing = self::getBill();

        return $this->render('card-number', [
            'billing' => $billing,
        ]);
    }

    /**
     * Активирует карту
     * avatar-bank/docs/card-number.md
     *
     * REQUEST:
     * - name - string - название счета
     * - card_number - string - номер карты
     *
     * errors
     * 101, 'Не переданы данные'
     * 102, $model->errors
     */
    public function actionActivateCard()
    {
        $model = new \avatar\models\validate\ActivateCard();
        if (!$model->load(Yii::$app->request->post())) {
            return self::jsonErrorId(101, 'Не переданы данные');
        }
        if (!$model->validate()) {
            return self::jsonErrorId(102, $model->errors);
        }

        return self::jsonSuccess($model->action());
    }

    /**
     * Формирует десятку карт
     *
     * REQUEST:
     * + offset - int - смещение в массиве относительно 0 (нуля) с которого будет формироваться десятка карт
     * - file - string - полный путь к файлу, если файл не указываетс то используется болванка-шаблон
     * - row - int - строка в картах, если не передано то используется 0 по умолчанию
     *
     *
     * Очищает все WebHook которые старше $time (в сек)
     */
    public function actionBuild()
    {
        ini_set('memory_limit', -1);
        ini_set('max_execution_time', 900);
        $offset = self::getParam('offset', 0);
        $back = self::getParam('file', \Yii::getAlias('@console/base/1.png'));
        $row = self::getParam('row', 0);

        $data = \common\models\avatar\QrCode::find()
            ->innerJoin('user_bill', 'user_bill.id = bill_codes.billing_id')
            ->select('user_bill.address')
            ->limit(2)
            ->offset($offset)
            ->orderBy(['bill_codes.id' => SORT_DESC])
            ->column();

        $image = ['path' => $back];
        $rowHeight = 676;

        $c = $offset;
        $i = 0;
        foreach ($data as $address) {
            $o = $i % 2;
            $image = $this->watermark(
                $image['path'],
                [
                    [
                        'file'  => [
                            'format'  => 'png',
                            'content' => (new \Endroid\QrCode\QrCode())
                                ->setText('bitcoin:' . $address . '?i-am-avatar=true')
                                ->setSize(218)
                                ->setPadding(5)
                                ->setErrorCorrection('medium')
                                ->setForegroundColor(['r' => 0, 'g' => 0, 'b' => 0, 'a' => 0])
                                ->setBackgroundColor(['r' => 255, 'g' => 255, 'b' => 255, 'a' => 0])
                                ->setLabelFontSize(16)
                                ->setImageType(\Endroid\QrCode\QrCode::IMAGE_TYPE_PNG)
                                ->get('png'),
                        ],
                        'start' => [248 + ($o * 1126), 180 + ($row * $rowHeight)],
                    ],
                ]
            );
            $cs = str_repeat('0', 3 - strlen($c)) . $c;
            $image = $this->text(
                $image['path'],
                [
                    [
                        'text'        => $cs,
                        'start'       => [344 + ($o * 1126), 408 + ($row * $rowHeight)],
                        'fontFile'    => '@console/base/Bender.otf',
                        'fontOptions' => [
                            'size'  => 24,
                            'color' => '000',
                        ],
                    ],
                ]
            );
            $c++;
            $i++;
        }

        if ($row == 4) {
            $tempFilePath = \Yii::getAlias('@runtime/watermarkDone');
            FileHelper::createDirectory($tempFilePath);
            $background = $tempFilePath . DIRECTORY_SEPARATOR . time() . '_' . Yii::$app->security->generateRandomString(10) . '.' . 'png';
            copy($image['path'], $background);
            return self::jsonSuccess(['path' => $background]);
        }

        return self::jsonSuccess(['path' => $image['path']]);
    }


    /**
     * AJAX
     * Генерирует код карты
     *
     * @return string JSON array
     * [
     *      'number' => string
     * ]
     */
    public function actionCardNumberGenerate()
    {
        $cardNumber = Card::generateNumber();
        $cardNumber2 = substr($cardNumber, 0, 4) . ' ' . substr($cardNumber, 4, 4) . ' ' . substr($cardNumber, 8, 4) . ' ' . substr($cardNumber, 12, 4);

        return self::jsonSuccess([
            'number'     => $cardNumber,
            'numberText' => $cardNumber2,
        ]);
    }

    /**
     * AJAX
     * Генерирует код карты
     *
     * REQUEST:
     * - id - int - идентификатор счета
     * - number - string - номер счета, с пробелами, всего д.б. 16 цифр
     *
     * @see /avatar-bank/docs/bill-card-number.md
     *
     * @return string JSON array
     * 101 - Не переданы данные
     * 102 - ошибки валиджации
     */
    public function actionCardNumberAccept()
    {
        $model = new CardNumberAccept();
        if (!$model->load(Yii::$app->request->post(), '')) {
            return self::jsonErrorId(101, 'Не переданы данные');
        }
        if (!$model->validate()) {
            return self::jsonErrorId(102, $model->errors);
        }
        $model->billing->card_number = $model->number;
        $model->billing->save();

        return self::jsonSuccess();
    }

    /**
     */
    public function actionTransactions($id)
    {
        return $this->render('transactions', ['billing' => $this->getBill()]);
    }

    /**
     * Открывает форму и сохраняет SEEDS
     */
    public function actionSaveSeeds()
    {
        $model = new \avatar\models\forms\CabinetBillsSaveSeeds();

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $seeds = $model->action();
            Yii::$app->session->set('seeds', $seeds);

            return $this->redirect(['cabinet-bills/save-seeds2']);
        }

        return $this->render(['model' => $model]);
    }

    /**
     * Сохраняет SEEDS
     */
    public function actionSaveSeeds2()
    {
        return $this->render();
    }

    /**
     * GET:
     * + id - int - идентификатор счета
     */
    public function actionTransactionsEth($id)
    {
        $billing = $this->getBill($id);
        if ($billing->currency != Currency::ETH) {
            throw new Exception('Не тот код валюты');
        }

        return $this->render(['billing' => $billing]);
    }

    /**
     * GET:
     * + id - int - идентификатор счета
     */
    public function actionTransactionsEtc($id)
    {
        $billing = $this->getBill($id);
        if ($billing->currency != Currency::ETC) {
            throw new Exception('Не тот код валюты');
        }

        return $this->render(['billing' => $billing]);
    }

    /**
     * GET:
     * + id - int - идентификатор счета
     */
    public function actionTransactionsToken($id)
    {
        return $this->render(['billing' => $this->getBill($id)]);
    }

    /**
     * Показывает карты которые можно скачать
     *
     */
    public function actionCards()
    {
        return $this->render(['billing' => $this->getBill()]);
    }

    /**
     * Возвращает баланс в счете
     * AJAX
     * REQUEST:
     * - id - int - идентификатор счета
     *
     * @return string json array
     * [
     *      '<confirmed>',   (BTC)
     *      '<unconfirmed>', (BTC)
     *      '<confirmed>',   (валюта установленная в качетсве просмотра)
     *      '<unconfirmed>', (валюта установленная в качетсве просмотра)
     *      '<currencyView>', установленный параметр просмотра валюты, если 0 значит нет и 3 и 4 параметры можно не
     *      учитывать
     * ]
     */
    public function actionGetBalance2()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        if (YII_ENV_DEV) return self::jsonSuccess([0, 0]);
        $bill = $this->getBill();
        $wallet = $bill->getWallet();
        $balance = $wallet->getBalance();

        $currencyView = Yii::$app->user->identity->currency_view;
        if (is_null($currencyView)) $currencyView = 0;
        $confirmed = 0;
        $unconfirmed = 0;
        switch ($currencyView) {
            case 1: // RUB
                $rate = Config::get(MoneyRateController::RATE_BTC_RUB);
                $confirmed = Yii::$app->formatter->asDecimal($rate * ($balance[0] / 100000000), 2);
                $unconfirmed = Yii::$app->formatter->asDecimal($rate * ($balance[1] / 100000000), 2);
                break;
            case 2: // USD
                $rate = Config::get(MoneyRateController::RATE_BTC_USD);
                $confirmed = Yii::$app->formatter->asDecimal($rate * ($balance[0] / 100000000), 2);
                $unconfirmed = Yii::$app->formatter->asDecimal($rate * ($balance[1] / 100000000), 2);
                break;
            default:
                $confirmed = Yii::$app->formatter->asDecimal($balance[0] / 100000000, 8);
                $unconfirmed = Yii::$app->formatter->asDecimal($balance[1] / 100000000, 8);
                break;

        }

        return self::jsonSuccess(
            [
                $balance[0] / 100000000,
                $balance[1] / 100000000,
                $confirmed,
                $unconfirmed,
                $currencyView,
            ]
        );
    }

    /**
     * Возвращает баланс в счете
     * AJAX
     * REQUEST:
     * - id - int - идентификатор счета
     *
     * @return string json array
     * [
     *      '<confirmed>',   (BTC)
     *      '<unconfirmed>', (BTC)
     *      '<confirmed>',   (валюта установленная в качетсве просмотра)
     *      '<unconfirmed>', (валюта установленная в качетсве просмотра)
     *      '<currencyView>', установленный параметр просмотра валюты, если 0 значит нет и 3 и 4 параметры можно не
     *      учитывать
     * ]
     */
    public function actionGetBalance()
    {
        $id = self::getParam('id');
        $bill = $this->getBill($id);
        switch ($bill->currency) {
            case Currency::BTC:
                return $this->actionBalanceInternal($id);
            case Currency::ETH:
                return $this->actionBalanceInternalEth($id);
            case Currency::ETC:
                return $this->actionBalanceInternalEtc($id);
            default:
                return $this->actionBalanceInternalToken($id);
        }
    }

    /**
     * Возвращает баланс в счете ETH
     * AJAX
     * REQUEST:
     * - id - int - идентификатор счета
     *
     * @return string json array
     * {
         * address: # address,
         * ETH: {   # ETH specific information
             * balance:  # ETH balance
             * totalIn:  # Total incoming ETH value
             * totalOut: # Total outgoing ETH value
         * },
         * contractInfo: {  # exists if specified address is a contract
             * creatorAddress:  # contract creator address,
             * transactionHash: # contract creation transaction hash,
             * timestamp:       # contract creation timestamp
         * },
         * tokenInfo:  # exists if specified address is a token contract address (same format as token info),
         * tokens: [   # exists if specified address has any token balances
             * {
             * tokenInfo: # token data (same format as token info),
                 * balance:   # token balance (as is, not reduced to a floating point value),
                 * totalIn:   # total incoming token value
                 * totalOut:  # total outgoing token value
             * },
             * ...
         * ],
         * countTxs:    # Total count of incoming and outcoming transactions (including creation one),
     * }
     */
    public function actionGetBalance3()
    {
        $id = self::getParam('id');
        $bill = $this->getBill($id);
        $provider = new ServiceEthPlorer();
        $response = $provider->get('getAddressInfo/' . $bill->address);

        return self::jsonSuccess($response);
    }

    /**
     * Конвертирует массив ошибок от формы чтобы их можно было выдать через JSON
     *
     * @param array $params
     *      [
     *      'amount' => ['Не верное число',...],
     *      ]
     *
     * @return array
     * [
     *      'name'  => 'amount',
     *      'value' => ['Не верное число',...],
     * ]
     */
    private function convert($params)
    {
        $ret = [];
        foreach ($params as $name => $arr) {
            $ret[] = [
                'name'  => $name,
                'value' => $arr,
            ];
        }
        return $ret;
    }

    /**
     *
     * AJAX
     *
     * Отправляет деньги
     *
     * REQUEST:
     * + address - string - адрес может быть или целым адресом или в формате bitcoin:rtyertyertytr?i-am-avatar=
     * + amount - string - сколько (разделитель - точка)
     * - comment - string
     * + password - string
     * + billing_id - int - идентификатор счета отправителя
     *
     * @return Response
     */
    public function actionSend()
    {
        $model = new \avatar\models\forms\PiramidaSend();
        $model->load(Yii::$app->request->post(), '');
        if (!$model->validate()) {
            return self::jsonErrorId(101, $this->convert($model->errors));
        }

        return self::jsonSuccess($model->prepare());
    }

    /**
     * AJAX
     * Отправляет деньги
     * REQUEST:
     * - address - string - биткойн адрес
     * - amount - string - сколько (разделитель - точка)
     * - comment - string
     * - password - string
     * - billing_id - int - идентификатор счета отправителя
     *
     * @return Response
     */
    public function actionSendConfirm()
    {
        $model = new \avatar\models\forms\PiramidaSend();
        $model->load(Yii::$app->request->post(), '');
        if (!$model->validate()) {
            return self::jsonErrorId(101, $this->convert($model->errors));
        }
        $transaction = $model->send();
        if ($transaction === false) {
            return self::jsonErrorId(102, $this->convert($model->errors));
        }

        return self::jsonSuccess([
            'address' => $transaction,
        ]);
    }

    /**
     * Скачивает карту с установленным QR кодом
     * REQUEST:
     * - billing_id - int - идентификатор счета user_bill.id
     * - card - int - идентификатор картинки
     *
     * @return Response файл для скачивания
     */
    public function actionCardDownload()
    {
        ini_set("memory_limit", "1000M");

        $billing = self::getBill(self::getParam('billing_id'));
        $card = self::getParam('card');
        $cardObject = self::$cardList[$card];
        if (!isset($cardObject['qr'])) {
            return Yii::$app->response->sendContentAsFile(file_get_contents(Yii::getAlias($cardObject['original'])), 'card_' . $billing->address . '.' . 'png');
        }

        $file = $cardObject['original'];
        $qr = $cardObject['qr'];
        $filePath = Yii::getAlias($file);

        $devImage = file_get_contents(Yii::getAlias('@webroot/images/controller/cabinet-bills/cards/transaction.png'));

        $image = $this->watermark(
            $filePath,
            [
                [
                    'file'  => [
                        'format'  => 'png',
                        'content' => (YII_ENV_DEV) ? $devImage : (new \Endroid\QrCode\QrCode())
                            ->setText('bitcoin:' . $billing->address . '?i-am-avatar=true')
                            ->setSize($qr['size'])
                            ->setPadding($qr['padding'])
                            ->setErrorCorrection('medium')
                            ->setForegroundColor(['r' => 0, 'g' => 0, 'b' => 0, 'a' => 0])
                            ->setBackgroundColor(['r' => 255, 'g' => 255, 'b' => 255, 'a' => 0])
                            ->setLabelFontSize(16)
                            ->setImageType(\Endroid\QrCode\QrCode::IMAGE_TYPE_PNG)
                            ->get('png'),
                    ],
                    'start' => $qr['start'],
                ],
            ]
        );

        return Yii::$app->response->sendContentAsFile(file_get_contents($image['path']), 'card_' . $billing->address . '.' . 'png');
    }


    /**
     * @param \Imagine\Image\ImageInterface $file
     * @param array                         $watermark
     *          [[
     *          'file' => string - полный путь | array [
     *          'format'  => string - расширение возможно jpg png
     *          'content' => string - содержимое
     *          ]
     *          'start' => array [x, y] - точка от верхнего левого угла оригинальной картинки
     *          ],...]
     *
     * @return \Imagine\Image\ImageInterface
     *
     * @throws \yii\base\Exception
     */
    private function watermark2($image, $watermark)
    {
        if (count($watermark) == 0) {
            throw new Exception('Не задан массив $watermark');
        }
        $tempFilePath = \Yii::getAlias('@runtime/watermark');
        FileHelper::createDirectory($tempFilePath);
        foreach ($watermark as $watermarkConfig) {
            if (is_array($watermarkConfig['file'])) {
                $temp = $tempFilePath . DIRECTORY_SEPARATOR . time() . '_' . Yii::$app->security->generateRandomString(10) . '.' . $watermarkConfig['file']['format'];
                file_put_contents($temp, $watermarkConfig['file']['content']);
            } else {
                $temp = $watermarkConfig['file'];
            }
            try {
                $image = Image::watermark($image, $temp, $watermarkConfig['start']);
            } catch (\Exception $e) {
                VarDumper::dump($image);
            }
        }

        return $image;
    }

    /**
     * @param \Imagine\Image\ImageInterface $file
     * @param array                         $watermark
     *      [[
     *      'text'     => string
     *      'start'    => array [x, y] - точка от верхнего левого угла оригинальной картинки
     *      'fontOptions' => string
     *      'fontFile' => string
     *      ],...]
     *
     * @return \Imagine\Image\ImageInterface
     *
     * @throws \yii\base\Exception
     */
    private function text2($image, $watermark)
    {
        if (count($watermark) == 0) {
            throw new Exception('Не задан массив $watermark');
        }
        $tempFilePath = Yii::getAlias('@runtime/watermark');
        FileHelper::createDirectory($tempFilePath);
        foreach ($watermark as $watermarkConfig) {
            if (!isset($watermarkConfig['fontOptions'])) {
                $watermarkConfig['fontOptions'] = [];
            }
            if (!isset($watermarkConfig['fontOptions'])) {
                $watermarkConfig['start'] = [0, 0];
            }

            $image = Image::text($image, $watermarkConfig['text'], $watermarkConfig['fontFile'], $watermarkConfig['start'], $watermarkConfig['fontOptions']);
        }

        return $image;
    }

    /**
     * @param string $file полный путь к файлу
     * @param array  $watermark
     *                     [[
     *                     'file' => string - полный путь | array [
     *                     'format'  => string - расширение возможно jpg png
     *                     'content' => string - содержимое
     *                     ]
     *                     'start' => array [x, y] - точка от верхнего левого угла оригинальной картинки
     *                     ],...]
     *
     * @return array
     * [
     *      'img'  => \Imagine\Image\ImageInterface
     *      'path' => string - полный путь к файлу png
     * ]
     *
     * @throws \yii\base\Exception
     */
    private function watermark($file, $watermark)
    {
        if (count($watermark) == 0) {
            throw new Exception('Не задан массив $watermark');
        }
        $tempFilePath = \Yii::getAlias('@runtime/watermark');
        FileHelper::createDirectory($tempFilePath);
        $background = $file;
        $image = null;
        foreach ($watermark as $watermarkConfig) {
            if (is_array($watermarkConfig['file'])) {
                $temp = $tempFilePath . DIRECTORY_SEPARATOR . time() . '_' . Yii::$app->security->generateRandomString(10) . '.' . $watermarkConfig['file']['format'];
                file_put_contents($temp, $watermarkConfig['file']['content']);
            } else {
                $temp = $watermarkConfig['file'];
            }
            $image = Image::watermark($background, $temp, $watermarkConfig['start']);
            $background = $tempFilePath . DIRECTORY_SEPARATOR . time() . '_' . Yii::$app->security->generateRandomString(10) . '.' . 'png';
            file_put_contents($background, $image->get('png'));
        }

        return [
            'img'  => $image,
            'path' => $background,
        ];
    }

    /**
     * @param string $file полный путь к файлу
     * @param array  $watermark
     *                     [[
     *                     'text'     => string
     *                     'start'    => array [x, y] - точка от верхнего левого угла оригинальной картинки
     *                     'fontOptions' => string
     *                     'fontFile' => string
     *                     ],...]
     *
     * @return array
     * [
     *      'img'  => \Imagine\Image\ImageInterface
     *      'path' => string - полный путь к файлу png
     * ]
     *
     * @throws \yii\base\Exception
     */
    private function text($file, $watermark)
    {
        if (count($watermark) == 0) {
            throw new Exception('Не задан массив $watermark');
        }
        $tempFilePath = Yii::getAlias('@runtime/watermark');
        FileHelper::createDirectory($tempFilePath);
        $background = $file;
        $image = null;
        foreach ($watermark as $watermarkConfig) {
            if (!isset($watermarkConfig['fontOptions'])) {
                $watermarkConfig['fontOptions'] = [];
            }
            if (!isset($watermarkConfig['fontOptions'])) {
                $watermarkConfig['start'] = [0, 0];
            }

            $image = Image::text($background, $watermarkConfig['text'], $watermarkConfig['fontFile'], $watermarkConfig['start'], $watermarkConfig['fontOptions']);
            $background = $tempFilePath . DIRECTORY_SEPARATOR . time() . '_' . Yii::$app->security->generateRandomString(10) . '.' . 'png';
            file_put_contents($background, $image->get('png'));
        }

        return [
            'img'  => $image,
            'path' => $background,
        ];
    }

    /**
     * AJAX
     * Создает кошелек
     *
     * REQUEST:
     * + name            string - название счета
     * - currency        int - валюта Currency::BTC,ETH, по умолчанию BTC
     * - password        string - пароль
     * - password_type   int - 2 - пароль от кабинета, 1 - открытый пароль
     *
     * @return string
     * ошибки
     * 101, Не переданы данные
     * 102, ошибки формы
     * 103, ошибка ETH сервера Invalid JSON RPC response
     * 104, ошибка
     */
    public function actionNew()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $model = new \avatar\models\validate\CabinetBillsControllerNew();
        if (!$model->load(Yii::$app->request->post(), '')) {
            return self::jsonErrorId(101, 'Не переданы данные');
        }
        if (!$model->validate()) {
            return self::jsonErrorId(102, $this->convert($model->errors));
        }
        try {
            $billing = $model->action();
        } catch (\Exception $e) {
            if (StringHelper::startsWith($e->getMessage(), 'Invalid JSON RPC response')) {
                return self::jsonErrorId(103, $e->getMessage());
            } else {
                return self::jsonErrorId(104, $e->getMessage());
            }
        }

        return self::jsonSuccess([
            'id'      => $billing->id,
            'address' => $billing->address,
        ]);
    }

    /**
     * AJAX
     * Добавляет токен
     *
     * REQUEST:
     * + name            string - название счета
     * + token_id        int - идентификатор токена
     * + password        string - пароль
     * + billing_id      int - идентификатор счета ETH для которого будет привязан токен
     * - password_type   int - 2 - пароль от кабинета, 3 - пароль индивидуальный от кошелька. По умолчанию - 2
     */
    public function actionNewToken()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $model = new \avatar\models\validate\CabinetBillsControllerNewToken();
        if (!$model->load(Yii::$app->request->post(), '')) {
            return self::jsonErrorId(101, 'Не переданы данные');
        }
        if (!$model->validate()) {
            return self::jsonErrorId(102, $this->convert($model->errors));
        }
        $wallet = $model->action();

        return self::jsonSuccess([
            'id'      => $wallet->billing->id,
            'address' => $wallet->billing->address,
        ]);
    }

    /**
     * AJAX
     * REQUEST:
     *
     * @return string json response
     * [
     *      'confirmedBalance'   => BTC
     *      'unconfirmedBalance' => BTC
     *      'all'                => mBTC
     * ]
     */
    public function actionBalanceInternal($id)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        if (YII_ENV_DEV) return self::jsonSuccess([
            'confirmedBalance'   => 0,
            'unconfirmedBalance' => 0,
            'all'                => 0,
        ]);
        $provider = new BitCoinBlockTrailPayment();
        $client = $provider->getClient();
        $billing = $this->getBill($id);
        $w = $client->getWalletBalance($billing->identity);
        $confirmedBalance = $w['confirmed'];

        /** @var \common\models\UserAvatar $user */
        $user = Yii::$app->user->identity;
        $currencyView = $user->currency_view;
        if (is_null($currencyView)) $currencyView = 0;
        $confirmed = $confirmedBalance / 100000000;

        $params = [
            'confirmedBalance'   => $confirmedBalance / 100000000,
            'confirmed'          => $confirmed,
            'currencyView'       => $currencyView,
            'currencyString'     => 'BTC',

            'coins'              => $confirmedBalance  / 100000000,
            'coinsFormated'      => Yii::$app->formatter->asDecimal($confirmedBalance / 100000000, 8),
        ];

        switch ($currencyView) {
            case 1: // RUB
                $confirmedConverted = Currency::convertBySettings($confirmedBalance / 100000000, 'BTC');

                $confirmed = Yii::$app->formatter->asDecimal($confirmedConverted, 2);

                $params['currencyString'] = 'RUB';
                $params['convert'] = $confirmedConverted;
                $params['convertFormated'] = $confirmed;
                break;
            case 2: // USD
                $confirmedConverted = Currency::convertBySettings($confirmedBalance / 100000000, 'BTC');

                $confirmed = Yii::$app->formatter->asDecimal($confirmedConverted, 2);

                $params['currencyString'] = 'USD';
                $params['convert'] = $confirmedConverted;
                $params['convertFormated'] = $confirmed;
                break;
        }

        return self::jsonSuccess($params);
    }

    /**
     * Выдает кол-во денег на счету
     *
     * AJAX
     * REQUEST:
     *
     * @return string json response
     * [
     *      'confirmed'             => string ETH
     *      'confirmedConverted'    => string              - не обязательное, если currencyView > 0 то присутствует
     *      'currencyView'          => int
     *      'currencyString'        => string например 'ETH'/'RUB'
     * ]
     */
    public function actionBalanceInternalEth($id)
    {
        if (YII_ENV_DEV) return self::jsonSuccess([
            'confirmedBalance' => 0,
        ]);
        if (!YII_ENV_PROD) {
            return $this->actionBalanceInternalEthTest($id);
        } else {
            return $this->actionBalanceInternalEthProd($id);
        }
    }

    /**
     * Выдает кол-во денег на счету
     *
     * AJAX
     * REQUEST:
     *
     * @return string json response
     * [
     *      'confirmed'             => string ETH
     *      'confirmedConverted'    => string              - не обязательное, если currencyView > 0 то присутствует
     *      'currencyView'          => int
     *      'currencyString'        => string например 'ETH'/'RUB'
     * ]
     */
    public function actionBalanceInternalEthProd($id)
    {
        if (YII_ENV_DEV) return self::jsonSuccess([
            'confirmedBalance' => 0,
        ]);

        $billing = $this->getBill($id);
        $provider = new ServiceEthPlorer();
        /**
         * Возвращает баланс в счете ETH
         * AJAX
         * REQUEST:
         * - id - int - идентификатор счета
         *
         * @return string json array
         * {
         * address: # address,
         * ETH: {   # ETH specific information
         * balance:  # ETH balance
         * totalIn:  # Total incoming ETH value
         * totalOut: # Total outgoing ETH value
         * },
         * contractInfo: {  # exists if specified address is a contract
         * creatorAddress:  # contract creator address,
         * transactionHash: # contract creation transaction hash,
         * timestamp:       # contract creation timestamp
         * },
         * tokenInfo:  # exists if specified address is a token contract address (same format as token info),
         * tokens: [   # exists if specified address has any token balances
         * {
         * tokenInfo: # token data (same format as token info),
         * balance:   # token balance (as is, not reduced to a floating point value),
         * totalIn:   # total incoming token value
         * totalOut:  # total outgoing token value
         * },
         * ...
         * ],
         * countTxs:    # Total count of incoming and outcoming transactions (including creation one),
         * }
         */
        $response = $provider->get('getAddressInfo/' . $billing->address);

        /** @var \avatar\models\forms\UserAvatar $user */
        $user = Yii::$app->user->identity;
        $currencyView = $user->currency_view;
        if (is_null($currencyView)) $currencyView = 0;
        $confirmedBalance = $response['ETH']['balance'];

        if ($currencyView > 0) {
            $currency = Currency::findOne($currencyView);
            $currencyString = $currency->code;
            $confirmedConverted = Currency::convertBySettings($confirmedBalance, 'ETH', false);

            $return = [
                'confirmed'          => $confirmedBalance,
                'currencyView'       => $currencyView,
                'currencyString'     => $currencyString,
                'unconfirmed'        => 0,
                'confirmedConverted' => $confirmedConverted,

                'coins'           => $confirmedBalance,
                'coinsFormated'   => Yii::$app->formatter->asDecimal($confirmedBalance, 8),
                'convert'         => $confirmedConverted,
                'convertFormated' => Yii::$app->formatter->asDecimal($confirmedConverted, 2),
            ];
        } else {
            $return = [
                'confirmed'      => Yii::$app->formatter->asDecimal($confirmedBalance, 18),
                'currencyView'   => $currencyView,
                'currencyString' => 'ETH',
                'unconfirmed'    => 0,

                'coins'         => $confirmedBalance,
                'coinsFormated' => Yii::$app->formatter->asDecimal($confirmedBalance, 8),
            ];
        }
        $response['avatarNetwork'] = $return;

        return self::jsonSuccess($response);
    }

    /**
     * Выдает кол-во денег на счету
     *
     * AJAX
     * REQUEST:
     *
     * @return string json response
     * [
     *      'confirmed'             => string ETH
     *      'confirmedConverted'    => string              - не обязательное, если currencyView > 0 то присутствует
     *      'currencyView'          => int
     *      'currencyString'        => string например 'ETH'/'RUB'
     * ]
     */
    public function actionBalanceInternalEthTest($id)
    {
        $billing = $this->getBill($id);
        $provider = new ServiceEtherScan();
        $data = $provider->getBalance($billing->address);

        $balance = $data[0]['balance'] / pow(10, 18);

        /** @var \avatar\models\forms\UserAvatar $user */
        $user = Yii::$app->user->identity;
        $currencyView = $user->currency_view;
        if (is_null($currencyView)) $currencyView = 0;
        $confirmedBalance = $balance;

        if ($currencyView > 0) {
            $currency = Currency::findOne($currencyView);
            $currencyString = $currency->code;
            $confirmedConverted = Currency::convertBySettings($confirmedBalance, 'ETH', false);

            $return = [
                'confirmed'          => $confirmedBalance,
                'currencyView'       => $currencyView,
                'currencyString'     => $currencyString,
                'unconfirmed'        => 0,
                'confirmedConverted' => $confirmedConverted,

                'coins'           => $confirmedBalance,
                'coinsFormated'   => Yii::$app->formatter->asDecimal($confirmedBalance, 8),
                'convert'         => $confirmedConverted,
                'convertFormated' => Yii::$app->formatter->asDecimal($confirmedConverted, 2),
            ];
        } else {
            $return = [
                'confirmed'      => Yii::$app->formatter->asDecimal($confirmedBalance, 18),
                'currencyView'   => $currencyView,
                'currencyString' => 'ETH',
                'unconfirmed'    => 0,

                'coins'         => $confirmedBalance,
                'coinsFormated' => Yii::$app->formatter->asDecimal($confirmedBalance, 8),
            ];
        }
        $response['avatarNetwork'] = $return;

        return self::jsonSuccess($response);
    }

    /**
     * Выдает кол-во денег на счету
     *
     * AJAX
     * REQUEST:
     *
     * @return string json response
     * [
     *      'confirmed'             => string ETH
     *      'confirmedConverted'    => string              - не обязательное, если currencyView > 0 то присутствует
     *      'currencyView'          => int
     *      'currencyString'        => string например 'ETC','RUB'
     * ]
     */
    public function actionBalanceInternalEtc($id)
    {
        if (YII_ENV_DEV) return self::jsonSuccess([
            'confirmedBalance' => 0,
        ]);
        $billing = $this->getBill($id);
        $walletEtc = $billing->getWalletETC();
        $confirmedBalance = $walletEtc->getBalance();

        /** @var \avatar\models\forms\UserAvatar $user */
        $user = Yii::$app->user->identity;
        $currencyView = $user->currency_view;
        if (is_null($currencyView)) $currencyView = 0;

        if ($currencyView > 0) {
            $currency = Currency::findOne($currencyView);
            $currencyString = $currency->code;

            $return = [
                'confirmed'      => $confirmedBalance,
                'currencyView'   => $currencyView,
                'currencyString' => $currencyString,
                'unconfirmed'    => 0,
            ];

            $return['confirmedConverted'] = Currency::convertBySettings($confirmedBalance, 'ETC', true);
            $return['confirmed'] = $return['confirmedConverted'];
        } else {
            $return = [
                'confirmed'      => $confirmedBalance,
                'currencyView'   => $currencyView,
                'currencyString' => 'ETC',
                'unconfirmed'    => 0,
            ];

            $return['confirmed'] = Yii::$app->formatter->asDecimal($confirmedBalance, 18);
        }

        return self::jsonSuccess($return);
    }

    /**
     * Выдает кол-во денег на счету для токена
     *
     * AJAX
     * REQUEST:
     *
     * @return string json response
     * [
     *      'confirmed'             => string кол-во монет токенов с десятичными
     *      'confirmedConverted'    => string              - не обязательное, если currencyView > 0 то присутствует
     *      'currencyView'          => int
     *      'currencyString'        => string например 'ETH'/'RUB'
     * ]
     */
    public function actionBalanceInternalToken($id)
    {
        if (YII_ENV_DEV) return self::jsonSuccess([
            'confirmedBalance' => 0,
        ]);
        $billing = $this->getBill($id);
        $currency = $billing->currency;
        $currencyObject = Currency::findOne($currency);
        $token = $currencyObject->getToken();

        $balanceInt = $token->getProvider()->getBalance($billing->address);
        Yii::info('token=' .$token->address. ' address = ' . $billing->address. ' balance='. $balanceInt , 'avatar\controllers\CabinetBillsController::actionBalanceInternalToken');
        $decimals = pow(10, $currencyObject->decimals);
        $confirmedBalance = $balanceInt / $decimals;

        /** @var \common\models\UserAvatar $user */
        $user = Yii::$app->user->identity;
        $currencyView = $user->currency_view;
        $return = [
            'confirmed'          => Yii::$app->formatter->asDecimal($confirmedBalance, 2),
            'confirmedConverted' => Yii::$app->formatter->asDecimal($confirmedBalance, $currencyObject->decimals),
            'currencyView'       => $currencyView,
            'currencyString'     => $currencyObject->code,
            'unconfirmed'        => 0,
        ];

        return self::jsonSuccess($return);
    }

    public function ajaxActions()
    {
        return [
            'close',
            'password-type2',
        ];
    }

    /**
     * Закрывает счет
     *
     * AJAX
     * REQUEST:
     * - id - int идентификатор счета
     */
    public function actionClose()
    {
        $bill = $this->getBill();
        $bill->mark_deleted = 1;
        $bill->save();
        if (YII_ENV_PROD) {
            switch ($bill->currency) {
                case Currency::BTC:
//                    $wallet = $bill->getWalletBTC();
//                    $ret = $wallet->blockTrailWallet->deleteWallet();
//                    Yii::info(\yii\helpers\VarDumper::dumpAsString($ret), 'avatar\actionClose\deleteWallet\result');
                    break;
                case Currency::ETH:
                    break;
            }
        }

        return self::jsonSuccess();
    }

    /**
     * Шифрует кошельки под пароль на тип 2 и генерирует SEEDs если не было
     *
     * AJAX
     * REQUEST:
     * + password - string - пароль от кабинета
     *
     */
    public function actionPasswordType2()
    {
        $model = new \avatar\models\validate\CabinetBillsControllerActionPasswordType2();

        if (!$model->load(\Yii::$app->request->post(), '')) {
            return self::jsonErrorId(101, 'Не переданы данные');
        }
        if (!$model->validate()) {
            return self::jsonErrorId(102, $model->errors);
        }
        $ret = $model->action();
        $is_seeds_exist = $ret[0];
        if ($is_seeds_exist == 0) {
            Yii::$app->session->set('seeds', $ret[1]);
        }

        return self::jsonSuccess([
            'is_seeds_exist' => $is_seeds_exist,
        ]);
    }

    /**
     * Выдает на скачивание документ PDF с ключами для восстановления пароля
     *
     * REQUEST:
     * - нет
     */
    public function actionPasswordTypePdf()
    {
        // получаю SEEDS
        $seeds = Yii::$app->session->get('seeds');
        if (is_null($seeds)) throw new \Exception('Нет seeds');

        // готовлю HTML для PDF
        $filePath = \Yii::getAlias('@avatar/views/cabinet-bills/_seeds-pdf.php');
        $contentHtml = \Yii::$app->view->renderFile($filePath, [
            'seeds' => $seeds,
            'user'  => Yii::$app->user->identity,
        ]);

        // формирую PDF из HTML
        require \Yii::getAlias('@common/components/mpdf60/mpdf.php');
        $pdfDriver = new \mPDF();
        $pdfDriver->WriteHTML($contentHtml);
        $contentPdf = $pdfDriver->Output('', 'S');

        // выдаю на скачивание
        return Yii::$app->response->sendContentAsFile($contentPdf, 'SeedsAvatarBank' . '.pdf', [
            'mimeType' => 'application/pdf',
        ]);
    }

    /**
     * Получает счет ID
     * Делает проверку на хозяина
     *
     * @return \common\models\avatar\UserBill
     * @throws \yii\base\Exception
     */
    public function getBill($id = null)
    {
        if (is_null($id)) {
            $id = self::getParam('id');
        }
        $bill = UserBill::findOne($id);
        if ($bill->user_id != Yii::$app->user->id) {
            throw new \yii\base\Exception('Не ваш счет');
        }

        return $bill;
    }

}
