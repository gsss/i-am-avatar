<?php

namespace avatar\controllers;

use app\common\components\Piramida;
use app\models\Article;
use app\models\Form\Shop\Order;
use app\models\Piramida\WalletSource\Custom;
use app\models\Piramida\WalletSource\Pochta;
use app\models\Piramida\WalletSource\Yandex;
use app\models\Shop;
use app\models\Shop\Product;
use app\models\Shop\Request;
use app\models\Shop\RequestMessage;
use app\models\Shop\RequestProduct;
use app\models\Union;
use app\models\User;
use app\modules\Shop\services\Basket;
use common\models\school\AdminLink;
use common\models\school\School;
use cs\Application;
use cs\services\VarDumper;
use cs\web\Exception;
use Yii;
use yii\base\UserException;
use yii\bootstrap\Html;
use yii\db\Query;


/**
 * Обслуживает действия магазина
 *
 * Class Cabinet_shop_shopController
 * @package app\controllers
 */
class CabinetSchoolShopGoodsImagesController extends \avatar\controllers\CabinetSchoolBaseController
{
    public $type_id = 26;

    public $school_id;

    /** @var  \common\models\shop\Product */
    public $product;

    public function behaviors()
    {
        return [
            'access' => [
                'class' => '\yii\filters\AccessControl',
                'rules' => [
                    [
                        'allow'         => true,
                        'matchCallback' => function ($rule, $action) {
                            if (in_array($action->id, ['add', 'index'])) {

                                $product_id = Yii::$app->request->get('id');
                                if (is_null($product_id)) {
                                    throw new \Exception('Не указан параметр ID');
                                }
                                $product = \common\models\shop\Product::findOne($product_id);
                                if (is_null($product)) {
                                    throw new \Exception('Не найден product');
                                }

                                $school_id = $product->school_id;
                                $this->school_id = $school_id;
                                $this->product = $product;

                            } else if (in_array($action->id, ['edit', 'delete'])) {

                                $image_id = Yii::$app->request->get('id');
                                if (is_null($image_id)) {
                                    throw new \Exception('Не указан параметр ID');
                                }
                                $image = \common\models\shop\ProductImage::findOne($image_id);

                                if (is_null($image)) {
                                    throw new \Exception('Не найден image');
                                }
                                $product = \common\models\shop\Product::findOne($image->product_id);
                                if (is_null($product)) {
                                    throw new \Exception('Не найден product');
                                }
                                $school_id = $product->school_id;
                                $this->school_id = $school_id;
                                $this->product = $product;

                            }
                            if (!Application::isInteger($this->school_id)) throw new \Exception('$school_id не целое');

                            $isAdmin = AdminLink::find()->where([
                                'user_id'   => Yii::$app->user->id,
                                'school_id' => $this->school_id,
                            ])->exists();

                            return $isAdmin;
                        },
                    ],
                ],
            ],
        ];
    }

    /**
     *
     * @param int $id gs_unions_shop_product.id
     */
    public function actionIndex($id)
    {
        $school = School::findOne($this->school_id);

        return $this->render([
            'school'  => $school,
            'product' => $this->product,
        ]);
    }

    /**
     * Добавление картинки
     *
     * @param int $id gs_unions_shop_product.id
     *
     * @return string|\yii\web\Response
     * @throws \cs\web\Exception
     */
    public function actionAdd($id)
    {
        $model = new \avatar\models\forms\shop\ProductImage([
            '_school_id' => $this->school_id,
            '_type_id'   => $this->type_id,
            'product_id' => $this->product->id,
        ]);
        $school = School::findOne($this->school_id);

        if (Yii::$app->request->isPost) {
            if ($model->load(Yii::$app->request->post()) && $model->validate()) {
                return self::jsonSuccess($model->save());
            } else {
                return self::jsonErrorId(102, $model->getErrors102());
            }
        }
        return $this->render([
            'model'   => $model,
            'school'  => $school,
            'product' => $this->product,
        ]);
    }

    /**
     * Редактирование товара
     *
     * @param int $id идентификатор товара gs_unions_shop_product.id
     *
     * @return string|\yii\web\Response
     *
     * @throws
     */
    public function actionEdit($id)
    {
        $model = \avatar\models\forms\shop\ProductImage::findOne($id);
        $model->_school_id = $this->school_id;
        $model->_type_id = $this->type_id;
        $school = School::findOne($this->school_id);

        if (Yii::$app->request->isPost) {
            if ($model->load(Yii::$app->request->post()) && $model->validate()) {
                return self::jsonSuccess($model->save());
            } else {
                return self::jsonErrorId(102, $model->getErrors102());
            }
        }

        return $this->render([
            'model'  => $model,
            'school' => $school,
            'product' => $this->product,
        ]);
    }

    public function actionDelete($id)
    {
        $i = \avatar\models\forms\shop\ProductImage::findOne($id);
        if (is_null($i)) throw new \Exception('Не найден ProductImage');

        $i->delete();

        return self::jsonSuccess();
    }

}
