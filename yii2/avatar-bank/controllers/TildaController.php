<?php

namespace avatar\controllers;

use aki\telegram\Telegram;
use app\models\Article;
use app\models\SiteUpdate;
use avatar\models\forms\BlogItem;
use avatar\models\forms\Contact;
use avatar\services\LogReader;
use common\components\providers\ETH;
use common\components\sms\IqSms;
use common\models\avatar\Currency;
use common\models\avatar\UserBill;
use common\models\CompanyCustomizeItem;
use common\models\NewsItem;
use common\models\PaymentBitCoin;
use common\models\school\Page;
use common\models\school\PotokUser3Link;
use common\models\school\School;
use common\models\school\TildaPlugin;
use common\models\UserAvatar;
use common\models\UserRegistration;
use common\models\UserTelegramConnect;
use common\models\UserTelegramTemp;
use common\payment\BitCoinBlockTrailPayment;
use cs\Application;
use cs\base\BaseController;
use cs\services\Security;
use cs\services\UploadFolderDispatcher;
use cs\services\VarDumper;
use cs\web\Exception;
use Yii;
use yii\base\UserException;
use yii\db\Connection;
use yii\db\Query;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use yii\helpers\FileHelper;
use yii\helpers\Json;
use yii\helpers\Url;
use yii\httpclient\Client;
use yii\web\ForbiddenHttpException;
use yii\web\Response;

class TildaController extends \avatar\base\BaseController
{
    public $enableCsrfValidation = false;

    /**
     */
    public function actionCallBack()
    {
        Yii::info(Yii::$app->request->get(), 'avatar\controllers\TildaController::actionCallBack');

        if (count(Yii::$app->request->get()) == 0) {
            return 'ok';
        }
        $projectId = Yii::$app->request->get('projectid');
        $pageId = Yii::$app->request->get('pageid');
        $publickey = Yii::$app->request->get('publickey');
        $settings = TildaPlugin::findOne(['publickey']);

        $page = Page::findOne(['tilda_page_id' => $pageId]);
        if (is_null($page)) {
            return 'ok';
        }
        $options = TildaPlugin::findOne(['school_id' => $page->school_id]);
        $client = $options->getProviderTilda();
        $data = $client->call('get', 'v1/getpagefullexport', ['pageid' => $pageId]);
        $school = School::findOne($page->school_id);
        $prefix = '';
        if ($school->is_super_company) {
            $prefix = '@webroot';
        } else {
            $prefix = '@school/web';
        }
        $path = $prefix.'/tilda/' . $page->school_id . '/images';

        // Сохраняю картинки
        FileHelper::createDirectory(Yii::getAlias($path));
        foreach ($data['images'] as $i){
            $content = file_get_contents($i['from']);
            $name = $i['to'];
            $path = $prefix . '/tilda/' . $page->school_id . '/images/' . $name;
            file_put_contents(Yii::getAlias($path), $content);
        }
        // Сохраняю js
        $path = $prefix . '/tilda/' . $page->school_id . '/js';
        FileHelper::createDirectory(Yii::getAlias($path));
        foreach ($data['js'] as $i){
            $content = file_get_contents($i['from']);
            $name = $i['to'];
            $path = $prefix . '/tilda/' . $page->school_id . '/js/' . $name;
            file_put_contents(Yii::getAlias($path), $content);
        }
        // Сохраняю css
        $path = $prefix . '/tilda/' . $page->school_id . '/css';
        FileHelper::createDirectory(Yii::getAlias($path));
        foreach ($data['css'] as $i){
            $content = file_get_contents($i['from']);
            $name = $i['to'];
            $path = $prefix . '/tilda/' . $page->school_id . '/css/' . $name;
            file_put_contents(Yii::getAlias($path), $content);
        }
        $path = $prefix . '/tilda/' . $page->school_id . '/html';
        FileHelper::createDirectory(Yii::getAlias($path));
        $path = $prefix . '/tilda/' . $page->school_id . '/html/' . 'file' . $page->id . '.' . 'html';
        file_put_contents(Yii::getAlias($path), $data['html']);

        return 'ok';
    }


}
