<?php

namespace avatar\controllers;

use avatar\modules\UniSender\UniSender;
use common\components\providers\ETH;
use common\models\avatar\Currency;
use common\models\avatar\UserBill;
use common\models\avatar\UserBillDefault;
use common\models\HD;
use common\models\HDtown;
use common\models\school\PotokUser3Link;
use common\models\school\School;
use common\models\subscribe\UserSubscribe;
use common\models\UserAvatar;
use common\models\UserDocument;
use common\models\UserMaster;
use common\models\UserTelegramConnect;
use common\models\UserTelegramTemp;
use common\services\Security\AES;
use cs\base\BaseController;
use cs\services\SitePath;
use cs\services\UploadFolderDispatcher;
use cs\services\VarDumper;
use cs\web\Exception;
use Yii;
use yii\base\UserException;
use yii\bootstrap\ActiveForm;
use yii\db\Connection;
use yii\db\Query;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use yii\helpers\FileHelper;
use yii\helpers\StringHelper;
use yii\web\Response;

class CabinetTelegramController extends \avatar\controllers\CabinetBaseController
{

    /**
     */
    public function actionIndex()
    {
        return $this->renderFrontend();
    }

    /**
     * Присоединяет telegram_username и telegram_chat_id
     */
    public function actionPin()
    {
        $messages = Yii::$app->params['telegram_root']['messages'];

        /** @var \common\models\UserAvatar $user */
        $user = Yii::$app->user->identity;

        $temp = UserTelegramTemp::findOne(['email' => $user->email]);
        $connect = UserTelegramConnect::findOne(['username' => $temp->username]);
        if (!is_null($connect)) {
            $connect->delete();
        }
        $user->telegram_username = $temp->username;
        $user->telegram_chat_id = $temp->chat_id;
        $user->save();

        /** @var \aki\telegram\Telegram $telegram */
        $telegram = Yii::$app->telegram;

        $telegram->sendMessage(['chat_id' => $user->telegram_chat_id, 'text' => $messages['link']]);
        $temp->delete();

        return self::jsonSuccess();
    }

    /**
     * Отсоединяет telegram_username и telegram_chat_id
     */
    public function actionUnPin()
    {
        $messages = Yii::$app->params['telegram_root']['messages'];

        /** @var \common\models\UserAvatar $user */
        $user = Yii::$app->user->identity;

        /** @var \aki\telegram\Telegram $telegram */
        $telegram = Yii::$app->telegram;
        $telegram->sendMessage(['chat_id' => $user->telegram_chat_id, 'text' => $messages['unpin']]);

        $user->telegram_username = null;
        $user->telegram_chat_id = null;
        $user->save();

        return self::jsonSuccess();
    }

}
