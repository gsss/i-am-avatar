<?php

namespace avatar\controllers;

use app\models\Article;
use app\models\SiteUpdate;
use app\services\Subscribe;
use avatar\models\forms\Contact;
use avatar\services\LogReader;
use common\components\sms\IqSms;
use common\models\avatar\Currency;
use common\models\avatar\UserBill;
use common\models\PaymentBitCoin;
use common\models\Server;
use common\models\UserAvatar;
use common\models\UserDocument;
use common\models\UserRegistration;
use common\payment\BitCoinBlockTrailPayment;
use cs\Application;
use cs\base\BaseController;
use cs\services\UploadFolderDispatcher;
use cs\services\VarDumper;
use cs\web\Exception;
use Yii;
use yii\base\UserException;
use yii\db\Connection;
use yii\db\Query;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use yii\helpers\FileHelper;
use yii\helpers\Json;
use yii\helpers\Url;
use yii\httpclient\Client;
use yii\web\Response;

class DevelopmentController extends \avatar\base\BaseController
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow'   => true,
                        'roles'   => ['role_developer'],
                    ],
                ],
            ],
        ];
    }

    /**
     */
    public function actionIndex()
    {
        return $this->render();
    }

    /**
     * Выдает размер оставшегося места на сервере
     *
     * @param $id
     * @return Response
     */
    public function actionGetSize($id)
    {
        $server = Server::findOne($id);
        $url = $server->api;
        if ($server->api) {
            $url = $url . '/site/disk';
            $client = new Client();
            $response = $client->get($url)->send();
            if ($response->headers['http-code'] != 200) {
                return self::jsonErrorId(502, 'Не отвечает сервер');
            }
            try {
                $data = Json::decode($response->content);
            } catch (\Exception $e) {
                return self::jsonErrorId(400, 'Json error');
            }

            return self::jsonSuccess($data);
        } else {
            return self::jsonErrorId(401, 'Не указан API');
        }

    }

    /**
     * Выдает статус доступности сервера
     *
     * REQUEST:
     * - period - int - кол-во часов
     *
     * @param $id
     *
     *
     * @return Response
     * [
     *  [
     *     'time'   => int
     *     'status' => int
     *  ], ...
     * ]
     */
    public function actionGetStatus($id)
    {
        $period = 24 * 7 * 2;
        $period = self::getParam('period', $period);
        $server = Server::findOne($id);
        if (is_null($server)) return self::jsonErrorId(101, 'не найден сервер');
        $rows = \common\models\statistic\PingStatus::find()
            ->select([
                'time',
                'status',
            ])
            ->where(['server_id' => $server->id])
            ->andWhere(['between', 'time', time() - 60 * 60 * $period, time()])
            ->all();

        return self::jsonSuccess(['rows' => $rows, 'server' => $server]);
    }
}
