<?php

namespace avatar\controllers;

use avatar\models\forms\UserAvatar;
use avatar\models\validate\CardNumberAccept;
use common\components\Card;
use common\components\providers\BTC;
use common\components\providers\ETH;
use common\models\avatar\Currency;
use common\models\avatar\UserBill;
use common\models\Config;
use common\models\Contract;
use common\models\UserSeed;
use common\payment\BitCoinBlockTrailPayment;
use common\services\Security;
use console\controllers\MoneyRateController;
use cs\services\VarDumper;
use cs\web\Exception;
use Yii;
use yii\helpers\FileHelper;
use yii\helpers\Json;
use yii\helpers\StringHelper;
use yii\imagine\Image;
use yii\web\Response;

class CabinetBillsEthController extends \avatar\controllers\CabinetBaseController
{

    /**
     * Вызывает функцию в контракте
     *
     * @param int $id идентификатор счета
     *
     * @return string
     */
    public function actionExportKey($id)
    {
        $billing = $this->getBill($id);
        $model = new \avatar\models\forms\CabinetBillsEthExportKey(['billing' => $billing]);

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            return $model->action();
        }

        return $this->render([
            'billing' => $billing,
            'model'   => $model,
        ]);
    }

    /**
     * @param int $id идентификатор счета
     *
     * @return string
     */
    public function actionExportJson($id)
    {
        $billing = $this->getBill($id);
        $model = new \avatar\models\forms\CabinetBillsEthExportJson(['billing' => $billing]);

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            return $model->action();
        }

        return $this->render([
            'billing' => $billing,
            'model'   => $model,
        ]);
    }

    /**
     * Вызывает функцию в контракте
     *
     * @param int $id идентификатор счета
     *
     * @return string
     */
    public function actionContract($id)
    {
        $billing = $this->getBill($id);

        return $this->render([
            'billing' => $billing,
        ]);
    }

    /**
     * Выдает курс Эфира
     *
     * @param int $id идентификатор валюты
     *
     * @return string
     */
    public function actionKurs()
    {
        $rows = \common\models\ChartPoint::find()
            ->select([
                'usd',
                'time',
            ])
            ->where(['>', 'time', time() - (60 * 60 * 24 * 30)])
            ->andWhere(['currency_id' => \common\models\avatar\Currency::ETH])
            ->all()
        ;

        return self::jsonSuccess([
            'rows'     => $rows,
            'currency' => [
                'code' => 'USD',
            ],
        ]);
    }

    /**
     * Вызывает функцию в контракте
     *
     * @return string
     */
    public function actionGetMax()
    {
        $model = new \avatar\models\forms\CabinetBillsEthGetMax();

        if (!$model->load(Yii::$app->request->post(), '')) {
            return self::jsonErrorId(400, 'Не загружены данные');
        }

        if (!$model->validate()) {
            return self::jsonErrorId(102, $this->convert($model->errors));
        }

        return self::jsonSuccess($model->action());
    }


    /**
     */
    public function actionImportJson()
    {
        $model = new \avatar\models\forms\CabinetBillsImportJson();

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $model->action();
            Yii::$app->session->setFlash('form', 1);
        }

        return $this->render([
            'model' => $model,
        ]);
    }

    /**
     */
    public function actionImportPrivateKey()
    {
        $model = new \avatar\models\forms\CabinetBillsImportJson();

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $model->action();
            Yii::$app->session->setFlash('form', 1);
        }

        return $this->render([
            'model' => $model,
        ]);
    }

    /**
     * Получает счет ID
     * Делает проверку на хозяина
     *
     * @return \common\models\avatar\UserBill
     * @throws \yii\base\Exception
     */
    public function getBill($id = null)
    {
        if (is_null($id)) {
            $id = self::getParam('id');
        }
        $bill = UserBill::findOne($id);
        if ($bill->user_id != Yii::$app->user->id) {
            throw new \yii\base\Exception('Не ваш счет');
        }

        return $bill;
    }

    /**
     *
     * AJAX
     *
     * Отправляет деньги
     *
     * REQUEST:
     * + address - string - адрес может быть или целым адресом или в формате bitcoin:rtyertyertytr?i-am-avatar=
     * + amount - string - сколько (разделитель - точка)
     * - comment - string
     * + password - string
     * + billing_id - int - идентификатор счета отправителя
     *
     * @return Response
     */
    public function actionSend()
    {
        $model = new \avatar\models\forms\PiramidaSendEth();
        $model->load(Yii::$app->request->post(), '');
        if (!$model->validate()) {
            return self::jsonErrorId(101, $this->convert($model->errors));
        }

        return self::jsonSuccess($model->prepare());
    }

    /**
     * AJAX
     * Отправляет деньги
     * REQUEST:
     * - address - string - биткойн адрес
     * - amount - string - сколько (разделитель - точка)
     * - comment - string
     * - password - string
     * - billing_id - int - идентификатор счета отправителя
     *
     * @return Response
     */
    public function actionSendConfirm()
    {
        $model = new \avatar\models\forms\PiramidaSendEth();
        $model->load(Yii::$app->request->post(), '');
        if (!$model->validate()) {
            return self::jsonErrorId(101, $this->convert($model->errors));
        }
        $transaction = $model->send();
        if ($transaction === false) {
            return self::jsonErrorId(102, $this->convert($model->errors));
        }

        return self::jsonSuccess([
            'address' => $transaction,
        ]);
    }

    /**
     * Конвертирует массив ошибок от формы чтобы их можно было выдать через JSON
     *
     * @param array $params
     *      [
     *      'amount' => ['Не верное число',...],
     *      ]
     *
     * @return array
     * [
     *      'name'  => 'amount',
     *      'value' => ['Не верное число',...],
     * ]
     */
    private function convert($params)
    {
        $ret = [];
        foreach ($params as $name => $arr) {
            $ret[] = [
                'name'  => $name,
                'value' => $arr,
            ];
        }
        return $ret;
    }


    /**
     * Вызывает функцию в контракте
     *
     * @param int $id идентификатор  контракта contract.id
     * @param int $billing_id идентификатор счета
     *
     * @return string
     */
    public function actionCallFunction($id, $billing_id)
    {
        $billing = $this->getBill($billing_id);
        $model = new \avatar\models\forms\CabinetBillsEthContract();
        $contract = Contract::findOne($id);

        return $this->render([
            'billing'   => $billing,
            'model'     => $model,
            'contract'  => $contract,
        ]);
    }

    /**
     * Запрашивает комиссию на исполнение контракта
     *
     * @param int $id идентификатор  контракта contract.id
     * @param int $billing_id идентификатор счета
     *
     * @return string
     */
    public function actionCallFunctionAjax($id, $billing_id)
    {
        $billing = $this->getBill($billing_id);
        $contract = Contract::findOne($id);
        $model = new \avatar\models\forms\CabinetBillsEthContract(['billing' => $billing, 'contract' => $contract]);

        if (!$model->load(Yii::$app->request->post())) {
            return self::jsonErrorId(101, 'Не загружены данные');
        }
        if (!$model->validate()) {
            if (isset($model->errors['params'])) {
                return self::jsonErrorId(102, $model->paramErrors);
            }

            return self::jsonErrorId(103, $model->convert($model->errors));
        }
        $data = $model->action();

        return self::jsonSuccess($data);
    }

    /**
     * Исполняет функцию в контракте
     * @param int $id идентификатор  контракта contract.id
     * @param int $billing_id идентификатор счета
     *
     * @return string
     */
    public function actionCallFunctionExecuteAjax($id, $billing_id)
    {
        $billing = $this->getBill($billing_id);
        $contract = Contract::findOne($id);
        $model = new \avatar\models\forms\CabinetBillsEthContract(['billing' => $billing, 'contract' => $contract]);

        if (!$model->load(Yii::$app->request->post())) {
            return self::jsonErrorId(101, 'Не загружены данные');
        }
        if (!$model->validate()) {
            if (isset($model->errors['params'])) {
                return self::jsonErrorId(102, $model->errors);
            }

            return self::jsonErrorId(103, [
                'functionName' => $model->functionName,
                'params'       => $model->params,
            ]);
        }
        $data = $model->execute();

        return self::jsonSuccess($data);
    }

}
