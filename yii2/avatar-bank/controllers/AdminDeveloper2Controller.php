<?php

namespace avatar\controllers;

use app\models\Article;
use app\models\SiteUpdate;
use app\services\Subscribe;
use cs\services\VarDumper;
use cs\web\Exception;
use Yii;
use yii\base\UserException;

class AdminDeveloper2Controller extends AdminBaseController
{
    public function actions()
    {
        $rows = [];
        $path = dir(Yii::getAlias('@avatar/views/admin-developer2'));
        while (($file = $path->read()) !== false){
            if (!in_array($file, ['.', '..'])) {
                $i = pathinfo($file);
                if ($i['extension'] == 'md') {
                    $rows[$i['filename']] = '\avatar\controllers\actions\AdminDeveloper2Action';
                }
            }
        }
        $path->close();

        return $rows;
    }
}
