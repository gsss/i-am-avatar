<?php

namespace avatar\controllers;

use avatar\models\forms\school\CommandLink;
use avatar\models\forms\UserAvatar;
use avatar\models\validate\CabinetSchoolTaskListControllerHide;
use avatar\models\validate\CardNumberAccept;
use avatar\modules\ETH\ServiceEtherScan;
use avatar\modules\ETH\ServiceEthPlorer;
use common\components\Card;
use common\components\providers\BTC;
use common\components\providers\ETH;
use common\models\avatar\Currency;
use common\models\avatar\UserBill;
use common\models\comment\Comment;
use common\models\comment\CommentList;
use common\models\Config;
use common\models\piramida\Wallet;
use common\models\school\AdminLink;
use common\models\school\Kurs;
use common\models\school\Lesson;
use common\models\school\Page;
use common\models\school\PageBlockContent;
use common\models\school\ProjectManagerLink;
use common\models\school\School;
use common\models\task\Category;
use common\models\task\Session;
use common\models\task\Task;
use common\models\UserSeed;
use common\models\UserWallet;
use common\payment\BitCoinBlockTrailPayment;
use common\services\Security;
use common\services\Subscribe;
use console\controllers\MoneyRateController;
use cs\Application;
use cs\services\Url;
use cs\services\VarDumper;
use cs\web\Exception;
use cs\Widget\FileUpload4\ModelFields;
use Yii;
use yii\base\Event;
use yii\helpers\ArrayHelper;
use yii\helpers\FileHelper;
use yii\helpers\Html;
use yii\helpers\Json;
use yii\helpers\StringHelper;
use yii\httpclient\Client;
use yii\imagine\Image;
use yii\web\ForbiddenHttpException;
use yii\web\NotFoundHttpException;
use yii\web\Request;
use yii\web\Response;

class CabinetSchoolTaskListActionsController extends CabinetSchoolBaseController
{


    public function actions()
    {
        return [
            'view-start'               => [
                'class' => '\avatar\controllers\actions\CabinetSchoolTaskListController\actionViewStart',
            ],
            'view-ajax'                => [
                'class' => '\avatar\controllers\actions\DefaultAjax',
                'model' => '\avatar\models\validate\CabinetSchoolTaskListViewAjax',
            ],
            'view-done-change-time'      => [
                'class' => '\avatar\controllers\actions\DefaultAjax',
                'model' => '\avatar\models\validate\CabinetSchoolTaskListDoneChangeTime',
            ],
            'view-done'                => [
                'class' => '\avatar\controllers\actions\CabinetSchoolTaskListController\actionViewDone',
            ],
            'view-reject'              => [
                'class' => '\avatar\controllers\actions\CabinetSchoolTaskListController\actionViewReject',
            ],
            'view-finish'              => [
                'class' => '\avatar\controllers\actions\CabinetSchoolTaskListController\actionViewFinish',
            ],
            'view-finish-reject'       => [
                'class' => '\avatar\controllers\actions\CabinetSchoolTaskListController\actionViewFinishReject',
            ],
            'session-start'            => [
                'class' => '\avatar\controllers\actions\CabinetSchoolTaskListController\actionSessionStart',
            ],
            'session-finish'           => [
                'class' => '\avatar\controllers\actions\CabinetSchoolTaskListController\actionSessionFinish',
            ],
            'view-close-session-start' => [
                'class' => '\avatar\controllers\actions\DefaultAjax',
                'model' => '\avatar\models\validate\CabinetSchoolTaskListControllerViewCloseSessionStart',
            ],

            'budjet-search-ajax' => [
                'class' => '\avatar\controllers\actions\DefaultAjax',
                'model' => '\avatar\models\validate\CabinetSchoolTaskListBudjetSearchAjax',
            ],

            'executor-search-ajax' => [
                'class' => '\avatar\controllers\actions\DefaultAjax',
                'model' => '\avatar\models\validate\CabinetSchoolTaskListExecutorSearchAjax',
            ],
            'executor-search-ajax2' => [
                'class' => '\avatar\controllers\actions\DefaultAjax',
                'model' => '\avatar\models\validate\CabinetSchoolTaskListExecutorSearchAjax2',
            ],
            'executor-add'         => [
                'class' => '\avatar\controllers\actions\DefaultAjax',
                'model' => '\avatar\models\validate\CabinetSchoolTaskListAddExecutor',
            ],

            'helper-search-ajax' => [
                'class' => '\avatar\controllers\actions\DefaultAjax',
                'model' => '\avatar\models\validate\CabinetSchoolTaskListHelperSearchAjax',
            ],
            'helper-add'         => [
                'class' => '\avatar\controllers\actions\DefaultAjax',
                'model' => '\avatar\models\validate\CabinetSchoolTaskListHelperAdd',
            ],

            'ajail-task' => [
                'class' => '\avatar\controllers\actions\DefaultAjax',
                'model' => '\avatar\models\validate\CabinetSchoolTaskListAjailTask',
            ],
            'sort'       => [
                'class' => '\avatar\controllers\actions\DefaultAjax',
                'model' => '\avatar\models\validate\CabinetSchoolTaskListSort',
            ],
        ];
    }

    /**
     * @return string
     */
    public function actionExport($id, $search)
    {
        $model = new \avatar\models\validate\CabinetSchoolTaskListExport();

        if ($model->load(Yii::$app->request->get(), '') && $model->validate()) {
            return $model->action();
        }

        return self::jsonErrorId(102, $model->errors);
    }

    /**
     * @param int $id school_task.id
     *
     * @return string
     * @throws ForbiddenHttpException
     * @throws NotFoundHttpException
     */
    public function actionView($id)
    {
        $model = Task::findOne($id);
        $school = School::findOne($model->school_id);

        return $this->render([
            'model'  => $model,
            'school' => $school,
        ]);
    }

    /**
     * - item_id - идентификатор задачи
     * - list_id - идентификатор статуса
     *
     * @return string
     */
    public function actionSet()
    {
        $item_id = self::getParam('item_id');
        $list_id = self::getParam('list_id');
        $Task = Task::findOne($item_id);
        $Task->status = $list_id;
        $Task->save();

        return self::jsonSuccess();
    }

    /**
     * Скрывает задачу
     *
     * @return string
     */
    public function actionHide($id)
    {
        $model = new CabinetSchoolTaskListControllerHide();
        if (!$model->load(['id' => $id], '')) {
            return self::jsonErrorId(400, 'Не загружены данные');
        }
        if (!$model->validate()) {
            return self::jsonErrorId(102, $model->errors);
        }
        $model->action();

        return self::jsonSuccess();
    }

    public function actionAddHelper()
    {
        $model = new \avatar\models\validate\CabinetSchoolTaskListControllerAddHelper();

        if (!$model->load(Yii::$app->request->post(), '')) {
            return self::jsonErrorId(400, 'Не загружены данные');
        }
        if (!$model->validate()) {
            return self::jsonErrorId(102, $model->errors);
        }
        $helper = $model->action();
        $user = $helper->getUser();

        return self::jsonSuccess([
            'helper' => $helper,
            'user'   => [
                'id'     => $helper->user_id,
                'avatar' => $user->getAvatar(),
                'name'   => $user->getName2(),
            ],
        ]);
    }

    public function actionSearchAjax($id)
    {
        $term = self::getParam('term');
        $items = ProjectManagerLink::find()->where(['school_id' => $id])->select(['user_id'])->column();
        $rows2 = [];
        $query = \common\models\UserAvatar::find()
            ->select([
                'id',
                'email',
                'name_first',
                'name_last',
            ])
            ->where([
                'or',
                ['like', 'email', $term],
                ['like', 'name_first', $term],
                ['like', 'name_last', $term],
            ]);
        if (count($items) > 0) $query->andWhere(['not', ['id' => $items]]);

        $rows = $query
            ->asArray()
            ->limit(10)
            ->all();

        foreach ($rows as $item) {
            $value = ($item['name_first'] . $item['name_last'] . '' == '') ? $item['email'] : $item['name_first'] . ' ' . $item['name_last'] . ' (' . $item['email'] . ')';
            $value = Html::encode($value);
            $rows2[] = [
                'id'    => $item['id'],
                'value' => $value,
            ];
        }

        return self::jsonSuccess($rows2);
    }

    public function actionHelperSearchAjax()
    {
        $model = new \avatar\models\validate\CabinetSchoolTaskListHelperSearchAjax();

        if (!$model->load(Yii::$app->request->post(), '')) {
            return self::jsonErrorId(400, 'Не загружены данные');
        }
        if (!$model->validate()) {
            return self::jsonErrorId(102, $model->errors);
        }

        return self::jsonSuccess($model->action());
    }

    /**
     * Редаkктирование задачи, выводит форму и обрабатывает ее
     *
     * @param $id
     *
     * @return string
     * @throws ForbiddenHttpException
     * @throws NotFoundHttpException
     */
    public function actionEdit($id)
    {
        $model = Task::findOne($id);
        if (is_null($model)) {
            throw new NotFoundHttpException('Не найдена задача');
        }
        if ($model->currency_id) {
            $c = \common\models\piramida\Currency::findOne($model->currency_id);
            $model->price = $model->price / pow(10, $c->decimals);
        }

        $school = School::findOne($model->school_id);

        $isAuthor = $model->user_id == Yii::$app->user->id;
        $isProjectManager = \common\models\school\ProjectManagerLink::find()->where(['user_id' => Yii::$app->user->id, 'school_id' => $school->id])->exists();
        $isAdmin = \common\models\school\AdminLink::find()->where(['user_id' => Yii::$app->user->id, 'school_id' => $school->id])->exists();
        if (!($isAuthor || $isProjectManager || $isAdmin)) {
            throw new ForbiddenHttpException('Задачу может редактировать только автор, админ или руководитеь проекта');
        }

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->price) {
                if ($model->currency_id) {
                    $c = \common\models\piramida\Currency::findOne($model->currency_id);
                    $model->price = $model->price * pow(10, $c->decimals);
                }
            }
            $model->save();
            Yii::$app->session->setFlash('form', $model->id);
        }

        return $this->render([
            'model'  => $model,
            'school' => $school,
        ]);
    }

    public function actionChangePrice($id)
    {
        $model = Task::findOne($id);
        if (is_null($model)) {
            throw new NotFoundHttpException('Не найдена задача');
        }
        if ($model->currency_id) {
            $c = \common\models\piramida\Currency::findOne($model->currency_id);
            $model->price = $model->price / pow(10, $c->decimals);
        }

        $school = School::findOne($model->school_id);

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->price) {
                if ($model->currency_id) {
                    $c = \common\models\piramida\Currency::findOne($model->currency_id);
                    $model->price = $model->price * pow(10, $c->decimals);
                }
            }
            if ($model->oldAttributes['price'] != $model->attributes['price']) {
                $model->trigger(Task::EVENT_TASK_PRICE_CHANGE);
                // добавляю комментарий
                // ищу куда добавить комент
                $list = CommentList::get(1, $model->id);
                Comment::add([
                    'list_id' => $list->id,
                    'user_id' => Yii::$app->user->id,
                    'text'    => 'Изменена цена с ' . $model->oldAttributes['price'] . ' на ' . $model->attributes['price'],
                ]);
            }
            $model->save();
            Yii::$app->session->setFlash('form', $model->id);
        }

        return $this->render([
            'model'  => $model,
            'school' => $school,
        ]);
    }
}
