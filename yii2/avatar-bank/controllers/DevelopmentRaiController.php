<?php

namespace avatar\controllers;

use yii\filters\AccessControl;

class DevelopmentRaiController extends \avatar\base\BaseController
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow'   => true,
                        'roles'   => ['role_rai-developer'],
                    ],
                ],
            ],
        ];
    }

    /**
     */
    public function actionIndex()
    {
        return $this->render();
    }

    /**
     */
    public function actionAkcioneri()
    {
        return $this->render();
    }
}
