<?php

namespace avatar\controllers;

use avatar\models\forms\school\CommandLink;
use avatar\models\forms\UserAvatar;
use avatar\models\validate\CabinetSchoolTaskListControllerHide;
use avatar\models\validate\CardNumberAccept;
use avatar\modules\ETH\ServiceEtherScan;
use avatar\modules\ETH\ServiceEthPlorer;
use common\components\Card;
use common\components\providers\BTC;
use common\components\providers\ETH;
use common\models\avatar\Currency;
use common\models\avatar\UserBill;
use common\models\comment\Comment;
use common\models\comment\CommentList;
use common\models\Config;
use common\models\piramida\Wallet;
use common\models\school\AdminLink;
use common\models\school\Kurs;
use common\models\school\Lesson;
use common\models\school\Page;
use common\models\school\PageBlockContent;
use common\models\school\ProjectManagerLink;
use common\models\school\School;
use common\models\task\Category;
use common\models\task\Session;
use common\models\task\Task;
use common\models\UserSeed;
use common\models\UserWallet;
use common\payment\BitCoinBlockTrailPayment;
use common\services\Security;
use common\services\Subscribe;
use console\controllers\MoneyRateController;
use cs\services\VarDumper;
use cs\web\Exception;
use cs\Widget\FileUpload4\ModelFields;
use Yii;
use yii\base\Event;
use yii\helpers\ArrayHelper;
use yii\helpers\FileHelper;
use yii\helpers\Html;
use yii\helpers\Json;
use yii\helpers\StringHelper;
use yii\httpclient\Client;
use yii\imagine\Image;
use yii\web\ForbiddenHttpException;
use yii\web\NotFoundHttpException;
use yii\web\Request;
use yii\web\Response;

class CabinetSchoolTaskListCategoryController extends CabinetSchoolBaseController
{

    /**
     * @return string
     */
    public function actionIndex($id)
    {
        $school = School::findOne($id);
        $this->isAccess($id);

        return $this->render(['school' => $school]);
    }

    public function actionAdd($id)
    {
        $model = new Category();
        $this->isAccess($id);
        $school = School::findOne($id);

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            Yii::$app->session->setFlash('form', 1);
            $model->school_id = $id;
            $model->save();
        }

        return $this->render([
            'model'  => $model,
            'school' => $school,
        ]);
    }

    /**
     * Удаляет категорию
     *
     * @param int $id идентификатор категории
     *
     * @return Response
     */
    public function actionDelete($id)
    {
        $schoolList = Task::find()->where(['category_id' => $id])->groupBy(['school_id'])->select(['school_id'])->column();
        if (count($schoolList) > 0) {
            return self::jsonErrorId(101, 'Школ на категории присоединено более чем одна');
        }
        if (count($schoolList) == 1) {
            return self::jsonErrorId(102, 'Для категории существуют прикрепленные задачи');
        }

        $Category = Category::findOne($id);
        $this->isAccess($Category->school_id);
        $Category->delete();

        return self::jsonSuccess();
    }

    public function actionEdit($id)
    {
        $model = Category::findOne($id);
        if (is_null($model)) {
            throw new NotFoundHttpException('Не найдена категория');
        }
        $this->isAccess($model->school_id);
        $school = School::findOne($model->school_id);

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $model->save();
            Yii::$app->session->setFlash('form', $model->id);
        }

        return $this->render([
            'model'  => $model,
            'school' => $school,
        ]);
    }
}
