<?php

namespace avatar\controllers;

use app\models\Article;
use app\models\SiteUpdate;
use app\services\Subscribe;
use avatar\models\forms\Contact;
use avatar\services\LogReader;
use common\components\providers\ETH;
use common\components\sms\IqSms;
use common\models\avatar\Currency;
use common\models\avatar\UserBill;
use common\models\CompanyCustomizeItem;
use common\models\Config;
use common\models\PaymentBitCoin;
use common\models\UserAvatar;
use common\models\UserRegistration;
use common\payment\BitCoinBlockTrailPayment;
use cs\Application;
use cs\base\BaseController;
use cs\services\UploadFolderDispatcher;
use cs\services\VarDumper;
use cs\web\Exception;
use Yii;
use yii\base\UserException;
use yii\db\Connection;
use yii\db\Query;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use yii\helpers\FileHelper;
use yii\helpers\Url;
use yii\httpclient\Client;
use yii\web\ForbiddenHttpException;
use yii\web\Response;

class VkController extends \avatar\base\BaseController
{

    /**
     */
    public function actionCallBack()
    {
        $message = '';
        Yii::info(\yii\helpers\VarDumper::dumpAsString([
            'post' => Yii::$app->request->post(),
            'get' => Yii::$app->request->get(),
            'rawBody' => Yii::$app->request->rawBody,
        ]), 'avatar\controllers\VkController::actionCallBack');


        return Config::get('vk-response');
    }

}
