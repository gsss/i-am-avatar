<?php

namespace avatar\controllers;

use avatar\base\Application;
use avatar\models\search\UserAvatar;
use common\models\avatar\UserBill;
use common\models\avatar\UserBillSystem;
use common\models\school\PotokUser3Link;
use common\models\UserRoot;
use console\controllers\MoneyRateController;
use cs\services\VarDumper;
use cs\web\Exception;
use Yii;
use yii\filters\AccessControl;
use yii\helpers\FileHelper;
use yii\helpers\Html;
use yii\rbac\Role;
use yii\web\Response;

class AdminUserRootController extends \avatar\controllers\AdminBaseController
{
    public static $isStatistic = false;

    /**
     */
    public function actionIndex()
    {
        return $this->render();
    }

    /**
     * @return
     * Ошибки:
     * 101 - Не найден пользователь
     */
    public function actionDelete($id)
    {
        $u = UserRoot::findOne($id);
        if (is_null($u)) return self::jsonErrorId(101, 'Не найден пользователь');
        if ($u->avatar_status == UserRoot::STATUS_REGISTERED) return self::jsonErrorId(102, 'Пользователь зарегистрирован, нельзя удалить');
        PotokUser3Link::deleteAll(['user_root_id' => $id]);
        $u->delete();

        return self::jsonSuccess();
    }
}
