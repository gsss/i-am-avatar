<?php

namespace avatar\controllers;

use avatar\models\forms\Contact;
use avatar\modules\ETH\ServiceEtherScan;
use avatar\services\LogReader;
use common\components\providers\BTC;
use common\components\providers\ETH;
use common\components\sms\IqSms;
use common\models\avatar\Currency;
use common\models\avatar\UserBill;
use common\models\Card;
use common\models\PaymentBitCoin;
use common\models\UserAvatar;
use common\models\UserRegistration;
use common\models\UserSeed;
use common\payment\BitCoinBlockTrailPayment;
use common\services\Security;
use cs\Application;
use cs\base\BaseController;
use cs\services\Str;
use cs\services\UploadFolderDispatcher;
use cs\services\VarDumper;
use cs\web\Exception;
use Yii;
use yii\base\UserException;
use yii\db\Connection;
use yii\db\Query;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use yii\helpers\FileHelper;
use yii\helpers\Json;
use yii\helpers\Url;
use yii\httpclient\Client;
use yii\web\Response;
use Blocktrail\SDK\Services\BlocktrailBatchUnspentOutputFinder;
use Blocktrail\SDK\WalletV1Sweeper;

class CabinetTradeBinanceApiController extends \avatar\controllers\CabinetBaseController
{
    /**
     * Отображет форму если не установлены ключи
     * Если у пользователя установлены коды то перенаправляю на `view`
     */
    public function actionIndex()
    {
        /** @var \common\models\UserAvatar $user */
        $user = \Yii::$app->user->identity;
        if ($user->hasBinanceAPI()) {
            return $this->redirect(['view']);
        } else {
            return $this->redirect(['set']);
        }
    }

    public function actionView()
    {
        return $this->render();
    }

    public function actionSet()
    {
        $model = new \avatar\models\forms\CabinetTradeBinanceApiSet();

        if ($model->load(\Yii::$app->request->post()) && $model->validate()) {
            $result = $model->action();
            \Yii::$app->session->setFlash('form', 1);
        }

        return $this->render([
            'model' => $model,
        ]);
    }
}
