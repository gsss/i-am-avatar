<?php

namespace avatar\controllers;

use app\services\Subscribe;
use avatar\controllers\AdminBaseController;
use avatar\models\forms\BlogItem;
use avatar\models\forms\UserAvatar;
use cs\models\Tables\Client;
use cs\services\VarDumper;
use cs\web\Exception;
use cs\Widget\BreadCrumbs\BreadCrumbs;
use Yii;
use yii\base\UserException;
use yii\filters\AccessControl;
use yii\helpers\Json;
use yii\helpers\Url;
use yii\httpclient\Client as ClientHttp;
use yii\web\Controller;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use app\models\NewsItem;
use app\models\Chenneling;
use cs\base\BaseController;
use yii\web\Response;
use yii\widgets\ActiveForm;
use app\models\SiteUpdate;

class AdminTokensController extends AdminBaseController
{

    public function actionIndex()
    {
        return $this->render();
    }

    public function actionAdd()
    {
        $model = new \avatar\models\forms\TokenAdd();

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $model->action();
            Yii::$app->session->setFlash('form', 1);

            return $this->refresh();
        } else {
            return $this->render([
                'model' => $model,
            ]);
        }
        BreadCrumbs::widget();
    }

    /**
     * REQUEST:
     * - id - int - идентификатор токена
     *
     * @return string|Response
     * @throws
     */
    public function actionEdit()
    {
        $id = self::getParam('id');
        if (is_null($id)) {
            throw new \Exception('Нужен обязательный параметр');
        }
        $model = new \avatar\models\forms\TokenEdit();
        $model->set($id);

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $model->save($id);
            Yii::$app->session->setFlash('form', 1);

            return $this->refresh();
        } else {
            return $this->render([
                'model' => $model,
            ]);
        }
    }

    public function actionRegistration()
    {
        $model = new \avatar\models\forms\TokenRegistration();

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $model->action();
            Yii::$app->session->setFlash('form', 1);
        }

        return $this->render([
            'model' => $model,
        ]);
    }

    public function actionDelete()
    {
        return self::jsonSuccess();
    }


}
