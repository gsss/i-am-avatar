<?php

namespace avatar\controllers;

use app\models\Article;
use app\models\SiteUpdate;
use app\services\Subscribe;
use avatar\models\forms\Contact;
use avatar\services\LogReader;
use common\components\providers\ETH;
use common\components\sms\IqSms;
use common\models\avatar\Currency;
use common\models\avatar\UserBill;
use common\models\CompanyCustomizeItem;
use common\models\PaymentBitCoin;
use common\models\school\Page;
use common\models\school\School;
use common\models\task\Category;
use common\models\UserAvatar;
use common\models\UserRegistration;
use common\payment\BitCoinBlockTrailPayment;
use cs\Application;
use cs\base\BaseController;
use cs\services\UploadFolderDispatcher;
use cs\services\VarDumper;
use cs\web\Exception;
use Yii;
use yii\base\Security;
use yii\base\UserException;
use yii\db\Connection;
use yii\db\Query;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use yii\helpers\FileHelper;
use yii\helpers\Url;
use yii\web\ForbiddenHttpException;
use yii\web\Response;

class SiteController extends \avatar\base\BaseController
{
    public function actions()
    {
        return [
            'error'   => [
                'class' => 'yii\web\ErrorAction',
                'view'  => '@avatar/views/site/error.php',
            ],
            'captcha' => [
                'class'           => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => !YII_ENV_PROD ? '123' : null,
            ],
        ];
    }

    public function actionFormSubmission()
    {
        $security = new Security();
        $string = Yii::$app->request->post('string');
        $stringHash = '';
        if (!is_null($string)) {
            $stringHash = $security->generatePasswordHash($string);
        }
        $school = School::findOne(2);
        $model = new Category();
        return $this->render('form-submission', [
            'stringHash' => $stringHash,
            'model'  => $model,
            'school' => $school,
        ]);
    }

    /**
     */
    public function actionFormKurs()
    {
        $model = new \avatar\models\forms\Kurs();

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            Yii::$app->session->setFlash('form', 1);
            $model->action(3);
        }

        return $this->render([
            'model' => $model,
        ]);
    }

    /**
     */
    public function actionFormSubscribe()
    {
        $model = new \avatar\models\forms\Kurs();

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            Yii::$app->session->setFlash('form', 1);
            $model->action(2);
        }

        return $this->render([
            'model' => $model,
        ]);
    }

    /**
     */
    public function actionFormTeam()
    {
        $model = new \avatar\models\forms\Kurs();

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            Yii::$app->session->setFlash('form', 1);
            $model->action(4);
        }

        return $this->render([
            'model' => $model,
        ]);
    }

    /**
     */
    public function actionToken()
    {
        return $this->render();
    }

    /**
     */
    public function actionPush()
    {
        Yii::info(\yii\helpers\VarDumper::dumpAsString(
            Yii::$app->request->post(),
            Yii::$app->request->get(),
            Yii::$app->request->rawBody
        ), 'avatar\controllers\SiteController::actionPush');
        return 1;
    }

    /**
     */
    public function actionCards()
    {
        return $this->render();
    }

    /**
     */
    public function actionToken2()
    {
        return $this->render();
    }

    /**
     */
    public function actionCallback()
    {
        return $this->render();
    }

    /**
     */
    public function actionReklama()
    {
        return $this->render();
    }

    /**
     * Политика конфиденциальности
     */
    public function actionPrivacyPolicy()
    {
        return $this->render();
    }

    /**
     * Пользовательское соглашение
     */
    public function actionUserAgreement ()
    {
        return $this->render();
    }

    /**
     */
    public function actionTarif()
    {
        return $this->render();
    }

    /**
     */
    public function actionElixir()
    {
        return $this->render();
    }

    /**
     */
    public function actionHologramAvatar()
    {
        return $this->render();
    }

    /**
     */
    public function actionContracts()
    {
        return $this->render();
    }

    /**
     */
    public function actionCloses()
    {
        return $this->render();
    }

    /**
     */
    public function actionAvatarPay()
    {
        return $this->render('avatar-pay');
    }

    /**
     */
    public function actionFreePrivacyPolicy()
    {
        return $this->render('free-privacy-policy');
    }

    /**
     */
    public function actionMap()
    {
        return $this->render();
    }

    /**
     */
    public function actionConvert()
    {
        $model = new \avatar\models\forms\ConvertJson();

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $text = $model->action();
            Yii::$app->session->setFlash('form', $text);
        }

        return $this->render([
            'model' => $model,
        ]);
    }

    /**
     */
    public function actionService()
    {
        return $this->render();
    }

    /**
     */
    public function actionIndex()
    {
        $this->layout = 'blank';
        $school = School::get();

        if ($school->id == 84 ) {
            $path1 = '@webroot/tilda/84/html/file211.html';
            $path = Yii::getAlias($path1);

            if (file_exists($path)) {
                return file_get_contents($path);
            }
        }

        $page = \common\models\school\Page::findOne(['school_id' => $school->id, 'url' => '/']);
        $page->view();

        return $this->render([
            'page' => $page,
        ]);
    }

    /**
     */
    public function actionIndex2()
    {
        $this->layout = 'landing2';

        return $this->render();
    }

    /**
     */
    public function actionMain()
    {
        return $this->render();
    }


    /**
     */
    public function actionAvatar()
    {
        return $this->render();
    }


    /**
     */
    public function actionHelp()
    {
        return $this->render();
    }


    /**
     */
    public function actionAbout()
    {
        return $this->render();
    }

    /**
     */
    public function actionProtection()
    {
        return $this->render();
    }

    /**
     */
    public function actionDb($file, $key)
    {
        if ($key != '9caf02a919d5411bde83c84bfde200a9e002c231') throw new ForbiddenHttpException();

        return Yii::$app->response->sendContentAsFile(file_get_contents(Yii::getAlias('@avatar/../../backup/empty/' . $file . '.sql')), 'db.sql');
    }

    /**
     */
    public function actionContact()
    {
        $model = new Contact();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {


            $model->contact('info@i-am-avatar.com');
            Yii::$app->session->setFlash('contactFormSubmitted');
        }

        return $this->render('contact', [
            'model' => $model,
        ]);
    }
}
