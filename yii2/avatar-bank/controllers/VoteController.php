<?php

namespace avatar\controllers;

use app\services\Subscribe;
use avatar\models\forms\Contact;
use avatar\services\LogReader;
use common\components\sms\IqSms;
use common\models\avatar\UserBill;
use common\models\NewsItem;
use common\models\PaymentBitCoin;
use common\models\piramida\Wallet;
use common\models\school\School;
use common\models\UserAvatar;
use common\models\UserRegistration;
use common\models\VoteAnswer;
use common\models\VoteItem;
use common\models\VoteList;
use common\payment\BitCoinBlockTrailPayment;
use cs\Application;
use cs\base\BaseController;
use cs\services\UploadFolderDispatcher;
use cs\services\VarDumper;
use cs\web\Exception;
use Yii;
use yii\base\UserException;
use yii\db\Connection;
use yii\db\Query;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use yii\helpers\FileHelper;
use yii\helpers\Url;
use yii\web\NotFoundHttpException;
use yii\web\Response;

class VoteController extends \avatar\controllers\CabinetBaseController
{
    public $layout = 'main';

    /**
     */
    public function actionIndex()
    {
        return $this->render();
    }

    /**
     */
    public function actionItem($id)
    {
        $item = VoteList::findOne($id);

        return $this->render([
            'item' => $item,
        ]);
    }

    /**
     */
    public function actionAnswer($id)
    {
        $VoteAnswer = VoteAnswer::findOne($id);

        // Проверяю голосовал ли уже
        if (VoteItem::find()->where(['user_id' => Yii::$app->user->id, 'answer_id'  => $id])->exists()) {
            return self::jsonErrorId(1, 'Голос уже отдан');
        }



        // Записываю голос
        $item = new VoteItem([
            'comment'    => self::getParam('comment'),
            'user_id'    => Yii::$app->user->id,
            'answer_id'  => $id,
            'created_at' => time(),
            'list_id'    => $VoteAnswer->list_id,
        ]);
        $item->save();



        return self::jsonSuccess();
    }
}
