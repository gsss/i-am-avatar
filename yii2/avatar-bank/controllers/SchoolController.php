<?php

namespace avatar\controllers;

use app\models\Article;
use app\models\SiteUpdate;
use app\services\Subscribe;
use avatar\models\forms\Contact;
use avatar\services\LogReader;
use common\components\sms\IqSms;
use common\models\avatar\UserBill;
use common\models\PaymentBitCoin;
use common\models\school\School;
use common\models\UserAvatar;
use common\models\UserMaster;
use common\models\UserRegistration;
use common\payment\BitCoinBlockTrailPayment;
use cs\Application;
use cs\base\BaseController;
use cs\services\UploadFolderDispatcher;
use cs\services\VarDumper;
use cs\web\Exception;
use Yii;
use yii\base\UserException;
use yii\db\Connection;
use yii\db\Query;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use yii\helpers\FileHelper;
use yii\helpers\Url;
use yii\web\Response;

class SchoolController extends \avatar\base\BaseController
{
    /**
     * @return string Response
     */
    public function actionIndex()
    {
        return $this->render([]);
    }

    /**
     */
    public function actionList()
    {
        return $this->render([]);
    }

    /**
     */
    public function actionItem($id)
    {
        $item = School::findOne($id);

        if (is_null($item)) {
            throw new Exception('Не найдена Школа');
        }
        if ($item->is_catalog  == 0) {
            throw new Exception('Школа скрыта');
        }

        return $this->render([
            'item'       => $item,
        ]);
    }
}
