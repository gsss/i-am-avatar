<?php

namespace avatar\controllers;

use avatar\models\forms\school\CommandLink;
use avatar\models\forms\UserAvatar;
use avatar\models\validate\CardNumberAccept;
use avatar\modules\ETH\ServiceEtherScan;
use avatar\modules\ETH\ServiceEthPlorer;
use common\components\Card;
use common\components\providers\BTC;
use common\components\providers\ETH;
use common\models\avatar\Currency;
use common\models\avatar\UserBill;
use common\models\comment\Comment;
use common\models\comment\CommentList;
use common\models\Config;
use common\models\piramida\Wallet;
use common\models\school\Kurs;
use common\models\school\Lesson;
use common\models\school\Page;
use common\models\school\PageBlockContent;
use common\models\school\ProjectManagerLink;
use common\models\school\School;
use common\models\school\UserLink;
use common\models\task\Category;
use common\models\task\Task;
use common\models\UserSeed;
use common\models\UserWallet;
use common\payment\BitCoinBlockTrailPayment;
use common\services\Security;
use common\services\Subscribe;
use console\controllers\MoneyRateController;
use cs\services\VarDumper;
use cs\web\Exception;
use cs\Widget\FileUpload4\ModelFields;
use Yii;
use yii\base\Event;
use yii\helpers\FileHelper;
use yii\helpers\Json;
use yii\helpers\StringHelper;
use yii\httpclient\Client;
use yii\imagine\Image;
use yii\web\Response;

class CabinetSchoolTaskListCommandController extends CabinetSchoolBaseController
{
    /**
     * @return string
     */
    public function actionIndex($id)
    {
        $school = School::findOne($id);
        return $this->render(['school' => $school]);
    }


    public function actionAdd($id)
    {
        $model = new \avatar\models\forms\school\CommandLink(['_school_id' => $id]);
        $school = School::findOne($id);

        if ($model->load(Yii::$app->request->post()) && $model->insert()) {
            Yii::$app->session->setFlash('form', 1);

            // устанавливаю идентификатор школы
            $i = \common\models\school\CommandLink::findOne($model->id);
            $i->school_id = $id;
            $i->save();

            // Добавляю пользователя также в школу если он еще не там
            $link = UserLink::findOne([
                'user_root_id' => \common\models\UserAvatar::findOne($model->user_id)->user_root_id,
                'school_id'    => $id,
            ]);
            if (is_null($link)) {
                UserLink::add([
                    'user_root_id' => \common\models\UserAvatar::findOne($model->user_id)->user_root_id,
                    'school_id'    => $id,
                ]);
            }
        }

        return $this->render([
            'model'  => $model,
            'school' => $school,
        ]);
    }

    public function actionEdit($id)
    {
        $model = \avatar\models\forms\school\CommandLinkEdit::findOne($id);
        $school = School::findOne($model->school_id);

        if (Yii::$app->request->isPost) {
            if ($model->load(Yii::$app->request->post()) && $model->validate()) {
                $model->hour_price_currency_id = 7;
                $s = $model->save();
                return self::jsonSuccess($s);
            } else {
                return self::jsonErrorId(102, $model->getErrors102());
            }
        }

        return $this->render([
            'model'  => $model,
            'school' => $school,
        ]);
    }


    public function actionDelete($id)
    {
        $school = \common\models\school\CommandLink::findOne($id);
        $school->delete();

        return self::jsonSuccess();
    }

    public function actionSet()
    {
        $model = new \avatar\models\validate\CabinetSchoolCommandControllerSet();

        if (!$model->load(Yii::$app->request->post(), '')) {
            return self::jsonErrorId(400, 'Не загружены данные');
        }
        if (!$model->validate()) {
            return self::jsonErrorId(102, $model->errors);
        }

        return self::jsonSuccess($model->action());
    }


    public function actionSearchAjax($id)
    {
        $this->isAccess($id);
        $term = self::getParam('term');
        $items = CommandLink::find()->where(['school_id' => $id])->select(['user_id'])->column();
        $rows2 = [];
        $query = \common\models\UserAvatar::find()
            ->select([
                'id',
                'email',
                'name_first',
                'name_last',
            ])
            ->where([
                'or',
                ['like', 'email', $term],
                ['like', 'name_last', $term],
                ['like', 'name_first', $term],
            ]);

        if (count($items) > 0) $query->andWhere(['not', ['id' => $items]]);

        $rows = $query
            ->asArray()
            ->limit(10)
            ->all();

        foreach ($rows as $item) {
            $rows2[] = [
                'id'    => $item['id'],
                'value' => ($item['name_first'] . $item['name_last'] . '' == '') ? $item['email'] : $item['name_first'] . ' ' . $item['name_last'] . ' (' . $item['email'] . ')',
            ];
        }

        return self::jsonSuccess($rows2);
    }
}
