<?php

namespace avatar\controllers;

use app\models\Article;
use app\models\SiteUpdate;
use app\services\Subscribe;
use common\components\providers\ETH;
use common\models\avatar\UserBill;
use common\models\avatar\UserBillDefault;
use common\models\HD;
use common\models\HDtown;
use common\models\PassportLink;
use common\models\UserAvatar;
use common\models\UserDocument;
use cs\base\BaseController;
use cs\services\SitePath;
use cs\services\UploadFolderDispatcher;
use cs\services\VarDumper;
use cs\web\Exception;
use Yii;
use yii\base\UserException;
use yii\bootstrap\ActiveForm;
use yii\db\Connection;
use yii\db\Query;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use yii\helpers\FileHelper;
use yii\helpers\Json;
use yii\web\Response;

class CabinetPassportController extends \avatar\controllers\CabinetBaseController
{
    public function actions()
    {
        return [
            'create-passport' => [
                'class' => '\avatar\controllers\actions\CabinetPassport\CreatePassport',
            ]
        ];
    }

    /**
     */
    public function actionIndex()
    {
        if (Yii::$app->request->isPost) {
            $v = Yii::$app->request->post('billing');
            $info = PassportLink::findOne(['user_id' => Yii::$app->user->id]);
            if (is_null($info)) {
                $info = new PassportLink([
                    'user_id'    => Yii::$app->user->id,
                    'billing_id' => $v,
                ]);
                $info->save();
            } else {
                $info->billing_id = $v;
                $info->save();
            }
            Yii::$app->session->setFlash('form', 1);

            return $this->refresh();
        } else {
            return $this->render();
        }
    }

    /**
     */
    public function actionContract()
    {
        $model = new \avatar\models\forms\PassportContract();

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $txid = $model->action();
            Yii::$app->session->setFlash('form', $txid);

            return $this->refresh();
        } else {
            return $this->render([
                'model' => $model,
            ]);
        }
    }

    /**
     * REQUEST:
     * - id - int
     * - password - string
     */
    public function actionReadAjax()
    {
        $id = self::getParam('id');
        $password = self::getParam('password');

        /** @var \common\models\UserAvatar $user */
        $user = Yii::$app->user->identity;
        $walletPassport = $user->getWalletEthPassport();
        $contractAddress = '0x7540224488976689302ce187b402cfc069daf25b';
        $abi = '[{"constant":true,"inputs":[],"name":"getDocumentsCount","outputs":[{"name":"","type":"uint256"}],"payable":false,"type":"function"},{"constant":false,"inputs":[{"name":"hash","type":"string"},{"name":"namef","type":"string"},{"name":"namel","type":"string"},{"name":"link","type":"string"},{"name":"data","type":"string"}],"name":"registerDocument","outputs":[],"payable":false,"type":"function"},{"constant":false,"inputs":[{"name":"id","type":"uint256"}],"name":"addSignature","outputs":[],"payable":false,"type":"function"},{"constant":true,"inputs":[{"name":"id","type":"uint256"}],"name":"getDocument","outputs":[{"name":"hash","type":"string"},{"name":"namef","type":"string"},{"name":"namel","type":"string"},{"name":"link","type":"string"},{"name":"data","type":"string"},{"name":"creator","type":"address"},{"name":"date","type":"uint256"},{"name":"count","type":"uint256"}],"payable":false,"type":"function"},{"constant":false,"inputs":[],"name":"kill","outputs":[],"payable":false,"type":"function"},{"constant":true,"inputs":[{"name":"","type":"uint256"}],"name":"documentsIds","outputs":[{"name":"hash","type":"string"},{"name":"namef","type":"string"},{"name":"namel","type":"string"},{"name":"link","type":"string"},{"name":"data","type":"string"},{"name":"creator","type":"address"},{"name":"date","type":"uint256"},{"name":"signsCount","type":"uint256"}],"payable":false,"type":"function"},{"constant":true,"inputs":[{"name":"id","type":"uint256"}],"name":"getDocumentSignsCount","outputs":[{"name":"","type":"uint256"}],"payable":false,"type":"function"},{"constant":false,"inputs":[{"name":"newOwner","type":"address"}],"name":"changeOwner","outputs":[],"payable":false,"type":"function"},{"constant":true,"inputs":[{"name":"id","type":"uint256"},{"name":"index","type":"uint256"}],"name":"getDocumentSign","outputs":[{"name":"member","type":"address"},{"name":"date","type":"uint256"}],"payable":false,"type":"function"},{"inputs":[],"payable":false,"type":"constructor"},{"anonymous":false,"inputs":[{"indexed":false,"name":"id","type":"uint256"},{"indexed":false,"name":"member","type":"address"}],"name":"DocumentSigned","type":"event"},{"anonymous":false,"inputs":[{"indexed":false,"name":"id","type":"uint256"},{"indexed":false,"name":"hash","type":"string"}],"name":"DocumentRegistered","type":"event"}]';

        $data = $walletPassport->contract(
            $password,
            $contractAddress,
            $abi,
            'getDocument',
            [$id]
        );
        $data[4] = Json::decode($data[4]);
        return self::jsonSuccess(['jsonString' => \yii\helpers\VarDumper::dumpAsString($data,10, true)]);
    }

    /**
     */
    public function actionRead()
    {
//        $d = '["3a86e98e9c51a8445c8a6710fc87467c3f562ace8b2f1bf5e9f4ea9fe67757ab","Zxsdcvfgbhnjcvbn","xcvbnmnbvcxzv","http://new-earth.avatar-bank.com/upload/FileUploadMany2/image/1504995392_zY1QF6LCM5.png","[\"x cvbnmnbv cxvb\",\"01:16:10\",\"19.09.2017\",\"bvnmbvcbnm\",\"48.90372298758945, 43.095950089843754\",\"vnbmbcv b\"]","0x0796348668b34d5867d114a4487f6ba529ea8553","1504995402","0"]';
//        $d = Json::decode($d);
//        $d[4] = Json::decode($d[4]);
//        VarDumper::dump($d);

        return $this->render();
    }

}
