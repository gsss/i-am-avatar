<?php

namespace avatar\controllers;

use app\models\Article;
use app\models\SiteUpdate;
use app\services\Subscribe;
use avatar\models\forms\Contact;
use avatar\services\LogReader;
use common\components\providers\ETH;
use common\components\sms\IqSms;
use common\models\avatar\Currency;
use common\models\avatar\UserBill;
use common\models\CompanyCustomizeItem;
use common\models\PaymentBitCoin;
use common\models\UserAvatar;
use common\models\UserRegistration;
use common\payment\BitCoinBlockTrailPayment;
use cs\Application;
use cs\base\BaseController;
use cs\services\UploadFolderDispatcher;
use cs\services\VarDumper;
use cs\web\Exception;
use Yii;
use yii\base\UserException;
use yii\db\Connection;
use yii\db\Query;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use yii\helpers\FileHelper;
use yii\helpers\Url;
use yii\web\ForbiddenHttpException;
use yii\web\Response;

class CoinExplorerController extends \avatar\base\BaseController
{
    /**
     */
    public function actionIndex()
    {
        return $this->render('@avatar/views/' . $this->id . '/' . $this->action->id);
    }

    /**
     */
    public function actionCurrency()
    {
        return $this->render('@avatar/views/' . $this->id . '/' . $this->action->id);
    }

    /**
     */
    public function actionCurrencyItem($id)
    {
        $c = \common\models\piramida\Currency::findOne($id);
        if (is_null($c)) throw new Exception('Монета не найдена');

        return $this->render('@avatar/views/' . $this->id . '/' . $this->action->id, ['item' => $c]);
    }

    /**
     */
    public function actionOperation()
    {
        return $this->render('@avatar/views/' . $this->id . '/' . $this->action->id);
    }

    /**
     */
    public function actionWallet()
    {
        return $this->render('@avatar/views/' . $this->id . '/' . $this->action->id);
    }

    /**
     */
    public function actionTransaction()
    {
        return $this->render('@avatar/views/' . $this->id . '/' . $this->action->id);
    }
}
