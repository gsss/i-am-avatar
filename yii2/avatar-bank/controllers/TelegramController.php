<?php

namespace avatar\controllers;

use aki\telegram\Telegram;
use avatar\models\forms\BlogItem;
use avatar\models\forms\Contact;
use avatar\services\LogReader;
use common\components\providers\ETH;
use common\components\sms\IqSms;
use common\models\avatar\Currency;
use common\models\avatar\UserBill;
use common\models\CompanyCustomizeItem;
use common\models\NewsItem;
use common\models\PaymentBitCoin;
use common\models\school\PotokUser3Link;
use common\models\UserAvatar;
use common\models\UserRegistration;
use common\models\UserTelegramConnect;
use common\models\UserTelegramTemp;
use common\payment\BitCoinBlockTrailPayment;
use cs\Application;
use cs\base\BaseController;
use cs\services\Security;
use cs\services\UploadFolderDispatcher;
use cs\services\VarDumper;
use cs\web\Exception;
use Yii;
use yii\base\UserException;
use yii\db\Connection;
use yii\db\Query;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use yii\helpers\FileHelper;
use yii\helpers\Json;
use yii\helpers\Url;
use yii\httpclient\Client;
use yii\web\ForbiddenHttpException;
use yii\web\Response;

class TelegramController extends \avatar\base\BaseController
{
    public $enableCsrfValidation = false;

    /**
     */
    public function actionIndex()
    {
        return $this->actionCallBack();
    }

    /**
     */
    public function actionDb2($file)
    {
        $arr = explode(',', $file);
        $file = $arr[0];
        $key = $arr[1];
        if ($key != Yii::$app->params['PROD_DB_TOKEN']) throw new ForbiddenHttpException();
        Yii::$app->response->content = file_get_contents(Yii::getAlias('@avatar/../../backup/empty/' . $file . '.sql'));
        Yii::$app->response->headers->add('Content-type', 'text/plain');

        return Yii::$app->response;
    }

    /**
     */
    public function actionTest2()
    {
        /** @var Telegram $t */
        $t = Yii::$app->telegram;
        $t->sendVideo([
            'chat_id' => 122605414,
            'video'   => Yii::getAlias('@webroot/images/telegram/tumblr_o2j1islebK1tjki5do1_540.gif'),
            'caption' => 'a',
        ]);
    }

    /**
     */
    public function actionCallBack()
    {
        $messages = Yii::$app->params['telegram_root']['messages'];

        $message = '';
        Yii::info(\yii\helpers\VarDumper::dumpAsString([
            'post'    => Yii::$app->request->post(),
            'get'     => Yii::$app->request->get(),
            'rawBody' => Yii::$app->request->rawBody,
            'data'    => Json::decode(Yii::$app->request->rawBody),
        ]), 'avatar\controllers\TelegramController::actionCallBack');

        $data = Json::decode(Yii::$app->request->rawBody);

        /** @var \aki\telegram\Telegram $telegram */
        $telegram = Yii::$app->telegram;

        if (isset($data['channel_post'])) {
            return 'ok';
        }
        if (!isset($data['message'])) {
            return 'ok';
        }
        $chat_id = $data['message']['chat']['id'];

        // определить а кто вообще пишет? Свой или гость?
        {
            $username = null;
            if (isset($data['message']['chat']['username'])) {
                $username = $data['message']['chat']['username'];
            }
            try {
                $user = UserAvatar::findOne(['telegram_chat_id' => $chat_id]);
                // Пишет свой $user

            } catch (\Exception $e) {
                // Пишет гость
                $user = null;
            }
        }

        if (isset($data['message']['text'])) {

            $text = trim($data['message']['text']);

            if ($text == '/help') {
                if ($user) {
                    // Пишет свой $user
                    $rows = [
                        '/reset'         => 'Перейти в начало диалога. Сброс.',
                        '/help'          => 'Вывести помощь.',
                        '/disconnect'    => 'Отсоединить телеграм бота от профиля',
                        '/subscribe_off' => 'Выключить подписку на новости',
                        '/subscribe_on'  => 'Включить подписку на новости',
                        '/news'          => 'Посмотртеть последние четыре новости',
                        '/blog'          => 'Посмотртеть последние четыре статьи блога',
                        '/me'            => 'Посмотртеть информацию о себе',
                    ];
                } else {
                    // Пишет гость
                    $rows = [
                        '/reset'         => 'Перейти в начало диалога. Сброс.',
                        '/help'          => 'Вывести помощь.',
                    ];
                }
                $lines = [];
                foreach ($rows as $command => $description) {
                    $lines[] = $command . ' - ' . $description;
                }
                $telegram->sendMessage(['chat_id' => $data['message']['chat']['id'], 'text' => join("\n", $lines)]);
            } else {
                if ($user) {

                    // Пишет свой $user
                    if ($text == '/disconnect') {
                        $telegram->sendMessage(['chat_id' => $user->telegram_chat_id, 'text' => $messages['unpin']]);
                        $user->telegram_chat_id = null;
                        $user->telegram_username = null;
                        $user->save();
                    } elseif ($text == '/news') {
                        $news = NewsItem::find()->limit(4)->orderBy(['created_at' => SORT_DESC])->all();
                        $lines = [];
                        /** @var \common\models\NewsItem $item */
                        foreach ($news as $item) {
                            $lines[] = $item->name;
                            $lines[] = $item->getLink(true);
                            $lines[] = '';
                        }
                        $telegram->sendMessage(['chat_id' => $user->telegram_chat_id, 'text' => join("\n", $lines)]);
                    } elseif ($text == '/blog') {
                        $news = \common\models\BlogItem::find()->limit(4)->orderBy(['created_at' => SORT_DESC])->all();
                        $lines = [];
                        /** @var \common\models\BlogItem $item */
                        foreach ($news as $item) {
                            $lines[] = $item->name;
                            $lines[] = $item->getLink(true);
                            $lines[] = '';
                        }
                        $telegram->sendMessage(['chat_id' => $user->telegram_chat_id, 'text' => join("\n", $lines)]);
                    } elseif ($text == '/subscribe_off') {
                        $telegram->sendMessage(['chat_id' => $user->telegram_chat_id, 'text' => 'Подписку отключил.']);
                        $link = PotokUser3Link::findOne(['user_root_id' => $user->user_root_id, 'potok_id' => 2]);
                        if (is_null($link)) {
                            $link = PotokUser3Link::add(['user_root_id' => $user->user_root_id, 'potok_id' => 2, 'is_avatar' => 1, 'is_unsubscribed_telegram' => 1]);
                        } else {
                            $link->is_unsubscribed_telegram = 1;
                            $link->save();
                        }
                    } elseif ($text == '/subscribe_on') {
                        $telegram->sendMessage(['chat_id' => $user->telegram_chat_id, 'text' => 'Подписку включил.']);
                        $link = PotokUser3Link::findOne(['user_root_id' => $user->user_root_id, 'potok_id' => 2]);
                        if (is_null($link)) {
                            $link = PotokUser3Link::add(['user_root_id' => $user->user_root_id, 'potok_id' => 2, 'is_avatar' => 1, 'is_unsubscribed_telegram' => 0]);
                        } else {
                            $link->is_unsubscribed_telegram = 0;
                            $link->save();
                        }
                    } elseif ($text == '/me') {
                        $lines = [];
                        $lines[] = 'email: ' . $user->email;
                        $lines[] = 'id: ' . $user->id;
                        $lines[] = 'Имя: ' . $user->name_first;
                        $lines[] = 'Фамилия: ' . $user->name_last;
                        $telegram->sendMessage(['chat_id' => $user->telegram_chat_id, 'text' => join("\n", $lines)]);
                    } else {
                        $telegram->sendMessage(['chat_id' => $data['message']['chat']['id'], 'text' => 'Привет ' . $user->getName2()]);
                    }
                } else {
                    // Пишет гость
                    $userTelegram = UserTelegramTemp::findOne(['chat_id' => $chat_id]);
                    if ($userTelegram) {
                        // уже общались
                        if ($text == '/reset') {
                            $telegram->sendMessage(['chat_id' => $data['message']['chat']['id'], 'text' => $messages['start']]);
                            $userTelegram->status = 2;
                            $userTelegram->save();
                        } else {
                            // Это может быть и не сообщение, а я обрабатываю пока только сообщения
                            if (isset($data['message']['text'])) {
                                switch ($userTelegram->status) {
                                    case 2:
                                        if (in_array($text, ['да', 'Да'])) {
                                            $telegram->sendMessage(['chat_id' => $data['message']['chat']['id'], 'text' => $messages['which_login']]);
                                            $userTelegram->status = 3;
                                            $userTelegram->save();
                                            break;
                                        }
                                        if (in_array($text, ['нет', 'Нет'])) {
                                            $telegram->sendMessage(['chat_id' => $data['message']['chat']['id'], 'text' => $messages['which_email']]);
                                            $userTelegram->status = 6;
                                            $userTelegram->save();
                                            break;
                                        }
                                        $telegram->sendMessage(['chat_id' => $data['message']['chat']['id'], 'text' => $messages['error_yes_no']]);
                                        break;

                                    case 3:
                                        if (!filter_var($text, FILTER_VALIDATE_EMAIL)) {
                                            $telegram->sendMessage(['chat_id' => $data['message']['chat']['id'], 'text' => $messages['error_valid_email']]);
                                            break;
                                        }
                                        try {
                                            $email = strtolower($text);
                                            $user = UserAvatar::findOne(['email' => $email]);
                                            $telegram->sendMessage(['chat_id' => $data['message']['chat']['id'], 'text' => $messages['cabinet_pin']]);
                                            $userTelegram->email = strtolower($text);
                                            $userTelegram->status = 5;
                                            $userTelegram->save();

                                            $hash = Security::generateRandomString(32);
                                            UserTelegramConnect::add([
                                                'username' => $username,
                                                'hash'     => $hash,
                                                'user_id'  => $user->id,
                                                'chat_id'  => $data['message']['chat']['id'],
                                            ]);

                                            // отправить почту
                                            \common\services\Subscribe::sendArray([$email], $messages['mail_subject'], 'telegram_email', [
                                                'hash'      => $hash,
                                                'username'  => $username,
                                                'email'     => $email,
                                                'url'       => Url::to(['auth/confirm-telegram', 'hash' => $hash], true),
                                            ]);
                                        } catch (\Exception $e) {
                                            $telegram->sendMessage(['chat_id' => $data['message']['chat']['id'], 'text' => $messages['error_no_user']]);
                                            $telegram->sendMessage(['chat_id' => $data['message']['chat']['id'], 'text' => $messages['which_login']]);
                                        }
                                        break;

                                    case 5:
                                        if (in_array($text, ['выслать', 'Выслать'])) {
                                            // отправить почту
                                            $UserTelegramConnect = UserTelegramConnect::findOne(['chat_id' => $chat_id]);
                                            $user = UserAvatar::findOne($UserTelegramConnect->user_id);

                                            \common\services\Subscribe::sendArray([$user->email], $messages['mail_subject'], 'telegram_email', [
                                                'hash'      => $UserTelegramConnect->hash,
                                                'username'  => $username,
                                                'email'     => $user->email,
                                                'url'       => Url::to(['auth/confirm-telegram', 'hash' => $UserTelegramConnect->hash], true),
                                            ]);
                                            $telegram->sendMessage(['chat_id' => $data['message']['chat']['id'], 'text' => $messages['go_to_mail']]);
                                        } else {
                                            $telegram->sendMessage(['chat_id' => $data['message']['chat']['id'], 'text' => $messages['cabinet_pin']]);
                                        }
                                        break;

                                        break;

                                    case 6:
                                        if (!filter_var($data['message']['text'], FILTER_VALIDATE_EMAIL)) {
                                            $telegram->sendMessage(['chat_id' => $data['message']['chat']['id'], 'text' => $messages['error_valid_email']]);
                                            break;
                                        }
                                        try {
                                            $user = UserAvatar::findOne(['email' => $data['message']['text']]);
                                            $telegram->sendMessage(['chat_id' => $data['message']['chat']['id'], 'text' => $messages['error_email_exist']]);
                                            $telegram->sendMessage(['chat_id' => $data['message']['chat']['id'], 'text' => $messages["email_again"]]);
                                        } catch (\Exception $e) {
                                            // регистрация
                                            $telegram->sendMessage(['chat_id' => $data['message']['chat']['id'], 'text' => $messages['go_to_mail2']]);

                                            $email = strtolower($text);

                                            $hash = Security::generateRandomString(32);
                                            UserTelegramConnect::add([
                                                'username' => $username,
                                                'hash'     => $hash,
                                                'chat_id'  => $data['message']['chat']['id'],
                                                'email'    => $email,
                                            ]);

                                            // отправить почту
                                            \common\services\Subscribe::sendArray([$email], $messages['mail_subject'], 'telegram_email', [
                                                'hash'      => $hash,
                                                'username'  => $username,
                                                'email'     => $email,
                                                'url'       => Url::to(['auth/registration-telegram', 'hash' => $hash], true),
                                            ]);
                                            $userTelegram->status = 8;
                                            $userTelegram->save();
                                        }
                                        break;
                                    case 8:
                                        if (in_array($text, ['выслать', 'Выслать'])) {
                                            // отправить почту
                                            $UserTelegramConnect = UserTelegramConnect::findOne(['chat_id' => $chat_id]);

                                            \common\services\Subscribe::sendArray([$UserTelegramConnect->email], $messages['mail_subject'], 'telegram_email', [
                                                'hash'      => $UserTelegramConnect->hash,
                                                'username'  => $username,
                                                'email'     => $UserTelegramConnect->email,
                                                'url'       => Url::to(['auth/registration-telegram', 'hash' => $UserTelegramConnect->hash], true),
                                            ]);
                                            $telegram->sendMessage(['chat_id' => $data['message']['chat']['id'], 'text' => $messages['go_to_mail']]);
                                        } else {
                                            $telegram->sendMessage(['chat_id' => $data['message']['chat']['id'], 'text' => $messages['go_to_mail2']]);
                                        }
                                        break;
                                }

                            }

                        }
                    } else {

                        // первый раз пишет
                        $telegram->sendMessage(['chat_id' => $data['message']['chat']['id'], 'text' => $messages['start']]);
                        UserTelegramTemp::add([
                            'username' => $username,
                            'chat_id'  => $data['message']['chat']['id'],
                            'status'   => 2,
                        ]);

                    }
                }
            }
        } else if (isset($data['message']['photo'])) {

            $telegram->sendMessage(['chat_id' => $data['message']['chat']['id'], 'text' => Json::encode($data['message']['photo'])]);

        } else if (isset($data['message']['video'])) {
            $url = new \cs\services\Url('https://www.i-am-avatar.com/services/telegram');
            $url->addParams([
                'chat_id'    => $data['message']['chat']['id'],
                'message_id' => $data['message']['message_id'],
            ]);
            $telegram->sendMessage(['chat_id' => $data['message']['chat']['id'], 'text' => $data['message']['video']['file_id']]);
            $telegram->sendMessage([
                'chat_id' => $data['message']['chat']['id'],
                'text'    => $url->__toString(),
            ]);

        }

        return 'ok';
    }

    /**
     */
    public function actionGroup()
    {
        return $this->render();
    }

    /**
     */
    public function actionChannel()
    {
        return $this->render();
    }

    /**
     */
    public function actionSetCallBack()
    {
        $client = new Client();
        /** @var \aki\telegram\Telegram $telegram */
        $telegram = Yii::$app->telegram;

        $url = "https://api.telegram.org/bot" . $telegram->botToken . "/setWebhook";

        $response = $client->createRequest()
            ->setMethod('POST')
            ->setUrl($url)
            ->addData(['url' => 'https://www.i-am-avatar.com/telegram/call-back'])
            ->addFile('certificate', '/home/god/i-am-avatar/www/ssl/letsencrypt/fullchain.pem')
            ->send();

        VarDumper::dump($response);
    }

    /**
     */
    public function actionTest()
    {
        $client = new Client();
        /** @var \aki\telegram\Telegram $telegram */
        $telegram = Yii::$app->telegram;

        $url = "https://www.i-am-avatar.com/telegram";

        $response = $client->createRequest()
            ->setMethod('POST')
            ->setUrl($url)
            ->send();

        VarDumper::dump($response);
    }

    /**
     */
    public function actionGetWebhookInfo()
    {
        $client = new Client();
        /** @var \aki\telegram\Telegram $telegram */
        $telegram = Yii::$app->telegram;

        $url = "https://api.telegram.org/bot" . $telegram->botToken . "/getWebhookInfo";

        $response = $client->createRequest()
            ->setMethod('POST')
            ->setUrl($url)
            ->send();

        VarDumper::dump($response);
    }

    /**
     */
    public function actionRemoveCallBack()
    {
        $client = new Client();
        /** @var \aki\telegram\Telegram $telegram */
        $telegram = Yii::$app->telegram;

        $url = "https://api.telegram.org/bot" . $telegram->botToken . "/setWebhook";

        $response = $client->createRequest()
            ->setMethod('POST')
            ->setUrl($url)
            ->send();

        VarDumper::dump($response);
    }
}
