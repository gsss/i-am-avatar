<?php

namespace avatar\controllers;

use avatar\models\forms\school\CommandLink;
use avatar\models\forms\UserAvatar;
use avatar\models\validate\CabinetSchoolTaskListControllerHide;
use avatar\models\validate\CardNumberAccept;
use avatar\modules\ETH\ServiceEtherScan;
use avatar\modules\ETH\ServiceEthPlorer;
use common\components\Card;
use common\components\providers\BTC;
use common\components\providers\ETH;
use common\models\avatar\Currency;
use common\models\avatar\UserBill;
use common\models\comment\Comment;
use common\models\comment\CommentList;
use common\models\Config;
use common\models\piramida\Wallet;
use common\models\school\AdminLink;
use common\models\school\Kurs;
use common\models\school\Lesson;
use common\models\school\Page;
use common\models\school\PageBlockContent;
use common\models\school\ProjectManagerLink;
use common\models\school\School;
use common\models\task\Category;
use common\models\task\Session;
use common\models\task\Task;
use common\models\UserSeed;
use common\models\UserWallet;
use common\payment\BitCoinBlockTrailPayment;
use common\services\Security;
use common\services\Subscribe;
use console\controllers\MoneyRateController;
use cs\Application;
use cs\services\Url;
use cs\services\VarDumper;
use cs\web\Exception;
use cs\Widget\FileUpload4\ModelFields;
use Yii;
use yii\base\Event;
use yii\helpers\ArrayHelper;
use yii\helpers\FileHelper;
use yii\helpers\Html;
use yii\helpers\Json;
use yii\helpers\StringHelper;
use yii\httpclient\Client;
use yii\imagine\Image;
use yii\web\ForbiddenHttpException;
use yii\web\NotFoundHttpException;
use yii\web\Request;
use yii\web\Response;

class CabinetSchoolTaskListController extends CabinetSchoolBaseController
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => '\yii\filters\AccessControl',
                'rules' => [
                    [
                        'allow'         => true,
                        'matchCallback' => function ($rule, $action) {
                            if (in_array($action->id, [
                                'add',
                                'add-sub',
                                'index',
                                'index2',
                                'ajail',
                                'ajail2',
                                'export',
                                'search-ajax',
                                'project-manager-list',
                                'project-manager-list-add',
                                ])) {

                                $school_id = Yii::$app->request->get('id');

                            } else if (in_array($action->id, [
                                'view-ajax',
                                'view-done-change-time',
                            ])) {
                                $id = Yii::$app->request->post('id');
                                $task = Task::findOne($id);
                                $school_id = $task->school_id;
                            } else if (in_array($action->id, [
                                'set'
                            ])) {
                                $id = Yii::$app->request->get('item_id') . Yii::$app->request->post('item_id');
                                $task = Task::findOne($id);
                                $school_id = $task->school_id;
                            } else if (in_array($action->id, [
                                'executor-search-ajax',
                                'executor-search-ajax2',
                                'helper-search-ajax',
                                'sort'
                            ])) {
                                $school_id = Yii::$app->request->post('school_id');
                            } else if (in_array($action->id, [
                                'budjet-search-ajax',
                            ])) {
                                $school_id = Yii::$app->request->post('company_id');
                            } else if (in_array($action->id, [
                                'executor-add',
                                'helper-add',
                            ])) {
                                $id = Yii::$app->request->post('task_id');
                                $task = Task::findOne($id);
                                if (is_null($task)) {
                                    throw new NotFoundHttpException('Не найдена задача');
                                }
                                $school_id = $task->school_id;
                            } else {
                                $id = Yii::$app->request->get('id') . Yii::$app->request->post('id');
                                $task = Task::findOne($id);
                                if (is_null($task)) {
                                    throw new NotFoundHttpException('Не найдена задача');
                                }
                                $school_id = $task->school_id;
                            }
                            if (!Application::isInteger($school_id)) throw new \Exception('$school_id не целое');

                            $isAdmin = AdminLink::find()->where([
                                'user_id'   => Yii::$app->user->id,
                                'school_id' => $school_id,
                            ])->exists();

                            $isCommand = \common\models\school\CommandLink::find()->where([
                                'user_id'   => Yii::$app->user->id,
                                'school_id' => $school_id,
                            ])->exists();

                            return $isAdmin || $isCommand;
                        },
                    ],
                ],
            ],
        ];
    }

    public function actions()
    {
        return [
            'view-start'               => [
                'class' => '\avatar\controllers\actions\CabinetSchoolTaskListController\actionViewStart',
            ],
            'view-ajax'                => [
                'class' => '\avatar\controllers\actions\DefaultAjax',
                'model' => '\avatar\models\validate\CabinetSchoolTaskListViewAjax',
            ],
            'view-done-change-time'      => [
                'class' => '\avatar\controllers\actions\DefaultAjax',
                'model' => '\avatar\models\validate\CabinetSchoolTaskListDoneChangeTime',
            ],
            'view-done'                => [
                'class' => '\avatar\controllers\actions\CabinetSchoolTaskListController\actionViewDone',
            ],
            'view-reject'              => [
                'class' => '\avatar\controllers\actions\CabinetSchoolTaskListController\actionViewReject',
            ],
            'view-finish'              => [
                'class' => '\avatar\controllers\actions\CabinetSchoolTaskListController\actionViewFinish',
            ],
            'view-finish-reject'       => [
                'class' => '\avatar\controllers\actions\CabinetSchoolTaskListController\actionViewFinishReject',
            ],
            'session-start'            => [
                'class' => '\avatar\controllers\actions\CabinetSchoolTaskListController\actionSessionStart',
            ],
            'session-finish'           => [
                'class' => '\avatar\controllers\actions\CabinetSchoolTaskListController\actionSessionFinish',
            ],
            'view-close-session-start' => [
                'class' => '\avatar\controllers\actions\DefaultAjax',
                'model' => '\avatar\models\validate\CabinetSchoolTaskListControllerViewCloseSessionStart',
            ],

            'budjet-search-ajax' => [
                'class' => '\avatar\controllers\actions\DefaultAjax',
                'model' => '\avatar\models\validate\CabinetSchoolTaskListBudjetSearchAjax',
            ],

            'executor-search-ajax' => [
                'class' => '\avatar\controllers\actions\DefaultAjax',
                'model' => '\avatar\models\validate\CabinetSchoolTaskListExecutorSearchAjax',
            ],
            'executor-search-ajax2' => [
                'class' => '\avatar\controllers\actions\DefaultAjax',
                'model' => '\avatar\models\validate\CabinetSchoolTaskListExecutorSearchAjax2',
            ],
            'executor-add'         => [
                'class' => '\avatar\controllers\actions\DefaultAjax',
                'model' => '\avatar\models\validate\CabinetSchoolTaskListAddExecutor',
            ],

            'helper-search-ajax' => [
                'class' => '\avatar\controllers\actions\DefaultAjax',
                'model' => '\avatar\models\validate\CabinetSchoolTaskListHelperSearchAjax',
            ],
            'helper-add'         => [
                'class' => '\avatar\controllers\actions\DefaultAjax',
                'model' => '\avatar\models\validate\CabinetSchoolTaskListHelperAdd',
            ],

            'ajail-task' => [
                'class' => '\avatar\controllers\actions\DefaultAjax',
                'model' => '\avatar\models\validate\CabinetSchoolTaskListAjailTask',
            ],
            'sort'       => [
                'class' => '\avatar\controllers\actions\DefaultAjax',
                'model' => '\avatar\models\validate\CabinetSchoolTaskListSort',
            ],
        ];
    }

    /**
     * @return string
     */
    public function actionIndex($id)
    {
        $school = School::findOne($id);

        return $this->render(['school' => $school]);
    }


    /**
     * @return string
     */
    public function actionIndex2($id)
    {
        $school = School::findOne($id);

        return $this->render(['school' => $school]);
    }

    /**
     * @return string
     */
    public function actionExport($id, $search)
    {
        $model = new \avatar\models\validate\CabinetSchoolTaskListExport();

        if ($model->load(Yii::$app->request->get(), '') && $model->validate()) {
            return $model->action();
        }

        return self::jsonErrorId(102, $model->errors);
    }

    /**
     * @param int $id school_task.id
     *
     * @return string
     * @throws ForbiddenHttpException
     * @throws NotFoundHttpException
     */
    public function actionView($id)
    {
        $model = Task::findOne($id);
        $school = School::findOne($model->school_id);

        return $this->render([
            'model'  => $model,
            'school' => $school,
        ]);
    }

    /**
     * @return string
     */
    public function actionAjail($id)
    {
        $school = School::findOne($id);
        $cid = Yii::$app->request->get('cid');
        $category = null;
        if (!Application::isEmpty($cid)) {
            $category = Category::findOne($cid);
        }

        $aid = Yii::$app->request->get('aid');
        $author = null;
        if (!Application::isEmpty($aid)) {
            $author = UserAvatar::findOne($aid);
        }

        $eid = Yii::$app->request->get('eid');
        $executor = null;
        if (!Application::isEmpty($eid)) {
            $executor = UserAvatar::findOne($eid);
        }

        return $this->render([
            'school'   => $school,
            'category' => $category,
            'author'   => $author,
            'executor' => $executor,
        ]);
    }

    /**
     * @return string
     */
    public function actionAjail2($id)
    {
        $school = School::findOne($id);
        $cid = Yii::$app->request->get('cid');
        $category = null;
        if (!Application::isEmpty($cid)) {
            $category = Category::findOne($cid);
        }

        $aid = Yii::$app->request->get('aid');
        $author = null;
        if (!Application::isEmpty($aid)) {
            $author = UserAvatar::findOne($aid);
        }

        $eid = Yii::$app->request->get('eid');
        $executor = null;
        if (!Application::isEmpty($eid)) {
            $executor = UserAvatar::findOne($eid);
        }

        return $this->render([
            'school'   => $school,
            'category' => $category,
            'author'   => $author,
            'executor' => $executor,
        ]);
    }

    /**
     * - item_id - идентификатор задачи
     * - list_id - идентификатор статуса
     *
     * @return string
     */
    public function actionSet()
    {
        $item_id = self::getParam('item_id');
        $list_id = self::getParam('list_id');
        $Task = Task::findOne($item_id);
        $Task->status = $list_id;
        $Task->save();

        return self::jsonSuccess();
    }

    /**
     * Скрывает задачу
     *
     * @return string
     */
    public function actionHide($id)
    {
        $model = new CabinetSchoolTaskListControllerHide();
        if (!$model->load(['id' => $id], '')) {
            return self::jsonErrorId(400, 'Не загружены данные');
        }
        if (!$model->validate()) {
            return self::jsonErrorId(102, $model->errors);
        }
        $model->action();

        return self::jsonSuccess();
    }

    public function actionAdd($id)
    {
        $model = new \avatar\models\forms\Task(['school_id' => $id, 'user_id' => Yii::$app->user->id]);
        $school = School::findOne($id);

        if (Yii::$app->request->isPost) {
            if ($model->load(Yii::$app->request->post()) && $model->validate()) {
                $model->created_at = time();
                if (!Application::isEmpty($model->currency_id)) {
                    $model->price = \common\models\piramida\Currency::getAtomFromValue($model->price, $model->currency_id);
                }
                $model->last_comment_time = time();
                $model->school_id = $id;
                $model->is_hide = 0;
                $model->user_id = Yii::$app->user->id;
                $s = $model->save();

                return self::jsonSuccess($s);
            } else {
                return self::jsonErrorId(102, $model->getErrors102());
            }
        }

        return $this->render([
            'model' => $model,
            'school' => $school,
        ]);
    }

    public function actionAddSub($id)
    {
        $model = new Task();
        $school = School::findOne($id);

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $model->parent_id = self::getParam('parent_id');
            $model->user_id = Yii::$app->user->id;
            if ($model->price) {
                if ($model->currency_id) {
                    $c = \common\models\piramida\Currency::findOne($model->currency_id);
                    $model->price = $model->price * pow(10, $c->decimals);
                }
            }
            $model->category_id = Task::findOne($model->parent_id)->category_id;
            $model->school_id = $id;
            $model->save();
            $model->id = $model::getDb()->lastInsertID;
            Yii::$app->session->setFlash('form', $model->id);
        }

        return $this->render([
            'model' => $model, 'school' => $school,
        ]);
    }

    public function actionProjectManagerList($id)
    {
        $school = School::findOne($id);

        return $this->render([
            'school' => $school,
        ]);
    }

    /**
     * @param int $id school_project_manager_link.id
     *
     * @return string
     */
    public function actionProjectManagerListDelete($id)
    {
        $link = ProjectManagerLink::findOne($id);
        if (is_null($link)) {
            return self::jsonErrorId(404, 'Не найден ProjectManagerLink');
        }
        $link->delete();

        return self::jsonSuccess();
    }

    /**
     * @param int $id school.id
     *
     * @return string
     */
    public function actionProjectManagerListAdd($id)
    {
        $model = new \avatar\models\forms\school\ProjectManagerLink(['_school_id' => $id]);
        $school = School::findOne($id);

        if ($model->load(Yii::$app->request->post()) && $model->insert()) {
            Yii::$app->session->setFlash('form', 1);
            $i = ProjectManagerLink::findOne($model->id);
            $i->school_id = $id;
            $i->save();
        }

        return $this->render([
            'model'  => $model,
            'school' => $school,
        ]);
    }

    public function actionAddHelper()
    {
        $model = new \avatar\models\validate\CabinetSchoolTaskListControllerAddHelper();

        if (!$model->load(Yii::$app->request->post(), '')) {
            return self::jsonErrorId(400, 'Не загружены данные');
        }
        if (!$model->validate()) {
            return self::jsonErrorId(102, $model->errors);
        }
        $helper = $model->action();
        $user = $helper->getUser();

        return self::jsonSuccess([
            'helper' => $helper,
            'user'   => [
                'id'     => $helper->user_id,
                'avatar' => $user->getAvatar(),
                'name'   => $user->getName2(),
            ],
        ]);
    }

    public function actionSearchAjax($id)
    {
        $term = self::getParam('term');
        $items = ProjectManagerLink::find()->where(['school_id' => $id])->select(['user_id'])->column();
        $rows2 = [];
        $query = \common\models\UserAvatar::find()
            ->select([
                'id',
                'email',
                'name_first',
                'name_last',
            ])
            ->where([
                'or',
                ['like', 'email', $term],
                ['like', 'name_first', $term],
                ['like', 'name_last', $term],
            ]);
        if (count($items) > 0) $query->andWhere(['not', ['id' => $items]]);

        $rows = $query
            ->asArray()
            ->limit(10)
            ->all();

        foreach ($rows as $item) {
            $value = ($item['name_first'] . $item['name_last'] . '' == '') ? $item['email'] : $item['name_first'] . ' ' . $item['name_last'] . ' (' . $item['email'] . ')';
            $value = Html::encode($value);
            $rows2[] = [
                'id'    => $item['id'],
                'value' => $value,
            ];
        }

        return self::jsonSuccess($rows2);
    }

    public function actionHelperSearchAjax()
    {
        $model = new \avatar\models\validate\CabinetSchoolTaskListHelperSearchAjax();

        if (!$model->load(Yii::$app->request->post(), '')) {
            return self::jsonErrorId(400, 'Не загружены данные');
        }
        if (!$model->validate()) {
            return self::jsonErrorId(102, $model->errors);
        }

        return self::jsonSuccess($model->action());
    }

    /**
     * Редаkктирование задачи, выводит форму и обрабатывает ее
     *
     * @param $id
     *
     * @return string
     * @throws ForbiddenHttpException
     * @throws NotFoundHttpException
     */
    public function actionEdit($id)
    {
        $model = Task::findOne($id);
        if (is_null($model)) {
            throw new NotFoundHttpException('Не найдена задача');
        }
        if ($model->currency_id) {
            $c = \common\models\piramida\Currency::findOne($model->currency_id);
            $model->price = $model->price / pow(10, $c->decimals);
        }

        $school = School::findOne($model->school_id);

        $isAuthor = $model->user_id == Yii::$app->user->id;
        $isProjectManager = \common\models\school\ProjectManagerLink::find()->where(['user_id' => Yii::$app->user->id, 'school_id' => $school->id])->exists();
        $isAdmin = \common\models\school\AdminLink::find()->where(['user_id' => Yii::$app->user->id, 'school_id' => $school->id])->exists();
        if (!($isAuthor || $isProjectManager || $isAdmin)) {
            throw new ForbiddenHttpException('Задачу может редактировать только автор, админ или руководитеь проекта');
        }

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->price) {
                if ($model->currency_id) {
                    $c = \common\models\piramida\Currency::findOne($model->currency_id);
                    $model->price = $model->price * pow(10, $c->decimals);
                }
            }
            $model->save();
            Yii::$app->session->setFlash('form', $model->id);
        }

        return $this->render([
            'model'  => $model,
            'school' => $school,
        ]);
    }

    public function actionChangePrice($id)
    {
        $model = Task::findOne($id);
        if (is_null($model)) {
            throw new NotFoundHttpException('Не найдена задача');
        }
        if ($model->currency_id) {
            $c = \common\models\piramida\Currency::findOne($model->currency_id);
            $model->price = $model->price / pow(10, $c->decimals);
        }

        $school = School::findOne($model->school_id);

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->price) {
                if ($model->currency_id) {
                    $c = \common\models\piramida\Currency::findOne($model->currency_id);
                    $model->price = $model->price * pow(10, $c->decimals);
                }
            }
            if ($model->oldAttributes['price'] != $model->attributes['price']) {
                $model->trigger(Task::EVENT_TASK_PRICE_CHANGE);
                // добавляю комментарий
                // ищу куда добавить комент
                $list = CommentList::get(1, $model->id);
                Comment::add([
                    'list_id' => $list->id,
                    'user_id' => Yii::$app->user->id,
                    'text'    => 'Изменена цена с ' . $model->oldAttributes['price'] . ' на ' . $model->attributes['price'],
                ]);
            }
            $model->save();
            Yii::$app->session->setFlash('form', $model->id);
        }

        return $this->render([
            'model'  => $model,
            'school' => $school,
        ]);
    }
}
