<?php

namespace avatar\controllers;

use avatar\modules\UniSender\UniSender;
use common\components\providers\ETH;
use common\models\avatar\Currency;
use common\models\avatar\UserBill;
use common\models\avatar\UserBillDefault;
use common\models\BillingMain;
use common\models\HD;
use common\models\HDtown;
use common\models\PaySystemSuccess;
use common\models\school\PotokUser3Link;
use common\models\school\School;
use common\models\subscribe\UserSubscribe;
use common\models\UserAvatar;
use common\models\UserDocument;
use common\models\UserMaster;
use common\models\UserTelegramConnect;
use common\models\UserTelegramTemp;
use common\services\Security\AES;
use cs\base\BaseController;
use cs\services\SitePath;
use cs\services\UploadFolderDispatcher;
use cs\services\VarDumper;
use cs\web\Exception;
use Yii;
use yii\base\Model;
use yii\base\UserException;
use yii\bootstrap\ActiveForm;
use yii\db\Connection;
use yii\db\Query;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use yii\helpers\FileHelper;
use yii\helpers\StringHelper;
use yii\web\Response;

class CabinetSchoolBudjetController extends CabinetSchoolBaseController
{


    /**
     * @return string
     */
    public function actionTest($id)
    {
        $this->isAccess($id);
        $school = School::findOne($id);

        return $this->render(['school' => $school]);
    }

    /**
     * @return string
     */
    public function actionTrud($id)
    {
        $this->isAccess($id);
        $school = School::findOne($id);

        return $this->render(['school' => $school]);
    }

    /**
     * @return string
     */
    public function actionImush($id)
    {
        $this->isAccess($id);
        $school = School::findOne($id);

        return $this->render(['school' => $school]);
    }

    /**
     * @return string
     */
    public function actionMoney($id)
    {
        $this->isAccess($id);
        $school = School::findOne($id);

        return $this->render(['school' => $school]);
    }

    /**
     * @return string
     */
    public function actionRashod($id)
    {
        $this->isAccess($id);
        $school = School::findOne($id);

        return $this->render(['school' => $school]);
    }
}