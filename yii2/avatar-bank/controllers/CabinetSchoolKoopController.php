<?php

namespace avatar\controllers;

use avatar\models\forms\UserAvatar;
use avatar\models\UserEnter;
use common\models\school\AdminLink;
use common\models\school\School;
use common\models\SchoolKoop;
use common\models\SchoolKoopWallet;
use common\services\FormAjax\ActiveForm;
use cs\Application;
use cs\services\VarDumper;
use iAvatar777\services\FormAjax\DefaultFormAjax;
use Yii;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;

class CabinetSchoolKoopController extends CabinetSchoolBaseController
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => '\yii\filters\AccessControl',
                'rules' => [
                    [
                        'allow'         => true,
                        'matchCallback' => function ($rule, $action) {
                            if (in_array($action->id, [
                                'index',
                                'wallets',
                            ])) {

                                $school_id = Yii::$app->request->get('id');

                            } else if (in_array($action->id, [
                                'view',
                            ])) {

                                $id = Yii::$app->request->get('id');
                                $u = UserEnter::findOne($id);
                                $school_id = $u->school_id;

                            } else if (in_array($action->id, [
                                'view-sign',
                            ])) {

                                $id = ArrayHelper::getValue(Yii::$app->request->post(),'CabinetSchoolKoopSign.id');
                                $u = UserEnter::findOne($id);
                                $school_id = $u->school_id;

                            } else if (in_array($action->id, [
                                'wallets-item',
                            ])) {

                                $id = ArrayHelper::getValue(Yii::$app->request->get(),'id');
                                $u = SchoolKoopWallet::findOne($id);
                                $school_id = $u->school_id;

                            }
                            if (!Application::isInteger($school_id)) throw new \Exception('$school_id не целое');

                            $isAdmin = AdminLink::find()
                                ->where([
                                    'user_id'   => Yii::$app->user->id,
                                    'school_id' => $school_id,
                                ])
                                ->exists();

                            return $isAdmin;
                        },
                    ],
                ],
            ],
        ];
    }

    public function actions()
    {
        return [
//            'sign-ajax' => [
//                'class'    => '\avatar\controllers\actions\DefaultAjax',
//                'model'    => '\avatar\models\forms\KoopSign',
//            ],
            'view-sign' => [
                'class'    => '\iAvatar777\services\FormAjax\DefaultFormAjax',
                'model'    => '\avatar\models\validate\CabinetSchoolKoopSign',
            ],
        ];
    }

    /**
     * @return string
     */
    public function actionIndex($id)
    {
        $school = School::findOne($id);

        return $this->render(['school' => $school]);
    }

    /**
     * @return string
     */
    public function actionWallets($id)
    {
        $school = School::findOne($id);

        return $this->render(['school' => $school]);
    }

    /**
     * @param  int $id
     * @return string
     */
    public function actionWalletsItem($id)
    {
        $wallet = SchoolKoopWallet::findOne($id);
        $school = School::findOne($wallet->school_id);

        return $this->render([
            'wallet' => $wallet,
                          'school' => $school
        ]);
    }

    /**
     * @return string
     */
    public function actionView($id)
    {
        $item = UserEnter::findOne($id);
        $school = School::findOne($item->school_id);

        return $this->render([
            'item'   => $item,
            'school' => $school,
        ]);
    }

}
