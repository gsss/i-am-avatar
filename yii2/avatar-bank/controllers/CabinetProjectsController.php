<?php

namespace avatar\controllers;

use app\services\Subscribe;
use avatar\controllers\AdminBaseController;
use avatar\models\forms\BlogItem;
use common\models\avatar\UserBill;
use common\models\investment\IcoRequest;
use common\models\investment\IcoRequestAddress;
use common\models\investment\IcoRequestSuccess;
use common\models\investment\Project;
use common\models\investment\ProjectIco;
use common\models\MapBankomat;
use common\models\PaySystemConfig;
use common\models\PaySystemListItem;
use cs\Application;
use cs\services\VarDumper;
use cs\web\Exception;
use Yii;
use yii\base\UserException;
use yii\filters\AccessControl;
use yii\helpers\StringHelper;
use yii\helpers\Url;
use yii\web\Controller;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use app\models\NewsItem;
use app\models\Chenneling;
use cs\base\BaseController;
use yii\web\Response;
use yii\widgets\ActiveForm;
use app\models\SiteUpdate;

class CabinetProjectsController extends \avatar\controllers\CabinetBaseController
{
    public function actionIndex()
    {
        return $this->render();
    }

    public function actionAdd()
    {
        $model = new \avatar\models\forms\Project();

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $model->action();

            return $this->redirect(['cabinet-projects/add-step2', 'id' => $model->id]);
        } else {
            return $this->render([
                'model' => $model,
            ]);
        }
    }

    /**
     *
     * @param int $id идентификатор проекта
     * @return string
     * @throws
     */
    public function actionView($id)
    {
        $project = Project::findOne($id);
        if ($project->user_id != Yii::$app->user->id) {
            throw new Exception('Это не ваш проект');
        }

        return $this->render(['project' => $project]);
    }

    public function actionAddStep2($id)
    {
        $model = ProjectIco::findOne($id);
        if (is_null($model)) {
            $model = new ProjectIco(['id' => $id]);
        }

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {

            $billing_id = $model->billing_id;
            $model->all_tokens = $model->remained_tokens;
            $billing = UserBill::findOne($billing_id);
            $project = Project::findOne($model->id);
            $project->currency = $billing->currency;
            $project->save();
            $model->currency_id = $billing->currency;
            $model->save();

            Yii::$app->session->setFlash('form', $model->id);
        }

        return $this->render([
            'model' => $model,
        ]);
    }


    /**
     * Добавление доставки
     *
     * REQUEST:
     * - list_id        - int - идентификатор объединения paysystems_config_list.id
     * - paysystem_id   - int - идентификатор платежной системы paysystems.id
     *
     * @return string|\yii\web\Response
     * @throws \cs\web\Exception
     */
    public function actionItemEdit()
    {
        $list_id = self::getParam('list_id');
        $paysystem_id = self::getParam('paysystem_id');
        $fields = [
            'parent_id'    => $list_id,
            'paysystem_id' => $paysystem_id,
        ];
        $listItem = PaySystemListItem::findOne($list_id);
        $project = Project::findOne(['payments_list_id' => $list_id]);
        $paymentConfig = PaySystemConfig::findOne($fields);
        if (is_null($paymentConfig)) {
            $paymentConfig = new PaySystemConfig($fields);
        }
        /** @var \common\models\PaySystem $paySystem */
        $paySystem = $paymentConfig->getPaySystem();
        $className = 'avatar\\modules\\Piramida\\PaySystems\\' . $paySystem->class_name . '\\Model';
        if (!class_exists($className)) {
            $className = 'avatar\\modules\\Piramida\\PaySystems\\Base\\Model';
        }
        /** @var \yii\base\Model $model */
        $model = new $className;
        if ($paymentConfig->config) {
            $model->setConfig($paymentConfig->config);
        }
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $paymentConfig->config = $model->getConfig();
            $ret = $paymentConfig->save();
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        } else {
            return $this->render('item-edit',[
                'model'    => $model,
                'listItem' => $listItem,
                'title'    => $paySystem->title,
                'class'    => $paySystem->class_name,
                'project'  => $project,
            ]);
        }
    }

    /**
     * Платежные реквизиты проекта
     *
     * @param int $id
     *
     * @return string
     *
     * @throws
     */
    public function actionPayments($id)
    {
        $project = Project::findOne($id);
        if ($project->user_id != Yii::$app->user->id) {
            throw new Exception('Это не ваш проект');
        }

        return $this->render(['project' => $project]);
    }

    /**
     * отправляет на модерацию
     *
     * @return string
     */
    public function actionSendModeration()
    {
        $id = self::getParam('id');
        $project = Project::findOne($id);
        if ($project->user_id != Yii::$app->user->id) {
            return self::jsonErrorId(101, 'Это не ваш проект');
        }
        $project->status = Project::STATUS_WAIT;
        $project->save();

        Application::mail(
            Yii::$app->params['projects']['moderationMail'],
            'Проект отправлен на модерацию',
            'ico_moderation_send',
            [
                'project' => $project,
            ]
        );

        return self::jsonSuccess();
    }

    /**
     * Удаляет конфиг ПС
     *
     * REQUEST:
     * - id - int - идентификатор конфига ПС
     *
     * @return string
     *
     * @throws
     */
    public function actionPaymentsRemove()
    {
        $id = self::getParam('id');
        if (is_null($id)) {
            return self::jsonErrorId(101, 'Не передан параметр');
        }
        if (!Application::isInteger($id)) {
            return self::jsonErrorId(102, 'Не верные данные');
        }
        $config = \common\models\PaySystemConfig::findOne($id);
        $project = Project::findOne(['payments_list_id' => $config->parent_id]);
        if (is_null($project)) {
            return self::jsonErrorId(104, 'Не найден проект');
        }
        if ($project->user_id != Yii::$app->user->id) {
            return self::jsonErrorId(103, 'Это не ваш проект');
        }
        $config->delete();

        return self::jsonSuccess();
    }

    /**
     * Отправляет токены клиенту
     *
     * REQUEST:
     * + password - string
     * + id - ico_request.id
     *
     * @return string
     * ошибки:
     * 101, 'Не загружены данные'
     * 102, $model->convertErrors()
     * 103, 'не удачная отправка' // Error: Invalid JSON RPC response
     * 104, $e->getMessage()
     */
    public function actionSend()
    {
        $model = new \avatar\models\validate\RequestSend();
        if (!$model->load(Yii::$app->request->post(), '')) {
            return self::jsonErrorId(101, 'Не загружены данные');
        }
        if (!$model->validate()) {
            return self::jsonErrorId(102, $model->convertErrors());
        }
        try {
            $txid = $model->action();
        } catch (\Exception $e) {
            if (StringHelper::startsWith($e->getMessage(),'Error: Invalid JSON RPC response')) {
                return self::jsonErrorId(103, 'не удачная отправка');
            } else {
                return self::jsonErrorId(104, $e->getMessage());
            }
        }

        return self::jsonSuccess(['txid' => $txid]);
    }


    /**
     * Заказы проекта
     *
     * @param int $id - идентификатор проекта
     *
     * @return string
     */
    public function actionRequests($id)
    {
        $project = Project::findOne($id);

        return $this->render(['project' => $project]);
    }

    /**
     * Заказы проекта
     *
     * @param int $id - идентификатор проекта
     *
     * @return string
     */
    public function actionHide()
    {
        $id = self::getParam('id');
        $request = IcoRequest::findOne($id);
        $request->is_hide = 1;
        $request->save();

        return self::jsonSuccess();
    }

    /**
     * Заказы проекта архив
     *
     * @param int $id - идентификатор проекта
     *
     * @return string
     */
    public function actionRequestsArchive($id)
    {
        $project = Project::findOne($id);

        return $this->render(['project' => $project]);
    }

    /**
     * Заказ проекта
     *
     * @param int $id - идентификатор заказа ico_request
     *
     * @return string
     */
    public function actionRequest($id)
    {
        $request = IcoRequest::findOne($id);

        return $this->render(['request' => $request]);
    }

    public function actionEdit($id)
    {
        $model = new \avatar\models\forms\Project();
        $m = Project::findOne($id);
        $model->set($m);

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $model->actionUpdate($m);
            Yii::$app->session->setFlash('form', 1);

            return $this->refresh();
        } else {
            return $this->render([
                'model' => $model,
            ]);
        }
    }

    /**
     * @param int $id идентификатор проекта
     *
     * @return string
     */
    public function actionEditIco($id)
    {
        $model = \common\models\investment\ProjectIco($id);

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $model->save();
            Yii::$app->session->setFlash('form', 1);

            return $this->refresh();
        }

        return $this->render([
            'model' => $model,
        ]);
    }

    /**
     * Ставит флаг удаления
     *
     * @param $id
     * @return Response
     */
    public function actionDelete($id)
    {
        $project = Project::findOne($id);
        if (is_null($project)) {
            return self::jsonErrorId(101, 'Проекта не существует');
        }
        if ($project->user_id != Yii::$app->user->id) {
            return self::jsonErrorId(102, 'Это не ваш проект');
        }

        $project->mark_deleted = 1;
        $project->save();

        return self::jsonSuccess();
    }
}
