<?php

namespace avatar\controllers;

use avatar\models\forms\UserAvatar;
use avatar\models\validate\CardNumberAccept;
use avatar\modules\ETH\ServiceEtherScan;
use avatar\modules\ETH\ServiceEthPlorer;
use common\components\Card;
use common\components\providers\BTC;
use common\components\providers\ETH;
use common\models\avatar\Currency;
use common\models\avatar\UserBill;
use common\models\Config;
use common\models\school\Kurs;
use common\models\school\Potok;
use common\models\school\PotokUser3Link;
use common\models\school\PotokUserExt;
use common\models\school\School;
use common\models\school\UserLink;
use common\models\UserRoot;
use common\models\UserSeed;
use common\payment\BitCoinBlockTrailPayment;
use common\services\Security;
use console\controllers\MoneyRateController;
use cs\services\VarDumper;
use cs\web\Exception;
use cs\Widget\FileUpload4\ModelFields;
use Yii;
use yii\helpers\FileHelper;
use yii\helpers\Json;
use yii\helpers\StringHelper;
use yii\httpclient\Client;
use yii\imagine\Image;
use yii\web\Response;

class CabinetSchoolPotokSaleController extends CabinetSchoolBaseController
{

    /**
     * @param int $id идентификатор потока school_potok.id
     *
     * @return string
     * @throws
     */
    public function actionIndex($id)
    {
        $potok = Potok::findOne($id);
        if (is_null($potok)) {
            throw new Exception('Не найден поток');
        }
        $kurs = Kurs::findOne($potok->kurs_id);
        if (is_null($kurs)) {
            throw new Exception('Не найден курс');
        }
        $this->isAccess($kurs->school_id);
        $school = School::findOne($kurs->school_id);

        return $this->render([
            'school' => $school,
            'kurs'   => $kurs,
            'potok'  => $potok,
        ]);
    }

    /**
     * @param int $id идентификатор потока school_potok.id
     *
     * @return string
     * @throws
     */
    public function actionAdd($id)
    {
        $model = new \common\models\school\SchoolSale();

        $potok = Potok::findOne($id);
        if (is_null($potok)) {
            throw new Exception('Не найден поток');
        }
        $kurs = Kurs::findOne($potok->kurs_id);
        if (is_null($kurs)) {
            throw new Exception('Не найден курс');
        }
        $this->isAccess($kurs->school_id);
        $school = School::findOne($kurs->school_id);

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $model->potok_id = $id;
            $model->school_id = $school->id;
            $model->save();
            Yii::$app->session->setFlash('form', $model::getDb()->lastInsertID);
        }

        return $this->render([
            'model'  => $model,
            'school' => $school,
            'kurs'   => $kurs,
            'potok'  => $potok,
        ]);
    }

    /**
     * @param int $id идентификатор продажи school_kurs_sale.id
     *
     * @return string
     * @throws
     */
    public function actionEdit($id)
    {
        $model = \common\models\school\SchoolSale::findOne($id);

        $potok = Potok::findOne($model->potok_id);
        if (is_null($potok)) {
            throw new Exception('Не найден поток');
        }
        $kurs = Kurs::findOne($potok->kurs_id);
        if (is_null($kurs)) {
            throw new Exception('Не найден курс');
        }
        $this->isAccess($kurs->school_id);
        $school = School::findOne($kurs->school_id);


        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $model->save();
            Yii::$app->session->setFlash('form', $model->id);
        }

        return $this->render([
            'model'  => $model,
            'school' => $school,
            'kurs'   => $kurs,
            'potok'  => $potok,
        ]);
    }


    public function actionDelete($id)
    {
        $model = \common\models\school\SchoolSale::findOne($id);

        $potok = Potok::findOne($id);
        if (is_null($potok)) {
            throw new Exception('Не найден поток');
        }
        $kurs = Kurs::findOne($potok->kurs_id);
        if (is_null($kurs)) {
            throw new Exception('Не найден курс');
        }
        $this->isAccess($kurs->school_id);

        $model->delete();

        return self::jsonSuccess();
    }
}
