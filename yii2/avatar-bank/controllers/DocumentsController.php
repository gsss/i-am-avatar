<?php

namespace avatar\controllers;

use app\models\Article;
use app\models\SiteUpdate;
use app\services\Subscribe;
use avatar\models\forms\Contact;
use avatar\models\forms\DocumentsRead;
use avatar\services\LogReader;
use common\components\sms\IqSms;
use common\models\avatar\Currency;
use common\models\avatar\UserBill;
use common\models\PaymentBitCoin;
use common\models\UserAvatar;
use common\models\UserDocument;
use common\models\UserRegistration;
use common\payment\BitCoinBlockTrailPayment;
use cs\Application;
use cs\base\BaseController;
use cs\services\UploadFolderDispatcher;
use cs\services\VarDumper;
use cs\web\Exception;
use Yii;
use yii\base\UserException;
use yii\bootstrap\ActiveForm;
use yii\db\Connection;
use yii\db\Query;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use yii\helpers\FileHelper;
use yii\helpers\Url;
use yii\web\Response;

class DocumentsController extends \avatar\base\BaseController
{

    /**
     * Выдает документ на скачивание
     *
     * @return string
     * @throws \cs\web\Exception
     */
    public function actionRead($id)
    {
        $model = new DocumentsRead(['id' => $id]);

        return $this->render([
            'model' => $model,
        ]);
    }

    /**
     * @return string
     * @throws \cs\web\Exception
     */
    public function actionReadAjax()
    {
        $model = new DocumentsRead();

        $model->load(Yii::$app->request->post());
        if (!$model->validate()) {
            return self::jsonErrorId(101, $model->convert($model->errors));
        }

        return self::jsonSuccess(['html' => \yii\helpers\VarDumper::dumpAsString($model->action(),6, true)]);
    }

    /**
     * Выдает документ на скачивание
     *
     * @return string
     * @throws \cs\web\Exception
     */
    public function actionDownloadPdf($id)
    {
        $doc = UserDocument::findOne($id);
        if (is_null($doc)) {
            throw new Exception('Не найден документ');
        }
        $info = pathinfo($doc->file);

        require \Yii::getAlias('@common/components/mpdf60/mpdf.php');
        $pdfDriver = new \mPDF();
        $filePath = \Yii::getAlias('@avatar/views/documents/pdf.php');
        $contentHtml = \Yii::$app->view->renderFile($filePath, ['document' => $doc]);
        $pdfDriver->WriteHTML($contentHtml);
        $contentPdf = $pdfDriver->Output('', 'S');

        return Yii::$app->response->sendContentAsFile($contentPdf, 'document' . '_' . $id . '.pdf', [
            'mimeType' => 'application/pdf',
        ]);
    }

    /**
     * Выдает документ на скачивание
     *
     * @return string
     * @throws \cs\web\Exception
     */
    public function actionDownload($id)
    {
        $doc = UserDocument::findOne($id);
        if (is_null($doc)) {
            throw new Exception('Не найден документ');
        }
        $info = pathinfo($doc->file);

        return Yii::$app->response->sendContentAsFile(file_get_contents(Yii::getAlias('@webroot' . $doc->file)), $doc->id . '.'  . $info['extension']);
    }

    /**
     * Показывает документ
     *
     * @return string
     * @throws Exception
     */
    public function actionIndex()
    {
        $id = self::getParam('id');
        if (is_null($id)) {
            return $this->render('index');
        }
        $document = UserDocument::findOne($id);
        if (is_null($document)) {
            throw new Exception('Нет такого документа');
        }

        return $this->render('view', [
            'document' => $document,
        ]);
    }

    /**
     * Показывает форму для подписи
     *
     *
     * @return string
     * @throws Exception
     */
    public function actionAddSignature($id)
    {
        return $this->render(['id' => $id]);
    }

    /**
     * подписывает документ
     *
     * REQUEST
     * - id - int - идентификатор документа
     * - password - int - идентификатор документа
     *
     * @return string
     * @throws Exception
     */
    public function actionAddSignatureAjax()
    {
        $model = new \avatar\models\forms\UserDocumentAddSignature();

        if (!$model->load(Yii::$app->request->post())) {
            return self::jsonErrorId(101, 'Не загружены данные');
        }
        if (!$model->validate()) {
            return self::jsonErrorId(102, $model->convert($model->errors));
        }
        $txid = $model->action();

        return self::jsonSuccess(['txid' => $txid]);
    }
}
