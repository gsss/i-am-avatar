<?php

namespace avatar\controllers;

use common\models\avatar\UserBill;
use common\models\avatar\UserBillSystem;
use console\controllers\MoneyRateController;
use cs\web\Exception;
use Yii;
use yii\filters\AccessControl;
use yii\helpers\FileHelper;
use yii\helpers\Html;
use yii\web\Response;

class AdminPartnerController extends \avatar\base\BaseController
{
    public static $isStatistic = false;

    public $layout = 'cabinet';

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow'   => true,
                        'roles'   => ['role_admin', 'role_partner-admin'],
                    ],
                ],
            ],
        ];
    }

    public function actions()
    {
        return [
            'transactions-list' => [
                'class' => 'avatar\controllers\actions\AdminPartnerController\TransactionsList'
            ]
        ];
    }


    /**
     */
    public function actionIndex()
    {
        return $this->render();
    }


    /**
     * Показывает карты которые можно скачать
     *
     */
    public function actionCards()
    {
        return $this->render(['billing' => $this->getBill()]);
    }

    /**
     * AJAX
     * REQUEST:
     *
     * @return string json response
     * [
     *      'confirmedBalance'   => BTC
     *      'unconfirmedBalance' => BTC
     *      'all'                => mBTC
     * ]
     */
    public function actionBalanceInternal($id)
    {
        if (YII_ENV_DEV) return self::jsonSuccess([
            'confirmedBalance'   => 0,
            'unconfirmedBalance' => 0,
            'all'                => 0
        ]);
        list($confirmedBalance, $unconfirmedBalance) = $this->getBill($id)->getWallet()->getBalance();
        $confirmedBalance1 = ($confirmedBalance + $unconfirmedBalance) / 100000;

        return self::jsonSuccess([
            'confirmedBalance'   => $confirmedBalance / 100000000,
            'unconfirmedBalance' => $unconfirmedBalance / 100000000,
            'all'                => $confirmedBalance1
        ]);
    }

    /**
     * AJAX
     * Создает кошелек
     *
     * REQUEST:
     * - name string - название счета
     *
     */
    public function actionNew()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $name = self::getParam('name');

        /** @var \common\components\providers\BTC $BTC */
        $BTC = \Yii::$app->BTC;
        $wallet = $BTC->create($name);
        (new UserBillSystem(['id' => $wallet->billing->id]))->save();

        return self::jsonSuccess([
            'id'      => $wallet->billing->id,
            'address' => $wallet->billing->address,
        ]);
    }

    /**
     * Получает счет ID
     * Делает проверку на хозяина
     *
     * @return \common\models\avatar\UserBill
     * @throws \yii\base\Exception
     */
    private function getBill($id = null)
    {
        if (is_null($id)) {
            $id = self::getParam('id');
        }
        $bill = UserBill::findOne($id);

        return $bill;
    }

    /**
     * AJAX
     * Выдает адрес потоянный для приема платежа
     * REQUEST:
     * - id - int
     *
     * @return Response json
     * [
     *      'html' => string
     * ]
     */
    public function actionIn()
    {
        $id = self::getParam('id');
        $billing = UserBill::findOne($id);
        $wallet = $billing->getWalletAvatar();
        $address = $wallet->getNewAddress();
        $src = 'data:image/png;base64,' . base64_encode(
                (new \Endroid\QrCode\QrCode())
                    ->setText('bitcoin:' . $address)
                    ->setSize(180)
                    ->setPadding(0)
                    ->setErrorCorrection('high')
                    ->setForegroundColor(['r' => 0, 'g' => 0, 'b' => 0, 'a' => 0])
                    ->setBackgroundColor(['r' => 255, 'g' => 255, 'b' => 255, 'a' => 0])
                    ->setLabelFontSize(16)
                    ->setImageType(\Endroid\QrCode\QrCode::IMAGE_TYPE_PNG)
                    ->get('png')
            );
        $rows = [];
        $rows[] = Html::tag('p', Html::img($src));
        $rows[] = Html::tag('p', Html::tag('code', $address));

        return self::jsonSuccess([
            'html' => join("\n", $rows),
        ]);
    }

    /**
     * AJAX
     * Отправляет деньги
     * REQUEST:
     * - address - string - адрес может быть или целым адресом или в формате bitcoin:rtyertyertytr?i-am-avatar=
     * - amount - string - сколько (разделитель - точка)
     * - comment - string
     * - password - string
     *
     * @return Response
     */
    public function actionSend()
    {
        $model = new \avatar\models\forms\PiramidaSend();
        $model->load(Yii::$app->request->post(), '');
        if (!$model->validate()) {
            return self::jsonErrorId(101, $this->convert($model->errors));
        }
        $transaction = $model->send();
        if ($transaction === false) {
            return self::jsonErrorId(101, $this->convert($model->errors));
        }

        return self::jsonSuccess([
            'address' => $transaction,
        ]);
    }

    /**
     */
    public function actionTransactions($id)
    {
        return $this->render('transactions', ['billing' => $this->getBill()]);
    }

    /**
     * Конвертирует массив ошибок от формы чтобы их можно было выдать через JSON
     *
     * @param array $params
     * [
     *      'amount' => ['Не верное число',...],
     * ]
     * @return array
     * [
     *      'name'  => 'amount',
     *      'value' => ['Не верное число',...],
     * ]
     */
    private function convert($params)
    {
        $ret = [];
        foreach ($params as $name => $arr) {
            $ret[] = [
                'name'  => $name,
                'value' => $arr,
            ];
        }
        return $ret;
    }
    /**
     * Возвращает баланс в счете
     * AJAX
     * REQUEST:
     * - id - int - идентификатор счета
     *
     * @return string json array
     * [
     *      '<confirmed>',   (BTC)
     *      '<unconfirmed>', (BTC)
     *      '<confirmed>',   (валюта установленная в качетсве просмотра)
     *      '<unconfirmed>', (валюта установленная в качетсве просмотра)
     *      '<currencyView>', установленный параметр просмотра валюты, если 0 значит нет и 3 и 4 параметры можно не учитывать
     * ]
     */
    public function actionGetBalance()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        if (YII_ENV_DEV) return self::jsonSuccess([0, 0]);
        $bill = $this->getBill();
        $wallet = $bill->getWallet();
        $balance = $wallet->getBalance();

        $currencyView = Yii::$app->user->identity->currency_view;
        if (is_null($currencyView)) $currencyView = 0;
        $confirmed = 0;
        $unconfirmed = 0;
        switch ($currencyView) {
            case 1: // RUB
                $rate = \common\models\Config::get(MoneyRateController::RATE_BTC_RUB);
                $confirmed = Yii::$app->formatter->asDecimal($rate * ($balance[0] / 100000000) , 2);
                $unconfirmed = Yii::$app->formatter->asDecimal($rate * ($balance[1] / 100000000) , 2);
                break;
            case 2: // USD
                $rate = \common\models\Config::get(MoneyRateController::RATE_BTC_USD);
                $confirmed = Yii::$app->formatter->asDecimal($rate * ($balance[0] / 100000000) , 2);
                $unconfirmed = Yii::$app->formatter->asDecimal($rate * ($balance[1] / 100000000) , 2);
                break;
        }

        return self::jsonSuccess(
            [
                $balance[0] / 100000000,
                $balance[1] / 100000000,
                $confirmed,
                $unconfirmed,
                $currencyView,
            ]
        );
    }

}
