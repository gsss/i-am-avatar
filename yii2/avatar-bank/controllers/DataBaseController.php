<?php

namespace avatar\controllers;

use app\models\Article;
use app\models\SiteUpdate;
use app\services\Subscribe;
use common\models\avatar\UserBill;
use common\models\AvatarHash;
use common\widgets\FileUpload3\FileUpload;
use cs\base\BaseController;
use cs\services\UploadFolderDispatcher;
use cs\services\VarDumper;
use cs\web\Exception;
use Yii;
use yii\base\UserException;
use yii\db\Connection;
use yii\db\Query;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use yii\helpers\FileHelper;
use yii\web\Response;

class DataBaseController extends \avatar\base\BaseController
{
    /**
     */
    public function actionAvatar()
    {
        return $this->render();
    }

    public function actionAvatarAdd()
    {
        $model = new \avatar\models\forms\AvatarHash();

        if ($model->load(Yii::$app->request->post()) && $model->insert()) {
            Yii::$app->session->setFlash('form', $model->id);
            $i = \common\models\AvatarHash::findOne($model->id);
            $i->created_at = time();
            $content = file_get_contents(Yii::getAlias('@webroot' . FileUpload::getOriginal($i->image)));
            $i->hash = hash('sha256', $content . $i->data);
            $i->save();

            return $this->refresh();
        } else {
            return $this->render('avatar-add', [
                'model' => $model,
            ]);
        }
    }

    public function actionAvatarView($id)
    {
        $model = AvatarHash::findOne($id);

        return $this->render('avatar-view', [
            'model' => $model,
        ]);
    }
}
