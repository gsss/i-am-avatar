<?php

namespace avatar\controllers;

use app\services\Subscribe;
use avatar\controllers\actions\DefaultAjax;
use avatar\controllers\AdminBaseController;
use avatar\models\forms\BlogItem;
use common\models\avatar\UserBill;
use common\models\investment\IcoRequest;
use common\models\investment\IcoRequestAddress;
use common\models\investment\IcoRequestSuccess;
use common\models\investment\Project;
use common\models\investment\ProjectIco;
use common\models\MapBankomat;
use common\models\PaySystemConfig;
use common\models\PaySystemListItem;
use cs\Application;
use cs\services\VarDumper;
use cs\web\Exception;
use Yii;
use yii\base\UserException;
use yii\filters\AccessControl;
use yii\helpers\StringHelper;
use yii\helpers\Url;
use yii\web\Controller;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use app\models\NewsItem;
use app\models\Chenneling;
use cs\base\BaseController;
use yii\web\Response;
use yii\widgets\ActiveForm;
use app\models\SiteUpdate;

class InputController extends \avatar\controllers\CabinetBaseController
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => '\yii\filters\AccessControl',
                'only'  => [
                    'request-create',
                    'index',
                    'pay',
                ],
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    public function actions()
    {
        return [
            'request-create' => [
                'class'    => '\avatar\controllers\actions\DefaultAjax',
                'model'    => '\avatar\models\validate\CabinetInputRequestCreate',
                'formName' => 'CabinetInputRequestCreate'
            ],
            'login'          => [
                'class'    => '\avatar\controllers\actions\DefaultAjax',
                'model'    => '\avatar\models\forms\shop\ShopLogin',
                'formName' => 'ShopLogin'
            ],
            'registration'   => [
                'class'    => '\avatar\controllers\actions\DefaultAjax',
                'model'    => '\avatar\models\forms\shop\ShopRegistration',
                'formName' => 'ShopRegistration'
            ],
        ];
    }

    public function actionIndex()
    {
        return $this->render();
    }

    public function actionSuccess($id)
    {
        $model = \common\models\school\RequestInput::findOne($id);
        if (is_null($model)) {
            throw new \Exception('Не найдена заявка');
        }

        return $this->render([
            'request' => $model,
        ]);
    }

    public function actionWait($id)
    {
        $model = \common\models\school\RequestInput::findOne($id);
        if (is_null($model)) {
            throw new \Exception('Не найдена заявка');
        }

        return $this->render([
            'request' => $model,
        ]);
    }

    public function actionPay($id)
    {
        $model = \common\models\school\RequestInput::findOne($id);
        if (is_null($model)) {
            throw new \Exception('Не найдена заявка');
        }

        return $this->render([
            'request' => $model,
        ]);
    }

}
