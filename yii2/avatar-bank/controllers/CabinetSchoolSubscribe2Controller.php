<?php

namespace avatar\controllers;

use avatar\models\forms\UserAvatar;
use avatar\models\validate\CardNumberAccept;
use avatar\modules\ETH\ServiceEtherScan;
use avatar\modules\ETH\ServiceEthPlorer;
use common\components\Card;
use common\components\providers\BTC;
use common\components\providers\ETH;
use common\models\avatar\Currency;
use common\models\avatar\UserBill;
use common\models\Config;
use common\models\school\Kurs;
use common\models\school\Potok;
use common\models\school\PotokUser3Link;
use common\models\school\PotokUserLink;
use common\models\school\School;
use common\models\school\Subscribe;
use common\models\school\Subscribe2;
use common\models\school\SubscribeItem;
use common\models\subscribe\SubscribeMail;
use common\models\UserSeed;
use common\payment\BitCoinBlockTrailPayment;
use common\services\Security;
use console\controllers\MoneyRateController;
use cs\services\VarDumper;
use cs\web\Exception;
use cs\Widget\FileUpload4\ModelFields;
use Yii;
use yii\helpers\FileHelper;
use yii\helpers\Json;
use yii\helpers\StringHelper;
use yii\httpclient\Client;
use yii\imagine\Image;
use yii\web\Response;

class CabinetSchoolSubscribe2Controller extends CabinetSchoolBaseController
{

    /**
     * @param int $id идентификатор школы school.id
     *
     * @return string
     */
    public function actionIndex($id)
    {
        $this->isAccess($id);
        $school = School::findOne($id);

        return $this->render([
            'school' => $school,
        ]);
    }

    public function actionAdd($id)
    {
        $this->isAccess($id);
        $school = School::findOne($id);
        $model = new \avatar\models\forms\school\Subscribe2();

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            Yii::$app->session->setFlash('form', 1);
            $model->insert();

            $i = Subscribe2::findOne($model->id);
            $i->created_at = time();
            $i->school_id = $id;
            $i->save();
        }

        return $this->render([
            'model'  => $model,
            'school' => $school,
        ]);
    }

    public function actionEdit($id)
    {
        $model = \avatar\models\forms\school\Subscribe2::findOne($id);
        $this->isAccess($model->school_id);
        $school = School::findOne($model->school_id);

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $model->update();
            Yii::$app->session->setFlash('form', 1);
        }

        return $this->render([
            'model'  => $model,
            'school' => $school,
        ]);
    }


    /**
     * Отсылает хозяину школы
     *
     * REQUEST:
     * - id - int -
     * - email - string -
     *
     * @return string
     */
    public function actionSubscribeOne()
    {
        $id = self::getParam('id');
        $model = Subscribe2::findOne($id);
        $school = School::findOne($model->school_id);
        $this->isAccess($school->id);

        $email = $school->getUser()->email;
        $options = [
            'layout' => 'layouts/html/subscribe',
        ];
        if ($school->subscribe_from_email) $options['image'] = $school->subscribe_image;
        if ($school->subscribe_from_email) $options['from_email'] = $school->subscribe_from_email;
        if ($school->subscribe_from_name) $options['from_name'] = $school->subscribe_from_name;

        \common\services\Subscribe::sendArray(
            [$email],
            $model->subject,
            'subscribe/manual2',
            ['item' => $model],
            $options
        );

        self::jsonSuccess();
    }

    /**
     * Отсылает хозяину школы
     *
     * REQUEST:
     * - id - int -
     * - email - string -
     *
     * @return string
     */
    public function actionSubscribeMe()
    {
        $id = self::getParam('id');
        $model = Subscribe2::findOne($id);
        $school = School::findOne($model->school_id);
        $this->isAccess($school->id);

        $email = Yii::$app->user->identity->email;
        $options = [
            'layout' => 'layouts/html/subscribe',
        ];
        if ($school->subscribe_image) $options['image'] = $school->subscribe_image;
        if ($school->subscribe_from_email) $options['from_email'] = $school->subscribe_from_email;
        if ($school->subscribe_from_name) $options['from_name'] = $school->subscribe_from_name;

        \common\services\Subscribe::sendArray(
            [$email],
            $model->subject,
            'subscribe/manual2',
            [
                'item'  => $model,
                'image' => $options['image'],
            ],
            $options
        );

        self::jsonSuccess();
    }

    /**
     * @param int $id
     *
     * @return Response
     */
    public function actionSubscribe($id)
    {
        $id = self::getParam('id');
        $model = Subscribe2::findOne($id);
        $school = School::findOne($model->school_id);
        $this->isAccess($school->id);

        $emailList = \common\models\UserRoot::find()
            ->innerJoin('user_school_link', 'user_school_link.user_root_id = user_root.id')
            ->where(['user_school_link.school_id' => $school->id])
            ->select([
                'user_root.email',
            ])
            ->column();

        $options = [
            'layout' => 'layouts/html/subscribe',
        ];
        if ($school->subscribe_from_email) $options['image'] = $school->subscribe_image;
        if ($school->subscribe_from_email) $options['from_email'] = $school->subscribe_from_email;
        if ($school->subscribe_from_name) $options['from_name'] = $school->subscribe_from_name;

        $subscribeProcess = \common\services\Subscribe::sendArray(
            $emailList,
            $model->subject,
            'subscribe/manual2',
            [
                'item'  => $model,
                'image' => $options['image'],
            ],
            $options
        );

        SubscribeItem::add([
            'subscribe_id'    => $model->id,
            'created_at'      => time(),
            'gs_subscribe_id' => $subscribeProcess->id,
        ]);

        return self::jsonSuccess();
    }


    public function actionDelete($id)
    {
        $original = \avatar\models\forms\school\Subscribe::findOne($id);
        $Potok = Potok::findOne($original->potok_id);
        $Kurs = Kurs::findOne($Potok->kurs_id);
        $this->isAccess($Kurs->school_id);
        $original->delete();

        return self::jsonSuccess();
    }
}
