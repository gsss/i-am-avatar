<?php

namespace avatar\controllers;

use app\common\components\Piramida;
use app\models\Article;
use app\models\Form\Shop\Order;
use app\models\Piramida\WalletSource\Custom;
use app\models\Piramida\WalletSource\Pochta;
use app\models\Piramida\WalletSource\Yandex;
use app\models\Shop;
use app\models\Shop\Product;
use app\models\Shop\Request;
use app\models\Shop\RequestMessage;
use app\models\Shop\RequestProduct;
use app\models\Union;
use app\models\User;
use app\modules\Shop\services\Basket;
use common\models\school\School;
use cs\Application;
use cs\services\VarDumper;
use cs\web\Exception;
use Yii;
use yii\base\UserException;
use yii\bootstrap\Html;
use yii\db\Query;


/**
 * Обслуживает действия магазина
 *
 * Class Cabinet_shop_shopController
 * @package app\controllers
 */
class CabinetSchoolShopGoodsController extends \avatar\controllers\CabinetSchoolBaseController
{
    public $type_id = 5;

    /**
     *
     * @param int $id school.id
     */
    public function actionIndex($id)
    {
        $this->isAccess($id);
        $school = School::findOne($id);

        return $this->render(['school' => $school]);
    }


    /**
     * Добавление товара
     * @param int $id идентификатор объединения gs_unions.id
     * @return string|\yii\web\Response
     * @throws \cs\web\Exception
     */
    public function actionAdd($id)
    {
        $this->isAccess($id);
        $model = new \avatar\models\forms\shop\Product(['_school_id' => $id, '_type_id' => $this->type_id]);
        $school = School::findOne($id);

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->price) {
                $model->price = $model->price * 100;
            }
            $model->insert();
            Yii::$app->session->setFlash('form');

            $p = \common\models\shop\Product::findOne($model->id);

            $p->date_insert = time();
            $p->school_id = $id;
            $p->save();
        }

        return $this->render([
            'model'  => $model,
            'school' => $school,
        ]);
    }

    /**
     * Редактирование товара
     *
     * @param int $id идентификатор товара gs_unions_shop_product.id
     *
     * @return string|\yii\web\Response
     *
     * @throws
     */
    public function actionEdit($id)
    {
        $p = \common\models\shop\Product::findOne($id);
        $model = \avatar\models\forms\shop\Product::findOneWithInit($id, [
            '_school_id' => $p->school_id,
            '_type_id'   => $this->type_id,
        ]);
        if ($model->price) {
            $model->price = $model->price / 100;
        }
        $school = School::findOne($p->school_id);
        $this->isAccess($p->school_id);

        if ($model->load(Yii::$app->request->post()) && $model->validate($id)) {
            if ($model->price) {
                $model->price = $model->price * 100;
            }
            $model->update($id);
            Yii::$app->session->setFlash('form');
        }

        return $this->render([
            'model'  => $model,
            'school' => $school,
        ]);
    }

    public function actionDelete($id)
    {
        $product = \avatar\models\forms\shop\Product::findOne($id);
        if (is_null($product)) throw new \Exception('Не найден $potok');
        $this->isAccess($product->school_id);

        $product->delete();

        return self::jsonSuccess();
    }

}
