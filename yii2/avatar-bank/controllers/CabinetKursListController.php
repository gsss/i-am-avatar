<?php

namespace avatar\controllers;

use avatar\models\forms\UserAvatar;
use avatar\models\validate\CardNumberAccept;
use avatar\modules\ETH\ServiceEtherScan;
use avatar\modules\ETH\ServiceEthPlorer;
use common\components\Card;
use common\components\providers\BTC;
use common\components\providers\ETH;
use common\models\avatar\Currency;
use common\models\avatar\UserBill;
use common\models\Config;
use common\models\school\Kurs;
use common\models\school\Lesson;
use common\models\school\LessonPotokState;
use common\models\school\Potok;
use common\models\school\PotokUser3Link;
use common\models\school\PotokUserExt;
use common\models\school\School;
use common\models\UserSeed;
use common\payment\BitCoinBlockTrailPayment;
use common\services\Security;
use console\controllers\MoneyRateController;
use cs\services\VarDumper;
use cs\web\Exception;
use cs\Widget\FileUpload4\ModelFields;
use Yii;
use yii\db\Query;
use yii\helpers\FileHelper;
use yii\helpers\Json;
use yii\helpers\StringHelper;
use yii\httpclient\Client;
use yii\imagine\Image;
use yii\web\Response;

class CabinetKursListController extends \avatar\controllers\CabinetBaseController
{

    /**
     * @return string
     */
    public function actionIndex()
    {
        return $this->render();
    }

    /**
     * @return string
     */
    public function actionLessonList($id)
    {
        $Potok = Potok::findOne($id);

        return $this->render(['Potok' => $Potok]);
    }

    /**
     * @param int $id school_urok_potok_state.id
     * @return string
     */
    public function actionLessonView($id)
    {
        $state = LessonPotokState::findOne($id);
        $Lesson = Lesson::findOne($state->lesson_id);
        $kurs = Kurs::findOne($Lesson->kurs_id);

        return $this->render([
            'kurs'   => $kurs,
            'lesson' => $Lesson,
        ]);
    }

    /**
     * @param int $id идентификатор курса
     *
     * @return string
     */
    public function actionDelete($id)
    {
        $kurs = Kurs::findOne($id);
        $lessonList = \avatar\models\forms\school\Lesson::find()->where(['kurs_id' => $id])->all();
        foreach ($lessonList as $lesson) {
            $lesson->delete();
        }
        $potokList = Potok::find()->where(['kurs_id' => $id])->all();
        foreach ($potokList as $potok) {
            $potok->delete();
            $linkList = PotokUser3Link::find()->where(['potok_id' => $id])->select(['id'])->column();
            PotokUserExt::deleteAll(['link_id' => $linkList]);
            PotokUser3Link::deleteAll(['id' => $linkList]);
        }
        $kurs->delete();

        return self::jsonSuccess();
    }
}
