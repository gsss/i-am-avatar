<?php

namespace avatar\controllers;

use avatar\models\forms\UserAvatar;
use avatar\models\validate\CardNumberAccept;
use avatar\modules\ETH\ServiceEtherScan;
use avatar\modules\ETH\ServiceEthPlorer;
use common\components\Card;
use common\components\providers\BTC;
use common\components\providers\ETH;
use common\models\avatar\Currency;
use common\models\avatar\CurrencyLink;
use common\models\avatar\UserBill;
use common\models\Config;
use common\models\piramida\Wallet;
use common\models\school\Input;
use common\models\school\InputTransaction;
use common\models\school\Kurs;
use common\models\school\Lesson;
use common\models\school\LessonPage;
use common\models\school\LessonPlace;
use common\models\school\LessonVideo;
use common\models\school\Master;
use common\models\school\Potok;
use common\models\school\School;
use common\models\school\Subscribe;
use common\models\school\SubscribeItem;
use common\models\subscribe\SubscribeMail;
use common\models\UserRoot;
use common\models\UserSeed;
use common\payment\BitCoinBlockTrailPayment;
use common\services\Security;
use console\controllers\MoneyRateController;
use cs\Application;
use cs\services\VarDumper;
use cs\web\Exception;
use cs\Widget\FileUpload4\ModelFields;
use Yii;
use yii\helpers\ArrayHelper;
use yii\helpers\FileHelper;
use yii\helpers\Json;
use yii\helpers\StringHelper;
use yii\httpclient\Client;
use yii\imagine\Image;
use yii\web\Response;

class CabinetSchoolWalletsController extends CabinetSchoolBaseController
{

    /**
     * @return string
     */
    public function actionIndex($id)
    {
        $this->isAccess($id);
        $school = School::findOne($id);

        return $this->render(['school' => $school]);
    }

    /**
     * Создает монету для школы
     *
     * @return string
     */
    public function actionAddCurrency($id)
    {
        $this->isAccess($id);
        $school = School::findOne($id);

        return $this->render(['school' => $school]);
    }

    /**
     * @param int $id wallet.id
     * @return string
     * @throws
     */
    public function actionView($id)
    {
        $Wallet = Wallet::findOne($id);
        if (!Application::isInteger($id)) throw new Exception('В параметре должно быть целое число');
        if (is_null($Wallet)) throw new Exception('Кошелек не найден');

        return $this->render([
            'wallet' => $Wallet,
        ]);
    }

    /**
     * Создает монету для школы
     *
     * @return string
     */
    public function actionItem($id)
    {
        $input = Input::findOne($id);
        $this->isAccess($input->school_id);
        $school = School::findOne($input->school_id);

        return $this->render([
            'input'  => $input,
            'school' => $school,
        ]);
    }

    /**
     * Создает монету для школы
     *
     * @return string
     */
    public function actionAddInput($id)
    {
        $this->isAccess($id);
        $school = School::findOne($id);

        $model = new Input();

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            Yii::$app->session->setFlash('form', 1);
            $model->school_id = $id;
            $model->save();
            $model->id = $model::getDb()->lastInsertID;

            // атомов поступило
            $amountInput = $model->amount;

            $wallet1 = Wallet::findOne($school->fund_rub);
            $wallet2 = Wallet::findOne($school->fund_rub2);

            // Нахожу у кого висят Эликсиры небесные
            $amount20 = (int)($amountInput * 0.2);

            $arr = $this->getBills();
            $rowsReturn = $arr[0];
            // атомов Сколько должны аватарам
            $amountUsers = $arr[1];

            // $amountUsers < amount20 то $amountUsersReturn - денег на распределение = $amountUsers
            if ($amountUsers < $amount20) {
                // атомов будем раздавать на аватаров
                $amountUsersReturn = $amountUsers;
            } else {
                $amountUsersReturn = $amount20;
            }
            $amountFund = $amountInput;

            /**
             * @var array
             * [
             *  user_id => amount
             * ]
             */
            $rows = [];
            foreach ($rowsReturn as $user_id => $w) {
                // сколько аватару надо выплатить
                $d = $w['diff'];

                // сколько надо положить пропорционально
                $b = (int)($amountUsersReturn * ($d / $amountUsers));

                // вычитаю из фонда чтобы скинуть туда что останется
                $amountFund -= $b;
                $rows[$user_id] = $b;
            }

            $op20 = $wallet1->in($amountInput - $amountFund, 'Приход #' . $model->id);
            foreach ($rows as $user_id => $amount) {
                $w = $rowsReturn[$user_id];
                $walletRub = $w['rubWallet'];
                $t = $wallet1->move($walletRub, $amount, 'Приход #' . $model->id . ' Для Аватара id=' . $user_id);
                InputTransaction::add([
                    'input_id'       => $model->id,
                    'transaction_id' => $t->id,
                    'balance_elxsky' => $w['elxSkyWallet']->amount,
                    'balance_rub'    => $walletRub->amount,
                ]);
            }

            // эмиттирую 80%, а точнее что осталось.
            $op80 = $wallet2->in($amountFund, 'Приход #' . $model->id);
            $model->emission_operation_id = $op20->id;
            $model->emission2_operation_id = $op80->id;
            $model->save();
        }

        return $this->render(['school' => $school, 'model' => $model]);
    }

    /**
     * @return array
     * [
     *      [
     *          user_id => [
     *              rubWallet
     *              rubBill
     *              elxSkyWallet
     *              elxSkyBill
     *              diff
     *          ]
     *      ]
     *      $amountUsers
     * ]
     */
    private function getBills()
    {
        $elxSkyList = UserBill::find()->where(['currency' => 2])->andWhere(['mark_deleted' => 0])->andWhere(['is_default' => 1])->all();
        $elxSkyAddress = ArrayHelper::getColumn($elxSkyList, 'address');
        $elxSkyWallet = Wallet::find()->where(['in', 'id', $elxSkyAddress])->all();
        $rubList = UserBill::find()->where(['currency' => 3])->andWhere(['mark_deleted' => 0])->andWhere(['is_default' => 1])->all();
        $rubAddress = ArrayHelper::getColumn($rubList, 'address');
        $rubWallet = Wallet::find()->where(['in', 'id', $rubAddress])->all();

        $rowsReturn = [];

        // атомов Сколько должны аватарам
        $amountUsers = 0;

        /** @var \common\models\avatar\UserBill $item */
        foreach ($elxSkyList as $item) {
            $item1 = ['elxSkyBill' => $item];
            /** @var \common\models\piramida\Wallet $w */
            foreach ($elxSkyWallet as $w) {
                if ($w->id == $item->address) {
                    $item1['elxSkyWallet'] = $w;
                    break;
                }
            }
            if ($item1['elxSkyWallet']->amount > 0) {
                /** @var \common\models\avatar\UserBill $b */
                foreach ($rubList as $b) {
                    if ($b->user_id == $item->user_id) {
                        $item1['rubBill'] = $b;
                        break;
                    }
                }
                $rubWalletAmount = 0;
                if (isset($item1['rubBill'])) {
                    /** @var \common\models\piramida\Wallet $w */
                    foreach ($rubWallet as $w) {
                        if ($w->id == $item1['rubBill']->address) {
                            $item1['rubWallet'] = $w;
                            break;
                        }
                    }
                    $rubWalletAmount = $item1['rubWallet']->amount;
                } else {
                    $item1['rubWallet'] = Wallet::addNew(['currency_id' => 4]);
                    $item1['rubBill'] = UserBill::add([
                        'user_id'    => $item1['elxSkyBill']->user_id,
                        'currency'   => 3,
                        'address'    => $item1['rubWallet']->id,
                        'is_default' => 1,
                    ]);
                }

                if ($rubWalletAmount < $item1['elxSkyWallet']->amount) {
                    $item1['diff'] = $item1['elxSkyWallet']->amount - $rubWalletAmount;
                    $rowsReturn[$item1['elxSkyBill']->user_id] = $item1;
                    $amountUsers += $item1['diff'];
                } else {
                    // ничего не делать, все уже выплачено
                    $item1['diff'] = 0;
                }

            }
        }

        return [$rowsReturn, $amountUsers];
    }

    /**
     * @return string
     */
    public function actionAddFundRub($id)
    {
        $this->isAccess($id);
        $school = School::findOne($id);

        $ELXSKYRUB_id = 4; // ELXSKYRUB
        $RUB_id = 7; // RUB

        $wallet = Wallet::addNew(['currency_id' => $ELXSKYRUB_id]);
        $school->fund_rub = $wallet->id;
        $wallet2 = Wallet::addNew(['currency_id' => $RUB_id]);
        $school->fund_rub2 = $wallet2->id;
        $school->save();

        return self::jsonSuccess([
            'wallet'    => $wallet,
            'wallet2'   => $wallet2,
        ]);
    }
}
