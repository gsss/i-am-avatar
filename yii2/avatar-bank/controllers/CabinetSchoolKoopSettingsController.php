<?php

namespace avatar\controllers;

use avatar\models\forms\UserAvatar;
use avatar\models\UserEnter;
use common\models\school\AdminLink;
use common\models\school\School;
use common\services\FormAjax\ActiveForm;
use cs\Application;
use cs\services\VarDumper;
use iAvatar777\services\FormAjax\ActiveRecord;
use iAvatar777\services\FormAjax\DefaultFormAdd;
use iAvatar777\services\FormAjax\DefaultFormAjax;
use iAvatar777\services\FormAjax\DefaultFormDelete;
use Yii;
use yii\base\Model;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;

class CabinetSchoolKoopSettingsController extends CabinetSchoolBaseController
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => '\yii\filters\AccessControl',
                'rules' => [
                    [
                        'allow'         => true,
                        'matchCallback' => function ($rule, $action) {
                            if (in_array($action->id, [
                                'index',
                            ])) {

                                $school_id = Yii::$app->request->get('id');

                            }
                            if (!Application::isInteger($school_id)) throw new \Exception('$school_id не целое');

                            $isAdmin = AdminLink::find()
                                ->where([
                                    'user_id'   => Yii::$app->user->id,
                                    'school_id' => $school_id,
                                ])
                                ->exists();

                            return $isAdmin;
                        },
                    ],
                ],
            ],
        ];
    }


    public function actionIndex($id)
    {
        $s = School::findOne($id);
        $class = '\avatar\models\validate\SchoolKoop';

        /** @var Model | ActiveRecord $model */
        $model = \avatar\models\validate\SchoolKoop::findOne(['school_id' => $id]);
        if (is_null($model)) {
            $model = new \avatar\models\validate\SchoolKoop(['school_id' => $id]);
        }

        if (Yii::$app->request->isPost) {
            if ($model->load(Yii::$app->request->post()) && $model->validate()) {
                $s = $model->save();

                return self::jsonSuccess($s);
            } else {
                return self::jsonErrorId(102, $model->getErrors102());
            }
        }

        return $this->render([
            'model'  => $model,
            'school' => $s,
        ]);
    }
}
