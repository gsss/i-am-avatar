<?php

namespace avatar\controllers;

use avatar\models\forms\UserAvatar;
use avatar\models\validate\CardNumberAccept;
use avatar\modules\ETH\ServiceEtherScan;
use avatar\modules\ETH\ServiceEthPlorer;
use common\components\Card;
use common\components\providers\BTC;
use common\components\providers\ETH;
use common\models\avatar\Currency;
use common\models\avatar\UserBill;
use common\models\Config;
use common\models\school\Kurs;
use common\models\school\Potok;
use common\models\school\PotokUser3Link;
use common\models\school\PotokUserLink;
use common\models\school\School;
use common\models\school\Subscribe;
use common\models\school\SubscribeItem;
use common\models\subscribe\SubscribeMail;
use common\models\UserSeed;
use common\payment\BitCoinBlockTrailPayment;
use common\services\Security;
use console\controllers\MoneyRateController;
use cs\services\VarDumper;
use cs\web\Exception;
use cs\Widget\FileUpload4\ModelFields;
use Yii;
use yii\helpers\FileHelper;
use yii\helpers\Json;
use yii\helpers\StringHelper;
use yii\httpclient\Client;
use yii\imagine\Image;
use yii\web\Response;

class CabinetSchoolSubscribeController extends CabinetSchoolBaseController
{

    /**
     * @param int $id идентификатор потока school_potok.id
     *
     * @return string
     */
    public function actionIndex($id)
    {
        $Potok = Potok::findOne($id);
        $Kurs = Kurs::findOne($Potok->kurs_id);
        $school = School::findOne($Kurs->school_id);
        $this->isAccess($school->id);

        return $this->render([
            'potok'  => $Potok,
            'kurs'   => $Kurs,
            'school' => $school,
        ]);
    }


    public function actionAdd($id)
    {
        $model = new \avatar\models\forms\school\Subscribe();
        $Potok = Potok::findOne($id);
        $Kurs = Kurs::findOne($Potok->kurs_id);
        $school = School::findOne($Kurs->school_id);
        $this->isAccess($school->id);

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            Yii::$app->session->setFlash('form', 1);
            $model->insert();
            $i = Subscribe::findOne($model->id);
            $i->created_at = time();
            $i->potok_id = $id;
            $i->save();
        }

        return $this->render([
            'model'  => $model,
            'school' => $school,
            'potok'  => $Potok,
        ]);
    }


    public function actionEdit($id)
    {
        $model = \avatar\models\forms\school\Subscribe::findOne($id);
        $Potok = Potok::findOne($model->potok_id);
        $Kurs = Kurs::findOne($Potok->kurs_id);
        $school = School::findOne($Kurs->school_id);
        $this->isAccess($school->id);

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $model->update();
            Yii::$app->session->setFlash('form', 1);
        }

        return $this->render([
            'model'  => $model,
            'school' => $school,
            'potok'  => $Potok,
        ]);
    }

    public function actionView($id)
    {
        $model = Subscribe::findOne($id);
        $Potok = Potok::findOne($model->potok_id);
        $Kurs = Kurs::findOne($Potok->kurs_id);
        $school = School::findOne($Kurs->school_id);
        $this->isAccess($school->id);

        return $this->render([
            'model'  => $model,
            'school' => $school,
        ]);
    }

    /**
     * Отсылает хозяину школы
     *
     * REQUEST:
     * - id - int -
     * - email - string -
     *
     * @return string
     */
    public function mactionSubscribeOne()
    {
        $id = self::getParam('id');
        $model = Subscribe::findOne($id);
        $Potok = Potok::findOne($model->potok_id);
        $Kurs = Kurs::findOne($Potok->kurs_id);
        $this->isAccess($Kurs->school_id);
        $school = $Kurs->getSchool();
        $email = $school->getUser()->email;

        \common\services\Subscribe::sendArraySchool(
            $school,
            [$email],
            $model->subject,
            'subscribe/manual2',
            [
                'item'  => $model,
                'potok' => $Potok,
                'sid'   => 1,
            ],
            'layouts/html/subscribe'
        );

        self::jsonSuccess();
    }

    /**
     * Отсылает хозяину школы
     *
     * REQUEST:
     * - id - int -
     * - email - string -
     *
     * @return string
     */
    public function actionSubscribeMe()
    {
        $id = self::getParam('id');
        $model = Subscribe::findOne($id);
        $Potok = Potok::findOne($model->potok_id);
        $Kurs = Kurs::findOne($Potok->kurs_id);
        $this->isAccess($Kurs->school_id);
        $school = $Kurs->getSchool();

        $email = Yii::$app->user->identity->email;

        \common\services\Subscribe::sendArraySchool(
            $school,
            [$email],
            $model->subject,
            'subscribe/manual2',
            [
                'item'  => $model,
                'potok' => $Potok,
                'sid'   => 1,
            ],
            'layouts/html/subscribe'
        );

        self::jsonSuccess();
    }

    /**
     * @param int $id \common\models\school\Subscribe.id
     *
     * @return Response
     */
    public function actionSubscribe($id)
    {
        $model = new \avatar\models\validate\CabinetSchoolSubscribeControllerSubscribe();

        if (!$model->load(Yii::$app->request->get(), '')) {
            return self::jsonErrorId(400, 'Не загружены данные');
        }
        if (!$model->validate()) {
            return self::jsonErrorId(102, $model->errors);
        }

        return self::jsonSuccess(['item' => $model->action()]);
    }

    public function actionDelete($id)
    {
        $original = \avatar\models\forms\school\Subscribe::findOne($id);
        $Potok = Potok::findOne($original->potok_id);
        $Kurs = Kurs::findOne($Potok->kurs_id);
        $this->isAccess($Kurs->school_id);
        $original->delete();

        return self::jsonSuccess();
    }
}
