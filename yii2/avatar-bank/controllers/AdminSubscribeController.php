<?php

namespace avatar\controllers;

use app\services\Subscribe;
use avatar\controllers\AdminBaseController;
use avatar\models\forms\BlogItem;
use avatar\models\forms\UserAvatar;
use avatar\modules\UniSender\UniSender;
use common\models\subscribe\SubscribeMail;
use cs\models\Tables\Client;
use cs\services\VarDumper;
use cs\web\Exception;
use Yii;
use yii\base\UserException;
use yii\filters\AccessControl;
use yii\helpers\Json;
use yii\helpers\Url;
use yii\httpclient\Client as ClientHttp;
use yii\web\Controller;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use app\models\NewsItem;
use app\models\Chenneling;
use cs\base\BaseController;
use yii\web\Response;
use yii\widgets\ActiveForm;
use app\models\SiteUpdate;

class AdminSubscribeController extends AdminBaseController
{

    public function actionIndex()
    {
        return $this->render();
    }

    public function actionAdd()
    {
        $model = new \avatar\models\forms\SubscribeOriginal();

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {

            $model->insert();
            $model->date_insert = time();
            Yii::$app->session->setFlash('form', 1);

            return $this->refresh();
        } else {
            return $this->render([
                'model' => $model,
            ]);
        }
    }


    public function actionEdit($id)
    {
        $model = \avatar\models\forms\SubscribeOriginal::findOne($id);

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $model->update();
            Yii::$app->session->setFlash('form', 1);
        }

        return $this->render([
            'model' => $model,
        ]);
    }

    public function actionSubscribe($id)
    {
        $original = \common\models\subscribe\SubscribeOriginal::findOne($id);

        /** @var \yii\swiftmailer\Mailer $mailer */
        $mailer = Yii::$app->mailer;
        $html = $mailer->render('html/' . 'subscribe/manual', ['newsItem' => $original], 'layouts/html/subscribe');

        $subscribe = \common\models\subscribe\Subscribe::add([
            'html'    => $html,
            'subject' => $original->subject,
        ]);
        $emailList = \common\models\UserAvatar::find()->where(['email_is_confirm' => 1, 'mark_deleted' => 0])->select(['email'])->column();
        foreach ($emailList as $mail) {
            SubscribeMail::add([
                'subscribe_id'  => $subscribe->id,
                'mail'          => $mail,
            ]);
        }

        return self::jsonSuccess();
    }

    public function actionDelete($id)
    {
        \avatar\models\forms\SubscribeOriginal::findOne($id)->delete();

        return self::jsonSuccess();
    }

    public function actionStatistic()
    {
        return $this->render([
        ]);    }
}
