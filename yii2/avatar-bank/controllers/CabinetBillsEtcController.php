<?php

namespace avatar\controllers;

use avatar\models\forms\UserAvatar;
use avatar\models\validate\CardNumberAccept;
use common\components\Card;
use common\components\providers\BTC;
use common\components\providers\ETH;
use common\models\avatar\Currency;
use common\models\avatar\UserBill;
use common\models\Config;
use common\models\UserSeed;
use common\payment\BitCoinBlockTrailPayment;
use common\services\Security;
use console\controllers\MoneyRateController;
use cs\services\VarDumper;
use cs\web\Exception;
use Yii;
use yii\helpers\FileHelper;
use yii\helpers\Json;
use yii\helpers\StringHelper;
use yii\imagine\Image;
use yii\web\Response;

class CabinetBillsEtcController extends \avatar\controllers\CabinetBaseController
{

    /**
     *
     * AJAX
     *
     * Отправляет деньги
     *
     * REQUEST:
     * + address - string - адрес может быть или целым адресом или в формате bitcoin:rtyertyertytr?i-am-avatar=
     * + amount - string - сколько (разделитель - точка)
     * - comment - string
     * + password - string
     * + billing_id - int - идентификатор счета отправителя
     *
     * @return Response
     */
    public function actionSend()
    {
        $model = new \avatar\models\forms\PiramidaSendEtc();
        $model->load(Yii::$app->request->post(), '');
        if (!$model->validate()) {
            return self::jsonErrorId(101, $this->convert($model->errors));
        }

        return self::jsonSuccess($model->prepare());
    }

    /**
     * Конвертирует массив ошибок от формы чтобы их можно было выдать через JSON
     *
     * @param array $params
     *      [
     *      'amount' => ['Не верное число',...],
     *      ]
     *
     * @return array
     * [
     *      'name'  => 'amount',
     *      'value' => ['Не верное число',...],
     * ]
     */
    private function convert($params)
    {
        $ret = [];
        foreach ($params as $name => $arr) {
            $ret[] = [
                'name'  => $name,
                'value' => $arr,
            ];
        }
        return $ret;
    }

    /**
     * AJAX
     * Отправляет деньги
     * REQUEST:
     * - address - string - биткойн адрес
     * - amount - string - сколько (разделитель - точка)
     * - comment - string
     * - password - string
     * - billing_id - int - идентификатор счета отправителя
     *
     * @return Response
     */
    public function actionSendConfirm()
    {
        $model = new \avatar\models\forms\PiramidaSendEtc();
        $model->load(Yii::$app->request->post(), '');
        if (!$model->validate()) {
            return self::jsonErrorId(101, $this->convert($model->errors));
        }
        $transaction = $model->send();
        if ($transaction === false) {
            return self::jsonErrorId(101, $this->convert($model->errors));
        }

        return self::jsonSuccess([
            'address' => $transaction,
        ]);
    }

    /**
     * Вызывает функцию в контракте
     *
     * @param int $id идентификатор счета
     *
     * @return string
     */
    public function actionExportKey($id)
    {
        $billing = $this->getBill($id);
        $model = new \avatar\models\forms\CabinetBillsEthExportKey(['billing' => $billing]);

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            return $model->action();
        }

        return $this->render([
            'billing' => $billing,
            'model'   => $model,
        ]);
    }

    /**
     * Вызывает функцию в контракте
     *
     * @param int $id идентификатор счета
     *
     * @return string
     */
    public function actionExportJson($id)
    {
        $billing = $this->getBill($id);
        $model = new \avatar\models\forms\CabinetBillsEthExportJson(['billing' => $billing]);

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $data = $model->action();
            Yii::$app->session->setFlash('form', $data);
        }

        return $this->render([
            'billing' => $billing,
            'model'   => $model,
        ]);
    }


    /**
     * Получает счет ID
     * Делает проверку на хозяина
     *
     * @return \common\models\avatar\UserBill
     * @throws \yii\base\Exception
     */
    public function getBill($id = null)
    {
        if (is_null($id)) {
            $id = self::getParam('id');
        }
        $bill = UserBill::findOne($id);
        if ($bill->user_id != Yii::$app->user->id) {
            throw new \yii\base\Exception('Не ваш счет');
        }

        return $bill;
    }

}
