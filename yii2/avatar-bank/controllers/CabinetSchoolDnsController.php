<?php

namespace avatar\controllers;

use avatar\models\forms\UserAvatar;
use avatar\models\validate\CardNumberAccept;
use avatar\modules\ETH\ServiceEtherScan;
use avatar\modules\ETH\ServiceEthPlorer;
use common\components\Card;
use common\components\providers\BTC;
use common\components\providers\ETH;
use common\models\avatar\Currency;
use common\models\avatar\UserBill;
use common\models\Config;
use common\models\school\AdminLink;
use common\models\school\Kurs;
use common\models\school\Lesson;
use common\models\school\Page;
use common\models\school\PageBlockContent;
use common\models\school\School;
use common\models\task\Task;
use common\models\UserSeed;
use common\payment\BitCoinBlockTrailPayment;
use common\services\Security;
use common\services\Subscribe;
use console\controllers\MoneyRateController;
use cs\Application;
use cs\services\VarDumper;
use cs\web\Exception;
use cs\Widget\FileUpload4\ModelFields;
use Yii;
use yii\bootstrap\ActiveForm;
use yii\helpers\FileHelper;
use yii\helpers\Json;
use yii\helpers\StringHelper;
use yii\httpclient\Client;
use yii\imagine\Image;
use yii\web\NotFoundHttpException;
use yii\web\Response;

class CabinetSchoolDnsController extends CabinetSchoolBaseController
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => '\yii\filters\AccessControl',
                'rules' => [
                    [
                        'allow'         => true,
                        'matchCallback' => function ($rule, $action) {
                            if (in_array($action->id, ['index', 'search'])) {
                                $school_id = Yii::$app->request->get('id');
                            } else if (in_array($action->id, ['reject', 'search-ajax'])) {
                                $school_id = Yii::$app->request->post('id');
                            }
                            if (!Application::isInteger($school_id)) throw new \Exception('$school_id не целое');

                            $isAdmin = AdminLink::find()
                                ->where([
                                    'user_id'   => Yii::$app->user->id,
                                    'school_id' => $school_id,
                                ])
                                ->exists();

                            return $isAdmin;
                        },
                    ],
                ],
            ],
        ];
    }

    public function actions()
    {
        return [
            'reject' => [
                'class'    => '\avatar\controllers\actions\DefaultAjax',
                'model'    => '\avatar\models\validate\CabinetSchoolDnsReject',
                'formName' => '',
            ],
            'search-ajax' => [
                'class'    => '\avatar\controllers\actions\DefaultAjax',
                'model'    => '\avatar\models\validate\CabinetSchoolDnsSearchAjax',
                'formName' => '',
            ],
        ];
    }

    public function actionIndex($id)
    {
        $school = School::findOne($id);
        $model = new \avatar\models\forms\DnsRequest();

        if (Yii::$app->request->isAjax) {
            if ($model->load(Yii::$app->request->post())) {
                Yii::$app->response->format = Response::FORMAT_JSON;
                return ActiveForm::validate($model);
            }
        }

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $model->school_id = $id;
            $model->user_id = Yii::$app->user->id;
            $model->save();
            $model->id = $model::getDb()->lastInsertID;
            Subscribe::sendArray(
                'role_admin',
                'Отправлена заявка на DNS',
                'request_dns',
                ['request' => $model]
            );
        }

        return $this->render([
            'model'  => $model,
            'school' => $school,
        ]);
    }


    /**
     */
    public function actionSearch($id)
    {
        $school = School::findOne($id);

        return $this->render([
            'school' => $school,
        ]);
    }
}
