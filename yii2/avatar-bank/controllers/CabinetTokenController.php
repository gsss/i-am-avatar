<?php
/**
 * Created by PhpStorm.
 * User: god
 * Date: 06.08.2017
 * Time: 1:17
 */

namespace avatar\controllers;


use avatar\models\forms\CabinetTokenAdd;
use avatar\models\forms\CabinetTokenBurn;
use avatar\models\validate\CabinetTokenControllerNew;
use avatar\models\validate\CabinetTokenControllerNewCheck;
use avatar\models\WalletToken;
use common\components\providers\ETH;
use common\models\avatar\Currency;
use common\models\koop\RequestPayIn;
use common\models\RequestTokenCreate;
use common\models\UserAvatar;
use common\models\UserToken;
use cs\services\File;
use cs\services\VarDumper;
use cs\web\Exception;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;
use yii\helpers\StringHelper;

class CabinetTokenController extends \avatar\controllers\CabinetBaseController
{
    public function actionNew()
    {
        return $this->render();
    }

    public function actionRequestList()
    {
        return $this->render();
    }

    /**
     * Просмотр заявки
     *
     * @param $id
     *
     * @return string
     * @throws
     */
    public function actionView($id)
    {
        $request = RequestTokenCreate::findOne($id);
        if (is_null($request)) {
            throw new Exception('Не найдена заявка');
        }
        if ($request->user_id != \Yii::$app->user->id) {
            throw new Exception('Это не ваша заявка');
        }

        return $this->render([
            'request' => $request,
        ]);
    }

    /**
     * AJAX
     * Регистрирует токен в сети
     *
     * REQUEST
     * + total - int - кол-во монет
     * + decimals - int - кол-во цифр после запятой
     * + code - string - наименование сокращения
     * + name - string - название монеты
     * + password - string - пароль
     */
    public function actionNewAjax()
    {
        $model = new CabinetTokenControllerNew();
        if (!$model->load(\Yii::$app->request->post())) {
            return self::jsonErrorId(101, 'Не загружены данные');
        }
        if (!$model->validate()) {
            return self::jsonErrorId(102, $this->convert($model->errors));
        }

        return self::jsonSuccess($model->action());
    }

    /**
     *
     */
    public function actionAdd($id)
    {
        $model = new CabinetTokenAdd(['request_id' => $id]);

        if ($model->load(\Yii::$app->request->post()) && $model->validate()) {
            $txid = $model->action();
            \Yii::$app->session->setFlash('form', $txid);
        }

        return $this->render([
            'model' => $model,
        ]);
    }

    /**
     */
    public function actionBurn($id)
    {
        $model = new CabinetTokenBurn(['request_id' => $id]);

        if ($model->load(\Yii::$app->request->post()) && $model->validate()) {
            $txid = $model->action();
            \Yii::$app->session->setFlash('form', $txid);
        }

        return $this->render([
            'model' => $model,
        ]);
    }

    /**
     * AJAX
     * Регистрирует токен в БД
     *
     * REQUEST
     * + name
     * + code
     * + decimals
     * + address
     * + abi
     * + password - string - пароль от кошелька
     * - image
     */
    public function actionRegistration()
    {
        // добавляю currency
        $currency = new Currency([
            'code'     => self::getParam('code'),
            'title'    => self::getParam('name'),
            'decimals' => self::getParam('decimals'),
            'is_view'  => 1,
        ]);
        $currency->save();
        $currency->id = $currency::getDb()->lastInsertID;

        // сохраняю картинку
        $model = new CabinetTokenControllerNew();
        $class = new \common\widgets\FileUpload31\FileUpload([
            'tableName' => 'currency',
            'model'     => $model,
            'attribute' => 'image',
            'value'     => $model->image,
            'id'        => $currency->id,
        ]);
        $fileJson = self::getParam('image');
        $fileArray = Json::decode($fileJson);
        $filePath = $fileArray[0][2];
        $file = \Yii::getAlias('@webroot'.$filePath);
        $content = file_get_contents($file);
        $fileObject = File::content($content);
        $fileInfo = pathinfo($filePath);

        $optionsImage = [
            'image',
            'image',
            'image',
            'widget' => [
                '\common\widgets\FileUpload31\FileUpload',
                [
                    'tableName' => 'currency',
                    'options'   => [
                        'small' => [
                            500,
                            500,
                            \common\widgets\FileUpload3\FileUpload::MODE_THUMBNAIL_CUT,
                        ],
                    ]
                ]
            ],
        ];
        $class->save($fileObject, $fileInfo['extension'], $optionsImage);
        $currency->image = $model->image;
        $currency->save();

        // добавляю token
        $token = new \common\models\Token([
            'address'     => self::getParam('address'),
            'abi'         => self::getParam('abi'),
            'currency_id' => $currency->id,
        ]);
        $token->save();

        // добавляю счет с токеном
        /** @var \common\models\UserAvatar $user */
        $user = \Yii::$app->user->identity;
        $wallet = WalletToken::create(
            $user->getWalletEthPassport()->billing,
            $token,
            self::getParam('name'),
            self::getParam('password'),
            $user->id,
            $user->password_save_type
        );

        return self::jsonSuccess([
            'wallet' => [
                'address' => $wallet->billing->address,
            ]
        ]);
    }

    /**
     * AJAX
     * Регистрирует токен в БД
     *
     * REQUEST
     * + id
     * + address
     */
    public function actionRegistration2()
    {
        $id = self::getParam('id');
        $request = RequestTokenCreate::findOne($id);
        $address = self::getParam('address');
        $request->address = $address;
        $request->save();

        // добавляю currency
        $currency = new Currency([
            'code'     => $request->code,
            'title'    => $request->name,
            'decimals' => $request->decimals,
            'is_view'  => 1,
        ]);
        $currency->save();
        $currency->id = $currency::getDb()->lastInsertID;

        // сохраняю картинку
        $this->saveImage($request, $currency, 'image');
        $currency->image = $request->image; // TODO пересохранение картинки
        $currency->save();

        // добавляю token
        $token = new \common\models\Token([
            'address'     => $address,
            'abi'         => $request->abi,
            'currency_id' => $currency->id,
        ]);
        $token->save();
        $token->id = $token::getDb()->lastInsertID;

        // добавляю счет с токеном
        /** @var \common\models\UserAvatar $user */
        $user = \Yii::$app->user->identity;
        $wallet = WalletToken::createFromEth(
            $user->getWalletEthPassport()->billing,
            $token,
            $request->name,
            $user->id
        );

        // Добавляю токен пользователю
        $t = new UserToken([
            'user_id'  => $user->id,
            'token_id' => $token->id,
        ]);
        $t->save();

        return self::jsonSuccess([
            'wallet' => [
                'id'        => $wallet->billing->id,
                'address'   => $wallet->billing->address,
            ]
        ]);
    }

    /**
     * Пересохораняет картинку токена из заявки в картинку валюты
     *
     * @param \common\models\RequestTokenCreate $from
     * @param \common\models\avatar\Currency    $to
     *
     * @return bool
     */
    private function saveImage($from, $to, $attribute)
    {
        $value = ArrayHelper::getValue($from, $attribute);
        $to[$attribute] = $value;

        return true;
    }

    /**
     * Оплата выпуска токена
     */
    public function actionStep2($id)
    {
        $request = RequestTokenCreate::findOne($id);
        if (is_null($request)) throw new Exception('Нет такой заявки');
        if ($request->user_id != \Yii::$app->user->id) throw new Exception('Это не ваша заявка');
        if ($request->step != 2) throw new Exception('Не верный шаг');

        return $this->render(['request' => $request]);
    }

    /**
     * AJAX
     * Оплата выпуска токена
     */
    public function actionStep2Ajax()
    {
        $model = new \avatar\models\forms\CabinetTokenStep2();
        if (!$model->load(\Yii::$app->request->post())) {
            return self::jsonErrorId(101, 'Не загружены данные');
        }
        if (!$model->validate()) {
            return self::jsonErrorId(102, $model->convert($model->errors));
        }

        return self::jsonSuccess(['txid' => $model->action()]);
    }

    /**
     * Расчет стоимости регистрации контракта
     */
    public function actionStep3($id)
    {
        $request = RequestTokenCreate::findOne($id);
        if (is_null($request)) throw new Exception('Нет такой заявки');
        if ($request->user_id != \Yii::$app->user->id) throw new Exception('Это не ваша заявка');
        if ($request->step != 3) throw new Exception('Не верный шаг');

        $password = \Yii::$app->cache->get(['session_id' => \Yii::$app->session->id, 'name' => 'createToken']);
        if ($password === false) {
            return $this->redirect(['step3-password', 'id' => $id]);
        }

        return $this->render(['request' => $request]);
    }

    /**
     * Расчет стоимости регистрации контракта
     */
    public function actionStep3Password($id)
    {
        $request = RequestTokenCreate::findOne($id);
        if (is_null($request)) throw new Exception('Нет такой заявки');
        if ($request->user_id != \Yii::$app->user->id) throw new Exception('Это не ваша заявка');
        if ($request->step != 3) throw new Exception('Не верный шаг');

        $password = \Yii::$app->cache->get(['session_id' => \Yii::$app->session->id, 'name' => 'createToken']);
        if ($password !== false) {
            return $this->redirect(['step3', 'id' => $id]);
        }

        return $this->render(['request' => $request]);
    }

    /**
     * AJAX
     * Расчет стоимости регистрации контракта
     *
     * @return string
     *
     * 102
     * 103 - Глюк сервера, нужно вызвать еще раз через 3 сек
     * 104 - Не хватает денег на эфировском кошельке чтобы оплатить комиссию, пополните кошелек эфира и зайдите снова в заявку чтобы закончить выпуск токена
     */
    public function actionStep3Ajax()
    {
        $model = new \avatar\models\forms\CabinetTokenStep3();
        if (!$model->load(\Yii::$app->request->post())) {
            return self::jsonErrorId(101, 'Не загружены данные');
        }
        if (!$model->validate()) {
            if (isset($model->errors['request_id'])) {
                if (StringHelper::startsWith($model->errors['request_id'][0], 'Error: Invalid JSON RPC response')) {
                    return self::jsonErrorId(103, $model->convert($model->errors));
                } else if (StringHelper::startsWith($model->errors['request_id'][0], 'Error: insufficient funds for gas * price + value')) {
                    return self::jsonErrorId(104, $model->convert($model->errors));
                } else {
                    return self::jsonErrorId(102, $model->convert($model->errors));
                }
            } else {
                return self::jsonErrorId(102, $model->convert($model->errors));
            }
        }

        return self::jsonSuccess(['txid' => $model->action()]);
    }

    /**
     * Регистрация контракта
     */
    public function actionStep4($id)
    {
        $request = RequestTokenCreate::findOne($id);
        if (is_null($request)) throw new Exception('Нет такой заявки');
        if ($request->user_id != \Yii::$app->user->id) throw new Exception('Это не ваша заявка');
        if ($request->step != 3) throw new Exception('Не верный шаг');

        $password = \Yii::$app->cache->get(['session_id' => \Yii::$app->session->id, 'name' => 'createToken']);
        if ($password === false) {
            return $this->redirect(['step4-password', 'id' => $id]);
        }

        return $this->render(['request' => $request]);
    }

    /**
     * Регистрация контракта
     */
    public function actionStep4Password($id)
    {
        $request = RequestTokenCreate::findOne($id);
        if (is_null($request)) throw new Exception('Нет такой заявки');
        if ($request->user_id != \Yii::$app->user->id) throw new Exception('Это не ваша заявка');
        if ($request->step != 3) throw new Exception('Не верный шаг');

        $password = \Yii::$app->cache->get(['session_id' => \Yii::$app->session->id, 'name' => 'createToken']);
        if ($password !== false) {
            return $this->redirect(['step4', 'id' => $id]);
        }

        return $this->render(['request' => $request]);
    }

    /**
     * AJAX
     * Регистрация контракта
     *
     * @return string
     *
     * 102
     * 103 - Глюк сервера, нужно вызвать еще раз через 3 сек
     * 104 - Не хватает денег на эфировском кошельке чтобы оплатить комиссию, пополните кошелек эфира и зайдите снова в заявку чтобы закончить выпуск токена
     */
    public function actionStep4Ajax()
    {
        $model = new \avatar\models\forms\CabinetTokenStep4();
        if (!$model->load(\Yii::$app->request->post())) {
            return self::jsonErrorId(101, 'Не загружены данные');
        }
        if (!$model->validate()) {
            if (isset($model->errors['request_id'])) {
                if (StringHelper::startsWith($model->errors['request_id'][0], 'Error: Invalid JSON RPC response')) {
                    return self::jsonErrorId(103, $model->convert($model->errors));
                } else if (StringHelper::startsWith($model->errors['request_id'][0], 'Error: insufficient funds for gas * price + value')) {
                    return self::jsonErrorId(104, $model->convert($model->errors));
                } else {
                    return self::jsonErrorId(102, $model->convert($model->errors));
                }
            } else {
                return self::jsonErrorId(102, $model->convert($model->errors));
            }
        }

        return self::jsonSuccess(['txid' => $model->action()]);
    }

    /**
     * Конвертирует массив ошибок от формы чтобы их можно было выдать через JSON
     *
     * @param array $params
     *      [
     *          'amount' => ['Не верное число',...],
     *      ]
     *
     * @return array
     * [
     *      'name'  => 'amount',
     *      'value' => ['Не верное число',...],
     * ]
     */
    private function convert($params)
    {
        $ret = [];
        foreach ($params as $name => $arr) {
            $ret[] = [
                'name'  => $name,
                'value' => $arr,
            ];
        }

        return $ret;
    }

    /**
     * AJAX
     * Проверяет вошла ли транзакция в блок
     *
     * REQUEST
     * + txid - string - адрес транзакции
     *
     * @return string JSON
     * [
     *      bool
     *      address - if success
     * ]
     */
    public function actionNewCheckAjax()
    {
        $model = new CabinetTokenControllerNewCheck();
        if (!$model->load(\Yii::$app->request->post(), '')) {
            return self::jsonErrorId(101, 'Не загружены данные');
        }
        if (!$model->validate()) {
            return self::jsonErrorId(102, $model->errors);
        }

        return self::jsonSuccess($model->action());
    }

    /**
     * AJAX
     * Проверяет заявку на подтверждение блока
     * Если успешно то сохраняет адрес контракта в заявку
     *
     * REQUEST
     * + id - int - идентификатор заявки
     *
     * @return string JSON address
     */
    public function actionTestTx()
    {
        $id = self::getParam('id');
        $request = RequestTokenCreate::findOne($id);
        if (is_null($request)) {
            return self::jsonErrorId(101,'Не найдена заявка');
        }
        if ($request->user_id != \Yii::$app->user->id) {
            return self::jsonErrorId(102,'Это не ваша заявка');
        }
        $model = new CabinetTokenControllerNewCheck(['txid' => $request->txid]);
        $result = $model->action();
        if ($result[0] == false) {
            return self::jsonErrorId(103,'Не подтвержден блок');
        }
        $request->address = $result[1];
        $request->save();

        return self::jsonSuccess($result[1]);
    }

    public function actionConvertParams()
    {
        $model = new CabinetTokenControllerNew();

        if (!$model->load(\Yii::$app->request->post())) {
            return self::jsonErrorId(101, 'Не загружены данные');
        }
        if (!$model->validate()) {
            return self::jsonErrorId(102, $this->convert($model->errors));
        }

        return self::jsonSuccess($model->action());
    }

    /**
     * AJAX
     * Регистрирует контракт в блокчейне
     *
     * REQUEST:
     * + id - int - идентификатор заявки
     */
    public function actionRegistrationAjax()
    {
        $model = new \avatar\models\validate\CabinetTokenControllerRegisterAjax();

        if (!$model->load(\Yii::$app->request->post())) {
            return self::jsonErrorId(101, 'Не загружены данные');
        }
        if (!$model->validate()) {
            return self::jsonErrorId(102, $this->convert($model->errors));
        }

        return self::jsonSuccess($model->action());
    }

    /**
     * AJAX
     * Расчитывает стоимость регистрации контракта в блокчейне
     *
     * REQUEST:
     * + id - int - идентификатор заявки
     */
    public function actionRegistrationCalculateAjax()
    {
        $model = new \avatar\models\validate\CabinetTokenControllerRegisterCalculateAjax();

        if (!$model->load(\Yii::$app->request->post())) {
            return self::jsonErrorId(101, 'Не загружены данные');
        }
        if (!$model->validate()) {
            return self::jsonErrorId(102, $this->convert($model->errors));
        }

        return self::jsonSuccess($model->action());
    }
}