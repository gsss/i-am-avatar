<?php

namespace avatar\controllers;

use avatar\controllers\AdminBaseController;
use avatar\models\forms\NewsItem;
use avatar\models\forms\UserAvatar;
use avatar\modules\UniSender\UniSender;
use common\models\piramida\Wallet;
use cs\Application;
use cs\services\VarDumper;
use cs\web\Exception;
use Yii;
use yii\base\UserException;
use yii\filters\AccessControl;
use yii\helpers\Json;
use yii\helpers\Url;
use yii\httpclient\Client;
use yii\web\Controller;
use yii\filters\VerbFilter;
use cs\base\BaseController;
use yii\web\Response;
use yii\widgets\ActiveForm;

class AdminWalletsController extends AdminBaseController
{

    public function actionIndex()
    {
        return $this->render();
    }

    public function actionSend($id)
    {
        $model = new \avatar\models\forms\SendAtom();
        $Wallet = Wallet::findOne($id);

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $t = $model->action();
            Yii::$app->session->setFlash('form', $t->getAddress());
        }

        return $this->render([
            'model'  => $model,
            'wallet' => $Wallet,
        ]);
    }

    /**
     * @param int $id
     * @return string
     */
    public function actionAdd($id)
    {
        $model = new \avatar\models\forms\WalletAdd();
        $Wallet = Wallet::findOne($id);

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $t = $model->action();
            Yii::$app->session->setFlash('form', $t->getAddress());
        }

        return $this->render([
            'model'  => $model,
            'wallet' => $Wallet,
        ]);
    }

    /**
     * @param int $id wallet.id
     * @return string
     * @throws
     */
    public function actionView($id)
    {
        $Wallet = Wallet::findOne($id);
        if (!Application::isInteger($id)) throw new Exception('В параметре должно быть целое число');
        if (is_null($Wallet)) throw new Exception('Кошелек не найден');

        return $this->render([
            'wallet' => $Wallet,
        ]);
    }

    /**
     */
    public function actionTest()
    {
        return $this->render([
        ]);
    }

    /**
     * @param int $id
     * @return string
     */
    public function actionSub($id)
    {
        $model = new \avatar\models\forms\WalletSub();
        $Wallet = Wallet::findOne($id);

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $t = $model->action();
            Yii::$app->session->setFlash('form', $t->getAddress());
        }

        return $this->render([
            'model'  => $model,
            'wallet' => $Wallet,
        ]);
    }

}
