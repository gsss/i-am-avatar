<?php

namespace avatar\controllers;

use app\models\Article;
use app\models\SiteUpdate;
use app\services\Subscribe;
use avatar\models\forms\Contact;
use avatar\services\LogReader;
use common\components\sms\IqSms;
use common\models\avatar\UserBill;
use common\models\Card;
use common\models\CardSchoolLink;
use common\models\PaymentBitCoin;
use common\models\school\School;
use common\models\UserAvatar;
use common\models\UserMaster;
use common\models\UserRegistration;
use common\payment\BitCoinBlockTrailPayment;
use cs\Application;
use cs\base\BaseController;
use cs\services\UploadFolderDispatcher;
use cs\services\VarDumper;
use cs\web\Exception;
use Yii;
use yii\base\UserException;
use yii\db\Connection;
use yii\db\Query;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use yii\helpers\FileHelper;
use yii\helpers\Url;
use yii\web\Response;

class PartnerController extends \avatar\base\BaseController
{
    /**
     * редирект на покупку карты
     *
     * @param string $card номер карты
     *
     * @return string Response
     * @throws
     */
    public function actionIndex($card)
    {
        /** @var \common\models\Card $cardObject */
        $cardObject = Card::findOne(['number' => $card]);
        if (is_null($cardObject)) {
            throw new \cs\web\Exception('Нет такой карты');
        }

        $url = 'https://for-people.life';
        if (!Application::isEmpty($cardObject->user_id)) {

            $user = UserAvatar::findOne($cardObject->user_id);
            switch ($user->card_action) {
                case 1:
                    return $this->redirect($url.'/user/' . $cardObject->user_id);
                    break;
                case 2:
                    return $this->redirect($url.'/user/' . $cardObject->user_id);
                    break;
                case 3:
                    return $this->redirect($url.'/user/' . $cardObject->user_id);
                    break;
                case 4:
                    return $this->redirect($url.'/shop/order?id=1&partner_id=' . $cardObject->user_id);
                    break;
                case 5:
                    return $this->redirect($url.'/card/ask?card=' . $card);
                    break;
                case 6:
                    // ищу к какой школе привязана карта
                    $rows = CardSchoolLink::find()->where(['card_id' => $cardObject->id])->all();
                    if (count($rows) == 0) {
                        return $this->redirect($url.'/shop/card?card=' . $card);
                    }
                    if (count($rows) == 1) {
                        $school = School::findOne($rows[0]['school_id']);
                        return $this->redirect($school->getUrl('/shop/card?card=' . $card));
                    }
                    if (count($rows) > 1) {
                        return $this->redirect($url.'/card/ask-quick?card=' . $card);
                    }
                    break;
                default:
                    return $this->redirect($url.'/user/' . $cardObject->user_id);
                    break;
            }

        } else {

            return $this->redirect($url.'/qr/enter?card=' . $card);

        }
    }

}
