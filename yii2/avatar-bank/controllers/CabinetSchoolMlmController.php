<?php

namespace avatar\controllers;

use avatar\models\forms\UserAvatar;
use avatar\models\validate\CardNumberAccept;
use avatar\models\Wallet;
use avatar\modules\ETH\ServiceEtherScan;
use avatar\modules\ETH\ServiceEthPlorer;
use common\components\Card;
use common\components\providers\BTC;
use common\components\providers\ETH;
use common\models\avatar\Currency;
use common\models\avatar\CurrencyLink;
use common\models\avatar\UserBill;
use common\models\Config;
use common\models\school\Kurs;
use common\models\school\Lesson;
use common\models\school\Master;
use common\models\school\School;
use common\models\UserSeed;
use common\payment\BitCoinBlockTrailPayment;
use common\services\Security;
use console\controllers\MoneyRateController;
use cs\services\VarDumper;
use cs\web\Exception;
use cs\Widget\FileUpload4\ModelFields;
use Yii;
use yii\helpers\FileHelper;
use yii\helpers\Json;
use yii\helpers\StringHelper;
use yii\httpclient\Client;
use yii\imagine\Image;
use yii\web\Response;

class CabinetSchoolMlmController extends CabinetSchoolBaseController
{

    /**
     * @param int $id идентификатор школы
     *
     * @return string
     */
    public function actionIndex($id)
    {
        $this->isAccess($id);
        $school = School::findOne($id);

        return $this->render([
            'school' => $school,
        ]);
    }

    /**
     * @param int $id идентификатор школы
     *
     * @return string
     */
    public function actionCoinAdd($id)
    {
        $model = new \avatar\models\forms\CoinAdd();
        $school = School::findOne($id);

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            /**
             * [
             * 'wallet'    => $wallet,
             * 'operation' => $operation,
             * 'link'      => $link,
             * ];
             */
            $data = $model->add();
            $school->referal_wallet_id = $data['wallet']->id;
            $school->save();
            Yii::$app->session->setFlash('form', $data);
        }

        return $this->render([
            'model' => $model,
            'school' => $school,
        ]);
    }


    /**
     * @param int $id идентификатор школы
     *
     * @return string
     */
    public function actionCoinEdit($id)
    {
        $model = new \avatar\models\forms\CoinEdit();
        $school = School::findOne($id);

        $wid = $school->referal_wallet_id;
        $wallet = \common\models\piramida\Wallet::findOne($wid);
        $model->init2($wallet->getCurrency());

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            Yii::$app->session->setFlash('form', 1);
            $model->edit();
        }

        return $this->render([
            'model'  => $model,
            'school' => $school,
        ]);
    }

    /**
     * @return string
     */
    public function actionSave()
    {
        $model = new \avatar\models\forms\Levels();

        if (!$model->load(Yii::$app->request->post())) {
            return self::jsonErrorId(400, 'Не загружены данные');
        }
        if (!$model->validate()) {
            return self::jsonErrorId(102, $model->errors);
        }

        self::jsonSuccess($model->action());
    }

}
