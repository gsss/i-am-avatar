<?php
/**
 * Created by PhpStorm.
 * User: god
 * Date: 28.04.2017
 * Time: 0:55
 */

namespace avatar\controllers;


use avatar\models\forms\UserAvatar;
use avatar\models\validate\SubscribeAdd;
use common\models\school\SubscribeItem;
use common\models\subscribe\Subscribe;
use common\models\subscribe\SubscribeMail;
use cs\web\Exception;
use yii\db\Query;
use yii\helpers\VarDumper;
use yii\web\Response;

class SubscribeController extends \avatar\base\BaseController
{

    /**
     */
    public function actionUnsubscribe()
    {
        $model = new \avatar\models\validate\UnSubscribe();

        if (!$model->load(\Yii::$app->request->get(), '')) {
            throw new Exception('Не переданы данные');
        }
        if (!$model->validate()) {
            throw new Exception('Ошибки валидации ' . VarDumper::dumpAsString($model->errors));
        }
        $model->action();

        return $this->render();
    }

    /**
     * REQUEST:
     * - sid - int school_potok_subscribe_item.id
     */
    public function actionPixel()
    {
        $path = '/images/1x1.png';
        $full = \Yii::getAlias('@webroot' . $path);
        $content = file_get_contents($full);
        $sid = self::getParam('sid');

        $s = SubscribeItem::findOne($sid);
        if (!is_null($s)) {
            $s->views_count++;
            $s->save();
        }

        \Yii::$app->response->headers->add('Content-Type', 'image/png');
        \Yii::$app->response->headers->add('Cache-Control', 'max-age=86400');
        \Yii::$app->response->content = $content;
        \Yii::$app->response->format = Response::FORMAT_RAW;
        \Yii::$app->response->send();
    }

    /**
     * Отписаться от рассылки
     *
     * REQUEST:
     * + mail - string - почта
     * + type - int - тип рассылки
     * + hash - проверочный хеш
     *
     */
    public function actionUnsubscribe2()
    {
        $model = new \avatar\models\validate\UnSubscribe2();

        if (!$model->load(\Yii::$app->request->get(), '')) {
            throw new Exception('Не переданы данные');
        }
        if (!$model->validate()) {
            throw new Exception('Ошибки валидации ' . VarDumper::dumpAsString($model->errors));
        }
        $model->action();

        return $this->render();
    }

    /**
     * Отписаться от рассылки
     *
     * REQUEST:
     * + email - string - почта
     * + sid - int - school_potok_subscribe_item.id
     * + hash - проверочный хеш
     *
     */
    public function actionUnsubscribe3()
    {
        $model = new \avatar\models\validate\UnSubscribe3();

        if (!$model->load(\Yii::$app->request->get(), '')) {
            throw new Exception('Не переданы данные');
        }
        if (!$model->validate()) {
            throw new Exception('Ошибки валидации ' . VarDumper::dumpAsString($model->errors));
        }
        $model->action();

        return $this->render();
    }



}