<?php

namespace avatar\controllers;

use common\models\MerchantRequest;
use common\models\piramida\InRequest;
use common\models\piramida\WalletSource\BitCoin;
use common\models\piramida\WalletSource\RubRubYandex;
use common\models\piramida\WalletSourceInterface;
use common\models\school\Potok;
use common\models\school\PotokUserLink;
use common\models\school\SubscribeItem;
use common\models\subscribe\SubscribeMail;
use common\models\User2;
use cs\Application;
use Yii;
use yii\helpers\ArrayHelper;
use yii\helpers\VarDumper;

/**
 */
class ServiceController extends \avatar\base\BaseController
{
    public $enableCsrfValidation = false;

    /**
     * Получает данные с формы
     * $_POST  = [
     *   'Email'  => 'dram1008@yandex.ru',
     *   'Name'   => 'Святослав Анатольевич Архангельский',
     *   'Phone'  => '89252374501',
     *   'tranid' => '977108:105157212',
     *   'formid' => 'form77501868',
     * ],
     * https://www.i-am-avatar.com/service/tilda-form
     */
    public function actionTildaForm()
    {
        \Yii::info([$_POST, $_GET], 'avatar\\actionTildaForm');
        try {
            $potokId = $this->getPotok(Yii::$app->request->post('formid'));
        } catch (\Exception $e) {
            // Если не найден поток то выхожу
            return self::jsonSuccess();
        }

        \school\controllers\PagesController::tildaForm2(Potok::findOne($potokId), [
            'email' => Yii::$app->request->post('Email'),
            'name'  => Yii::$app->request->post('Name'),
            'phone' => Yii::$app->request->post('Phone'),
        ]);

        return self::jsonSuccess();
    }

    /**
     * @param string $formId
     *
     * @return int
     * @throws \Exception
     */
    private function getPotok($formId)
    {
        $setting = \common\models\school\PluginInTilda::findOne(['name' => $formId]);
        if (is_null($setting)) throw new \Exception('Не найдена запись');

        return $setting->potok_id;
    }
}
