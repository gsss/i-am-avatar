<?php

namespace avatar\controllers;

use app\models\Article;
use app\models\SiteUpdate;
use app\services\Subscribe;
use avatar\models\forms\Contact;
use avatar\services\LogReader;
use common\components\providers\ETH;
use common\components\sms\IqSms;
use common\models\avatar\Currency;
use common\models\avatar\UserBill;
use common\models\CompanyCustomizeItem;
use common\models\PaymentBitCoin;
use common\models\school\Page;
use common\models\school\School;
use common\models\UserAvatar;
use common\models\UserDocument;
use common\models\UserRegistration;
use common\payment\BitCoinBlockTrailPayment;
use cs\Application;
use cs\base\BaseController;
use cs\services\UploadFolderDispatcher;
use cs\services\VarDumper;
use cs\web\Exception;
use Yii;
use yii\base\UserException;
use yii\db\Connection;
use yii\db\Query;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use yii\helpers\FileHelper;
use yii\helpers\Url;
use yii\web\ForbiddenHttpException;
use yii\web\Response;

class ServicesController extends \avatar\base\BaseController
{
    public function actions()
    {
        return [
            'telegram-file-ajax' => [
                'class'    => '\avatar\controllers\actions\DefaultAjax',
                'model'    => '\avatar\models\validate\ServicesTelegramFileAjax',
                'formName' => 'ServicesTelegramFileAjax',
            ],
        ];
    }

    /**
     */
    public function actionKoladaDar()
    {
        return $this->render();
    }

    /**
     */
    public function actionTelegramFile()
    {
        return $this->render();
    }

    /**
     */
    public function actionTelegramFileDownload()
    {
        if (!Yii::$app->request->isPost) {
            throw new Exception('must be post');
        }
        $path = Yii::$app->request->get('path');

        /** @var \aki\telegram\Telegram $telegram */
        $telegram = Yii::$app->telegram;

        $url = 'https://api.telegram.org/file/bot' . $telegram->botToken . '/' . $path;
        $content = file_get_contents($url);

        $i = pathinfo($path);
        Yii::info($i, 'avatar\controllers\ServicesController::actionTelegramFileDownload');

        return Yii::$app->response->sendContentAsFile($content, $i['basename']);
    }


    /**
     */
    public function actionKoladaDar2()
    {
        return $this->render();
    }

    /**
     */
    public function actionKoladaDarDownload()
    {
        require \Yii::getAlias('@common/components/mpdf60/mpdf.php');
        $pdfDriver = new \mPDF();
        $filePath = \Yii::getAlias('@avatar/views/services/kolada-dar-download.php');
        $contentHtml = \Yii::$app->view->renderFile($filePath);
        $pdfDriver->WriteHTML($contentHtml);
        $contentPdf = $pdfDriver->Output('', 'S');

        return Yii::$app->response->sendContentAsFile($contentPdf, 'document7528.pdf', [
            'mimeType' => 'application/pdf',
        ]);
    }

}
