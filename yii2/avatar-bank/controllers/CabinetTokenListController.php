<?php
/**
 * Created by PhpStorm.
 * User: god
 * Date: 06.08.2017
 * Time: 1:17
 */

namespace avatar\controllers;


use avatar\models\forms\CabinetTokenAdd;
use avatar\models\forms\CabinetTokenBurn;
use avatar\models\validate\CabinetTokenControllerNew;
use avatar\models\validate\CabinetTokenControllerNewCheck;
use avatar\models\WalletToken;
use common\components\providers\ETH;
use common\models\avatar\Currency;
use common\models\koop\RequestPayIn;
use common\models\RequestTokenCreate;
use common\models\UserAvatar;
use common\models\UserToken;
use cs\services\File;
use cs\services\VarDumper;
use cs\web\Exception;
use yii\helpers\Json;
use yii\helpers\StringHelper;

class CabinetTokenListController extends \avatar\controllers\CabinetBaseController
{
    /**
     * Показывает список всех моих токенов
     */
    public function actionIndex()
    {
        return $this->render();
    }

    /**
     * Просмотр токена
     *
     * @param $id
     *
     * @return string
     * @throws
     */
    public function actionView($id)
    {
        $userToken = UserToken::findOne($id);
        if (is_null($userToken)) {
            throw new Exception('Не найден токен');
        }
        if ($userToken->user_id != \Yii::$app->user->id) {
            throw new Exception('Это не ваш токен');
        }

        return $this->render([
            'userToken' => $userToken,
        ]);
    }

    /**
     * Загрузка картинки токена
     *
     * @param int $id - user_token.id
     *
     * @return string
     * @throws
     */
    public function actionImage($id)
    {
        $userToken = UserToken::findOne($id);
        if (is_null($userToken)) {
            throw new Exception('Не найден токен');
        }
        if ($userToken->user_id != \Yii::$app->user->id) {
            throw new Exception('Это не ваш токен');
        }
        $cid = $userToken->getToken()->getCurrency()->id;

        $model = \avatar\models\forms\UserTokenImage::findOne($cid);

        if ($model->load(\Yii::$app->request->post()) && $model->validate()) {
            $model->update();
            \Yii::$app->session->setFlash('form', $id);
        }

        return $this->render([
            'model' => $model,
        ]);
    }
}