<?php

namespace avatar\controllers;

use app\models\Article;
use app\models\SiteUpdate;
use app\services\Subscribe;
use avatar\models\forms\Contact;
use avatar\services\LogReader;
use common\components\providers\ETH;
use common\components\sms\IqSms;
use common\models\avatar\Currency;
use common\models\avatar\UserBill;
use common\models\PaymentBitCoin;
use common\models\UserAvatar;
use common\models\UserRegistration;
use common\payment\BitCoinBlockTrailPayment;
use cs\Application;
use cs\base\BaseController;
use cs\services\UploadFolderDispatcher;
use cs\services\VarDumper;
use cs\web\Exception;
use Yii;
use yii\base\UserException;
use yii\db\Connection;
use yii\db\Query;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use yii\helpers\FileHelper;
use yii\helpers\Url;
use yii\web\ForbiddenHttpException;
use yii\web\Response;

class LogController extends \avatar\base\BaseController
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow'   => true,
                        'roles'   => ['role_admin'],
                    ],
                ],
            ],
        ];
    }

    public function actionIndex()
    {
        $first = self::getParam('first');

        return $this->render([
            'log'   => LogReader::file('@runtime/logs/app.log')->readLast(
                ArrayHelper::merge(
                    ['maxStrings' => 10],
                    Yii::$app->request->get()
                )
            ),
            'first' => ($first) ? $first : 0,
        ]);
    }

    public function actionLogDbAvatar()
    {
        return $this->render('log-db-avatar');
    }

    public function actionLogDbConsole()
    {
        return $this->render('log-db-console');
    }

    public function actionLogDbSchool()
    {
        return $this->render('log-db-school');
    }

    public function actionLogDbApi()
    {
        return $this->render('log-db-api');
    }

}
