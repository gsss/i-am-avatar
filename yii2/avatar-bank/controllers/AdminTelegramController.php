<?php

namespace avatar\controllers;

use common\models\Anketa;
use common\models\task\Task;
use cs\services\VarDumper;
use Yii;
use yii\filters\AccessControl;
use yii\helpers\Json;
use yii\helpers\Url;
use yii\httpclient\Client;
use yii\web\Response;

class AdminTelegramController extends AdminBaseController
{
    public function actionIndex()
    {
        return $this->render([]);
    }

    public function actionSubscribe()
    {
        $model = new \avatar\models\forms\school\AdminTelegram();

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $model->action();
            Yii::$app->session->setFlash('form', 1);
        }

        return $this->render(['model' => $model]);
    }

    /**
     */
    public function actionSetCallBack()
    {
        $path = self::getParam('path');
        $path = Yii::getAlias('@avatar/../../webserver/ssl/platform.qualitylive.su/platform_qualitylive_su.ca-bundle');
        $path = Yii::getAlias('/application/platform_qualitylive_su.ca-bundle');

        $client = new Client();
        /** @var \aki\telegram\Telegram $telegram */
        $telegram = Yii::$app->telegram;

        $url = "https://api.telegram.org/bot" . $telegram->botToken . "/setWebhook";

        $response = $client->createRequest()
            ->setMethod('POST')
            ->setUrl($url)
            ->addData(['url' => self::getParam('url')])
            ->addFile('certificate', $path)
            ->send();

        return self::jsonSuccess(Json::decode($response->content));
    }

    /**
     */
    public function actionGetWebhookInfo()
    {
        $client = new Client();
        /** @var \aki\telegram\Telegram $telegram */
        $telegram = Yii::$app->telegram;

        $url = "https://api.telegram.org/bot" . $telegram->botToken . "/getWebhookInfo";

        $response = $client->createRequest()
            ->setMethod('POST')
            ->setUrl($url)
            ->send();

        return self::jsonSuccess(Json::decode($response->content));
    }

    /**
     */
    public function actionRemoveCallBack()
    {
        $client = new Client();
        /** @var \aki\telegram\Telegram $telegram */
        $telegram = Yii::$app->telegram;

        $url = "https://api.telegram.org/bot" . $telegram->botToken . "/setWebhook";

        $response = $client->createRequest()
            ->setMethod('POST')
            ->setUrl($url)
            ->send();

        return self::jsonSuccess(Json::decode($response->content));
    }
}
