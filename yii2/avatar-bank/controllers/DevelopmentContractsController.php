<?php

namespace avatar\controllers;

use avatar\models\forms\Contact;
use avatar\services\LogReader;
use common\components\sms\IqSms;
use common\models\avatar\Currency;
use common\models\avatar\UserBill;
use common\models\PaymentBitCoin;
use common\models\UserAvatar;
use common\models\UserDocument;
use common\models\UserRegistration;
use common\payment\BitCoinBlockTrailPayment;
use cs\Application;
use cs\base\BaseController;
use cs\services\UploadFolderDispatcher;
use cs\services\VarDumper;
use cs\web\Exception;
use Yii;
use yii\base\UserException;
use yii\db\Connection;
use yii\db\Query;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use yii\helpers\FileHelper;
use yii\helpers\Url;
use yii\web\Response;

class DevelopmentContractsController extends \avatar\base\BaseController
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow'   => true,
                        'roles'   => ['role_developer'],
                    ],
                ],
            ],
        ];
    }

    /**
     */
    public function actionIndex()
    {
        return $this->render();
    }

    /**
     */
    public function actionFranshiza()
    {
        return $this->render();
    }

    /**
     */
    public function actionZalog()
    {
        return $this->render();
    }

    /**
     * Свидетельство о рождении
     */
    public function actionRojd()
    {
        return $this->render();
    }

    /**
     */
    public function actionDocuments()
    {
        return $this->render();
    }

    /**
     */
    public function actionTreeIdenty()
    {
        return $this->render('tree-identy');
    }

    /**
     */
    public function actionAvrToken()
    {
        return $this->render('avr-token');
    }

    /**
     */
    public function actionKoop()
    {
        return $this->render();
    }

    /**
     */
    public function actionAccreditiv()
    {
        return $this->render();
    }

    /**
     */
    public function actionRegistration()
    {
        return $this->render();
    }

    /**
     */
    public function actionRegistration11()
    {
        return $this->render('registration-1-1');
    }

    /**
     */
    public function actionRegistration12()
    {
        return $this->render('registration-1-2');
    }


}
