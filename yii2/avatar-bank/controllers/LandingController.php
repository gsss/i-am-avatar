<?php

namespace avatar\controllers;

use app\models\Article;
use app\models\SiteUpdate;
use app\services\Subscribe;
use avatar\models\forms\Contact;
use avatar\services\LogReader;
use common\components\providers\ETH;
use common\components\sms\IqSms;
use common\models\avatar\Currency;
use common\models\avatar\UserBill;
use common\models\PaymentBitCoin;
use common\models\UserAvatar;
use common\models\UserRegistration;
use common\payment\BitCoinBlockTrailPayment;
use cs\Application;
use cs\base\BaseController;
use cs\services\UploadFolderDispatcher;
use cs\services\VarDumper;
use cs\web\Exception;
use Yii;
use yii\base\UserException;
use yii\db\Connection;
use yii\db\Query;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use yii\helpers\FileHelper;
use yii\helpers\Url;
use yii\web\ForbiddenHttpException;
use yii\web\NotFoundHttpException;
use yii\web\Response;

class LandingController extends \avatar\base\BaseController
{
    public $layout = 'landing';

    /**
     */
    public function actionIndex()
    {
        return $this->render();
    }

    /**
     */
    public function actionForm($id)
    {
        $file = Yii::getAlias('@webroot/network/forms/'.$id.'.php');
        Yii::$app->response->format = Response::FORMAT_HTML;
        if (!file_exists($file)) {
            throw new NotFoundHttpException();
        }
        Yii::$app->response->content = Yii::$app->view->renderPhpFile($file);

        return Yii::$app->response;
    }

    /**
     * AJAX
     *
     * REQUEST:
     * + name
     * + email
     * + text
     */
    public function actionMail()
    {
        $model = new \avatar\models\forms\LandingSend();

        if (!$model->load(Yii::$app->request->post(), '')) {
            return self::jsonErrorId(101, 'Не загружены данные');
        }
        if (!$model->validate()) {
            return self::jsonErrorId(102, $model->convert($model->errors));
        }
        $model->action();

        return self::jsonSuccess();
    }

}
