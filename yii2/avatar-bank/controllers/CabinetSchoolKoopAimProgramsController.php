<?php

namespace avatar\controllers;

use avatar\models\forms\UserAvatar;
use avatar\models\UserEnter;
use common\models\school\AdminLink;
use common\models\school\School;
use common\models\SchoolKoop;
use common\services\FormAjax\ActiveForm;
use cs\Application;
use cs\services\VarDumper;
use iAvatar777\services\FormAjax\DefaultFormAjax;
use iAvatar777\services\FormAjax\DefaultFormDelete;
use Yii;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;

class CabinetSchoolKoopAimProgramsController extends CabinetSchoolBaseController
{

    public function actions()
    {

        return [
                'delete' => [
                    'class' => '\iAvatar777\services\FormAjax\DefaultFormDelete',
                    'model' => '\avatar\models\forms\CabinetKoopAimAdd'
                ]
        ];
    }

    /**
     * @return string
     */
    public function actionIndex($id)
    {
        $school = School::findOne($id);
        $koop = SchoolKoop::findOne(['school_id' => $id]);

        return $this->render(['school' => $school, 'koop' => $koop]);
    }

    /**
     * @return string
     */
    public function actionView($id)
    {
        $item = UserEnter::findOne($id);
        $school = School::findOne($item->school_id);

        return $this->render([
            'item'   => $item,
            'school' => $school,
        ]);
    }

    /**
     * @param int $id school.id
     * @return string
     */
    public function actionAdd($id)
    {
        $school = School::findOne($id);
        $model = new \avatar\models\forms\CabinetKoopAimAdd(['school_id' => $id]);

        if (Yii::$app->request->isPost) {
            if ($model->load(Yii::$app->request->post()) && $model->validate()) {
                $s = $model->save(false);
                return self::jsonSuccess($s);
            } else {
                return self::jsonErrorId(102, $model->getErrors102());
            }
        }

        return $this->render([
            'model'   => $model,
            'school' => $school,
        ]);
    }

    /**
     * @param int $id school.id
     * @return string
     */
    public function actionEdit($id)
    {
        $model = \avatar\models\forms\CabinetKoopAimAdd::findOne($id);
        $school = School::findOne($model->school_id);

        if (Yii::$app->request->isPost) {
            if ($model->load(Yii::$app->request->post()) && $model->validate()) {
                $s = $model->save(false);
                return self::jsonSuccess($s);
            } else {
                return self::jsonErrorId(102, $model->getErrors102());
            }
        }

        return $this->render([
            'model'  => $model,
            'school' => $school,
        ]);
    }
}
