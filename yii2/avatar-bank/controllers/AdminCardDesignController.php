<?php

namespace avatar\controllers;

use Yii;

class AdminCardDesignController extends AdminBaseController
{
    public $type_id = 23;

    public function actionIndex()
    {
        return $this->render();
    }

    public function actionAdd()
    {
        $model = new \avatar\models\forms\CardDesign(['_school_id' => 2, '_type_id' => $this->type_id]);

        if ($model->load(Yii::$app->request->post()) && $model->insert()) {
            Yii::$app->session->setFlash('form', $model->id);
        }

        return $this->render([
            'model' => $model,
        ]);
    }

    public function actionEdit($id)
    {
        $model = \avatar\models\forms\CardDesign::findOne($id);
        $model->_school_id = 2;
        $model->_type_id = $this->type_id;

        if ($model->load(Yii::$app->request->post()) && $model->update()) {
            Yii::$app->session->setFlash('form', 1);
        }

        return $this->render([
            'model' => $model,
        ]);
    }

    public function actionDelete($id)
    {
        $model = \avatar\models\forms\CardDesign::findOne($id);
        $model->_school_id = 2;
        $model->_type_id = $this->type_id;
        $model->delete();

        return self::jsonSuccess();
    }
}
