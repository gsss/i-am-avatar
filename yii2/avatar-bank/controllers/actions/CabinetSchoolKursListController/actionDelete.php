<?php

namespace avatar\controllers\actions\CabinetSchoolKursListController;


use avatar\models\WalletETH;
use avatar\models\WalletToken;
use common\models\avatar\Currency;
use common\models\avatar\UserBill;
use common\models\BillingMain;
use common\models\comment\Comment;
use common\models\comment\CommentList;
use common\models\investment\IcoRequest;
use common\models\PaySystemConfig;
use common\models\piramida\Wallet;
use common\models\school\Kurs;
use common\models\school\Lesson;
use common\models\school\LessonPage;
use common\models\school\LessonVebinar;
use common\models\school\LessonVideo;
use common\models\school\PageBlockContent;
use common\models\school\PageStat;
use common\models\school\Potok;
use common\models\school\PotokUser3Link;
use common\models\school\School;
use common\models\school\SchoolSale;
use common\models\school\SchoolSaleRequest;
use common\models\school\SubscribeItem;
use common\models\school\SubscribeSms;
use common\models\school\SubscribeSmsResult;
use common\models\task\Task;
use common\models\Token;
use common\models\UserAvatar;
use common\models\UserWallet;
use common\services\Subscribe;
use cs\Application;
use cs\services\File;
use Yii;
use yii\base\Exception;
use yii\base\Model;
use yii\bootstrap\ActiveForm;
use yii\helpers\FileHelper;
use yii\imagine\Image;
use yii\web\Response;

/**
 * REQUEST
 * - id
 */
class actionDelete extends \avatar\base\BaseAction
{

    public function run()
    {
        $isDeletePage = true;
        $id = Yii::$app->request->get('id');
        $k = Kurs::findOne($id);

        // удалаю потоки
        $PotokRows = Potok::find()->where(['kurs_id' => $k->id])->all();
        /** @var \common\models\school\Potok $Potok */
        foreach ($PotokRows as $Potok) {

            // удалаю рассылки
            $SubscribeRows = \common\models\school\Subscribe::find()->where(['potok_id' => $Potok->id])->all();
            /** @var \common\models\school\Subscribe $Subscribe */
            foreach ($SubscribeRows as $Subscribe) {
                SubscribeItem::deleteAll(['subscribe_id' => $Subscribe->id]);
            }

            // удалаю ЛИД
            PotokUser3Link::deleteAll(['potok_id' => $Potok->id]);

            // удалаю продажи
            $SchoolSaleRows = SchoolSale::find()->where(['potok_id' => $Potok->id])->all();
            /** @var \common\models\school\SchoolSale $SchoolSale */
            foreach ($SchoolSaleRows as $SchoolSale) {
                // удалаю заявки на продажи
                SchoolSaleRequest::find()->where(['sale_id' => $SchoolSale->id]);
            }

            // СМС рассылки
            $SubscribeSmsRows = SubscribeSms::find()->where(['potok_id' => $Potok->id])->all();
            foreach ($SubscribeSmsRows as $SubscribeSms) {
                SubscribeSmsResult::deleteAll(['subscribe_id' => $SubscribeSms->id]);
                $SubscribeSms->delete();
            }

            // страницы
            if ($isDeletePage) {
                // Если с потоком сополставлена страница то ее тоже удалю
                if (!Application::isEmpty($Potok->page_id)) {
                    PageBlockContent::deleteAll(['page_id' => $Potok->page_id]);
                    PageStat::deleteAll(['page_id' => $Potok->page_id]);
                    $p = \avatar\models\forms\school\Page::findOne($Potok->page_id);
                    $p->delete();
                }
            }

            // удалаю поток
            $Potok->delete();
        }

        // удалаю уроки
        $LessonRows = Lesson::find()->where(['kurs_id' => $k->id])->all();
        /** @var \common\models\school\Lesson $Lesson */
        foreach ($LessonRows as $Lesson) {
            switch ($Lesson->type_id) {
                case 1:
                    break;
                case 2:
                    LessonVideo::deleteAll(['lesson_id' => $Lesson->id]);
                    break;
                case 3:
                    break;
                case 4:
                    break;
                case 5:
                    LessonPage::deleteAll(['lesson_id' => $Lesson->id]);
                    break;
                case 6:
                    LessonVebinar::deleteAll(['lesson_id' => $Lesson->id]);
                    break;
            }

            // удалаю коментарии к урокам
            $comment_type_id = 2;
            $list_id = CommentList::find()->where(['type_id' => $comment_type_id, 'object_id' => $Lesson->id])->select('id')->scalar();
            Comment::deleteAll(['list_id' => $list_id]);
        }

        $model = \avatar\models\forms\school\Kurs::findOneWithInit($id, [
            '_school_id' => $k->school_id,
            '_type_id'   => $this->type_id,
        ]);
        $model->delete();

        return self::jsonSuccess();
    }
}