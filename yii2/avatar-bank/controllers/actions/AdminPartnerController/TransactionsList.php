<?php
/**
 * Created by PhpStorm.
 * User: Бог-Творец
 * Date: 25.11.2016
 * Time: 2:04
 */
namespace avatar\controllers\actions\AdminPartnerController;

use common\models\avatar\UserBill;
use cs\services\VarDumper;
use Yii;
use yii\base\Action;
use yii\helpers\ArrayHelper;

class TransactionsList extends Action
{
    /**
     * AJAX
     *
     *
     * REQUEST:
     * - page - int - номер страницы. Если нет траницы то показываю первую
     *
     * @param int $id идентификатор \common\models\avatar\UserBill
     *
     * @return string
     * @throws
     */
    public function run()
    {
        $id = self::getParam('id', '');
        if ($id == '') {
            return self::jsonErrorId(101, 'Не указан параметр id');
        }
        $page = self::getParam('page', 1);
        $itemsPerPage = 20;

        $bill = $this->getBill($id);
        if (YII_ENV_DEV) {
            $transactions = [];
            $transactions['data'] = [];

        } else {
            $wallet = $bill->getWallet();
            $transactions = $wallet->transactions($page, $itemsPerPage, 'desc');
            $transactions['data'] = $this->concatComments($bill, $transactions['data']);
        }

        $file = Yii::getAlias('@avatar/views/' . $this->controller->id . '/transactions-list.php');
        $html = $this->controller->view->renderFile($file, [
            'transactions'     => $transactions,
            'page'             => $page,
            'id'               => $id,
            'billing'          => $bill,
        ]);

        return self::jsonSuccess([
            'html' => $html
        ]);
    }


    /**
     * Получает счет ID
     * Делает проверку на хозяина
     *
     * @return \common\models\avatar\UserBill
     * @throws \yii\base\Exception
     */
    private function getBill($id = null)
    {
        if (is_null($id)) {
            $id = self::getParam('id');
        }
        $bill = UserBill::findOne($id);

        return $bill;
    }

    /**
     * Присоединяет колонку комментарий для транзакций
     *
     * @param \common\models\avatar\UserBill $billing
     * @param array $data
     * [
     *      [
     *          //...
     *      ],
     *      //...
     * ]
     *
     * @return array
     * [
     *      [
     *          //...
     *          'comment' => 'Перевод от клиента №'
     *          'type' => 1
     *      ],
     *      //...
     * ]
     */
    private function concatComments($billing, $data, $itemsPerPage = 20)
    {
        $firstTime = $data[0]['time'];
        $firstTime = (new \DateTime($firstTime))->format('U');

        $comments = \common\models\avatar\UserBillOperation::find()
            ->where([
                'and',
                ['bill_id' => $billing->id],
                ['<=', 'created_at', $firstTime + 10],
            ])
            ->limit($itemsPerPage + 2)
            ->orderBy(['created_at' => SORT_DESC])
            ->all();
        ;

        $rows = [];
        foreach($data as $rowOld) {
            $rowNew = $rowOld;
            $comment = $this->getTransaction($rowOld['hash'], $comments);
            if (!is_null($comment)) {
                $rowNew['comment'] = $comment->message;
                $rowNew['type'] = $comment->type;
            } else {
                $rowNew['comment'] = '';
                $rowNew['type'] = null;
            }
            $rows[] = $rowNew;
        }

        return $rows;
    }

    /**
     * Ищет определенную транзакцию и возвращает ее
     *
     * @param int $hash
     * @param \common\models\avatar\UserBillOperation[] $array
     * @return \common\models\avatar\UserBillOperation | null
     */
    public function getTransaction($hash, $array)
    {
        foreach($array as $row) {
            $t = ArrayHelper::getValue($row, 'transaction', '');
            if ($t) {
                if ($t == $hash) {
                    return $row;
                }
            }
        }
        return null;
    }



    /**
     * Возвращает стандартный ответ JSON при положительном срабатывании
     * https://redmine.suffra.com/projects/suffra/wiki/Стандартный_ответ_JSON
     *
     * @param mixed $data [optional] возвращаемые данные
     *
     * @return \yii\web\Response json
     */
    public static function jsonSuccess($data = null)
    {
        if (is_null($data)) $data = 'ok';

        return self::json(['success' => $data]);
    }

    /**
     * Возвращает стандартный ответ JSON при отрицательном срабатывании
     * https://redmine.suffra.com/projects/suffra/wiki/Стандартный_ответ_JSON
     *
     * @param mixed $data [optional] возвращаемые данные
     *
     * @return \yii\web\Response json
     */
    public static function jsonError($data = null)
    {
        if (is_null($data)) $data = '';

        return self::json(['error' => $data]);
    }

    /**
     * Возвращает стандартный ответ JSON при отрицательном срабатывании
     * https://redmine.suffra.com/projects/suffra/wiki/Стандартный_ответ_JSON
     *
     * @param integer $id   идентификатор ошибки
     * @param mixed   $data [optional] возвращаемые данные
     *
     * @return \yii\web\Response json
     */
    public static function jsonErrorId($id, $data = null)
    {
        $return = [
            'id' => $id,
        ];
        if (!is_null($data)) $return['data'] = $data;

        return self::jsonError($return);
    }


    /**
     * Закодировать в JSON
     *
     * @return \yii\web\Response json
     * */
    public static function json($array)
    {
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        Yii::$app->response->data = $array;

        return Yii::$app->response;
    }

    /**
     * Возвращает переменную из REQUEST
     *
     * @param string $name    имя переменной
     * @param mixed  $default значние по умолчанию [optional]
     *
     * @return string|null
     * Если такой переменной нет, то будет возвращено null
     */
    public static function getParam($name, $default = null)
    {
        $vGet = \Yii::$app->request->get($name);
        $vPost = \Yii::$app->request->post($name);
        $value = (is_null($vGet)) ? $vPost : $vGet;

        return (is_null($value)) ? $default : $value;
    }

} 