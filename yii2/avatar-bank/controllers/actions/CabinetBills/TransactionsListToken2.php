<?php
/**
 * Created by PhpStorm.
 * User: Бог-Творец
 * Date: 25.11.2016
 * Time: 2:04
 */
namespace avatar\controllers\actions\CabinetBills;

use avatar\modules\ETH\ServiceEtherScan;
use common\models\avatar\Currency;
use common\models\avatar\UserBill;
use common\models\Config;
use common\models\UserAvatar;
use console\controllers\MoneyRateController;
use cs\services\VarDumper;
use Yii;
use yii\base\Action;
use yii\base\ExitException;
use yii\helpers\ArrayHelper;

class TransactionsListToken2 extends \avatar\controllers\action\BaseAction
{
    /** @var  \common\models\avatar\UserBill счет на который вычисляются транзакции */
    public $billing;

    /** @var  \avatar\controllers\CabinetBillsController */
    public $controller;

    /**
     * AJAX
     *
     * REQUEST:
     * - page - int - номер страницы. Если нет траницы то показываю первую
     * - id - int - идентификатор счета \common\models\avatar\UserBill
     *
     * @return string
     * @throws
     */
    public function run()
    {
        $id = self::getParam('id', '');
        if ($id == '') {
            return $this->controller->jsonErrorId(101, 'Не указан параметр id');
        }
        $page = self::getParam('page', 1);
        $itemsPerPage = 20;

        $bill = $this->getBill($id);
        $wallet = $bill->getWalletToken();
        try {
            $transactions = $wallet->transactionListComments2();
        } catch (\Exception $e) {
            Yii::info($e->getMessage(), 'avatar\controllers\actions\CabinetBills\TransactionsListToken::run');
            throw $e;
        }

        $file = Yii::getAlias('@avatar/views/' . $this->controller->id . '/transactions-list-token2.php');
        $html = $this->controller->view->renderFile($file, [
            'transactions'     => $transactions,
            'page'             => $page,
            'id'               => $id,
            'billing'          => $bill,
        ]);

        return $this->controller->jsonSuccess([
            'html' => $html
        ]);
    }

    public function getCurrencyTo()
    {
        /**
         * @var array $list
         * [
         *      [
         *          'id' => 1,
         *          'name' => 1,
         *          'code' => 1,
         *          'kurs' => 1,
         *      ],
         * ]
         */
        $list = Currency::getList();
        $currencyView = $this->billing->currency;
        foreach ($list as $row) {
            if ($row['id'] == $currencyView) {
                return $row['code'];
            }
        }
        throw new \Exception('Не найдена валюта');
    }

    /**
     * Добавляет расширенную информацию
     *
     * @param $transactionList
     * @return array
     */
    public function addExtendedInfo($transactionList)
    {
        foreach($transactionList as $transaction) {

        }
//        commentObject
        return $transactionList;
    }

    /**
     * Получает счет ID
     * Делает проверку на хозяина
     *
     * @return \common\models\avatar\UserBill
     * @throws \yii\base\Exception
     */
    private function getBill($id = null)
    {
        if (is_null($id)) {
            $id = self::getParam('id');
        }
        $bill = UserBill::findOne($id);
        if ($bill->user_id != Yii::$app->user->id) {
            throw new \yii\base\Exception('Не ваш счет');
        }

        return $bill;
    }

    /**
     * Присоединяет колонку комментарий для транзакций
     *
     * @param \common\models\avatar\UserBill $billing
     * @param array $data
     * [
     *      [
     *          //...
     *      ],
     *      //...
     * ]
     *
     * @return array
     * [
     *      [
     *          //...
     *          'comment' => 'Перевод от клиента №'
     *          'type' => 1
     *      ],
     *      //...
     * ]
     */
    private function concatComments($billing, $data, $itemsPerPage = 20)
    {
        $firstTime = $data[0]['time'];

        $comments = \common\models\avatar\UserBillOperation::find()
            ->where([
                'and',
                ['bill_id' => $billing->id],
                ['<=', 'created_at', $firstTime + 60],
            ])
            ->limit($itemsPerPage + 10)
            ->orderBy(['created_at' => SORT_DESC])
            ->all();
        ;

        $rows = [];
        foreach($data as $rowOld) {
            $rowNew = $rowOld;
            $comment = $this->getTransaction($rowOld['hash'], $comments);
            if (!is_null($comment)) {
                $rowNew['commentObject'] = $comment;
                $rowNew['comment'] = $comment->message;
                $rowNew['type'] = $comment->type;
                $rowNew['data'] = $comment->data;
            } else {
                $rowNew['comment'] = '';
                $rowNew['type'] = null;
            }
            $rows[] = $rowNew;
        }

        return $rows;
    }

    /**
     * Ищет определенную транзакцию в списке коментариев и возвращает ее
     *
     * @param int $hash
     * @param \common\models\avatar\UserBillOperation[] $array
     * @return \common\models\avatar\UserBillOperation | null
     */
    public function getTransaction($hash, $array)
    {
        foreach($array as $row) {
            $t = ArrayHelper::getValue($row, 'transaction', '');
            if ($t) {
                if ($t == $hash) {
                    return $row;
                }
            }
        }
        return null;
    }



    /**
     * Возвращает переменную из REQUEST
     *
     * @param string $name    имя переменной
     * @param mixed  $default значние по умолчанию [optional]
     *
     * @return string|null
     * Если такой переменной нет, то будет возвращено null
     */
    public static function getParam($name, $default = null)
    {
        $vGet = \Yii::$app->request->get($name);
        $vPost = \Yii::$app->request->post($name);
        $value = (is_null($vGet)) ? $vPost : $vGet;

        return (is_null($value)) ? $default : $value;
    }

} 