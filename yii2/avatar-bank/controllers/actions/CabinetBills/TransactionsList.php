<?php
/**
 * Created by PhpStorm.
 * User: Бог-Творец
 * Date: 25.11.2016
 * Time: 2:04
 */
namespace avatar\controllers\actions\CabinetBills;

use common\models\avatar\Currency;
use common\models\avatar\UserBill;
use common\models\Config;
use common\models\UserAvatar;
use common\payment\BitCoinBlockTrailPayment;
use console\controllers\MoneyRateController;
use cs\services\VarDumper;
use Yii;
use yii\base\Action;
use yii\base\ExitException;
use yii\helpers\ArrayHelper;

class TransactionsList extends Action
{
    /** @var  \common\models\avatar\UserBill счет на который вычисляются транзакции */
    public $billing;

    /** @var  \avatar\controllers\CabinetBillsController */
    public $controller;

    /**
     * AJAX
     *
     * REQUEST:
     * - page - int - номер страницы. Если нет траницы то показываю первую
     * - id - int - идентификатор счета \common\models\avatar\UserBill
     *
     * @return string
     * @throws
     */
    public function run()
    {
        $id = $this->controller->getParam('id', '');
        if ($id == '') {
            return $this->controller->jsonErrorId(101, 'Не указан параметр id');
        }
        $page = $this->controller->getParam('page', 1);
        $itemsPerPage = 20;

        $bill = $this->controller->getBill($id);
        $this->billing = $bill;
        Yii::info($bill->address, 'avatar\controllers\actions\CabinetBills\TransactionsList::run');
        if (YII_ENV_DEV) {
            $transactions = [];
            $transactions['data'] = [];

        } else {
            $provider = new BitCoinBlockTrailPayment();
            $client = $provider->getClient();
            $transactions = $client->walletTransactions($bill->identity, $page, $itemsPerPage, 'desc');
            $transactions['data'] = $this->concatComments($bill, $transactions['data']);
            $transactions['data'] = $this->changeRateComments2($transactions['data']);
        }
        $file = Yii::getAlias('@avatar/views/' . $this->controller->id . '/transactions-list.php');
        $html = $this->controller->view->renderFile($file, [
            'transactions'     => $transactions,
            'page'             => $page,
            'id'               => $id,
            'billing'          => $bill,
        ]);

        return $this->controller->jsonSuccess([
            'html' => $html
        ]);
    }

    /**
     * Если необхдимо то конвертирует валюту
     *
     * @param array $transactionList
     *
     * @return array
     */
    public function changeRateComments2($transactionList)
    {
        /** @var \common\models\UserAvatar $user */
        $user = Yii::$app->user->identity;
        /** @var int $currencyView валюта в которой хочет видеть пользователь */
        $currencyView = $user->currency_view;

        /** @var string $CurrencyTo трехбуквенный код валююты в который надо конвертировать в зависимости от счета */
        $CurrencyTo = 'BTC';
        if (!is_null($currencyView)) {
            for ($i = 0; $i < count($transactionList); $i++) {
                $v =  $transactionList[$i]['wallet_value_change'];
                if ($v < 0) {
                    $v += $transactionList[$i]['total_fee'];
                }
                $confirmed = Currency::convertBySettingsFormat($v / 100000000, $CurrencyTo);
                $fee = $transactionList[$i]['total_fee'];
                $fee = Currency::convertBySettingsFormat($fee / 100000000, $CurrencyTo);

                $transactionList[$i]['wallet_value_change_currency'] = [
                    'value' => $confirmed,
                    'type'  => ($transactionList[$i]['wallet_value_change'] < 0) ? -1 : 1,
                    'fee'   => $fee,
                ];
            }
        }

        return $transactionList;
    }

    /**
     * Присоединяет колонку комментарий для транзакций
     *
     * @param \common\models\avatar\UserBill $billing
     * @param array $data
     * [
     *      [
     *          //...
     *      ],
     *      //...
     * ]
     *
     * @return array
     * [
     *      [
     *          //...
     *          'comment' => 'Перевод от клиента №'
     *          'type' => 1
     *      ],
     *      //...
     * ]
     */
    private function concatComments($billing, $data, $itemsPerPage = 20)
    {
        $firstTime = $data[0]['time'];
        Yii::trace('$firstTime=' . \yii\helpers\VarDumper::dumpAsString($firstTime) . ' ' . \yii\helpers\VarDumper::dumpAsString((new \DateTime($firstTime))->format('U')), 'avatar\\concatComments');
        $firstTime = (int)(new \DateTime($firstTime))->format('U');

        $comments = \common\models\avatar\UserBillOperation::find()
            ->where([
                'and',
                ['bill_id' => $billing->id],
                ['<=', 'created_at', $firstTime + 60],
            ])
            ->limit($itemsPerPage + 10)
            ->orderBy(['created_at' => SORT_DESC])
            ->all();
        ;

        $rows = [];
        foreach($data as $rowOld) {
            $rowNew = $rowOld;
            $comment = $this->getTransaction($rowOld['hash'], $comments);
            if (!is_null($comment)) {
                $rowNew['commentObject'] = $comment;
                $rowNew['comment'] = $comment->message;
                $rowNew['type'] = $comment->type;
                $rowNew['data'] = $comment->data;
            } else {
                $rowNew['comment'] = '';
                $rowNew['type'] = null;
            }
            $rows[] = $rowNew;
        }

        return $rows;
    }

    /**
     * Ищет определенную транзакцию в списке коментариев и возвращает ее
     *
     * @param int $hash
     * @param \common\models\avatar\UserBillOperation[] $array
     * @return \common\models\avatar\UserBillOperation | null
     */
    public function getTransaction($hash, $array)
    {
        foreach($array as $row) {
            $t = ArrayHelper::getValue($row, 'transaction', '');
            if ($t) {
                if ($t == $hash) {
                    return $row;
                }
            }
        }
        return null;
    }
}