<?php

namespace avatar\controllers\actions\CabinetSchoolTaskListController;

/**
 * Created by PhpStorm.
 * User: s.arhangelskiy
 * Date: 08.08.2017
 * Time: 17:58
 */

use avatar\models\WalletETH;
use avatar\models\WalletToken;
use common\models\avatar\Currency;
use common\models\avatar\UserBill;
use common\models\BillingMain;
use common\models\comment\Comment;
use common\models\comment\CommentList;
use common\models\CompanyTaskTransaction;
use common\models\investment\IcoRequest;
use common\models\PaySystemConfig;
use common\models\piramida\Wallet;
use common\models\school\School;
use common\models\SchoolCoin;
use common\models\task\Task;
use common\models\Token;
use common\models\UserAvatar;
use common\models\UserWallet;
use common\services\Subscribe;
use cs\Application;
use cs\services\File;
use Yii;
use yii\base\Exception;
use yii\helpers\FileHelper;
use yii\imagine\Image;

/**
 * REQUEST
 * - id
 */
class actionViewDone extends \avatar\base\BaseAction
{

    public function run()
    {
        $task = Task::findOne(self::getParam('id'));
        $statusList = \common\models\task\Status::find()
            ->where(['school_id' => $task->school_id])
            ->orderBy(['sort_index' => SORT_ASC])
            ->select('id')
            ->column();

        $task->status = $statusList[2];
        $task->save();

        $comment = '';
        $s = School::findOne($task->school_id);

        // Высылаю деньги
        if (is_null($s)) {
            throw new \Exception('Не найдена школа которая указана в задаче');
        }

        self::doMoney($task, $s);

        if ($comment == '') {
            $comment = 'Задача #' . $task->id . ' принята';

            // добавляю коментарий
            $commentObject = Comment::add([
                'text'    => $comment,
                'list_id' => CommentList::get(1, $task->id)->id,
                'user_id' => Yii::$app->user->id,
            ]);
        }

        // Высылаю на почту исполнителю
        Subscribe::sendArraySchool(
            $task->getSchool(),
            [
                $task->getExecuter()->email,
            ],
            'Задача принята',
            'task/done',
            [
                'task' => $task,
                'text' => $comment,
            ],
            'layouts/html/task'
        );

        return self::jsonSuccess();
    }

    /**
     * Переводит деньги исполнителю
     *
     * @param \common\models\task\Task $task
     * @param \common\models\school\School $s
     * @throws \Exception
     */
    public static function doMoney($task, $s)
    {
        $sc = SchoolCoin::findOne(['currency_id' => $task->currency_id, 'school_id' => $task->school_id]);
        if (!is_null($sc)) {
            switch ($task->wallet_type) {
                case Task::WALLET_TYPE_SCHOOL:
                    $wallet = Wallet::findOne($sc->wallet_id);
                    if (!is_null($wallet)) {
                        if ($task->currency_id == $wallet->currency_id) {
                            // валюта школы
                            if ($task->price > 0) {
                                if ($wallet->amount >= $task->price) {
                                    $currency = $wallet->getCurrency();
                                    $comment = 'Вознаграждение ' . Yii::$app->formatter->asDecimal($task->price / pow(10, $currency->decimals), $currency->decimals) . ' ' . $currency->code . ' За задачу #' . $task->id;
                                    $wallet->move(
                                        UserBill::getBillByCurrencyAvatarProcessing($task->currency_id, $task->executer_id)->address,
                                        $task->price,
                                        $comment
                                    );
                                    // добавляю коментарии
                                    $commentObject = Comment::add([
                                        'text'    => $comment,
                                        'list_id' => CommentList::get(1, $task->id)->id,
                                        'user_id' => Yii::$app->user->id,
                                    ]);
                                }
                            }
                        }
                    }


                    break;

                case Task::WALLET_TYPE_AUTHOR:
                    if (!Application::isEmpty($task->currency_id)) {
                        if ($task->price > 0) {
                            $sc = SchoolCoin::findOne(['currency_id' => $task->currency_id, 'school_id' => $task->school_id]);
                            $budjet_currency_id = $sc->budjet_currency_id;

                            $cInt = $task->currency_id;
                            $billFrom = UserBill::getBillByCurrencyAvatarProcessing($budjet_currency_id, $task->user_id);
                            $billTo = UserBill::getBillByCurrencyAvatarProcessing($cInt, $task->executer_id);
                            $wFrom = Wallet::findOne($billFrom->address);
                            $wTo = Wallet::findOne($billTo->address);

                            // списываю бюджет
                            $comment =  'Вознаграждение за задачу #' . $task->id;
                            $tburn = $wFrom->out($task->price, $comment);

                            // Докладываю вознаграждение пользователю
                            $comment =  'Вознаграждение за задачу #' . $task->id;
                            $temission = $wTo->in($task->price, $comment);

                            CompanyTaskTransaction::add([
                                'task_id'      => $task->id,
                                'emission_oid' => $temission->id,
                                'burn_oid'     => $tburn->id,
                            ]);

                            // добавляю коментарий в задачу
                            $commentObject = Comment::add([
                                'text'    => $comment . ' oid='.$temission->getAddress(),
                                'list_id' => CommentList::get(1, $task->id)->id,
                                'user_id' => $task->user_id,
                            ]);
                        }
                    }
                    break;

                case Task::WALLET_TYPE_USER:
                    if (!Application::isEmpty($task->currency_id)) {
                        if (!Application::isEmpty($task->wallet_type3_user_id)) {
                            if ($task->price > 0) {
                                $sc = SchoolCoin::findOne(['currency_id' => $task->currency_id, 'school_id' => $task->school_id]);
                                $budjet_currency_id = $sc->budjet_currency_id;

                                $cInt = $task->currency_id;
                                $billFrom = UserBill::getBillByCurrencyAvatarProcessing($budjet_currency_id, $task->wallet_type3_user_id);
                                $billTo = UserBill::getBillByCurrencyAvatarProcessing($cInt, $task->executer_id);
                                $wFrom = Wallet::findOne($billFrom->address);
                                $wTo = Wallet::findOne($billTo->address);

                                // списываю бюджет
                                $comment =  'Вознаграждение за задачу #' . $task->id;
                                $tburn = $wFrom->out($task->price, $comment);

                                // Докладываю вознаграждение пользователю
                                $comment =  'Вознаграждение за задачу #' . $task->id;
                                $temission = $wTo->in($task->price, $comment);

                                CompanyTaskTransaction::add([
                                    'task_id'      => $task->id,
                                    'emission_oid' => $temission->id,
                                    'burn_oid'     => $tburn->id,
                                ]);

                                // добавляю коментарий в задачу
                                $commentObject = Comment::add([
                                    'text'    => $comment . ' oid=' . $temission->getAddress(),
                                    'list_id' => CommentList::get(1, $task->id)->id,
                                    'user_id' => $task->user_id,
                                ]);
                            }
                        }
                    }
                    break;
                default:
                    break;
            }
        }
    }
}