<?php

namespace avatar\controllers\actions\CabinetSchoolTaskListController;

/**
 * Created by PhpStorm.
 * User: s.arhangelskiy
 * Date: 08.08.2017
 * Time: 17:58
 */

use avatar\models\WalletETH;
use avatar\models\WalletToken;
use common\models\avatar\Currency;
use common\models\avatar\UserBill;
use common\models\BillingMain;
use common\models\comment\Comment;
use common\models\comment\CommentList;
use common\models\investment\IcoRequest;
use common\models\PaySystemConfig;
use common\models\piramida\Wallet;
use common\models\school\School;
use common\models\task\Task;
use common\models\Token;
use common\models\UserAvatar;
use common\models\UserWallet;
use common\services\Subscribe;
use cs\services\File;
use Yii;
use yii\base\Exception;
use yii\helpers\FileHelper;
use yii\imagine\Image;

/**
 * REQUEST
 * - id
 */
class actionChangePrice extends \avatar\base\BaseAction
{

    public function run()
    {
        $task = Task::findOne(self::getParam('id'));



        $statusList = \common\models\task\Status::find()
            ->where(['school_id' => $task->school_id])
            ->orderBy(['sort_index' => SORT_ASC])
            ->select('id')
            ->column();



        $comment = '';
        $s = School::findOne($task->school_id);

        // Высылаю деньги
        if (is_null($s)) {
            throw new \Exception('Не найдена школа которая указана в задаче');
        }

        if ($task->currency_id == 7) {
            // Билет Банка России 810

            $wallet = Wallet::findOne($s->fund_rub2);
            if (!is_null($wallet)) {
                if ($task->price > 0) {
                    if ($wallet->amount >= $task->price) {
                        $currency = $wallet->getCurrency();
                        $comment = 'Ты - это Я! Я - это Ты! Нет меня без Тебя! Я люблю Тебя! Посылаю Тебе ' . Yii::$app->formatter->asDecimal($task->price / pow(10, $currency->decimals), $currency->decimals) . ' ' . $wallet->getCurrency()->code . '! За задачу #' . $task->id;
                        $wallet->move(
                            UserBill::getBillByCurrencyAvatarProcessing($task->currency_id, $task->executer_id)->address,
                            $task->price,
                            $comment
                        );
                        // добавляю коментарии
                        $commentObject = Comment::add([
                            'text'    => $comment,
                            'list_id' => CommentList::get(1, $task->id)->id,
                            'user_id' => Yii::$app->user->id,
                        ]);
                    }
                }
            }
        } else if ($s->wallet_id) {
            $wallet = Wallet::findOne($s->wallet_id);
            if (!is_null($wallet)) {
                if ($task->currency_id == $wallet->currency_id) {
                    // валюта школы
                    if ($task->price > 0) {
                        if ($wallet->amount >= $task->price) {
                            $currency = $wallet->getCurrency();
                            $comment = 'Ты - это Я! Я - это Ты! Нет меня без Тебя! Я люблю Тебя! Посылаю Тебе ' . Yii::$app->formatter->asDecimal($task->price / pow(10, $currency->decimals), $currency->decimals) . ' ' . $currency->code . '! За задачу #' . $task->id;
                            $wallet->move(
                                UserBill::getBillByCurrencyAvatarProcessing($task->currency_id, $task->executer_id)->address,
                                $task->price,
                                $comment
                            );
                            // добавляю коментарии
                            $commentObject = Comment::add([
                                'text'    => $comment,
                                'list_id' => CommentList::get(1, $task->id)->id,
                                'user_id' => Yii::$app->user->id,
                            ]);
                        }
                    }
                }
            }
        }
        if ($comment == '') {
            $comment = 'Задача #' . $task->id . ' принята';

            // добавляю коментарий
            $commentObject = Comment::add([
                'text'    => $comment,
                'list_id' => CommentList::get(1, $task->id)->id,
                'user_id' => Yii::$app->user->id,
            ]);
        }

        // Высылаю на почту исполнителю
        Subscribe::sendArraySchool(
            $task->getSchool(),
            [
                $task->getExecuter()->email,
            ],
            'Задача принята',
            'task/done',
            [
                'task' => $task,
                'text' => $comment,
            ],
            'layouts/html/task'
        );

        return self::jsonSuccess();
    }
}