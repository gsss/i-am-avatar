<?php

namespace avatar\controllers\actions\CabinetSchoolTaskListController;

/**
 * Created by PhpStorm.
 * User: s.arhangelskiy
 * Date: 08.08.2017
 * Time: 17:58
 */

use avatar\models\WalletETH;
use avatar\models\WalletToken;
use common\models\avatar\Currency;
use common\models\avatar\UserBill;
use common\models\BillingMain;
use common\models\comment\Comment;
use common\models\comment\CommentList;
use common\models\investment\IcoRequest;
use common\models\PaySystemConfig;
use common\models\piramida\Wallet;
use common\models\school\School;
use common\models\task\Session;
use common\models\task\Task;
use common\models\Token;
use common\models\UserAvatar;
use common\models\UserWallet;
use common\services\Subscribe;
use cs\services\File;
use Yii;
use yii\base\Exception;
use yii\helpers\FileHelper;
use yii\imagine\Image;
use yii\web\Response;

/**
 */
class actionSessionFinish extends \avatar\base\BaseAction
{

    public function run()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        $task = Task::findOne(self::getParam('id'));
        if (is_null($task)) {
            throw new \Exception('Не найдена задача');
        }

        // проверить а есть ли не закрытая сессия
        /** @var \common\models\task\Session $session */
        $session = Session::find()->where(['task_id' => $task->id, 'is_finish' => 0])->one();

        if (is_null($session)) {
            throw new \Exception('Вы еще не начали сессию');
        }
        $session->finish = time();
        $session->is_finish = 1;
        $session->save();

        return self::jsonSuccess();
    }
}