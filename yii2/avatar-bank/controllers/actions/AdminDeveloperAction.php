<?php
/**
 * Created by PhpStorm.
 * User: Бог-Творец
 * Date: 25.11.2016
 * Time: 2:04
 */
namespace avatar\controllers\actions;

use common\models\piramida\UserBillOperation;
use Yii;
use yii\base\Action;
use yii\helpers\ArrayHelper;

class AdminDeveloperAction extends \avatar\controllers\actions\BaseAction
{
    public function run()
    {
        return $this->controller->render($this->id);
    }
}