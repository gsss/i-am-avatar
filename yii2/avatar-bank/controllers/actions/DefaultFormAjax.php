<?php
/**
 * Created by PhpStorm.
 * User: s.arhangelskiy
 * Date: 18.01.2017
 * Time: 13:17
 */

namespace avatar\controllers\actions;

use cs\services\VarDumper;
use Yii;
use yii\base\Action;
use yii\base\Model;
use yii\helpers\Html;
use yii\web\Response;

class DefaultFormAjax extends BaseAction
{
    public $model;
    public $formName = '';

    public function run()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        $class = $this->model;
        /** @var Model $model */
        $model = new $class();
        if (Yii::$app->request->isPost) {
            if ($model->load(Yii::$app->request->post()) && $model->validate()) {
                $s = $model->save();

                return self::jsonSuccess($s);
            } else {
                $fields = [];
                foreach ($model->attributes as $k => $v) {
                    $fields[$k] = Html::getInputId($model, $k);
                }

                return self::jsonErrorId(102, [
                    'errors' => $model->errors,
                    'fields' => $fields,
                ]);
            }
        }
    }
}