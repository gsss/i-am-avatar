<?php

namespace avatar\controllers;


use app\models\Chenneling;
use app\models\Form\Event;
use app\models\HD;
use app\models\HdGenKeys;
use app\models\HDtown;
use app\models\Log;
use app\models\SiteUpdate;
use app\models\Union;
use app\models\User;
use app\models\UserRod;
use app\services\GetArticle\YouTube;
use app\services\GraphExporter;
use app\services\HumanDesign2;
use app\services\investigator\MidWay;
use app\services\LogReader;
use cs\Application;
use cs\base\BaseController;
use cs\helpers\Html;
use cs\services\SitePath;
use cs\services\Str;
use cs\services\VarDumper;
use Yii;
use yii\base\Exception;
use yii\base\UserException;
use yii\data\ActiveDataProvider;
use yii\data\Sort;
use yii\db\Query;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;
use yii\helpers\StringHelper;
use yii\web\Controller;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use yii\web\Response;
use yii\widgets\ActiveForm;
use app\services\RegistrationDispatcher;

class HumanDesignController extends \avatar\base\BaseController
{
    const CACHE_NAME_VERIFY_CODE = 'route:human_design/index/code';
    const SESSION_KEY = 'humanDesignCache';

    public $sessionKey = 'humanDesignCache';

    public $layout = 'menu';

    public function actionIndex()
    {
        return $this->render();
    }

    public function actionAbout()
    {
        return $this->render();
    }

    public function actionKeys()
    {
        return $this->render();
    }

    public function actionCross()
    {
        return $this->render();
    }

    /**
     * AJAX
     * Расчет дизайна
     *
     * REQUEST:
     * - date - string - 'dd.mm.yyyy'
     * - time - string - 'hh:mm'
     * - londonOffset - int - смещение при летнем времени в секундах на лондоне
     * - placeOffset - int - смещение в секундах для места назначения
     * - location - string - lat,lng
     * - verifyCode - string - строка генерируемая страницей отображения формы и записываемая в переменную сессии под ключом 'route:human_design/index/code'
     *
     * @return array
     * @throws
     */
    public function actionAjax()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        // валидация данных
        {
            $date = self::getParam('date');
            $v = $this->validateDate($date);
            if ($v != 1) {
                return self::jsonErrorId(103, [$v, 'Не верный формат даты']);
            }
            $time = self::getParam('time');
            if (!$this->validateTime($time)) {
                return self::jsonErrorId(104, 'Не верный формат времени');
            }
            $a = explode(':', $time);
            if (strlen($a[0]) == 1) $time = '0' . $a[0] . ':' . $a[1];
            $verifyCode = self::getParam('verifyCode');
            $session = Yii::$app->getSession();
            $sessionVerifyCode = $session->get(self::CACHE_NAME_VERIFY_CODE, '');
            if ($sessionVerifyCode == '') {
                return self::jsonErrorId(101, 'Проверочный код не соответсвует указанному в форме');
            } else {
                if ($sessionVerifyCode != $verifyCode) {
                    return self::jsonErrorId(102, 'Проверочный код не соответсвует указанному в форме');
                }
            }
        }

        $date = explode('.', $date);
        $placeOffset = (int)self::getParam('placeOffset');
        $londonOffset = (int)self::getParam('londonOffset');
        $location = self::getParam('location');
        $location = explode(',', $location);
        // получаю время и дату в лондоне
        {
            $t2 = new \DateTime($date[2].'-'.$date[1].'-'.$date[0].' '.$time.':00', new \DateTimeZone('UTC'));
            $timestamp = (int)$t2->format('U');
            $timestamp = $timestamp - $placeOffset + $londonOffset;
            $londonTime = new \DateTime('@'.$timestamp, new \DateTimeZone('UTC'));
            $dateLondon = $londonTime->format('Y-m-d H:i:s');
            $t = new \DateTime($dateLondon, new \DateTimeZone('Europe/London'));
        }

        // получаю данные Дизайна Человека
        $extractor = new \app\modules\HumanDesign\calculate\HumanDesignAmericaCom();
        $data = $extractor->calc($t->format('U') . '000', $t->format('d/m/Y'), $t->format('H:i'));

        $html = Yii::$app->view->renderFile('@avatar/views/human-design/ajax.php', [
            'human_design'  => $data,
            'birth_date'    => $date[2] . '-' . $date[1] . '-' . $date[0],
            'birth_time'    => $time.':00',
            'birth_lat'     => $location[0],
            'birth_lng'     => $location[1],
        ]);
        // сохраняю данные в сессию
        Yii::$app->session->set($this->sessionKey, $html);

        return self::jsonSuccess($html);
    }

    public function actionAjaxDelete()
    {
        Yii::$app->session->remove($this->sessionKey);

        return self::jsonSuccess();
    }

    /**
     * Проверяет дату на валидность
     *
     * @param string $value строка в формате dd.mm.yyyy
     *
     * @return int
     * 1 - все хорошо
     * больше 1 - ошибка
     */
    public function validateDate($value)
    {
        if (strlen($value) != 10) return 2;
        $arr = explode('.', $value);
        if (count($arr) < 3) return 3;
        if (strlen($arr[0]) > 2) return 4;
        if (strlen($arr[1]) > 2) return 5;
        if (strlen($arr[2]) > 4) return 6;
        if (strlen($arr[0]) < 2) return 7;
        if (strlen($arr[1]) < 2) return 8;
        if (strlen($arr[2]) < 4) return 9;
        if ($arr[0] < 0) return 10;
        if ($arr[0] > 31) return 11;
        if ($arr[1] < 0) return 12;
        if ($arr[1] > 12) return 13;
        if ($arr[1] < 0) return 14;

        return 1;
    }

    /**
     * Проверяет время на валидность
     *
     * @param string $value строка в формате hh:mm
     *
     * @return bool
     */
    public function validateTime($value)
    {
        $arr = explode(':', $value);
        if (count($arr) < 2) return false;
//        if (strlen($arr[0]) < 2) return false;
        if (strlen($arr[1]) < 2) return false;
        if ($arr[0] < 0) return false;
        if ($arr[0] > 23) return false;
        if ($arr[1] < 0) return false;
        if ($arr[1] >= 60) return false;

        return true;
    }

    /**
     * @param int $id номер генного ключа
     * @return string
     * @throws Exception
     */
    public function actionGenKeysItem($id)
    {
        $genKey = \common\models\HdGenKeys::findOne(['num' => $id]);
        if (is_null($genKey)) {
            throw new Exception('Не найден такой ключ');
        }

        return $this->render('gen-keys-item', [
            'item' => $genKey,
        ]);
    }

}
