<?php

namespace avatar\controllers;

use avatar\models\forms\Contact;
use avatar\modules\ETH\ServiceEtherScan;
use avatar\modules\UniSender\UniSender;
use avatar\services\LogReader;
use avatar\widgets\SecretKey;
use common\components\providers\BTC;
use common\components\providers\ETH;
use common\components\sms\IqSms;
use common\models\avatar\Currency;
use common\models\avatar\QrCode;
use common\models\avatar\UserBill;
use common\models\avatar\UserBillConfig;
use common\models\BillingMain;
use common\models\Card;
use common\models\Config;
use common\models\PaymentBitCoin;
use common\models\piramida\Billing;
use common\models\piramida\Wallet;
use common\models\RequestTokenCreate;
use common\models\school\Page;
use common\models\school\PotokUser3Link;
use common\models\school\School;
use common\models\school\Sertificate;
use common\models\school\TildaPlugin;
use common\models\school\UserLink;
use common\models\statistic\UserBinanceStatisticItem;
use common\models\Token;
use common\models\UserAvatar;
use common\models\UserDigitalSign;
use common\models\UserDocument;
use common\models\UserRegistration;
use common\models\UserRoot;
use common\models\UserSeed;
use common\payment\BitCoinBlockTrailPayment;
use common\services\CloudFlare;
use common\services\Security;
use common\services\Subscribe;
use common\services\TildaApi;
use cs\Application;
use cs\base\BaseController;
use cs\services\File;
use cs\services\Str;
use cs\services\UploadFolderDispatcher;
use cs\services\VarDumper;
use cs\web\Exception;
use Imagine\Gd\Image;
use Yii;
use yii\base\UserException;
use yii\db\Connection;
use yii\db\Query;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use yii\helpers\FileHelper;
use yii\helpers\Json;
use yii\helpers\StringHelper;
use yii\helpers\Url;
use yii\httpclient\Client;
use yii\web\Response;
use iAvatar777\services\DateRus\DateRus;

class TestController extends \yii\web\Controller
{
    public $layout = 'menu';


    public function actionTest1()
    {
//        $u = UserAvatar::findOne(4617);
        $UserBill = UserBill::getBillByCurrencyAvatarProcessing(25, 4617);

        VarDumper::dump($UserBill);
    }

    public function actionTest2()
    {
        $b = BillingMain::findOne(259);
        $b->is_paid = 0;
        $b->is_paid_client = 0;
        $b->save();
        VarDumper::dump(1);
    }

    private function sendError($text)
    {
        /** @var \aki\telegram\Telegram $telegram */
        $telegram = \Yii::$app->telegram;

        $users = \Yii::$app->authManager->getUserIdsByRole('role_admin');
        foreach ($users as $uid) {
            $u = UserAvatar::findOne($uid);
            if (!Application::isEmpty($u->telegram_chat_id)) {
                $response = $telegram->sendMessage([
                    'chat_id' => $u->telegram_chat_id,
                    'text'    => $text
                ]);
            }
        }
    }
    public function actionApp()
    {

        VarDumper::dump(Yii::$app);
    }

    public function actionEmail()
    {
        $v = Application::mail('dram1008@yandex.ru', '1', 'test', ['text' => 'sss']);
        VarDumper::dump($v);
    }

    public function actionEmailOrder()
    {
        $v = Subscribe::sendArray(['dram1008@yandex.ru'], '1', 'test', ['text' => 'sss']);
        VarDumper::dump($v);
    }

    /**
     */
    public function actionTest1112()
    {
        $db = [
            'i_am_avatar_prod_main'   => 'db',
            'i_am_avatar_prod_stat'   => 'dbStatistic',
            'i_am_avatar_prod_wallet' => 'dbWallet',
        ];
        $dbList = [];

        $rows1 = \common\models\information_schema\Tables::find()
            ->select([
                'TABLE_SCHEMA',
            ])
            ->groupBy(['TABLE_SCHEMA'])
            ->asArray()
            ->all();

        foreach ($rows1 as $item) {
            $dbName = $item['TABLE_SCHEMA'];

            if (isset($db[$dbName])) {
                $dbItem = [
                    'name'      => $dbName,
                    'component' => $db[$dbName],
                ];
                $rows2 = \common\models\information_schema\Tables::find()
                    ->select([
                        'TABLE_NAME',
                    ])
                    ->where(['TABLE_SCHEMA' => $dbName])
                    ->groupBy(['TABLE_NAME'])
                    ->asArray()
                    ->all();
                $rows3 = [];
                foreach ($rows2 as $e) {
                    $rows3[] = [
                        'table' => $e['TABLE_NAME'],
                        'columns' => ArrayHelper::map(\common\models\information_schema\Column::find()
                            ->where([
                                'TABLE_SCHEMA' => $dbName,
                                'TABLE_NAME'   => $e['TABLE_NAME'],
                            ])
                            ->select([
                                'ORDINAL_POSITION',
                                'COLUMN_NAME',
                                'DATA_TYPE',
                                'COLUMN_TYPE',
                                'NUMERIC_PRECISION',
                                'NUMERIC_SCALE',
                                'IS_NULLABLE',
                                'COLUMN_DEFAULT',
                                'COLUMN_KEY',
                                'CHARACTER_MAXIMUM_LENGTH',
                                'CHARACTER_OCTET_LENGTH',
                            ])
                            ->asArray()
                            ->all(),
                            'ORDINAL_POSITION',
                            function ($i) {
                                return array_values($i);
                            }
                        )
                    ];
                }
                $dbItem['tables'] = $rows3;
                $dbList[] = $dbItem;
            }
        }
        $f = Yii::getAlias('@common/config/db.json');
        if (file_exists($f)) {
            unlink($f);
        }
        file_put_contents($f, Json::encode($dbList));

        echo 'saved @common/config/db.json'."\n";
    }


    /**
     */
    public function actionTest3()
    {
        VarDumper::dump(Application::mail('dram1008@yandex.ru', 'a', 'a'));
    }


    /**
     */
    public function actionTest5()
    {
        VarDumper::dump([UserAvatar::hashPassword('123')]);
    }

    /**
     */
    public function actionTest6()
    {
        $cid = [8];

        $w = [];
        foreach ($cid as $id) {
            if (!UserBill::find()->where(['card_id' => $id])->exists()) {
                $walletELXGOLD = Wallet::addNew(['currency_id' => 1]);
                $walletELXSKY = Wallet::addNew(['currency_id' => 2]);
                $billELXGOLD = UserBill::add([
                    'address'  => (string)$walletELXGOLD->id,
                    'currency' => 1,
                    'card_id'  => $id,
                    'name'     => $walletELXGOLD->getCurrency()->name,
                ]);
                $billELXSKY = UserBill::add([
                    'address'  => (string)$walletELXSKY->id,
                    'currency' => 2,
                    'card_id'  => $id,
                    'name'     => $walletELXSKY->getCurrency()->name,
                ]);
                $w[] = [$walletELXGOLD, $walletELXSKY, $billELXGOLD, $billELXSKY];
            }
        }

        VarDumper::dump($w);
    }

    /**
     */
    public function actionTest7()
    {
        /** @var \common\services\AvatarCloud $cloud */
        $cloud = Yii::$app->AvatarCloud;
        $path = '/FileUpload3/school_blog_article/00000037/original/image.png';
        $path = Yii::getAlias('@upload' . $path);
        $file = File::path($path);
        $path2 = '/FileUpload3/school_blog_article/00000034/original/image.jpg';
        $path2 = Yii::getAlias('@upload' . $path2);
        $file2 = File::path($path2);
        $response = $cloud->_post(null, 'upload/image', [
            'update' => Json::encode([
                [
                    'function' => 'crop',
                    'index'    => 'crop',
                    'options'  => [
                        'width'  => '300',
                        'height' => '300',
                        'mode'   => 'MODE_THUMBNAIL_CUT',
                    ],
                ],
            ]),
        ], ['files' => [$file, $file2]]);
        $data = Json::decode($response->content);
        if ($data['success']) {
            $fileList = $data['data']['fileList'];
            /**
             * 'file' => '58292_q217BWHG6d.png'
             * 'url' => 'https://cloud1.i-am-avatar.com/upload/cloud/15585/58292_q217BWHG6d.png'
             * 'size' => 771754
             * 'update' => [
             *      'crop' => 'https://cloud1.i-am-avatar.com/upload/cloud/15585/58292_q217BWHG6d_crop.png'
             * ]
             */
            $file = $fileList[0];

        }
    }

    /**
     */
    public function actionTest8()
    {

        VarDumper::dump([
            (new \DateTime('2019-05-22 19:43:01'))->format('U'),
            (new \DateTime('2019-05-22 19:45:01'))->format('U'),
        ]);
    }

    /**
     */
    public function actionChat()
    {

        return $this->render('chat');
    }

    /**
     */
    public function actionClouflare()
    {
        /** @var \common\services\CloudFlare $CloudFlare */
        $CloudFlare = Yii::$app->CloudFlare;
        $data = $CloudFlare->_get('zones');
        $d = Json::decode($data->content);
        VarDumper::dump($d);
    }


}
