<?php

namespace avatar\controllers;

use avatar\modules\UniSender\UniSender;
use common\components\providers\ETH;
use common\models\avatar\Currency;
use common\models\avatar\UserBill;
use common\models\avatar\UserBillDefault;
use common\models\BillingMain;
use common\models\HD;
use common\models\HDtown;
use common\models\PaySystemSuccess;
use common\models\school\PotokUser3Link;
use common\models\school\School;
use common\models\subscribe\UserSubscribe;
use common\models\UserAvatar;
use common\models\UserDocument;
use common\models\UserMaster;
use common\models\UserTelegramConnect;
use common\models\UserTelegramTemp;
use common\services\Security\AES;
use cs\base\BaseController;
use cs\services\SitePath;
use cs\services\UploadFolderDispatcher;
use cs\services\VarDumper;
use cs\web\Exception;
use Yii;
use yii\base\Model;
use yii\base\UserException;
use yii\bootstrap\ActiveForm;
use yii\db\Connection;
use yii\db\Query;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use yii\helpers\FileHelper;
use yii\helpers\StringHelper;
use yii\web\Response;

class CabinetBillingController extends \avatar\controllers\CabinetController
{
    public $layout = 'cabinet';



    /**
     * Подтверждает платеж от клиента
     * AJAX POST
     * + billing_id int  идентификатор счета db.billing.id
     * - form [] array данные для формы если ои нужны
     */
    public function actionSuccess($id)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        $billing = BillingMain::findOne($id);
        if (is_null($billing)) {
            return self::jsonErrorId(404, 'Не найден счет');
        }
        $successObject = PaySystemSuccess::findOne(['paysystem_id' => $billing->getPaySystem()->id]);
        $message = null;
        if (!is_null($billing)) {
            $model =  $successObject->model;
            /** @var Model $modelObject */
            $modelObject = new $model(['billing' => $billing]);

            if (!$modelObject->load(Yii::$app->request->post(), 'model')) {
                return self::jsonErrorId(400, 'Не загружены данные');
            }
            if (!$modelObject->validate()) {
                return self::jsonErrorId(102, $modelObject->errors);
            }
            $message = $modelObject->action();
        }

        $billing->successClient($message);

        return self::jsonSuccess();
    }

    /**
     * Подтверждает платеж от клиента без формы
     * AJAX POST
     * + billing_id int  идентификатор счета db.billing.id
     * - form [] array данные для формы если ои нужны
     */
    public function actionSuccess2($id)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        $billing = BillingMain::findOne($id);
        if (is_null($billing)) {
            return self::jsonErrorId(404, 'Не найден счет');
        }
        $billing->successClient();

        return self::jsonSuccess();
    }

}