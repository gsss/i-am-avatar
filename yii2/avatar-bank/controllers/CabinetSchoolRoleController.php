<?php

namespace avatar\controllers;

use avatar\models\forms\UserAvatar;
use avatar\models\validate\CardNumberAccept;
use avatar\modules\ETH\ServiceEtherScan;
use avatar\modules\ETH\ServiceEthPlorer;
use common\components\Card;
use common\components\providers\BTC;
use common\components\providers\ETH;
use common\models\avatar\Currency;
use common\models\avatar\UserBill;
use common\models\blog\Article;
use common\models\Config;
use common\models\school\Kurs;
use common\models\school\Lesson;
use common\models\school\LessonPlace;
use common\models\school\LessonVideo;
use common\models\school\Role;
use common\models\school\School;
use common\models\UserSeed;
use common\payment\BitCoinBlockTrailPayment;
use common\services\Security;
use console\controllers\MoneyRateController;
use cs\services\Str;
use cs\services\VarDumper;
use cs\web\Exception;
use cs\Widget\FileUpload4\ModelFields;
use iAvatar777\services\FormAjax\DefaultFormAdd;
use iAvatar777\services\FormAjax\DefaultFormAjax;
use iAvatar777\services\FormAjax\DefaultFormDelete;
use Yii;
use yii\helpers\FileHelper;
use yii\helpers\Json;
use yii\helpers\StringHelper;
use yii\httpclient\Client;
use yii\imagine\Image;
use yii\web\Response;

class CabinetSchoolRoleController extends CabinetSchoolBaseController
{
    public function actions()
    {
        return [
            'delete' => [
                'class'    => '\iAvatar777\services\FormAjax\DefaultFormDelete',
                'model'    => '\avatar\models\forms\CabinetSchoolRoleAdd',
            ],
        ];
    }


    /**
     * @param int $id school.id
     *
     * @return string
     */
    public function actionIndex($id)
    {
        $this->isAccess($id);
        $school = School::findOne($id);

        return $this->render([
            'school' => $school,
        ]);
    }

    /**
     * @param int $id school.id
     *
     * @return string
     */
    public function actionAdd($id)
    {
        $this->isAccess($id);
        $model = new \avatar\models\forms\CabinetSchoolRoleAdd(['school_id' => $id]);
        $school = School::findOne($id);

        if (Yii::$app->request->isPost) {
            if ($model->load(Yii::$app->request->post()) && $model->validate()) {
                $s = $model->save();

                return self::jsonSuccess($s);
            } else {
                return self::jsonErrorId(102, $model->getErrors102());
            }
        }

        return $this->render([
            'model'  => $model,
            'school' => $school,
        ]);
    }

    /**
     * @param int $id school_blog_article.id
     *
     * @return string
     */
    public function actionEdit($id)
    {
        $model = \avatar\models\forms\CabinetSchoolRoleAdd::findOne($id);
        $this->isAccess($model->school_id);
        $school = School::findOne($model->school_id);

        if (Yii::$app->request->isPost) {
            if ($model->load(Yii::$app->request->post()) && $model->validate()) {
                $s = $model->save();

                return self::jsonSuccess($s);
            } else {
                return self::jsonErrorId(102, $model->getErrors102());
            }
        }

        return $this->render([
            'model' => $model,
            'school' => $school,
        ]);
    }

}
