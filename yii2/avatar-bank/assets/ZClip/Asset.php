<?php
namespace avatar\assets\ZClip;

use yii\web\AssetBundle;


/**
 * Class Asset
 * @package cs\assets\ZClip
 *
 * Пример использования
 * php```
 * $this->registerJs("var pathZClip = '".\Yii::$app->assetManager->getBundle('cs\assets\ZClip\Asset')->baseUrl."';", \yii\web\View::POS_HEAD);
$this->registerJs(<<<JS
$(".buttonCopy1").zclip({
path: pathZClip + '/ZeroClipboard.swf',
copy: $('#textCode1').val(),
beforeCopy: function () {
},
afterCopy: function () {
infoWindow('Скопировано');
}
});
JS
);

 * ```
 *
 *
 * ```php
 * \cs\assets\ZClip\Asset::register($this);
$p = \Yii::$app->assetManager->getBundle('cs\assets\ZClip\Asset')->baseUrl;
$this->registerJs(<<<JS
$(".buttonCopy1").zclip({
path: '{$p}' + '/ZeroClipboard.swf',
copy: $('#textCode1').val(),
beforeCopy: function () {
},
afterCopy: function () {
infoWindow('Скопировано');
}
});
JS
);
 * ```
 *
 */
class Asset extends AssetBundle
{
    public $sourcePath = '@avatar/assets/ZClip/source';
    public $js = [
        'jquery.zclip.min.js'
    ];
    public $css = [
    ];
    public $depends = [
        'yii\web\JqueryAsset',
    ];
}