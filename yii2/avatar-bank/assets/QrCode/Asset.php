<?php
namespace avatar\assets\QrCode;

use yii\web\AssetBundle;


/**

 */
class Asset extends AssetBundle
{
    public $sourcePath = '@vendor/jeromeetienne/jquery-qrcode';
    public $js = [
        'jquery.qrcode.min.js'
    ];
    public $css = [
    ];
    public $depends = [
        'yii\web\JqueryAsset',
    ];
}