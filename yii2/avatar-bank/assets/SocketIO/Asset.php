<?php
namespace avatar\assets\SocketIO;

use yii\web\AssetBundle;


/**

 */
class Asset extends AssetBundle
{

    public static function getHost()
    {
        return YII_ENV_DEV ? 'http://localhost:3000' : 'http://chat.qualitylive.su';
    }

    public function init()
    {
        $serverName = self::getHost();

        \Yii::$app->view->registerJsFile($serverName . '/socket.io/socket.io.js', ['depends' => ['yii\web\JqueryAsset']]);
    }

}