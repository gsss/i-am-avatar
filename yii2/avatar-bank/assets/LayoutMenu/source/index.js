/**
 * depends \app\assets\App\Asset
 *
 * @type {{init: Function, getDate: Function, getDateTime: Function}}
 */

var LayoutMenu = {
    init: function(maya) {
        var d = new Date();
        var day = d.getDate();
        var m = d.getMonth();
        var dayString = LayoutMenu.getDate(d);
        var y = d.getFullYear();
        $('#dateThis').html(dayString);

        // установка popover
        if (typeof maya == 'undefined') {
            maya = GSSS.calendar.maya.driver1.calc([day, m + 1, y]);
            ajaxJson({
                url: '/calendar/save',
                data: {
                    maya: GSSS.jsonEncode(maya),
                    stamp: maya.stamp,
                    date: dayString,
                    datetime: LayoutMenu.getDateTime(d),
                    timeZone: (-d.getTimezoneOffset()) * 60
                },
                success: function(ret) {

                }
            });
            $('#calendarMayaStamp').attr('src', pathMaya + '/images/stamp3/' + maya.stamp + '.gif');
        }

        var stampItem = GSSS.calendar.maya.stampList[maya.stamp - 1];
        var objPopover = $('#calendarMayaDescription');
        objPopover.find('h4').html(stampItem[0] + ' <sup title="Тон"><abbr>' + maya.ton + '</abbr></sup>');
        $(objPopover.find('p')[0]).html(stampItem[3]);
        // ближайший портал
        switch (maya.nearPortal) {
            case 0:
                $(objPopover.find('p')[1]).html('<span class="glyphicon glyphicon-ok text-success" aria-hidden="true" style="padding-right: 5px;"></span>Сегодня <abbr title="День имеет прямую связь с духом и космосом">портал галактической активации</abbr>');
                break;
            case 1:
                $(objPopover.find('span[class="days"]')).html('завтра');
                break;
            default :
                objPopover.find('kbd').html(maya.nearPortal);
                if ($.inArray(maya.nearPortal, [2,3,4]) >= 0) {
                    $(objPopover.find('span[class="days2"]')).html('дня');
                } else {
                    $(objPopover.find('span[class="days2"]')).html('дней');
                }
                break;
        }
        $('#linkCalendar').popover({
            content: $('#calendarMayaDescription').html()
        });
        $('#linkCalendar').on('shown.bs.popover', function () {
            var idPopover = $(this).attr('aria-describedby');
            $('#' + idPopover + ' abbr').tooltip();
            $('#' + idPopover + ' sup').tooltip();
        });

    },

    /**
     * Преоразует DateTime в строку формата 'YYYY-MM-DD HH:MM:SS'
     * @param d DateTime
     * 
     * @return string 'YYYY-MM-DD HH:MM:SS'
     */
    getDate: function(d)
    {
        var m = d.getMonth();
        var mArr = [
            'Янв',
            'Фев',
            'Мар',
            'Апр',
            'Май',
            'Июн',
            'Июл',
            'Авг',
            'Сен',
            'Окт',
            'Ноя',
            'Дек'
        ];

        var mStr = mArr[m];
        var y = d.getFullYear();
        var day = d.getDate();
        
        return day + ' ' + mStr + ' ' + y + ' г.';
    },

    /**
     * Преоразует DateTime в строку формата '1 апр 2012 г.'
     * @param d DateTime
     * 
     * @return string 'YYYY-MM-DD HH:MM:SS'
     */
    getDateTime: function(d)
    {
        var y = d.getFullYear();
        var m = d.getMonth();
        var day = d.getDate();
        var h = d.getHours();
        if (h < 10) h = '0' + h;
        var i = d.getMinutes();
        if (i < 10) i = '0' + i;
        var s = d.getSeconds();
        if (s < 10) s = '0' + s;
        
        return y + '-' + ((m < 10) ? '0' + m : m) + '-' + ((day < 10) ? '0' + day : day) + ' ' + h + ':' + i + ':' + s;
    }
};


$(document).ready(function () {

    // форма авторизации
    {
        $('#modalLogin, .buttonLogin').click(function () {
            $('#loginModal').modal('show');
        });
        $('#buttonLogin').click(function () {
            if ($('#field-email').val() == '') return showError('Введите логин');
            if ($('#field-password').val() == '') return showError('Введите пароль');
            ajaxJson({
                url: '/auth/login-ajax',
                data: {
                    email: $('#field-email').val(),
                    password: $('#field-password').val()
                },
                beforeSend: function () {
                    $('#buttonLogin').html($('#loginFormLoading').html());
                },
                success: function (ret) {
                    if (ret.code == 200) {
                        window.location = '/cabinet/index';
                    } else if (ret.code == 201) {
                        window.location = ret.redirect;
                    }
                },
                errorScript: function (ret) {
                    $('#buttonLogin').html('Войти');
                    $('#loginFormError').html(ret.data).show();
                }
            })
        });
        $('#field-email').on('focus', function () {
            $('#loginFormError').hide();
        });
        $('#field-password').on('focus', function () {
            $('#loginFormError').hide();
        });
        $('#field-password').on('keyup', function (event) {
            if (event.keyCode == 13) {
                $('#buttonLogin').click();
            }
        });
    }

});