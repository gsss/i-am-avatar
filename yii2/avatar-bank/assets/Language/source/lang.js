/**
 * Created by god on 02.09.2017.
 */

$('.buttonLanguageSet').click(function(e){
    function setCookie2 (name, value) {
        var date = new Date();
        var now = new Date();
        var time = now.getTime();
        var expireTime = time + (1000 * 60 * 60 * 24 * 365);
        now.setTime(expireTime);
        document.cookie = name + "=" + value +
            "; expires=" + now.toGMTString() +
            "; path=/";
    }
    setCookie2('language', $(this).data('code'));
    window.location.reload();
});