

function ajaxJson(options) {
    return $.ajax({
        url: options.url,
        type: (typeof options.type != 'undefined') ? options.type : 'post',
        data: options.data,
        dataType: 'json',
        beforeSend: options.beforeSend,
        success: function (ret) {
            var status;
            if (ret.success) {
                options.success(ret.data);
                return;
            } else {
                if (typeof options.errorScript != 'undefined') {
                    options.errorScript(ret.data);
                } else {
                    alert(ret.error);
                }
            }
        },
        error: function (ret) {
            $.ajax({
                url: '/errorAjaxReport',
                type: 'post',
                data: {
                    status: ret.status,
                    url: options.url,
                    type: (typeof options.type != 'undefined') ? options.type : 'post',
                    data: options.data
                },
                success: function(ret1) {

                }
            });
            if ((typeof options.error != 'undefined')) {
                options.error(ret);
            }
        },
        complete: function(xhr, textStatus) {
            if (xhr.status == 500) {
                $.ajax({
                    url: '/errorAjaxReport',
                    type: 'post',
                    data: {
                        status: xhr.status,
                        url: options.url,
                        type: (typeof options.type != 'undefined') ? options.type : 'post',
                        data: options.data
                    },
                    success: function(ret) {

                    }
                });
            }
            if ((typeof options.complete != 'undefined')) {
                options.complete(xhr, textStatus);
            }
        }
    });
}
