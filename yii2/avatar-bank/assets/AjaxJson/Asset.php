<?php


namespace avatar\assets\AjaxJson;

use yii\web\AssetBundle;


/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since  2.0
 */
class Asset extends AssetBundle
{
    public $sourcePath = '@avatar/assets/AjaxJson/source';
    public $css      = [
        'sort-table.css'
    ];
    public $js       = [
        'gsss.js',
    ];
    public $depends  = [
        'yii\web\YiiAsset',
    ];
}
