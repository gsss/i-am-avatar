
var GSSS = {
    calendar: {
        maya: {}
    },

    /**
     * Навешивает обработчик на кнопку с селектором selector который отправляет запрос на создание рассылки на url
     * которая формируется через function(id) {return url;}
     *
     * @param selector   string
     * @param url        function
     * @param urlSuccess function
     *
     */
    subscribe: function(selector, url, urlSuccess) {
        if (typeof(urlSuccess) == 'undefined') {
            var url2 = url(id) + 'Success';
            urlSuccess = function(id) { return url2; };
        }
        $(selector).click(function (e) {
            e.preventDefault();
            e.stopPropagation();
            if (confirm('Подтведите свое намерение')) {
                var buttonSubscribe = $(this);
                var id = $(this).data('id');
                ajaxJson({
                    url: url(id),
                    success: function (ret) {
                        var r = $.ajax({
                            url: 'http://service.galaxysss.com/subscribe/add',
                            dataType: 'json',
                            type: 'post',
                            data: {
                                data: GSSS.jsonEncode(ret)
                            },
                            success: function(ret1) {
                                if (ret1.status) {
                                    ajaxJson({
                                        url: urlSuccess(id),
                                        success: function(ret) {
                                            infoWindow('Успешно', function() {
                                                buttonSubscribe.remove();
                                            });
                                        }
                                    });
                                }
                            },
                            error: function(ret) {
                                alert('Системная ошибка сервиса');
                                console.log(ret);
                            },
                            complete: function(xhr, textStatus) {
                                if (xhr.status != 200) {
                                    alert('Системная ошибка сервиса. Код=' + xhr.status );
                                    console.log([xhr, textStatus]);
                                }
                            }
                        });
                    }
                });
            }
        });
    },
    modal: function(selector, options) {
        if (typeof options == 'undefined') options = {};
        var options2 = $.extend(options, {
            type: 'inline',

            fixedContentPos: false,
            fixedBgPos: true,

            overflowY: 'auto',

            closeBtnInside: true,
            preloader: false,

            midClick: true,
            removalDelay: 300,
            mainClass: 'my-mfp-zoom-in',
            items: {
                src: selector, // can be a HTML string, jQuery object, or CSS selector
                type: 'inline'
            }
        });
        $.magnificPopup.open(options2);
    },


    /**
     * Парсит объект (object) в строку JSON
     *
     * @param object
     *
     * @returns {*}
     */
    jsonEncode: function (object) {
        return JSON.stringify(object);
    },

    /**
     * Парсит строку JSON (string) в объект
     *
     * @param string
     *
     * @returns {*}
     */
    jsonDecode: function (string) {
        return $.parseJSON(string);
    }
};


function ajaxJson(options) {
    return $.ajax({
        url: options.url,
        type: (typeof options.type != 'undefined') ? options.type : 'post',
        data: options.data,
        dataType: 'json',
        beforeSend: options.beforeSend,
        success: function (ret) {
            var status;
            if (ret.success) {
                options.success(ret.data);
                return;
            } else {
                if (typeof options.errorScript != 'undefined') {
                    options.errorScript(ret.data);
                } else {
                    alert(ret.error);
                }
            }
        },
        error: function (ret) {
            $.ajax({
                url: '/errorAjaxReport',
                type: 'post',
                data: {
                    status: ret.status,
                    url: options.url,
                    type: (typeof options.type != 'undefined') ? options.type : 'post',
                    data: options.data
                },
                success: function(ret1) {

                }
            });
            if ((typeof options.error != 'undefined')) {
                options.error(ret);
            }
        },
        complete: function(xhr, textStatus) {
            if (xhr.status == 500) {
                $.ajax({
                    url: '/errorAjaxReport',
                    type: 'post',
                    data: {
                        status: xhr.status,
                        url: options.url,
                        type: (typeof options.type != 'undefined') ? options.type : 'post',
                        data: options.data
                    },
                    success: function(ret) {

                    }
                });
            }
            if ((typeof options.complete != 'undefined')) {
                options.complete(xhr, textStatus);
            }
        }
    });
}

/**
 * Выводит информационное модальное окно
 * Если надо повесить событие на закрытие окна то надо добавить событие так
 *
 * @param text - string -  выводимое сообщение
 * @param closeFunction - function - функция выполняемая по закрытию окна
 */
function infoWindow(text, closeFunction) {

    $('#infoModal').html('').append($('<p>').html(text));
    $('#infoModal').magnificPopup({

        fixedContentPos: false,
        fixedBgPos: true,

        overflowY: 'auto',

        closeBtnInside: false,
        preloader: false,

        midClick: true,
        removalDelay: 300,
        mainClass: 'my-mfp-zoom-in',

        items: {
            src: '#infoModal', // CSS selector of an element on page that should be used as a popup
            type: 'inline'
        },
        callbacks:{
            afterClose: closeFunction
        }
    }).magnificPopup('open');

}


$(document).ready(function() {
    $("[data-toggle='tooltip']").tooltip();
});