<?php
/**
 * @link      http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license   http://www.yiiframework.com/license/
 */

namespace avatar\assets;

use yii\web\AssetBundle;

/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since  2.0
 */
class JsonEditor extends AssetBundle
{
    public $css = [
    ];
    public $js = [
        'https://cdn.jsdelivr.net/npm/@json-editor/json-editor/dist/jsoneditor.min.js'
    ];
    public $depends = [
        '\yii\web\JqueryAsset',
    ];
}
