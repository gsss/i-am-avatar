<?php
namespace avatar\assets\Timeline;

use yii\web\AssetBundle;


/**

 */
class Asset extends AssetBundle
{
    public $sourcePath = '@avatar/assets/Timeline/source';
    public $js = [
    ];
    public $css = [
        'default.css'
    ];
    public $depends = [
    ];
}