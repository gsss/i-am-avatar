<?php
/**
 * @link      http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license   http://www.yiiframework.com/license/
 */

namespace avatar\assets;

use yii\web\AssetBundle;

/**
 * https://ned.im/noty/#/
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since  2.0
 */
class Notify extends AssetBundle
{
    public $sourcePath = '@vendor/needim/noty';
    public $css = [
        'lib/noty.css'
    ];
    public $js = [
        'lib/noty.js'
    ];
    public $depends = [
        '\yii\web\JqueryAsset',
    ];
}
