<?php
/**
 * @link      http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license   http://www.yiiframework.com/license/
 */

namespace avatar\assets;

use yii\web\AssetBundle;

/**
 */
class Paralax extends AssetBundle
{
    public $css = [
    ];

    public $js = [
        'https://platform.qualitylive.su/js/parallax.js-1.5.0/parallax.js'
    ];

    public $depends = [
        '\yii\web\JqueryAsset',
    ];
}
