<?php
/**
 * Created by PhpStorm.
 * User: s.arhangelskiy
 * Date: 12.04.2016
 * Time: 16:13
 */

namespace avatar\modules\chat\models;


use yii\db\ActiveRecord;
use yii\db\Query;
use yii\helpers\ArrayHelper;
use yii\helpers\VarDumper;

/**
 *
 * @property int    id
 * @property int    type_id
 * @property int    last_message
 */
class Room extends ActiveRecord
{
    const TYPE_ID_TET_A_TET = 1;
    const TYPE_ID_GROUP = 2;

    public static function tableName()
    {
        return 'chat_room';
    }

    public static function add($fields)
    {
        $i = new static($fields);
        $i->save();
        $i->id = self::getDb()->lastInsertID;

        return $i;
    }
}