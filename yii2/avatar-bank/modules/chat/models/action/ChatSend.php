<?php
/**
 * Created by PhpStorm.
 * User: s.arhangelskiy
 * Date: 31.07.2017
 * Time: 10:50
 */

namespace avatar\modules\chat\models\action;


use common\models\avatar\Currency;
use common\models\avatar\UserBill;
use common\models\BlogItem;
use common\models\chat\Message;
use common\models\chat\Room;
use common\models\school\SubscribeItem;
use common\models\Token;
use common\models\UserRoot;
use common\models\VebinarChatItem;
use cs\services\VarDumper;
use cs\web\Exception;
use yii\helpers\Html;
use yii\helpers\Json;
use yii\helpers\StringHelper;

class ChatSend extends \yii\base\Model
{
    public $room_id;
    public $text;
    public $user_id;
    public $files;

    public function rules()
    {
        return [
            ['room_id', 'required'],
            ['room_id', 'integer'],

            ['text', 'required'],
            ['text', 'string'],

            ['user_id', 'required'],
            ['user_id', 'string'],

            ['files', 'string'],
            ['files', 'normalizeFiles'],
        ];
    }

    public function normalizeFiles($a,$p)
    {
        try {
            $f = Json::decode($this->files);
        } catch (\Exception $e) {
            $f = [];
        }
        if (!is_array($f)) {
            try {
                $f = Json::decode($f);
            } catch (\Exception $e) {
                $f = [];
            }
        }

        $this->files = $f;
    }

    public function action()
    {
        $data = ['text' => $this->text];
        if ($this->files) {
            $data['files'] = $this->files;
        }
        $text = Json::encode($data);
        $n = \avatar\modules\chat\models\Message::add([
            'room_id' => $this->room_id,
            'text'    => $text,
            'user_id' => $this->user_id,
        ]);

        $htmlWithFiles = $this->htmlWithFiles($n);

        // Добавляю время последнего сообщения в комнату
        $room = \avatar\modules\chat\models\Room::findOne($this->room_id);
        $room->last_message = time();
        $room->save();

        $f = $n->attributes;
        $f['timeFormatted'] = \Yii::$app->formatter->asDatetime($n->created_at, 'php:H:i:s');

        return [
            'message' => $f,
            'html'    => $htmlWithFiles
        ];
    }

    /**
     * @param string $text
     * @return string
     */
    public function html($text)
    {
        $rows = explode("\n", $text);
        $rows2 = [];
        foreach ($rows as $row) {
            $arr = explode(' ', $row);
            $arr2 = [];
            foreach ($arr as $word) {
                $w = trim($word);
                if (StringHelper::startsWith($w, 'https://') or StringHelper::startsWith($w, 'http://') or StringHelper::startsWith($w, 'blob:http')) {
                    $w = Html::a($w, $w, ['target' => '_blank']);
                    $arr2[] = $w;
                } else {
                    $arr2[] = Html::encode($w);
                }
            }
            $rows2[] = join(' ', $arr2);
        }

        $textHtml = join('<br>', $rows2);

        return $textHtml;
    }

    /**
     * @param \avatar\modules\chat\models\Message $m
     * @return string
     */
    public function htmlWithFiles($m)
    {
        return \Yii::$app->view->render('@avatar/modules/chat/views/site/item-message2.php', ['message' => $m]);
    }
}