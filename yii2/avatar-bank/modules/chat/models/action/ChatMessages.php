<?php
/**
 * Created by PhpStorm.
 * User: s.arhangelskiy
 * Date: 31.07.2017
 * Time: 10:50
 */

namespace avatar\modules\chat\models\action;


use common\models\avatar\Currency;
use common\models\avatar\UserBill;
use common\models\BlogItem;
use common\models\chat\Group;
use common\models\chat\GroupUser;
use common\models\chat\ListUser;
use common\models\chat\Message;
use common\models\chat\Room;
use common\models\chat\Tet;
use common\models\school\SubscribeItem;
use common\models\Token;
use common\models\UserAvatar;
use common\models\UserRoot;
use common\models\VebinarChatItem;
use cs\services\VarDumper;
use yii\helpers\Html;
use yii\helpers\Json;
use yii\helpers\StringHelper;

class ChatMessages extends \yii\base\Model
{
    /** @var int комната */
    public $id;

    /** @var \avatar\modules\chat\models\Room */
    public $Room;

    public function rules()
    {
        return [
            ['id', 'required'],
            ['id', 'integer'],
            ['id', 'validateAccess'],
        ];
    }

    public function validateAccess($a, $p)
    {
        if (!$this->hasErrors()) {
            $Room = \avatar\modules\chat\models\Room::findOne($this->id);
            if (is_null($Room)) {
                $this->addError($a, 'Нет накого чата');
                return;
            }
            $this->Room = $Room;

            if (!\avatar\modules\chat\models\ListUser::find()->where(['room_id' => $this->id, 'user_id' => \Yii::$app->user->id])->exists()) {
                $this->addError($a, 'У вас нет доступа к чату');
                return;
            }
        }
    }

    public function action()
    {
        // ищу сообщения
        $messages = \avatar\modules\chat\models\Message::find()
            ->orderBy(['id' => SORT_DESC])
            ->where(['room_id' => $this->id])
            ->limit(20)
            ->all();
        $messages = array_reverse($messages);
        $html = '';
        foreach ($messages as $m) {
            $html .= \Yii::$app->view->renderFile('@avatar/modules/chat/views/site/item-message2.php', ['message' => $m]);
        }

        // ищу описание комнаты $room_type
        if ($this->Room->type_id == \avatar\modules\chat\models\Room::TYPE_ID_GROUP) {
            $room_type = \avatar\modules\chat\models\Group::findOne(['room_id' => $this->id]);
        } else {
            $tet = \avatar\modules\chat\models\Tet::findOne(['room_id' => $this->id]);
            if (\Yii::$app->user->id == $tet->user1_id) {
                $user_id = $tet->user2_id;
            } else {
                $user_id = $tet->user1_id;
            }
            $u = UserAvatar::findOne($user_id);

            $room_type = [
                'tet'  => $tet,
                'user' => [
                    'id'        => $u->id,
                    'name2'     => $u->getName2(),
                    'avatar'    => $u->getAvatar(),
                ],
            ];
        }

        return [
            'room'      => $this->Room,
            'room_type' => $room_type,
            'messages'  => $messages,
            'html'      => $html,
        ];
    }

}