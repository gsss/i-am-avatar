<?php

/** @var \yii\web\View $this */

use yii\helpers\ArrayHelper;
use yii\helpers\Html;

/** @var int $id идентификатор вебинара */
$id = 1;


$this->title = 'Чат';

\common\assets\SocketIO\Asset::register($this);
\avatar\assets\Notify::register($this);


/** @var \common\models\UserAvatar $user */
$user = Yii::$app->user->identity;
$user_id = Yii::$app->user->id;
$user_name = $user->getName2();
$user_avatar = $user->getAvatar();

// Список комнат к которым я подключаюсь
//$roomsIds = \yii\helpers\ArrayHelper::getColumn($rooms, 'id');
//$roomsIdsJson = \yii\helpers\Json::encode($roomsIds);
$server = \avatar\assets\SocketIO\Asset::getHost();
$roomMain = \avatar\modules\chat\models\Room::findOne($id);

$tet_a_tet = ($roomMain->type_id == \avatar\modules\chat\models\Room::TYPE_ID_TET_A_TET)? 1 : 0;

$roomsQ = \avatar\modules\chat\models\ListUser::find()
    ->innerJoin('chat_room', 'chat_room.id = chat_list.room_id')
    ->select([
        'chat_room.*'
    ])
    ->where(['chat_list.user_id' => Yii::$app->user->id])
    ->orderBy(['chat_room.last_message' => SORT_DESC])
    ->asArray();

$rooms = $roomsQ->all();

$this->registerJs(<<<JS
$('.js-personChat').click(function(e) {
    window.location = '/chat/item?id=' + $(this).data('id');
})
JS
);


$host = \avatar\assets\SocketIO\Asset::getHost();

?>
<div class="main-container">


    <!-- Page header start -->
    <div class="page-header">
        <ol class="breadcrumb">
            <li class="breadcrumb-item">ДОМ</li>
            <li class="breadcrumb-item active">Чат</li>
        </ol>
    </div>
    <!-- Page header end -->


    <!-- Content wrapper start -->
    <div class="content-wrapper">
        <?= $this->render('1') ?>
    </div>
</div>

