<?php
/** @var \avatar\modules\chat\models\Message $message */

?>
<?php $user = \common\models\UserAvatar::findOne($message->user_id); ?>


<li class="left clearfix" data-id="<?= $message->id ?>">
    <span class="chat-img pull-left">
        <img width="30" src="<?= $user->getAvatar() ?>" alt="" class="img-circle"
        data-toggle="tooltip"
             title="<?= $user->getName2() ?>"
        />
    </span>
    <div class="chat-body clearfix">
        <div class="header2">
            <small class="pull-right text-muted">
                <span class="glyphicon glyphicon-time"
                      data-toggle="tooltip"
                      title="<?= Yii::$app->formatter->asDatetime($message->created_at, 'php:d.m.Y H:i:s') ?>"
                ></span>

            </small>
        </div>
        <p>
            <?= $this->render('item-message', ['message' => $message]); ?>
        </p>
    </div>
</li>
