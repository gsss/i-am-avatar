<?php
?>

<div id="chat">


    <style>
        .chat
        {
            list-style: none;
            margin: 0;
            padding: 0;
        }

        .chat li
        {
            margin-bottom: 10px;
            padding-bottom: 5px;
            border-bottom: 1px dotted #B3A9A9;
        }

        .chat li.left .chat-body
        {
            margin-left: 60px;
        }

        .chat li.right .chat-body
        {
            margin-right: 60px;
        }


        .chat li .chat-body p
        {
            margin: 0;
            color: #777777;
        }

        .panel .slidedown .glyphicon, .chat .glyphicon
        {
            margin-right: 5px;
        }

        .panel-body
        {
            overflow-y: scroll;
            height: 600px;
        }

        ::-webkit-scrollbar-track
        {
            -webkit-box-shadow: inset 0 0 6px rgba(0,0,0,0.3);
            background-color: #F5F5F5;
        }

        ::-webkit-scrollbar
        {
            width: 30px;
            background-color: #F5F5F5;
        }

        ::-webkit-scrollbar-thumb
        {
            -webkit-box-shadow: inset 0 0 6px rgba(0,0,0,.3);
            background-color: #555;
        }

        .panel-primary > .panel-heading {
            background-image: linear-gradient(to bottom, #750f0b 0%, #750f0b 100%) !important;
        }

        .panel-primary {
            border-color: #750f0b !important;
        }

        .btn-warning {
            background-image: linear-gradient(to bottom, #5cb85c 0%, #5cb85c 100%);
        }

        .btn-warning:hover, .btn-warning:focus {
            background-color: #5cb85c !important;
            background-position: 0 -15px;
        }

        .btn-warning:hover {
            color: #fff;
            background-color: #5cb85c !important;
            border-color: #5cb85c !important;
        }

    </style>


    <div class="panel panel-primary">
        <div class="panel-heading">
            <span class="glyphicon glyphicon-comment"></span> Чат
        </div>
        <div class="panel-body">

            <ul class="chat">
                <li class="left clearfix" data-id="292">
    <span class="chat-img pull-left">
        <img width="50" src="/images/cabinet/index/photo_2020-01-28_15-58-38.jpg" alt="MichMan28 Михаил" class="img-circle" />
    </span>
                    <div class="chat-body clearfix">
                        <div class="header2">
                            <strong class="primary-font">MichMan28 Михаил</strong>
                            <small class="pull-right text-muted">
                                <span class="glyphicon glyphicon-time"></span>08:34:22            </small>
                        </div>
                        <p>
                            Приветствую! Ну что, попробуем сервис?))<br />
                        </p>

                    </div>
                </li>
                <li class="left clearfix" data-id="293">
    <span class="chat-img pull-left">
        <img width="50" src="https://topmate.one/upload/cloud/16194/16004_5ptK0PA8sd_crop.png" alt="01.TM.Manager_(UTC+3)" class="img-circle" />
    </span>
                    <div class="chat-body clearfix">
                        <div class="header2">
                            <strong class="primary-font">01.TM.Manager_(UTC+3)</strong>
                            <small class="pull-right text-muted">
                                <span class="glyphicon glyphicon-time"></span>08:36:30            </small>
                        </div>
                        <p>
                            Здравствуйте! Конечно попробуем.            </p>

                    </div>
                </li>
                <li class="left clearfix" data-id="294">
    <span class="chat-img pull-left">
        <img width="50" src="/images/cabinet/index/photo_2020-01-28_15-58-38.jpg" alt="MichMan28 Михаил" class="img-circle" />
    </span>
                    <div class="chat-body clearfix">
                        <div class="header2">
                            <strong class="primary-font">MichMan28 Михаил</strong>
                            <small class="pull-right text-muted">
                                <span class="glyphicon glyphicon-time"></span>08:37:15            </small>
                        </div>
                        <p>
                            Отлично, мои действия далее?<br />
                        </p>

                    </div>
                </li>
                <li class="left clearfix" data-id="295">
    <span class="chat-img pull-left">
        <img width="50" src="https://topmate.one/upload/cloud/16194/16004_5ptK0PA8sd_crop.png" alt="01.TM.Manager_(UTC+3)" class="img-circle" />
    </span>
                    <div class="chat-body clearfix">
                        <div class="header2">
                            <strong class="primary-font">01.TM.Manager_(UTC+3)</strong>
                            <small class="pull-right text-muted">
                                <span class="glyphicon glyphicon-time"></span>08:38:36            </small>
                        </div>
                        <p>
                            несколько минут, определимся с заказом            </p>

                    </div>
                </li>
                <li class="left clearfix" data-id="296">
    <span class="chat-img pull-left">
        <img width="50" src="https://topmate.one/upload/cloud/16194/16004_5ptK0PA8sd_crop.png" alt="01.TM.Manager_(UTC+3)" class="img-circle" />
    </span>
                    <div class="chat-body clearfix">
                        <div class="header2">
                            <strong class="primary-font">01.TM.Manager_(UTC+3)</strong>
                            <small class="pull-right text-muted">
                                <span class="glyphicon glyphicon-time"></span>08:44:00            </small>
                        </div>
                        <p>
                            У меня цена 6,999р а не 6.799р. Вы по какому региону заказываете? Или может вы из своего ЛК в ДНС заказываете?            </p>

                    </div>
                </li>
                <li class="left clearfix" data-id="297">
    <span class="chat-img pull-left">
        <img width="50" src="/images/cabinet/index/photo_2020-01-28_15-58-38.jpg" alt="MichMan28 Михаил" class="img-circle" />
    </span>
                    <div class="chat-body clearfix">
                        <div class="header2">
                            <strong class="primary-font">MichMan28 Михаил</strong>
                            <small class="pull-right text-muted">
                                <span class="glyphicon glyphicon-time"></span>08:45:29            </small>
                        </div>
                        <p>
                            Ангарск, Иркутская обл. В Магазине в наличии.            </p>

                    </div>
                </li>
                <li class="left clearfix" data-id="298">
    <span class="chat-img pull-left">
        <img width="50" src="https://topmate.one/upload/cloud/16194/16004_5ptK0PA8sd_crop.png" alt="01.TM.Manager_(UTC+3)" class="img-circle" />
    </span>
                    <div class="chat-body clearfix">
                        <div class="header2">
                            <strong class="primary-font">01.TM.Manager_(UTC+3)</strong>
                            <small class="pull-right text-muted">
                                <span class="glyphicon glyphicon-time"></span>08:46:39            </small>
                        </div>
                        <p>
                            да, все верно            </p>

                    </div>
                </li>
                <li class="left clearfix" data-id="299">
    <span class="chat-img pull-left">
        <img width="50" src="/images/cabinet/index/photo_2020-01-28_15-58-38.jpg" alt="MichMan28 Михаил" class="img-circle" />
    </span>
                    <div class="chat-body clearfix">
                        <div class="header2">
                            <strong class="primary-font">MichMan28 Михаил</strong>
                            <small class="pull-right text-muted">
                                <span class="glyphicon glyphicon-time"></span>08:47:55            </small>
                        </div>
                        <p>
                            <a href="https://topmate.one/upload/cloud/16215/76048_ZKQxVg9ns5.png">https://topmate.one/upload/cloud/16215/76048_ZKQxVg9ns5.png</a>            </p>

                    </div>
                </li>
                <li class="left clearfix" data-id="300">
    <span class="chat-img pull-left">
        <img width="50" src="https://topmate.one/upload/cloud/16194/16004_5ptK0PA8sd_crop.png" alt="01.TM.Manager_(UTC+3)" class="img-circle" />
    </span>
                    <div class="chat-body clearfix">
                        <div class="header2">
                            <strong class="primary-font">01.TM.Manager_(UTC+3)</strong>
                            <small class="pull-right text-muted">
                                <span class="glyphicon glyphicon-time"></span>08:51:02            </small>
                        </div>
                        <p>
                            так, теперь несколько вопросов:<br />
                            1. вы заберете сами в ДНС?<br />
                            2. если заказываете доставку на конкретный адрес, нужен сам адрес, конт.телефон и имя (у вас профиль не заполнен)            </p>

                    </div>
                </li>
                <li class="left clearfix" data-id="301">
    <span class="chat-img pull-left">
        <img width="50" src="/images/cabinet/index/photo_2020-01-28_15-58-38.jpg" alt="MichMan28 Михаил" class="img-circle" />
    </span>
                    <div class="chat-body clearfix">
                        <div class="header2">
                            <strong class="primary-font">MichMan28 Михаил</strong>
                            <small class="pull-right text-muted">
                                <span class="glyphicon glyphicon-time"></span>08:51:31            </small>
                        </div>
                        <p>
                            Заберу сам.<br />
                        </p>

                    </div>
                </li>
                <li class="left clearfix" data-id="302">
    <span class="chat-img pull-left">
        <img width="50" src="https://topmate.one/upload/cloud/16194/16004_5ptK0PA8sd_crop.png" alt="01.TM.Manager_(UTC+3)" class="img-circle" />
    </span>
                    <div class="chat-body clearfix">
                        <div class="header2">
                            <strong class="primary-font">01.TM.Manager_(UTC+3)</strong>
                            <small class="pull-right text-muted">
                                <span class="glyphicon glyphicon-time"></span>08:52:03            </small>
                        </div>
                        <p>
                            отлично, оформляем тогда            </p>

                    </div>
                </li>
                <li class="left clearfix" data-id="303">
    <span class="chat-img pull-left">
        <img width="50" src="/images/cabinet/index/photo_2020-01-28_15-58-38.jpg" alt="MichMan28 Михаил" class="img-circle" />
    </span>
                    <div class="chat-body clearfix">
                        <div class="header2">
                            <strong class="primary-font">MichMan28 Михаил</strong>
                            <small class="pull-right text-muted">
                                <span class="glyphicon glyphicon-time"></span>08:52:16            </small>
                        </div>
                        <p>
                            Конечно!)<br />
                        </p>

                    </div>
                </li>
                <li class="left clearfix" data-id="304">
    <span class="chat-img pull-left">
        <img width="50" src="/images/cabinet/index/photo_2020-01-28_15-58-38.jpg" alt="MichMan28 Михаил" class="img-circle" />
    </span>
                    <div class="chat-body clearfix">
                        <div class="header2">
                            <strong class="primary-font">MichMan28 Михаил</strong>
                            <small class="pull-right text-muted">
                                <span class="glyphicon glyphicon-time"></span>08:53:23            </small>
                        </div>
                        <p>
                            Я жму на сайте "Оформить заказ"?<br />
                        </p>

                    </div>
                </li>
                <li class="left clearfix" data-id="305">
    <span class="chat-img pull-left">
        <img width="50" src="https://topmate.one/upload/cloud/16194/16004_5ptK0PA8sd_crop.png" alt="01.TM.Manager_(UTC+3)" class="img-circle" />
    </span>
                    <div class="chat-body clearfix">
                        <div class="header2">
                            <strong class="primary-font">01.TM.Manager_(UTC+3)</strong>
                            <small class="pull-right text-muted">
                                <span class="glyphicon glyphicon-time"></span>08:56:06            </small>
                        </div>
                        <p>
                            Здесь 2 варианта на выбор.<br />
                            1. Можете сами оформить на себя, например если у вас есть ЛК в ДНС<br />
                            2. Можем мы оформить через себя. Неудобство в том, что в ДНС телефон контактный менять нельзя            </p>

                    </div>
                </li>
                <li class="left clearfix" data-id="306">
    <span class="chat-img pull-left">
        <img width="50" src="/images/cabinet/index/photo_2020-01-28_15-58-38.jpg" alt="MichMan28 Михаил" class="img-circle" />
    </span>
                    <div class="chat-body clearfix">
                        <div class="header2">
                            <strong class="primary-font">MichMan28 Михаил</strong>
                            <small class="pull-right text-muted">
                                <span class="glyphicon glyphicon-time"></span>08:57:11            </small>
                        </div>
                        <p>
                            Давайте я сам оформлю на себя, а оплата с вашей карты. Вроде так в видео было показано.<br />
                        </p>

                    </div>
                </li>
                <li class="left clearfix" data-id="307">
    <span class="chat-img pull-left">
        <img width="50" src="/images/cabinet/index/photo_2020-01-28_15-58-38.jpg" alt="MichMan28 Михаил" class="img-circle" />
    </span>
                    <div class="chat-body clearfix">
                        <div class="header2">
                            <strong class="primary-font">MichMan28 Михаил</strong>
                            <small class="pull-right text-muted">
                                <span class="glyphicon glyphicon-time"></span>08:57:56            </small>
                        </div>
                        <p>
                            ЛК есть конечно.)            </p>

                    </div>
                </li>
                <li class="left clearfix" data-id="308">
    <span class="chat-img pull-left">
        <img width="50" src="https://topmate.one/upload/cloud/16194/16004_5ptK0PA8sd_crop.png" alt="01.TM.Manager_(UTC+3)" class="img-circle" />
    </span>
                    <div class="chat-body clearfix">
                        <div class="header2">
                            <strong class="primary-font">01.TM.Manager_(UTC+3)</strong>
                            <small class="pull-right text-muted">
                                <span class="glyphicon glyphicon-time"></span>08:58:06            </small>
                        </div>
                        <p>
                            Для вас надежней конечно на себя. Оформляйте, когда будете готовы, напишите, я дам вам реквизиты карты            </p>

                    </div>
                </li>
                <li class="left clearfix" data-id="309">
    <span class="chat-img pull-left">
        <img width="50" src="/images/cabinet/index/photo_2020-01-28_15-58-38.jpg" alt="MichMan28 Михаил" class="img-circle" />
    </span>
                    <div class="chat-body clearfix">
                        <div class="header2">
                            <strong class="primary-font">MichMan28 Михаил</strong>
                            <small class="pull-right text-muted">
                                <span class="glyphicon glyphicon-time"></span>08:58:20            </small>
                        </div>
                        <p>
                            Ок!            </p>

                    </div>
                </li>
                <li class="left clearfix" data-id="310">
    <span class="chat-img pull-left">
        <img width="50" src="/images/cabinet/index/photo_2020-01-28_15-58-38.jpg" alt="MichMan28 Михаил" class="img-circle" />
    </span>
                    <div class="chat-body clearfix">
                        <div class="header2">
                            <strong class="primary-font">MichMan28 Михаил</strong>
                            <small class="pull-right text-muted">
                                <span class="glyphicon glyphicon-time"></span>08:59:35            </small>
                        </div>
                        <p>
                            Я готов!            </p>
                        <p>
                            <a href="https://topmate.one/upload/cloud/16215/76774_BVjCFlDhxW.png">https://topmate.one/upload/cloud/16215/76774_BVjCFlDhxW.png</a>            </p>

                    </div>
                </li>
                <li class="left clearfix" data-id="311">
    <span class="chat-img pull-left">
        <img width="50" src="https://topmate.one/upload/cloud/16194/16004_5ptK0PA8sd_crop.png" alt="01.TM.Manager_(UTC+3)" class="img-circle" />
    </span>
                    <div class="chat-body clearfix">
                        <div class="header2">
                            <strong class="primary-font">01.TM.Manager_(UTC+3)</strong>
                            <small class="pull-right text-muted">
                                <span class="glyphicon glyphicon-time"></span>09:03:25            </small>
                        </div>
                        <p>
                            Карта Тиньков:  5534200036849297 * valid: 04/29 * cvv: 056            </p>

                    </div>
                </li>
                <li class="left clearfix" data-id="312">
    <span class="chat-img pull-left">
        <img width="50" src="https://topmate.one/upload/cloud/16194/16004_5ptK0PA8sd_crop.png" alt="01.TM.Manager_(UTC+3)" class="img-circle" />
    </span>
                    <div class="chat-body clearfix">
                        <div class="header2">
                            <strong class="primary-font">01.TM.Manager_(UTC+3)</strong>
                            <small class="pull-right text-muted">
                                <span class="glyphicon glyphicon-time"></span>09:04:49            </small>
                        </div>
                        <p>
                            код 6661            </p>

                    </div>
                </li>
                <li class="left clearfix" data-id="313">
    <span class="chat-img pull-left">
        <img width="50" src="/images/cabinet/index/photo_2020-01-28_15-58-38.jpg" alt="MichMan28 Михаил" class="img-circle" />
    </span>
                    <div class="chat-body clearfix">
                        <div class="header2">
                            <strong class="primary-font">MichMan28 Михаил</strong>
                            <small class="pull-right text-muted">
                                <span class="glyphicon glyphicon-time"></span>09:04:59            </small>
                        </div>
                        <p>
                            Код с СМС            </p>

                    </div>
                </li>
                <li class="left clearfix" data-id="314">
    <span class="chat-img pull-left">
        <img width="50" src="/images/cabinet/index/photo_2020-01-28_15-58-38.jpg" alt="MichMan28 Михаил" class="img-circle" />
    </span>
                    <div class="chat-body clearfix">
                        <div class="header2">
                            <strong class="primary-font">MichMan28 Михаил</strong>
                            <small class="pull-right text-muted">
                                <span class="glyphicon glyphicon-time"></span>09:05:05            </small>
                        </div>
                        <p>
                            ок            </p>

                    </div>
                </li>
                <li class="left clearfix" data-id="315">
    <span class="chat-img pull-left">
        <img width="50" src="https://topmate.one/upload/cloud/16194/16004_5ptK0PA8sd_crop.png" alt="01.TM.Manager_(UTC+3)" class="img-circle" />
    </span>
                    <div class="chat-body clearfix">
                        <div class="header2">
                            <strong class="primary-font">01.TM.Manager_(UTC+3)</strong>
                            <small class="pull-right text-muted">
                                <span class="glyphicon glyphicon-time"></span>09:05:55            </small>
                        </div>
                        <p>
                            Поздравляю с покупкой! Удачи!            </p>

                    </div>
                </li>
                <li class="left clearfix" data-id="316">
    <span class="chat-img pull-left">
        <img width="50" src="/images/cabinet/index/photo_2020-01-28_15-58-38.jpg" alt="MichMan28 Михаил" class="img-circle" />
    </span>
                    <div class="chat-body clearfix">
                        <div class="header2">
                            <strong class="primary-font">MichMan28 Михаил</strong>
                            <small class="pull-right text-muted">
                                <span class="glyphicon glyphicon-time"></span>09:06:20            </small>
                        </div>
                        <p>
                            Ну вроде всё!            </p>
                        <p>
                            <a href="https://topmate.one/upload/cloud/16215/77177_YZ0CgNvnfr.png">https://topmate.one/upload/cloud/16215/77177_YZ0CgNvnfr.png</a>            </p>

                    </div>
                </li>
                <li class="left clearfix" data-id="317">
    <span class="chat-img pull-left">
        <img width="50" src="/images/cabinet/index/photo_2020-01-28_15-58-38.jpg" alt="MichMan28 Михаил" class="img-circle" />
    </span>
                    <div class="chat-body clearfix">
                        <div class="header2">
                            <strong class="primary-font">MichMan28 Михаил</strong>
                            <small class="pull-right text-muted">
                                <span class="glyphicon glyphicon-time"></span>09:07:00            </small>
                        </div>
                        <p>
                            Спасибо! Сервис на высоте! Удачи вам!!!)            </p>

                    </div>
                </li>
                <li class="left clearfix" data-id="318">
    <span class="chat-img pull-left">
        <img width="50" src="https://topmate.one/upload/cloud/16194/16004_5ptK0PA8sd_crop.png" alt="01.TM.Manager_(UTC+3)" class="img-circle" />
    </span>
                    <div class="chat-body clearfix">
                        <div class="header2">
                            <strong class="primary-font">01.TM.Manager_(UTC+3)</strong>
                            <small class="pull-right text-muted">
                                <span class="glyphicon glyphicon-time"></span>09:07:51            </small>
                        </div>
                        <p>
                            Вы один из первых покупателей! Расскажите знакомым о сервисе, мы будем признательны!            </p>

                    </div>
                </li>
                <li class="left clearfix" data-id="319">
    <span class="chat-img pull-left">
        <img width="50" src="/images/cabinet/index/photo_2020-01-28_15-58-38.jpg" alt="MichMan28 Михаил" class="img-circle" />
    </span>
                    <div class="chat-body clearfix">
                        <div class="header2">
                            <strong class="primary-font">MichMan28 Михаил</strong>
                            <small class="pull-right text-muted">
                                <span class="glyphicon glyphicon-time"></span>09:08:37            </small>
                        </div>
                        <p>
                            Обязательно! Сегодня заберу, завтра все узнают!)))            </p>

                    </div>
                </li>
                <li class="left clearfix" data-id="320">
    <span class="chat-img pull-left">
        <img width="50" src="https://topmate.one/upload/cloud/16194/16004_5ptK0PA8sd_crop.png" alt="01.TM.Manager_(UTC+3)" class="img-circle" />
    </span>
                    <div class="chat-body clearfix">
                        <div class="header2">
                            <strong class="primary-font">01.TM.Manager_(UTC+3)</strong>
                            <small class="pull-right text-muted">
                                <span class="glyphicon glyphicon-time"></span>09:10:41            </small>
                        </div>
                        <p>
                            Отлично! Вам спасибо за PR.            </p>

                    </div>
                </li>
            </ul>
        </div>


        <div class="panel-footer js-chat-panel-footer">
            <div class="input-group">
                <textarea id="btn-input" class="form-control input-sm" placeholder="Напишите свое сообщение здесь..." rows="3"></textarea>
                <span class="input-group-btn">
            <button class="btn btn-warning btn-sm" id="btn-chat" style="height: 66px;">
                Отправить            </button>
        </span>
            </div>
            <p>Если необходимо:</p>
            <div style="margin-top: 10px;">
                <div id="upload1" class="content-box"><input type="button" class="btn btn-primary btn-large clearfix upload-btn buttonDragAndDrop" value="Прикрепите файл"><span style="padding-left:5px;vertical-align:middle;"><i>jpg,jpeg,png (5000 Кб макс)</i></span><div class="clearfix redtext errormsg" style="padding-top: 10px;"></div><input type="hidden" class="inputValue" name="ChatFile[upload1]"><div class="progress-wrap pic-progress-wrap" style="margin-top:10px;margin-bottom:10px;"></div><div class="clear picbox" data-id="picture" style="padding-top:0px;padding-bottom:10px;width:100%;max-width:200px"></div><div class="clear-line" style="margin-top:10px;"></div></div>    </div>
        </div>
    </div>
</div>
