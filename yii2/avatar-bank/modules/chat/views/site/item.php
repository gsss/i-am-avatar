<?php
use yii\helpers\Html;
use yii\helpers\Url;

/** @var $this yii\web\View */
/** @var $id int room.id */

$this->title = 'Чат';

//\chat\assets\Gallaty::register($this);


$roomMain = \avatar\modules\chat\models\Room::findOne($id);

$rooms = \avatar\modules\chat\models\ListUser::find()
    ->innerJoin('chat_room', 'chat_room.id = chat_list.room_id')
    ->select([
        'chat_room.*'
    ])
    ->where(['chat_list.user_id' => Yii::$app->user->id])
    ->orderBy(['chat_room.last_message' => SORT_DESC])
    ->asArray()
    ->all()
;

$this->render('item-chat', ['id' => $id, 'roomMain' => $roomMain, 'rooms' => $rooms]);

$this->registerJs(<<<JS
// Какая комната включена
var chatThis = {$id};

$('.js-personChat').click(function(e) {
    ajaxJson({
        url: '/chat/messages',
        data: {id: $(this).data('id')},
        success: function(ret) {
            
            // Переключить выделение в левом списке
            $('.js-personChat[data-id=' + chatThis + ']').css('background-color', '#fff');
            $('.js-personChat[data-id=' + ret.room.id + ']').css('background-color', '#ddd');

            // Установить заголовок
            chatThis = ret.room.id;
            
            
            
            // Установить заголовок
            var img = $('.active-user-info').find('img');
            var h5 = $('.active-user-info').find('h5');
            if (ret.room.type_id == 1) {
                img.attr('src', ret.room_type.user.avatar);
                h5.html(ret.room_type.user.name2);
            } else {
                img.attr('src', ret.room_type.image);
                h5.html(ret.room_type.name);
            }

            // Добавить сообщения
            $('.chat-box').html(ret.html);
            
            // Перемотать вниз
            chatScroll();
        }
    });
})
JS
);

$messages = \common\models\chat\Message::find()
    ->orderBy(['id' => SORT_ASC])
    ->where(['room_id' => $id])
    ->all();

?>


<div class="main-container">


    <!-- Page header start -->
    <div class="page-header">
        <ol class="breadcrumb">
            <li class="breadcrumb-item">ДОМ</li>
            <li class="breadcrumb-item active">Чат</li>
        </ol>
    </div>
    <!-- Page header end -->


    <!-- Content wrapper start -->
    <div class="content-wrapper">


        <!-- Row start -->
        <div class="row gutters">
            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                <div class="chat-section">
                    <!-- Row start -->
                    <div class="row no-gutters">
                        <div class="col-xl-3 col-lg-3 col-md-4 col-sm-2 col-3">
                            <div class="users-container">
                                <div class="chat-search-box">
                                    <div class="input-group">
                                        <input class="form-control" placeholder="Search" />
                                        <div class="input-group-btn">
                                            <button type="button" class="btn btn-primary">
                                                <i class="icon-magnifying-glass"></i>
                                            </button>
                                        </div>
                                    </div>
                                </div>



                                <div class="usersContainerScroll">
                                    <ul class="users">
                                        <?php /** @var  \common\models\chat\Room $room */ ?>
                                        <?php foreach ($rooms as $room) { ?>
                                            <?php
                                            $isGroup = false;
                                            if ($room['type_id'] == \common\models\chat\Room::TYPE_ID_GROUP) {
                                                $g = \common\models\chat\Group::findOne(['room_id' => $room['id']]);
                                                try {
                                                    $name = $g->name;
                                                } catch (Exception $e) {
                                                    \cs\services\VarDumper::dump($room);
                                                }
                                                $avatar = \iAvatar777\widgets\FileUpload7\FileUpload::getFile($g->image, 'crop');
                                                $isGroup = true;
                                            } else {
                                                $tet = \common\models\chat\Tet::findOne(['room_id' => $room['id']]);
                                                if (Yii::$app->user->id == $tet['user1_id']) {
                                                    $user = \common\models\UserAvatar::findOne($tet['user2_id']);
                                                } else {
                                                    $user = \common\models\UserAvatar::findOne($tet['user1_id']);
                                                }
                                                $name = $user->getName2();
                                                $avatar = $user->getAvatar();
                                            }
                                            $style = '';
                                            if ($room['id'] == $id) {
                                                $style = ' style="background-color: #ddd"';
                                            }
                                            ?>
                                            <li class="person row m-0 js-personChat" data-id="<?= $room['id'] ?>" <?= $style ?>>
                                                <div class="user">
                                                    <img src="<?= $avatar ?>" alt="<?= $name ?>" />
                                                    <?php if ($isGroup) { ?>
                                                        <span class="status online"></span>
                                                    <?php } ?>
                                                </div>
                                                <div class="col p-0">
                                                    <div class="d-flex justify-content-between">
                                                        <p class="name-time">
                                                            <span class="name"><?= $name ?></span>
                                                            <span class="time"><?= \cs\services\DatePeriod::back($room['last_message'], ['isShort' => true,]) ?></span>
                                                        </p>
                                                        <div class="number-message">
                                                            <span class="badge badge-pill badge-white badgeMessages" data-counter="0"></span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </li>
                                        <?php } ?>

                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="col-xl-9 col-lg-9 col-md-8 col-sm-10 col-9">
                            <div class="active-user-chatting">
                                <div class="active-user-info">
                                    <?php
                                    if ($roomMain['type_id'] == \common\models\chat\Room::TYPE_ID_GROUP) {
                                        $g = \common\models\chat\Group::findOne(['room_id' => $roomMain['id']]);
                                        $name = $g->name;
                                        $avatar = \iAvatar777\widgets\FileUpload7\FileUpload::getFile($g->image, 'crop');
                                    } else {
                                        $tet = \common\models\chat\Tet::findOne(['room_id' => $roomMain['id']]);
                                        if (Yii::$app->user->id == $tet['user1_id']) {
                                            $user = \common\models\UserAvatar::findOne($tet['user2_id']);
                                        } else {
                                            $user = \common\models\UserAvatar::findOne($tet['user1_id']);
                                        }
                                        $name = $user->getName2();
                                        $avatar = $user->getAvatar();
                                    }
                                    ?>
                                    <img src="<?= $avatar ?>" class="avatar" alt="avatar" />
                                    <div class="avatar-info">
                                        <h5><?= $name ?></h5>
                                        <div class="typing"></div>
                                        <div class="typing" style="display: none;">Typing ...</div>
                                    </div>
                                </div>


                                <!-- Audio Call Modal -->
                                <div class="modal fade" id="audioCall" tabindex="-1" role="dialog" aria-labelledby="audioCallLabel" aria-hidden="true">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                            <div class="modal-body">
                                                <div class="call-container">
                                                    <div class="current-user">
                                                        <img src="/img/user24.png" alt="Avatar" >
                                                    </div>
                                                    <h5 class="calling-user-name">
                                                        Amy Hood <span class="calling">Calling...</span>
                                                    </h5>
                                                    <div class="calling-btns">
                                                        <button class="btn btn-secondary" data-dismiss="modal">
                                                            <i class="icon-x"></i>
                                                        </button>
                                                        <button class="btn btn-primary">
                                                            <i class="icon-phone1"></i>
                                                        </button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <!-- Video Call Modal -->
                                <div class="modal fade" id="videoCall" tabindex="-1" role="dialog" aria-labelledby="videoCallLabel" aria-hidden="true">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                            <div class="modal-body">
                                                <div class="call-container">
                                                    <div class="current-user">
                                                        <img src="/img/user22.png" alt="Avatar" >
                                                    </div>
                                                    <h5 class="calling-user-name">
                                                        Zhenya Rynzhuk <span class="calling">Calling...</span>
                                                    </h5>
                                                    <div class="calling-btns">
                                                        <button class="btn btn-secondary" data-dismiss="modal">
                                                            <i class="icon-x"></i>
                                                        </button>
                                                        <button class="btn btn-primary">
                                                            <i class="icon-video"></i>
                                                        </button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                            <div class="chat-container">

                                <div class="chatContainerScroll">
                                    <ul class="chat-box">
                                        <?php /** @var \common\models\chat\Message $m  */ ?>
                                        <?php foreach ($messages as $m) { ?>
                                            <?php $user = \common\models\UserAvatar::findOne($m->user_id); ?>
                                            <?php if ($m->user_id == Yii::$app->user->id) { ?>
                                                <li class='chat-right js-chatMessage' data-id="<?= $m->id ?>">
                                                    <div class='chat-text'>
                                                        <?= $this->render('item-message', ['message' => $m]); ?>
                                                        <div class='chat-hour'><?= Yii::$app->formatter->asTime($m->created_at) ?></div>
                                                    </div>
                                                    <div class='chat-avatar'>
                                                        <img src="<?= $user->getAvatar() ?>" alt="<?= $user->getName2() ?>" />
                                                    </div>
                                                </li>
                                            <?php } else { ?>
                                                <li class='chat-left js-chatMessage' data-id="<?= $m->id ?>">
                                                    <div class='chat-avatar'>
                                                        <img src="<?= $user->getAvatar() ?>" alt="<?= $user->getName2() ?>" />
                                                    </div>
                                                    <div class='chat-text'>
                                                        <?= $this->render('item-message', ['message' => $m]); ?>
                                                        <div class='chat-hour'><?= Yii::$app->formatter->asTime($m->created_at) ?></div>
                                                    </div>
                                                </li>
                                            <?php } ?>
                                        <?php } ?>
                                    </ul>
                                </div>

                                <div class="chat-form">

                                    <div class="form-group">

                                        <textarea class="form-control textareaChat" placeholder="Введи свое сообщение..." data-files="[]"></textarea>
                                        <button class="btn btn-primary buttonSend">
                                            <i class="icon-send"></i>
                                        </button>

                                    </div>

                                </div>
                                <?= \common\widgets\FileUpload8\FileUpload::widget([
                                    'id'        => 'upload' . 1,
                                    'name'      => 'upload' . 1,
                                    'attribute' => 'upload' . 1,
                                    'model'     => new \yii\base\Model(),

                                    'update'    => [
                                        [
                                            'function' => 'crop',
                                            'index'    => 'crop',
                                            'options'  => [
                                                'width'  => '300',
                                                'height' => '300',
                                                'mode'   => 'MODE_THUMBNAIL_CUT',
                                            ],
                                        ],
                                    ],

                                    'settings'  => [
                                        'maxSize'         => 20 * 1000,
                                        'functionSuccess' => new \yii\web\JsExpression(<<<JS
function (response) {
    var files = $('.textareaChat').attr('data-files');
    var filesJson = JSON.parse(files);
    filesJson.push(response.url);
    $('.textareaChat').attr('data-files', JSON.stringify(filesJson));
    
    // Вызываю AJAX для записи в school_file
    ajaxJson({
        url: '/cabinet/file-upload7-save',
        data: {
            file: response.url,
            school_id: 71,
            type_id: 23,
            size: response.size, // Размер файла в байтах
            update: response.update
        },
        success: function (ret) {
            
        }
    });
}

JS
                                        ),
                                    ],
                                ]); ?>
                            </div>
                        </div>
                    </div>
                    <!-- Row end -->
                </div>
            </div>
        </div>
        <!-- Row end -->
    </div>
    <!-- Content wrapper end -->
</div>



