<?php

/** @var \yii\web\View $this */

use yii\helpers\ArrayHelper;
use yii\helpers\Html;

/** @var int $id идентификатор вебинара */
$id = 1;


$this->title = 'Чат';

\common\assets\SocketIO\Asset::register($this);
\avatar\assets\Notify::register($this);


/** @var \common\models\UserAvatar $user */
$user = Yii::$app->user->identity;
$user_id = Yii::$app->user->id;
$user_name = $user->getName2();
$user_avatar = $user->getAvatar();

// Список комнат к которым я подключаюсь
//$roomsIds = \yii\helpers\ArrayHelper::getColumn($rooms, 'id');
//$roomsIdsJson = \yii\helpers\Json::encode($roomsIds);
$server = \avatar\assets\SocketIO\Asset::getHost();
$roomMain = \avatar\modules\chat\models\Room::findOne($id);

$tet_a_tet = ($roomMain->type_id == \avatar\modules\chat\models\Room::TYPE_ID_TET_A_TET)? 1 : 0;

$roomsQ = \avatar\modules\chat\models\ListUser::find()
    ->innerJoin('chat_room', 'chat_room.id = chat_list.room_id')
    ->select([
        'chat_room.*'
    ])
    ->where(['chat_list.user_id' => Yii::$app->user->id])
    ->orderBy(['chat_room.last_message' => SORT_DESC])
    ->asArray();

$rooms = $roomsQ->all();

$this->registerJs(<<<JS
$('.js-personChat').click(function(e) {
    window.location = '/chat/item?id=' + $(this).data('id');
})
JS
);


$host = \avatar\assets\SocketIO\Asset::getHost();
$this->registerJs(<<<JS

var socket = io.connect('{$host}');

function chatScroll() {
    var chat = $(".panel-body");
    chat.scrollTop(chat.prop("scrollHeight"));
}

chatScroll();

var name;
var chatThis = {$id};

const messageInput = document.getElementById('bt-input');
const buttonSend = document.getElementById('btn-chat');
let roomName = {$id};

// объявляю чат сервису что я добавляюсь в комнату roomName
socket.emit('room-new-user', roomName, '{$user_id}');

// $("#btn-input").keyup(function(event) {
//     if (event.keyCode == 13 && $('#btn-input').val().length > 0) {
//         if ($('#btn-input').val().length > 0) {
//             appendMessage();
//         } 
//     }
// });

$(buttonSend).click(function(e) {
    e.preventDefault();
    if ($('#btn-input').val().length > 0) {
        appendMessage();
    }           
});

/**
* @param object data
* {
*     user: {id: int, name: str, avatar: str }
*     message: {html:'', message: {}}
*     room: int
* }
*/
socket.on('room-send-message', data => {
    console.log(data);
    if (data.room == roomName) {

        appendMessage2(data.message);

        var tet_a_tet = {$tet_a_tet};
        if (tet_a_tet) {
            console.log(['room-confirm', roomName, data]);
            console.log(socket.emit('room-send-message1', roomName, data));
        }
    } else {
        
        var o = $('.js-personChat[data-id=' + data.room + ']').find('.badgeMessages');
        var c = o.data('counter');
        c++;
        o.html(c);
        o.data('counter', c);
    }
});

socket.on('room-send-message1', (p1,p2) => {
    console.log(["room-send-message1", p1,p2]);
    console.log(["room-send-message1_1", p1.user.message.message.id, $('.js-chatMessage[data-id='+p1.user.message.message.id+']')]);
    new Noty({
        timeout: 1000,
        theme: 'relax',
        type: 'success',
        layout: 'bottomLeft',
        text: 'Доставлено'
    }).show();
    var dd = $('.js-chatMessage[data-id='+ p1.user.message.message.id+']').find('div.chat-hour').find('span.icon-done');
    dd.removeClass('icon-done').addClass('icon-done_all');
});

socket.on('user-connected', name => {
    console.log("user-connected: " + name +' вошел')
});

socket.on('user-disconnected', name => {
    console.log("user-disconnected: " + name +' вышел')
});

/**
* 
* @param object ret
* {
*     message: {}
*     html: ''
* }
*/
function appendMessage2(ret) 
{
    var d = $(ret.html);
    d.find('[data-toggle="tooltip"]').tooltip();
    
    // Добавляю HTML
    $('.chat').append(d);
    
    // прокручиваю чат вниз
    chatScroll();
}

function appendMessage() {
    if ($('#btn-input').val().length > 0) {
        ajaxJson({
            url: '/chat/site/send',
            data: {
                text: $('#btn-input').val(),
                room_id: {$id},
                user_id: '{$user_id}',
                files: $('#btn-input').attr('data-files')
            },
            success: function(ret) {
                // отправляю сообщение на сервер
                socket.emit('room-send-message', roomName, {id: {$user_id}, name: '{$user_name}', avatar: '{$user_avatar}' }, ret);
                
                appendMessage2(ret);
                
                // очищаю поле сообщения
                $('#btn-input').val('');
                $('#btn-input').attr('data-files','[]');
                $('#upload1 .fileUploadedUrl').html('');

                // Если я пишу в чат который в списке чатов не наверху находится то переношу его вверх
                var i1 = 0;
//                $('li.js-personChat').each(function(i,e) {
//                    // если это выделенный чат?
//                    if ($(e).data('id') == {$id}) {
//                        // если выделенный чат не наверху? 
//                        if (i > 0) {
//                            // ставлю наверх
//                            var first = $('.users')[0].firstChild;
//                            var n1 = $(e)[0];
//                            var list = $('.users')[0];
//                            
//                            list.insertBefore(n1, first);
//                        }
//                    }
//                });
            }
        });
    }
}

JS
);
?>

<style>
    .chat
    {
        list-style: none;
        margin: 0;
        padding: 0;
    }

    .chat li
    {
        margin-bottom: 10px;
        padding-bottom: 5px;
        border-bottom: 1px dotted #B3A9A9;
    }

    .chat li.left .chat-body
    {
        margin-left: 60px;
    }

    .chat li.right .chat-body
    {
        margin-right: 60px;
    }


    .chat li .chat-body p
    {
        margin: 0;
        color: #777777;
    }

    .panel .slidedown .glyphicon, .chat .glyphicon
    {
        margin-right: 5px;
    }

    .panel-body
    {
        overflow-y: scroll;
        height: 600px;
    }

    ::-webkit-scrollbar-track
    {
        -webkit-box-shadow: inset 0 0 6px rgba(0,0,0,0.3);
        background-color: #F5F5F5;
    }

    ::-webkit-scrollbar
    {
        width: 30px;
        background-color: #F5F5F5;
    }

    ::-webkit-scrollbar-thumb
    {
        -webkit-box-shadow: inset 0 0 6px rgba(0,0,0,.3);
        background-color: #555;
    }

    .panel-primary > .panel-heading {
        background-image: linear-gradient(to bottom, #750f0b 0%, #750f0b 100%) !important;
    }

    .panel-primary {
        border-color: #750f0b !important;
    }

    .btn-warning {
        background-image: linear-gradient(to bottom, #5cb85c 0%, #5cb85c 100%);
    }

    .btn-warning:hover, .btn-warning:focus {
        background-color: #5cb85c !important;
        background-position: 0 -15px;
    }

    .btn-warning:hover {
        color: #fff;
        background-color: #5cb85c !important;
        border-color: #5cb85c !important;
    }

</style>


<div class="container">
    <div class="row">

        <div class="col-md-6" style="padding-top: 50px;">

            <?php
            $this->registerJS(<<<JS
$('.rowTable').click(function() {
    if (chatThis != $(this).data('id')) {
        ajaxJson({
            url: '/chat/site/messages',
            data: {id: $(this).data('id')},
            success: function(ret) {
                // Переключить выделение в левом списке
                $('.rowTable[data-id=' + chatThis + ']').css('background-color', '#fff');
                $('.rowTable[data-id=' + ret.room.id + ']').css('background-color', '#ddd');
    
                // Установить заголовок
                chatThis = ret.room.id;
                
                // Добавить сообщения
                var d = $(ret.html);
                d.find('[data-toggle="tooltip"]').tooltip();
                $('.chat').html(d);
                
                // Перемотать вниз
                chatScroll();
           }
        });
    }
});
JS
            );
            ?>
            <?= \yii\grid\GridView::widget([
                'dataProvider' => new \yii\data\ActiveDataProvider([
                    'query'      => $roomsQ
                    ,
                    'pagination' => [
                        'pageSize' => 20,
                    ],
                ]),
                'showHeader' => false,
                'summary' => '',
                'tableOptions' => [
                    'class' => 'table table-striped table-hover',
                ],
                'rowOptions'   => function ($item) {
                    $data = [
                        'data'  => ['id' => $item['id']],
                        'role'  => 'button',
                        'class' => 'rowTable'
                    ];
                    return $data;
                },
                'columns'      => [
                    [
                        'header'  => 'Картинка',
                        'content' => function ($item) {

                            if ($item['type_id'] == \avatar\modules\chat\models\Room::TYPE_ID_GROUP) {
                                $g = \avatar\modules\chat\models\Group::findOne(['room_id' => $item['id']]);
                                $name = $g->name;
                                $avatar = \iAvatar777\widgets\FileUpload7\FileUpload::getFile($g->image, 'crop');
                            } else {
                                $tet = \avatar\modules\chat\models\Tet::findOne(['room_id' => $item['id']]);
                                if (Yii::$app->user->id == $tet['user1_id']) {
                                    $user = \common\models\UserAvatar::findOne($tet['user2_id']);
                                } else {
                                    $user = \common\models\UserAvatar::findOne($tet['user1_id']);
                                }
                                $name = $user->getName2();
                                $avatar = $user->getAvatar();
                            }

                            return Html::img(
                                $avatar,
                                [
                                    'class'  => "img-circle",
                                    'width'  => 50,
                                    'height' => 50,
                                    'style'  => 'margin-bottom: 0px;',
                                ]);
                        }
                    ],
                     [
                        'header'  => 'name',
                        'content' => function ($item) {

                            if ($item['type_id'] == \avatar\modules\chat\models\Room::TYPE_ID_GROUP) {
                                $g = \avatar\modules\chat\models\Group::findOne(['room_id' => $item['id']]);
                                $name = $g->name;
                                $avatar = \iAvatar777\widgets\FileUpload7\FileUpload::getFile($g->image, 'crop');
                            } else {
                                $tet = \avatar\modules\chat\models\Tet::findOne(['room_id' => $item['id']]);
                                if (Yii::$app->user->id == $tet['user1_id']) {
                                    $user = \common\models\UserAvatar::findOne($tet['user2_id']);
                                } else {
                                    $user = \common\models\UserAvatar::findOne($tet['user1_id']);
                                }
                                $name = $user->getName2();
                                $avatar = $user->getAvatar();
                            }

                            return $name;
                        }
                    ],
                ],
            ]) ?>


        </div>

        <div class="col-md-6" style="padding-top: 50px;">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <span class="glyphicon glyphicon-comment"></span> Чат
                </div>
                <div class="panel-body">
                    <ul class="chat">
                        <?php /** @var \avatar\modules\chat\models\Message $message */ ?>
                        <?php foreach (\avatar\modules\chat\models\Message::find()->where(['room_id' => $id])->all() as $message) { ?>
                            <?= $this->render('item-message2', ['message' => $message]); ?>
                        <?php } ?>
                    </ul>
                </div>

                <div class="panel-footer">
                    <div class="input-group">
                        <textarea id="btn-input"
                               type="text"
                               class="form-control input-sm"
                               placeholder="Напиши свое сообщение здесь..."
                               data-files="[]"
                               rows="3"
                        ></textarea>
                        <span class="input-group-btn">

                            <button class="btn btn-warning btn-sm" id="btn-chat" style="height: 66px;">
                                Отправить</button>
                        </span>
                    </div>
                    <hr>
                    <?= \iAvatar777\widgets\FileUpload8\FileUpload::widget([
                        'id'        => 'upload' . 1,
                        'name'      => 'upload' . 1,
                        'attribute' => 'upload' . 1,
                        'model'     => new \avatar\models\forms\Chat1(),

                        'update'    => [
                            [
                                'function' => 'crop',
                                'index'    => 'crop',
                                'options'  => [
                                    'width'  => '300',
                                    'height' => '300',
                                    'mode'   => 'MODE_THUMBNAIL_CUT',
                                ],
                            ],
                        ],

                        'settings'  => [
                            'maxSize'         => 20 * 1000,
                            'server'          => Yii::$app->AvatarCloud->url,
                            'accept'          => '*/*',
                            'functionSuccess' => new \yii\web\JsExpression(<<<JS
function (response) {
    var files = $('#btn-input').attr('data-files');
    var filesJson = JSON.parse(files);
    filesJson.push(response.url);
    $('#btn-input').attr('data-files', JSON.stringify(filesJson));
    
    // Вызываю AJAX для записи в school_file
    ajaxJson({
        url: '/cabinet/file-upload7-save',
        data: {
            file: response.url,
            school_id: 71,
            type_id: 23,
            size: response.size, // Размер файла в байтах
            update: response.update
        },
        success: function (ret) {
            
        }
    });
}

JS
                            ),
                        ],
                    ]); ?>
                </div>
            </div>
        </div>
    </div>
</div>

