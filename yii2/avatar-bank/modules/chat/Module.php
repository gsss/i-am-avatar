<?php
/**
 * Created by PhpStorm.
 * User: Ra-m-ha
 * Date: 23.10.2019
 * Time: 17:45
 */

namespace avatar\modules\chat;

use cs\services\VarDumper;
use yii\base\BootstrapInterface;
use Yii;
use yii\base\Application;

class Module extends \yii\base\Module implements BootstrapInterface
{

//    public $defaultRoute = 'site/index';

    public function init()
    {
        parent::init();

        // инициализация модуля с помощью конфигурации, загруженной из config.php
        \Yii::configure($this, require __DIR__ . '/config.php');

    }

    /**
     * @param Application $app
     */
    public function bootstrap($app)
    {
//        $app->urlManager->addRules([
//            'chat/index'              => 'chat/site/index',
//            'chat/<action>'           => 'chat/site/<action>',
//        ]);
    }

}