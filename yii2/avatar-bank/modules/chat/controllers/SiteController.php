<?php

namespace avatar\modules\chat\controllers;


use cs\services\VarDumper;
use yii\helpers\ArrayHelper;

class SiteController extends  \avatar\controllers\CabinetBaseController
{
    public function actions()
    {
        return [
            'messages'              => [
                'class'    => '\avatar\controllers\actions\DefaultAjax',
                'model'    => '\avatar\modules\chat\models\action\ChatMessages',
            ],
            'send'              => [
                'class'    => '\avatar\controllers\actions\DefaultAjax',
                'model'    => '\avatar\modules\chat\models\action\ChatSend',
                'formName' => '',
            ],
            'file-upload7-save' => [
                'class'    => '\avatar\controllers\actions\DefaultAjax',
                'model'    => '\avatar\models\validate\CabinetControllerFileUpload7Save',
                'formName' => '',
            ],
        ];
    }

    /**
     */
    public function actionIndex()
    {
        return $this->render();
    }


    /**
     */
    public function actionIndex2()
    {
        return $this->render();
    }

    /**
     * @param int $id идентификатор комнаты room.id
     *
     * @return string
     */
    public function actionItem($id)
    {
        return $this->render(['id' => $id]);
    }

}
