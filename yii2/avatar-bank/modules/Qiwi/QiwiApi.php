<?php

namespace avatar\modules\ETH;

use cs\services\VarDumper;
use yii\base\Component;
use yii\base\Exception;

use yii\helpers\Json;
use yii\helpers\StringHelper;
use Yii;
use yii\httpclient\Client;


/**
 * https://developer.qiwi.com/ru/qiwi-wallet-personal/
 */
class QiwiApi extends Component
{
    public $apiUrl = 'https://edge.qiwi.com';
    public $token;

    /**
     * @param $path
     * @param $data
     * @return array
     * @throws \Exception
     */
    public function _call($path, $data = [])
    {
        if (empty($this->token)) throw new \Exception('Не задан токен', 101);

        $client = new Client(['baseUrl' => $this->apiUrl]);
        $response = $client->post(
            $path,
            Json::encode($data),
            [
                'Authorization' => 'Bearer' . ' ' . $this->token,
                'Content-Type'  => 'application/json',
                'Accept'        => 'application/json',
            ]
        )
            ->send();
        if ($response->headers['http-code'] != 200) {
            throw new \Exception('Ошибка на стороне провайдера', $response->headers['http-code']);
        }
        try {
            $data = Json::decode($response->content);
        } catch (\Exception $e) {
            throw new \Exception('JSON', 11);
        }

        return $data;
    }

    /**
     * @param string $phone начиная с '+7'
     * @param float $amount
     * @param string $comment
     * @return array
     * [
     * 'id'
     * 'terms'
     * 'fields'
     * 'sum'
     * 'source'
     * 'comment'
     * 'transaction' =>
     *      [
     *          'id' => '5646565'
     *          'state' =>
     *          [
     *              'code' => 'Accepted'
     *          ]
     *      ]
     * ]
     */
    public function pay($phone, $amount, $comment = '')
    {
        $params = [
            'id'            => (int)(microtime(true) * 1000),
            'sum'           => [
                'amount'   => $amount,
                'currency' => '643',
            ],
            'paymentMethod' => [
                'type'      => 'Account',
                'accountId' => '643',
            ],
            'comment'       => $comment,
            'fields'        => [
                'account' => $phone,
            ],
        ];

        return $this->_call('sinap/api/v2/terms/99/payments', $params);
    }

    /**
     * Получение баланса кошелька
     *
     * @return array
     * {
            "accounts":
             [
                {
                    "alias": "mc_beeline_rub",
                    "fsAlias": "qb_mc_beeline",
                    "title": "MC",
                    "type": {
                        "id": "MC",
                        "title": "Счет мобильного кошелька"
                    },
                    "hasBalance": false,
                    "balance": null,
                    "currency": 643
                },
                {
                    "alias": "qw_wallet_rub",
                    "fsAlias": "qb_wallet",
                    "title": "WALLET",
                    "type": {
                        "id": "WALLET",
                        "title": "QIWI Wallet"
                    },
                    "hasBalance": true,
                    "balance": {
                        "amount": 8.74,
                        "currency": 643
                    },
                    "currency": 643
                }
            ]
        }
     *
     * https://developer.qiwi.com/ru/qiwi-wallet-personal/#balance
     */
    public function balance()
    {
        return $this->_call('funding-sources/v1/accounts/current');
    }

    /**
     * Список платежей
     *
     * @param string $wallet номер кошелька, для которого получен токен доступа (с международным префиксом, но без +)
     * @param array $options
     *
     * + rows - Integer - Число платежей в ответе, для разбивки отчета на части. Целое число от 1 до 50. Обязательный параметр
     *
     * - operation - String - Тип операций в отчете, для отбора. Допустимые значения:
     * ALL - все операции,
     * IN - только пополнения,
     * OUT - только платежи,
     * QIWI_CARD - только платежи по картам QIWI (QVC, QVP).
     * По умолчанию ALL
     *
     * - sources - Array[String] - Источники платежа, для отбора. Каждый источник задается как отдельный параметр и нумеруется элементом массива, начиная с нуля (sources[0], sources[1] и т.д.). Допустимые значения:
     * QW_RUB - рублевый счет кошелька,
     * QW_USD - счет кошелька в долларах,
     * QW_EUR - счет кошелька в евро,
     * CARD - привязанные и непривязанные к кошельку банковские карты,
     * MK - счет мобильного оператора. Если не указаны, учитываются все источники
     *
     * - startDate - DateTime URL-encoded - Начальная дата поиска платежей. Дату можно указать в любой временной зоне TZD (формат ГГГГ-ММ-ДД'T'чч:мм:ссTZD), однако она должна совпадать с временной зоной в параметре endDate. Обозначение временной зоны TZD: +чч:мм или -чч:мм (временной сдвиг от GMT). Используется только вместе с endDate. По умолчанию, равна суточному сдвигу от текущей даты по московскому времени.
     * - endDate - DateTime URL-encoded - Конечная дата поиска платежей. Дату можно указать в любой временной зоне TZD (формат ГГГГ-ММ-ДД'T'чч:мм:ссTZD), однако она должна совпадать с временной зоной в параметре startDate. Обозначение временной зоны TZD: +чч:мм или -чч:мм (временной сдвиг от GMT). Используется только вместе с startDate. По умолчанию, равна текущим дате/времени по московскому времени.
     * - nextTxnDate - DateTime URL-encoded - Дата транзакции для отсчета от предыдущего списка (равна параметру nextTxnDate в предыдущем списке). Используется только вместе с nextTxnId
     * - nextTxnId - Long - Номер предшествующей транзакции для отсчета от предыдущего списка (равен параметру nextTxnId в предыдущем списке). Используется только вместе с nextTxnDate
     *
     * @return array
     * {"data":
     * [{
     * "txnId":9309,
     * "personId":79112223344,
     * "date":"2017-01-21T11:41:07+03:00",
    * "errorCode":0,
    * "error":null,
    * "status":"SUCCESS",
    * "type":"OUT",
    * "statusText":"Успешно",
    * "trmTxnId":"1489826461807",
    * "account":"0003***",
    * "sum":{
    * "amount":70,
    * "currency":"RUB"
    * },
    * "commission":{
    * "amount":0,
    * "currency":"RUB"
    * },
    * "total":{
    * "amount":70,
    * "currency":"RUB"
    * },
    * "provider":{
    * ...
    * },
    * "source": {},
    * "comment":null,
    * "currencyRate":1,
    * "extras":null,
    * "chequeReady":true,
    * "bankDocumentAvailable":false,
    * "bankDocumentReady":false,
    * "repeatPaymentEnabled":false,
    * "favoritePaymentEnabled": true,
    * "regularPaymentEnabled": true
    * }],
    * "nextTxnId":9001,
    * "nextTxnDate":"2017-01-31T15:24:10+03:00"
    * }
     *
     * https://developer.qiwi.com/ru/qiwi-wallet-personal/#section
     */
    public function paymentList($wallet, $options = [])
    {
        return $this->_call('payment-history/v2/persons/wallet/payments');
    }

}