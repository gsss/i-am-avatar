<?php
/**
 * Created by PhpStorm.
 * User: Ra-m-ha
 * Date: 18.07.2019
 * Time: 13:17
 */
namespace avatar\modules\Shop;

use yii\helpers\ArrayHelper;

class Shop
{

    /**
     * Обрабатывает ссылки для реферальной программы типа
     * https://site/projects/item?id=1&partner_id=########## Переносит параметр в сессию
     * `ReferalProgram` ['parent_id' => $value]
     */
    public static function onBeforeRequest()
    {
        $value = \Yii::$app->request->get('partner_id');
        if (!is_null($value)) {
            \Yii::$app->session->set('ReferalProgram', ['parent_id' => $value]);
        }
    }
}