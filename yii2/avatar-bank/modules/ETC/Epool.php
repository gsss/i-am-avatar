<?php

namespace avatar\modules\ETC;

use cs\services\VarDumper;
use yii\base\Component;
use yii\base\Exception;

use yii\helpers\Json;
use yii\helpers\StringHelper;
use Yii;
use yii\httpclient\Client;


/**
 */
class Epool extends Component
{
    public $apiUrl = 'https://mewapi.epool.io';


    /**
     * Возвращает последний блок
     *
     * @return int
     */
    public function blockNumber()
    {
        $value = $this->method('eth_blockNumber');

        return hexdec($value);
    }

    /**
     *
     * @param string | int $height dec value of a block number, or the string "earliest", "latest" or "pending".
     * @param bool $full_list TRUE = full transaction objects, FALSE = only hashes of trasactions.
     * @return array
     */
    public function getBlockByNumber($height, $full_list)
    {
        return $this->method('eth_getBlockByNumber', [
            '0x'.dechex ($height),
            $full_list
        ]);
    }

    /**
     * Делает вызов метода
     *
     * @param string $name
     * @param array $params
     *
     * @return array
     *
     * @throws
     */
    public function method($name, $params = [])
    {
        Yii::info(\yii\helpers\VarDumper::dumpAsString([$name, $params]), 'avatar\modules\ETC\Epool::method');
        $params = [
            'jsonrpc' => '2.0',
            'method'  => $name,
            'params'  => $params,
            'id'      => 1,
        ];
        $response = $this->_call('', Json::encode($params));


        return $response;
    }

    /**
     *
     *
     * @param string    $address
     * @param int       $page
     *
     * @return array сортируются транзакции по дате, первые - молодые
     * {
     * confirms: 3022
     * failed: false
     * from: "0x3f5CE5FBFe3E9af3971dD833D26bA9b5C936f0bE"
     * hash: "0xceb2ba8854d3a2551ee5beefd03bcfa2fc429b0d6c81047acb6edf9b95c01fdf"
     * height: 5104307
     * internal: false
     * isSend: false
     * timestamp: "2017-12-29T01:07:10"
     * to: "0x913C2Ef0Fe908F41f53ceE0b52677B001297b576"
     * value:{
     *      ether: 0.01
     *      hex: "0x2386f26fc10000"
     *      wei: "10000000000000000"
     * }
     * }*
     * @throws \Exception
     */
    public function addressTransationList($address, $page = 1)
    {
        try {
            $client = new Client(['baseUrl' => 'https://api.gastracker.io/v1']);
            $response = $client->get(
                'addr/' . $address . '/operations',
                [
                    ':authority'      => 'api.gastracker.io',
                    ':method'         => 'GET',
                    ':path'           => '/v1/addr/' . $address . '/operations',
                    ':scheme'         => 'https',
                    'accept'          => '*/*',
                    'accept-encoding' => 'gzip, deflate, br',
                    'accept-language' => 'ru-RU,ru;q=0.9,en-US;q=0.8,en;q=0.7',
                    'origin'          => 'http://gastracker.io',
                    'referer'         => 'http://gastracker.io/addr/' . $address,
                    'user-agent'      => 'Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.84 Safari/537.36',
                    'x-compress'      => 'null',
                ])
                ->send();
            if ($response->headers['http-code'] != 200) {
                throw new \Exception('http-code = ' . $response->headers['http-code'], 103);
            }
            $content = $response->content;
            try {
                $data = Json::decode($content);
            } catch (\Exception $e) {
                throw new \Exception('JSON ERROR', 102);
            }

            return $data;
        } catch (\Exception $e) {
            throw $e;
        }
    }

    /**
     *
     *
     * @param $path
     * @param string $data
     *
     * @return mixed
     *
     * @throws
     */
    public function _call($path, $data)
    {
        $client = new Client(['baseUrl' => $this->apiUrl]);
        $response = $client->post($path, $data, ['Content-Type' => 'application/json'])->send();
        Yii::info(\yii\helpers\VarDumper::dumpAsString($response), 'avatar\modules\ETC\Epool::_call');
        if ($response->headers['http-code'] != 200) {
            throw new \Exception('response of ' . $this->apiUrl . ' = ' . $response->headers['http-code']);
        }
        try {
            $data = Json::decode($response->content);
        } catch (\Exception $e) {
            throw new \Exception('server ' . $this->apiUrl . ' response is not valid JSON');
        }
        if (isset($data['error'])) {
            throw new \Exception($response['message']);
        }

        return $data['result'];
    }

}