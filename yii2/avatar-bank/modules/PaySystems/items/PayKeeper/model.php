<?php

namespace avatar\modules\PaySystems\items\PayKeeper;

use cs\Application;
use cs\services\VarDumper;
use yii\base\Event;

use yii\helpers\ArrayHelper;
use yii\helpers\Json;


/**
 *
 */
class model extends \yii\base\Model
{
    /** @var  string */
    public $url;

    /** @var  string */
    public $user;

    /** @var  string  */
    public $password;

    /** @var  string */
    public $secret;

    public function rules()
    {
        return [
            ['url', 'string'],

            ['user', 'string'],

            ['password', 'string'],

            ['secret', 'string'],
        ];
    }

    /**
     * @param string $config JSON
     */
    public function setData($config)
    {
        if (Application::isEmpty($config)) {
            $data = Json::decode('{"url":"","user":"","password":"","secret":""}');
        } else {
            $data = Json::decode($config);
        }
        $this->url = $data['url'];
        $this->user = $data['user'];
        $this->password = $data['password'];
        $this->secret = ArrayHelper::getValue($data, 'secret', '');
    }

    /**
     * @return string JSON
     */
    public function save()
    {
        $data = [
            'url' => $this->url,
            'user'      => $this->user,
            'password' => $this->password,
            'secret'   => $this->secret,
        ];

        return Json::encode($data);
    }

    public function attributeLabels()
    {
        return [
            'url'      => 'url',
            'user'     => 'user',
            'password' => 'password',
            'secret'   => 'secret',
        ];
    }

}