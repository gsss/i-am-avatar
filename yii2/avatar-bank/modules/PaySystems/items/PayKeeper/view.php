<?php

/** $this \yii\web\View  */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \avatar\modules\PaySystems\items\PayKeeper\model */


use yii\helpers\Url;
use app\services\GsssHtml;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use app\services\GetArticle\Collection;

?>

<?= $form->field($model, 'url') ?>
<?= $form->field($model, 'user') ?>
<?= $form->field($model, 'password')->passwordInput() ?>
<?= $form->field($model, 'secret')->passwordInput() ?>
