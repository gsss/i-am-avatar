<?php
/**
 * Created by PhpStorm.
 * User: Бог-Творец
 * Date: 10.04.2016
 * Time: 2:04
 */

namespace avatar\modules\PaySystems\items\PayKeeper\in;

use app\models\Shop\Request;
use app\models\Shop\RequestTransaction;
use common\models\avatar\Currency;
use common\models\BillingMain;
use common\models\BillingMainClass;
use common\models\PaySystemConfig;
use common\services\SberKassa;
use cs\Application;
use Yii;
use app\models\Shop\Payments;
use cs\web\Exception;

use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Json;
use yii\helpers\StringHelper;
use yii\helpers\VarDumper;
use yii\rbac\Role;

class Class1 extends \common\models\piramida\WalletSource\Base implements \common\models\piramida\WalletSourceInterface
{
    /** @var  mixed транзакция от платежной системы */
    public $transaction;

    /** @var  double */
    public $sumAfter;

    /** @var  double */
    public $sumBefore;

    public function getTransactionInfo()
    {
        return $this->transaction;
    }

    public function getTypeId() {}

    /**
     * Выдает цену с учетом оплаты комиссии
     *
     * @param float $price цена которую хотим получить от клиента через прием денег через эту платежную систему
     *
     * @return float цена для оплаты в платежной соистеме
     */
    public function getPriceWithTax($price)
    {
        $d = $price / (1 - ($this->getTax() / 100));
        $d = explode('.', $d);
        if (count($d) > 1) {
            return (float)$d[0] . '.' . substr($d[1], 0, 2);
        }
        return (float)$d[0];
    }

    public function getTax()
    {
        return 0;
    }

    public function getSumBefore()
    {
        return $this->sumBefore;
    }

    public function getSumAfter()
    {
        return $this->sumAfter;
    }

    /**
     * Формирует форму
     *
     * @param BillingMain $billing
     * @param string $description
     * @param string $destinationAddress - счет поучателя
     *
     * @param null | array $options
     * - form - array - параметры(аттрибуты) тега формы
     * - values - array - доп параметры для формы как скрытые поля (ключ => значение)
     * - action - string - поле label в форме
     *
     * @return string
     */
    public function getForm($billing, $description, $destinationName, $options = null)
    {
        $config  = Json::decode($this->config);

        $client = \iAvatar777\components\PayKeeperApi\PayKeeperApi::createObject([
            'url'      => $config['url'],
            'user'     => $config['user'],
            'password' => $config['password'],
            'secret'   => $config['secret'],
        ]);

        /** @var \common\models\PaymentPayKeeper $payment */
        $payment1 = \common\models\PaymentPayKeeper::find()->where(['billing_id' => $billing->id])->one();
        $currency = Currency::findOne(6);

        if (is_null($payment1)) {
            $Payment = $client->createPayment([
                'pay_amount'   => $billing->sum_after / pow(10, $currency->decimals),
                'orderid'      => $billing->id,
                'service_name' => $description,
            ]);
            Yii::info($Payment, 'avatar\modules\PaySystems\items\PaymentPayKeeper\in\Base::getForm1');

            $payment1 = \common\models\PaymentPayKeeper::add([
                'amount'      => $billing->sum_after,
                'billing_id'  => $billing->id,
                'payment_id'  => $Payment['invoice_id'],
                'form_url'    => $Payment['invoice_url'],
                'currency_id' => $currency->id,
            ]);
            Yii::info($payment1, 'avatar\modules\PaySystems\items\PaymentPayKeeper\in\Base::getForm2');
        } else {
//            $Payment = $client->getPaymentInfo($payment1->payment_id);
        }

        return \Yii::$app->view->renderFile('@avatar/modules/PaySystems/items/PayKeeper/in/template.php', [
            'billing'            => $billing,
            'destinationAddress' => $this->config,
            'payment1'           => $payment1,
        ]);
    }

    /**
     * GET
     * [
    1	id	Уникальный номер платежа	Да
    2	sum	Сумма платежа	Да
    3	clientid	Фамилия Имя Отчество	Нет
    4	orderid	Номер заказа	Нет
    5	key	Цифровая подпись запроса, строка из символов a-f и 0-9	Да
     * ]
     */
    public function success($a)
    {
        Yii::info(Yii::$app->request->get(), 'avatar\modules\PaySystems\items\PaymentPayKeeper\in\Base::success');
        $billing_id = Yii::$app->request->get('orderid');

        // Проверяю контрольную сумму
        $client = $this->getProvider($billing_id);
        $params = Yii::$app->request->get();
        $checksum = $params['checksum'];
        unset($params['checksum']);
        if (!$client->isCheckSum($params, $checksum)) {
            throw new \Exception('Не верная контрольная сумма'.VarDumper::dumpAsString([$params, $checksum]));
            return;
        }

        // обобряю счет
        $bill = BillingMain::findOne($billing_id);
        $bill->successClient();
        Yii::info($bill, 'avatar\modules\PaySystems\items\PayKeeper\in\Base::success2');

        $bill->successShop();
        Yii::info('finish', 'avatar\modules\PaySystems\items\PayKeeper\in\Base::success3');
    }

    /**
     * @param $billing_id
     * @return \common\models\PaymentPayKeeper
     */
    public function getProvider($billing_id)
    {
        $bill = BillingMain::findOne($billing_id);
        $class_id = $bill->class_id;
        $o = BillingMainClass::findOne($class_id);
        $class = $o->name;
        /** @var \common\interfaceList\BillingInterface $object */
        $object = new $class;
        $company_id = $object->getCompanyId();
        /** @var \common\models\PaySystemConfig $configObject */
        $configObject = PaySystemConfig::findOne(['school_id' => $company_id, 'paysystem_id' => $bill->source_id]);
        if (is_null($configObject)) {
            throw new \Exception('Нет такой ПС');
        }
        $config = Json::decode($configObject->config);

        $client = new \common\models\PaymentPayKeeper($config);

        return $client;
    }

}