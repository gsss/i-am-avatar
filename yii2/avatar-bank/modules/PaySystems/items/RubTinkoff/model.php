<?php

namespace avatar\modules\PaySystems\items\RubTinkoff;

use cs\Application;
use cs\services\VarDumper;
use yii\base\Event;

use yii\helpers\ArrayHelper;
use yii\helpers\Json;


/**
 *
 */
class model extends \yii\base\Model
{
    /** @var  string Идентификатор магазина */
    public $num;

    public function rules()
    {
        return [
            ['num', 'required'],
            ['num', 'string'],
        ];
    }

    /**
     * @param string $config JSON
     */
    public function setData($config)
    {
        if (Application::isEmpty($config)) {
            $data = Json::decode('{"num":""}');
        } else {
            $data = Json::decode($config);
        }
        $this->num = $data['num'];
    }

    /**
     * @return string JSON
     */
    public function save()
    {
        $data = ['num' => $this->num];

        return Json::encode($data);
    }

    public function attributeLabels()
    {
        return [
            'num' => 'Идентификатор магазина',
        ];
    }

}