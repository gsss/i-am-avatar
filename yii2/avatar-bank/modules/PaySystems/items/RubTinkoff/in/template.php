<?php
/**
 * Created by PhpStorm.
 * User: Бог-Творец
 * Date: 10.04.2016
 * Time: 2:04
 */
/** @var \yii\web\View                  $this */
/** @var \common\models\BillingMain     $billing */
/** @var string                         $destinationAddress */


use yii\helpers\ArrayHelper;
use yii\helpers\Html;

$config = \yii\helpers\Json::decode($destinationAddress);

\Yii::$app->view->registerJs(<<<JS
    // $('#formPay').submit();
JS
);

$c = \common\models\avatar\Currency::findOne($billing->currency_id);

$v = $billing->sum_after / pow(10, $c->decimals);
/** @var \common\models\UserAvatar $user */
$user = Yii::$app->user->identity;
$request = \common\models\shop\Request::findOne(['billing_id' => $billing->id ]);

?>



<script src="https://securepay.tinkoff.ru/html/payForm/js/tinkoff_v2.js"></script>
<form name="TinkoffPayForm" onsubmit="pay(this); return false;" id="formPay">
    <input class="tinkoffPayRow" type="hidden" name="terminalkey" value="<?= $config['num'] ?>">
    <input class="tinkoffPayRow" type="hidden" name="frame" value="true">
    <input class="tinkoffPayRow" type="hidden" name="language" value="ru">
    <input class="tinkoffPayRow" type="hidden" placeholder="Сумма заказа" name="amount" required value="<?= $v ?>">
    <input class="tinkoffPayRow" type="hidden" placeholder="Номер заказа" name="order" value="<?= $request->id ?>">
    <input class="tinkoffPayRow form-control" type="text" placeholder="Описание заказа" name="description">
    <input class="tinkoffPayRow form-control" type="text" placeholder="ФИО плательщика" name="name">
    <input class="tinkoffPayRow" type="hidden" placeholder="E-mail" name="email" value="<?= $user->email ?>">
    <input class="tinkoffPayRow form-control" type="text" placeholder="Контактный телефон" name="phone">

    <input class="tinkoffPayRow" type="submit" value="Оплатить">
</form>