<?php
/**
 * Created by PhpStorm.
 * User: Бог-Творец
 * Date: 10.04.2016
 * Time: 2:04
 */
/** @var \yii\web\View                  $this */
/** @var \common\models\BillingMain     $billing */
/** @var string                         $destinationAddress */

\avatar\assets\Notify::register($this);


/**
 * @var null | array $options
 * - form - array - параметры(аттрибуты) тега формы
 * - values - array - доп параметры для формы как скрытые поля (ключ => значение)
 * - action - string - поле label в форме
 * - successURL - string -
 *
 */
$successURL = '';
if (!is_null($options)) {
    if (isset($options['successURL'])) {
        $successURL = $options['successURL'];
    }
}


use yii\helpers\ArrayHelper;
use yii\helpers\Html;

$config = \yii\helpers\Json::decode($destinationAddress);

\Yii::$app->view->registerJs(<<<JS
    $('.buttonPay').click(function(e) {
        ajaxJson({
            url: '/shop/pay-gus',
            data: {
                billing_id: {$billing->id},
                wallet_id: $($('#selectWallet')[0][$('#selectWallet')[0].selectedIndex]).attr('id')
            },
            success: function(ret) {
                if ('{$successURL}' != '') {
                    window.location = '{$successURL}';
                }
            },
            errorScript: function(ret) {
                if (ret.id == 102) {
                    for (var key in ret.data) {
                        if (ret.data.hasOwnProperty(key)) {
                            var name = key;
                            var value = ret.data[key];
                            new Noty({
                                timeout: 1000,
                                theme: 'relax',
                                type: 'warning',
                                layout: 'bottomLeft',
                                text: value.join('<br>')
                            }).show();
                        }
                    }
                }
            }
        });
    });
JS
);

$c = \common\models\avatar\Currency::findOne($billing->currency_id);

$v = $billing->sum_after / pow(10, $c->decimals);
/** @var \common\models\UserAvatar $user */
$user = Yii::$app->user->identity;
$request = \common\models\shop\Request::findOne(['billing_id' => $billing->id ]);



?>

<p>Выберите свой кошелек с которого будете оплачивать</p>



<select id="selectWallet" class="form-control">
    <?php /** @var \common\models\avatar\UserBill $i */ ?>
    <?php foreach (\common\models\avatar\UserBill::find()->where(['user_id' => Yii::$app->user->id, 'currency' => 14, 'mark_deleted' => 0])->all() as $i) { ?>
        <?php
        $w = $i->address;
        if (\yii\helpers\StringHelper::startsWith($i->address, 'W_')) {
            $id = (int) substr($i->address, 2);
        } else {
            $id = (int) $i->address;
        }
        $w = \common\models\piramida\Wallet::findOne($id);
        $c = \common\models\piramida\Currency::findOne($w->currency_id);
        $amount = Yii::$app->formatter->asDecimal($w->amount/pow(10, $c->decimals), $c->decimals);
        ?>
        <option id="<?= $w->id ?>"><?= $i->name ?> (<?= $amount ?> <?= $c->code ?>)</option>
    <?php } ?>
</select>
<hr>
<input type="button" value="Оплатить" class="btn btn-success buttonPay">
