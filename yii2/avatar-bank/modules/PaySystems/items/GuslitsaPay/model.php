<?php

namespace avatar\modules\PaySystems\items\GuslitsaPay;

use common\models\piramida\Wallet;
use cs\Application;
use cs\services\VarDumper;
use yii\base\Event;

use yii\helpers\ArrayHelper;
use yii\helpers\Json;
use yii\helpers\StringHelper;


/**
 *
 */
class model extends \yii\base\Model
{
    /** @var  string адрес кошелька  */
    public $address;

    public function rules()
    {
        return [
            ['address', 'required'],
            ['address', 'string'],
            ['address', 'validateWallet'],
        ];
    }

    /**
     *
     */
    public function validateWallet($attribute, $params)
    {
        if (!$this->hasErrors()) {
            if (!StringHelper::startsWith($this->address, 'W_')) {
                $this->addError($attribute, 'Кошелек должен начинаться с W_');
                return;
            }
            $id = (int)(substr($this->address,2));
            $wallet = Wallet::findOne($id);
            if (is_null($wallet)) {
                $this->addError($attribute, 'Не найден кошелек');
                return;
            }
        }
    }

    /**
     * @param string $config JSON
     */
    public function setData($config)
    {
        if (Application::isEmpty($config)) {
            $data = Json::decode('{"address":""}');
        } else {
            $data = Json::decode($config);
        }
        $this->address = $data['address'];
    }

    /**
     * @return string JSON
     */
    public function save()
    {
        $data = ['address' => $this->address];

        return Json::encode($data);
    }

    public function attributeLabels()
    {
        return [
            'address' => 'адрес кошелька',
        ];
    }

}