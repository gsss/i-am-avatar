<?php

namespace avatar\modules\PaySystems\items\RubSber\success;

use common\models\BillingMain;
use common\models\PaymentEthereum;
use common\models\PaymentSber;
use cs\Application;
use cs\services\VarDumper;
use yii\base\Event;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;
use yii\helpers\StringHelper;


/**
 *
 */
class model extends \yii\base\Model
{
    /** @var  BillingMain он должен передаваться при инициализации */
    public $billing;

    /** @var  string Идентификатор транзакции */
    public $txid;

    public function rules()
    {
        return [
            ['txid', 'required'],
            ['txid', 'string'],
        ];
    }


    /**
     * @return string возвращает данные для вывода в заказ что заказ оплачен и какие параменты клиент указал
     * @throws
     */
    public function action()
    {
        $payment = PaymentSber::findOne(['billing_id' => $this->billing->id]);
        $payment->transaction = $this->txid;
        $payment->save();

        return 'txid = ' . $this->txid;
    }

    public function attributeLabels()
    {
        return [
            'txid' => 'Идентификатор транзакции',
        ];
    }

}