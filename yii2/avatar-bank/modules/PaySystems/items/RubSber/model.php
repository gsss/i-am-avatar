<?php

namespace avatar\modules\PaySystems\items\RubSber;

use cs\Application;
use cs\services\VarDumper;
use yii\base\Event;

use yii\helpers\ArrayHelper;
use yii\helpers\Json;


/**
 *
 */
class model extends \yii\base\Model
{
    /** @var  string кошелек */
    public $card;

    public function rules()
    {
        return [
            ['card', 'required'],
            ['card', 'string'],
        ];
    }

    /**
     * @param string $config JSON
     */
    public function setData($config)
    {
        if (Application::isEmpty($config)) {
            $data = Json::decode('{"card":""}');
        } else {
            $data = Json::decode($config);
        }
        $this->card = $data['card'];
    }

    /**
     * @return string JSON
     */
    public function save()
    {
        $data = ['card' => $this->card];

        return Json::encode($data);
    }

    public function attributeLabels()
    {
        return [
            'card' => 'Карта',
        ];
    }

}