<?php

namespace avatar\modules\PaySystems\items\ElxgoldAvatarPay;

use common\models\piramida\Wallet;
use cs\Application;
use cs\services\VarDumper;
use yii\base\Event;

use yii\helpers\ArrayHelper;
use yii\helpers\Json;


/**
 *
 */
class model extends \yii\base\Model
{
    /** @var  string адрес кошелька  */
    public $address;

    public function rules()
    {
        return [
            ['address', 'required'],
            ['address', 'string'],
            ['address', 'validateWallet'],
        ];
    }

    /**
     *
     */
    public function validateWallet($attribute, $params)
    {
        if (!$this->hasErrors()) {
            $wallet = Wallet::findOne(['address' => $this->address]);
            if (is_null($wallet)) {
                $this->addError($attribute, 'Не найден кошелек');
                return;
            }
            if ($wallet->currency_id != 1) {
                $this->addError($attribute, 'Этот кошелек не золотого элисира');
            }
        }
    }

    /**
     * @param string $config JSON
     */
    public function setData($config)
    {
        if (Application::isEmpty($config)) {
            $data = Json::decode('{"address":""}');
        } else {
            $data = Json::decode($config);
        }
        $this->address = $data['address'];
    }

    /**
     * @return string JSON
     */
    public function save()
    {
        $data = ['address' => $this->address];

        return Json::encode($data);
    }

    public function attributeLabels()
    {
        return [
            'address' => 'адрес кошелька',
        ];
    }

}