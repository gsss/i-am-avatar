<?php
/**
 * Created by PhpStorm.
 * User: Бог-Творец
 * Date: 10.04.2016
 * Time: 2:04
 */

namespace avatar\modules\PaySystems\items\RubYandexWallet\in;

use app\models\Piramida\InRequest;
use app\models\Shop\Request;
use app\models\Shop\RequestTransaction;
use app\modules\Shop\services\Basket;
use common\models\BillingMain;
use common\models\PaySystem;
use common\models\piramida\YandexTransaction;
use cs\Application;
use Yii;
use app\models\Shop\Payments;
use cs\web\Exception;

use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\StringHelper;
use yii\helpers\VarDumper;
use yii\rbac\Role;

class Base extends \common\models\piramida\WalletSource\Base implements \common\models\piramida\WalletSourceInterface
{
    /** @var  array транзакция от платежной системы */
    public $transaction;

    /** @var  string тип операци от яндекса 'PC' 'AC' */
    public $paymentType;

    /** @var  double */
    public $sumAfter;

    /** @var  double */
    public $sumBefore;

    public function getTransactionInfo()
    {
        return $this->transaction;
    }

    /**
     * Выдает цену с учетом оплаты комиссии
     *
     * @param float $price цена которую хотим получить от клиента через прием денег через эту платежную систему
     *
     * @return float цена для оплаты в платежной соистеме
     */
    public function getPriceWithTax($price)
    {
        $d = $price / (1 - ($this->getTax() / 100));
        $d = explode('.', $d);
        if (count($d) > 1) {
            return (float)$d[0] . '.' . substr($d[1], 0, 2);
        }
        return (float)$d[0];
    }

    public function getTax()
    {
        return 2;
    }

    public function getSumBefore()
    {
        return $this->sumBefore;
    }

    public function getSumAfter()
    {
        return $this->sumAfter;
    }

    /**
     * Формирует форму
     *
     * @param BillingMain $billing
     * @param string $description
     * @param string $destinationName - наименование поучателя
     * @param string $destinationAddress - счет поучателя
     *
     * @param null | array $options
     * - form - array - параметры(аттрибуты) тега формы
     * - values - array - доп параметры для формы как скрытые поля (ключ => значение)
     * - action - string - поле label в форме
     * - successURL - string - URL-адрес для редиректа после совершения перевода.
     *
     * @return string
     */
    public function getForm($billing, $description, $destinationName, $options = null)
    {
        if ($billing->sum_after >= 1500000) {
            if ($this->paymentType == 'PC') {
                return \Yii::$app->view->renderFile('@avatar/modules/PaySystems/items/RubYandexWallet/in/template.php', [
                    'billing'            => $billing,
                    'destinationAddress' => $this->config,
                ]);
            } else if ($this->paymentType == 'AC') {
                return \Yii::$app->view->renderFile('@avatar/modules/PaySystems/items/RubYandexWallet/in/template.php', [
                    'billing'            => $billing,
                    'destinationAddress' => $this->config,
                ]);
            }
        } else {
            return $this->getFormToYandex($billing, $description, $destinationName, $this->config, $options);
        }
    }

    /**
     * Рисует форму и вызывает событие отправки формы
     *
     * https://yandex.ru/dev/money/doc/payment-buttons/reference/forms-docpage/
     */
    private function getFormToYandex($billing, $description, $destinationName, $destinationAddress, $options = null)
    {
        $content = [];
        $config = ArrayHelper::toArray(json_decode($destinationAddress));
        $values = [
            'quickpay-form' => 'donate',
            'short-dest'    => $description,
            'targets'       => $description,
            'formcomment'   => $destinationName,
            'comment'       => '',
            'need-fio'      => 'false',
            'need-email'    => 'false',
            'need-phone'    => 'false',
            'need-address'  => 'false',
            'paymentType'   => $this->paymentType,
            'sum'           => round($billing->sum_before / 100, 2),
            'receiver'      => $config['wallet'],
        ];
        if (isset($options['action'])) {
            $values['label'] = $options['action'];
        }
        if (isset($options['successURL'])) {
            $values['successURL'] = $options['successURL'];
        }
        foreach ($values as $key => $value) {
            $content[] = Html::hiddenInput($key, $value);
        }
        $formOptions = ArrayHelper::getValue($options, 'form', []);
        $formOptions = ArrayHelper::merge([
            'id'     => 'formPay',
            'method' => 'post',
            'action' => 'https://money.yandex.ru/quickpay/confirm.xml',
        ], $formOptions);
        foreach (ArrayHelper::getValue($options, 'values', []) as $key => $value) {
            $content[] = Html::hiddenInput($key, $value);
        }
        \Yii::$app->view->registerJs(<<<JS
$('#formPay').submit();
JS

        );
        $form = Html::tag('form', join("\n", $content), $formOptions);

        return $form;
    }

    public function validate($secretCode)
    {
        $fields = \Yii::$app->request->post();

        return $this->isValidSha1($fields, $secretCode);
    }

    /**
     * Проверка целостности полей и подлинности
     *
     * @param array $fields поля
     * @param string $notification_secret секретный код от Яндекса
     *
     * @return bool
     */
    private function isValidSha1($fields, $notification_secret)
    {
        // notification_type&operation_id&amount&currency&datetime&sender&codepro&notification_secret&label
        $arr = [
            'notification_type',
            'operation_id',
            'amount',
            'currency',
            'datetime',
            'sender',
            'codepro',
            'notification_secret',
            'label',
        ];
        $str = [];
        foreach ($arr as $i) {
            if ($i == 'notification_secret') {
                $str[] = $notification_secret;
            } else {
                $str[] = ArrayHelper::getValue($fields, $i, '');
            }
        }
        $str = join('&', $str);
        $sha1 = sha1($str);

        $sha1Fields = ArrayHelper::getValue($fields, 'sha1_hash', '');
        if ($sha1Fields == '') return false;

        return $sha1Fields == $sha1;
    }

    /**
     * Прием денег из яндекса
     *
     * @params array $actions
     *
     * @return string
     */
    public function success($actions)
    {
        if (Yii::$app->request->get('type') != 'yandex') {
            return false;
        }
        $fields = Yii::$app->request->post();
        Yii::info(VarDumper::dumpAsString($fields), 'avatar\\payments\\yandex\\success');
        if (ArrayHelper::getValue($fields, 'test_notification', '') == 'true') {
            return true;
        }

        $this->transaction = $fields;

        // https://money.yandex.ru/doc.xml?id=526991
        // Удостоверение подлинности и целостности уведомления

        // живой платеж


//        $secretCode = ArrayHelper::toArray(json_decode(PaySystem::findOne($this->source_id)->destination));
//        $secretCode = $secretCode['secret'];
//
//        $fields['is_valid'] = ($this->isValidSha1($fields, $secretCode)) ? 1 : 0;

        // добавляю в БД
        if (YandexTransaction::find()->where([
            'operation_id'    => $fields['operation_id'],
            'operation_label' => $fields['operation_label'],
            'sha1_hash'       => $fields['sha1_hash'],
        ])->exists()) {
            Yii::info('payments operation_id = ' . $fields['operation_id'] . ' already exists', 'avatar\\pay');
            return false;
        }
        $dbFields = [
            'id',
            'notification_type',
            'amount',
            'datetime',
            'date_insert',
            'codepro',
            'withdraw_amount',
            'sender',
            'sha1_hash',
            'unaccepted',
            'operation_label',
            'operation_id',
            'currency',
            'label',
            'is_valid',
            'zip',
            'firstname',
            'city',
            'building',
            'lastname',
            'suite',
            'phone',
            'street',
            'flat',
            'email',
            'fathersname',
        ];
        $new = [];
        foreach($fields as $n =>  $v) {
            if (in_array($n, $dbFields)) {
                $new[$n] = $v;
            }
        }
        YandexTransaction::add($new);

        // проверка на верность
        $label = ArrayHelper::getValue($fields, 'label', '');
        Yii::info($label, 'avatar\\payments\\yandex\\success\\label');
        if ($label != '') {
            $pos = strpos($label, '.');
            if ($pos === false) return false;
            $prefix = substr($label, 0, $pos);
            $id = substr($label, $pos + 1);
            Yii::info(VarDumper::dumpAsString([$prefix, $actions]), 'avatar\\payments\\yandex\\success\\label2');
            if (array_key_exists($prefix, $actions)) {
                Yii::info($label, 'avatar\\payments\\yandex\\success\\label3');
                $function = $actions[$prefix];
                $function($id, $this);
            }
        }

        return true;
    }

}