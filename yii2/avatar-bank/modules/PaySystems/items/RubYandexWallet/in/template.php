<?php
/**
 * Created by PhpStorm.
 * User: Бог-Творец
 * Date: 10.04.2016
 * Time: 2:04
 */
/** @var \yii\web\View $this */
/** @var \common\models\BillingMain $billing */
/** @var string $destinationAddress */


use yii\helpers\ArrayHelper;
use yii\helpers\Html;

$config = ArrayHelper::toArray(json_decode($destinationAddress));

?>
<p><?= \Yii::t('c18', 'Вам нужно перевести {sum} руб. на счет: <b>{card}</b>', [
        'sum'  => \Yii::$app->formatter->asDecimal($billing->sum_before / 100, 2),
        'card' => $config['wallet'],
    ])
    ?></p>
<p>И нажмите "Подтвердить"</p>
<p>Мы подтвердим оплату и вы получите энергопакет по почте</p>

<p>
    <button class="btn btn-success btn-lg" id="buttonSuccess">Подтвердить</button>
</p>
