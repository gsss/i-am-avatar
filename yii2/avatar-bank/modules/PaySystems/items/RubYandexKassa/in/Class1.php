<?php
/**
 * Created by PhpStorm.
 * User: Бог-Творец
 * Date: 10.04.2016
 * Time: 2:04
 */

namespace avatar\modules\PaySystems\items\RubYandexKassa\in;

use app\models\Shop\Request;
use app\models\Shop\RequestTransaction;
use common\models\BillingMain;
use common\models\PaymentYandexKassa;
use common\models\school\School;
use cs\Application;
use YandexCheckout\Client;
use YandexCheckout\Model\NotificationEventType;
use Yii;
use app\models\Shop\Payments;
use cs\web\Exception;

use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Json;
use yii\helpers\StringHelper;
use yii\helpers\VarDumper;
use yii\rbac\Role;

class Class1 extends \common\models\piramida\WalletSource\Base implements \common\models\piramida\WalletSourceInterface
{


    /** @var  array транзакция от платежной системы */
    public $transaction;

    /** @var  string тип операци от яндекса 'PC' 'AC' */
    public $paymentType;

    /** @var  double */
    public $sumAfter;

    /** @var  double */
    public $sumBefore;

    public function getTransactionInfo()
    {
        return $this->transaction;
    }

    /**
     * Выдает цену с учетом оплаты комиссии
     *
     * @param float $price цена которую хотим получить от клиента через прием денег через эту платежную систему
     *
     * @return float цена для оплаты в платежной соистеме
     */
    public function getPriceWithTax($price)
    {
        $d = $price / (1 - ($this->getTax() / 100));
        $d = explode('.', $d);
        if (count($d) > 1) {
            return (float)$d[0] . '.' . substr($d[1], 0, 2);
        }
        return (float)$d[0];
    }

    public function getTax()
    {
        return 2;
    }

    public function getSumBefore()
    {
        return $this->sumBefore;
    }

    public function getSumAfter()
    {
        return $this->sumAfter;
    }

    /**
     * Генерирует форму
     *
     * @param BillingMain $billing
     * @param string $description
     * @param string $destinationName    - наименование получателя
     * @param string $destinationAddress - счет получателя
     *
     * @param null | array $options
     * - form - array - параметры(аттрибуты) тега формы
     * - values - array - доп параметры для формы как скрытые поля (ключ => значение)
     * - action - string - поле label в форме
     * - successURL - string - URL-адрес для редиректа после совершения перевода.
     *
     * @return string
     */
    public function getForm($billing, $description, $destinationName, $options = null)
    {
        $config  = Json::decode($this->config);

        $client = new Client();
        $client->setAuth($config['wallet'], $config['secret']);

        /** @var \common\models\PaymentYandexKassa $payment */
        $payment1 = \common\models\PaymentYandexKassa::find()->where(['billing_id' => $billing->id])->one();
        if (is_null($payment1)) {
            $Payment = $client->createPayment(
                [
                    'amount'       => [
                        'value'    => round($billing->sum_before / 100, 2),
                        'currency' => 'RUB',
                    ],
                    'confirmation' => [
                        'type'       => 'redirect',
                        'return_url' => ArrayHelper::getValue($options, 'successURL', ''),
                    ],
                    'metadata' => [
                        'billing_id' => $billing->id,
                    ],
                    'capture'      => true,
                    'description'  => $description,
                ],
                uniqid('', true)
            );

            $payment1 = \common\models\PaymentYandexKassa::add([
                'amount'      => $billing->sum_after,
                'billing_id'  => $billing->id,
                'payment_id'  => $Payment->id,
                'currency_id' => 6,
            ]);
        } else {
            $Payment = $client->getPaymentInfo($payment1->payment_id);
        }

        return \Yii::$app->view->renderFile('@avatar/modules/PaySystems/items/RubYandexKassa/in/template.php', [
            'billing'            => $billing,
            'destinationAddress' => $this->config,
            'client'             => $client,
            'Payment'            => $Payment,
        ]);
    }

    /**
     */
    private function addWebhook($client)
    {
        $response = $client->addWebhook([
            "event" => NotificationEventType::PAYMENT_SUCCEEDED,
            "url"   => "https://for-people.life/shop-order/success?type=yandexkassa",
        ]);
    }

    public function success($actions)
    {
        $rawBody = Yii::$app->request->rawBody;
        $rawBody = str_replace('\\"', '"', $rawBody);
        $data = Json::decode($rawBody);
        Yii::info(VarDumper::dumpAsString($data), 'avatar\modules\PaySystems\items\RubYandexKassa\in\Class1::success');
        if (ArrayHelper::getValue($data, 'type') == 'notification') {
            if (ArrayHelper::getValue($data, 'event') == 'payment.succeeded') {
                $billing = BillingMain::findOne($data['object']['metadata']['billing_id']);
                if ($billing->is_paid_client == 0) $billing->successClient();
                $billing->successShop();
            }
        }
    }

    public function aa1()
    {
        $psconfig = $this->psconfig;
        $data = Json::decode($psconfig->config);

        /** @var \common\services\YandexKassa $YandexKassa */
        $YandexKassa = Yii::$app->YandexKassa;
        $YandexKassa->shopID = $data['wallet'];
        $YandexKassa->secret = $data['secret'];

        $payment = $YandexKassa->createPatment([
            'amount'       => [
                'value'    => 1000.0,
                'currency' => 'RUB',
            ],
            'confirmation' => [
                'type'       => 'redirect',
                'return_url' => School::get()->getUrl('/koop/pay'),
            ],
            'metadata' => [
                'label' => 'koopEnter.' . $this->item->id,
            ],
            'capture'      => true,
            'description'  => 'Оплата заявки №' . $this->item->id,
        ]);

        $this->item->pay_id = $payment['id'];
        $this->item->save();

        /**
         * [
         * 'id' => '26413069-000f-5000-9000-10f8079fbc84'
         * 'status' => 'pending'
         * 'paid' => false
         * 'amount' => [
         * 'value' => '100.00'
         * 'currency' => 'RUB'
         * ]
         * 'confirmation' => [
         * 'type' => 'redirect'
         * 'confirmation_url' => 'https://money.yandex.ru/api-pages/v2/payment-confirm/epl?orderId=26413069-000f-5000-9000-10f8079fbc84'
         * ]
         * 'created_at' => '2020-05-03T19:34:01.276Z'
         * 'description' => 'Заказ №1'
         * 'metadata' => []
         * 'recipient' => [
         * 'account_id' => '701775'
         * 'gateway_id' => '1712759'
         * ]
         * 'refundable' => false
         * 'test' => true
         * ]
         */
        return $payment;
    }

}