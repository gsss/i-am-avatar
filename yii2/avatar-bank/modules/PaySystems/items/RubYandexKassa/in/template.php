<?php
/**
 * Created by PhpStorm.
 * User: Бог-Творец
 * Date: 10.04.2016
 * Time: 2:04
 */
/** @var \yii\web\View $this */
/** @var \common\models\BillingMain $billing */
/** @var string $destinationAddress */
/** @var \YandexCheckout\Client $client */
/** @var \YandexCheckout\Model\PaymentInterface $Payment */

use YandexCheckout\Client;

use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Json;

$config  = Json::decode($destinationAddress);

$payment = ArrayHelper::toArray($Payment);
$href = $Payment['confirmation']['confirmation_url'];

$this->registerJs(<<<JS

window.location = '{$href}';

JS
)
?>

