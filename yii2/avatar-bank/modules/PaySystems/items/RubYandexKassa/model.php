<?php

namespace avatar\modules\PaySystems\items\RubYandexKassa;

use cs\Application;
use cs\services\VarDumper;
use yii\base\Event;

use yii\helpers\ArrayHelper;
use yii\helpers\Json;


/**
 *
 */
class model extends \yii\base\Model
{
    /** @var  string кошелек */
    public $wallet;

    /** @var  string секретный код */
    public $secret;

    public function rules()
    {
        return [
            ['wallet', 'required'],
            ['wallet', 'string'],

            ['secret', 'string'],
        ];
    }

    /**
     * @param string $config JSON
     */
    public function setData($config)
    {
        if (Application::isEmpty($config)) {
            $data = Json::decode('{"wallet":"","secret":""}');
        } else {
            $data = Json::decode($config);
        }
        $this->wallet = $data['wallet'];
        $this->secret = $data['secret'];
    }

    /**
     * @return string JSON
     */
    public function save()
    {
        $data = ['wallet' => $this->wallet, 'secret' => $this->secret];

        return Json::encode($data);
    }

    public function attributeLabels()
    {
        return [
            'wallet' => 'Идентификатор магазина',
            'secret' => 'Секретный ключ',
        ];
    }

}