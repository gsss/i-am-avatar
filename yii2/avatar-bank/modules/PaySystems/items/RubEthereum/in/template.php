<?php
/**
 * Created by PhpStorm.
 * User: Бог-Творец
 * Date: 10.04.2016
 * Time: 2:04
 */
/** @var \yii\web\View $this */
/** @var \common\models\BillingMain $billing */
/** @var string $destinationAddress */


use yii\helpers\ArrayHelper;
use yii\helpers\Html;

$config = \yii\helpers\Json::decode($destinationAddress);

/** @var \common\models\PaymentEthereum $payment */
$payment = \common\models\PaymentEthereum::find()->where(['billing_id' => $billing->id])->one();
if (is_null($payment)) {
    $eth = \common\models\avatar\Currency::convert($billing->sum_after / 100, 'RUB', 'ETH');
    $payment = new \common\models\PaymentEthereum([
        'value'      => $eth * pow(10, 18),
        'billing_id' => $billing->id,
        'address'    => $config['wallet'],
    ]);
    $payment->save();
} else {
    $eth = $payment->value / pow(10, 18);
}

?>
<p><?= \Yii::t('c18', 'Вам нужно перевести {sum} ETH. на счет: <b>{card}</b>', [
        'sum'  => \Yii::$app->formatter->asDecimal($eth, 18),
        'card' => $config['wallet'],
    ])
    ?></p>
<p>И нажмите "Подтвердить"</p>
<p>Мы подтвердим оплату и вы получите товар</p>
<p>
    <button class="btn btn-success btn-lg" id="buttonSuccess">Подтвердить</button>
</p>
