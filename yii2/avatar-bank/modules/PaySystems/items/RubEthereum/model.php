<?php

namespace avatar\modules\PaySystems\items\RubEthereum;

use cs\Application;
use cs\services\VarDumper;
use yii\base\Event;

use yii\helpers\ArrayHelper;
use yii\helpers\Json;
use yii\helpers\StringHelper;


/**
 *
 */
class model extends \yii\base\Model
{
    /** @var  string Идентификатор кошелька */
    public $wallet;

    public function rules()
    {
        return [
            ['wallet', 'required'],
            ['wallet', 'string'],
            ['wallet', 'validateWallet'],
        ];
    }

    public function validateWallet($attr, $p)
    {
        if (!$this->hasErrors()) {
            if (!StringHelper::startsWith($this->wallet, '0x')) {
                $this->addError($attr,'Адрес кошелька должен начинаться с 0x');
                return;
            }
            $this->wallet = strtolower($this->wallet);
        }
    }
    /**
     * @param string $config JSON
     */
    public function setData($config)
    {
        if (Application::isEmpty($config)) {
            $data = Json::decode('{"wallet":""}');
        } else {
            $data = Json::decode($config);
        }

        $this->wallet = $data['wallet'];
    }

    /**
     * @return string JSON
     */
    public function save()
    {
        $data = ['wallet' => $this->wallet];

        return Json::encode($data);
    }

    public function attributeLabels()
    {
        return [
            'wallet' => 'Идентификатор кошелька назначения',
        ];
    }

}