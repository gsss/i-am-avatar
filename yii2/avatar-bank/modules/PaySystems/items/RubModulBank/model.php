<?php

namespace avatar\modules\PaySystems\items\RubModulBank;

use cs\Application;
use cs\services\VarDumper;
use yii\base\Event;

use yii\helpers\ArrayHelper;
use yii\helpers\Json;


/**
 *
 */
class model extends \yii\base\Model
{
    /** @var  string кошелек */
    public $login;

    /** @var  string секретный код */
    public $password;

    public function rules()
    {
        return [
            ['login', 'required'],
            ['login', 'string'],
            ['password', 'string'],
        ];
    }

    /**
     * @param string $config JSON
     */
    public function setData($config)
    {
        if (Application::isEmpty($config)) {
            $data = Json::decode('{"login":"","password":""}');
        } else {
            $data = Json::decode($config);
        }
        $this->login = $data['login'];
        $this->password = $data['password'];
    }

    /**
     * @return string JSON
     */
    public function save()
    {
        $data = ['login' => $this->login, 'password' => $this->password];

        return Json::encode($data);
    }

    public function attributeLabels()
    {
        return [
            'login'     => 'Логин',
            'password'  => 'Пароль',
        ];
    }

}