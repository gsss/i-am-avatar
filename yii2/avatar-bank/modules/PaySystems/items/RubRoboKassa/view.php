<?php

/** $this \yii\web\View  */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \avatar\modules\PaySystems\items\RubRoboKassa\model */


use yii\helpers\Url;
use app\services\GsssHtml;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use app\services\GetArticle\Collection;

?>

<?= $form->field($model, 'wallet') ?>
<?= $form->field($model, 'secret')->widget('\avatar\widgets\SecretKey') ?>
