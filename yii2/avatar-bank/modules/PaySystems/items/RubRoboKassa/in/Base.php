<?php
/**
 * Created by PhpStorm.
 * User: Бог-Творец
 * Date: 10.04.2016
 * Time: 2:04
 */

namespace avatar\modules\PaySystems\items\RubRoboKassa\in;

use app\models\Piramida\InRequest;
use app\models\Shop\Request;
use app\models\Shop\RequestTransaction;
use app\modules\Shop\services\Basket;
use common\models\BillingMain;
use common\models\PaySystem;
use common\models\piramida\YandexTransaction;
use cs\Application;
use Yii;
use app\models\Shop\Payments;
use cs\web\Exception;

use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Json;
use yii\helpers\StringHelper;
use yii\helpers\VarDumper;
use yii\rbac\Role;
use YandexCheckout\Client;
use YandexCheckout\Model\NotificationEventType;

class Base extends \common\models\piramida\WalletSource\Base implements \common\models\piramida\WalletSourceInterface
{
    /** @var  array транзакция от платежной системы */
    public $transaction;

    /** @var  double */
    public $sumAfter;

    /** @var  double */
    public $sumBefore;

    public function getTransactionInfo()
    {
        return $this->transaction;
    }

    /**
     * Выдает цену с учетом оплаты комиссии
     *
     * @param float $price цена которую хотим получить от клиента через прием денег через эту платежную систему
     *
     * @return float цена для оплаты в платежной соистеме
     */
    public function getPriceWithTax($price)
    {
        $d = $price / (1 - ($this->getTax() / 100));
        $d = explode('.', $d);
        if (count($d) > 1) {
            return (float)$d[0] . '.' . substr($d[1], 0, 2);
        }
        return (float)$d[0];
    }

    public function getTax()
    {
        return 2;
    }

    public function getSumBefore()
    {
        return $this->sumBefore;
    }

    public function getSumAfter()
    {
        return $this->sumAfter;
    }

    /**
     * Формирует форму
     *
     * @param BillingMain   $billing
     * @param string        $description       - Наименование заказа
     * @param string        $destinationName   - наименование поучателя
     * @param null | array  $options
     * - form - array - параметры(аттрибуты) тега формы
     * - values - array - доп параметры для формы как скрытые поля (ключ => значение)
     * - action - string - поле label в форме
     * - successURL - string - URL-адрес для редиректа после совершения перевода.
     *
     * @return string
     */
    public function getForm($billing, $description, $destinationName, $options = null)
    {
        /** @var \common\models\PaymentRoboKassa $payment */
        $payment1 = \common\models\PaymentRoboKassa::find()->where(['billing_id' => $billing->id])->one();
        if (is_null($payment1)) {
            $payment1 = \common\models\PaymentRoboKassa::add([
                'amount'      => $billing->sum_after,
                'billing_id'  => $billing->id,
                'currency_id' => 6,
            ]);
        }

        return \Yii::$app->view->renderFile('@avatar/modules/PaySystems/items/RubRoboKassa/in/template.php', [
            'billing'            => $billing,
            'description'        => $description,
            'destinationName'    => $destinationName,
            'destinationAddress' => $this->config,
            'payment'            => $payment1,
        ]);
    }

    /**
     * Прием денег из яндекса
     *
     * @params array $actions
     *
     * @return string
     */
    public function success($actions)
    {
        if (Yii::$app->request->get('type') != 'robokassa') {
            return false;
        }

        return true;
    }

}