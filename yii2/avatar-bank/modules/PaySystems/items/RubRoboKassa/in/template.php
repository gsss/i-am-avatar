<?php
/**
 * Created by PhpStorm.
 * User: Бог-Творец
 * Date: 10.04.2016
 * Time: 2:04
 */
/** @var \yii\web\View $this */
/** @var \common\models\BillingMain $billing */
/** @var string $destinationAddress JSON */
/** @var \common\models\PaymentRoboKassa $payment */
/** @var string $description */
/** @var string $descriptionName */

use YandexCheckout\Client;

use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Json;


$config = Json::decode($destinationAddress);

$this->registerJs(<<<JS
$('#formPay').submit();
JS
);

// регистрационная информация (Идентификатор магазина, пароль #1)
// registration info (Merchant ID, password #1)
$mrh_login = $config['wallet'];
$mrh_pass1 = $config['password'];

// номер заказа
// number of order
$inv_id = $billing->id;

// описание заказа
// order description
$inv_desc = $description;

// сумма заказа
// sum of order
$out_summ = $billing->sum_after / 100;

// предлагаемая валюта платежа
// default payment e-currency
$in_curr = "BANKOCEAN2R";

// язык
// language
$culture = "ru";

// кодировка
// encoding
$encoding = "utf-8";

// Адрес электронной почты покупателя
// E-mail
$user = Yii::$app->user->identity;
$Email = $user->email;

// Срок действия счёта
// Expiration Date
$date = new DateTime();
$date->add(new DateInterval('P1D'));
$ExpirationDate = $date->format('Y-m-dTH:i');

// Валюта счёта
// OutSum Currency
$OutSumCurrency = "RUB";

// формирование подписи
// generate signature
$crc  = md5("$mrh_login:$out_summ:$inv_id:$OutSumCurrency:$mrh_pass1");

$params = [
    'MerchantLogin'  => $mrh_login,
    'OutSum'         => $out_summ,
    'InvId'          => $inv_id,
    'Description'    => $inv_desc,
    'SignatureValue' => $crc,
    'IncCurrLabel'   => $in_curr,
    'Culture'        => $culture,
    'Email'          => $Email,
    'ExpirationDate' => $ExpirationDate,
    'OutSumCurrency' => $OutSumCurrency,
];

$action = 'https://auth.robokassa.ru/Merchant/Index.aspx';

$rows = [];
foreach ($params as $k => $v) {
    $rows[] = Html::hiddenInput($k, $v);
}


?>
<?= Html::tag('form', join('', $rows), ['action' => $action, 'method' => 'post', 'id' => 'formPay']); ?>