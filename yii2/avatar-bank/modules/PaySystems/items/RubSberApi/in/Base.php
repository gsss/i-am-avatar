<?php
/**
 * Created by PhpStorm.
 * User: Бог-Творец
 * Date: 10.04.2016
 * Time: 2:04
 */

namespace avatar\modules\PaySystems\items\RubSberApi\in;

use app\models\Shop\Request;
use app\models\Shop\RequestTransaction;
use common\models\BillingMain;
use common\models\BillingMainClass;
use common\models\PaySystem;
use common\models\PaySystemConfig;
use common\models\school\School;
use common\services\SberKassa;
use cs\Application;
use Yii;
use app\models\Shop\Payments;
use cs\web\Exception;

use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Json;
use yii\helpers\StringHelper;
use yii\helpers\VarDumper;
use yii\rbac\Role;

class Base extends \common\models\piramida\WalletSource\Base implements \common\models\piramida\WalletSourceInterface
{
    public $url = 'https://securepayments.sberbank.ru/';

    public $url2 = 'https://3dsec.sberbank.ru/payment/rest/register.do?
    amount=100&
    orderNumber=87654321&
    password=password&
    returnUrl=https://3dsec.sberbank.ru/payment/finish.html&
    userName=userName&
    jsonParams={"orderNumber":1234567890}&
    pageView=DESKTOP&
    expirationDate=2014-09-08T14:14:14&
    merchantLogin=merch_child&
    features=AUTO_PAYMENT';

    /** @var  mixed транзакция от платежной системы */
    public $transaction;

    /** @var  string тип операци от яндекса 'PC' 'AC' */
    public $paymentType;

    /** @var  double */
    public $sumAfter;

    /** @var  double */
    public $sumBefore;

    public function getTransactionInfo()
    {
        return $this->transaction;
    }

    public function getTypeId() {}

    /**
     * Выдает цену с учетом оплаты комиссии
     *
     * @param float $price цена которую хотим получить от клиента через прием денег через эту платежную систему
     *
     * @return float цена для оплаты в платежной соистеме
     */
    public function getPriceWithTax($price)
    {
        $d = $price / (1 - ($this->getTax() / 100));
        $d = explode('.', $d);
        if (count($d) > 1) {
            return (float)$d[0] . '.' . substr($d[1], 0, 2);
        }
        return (float)$d[0];
    }

    public function getTax()
    {
        return 0;
    }

    public function getSumBefore()
    {
        return $this->sumBefore;
    }

    public function getSumAfter()
    {
        return $this->sumAfter;
    }

    /**
     * Формирует форму
     *
     * @param BillingMain $billing
     * @param string $description
     * @param string $destinationAddress - счет поучателя
     *
     * @param null | array $options
     * - form - array - параметры(аттрибуты) тега формы
     * - values - array - доп параметры для формы как скрытые поля (ключ => значение)
     * - action - string - поле label в форме
     *
     * @return string
     */
    public function getForm($billing, $description, $destinationName, $options = null)
    {
        $config  = Json::decode($this->config);

        $client = new SberKassa([
            'operator' => $config['operator'],
            'api'      => $config['api'],
            'password' => $config['password'],
            'secret'   => $config['secret'],
        ]);

        /** @var \common\models\PaymentYandexKassa $payment */
        $payment1 = \common\models\PaymentSberKassa::find()->where(['billing_id' => $billing->id])->one();

        if (is_null($payment1)) {
            $Payment = $client->createPayment($billing, $description, $destinationName, ['successUrl' => $billing->successUrl]);
            Yii::info($Payment, 'avatar\modules\PaySystems\items\RubSberApi\in\Base::getForm1');

            $payment1 = \common\models\PaymentSberKassa::add([
                'amount'      => $billing->sum_after,
                'billing_id'  => $billing->id,
                'payment_id'  => $Payment['orderId'],
                'form_url'    => $Payment['formUrl'],
                'currency_id' => 6,
            ]);
            Yii::info($payment1, 'avatar\modules\PaySystems\items\RubSberApi\in\Base::getForm2');
        } else {
//            $Payment = $client->getPaymentInfo($payment1->payment_id);
        }

        return \Yii::$app->view->renderFile('@avatar/modules/PaySystems/items/RubSberApi/in/template.php', [
            'billing'            => $billing,
            'destinationAddress' => $this->config,
            'payment1'           => $payment1,
        ]);
    }

    /**
     * GET
     * [
     * 'type'           => 'sberkassa'
     * 'orderNumber'    => '227'
     * 'mdOrder'        => 'ce0ce261-d4c2-7c5c-ae19-cb4504b09de5'
     * 'operation'      => 'deposited'
     * 'status'         => '1'
     * ]
     */
    public function success($a)
    {
        Yii::info(Yii::$app->request->get(), 'avatar\modules\PaySystems\items\RubSberApi\in\Base::success');
        $billing_id = Yii::$app->request->get('orderNumber');
        $operation = Yii::$app->request->get('operation');
        $status = Yii::$app->request->get('status');

        if ($operation == 'deposited' and $status == '1') {

            // Проверяю контрольную сумму
            $b = BillingMain::findOne($billing_id);
            $configObject = PaySystemConfig::findOne($b->config_id);
            $config = Json::decode($configObject->config);
            $client = new SberKassa($config);

            $params = Yii::$app->request->get();
            $checksum = $params['checksum'];
            unset($params['checksum']);
            if (!$client->isCheckSum($params, $checksum)) {
                throw new \Exception('Не верная контрольная сумма'.VarDumper::dumpAsString([$params, $checksum]));
                return;
            }

            // одобряю счет
            $bill = BillingMain::findOne($billing_id);
            $bill->successClient();
            Yii::info($bill, 'avatar\modules\PaySystems\items\RubSberApi\in\Base::success2');

            $bill->successShop();
            Yii::info('finish', 'avatar\modules\PaySystems\items\RubSberApi\in\Base::success3');
        }
    }

    /**
     * @param int $billing_id
     * @return \common\services\SberKassa
     * @throws
     */
    public function getProvider($billing_id)
    {
        $bill = BillingMain::findOne($billing_id);
        $class_id = $bill->class_id;
        $o = BillingMainClass::findOne($class_id);
        $class = $o->name;
        /** @var \common\interfaceList\BillingInterface $object */
        $object = new $class;
        $company_id = $object->getCompanyId();
        /** @var \common\models\PaySystemConfig $configObject */
        $configObject = PaySystemConfig::findOne(['school_id' => $company_id, 'paysystem_id' => $bill->source_id]);
        if (is_null($configObject)) {
            throw new \Exception('Нет такой ПС');
        }
        $config = Json::decode($configObject->config);

        $client = new SberKassa($config);

        return $client;
    }

}