<?php
/**
 * Created by PhpStorm.
 * User: Бог-Творец
 * Date: 10.04.2016
 * Time: 2:04
 */

namespace avatar\modules\PaySystems\items\RubSroutesSaleBall\in;

use app\models\Piramida\InRequest;
use app\models\Shop\Request;
use app\models\Shop\RequestTransaction;
use app\modules\Shop\services\Basket;
use common\models\BillingMain;
use cs\Application;
use Yii;
use app\models\Shop\Payments;
use cs\web\Exception;

use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\StringHelper;
use yii\rbac\Role;

class Class1 extends \common\models\piramida\WalletSource\Base implements \common\models\piramida\WalletSourceInterface
{
    /** @var  mixed транзакция от платежной системы */
    public $transaction;

    /** @var  double */
    public $sumAfter;

    /** @var  double */
    public $sumBefore;

    public function getTransactionInfo()
    {
        return $this->transaction;
    }

    public function getTypeId() {}
    public function success($actions) {}

    /**
     * Выдает цену с учетом оплаты комиссии
     *
     * @param float $price цена которую хотим получить от клиента через прием денег через эту платежную систему
     *
     * @return float цена для оплаты в платежной соистеме
     */
    public function getPriceWithTax($price)
    {
        $d = $price / (1 - ($this->getTax() / 100));
        $d = explode('.', $d);
        if (count($d) > 1) {
            return (float)$d[0] . '.' . substr($d[1], 0, 2);
        }
        return (float)$d[0];
    }

    public function getTax()
    {
        return 0;
    }

    public function getSumBefore()
    {
        return $this->sumBefore;
    }

    public function getSumAfter()
    {
        return $this->sumAfter;
    }

    /**
     * Формирует форму
     *
     * @param BillingMain $billing
     * @param string $description
     * @param string $destinationAddress - счет поучателя
     *
     * @param null | array $options
     * - form - array - параметры(аттрибуты) тега формы
     * - values - array - доп параметры для формы как скрытые поля (ключ => значение)
     * - action - string - поле label в форме
     *
     * @return string
     */
    public function getForm($billing, $description, $destinationName, $options = null)
    {
        return \Yii::$app->view->renderFile('@avatar/modules/PaySystems/items/RubSroutesSaleBall/in/template.php', [
            'billing'            => $billing,
            'destinationAddress' => $this->config,
        ]);
    }
}