<?php

namespace avatar\modules\Binance;

use cs\services\VarDumper;
use yii\base\Component;
use yii\base\Exception;

use yii\helpers\Json;
use yii\helpers\StringHelper;
use Yii;
use yii\httpclient\Client;


/**
 * https://github.com/binance-exchange/binance-official-api-docs/blob/master/rest-api.md
 */
class Binance extends Object
{
    /** @var  string слеша в конце не должно быть */
    public $apiUrl = 'https://api.binance.com';

    /** @var  string */
    public $apiKey;

    /** @var  string */
    public $apiSecret;

    /** @var int An additional parameter, recvWindow, may be sent to specific the number of milliseconds after timestamp the request is valid for. If recvWindow is not sent, it defaults to 5000. */
    public $recvWindow;

    public static function initialize($apiKey, $apiSecret)
    {
        $t = new self([
            'apiKey'    => $apiKey,
            'apiSecret' => $apiSecret,
        ]);

        return $t;
    }

    /**
     * Вызывает функции не подписанные и без APIKEY
     *
     * @param string                    $path
     * @param array                     $data
     *
     * @return mixed
     * @throws
     */
    public function _call($path, $data = [])
    {
        $request = $this->_createRequest($path, $data);

        return $this->_execute($request, $path, $data);
    }

    /**
     * Вызывает подписанные функции
     *
     * @param \yii\httpclient\Request   $request
     * @param string                    $path
     * @param array                     $data
     *
     * @return mixed
     * @throws
     */
    public function _execute($request, $path, $data)
    {
        $request->setData($data);
        Yii::info(\yii\helpers\VarDumper::dumpAsString([$request, $path, $data]), 'avatar\modules\Binance\Binance::_call::before');
        $response = $request->send();
        Yii::info(\yii\helpers\VarDumper::dumpAsString($response), 'avatar\modules\Binance\Binance::_call::after');
        if ($response->headers['http-code'] != 200) {
            throw new \Exception('response of ' . $this->apiUrl . ' = ' . $response->headers['http-code'] . ' data = ' . $response->content);
        }
        $responseData = $response->getData();
        if (isset($responseData['error'])) {
            throw new \Exception($response['msg']);
        }

        return $responseData;
    }

    /**
     * Вызывает функции с передачей API ключей
     *
     * @param string $path
     * @param array $data
     *
     * @return mixed
     * @throws
     */
    public function _callApiKey($path, $data)
    {
        $request = $this->_createRequestApiKey($path, $data);

        return $this->_execute($request, $path, $data);
    }

    /**
     * Возвращает информацию о пользователе
     *
     * @return array
     * [
     *  [
     *      'asset' => "BTC", // код валюты по классификатору Binance
     *      'free' => "4723846.89208129",
     *      'locked' => "0.00000000"
     *  ]
     * ]
     */
    public function account()
    {
        return $this->_callApiKeyAndSigned('api/v3/account');
    }

    /**
     * Вызывает функции с передачей API ключей и подписанные
     *
     * @param string $path
     * @param array  $data
     *
     * @return mixed
     * @throws
     */
    public function _callApiKeyAndSigned($path, $data = [])
    {
        $request = $this->_createRequestApiKey($path, $data);

        // устанавливаю параметры времени
        $timestamp = (int)(microtime(true) * 1000);
        $data['timestamp'] = $timestamp;
        if ($this->recvWindow) {
            $data['recvWindow'] = $this->recvWindow;
        }

        // устанавливаю подпись
        $data['signature'] = $this->getSignature($data);

        return $this->_execute($request, $path, $data);
    }

    /**
     * Вычисляет подпись, значения кодируются через `urlencode`
     * @param array $data
     * @return string
     */
    private function getSignature($data)
    {
        $strings = [];
        foreach ($data as $key => $value) {
            $strings[] = $key . '=' . urlencode($value);
        }
        $stringData = join('&', $strings);
        $hmac = hash_hmac( 'sha256', $stringData, $this->apiSecret);

        return $hmac;
    }

    /**
     * @param string $path
     * @param array $data
     * @return \yii\httpclient\Request
     */
    public function _createRequest($path, $data = [])
    {
        $client = new Client([
            'baseUrl'        => $this->apiUrl,
            'responseConfig' => [
                'format' => Client::FORMAT_JSON,
            ],
        ]);

        return $client->get($path, $data);
    }

    /**
     * @param string $path
     * @param array $data
     * @return \yii\httpclient\Request
     */
    public function _createRequestApiKey($path, $data = [])
    {
        $request = $this->_createRequest($path, $data);
        $request->addHeaders(['X-MBX-APIKEY' => $this->apiKey]);

        return $request;
    }

}