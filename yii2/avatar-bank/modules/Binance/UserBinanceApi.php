<?php

namespace avatar\modules\Binance;

use common\models\UserAvatar;
use cs\services\VarDumper;
use yii\base\Component;
use yii\base\Exception;

use yii\helpers\Json;
use yii\helpers\StringHelper;
use Yii;
use yii\httpclient\Client;


/**
 * https://github.com/binance-exchange/binance-official-api-docs/blob/master/rest-api.md
 */
class UserBinanceApi extends \avatar\modules\Binance\Binance
{
    /** @var  UserAvatar */
    public $_user;

}