<?php

namespace avatar\modules\Share;

use cs\services\Security;
use cs\services\UploadFolderDispatcher;
use cs\services\VarDumper;
use yii\base\Component;
use yii\base\Exception;

use yii\helpers\ArrayHelper;
use yii\helpers\FileHelper;
use yii\helpers\Json;
use yii\helpers\StringHelper;
use Yii;
use yii\httpclient\Client;

/**
 */
class Share
{

    /**
     * @param string $text
     * @param string $file __FILE__
     * @param array $options
     *  - background - файл для обложки. Полный путь на сервере
     *  - start - array - точка старта текста
     *  - size - int - Высота шрифта
     *  - color - string - цвет 3/6 символов в шестнадцатеричных значениях
     *
     * @return \cs\services\SitePath
     * Вызывайте ->getPath() для получения относительного пути
     *
     */
    public static function image($text, $filePhp, $options = [])
    {
        if (isset($options['background'])) {
            $file = $options['background'];
        } else {
            $s = '/images/share/template.png';
            $file = Yii::getAlias('@avatar/../public_html' . $s);
        }
        $hash = sha1($filePhp . $text . serialize($options));
        $folder = UploadFolderDispatcher::createFolder('share');
        $fileObject = $folder->add($hash . '.png');
        if (!$fileObject->exist()) {
            $start = ArrayHelper::getValue($options, 'start', [634, 147]);
            $size = ArrayHelper::getValue($options, 'size', 54);
            $color = ArrayHelper::getValue($options, 'color', 'fff');
            $res = self::text($file, [
                [
                    'text'        => $text,
                    'start'       => $start,
                    'fontOptions' => ['color' => $color, 'size' => $size],
                    'fontFile'    => Yii::getAlias('@avatar/../public_html' . '/images/share/Sansation/Sansation_Regular.ttf'),
                ],
            ]);
            copy($res['path'], $fileObject->getPathFull());
        }

        return $fileObject;
    }

    /**
     * @param string $file полный путь к файлу
     * @param array $watermark
     *                     [[
     *                     'text'        => string
     *                     'start'       => array [x, y] - точка от верхнего левого угла оригинальной картинки
     *                     'fontOptions' => string
     *                     'fontFile'    => string
     *                     ],...]
     *
     * @return array
     * [
     *      'img'  => \Imagine\Image\ImageInterface
     *      'path' => string - полный путь к файлу png
     * ]
     *
     * @throws \yii\base\Exception
     */
    private static function text($file, $watermark)
    {
        if (count($watermark) == 0) {
            throw new Exception('Не задан массив $watermark');
        }
        $tempFilePath = Yii::getAlias('@runtime/watermark');
        FileHelper::createDirectory($tempFilePath);
        $background = $file;
        $image = null;
        foreach ($watermark as $watermarkConfig) {
            if (!isset($watermarkConfig['fontOptions'])) {
                $watermarkConfig['fontOptions'] = [];
            }
            if (!isset($watermarkConfig['fontOptions'])) {
                $watermarkConfig['start'] = [0, 0];
            }

            $image = \yii\imagine\Image::text($background, $watermarkConfig['text'], $watermarkConfig['fontFile'], $watermarkConfig['start'], $watermarkConfig['fontOptions']);
            $background = $tempFilePath . DIRECTORY_SEPARATOR . time() . '_' . Security::generateRandomString(10) . '.' . 'png';
            file_put_contents($background, $image->get('png'));
        }

        return [
            'img'  => $image,
            'path' => $background,
        ];
    }

}