<?php

namespace avatar\modules\ETH;

use cs\services\VarDumper;
use yii\base\Component;
use yii\base\Exception;

use yii\helpers\Json;
use yii\helpers\StringHelper;
use Yii;
use yii\httpclient\Client;

/**
 * https://info.shapeshift.io/api
 */
class ShapeShiftApi extends Component
{
    public $apiUrl = 'https://shapeshift.io';
    public $PublicKey = 'bd735926edf28edf74ea090f7b15b161f5a88ffbc11578425535517c0471ca6350ec115e49dd22a0b6203e8477517bbd481b07c0ac8ae6b4240b83e1f50fc566';
    public $PrivateKey = 'dec1c62c807fb51e182e001e5ecf1cfd97dce84262eaef7e34d8ecd48f28a84f6abd18b15ae6686143b3b5e70f9924279ac8ecd0162558716e203e7e0979fdbf';

    /**
     * Gets the current rate offered by Shapeshift. This is an estimate because the rate can occasionally change
     * rapidly depending on the markets. The rate is also a 'use-able' rate not a direct market rate. Meaning
     * multiplying your input coin amount times the rate should give you a close approximation of what will be sent
     * out. This rate does not include the transaction (miner) fee taken off every transaction.
     *
     * @param $pair
     *
     * @return string JSON
     * {
     * "pair" : "btc_ltc",
     * "rate" : "70.1234"
     * }
     */
    public function rate($pair)
    {
        return $this->_call('rate' . '/'  . $pair);
    }

    /**
     * Вызывает функцию
     *
     * @param string $path
     * @param array  $params
     *
     * @return \yii\httpclient\Response
     */
    protected function _post($path, $params = [])
    {
        $client = new Client(['baseUrl' => $this->apiUrl]);

        return $client->post($path, $params)->send();
    }

    /**
     * Вызывает функцию, если произошла ошибка то вызывается исключение
     *
     * @param      $path
     * @param null $params
     *
     * @return mixed содержимое параметра `result` уже раскодированное из JSON
     * @throws
     */
    protected function _call($path, $params = null)
    {
        Yii::info(\yii\helpers\VarDumper::dumpAsString([$path, $params]), 'avatar\modules\ETH\ShapeShiftApi::_call');
        $response = $this->_post($path, $params);
        Yii::info(\yii\helpers\VarDumper::dumpAsString($response), 'avatar\modules\ETH\ShapeShiftApi::_call');
        try {
            $data = Json::decode($response->content);
        } catch (\Exception $e) {
            throw new Exception($e->getMessage());
        }

        if (isset($data['error'])) {
            throw new Exception($data['error']);
        }

        return $data;
    }
}