<?php

namespace avatar\modules\ETH;

use cs\services\VarDumper;
use yii\base\Component;
use yii\base\Exception;

use yii\helpers\Json;
use yii\helpers\StringHelper;
use Yii;
use yii\httpclient\Client;


/**
 * Created by PhpStorm.
 * User: s.arhangelskiy
 * Date: 02.06.2017
 * Time: 17:21
 *
 * @see https://www.blockcypher.com/dev/ethereum/
 */
class ServiceBlockCypher extends Component
{
    public $apiUrl = 'https://api.blockcypher.com/v1';
    public $token = 'aa654f89cb524bebb7078b58fd9447b5';

    public function _call($path, $data)
    {
        $url = $this->apiUrl . '/' . $path . '?' . 'token' . '=' . $this->token;
        $client = new Client(['baseUrl' => $url]);
        $response = $client->post('', $data)->send();

        return $response;
    }
}