<?php

namespace avatar\modules\ETH;

use cs\services\VarDumper;
use yii\base\Component;
use yii\base\Exception;

use yii\helpers\Json;
use yii\helpers\StringHelper;
use Yii;
use yii\httpclient\Client;


/**
 * Class ServiceInfura
 *
 * https://infura.io/docs/
 *
 * @package avatar\modules\ETH
 */
class ServiceInfura extends Component
{
    public $apiUrl = 'https://mainnet.infura.io';

    public $apiUrl2 = [
        'mainnet'   => 'https://mainnet.infura.io',
        'ropsten'   => 'https://ropsten.infura.io',
        'infuranet' => 'https://infuranet.infura.io',
        'kovan'     => 'https://kovan.infura.io',
        'rinkeby'   => 'https://rinkeby.infura.io',
    ];

    public $token = 'ipxAZB4wjrMXEr2UazuA';

    public function init()
    {
        if (!YII_ENV_PROD) {
            $this->apiUrl = $this->apiUrl2['ropsten'];
        }
    }

    /**
     * @param $path
     * @param string $data
     *
     * @return mixed
     *
     * @throws
     */
    public function _call($path, $data)
    {
        $client = new Client(['baseUrl' => $this->apiUrl]);
        $response = $client->post($path, $data, ['Content-Type' => 'application/json'])->send();
        if ($response->headers['http-code'] != 200) {
            throw new \Exception('response of ' . $this->apiUrl . ' = ' . $response->headers['http-code']);
        }
        try {
            $data = Json::decode($response->content);
        } catch (\Exception $e) {
            throw new \Exception('server ' . $this->apiUrl . ' response is not valid JSON');
        }

        return $data['result'];
    }

    /**
     *
     * @param string $name
     * @param array $params
     *
     * @return array | string
     */
    public function _method($name, $params = [])
    {
        $params = [
            'jsonrpc' => '2.0',
            'method'  => $name,
            'params'  => $params,
            'id'      => 1,
        ];
        $data = $this->_call('', Json::encode($params));

        return $data;
    }


    /**
     * Возвращает последний блок
     *
     * @return int
     */
    public function blockNumber()
    {
        $value = $this->_method('eth_blockNumber');

        return hexdec($value);
    }

    /**
     * Возвращает список транзакций
     *
     * @param string | int  $height     dec value of a block number, or the string "earliest", "latest" or "pending".
     * @param bool          $full_list  TRUE = full transaction objects, FALSE = only hashes of trasactions.
     *
     * @return array
     * [
         * 'difficulty' => '0x217adb45'
         * 'extraData' => '0xd783010702846765746885676f312e398777696e646f7773'
         * 'gasLimit' => '0x56c12f'
         * 'gasUsed' => '0x6886e'
         * 'hash' => '0xc1987878454f83cc7a8889edc79076074543116a350e192a1c8815dd095c3e9                                                                                                                                                             7'
         * 'logsBloom' => '0x0000000000000000000000000000000000000000000000000000000000                                                                                                                                                             00000000000000000000000000000000000000000000000000000100000000000000000000000000                                                                                                                                                             00000000000000800000000000000000000000000000000000200000000020000000000000000000                                                                                                                                                             00000000000000000000000000000000000000000000000000000000000000000000000000800000                                                                                                                                                             00000000000000000000000000000000000000000000000000000000000000000000000000000000                                                                                                                                                             00000000000000000000000000000000000000000000000000000000000000000000000000000000                                                                                                                                                             000000000000000000000000000000000000000000000000000020'
         * 'miner' => '0x4badd354e0edda5ebdc7ca00e084be02449db03f'
         * 'mixHash' => '0xb66edec68951cc1bcb4d7934e7ccf2a76c45f5e2e54903f61fe7e1cb1cff                                                                                                                                                             a9e2'
         * 'nonce' => '0x6e22fec401bc8416'
         * 'number' => '0x2932e0'
         * 'parentHash' => '0xd30a0acfa7ec7253cd897e4a3dfe1a3d8ccf4f63c6eee3a7238acae15                                                                                                                                                             9b9e657'
         * 'receiptsRoot' => '0x644555fc5936a6146751910493292251b842947e98f9baae52d3d52                                                                                                                                                             335d9766d'
         * 'sha3Uncles' => '0x6ae283e461683edf87414e4c865a12361ce793307cd8ddca9844d286b                                                                                                                                                             0f64080'
         * 'size' => '0xae7'
         * 'stateRoot' => '0x4fa12ec84532af1678fe83367d2a375705f238e5fecb0dd813b8da0095                                                                                                                                                             e49779'
         * 'timestamp' => '0x5a8e3465'
         * 'totalDifficulty' => '0x1bb1ebf32baf69'
         * 'transactions' => [
             * 0 => [
                 * 'blockHash' => '0xc1987878454f83cc7a8889edc79076074543116a350e192a1c                                                                                                                                                             8815dd095c3e97'
                 * 'blockNumber' => '0x2932e0'
                 * 'from' => '0x81b7e08f65bdf5648606c89998a9cc8164397647'
                 * 'gas' => '0x5208'
                 * 'gasPrice' => '0x3b9aca00'
                 * 'hash' => '0xf0014b375a596ef1a976bca364ecb070cce3f097a12cee08e148a2b                                                                                                                                                             8caad6226'
                 * 'input' => '0x'
                 * 'nonce' => '0x1f4852'
                 * 'to' => '0xddab7510d05cf18f31625eff57d499070f65239e'
                 * 'transactionIndex' => '0x0'
                 * 'value' => '0xde0b6b3a7640000'
                 * 'v' => '0x1b'
                 * 'r' => '0xce2035ac6d94e55a6aebc18c4d8cb13622a9c46e29777f450fd0d1ca5c                                                                                                                                                             367c12'
                 * 's' => '0x51c3312be6f9594b636739dc0f8b87b553552d97447b90c741d24cbeed                                                                                                                                                             4e4b11'
             * ]
         * ]
         * 'transactionsRoot' => '0x1b26025b1175bbda3b0cea5b3f3ad881a56c83bb47140484b50                                                                                                                                                             59ca24e15eabb'
         * 'uncles' => [
            * 0 => '0xe225fdbec68b98ba23a21a5ad25196de1bf75401ae3dc1a59a739cdee58dc3b3                                                                                                                                                             '
         * ]
     * ]
     */
    public function getBlockByNumber($height, $full_list = true)
    {
        return $this->_method('eth_getBlockByNumber', [
            '0x' . dechex ($height),
            $full_list
        ]);
    }


}