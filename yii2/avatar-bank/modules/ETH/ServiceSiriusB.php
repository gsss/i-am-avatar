<?php

namespace avatar\modules\ETH;

use cs\services\VarDumper;
use yii\base\Component;
use yii\base\Exception;

use yii\helpers\Json;
use yii\helpers\StringHelper;
use Yii;
use yii\httpclient\Client;


/**
 *
 * @package avatar\modules\ETH
 */
class ServiceSiriusB extends Component
{
    public $apiUrl = 'https://ra-php.servers.atlantida.io';

    public $token = 'ipxAZB4wjrMXEr2UazuA';

    public function init()
    {
        if (!YII_ENV_PROD) {
            $this->apiUrl = 'https://eth-test-php.servers.atlantida.io';
        }
    }

    /**
     * @param string    $path
     * @param array     $data
     *
     * @return array
     * @throws
     */
    public function _call($path, $data, $method = 'post')
    {
        $client = new Client(['baseUrl' => $this->apiUrl]);
        if ($method == 'post') {
            $response = $client->post($path, $data)->send();
        } else {
            $response = $client->get($path, $data)->send();
        }
        if ($response->headers['http-code'] != 200) {
            throw new \Exception(\yii\helpers\VarDumper::dumpAsString(['response of ' . $this->apiUrl . ' = ' . $response->headers['http-code'], $response]));
        }
        try {
            $data = Json::decode($response->content);
        } catch (\Exception $e) {
            throw new \Exception('server ' . $this->apiUrl . ' response is not valid JSON');
        }

        return $data;
    }

    /**
     * @param string$path
     * @param array $data
     *
     * @return array | string
     * @throws
     */
    public function call($path, $params, $method = 'post')
    {
        $data = $this->_call($path, $params, $method);
        if (!$data['success']) {
            throw new \Exception(\yii\helpers\VarDumper::dumpAsString($data['data']));
        }

        return $data['data'];
    }
}