<?php

namespace avatar\modules\ETH;

use cs\services\VarDumper;
use yii\base\Component;
use yii\base\Exception;

use yii\helpers\Json;
use yii\helpers\StringHelper;
use Yii;
use yii\httpclient\Client;


/**
 * Class ServiceInfura
 *
 * https://infura.io/docs/
 *
 * @package avatar\modules\ETH
 */
class ServiceEthPlorer extends Component
{
    public $apiUrl = 'https://api.ethplorer.io';

    public $token = 'freekey';

    /**
     * @param        $address
     * @param        $token
     * @param string $type
     * @param null | int   $limit
     * @param int    $page
     *
     * @return mixed
     */
    public function getAddressHistory($address, $token, $type = '', $limit = null, $page = 1)
    {
        $path = 'getAddressHistory/' . $address;
        $params = [
            'token'  => $token,
        ];
        if ($type != '') {
            $params['type'] = $type;
        }
        if (!is_null($limit)) {
            $params['limit'] = $limit;
        }
        if ($page != 1) {
            $params['page'] = $page;
        }
        $data = $this->get($path, $params);

        return $data['operations'];
    }

    /**
     * @param $path
     * @param $params
     *
     * @return mixed
     * @throws \Exception
     */
    public function get($path, $params = [])
    {
        $params['apiKey'] = $this->token;
        $client = new Client(['baseUrl' => $this->apiUrl]);
        $response = $client->get($path, $params)->send();
        $data = Json::decode($response->content);
        if (isset($data['error'])) {
            throw new \Exception($data['error']['message']);
        }

        return $data;
    }
}