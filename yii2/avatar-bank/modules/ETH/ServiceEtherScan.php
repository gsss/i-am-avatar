<?php

namespace avatar\modules\ETH;

use cs\services\VarDumper;
use yii\base\Component;
use yii\base\Exception;

use yii\helpers\Json;
use yii\helpers\StringHelper;
use Yii;
use yii\httpclient\Client;

/**
 * Created by PhpStorm.
 * User: s.arhangelskiy
 * Date: 02.06.2017
 * Time: 17:21
 */
class ServiceEtherScan extends Component
{
    public $apiUrl = 'https://api.etherscan.io/api';
    public $url = 'https://etherscan.io';

    public $apiKey = 'KSEDTX591Z9429INNM9BFKE2ZIY9BI7YF1';

    public function init()
    {
        if (!YII_ENV_PROD) {
            $this->apiUrl = 'https://ropsten.etherscan.io/api';
            $this->url = 'https://ropsten.etherscan.io';
        }
    }

    /**
     * формирует адрес из того который от корня в полный в зависимости от окружения
     *
     * @param string $url
     *
     * @return string
     */
    public function getUrl($url = '')
    {
        return $this->url . $url;
    }

    /**
     * @param string $address
     * @param int   $page
     * @param int   $perPage
     *
     * @return array
     * {
     * "blockNumber": "65204",
     * "timeStamp": "1439232889",
     * "hash": "0x98beb27135aa0a25650557005ad962919d6a278c4b3dde7f4f6a3a1e65aa746c",
     * "nonce": "0",
     * "blockHash": "0x373d339e45a701447367d7b9c7cef84aab79c2b2714271b908cda0ab3ad0849b",
     * "transactionIndex": "0",
     * "from": "0x3fb1cd2cd96c6d5c0b5eb3322d807b34482481d4",
     * "to": "0xde0b295669a9fd93d5f28d9ec85e40f4cb697bae",
     * "value": "0",
     * "gas": "122261",
     * "gasPrice": "50000000000",
     * "isError": "0",
     * "input": "0xf00d4b5d000000000000000000000000036c8cecce8d8bbf0831d840d7f29c9e3ddefa63000000000000000000000000c5a96db085dda36ffbe390f455315d30d6d3dc52",
     * "contractAddress": "",
     * "cumulativeGasUsed": "122207",
     * "gasUsed": "122207",
     * "confirmations": "3745410"
     * }
     */
    public function transactionList($address, $page = 1, $perPage = 20)
    {
        ini_set('memory_limit', '1000M');

        $params = [
            'module'  => 'account',
            'action'  => 'txlist',
            'address' => $address,
            'sort'    => 'desc',
            'page'    => $page,
            'offset'  => $perPage,
        ];
        $result = $this->_callData('', $params);

        for ($i = 0; $i < count($result); $i++) {
            $d = substr($result[$i]['input'], 0, 10);
            $result[$i]['input'] = $d;
        }

        return $result;
    }

    /**
     * Выдает баланс эфира на кошельке
     *
     * @param string | array $address
     *
     * @return array массив кошельков, если был передан один адрес то значит будет возвращен массив из одного элемента
     * [
     * ['balance' => 8846542 wei], ...
     * ]
     */
    public function getBalance($address)
    {
        if (!is_array($address)) {
            $address = [$address];
        }
        $params = [
            'module'  => 'account',
            'action'  => 'balancemulti',
            'address' => join(',', $address),
            'tag'     => 'latest',
        ];
        $result = $this->_callData('', $params);

        return $result;
    }

    /**
     * @param string $token Имя или адрес
     * @param string $address
     * @param int $page
     *
     * @return array
     * [[
     * 'txid'       => '0xfdb16565b914182c6fee3442a1ef9bd2f33729ed2f769180529601f5774db70e'
     * 'block'      => '3875889'
     * 'time'       => '1497515650'
     * 'from'       => '0x6a710a74fc4b0033c4890d425f68032479b22c2a'
     * 'to'         => '0x7fe2b88f2e4858de375832fbf54ac7cf1a78ca51'
     * 'direction'  => 1
     * 'value'      => '376.87907886'
     * ], ... ]
     */
    public function tokenTransactionList($token, $address, $page = 1)
    {
        $client = new Client(['baseUrl' => $this->url]);
        $params = [
            'a'               => $address,
            'contractAddress' => $token,
            'mode'            => 'erc20',
        ];
        if ($page > 1) $params['p'] = $page;
        $response = $client->get('token/generic-tokentxns2', $params)->send();
        $content = $response->content;
        if ($response->headers['http-code'] == 503) {
            throw new \Exception('Service EtherScan not evaliable.'); // TODO Вывести ошибку
        }
        require_once(Yii::getAlias('@csRoot/services/simplehtmldom_1_5/simple_html_dom.php'));
        $content = str_get_html($content);
        /** @var \simple_html_dom_node $rows */
        $rows = $content->find('tr');

        $return = [];
        if (count($rows) <= 1) {
            return $return;
        }
        try {
            if (count($rows[1]->find('td')) == 3) return $return;

            for ($i = 1; $i < count($rows); $i++) {
                $new = [];

                /** @var \simple_html_dom_node $item */
                $item = $rows[$i];

                // txid
                if (is_null($item)) continue;
                try {
                    $td1 = $item->children(0);
                } catch (\Exception $e) {
                    VarDumper::dump(1);
                }
                $txid = join('', $td1->children(0)->children(0)->nodes[0]->_);
                $new['txid'] = $txid;

                // time
                $td3 = $item->children(1);
                $time = $td3->children(0)->attr['title'];
                $t = \DateTime::createFromFormat('M-d-Y H:i:s A', $time, new \DateTimeZone('UTC'));
                $new['time'] = $t->format('U');

                // from
                /** @var \simple_html_dom_node $td5 */
                $td3 = $item->children(2);
                $value = null;
                if (count($td3->children) == 1) {
                    // адрес
                    $text = trim($td3->children(0)->text());
                    if (StringHelper::startsWith($text, '0x')) {
                        $value = $text;
                    }
                }
                $new['from'] = $value;

                // to
                /** @var \simple_html_dom_node $td5 */
                $td5 = $item->children(4);
                $value = null;
                if (count($td5->children) == 1) {
                    // адрес
                    $text = trim($td5->children(0)->text());
                    if (StringHelper::startsWith($text, '0x')) {
                        $value = $text;
                    }
                }
                $new['to'] = $value;

                // direction
                if ($new['from'] == $new['to']) {
                    $new['direction'] = 0;
                } else if ($new['from'] == $address) {
                    $new['direction'] = -1;
                } else if ($new['to'] == $address) {
                    $new['direction'] = 1;
                }

                // value
                /** @var \simple_html_dom_node $td6 */
                $td6 = $item->children(5);
                $value = str_replace(',','',$td6->text());
                $new['value'] = $value;

                $return[] = $new;
            }
        } catch (\Exception $e) {
            VarDumper::dump($e->getTrace(),5,false);
        }

        return $return;
    }

    /**
     * Возвращает баланс токена на адресе
     *
     * @param string $token Имя или адрес
     * @param string $address
     *
     * @return int баланс в малых единицах токена
     */
    public function tokenGetBalance($token, $address)
    {
        $response = $this->_callData('', [
            'module'          => 'account',
            'action'          => 'tokenbalance',
            'contractaddress' => $token,
            'address'         => $address,
            'tag'             => 'latest',
        ]);

        return $response;
    }

    /**
     * Вызывает функцию
     *
     * @param      $path
     * @param null $params
     *
     * @return \yii\httpclient\Response
     */
    protected function _call($path, $params = null)
    {
        $client = new Client(['baseUrl' => $this->apiUrl]);
        if (is_null($params)) {
            $params = [];
        }
        $params['apikey'] = $this->apiKey;

        return $client->get($path, $params)->send();
    }

    /**
     * Вызывает функцию, если произошла ошибка то вызывается исключение
     *
     * @param      $path
     * @param null $params
     *
     * @return mixed содержимое параметра `result` уже раскодированное из JSON
     * @throws
     */
    protected function _callData($path, $params = null)
    {
        $response = $this->_call($path, $params);
        $data = Json::decode($response->content);
        if ($data['status'] != 1) {
            throw new Exception($data['message']);
        }

        return $data['result'];
    }
}