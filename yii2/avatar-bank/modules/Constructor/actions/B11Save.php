<?php

namespace avatar\modules\Constructor\actions;

/**
 * Created by PhpStorm.
 * User: s.arhangelskiy
 * Date: 08.08.2017
 * Time: 17:58
 */

use avatar\models\WalletETH;
use avatar\models\WalletToken;
use common\models\avatar\Currency;
use common\models\avatar\UserBill;
use common\models\BillingMain;
use common\models\comment\Comment;
use common\models\comment\CommentList;
use common\models\investment\IcoRequest;
use common\models\PaySystemConfig;
use common\models\piramida\Wallet;
use common\models\school\PageBlockContent;
use common\models\school\School;
use common\models\task\Task;
use common\models\Token;
use common\models\UserAvatar;
use common\models\UserWallet;
use common\services\Subscribe;
use cs\services\File;
use cs\services\VarDumper;
use Yii;
use yii\base\Exception;
use yii\helpers\FileHelper;
use yii\helpers\Json;
use yii\imagine\Image;

/**
 * REQUEST
 * - id
 */
class B11Save extends \avatar\base\BaseAction
{

    public function run()
    {
        $id = self::getParam('id');
        $block = PageBlockContent::findOne($id);
        $block->content = Json::encode([
            'text' => self::getParam('content'),
        ]);
        $block->save();

        return self::jsonSuccess();
    }
}