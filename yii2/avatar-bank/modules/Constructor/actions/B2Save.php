<?php

namespace avatar\modules\Constructor\actions;

/**
 */

use avatar\models\WalletETH;
use avatar\models\WalletToken;
use common\models\avatar\Currency;
use common\models\avatar\UserBill;
use common\models\BillingMain;
use common\models\comment\Comment;
use common\models\comment\CommentList;
use common\models\investment\IcoRequest;
use common\models\PaySystemConfig;
use common\models\piramida\Wallet;
use common\models\school\PageBlockContent;
use common\models\school\School;
use common\models\task\Task;
use common\models\Token;
use common\models\UserAvatar;
use common\models\UserWallet;
use common\services\Subscribe;
use cs\services\File;
use cs\services\Url;
use cs\services\VarDumper;
use Yii;
use yii\base\Exception;
use yii\helpers\FileHelper;
use yii\helpers\Json;
use yii\helpers\StringHelper;
use yii\imagine\Image;

/**
 * REQUEST
 * - id
 * - form [
 *      - id - string - video id или ссылка YouTube
 * ]
 */
class B2Save extends \avatar\base\BaseAction
{

    public function run()
    {
        $id = self::getParam('id');
        $block = PageBlockContent::findOne($id);
        if (is_null($block)) {
            return self::jsonErrorId(404, 'Не найден блок');
        }
        $form = self::getParam('form');
        if (!isset($form['id'])) {
            return self::jsonErrorId(102, 'Нет поля id');
        }
        $id = $form['id'];

        if (StringHelper::startsWith($id, 'http')) {
            $url = new \cs\services\Url($id);
            if ($url->host == 'youtu.be') {
                $v = substr($url->path, 1);
            } else if ($url->host == 'www.youtube.com') {
                $v = $url->getParam('v');
                if ($v == '') {
                    return self::jsonErrorId(102, 'Поле v пустое');
                }
            } else {
                return self::jsonErrorId(102, 'Адрес сайта должен быть www.youtube.com или youtu.be');
            }
        } else {
            $v = $id;
        }
        $block->content = Json::encode([
            'id' => $v,
        ]);
        $block->save();

        return self::jsonSuccess();
    }
}