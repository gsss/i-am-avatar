<?php

namespace frontend\tests\functional;

use frontend\tests\FunctionalTester;

class SiteControllerCest
{
    public function actionIndex(FunctionalTester $I)
    {
        //Идем на главную
        $I->amOnPage(\Yii::$app->homeUrl);

        //Проверяем наличие всех ключевых моментов
        $I->see('Проект ЯАватар');
    }
}