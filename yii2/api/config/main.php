<?php

$params = \yii\helpers\ArrayHelper::merge(
    require(__DIR__ . '/../../common/config/params.php'),
    require(__DIR__ . '/params.php')
);

$config = [
    'id'                  => 'avatar-api',
    'basePath'            => dirname(__DIR__),
    'bootstrap'           => ['log'],
    'controllerNamespace' => 'api\controllers',
    'vendorPath'          => dirname(dirname(__DIR__)) . '/vendor',
    'language'            => 'ru',
    'sourceLanguage'      => 'ru',
    'timeZone'            => 'Etc/GMT-3',
    'aliases'             => [
        '@csRoot' => __DIR__ . '/../../common/app',
        '@cs'     => __DIR__ . '/../../common/app',
    ],
    'components'          => [
        'session'         => [
            'timeout' => 600,
            'class'   => 'yii\web\CacheSession',
            'cache'   => 'cacheSession',
        ],
        'urlManager'      => [
            'rules' => [
                '/'                                  => 'site/index',
                'v1'                                 => 'site/index',
                'docs'                               => 'docs/index',
                'docs/<action>'                               => 'docs/<action>',
                'v1/<controller>/<action>'           => '<controller>/<action>',
                'v1/<controller>/<action>/<id:\\w+>' => '<controller>/<action>',
            ],

        ],
        'languageManager' => [
            'class'    => 'common\components\LanguageManager',
            'strategy' => [
                'ru' => ['ru', 'be', 'uk'],
                'en' => ['en', 'fr', 'de'],
            ],
        ],
        'authManager'     => [
            'class' => 'yii\rbac\PhpManager',
        ],
        'assetManager'    => [
            'appendTimestamp' => true,
        ],
        'piramida'        => [
            'class' => 'app\common\components\Piramida',
        ],
        'request'         => [
            // !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
            'cookieValidationKey'    => '',
            'enableCookieValidation' => false,
        ],
        'deviceDetect'    => [
            'class'     => 'avatar\services\DeviceDetect',
            'setParams' => 'false',
        ],
        'user'            => [
            'identityClass'   => 'common\models\UserAvatar',
            'authTimeout'     => 60 * 60 * 24 * 30,
            'enableAutoLogin' => true,
            'loginUrl'        => ['auth/login'],
        ],
        'errorHandler'    => [
            'errorAction' => 'site/error',
            'errorView'   => '@avatar/views/site/error.php',
        ],
        'formatter'       => [
            'dateFormat'        => 'dd.MM.yyyy',
            'timeFormat'        => 'php:H:i:s',
            'datetimeFormat'    => 'php:d.m.Y H:i',
            'decimalSeparator'  => '.',
            'thousandSeparator' => ',',
            'currencyCode'      => 'RUB',
            'locale'            => 'ru',
            'nullDisplay'       => '',
        ],
        'log'             => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets'    => [
                [
                    'class'  => 'common\services\DbTarget',
                    'levels' => [
                        'warning',
                        'error',
                    ],
                    'except' => [
                        'yii\web\HttpException:404',
                        'yii\web\HttpException:403',
                        'cs\web\Exception',
                    ],

                    'db'       => 'dbStatistic',
                    'logTable' => 'log2_api',
                ],
                [
                    'class'      => 'common\services\DbTarget',
                    'categories' => ['avatar\\*'],
                    'levels'     => [
                        'info',
                        'trace',
                    ],

                    'db'       => 'dbStatistic',
                    'logTable' => 'log2_api',
                ],
            ],
        ],
    ],
    'on beforeRequest'                                => function ($event) {
        \common\models\statistic\ServerRequest::add();
    },
    'modules'             => [
    ],
    'params'              => $params,
];


if (env('MEMCACHE_ENABLED', false)) {
    $config['components']['cacheSession'] = [
        'keyPrefix'    => 'session',
        'class'        => 'yii\caching\MemCache',
        'useMemcached' => true,
        'servers'      => [
            [
                'host' => env('MEMCACHE_HOST', 'localhost'),
                'port' => env('MEMCACHE_PORT', 11211),
            ],
        ],
    ];
} else {
    $config['components']['cacheSession'] = [
        'keyPrefix'    => 'session',
        'class'        => 'yii\caching\FileCache',
    ];
}

return $config;
