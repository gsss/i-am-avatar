<?php

namespace api\base;


use common\models\school\School;
use common\models\UserAccessToken;
use common\models\UserAvatar;
use common\models\UserRegistration;
use common\payment\BitCoinBlockTrailPayment;
use cs\Application;
use cs\services\UploadFolderDispatcher;
use cs\services\VarDumper;
use cs\web\Exception;
use Yii;
use yii\base\UserException;
use yii\db\Connection;
use yii\db\Query;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use yii\helpers\FileHelper;
use yii\helpers\Json;
use yii\helpers\Url;
use yii\web\BadRequestHttpException;
use yii\web\ForbiddenHttpException;
use yii\web\HttpException;
use yii\web\NotFoundHttpException;
use yii\web\Response;
use yii\web\UnauthorizedHttpException;

class BaseAuthController extends BaseController
{
    public $enableCsrfValidation = false;

    public $school;

    public function init()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        $auth = \Yii::$app->request->headers->get('Authorization');
        if (is_null($auth)) {
            throw new UnauthorizedHttpException('Отсутствует обязательный параметр Authorization', 10);
        }
        $arr = explode(' ', $auth);
        if (count($arr) != 2) {
            throw new UnauthorizedHttpException('параметр Authorization должен состоять из двух слов', 11);
        }
        if ($arr[0] != 'Bearer') {
            throw new UnauthorizedHttpException('параметр Authorization. Первое слово должно быть Bearer', 12);
        }
        $userToken = UserAccessToken::findOne(['token' => $arr[1]]);
        if (is_null($userToken)) {
            throw new UnauthorizedHttpException('Не найден такой токен', 13);
        }
        if ($userToken->expire < time()) {
            throw new UnauthorizedHttpException('Токен не действителен', 14);
        }

        $school = School::findOne($userToken->school_id);
        $this->school = $school;

        $user = UserAvatar::findOne($userToken->user_id);
        Yii::$app->user->login($user);

        parent::init();
    }

}
