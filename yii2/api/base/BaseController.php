<?php

namespace api\base;

use avatar\models\forms\Contact;
use avatar\services\LogReader;
use common\components\providers\ETH;
use common\components\sms\IqSms;
use common\models\avatar\Currency;
use common\models\avatar\UserBill;
use common\models\CompanyCustomizeItem;
use common\models\PaymentBitCoin;
use common\models\school\School;
use common\models\UserAvatar;
use common\models\UserRegistration;
use common\payment\BitCoinBlockTrailPayment;
use cs\Application;
use cs\services\UploadFolderDispatcher;
use cs\services\VarDumper;
use cs\web\Exception;
use Yii;
use yii\base\UserException;
use yii\db\Connection;
use yii\db\Query;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use yii\helpers\FileHelper;
use yii\helpers\Json;
use yii\helpers\Url;
use yii\web\BadRequestHttpException;
use yii\web\ForbiddenHttpException;
use yii\web\HttpException;
use yii\web\NotFoundHttpException;
use yii\web\Response;

class BaseController extends \avatar\base\BaseController
{
    public $enableCsrfValidation = false;

    // https://learn.javascript.ru/xhr-crossdomain
    public function behaviors()
    {
        return [
            'corsFilter' => [
                'class' => \yii\filters\Cors::className(),
                'cors'  => [
                    'Origin'                           => ['*'],
                    'Access-Control-Allow-Origin'      => ['*'],
                    'Access-Control-Request-Method'    => ['GET', 'POST', 'OPTIONS'],
                    'Access-Control-Request-Headers'   => ['*'],
                    'Access-Control-Allow-Credentials' => true,
                    'Access-Control-Max-Age'           => 86400,
                    'Access-Control-Expose-Headers'    => [],
                ],
            ],
        ];
    }

    public function init()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        parent::init();
    }

}
