<?php

namespace api\base;


use common\models\school\School;
use cs\services\VarDumper;
use yii\helpers\Json;
use yii\web\HttpException;
use yii\web\Response;
use Yii;

class BaseSchoolGuestController extends BaseController
{
    public $school;

    public function init()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        $sid = Yii::$app->request->post('school_id');
        if (is_null($sid)) {
            throw new HttpException(400, 'Нет обязательного параметра school_id');
        }
        $school = School::findOne($sid);
        $this->school = $school;

        parent::init();
    }
}
