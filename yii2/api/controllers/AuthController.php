<?php

namespace api\controllers;

use app\models\Article;
use app\models\SiteUpdate;
use app\services\Subscribe;
use avatar\models\forms\Contact;
use avatar\services\LogReader;
use common\components\providers\ETH;
use common\components\sms\IqSms;
use common\models\avatar\Currency;
use common\models\avatar\UserBill;
use common\models\CompanyCustomizeItem;
use common\models\PaymentBitCoin;
use common\models\school\School;
use common\models\UserAccessToken;
use common\models\UserAvatar;
use common\models\UserRegistration;
use common\payment\BitCoinBlockTrailPayment;
use cs\Application;
use cs\base\BaseController;
use cs\services\UploadFolderDispatcher;
use cs\services\VarDumper;
use cs\web\Exception;
use Yii;
use yii\base\UserException;
use yii\db\Connection;
use yii\db\Query;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use yii\helpers\FileHelper;
use yii\helpers\Json;
use yii\helpers\Url;
use yii\web\BadRequestHttpException;
use yii\web\ForbiddenHttpException;
use yii\web\HttpException;
use yii\web\NotFoundHttpException;
use yii\web\Response;

class AuthController extends \avatar\base\BaseController
{
    public $enableCsrfValidation = false;

    public $school;

    // https://learn.javascript.ru/xhr-crossdomain
    public function behaviors()
    {
        return [
            'corsFilter' => [
                'class' => \yii\filters\Cors::className(),
                'cors'  => [
                    'Origin'                           => ['*'],
                    'Access-Control-Allow-Origin'      => ['*'],
                    'Access-Control-Request-Method'    => ['POST', 'OPTIONS'],
                    'Access-Control-Request-Headers'   => ['*'],
                    'Access-Control-Allow-Credentials' => true,
                    'Access-Control-Max-Age'           => 86400,
                    'Access-Control-Expose-Headers'    => [],
                ],
            ],
        ];
    }

    public function init()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        $school = School::findOne(20);
        $this->school = $school;
    }

    /**
     * логинит пользователя если у него нет 2FA
     */
    public function actionLogin()
    {
        $model = new \api\models\validate\AuthControllerLogin();

        if (!$model->load(ArrayHelper::merge(Yii::$app->request->post(), Yii::$app->request->get()), '')) {
            throw new BadRequestHttpException('Не загружены данные', 4);
        }
        if (!$model->validate()) {
            throw new HttpException(422, Json::encode([
                'fields' => ['login', 'password', 'company_id'],
                'errors' => $model->errors
            ]));
        }

        return $model->action();
    }

    /**
     * логинит пользователя если у него нет 2FA
     */
    public function actionLoginAfter2fa()
    {
        $model = new \api\models\validate\AuthControllerLoginAfter2fa();

        if (!$model->load(ArrayHelper::merge(Yii::$app->request->post(), Yii::$app->request->get()), '')) {
            throw new BadRequestHttpException('Не загружены данные', 4);
        }
        if (!$model->validate()) {
            throw new HttpException(422, Json::encode([
                'fields' => ['key', 'company_id'],
                'errors' => $model->errors
            ]));
        }

        return $model->action();
    }

    /**
     * логинит пользователя если у него нет 2FA
     */
    public function actionValidate2fa()
    {
        $model = new \api\models\validate\AuthControllerValidate2fa();

        if (!$model->load(ArrayHelper::merge(Yii::$app->request->post(), Yii::$app->request->get()), '')) {
            throw new BadRequestHttpException('Не загружены данные', 4);
        }
        if (!$model->validate()) {
            throw new HttpException(422, Json::encode([
                'fields' => ['login', 'password', 'code'],
                'errors' => $model->errors
            ]));
        }

        return $model->action();
    }

    /**
     */
    public function actionLoginFirst()
    {
        $model = new \api\models\validate\AuthControllerLoginFirst();

        if (!$model->load(ArrayHelper::merge(Yii::$app->request->post(), Yii::$app->request->get()), '')) {
            throw new BadRequestHttpException('Не загружены данные', 4);
        }
        if (!$model->validate()) {
            throw new HttpException(422, Json::encode([
                'fields' => ['login', 'password'],
                'errors' => $model->errors
            ]));
        }

        return $model->action();
    }

    /**
     */
    public function actionRegistration()
    {
        $sid = \Yii::$app->request->headers->get('X-SCHOOL-ID');
        if (is_null($sid)) {
            throw new BadRequestHttpException('Отсутствует обязательный параметр cookie X-SCHOOL-ID', 1);
        }
        if (!Application::isInteger($sid)) {
            throw new BadRequestHttpException('Параметр X-SCHOOL-ID должен быть целым числом', 2);
        }
        $school = School::findOne($sid);
        if (is_null($school)) {
            throw new BadRequestHttpException('Школа соответствующая X-SCHOOL-ID не найдена', 3);
        }

        $model = new \api\models\validate\AuthControllerRegistration();

        if (!$model->load(ArrayHelper::merge(Yii::$app->request->post(), Yii::$app->request->get()), '')) {
            throw new BadRequestHttpException('Не загружены данные', 4);
        }
        if (!$model->validate()) {
            throw new HttpException(422, Json::encode($model->errors));
        }

        return $model->action();
    }

    /**
     */
    public function actionUserDelete()
    {
        if (!YII_ENV_PROD) throw new \Exception('Эта функция доступна только на TEST');

        $model = new \api\models\validate\AuthControllerUserDelete();

        if (!$model->load(ArrayHelper::merge(Yii::$app->request->post(), Yii::$app->request->get()), '')) {
            throw new BadRequestHttpException('Не загружены данные', 4);
        }
        if (!$model->validate()) {
            throw new HttpException(422, Json::encode($model->errors));
        }

        return $model->action();
    }

    /**
     */
    public function actionRefresh()
    {
        $model = new \api\models\validate\AuthControllerRefresh();

        if (!$model->load(ArrayHelper::merge(Yii::$app->request->post(), Yii::$app->request->get()), '')) {
            throw new BadRequestHttpException('Не загружены данные', 4);
        }
        if (!$model->validate()) {
            throw new HttpException(422, Json::encode($model->errors));
        }

        return $model->action();
    }

    /**
     */
    public function actionValidateAccessToken()
    {
        $model = new \api\models\validate\AuthControllerValidateAccessToken();

        if (!$model->load(ArrayHelper::merge(Yii::$app->request->post(), Yii::$app->request->get()), '')) {
            throw new BadRequestHttpException('Не загружены данные', 4);
        }
        if (!$model->validate()) {
            throw new HttpException(422, Json::encode($model->errors));
        }

        return $model->action();
    }
}
