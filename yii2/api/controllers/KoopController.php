<?php

namespace api\controllers;

use app\services\Subscribe;
use avatar\models\forms\Contact;
use avatar\services\LogReader;
use common\components\providers\ETH;
use common\components\sms\IqSms;
use common\models\avatar\Currency;
use common\models\avatar\UserBill;
use common\models\Card;
use common\models\CardDesign;
use common\models\CompanyCustomizeItem;
use common\models\PaymentBitCoin;
use common\models\school\School;
use common\models\shop\Product;
use common\models\UserAvatar;
use common\models\UserRegistration;
use common\payment\BitCoinBlockTrailPayment;
use common\widgets\FileUpload7\FileUpload;
use cs\Application;
use cs\base\BaseController;
use cs\services\UploadFolderDispatcher;
use cs\services\VarDumper;
use cs\web\Exception;
use Yii;
use yii\base\UserException;
use yii\db\Connection;
use yii\db\Query;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use yii\helpers\FileHelper;
use yii\helpers\Json;
use yii\helpers\Url;
use yii\web\BadRequestHttpException;
use yii\web\ForbiddenHttpException;
use yii\web\HttpException;
use yii\web\NotFoundHttpException;
use yii\web\Response;

class KoopController extends \api\base\BaseController
{
    /**
     */
    public function actionPay()
    {
        $type_id = Yii::$app->request->post('type_id');

        return self::jsonSuccess();
    }

    /**
     */
    public function actionPay2()
    {
        $list = Yii::$app->request->post('list');

        return self::jsonSuccess();
    }

    /**
     */
    public function actionPay3()
    {
        $list = Yii::$app->request->post('list');

        return self::jsonSuccess();
    }

    /**
     */
    public function actionRegPay1()
    {
        $list = Yii::$app->request->post('list');

        return self::jsonSuccess();
    }

    /**
     */
    public function actionRegPay2()
    {
        $list = Yii::$app->request->post('list');

        return self::jsonSuccess();
    }

}
