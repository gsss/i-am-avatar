<?php

namespace api\controllers;

use app\models\Article;
use app\models\SiteUpdate;
use app\services\Subscribe;
use avatar\controllers\actions\DefaultAjax;
use avatar\models\forms\Contact;
use avatar\services\LogReader;
use common\components\providers\ETH;
use common\components\sms\IqSms;
use common\models\avatar\Currency;
use common\models\avatar\CurrencyLink;
use common\models\avatar\UserBill;
use common\models\Card;
use common\models\CardDesign;
use common\models\CompanyCustomizeItem;
use common\models\PaymentBitCoin;
use common\models\school\School;
use common\models\UserAvatar;
use common\models\UserRegistration;
use common\payment\BitCoinBlockTrailPayment;
use cs\Application;
use cs\base\BaseController;
use cs\services\UploadFolderDispatcher;
use cs\services\VarDumper;
use cs\web\Exception;
use iAvatar777\services\Processing\Wallet;
use iAvatar777\widgets\FileUpload7\FileUpload;
use Yii;
use yii\base\UserException;
use yii\db\Connection;
use yii\db\Query;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use yii\helpers\FileHelper;
use yii\helpers\Json;
use yii\helpers\Url;
use yii\web\BadRequestHttpException;
use yii\web\ForbiddenHttpException;
use yii\web\HttpException;
use yii\web\NotFoundHttpException;
use yii\web\Response;

class WalletController extends \api\base\BaseAuthController
{
    public function actions()
    {
        return [
            'transactions' => [
                'class' => '\api\controllers\action\DefaultAjaxGet',
                'model' => '\api\models\validate\WalletControllerTransactions',
                'fields' => ['bill_id', 'page'],
            ],
            'item' => [
                'class' => '\api\controllers\action\DefaultAjaxGet',
                'model' => '\api\models\validate\WalletControllerItem',
                'fields' => ['bill_id'],
            ],
        ];
    }

    /**
     */
    public function actionList()
    {
        $walletList = UserBill::find()->where(['user_id' => Yii::$app->user->id, 'mark_deleted' => 0])->all();
        $arr = [];
        /** @var \common\models\avatar\UserBill $w */
        foreach ($walletList as $w) {
            $c = Currency::findOne($w->currency);
            try {
                if (Application::isEmpty($c->image)) {
                    $image = '';
                } else {
                    $image =  FileUpload::getFile($c->image, 'crop');
                }
                $w1 = Wallet::findOne($w->address);
                $arr[] = [
                    'id'         => $w->id,
                    'is_default' => $w->is_default,
                    'card_id'    => $w->card_id,
                    'name'       => $w->name,
                    'amount'     => $w1->amount,
                    'currency'   => [
                        'id'       => $c->id,
                        'image'    => $image,
                        'decimals' => $c->decimals,
                        'code'     => $c->code,
                    ],
                ];

            } catch (\Exception $e) {
                VarDumper::dump($c);
            }
        }
//        $cardList = Card::find()->where(['user_id' => Yii::$app->user->id])->all();
//        $arr2 = [];
//        /** @var \common\models\Card $c */
//        foreach ($cardList as $c) {
//            $arr2[] = [
//                'id'        => $c->id,
//                'number'    => $c->number,
//                'design_id' => $c->design_id,
//                'design'    => CardDesign::findOne($c->design_id)->getApiFields(),
//            ];
//        }

        return $arr;
    }

}
