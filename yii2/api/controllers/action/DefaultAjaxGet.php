<?php
/**
 * Created by PhpStorm.
 * User: s.arhangelskiy
 * Date: 18.01.2017
 * Time: 13:17
 */

namespace api\controllers\action;

use cs\services\VarDumper;
use Yii;
use yii\base\Action;
use yii\base\Model;
use yii\helpers\Json;
use yii\web\BadRequestHttpException;
use yii\web\HttpException;
use yii\web\Response;

class DefaultAjaxGet extends \avatar\controllers\actions\BaseAction
{
    public $model;
    public $formName = '';
    public $fields = [];

    public function run()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        $class = $this->model;
        /** @var Model $model */
        $model = new $class();
        if (!Yii::$app->request->isGet) {
            throw new BadRequestHttpException('Только GET', 1);
        }
        if (!$model->load(Yii::$app->request->get(), $this->formName)) {
            throw new BadRequestHttpException('Не загружены данные', 4);
        }
        if (!$model->validate()) {
            throw new HttpException(422, Json::encode([
                'fields' => $this->fields,
                'errors' => $model->errors
            ]));
        }

        return $model->action();
    }
}