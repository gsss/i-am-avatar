<?php

namespace api\controllers;

use app\services\Subscribe;
use avatar\models\forms\Contact;
use avatar\services\LogReader;
use common\components\providers\ETH;
use common\components\sms\IqSms;
use common\models\avatar\Currency;
use common\models\avatar\UserBill;
use common\models\Card;
use common\models\CardDesign;
use common\models\CompanyCustomizeItem;
use common\models\PaymentBitCoin;
use common\models\school\School;
use common\models\shop\Product;
use common\models\UserAvatar;
use common\models\UserRegistration;
use common\payment\BitCoinBlockTrailPayment;
use common\widgets\FileUpload7\FileUpload;
use cs\Application;
use cs\base\BaseController;
use cs\services\UploadFolderDispatcher;
use cs\services\VarDumper;
use cs\web\Exception;
use Yii;
use yii\base\UserException;
use yii\db\Connection;
use yii\db\Query;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use yii\helpers\FileHelper;
use yii\helpers\Json;
use yii\helpers\Url;
use yii\web\BadRequestHttpException;
use yii\web\ForbiddenHttpException;
use yii\web\HttpException;
use yii\web\NotFoundHttpException;
use yii\web\Response;

class ShopController extends \api\base\BaseSchoolGuestController
{
    /**
     * REQUEST:
     * - school_id - int
     */
    public function actionIndex()
    {
        $list = Product::find()->where(['school_id' => $this->school->id])
            ->select([
                'id',
                'name',
                'price',
                'date_insert',
                'image',
                'currency_id',
                ])
            ->all();
        $rows = [];

        foreach ($list as $i) {
            if (is_null($i['currency_id'])) {
                $rows[] = [
                    'id'          => $i['id'],
                    'name'        => $i['name'],
                    'price'       => $i['price'] / 100,
                    'date_insert' => $i['date_insert'],
                    'image'       => FileUpload::getFile($i['image'], 'crop'),
                    'currency'    => null,
                ];
            } else {
                $c = Currency::findOne($i['currency_id']);
                $rows[] = [
                    'id'          => $i['id'],
                    'name'        => $i['name'],
                    'price'       => $i['price'] / 100,
                    'date_insert' => $i['date_insert'],
                    'image'       => FileUpload::getFile($i['image'], 'crop'),
                    'currency'    => [
                        'id'       => $i['currency_id'],
                        'code'     => $c->code,
                        'decimals' => $c->decimals,
                    ],
                ];
            }
        }

        return [
            'items' => $rows
        ];
    }

    /**
     * REQUEST:
     * - id - int - идентификатор товара
     * - school_id - int
     */
    public function actionItem()
    {
        $id = Yii::$app->request->post('id');
        if (Application::isEmpty($id)) throw new HttpException(400, 'Нет обязательного параметра id');
        if (!Application::isInteger($id)) throw new HttpException(400, 'id не число');

        $item = Product::findOne($id);

        if (is_null($item)) throw new HttpException(404, 'Товар не найден');
        if ($item->school_id != $this->school->id) throw new HttpException(404, 'Товар не найден в этом сообществе');

        return $item;
    }
}
