<?php

/** $this \yii\web\View  */

use yii\helpers\Url;
use app\services\GsssHtml;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use app\services\GetArticle\Collection;

$this->title = 'API платформы ЯАватар';


?>




<div class="container">
    <div class="col-lg-12">
        <h1 class="page-header"><?= Html::encode($this->title) ?></h1>

        <p class="alert alert-danger">В разработке</p>
        <p><a href="https://www.yiiframework.com/doc/api/2.0/yii-rest-controller" target="_blank">https://www.yiiframework.com/doc/guide/2.0/ru/rest-quick-start</a></p>
        <p><a href="https://www.yiiframework.com/doc/guide/2.0/ru/rest-quick-start" target="_blank">Быстрый старт</a></p>
        <p><a href="https://www.yiiframework.com/doc/guide/2.0/ru/rest-resources" target="_blank">Ресурсы</a></p>
        <p><a href="https://www.yiiframework.com/doc/guide/2.0/ru/rest-controllers" target="_blank">Контроллеры</a></p>
        <p><a href="https://www.yiiframework.com/doc/guide/2.0/ru/rest-routing" target="_blank">Роутинг</a></p>
        <p><a href="https://www.yiiframework.com/doc/guide/2.0/ru/rest-response-formatting" target="_blank">Форматирование ответа</a></p>
        <p><a href="https://www.yiiframework.com/doc/guide/2.0/ru/rest-authentication" target="_blank">Аутентификация</a> <a href="https://habr.com/ru/company/dataart/blog/262817/" target="_blank">https://habr.com/ru/company/dataart/blog/262817/</a></p>
        <p><a href="https://www.yiiframework.com/doc/guide/2.0/ru/rest-rate-limiting" target="_blank">Ограничение частоты запросов</a></p>
        <p><a href="https://www.yiiframework.com/doc/guide/2.0/ru/rest-versioning" target="_blank">Версионирование</a></p>
        <p><a href="https://www.yiiframework.com/doc/guide/2.0/ru/rest-error-handling" target="_blank">Обработка ошибок</a></p>


    </div>
</div>

