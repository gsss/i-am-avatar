<?php
use yii\helpers\Url;
use yii\helpers\Html;

function liMenu12($route, $name = null, $options = [])
{
    if (is_array($route)) {
        $arr = [];
        foreach ($route as $route1 => $name1) {
            $arr[] = liMenu12($route1, $name1);
        }
        return join("\n", $arr);
    }
    if (Yii::$app->requestedRoute == $route) {
        $options['class'] = 'active';
    }

    return \yii\helpers\Html::tag('li', \common\helpers\Html::a($name, [$route]), $options);
}

$menu = [];

$menu[] = [
    'label' => Yii::t('c.oeVo5Oaket', 'Новости'),
    'route' => 'news/index',
    'urlList' => [
        'controller' => 'news'
    ]
];
$menu[] = [
    'label' => Yii::t('c.oeVo5Oaket', 'Блог'),
    'route' => 'blog/index',
    'urlList' => [
        'controller' => 'blog'
    ]
];
$menu[] = [
    'label' => Yii::t('c.oeVo5Oaket', 'О нас'),
    'route' => 'site/about',
];

$menu[] = [
    'label' => Yii::t('c.oeVo5Oaket', 'Контакты'),
    'route' => 'site/contact',
];

/**
 * @param $item
 * @param $route
 * @return bool
 */
function hasRoute1($item, $route)
{
    if (isset($item['route'])) {
        if ($item['route'] == $route) return true;
    }

    if (isset($item['items'])) {
        foreach($item['items'] as $i) {
            if ($i['route'] == $route) {
                return true;
            }
        }
    }

    if (\yii\helpers\ArrayHelper::keyExists('urlList', $item)) {
        foreach($item['urlList'] as $i => $v) {
            switch($i) {
                case 'startsWith':
                    if (\yii\helpers\StringHelper::startsWith($route, $v)) return true;
                    break;
                case 'controller':
                    $arr = explode('/', $route);
                    if ($arr[0] == $v) return true;
                    break;
            }
        }
    }

    return false;
}
?>

<?php
$rows = [];
foreach($menu as $item) {
    $class = ['dropdown'];
    if (hasRoute1($item, Yii::$app->requestedRoute)) {
        $class[] = 'active';
    }
    if (\yii\helpers\ArrayHelper::keyExists('items', $item)) {
        $html = Html::tag(
            'li',
            Html::a(
                $item['label']
                .
                ' '
                .
                Html::tag('span', null, ['class' => 'caret'])
                ,
                '#'
                ,
                [
                    'class'         => "dropdown-toggle",
                    'data-toggle'   => "dropdown",
                    'role'          => "button",
                    'aria-expanded' => "false",
                ]
            )
            .
            Html::tag('ul',
                liMenu12(\yii\helpers\ArrayHelper::map($item['items'], 'route', 'label'))
                , [
                    'class' => "dropdown-menu",
                    'role'  => "menu",
                ])
            ,
            ['class' => join(' ', $class)]);
    } else {
        $html = Html::tag(
            'li',
            Html::a($item['label'], [$item['route']]),
            ['class' => join(' ', $class)]
        );
    }

    $rows[] = $html;
}

?>
<?= join("\n", $rows) ?>
