<?php

namespace api\models\validate;

use app\models\Article;
use app\models\SiteUpdate;
use app\services\Subscribe;
use avatar\controllers\actions\DefaultAjax;
use avatar\models\forms\Contact;
use avatar\services\LogReader;
use common\components\providers\ETH;
use common\components\sms\IqSms;
use common\models\avatar\Currency;
use common\models\avatar\CurrencyLink;
use common\models\avatar\UserBill;
use common\models\Card;
use common\models\CardDesign;
use common\models\CompanyCustomizeItem;
use common\models\PaymentBitCoin;
use common\models\school\School;
use common\models\UserAvatar;
use common\models\UserRegistration;
use common\payment\BitCoinBlockTrailPayment;
use cs\Application;
use cs\base\BaseController;
use cs\services\UploadFolderDispatcher;
use cs\services\VarDumper;
use cs\web\Exception;
use iAvatar777\services\Processing\Operation;
use iAvatar777\services\Processing\Transaction;
use iAvatar777\services\Processing\Wallet;
use iAvatar777\widgets\FileUpload7\FileUpload;
use Yii;
use yii\base\UserException;
use yii\db\Connection;
use yii\db\Query;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use yii\helpers\FileHelper;
use yii\helpers\Json;
use yii\helpers\Url;
use yii\web\BadRequestHttpException;
use yii\web\ForbiddenHttpException;
use yii\web\HttpException;
use yii\web\NotFoundHttpException;
use yii\web\Response;

class WalletControllerTransactions extends \yii\base\Model
{
    public $bill_id;

    public $page;

    /** @var Wallet */
    public $wallet;

    /** @var  \common\models\avatar\UserBill */
    public $bill;

    public function rules()
    {
        return [
            ['bill_id', 'required'],
            ['bill_id', 'integer'],
            ['bill_id', 'ValidateWallet'],

            ['page', 'integer'],
            ['page', 'ValidatePage', 'skipOnEmpty' => false],
        ];
    }

    public function ValidateWallet($attribute, $params)
    {
        if (!$this->hasErrors()) {
            $UserBill = UserBill::findOne($this->bill_id);
            if (is_null($UserBill)) {
                $this->addError($attribute, 'Не найден счет');
                return;
            }
            if ($UserBill->user_id != Yii::$app->user->id) {
                $this->addError($attribute, 'Это не ваш счет');
                return;
            }
            if (is_null($UserBill)) {
                $this->addError($attribute, 'Не найден счет');
                return;
            }
            $w = Wallet::findOne($UserBill->address);

            $this->bill_id = $UserBill;
            $this->wallet = $w;
        }
    }

    public function ValidatePage($attribute, $params)
    {
        if (!$this->hasErrors()) {
            if (Application::isEmpty($this->page)) {
                $this->page = 1;
            }
        }
    }

    public function action()
    {
        $rows = Operation::find()
            ->where(['wallet_id' => $this->wallet->id])
            ->limit(20)
            ->offset(($this->page - 1) * 20)
            ->asArray()
            ->all()
        ;
        if (count($rows) == 0) {
            return [
                'list'  => [],
                'pages' => [
                    "current" => 1,
                    "count"   => 1,
                ],
            ];
        }

        $w = Wallet::findOne($rows[0]['wallet_id']);
        $currency = \iAvatar777\services\Processing\Currency::findOne($w['currency_id']);

        $count = Operation::find()
            ->where(['wallet_id' => $this->wallet->id])
            ->limit(20)
            ->count();
        $count = (int)(($count + 19) / 20);

        $list = [];
        foreach ($rows as $r) {
            unset($r['wallet_id']);
            unset($r['before']);
            unset($r['after']);
            unset($r['address']);
            $r['amount_decimals'] = \iAvatar777\services\Processing\Currency::getValueFromAtom($r['amount'], $currency);
            $r['amount'] = (int)$r['amount'];
            $list[] = $r;
        }

        return [
            'list'  => $list,
            'pages' => [
                "current" => $this->page,
                "count"   => $count,
            ],
        ];

    }
}