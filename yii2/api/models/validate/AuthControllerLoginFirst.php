<?php
/**
 * Created by PhpStorm.
 * User: Ra-m-ha
 * Date: 02.08.2019
 * Time: 3:01
 */

namespace api\models\validate;

use common\models\school\AdminLink;
use common\models\school\CommandLink;
use common\models\school\School;
use common\models\UserAccessToken;
use common\models\UserAvatar;
use cs\Application;
use cs\services\Security;
use cs\services\VarDumper;
use iAvatar777\widgets\FileUpload7\FileUpload;
use yii\helpers\ArrayHelper;

class AuthControllerLoginFirst extends \yii\base\Model
{
    public $login;
    public $password;

    /** @var  \common\models\school\School */
    public $school;

    /** @var  UserAvatar */
    public $user;

    public function rules()
    {
        return [
            ['login', 'required'],
            ['login', 'email'],
            ['login', 'ValidateUser'],

            ['password', 'required'],
            ['password', 'string'],
            ['password', 'ValidatePassword'],
        ];
    }

    public function ValidateUser($attribute, $params)
    {
        if (!$this->hasErrors()) {
            try {
                $user = UserAvatar::findByUsername($this->login);
            } catch (\Exception $e) {
                $this->addError($attribute, 'Пользователь не найден');
                return;
            }

            $this->user = $user;
        }
    }

    public function ValidatePassword($attribute, $params)
    {
        if (!$this->hasErrors()) {
            if (!$this->user->validatePassword($this->password)) {
                $this->addError($attribute, 'Пароль не верный');
                return;
            }
        }
    }

    /**
     * @return array
     */
    public function action()
    {
        $rowsAdminLink = AdminLink::find()->where(['user_id' => $this->user->id])->select(['school_id'])->column();
        $rowsCommandLink = CommandLink::find()->where(['user_id' => $this->user->id])->select(['school_id'])->column();

        $rows = ArrayHelper::merge($rowsAdminLink, $rowsCommandLink);
        $rows = array_unique($rows);

        $rows2 = School::find()->where(['id' => $rows])
            ->select([
                'id',
                'name',
                'image',
            ])
            ->asArray()
            ->all();
        $rows3 = [];

        foreach ($rows2 as $r) {
            $params = [
                'id'    => $r['id'],
                'name'  => $r['name'],
            ];
            if (is_null($r['image'])) {
                $params['image'] = $r['image'];
            } else {
                try {
                    $params['image'] = FileUpload::getFile($r['image'], 'crop');
                } catch (\Exception $e) {
                    VarDumper::dump($r);
                }
            }
            $rows3[] = $params;
        }

        return [
            'is_2fa'       => Application::isEmpty($this->user->google_auth_code) ? 0 : 1,
            'company_list' => $rows2,
        ];
    }
}