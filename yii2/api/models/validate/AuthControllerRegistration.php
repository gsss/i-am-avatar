<?php
/**
 * Created by PhpStorm.
 * User: Ra-m-ha
 * Date: 02.08.2019
 * Time: 3:01
 */

namespace api\models\validate;

use common\models\UserAccessToken;
use common\models\UserAvatar;
use cs\services\Security;
use cs\services\VarDumper;
use common\models\UserRoot;
use common\models\school\UserLink;

class AuthControllerRegistration extends \yii\base\Model
{
    public $login;
    public $password;

    /** @var  int */
    public $school_id;

    /** @var  \common\models\UserRoot */
    public $userRoot;

    public function rules()
    {
        return [
            ['school_id', 'required'],
            ['school_id', 'integer'],

            ['login', 'required'],
            ['login', 'string'],
            ['login', 'trim'],
            ['login', 'toLower'],
            ['login', 'email'],
            ['login', 'validateUser'],
            ['login', 'validateUserRoot'],

            ['password', 'required'],
            ['password', 'string'],
            ['password', 'ValidatePassword'],
        ];
    }

    public function validateUser($attribute, $params)
    {
        if (!$this->hasErrors()) {
            try {
                $user = \common\models\UserAvatar::findOne(['email' => $this->login]);
                $this->addError($attribute, 'Такой пользователь уже есть, придумайте другой или если это ваш логин то войдите');
                return;
            } catch (\Exception $e) {

            }
        }
    }

    /**
     */
    public function validateUserRoot($attribute, $params)
    {
        if (!$this->hasErrors()) {
            $userRoot = UserRoot::findOne(['email' => $this->login]);
            if (is_null($userRoot)) {
                $userRoot = UserRoot::add(['email' => $this->login]);
            }
            $this->userRoot = $userRoot;
        }
    }

    /**
     */
    public function toLower($attribute, $params)
    {
        if (!$this->hasErrors()) {
            $this->login = strtolower($this->login);
        }
    }

    public function ValidatePassword($attribute, $params)
    {
        if (!$this->hasErrors()) {

        }
    }

    public function action()
    {
        \common\models\UserAvatar::registration($this->login, $this->password);

        // добавляю ссылку
        $link = UserLink::findOne(['user_root_id' => $this->userRoot->id, 'school_id' => $this->school_id]);
        if (is_null($link)) {
            $link = UserLink::add(['user_root_id' => $this->userRoot->id, 'school_id' => $this->school_id]);
        }

        return true;
    }
}