<?php
/**
 * Created by PhpStorm.
 * User: Ra-m-ha
 * Date: 02.08.2019
 * Time: 3:01
 */

namespace api\models\validate;

use common\models\UserAccessToken;
use common\models\UserAvatar;
use cs\services\Security;
use cs\services\VarDumper;

class AuthControllerRefresh extends \yii\base\Model
{
    public $refresh_token;

    /** @var  UserAvatar */
    public $user;

    public function rules()
    {
        return [
            ['refresh_token', 'required'],
            ['refresh_token', 'ValidateUser'],
        ];
    }

    public function ValidateUser($attribute, $params)
    {
        if (!$this->hasErrors()) {
            $token = UserAccessToken::findOne(['refresh_token' => $this->refresh_token]);
            if (is_null($token)) {
                $this->addError($attribute, 'Токен не найден');
                return;
            }
            try {
                $user = UserAvatar::findOne($token->user_id);
            } catch (\Exception $e) {
                $this->addError($attribute, 'Пользователь не найден');
                return;
            }
            $this->user = $user;
        }
    }

    public function action($ttl = 60 * 60 * 24 * 150)
    {
        $access_token = Security::generateRandomString(64);
        $refresh_token = Security::generateRandomString(20);
        $expire = time() + $ttl;
        $school_id = \Yii::$app->request->headers->get('X-SCHOOL-ID');
        UserAccessToken::deleteAll([
            'user_id'       => $this->user->id,
            'school_id'     => $school_id,
        ]);
        $UserAccessToken = UserAccessToken::add([
            'token'         => $access_token,
            'expire'        => $expire,
            'user_id'       => $this->user->id,
            'school_id'     => $school_id,
            'refresh_token' => $refresh_token,
        ]);

        return [
            'access_token'  => $access_token,
            'token_type'    => 'Bearer',
            'expire'        => $expire,
            'refresh_token' => $refresh_token,
        ];
    }
}