<?php
/**
 * Created by PhpStorm.
 * User: Ra-m-ha
 * Date: 02.08.2019
 * Time: 3:01
 */

namespace api\models\validate;

use common\models\UserAccessToken;
use common\models\UserAvatar;
use cs\services\Security;

class AuthControllerValidateAccessToken extends \yii\base\Model
{
    public $access_token;

    /** @var  \common\models\UserAccessToken */
    public $user_access_token;

    public function rules()
    {
        return [
            ['access_token', 'required'],
            ['access_token', 'string', 'length' => 64],
            ['access_token', 'validateToken'],
        ];
    }

    public function validateToken($attribute, $params)
    {
        if (!$this->hasErrors()) {
            $uat = UserAccessToken::findOne(['token' => $this->access_token]);
            if (is_null($uat)) {
                $this->addError($attribute, 'Токен не найден');
                return;
            }
            $this->user_access_token = $uat;
        }
    }

    public function action()
    {
        return $this->user_access_token->expire <= time();
    }
}