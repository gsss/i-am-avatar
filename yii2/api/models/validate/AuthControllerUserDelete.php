<?php
/**
 * Created by PhpStorm.
 * User: Ra-m-ha
 * Date: 02.08.2019
 * Time: 3:01
 */

namespace api\models\validate;

use common\models\UserAccessToken;
use common\models\UserAvatar;
use cs\services\Security;
use cs\services\VarDumper;
use common\models\UserRoot;
use common\models\school\UserLink;

class AuthControllerUserDelete extends \yii\base\Model
{
    public $email;

    /** @var \common\models\UserAvatar  */
    public $user;

    public function rules()
    {
        return [
            ['email', 'required'],
            ['email', 'string'],
            ['email', 'trim'],
            ['email', 'toLower'],
            ['email', 'email'],
            ['email', 'validateUser'],
        ];
    }

    public function validateUser($attribute, $params)
    {
        if (!$this->hasErrors()) {
            try {
                \common\models\UserAvatar::findOne(['email' => $this->login]);
            } catch (\Exception $e) {
                $this->addError($attribute, 'Нет такого пользователя');
                return;
            }
        }
    }

    public function action()
    {
        $this->user->delete();

        return true;
    }
}