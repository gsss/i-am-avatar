<?php
/**
 * Created by PhpStorm.
 * User: Ra-m-ha
 * Date: 02.08.2019
 * Time: 3:01
 */

namespace api\models\validate;

use common\models\UserAvatar;
use cs\services\Security;

class KursControllerList extends \yii\base\Model
{
    public $offset;
    public $limit;
    public $sort;


    public function rules()
    {
        return [
            ['offset', 'string'],
            ['limit', 'string'],
            ['sort', 'string'],
        ];
    }

    public function ValidateUser($attribute, $params)
    {
        if (!$this->hasErrors()) {
            try {
                $user = UserAvatar::findByUsername($this->login);
            } catch (\Exception $e) {
                $this->addError($attribute, 'Пользователь не найден');
                return;
            }

            $this->user = $user;
        }
    }

    public function ValidatePassword($attribute, $params)
    {
        if (!$this->hasErrors()) {
            if (!$this->user->validatePassword($this->password)) {
                $this->addError($attribute, 'Пароль не верный');
                return;
            }
        }
    }

    public function action()
    {
        $access_token = Security::generateRandomString(64);
        $this->user->access_token = $access_token;
        $this->user->save();

        return ['access_token' => $access_token, 'token_type' => 'Bearer', 'expire' => time() + 60*60*24*150];
    }
}