<?php
/**
 * Created by PhpStorm.
 * User: Ra-m-ha
 * Date: 02.08.2019
 * Time: 3:01
 */

namespace api\models\validate;

use common\models\Card;
use common\models\CardDesign;
use common\models\UserAccessToken;
use common\models\UserAvatar;
use cs\services\Security;
use cs\services\Url;
use cs\services\VarDumper;

class QrControllerGet extends \yii\base\Model
{
    public $data;

    /** @var  UserAvatar */
    public $user;

    /** @var  \common\models\Card */
    public $card;

    public function rules()
    {
        return [
            ['data', 'required'],
            ['data', 'string'],
            ['data', 'ValidateCard'],
        ];
    }

    public function ValidateCard($attribute, $params)
    {
        if (!$this->hasErrors()) {
            try {
                $urlObject = new Url($this->data);
            } catch (\Exception $e) {
                $this->addError($attribute, 'Ошибка в распознавании URL');
                return;
            }

            if (!isset($urlObject->params['card'])) {
                $this->addError($attribute, 'Не указан обязательный параметр card в URL');
                return;
            }

            $number = $urlObject->params['card'];
            $card = Card::findOne(['number' => $number]);
            if (is_null($card)) {
                $this->addError($attribute, 'Карта не найдена');
                return;
            }
            $this->card = $card;

            $user = null;
            if (!is_null($card->user_id)) {
                $user = UserAvatar::findOne($card->user_id);
            }

            $this->user = $user;
        }
    }

    public function action()
    {
        $return = [
            'card'  => $this->card->getApiFields(),
        ];
        $design = CardDesign::findOne($this->card->design_id);
        $return['card']['design'] = $design->getApiFields();
        if (!is_null($this->user)) {
            $return['user'] = $this->user->getApiFields();
        }

        return $return;
    }
}