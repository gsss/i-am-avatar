<?php
/**
 * Created by PhpStorm.
 * User: Ra-m-ha
 * Date: 02.08.2019
 * Time: 3:01
 */

namespace api\models\validate;

use common\models\school\School;
use common\models\UserAccessToken;
use common\models\UserAvatar;
use cs\Application;
use cs\services\Security;
use cs\services\VarDumper;

class AuthControllerLogin extends \yii\base\Model
{
    public $login;
    public $password;
    public $company_id;

    /** @var  \common\models\school\School */
    public $school;

    /** @var  UserAvatar */
    public $user;

    public function rules()
    {
        return [
            ['login', 'required'],
            ['login', 'email'],
            ['login', 'ValidateUser'],
            ['login', 'Validate2fa'],

            ['company_id', 'required'],
            ['company_id', 'integer'],
            ['company_id', 'default', 'value' => 20],
            ['company_id', 'ValidateSchool'],

            ['password', 'required'],
            ['password', 'string'],
            ['password', 'ValidatePassword'],
        ];
    }

    public function ValidateUser($attribute, $params)
    {
        if (!$this->hasErrors()) {
            try {
                $user = UserAvatar::findByUsername($this->login);
            } catch (\Exception $e) {
                $this->addError($attribute, 'Пользователь не найден');
                return;
            }

            $this->user = $user;
        }
    }

    public function Validate2fa($attribute, $params)
    {
        if (!$this->hasErrors()) {
            if (!Application::isEmpty($this->user->google_auth_code)) {
                $this->addError($attribute, 'У пользователя включена 2FA');
                return;

            }
        }
    }

    public function ValidateSchool($attribute, $params)
    {
        if (!$this->hasErrors()) {
            $s = School::findOne($this->company_id);
            if (is_null($s)) {
                $this->addError($attribute, 'Школа не найдена');
                return;
            }

            $this->school = $s;
        }
    }

    public function ValidatePassword($attribute, $params)
    {
        if (!$this->hasErrors()) {
            if (!$this->user->validatePassword($this->password)) {
                $this->addError($attribute, 'Пароль не верный');
                return;
            }
        }
    }

    public function action($ttl = 60 * 60 * 24 * 150)
    {
        $access_token = Security::generateRandomString(64);
        $refresh_token = Security::generateRandomString(20);
        $expire = time() + $ttl;
        $school_id = $this->company_id;

        UserAccessToken::deleteAll([
            'user_id'       => $this->user->id,
            'school_id'     => $school_id,
        ]);

        $UserAccessToken = UserAccessToken::add([
            'token'         => $access_token,
            'expire'        => $expire,
            'user_id'       => $this->user->id,
            'school_id'     => $school_id,
            'refresh_token' => $refresh_token,
        ]);

        return [
            'access_token'  => $access_token,
            'token_type'    => 'Bearer',
            'expire'        => $expire,
            'refresh_token' => $refresh_token,
        ];
    }
}