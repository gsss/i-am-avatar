<?php
require(__DIR__ . '/../vendor/autoload.php');
require(__DIR__ . '/../vendor/yiisoft/yii2/Yii.php');
require(__DIR__ . '/../common/config/bootstrap.php');

require('../avatar-bank/services/upload/extras/Uploader.php');
require('../public_html_php/db.php');



$upload_dir = '/home/god/i-am-avatar/www/public_html/upload/cloud/';

$Upload = new FileUpload('imgfile');
$ext = $Upload->getExtension(); // Get the extension of the uploaded file
$Upload->newFileName = time() . '_' . substr(str_shuffle('abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890'), 0, 10) . '.' . $ext;
$result = $Upload->handleUpload($upload_dir);

if (!$result) {
    echo json_encode(['success' => false, 'msg' => $Upload->getErrorMsg()]);
} else {
    // TODO: добавить файл в БД
    $fileName = $Upload->getFileName();
    if (isset($_GET['id'])) {
        addFile('/upload/cloud/' . $fileName, $_GET['id']);
    }
    echo json_encode(['success' => true, 'file' => $fileName]);
}