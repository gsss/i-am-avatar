<?php

namespace school\modules\sroutes\controllers;

use common\models\avatar\Currency;
use common\models\avatar\UserBill;
use common\models\avatar\UserBillMerchant;
use common\models\BillingMain;
use school\modules\sroutes\models\Link;
use school\modules\sroutes\models\Deal;
use school\modules\sroutes\models\DealStatus;
use common\models\investment\IcoRequest;
use common\models\investment\IcoRequestSuccess;
use common\models\PaySystemConfig;
use common\models\piramida\Billing;
use common\models\piramida\Wallet;
use common\models\UserAvatar;
use common\services\Security;
use cs\Application;
use cs\base\BaseController;
use cs\services\UploadFolderDispatcher;
use cs\services\VarDumper;
use cs\web\Exception;
use Yii;
use yii\base\UserException;
use yii\db\Connection;
use yii\db\Query;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use yii\helpers\FileHelper;
use yii\web\ErrorAction;
use yii\web\Response;

class CoinExplorerController extends \avatar\controllers\CoinExplorerController
{
    public $layout = 'cabinet';
}