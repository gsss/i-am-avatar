<?php

namespace school\modules\sroutes\controllers;

use common\models\avatar\Currency;
use common\models\avatar\UserBill;
use common\models\avatar\UserBillMerchant;
use common\models\BillingMain;
use school\modules\sroutes\models\Link;
use school\modules\sroutes\models\Deal;
use school\modules\sroutes\models\DealStatus;
use common\models\investment\IcoRequest;
use common\models\investment\IcoRequestSuccess;
use common\models\PaySystemConfig;
use common\models\piramida\Billing;
use common\models\piramida\Wallet;
use common\models\UserAvatar;
use common\services\Security;
use cs\Application;
use cs\base\BaseController;
use cs\services\UploadFolderDispatcher;
use cs\services\VarDumper;
use cs\web\Exception;
use Yii;
use yii\base\UserException;
use yii\db\Connection;
use yii\db\Query;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use yii\helpers\FileHelper;
use yii\web\ErrorAction;
use yii\web\Response;

class CabinetWalletController extends \avatar\controllers\CabinetWalletController
{
    public $layout = 'cabinet';

    public function actionIndex()
    {
        return $this->render();
    }

    public function actionItem($id)
    {
        try {
            $bill = UserBill::findOne($id);
            if ($bill->user_id != Yii::$app->user->id) {
                return $this->render('@avatar/views/site/error', ['name' => 'Ошибка', 'message' => 'Это не ваш счет']);
            }
        } catch (\Exception $e) {
            throw new Exception('Счет не найден');
        }

        return $this->render(['bill' => $bill]);
    }

    public function actionSend($id)
    {
        try {
            $bill = UserBill::findOne($id);
            if ($bill->user_id != Yii::$app->user->id) {
                return $this->render('@avatar/views/site/error', ['name' => 'Ошибка', 'message' => 'Это не ваш счет']);
            }
        } catch (\Exception $e) {
            throw new Exception('Счет не найден');
        }

        $model = new \avatar\models\forms\SendAtom2();

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $t = $model->action();
            Yii::$app->session->setFlash('form', $t->getAddress());
        }

        return $this->render([
            'bill'   => $bill,
            'model'  => $model,
        ]);
    }

    public function actionAdd()
    {
        $model = new \avatar\models\validate\CabinetWalletControllerActionAdd();

        if (!$model->load(Yii::$app->request->post(), '')) {
            return self::jsonErrorId(400, 'Не загружены данные');
        }
        if (!$model->validate()) {
            return self::jsonErrorId(102, $model->convert($model->errors));
        }
        $userBill = $model->action();

        return self::jsonSuccess($userBill);
    }

    public function actionDelete()
    {
        $model = new \avatar\models\validate\CabinetWalletControllerDelete();

        if (!$model->load(Yii::$app->request->post(), '')) {
            return self::jsonErrorId(400, 'Не загружены данные');
        }
        if (!$model->validate()) {
            return self::jsonErrorId(103, $model->errors);
        }
        $result = $model->action();
        if (!$result) return self::jsonErrorId(2, 'Кошелек не пустой');

        return self::jsonSuccess();
    }

    public function actionDeleteAgree()
    {
        $model = new \avatar\models\validate\CabinetWalletControllerDeleteAgree();

        if (!$model->load(Yii::$app->request->post(), '')) {
            return self::jsonErrorId(400, 'Не загружены данные');
        }
        if (!$model->validate()) {
            return self::jsonErrorId(103, $model->errors);
        }
        $model->action();

        return self::jsonSuccess();
    }
}