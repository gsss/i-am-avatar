<?php

namespace school\modules\sroutes\controllers;

use app\models\Form\Union;
use app\models\HD;
use app\models\HDtown;
use app\models\SiteUpdate;
use app\models\User;
use app\services\Subscribe;
use cs\Application;
use cs\services\SitePath;
use cs\services\VarDumper;
use cs\web\Exception;
use Yii;
use yii\base\UserException;
use yii\filters\AccessControl;
use yii\helpers\StringHelper;
use yii\helpers\Url;
use yii\web\Controller;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use app\models\NewsItem;
use cs\base\BaseController;
use yii\web\Response;
use yii\widgets\ActiveForm;

class AdminBaseController extends Controller
{
    public $layout = 'cabinet';

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow'   => true,
                        'roles'   => ['role_admin'],
                    ],
                ],
            ],
        ];
    }

    /**
     * Возвращает стандартный ответ JSON при положительном срабатывании
     * https://redmine.suffra.com/projects/suffra/wiki/Стандартный_ответ_JSON
     *
     * @param mixed $data [optional] возвращаемые данные
     *
     * @return \yii\web\Response json
     */
    public static function jsonSuccess($data = null)
    {
        $ret = [
            'success' => true,
        ];
        if (!is_null($data)) {
            $ret['data'] = $data;
        };

        return self::json($ret);
    }

    /**
     * Возвращает стандартный ответ JSON при отрицательном срабатывании
     * https://redmine.suffra.com/projects/suffra/wiki/Стандартный_ответ_JSON
     *
     * @param mixed $data [optional] возвращаемые данные
     *
     * @return \yii\web\Response json
     */
    public static function jsonError($data = null)
    {
        if (is_null($data)) $data = '';

        return self::json([
            'success' => false,
            'data'    => $data,
        ]);
    }

    /**
     * Возвращает стандартный ответ JSON при отрицательном срабатывании
     * https://redmine.suffra.com/projects/suffra/wiki/Стандартный_ответ_JSON
     *
     * @param integer $id   идентификатор ошибки
     * @param mixed   $data [optional] возвращаемые данные
     *
     * @return \yii\web\Response json
     */
    public static function jsonErrorId($id, $data = null)
    {
        $return = [
            'id' => $id,
        ];
        if (!is_null($data)) $return['data'] = $data;

        return self::jsonError($return);
    }


    /**
     * Закодировать в JSON
     *
     * @return \yii\web\Response json
     * */
    public static function json($array)
    {
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        Yii::$app->response->data = $array;

        return Yii::$app->response;
    }

    /**
     * Вызов возможен как render($view, $params)
     * или как render($params)
     * тогда $view = название функции action
     * например если вызов произошел из метода actionOrders то $view = 'orders'
     *
     * @param string|array $view   шаблон или параметры шаблона
     * @param array        $params параметры шаблона если $view = шаблон, иначе не должен указываться
     *
     * @return string = \yii\base\Controller::render()
     */
    public function render($view = '', $params = [])
    {
        if (is_array($view)) {
            $params = $view;
            $view = Yii::$app->controller->action->id;
        } else if ($view == '') {
            $params = [];
            $view = Yii::$app->controller->action->id;
        }


        if (self::getParam('_view', '') != '') {
            \cs\services\VarDumper::dump($params, 10);

            return '';
        }

        return parent::render($view, $params);
    }


    /**
     * Возвращает переменную из REQUEST
     *
     * @param string $name    имя переменной
     * @param mixed  $default значние по умолчанию [optional]
     *
     * @return string|null
     * Если такой переменной нет, то будет возвращено null
     */
    public static function getParam($name, $default = null)
    {
        $vGet = \Yii::$app->request->get($name);
        $vPost = \Yii::$app->request->post($name);
        $value = (is_null($vGet)) ? $vPost : $vGet;

        return (is_null($value)) ? $default : $value;
    }

}
