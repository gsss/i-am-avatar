<?php
/**
 * Created by PhpStorm.
 * User: god
 * Date: 06.08.2017
 * Time: 1:17
 */

namespace school\modules\sroutes\controllers;


use avatar\models\forms\CabinetTokenAdd;
use avatar\models\forms\CabinetTokenBurn;
use avatar\models\validate\CabinetTokenControllerNew;
use avatar\models\validate\CabinetTokenControllerNewCheck;
use avatar\models\WalletToken;
use common\components\providers\ETH;
use common\models\avatar\Currency;
use common\models\koop\RequestPayIn;
use common\models\RequestTokenCreate;
use common\models\UserAvatar;
use common\models\UserToken;
use cs\services\File;
use cs\services\VarDumper;
use cs\web\Exception;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;
use yii\helpers\StringHelper;
use Yii;

class CabinetPifController extends \avatar\controllers\CabinetBaseController
{
    public function actionIndex()
    {
        $model = new \school\modules\sroutes\forms\PifParams();

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $model->action();
        }

        return $this->render('step2', [
            'model' => $model
        ]);
    }

    public function actionStep2()
    {
        $model = new \school\modules\sroutes\forms\PifParams();

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $model->action();
        }

        return $this->render([
            'model' => $model
        ]);
    }

}