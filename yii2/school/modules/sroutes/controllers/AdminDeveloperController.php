<?php

namespace school\modules\sroutes\controllers;

use cs\services\VarDumper;
use cs\web\Exception;
use Yii;
use yii\base\UserException;

class AdminDeveloperController extends AdminBaseController
{
    public function actions()
    {
        $rows = [];
        $path = dir(Yii::getAlias('@school/modules/sroutes/views/admin-developer'));
        while (($file = $path->read()) !== false) {
            if (!in_array($file, ['.', '..'])) {
                $key = substr($file, 0, strlen($file) - 4);
                $rows[$key] = '\avatar\controllers\actions\AdminDeveloperAction';
            }
        }
        $path->close();

        return $rows;
    }
}
