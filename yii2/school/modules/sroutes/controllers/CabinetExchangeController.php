<?php

namespace school\modules\sroutes\controllers;

use avatar\controllers\AdminBaseController;
use avatar\models\forms\BlogItem;
use common\models\avatar\Currency;
use common\models\avatar\UserBill;
use school\modules\sroutes\models\Assessment;
use school\modules\sroutes\models\Deal;
use school\modules\sroutes\models\DealStatus;
use school\modules\sroutes\models\Offer;
use common\models\investment\IcoRequest;
use common\models\investment\IcoRequestAddress;
use common\models\investment\IcoRequestSuccess;
use common\models\investment\Project;
use common\models\investment\ProjectIco;
use common\models\MapBankomat;
use common\models\PaySystemConfig;
use common\models\PaySystemListItem;
use common\models\piramida\Wallet;
use cs\Application;
use cs\services\VarDumper;
use cs\web\Exception;
use Yii;
use yii\base\UserException;
use yii\filters\AccessControl;
use yii\helpers\Json;
use yii\helpers\StringHelper;
use yii\helpers\Url;
use yii\httpclient\Client;
use yii\web\Controller;
use yii\filters\VerbFilter;
use cs\base\BaseController;
use yii\web\Response;
use yii\widgets\ActiveForm;

class CabinetExchangeController extends \avatar\controllers\CabinetBaseController
{
    public function actionIndex()
    {
        return $this->render([
        ]);
    }

    public function actionOffer()
    {
        return $this->render([
        ]);
    }

    public function actionMyDeals()
    {
        return $this->render([]);
    }

    public function actionOfferAdd()
    {
        $model = new \school\modules\sroutes\forms\OfferAdd();

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $offer = $model->action();
            Yii::$app->session->setFlash('form', $offer->id);
        }

        return $this->render([
            'model' => $model,
        ]);
    }

    public function actionDealOpen($id)
    {
        $Offer = Offer::findOne($id);
        if (is_null($Offer)) throw new \Exception('Не найден Offer ' . $id);

        $model = new \school\modules\sroutes\forms\DealOpen([
            'offer' => $Offer,
        ]);

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $deal = $model->action();

            return $this->redirect(['cabinet-exchange/deal-action', 'id' => $deal->id]);
        }

        return $this->render([
            'model' => $model,
            'offer' => $Offer,
        ]);
    }

    public function actionDealAction($id)
    {
        $Deal = Deal::findOne($id);
        if (is_null($Deal)) throw new \Exception('Не найден Deal ' . $id);

        return $this->render([
            'Deal' => $Deal,
        ]);
    }



    public function actionSellOpenAccept($id)
    {
        $Deal = Deal::findOne($id);
        if (is_null($Deal)) throw new \Exception('Не найден Deal ' . $id);

        $t = Yii::$app->db->beginTransaction();
        try {
            $Deal->trigger(Deal::EVENT_BEFORE_BLOCK);

            $Deal->status = Deal::STATUS_BLOCK;
            $Deal->save();
            DealStatus::add(['status' => Deal::STATUS_BLOCK, 'deal_id' => $Deal->id]);

            // Блокирую деньги
            {
                // Ищу кошелек
                $offer = $Deal->getOffer();
                $userBill = UserBill::findOne(['currency' => Currency::VVB, 'user_id' => $offer->user_id, 'mark_deleted' => 0]);
                $wallet = Wallet::findOne($userBill->address);

                // получение дочернего модуля с идентификатором "forum"
                $module = \Yii::$app->getModule('sroutes');
                $walletEscrow = $module->params['walletEscrow'];

                // делаю перевод для блокировки
                $transactionBlock = $wallet->move($walletEscrow, $Deal->volume_vvb, 'блокировка средств от сделки ' . $Deal->id . ' от пользователя ' . $offer->user_id . ' с кошелька ' . $wallet->id);
            }

            $Deal->trigger(Deal::EVENT_AFTER_BLOCK);

            $t->commit();
        }catch ( \Exception $e) {
            $t->rollBack();
            throw new \Exception($e);
        }


        return self::jsonSuccess();
    }

    public function actionSellMoneySended($id)
    {
        $Deal = Deal::findOne($id);
        if (is_null($Deal)) throw new \Exception('Не найден Deal ' . $id);

        $t = Yii::$app->db->beginTransaction();
        try {
            $Deal->trigger(Deal::EVENT_BEFORE_MONEY_SENDED);

            $Deal->status = Deal::STATUS_MONEY_SENDED;
            $Deal->save();
            DealStatus::add(['status' => Deal::STATUS_MONEY_SENDED, 'deal_id' => $Deal->id]);

            $Deal->trigger(Deal::EVENT_AFTER_MONEY_SENDED);

            $t->commit();
        }catch ( \Exception $e) {
            $t->rollBack();
            throw new \Exception($e);
        }


        return self::jsonSuccess();
    }

    public function actionSellMoneyReceived($id)
    {
        $Deal = Deal::findOne($id);
        if (is_null($Deal)) throw new \Exception('Не найден Deal ' . $id);

        $t = Yii::$app->db->beginTransaction();
        try {
            $Deal->trigger(Deal::EVENT_BEFORE_MONEY_SENDED);

            $Deal->status = Deal::STATUS_MONEY_RECEIVED;
            $Deal->save();
            DealStatus::add(['status' => Deal::STATUS_MONEY_RECEIVED, 'deal_id' => $Deal->id]);

            // Разблокирую деньги
            {
                // получение дочернего модуля с идентификатором "forum"
                $module = \Yii::$app->getModule('sroutes');
                $walletEscrow = $module->params['walletEscrow'];

                // Ищу кошелек
                $userBill = UserBill::findOne(['currency' => Currency::VVB, 'user_id' => $Deal->user_id, 'mark_deleted' => 0]);
                $wallet = Wallet::findOne($userBill->address);
                $walletFrom = Wallet::findOne($walletEscrow);

                // делаю перевод для разблокировки
                $transactionUnBlock = $walletFrom->move($wallet, $Deal->volume_vvb, 'разблокировка средств от сделки ' . $Deal->id . ' в пользу пользователя ' . $Deal->user_id . ' в кошелек ' . $wallet->id);
            }

            // Завершаю сделку (вычитает из предложения объем сделки)
            $Deal->finish();

            $Deal->trigger(Deal::EVENT_AFTER_MONEY_SENDED);

            $t->commit();
        }catch ( \Exception $e) {
            $t->rollBack();
            throw new \Exception($e);
        }


        return self::jsonSuccess();
    }

    public function actionBuyOpenAccept($id)
    {
        $Deal = Deal::findOne($id);
        if (is_null($Deal)) throw new \Exception('Не найден Deal ' . $id);

        $t = Yii::$app->db->beginTransaction();
        try {
            $Deal->trigger(Deal::EVENT_BEFORE_BLOCK);

            $Deal->status = Deal::STATUS_BLOCK;
            $Deal->save();
            DealStatus::add(['status' => Deal::STATUS_BLOCK, 'deal_id' => $Deal->id]);

            // Блокирую деньги
            {
                // Ищу кошелек
                $userBill = UserBill::findOne(['currency' => Currency::VVB, 'user_id' => $Deal->user_id, 'mark_deleted' => 0]);
                $walletVVB = Wallet::findOne($userBill->address);

                // получение дочернего модуля с идентификатором "forum"
                $module = \Yii::$app->getModule('sroutes');
                $walletEscrow = $module->params['walletEscrow'];

                // делаю перевод для блокировки
                $transactionBlock = $walletVVB->move($walletEscrow, $Deal->volume_vvb, 'блокировка средств от сделки ' . $Deal->id . ' от пользователя ' . $Deal->user_id . ' с кошелька ' . $walletVVB->id);
            }

            $Deal->trigger(Deal::EVENT_AFTER_BLOCK);

            $t->commit();
        }catch ( \Exception $e) {
            $t->rollBack();
            throw new \Exception($e);
        }


        return self::jsonSuccess();
    }

    public function actionBuyMoneySended($id)
    {
        return $this->actionSellMoneySended($id);
    }

    public function actionBuyMoneyReceived($id)
    {
        $Deal = Deal::findOne($id);
        if (is_null($Deal)) throw new \Exception('Не найден Deal ' . $id);

        $t = Yii::$app->db->beginTransaction();
        try {
            $Deal->trigger(Deal::EVENT_BEFORE_MONEY_SENDED);

            $Deal->status = Deal::STATUS_MONEY_RECEIVED;
            $Deal->save();
            DealStatus::add(['status' => Deal::STATUS_MONEY_RECEIVED, 'deal_id' => $Deal->id]);

            // Разблокирую деньги
            {
                // получение дочернего модуля с идентификатором "forum"
                $module = \Yii::$app->getModule('sroutes');
                $walletEscrow = $module->params['walletEscrow'];

                // Ищу кошелек
                $offer = $Deal->getOffer();
                $userBill = UserBill::findOne(['currency' => Currency::VVB, 'user_id' => $offer->user_id, 'mark_deleted' => 0]);
                $walletVVB = Wallet::findOne($userBill->address);
                $walletFrom = Wallet::findOne($walletEscrow);

                // делаю перевод для разблокировки
                $transactionUnBlock = $walletFrom->move($walletVVB, $Deal->volume_vvb, 'разблокировка средств от сделки ' . $Deal->id . ' в пользу пользователя ' . $offer->user_id . ' в кошелек ' . $walletVVB->id);
            }
            // Завершаю сделку (вычитает из предложения объем сделки)
            $Deal->finish();

            $Deal->trigger(Deal::EVENT_AFTER_MONEY_SENDED);

            $t->commit();
        } catch (\Exception $e) {
            $t->rollBack();
            throw new \Exception($e);
        }

        return self::jsonSuccess();
    }

    /**
     * Обрабатывает кнопку "Пригласить арбитра"
     *
     * @param $id
     *
     * @return Response
     * @throws \Exception
     */
    public function actionInviteArbitrator($id)
    {
        $Deal = Deal::findOne($id);
        if (is_null($Deal)) throw new \Exception('Не найден Deal ' . $id);

        $t = Yii::$app->db->beginTransaction();
        try {
            $Deal->trigger(Deal::EVENT_BEFORE_AUDIT_WAIT);

            $Deal->status = Deal::STATUS_AUDIT_WAIT;
            $Deal->save();
            DealStatus::add(['status' => Deal::STATUS_AUDIT_WAIT, 'deal_id' => $Deal->id]);

            $Deal->trigger(Deal::EVENT_AFTER_AUDIT_WAIT);

            $t->commit();
        }catch ( \Exception $e) {
            $t->rollBack();
            throw new \Exception($e);
        }


        return self::jsonSuccess();
    }

    public function actionSellAssessmentDown($id)
    {
        return $this->setAssessment($id, -1);
    }

    public function actionSellAssessmentNormal($id)
    {
        return $this->setAssessment($id, 0);
    }

    public function actionSellAssessmentUp($id)
    {
        return $this->setAssessment($id, 1);
    }

    public function actionBuyAssessmentDown($id)
    {
        return $this->setAssessment($id, -1);
    }

    public function actionBuyAssessmentNormal($id)
    {
        return $this->setAssessment($id, 0);
    }

    public function actionBuyAssessmentUp($id)
    {
        return $this->setAssessment($id, 1);
    }

    /**
     * @param int $id идентификатор сделки
     * @param int $value оценка
     *
     * @return Response
     * @throws \Exception
     */
    private function setAssessment($id, $value)
    {
        $Deal = Deal::findOne($id);
        if (is_null($Deal)) throw new \Exception('Не найден Deal ' . $id);

        $t = Yii::$app->db->beginTransaction();
        try {
            $Deal->trigger(Deal::EVENT_BEFORE_CLOSE);

            // Добавляю оценку
            Assessment::add([
                'deal_id' => $Deal->id,
                'value'   => $value,
                'user_id' => $Deal->getOffer()->user_id,
            ]);

            // Устанавливаю статус
            $Deal->status = Deal::STATUS_CLOSE;
            $Deal->save();
            DealStatus::add(['status' => Deal::STATUS_CLOSE, 'deal_id' => $Deal->id]);

            $Deal->trigger(Deal::EVENT_AFTER_CLOSE);

            $t->commit();
        }catch ( \Exception $e) {
            $t->rollBack();
        }


        return self::jsonSuccess();
    }

}
