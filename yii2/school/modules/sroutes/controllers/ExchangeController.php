<?php

namespace school\modules\sroutes\controllers;

use app\models\Article;
use app\models\SiteUpdate;
use app\services\Subscribe;
use avatar\models\forms\Contact;
use avatar\services\LogReader;
use common\components\providers\ETH;
use common\components\sms\IqSms;
use common\models\avatar\Currency;
use common\models\avatar\UserBill;
use common\models\CompanyCustomizeItem;
use common\models\PaymentBitCoin;
use common\models\UserAvatar;
use common\models\UserRegistration;
use common\payment\BitCoinBlockTrailPayment;
use cs\Application;
use cs\base\BaseController;
use cs\services\UploadFolderDispatcher;
use cs\services\VarDumper;
use cs\web\Exception;
use Yii;
use yii\base\UserException;
use yii\db\Connection;
use yii\db\Query;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use yii\helpers\FileHelper;
use yii\helpers\Url;
use yii\web\ForbiddenHttpException;
use yii\web\Response;

class ExchangeController extends \avatar\base\BaseController
{


    /**
     */
    public function actionIndex()
    {
        return $this->render();
    }

    public function actionIndexAjax()
    {
        $model = new \school\modules\sroutes\search\Offer();
        $where = null;
        if (!Yii::$app->user->isGuest) {
            $where = ['not', ['user_id' => Yii::$app->user->id]];
        }
        $dataProvider = $model->search(['defaultOrder' => ['created_at' => SORT_DESC]], Yii::$app->request->post(), $where);
        $html = $this->view->renderAjax('@avatar/views/cabinet-exchange/index-table.php', ['ActiveDataProvider' => $dataProvider]);

        return self::jsonSuccess(['html' => $html]);
    }

}
