<?php

namespace school\modules\sroutes\controllers;

use avatar\modules\UniSender\UniSender;
use common\components\providers\ETH;
use common\models\avatar\Currency;
use common\models\avatar\UserBill;
use common\models\avatar\UserBillDefault;
use common\models\HD;
use common\models\HDtown;
use common\models\school\PotokUser3Link;
use common\models\school\School;
use common\models\subscribe\UserSubscribe;
use common\models\UserAvatar;
use common\models\UserDocument;
use common\models\UserMaster;
use common\models\UserTelegramConnect;
use common\models\UserTelegramTemp;
use common\services\Security\AES;
use cs\base\BaseController;
use cs\services\SitePath;
use cs\services\UploadFolderDispatcher;
use cs\services\VarDumper;
use cs\web\Exception;
use Yii;
use yii\base\UserException;
use yii\bootstrap\ActiveForm;
use yii\db\Connection;
use yii\db\Query;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use yii\helpers\FileHelper;
use yii\helpers\StringHelper;
use yii\web\Response;

class CabinetController extends \avatar\controllers\CabinetController
{
    public $layout = 'cabinet';

    public function actions()
    {
        $parent = parent::actions();
        $t = [
            'file-upload7-save' => '\common\widgets\FileUpload7\actionFileUpload7Save'
        ];

        return ArrayHelper::merge($parent, $t);
    }


    /**
     */
    public function actionIndex()
    {
        return $this->render();
    }

    /**
     */
    public function actionBall()
    {
        return $this->render();
    }

    /**
     */
    public function actionZaim()
    {
        return $this->render();
    }

    /**
     */
    public function actionChain()
    {
        return $this->render();
    }

    /**
     */
    public function actionToken()
    {
        return $this->render();
    }

    /**
     */
    public function actionPacket()
    {
        return $this->render();
    }

    /**
     */
    public function actionReferalLink()
    {
        return $this->render();
    }

    /**
     */
    public function actionCardAction()
    {
        $model = new \avatar\models\forms\CardAction();

        $model->load(Yii::$app->request->post()) && $model->validate();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $model->action();
            Yii::$app->session->setFlash('form', 1);
        }

        return $this->render(['model' => $model]);
    }

    /**
     */
    public function actionReferalTransactions()
    {
        return $this->render();
    }

    /**
     */
    public function actionReferalStructure()
    {
        return $this->render();
    }

    /**
     */
    public function actionShopRequests()
    {
        return $this->render(['school' => School::get()]);
    }

    /**
     */
    public function actionShopRequestsItem($id)
    {
        $request = \common\models\shop\Request::findOne($id);
        $school = School::findOne($request->school_id);
        if ($request->user_id != Yii::$app->user->id) {
            throw new Exception('Это не ваш заказ');
        }

        return $this->render([
            'school'  => $school,
            'request' => $request,
        ]);
    }

    /**
     */
    public function actionShopRequestsItemPaid($id)
    {
        $request = \common\models\shop\Request::findOne($id);
        $school = School::findOne($request->school_id);
        $request->successClient();

        return self::jsonSuccess();
    }
    /**
     * REQUEST:
     * - value: 0/1
     */
    public function actionModalMasterSetValue()
    {
        $value = self::getParam('value');
        if (!in_array($value, ['0', '1'])) {
            throw new \Exception('Не верное значение');
        }

        /** @var \common\models\UserAvatar $user */
        $user = Yii::$app->user->identity;
        $user->is_hide_master_enter_window = $value;
        $user->save();

        return self::jsonSuccess();
    }

    /**
     */
    public function actionModalMaster()
    {
        return $this->render();
    }

    /**
     */
    public function actionCatalogMaster()
    {
        $model = \avatar\models\forms\UserMaster::findOne(['user_id' => Yii::$app->user->id]);
        if (is_null($model)) {
            $model = new \avatar\models\forms\UserMaster();
        }

        if ($model->load(Yii::$app->request->post()) && ($model->isNewRecord ? $model->insert() : $model->update())) {
            Yii::$app->session->setFlash('form', 1);
            if (is_null($model->user_id)) {
                $i = UserMaster::findOne($model->id);
                $i->user_id = Yii::$app->user->id;
                $i->save();
            }
        }

        return $this->render([
            'model' => $model,
        ]);
    }

    /**
     * REQUEST:
     * - value: 0/1
     */
    public function actionCatalogMasterSetValue()
    {
        $value = self::getParam('value');
        if (!in_array($value, ['0', '1'])) {
            throw new \Exception('Не верное значение');
        }

        /** @var \common\models\UserAvatar $user */
        $user = Yii::$app->user->identity;
        $user->is_master = $value;
        $user->save();

        return self::jsonSuccess();
    }

    /**
     * Возвращает картинку QR кода
     * AJAX
     * REQUEST:
     * - address - string - адрес
     *
     * @return string json
     */
    public function actionGetQrCode()
    {
        $address = self::getParam('address');
        $content = (new \Endroid\QrCode\QrCode())
            ->setText('bitcoin:' . $address)
            ->setSize(180)
            ->setPadding(0)
            ->setErrorCorrection('high')
            ->setForegroundColor(['r' => 0, 'g' => 0, 'b' => 0, 'a' => 0])
            ->setBackgroundColor(['r' => 255, 'g' => 255, 'b' => 255, 'a' => 0])
            ->setLabelFontSize(16)
            ->setImageType(\Endroid\QrCode\QrCode::IMAGE_TYPE_PNG)
            ->get('png');

        return Yii::$app->response->sendContentAsFile($content, $address . '.' . 'png');
    }

    /**
     */
    public function actionMailTest()
    {
        $model = new \avatar\models\forms\MailTest();

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $model->action();
            Yii::$app->session->setFlash('form', 1);
        }

        return $this->render([
            'model' => $model,
        ]);
    }

    /**
     */
    public function actionTelegram()
    {
        return $this->render();
    }

    /**
     * Присоединяет telegram_username и telegram_chat_id
     */
    public function actionTelegramPin()
    {
        /** @var \common\models\UserAvatar $user */
        $user = Yii::$app->user->identity;

        $temp = UserTelegramTemp::findOne(['email' => $user->email]);
        $connect = UserTelegramConnect::findOne(['username' => $temp->username]);
        if (!is_null($connect)) {
            $connect->delete();
        }
        $user->telegram_username = $temp->username;
        $user->telegram_chat_id = $temp->chat_id;
        $user->save();

        /** @var \aki\telegram\Telegram $telegram */
        $telegram = Yii::$app->telegram;
        $telegram->sendVideo([
            'chat_id' => $user->telegram_chat_id,
            'video'   => Yii::getAlias('@webroot/images/telegram/tumblr_o2j1islebK1tjki5do1_540.gif'),
            'caption' => 'Ура! Получилось, мы соединились с тобой! Я - это Ты! Ты - это Я! Нет меня без Тебя! Я люблю Тебя!',
        ]);
        $temp->delete();

        return self::jsonSuccess();
    }

    /**
     * Отсоединяет telegram_username и telegram_chat_id
     */
    public function actionTelegramUnPin()
    {
        /** @var \common\models\UserAvatar $user */
        $user = Yii::$app->user->identity;

        /** @var \aki\telegram\Telegram $telegram */
        $telegram = Yii::$app->telegram;
        $telegram->sendMessage(['chat_id' => $user->telegram_chat_id, 'text' => 'Я знаю что мы всегда едины, но ты меняешь тела и логины и это нормально, Я отсоединяюсь! До скорых встреч!']);

        $user->telegram_username = null;
        $user->telegram_chat_id = null;
        $user->save();

        return self::jsonSuccess();
    }

    /**
     * Возвращает кол-во записей в документе
     *
     * AJAX
     * REQUEST:
     * - id - int - идентификатор документа в контракте
     *
     * @return string json
     *
     *
     */
    public function actionDocumentsGetSignatureCount()
    {
        $id = self::getParam('id');
        $data = \cs\Application::readContract(
            '0x2fbfb5df17f566f29b25e227402b449e305c182c',
            '[{"constant":true,"inputs":[],"name":"getDocumentsCount","outputs":[{"name":"","type":"uint256"}],"payable":false,"type":"function"},{"constant":false,"inputs":[{"name":"hash","type":"string"},{"name":"link","type":"string"},{"name":"data","type":"string"}],"name":"registerDocument","outputs":[],"payable":false,"type":"function"},{"constant":false,"inputs":[{"name":"id","type":"uint256"}],"name":"addSignature","outputs":[],"payable":false,"type":"function"},{"constant":true,"inputs":[{"name":"id","type":"uint256"}],"name":"getDocument","outputs":[{"name":"hash","type":"string"},{"name":"link","type":"string"},{"name":"data","type":"string"},{"name":"creator","type":"address"},{"name":"date","type":"uint256"},{"name":"count","type":"uint256"}],"payable":false,"type":"function"},{"constant":false,"inputs":[],"name":"kill","outputs":[],"payable":false,"type":"function"},{"constant":true,"inputs":[{"name":"","type":"uint256"}],"name":"documentsIds","outputs":[{"name":"hash","type":"string"},{"name":"link","type":"string"},{"name":"data","type":"string"},{"name":"creator","type":"address"},{"name":"date","type":"uint256"},{"name":"signsCount","type":"uint256"}],"payable":false,"type":"function"},{"constant":true,"inputs":[{"name":"id","type":"uint256"}],"name":"getDocumentSignsCount","outputs":[{"name":"","type":"uint256"}],"payable":false,"type":"function"},{"constant":false,"inputs":[{"name":"newOwner","type":"address"}],"name":"changeOwner","outputs":[],"payable":false,"type":"function"},{"constant":true,"inputs":[{"name":"id","type":"uint256"},{"name":"index","type":"uint256"}],"name":"getDocumentSign","outputs":[{"name":"member","type":"address"},{"name":"date","type":"uint256"}],"payable":false,"type":"function"},{"inputs":[],"payable":false,"type":"constructor"},{"anonymous":false,"inputs":[{"indexed":false,"name":"id","type":"uint256"},{"indexed":false,"name":"member","type":"address"}],"name":"DocumentSigned","type":"event"},{"anonymous":false,"inputs":[{"indexed":false,"name":"id","type":"uint256"},{"indexed":false,"name":"hash","type":"string"}],"name":"DocumentRegistered","type":"event"}]',
            'getDocumentSignsCount',
            [$id]
        );

        return self::jsonSuccess($data);
    }

    public function actionPasswordChange()
    {
        $model = new \avatar\models\forms\PasswordNew();

        if (Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) {
            Yii::$app->response->format = Response::FORMAT_JSON;

            return ActiveForm::validate($model);
        }

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $model->action(\Yii::$app->user->identity);
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        }
        else {
            return $this->render('password-change', [
                'model' => $model,
            ]);
        }
    }

    public function actionSubscribe()
    {
        $model = \avatar\models\forms\Subscribe::findOne(Yii::$app->user->id);

        if ($model->load(Yii::$app->request->post()) && $model->update()) {
            Yii::$app->session->setFlash('form');
            return $this->refresh();
        }

        return $this->render([
            'model' => $model,
        ]);
    }

    /**
     * REQUEST:
     * - name - string (mail#,telegram#)
     * - value - bool
     *
     * @return string|Response
     */
    public function actionSubscribeSet()
    {
        $name = self::getParam('name');
        $value = self::getParam('value');
        $value = $value == 'false' ? false : true;
        $field = '';
        $name = substr($name,3);

        if (StringHelper::startsWith($name,'mail')) {
            $field = 'mail';
            $id = substr($name, strlen($field));
        } else {
            $field = 'telegram';
            $id = substr($name, strlen($field));
        }

        if ($id == 999999) {
            // Рассылка

            /** @var \common\models\school\PotokUser3Link $potokLink */
            $potokLink = PotokUser3Link::findOne(['user_root_id' => Yii::$app->user->identity->user_root_id, 'potok_id' => 2]);
            if (is_null($potokLink)) {
                PotokUser3Link::add([
                    'user_root_id'             => Yii::$app->user->identity->user_root_id,
                    'potok_id'                 => 2,
                    'is_avatar'                => 1,
                    'is_unsubscribed'          => ($field == 'mail') ? ($value ? 0 : 1) : 1,
                    'is_unsubscribed_telegram' => ($field == 'telegram') ? ($value ? 0 : 1) : 1,
                ]);
            } else {
                if ($field == 'mail') {
                    $potokLink->is_unsubscribed = $value ? 0 : 1;
                    $potokLink->save();
                }
                if ($field == 'telegram') {
                    $potokLink->is_unsubscribed_telegram = $value ? 0 : 1;
                    $potokLink->save();
                }
            }
        } else {
            // таблица user_subscribe
            $userSubscribe = UserSubscribe::findOne(['user_id' => Yii::$app->user->id, 'action_id' => $id]);
            if (is_null($userSubscribe)) {
                UserSubscribe::add([
                    'user_id'     => Yii::$app->user->id,
                    'action_id'   => $id,
                    'is_mail'     => ($field == 'mail') ? ($value ? 1 : 0) : 0,
                    'is_telegram' => ($field == 'telegram') ? ($value ? 1 : 0) : 0,
                ]);
            } else {
                if ($field == 'mail') {
                    $userSubscribe->is_mail = $value ? 1 : 0;
                    $userSubscribe->save();
                }
                if ($field == 'telegram') {
                    $userSubscribe->is_telegram = $value ? 1 : 0;
                    $userSubscribe->save();
                }
            }
        }

        return self::jsonSuccess();
    }

    public function actionSubscribe2()
    {
        $model = new \avatar\models\forms\Subscribe2();

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            Yii::$app->session->setFlash('form', 1);
            $model->action();
        }

        return $this->render([
            'model' => $model,
        ]);

    }

    /**
     * Пин код для мобильников
     *
     * @return string|Response
     */
    public function actionPinCodeMobile()
    {
        $model = new \avatar\models\forms\ProfilePin();

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $model->save();
            Yii::$app->session->setFlash('form');

            return $this->refresh();
        } else {
            return $this->render([
                'model' => $model,
            ]);
        }
    }

    public function actionProfile()
    {
        $model = \avatar\models\forms\Profile::findOne(Yii::$app->user->id);
        $model->_school_id = 2;
        $model->_type_id = 18;

        if ($model->load(Yii::$app->request->post()) && ($fields = $model->update())) {
            Yii::$app->session->setFlash('contactFormSubmitted');
        }

        return $this->render([
            'model' => $model,
        ]);
    }


    /**
     * Добавляет документ
     *
     * @return string|Response
     */
    public function actionDocumentsAdd()
    {
        $model = new \avatar\models\forms\UserDocument();

        if ($model->load(Yii::$app->request->post()) && $model->insert()) {
            Yii::$app->session->setFlash('contactFormSubmitted', $model->id);

            $document = UserDocument::findOne($model->id);
            $document->created_at = time();
            $document->hash = hash('sha256', file_get_contents(Yii::getAlias('@webroot' . $document->file)) . $document->data);
            $document->user_id = Yii::$app->user->id;
            $document->save();

            return $this->refresh();
        } else {
            return $this->render('documents-add', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Добавляет документ. Шаг2
     *
     * @return string|Response
     */
    public function actionDocumentsAddStep2()
    {
        $model = new \avatar\models\forms\UserDocumentStep2();

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            Yii::$app->session->setFlash('contactFormSubmitted');
            $model->action();

            return $this->refresh();
        } else {
            return $this->render('documents-add-step2', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Показывает все документы пользователя
     *
     * @return string
     */
    public function actionDocuments()
    {
        return $this->render();
    }

    /**
     * Показывает все документы пользователя
     *
     * @return string
     */
    public function actionRepareWallets()
    {
        $model = new \avatar\models\forms\RepareWallets();

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $model->action();
            Yii::$app->session->setFlash('form', 1);

            return $this->refresh();
        } else {
            return $this->render('repare-wallets', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Показывает все документы пользователя
     *
     * @return string
     * @throws
     */
    public function actionPasswordType()
    {
        /** @var \common\models\UserAvatar $user */
        $user = Yii::$app->user->identity;

        if ($user->password_save_type == UserBill::PASSWORD_TYPE_OPEN) return $this->redirect(['password-type4']);
        if ($user->password_save_type == UserBill::PASSWORD_TYPE_OPEN_CRYPT) return $this->redirect(['password-type4']);
        if ($user->password_save_type == UserBill::PASSWORD_TYPE_HIDE_CABINET) return $this->redirect(['password-type2']);

        throw new \Exception('Не определено');
    }

    /**
     * @return string
     */
    public function actionPasswordType4()
    {
        $seedsStart = Yii::$app->session->get('seeds-start');
        if (is_null($seedsStart)) {
            $seedsStart = \common\services\Security::getSeeds();
            Yii::$app->session->set('seeds-start', $seedsStart);
            Yii::$app->session->set('seeds', $seedsStart);
        }

        $model = new \avatar\models\forms\PasswordType4();

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $model->action();
            Yii::$app->session->setFlash('form', 1);

            return $this->refresh();
        }

        return $this->render([
            'model' => $model,
        ]);
    }

    /**
     * @return string
     */
    public function actionPasswordType2()
    {
        /** @var \common\models\UserAvatar $user */
        $user = Yii::$app->user->identity;
        if ($user->wallets_is_locked == 1) return $this->redirect(['password-type21']);
        else return $this->redirect(['password-type20']);
    }


    /**
     * Клиент в 2 коде и надо перейти в 4 (снять защиту)
     *
     * @return string
     */
    public function actionPasswordType20()
    {
        $model = new \avatar\models\forms\PasswordType20();

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $model->action();
            Yii::$app->session->setFlash('form', 1);

            return $this->refresh();
        } else {
            return $this->render([
                'model' => $model,
            ]);
        }
    }

    /**
     * Предложение снять защиту или разблокировать кошельки
     *
     * @return string
     */
    public function actionPasswordType21()
    {
        return $this->render();
    }

    /**
     * Меняет пароль, перешифровывает кошельки
     *
     * @return string
     */
    public function actionPasswordType211()
    {
        $model = new \avatar\models\forms\PasswordType211();

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $model->action();
            Yii::$app->session->setFlash('form', 1);
        }
        return $this->render([
            'model' => $model,
        ]);
    }

    /**
     * Снимает защиту
     *
     * @return string
     */
    public function actionPasswordType212()
    {
        $model = new \avatar\models\forms\PasswordType212();

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $model->action();
            Yii::$app->session->setFlash('form', 1);
        }
        return $this->render([
            'model' => $model,
        ]);
    }

    /**
     * Заканчивает процедуру регистрации
     *
     * @return string
     */
    public function actionRegistrationActivate()
    {
        $model = new \avatar\models\forms\RegistrationActivate();

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $seeds = $model->action();
            Yii::$app->session->set('seeds', $seeds);
            Yii::$app->session->setFlash('form', 1);

            return $this->refresh();
        } else {
            return $this->render('registration-activate', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Выдает на скачивание SEEDs
     *
     * @return string
     */
    public function actionRegistrationActivateGetSeeds()
    {
        // получаю SEEDS
        $seeds = Yii::$app->session->get('seeds');

        // готовлю HTML для PDF
        $filePath = \Yii::getAlias('@avatar/views/cabinet-bills/_seeds-pdf.php');
        $contentHtml = \Yii::$app->view->renderFile($filePath, [
            'seeds' => $seeds,
            'user'  => Yii::$app->user->identity,
        ]);

        // формирую PDF из HTML
        require \Yii::getAlias('@common/components/mpdf60/mpdf.php');
        $pdfDriver = new \mPDF();
        $pdfDriver->WriteHTML($contentHtml);
        $contentPdf = $pdfDriver->Output('', 'S');

        // выдаю на скачивание
        return Yii::$app->response->sendContentAsFile($contentPdf, 'SeedsAvatarBank' . '.pdf', [
            'mimeType' => 'application/pdf',
        ]);
    }

    /**
     * Выдает документ на скачивание
     *
     * @return string
     * @throws \cs\web\Exception
     */
    public function actionDocumentsDownload($id)
    {
        $doc = UserDocument::findOne($id);
        if (is_null($doc)) {
            throw new Exception('Не найден документ');
        }
        if ($doc->user_id != Yii::$app->user->id) {
            throw new Exception('Это не ваш документ');
        }
        $info = pathinfo($doc->file);

        return Yii::$app->response->sendContentAsFile(file_get_contents(Yii::getAlias('@webroot' . $doc->file)), $doc->id . '.'  . $info['extension']);
    }

    /**
     * Выдает документ на скачивание
     *
     * @return string
     * @throws \cs\web\Exception
     */
    public function actionDocumentsDownloadPdf($id)
    {
        $doc = UserDocument::findOne($id);
        if (is_null($doc)) {
            throw new Exception('Не найден документ');
        }
        if ($doc->user_id != Yii::$app->user->id) {
            throw new Exception('Это не ваш документ');
        }
        $info = pathinfo($doc->file);

        require \Yii::getAlias('@common/components/mpdf60/mpdf.php');
        $pdfDriver = new \mPDF();
        $filePath = \Yii::getAlias('@avatar/views/cabinet/documents-pdf.php');
        $contentHtml = \Yii::$app->view->renderFile($filePath, ['document' => $doc]);
        $pdfDriver->WriteHTML($contentHtml);
        $contentPdf = $pdfDriver->Output('', 'S');

        return Yii::$app->response->sendContentAsFile($contentPdf, 'document' . '_' . $id . '.pdf', [
            'mimeType' => 'application/pdf',
        ]);
    }

    /**
     * Показывает документ
     *
     * @param $id
     * @return string
     * @throws Exception
     */
    public function actionDocumentsView($id)
    {
        $document = UserDocument::findOne($id);
        if (is_null($document)) {
            throw new Exception('Нет такого документа');
        }

        return $this->render('documents-view', [
            'document' => $document,
        ]);
    }

    public function actionDefaultBtc()
    {
        if (Yii::$app->request->isPost) {
            $v = Yii::$app->request->post('billing');
            $info = UserBillDefault::findOne(['user_id' => Yii::$app->user->id, 'currency_id' => Currency::BTC]);
            if (is_null($info)) {
                $info = new UserBillDefault([
                    'user_id'     => Yii::$app->user->id,
                    'id'          => $v,
                    'currency_id' => Currency::BTC,
                ]);
                $info->save();
            } else {
                $info->id = $v;
                $info->save();
            }
            Yii::$app->session->setFlash('form', 1);

            return $this->refresh();
        } else {
            return $this->render('default-btc');
        }
    }

    public function actionDefaultEth()
    {
        if (Yii::$app->request->isPost) {
            $v = Yii::$app->request->post('billing');
            $info = UserBillDefault::findOne(['user_id' => Yii::$app->user->id, 'currency_id' => Currency::ETH]);
            if (is_null($info)) {
                $info = new UserBillDefault([
                    'user_id'     => Yii::$app->user->id,
                    'id'          => $v,
                    'currency_id' => Currency::ETH,
                ]);
                $info->save();
            } else {
                $info->id = $v;
                $info->save();
            }
            Yii::$app->session->setFlash('form', 1);

            return $this->refresh();
        } else {
            return $this->render('default-eth');
        }
    }

    public function actionProfileSettings()
    {
        /** @var \avatar\models\forms\ProfileSettings $model */
        $model = \avatar\models\forms\ProfileSettings::findOne(Yii::$app->user->id);
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $model->save();
            Yii::$app->session->setFlash('form');

            return $this->refresh();
        }
        else {
            return $this->render('profile-settings', [
                'model' => $model,
            ]);
        }
    }

    public function actionProfileSecurity()
    {
        return $this->render('profile-security');
    }

    /**
     * Устанавливает флаг is_2step_login в таблице user
     *
     * REQUEST:
     * - is_2step_login - int -
     *
     * @return string JSON
     * errors:
     * 101, 'Нет параметра'
     * 102, 'Значение мб только 0 или 1'
     */
    public function actionProfileSecurityAjax()
    {
        $v = self::getParam('is_2step_login');
        if (is_null($v)) {
            return self::jsonErrorId(101, 'Нет параметра');
        }
        if (!in_array($v, [0,1,2])) {
            return self::jsonErrorId(102, 'Значение мб только 0 или 1');
        }
        Yii::$app->user->identity->is_2step_login = $v;
        Yii::$app->user->identity->save();

        return self::jsonSuccess();
    }

    /**
     * AJAX
     * REQUEST:
     * - address - string - адрес счета
     */
    public function actionQrCode()
    {
        $address = self::getParam('address');
        if (is_null($address)) {
            return self::jsonErrorId(101, 'Нет обязательного параметра');
        }
        Yii::setAlias('@Endroid/QrCode', '@vendor/endroid/QrCode/src');
        $src = 'data:image/png;base64,' . base64_encode(
                (new \Endroid\QrCode\QrCode())
                    ->setText('bitcoin:' . $address)
                    ->setSize(180)
                    ->setPadding(0)
                    ->setErrorCorrection('high')
                    ->setForegroundColor(['r' => 0, 'g' => 0, 'b' => 0, 'a' => 0])
                    ->setBackgroundColor(['r' => 255, 'g' => 255, 'b' => 255, 'a' => 0])
                    ->setLabelFontSize(16)
                    ->setImageType(\Endroid\QrCode\QrCode::IMAGE_TYPE_PNG)
                    ->get('png')
            );

        return self::jsonSuccess([
            'src' => $src,
            'address' => $address,
        ]);
    }


    public function actionHumanDesign()
    {
        $model = new \avatar\models\forms\ProfileHumanDesignCalc();

        return $this->render('@avatar/views/cabinet/human-design', [
            'model' => $model,
        ]);
    }

    /**
     * AJAX
     * Расчет дизайна
     *
     * REQUEST:
     * - date - string - 'dd.mm.yyyy'
     * - time - string - 'hh:mm'
     * - country - int - страна
     * - town - int - город
     *
     * @return string
     * @throws
     */
    public function actionHumanDesignAjax()
    {
        $date = self::getParam('date');
        $time = self::getParam('time');
        $date = explode('.', $date);
        $country = self::getParam('country');
        $town = self::getParam('town');
        $countryObject = HD::findOne($country);
        if ($town == 0) {
            $townObject = HDtown::findOne(['country_id' => $countryObject->id]);
        } else {
            $townObject = HDtown::findOne($town);
        }

        // получаю данные Дизайна Человека
        $extractor = new \avatar\modules\HumanDesign\calculate\HumanDesignAmericaCom();
        $data = $extractor->calc2($date[0] . '/' . $date[1] . '/' . $date[2], $time, $countryObject->name, $townObject->name);

        // сохраняю картинку
        $path = new SitePath('/upload/HumanDesign');
        $path->add([Yii::$app->user->id, 8, 'png']);
        $path->write(file_get_contents($data->image));
        $data->image = $path->getPath();

        // обновляю пользовтельские данные
        /** @var \common\models\UserAvatar $user */
        $user = Yii::$app->user->identity;
        $fields = [
            'human_design' => $data->getJson(),
            'birth_date'   => $date[2] . $date[1] . $date[0],
            'birth_time'   => $time . ':00',
            'birth_place'  => self::getParam('born'),
        ];
        foreach ($fields as $k => $v) {
            $user->$k = $v;
        }
        $user->save();

        return self::jsonSuccess();
    }

    /**
     * AJAX
     * Выдает города
     *
     * REQUEST:
     * + id - int - country_id
     *
     * @return string
     */
    public function actionHumanDesignTown()
    {
        $id = self::getParam('id');
        $rows = HDtown::find()
            ->select([
                'id',
                'name',
                'title',
            ])
            ->where(['country_id' => $id])
            ->asArray()
            ->all()
        ;

        return self::jsonSuccess($rows);
    }

    /**
     * AJAX
     * Удаляет дизайн человека
     *
     * @return string
     */
    public function actionHumanDesignDelete()
    {
        /** @var \common\models\UserAvatar $user */
        $user = Yii::$app->user->identity;
        $hd = $user->getHumanDesign();
        $path = new SitePath($hd->getImage());
        $path->deleteFile();
        $user->human_design = '';
        $user->save();

        return self::jsonSuccess();
    }
}