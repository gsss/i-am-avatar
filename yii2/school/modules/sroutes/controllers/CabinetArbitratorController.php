<?php

namespace school\modules\sroutes\controllers;

use common\models\avatar\Currency;
use common\models\avatar\UserBill;
use common\models\avatar\UserBillMerchant;
use common\models\BillingMain;
use school\modules\sroutes\models\Link;
use school\modules\sroutes\models\Deal;
use school\modules\sroutes\models\DealStatus;
use common\models\investment\IcoRequest;
use common\models\investment\IcoRequestSuccess;
use common\models\PaySystemConfig;
use common\models\piramida\Billing;
use common\models\piramida\Wallet;
use common\models\UserAvatar;
use common\services\Security;
use cs\Application;
use cs\base\BaseController;
use cs\services\UploadFolderDispatcher;
use cs\services\VarDumper;
use cs\web\Exception;
use Yii;
use yii\base\UserException;
use yii\db\Connection;
use yii\db\Query;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use yii\helpers\FileHelper;
use yii\web\ErrorAction;
use yii\web\Response;

class CabinetArbitratorController extends \avatar\controllers\CabinetBaseController
{

    public function behaviors()
    {
        return [
            'access' => [
                'class' => '\yii\filters\AccessControl',
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['role_arbitrator'],
                    ],
                ],
            ],
        ];
    }

    /**
     */
    public function actionIndex()
    {
        return $this->render([]);
    }

    /**
     * Принимает сделку арбитром
     *
     * @param int $id Идентификатор сделки
     *
     * @return Response
     * @throws \Exception
     */
    public function actionAccept($id)
    {
        $Deal = Deal::findOne($id);
        if (is_null($Deal)) throw new \Exception('Не найден Deal ' . $id);

        $t = Yii::$app->db->beginTransaction();
        try {
            $Deal->trigger(Deal::EVENT_BEFORE_AUDIT_ACCEPTED);

            $Deal->status = Deal::STATUS_AUDIT_ACCEPTED;
            $Deal->save();
            DealStatus::add(['status' => Deal::STATUS_AUDIT_ACCEPTED, 'deal_id' => $Deal->id]);

            $arbitrator = Link::add([
                'deal_id'       => $Deal->id,
                'arbitrator_id' => Yii::$app->user->id,
            ]);

            $Deal->trigger(Deal::EVENT_AFTER_AUDIT_ACCEPTED);

            $t->commit();
        }catch ( \Exception $e) {
            $t->rollBack();
        }


        return self::jsonSuccess();
    }

    public function actionFinish1($id)
    {
        $Deal = Deal::findOne($id);
        if (is_null($Deal)) throw new \Exception('Не найден Deal ' . $id);

        $Deal->trigger(Deal::EVENT_BEFORE_AUDIT_FINISH);

        $t = Yii::$app->db->beginTransaction();

        try {
            // Устанавливаю статус
            $Deal->status = Deal::STATUS_AUDIT_FINISH;
            $ret = $Deal->save();
            if (!$ret) throw new \Exception('Не сохранился статус');
            DealStatus::add(['status' => Deal::STATUS_AUDIT_FINISH, 'deal_id' => $Deal->id]);

            // Разблокирую монеты в пользу пользователя 1
            {
                // получение дочернего модуля с идентификатором "forum"
                $module = \Yii::$app->getModule('sroutes');
                $walletEscrow = $module->params['walletEscrow'];

                // Ищу кошелек
                $userBill = UserBill::findOne(['currency' => Currency::VVB, 'user_id' => $Deal->user_id, 'mark_deleted' => 0]);
                $walletVVB = Wallet::findOne($userBill->address);
                $walletEscrow = Wallet::findOne($walletEscrow);

                // делаю перевод для разблокировки
                $transactionUnBlock = $walletEscrow->move($walletVVB, $Deal->volume_vvb, 'Арбитр ' . Yii::$app->user->id . ' разблокировал средства от сделки ' . $Deal->id . ' в пользу пользователя ' . $Deal->user_id . ' в кошелек ' . $walletVVB->id);

                // Записываю информацию о принятии решения арбитром
                $arbitration = Link::findOne(['deal_id' => $Deal->id]);
                $arbitration->decision_user_id = $Deal->user_id;
                $arbitration->finish_at = time();
                $arbitration->unblock_transaction_id = $transactionUnBlock->id;
                $arbitration->save();
            }
            // Завершаю сделку (вычитает из предложения объем сделки)
            $Deal->finish();

            $t->commit();
        } catch (\Exception $e) {
            $t->rollBack();
            throw $e;
        }


        $Deal->trigger(Deal::EVENT_AFTER_AUDIT_FINISH);


        return self::jsonSuccess();
    }

    public function actionFinish2($id)
    {
        $Deal = Deal::findOne($id);
        if (is_null($Deal)) throw new \Exception('Не найден Deal ' . $id);

        $Deal->trigger(Deal::EVENT_BEFORE_AUDIT_FINISH);

        $t = Yii::$app->db->beginTransaction();

        try {
            $Deal->status = Deal::STATUS_AUDIT_FINISH;
            $ret = $Deal->save();
            if (!$ret) throw new \Exception('Не сохранился статус');
            DealStatus::add(['status' => Deal::STATUS_AUDIT_FINISH, 'deal_id' => $Deal->id]);

            // Разблокирую монеты в пользу пользователя 2
            {
                // получение дочернего модуля с идентификатором "forum"
                $module = \Yii::$app->getModule('sroutes');
                $walletEscrow = $module->params['walletEscrow'];

                // Ищу кошелек
                $offer = $Deal->getOffer();
                $userBill = UserBill::findOne(['currency' => Currency::VVB, 'user_id' => $offer->user_id, 'mark_deleted' => 0]);
                $walletVVB = Wallet::findOne($userBill->address);
                $walletEscrow = Wallet::findOne($walletEscrow);

                // делаю перевод для разблокировки
                $transactionUnBlock = $walletEscrow->move($walletVVB, $Deal->volume_vvb, 'Арбитр ' . Yii::$app->user->id . ' разблокировал средства от сделки ' . $Deal->id . ' в пользу пользователя ' . $Deal->user_id . ' в кошелек ' . $walletVVB->id);

                // Записываю информацию о принятии решения арбитром
                $arbitration = Link::findOne(['deal_id' => $Deal->id]);
                $arbitration->decision_user_id = $offer->user_id;
                $arbitration->finish_at = time();
                $arbitration->unblock_transaction_id = $transactionUnBlock->id;
                $arbitration->save();
            }

            // Завершаю сделку (вычитает из предложения объем сделки)
            $Deal->finish();

            $t->commit();
        } catch (\Exception $e) {
            $t->rollBack();
            throw $e;
        }

        $Deal->trigger(Deal::EVENT_AFTER_AUDIT_FINISH);

        return self::jsonSuccess();
    }

    public function actionItem($id)
    {
        $Deal = Deal::findOne($id);
        if (is_null($Deal)) throw new \Exception('Не найден Deal ' . $id);

        return $this->render([
            'Deal' => $Deal,
        ]);
    }
}