<?php

namespace school\modules\sroutes\controllers;

use app\models\Article;
use app\models\SiteUpdate;
use app\services\Subscribe;
use avatar\models\forms\Contact;
use avatar\services\LogReader;
use common\components\providers\ETH;
use common\components\sms\IqSms;
use common\models\avatar\Currency;
use common\models\avatar\UserBill;
use common\models\CompanyCustomizeItem;
use common\models\PaymentBitCoin;
use common\models\school\School;
use common\models\shop\CatalogItem;
use common\models\shop\Product;
use common\models\shop\Request;
use common\models\UserAvatar;
use common\models\UserRegistration;
use common\payment\BitCoinBlockTrailPayment;
use cs\Application;
use cs\base\BaseController;
use cs\services\Security;
use cs\services\UploadFolderDispatcher;
use cs\services\VarDumper;
use cs\web\Exception;
use Yii;
use yii\base\Event;
use yii\base\UserException;
use yii\db\Connection;
use yii\db\Query;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use yii\helpers\FileHelper;
use yii\helpers\Url;
use yii\web\ForbiddenHttpException;
use yii\web\Response;

class ShopController extends \avatar\base\BaseController
{
    public $layout = 'main';

    /**
     */
    public function actionIndex()
    {
        return $this->render(['school' => School::get()]);
    }

    /**
     */
    public function actionCatalog($id)
    {
        $catalog = CatalogItem::findOne($id);
        $school = School::findOne($catalog->school_id);

        return $this->render([
            'school'  => $school,
            'catalog' => $catalog,
        ]);
    }


    /**
     */
    public function actionItem($id)
    {
        $Product = Product::findOne($id);
        if (is_null($Product)) {
            throw new Exception('Не найден продукт');
        }

        return $this->render([
            'school' => School::get(),
            'item'   => $Product,
        ]);
    }

    /**
     */
    public function actionOrder()
    {
        $id = Yii::$app->request->get('id');
        $isProduct = false;
        $data = null;
        if (!is_null($id)) {
            $product = \common\models\shop\Product::findOne($id);
            $isProduct = true;
            $data = [
                'product' => $product,
            ];
            $request = [
                'school_id'   => $product->school_id,
                'price'       => $product->price,
                'currency_id' => $product->currency_id,
                'productList' => [$product->id => 1],
            ];
            Yii::$app->session->set('request', $request);
        }

        return $this->render([
            'isProduct' => $isProduct,
            'data'      => $data,
            'school'    => School::get(),
        ]);
    }

    /**
     */
    public function actionLogin()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        $model = new \avatar\models\forms\shop\ShopLogin();

        if (!$model->load(Yii::$app->request->post())) {
            return self::jsonErrorId(400, 'Не загружены данные');
        }
        if (!$model->validate()) {
            return self::jsonErrorId(102, $model->errors);
        }

        return self::jsonSuccess($model->action());
    }

    /**
     */
    public function actionRegistration()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        $model = new \avatar\models\forms\shop\ShopRegistration();

        if (!$model->load(Yii::$app->request->post())) {
            return self::jsonErrorId(400, 'Не загружены данные');
        }
        if (!$model->validate()) {
            return self::jsonErrorId(102, $model->errors);
        }

        return self::jsonSuccess($model->action());
    }

    /**
     */
    public function actionPaySb()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        $school = School::get();
        $model = new \school\modules\sroutes\forms\PaySb(['school' => $school]);

        if (!$model->load(Yii::$app->request->post(), '')) {
            return self::jsonErrorId(400, 'Не загружены данные');
        }
        if (!$model->validate()) {
            return self::jsonErrorId(102, $model->errors);
        }

        return self::jsonSuccess($model->action());
    }

    /**
     * Отправляет код
     */
    public function actionSendCode()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        $code = Yii::$app->session->get('dogovor_code');
        if (is_null($code)) {
            $code = Security::generateRandomString();
            Yii::$app->session->set('dogovor_code', $code);
        }
        $school = School::get();
        /** @var UserAvatar $user */
        $user = Yii::$app->user->identity;
        // получение дочернего модуля с идентификатором "forum"
        $module = \Yii::$app->getModule('sroutes');
        $walletEscrow = $module->params['walletEscrow'];

        \common\services\Subscribe::sendArraySchool(
            $school,
            [$user->email],
            'Код для подписания договора оферты',
            'oferta',
            [
                'code'   => $code,
                'mailer' => $module->mailer,
            ]
        );

        return self::jsonSuccess();
    }

    /**
     * Добавляет в корзину
     */
    public function actionCartAdd()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        $school = School::get();
        $model = new \school\modules\sroutes\forms\CartAdd();

        if (!$model->load(Yii::$app->request->post(), '')) {
            return self::jsonErrorId(400, 'Не загружены данные');
        }
        if (!$model->validate()) {
            return self::jsonErrorId(102, $model->errors);
        }

        return self::jsonSuccess(
            ['counter' => $model->action()]
        );
    }

    /**
     * Добавляет в корзину
     */
    public function actionCartDelete()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        $school = School::get();
        $model = new \school\modules\sroutes\forms\CartDelete();

        if (!$model->load(Yii::$app->request->post(), '')) {
            return self::jsonErrorId(400, 'Не загружены данные');
        }
        if (!$model->validate()) {
            return self::jsonErrorId(102, $model->errors);
        }

        return self::jsonSuccess($model->action());
    }

    /**
     * Показывает корзину
     */
    public function actionCart()
    {
        return $this->render();
    }

    /**
     * проверяет код
     */
    public function actionValidateCode()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        $model = new \school\modules\sroutes\forms\ValidateCode();

        if (!$model->load(Yii::$app->request->post(), '')) {
            return self::jsonErrorId(400, 'Не загружены данные');
        }
        if (!$model->validate()) {
            return self::jsonErrorId(102, $model->errors);
        }

        return self::jsonSuccess();
    }

    /**
     */
    public function actionDelivery()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        if (Yii::$app->user->isGuest) {
            return self::jsonErrorId(10, 'Пользователь должен быть авторизован');
        }

        $model = new \avatar\models\forms\ShopDelivery();

        if (!$model->load(Yii::$app->request->post())) {
            return self::jsonErrorId(400, 'Не загружены данные');
        }
        if (!$model->validate()) {
            return self::jsonErrorId(102, $model->errors);
        }

        return self::jsonSuccess($model->action());
    }

    /**
     */
    public function actionAddress()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        if (Yii::$app->user->isGuest) {
            return self::jsonErrorId(10, 'Пользователь должен быть авторизован');
        }

        $model = new \avatar\models\forms\ShopAddress();

        if (!$model->load(Yii::$app->request->post())) {
            return self::jsonErrorId(400, 'Не загружены данные');
        }
        if (!$model->validate()) {
            return self::jsonErrorId(102, $model->errors);
        }

        return self::jsonSuccess($model->action());
    }

    /**
     */
    public function actionAnketa($id)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        if (Yii::$app->user->isGuest) {
            return self::jsonErrorId(10, 'Пользователь должен быть авторизован');
        }

        $anketa = \common\models\school\AnketaShop::findOne($id);

        $model = new \avatar\models\forms\ShopAnketa(['anketa' => $anketa]);

        if (!$model->load(Yii::$app->request->post())) {
            return self::jsonErrorId(400, 'Не загружены данные');
        }
        if (!$model->validate()) {
            return self::jsonErrorId(102, $model->errors);
        }

        return self::jsonSuccess($model->action());
    }

    /**
     */
    public function actionPaySystem()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        if (Yii::$app->user->isGuest) {
            return self::jsonErrorId(10, 'Пользователь должен быть авторизован');
        }

        $model = new \avatar\models\forms\ShopPaySystem();

        if (!$model->load(Yii::$app->request->post())) {
            return self::jsonErrorId(400, 'Не загружены данные');
        }
        if (!$model->validate()) {
            return self::jsonErrorId(102, $model->errors);
        }

        return self::jsonSuccess($model->action());
    }


    /**
     * Оплата заявки
     *
     * @param int $id идентификатор заявки gs_users_shop_requests.id
     *
     * @return string Response
     * @throws
     */
    public function actionPay($id)
    {
        if (Yii::$app->user->isGuest) {
            return self::jsonErrorId(10, 'Пользователь должен быть авторизован');
        }

        $request = Request::findOne($id);
        if (is_null($request)) {
            throw new \Exception('Не найдена заявка');
        }

        return $this->render('@avatar/views/shop/pay',[
            'request' => $request,
        ]);
    }
}
