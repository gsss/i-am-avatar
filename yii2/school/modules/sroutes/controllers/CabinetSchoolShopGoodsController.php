<?php

namespace school\modules\sroutes\controllers;

use common\models\school\School;
use cs\Application;
use cs\services\VarDumper;
use cs\web\Exception;
use Yii;
use yii\base\UserException;
use yii\bootstrap\Html;
use yii\db\Query;


/**
 * Обслуживает действия магазина
 *
 * Class Cabinet_shop_shopController
 * @package app\controllers
 */
class CabinetSchoolShopGoodsController extends \avatar\controllers\CabinetSchoolBaseController
{
    public $type_id = 5;

    /**
     *
     */
    public function actionIndex()
    {
        $id = 32;
        $this->isAccess($id);
        $school = School::findOne($id);

        return $this->render(['school' => $school]);
    }


    /**
     * Добавление товара
     * @return string|\yii\web\Response
     * @throws \cs\web\Exception
     */
    public function actionAdd()
    {
        $id = 32;
        $this->isAccess($id);
        $model = new \school\modules\sroutes\forms\Product1();
        $school = School::findOne($id);

        if ($model->load(Yii::$app->request->post()) && $model->insert($id)) {
            Yii::$app->session->setFlash('form');
        }

        return $this->render([
            'model'  => $model,
            'school' => $school,
        ]);
    }

    /**
     * Редактирование товара
     *
     * @param int $id идентификатор товара gs_unions_shop_product.id
     *
     * @return string|\yii\web\Response
     *
     * @throws \cs\web\Exception
     */
    public function actionEdit($id)
    {
        $id1 = 32;
        $model = \school\modules\sroutes\forms\Product1::findOne($id);
        $school = School::findOne($id1);
        $this->isAccess($id1);

        if ($model->load(Yii::$app->request->post()) && $model->update($id)) {
            Yii::$app->session->setFlash('form');
        }

        return $this->render([
            'model'  => $model,
            'school' => $school,
        ]);
    }

    public function actionDelete($id)
    {
        $product = \avatar\models\forms\shop\Product::findOne($id);
        if (is_null($product)) throw new \Exception('Не найден $potok');
        $this->isAccess($product->school_id);

        $product->delete();

        return self::jsonSuccess();
    }

}
