<?php

namespace school\modules\sroutes\controllers;

use avatar\controllers\AdminBaseController;
use avatar\models\forms\BlogItem;
use common\models\avatar\Currency;
use common\models\avatar\UserBill;
use school\modules\sroutes\models\Assessment;
use school\modules\sroutes\models\ChatMessage;
use school\modules\sroutes\models\Deal;
use school\modules\sroutes\models\DealStatus;
use school\modules\sroutes\models\Offer;
use common\models\investment\IcoRequest;
use common\models\investment\IcoRequestAddress;
use common\models\investment\IcoRequestSuccess;
use common\models\investment\Project;
use common\models\investment\ProjectIco;
use common\models\MapBankomat;
use common\models\PaySystemConfig;
use common\models\PaySystemListItem;
use common\models\piramida\Wallet;
use common\models\UserAvatar;
use cs\Application;
use cs\services\VarDumper;
use cs\web\Exception;
use Yii;
use yii\base\UserException;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;
use yii\helpers\StringHelper;
use yii\helpers\Url;
use yii\httpclient\Client;
use yii\web\Controller;
use yii\filters\VerbFilter;
use cs\base\BaseController;
use yii\web\ForbiddenHttpException;
use yii\web\Response;
use yii\widgets\ActiveForm;

class CabinetExchangeChatController extends \avatar\controllers\CabinetBaseController
{


    public function actionSend($id)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        $Deal = Deal::findOne($id);
        if (is_null($Deal)) throw new \Exception('Не найден Deal ' . $id);

        if ($Deal->user_id != Yii::$app->user->id and $Deal->getOffer()->user_id != Yii::$app->user->id) {
            throw new ForbiddenHttpException('Нельзя писать сюда');
        }

        $Deal->trigger(Deal::EVENT_BEFORE_MESSAGE);

        $t = Yii::$app->db->beginTransaction();
        try {
            $message = ChatMessage::add([
                'deal_id' => $Deal->id,
                'user_id' => Yii::$app->user->id,
                'message' => Yii::$app->request->post('message'),
            ]);

            $Deal->trigger(Deal::EVENT_AFTER_MESSAGE);

            $t->commit();
        } catch (\Exception $e) {
            $t->rollBack();
            throw new \Exception($e);
        }

        $data = ArrayHelper::toArray($message);
        $data['timeFormatted'] = Yii::$app->formatter->asDatetime($message->created_at, 'php:d.m.Y H:i:s');
        $data['user'] = [
            'id'     => Yii::$app->user->id,
            'avatar' => Yii::$app->user->identity->getAvatar(),
            'name2'  => Yii::$app->user->identity->getName2(),
        ];

        return self::jsonSuccess([
            'message' => $data,
        ]);
    }

    /**
     * REQUEST:
     * - list: string набор идентификаторов сообщений которые разделены запятой и которые не нужно включать в список
     * выборки, потому что они уже есть на странице
     *
     * @param $id
     *
     * @return Response
     * @throws ForbiddenHttpException
     * @throws \Exception
     */
    public function actionGet($id)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        $Deal = Deal::findOne($id);
        if (is_null($Deal)) throw new \Exception('Не найден Deal ' . $id);

        if ($Deal->user_id != Yii::$app->user->id and $Deal->getOffer()->user_id != Yii::$app->user->id) {
            throw new ForbiddenHttpException('Нельзя писать сюда');
        }
        $list = Yii::$app->request->post('list');
        $ids = [];
        if ($list) {
            $ids = explode(',', $list);
        }
        $listMessage = ChatMessage::find()
            ->where(['deal_id' => $Deal->id])
            ->andWhere(['not', ['in', 'id', $ids]])
            ->all();
        $arr = [];
        /** @var \school\modules\sroutes\models\ChatMessage $message */
        foreach ($listMessage as $message) {
            $i = ArrayHelper::toArray($message);
            $u = UserAvatar::findOne($message->user_id);
            $i['timeFormatted'] = Yii::$app->formatter->asDatetime($message->created_at, 'php:d.m.Y H:i:s');
            $i['user'] = [
                'id'     => $u->id,
                'avatar' => $u->getAvatar(),
                'name2'  => $u->getName2(),
            ];
            $arr[] = $i;
        }


        return self::jsonSuccess([
            'list' => $arr,
        ]);
    }


}
