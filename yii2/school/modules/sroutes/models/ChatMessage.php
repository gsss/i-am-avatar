<?php
/**
 * Created by PhpStorm.
 * User: s.arhangelskiy
 * Date: 12.04.2016
 * Time: 16:13
 */

namespace school\modules\sroutes\models;

use common\models\BillingMain;
use common\models\UserAvatar;
use cs\Application;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;
use yii\helpers\VarDumper;

/**
 * @property int id                         идентификатор сообщения
 * @property int user_id                    Идентификатор пользователя
 * @property int deal_id                    Идентификатор сделкаи
 * @property string message                 Сообщение
 * @property int created_at                 время создания, отправки сообщения
 *
 *
 */
class ChatMessage extends ActiveRecord
{
    /** @var  \common\models\UserAvatar  */
    private $user;

    public function behaviors()
    {
        return [
            [
                'class'      => '\yii\behaviors\TimestampBehavior',
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at'],
                ],
                // если вместо метки времени UNIX используется datetime:
                // 'value' => new Expression('NOW()'),
            ],
        ];
    }

    /**
     * @return  \common\models\UserAvatar
     */
    public function getUser()
    {
        if (is_null($this->user)) {
            $this->user = UserAvatar::findOne($this->user_id);
        }

        return $this->user;
    }

    public static function add($fields)
    {
        $item = new self($fields);
        $ret = $item->save();
        if (!$ret) \cs\services\VarDumper::dump($item->errors);
        $item->id = $item::getDb()->lastInsertID;

        return $item;
    }

    public static function getDb()
    {
        $module = \Yii::$app->getModule('sroutes');

        return $module->db;

    }
}