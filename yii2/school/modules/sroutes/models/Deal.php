<?php
/**
 * Created by PhpStorm.
 * User: s.arhangelskiy
 * Date: 12.04.2016
 * Time: 16:13
 */

namespace school\modules\sroutes\models;

use common\models\BillingMain;
use common\models\UserAvatar;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;
use yii\helpers\VarDumper;

/**
 * @property int id                      Идентификатор заявки
 * @property int user_id                 Идентификатор пользователя кто сделал предложение
 * @property int price                   Цена по которой предлагается купить продать 1 VVB, в атомах
 * @property int currency_id             Валюта для price, указывает на db.currency
 * @property int offer_id                ссылка на предложение
 * @property int created_at              Момент создания предложения
 * @property int volume                  Объем сделки в атомах price и currency_id = (price/100) * volume_vvb
 * @property int volume_vvb              Объем сделки в атомах VVB
 * @property int type_id                 Тип сделки для user_id
 * @property int status                  Статус сделки
 *
 *
 */
class Deal extends ActiveRecord
{
    const TYPE_ID_BUY = 1;
    const TYPE_ID_SELL = 2;

    const STATUS_OPEN = 1;
    const STATUS_BLOCK = 2;
    const STATUS_MONEY_SENDED = 3;
    const STATUS_MONEY_RECEIVED = 4;
    const STATUS_CLOSE = 5;
    const STATUS_AUDIT_WAIT = 6;
    const STATUS_AUDIT_ACCEPTED = 7;
    const STATUS_AUDIT_FINISH = 8;
    const STATUS_HIDE = 9;          // закрыта но не завершилась, когда закончилось время но не завершилась.

    const EVENT_BEFORE_OPEN = 'beforeOpen';
    const EVENT_AFTER_OPEN = 'afterOpen';
    const EVENT_BEFORE_BLOCK = 'beforeBlock';
    const EVENT_AFTER_BLOCK = 'afterBlock';
    const EVENT_BEFORE_MONEY_SENDED = 'beforeMoneySended';
    const EVENT_AFTER_MONEY_SENDED = 'afterMoneySended';
    const EVENT_BEFORE_MONEY_RECEIVED = 'beforeMoneyReceived';
    const EVENT_AFTER_MONEY_RECEIVED = 'afterMoneyReceived';
    const EVENT_BEFORE_CLOSE = 'beforeClose';
    const EVENT_AFTER_CLOSE = 'afterClose';
    const EVENT_BEFORE_AUDIT_WAIT = 'beforeAuditWait';
    const EVENT_AFTER_AUDIT_WAIT = 'afterAuditWait';
    const EVENT_BEFORE_AUDIT_ACCEPTED = 'beforeAuditAccepted';
    const EVENT_AFTER_AUDIT_ACCEPTED = 'afterAuditAccepted';
    const EVENT_BEFORE_AUDIT_FINISH = 'beforeAuditFinish';
    const EVENT_AFTER_AUDIT_FINISH = 'afterAuditFinish';
    const EVENT_BEFORE_HIDE = 'beforeHide';
    const EVENT_AFTER_HIDE = 'afterHide';
    const EVENT_BEFORE_MESSAGE = 'beforeMessage';
    const EVENT_AFTER_MESSAGE = 'afterMessage';

    public static $statusList = [
        self::STATUS_OPEN           => 'Сделка открыта',
        self::STATUS_BLOCK          => 'Сделка подтверждена и монеты заблокированы',
        self::STATUS_MONEY_SENDED   => 'Деньги переведены',
        self::STATUS_MONEY_RECEIVED => 'Деньги получены',
        self::STATUS_CLOSE          => 'Сделка закрыта',
        self::STATUS_AUDIT_WAIT     => 'Сделка ожидает арбитра',
        self::STATUS_AUDIT_ACCEPTED => 'Сделка разбирается арбитром',
        self::STATUS_AUDIT_FINISH   => 'Сделка разобрана арбитром и завершена',
        self::STATUS_HIDE           => 'Сделка закрыта но не завершилась',
    ];

    public function rules()
    {
        return [
            ['user_id', 'integer'],

            ['price', 'required'],
            ['price', 'integer'],
            ['price', 'compare', 'operator' => '!=', 'compareValue' => 0],

            ['currency_id', 'required'],
            ['currency_id', 'integer'],
            ['currency_id', 'compare', 'operator' => '!=', 'compareValue' => 0],

            ['offer_id', 'required'],
            ['offer_id', 'integer'],
            ['offer_id', 'compare', 'operator' => '!=', 'compareValue' => 0],

            ['created_at', 'integer'],
            ['volume', 'integer'],
            ['volume_vvb', 'integer'],

            ['type_id', 'integer'],

            ['status', 'integer'],
        ];
    }

    public static function add($fields)
    {
        $item = new self($fields);
        $ret = $item->save();
        if (!$ret) \cs\services\VarDumper::dump($item->errors);
        $item->id = $item::getDb()->lastInsertID;

        return $item;
    }
    /**
     * @return \school\modules\sroutes\models\Offer
     */
    public function getOffer()
    {
        return Offer::findOne($this->offer_id);
    }

    public function behaviors()
    {
        return [
            [
                'class'      => '\yii\behaviors\TimestampBehavior',
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at'],
                ],
                // если вместо метки времени UNIX используется datetime:
                // 'value' => new Expression('NOW()'),
            ],
        ];
    }

    public function attributeLabels()
    {
        return [
            'user_id'     => 'Пользователь',
            'price'       => 'Цена',
            'currency_id' => 'Валюта',
            'offer_id'    => 'Предложение',
            'created_at'  => 'Создано',
            'volume'      => 'Объем',
            'type_id'     => 'Тип сделки',
            'status'      => 'Статус',
        ];
    }

    /**
     * Вычитает объем из предложения (должна вызываться после завершения сделки)
     */
    public function finish()
    {
        $offer = $this->getOffer();
        $dataVolume = Json::decode($offer->volume);
        $dataVolume[1] -= $this->volume;
        $offer->volume = Json::encode($dataVolume);
        $offer->save();
    }

    public static function getDb()
    {
        $module = \Yii::$app->getModule('sroutes');

        return $module->db;

    }
}