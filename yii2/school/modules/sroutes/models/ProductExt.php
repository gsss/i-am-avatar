<?php
/**
 * Created by PhpStorm.
 * User: s.arhangelskiy
 * Date: 12.04.2016
 * Time: 16:13
 */

namespace school\modules\sroutes\models;

use common\models\BillingMain;
use common\models\UserAvatar;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;
use yii\helpers\VarDumper;

/**
 * @property int        id                      идентификатор
 * @property int        product_id
 * @property int        oferta_id
 * @property int        is_send_vvb
 */
class ProductExt extends ActiveRecord
{
    public static function tableName()
    {
        return 'product_ext_info';
    }

    public static function add($fields)
    {
        $item = new self($fields);
        $ret = $item->save();
        if (!$ret) \cs\services\VarDumper::dump($item->errors);
        $item->id = $item::getDb()->lastInsertID;

        return $item;
    }

    public static function getDb()
    {
        $module = \Yii::$app->getModule('sroutes');

        return $module->db;

    }
}