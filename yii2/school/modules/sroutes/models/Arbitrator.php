<?php
/**
 * Created by PhpStorm.
 * User: s.arhangelskiy
 * Date: 12.04.2016
 * Time: 16:13
 */

namespace school\modules\sroutes\models;

use common\models\BillingMain;
use common\models\UserAvatar;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;
use yii\helpers\VarDumper;

/**
 * @property int id                      Идентификатор записи
 * @property int deal_id                 Идентификатор сделки
 * @property int arbitrator_id           Идентификатор арбитра
 * @property int unblock_transaction_id  идентификатор транзакции разблокировки средств
 * @property int decision_user_id        Идентификатор пользовтеля в пользу которого были разблокированы монеты
 * @property int created_at              Момент принятия арбитром сделки на разбор
 * @property int finish_at               Момент завершения арбитром сделки, то есть принятия статуса STATUS_AUDIT_FINISH
 *
 *
 */
class Arbitrator extends ActiveRecord
{
    /**
     * @return \school\modules\sroutes\models\Deal
     */
    public function getDeal()
    {
        return Deal::findOne($this->deal_id);
    }

    public function behaviors()
    {
        return [
            [
                'class'      => '\yii\behaviors\TimestampBehavior',
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at'],
                ],
                // если вместо метки времени UNIX используется datetime:
                // 'value' => new Expression('NOW()'),
            ],
        ];
    }

    /**
     * @param $fields
     *
     * @return Link
     */
    public static function add($fields)
    {
        $i = new self($fields);
        $i->save();
        $i->id = $i::getDb()->lastInsertID;

        return $i;
    }

    public static function getDb()
    {
        $module = \Yii::$app->getModule('sroutes');

        return $module->db;
    }
}