<?php
/**
 * Created by PhpStorm.
 * User: s.arhangelskiy
 * Date: 12.04.2016
 * Time: 16:13
 */

namespace school\modules\sroutes\models;

use common\models\BillingMain;
use common\models\UserAvatar;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;
use yii\helpers\VarDumper;

/**
 * @property int id                         идентификатор заявки
 * @property string content
 * @property int created_at                 Момент создания предложения
 *
 *
 */
class Oferta extends ActiveRecord
{
    public function behaviors()
    {
        return [
            [
                'class'      => '\yii\behaviors\TimestampBehavior',
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at'],
                ],
                // если вместо метки времени UNIX используется datetime:
                // 'value' => new Expression('NOW()'),
            ],
        ];
    }


    public static function getDb()
    {
        $module = \Yii::$app->getModule('sroutes');

        return $module->db;

    }
}