<?php
/**
 * Created by PhpStorm.
 * User: s.arhangelskiy
 * Date: 12.04.2016
 * Time: 16:13
 */

namespace school\modules\sroutes\models;

use common\models\BillingMain;
use common\models\UserAvatar;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;
use yii\helpers\VarDumper;

/**
 * @property int id                      Идентификатор заявки
 * @property int deal_id                 Идентификатор сделки
 * @property int created_at              Момент указания статуса
 * @property int status                  Статус сделки
 *
 *
 */
class DealStatus extends ActiveRecord
{
    public static function tableName()
    {
        return 'deal_status';
    }

    /**
     * @return \school\modules\sroutes\models\Deal
     */
    public function getDeal()
    {
        return Deal::findOne($this->deal_id);
    }

    public function behaviors()
    {
        return [
            [
                'class'      => '\yii\behaviors\TimestampBehavior',
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at'],
                ],
                // если вместо метки времени UNIX используется datetime:
                // 'value' => new Expression('NOW()'),
            ],
        ];
    }


    public static function add($fields)
    {
        $item = new self($fields);
        $item->save();
        $item->id = $item::getDb()->lastInsertID;

        return $item;
    }

    public static function getDb()
    {
        $module = \Yii::$app->getModule('sroutes');

        return $module->db;

    }
}