<?php
/**
 * Created by PhpStorm.
 * User: s.arhangelskiy
 * Date: 12.04.2016
 * Time: 16:13
 */

namespace school\modules\sroutes\models;

use common\models\BillingMain;
use common\models\UserAvatar;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;
use yii\helpers\VarDumper;

/**
 * @property int id                         идентификатор заявки
 * @property int user_id                    Идентификатор пользователя кто сделал предложение
 * @property int price                      Цена по которой предлагается купить продать VVB
 * @property int currency_id                Валюта, указывает на db.currency
 * @property int type_id                    Вид сделки 1 - купить, 2 - продать
 * @property string volume                  Объем предложения от и до, формат JSON "[100, 10000]"
 * @property string condition               Условия сделки, простой текст
 * @property string name                    Заголовок
 * @property int created_at                 Момент создания предложения
 *
 *
 */
class Offer extends ActiveRecord
{

    const TYPE_ID_BUY = 1; // Я покупаю
    const TYPE_ID_SELL = 2; // Я продаю

    public function rules()
    {
        return [
            ['type_id', 'required'],
            ['type_id', 'integer'],
            ['type_id', 'compare', 'operator' => '!=', 'compareValue' => 0, 'message' => 'Это поле обязательно для заполнения'],

            ['price', 'required'],
            ['price', 'integer'],
            ['price', 'compare', 'operator' => '!=', 'compareValue' => 0, 'message' => 'Это поле обязательно для заполнения'],

            ['currency_id', 'required'],
            ['currency_id', 'integer'],
            ['currency_id', 'compare', 'operator' => '!=', 'compareValue' => 0, 'message' => 'Это поле обязательно для заполнения'],

            ['user_id', 'integer'],
            ['condition', 'string'],
            ['volume', 'string'],
            ['name', 'string', 'max' => 255],
        ];
    }

    public function behaviors()
    {
        return [
            [
                'class'      => '\yii\behaviors\TimestampBehavior',
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at'],
                ],
                // если вместо метки времени UNIX используется datetime:
                // 'value' => new Expression('NOW()'),
            ],
        ];
    }

    public function attributeLabels()
    {
        return [
            'type_id'     => 'Тип сделки',
            'price'       => 'Цена',
            'currency_id' => 'Валюта',
            'user_id'     => 'Пользователь',
            'condition'   => 'Условия сделки',
            'volume'      => 'Объем предложения',
        ];
    }

    public static function getDb()
    {
        $module = \Yii::$app->getModule('sroutes');

        return $module->db;

    }
}