<?php

namespace school\modules\sroutes\search;

use common\models\language\Message;
use common\services\Debugger;
use cs\services\VarDumper;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\db\Query;
use yii\helpers\Html;
use yii\helpers\StringHelper;

/**
 */
class Offer extends Message
{
    public $type_id;
    public $pay_id;

    public function rules()
    {
        return [
            ['type_id', 'integer'],
            ['pay_id', 'integer'],
        ];
    }

    /**
     * @param array $params
     * @param null $where
     * @return ActiveDataProvider
     */
    public function search($sort, $params, $where = null)
    {
        $query = \school\modules\sroutes\models\Offer::find();

        if (!is_null($where)) $query->andWhere($where);

        $dataProvider = new ActiveDataProvider([
            'query'      => $query,
            'pagination' => [
                'pageSize' => 20,
            ],
        ]);
        if ($params['sort']) {
            $attribute = $params['sort'];
            $order = SORT_ASC;
            if (StringHelper::startsWith($params['sort'], '-')) {
                $order = SORT_DESC;
                $attribute = substr($params['sort'], 1);
            }
            $dataProvider->sort = ['defaultOrder' => [$attribute => $order]];
        } else {
            if (!is_null($sort)) $dataProvider->sort = $sort;
        }

        // load the search form data and validate
        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }
        // adjust the query by adding the filters
        if ($this->type_id) $query->andWhere(['offer.type_id' => $this->type_id]);
        if ($this->pay_id) {
            $query
                ->innerJoin('offer_pay_link', 'offer_pay_link.offer_id = offer.id')
                ->andWhere(['offer_pay_link.pay_id' => $this->pay_id])
                ->select(['offer.*'])
            ;
        }

        return $dataProvider;
    }
}
