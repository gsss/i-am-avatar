<?php
/**
 * Created by PhpStorm.
 * User: Ra-m-ha
 * Date: 23.10.2019
 * Time: 17:45
 */

namespace school\modules\sroutes;

use avatar\models\forms\shop\Product;
use common\models\avatar\Currency;
use common\models\avatar\UserBill;
use common\models\piramida\Wallet;
use common\models\shop\Request;
use common\models\shop\RequestProduct;
use cs\services\VarDumper;
use school\modules\sroutes\models\ProductExt;
use yii\base\BootstrapInterface;
use Yii;
use yii\base\Application;
use yii\base\Event;
use yii\web\UrlRule;

class Module extends \yii\base\Module implements BootstrapInterface
{

//    public $defaultRoute = 'cabinet-exchange/index';

    public function init()
    {
        parent::init();

        // инициализация модуля с помощью конфигурации, загруженной из config.php
        \Yii::configure($this, require __DIR__ . '/config.php');

        if (Yii::$app instanceof \yii\console\Application) {
            $this->controllerNamespace = 'school\modules\sroutes\commands';
        } else {
            $this->controllerNamespace = 'school\modules\sroutes\controllers';
        }
    }

    public function bootstrap($app)
    {
        Event::on(Request::className(), Request::EVENT_AFTER_SHOP_SUCCESS, function (yii\base\Event $item) {
            /** @var \common\models\shop\Request $r */
            $r = $item->sender;
            $items = [];
            if ($r['school_id'] == 32) {
                /** @var \common\models\shop\RequestProduct $i */
                foreach (RequestProduct::find()->where(['request_id' => $r['id']])->all() as $i) {
                    $pid = $i->product_id;
                    $pe = ProductExt::findOne(['product_id' => $pid]);
                    if (!is_null($pe)) {
                        if ($pe->is_send_vvb) {
                            $p = \common\models\shop\Product::findOne($pid);

                            $items[] = [
                                'amount'  => $p->price * $i->count,
                                'comment' => 'начисление баллов за товар ' . $pid . ' в заказе ' . $r['id'],
                            ];

                        }
                    }
                }

            }
            if (count($items) > 0) {
                $wallet = Wallet::findOne($this->params['walletVvbAll']);
                $userBill = self::getUserVvb($r['user_id']);
                foreach ($items as $item1) {
                    $wallet->move($userBill->address, $item1['amount'], $item1['comment']);
                }
            }
        });

        $app->urlManager->addRules([
            '/' => 'sroutes/default/index',

            'site/<action>'                                            => 'sroutes/site/<action>',

            // Блог
            'blog'                                                     => 'blog/index',
            'blog/<year:\\d{4}>/<month:\\d{2}>/<day:\\d{2}>/<id:\\w+>' => 'blog/item',
            'blog/<action>'                                            => 'blog/<action>',

            // Новости
            'news'                                                     => 'news/index',
            'news/<year:\\d{4}>/<month:\\d{2}>/<day:\\d{2}>/<id:\\w+>' => 'news/item',
            'news/<action>'                                            => 'news/<action>',

            // Комментарии
            'comments/<action>'                                        => 'comments/<action>',

            // Магазин
            'shop/<action>'                                            => 'sroutes/shop/<action>',

            'pages/<action>' => 'pages/<action>',
            'test/<action>'  => 'test/<action>',
            'kurs/<action>'  => 'kurs/<action>',

            'cabinet-billing/<action>'              => 'sroutes/cabinet-billing/<action>',
            'cabinet-school-shop-catalog/<action>'  => 'sroutes/cabinet-school-shop-catalog/<action>',
            'cabinet-school-shop-requests/<action>' => 'sroutes/cabinet-school-shop-requests/<action>',
            'cabinet-school-shop-goods/<action>'    => 'sroutes/cabinet-school-shop-goods/<action>',
            'admin-oferta/<action>'                 => 'sroutes/admin-oferta/<action>',
            'auth/<action>'                         => 'sroutes/auth/<action>',
            'cabinet-task-list/<action>'            => 'sroutes/cabinet-task-list/<action>',
            'cabinet/<action>'                      => 'sroutes/cabinet/<action>',
            'coin-explorer/<action>'                => 'sroutes/coin-explorer/<action>',
            'cabinet-wallet/<action>'               => 'sroutes/cabinet-wallet/<action>',
            'cabinet-exchange/<action>'             => 'sroutes/cabinet-exchange/<action>',
            'cabinet-exchange-chat/<action>'        => 'sroutes/cabinet-exchange-chat/<action>',
            'cabinet-arbitrator/<action>'           => 'sroutes/cabinet-arbitrator/<action>',
            'cabinet-pif/<action>'                  => 'sroutes/cabinet-pif/<action>',
            'page/<action>'                         => 'sroutes/page/<action>',
            'sroutes/<controller>/<action>'         => 'sroutes/<controller>/<action>',

            '<controller>'          => 'page/controller',
            '<controller>/<action>' => 'page/controller-action',
        ]);

    }

    public static function getUserVvb($id)
    {
        try {
            $userBill = UserBill::findOne(['currency' => Currency::VVB, 'user_id' => $id, 'mark_deleted' => 0, 'is_default' => 1]);
        } catch (\Exception $e) {
            $wallet = Wallet::addNew(['currency_id' => 10]);
            $userBill = UserBill::add([
                'address'      => $wallet->id,
                'name'         => 'VVB',
                'currency'     => Currency::VVB,
                'user_id'      => $id,
                'mark_deleted' => 0,
                'is_default'   => 1,
            ]);
        }

        return $userBill;
    }

}