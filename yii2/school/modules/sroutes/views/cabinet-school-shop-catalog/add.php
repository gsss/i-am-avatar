<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;
use app\models\UnionCategory;
use yii\db\Query;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \avatar\models\forms\shop\CatalogItem */
/* @var $school \common\models\school\School */

$this->title = 'Добавить в каталог';

?>
<div class="container">
    <div class="col-lg-12">
        <h1 class="page-header"><?= Html::encode($this->title) ?></h1>
    </div>

    <div class="col-lg-12">

    <?php if (Yii::$app->session->hasFlash('form')) { ?>

        <div class="alert alert-success">
            Успешно добавлено.
        </div>

        <?php $id = Yii::$app->session->getFlash('form'); ?>
        <p>
            <a href="<?= \yii\helpers\Url::to(['cabinet-school-shop-catalog/index', 'id' => $school->id]) ?>" class="btn btn-default">Весь каталог</a>
        </p>

    <?php  } else { ?>


        <div class="row">
            <div class="col-lg-5">
                <?php $form = ActiveForm::begin([
                    'options' => ['enctype' => 'multipart/form-data']
                ]); ?>
                <?= $form->field($model, 'name') ?>


                <hr>
                <div class="form-group">
                    <?= Html::submitButton('Добавить', [
                        'class' => 'btn btn-default',
                        'name'  => 'contact-button',
                        'style' => 'width:100%',
                    ]) ?>
                </div>
                <?php ActiveForm::end(); ?>
            </div>
        </div>

    <?php } ?>



</div>
</div>
