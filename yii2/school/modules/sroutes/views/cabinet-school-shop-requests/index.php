<?php

/** $this \yii\web\View  */
/** @var $school \common\models\school\School */

use yii\helpers\Url;
use app\services\GsssHtml;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use app\services\GetArticle\Collection;

$this->title = 'Заказы';


?>

<div class="container">
    <div class="col-lg-12">
        <h1 class="page-header"><?= Html::encode($this->title) ?></h1>
    </div>

    <?php \avatar\widgets\SchoolMenu::begin(['school' => $school]) ?>

    <?php \yii\widgets\Pjax::begin(); ?>
    <?php
    $this->registerJS(<<<JS
$('[data-toggle="tooltip"]').tooltip();
$('.buttonDelete').click(function (e) {
    e.preventDefault();
    e.stopPropagation();
    if (confirm('Подтвердите удаление')) {
        var button = $(this);
        var id = $(this).data('id');
        ajaxJson({
            url: '/cabinet-school-shop-requests/delete' + '?' + 'id' + '=' + id,
            success: function (ret) {
                $('#modalInfo').on('hidden.bs.modal', function() {
                    button.parent().parent().remove();
                }).modal();
            }
        });
    }
});

$('.rowTable').click(function() {
    window.location = '/cabinet-school-shop-requests/view' + '?' + 'id' + '=' + $(this).data('id');
});

JS
    );
    ?>
    <p>Заказы</p>

    <?= \yii\grid\GridView::widget([
        'dataProvider' => new \yii\data\ActiveDataProvider([
            'query'      => \common\models\shop\Request::find()
                ->where(['school_id' => $school->id])
                ->orderBy(['created_at' => SORT_DESC])
            ,
            'pagination' => [
                'pageSize' => 20,
            ],
        ]),
        'tableOptions' => [
            'class' => 'table table-striped table-hover',
        ],
        'rowOptions'   => function ($item) {
            $data = [
                'data'  => ['id' => $item['id']],
                'role'  => 'button',
                'class' => 'rowTable',
            ];
            return $data;
        },
        'columns'      => [
            'id',
            [
                'header'  => 'Пользователь',
                'content' => function ($item) {
                    $i = ArrayHelper::getValue($item, 'user_id', '');
                    if ($i == '') return '';
                    $user = \common\models\UserAvatar::findOne($i);

                    return Html::img($user->getAvatar(), [
                        'class'  => "img-circle",
                        'width'  => 60,
                        'height' => 60,
                        'style'  => 'margin-bottom: 0px;',
                    ]);
                },
            ],
            [
                'header'  => 'Создано',
                'content' => function ($item) {
                    $v = \yii\helpers\ArrayHelper::getValue($item, 'created_at', 0);
                    if ($v == 0) return '';

                    return Html::tag('abbr', \cs\services\DatePeriod::back($v, ['isShort' => true]), ['data' => ['toggle' => 'tooltip'], 'title' => Yii::$app->formatter->asDatetime($v)]);
                },
            ],
            [
                'header'  => 'Последнее сообщение',
                'content' => function ($item) {
                    $v = \yii\helpers\ArrayHelper::getValue($item, 'last_message_time', 0);
                    if ($v == 0) return '';

                    return Html::tag('abbr', \cs\services\DatePeriod::back($v, ['isShort' => true]), ['data' => ['toggle' => 'tooltip'], 'title' => Yii::$app->formatter->asDatetime($v)]);
                },
            ],
            [
                'header'  => 'Оплачено?',
                'content' => function ($item) {
                    $v = \yii\helpers\ArrayHelper::getValue($item, 'is_paid', 0);

                    if ($v) {
                        $html = Html::tag('span', 'Да', ['class' => 'label label-success']);
                    } else {
                        $html = Html::tag('span', 'Нет', ['class' => 'label label-default']);
                    }

                    return $html;
                },
            ],
            [
                'header'         => 'Стоимость',
                'headerOptions'  => ['class' => 'text-right'],
                'contentOptions' => ['class' => 'text-right'],
                'content'        => function ($item) {
                    $v = \yii\helpers\ArrayHelper::getValue($item, 'price', 0);
                    if ($v == 0) return '';

                    return Yii::$app->formatter->asDecimal($v / 100, 2);
                },
            ],
            [
                'header'  => 'Удалить',
                'content' => function ($item) {
                    return Html::button('Удалить', [
                        'class' => 'btn btn-danger btn-xs buttonDelete',
                        'data'  => [
                            'id' => $item['id'],
                        ],
                    ]);
                },
            ],
        ],
    ]) ?>
    <?php \yii\widgets\Pjax::end(); ?>

    <?php \avatar\widgets\SchoolMenu::end() ?>
</div>

<div class="modal fade" id="modalInfo" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="myModalLabel">Информация</h4>
            </div>
            <div class="modal-body">
                Успешно!
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
            </div>
        </div>
    </div>
</div>