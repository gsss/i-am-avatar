<?php
use yii\helpers\Url;
use yii\helpers\Html;

function liMenu($route, $name = null, $options = [])
{
    if (is_array($route)) {
        $arr = [];
        foreach ($route as $route1 => $name1) {
            $arr[] = liMenu($route1, $name1);
        }
        return join("\n", $arr);
    }
    if (Yii::$app->requestedRoute == $route) {
        $options['class'] = 'active';
    }

    return \yii\helpers\Html::tag('li', \cs\helpers\Html::a($name, [$route]), $options);
}

$menu = [
    [
        'label' => Yii::t('c.oeVo5Oaket', 'Карта Аватара'),
        'route' => 'card/index',
    ],
    [
        'label' => Yii::t('c.oeVo5Oaket', 'Активировать карту'),
        'route' => 'qr/enter',
    ],
    [
        'label' => Yii::t('c.oeVo5Oaket', 'Обучение'),
        'route' => 'docs/index',
        'items' => [
            [
                'label' => 'Как пользоваться нейросетью "Аватар"',
                'route' => 'docs/video',
            ],
            [
                'label' => 'Блокчейн',
                'route' => 'docs/blockchain',
            ],
            [
                'label' => 'Криптовалюты',
                'route' => 'docs/kripto',
            ],
            [
                'label' => 'Криптовалюта Ethereum',
                'route' => 'docs/eth',
            ],
            [
                'label' => 'Биткойн и блокчейн: парный выход',
                'route' => 'docs/bitcoin',
            ],
            [
                'label' => 'Крипто: мир будущего',
                'route' => 'docs/kripto2',
            ],
            [
                'label' => 'Теория общества изобилия',
                'route' => 'docs/artha',
            ],
            [
                'label' => 'Децентрализованные автономные компании (DAO)',
                'route' => 'docs/dao',
            ],
            [
                'label' => 'Блокчейн и будущее судебной системы',
                'route' => 'docs/sud',
            ],
            [
                'label' => 'Как определить банк по номеру карты?',
                'route' => 'docs/card',
            ],
            [
                'label' => 'Система защиты AvatarNetwork',
                'route' => 'docs/protection',
            ],
            [
                'label' => 'Часто задаваемые вопросы',
                'route' => 'docs/faq',
            ],
//            [
//                'label' => 'Игра',
//                'route' => 'docs/game',
//            ],
//            [
//                'label' => 'Кто такие Аватары?',
//                'route' => 'docs/avatars',
//            ],
//            [
//                'label' => 'Трансформация энергии денег в новой эре',
//                'route' => 'docs/transform',
//            ],
        ],
    ],
    [
        'label' => Yii::t('c.oeVo5Oaket', 'Новости'),
        'route' => 'news/index',
        'urlList' => [
            'controller' => 'news'
        ]
    ],
    [
        'label' => Yii::t('c.oeVo5Oaket', 'Блог'),
        'route' => 'blog/index',
        'urlList' => [
            'controller' => 'blog'
        ]
    ],
    [
        'label' => Yii::t('c.oeVo5Oaket', 'Статистика'),
        'route' => 'statistic/index',
    ],
    [
        'label' => Yii::t('c.oeVo5Oaket', 'О нас'),
        'route' => 'site/about',
    ],
    [
        'label' => Yii::t('c.oeVo5Oaket', 'Контакты'),
        'route' => 'site/contact',
    ],

];

function isSelected($item, $route)
{
    if (isset($item['route'])) {
        if ($item['route'] == $route) return true;
    }

    if (isset($item['items'])) {
        foreach($item['items'] as $i) {
            if ($i['route'] == $route) {
                return true;
            }
        }
    }

    if (\yii\helpers\ArrayHelper::keyExists('urlList', $item)) {
        foreach($item['urlList'] as $i => $v) {
            switch($i) {
                case 'startsWith':
                    if (\yii\helpers\StringHelper::startsWith($route, $v)) return true;
                    break;
                case 'controller':
                    $arr = explode('/', $route);
                    if ($arr[0] == $v) return true;
                    break;
            }
        }
    }

    return false;
}

?>

<?php
$rows = [];
foreach($menu as $item) {
    $class = ['dropdown'];
    if (isSelected($item, Yii::$app->requestedRoute)) {
        $class[] = 'active';
    }
    if (\yii\helpers\ArrayHelper::keyExists('items', $item)) {
        $html = Html::tag(
            'li',
            Html::a(
                $item['label']
                .
                ' '
                .
                Html::tag('span', null, ['class' => 'caret'])
                ,
                '#'
                ,
                [
                    'class'         => "dropdown-toggle",
                    'data-toggle'   => "dropdown",
                    'role'          => "button",
                    'aria-expanded' => "false",
                ]
            )
            .
            Html::tag('ul',
                liMenu(\yii\helpers\ArrayHelper::map($item['items'], 'route', 'label'))
                , [
                    'class' => "dropdown-menu",
                    'role'  => "menu",
                ])
            ,
            ['class' => join(' ', $class)]);
    } else {
        $html = Html::tag(
            'li',
            Html::a($item['label'], [$item['route']]),
            ['class' => join(' ', $class)]
        );
    }

    $rows[] = $html;
}


?>
<?= join("\n", $rows) ?>
