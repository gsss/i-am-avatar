<?php

/** @var $item array article */
/** @var $category string */

use yii\helpers\Html;
use yii\bootstrap\ButtonDropdown;
use yii\helpers\Url;
use cs\services\Url as csUrl;

$item = \app\models\Service::find(11);
$id = 2;
?>

<?php if ($id == 1) {?>
<div class="thumbnail">
    <a href="/category/medical/344/shop/product/1">
        <img alt="Получить Акселератор Мозга в подарок"
             src="<?= $item->getImage() ?>"
             style="width: 100%; display: block;">
    </a>

    <div class="caption">
        <h3><?= $item->getName() ?></h3>

        <p><?= $item->getDescription() ?></p>
    </div>
</div>
<?php } ?>

<?php if ($id == 2) {?>
    <div class="thumbnail">
        <a href="/shop/product/17">
            <img alt="Купить Ключ к Сердцу"
                 src="/upload/FileUpload3/gs_unions_shop_product/00000017/small/image.jpg"
                 style="width: 100%; display: block;">
        </a>

        <div class="caption">
            <h3>Купить Ключ к Сердцу</h3>

            <p>Это уникальная разработка от Лаборатории «Аватар», завоевавшая и открывшая сердца всех, кто ею пользовался. Этот ключ универсальный, подходит как для мужского, так и для женского сердца. Открывает даже самые скрипучие и тяжело заваленные камнями сердца. Поставляется с инструкцией по применению и открытию сердца.</p>
        </div>
    </div>
<?php } ?>
<div class="thumbnail">
    <a href="http://skyway-capital.com/?partner_id=126116" target="_blank">
        <img src="https://my.skyway.capital/images/users-banners/300x250.gif" style="width: 100%;">
    </a>
</div>