<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;
use app\models\UnionCategory;
use yii\db\Query;
use yii\helpers\ArrayHelper;

/** @var $this yii\web\View */
/** @var $form yii\bootstrap\ActiveForm */
/** @var $school \common\models\school\School */
/** @var $model \school\modules\sroutes\forms\Product1 */

$this->title = $model->name;

?>
<div class="container">
    <div class="col-lg-12">
        <h1 class="page-header"><?= Html::encode($this->title) ?></h1>
    </div>

    <div class="col-lg-12">

    <?php if (Yii::$app->session->hasFlash('form')) { ?>
        <div class="alert alert-success">
            Успешно обновлено.
        </div>
        <p>
            <a href="<?= \yii\helpers\Url::to(['cabinet-school-shop-goods/index', 'id' => $school->id]) ?>" class="btn btn-success">Товары</a>
        </p>
    <?php } else { ?>

        <div class="row">
            <div class="col-lg-8">
                <?php $form = ActiveForm::begin([
                    'id'      => 'contact-form',
                    'options' => ['enctype' => 'multipart/form-data'],
                ]); ?>
                <?= $form->field($model, 'name') ?>
                <?= $form->field($model, 'tree_node_id')->dropDownList(
                    ArrayHelper::map(
                        \common\models\shop\CatalogItem::find()->where(['school_id' => $school->id])->all(),
                        'id',
                        'name'

                    ), ['prompt' => '- Ничего не выбрано -']
                ) ?>
                <?= $form->field($model, 'price') ?>
                <?= $form->field($model, 'currency_id')->dropDownList(
                    ArrayHelper::merge(
                        ['- Ничего не выбрано -'],
                        [
                            6 => 'RUB',
                        ]
                    )
                )
                ?>
                <?= $form->field($model, 'image')->widget('\common\widgets\FileUpload7\FileUpload',
                    [
                        'update'    => \avatar\controllers\CabinetSchoolPagesConstructorController::getUpdate(),
                        'settings'  => \avatar\controllers\CabinetSchoolPagesConstructorController::getSettings(32, 5),
                        'events'    => [
                            'onDelete' => function (\common\models\shop\Product $item) {
                                $r = new \cs\services\Url($item->image);
                                $d = pathinfo($r->path);
                                $start = $d['dirname'] . '/' . $d['filename'];

                                \common\models\school\File::deleteAll(['like', 'file', $start]);
                            },
                        ],
                    ]) ?>
                <?= $form->field($model, 'content')->widget('\common\widgets\HtmlContent\HtmlContent') ?>

                <?= $form->field($model, 'oferta_id')->dropDownList(
                    ArrayHelper::merge(
                        ['- Ничего не выбрано -'],
                        ArrayHelper::map(
                            \school\modules\sroutes\models\Oferta::find()->all(),
                            'id',
                            'name'
                        )
                    )
                ) ?>
                <?= $form->field($model, 'is_send_vvb')->dropDownList(
                    [
                        0 => 'Нет',
                        1 => 'Да',
                    ]
                ) ?>

                <hr>
                <div class="form-group">
                    <?= Html::submitButton('Обновить', [
                        'class' => 'btn btn-default',
                        'name'  => 'contact-button',
                        'style' => 'width:100%',
                    ]) ?>
                </div>
                <?php ActiveForm::end(); ?>
            </div>
        </div>
    <?php } ?>


</div>
</div>
