<?php
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this \yii\web\View */
/* @var $isProduct bool Есть ли продукт в параметре $data */
/* @var $data array Если $isProduct=true то $data = ['product' => \common\models\shop\Product] . Если $isProduct=false то $data = null */
/* @var $school \common\models\school\School */

$this->title = 'Заказ в интернет магазине';

$anketa = null;
$fields = [];
if ($isProduct) {
    /** @var \common\models\shop\Product $product */
    $product = $data['product'];
    $sum = $product->price;
    $currency_id = $product->currency_id;

    $link = \common\models\school\AnketaShopLink::findOne(['product_id' => $product->id]);
    if (!is_null($link)) {
        $anketa = \common\models\school\AnketaShop::findOne($link->anketa_id);
        if (!is_null($anketa)) {
            $fields = \common\models\school\AnketaField::find()->where(['setting_id' => $anketa->id])->all();
        }
    }
} else {
    $sum = 100;
    $currency_id = 1;
}
$currency = \common\models\avatar\Currency::findOne($currency_id);
?>

<div class="container" style="padding-bottom: 70px;">
    <div class="col-lg-12">
        <h1 class="page-header text-center">
            <?= $this->title ?>
        </h1>
        <p>Сумма: <?= Yii::$app->formatter->asDecimal($sum / pow(10, $currency->decimals), $currency->decimals) ?> <?= Html::tag('span', $currency->code, ['class' => 'label label-info']) ?></p>

        <div class="collapse in" id="step1">
            <p>Вы не авторизовались, поэтому или войдите в форме ниже и слева или зарегистрируйтесь в форме ниже и справа.</p>
            <!-- Авторизация           -->
            <div class="col-lg-6">
                <?php $model = new \avatar\models\forms\shop\ShopLogin(); ?>
                <?php $form = \yii\bootstrap\ActiveForm::begin([
                    'id'                 => 'login-form',
                    'enableClientScript' => false,
                ]); ?>

                <?= Html::hiddenInput(Html::getInputName($model, 'school_id'), $product->school_id) ?>
                <?= $form->field($model, 'login')->label('Логин/почта') ?>
                <?= $form->field($model, 'password')->passwordInput()->label('Пароль') ?>
                <?php \yii\bootstrap\ActiveForm::end();?>
                <button class="btn btn-success buttonLogin" style="width: 100%;">Логин</button>
            </div>

            <div class="col-lg-6">
                <?php $model = new \avatar\models\forms\shop\ShopRegistration(); ?>
                <?php $form = \yii\bootstrap\ActiveForm::begin([
                    'id'                 => 'registration-form',
                    'enableClientScript' => false,
                ]); ?>
                <?= Html::hiddenInput(Html::getInputName($model, 'school_id'), $product->school_id) ?>
                <?= $form->field($model, 'login')->label('Логин/почта') ?>
                <?= $form->field($model, 'password')->passwordInput()->label('Пароль') ?>
                <?php \yii\bootstrap\ActiveForm::end();?>
                <button class="btn btn-success buttonRegistration" style="width: 100%;">Регистрация</button>
            </div>
            <?php
            $this->registerJs(<<<JS
$('.buttonLogin').click(function(e) {
    ajaxJson({
        url: '/shop/login',
        data: $('#login-form').serializeArray(),
        success: function(ret) {
            window.location.reload();
        },
        errorScript: function(ret) {
            switch (ret.id) {
                case 102:
                    var f = $('#login-form');
                    for (var key in ret.data) {
                        if (ret.data.hasOwnProperty(key)) {
                            var name = key;
                            var value = ret.data[key];
                            var t = f.find('.field-shoplogin-' + name);
                            t.addClass('has-error');
                            t.find('p.help-block-error').html(value.join('<br>')).show();
                        }
                    }
                    break;
            }
        }
    });
});
$('.buttonRegistration').click(function(e) {
    ajaxJson({
        url: '/shop/registration',
        data: $('#registration-form').serializeArray(),
        success: function(ret) {
            window.location.reload();
        },
        errorScript: function(ret) {
            switch (ret.id) {
                case 102:
                    var f = $('#registration-form');
                    for (var key in ret.data) {
                        if (ret.data.hasOwnProperty(key)) {
                            var name = key;
                            var value = ret.data[key];
                            var t = f.find('.field-shopregistration-' + name);
                            t.addClass('has-error');
                            t.find('p.help-block-error').html(value.join('<br>')).show();
                        }
                    }
                    break;
            }
        }
    });
});

$('#login-form .form-control').on('focus', function() {
    var o = $(this);
    var p = o.parent();
    if (p.hasClass('input-group')) {
        p = p.parent();
    }
    p.removeClass('has-error');
    p.find('p.help-block-error').hide();
});

$('#registration-form .form-control').on('focus', function() {
    var o = $(this);
    var p = o.parent();
    if (p.hasClass('input-group')) {
        p = p.parent();
    }
    p.removeClass('has-error');
    p.find('p.help-block-error').hide();
});
JS
)
            ?>



            <?php if (Yii::$app->user->isGuest) { ?>
            <?php } else { ?>
                <?php
                $this->registerJs(<<<JS
$('#step1').collapse('hide');
$('#step11').collapse('show');
JS
                )
                ?>
            <?php } ?>
        </div>
        <div class="collapse" id="step11">
            <h2>Договор оферты</h2>
            <div style="height: 450px; overflow-y: scroll; border: groove; padding: 20px;">
                <?php
                $oferta = \school\modules\sroutes\models\Oferta::findOne(1);
                echo ($oferta->content);
                ?>
            </div>
            <?php
            $this->registerJs(<<<JS
$('.buttonSend11').click(function(e) {
    ajaxJson({
        url: '/shop/send-code',
        success: function(ret) {
            
        }
    });
});
$('.buttonSend12').click(function(e) {
    ajaxJson({
        url: '/shop/validate-code',
        data: {code: $('.inputCode').val()},
        success: function(ret) {
            if ($('#deliveryItems table tbody tr').length > 1) {
                $('#step2').collapse('show');
            } else {
                ajaxJson({
                    url: '/shop/delivery',
                    data: {ShopDelivery: {id: 9}},
                    success: function(ret) {
                        $('#step11').collapse('hide');
                        $('#step3').collapse('show');
                        if (ret.type == 1) {
                            $('.field-shopaddress-index').hide();
                            $('.field-shopaddress-address').hide();
                            $('.field-shopaddress-fio').hide();
                        }
                    }
                });
            }
        }
    });
});

JS
            );
            ?>

            <p><button class="btn btn-success buttonSend11">Отправить</button></p>
            <p>Введите полученый код</p>
            <p><input class="form-control inputCode"></p>
            <p><button class="btn btn-success buttonSend12">Отправить</button></p>
        </div>
        <div class="collapse" id="step2">
            <!-- Тип доставки           -->
            <div class="col-lg-6">
                <input type="hidden" id="DeliveryItem" value="">
                <?php
                $this->registerJs(<<<JS
$('.rowTable2').click(function(e) {
    ajaxJson({
        url: '/shop/delivery',
        data: {ShopDelivery: {id: $(this).data('id')}},
        success: function(ret) {
            $('#step2').collapse('hide');
            $('#step3').collapse('show');
            if (ret.type == 1) {
                $('.field-shopaddress-index').hide();
                $('.field-shopaddress-address').hide();
                $('.field-shopaddress-fio').hide();
            }
        }
    });
});

JS
                );
                ?>
                <?= \yii\grid\GridView::widget([
                    'id' => 'deliveryItems',
                    'dataProvider' => new \yii\data\ActiveDataProvider([
                        'query' => \common\models\shop\DeliveryItem::find()
                        ->where(['school_id' => $school->id])
                    ]),
                    'tableOptions' => [
                        'class' => 'table table-striped table-hover',
                    ],
                    'rowOptions'   => function ($item) {
                        return [
                            'data'  => ['id' => $item['id']],
                            'role'  => 'button',
                            'class' => 'rowTable2',
                        ];
                    },
                    'summary' => '',
                    'columns' => [
                        'name',
                        'description',
                        [
                            'header' => 'Стоимость',
                            'contentOptions' => [
                                    'style' => 'text-align: right;'
                            ],
                            'content' => function($item) {
                                if (\cs\Application::isEmpty($item['currency_id'])){
                                    return '';
                                }
                                $currency = \common\models\avatar\Currency::findOne($item['currency_id']);
                                $c = Html::tag('span', $currency->code, ['class' => 'label label-info', 'style' => 'margin-left: 5px;']);

                                return Yii::$app->formatter->asDecimal($item['price'] / pow(10, $currency->decimals), $currency->decimals) . $c;
                            }
                        ],
                    ]
                ])

                ?>
            </div>

        </div>
        <div class="collapse" id="step3">
            <!-- Адрес           -->
            <div class="col-lg-6">
                <?php $model = new \avatar\models\forms\ShopAddress(); ?>
                <?php $form = \yii\bootstrap\ActiveForm::begin([
                    'id'                 => 'address-form',
                    'enableClientScript' => false,
                ]);
                $request = Yii::$app->session->get('request', []);
                ?>
                <?= $form->field($model, 'index') ?>
                <?= $form->field($model, 'address')->textarea(['rows' => 2]) ?>
                <?= $form->field($model, 'fio') ?>

                <?= $form->field($model, 'comment')->textarea(['rows' => 3]) ?>
                <?= $form->field($model, 'phone') ?>
                <?php \yii\bootstrap\ActiveForm::end();?>
                <button class="btn btn-success buttonAddress" style="width: 100%;">Далее</button>
                <?php
                $this->registerJs(<<<JS
$('.buttonAddress').click(function(e) {
    ajaxJson({
        url: '/shop/address',
        data: $('#address-form').serializeArray(),
        success: function(ret) {
            $('#step3').collapse('hide');
            if ($('#step32').length > 0) {
                $('#step32').collapse('show');
            } else {
                $('#step4').collapse('show');
            }
        },
        errorScript: function(ret) {
            switch (ret.id) {
                case 102:
                    var f = $('#address-form');
                    for (var key in ret.data) {
                        if (ret.data.hasOwnProperty(key)) {
                            var name = key;
                            var value = ret.data[key];
                            var t = f.find('.field-shopaddress-' + name);
                            t.addClass('has-error');
                            t.find('p.help-block-error').html(value.join('<br>')).show();
                        }
                    }
                    break;
            }
        }
    });
});

$('#address-form .form-control').on('focus', function() {
    var o = $(this);
    var p = o.parent();
    if (p.hasClass('input-group')) {
        p = p.parent();
    }
    p.removeClass('has-error');
    p.find('p.help-block-error').hide();
});
JS
                )
                ?>
            </div>

        </div>
        <?php if (!is_null($anketa)) { ?>
            <div class="collapse" id="step32">
                <!-- Анкета           -->
                <div class="col-lg-6">
                    <?php $model = new \avatar\models\forms\ShopAnketa(); ?>
                    <?php $form = \yii\bootstrap\ActiveForm::begin([
                        'id'                 => 'anketa-form',
                        'enableClientScript' => false,
                    ]);
                    $request = Yii::$app->session->get('request');
                    ?>

                    <?= Html::hiddenInput(Html::getInputName($model, 'anketa_id'), $anketa->id) ?>
                    <?php /** @var \common\models\school\AnketaField $field */ ?>
                    <?php foreach ($fields as $fieldDb) { ?>
                        <?php
                        $fieldForm = $form->field($model, 'field_' . $fieldDb->id);
                        if ($fieldDb->name) {
                            $fieldForm->label($fieldDb->name);
                        }
                        ?>
                        <?= $fieldForm->__toString() ?>
                    <?php } ?>

                    <?php \yii\bootstrap\ActiveForm::end();?>
                    <button class="btn btn-success buttonAnketa" style="width: 100%;">Далее</button>
                    <?php
                    $this->registerJs(<<<JS
$('.buttonAnketa').click(function(e) {
    ajaxJson({
        url: '/shop/anketa?id=' + {$anketa->id},
        data: $('#anketa-form').serializeArray(),
        success: function(ret) {
            $('#step32').collapse('hide');
            $('#step4').collapse('show');
        },
        errorScript: function(ret) {
            switch (ret.id) {
                case 102:
                    var f = $('#anketa-form');
                    for (var key in ret.data) {
                        if (ret.data.hasOwnProperty(key)) {
                            var name = key;
                            var value = ret.data[key];
                            var t = f.find('.field-shopanketa-' + name);
                            t.addClass('has-error');
                            t.find('p.help-block-error').html(value.join('<br>')).show();
                        }
                    }
                    break;
            }
        }
    });
});

$('#anketa-form .form-control').on('focus', function() {
    var o = $(this);
    var p = o.parent();
    if (p.hasClass('input-group')) {
        p = p.parent();
    }
    p.removeClass('has-error');
    p.find('p.help-block-error').hide();
});
JS
                    )
                    ?>
                </div>

            </div>
        <?php } ?>
        <div class="collapse" id="step4">
            <?php
            $currency = \common\models\avatar\Currency::findOne(6);

            $paySystems = \common\models\PaySystem::find()
                ->where(['currency' => $currency->code])
                ->select('id')
                ->column();

            $config = \common\models\PaySystemConfig::find()
                ->where(['school_id' => $school->id])
                ->andWhere(['paysystem_id' => $paySystems]);
            ?>

            <!-- Платежные системы           -->
            <div class="col-lg-6">
                <?php $model = new \avatar\models\forms\ShopPaySystem(); ?>
                <?php $form = \yii\bootstrap\ActiveForm::begin([
                    'id'                 => 'pay-system-form',
                    'enableClientScript' => false,
                ]); ?>
                <?= $form->field($model, 'item')->widget('\avatar\widgets\PaySystemList', ['rows' => $config]) ?>
                <?php \yii\bootstrap\ActiveForm::end();?>
                <button class="btn btn-success buttonPaySystem" style="width: 100%;">Далее</button>
                <?php
                $this->registerJs(<<<JS
$('.buttonPaySystem').click(function(e) {
    ajaxJson({
        url: '/shop/pay-system',
        data: $('#pay-system-form').serializeArray(),
        success: function(ret) {
            window.location = '/shop/pay?id=' + ret.request.id;
        }
    });
});
JS
                )
                ?>
            </div>

        </div>
    </div>
</div>


