<?php

use common\models\avatar\Currency;
use common\models\avatar\UserBill;
use yii\helpers\Html;
use yii\helpers\Url;

/** @var $this yii\web\View */
/** @var $school \common\models\school\School */
/** @var $item \common\models\shop\Product */


$this->title = 'Товар #'.$item->id;


?>

<div class="container" style="padding-bottom: 70px;">
    <div class="col-lg-12">
        <h1 class="page-header">
            <?= $item->name ?>
        </h1>
    </div>

    <div class="col-lg-4">
        <p><img src="<?= $item->image ?>" width="100%"></p>
    </div>
    <div class="col-lg-8">
        <?php
        $currency = Currency::findOne($item->currency_id);
        $price = $item->price / pow(10, $currency->decimals);
        ?>

        <p>
            Стоимость: <?= Yii::$app->formatter->asDecimal($price, $currency->decimals) ?> <span class="label label-info"><?= $currency->code ?></span>
        </p>
        <p>
            <?= nl2br($item->content) ?>
        </p>
        <p><a href="<?= Url::to(['shop/order','id' => $item->id]) ?>" class="btn btn-success" style="width: 100%">Купить</a></p>
        <?php

        $this->registerJs(<<<JS
$('.buttonAddToCart').click(function(e) {
    ajaxJson({
        url: '/shop/cart-add',
        data: {id: $(this).data('id')},
        success: function(ret) {
            $('#basketCounter').html(ret.counter);
        }
    })
});
JS
);
        ?>
        <p><button class="btn btn-success buttonAddToCart" style="width: 100%" data-id="<?= $item->id ?>">В корзину</button></p>
    </div>
</div>


