<?php

use common\models\avatar\Currency;
use common\models\avatar\UserBill;
use yii\helpers\Html;
use yii\helpers\Url;

/** @var $this yii\web\View */
/** @var $school \common\models\school\School */


$this->title = 'Корзина';


?>

<div class="container" style="padding-bottom: 70px;">
    <div class="col-lg-12">
        <h1 class="page-header text-center">
            <?= $this->title ?>
        </h1>
    </div>

    <div class="col-lg-8 col-lg-offset-2">

        <div class="row">
            <?php
            $items = \common\models\shop\Basket::get();
            $rows = [];
            foreach ($items as $i) {
                $rows[] = [
                    'id'          => $i['id'],
                    'count'       => $i['count'],
                    'name'        => $i['product']['name'],
                    'image'       => $i['product']['image'],
                    'price'       => $i['product']['price'],
                    'currency_id' => $i['product']['currency_id'],
                ];
            }
            ?>
            <?php
            $this->registerJS(<<<JS
$('.buttonDelete').click(function (e) {
    e.preventDefault();
    e.stopPropagation();
    if (confirm('Подтвердите удаление')) {
        var button = $(this);
        var id = $(this).data('id');
        ajaxJson({
            url: '/shop/cart-delete',
            data: {id: id},
            success: function (ret) {
                $('#modalInfo').on('hidden.bs.modal', function() {
                    button.parent().parent().remove();
                }).modal();
            }
        });
    }
});


JS
            );
            ?>
            <?= \yii\grid\GridView::widget([
                'dataProvider' => new \yii\data\ArrayDataProvider([
                    'allModels' => $rows,
                ]),
                'tableOptions' => [
                    'class' => 'table table-striped table-hover',
                ],
                'rowOptions'   => function ($item) {
                    $data = [
                        'data'  => ['id' => $item['id']],
                        'role'  => 'button',
                        'class' => 'rowTable',
                    ];
                    return $data;
                },
                'columns' => [
                    'id',
                    [
                        'header'  => 'Картинка',
                        'content' => function ($item) {
                            $i = \yii\helpers\ArrayHelper::getValue($item, 'image', '');
                            if ($i == '') return '';

                            return Html::img(\common\widgets\FileUpload7\FileUpload::getFile($i, 'crop'), [
                                'class'  => "thumbnail",
                                'width'  => 80,
                                'height' => 80,
                                'style'  => 'margin-bottom: 0px;',
                            ]);
                        },
                    ],
                    'name:text:Наименование',
                    [
                        'header'         => 'Цена',
                        'headerOptions'  => ['class' => 'text-right'],
                        'contentOptions' => ['class' => 'text-right'],
                        'content'        => function ($item) {
                            $cid = \yii\helpers\ArrayHelper::getValue($item, 'currency_id');
                            if (is_null($cid)) return '';
                            $c = Currency::findOne($cid);
                            $v = \yii\helpers\ArrayHelper::getValue($item, 'price', 0);
                            if ($v == 0) return '';
                            $cHtml = Html::tag('span', $c->code, ['class' => 'label label-info', 'style' => 'margin-left:5px;']);

                            return Yii::$app->formatter->asDecimal($v / pow(10, $c->decimals), $c->decimals) . $cHtml;
                        },
                    ],
                    'count:text:Кол-во',
                    [
                        'header'  => 'Удалить',
                        'content' => function ($item) {
                            return Html::button('Удалить', [
                                'class' => 'btn btn-danger btn-xs buttonDelete',
                                'data'  => [
                                    'id' => $item['id'],
                                ]
                            ]);
                        }
                    ],
                ]
            ]) ?>

            <?php
            $p = \common\models\shop\Basket::getPrice();
            $c = Currency::findOne(6);
            $cHtml = Html::tag('span', $c->code, ['class' => 'label label-info', 'style' => 'margin-left:5px;']);

            ?>
            <p>Итого: <?= Yii::$app->formatter->asDecimal($p / pow(10, $c->decimals), $c->decimals) . $cHtml ?></p>
        </div>

    </div>
</div>


<div class="modal fade" id="modalInfo" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Информация</h4>
            </div>
            <div class="modal-body">
                Успешно!
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
            </div>
        </div>
    </div>
</div>