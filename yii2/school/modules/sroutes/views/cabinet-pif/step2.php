<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;
use app\models\UnionCategory;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \school\modules\sroutes\forms\PifParams */

$this->title = 'Шаг 2';

$appAsset = Yii::$app->assetManager->getBundle('\school\modules\sroutes\assets\App\Asset');

$this->registerCssFile($appAsset->baseUrl . '/controller/cabinet-pif/step2/style.css');
$this->registerCssFile('https://fonts.googleapis.com/css?family=Fira+Sans+Condensed&display=swap');
$this->registerCssFile('https://fonts.googleapis.com/css?family=Exo+2&display=swap');

$this->registerJsFile('https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.1.6/Chart.bundle.min.js', ['depends' => ['yii\web\JqueryAsset']]);
$this->registerJsFile($appAsset->baseUrl.'/controller/cabinet-pif/step2/script.js', ['depends' => ['yii\web\JqueryAsset']]);
$this->registerJsFile('https://kit.fontawesome.com/b99e675b6e.js');

?>
<style>
    .sum {
        font-size: 18pt;
    }
</style>
<div class="container">
    <div class="col-lg-12">
        <h1 class="page-header"><?= Html::encode($this->title) ?></h1>
    </div>
    <div class="col-lg-12 collapse in step1">
        <?php $form = ActiveForm::begin([
            'id'      => 'contact-form',
            'options' => ['enctype' => 'multipart/form-data'],
        ]); ?>

        <?= $form->field($model, 'currency')->dropDownList([
            '- Ничего не выбрано -',
            1 => 'ETH',
            2 => 'USDT',
            3 => 'VIM',
        ]) ?>

        <hr class="featurette-divider">
        <div class="form-group">
            <?php
            $this->registerJs(<<<JS
$('.buttonSelect').click(function(e) {
    $('.step2').collapse('show');
    $('.step1').collapse('hide');
});
JS
)
            ?>
            <?= Html::button('Выбрать', [
                'class' => 'btn btn-default buttonSelect',
                'name'  => 'contact-button',
                'style' => 'width: 100%;',
            ]) ?>
        </div>
        <?php ActiveForm::end(); ?>
    </div>

    <div class="col-lg-12 collapse step2">



        <div class="block1">
            <div class="block1_cent">
                <div>
                    <div class="box box1">
                        <div class="block1_content">
                            <div class="time"><span class="time_number">3</span> года</div>
                            <div class="slash"></div>
                            <div class="sum_n">20%</div>
                        </div>
                    </div>
                    <div class="bg"></div>
                </div>

                <div>
                    <div class="box box2">
                        <div class="block1_content">
                            <div class="time"><span class="time_number">2</span> года</div>
                            <div class="slash"></div>
                            <div class="sum_n">18%</div>
                        </div>
                    </div>
                    <div class="bg"></div>
                </div>

                <div>
                    <div class="box box3">
                        <div class="block1_content">
                            <div class="time"><span class="time_number">1</span> год</div>
                            <div class="slash"></div>
                            <div class="sum_n">16%</div>
                        </div>
                    </div>
                    <div class="bg"></div>
                </div>

                <div>
                    <div class="box box4">
                        <div class="block1_content">
                            <div class="time"><span class="time_number">6</span> месяцев</div>
                            <div class="slash"></div>
                            <div class="sum_n">15%</div>
                        </div>
                    </div>
                    <div class="bg"></div>
                </div>

                <div>
                    <div class="box box5">
                        <div class="block1_content">
                            <div class="time"><span class="time_number">3</span> месяца</div>
                            <div class="slash"></div>
                            <div class="sum_n">13%</div>
                        </div>
                    </div>
                    <div class="bg"></div>
                </div>

                <div>
                    <div class="box box6">
                        <div class="block1_content">
                            <div class="time"><span class="time_number">1</span> месяц</div>
                            <div class="slash"></div>
                            <div class="sum_n">12%</div>
                        </div>
                    </div>
                    <div class="bg"></div>
                </div>

                <div>
                    <div class="box box7">
                        <div class="block1_content">
                            <div class="time"><span class="time_number">20</span> дней</div>
                            <div class="slash"></div>
                            <div class="sum_n">11%</div>
                        </div>
                    </div>
                    <div class="bg"></div>
                </div>

                <div>
                    <div class="box box8">
                        <div class="block1_content">
                            <div class="time"><span class="time_number">10</span> дней</div>
                            <div class="slash"></div>
                            <div class="sum_n">10%</div>
                        </div>
                    </div>
                    <div class="bg"></div>
                </div>
            </div>
        </div>

        <div class="ranges">
            <div class="range_block"><div class="icon_range"><img src="<?= $appAsset->baseUrl ?>/controller/cabinet-pif/step2/logo1.png"></div><div class="input_range"><input type="range" class="range1" value="0"></div></div>
            <div class="range_block"><div class="icon_range"><img src="<?= $appAsset->baseUrl ?>/controller/cabinet-pif/step2/logo2.png"></div><div class="input_range"><input type="range" class="range2" value="0"></div></div>
            <div class="range_block"><div class="icon_range"><img src="<?= $appAsset->baseUrl ?>/controller/cabinet-pif/step2/logo3.png"></div><div class="input_range"><input type="range" class="range3" value="0"></div></div>
        </div>


        <div class="block2">
            <canvas id="speedChart" width="1000" height="400"></canvas>
        </div>


        <div class="block3">
            <div class="diagramm_circle">
                <canvas id="oilChart" width="700" height="400"></canvas>
            </div>

            <div class="news">
                <div class="news_block">

                    <div class="news_one" data-accordion="open">
                        <div class="news_title">«Рома» увеличила предложение по Джеко до € 13 млн</div>
                        <div class="news_date">30 июля 2019, 01:36</div>
                        <div class="btn_n">Читать<i class="fas fa-angle-down"></i></div>
                        <div class="news_text">
                            «Интер» сделал новое предложение «Роме» по боснийскому нападающему Эдину Джеко, сообщает Football Italia.

                            По данным источника, римский клуб увеличил предложение до € 13 млн плюс € 2 млн в виде бонусов. Отмечается, что «Рома» отказывается отдавать боснийца за эту сумму и хочет получить за форварда € 20 млн.

                            33-летний Джеко выступает за «Рому» с 2015 года. В минувшем сезоне босниец принял участие в 33 матчах итальянского национального чемпионата, забил девять голов и отдал шесть результативных передач. Контракт игрока с клубом действует до лета 2020 года.

                            Ранее сообщалось, что «Интер» предлагал «Роме» € 12 млн. Джеко согласен на переход. В трансфере боснийца лично заинтересован главный тренер миланского клуба Антонио Конте.
                        </div>
                    </div>

                    <div class="news_one" data-accordion="open">
                        <div class="news_title">«Зенит» предложил за Малкома € 35 млн плюс € 5 млн в виде бонусов</div>
                        <div class="news_date">30 июля 2019, 00:18</div>
                        <div class="btn_n">Читать<i class="fas fa-angle-down"></i></div>
                        <div class="news_text">
                            Петербургский «Зенит» продолжает переговоры по трансферу бразильского полузащитника «Барселоны» Малкома и предложил за него € 35 млн, утверждает Sport.es.

                            По данным источника, ещё € 5 млн российский клуб предлагает в виде бонусов. При этом «Барселона» оценивает бразильца € 40 млн плюс € 5 в виде бонусов. Сам игрок готов перейти в «Зенит», так как клуб сделал ему выгодное предложение в финансовом плане, которое составляет € 6,5 млн за сезон.

                            22-летний Малком является воспитанником «Коринтианса». Футболист перешёл в «Барселону» из «Бордо» летом 2018 года. Сумма трансфера составила € 41 млн. В минувшем сезоне бразилец провёл 15 матчей в испанской Примере, забил 1 гол и отдал 2 результативные передачи.
                        </div>
                    </div>

                    <div class="news_one" data-accordion="open">
                        <div class="news_title">«Кристал Пэлас» намерен предложить ЦСКА за Чалова € 15 млн</div>
                        <div class="news_date">30 июля 2019, 14:38</div>
                        <div class="btn_n">Читать<i class="fas fa-angle-down"></i></div>
                        <div class="news_text">
                            Как стало известно «Чемпионату», «Кристал Пэлас» намерен в ближайшее время сделать предложение ЦСКА по нападающему Фёдору Чалову в размере € 15 млн.

                            Ранее в СМИ была информация, что интерес к футболисту также проявляют «Ливерпуль», «Манчестер Юнайтед», «Арсенал», «Манчестер Сити», «Тоттенхэм Хотспур», «Монако», «Лацио» и «Валенсия».

                            Чалов является воспитанником «армейского» клуба. За первую команду столичной команды он выступает с 2017 года. В прошлом сезоне на клубном уровне форвард принял участие в 37 играх. На его счету 17 забитых мячей и 7 результативных передач. С 15 голами в активе он стал лучшим бомбардиром Российской Премьер-Лиги. Контракт игрока с ЦСКА рассчитан до лета 2022 года.
                        </div>
                    </div>

                    <div class="news_one" data-accordion="open">
                        <div class="news_title">Егоров: пенальти в ворота «Зенита» был, в ворота «Оренбурга» — нет. Судья ошибся</div>
                        <div class="news_date">30 июля 2019, 14:32</div>
                        <div class="btn_n">Читать<i class="fas fa-angle-down"></i></div>
                        <div class="news_text">
                            Глава департамента судейства и инспектирования Российского футбольного союза Александр Егоров высказался о работе судьи Сергея Карасёва в матче 3-го тура Российской Премьер-Лиги «Оренбург» – «Зенит» (0:2), а также рассказал об итогах заседания экспертно-судейской комиссии по итогам встречи 2-го тура «Ростов» – «Спартак» (2:2).

                            «Момент с назначением пенальти в ворота «Оренбурга»: Карасёв ошибся, это не игра рукой, надо было продолжить игру. На Барриосе надо было назначить пенальти. Сергею снижена оценка за эти две ключевые ошибки. Если «Оренбург» напишет письмо, то передадим документы в экспертную комиссию.

                            Состоялась экспертно-судейская комиссия по обращению «Ростова» по игре со «Спартаком». Решение комиссии: было обращение насчёт моментов на 13-й минуте (падение Норманна), на 25-й минуте (пенальти «Ростову», фолы Фернандо и Бакаева). На 13-й минуте арбитр поступил правильно, не назначив пенальти. На 25-й минуте арбитр правильно назначил пенальти «Ростову». На 60-й минуте арбитр должен был назначить пенальти «Спартаку» за фол Фернандо, и на 86-й минуте Бакаеву должны были показать вторую жёлтую карточку. Будет ли отстранение? Это внутреннее дело департамента, «Ростов» не просил отстранить», — передаёт слова Егорова корреспондент «Чемпионата» Максим Пахомов.
                        </div>
                    </div>

                    <div class="news_one" data-accordion="open">
                        <div class="news_title">РФС оценил работу VAR на матче ЦСКА — «Локомотив»</div>
                        <div class="news_date">30 июля 2019, 14:26</div>
                        <div class="btn_n">Читать<i class="fas fa-angle-down"></i></div>
                        <div class="news_text">
                            Глава департамента судейства и инспектирования Российского футбольного союза Александр Егоров прокомментировал работу судей, а также систему VAR в матче 3-го тура чемпионата России ЦСКА — «Локомотив» (1:0).

                            «VAR сделал то, что и должен был сделать. Наши иностранные коллеги в плане работы VAR, у нас ноль ошибок, оценивают положительно нашу работу. Сёмин жаловался на момент со Смоловым? Есть протокол VAR, где смотрят очевидные моменты – как на седьмой минуте и так далее. А на Смолове очевидно было для всех, что это не пенальти. Снизили оценку за то, что Смолову не показали жёлтую карточку за симуляцию. Смысл останавливать игру, если всё очевидно? Я сидел на стадионе рядом с представителями «Локомотива» — они готовы были к пенальти на Живоглядове, а когда был спорный мяч, то они сильно удивились. VAR в этой игре оправдал себя на сто процентов. Почему не было карточки Эдеру за удар Васину? Арбитр, вне всяких сомнений и экспертов, должен был показать Эдеру жёлтую карточку.

                            Что касается момента, когда Магнуссону показали жёлтую карточку, а потом назначили пенальти, то почему карточки не было? Она показана. Если бы арбитр показал за срыв перспективной атаки, то да, а тут технический фол и грубая игра, мяч был в игре, и карточка никуда не девается. Что касается времени, пока смотрят моменты. Если мяч вышел, то идёт остановка игры. VAR ввели для того, чтобы были правильные решения. Пока у нас бывают паузы, была три минуты. Надеюсь, дальше она будет поменьше. Мы тоже тренируемся, наша задача – делать всё правильно.

                            Гилерме нельзя было играть в чёрной форме? Почему нельзя, покажите пункт правил, где это написано. Зрителям неудобно было? Напишите про это, может, это картинка такая была», — передаёт слова Егорова корреспондент «Чемпионата» Максим Пахомов.
                        </div>
                    </div>

                </div>
            </div>
        </div>


    </div>

</div>



