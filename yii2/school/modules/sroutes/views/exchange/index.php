<?php

use common\models\avatar\Currency;
use common\models\avatar\UserBill;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */


$this->title = 'Обменник';


?>

<div class="container" style="padding-bottom: 70px;">
    <div class="col-lg-12">
        <h1 class="page-header text-center">
            <?= $this->title ?>
        </h1>


    </div>
    <div class="row">
        <?php
        $this->registerJs(<<<JS
$('#select-function, #select-pay, #select-sort').change(function(e) {
    var pay_id = $('#select-pay').val();
    var type_id = $('#select-function').val();
    var sort = $('#select-sort').val();
    ajaxJson({
        url: '/exchange/index-ajax',
        data: {
            Offer: {
                type_id: type_id,
                pay_id: pay_id
            },
            sort: sort
        },
        success: function(ret) {
            $('#p0').html(ret.html);
        }
    });
});
JS
        );
        ?>

        <div class="col-lg-3">
            <p>Я бы хотел...</p>
            <select class="form-control" id="select-function">
                <option value="">Купить или продать</option>
                <option value="2">Купить</option>
                <option value="1">Продать</option>
            </select>
        </div>
        <div class="col-lg-3">
            <p>Способ оплаты</p>
            <select class="form-control" id="select-pay">
                <option value="">Любой способ оплаты</option>

                <?php /** @var \school\modules\sroutes\models\PayMethod $p */ ?>
                <?php foreach (\school\modules\sroutes\models\PayMethod::find()->all() as $p) { ?>
                    <option value="<?= $p->id ?>"><?= $p->name ?></option>
                <?php } ?>
            </select>
        </div>

        <div class="col-lg-3">
            <p>Сортировка по</p>
            <select class="form-control" id="select-sort">
                <option value="price">Сначала дешевые</option>
                <option value="-price">Сначала дорогие</option>
            </select>
        </div>
    </div>
    <hr>
    <div class="row">
        <div class="col-lg-12">
            <?php $currencyPZM = Currency::findOne(['code' => 'PZM']) ?>
            <h2>Курс 1 RUB = 1 VVB</h2>

            <?php \yii\widgets\Pjax::begin(); ?>

            <?php
            $query = \school\modules\sroutes\models\Offer::find();
            if (!Yii::$app->user->isGuest) {
                $query->where(['not', ['user_id' => Yii::$app->user->id]]);
            }
            ?>

            <?= $this->render('index-table', ['ActiveDataProvider' => new \yii\data\ActiveDataProvider([
                'query'      => $query
                ,
                'sort' => ['defaultOrder' => ['price' => SORT_ASC]],
                'pagination' => [
                    'pageSize' => 20,
                ],
            ])]) ?>
            <?php \yii\widgets\Pjax::end(); ?>
        </div>
    </div>


</div>



<div class="modal fade" id="modalInfo" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Информация</h4>
            </div>
            <div class="modal-body">
                <p class="alert alert-danger">Для того чтобы совершать покупки и продажи надо авторизоваться (войти) или зарегистрироваться!</p>
                <p><a href="/auth/login" class="btn btn-default" style="width: 100%;">Авторизоваться</a></p>
                <p><a href="/auth/registration" class="btn btn-default" style="width: 100%;">Регистрация</a></p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
            </div>
        </div>
    </div>
</div>