<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;
use app\models\UnionCategory;

/* @var $this yii\web\View */

$this->title = 'Реферальная ссылка';

\avatar\assets\Notify::register($this);
\avatar\assets\Clipboard::register($this);

$this->registerJs(<<<JS

var clipboard = new Clipboard('.buttonCopy');
clipboard.on('success', function(e) {

    new Noty({
        timeout: 1000,
        theme: 'sunset',
        type: 'success',
        layout: 'bottomLeft',
        text: 'Скопировано'
    }).show();

});

JS
);
if (\common\models\school\School::isRoot()) {
    $url = Url::to(['site/index', 'partner_id' => Yii::$app->user->id], true);
} else {
    $url = Url::to(['page/root', 'partner_id' => Yii::$app->user->id], true);
}

?>
<div class="container">
    <div class="col-lg-12">
        <h1 class="page-header"><?= Html::encode($this->title) ?></h1>
    </div>
    <div class="col-lg-8">

        <div class="input-group">
            <input type="text" class="form-control" value="<?= $url ?>">
            <span class="input-group-btn">
                <button class="btn btn-default buttonCopy" type="button" data-clipboard-text="<?= $url ?>">Скопировать</button>
              </span>
        </div><!-- /input-group -->

    </div>
    <div class="col-lg-4">
        <?= $this->render('_menu') ?>
    </div>
</div>



