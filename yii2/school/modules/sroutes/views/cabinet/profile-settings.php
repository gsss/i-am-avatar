<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;
use app\models\UnionCategory;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model cs\base\BaseForm */

$this->title = Yii::t('c.RbK39OEY1j', 'Настройки');
?>
<div class="container">
    <div class="col-lg-12">
        <h1 class="page-header"><?= Html::encode($this->title) ?></h1>


    </div>
    <?php
    if (Yii::$app->request->isAjax) {
        $this->registerJs(<<<JS
        $('[data-toggle="toggle"]').bootstrapToggle();
JS
        );
    }
    ?>
    <div class="col-lg-8">
        <?php if (Yii::$app->session->hasFlash('form')): ?>

            <div class="alert alert-success">
                <?= \Yii::t('c.RbK39OEY1j', 'Успешно обновлено') ?>.
            </div>

        <?php else: ?>

            <?php $form = ActiveForm::begin([
                'id'          => 'contact-form',
                'options'     => ['enctype' => 'multipart/form-data'],
                'layout'      => 'horizontal',
                'fieldConfig' => [
                    'template'             => "{label}\n{beginWrapper}\n{input}\n{hint}\n{error}\n{endWrapper}",
                    'horizontalCssClasses' => [
                        'label'   => 'col-sm-5',
                        'offset'  => 'col-sm-offset-4',
                        'wrapper' => 'col-sm-7',
                        'error'   => '',
                        'hint'    => '',
                    ],
                ],
            ]); ?>
            <?= $form->field($model, 'currency_view')->dropDownList(
                \yii\helpers\ArrayHelper::merge(['- '.Yii::t('c.RbK39OEY1j', 'Ничего не выбрано').' -'], \yii\helpers\ArrayHelper::map(
                    \common\models\avatar\Currency::find()->where(['in', 'id', [1, 2, 6]])->all(),
                    'id',
                    'code'
                ))
            ) ?>

            <hr class="featurette-divider">
            <?= Html::submitButton(Yii::t('c.RbK39OEY1j', 'Обновить'), [
                'class' => 'btn btn-default',
                'name'  => 'contact-button',
                'style' => 'width:100%',

            ]) ?>
            <?php ActiveForm::end(); ?>
        <?php endif; ?>
    </div>
    <div class="col-lg-4">
        <?= $this->render('_menu') ?>
    </div>

</div>