<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;
use app\models\UnionCategory;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model cs\base\BaseForm */

$this->title = 'Пакеты';


?>
<div class="container">
    <div class="col-lg-12">
        <h1 class="page-header"><?= Html::encode($this->title) ?></h1>
    </div>
    <div class="col-lg-8">

        <?php
        $this->registerJs(<<<JS

$('.buttonIN').click(function(e) {
    $('#modalInfo').modal();
});
$('.buttonSEND').click(function(e) {
    $('#sendToPull').modal();
});
$('.buttonOUT').click(function(e) {
    $('#returnToOwner').modal();
});
JS
);
        ?>

        <p>Пополнение баланса PZM</p>
        <p><button class="btn btn-default buttonIN">Пополнить баланс</button> </p>
        <p>Отправление PZM в пул</p>
        <p><button class="btn btn-default buttonSEND">Отправить в пул</button> </p>
        <p>Возврат PZM на кошелек владельца</p>
        <p><button class="btn btn-default buttonOUT">Вернуть PZM</button> </p>

    </div>
    <div class="col-lg-4">
        <?= $this->render('_menu') ?>
    </div>
</div>

<div class="modal fade" id="modalInfo" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">×</button>
                <h4 class="modal-title">Пополнение кошелька в системе балов</h4>
            </div>
            <div class="modal-body">
                <p>Для пополнение кошелька - переведите нужную сумму с кошелька <b>указанного в вашем аккаунте</b> на Мастер-кошелёк.</p>
                <div class="panel panel-default">
                    <div class="panel-body text-center"><b style="color: red; font-size: 26px;">PRIZM-GPN2-8CZ7-PNYP-8CEHG</b><p>759cc07249a9a45f063a2615f144931bb6fc5219385a496fb13df4bafea8812e</p>
                    </div>
                </div>
                <p>После окончании транзакции (1-5 минут) нажмите в личном кабинете кнопку <b>Синхронизации</b>
                </p><p style="font-style: italic;">После окончании транзакции (1-5 минут) нажмите в личном кабинете кнопку Синхронизации

                    Внимание! Для удачной синхронизации в системе не совершайте переводов со своего кошелька до синхронизации в ЛК. Также не используйте множество не синхронизированных переводов на Мастер-кошелёк для во избежании неточностей!
                </p>
                <p></p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal">Закрыть</button>
            </div>
        </div>
    </div>
</div>


<div class="modal fade" id="sendToPull" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">×</button>
                <h4 class="modal-title">Отправить в пул</h4>
            </div>
            <div class="modal-body">
                <h4>Хотите отправить в пул <span id="currentBalance">0</span> на процент <b id="currentPercent">0.7%</b> в день?</h4>
                <p></p>
                <div class="panel panel-default">
                    <div class="panel-body text-center">
                        <p>PZM отправленные в пул будут [?]</p>
                    </div>
                </div>
                <p class="text-center"></p>
            </div>
            <div class="modal-footer">
                <button class="btn btn-success" id="sendtopull">Отправить в пул</button>
                <button type="button" class="btn btn-danger" data-dismiss="modal">Закрыть</button>
            </div>
        </div>
    </div>
</div>


<div class="modal fade" id="returnToOwner" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">×</button>
                <h4 class="modal-title">Вернуть PZM</h4>
            </div>
            <div class="modal-body">
                <div class="text-center">
                    <table class="table">
                        <tbody><tr>
                            <th>Сумма</th>
                            <th><input type="text" id="summ" class="input-sm form-control"></th>
                        </tr>
                        <tr>
                            <th>Ключ</th>
                            <th><input type="text" id="field" class="input-sm form-control"></th>
                        </tr>
                        </tbody></table>
                </div>
                <p class="text-center"></p>
            </div>
            <div class="modal-footer">
                <button class="btn btn-success" id="returnPZM">Вернуть PZM</button>
                <button type="button" class="btn btn-danger" data-dismiss="modal">Закрыть</button>
            </div>
        </div>
    </div>
</div>