<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;
use app\models\UnionCategory;
use yii\db\Query;
use yii\helpers\Url;

/* @var $this yii\web\View */
$requestedRoute = Yii::$app->requestedRoute;

// получение дочернего модуля с идентификатором "forum"
$module = \Yii::$app->getModule('sroutes');

$menu = [
    [
        'route' => 'cabinet/profile',
        'label' => Yii::t('c.kTtfiHPqec', 'Профиль'),
    ],
    [
        'route' => 'cabinet/shop-requests',
        'label' => 'Заявки из магазина',
        'urlList' => [
            ['route', 'cabinet/shop-requests'],
            ['route', 'cabinet/shop-requests-item'],
        ],
    ],

    [
        'route'   => 'cabinet/card-action',
        'label'   => 'Модель поведения после сканирования QR кода вашей карты',
        'urlList' => [
            ['route', 'cabinet/card-action'],
        ],
    ],
    [
        'route'   => 'cabinet/ball',
        'label'   => 'Баллы',
        'urlList' => [
            ['route', 'cabinet/ball'],
        ],
    ],
    [
        'route'   => 'cabinet/zaim',
        'label'   => 'Займы',
        'urlList' => [
            ['route', 'cabinet/zaim'],
        ],
    ],
    [
        'route'   => 'cabinet/packet',
        'label'   => 'Пакеты',
        'urlList' => [
            ['route', 'cabinet/packet'],
        ],
    ],
    [
        'route'   => 'cabinet/chain',
        'label'   => 'Цепочки',
        'urlList' => [
            ['route', 'cabinet/chain'],
        ],
    ],
    [
        'route'   => 'cabinet/token',
        'label'   => 'Токены',
        'urlList' => [
            ['route', 'cabinet/token'],
        ],
    ],
];



function hasRoute12($item, $route)
{
    if (isset($item['items'])) {
        foreach ($item['items'] as $i) {
            if ($i['route'] == $route) {
                return true;
            }
        }
    }
    if (isset($item['route'])) {
        if ($item['route'] == $route) return true;
    }
    if (\yii\helpers\ArrayHelper::keyExists('urlList', $item)) {
        foreach ($item['urlList'] as $i) {
            switch ($i[0]) {
                case 'route':
                    if ($route == $i[1]) return true;
                    break;
                case 'startsWith':
                    if (\yii\helpers\StringHelper::startsWith($route, $i[1])) return true;
                    break;
                case 'controller':
                    $arr = explode('/', $route);
                    if ($arr[0] == $i[1]) return true;
                    break;
            }
        }
    }

    return false;
}

?>

<div class="list-group">
    <?php
    foreach ($menu as $item) {
        $options = ['class' => ['list-group-item']];

        if (hasRoute12($item, substr(Yii::$app->requestedRoute, 8))) {
            $options['class'][] = 'active';
        }
        if (\yii\helpers\ArrayHelper::keyExists('data', $item)) {
            $options['data'] = $item['data'];
        }
        $options['class'] = join(' ', $options['class']);
        echo Html::a($item['label'], [$item['route']], $options);
    }
    ?>
</div>
