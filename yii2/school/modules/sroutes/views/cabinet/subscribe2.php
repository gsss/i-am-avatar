<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \avatar\models\forms\Subscribe2 */

$this->title = Yii::t('c.UDkssceBqi', 'Подписки');
?>
<div class="container">
    <div class="col-lg-12">

        <h1 class="page-header"><?= Html::encode($this->title) ?></h1>


    </div>
    <div class="col-lg-8">
        <p><?= \Yii::t('c.UDkssceBqi', 'В этом разделе') ?></p>
        <p><?= \Yii::t('c.UDkssceBqi', 'Если вы') ?></p>
        <?php if (Yii::$app->session->hasFlash('form')): ?>

            <div class="alert alert-success">
                <?= \Yii::t('c.UDkssceBqi', 'Поздравляем вы успешно обновили подписку') ?>.
            </div>

        <?php else: ?>

            <?php $form = ActiveForm::begin([
                'layout'      => 'horizontal',
                'fieldConfig' => [
                    'template'             => "{label}\n{beginWrapper}\n{input}\n{hint}\n{error}\n{endWrapper}",
                    'horizontalCssClasses' => [
                        'label'   => 'col-sm-4',
                        'error'   => '',
                        'hint'    => '',
                    ],
                ],
            ]); ?>
            <?= $form->field($model, 'is_blog')->widget('common\widgets\CheckBox2\CheckBox') ?>
            <hr>
            <?= Html::submitButton(Yii::t('c.UDkssceBqi', 'Сохранить'), [
                'class' => 'btn btn-primary',
                'name'  => 'contact-button',
                'style' => 'width: 100%',
            ]) ?>
            <?php ActiveForm::end(); ?>

        <?php endif; ?>
    </div>
    <div class="col-lg-4">
        <?= $this->render('_menu') ?>
    </div>


</div>