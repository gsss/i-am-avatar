<?php
use common\models\school\PotokUser3Link;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;

/* @var $this yii\web\View */
/* @var $school \common\models\school\School */

$this->title = 'Заявки из магазина';

?>
<div class="container">
    <div class="col-lg-12">
        <h1 class="page-header"><?= Html::encode($this->title) ?></h1>
    </div>
    <div class="col-lg-8">
        <p>В этом разделе вы можете управлять своими заявками из разных магазинов.</p>


        <?php
        $this->registerJS(<<<JS

$('.rowTable').click(function() {
    window.location = '/cabinet/shop-requests-item' + '?' + 'id' + '=' + $(this).data('id');
});
JS
        );
        ?>
        <?= \yii\grid\GridView::widget([
            'dataProvider' => new \yii\data\ActiveDataProvider([
                'query' => \common\models\shop\Request::find()
                    ->where([
                        'user_id'   => Yii::$app->user->id,
                        'school_id' => $school->id,
                    ]),

                'sort' => ['defaultOrder' => ['created_at' => SORT_DESC]],
            ]),

            'tableOptions' => [
                'class' => 'table table-striped table-hover',
            ],
            'rowOptions'   => function ($item) {
                $data = [
                    'data'  => ['id' => $item['id']],
                    'role'  => 'button',
                    'class' => 'rowTable',
                ];
                return $data;
            },
            'summary'      => '',
            'columns'      => [
                'id',
                [
                    'header'         => 'Стоимость',
                    'headerOptions'  => ['class' => 'text-right'],
                    'contentOptions' => ['class' => 'text-right'],
                    'content'        => function ($item) {
                        $v = \yii\helpers\ArrayHelper::getValue($item, 'price', 0);
                        if ($v == 0) return '';
                        $currency_id = \yii\helpers\ArrayHelper::getValue($item, 'currency_id');
                        if (is_null($currency_id)) {
                            return '';
                        }
                        $c = \common\models\avatar\Currency::findOne($currency_id);
                        $cHtml = Html::tag('span', $c->code, ['class' => 'label label-info', 'style' => 'margin-left:5px;']);

                        return Yii::$app->formatter->asDecimal($v / 100, 2) . $cHtml ;
                    },
                ],


                [
                    'header'  => 'Платежная система',
                    'content' => function ($item) {
                        $billing_id = \yii\helpers\ArrayHelper::getValue($item, 'billing_id');

                        if (is_null($billing_id)) {
                            throw  new \cs\web\Exception('Не найден счет');
                        }
                        $billing = \common\models\BillingMain::findOne($billing_id);
                        $ps = $billing->getPaySystem();

                        return $ps->title;
                    },
                ],
                [
                    'header'  => 'Оплачено? (клиент)',
                    'content' => function ($item) {
                        $billing = \common\models\BillingMain::findOne($item['billing_id']);

                        if ($billing->is_paid_client) {
                            $html = Html::tag('span', 'Да', ['class' => 'label label-success']);
                        } else {
                            $html = Html::tag('span', 'Нет', ['class' => 'label label-default']);
                        }

                        return $html;
                    },
                ],
                [
                    'header'  => 'Оплачено? (магазин)',
                    'content' => function ($item) {
                        $v = \yii\helpers\ArrayHelper::getValue($item, 'is_paid', 0);

                        if ($v) {
                            $html = Html::tag('span', 'Да', ['class' => 'label label-success']);
                        } else {
                            $html = Html::tag('span', 'Нет', ['class' => 'label label-default']);
                        }

                        return $html;
                    },
                ],
                [
                    'header'  => 'Создано',
                    'content' => function ($item) {
                        $v = \yii\helpers\ArrayHelper::getValue($item, 'created_at', 0);
                        if ($v == 0) return '';

                        return Html::tag('abbr', \cs\services\DatePeriod::back($v, ['isShort' => true]), ['data' => ['toggle' => 'tooltip'], 'title' => Yii::$app->formatter->asDatetime($v)]);
                    },
                ],
//                [
//                    'header'  => 'Последнее сообщение',
//                    'content' => function ($item) {
//                        $v = \yii\helpers\ArrayHelper::getValue($item, 'last_message_time', 0);
//                        if ($v == 0) return '';
//
//                        return Html::tag('abbr', \cs\services\DatePeriod::back($v, ['isShort' => true]), ['data' => ['toggle' => 'tooltip'], 'title' => Yii::$app->formatter->asDatetime($v)]);
//                    },
//                ],
            ],
        ]) ?>


    </div>
    <div class="col-lg-4">
        <?= $this->render('_menu') ?>
    </div>


</div>