<?php
use common\models\school\PotokUser3Link;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \avatar\models\forms\Subscribe */

$this->title = 'Подписки';


$this->registerJs(<<<JS
$('.checkbox_m3').change(function(e) {
    ajaxJson({
        url: '/cabinet/subscribe-set',
        data: {
            name: $(this).attr('id'),
            value: $(this).is(':checked') 
        },
        success: function(ret) {
            
        }
    });
});
JS
);

?>
<div class="container">
    <div class="col-lg-12">

        <h1 class="page-header"><?= Html::encode($this->title) ?></h1>

    </div>
    <div class="col-lg-8">
        <p>В этом разделе вы можете управлять своими подписками на рассылки.</p>
        <p>Если вы включите опции то вы будете получать рассылку на почту которую указали при регистрации. Если
            выключите опцию то не будете получать. По умолчанию эти опции включены после регистрации.</p>


        <?php
        $potokLink = PotokUser3Link::findOne(['user_root_id' => Yii::$app->user->identity->user_root_id, 'potok_id' => 2]);
        $rows = [
            [
                'id'   => 999999,
                'name' => 'Рассылка',
                'form' => [
                    'mail'     => $potokLink->is_unsubscribed == 1 ? false : true,
                    'telegram' => $potokLink->is_unsubscribed_telegram == 1 ? false : true,
                ],
            ],
        ];
        $s_rows = \common\models\subscribe\UserSubscribe::find()->where(['user_id' => Yii::$app->user->id])->all();
        $actions = \common\models\subscribe\UserSubscribeAction::find()->all();
        /** @var \common\models\subscribe\UserSubscribeAction $action */
        foreach ($actions as $action) {
            $form = [
                'mail'     => false,
                'telegram' => false,
            ];
            /** @var \common\models\subscribe\UserSubscribe $s */
            foreach ($s_rows as $s) {
                if ($s->action_id == $action->id) {
                    $form = [
                        'mail'     => ($s->is_mail == 1) ? true : false,
                        'telegram' => ($s->is_telegram == 1) ? true : false,
                    ];
                    break;
                }
            }
            $rows[] = [
                'id'   => $action->id,
                'name' => $action->name,
                'form' => $form,
            ];
        }

        $rows2 = [];
        foreach ($rows as $row) {
            foreach ($row['form'] as $name => $value) {
                if ($value) $rows2[] = $name . $row['id'];
            }
        }
        Yii::$app->session->set(__FILE__, $rows2);
        ?>
        <?= \yii\grid\GridView::widget([
            'dataProvider' => new \yii\data\ArrayDataProvider([
                'allModels' => $rows,
            ]),
            'tableOptions' => [
                'class' => 'table table-striped table-hover',
            ],
            'rowOptions'   => function ($item) {
                $data = [
                    'data'  => ['id' => $item['id']],
                    'role'  => 'button',
                    'class' => 'rowTable',
                ];
                return $data;
            },
            'summary'      => '',
            'columns'      => [
                'name:text:Наименование',
                [
                    'header'  => 'Почта',
                    'content' => function ($item) {

                        return \common\widgets\CheckBox2\CheckBox::widget([
                            'model'     => new \avatar\models\M3(['user_roles' => Yii::$app->session->get(__FILE__)]),
                            'attribute' => 'mail' . $item['id'],
                            'options' => ['class' => 'checkbox_m3'],
                        ]);
                    },
                ],
                [
                    'header'  => 'Telegram',
                    'content' => function ($item) {

                        return \common\widgets\CheckBox2\CheckBox::widget([
                            'model'     => new \avatar\models\M3(['user_roles' => Yii::$app->session->get(__FILE__)]),
                            'attribute' => 'telegram' . $item['id'],
                            'options' => ['class' => 'checkbox_m3'],
                        ]);
                    },
                ],
            ],
        ]) ?>


    </div>
    <div class="col-lg-4">
        <?= $this->render('_menu') ?>
    </div>


</div>