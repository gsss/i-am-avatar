<?php

use common\models\avatar\Currency;
use common\models\avatar\UserBill;
use yii\helpers\Html;
use yii\helpers\Url;

/** @var $this yii\web\View */
/** @var $school \common\models\school\School */


$this->title = 'Договор Оферты';


?>

<div class="container" style="padding-bottom: 70px;">
    <div class="col-lg-12">
        <h1 class="page-header text-center">
            <?= $this->title ?>
        </h1>
    </div>

    <div class="col-lg-1">

    </div>
    <div class="col-sm-10">
        <h1>Договор оферты на предоставление консультаций, аналитических записок и стратегических прогнозов.</h1>
        <p><strong><span style="font-style: inherit; font-weight: inherit;">Договор-Оферты</span></strong></p>
        <p><span style="font-style: inherit; font-weight: inherit;">&nbsp;</span></p>
        <p><span style="font-style: inherit; font-weight: inherit;">О предоставлении консультаций и услуг в рамках проекта "МИГ", пользователям в сети Интернет.</span></p>
        <p><span style="font-style: inherit; font-weight: inherit;">Публичная оферта размещена на сайте непосредственного исполнителя услуг:&nbsp;migshoping.ru</span></p>
        <p><strong><span style="font-style: inherit; font-weight: inherit;">&nbsp;</span></strong></p>
        <p><strong><span style="font-style: inherit; font-weight: inherit;">Москва&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;1 октября 2018 г.</span></strong></p>
        <p><span style="font-style: inherit; font-weight: inherit;">Индивидуальный предприниматель Биткова Анна Сергеевна&nbsp;</span>ОГРНИП 318507400019101&nbsp;&nbsp;&nbsp;ИНН 507404443389<span style="font-style: inherit; font-weight: inherit;">, именуемый в дальнейшем "Исполнитель", предлагает консультации и другое информационное обслуживание в сети Интернет, любому юридическому и физическому лицу, именуемому в дальнейшем "Пользователь". В соответствии со статьей 438&nbsp; Гражданского Кодекса Российской Федерации (ГК РФ), безусловным принятием (акцептом) условий настоящей публичной оферты (далее - "оферты") считается осуществление Пользователем платежа в счет оплаты Услуг и получение соответствующего финансового документа, подтверждающего факт оплаты.</span></p>
        <ol>
            <li><strong><span style="font-style: inherit; font-weight: inherit;">ОБЩИЕ ПОЛОЖЕНИЯ</span></strong></li>
        </ol>
        <p><span style="font-style: inherit; font-weight: inherit;">1.1. Под Услугами понимается предоставление Исполнителем, Пользователю, информации&nbsp; в форме семинаров и консультаций, в том числе посредством проведения вебинаров и через систему дистанционного обучения и консультирования, смартфон терминалы и андроид приложения, дополнительную сервисную информацию и техническую поддержку связанную с непосредственным участием Пользователя в проекте.</span></p>
        <p><span style="font-style: inherit; font-weight: inherit;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 1.1.1 Доступ в личный кабинет пользователя.</span></p>
        <p><span style="font-style: inherit; font-weight: inherit;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 1.1.2 Актуальную информацию о проводимых маркетинговых исследованиях и подготовке аналитических отчетов прогнозов для Пользователя.</span></p>
        <p><span style="font-style: inherit; font-weight: inherit;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 1.1.3 Предоставление необходимых для подключения к Услуге и настройке программного обеспечения консультаций по электронной почте и телефону службы технической поддержки.</span></p>
        <p><span style="font-style: inherit; font-weight: inherit;">1.2. В состав Услуг не входит предоставление возможности приема-передачи электронных сообщений Пользователя в офисе Исполнителя, настройка или диагностика персонального компьютера, модема и программного обеспечения Пользователя как в офисе Исполнителя, так и с выездом к Участнику, а также обучение навыкам работы в сети Интернет и другие услуги, связанные с использованием Интернет или сайта Исполнителя или прямо (косвенно) сопутствующие темы, что регулируются настоящей офертой.</span></p>
        <ol start="2">
            <li><strong><span style="font-style: inherit; font-weight: inherit;">ПРАВА И ОБЯЗАННОСТИ СТОРОН</span></strong></li>
        </ol>
        <p><span style="font-style: inherit; font-weight: inherit;">2.1. Обязанности Исполнителя:</span></p>
        <p><span style="font-style: inherit; font-weight: inherit;">2.1.1. Предоставить Пользователю Услуги посредством регистрации Пользователю акаунта на сервере исполнителя.</span></p>
        <p><span style="font-style: inherit; font-weight: inherit;">2.1.2. Зарегистрировать Пользователя путем выдачи ему пароля и имени (логина) на бумажном носителе либо путем передачи пароля и логина на e-mail Пользователя указанный Пользователем при регистрации акаунта;</span></p>
        <p><span style="font-style: inherit; font-weight: inherit;">2.1.3. Предоставлять Услуги в соответствии с суммой оплаты и отображающейся в поле «личный счет», в кабинете пользователя.</span></p>
        <p><span style="font-style: inherit; font-weight: inherit;">2.1.4. Сохранять конфиденциальность информации Пользователя, полученной от него при регистрации, а также содержания частных сообщений электронной почты за исключением случаев, предусмотренных действующим законодательством РФ;</span></p>
        <p><span style="font-style: inherit; font-weight: inherit;">2.1.5. Публиковать официальные сообщения, связанные с обслуживанием Пользователей и изменением тарифов на оплату, на веб-сайте Исполнителя, а также рассылать их на электронную почту Пользователей.</span></p>
        <p><span style="font-style: inherit; font-weight: inherit;">2.1.6&nbsp; Исполнитель обязуется в течение 10 рабочих дней после окончания срока предоставления услуг, предоставить Пользователю акт оказанных услуг, на всю сумму договора. Акт оказанных услуг не предоставляется физическим лицам.</span></p>
        <p><span style="font-style: inherit; font-weight: inherit;">2.1.7 Предоставить Пользователю перечень и объем услуг согласно Справочной таблицы тарифов (пакетов авторизации услуг), опубликованных непосредственно на сайте Исполнителя:&nbsp;</span><span style="font-style: inherit; font-weight: inherit;">CityMig</span><span style="font-style: inherit; font-weight: inherit;">.</span><span style="font-style: inherit; font-weight: inherit;">ru</span><span style="font-style: inherit; font-weight: inherit;">&nbsp;или направленных Пользователю в виде документа для ознакомления средствами электронной или обычной почты.</span></p>
        <p><span style="font-style: inherit; font-weight: inherit;">2.2. Обязанности Пользователя:</span></p>
        <p><span style="font-style: inherit; font-weight: inherit;">2.2.1. Оплачивать Услуги Исполнителю в соответствии с выбранным тарифным планом, графиком обслуживания либо правилами оплаты выбранного информационного пакета. Пользователь обязуется самостоятельно знакомиться с информацией об условиях обслуживания и тарифах на веб-сайте Исполнителя&nbsp;</span><u><span style="font-style: inherit; font-weight: inherit;"><a href="http://www.citymig.ru%20/">migshoping.ru</a>&nbsp;</span></u><span style="font-style: inherit; font-weight: inherit;">в соответствующем разделе сайта.</span></p>
        <p><span style="font-style: inherit; font-weight: inherit;">2.2.2. Сохранять отправленные ему средствами электронной почты соответствующие финансовые документы, подтверждающие произведенную оплату Услуг по настоящему договору оферты;</span></p>
        <p><span style="font-style: inherit; font-weight: inherit;">2.2.3. Не реже одного раза в неделю знакомиться с официальной информацией, связанной с обслуживанием Пользователей, включая изменение тарифных планов Исполнителя, публикуемых в официальном порядке на сайте Исполнителя.</span></p>
        <p><span style="font-style: inherit; font-weight: inherit;">2.3. Права Исполнителя:</span></p>
        <p><span style="font-style: inherit; font-weight: inherit;">2.3.1. Временно прекратить предоставление Услуг Пользователю до получения Исполнителем письменных объяснений от Пользователя в следующих случаях:</span></p>
        <ul>
            <li><span style="font-style: inherit; font-weight: inherit;">не поступления оплаты за Услуги, в установленные Исполнителем сроки, а также по факту истечения срока уведомления о срочности погашения возникшей задолженности;</span></li>
            <li><span style="font-style: inherit; font-weight: inherit;">действия Пользователя, направленные на ограничение или препятствование в доступе других пользователей к Услугам, а также осуществления попыток несанкционированного доступа к ресурсам Исполнителя и к другим связанным с таким доступом системам, доступным через сеть Интернет;</span></li>
        </ul>
        <p><span style="font-style: inherit; font-weight: inherit;">2.3.2 Исполнитель вправе прекратить договорные отношения с Пользователем в одностороннем порядке, с одновременной отправкой письменного электронного уведомления, при нарушении Пользователем условий настоящей оферты. Моментом расторжения договора считается дата направления соответствующего сообщения Пользователю.</span></p>
        <p><span style="font-style: inherit; font-weight: inherit;">2.3.3 Исполнитель имеет право раскрывать сведения о Пользователе только в соответствии с законодательством РФ.</span></p>
        <p><span style="font-style: inherit; font-weight: inherit;">2.3.4. Исполнитель вправе изменять условия данной оферты в одностороннем порядке. Датой вступления в силу изменений настоящей оферты является дата их опубликования на веб-сайте Исполнителя.</span></p>
        <p><span style="font-style: inherit; font-weight: inherit;">2.4. Права Пользователя:</span></p>
        <p><span style="font-style: inherit; font-weight: inherit;">2.4.1. Требовать от Исполнителя предоставления Услуг в соответствии с условиями настоящей оферты.</span></p>
        <ol start="3">
            <li><strong><span style="font-style: inherit; font-weight: inherit;">СТОИМОСТЬ УСЛУГ И ПОРЯДОК РАСЧЕТОВ (Тарифы)</span></strong></li>
        </ol>
        <p><span style="font-style: inherit; font-weight: inherit;">3.1&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Стоимость Услуг определяется из расчета 1000 (Одна тысяча) рублей, за одну информационную емкость, в случае если количество информационных емкостей превышает единицу, Пользователь производит оплату кратно фактически потребляемым информационным емкостям, о чем Исполнитель извещает его письменно (в том числе средствами электронной почты).</span></p>
        <p><span style="font-style: inherit; font-weight: inherit;">3.2 Исполнитель вправе в одностороннем порядке пересматривать цены на Услуги и вводить новые цены (изменения Тарифов). О введении новых цен, Исполнитель,&nbsp; извещает Пользователя, опубликовав сообщение об этом на своем сайте. Датой вступления в силу новых тарифов является дата их опубликования на сайте Исполнителя.</span></p>
        <p><span style="font-style: inherit; font-weight: inherit;">3.3 Услуги предоставляются при условии наличия положительного баланса на условном лицевом счете Пользователя.</span></p>
        <p><span style="font-style: inherit; font-weight: inherit;">3.4 Оплата Услуг производится по безналичному или наличному расчету. Безналичная оплата может быть произведена Пользователем в любом Банке РФ безналичным путем, либо путем внесения наличных средств в любом банке РФ, под обязательства этого банка перечислить безналичные средства Исполнителю. При оплате через банк Пользователь обязан указать в платежном документе адрес сайта Исполнителя&nbsp;«</span><span style="font-style: inherit; font-weight: inherit;">citymig</span><span style="font-style: inherit; font-weight: inherit;">» или свой зарегистрированный в системе логин пользователя или персональный номер своего кабинета или&nbsp;&nbsp;номер контактного телефона или адрес электронной почты к которому привязан его аккаунт, в базе данных исполнителя.</span></p>
        <p><span style="font-style: inherit; font-weight: inherit;">3.5 Исполнитель вправе не предоставлять услуги, в случае если в платежном документе Пользователя не указан персональный логин (Имя Пользователя) или номер его личного кабинета, до момента приведения платежного документа в соответствие с этим требованием.</span></p>
        <p><span style="font-style: inherit; font-weight: inherit;">3.6 Пользователь самостоятельно несет ответственность за правильность производимых им платежей. При изменении банковских реквизитов Исполнителя, с момента опубликования новых реквизитов на его сайте, Пользователь самостоятельно несет ответственность за платежи, произведенные по устаревшим реквизитам.</span></p>
        <p><span style="font-style: inherit; font-weight: inherit;">3.7 Факт оплаты Услуги считается подтвержденным, а условный лицевой счет Пользователя&nbsp; открытым, после поступления сведений из банка о зачислении денежных средств на счет Исполнителя;</span></p>
        <p><span style="font-style: inherit; font-weight: inherit;">3.8 В течение 30 (тридцати) дней с даты образования нулевого баланса на условном лицевом счете Пользователя логин Пользователя и содержимое его почтового ящика сохраняются за Пользователем в резервном архиве на сервере Исполнителя. По истечении этого срока содержимое ящика электронной почты, папок проекта, папок с контентом Пользователя может быть автоматически удалено Исполнителем, без возможности дальнейшего восстановления (передачи)</span></p>
        <p><strong>4.Условия возврата средств .</strong></p>
        <p>4.1 За полностью оплаченную. и полученную консультацию или материалы предоставленные пользователю для дистанционного использования и получения консультаций дистанционно, через персональный компьютер, бук-ридер, VR-очки, представляет собой адресованное широкому кругу лиц публичное предложение, которое достаточно определенно, и выражает намерение получателя услуги - возврат денег не производится.</p>
        <p>4.2. Заказчик имеет право отказаться от предоставления оплаченных информационно-консультативных услуг и потребовать возврата денежных средств в следующих случаях:</p>
        <p>4.3. За оплаченный электронный товар, который сопутствует предоставленным консультациям, но только в тех случаях, когда он не был доставлен и не использовался в процессе предоставления консультаций. Возврат денег производится в течение 7 (семи) дней с момента оплаты. По истечении указанного времени денежные средства не возвращаются.</p>
        <p>4.4. Бесплатное предоставление информационно-консультативных услуг, бонусы, бесплатное участие в семинарах, бесплатные электронные товары, бесплатные физические товары возврату не подлежат.</p>
        <p>4.5. Обязательным условием для возврата денежных средств после предоставления консультаций Заказчику необходимо доказать: — что Заказчик в полном объеме применил те данные, которые были получены им в ходе консультаций или прохождения курса, произвел все необходимые действия, на которые указывал Эксперт; и несмотря на вышеперечисленные действия, результат, обещанный Экспертом, достигнут не был.</p>
        <p>4.6. Для получения возврата денежных средств Клиент должен прислать в установленный срок «заявку на возврат» на электронную почту службы поддержки: migshoping@yandex.ru</p>
        <p>4.7. В общих случаях, если не оговорено иное, заявка о возврате денежных средств должна быть подана в срок, не превышающий 14 календарный день с момента исполнения договора. В случае пропуска указанного срока, заявка не рассматривается, и денежные средства не возвращаются.</p>
        <p>4.8. Если заявка о возврате денежных средств предъявлена непосредственно во время исполнения договора, то уплаченная сумма возвращается Клиенту в части неполученных им информационно-консультационных услуг, из расчета от общей стоимости услуг и времени, фактически затраченных Исполнителем на их предоставление.</p>
        <p>4.9. Время рассмотрения заявки о возврате денежных средств составляет 15 календарных дней. В случае одобрения заявки, денежные средства возвращаются Клиенту в течение 15 календарных дней.</p>
        <p>5.<strong><span style="font-style: inherit; font-weight: inherit;">ОСОБЫЕ УСЛОВИЯ И ОТВЕТСТВЕННОСТЬ СТОРОН</span></strong></p>
        <p><span style="font-style: inherit; font-weight: inherit;">5.1 Исполнитель не гарантирует абсолютную бесперебойность или безошибочность функционирования обслуживаемых серверов и программ и не дает гарантию того, что предлагаемое программное обеспечение или любые другие материалы не содержат компьютерные вирусы и другие вредоносные компоненты. Исполнитель предпринимает все разумные усилия и меры с целью недопущения этого.</span></p>
        <p><span style="font-style: inherit; font-weight: inherit;">5.2 Исполнитель не несет ответственности за прямой или косвенный ущерб, причиненный Пользователю в результате&nbsp; использования или невозможности пользования Услугами или понесенный в результате ошибок, пропусков, перерывов в работе, удаления файлов, дефектов, задержек в работе или передаче данных, или изменения функций и других причин.</span></p>
        <p><span style="font-style: inherit; font-weight: inherit;">5.3 Исполнитель не гарантирует принятие почты (потерю, порчу сообщений) для Пользователя от удаленных сетей (серверов), функционирование которых привело к занесению адреса такой сети в списки, по которым программа доставки почты Провайдера не осуществляет прием почты.</span></p>
        <p><span style="font-style: inherit; font-weight: inherit;">5.4 Исполнитель не несет ответственности за качество каналов связи общего пользования, посредством которых Пользователь осуществляет доступ к его информационным Услугам и серверу.</span></p>
        <p><span style="font-style: inherit; font-weight: inherit;">5.5 Пользователь принимает на себя полную ответственность и риски, связанные с использованием сети Интернет посредством Услуг Исполнителя, в том числе ответственность за оценку точности, полноты и полезности любых мнений, идей, иной информации, а также качества и свойств товаров и услуг, распространяемых в Интернет и предоставляемых Пользователю&nbsp; посредством Услуг.</span></p>
        <p><span style="font-style: inherit; font-weight: inherit;">5.6 Пользователь полностью ответственен за сохранность своего пароля и за убытки, которые могут возникнуть по&nbsp; причине несанкционированного его использования. По факту кражи логина и пароля произошедшего по вине третьих лиц Пользователь вправе направить в адрес Исполнителя заявление о смене пароля (любых других личных данных), с обязательным приложением к заявлению соответствующего финансового документа, подтверждающего оплату Услуг. Исполнитель не несет ответственность за действия третьих лиц повлекшую кражу, а для возмещения денежных средств потраченных на украденное время, Пользователь вправе обратится в соответствующие следственные и правоохранительные органы.</span></p>
        <p><span style="font-style: inherit; font-weight: inherit;">5.7 Исполнитель не несет ответственности за извещение любых третьих сторон о лишении Пользователя доступа к серверу и за возможные последствия, возникшие в результате отсутствия такого предупреждения.</span></p>
        <p><span style="font-style: inherit; font-weight: inherit;">5.8 Исполнитель не является ответчиком или соответчиком по любым обязательствам и расходам, связанным с нарушением положений оферты Пользователем или другими лицами, использующими имя пользователя и пароль Пользователя; или связанным с использованием Интернет посредством Услуг; или связанным с помещением или передачей любого сообщения, информации, программного обеспечения или других материалов в сети И.нтернет Пользователем&nbsp; или другими лицами, использующими его логин и пароль.</span></p>
        <p><span style="font-style: inherit; font-weight: inherit;">6.</span><strong><span style="font-style: inherit; font-weight: inherit;">ПОРЯДОК РАССМОТРЕНИЯ ПРЕТЕНЗИЙ И СПОРОВ</span></strong></p>
        <p><span style="font-style: inherit; font-weight: inherit;">6.1. Претензии Пользователя по предоставляемым Услугам принимаются Исполнителем к рассмотрению только в письменном виде и в срок не позднее 3-х календарных дней с даты возникновения спорной ситуации. Срок рассмотрения претензий от Пользователя составляет не более 20 (двадцати) рабочих дней.</span></p>
        <p><span style="font-style: inherit; font-weight: inherit;">6.2 Рассмотрение претензий к Исполнителю, связанных с предоставлением Услуг, осуществляется при предъявлении Пользователем соответствующих финансовых документов, подтверждающих предоплату предоставленных Услуг.</span></p>
        <p><span style="font-style: inherit; font-weight: inherit;">6.3 Для решения технических вопросов при определении вины Пользователя в результате его неправомерных действий при пользовании сетью Интернет, Исполнитель вправе самостоятельно привлекать компетентные организации в качестве экспертов.</span></p>
        <p><span style="font-style: inherit; font-weight: inherit;">7.</span><strong><span style="font-style: inherit; font-weight: inherit;">МОМЕНТ ЗАКЛЮЧЕНИЯ ДОГОВОРА. СРОК ЕГО ДЕЙСТВИЯ.</span></strong></p>
        <p><strong><span style="font-style: inherit; font-weight: inherit;">ПОРЯДОК ИЗМЕНЕНИЯ И РАСТОРЖЕНИЯ</span></strong></p>
        <p><span style="font-style: inherit; font-weight: inherit;">7.1. Пользователь вправе в любое время в одностороннем порядке отказаться от Услуг Исполнителя.</span></p>
        <p><span style="font-style: inherit; font-weight: inherit;">7.2. В случае досрочного прекращения предоставления Услуг в соответствии настоящей Офертой Пользователю не производится возврат денежных средств. В случае пост-оплатой формы Пользователь обязан оплатить фактически использованное время и мощности из расчета количества фактически использованных информационных единиц или пакетов услуг.</span></p>
        <p><span style="font-style: inherit; font-weight: inherit;">7.3. Договор вступает в силу с момента внесения платы за Услуги в порядке, установленном настоящей офертой.</span></p>
        <p><span style="font-style: inherit; font-weight: inherit;">7.4. По всем вопросам, не урегулированным в настоящем тексте оферты, стороны руководствуются действующим законодательством РФ.</span></p>
        <p><span style="font-style: inherit; font-weight: inherit;">8. Реквизиты исполнителя</span></p>
        <p><strong>ИНДИВИДУАЛЬНЫЙ ПРЕДПРИНИМАТЕЛЬ:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</strong></p>
        <p><strong>БИТКОВА АННА СЕРГЕЕВНА</strong></p>
        <p>ОГРНИП 318507400019101&nbsp;&nbsp;&nbsp;ИНН 507404443389 КПП 0&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp;</p>
        <p>e-mail:migshoping@yandex.ru</p>
        <p><strong>Юридический адрес:</strong>&nbsp;МО, Подольский район, пос.Львовский, ул.Чеховская д.7</p>
        <p><strong>Адрес офиса:</strong>&nbsp;101000, г. Москва, Никитский бульвар д.12</p>
        <p><strong>Банк: Акционерное общество «Тинькофф Банк»&nbsp;</strong>&nbsp;БИК 044525974</p>
        <p>к/с 30101810145250000974 р/с 30232810100000000004</p>
        <p>&nbsp;</p>
        <p><strong><span style="font-style: inherit; font-weight: inherit;">Для безналичных и электронных платежей,&nbsp;</span></strong><strong><span style="font-style: inherit; font-weight: inherit;">Подпись Исполнителя не требуется.</span></strong><span style="font-style: inherit; font-weight: inherit;">&nbsp;</span></p>
        <p><span style="font-style: inherit; font-weight: inherit;">ГРАЖДАНСКИЙ КОДЕКС РОССИЙСКОЙ ФЕДЕРАЦИИ (Выдержка)</span></p>
        <p><span style="font-style: inherit; font-weight: inherit;">Статья 435. Оферта</span></p>
        <ol>
            <li><span style="font-style: inherit; font-weight: inherit;">Офертой признается адресованное одному или нескольким конкретным лицам предложение, которое достаточно определенно и выражает намерение лица, сделавшего предложение, считать себя заключившим договор с адресатом, которым будет принято предложение.</span></li>
        </ol>
        <p><span style="font-style: inherit; font-weight: inherit;">Оферта должна содержать существенные условия договора.</span></p>
        <ol start="2">
            <li><span style="font-style: inherit; font-weight: inherit;">Оферта связывает направившее ее лицо с момента ее получения адресатом. Если извещение об отзыве оферты поступило ранее или одновременно с самой офертой, оферта считается не полученной.</span></li>
        </ol>
        <p><span style="font-style: inherit; font-weight: inherit;">Статья 437. Приглашение делать оферты. Публичная оферта</span></p>
        <ol>
            <li><span style="font-style: inherit; font-weight: inherit;">Реклама и иные предложения, адресованные неопределенному кругу лиц, рассматриваются как приглашение делать оферты, если иное прямо не указано в предложении.</span></li>
            <li><span style="font-style: inherit; font-weight: inherit;">Содержащее все существенные условия договора предложение, из которого усматривается воля лица, делающего предложение, заключить договор на указанных в предложении условиях с любым, кто отзовется, признается офертой</span></li>
        </ol>
        <p><span style="font-style: inherit; font-weight: inherit;">&nbsp;</span>&nbsp;</p>
        <p>Предоставляя свои персональные данные Покупатель даёт согласие на&nbsp;обработку, хранение и&nbsp;использование своих персональных данных на&nbsp;основании ФЗ №&nbsp;152-ФЗ&nbsp;«О&nbsp;персональных данных» от&nbsp;27.07.2006&nbsp;г.&nbsp;в&nbsp;следующих целях:</p>
        <ul>
            <li>Регистрации Пользователя на&nbsp;сайте</li>
            <li>Осуществление клиентской поддержки</li>
            <li>Получения Пользователем информации о&nbsp;маркетинговых событиях</li>
            <li>Выполнение Продавцом обязательств перед Покупателем</li>
            <li>Проведения аудита и&nbsp;прочих внутренних исследований с&nbsp;целью повышения качества предоставляемых услуг.</li>
        </ul>
        <p>Под персональными данными подразумевается любая информация личного характера, позволяющая установить личность Покупателя такая как:</p>
        <ul>
            <li>Фамилия, Имя, Отчество</li>
            <li>Дата рождения</li>
            <li>Контактный телефон</li>
            <li>Адрес электронной почты</li>
            <li>Почтовый адрес</li>
        </ul>
        <p>Персональные данные Покупателей хранятся исключительно на&nbsp;электронных носителях и&nbsp;обрабатываются с&nbsp;использованием автоматизированных систем, за&nbsp;исключением случаев, когда неавтоматизированная обработка персональных данных необходима в&nbsp;связи с&nbsp;исполнением требований законодательства.</p>
        <p>Продавец обязуется не&nbsp;передавать полученные персональные данные третьим лицам, за&nbsp;исключением следующих случаев:</p>
        <ul>
            <li>По&nbsp;запросам уполномоченных органов государственной власти&nbsp;РФ только по&nbsp;основаниям и&nbsp;в&nbsp;порядке, установленным законодательством РФ</li>
            <li>Стратегическим партнерам, которые работают с&nbsp;Продавцом для предоставления продуктов и&nbsp;услуг, или тем из&nbsp;них, которые помогают Продавцу реализовывать продукты и&nbsp;услуги потребителям. Мы&nbsp;предоставляем третьим лицам минимальный объем персональных данных, необходимый только для оказания требуемой услуги или проведения необходимой транзакции.</li>
        </ul>
        <p>Продавец оставляет за&nbsp;собой право вносить изменения в&nbsp;одностороннем порядке в&nbsp;настоящие правила, при условии, что изменения не&nbsp;противоречат действующему законодательству РФ. Изменения условий настоящих правил вступают в&nbsp;силу после их&nbsp;публикации на&nbsp;Сайте.</p>
        <p><strong>ВНИМАНИЕ!</strong>&nbsp;Переходя на страницу оплаты, данного договора, Вы даете Продавцу свое согласие на обработку Ваших персональных данных.&nbsp;</p>
        <p>&nbsp;</p>
        <p>&nbsp;</p>            </div>
</div>


