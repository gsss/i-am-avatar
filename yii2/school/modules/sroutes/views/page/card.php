<?php
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
$this->title = 'Карта';
\common\assets\SlideShow\Asset::register($this);

$asset = Yii::$app->assetManager->getBundle('\school\modules\sroutes\assets\App\Asset');
$baseUrl = $asset->baseUrl;
?>

<div class="container" style="padding-bottom: 70px;">
    <div class="col-lg-12">
        <h1 class="page-header text-center">
            Карта
        </h1>
    </div>
    <div class="col-lg-6 col-lg-offset-3">
        <p class="text-center">
            <img src="<?= $baseUrl ?>/controller/page/card/credit-card-avatar-A21.png"
                 width="100%">
        </p>
    </div>
</div>


