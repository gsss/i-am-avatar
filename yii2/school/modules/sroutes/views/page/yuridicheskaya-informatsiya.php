<?php

use common\models\avatar\Currency;
use common\models\avatar\UserBill;
use yii\helpers\Html;
use yii\helpers\Url;

/** @var $this yii\web\View */
/** @var $school \common\models\school\School */


$this->title = 'Контакты и реквизиты';


?>

<div class="container" style="padding-bottom: 70px;">
    <div class="col-lg-12">
        <h1 class="page-header text-center">
            <?= $this->title ?>
        </h1>
    </div>

    <div class="col-lg-1">

    </div>
    <div class="col-sm-10">
        <h1>ИП Биткова</h1>
        <p><span style="font-style: inherit; font-weight: inherit;">ИНДИВИДУАЛЬНЫЙ ПРЕДПРИНИМАТЕЛЬ:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span><span style="font-style: inherit; font-weight: inherit;">БИТКОВА АННА СЕРГЕЕВНА</span></p>
        <p><span style="font-style: inherit; font-weight: inherit;">ОГРНИП 318507400019101&nbsp;&nbsp;&nbsp;ИНН 507404443389 КПП 0</span><span style="font-style: inherit; font-weight: inherit;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Тел: +7&nbsp;</span><span style="font-style: inherit; font-weight: inherit;">916&nbsp;126 06 21</span></p>
        <p><span style="font-style: inherit; font-weight: inherit;">e-mail:&nbsp;<a href="mailto:anna@citymig.ru">anna@citymig.ru</a>&nbsp;<a href="http://www.citimig.ru/">www.citimig.ru</a></span></p>
        <p><strong><span style="font-style: inherit; font-weight: inherit;">Юридический адрес:</span></strong><span style="font-style: inherit; font-weight: inherit;">&nbsp;МО, Подольский район, пос.Львовский, ул.Чеховская д.7</span></p>
        <p><strong><span style="font-style: inherit; font-weight: inherit;">Адрес офиса:</span></strong><span style="font-style: inherit; font-weight: inherit;">&nbsp;101000, г. Москва, Никитский бульвар д.12</span></p>
        <p><strong><span style="font-style: inherit; font-weight: inherit;">Банк: Акционерное общество «Тинькофф Банк» </span></strong><span style="font-style: inherit; font-weight: inherit;">&nbsp;БИК 044525974</span></p>
        <p><span style="font-style: inherit; font-weight: inherit;">к/с 30101810145250000974 р/с 30232810100000000004</span></p>
    </div>
</div>


