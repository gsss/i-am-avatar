<?php

use common\models\avatar\Currency;
use common\models\avatar\UserBill;
use school\modules\sroutes\models\Deal;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $ActiveDataProvider \yii\data\ActiveDataProvider */


$this->registerJS(<<<JS
$('[data-toggle="tooltip"]').tooltip();

$('.rowTable').click(function() {
});
$('.buttonBuy').click(function() {
    window.location = '/cabinet-exchange/deal-open' + '?' + 'id' + '=' + $(this).data('id');
});
JS
);
?>
<?= \yii\grid\GridView::widget([
    'dataProvider' => $ActiveDataProvider,
    'tableOptions' => [
        'class' => 'table table-striped table-hover',
    ],
    'summary'      => '',
    'rowOptions'   => function ($item) {
        $data = [
            'data'  => ['id' => $item['id']],
            'role'  => 'button',
            'class' => 'rowTable',
        ];
        return $data;
    },
    'columns'      => [
        'id',
        'name',
        [
            'header'        => 'Пользователь',
            'headerOptions' => [
            ],
            'content'       => function ($item) {
                $u = \common\models\UserAvatar::findOne($item['user_id']);
                $size = 40;
                $i = $u->getAvatar();
                $params = [
                    'class'  => "img-circle",
                    'width'  => $size,
                    'height' => $size,
                    'style'  => ['margin-bottom' => '0px'],
                    'data'   => ['toggle' => 'tooltip'],
                    'title'  => $u->getName2(),
                ];
                $params['style']['border'] = '1px solid #888';
                if (isset($params['style'])) {
                    if (is_array($params['style'])) {
                        $params['style'] = Html::cssStyleFromArray($params['style']);
                    }
                }

                return Html::img($i, $params);
            },
        ],
        [
            'header'        => 'Сделок',
            'headerOptions' => [
            ],
            'content'       => function ($item) {
                $count = Deal::find()
                    ->where(['offer_id' => $item['id']])
                    ->andWhere([
                        'status' => [
                            Deal::STATUS_MONEY_RECEIVED,
                            Deal::STATUS_CLOSE,
                            Deal::STATUS_AUDIT_FINISH,
                        ],
                    ])
                    ->count();

                return $count;

            },
        ],
        [
            'header'        => 'Цена',
            'headerOptions' => [
            ],
            'content'       => function ($item) {
                $currency = Currency::findOne($item['currency_id']);
                $price = $item['price'];
                $pricePrint = $price / pow(10, $currency->decimals);
                $decimalsPrint = ($currency->decimals > 2) ? 2 : $currency->decimals;

                $c = Html::tag('span', $currency->code, ['class' => 'label label-info', 'style' => 'margin-left: 5px;']);

                return Yii::$app->formatter->asDecimal($pricePrint, $decimalsPrint) . $c;
            },
        ],
        [
            'header'        => 'Объемы',
            'headerOptions' => [
            ],
            'content'       => function ($item) {
                $currency = Currency::findOne($item['currency_id']);
                $price = $item['price'];
                $pricePrint = $price / pow(10, $currency->decimals);
                $decimalsPrint = 0;

                $volume = $item['volume'];
                if (is_null($volume)) return '';
                try {
                    $data = \yii\helpers\Json::decode($volume);
                } catch (Exception $e) {
                    return '';
                }
                $data[0] = $data[0] / pow(10, $currency->decimals);
                $data[1] = $data[1] / pow(10, $currency->decimals);
                $from = Yii::$app->formatter->asDecimal($data[0], $decimalsPrint);
                $to = Yii::$app->formatter->asDecimal($data[1], $decimalsPrint);
                $print = Html::tag('span', 'от ', ['style' => 'color: #ccc;']) . $from . Html::tag('span', ' до ', ['style' => 'color: #ccc;']) . $to;

                $c = Html::tag('span', $currency->code, ['class' => 'label label-info', 'style' => 'margin-left: 5px;']);

                return $print . $c;
            },
        ],
        [
            'header'        => 'Платежная система',
            'headerOptions' => [
            ],
            'content'       => function ($item) {
                $list = \school\modules\sroutes\models\OfferPayLink::find()->where(['offer_id' => $item['id']])->select('pay_id')->column();
                $listName = \school\modules\sroutes\models\PayMethod::find()->where(['in', 'id', $list])->select('name')->column();

                return join(',', $listName);
            },
        ],
        [
            'header'  => '',
            'content' => function ($item) {
                $currency = Currency::findOne($item['currency_id']);
                $price = $item['price'];
                $pricePrint = $price / pow(10, $currency->decimals);
                $decimalsPrint = ($currency->decimals > 2) ? 2 : $currency->decimals;
                $print = Yii::$app->formatter->asDecimal($pricePrint, $decimalsPrint);

                $action = ($item['type_id'] == \school\modules\sroutes\models\Offer::TYPE_ID_BUY) ? 'Продать' : 'Купить';
                return Html::button($action . ' за ' . $print, [
                    'class' => 'btn btn-default buttonBuy',
                    'data'  => [
                        'id' => $item['id'],
                    ],
                ]);
            },
        ],
    ],
]) ?>