<?php

use common\models\avatar\Currency;
use common\models\avatar\UserBill;
use school\modules\sroutes\models\Deal;
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;

/** @var $this yii\web\View */
/** @var $Deal Deal */


$this->title = 'Сделка #' . $Deal->id;

$offer = $Deal->getOffer();
$user = \common\models\UserAvatar::findOne($offer->user_id);
$method = \school\modules\sroutes\models\PayMethod::findOne($offer->pay_id);

$asset = \cs\assets\CountDownAsset::register($this);
$countUrl = Yii::$app->assetManager->getBundle('\cs\assets\CountDownAsset')->baseUrl;

?>

<div class="container" style="padding-bottom: 70px;">
    <div class="col-lg-12">
        <h1 class="page-header text-center">
            <?= $this->title ?>
        </h1>
        <h2>Я <?= (($offer->type_id == \school\modules\sroutes\models\Offer::TYPE_ID_SELL)? (($Deal->user_id == Yii::$app->user->id)? 'покупаю' : 'продаю') : (($Deal->user_id == Yii::$app->user->id)? 'продаю' : 'покупаю')) . '. ' . $user->getName2() ?></h2>
    </div>
    <div class="col-lg-12">
        <div id="counter">

        </div>
        <?php
        $this->registerJs(<<<JS
var d1 = new Date({$Deal->created_at}000 + (60*90*1000));

$('#counter').countdown({
    format: "hh:mm:ss",
    endTime: d1,
    image: '{$countUrl}/img/digits.png'
  });
JS
        )
        ?>
    </div>
    <div class="col-lg-6">
        <h2 class="page-header text-center">
            Чат
        </h2>

        <div id="chat" data-chat="<?= Html::encode(\yii\helpers\Json::encode([
            'user1' => (int)$Deal->user_id,
            'user2' => (int)$offer->user_id,
        ])) ?>">
            <?= $this->render('deal-action-chat', ['deal' => $Deal, 'offer' => $offer]) ?>
        </div>
    </div>
    <div class="col-lg-6">
        <h1 class="page-header text-center">
            Статус сделки и информация о предложении
        </h1>
        <p>Начало сделки: <?= Yii::$app->formatter->asDatetime($Deal->created_at) . ' ' . '(' .\cs\services\DatePeriod::back($Deal->created_at).')' ?></p>
        <p>Статус: <span class="label label-info"><?= Deal::$statusList[$Deal->status] ?></span>
        </p>


        <?php // Тип сделки Продажа ?>
        <?php if ($offer->type_id == \school\modules\sroutes\models\Offer::TYPE_ID_SELL) { ?>
            <?php // Если это сам продавец ?>
            <?php if ($offer->user_id == Yii::$app->user->id) { ?>
                <?php // Если статус = 'Сделка открыта' ?>
                <?php if ($Deal->status == Deal::STATUS_OPEN) { ?>
                    <?php $this->registerJs(<<<JS
$('.buttonSellAccept').click(function(e) {
    ajaxJson({
        url: '/cabinet-exchange/sell-open-accept' + '?id=' + {$Deal->id},
        success: function(ret) {
            window.location.reload();
        }
        
    });
})
JS
                    ) ?>
                    <button class="btn btn-default buttonSellAccept" data-id="<?= $Deal->id ?>">Принять сделку</button>
                <?php } ?>
                <?php // Если статус = 'Деньги отправлены' ?>
                <?php if ($Deal->status == Deal::STATUS_MONEY_SENDED) { ?>
                    <?php $this->registerJs(<<<JS
$('.buttonSellMoneyReceived').click(function(e) {
    ajaxJson({
        url: '/cabinet-exchange/sell-money-received' + '?id=' + {$Deal->id},
        success: function(ret) {
            window.location.reload();
        }
        
    });
})
JS
                    ) ?>
                    <button class="btn btn-default buttonSellMoneyReceived" data-id="<?= $Deal->id ?>">Деньги получены</button>
                <?php } ?>
            <?php } ?>


            <?php // Если это сам покупатель ?>
            <?php if ($Deal->user_id == Yii::$app->user->id) { ?>
                <?php // Если статус = 'Сделка подтверждена и монеты заблокированы' ?>
                <?php if ($Deal->status == Deal::STATUS_BLOCK) { ?>
                    <?php $this->registerJs(<<<JS
$('.buttonSellMoneySended').click(function(e) {
    ajaxJson({
        url: '/cabinet-exchange/sell-money-sended' + '?id=' + {$Deal->id},
        success: function(ret) {
            window.location.reload();
        }
        
    });
})
JS
                    ) ?>
                    <button class="btn btn-default buttonSellMoneySended" data-id="<?= $Deal->id ?>">Деньги отправлены</button>
                <?php } ?>
                <?php // Если статус = 'Деньги получены' ?>
                <?php if ($Deal->status == Deal::STATUS_MONEY_RECEIVED) { ?>
                    <p>Оцените сделку</p>
                    <?php $this->registerJs(<<<JS
$('.buttonSellAssessmentDown').click(function(e) {
    ajaxJson({
        url: '/cabinet-exchange/sell-assessment-down' + '?id=' + {$Deal->id},
        success: function(ret) {
            window.location.reload();
        }
        
    });
});
$('.buttonSellAssessmentNormal').click(function(e) {
    ajaxJson({
        url: '/cabinet-exchange/sell-assessment-normal' + '?id=' + {$Deal->id},
        success: function(ret) {
            window.location.reload();
        }
        
    });
});
$('.buttonSellAssessmentUp').click(function(e) {
    ajaxJson({
        url: '/cabinet-exchange/sell-assessment-up' + '?id=' + {$Deal->id},
        success: function(ret) {
            window.location.reload();
        }
        
    });
});
JS
                    ) ?>
                    <div class="btn-group" role="group" aria-label="...">
                        <button type="button" class="btn btn-default buttonSellAssessmentDown"><span class="glyphicon glyphicon-thumbs-down"></span></button>
                        <button type="button" class="btn btn-default buttonSellAssessmentNormal"><span class="glyphicon glyphicon-certificate"></span></button>
                        <button type="button" class="btn btn-default buttonSellAssessmentUp"><span class="glyphicon glyphicon-thumbs-up"></span></button>
                    </div>
                <?php } ?>
            <?php } ?>
        <?php } ?>




        <?php // Тип сделки Покупка ?>
        <?php if ($offer->type_id == \school\modules\sroutes\models\Offer::TYPE_ID_BUY) { ?>
            <?php // Если это сам Покупатель ?>
            <?php if ($offer->user_id == Yii::$app->user->id) { ?>
                <?php // Если статус = 'Сделка открыта' ?>
                <?php if ($Deal->status == Deal::STATUS_OPEN) { ?>
                    <?php $this->registerJs(<<<JS
$('.buttonBuyAccept').click(function(e) {
    ajaxJson({
        url: '/cabinet-exchange/buy-open-accept' + '?id=' + {$Deal->id},
        success: function(ret) {
            window.location.reload();
        }
        
    });
})
JS
                    ) ?>
                    <button class="btn btn-default buttonBuyAccept" data-id="<?= $Deal->id ?>">Принять сделку</button>
                <?php } ?>

                <?php // Если статус = 'Сделка подтверждена и монеты заблокированы' ?>
                <?php if ($Deal->status == Deal::STATUS_BLOCK) { ?>
                    <?php $this->registerJs(<<<JS
$('.buttonBuyMoneySended').click(function(e) {
    ajaxJson({
        url: '/cabinet-exchange/buy-money-sended' + '?id=' + {$Deal->id},
        success: function(ret) {
            window.location.reload();
        }
        
    });
})
JS
                    ) ?>
                    <button class="btn btn-default buttonBuyMoneySended" data-id="<?= $Deal->id ?>">Деньги отправлены</button>
                <?php } ?>
            <?php } ?>


            <?php // Если это сам покупатель ?>
            <?php if ($Deal->user_id == Yii::$app->user->id) { ?>
                <?php // Если статус = 'Деньги отправлены' ?>
                <?php if ($Deal->status == Deal::STATUS_MONEY_SENDED) { ?>
                    <?php $this->registerJs(<<<JS
$('.buttonBuyMoneyReceived').click(function(e) {
    ajaxJson({
        url: '/cabinet-exchange/buy-money-received' + '?id=' + {$Deal->id},
        success: function(ret) {
            window.location.reload();
        }
        
    });
})
JS
                    ) ?>
                    <button class="btn btn-default buttonBuyMoneyReceived" data-id="<?= $Deal->id ?>">Деньги получены</button>
                <?php } ?>

                <?php // Если статус = 'Деньги получены' ?>
                <?php if ($Deal->status == Deal::STATUS_MONEY_RECEIVED) { ?>
                    <p>Оцените сделку</p>
                    <?php $this->registerJs(<<<JS
$('.buttonBuyAssessmentDown').click(function(e) {
    ajaxJson({
        url: '/cabinet-exchange/buy-assessment-down' + '?id=' + {$Deal->id},
        success: function(ret) {
            window.location.reload();
        }
        
    });
});
$('.buttonBuyAssessmentNormal').click(function(e) {
    ajaxJson({
        url: '/cabinet-exchange/buy-assessment-normal' + '?id=' + {$Deal->id},
        success: function(ret) {
            window.location.reload();
        }
        
    });
});
$('.buttonBuyAssessmentUp').click(function(e) {
    ajaxJson({
        url: '/cabinet-exchange/buy-assessment-up' + '?id=' + {$Deal->id},
        success: function(ret) {
            window.location.reload();
        }
        
    });
});
JS
                    ) ?>
                    <div class="btn-group" role="group" aria-label="...">
                        <button type="button" class="btn btn-default buttonBuyAssessmentDown"><span class="glyphicon glyphicon-thumbs-down"></span></button>
                        <button type="button" class="btn btn-default buttonBuyAssessmentNormal"><span class="glyphicon glyphicon-certificate"></span></button>
                        <button type="button" class="btn btn-default buttonBuyAssessmentUp"><span class="glyphicon glyphicon-thumbs-up"></span></button>
                    </div>
                <?php } ?>
            <?php } ?>
        <?php } ?>
        <hr>
        <?php if (in_array($Deal->status, [Deal::STATUS_BLOCK, Deal::STATUS_MONEY_SENDED])) { ?>
            <p>
                <?php $this->registerJs(<<<JS
$('.buttonInviteArbitrator').click(function(e) {
    ajaxJson({
        url: '/cabinet-exchange/invite-arbitrator' + '?id=' + {$Deal->id},
        success: function(ret) {
            window.location.reload();
        }
        
    });
});

JS
                ) ?>
                <button class="btn btn-warning buttonInviteArbitrator" data-id="<?= $Deal->id ?>">Пригласить арбитра</button>
            </p>
        <?php } ?>

    </div>
</div>