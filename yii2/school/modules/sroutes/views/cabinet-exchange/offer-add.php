<?php

use common\models\avatar\Currency;
use common\models\avatar\UserBill;
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;

/** @var $this yii\web\View */
/** @var $form yii\bootstrap\ActiveForm */
/** @var $model \avatar\models\forms\exchange\OfferAdd */


$this->title = 'Добавить предложение';


?>

<div class="container" style="padding-bottom: 70px;">
    <div class="col-lg-12">
        <h1 class="page-header text-center">
            <?= $this->title ?>
        </h1>

    </div>

    <div class="col-lg-6 col-lg-offset-3">
        <?php if (Yii::$app->session->hasFlash('form')) : ?>

            <div class="alert alert-success">
                Успешно добавлено.
            </div>

            <P><a class="btn btn-info" href="/cabinet-exchange/offer">Мои предложения</a></P>

        <?php else: ?>

            <h2>Курс 1 RUB = 1 VVB</h2>
            <hr>

            <?php $form = ActiveForm::begin([
                'options' => ['enctype' => 'multipart/form-data'],
            ]); ?>
            <?= $form->field($model, 'name') ?>
            <?= $form->field($model, 'type_id')
            ->dropDownList(
                [
                    \school\modules\sroutes\models\Offer::TYPE_ID_BUY  => 'Купить',
                    \school\modules\sroutes\models\Offer::TYPE_ID_SELL => 'Продать',
                ]
            ) ?>
            <?= $form->field($model, 'pay_list')
            ->checkboxList(
                \yii\helpers\ArrayHelper::map(
                    \school\modules\sroutes\models\PayMethod::find()->all(),
                    'id',
                    'name'
                )
            )->label('Платежные системы') ?>

            <?= $form->field($model, 'price') ?>

            <?= $form->field($model, 'currency_id')->dropDownList(
            [
                6 => 'RUB',
                2 => 'USD',
            ]
        ) ?>
            <?= $form->field($model, 'volume_start') ?>
            <?= $form->field($model, 'volume_end') ?>

            <?= $form->field($model, 'condition')->textarea(['rows' => 10]) ?>


            <hr>
            <div class="form-group">
                <?= Html::submitButton('Добавить', [
                    'class' => 'btn btn-default',
                    'name'  => 'contact-button',
                    'style' => 'width:100%',
                ]) ?>
            </div>
            <?php ActiveForm::end(); ?>

        <?php endif; ?>
    </div>


</div>