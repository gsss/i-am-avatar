<?php

use common\models\avatar\Currency;
use common\models\avatar\UserBill;
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;

/** @var $this yii\web\View */
/** @var $form yii\bootstrap\ActiveForm */
/** @var $model \avatar\models\forms\exchange\DealOpen */
/** @var $offer \school\modules\sroutes\models\Offer */


$this->title = 'Открытие сделки';


?>

<div class="container" style="padding-bottom: 70px;">
    <div class="col-lg-12">
        <h1 class="page-header text-center">
            <?= $this->title ?>
        </h1>
        <?php
        $user = \common\models\UserAvatar::findOne($offer->user_id);
        ?>
        <h2>Я <?= (($offer->type_id == \school\modules\sroutes\models\Offer::TYPE_ID_SELL)? 'покупаю' : 'продаю') . '. ' . $user->getName2() ?></h2>
    </div>


    <?php if (Yii::$app->session->hasFlash('form')) : ?>

    <div class="col-lg-12">
        <p class="alert alert-success">
            Успешно добавлено.
        </p>

        <p><a class="btn btn-info" href="/cabinet-exchange/index">Мои предложения</a></p>
        <p><a class="btn btn-info" href="<?= Url::to(['cabinet-exchange/deal-action', 'id' => Yii::$app->session->getFlash('form')]) ?>">Перейти к сделке</a></p>
    </div>

    <?php else: ?>

        <div class="col-lg-6">
            <?php
            $this->registerJs(<<<JS
$('#dealopen-volume').on('input',function(e) {
    var priceVVB = $('#priceVVB').val();
    var thisVal = $(this).val();
    $('#dealopen-volume_vvb').val(thisVal/priceVVB);
});
$('#dealopen-volume_vvb').on('input',function(e) {
    var priceVVB = $('#priceVVB').val();
    var thisVal = $(this).val();
    $('#dealopen-volume').val(thisVal*priceVVB);
});
JS
);
            ?>

            <h2>Открыть сделку</h2>

            <?php
            $currency = Currency::findOne($offer['currency_id']);
            $price = $offer['price'];
            $pricePrint = $price / pow(10, $currency->decimals);
            $decimalsPrint = 0;

            $volume = $offer['volume'];
            if (is_null($volume)) return '';
            try {
                $data = \yii\helpers\Json::decode($volume);
            } catch (Exception $e) {
                return '';
            }
            $data[0] = $data[0] / pow(10, $currency->decimals);
            $data[1] = $data[1] / pow(10, $currency->decimals);
            $from = Yii::$app->formatter->asDecimal($data[0], $decimalsPrint);
            $to = Yii::$app->formatter->asDecimal($data[1], $decimalsPrint);
            $print = Html::tag('span','от ',['style' => 'color: #ccc;']) . $from . Html::tag('span',' до ',['style' => 'color: #ccc;']) . $to;
            $c = Html::tag('span', $currency->code, ['class' => 'label label-info', 'style' => 'margin-left: 5px;']);

            ?>
            <p>Сумма сделки <?= $print  . $c ?></p>


            <?php $form = ActiveForm::begin([
                'options' => ['enctype' => 'multipart/form-data']
            ]); ?>
            <?= $form->field($model, 'volume') ?>
            <?= $form->field($model, 'volume_vvb') ?>

            <?php

            $currencyRUB = Currency::findOne(1);
            $currencyPZM = Currency::findOne(172);
            $currencyUSD = Currency::findOne(2);
            $currencyVVB = Currency::findOne(227);

            $priceVVB = '';
            // RUB
            if ($offer->currency_id == 1) {
                $priceVVB = $offer->price / pow(10, $currencyRUB->decimals);
            }
            // USD
            if ($offer->currency_id == 2) {
                $priceVVB = $offer->price / pow(10, $currencyUSD->decimals);
            }
            ?>
            <?= Html::hiddenInput(null, $priceVVB, ['id' => 'priceVVB']) ?>
            <hr>
            <div class="form-group">
                <?= Html::submitButton('Открыть сделку', [
                    'class' => 'btn btn-default',
                    'name'  => 'contact-button',
                    'style' => 'width:100%',
                ]) ?>
            </div>
            <?php ActiveForm::end(); ?>
        </div>
        <div class="col-lg-6">
            <h2>1 VVB = <?= Yii::$app->formatter->asDecimal($offer->price / 100, 2) ?> <?= $currency->code ?></h2>

            <h2><?= (($offer->type_id == \school\modules\sroutes\models\Offer::TYPE_ID_SELL)? 'О продавце' : 'О покупателе') ?></h2>

            <p><img src="<?= $user->getAvatar() ?>" width="80" class="img-circle"></p>
            <p><?= $user->getName2() ?></p>
            <p>Сделок: <?= $count = \school\modules\sroutes\models\Deal::find()->where(['user_id' => $user->id])->count(); ?></p>

            <h2>Условия сделки</h2>
            <p><?= Html::encode($offer->condition) ?></p>

        </div>



    <?php endif; ?>



</div>