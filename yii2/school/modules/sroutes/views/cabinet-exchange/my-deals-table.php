<?php

use common\models\avatar\Currency;
use common\models\avatar\UserBill;
use school\modules\sroutes\models\Deal;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $ActiveDataProvider \yii\data\ActiveDataProvider */


$this->registerJS(<<<JS
$('[data-toggle="tooltip"]').tooltip();

$('.rowTable').click(function() {
    window.location = '/cabinet-exchange/deal-action' + '?' + 'id' + '=' + $(this).data('id');
});
JS
);
?>
<?= \yii\grid\GridView::widget([
    'dataProvider' => $ActiveDataProvider,
    'tableOptions' => [
        'class' => 'table table-striped table-hover',
    ],
    'summary'      => '',
    'rowOptions'   => function ($item) {
        $data = [
            'data'  => ['id' => $item['id']],
            'role'  => 'button',
            'class' => 'rowTable',
        ];
        return $data;
    },
    'columns'      => [
        'id',
        [
            'header'        => 'Моя сделка?',

            'content'       => function ($item) {
                if ($item['user_id'] == Yii::$app->user->id) {
                    return Html::tag('span', 'Да', ['class' => 'label label-success']);
                }
                return Html::tag('span', 'Нет', ['class' => 'label label-default']);

            },
        ],
        [
            'header'        => 'Тип сдлеки',

            'content'       => function ($item) {
                if ($item['type_id'] == \school\modules\sroutes\models\Offer::TYPE_ID_BUY) {
                    return 'Я покупаю';
                }
                if ($item['type_id'] == \school\modules\sroutes\models\Offer::TYPE_ID_SELL) {
                    return 'Я продаю';
                }
                return '';
            },
        ],
        [
            'header'        => 'Цена',

            'content'       => function ($item) {
                $currency = Currency::findOne($item['currency_id']);
                $price = $item['price'];
                $pricePrint = $price / pow(10, $currency->decimals);
                $decimalsPrint = ($currency->decimals > 2) ? 2 : $currency->decimals;

                $c = Html::tag('span', $currency->code, ['class' => 'label label-info', 'style' => 'margin-left: 5px;']);

                return Yii::$app->formatter->asDecimal($pricePrint, $decimalsPrint) . $c;
            },
        ],
        [
            'header'        => 'Статус',

            'content'       => function ($item) {
                if (!isset( Deal::$statusList[$item['status']])) {
                    return '';
                }

                return
                    Html::tag('span', Deal::$statusList[$item['status']], ['class' => 'label label-info']);

            },
        ],
        [
            'header'  => 'Создано',
            'content' => function ($item) {
                $v = \yii\helpers\ArrayHelper::getValue($item, 'created_at', 0);
                if ($v == 0) return '';

                return Html::tag('abbr', \cs\services\DatePeriod::back($v, ['isShort' => true]), ['data' => ['toggle' => 'tooltip'], 'title' => Yii::$app->formatter->asDatetime($v)]);
            }
        ],
        [
            'header'  => 'Объем',
            'content' => function ($item) {
                $currency_id = $item['currency_id'];

                $currency = Currency::findOne($currency_id);

                $v = \yii\helpers\ArrayHelper::getValue($item, 'volume', 0);
                if ($v == 0) return '';

                return Yii::$app->formatter->asDecimal($v / pow(10, $currency->decimals), $currency->decimals) . Html::tag('span', $currency->code, ['class' => 'label label-info', 'style' => 'margin-left: 5px;']);
            }
        ],
        [
            'header'  => 'Объем VVB',
            'content' => function ($item) {
                $currency = Currency::findOne(Currency::VVB);

                $v = \yii\helpers\ArrayHelper::getValue($item, 'volume_vvb', 0);
                if ($v == 0) return '';

                return Yii::$app->formatter->asDecimal($v / pow(10, $currency->decimals), $currency->decimals) . Html::tag('span', $currency->code, ['class' => 'label label-info', 'style' => 'margin-left: 5px;']);
            }
        ],
    ],
]) ?>