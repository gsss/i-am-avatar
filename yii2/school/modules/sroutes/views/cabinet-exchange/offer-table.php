<?php

use common\models\avatar\Currency;
use common\models\avatar\UserBill;
use school\modules\sroutes\models\Deal;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $ActiveDataProvider \yii\data\ActiveDataProvider */


$this->registerJS(<<<JS
$('[data-toggle="tooltip"]').tooltip();

$('.rowTable').click(function() {
});
$('.buttonBuy').click(function() {
    window.location = '/cabinet-exchange/deal-open' + '?' + 'id' + '=' + $(this).data('id');
});
JS
);
?>
<?= \yii\grid\GridView::widget([
    'dataProvider' => $ActiveDataProvider,
    'tableOptions' => [
        'class' => 'table table-striped table-hover',
    ],
    'summary'      => '',
    'rowOptions'   => function ($item) {
        $data = [
            'data'  => ['id' => $item['id']],
            'role'  => 'button',
            'class' => 'rowTable',
        ];
        return $data;
    },
    'columns'      => [
        'id',
        'name',
        [
            'header'        => 'Тип предложения',

            'content'       => function ($item) {
                if ($item['type_id'] == \school\modules\sroutes\models\Offer::TYPE_ID_BUY) {
                    return 'Я покупаю';
                }
                if ($item['type_id'] == \school\modules\sroutes\models\Offer::TYPE_ID_SELL) {
                    return 'Я продаю';
                }
                return '';
            },
        ],
        [
            'header'        => 'Сделок',

            'content'       => function ($item) {
                $count = Deal::find()
                    ->where(['offer_id' => $item['id']])
                    ->andWhere(['status' => [
                        Deal::STATUS_MONEY_RECEIVED,
                        Deal::STATUS_CLOSE,
                        Deal::STATUS_AUDIT_FINISH,
                    ]])
                    ->count();

                return $count;

            },
        ],
        [
            'header'        => 'Цена',

            'content'       => function ($item) {
                $currency = Currency::findOne($item['currency_id']);
                $price = $item['price'];
                $pricePrint = $price / pow(10, $currency->decimals);
                $decimalsPrint = ($currency->decimals > 2) ? 2 : $currency->decimals;

                $c = Html::tag('span', $currency->code, ['class' => 'label label-info', 'style' => 'margin-left: 5px;']);

                return Yii::$app->formatter->asDecimal($pricePrint, $decimalsPrint) . $c;
            },
        ],
        [
            'header'        => 'Объемы',

            'content'       => function ($item) {
                $currency = Currency::findOne($item['currency_id']);
                $price = $item['price'];
                $pricePrint = $price / pow(10, $currency->decimals);
                $decimalsPrint = 0;

                $volume = $item['volume'];
                if (is_null($volume)) return '';
                try {
                    $data = \yii\helpers\Json::decode($volume);
                } catch (Exception $e) {
                    return '';
                }
                $data[0] = $data[0] / pow(10, $currency->decimals);
                $data[1] = $data[1] / pow(10, $currency->decimals);
                $from = Yii::$app->formatter->asDecimal($data[0], $decimalsPrint);
                $to = Yii::$app->formatter->asDecimal($data[1], $decimalsPrint);
                $print = Html::tag('span','от ',['style' => 'color: #ccc;']) . $from . Html::tag('span',' до ',['style' => 'color: #ccc;']) . $to;

                $c = Html::tag('span', $currency->code, ['class' => 'label label-info', 'style' => 'margin-left: 5px;']);

                return $print  . $c;
            },
        ],
        [
            'header'        => 'Платежная система',

            'content'       => function ($item) {
                $list = \school\modules\sroutes\models\OfferPayLink::find()->where(['offer_id' => $item['id']])->select('pay_id')->column();
                $listName = \school\modules\sroutes\models\PayMethod::find()->where(['in', 'id', $list])->select('name')->column();

                return join(',', $listName);
            },
        ],
    ],
]) ?>