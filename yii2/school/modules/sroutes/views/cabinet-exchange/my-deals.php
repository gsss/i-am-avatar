<?php

use common\models\avatar\Currency;
use common\models\avatar\UserBill;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $shapeShiftAvailable array */
/* @var $avatarCurrency array  \common\models\avatar\Currency[] */
/* @var $ret array  пересечение */


$this->title = 'Мои сделки';


?>

<div class="container" style="padding-bottom: 70px;">
    <div class="col-lg-12">
        <h1 class="page-header text-center">
            <?= $this->title ?>
        </h1>
    </div>

    <div class="col-lg-12">

        <?php \yii\widgets\Pjax::begin(); ?>
        <?= $this->render('my-deals-table', ['ActiveDataProvider' => new \yii\data\ActiveDataProvider([
            'query'      => \school\modules\sroutes\models\Deal::find()
                ->innerJoin('offer', 'offer.id = deal.offer_id')
                ->select(['deal.*'])
                ->where([
                    'or',
                    ['deal.user_id' => Yii::$app->user->id],
                    ['offer.user_id' => Yii::$app->user->id],
                ])
            ,
            'sort' => ['defaultOrder' => ['created_at' => SORT_DESC]],
            'pagination' => [
                'pageSize' => 20,
            ],
        ])]) ?>
        <?php \yii\widgets\Pjax::end(); ?>

    </div>
</div>