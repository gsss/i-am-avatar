<?php

use common\models\avatar\Currency;
use common\models\avatar\UserBill;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $shapeShiftAvailable array */
/* @var $avatarCurrency array  \common\models\avatar\Currency[] */
/* @var $ret array  пересечение */


$this->title = 'Мои предложения';


?>

<div class="container" style="padding-bottom: 70px;">
    <div class="col-lg-12">
        <h1 class="page-header text-center">
            <?= $this->title ?>
        </h1>
    </div>

    <div class="col-lg-12">

        <p>
            <a href="offer-add" class="btn btn-default">Добавить</a>
        </p>
    </div>
    <div class="col-lg-12">
        <h2>Курс 1 RUB = 1 VVB</h2>

        <?php \yii\widgets\Pjax::begin(); ?>
        <?= $this->render('offer-table', ['ActiveDataProvider' => new \yii\data\ActiveDataProvider([
            'query'      => \school\modules\sroutes\models\Offer::find()
                ->where(['user_id' => Yii::$app->user->id])
            ,
            'sort' => ['defaultOrder' => ['price' => SORT_ASC]],
            'pagination' => [
                'pageSize' => 20,
            ],
        ])]) ?>
        <?php \yii\widgets\Pjax::end(); ?>

    </div>
</div>