<?php
/**
 * Created by PhpStorm.
 * User: Ra-m-ha
 * Date: 13.10.2019
 * Time: 21:15
 */

/** @var \yii\web\View $this */
/** @var \school\modules\sroutes\models\Deal $deal */
/** @var \school\modules\sroutes\models\Offer $offer */

?>
<?php
$this->registerJs(<<<JS

setInterval(function(ret1) {
    var list = [];
    $('.chat li').each(function(o, i) {
        list.push($(i).data('id'));
    }); 
    ajaxJson({
        url: '/cabinet-exchange-chat/get?id=' + {$deal->id},
        // data:{
        //     list: list.join(',')
        // },
        success: function(ret1) {
            var i;
            for(i = 0; i < ret1.list.length; i++) {
                // если я не нахожу в списке уже выведенных сообщений мой id (то есть id сообщения), то значит это новое сообщение и его надо добавить 
                if (list.indexOf(parseInt(ret1.list[i].id)) == -1) {
                    var ret = {message: ret1.list[i]};
                    
                    var user = ret.message.user;
                    var liObject = $('<li>', {class:'left clearfix', "data-id": ret.message.id});
                    var spanObject = $('<span>', {class:'chat-img pull-left'});
                    var imgObject = $('<img>', {src:user.avatar, class: 'img-circle', alt: user.name2, width: 50});
                    spanObject.append(imgObject);
                    liObject.append(spanObject);
                    var div1Object = $('<div>', {class: 'chat-body clearfix'});
                    var div2Object = $('<div>', {class: 'header'});
                    var strongObject = $('<strong>', {class: 'primary-font'}).html(user.name2);
                    var smallObject = $('<small>', {class: 'pull-right text-muted'});
                    var spanTimeObject = $('<span>', {class: 'glyphicon glyphicon-time'});
                    div2Object.append(strongObject);
                    smallObject.append(spanTimeObject);
                    smallObject.append(ret.message.timeFormatted);
                    strongObject.append(smallObject);
                    var pObject = $('<p>').html(ret.message.message);
                    div1Object.append(div2Object);
                    div1Object.append(pObject);
                    liObject.append(div1Object);
                    
                    $('.chat').append(liObject);
                    $('#btn-input').val('');
                }
            }
        }
    });
}, 5000);

JS
);
?>

<style>
    .chat
    {
        list-style: none;
        margin: 0;
        padding: 0;
    }

    .chat li
    {
        margin-bottom: 10px;
        padding-bottom: 5px;
        border-bottom: 1px dotted #B3A9A9;
    }

    .chat li.left .chat-body
    {
        margin-left: 60px;
    }

    .chat li.right .chat-body
    {
        margin-right: 60px;
    }


    .chat li .chat-body p
    {
        margin: 0;
        color: #777777;
    }

    .panel .slidedown .glyphicon, .chat .glyphicon
    {
        margin-right: 5px;
    }

    .panel-body
    {
        overflow-y: scroll;
        height: 250px;
    }

    ::-webkit-scrollbar-track
    {
        -webkit-box-shadow: inset 0 0 6px rgba(0,0,0,0.3);
        background-color: #F5F5F5;
    }

    ::-webkit-scrollbar
    {
        width: 12px;
        background-color: #F5F5F5;
    }

    ::-webkit-scrollbar-thumb
    {
        -webkit-box-shadow: inset 0 0 6px rgba(0,0,0,.3);
        background-color: #555;
    }

</style>


<div class="container">
    <div class="row">
        <div class="col-md-5">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <span class="glyphicon glyphicon-comment"></span> Чат
                </div>
                <div class="panel-body">
                    <ul class="chat">
                        <?php /** @var \school\modules\sroutes\models\ChatMessage $message */ ?>
                        <?php foreach (\school\modules\sroutes\models\ChatMessage::find()->where(['deal_id' => $deal->id])->all() as $message) { ?>
                            <li class="left clearfix" data-id="<?= $message->id ?>"><span class="chat-img pull-left">
                            <img width="50" src="<?= $message->getUser()->getAvatar() ?>" alt="<?= $message->getUser()->getName2() ?>" class="img-circle" />
                        </span>
                                <div class="chat-body clearfix">
                                    <div class="header">
                                        <strong class="primary-font"><?= $message->getUser()->getName2() ?></strong> <small class="pull-right text-muted">
                                            <span class="glyphicon glyphicon-time"></span><?= Yii::$app->formatter->asDatetime($message->created_at, 'php:d.m.Y H:i:s') ?></small>
                                    </div>
                                    <p>
                                        <?= $message->message ?>
                                    </p>
                                </div>
                            </li>
                        <?php } ?>
                    </ul>
                </div>
                <div class="panel-footer">
                    <div class="input-group">
                        <input id="btn-input" type="text" class="form-control input-sm" placeholder="Напиши свое сообщение здесь..." />
                        <span class="input-group-btn">
                            <?php
                            $this->registerJs(<<<JS

$('#btn-chat').click(function(e) {
    ajaxJson({
        url: '/cabinet-exchange-chat/send?id=' + {$deal->id},
        data: {
            message: $('#btn-input').val()
        },
        success: function(ret) {
            var user = ret.message.user;
            var liObject = $('<li>', {class:'left clearfix', "data-id": ret.message.id});
            var spanObject = $('<span>', {class:'chat-img pull-left'});
            var imgObject = $('<img>', {src:user.avatar, class: 'img-circle', alt: user.name2, width: 50});
            spanObject.append(imgObject);
            liObject.append(spanObject);
            var div1Object = $('<div>', {class: 'chat-body clearfix'});
            var div2Object = $('<div>', {class: 'header'});
            var strongObject = $('<strong>', {class: 'primary-font'}).html(user.name2);
            var smallObject = $('<small>', {class: 'pull-right text-muted'});
            var spanTimeObject = $('<span>', {class: 'glyphicon glyphicon-time'});
            div2Object.append(strongObject);
            smallObject.append(spanTimeObject);
            smallObject.append(ret.message.timeFormatted);
            strongObject.append(smallObject);
            var pObject = $('<p>').html(ret.message.message);
            div1Object.append(div2Object);
            div1Object.append(pObject);
            liObject.append(div1Object);
            
            $('.chat').append(liObject);
            $('#btn-input').val('');
        }
    });
});
JS
);
                            ?>
                            <button class="btn btn-warning btn-sm" id="btn-chat">
                                Отправить</button>
                        </span>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
