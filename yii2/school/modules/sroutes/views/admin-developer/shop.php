<?php

/** $this \yii\web\View  */

use yii\helpers\Url;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;

$this->title = 'Магазин';


?>

<div class="container">
    <div class="col-lg-12">
        <h1 class="page-header"><?= Html::encode($this->title) ?></h1>

        <h2 class="page-header">Заказ</h2>
        <p>При заказе отправляется код для того чтобы подписать договор оферты. Код сохраняется в переменной сессии в переменной <code>dogovor_code</code>.</p>

        <h2 class="page-header">Оферты</h2>
        <p>Для каждого товара указываться может своя оферта</p>
        <?= \common\services\documentation\DbTable::widget([
            'name'        => 'oferta',
            'description' => 'Оферта',
            'model'       => '\school\modules\sroutes\models\Oferta',
            'columns'     => [
                [
                    'name'        => 'id',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Идентификатор',
                ],
                [
                    'name'        => 'name',
                    'type'        => 'string',
                    'isRequired'  => true,
                    'description' => 'Название',
                ],
                [
                    'name'        => 'content',
                    'type'        => 'string',
                    'isRequired'  => true,
                    'description' => 'HTML код оферты',
                ],
                [
                    'name'        => 'created_at',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Момент создания',
                ],
            ],
        ]) ?>

        <?= \common\services\documentation\DbTable::widget([
            'name'        => 'product_ext_info',
            'description' => 'Дополнительная информация о товаре',
            'model'       => '\school\modules\sroutes\models\ProductExt',
            'columns'     => [
                [
                    'name'        => 'id',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Идентификатор',
                ],
                [
                    'name'        => 'product_id',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Идентификатор продукта',
                ],
                [
                    'name'        => 'oferta_id',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Идентификатор оферты',
                ],
                [
                    'name'        => 'is_send_vvb',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Будет начисляться баллы по факту покупки товара? 0 - нет, 1 - да, по умолчанию - 0',
                ],
            ],
        ]) ?>
        <p>Что делать если клиент заказал несколько товаров а у товаров разные оферты?</p>

        <h2 class="page-header">Каталог</h2>
        <p>один товар может быть только в одном разделе</p>

        <h2 class="page-header">Получение баллов по факту покупки товара</h2>
        <p>Есть флаг <code>is_send_vvb</code> в таблице <code>product_ext_info</code>.</p>
        <p>В инициализации моделя <code>\school\modules\sroutes\Module::bootstrap()</code> навешивается тобытие <code>Request::EVENT_AFTER_SHOP_SUCCESS</code> которое отправляет баллы клиенту если надо.</p>



        <hr style="margin-top: 200px;">
    </div>
</div>


