<?php

/** $this \yii\web\View  */

use yii\helpers\Url;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;

$this->title = 'Чат';


?>

<div class="container">
    <div class="col-lg-12">
        <h1 class="page-header"><?= Html::encode($this->title) ?></h1>

        <p>Репозиторий: <code>git@github.com:dram1008/s-routes-chat.git</code> <code>https://github.com/dram1008/s-routes-chat.git</code></p>
        <p>Путь на сервере где лежит проект: <code>/var/www/s-routes-chat/www</code></p>
        <p>В автозапуске прописано <code>@reboot /var/www/s-routes-chat/start.sh</code></p>
        <p>Файл содержит запуск ноды:</p>
        <pre>#!/bin/sh
cd /var/www/s-routes-chat/www/
node index > /var/www/s-routes-chat/log-node/node.log 2>&1</pre>


        <h2 class="page-header">Установка Чата</h2>

        <h3 class="page-header">Установка NodeJS</h3>

        <p><a href="https://www.digitalocean.com/community/tutorials/node-js-ubuntu-18-04-ru" target="_blank">https://www.digitalocean.com/community/tutorials/node-js-ubuntu-18-04-ru</a></p>

        <p>Устанавливаем библиотеку</p>
        <pre>npm install socket.io</pre>
        <p>Устанавливаем наш чат</p>
        <pre>cd /var/www</pre>
        <pre>git clone git@github.com:dram1008/s-routes-chat.git</pre>

        <h2 class="page-header">БД</h2>
        <?= \common\services\documentation\DbTable::widget([
            'name'        => 'chat_message',
            'description' => 'Чат',
            'model'       => '\common\models\exchange\ChatMessage',
            'columns'     => [
                [
                    'name'        => 'id',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Идентификатор',
                ],
                [
                    'name'        => 'deal_id',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Идентификатор сделки',
                ],
                [
                    'name'        => 'user_id',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Идентификатор пользователя',
                ],
                [
                    'name'        => 'message',
                    'type'        => 'text',
                    'isRequired'  => true,
                    'description' => 'сообщение, формат текстовый с переносом строк',
                ],
                [
                    'name'        => 'created_at',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Момент указания статуса',
                ],
            ],
        ]) ?>
        <p><code>/avatar-bank/views/cabinet-exchange/deal-action-chat.php</code></p>

        <h2 class="page-header">Чат</h2>
        <p><code>/cabinet-exchange-chat/send</code> - отправка<br>
            <code>/cabinet-exchange-chat/get</code> - получение чата</p>
        <hr style="margin-top: 200px;">
    </div>
</div>


