<?php

/** $this \yii\web\View  */

use yii\helpers\Url;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;

$this->title = 'Магазин';

$asset = Yii::$app->assetManager->getBundle('\school\modules\sroutes\assets\App\Asset');
$baseUrl = $asset->baseUrl;
?>

<div class="container">
    <div class="col-lg-12">
        <h1 class="page-header"><?= Html::encode($this->title) ?></h1>

        <h2 class="page-header">Нода</h2>
        <p>У нас есть нода.</p>

        <h2 class="page-header">Кабинет</h2>
        <p><code>/cabinet/packet</code></p>
        <p><img src="<?= $baseUrl ?>/controller/admin-developer/prizm/2019-10-29_13-04-05.png"></p>


        <hr style="margin-top: 200px;">
    </div>
</div>


