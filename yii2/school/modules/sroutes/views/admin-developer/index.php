<?php

/** $this \yii\web\View  */

use yii\helpers\Url;
use app\services\GsssHtml;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use app\services\GetArticle\Collection;

$this->title = 'Разработка';


?>

<div class="container">
    <div class="col-lg-12">
        <h1 class="page-header"><?= Html::encode($this->title) ?></h1>

        <p><a href="main">VIM SERVER</a></p>
        <p><a href="sync">Синхронизация</a></p>
        <p><a href="exchange">Обменник</a></p>
        <p><a href="chat">Чат</a></p>
        <p><a href="shop">Магазин</a></p>
        <p><a href="prizm">Prizm</a></p>

    </div>
</div>