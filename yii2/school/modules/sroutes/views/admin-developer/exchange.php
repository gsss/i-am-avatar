<?php

/** $this \yii\web\View  */

use yii\helpers\Url;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;

$this->title = 'Обменник';


?>

<div class="container">
    <div class="col-lg-12">
        <h1 class="page-header"><?= Html::encode($this->title) ?></h1>

        <p>Путь: <code>/exchange</code></p>
        <p>Дизайн и интерфейс на подобии <a href="https://localethereum.com/ru/" target="_blank">https://localethereum.com/ru/</a>
        </p>
        <h2 class="page-header">Сущности</h2>
        <p>Пользователь <code>user</code></p>
        <p>Предложение <code>offer</code> нет еще такого</p>
        <p>Способ оплаты <code>pay_method</code> нет еще такого</p>
        <p>Местоположение <code>place</code> нет еще такого</p>
        <p>Сделка <code>deal</code> нет еще такого</p>

        <?= \common\services\documentation\DbTable::widget([
            'name'        => 'offer',
            'description' => 'Предложение на продажу или покупку',
            'model'       => '\common\models\exchange\Offer',
            'columns'     => [
                [
                    'name'        => 'id',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Идентификатор',
                ],
                [
                    'name'        => 'user_id',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Идентификатор пользователя кто сделал предложение',
                ],
                [
                    'name'        => 'price',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Цена по которой предлагается купить продать VVB, указывается в атомах',
                ],
                [
                    'name'        => 'currency_id',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Валюта, указывает на db.currency',
                ],
                [
                    'name'        => 'pay_id',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Метод оплаты',
                ],
                [
                    'name'        => 'type_id',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Вид сделки 1 - купить, 2 - продать. Относится к пользователю который добавил предложение',
                ],
                [
                    'name'        => 'volume',
                    'type'        => 'string',
                    'isRequired'  => true,
                    'description' => 'Объем предложения от и до, формат JSON "[100, 10000]", цена указана в атомах в валюте которая указана в цене',
                ],
                [
                    'name'        => 'condition',
                    'type'        => 'string',
                    'isRequired'  => true,
                    'description' => 'Условия сделки, простой текст',
                ],
                [
                    'name'        => 'created_at',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Момент создания предложения',
                ],
            ],
        ]) ?>
        <p>на одно предложение может быть несколько сделок</p>

        <?= \common\services\documentation\DbTable::widget([
            'name'        => 'pay_method',
            'description' => 'Метод оплаты',
            'model'       => '\common\models\exchange\PayMethod',
            'columns'     => [
                [
                    'name'        => 'id',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Идентификатор',
                ],
                [
                    'name'        => 'name',
                    'type'        => 'string',
                    'isRequired'  => true,
                    'description' => 'Название',
                ],
            ],
        ]) ?>

        <?= \common\services\documentation\DbTable::widget([
            'name'        => 'deal',
            'description' => 'Сделка',
            'model'       => '\common\models\exchange\Deal',
            'columns'     => [
                [
                    'name'        => 'id',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Идентификатор',
                ],
                [
                    'name'        => 'user_id',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Идентификатор пользователя кто сделал предложение',
                ],
                [
                    'name'        => 'price',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Цена по которой предлагается купить продать 1 VVB, указывается в атомах',
                ],
                [
                    'name'        => 'currency_id',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Валюта для price, указывает на db.currency',
                ],
                [
                    'name'        => 'offer_id',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Идентификатор на предложение, offer.id',
                ],
                [
                    'name'        => 'volume',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Объем сделки в атомах currency_id',
                ],

                [
                    'name'        => 'created_at',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Момент создания предложения',
                ],
                [
                    'name'        => 'status',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Статус сделки',
                ],
            ],
        ]) ?>

        <h2 class="page-header">Жизненный цикл сделки</h2>
        <p>
            <a href="https://www.draw.io/#G1ra4nc0JUAZv1dTkDAr8CzXavQ0BM8CQT" target="_blank">
                <img src="/images/controller/admin-developer/exchange/life.png" width="100%">
            </a>
        </p>
        <p>Рассмотреть ситуацию если Клиент 1 открыл сделку, Клиент 2 подтвердит, Клиент 1 отправил как положено, Клиент 2 говорит что деньги не получил, а реально получил.</p>


        <h2 class="page-header">В чем задается объем?</h2>
        <p>в валюте в которой ставится цена или от того кол-ва которое есть у человека * на цену?</p>
        <p>Сейчас объемы задаются в валюте в которой выставлена цена.</p>
        <p>Потом возможно будет и от колва балов и будет пересчет по цене.</p>

        <h2 class="page-header">Дизайн</h2>
        <p><a href="https://www.prizmtorg.ru" target="_blank">https://www.prizmtorg.ru</a></p>

        <h2 class="page-header">официальный курс обмена</h2>
        <p>официальный курс обмена вот отсюда лучше брать <a href="https://coinmarketcap.com/currencies/prizm/" target="_blank">https://coinmarketcap.com/currencies/prizm/</a></p>

        <h2 class="page-header">Страница просмотра предложений</h2>
        <p>Ссылка: <code>/cabinet-exchange/index</code>.</p>
        <p>По клику идет переход на открытие сделки <code>/cabinet-exchange/deal-open</code>.</p>

        <h2 class="page-header">Страница "Добавить предложение"</h2>
        <p>Страница: <code>/cabinet-exchange/offer-add</code></p>
        <p>Модель: <code>\avatar\models\forms\exchange\OfferAdd</code></p>
        <p>Цена - курс по которому будет продаваться VVB. Валюта указывает соответственно валюту для этой цены.</p>
        <p>Объемы указываются в валюте продажи или покупки</p>
        <p>1. При выставлении предложения на продажу и покупку в какаой валюте оно может быть выставлено? Для начала рубль, потом $</p>

        <p><b>Валидация формы</b></p>
        <p>"Объем до" должен быть больше "объема до"</p>
        <p>При продаже проверяется а есть ли данное кол-во на счету.</p>

        <h2 class="page-header">Страница "Открыть сделку"</h2>
        <p>Страница: <code>/cabinet-exchange/deal-open</code></p>
        <p>Модель: <code>\avatar\models\forms\exchange\DealOpen</code></p>
        <p>Как уведомить ответчика сделки о начале сделки? По почте пока.</p>
        <p>Если предложение на продажу, то сделка получается на покупку и обратно.</p>
        <p><b>Валидация формы</b></p>
        <p>Если я продаю, то проверяется, а есть ли данное кол-во у меня на счету.</p>

        <h2 class="page-header">Страница "Сделка"</h2>
        <p>Страница: <code>/cabinet-exchange/deal-action</code></p>
        <p>На странице есть элемент <code>#chat</code> в котором есть аттрибут <code>data-chat</code> в котором в JSON прописаны поля:</p>
        <p><code>user1</code> - тот, кто принял предложение и открыл сделку. <code>deal.user_id</code></p>
        <p><code>user2</code> - тот, кто сделал предложение. <code>offer.user_id</code></p>
        <p><a href="https://yadi.sk/i/itmpeDxVrs0JWQ" target="_blank">https://yadi.sk/i/itmpeDxVrs0JWQ</a></p>

        <p><b>Ситуация: пользователь хочет купить VVB и принимает сделку на продажу VVB</b></p>
        <p><img src="/images/controller/admin-developer/exchange/situation1.png"></p>
        <p><code>offer.type_id = \common\models\exchange\Offer::TYPE_ID_SELL</code> (2)</p>
        <p>После открытия сделки, user2 отправляется уведомление чтобы он принял сделку. Когда он открывает сделку там будет кнопка "Принять сделку". Вызывается событие <code>Deal::EVENT_AFTER_OPEN = 'afterOpen'</code></p>
        <p>Вызывается AJAX <code>/cabinet-exchange/seller-open-accept</code> и в нем Сделка меняется статус на <code>STATUS_BLOCK</code> и вызывается событие <code>Deal::EVENT_AFTER_BLOCK = 'afterBlock'</code>. <code>user2</code> уведомляется, что сделка принята. У <code>user1</code> блокируются монеты.</p>

        <p>
            <a href="https://www.draw.io/#G1hvf2PpyRu-sSkdMltPcWaH-ccgi9foFT" target="_blank">
                <img src="/images/controller/admin-developer/exchange/life2.png" width="100%">
            </a>
        </p>

        <p>Вызывается AJAX <code>/cabinet-exchange/buyer-money-sended</code> и в нем Сделка меняется статус на <code>STATUS_MONEY_SENDED</code> и вызывается событие <code>Deal::EVENT_AFTER_MONEY_SENDED = 'afterMoneySended'</code>. <code>user1</code> уведомляется, что деньги отправлены.</p>
        <p>Вызывается AJAX <code>/cabinet-exchange/seller-money-received</code> и в нем Сделка меняется статус на <code>STATUS_MONEY_RECEIVED</code> и вызывается событие <code>Deal::EVENT_AFTER_MONEY_RECEIVED = 'afterMoneyReceived'</code>. <code>user1</code> уведомляется, что деньги отправлены.</p>
        <p>После того как деньги отправлены Клиенту 1 предлагается оценить сделку Клиента 2. На оценку вызывается <code>/cabinet-exchange/buyer-assessment-*</code> в котором  формируется оценка, вызывается событие <code>EVENT_AFTER_CLOSE</code> и ставится статус <code>STATUS_CLOSE</code>.</p>
        <p>В итоге закрытыми сделками считаются со статусами <code>STATUS_MONEY_RECEIVED</code> и <code>STATUS_CLOSE</code>.</p>



        <h3 class="page-header">Осображение заголовка</h3>
        <p>В сделке вывести заголовок сделки зеркально для покупателя и продавца</p>
        <p>дело в том что если предложение = Offer::TYPE_ID_BUY это продажа, то для того кто сделал предложение это Я покупаю а для того кто открыл сделку это Я продаю.</p>
        <p>Итого так выглядит условие:</p>
        <pre>&lt;h2&gt;Я &lt;?= (($offer->type_id == \common\models\exchange\Offer::TYPE_ID_SELL)? (($Deal->user_id == Yii::$app->user->id)? 'покупаю' : 'продаю') : (($Deal->user_id == Yii::$app->user->id)? 'продаю' : 'покупаю')) . '. ' . $user->getName2() ?&gt;&lt;/h2&gt;</pre>


        <h2 class="page-header">Блокировка средств</h2>
        <p>Как блокировать деньги у пользователя?</p>
        <p>Они будут переводиться на кошелек "Эскроу". Он один. На него все монеты от сделок и поступают. Всего две транзакции на сделку.</p>
        <ul>
            <li>1. блокировка средств. Коментарий: блокировка средств от сделки ### от пользователя ### с кошелька ###</li>
            <li>2. разблокировка средств. Коментарий: разблокировка средств от сделки ### в пользу пользователя ### в кошелек ###</li>
        </ul>
        <p>Идентификатор кошелька Эскоу = 285. Хранится в переменной <code>Yii::$app->params['walletEscrow']</code></p>

        <h2 class="page-header">Оценка сделки</h2>
        <?= \common\services\documentation\DbTable::widget([
            'name'        => 'assessment',
            'description' => 'Оценка сделки',
            'model'       => '\common\models\exchange\Assessment',
            'columns'     => [
                [
                    'name'        => 'id',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Идентификатор',
                ],
                [
                    'name'        => 'user_id',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Идентификатор пользователя который оценивается',
                ],
                [
                    'name'        => 'value',
                    'type'        => 'tinyint',
                    'isRequired'  => true,
                    'description' => 'Оценка, возможно -1,0,1',
                ],
                [
                    'name'        => 'deal_id',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Идентификатор сделки',
                ],
                [
                    'name'        => 'created_at',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Момент создания предложения',
                ],
            ],
        ]) ?>

        <h2 class="page-header">Арбитраж</h2>
        <p>Кнопка "Пригласить арбитра" видна и доступна обоим участникам сделки. Если кто то ее нажимает то арбитру посылается уведомление.</p>
        <pre>По арбитрам
Я так понимаю их может быть более чем один
и если кто то нажимает "Пригласить арбитра", то уведомления отсылаются всем сразу
и кто первый возмет сделку в разбор тот и решает спорный момент в сделке.
Верно?</pre>
        <p>Так как есть статус пригласить Арбитра и Арбитр принимает приглашение участвовать в разборе, то надо добавить еще один статус</p>
        <p>Надо сделать роль арбитра
            <code>role_arbitrator</code>
            <code>permission_arbitrator</code></p>
        <pre>на статус STATUS_AUDIT_WAIT
отсылается уведомление всем арбитрам

на статус STATUS_AUDIT_ACCEPTED
отсылается уведомление обоим участникам сделки

на статус STATUS_AUDIT_FINISH
отсылается уведомление обоим участникам сделки
        </pre>

        <p>Контроллер для арбитров: <code>cabinet-arbitrator</code><p>

        <p>По факту принятия сделки арбитром он записывается в ... Допустим сделаю таблицу arbitrator и там буду записывать статусы, когда отправлено приглашение</p>
        <pre>Записывать ли как меняются статусы сделки (отправлены деньги, получены), то есть время?
            для арбитра актуальная информация будет</pre>
        <p>По хорошему да, там как раз и статусы арбитража можно прописывать.</p>
        <p>А что еще нужно записывать? В чью пользу было принято решение в сделке.</p>
        <p>Какие коментарии в транзакции после принятия решения арбитра?</p>
        <ul>
            <li>1. разблокировка в пользу пользователя. Коментарий: Арбитр ### разблокировал средства от сделки ### в пользу пользователя ### в кошелек ###</li>
            <li>2. разблокировка в пользу пользователя. Коментарий: Арбитр ### разблокировал средства от сделки ### в пользу пользователя ### в кошелек ###</li>
        </ul>
        <p>Куда записывать какой арбитр взял сделку в разбор?</p>
        <p>Таблицу надо сделать. Кто взял, В чью пользу были отправлены деньги. Например arbitrator</p>

        <?= \common\services\documentation\DbTable::widget([
            'name'        => 'arbitrator',
            'description' => 'Рабзор сделки арбитром',
            'model'       => '\common\models\exchange\Arbitrator',
            'columns'     => [
                [
                    'name'        => 'id',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Идентификатор',
                ],
                [
                    'name'        => 'deal_id',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Идентификатор сделки',
                ],
                [
                    'name'        => 'arbitrator_id',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Идентификатор арбитра',
                ],
                [
                    'name'        => 'unblock_transaction_id',
                    'type'        => 'int',
                    'isRequired'  => false,
                    'description' => 'идентификатор транзакции разблокировки средств',
                ],
                [
                    'name'        => 'decision_user_id',
                    'type'        => 'int',
                    'isRequired'  => false,
                    'description' => 'Идентификатор пользовтеля в пользу которого были разблокированы монеты',
                ],
                [
                    'name'        => 'created_at',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Момент принятия арбитром сделки на разбор',
                ],
                [
                    'name'        => 'finish_at',
                    'type'        => 'int',
                    'isRequired'  => false,
                    'description' => 'Момент завершения арбитром сделки, то есть принятия статуса STATUS_AUDIT_FINISH',
                ],
            ],
        ]) ?>

        <p>Когда туда добавляется запись? После принятия сделки арбитром.</p>

        <h2 class="page-header">Покупка</h2>
        <p>
            <a href="https://www.draw.io/#G1hvf2PpyRu-sSkdMltPcWaH-ccgi9foFT" target="_blank">
                <img src="/images/controller/admin-developer/exchange/life3.png" width="100%">
            </a>
        </p>

        <h2 class="page-header">Статусы сделки</h2>
        <?= \common\services\documentation\DbTable::widget([
            'name'        => 'deal_status',
            'description' => 'Статусы сделки',
            'model'       => '\common\models\exchange\DealStatus',
            'columns'     => [
                [
                    'name'        => 'id',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Идентификатор',
                ],
                [
                    'name'        => 'deal_id',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Идентификатор сделки',
                ],
                [
                    'name'        => 'status',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Идентификатор статуса',
                ],
                [
                    'name'        => 'created_at',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Момент указания статуса',
                ],
            ],
        ]) ?>
        <p>Статусы показываются для арбитра.</p>

        <h2 class="page-header">Списание объема после завершения сделки</h2>
        <p>После того как сделка завершается объем в сделке уменьшается. Где это происходит? <code>/cabinet-exchange/sell-money-received</code>, <code>/cabinet-exchange/buy-money-received</code>, <code>/cabinet-arbitrator/finish1</code>, <code>/cabinet-arbitrator/finish2</code></p>

        <h2 class="page-header">Добавить счетчик сделки и автозакрытие по факту окончания времени</h2>
        <p>Что делать когда люди пропадают? Ставить статус что сделка закрыта но не завершилась. Например STATUS_HIDE.</p>
        <p>где будет устанавливаться? в консольном контроллере который будет вызываться раз в минуту
            <code>\console\controllers\ExchangeController::actionCloseDeals()</code></p>
        <p>куда девать заблоченные деньги? Ситуация
            когда сделка открыта, деньги заблочились и никто арбитра так и не вызвал,
            я сделку закрою автоматом, а монеты зависшие в блоке куда девать? Пока оставлю там где заблочены.</p>
        <p>CRON: <code>* * * * * /usr/bin/php /var/www/54.38.54.52/yii exchange/close-deals</code></p>
        <p>Резервный фонд-держатель невыясненых платежей - <code>walletLost</code></p>

        <h2 class="page-header">Страница "Мои предложения"</h2>
        <p>Колонка сделки указывает завершенные сделки по данному предложению. То есть статусы
            <code>Deal::STATUS_MONEY_RECEIVED</code>,
            <code>Deal::STATUS_CLOSE</code>,
            <code>Deal::STATUS_AUDIT_FINISH</code>.
        </p>

        <h2 class="page-header">Страница "Пользователь"</h2>
        <p>Ссылка на страницу: <code>/user/{id}</code>.</p>
        <p>Что отображается? Кол-во сделок закрытых и процент успешных.</p>

        <p><b>Кол-во сделок</b></p>
        <p>Сделки со статусами:
            <code>Deal::STATUS_MONEY_RECEIVED</code>,
            <code>Deal::STATUS_CLOSE</code>,
            <code>Deal::STATUS_AUDIT_FINISH</code>.
        </p>
        <p>Это те где я принимал учатие как тот кто открывает сделку и тот кто предлагал сделку</p>
        <p>Тот кто открывал сделку это <code>['user_id' => Yii::$app->user->id]</code></p>
        <p>тот кто предлагал сделку это нужно присоединить еще и offer и выбрать сделки которые к ним присоединены по <code>offer.user_id</code></p>
        <p>В итоге вот формула:</p>
        <pre>$count = count(Deal::find()
->innerJoin(['offer', 'offer.id = deal.offer_id'])
->where(['in', 'deal.status', [Deal::STATUS_MONEY_RECEIVED, Deal::STATUS_CLOSE, Deal::STATUS_AUDIT_FINISH]])
->andWhere([
    'or',
    ['offer.user_id' => Yii::$app->user->id],
    ['deal.user_id' => Yii::$app->user->id],
])
->select([
    'deal.*'
])
->asArray()
->all());</pre>

        <p><b>Кол-во сделок</b></p>
        <p>Здесь надо думать. Сами оценки в присоединненной таблице, значит надо ее присоединять и вычислять так. Ну можно попробовать</p>
        <p>Второй вариант: добавлять оценку сделки в саму сделку. а кто оценивается таким образом? оценивается тот кто предлагал сделку. То есть <code>offer.user_id</code></p>
        <p>То есть получаетя что я мог учасвовать в 10 сделках, но в одной я только предлагал, и там меня оценили а в других 9-ти я был инициатором сделки.</p>
        <p>Вот здесь и вопрос.</p>
        <p>Буду считать пока учтенные сделки, то есть оцененные</p>

        <p><b>Объемы сделок</b></p>
        <p>Хорошо бы еще посчитать все объемы сделок.</p>

        <h2 class="page-header">Чат</h2>
        <?= \common\services\documentation\DbTable::widget([
            'name'        => 'chat_message',
            'description' => 'Чат',
            'model'       => '\common\models\exchange\ChatMessage',
            'columns'     => [
                [
                    'name'        => 'id',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Идентификатор',
                ],
                [
                    'name'        => 'deal_id',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Идентификатор сделки',
                ],
                [
                    'name'        => 'user_id',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Идентификатор пользователя',
                ],
                [
                    'name'        => 'message',
                    'type'        => 'text',
                    'isRequired'  => true,
                    'description' => 'сообщение, формат текстовый с переносом строк',
                ],
                [
                    'name'        => 'created_at',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Момент указания статуса',
                ],
            ],
        ]) ?>
        <p><code>/avatar-bank/views/cabinet-exchange/deal-action-chat.php</code></p>

        <h2 class="page-header">Чат</h2>
        <p><code>/cabinet-exchange-chat/send</code> - отправка<br>
                <code>/cabinet-exchange-chat/get</code> - получение чата</p>

        <h2 class="page-header">Платежные системы</h2>
        <p><code>/avatar-bank/views/cabinet-exchange/deal-action-chat.php</code></p>
        <?= \common\services\documentation\DbTable::widget([
            'name'        => 'offer_pay_link',
            'description' => 'Ссыдка предложения и платежной системы',
            'model'       => '\common\models\exchange\OfferPayLink',
            'columns'     => [
                [
                    'name'        => 'id',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Идентификатор',
                ],
                [
                    'name'        => 'offer_id',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Идентификатор предложения',
                ],
                [
                    'name'        => 'pay_id',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Идентификатор платежной системы',
                ],
            ],
        ]) ?>
        <h2 class="page-header">Страница "Мои сделки"</h2>
        <p>Страница: <code>/cabinet-exchange/my-deals</code></p>
        <p>Здесь в выборку я включаю как мои сделки <code>deal.user_id</code> так и сделки в которых я участвую как предлагающий и со мной открыли сделку <code>offer.user_id</code>.</p>

        <hr style="margin-top: 200px;">
    </div>
</div>


