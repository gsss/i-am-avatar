<?php

/** $this \yii\web\View  */

use yii\helpers\Url;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;

$this->title = 'Синхронизация S-ROUTES и SALE.S-ROUTES.COM';


?>

<div class="container">
    <div class="col-lg-12">
        <h1 class="page-header"><?= Html::encode($this->title) ?></h1>

        <p>
           <a href="https://www.draw.io/#G1jt1h8m0fPTFvQ5JeZDckC5d8cOnn51RP" target="_blank">
               <img src="/images/controller/admin-developer/sync/sync.png" class="thumbnail">
           </a>
        </p>

        <h2 class="page-header">SALE.S-ROUTES.COM</h2>
        <h3 class="page-header">Прием данных</h3>
        <p>нет</p>


        <h3 class="page-header">Вызов функции синхнонизации</h3>
        <p>Вызывается в <code>\Controller\UserController::registerAction()</code></p>

        <h2 class="page-header">S-ROUTES</h2>
        <h3 class="page-header">Прием данных</h3>
        <p>Вызывается в <code>http://www.s-routes.com/api/auth/registration</code></p>
        <p>Если такой пользователь уже есть то выдается соответствующая ошибка</p>

        <h3 class="page-header">Вызов функции синхнонизации</h3>
        <p>Вызывается в <code>\avatar\controllers\AuthController::actionRegistrationActivate()</code></p>
        <p>Здесь пишется в две БД</p>

        <h2 class="page-header">Синхронизация баллов</h2>
        <p>Баллы хранятся в таблице <code>s_roues_wallet.wallet</code></p>

        <p>Делается вызов GET <code>https://migshoping.ru:3000/billing/getUser/:email</code></p>
        <p>Показатели нужно выводить следующие:<br>
                1) Баланс  кошелька призм<br>
            2) Ваш баланс в системе (то что он перевел на наш мастер кошелек и то что мы ему обязаны вернуть по требованию.<br>
            3) Баланс в общем пуле (сколько монет он направил в фонд "в рост") (на migshoping - inPull) - вычисляется через :3000/billing/getUser/Email<br>
            4) Парамайнинг (сколько монет натикадо ему в нашем фонде под ппоценты 0.7 в день) (на migshoping - Param) - вычисляется через location.protocol+"//"+location.hostname+":3000/getParaTable/Email <br>
            5) Баллы (результат парамайнинга конвертируется в баллы, внутренняя валюта системы и дальше тратится в магазине или на обменнике) (на migshopting - userPoints) вычисляется через :3000/billing/getUser/Email поле balance</p>

    </div>
</div>


