<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\modules\Shop\services\Basket;

/* @var $this \yii\web\View */
/* @var $content string */

\avatar\assets\App\Asset::register($this);
\avatar\assets\FontAwesome::register($this);
\avatar\assets\LayoutMenu\Asset::register($this);
\school\modules\sroutes\assets\Prizmology\Asset::register($this);
\school\modules\sroutes\assets\App\Asset::register($this);

$asset = Yii::$app->assetManager->getBundle('\school\modules\sroutes\assets\App\Asset');
$baseUrl = $asset->baseUrl;

?>
<?php $this->beginPage(); ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <script src="https://kit.fontawesome.com/b99e675b6e.js"></script>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->

    <title><?= $this->title ?></title>
    <?php $this->head() ?>
    <link rel="shortcut icon" href="<?= $baseUrl ?>/images/controller/site/index/logo2.png">

</head>

<body>
<?php $this->beginBody(); ?>

<?= $this->render('top-cabinet') ?>



<div style="flex-grow: 1;">
    <?= $content ?>
</div>

<?php if (\Yii::$app->user->isGuest) : ?>
    <?= $this->render('_modalLogin') ?>
<?php endif; ?>

<?= $this->render('_footer') ?>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
