<?php
/**
 * Created by PhpStorm.
 * User: Бог-Творец
 * Date: 01.11.2016
 * Time: 9:42
 */

/** @var $this \yii\web\View */


use yii\helpers\Html;
use yii\helpers\Url;

\avatar\assets\Language\Asset::register($this);

?>
<?php $this->beginPage(); ?>

<!doctype html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="shortcut icon" href="/images-rai/favicon.ico">
    <link rel="icon" type="image/png" href="/images-rai/favicon.ico" sizes="16x16">
    <!--<link rel="icon" type="image/png" href="/network/assets/images/favicon-32.png" sizes="32x32">-->
    <!--<link rel="icon" type="image/png" href="/network/assets/images/favicon-96.png" sizes="96x96">-->
    <link rel="apple-touch-icon" href="/network/assets/images/apple-touch-120.png">
    <link rel="stylesheet" href="/network/assets/js/slick/slick.css">
    <link rel="stylesheet" href="/network/assets/js/slick/slick-theme.css">
    <link rel="stylesheet" href="/network/assets/css/animate.css?ver=1.0.1">
    <link rel="stylesheet" href="/network/assets/css/style.css?ver=1.0.442">
<!--    <link rel="stylesheet" href="/network/assets/css/roadmap.css?ver=1.0.437">-->
    <title><?= $this->title ?></title>
    <?= Html::csrfMetaTags() ?>
    <?php $this->head() ?>
</head>

<body>
<?php $this->beginBody(); ?>

<?= $content ?>

<?= $this->render('_counters') ?>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>