<?php
/**
 * Created by PhpStorm.
 * User: Бог-Творец
 * Date: 11.11.2016
 * Time: 1:44
 */

/** @var $this \yii\web\View */

use yii\helpers\Html;
use yii\helpers\Url;

$asset = Yii::$app->assetManager->getBundle('\school\modules\sroutes\assets\App\Asset');
$baseUrl = $asset->baseUrl;


?>



<div class="tabs">
    <div class="tabs-header">
        <a href="/" class="logo"><img src="<?= $baseUrl ?>/images/controller/site/index/logo2.png" data-toggle="tooltip" data-placement="bottom"></a>

        <div class="burg"><span></span></div>
        <div class="menu_lk">
            <div class="menu">
                <div class="burg_open"><span></span></div>
                <ul>
                    <li><a href="/page/card"><div class="icon"><i class="far fa-credit-card"></i></div><div class="text">Карта</div></a></li>
                    <li><a href="/cabinet-exchange/index"><div class="icon"><i class="glyphicon glyphicon-random"></i></div><div class="text">Обменник</div></a></li>
                    <li><a href="/shop/index"><div class="icon"><i class="glyphicon glyphicon-globe"></i></div><div class="text">Магазин</div></a></li>
                    <li><a href="/page/activate"><div class="icon"><i class="fa fa-bullseye" aria-hidden="true"></i></div><div class="text">Активировать карту</div></a></li>
                    <li><a href="/page/news"><div class="icon"><i class="far fa-newspaper"></i></div><div class="text">Новости</div></a></li>
                    <li><a href="/page/blog"><div class="icon"><i class="far fa-handshake"></i></div><div class="text">Блог</div></a></li>
                    <li><a href="/page/statistic"><div class="icon"><i class="fas fa-file-alt"></i></div><div class="text">Статистика</div></a></li>
                    <li><a href="/page/about"><div class="icon"><i class="fas fa-user-alt"></i></div><div class="text">О нас</div></a></li>
                    <li><a href="/page/contact"><div class="icon"><i class="fas fa-address-card"></i></div><div class="text">Контакты</div></a></li>

                    <?php $c = \common\models\shop\Basket::getCount(); ?>

                    <li><a href="/shop/cart"><div class="icon"><i class="glyphicon glyphicon-shopping-cart"></i></div><div class="text">Корзина</div></a></li>
                    <span class="label label-success" id="basketCounter" style="
    margin-top: 10px;
    margin-bottom: 40px;
"><?= $c ?></span>
                </ul>
            </div>

            <div class="lk" id="basketPanel">

                <div class="img">
                    <?php if (Yii::$app->user->isGuest)  {?>
                        <img src="<?= $baseUrl ?>/images/cabinet/index/def_photo_polz.png">
                    <?php } else {?>
                        <img src="<?= Yii::$app->user->identity->getAvatar() ?>">
                    <?php }?>
                </div>
                <div class="icon">
                    <i class="fas fa-angle-down"></i>
                </div>
                <ul>
                    <li><a href="/cabinet/index">Мой кабинет</a></li>
                    <li><a href="/cabinet-wallet/index">Мои счета</a></li>
                    <li><a href="/cabinet/profile">Мой профиль</a></li>
                    <li><a href="/cabinet/ball">Мои баллы</a></li>
                    <li><a href="/cabinet-exchange/my-deals">Мои сделки</a></li>
                    <li><a href="/cabinet-exchange/offer">Мои предложения</a></li>
                    <li><a href="/cabinet-pif/index">ПИФ</a></li>
                    <?php if (Yii::$app->user->can('permission_sroutes_admin')) { ?>
                        <li><a href="/cabinet-school-shop-goods/index">Товары</a></li>
                        <li><a href="/cabinet-school-shop-requests/index">Заявки в магазин</a></li>
                        <li><a href="/admin-oferta/index">Оферта</a></li>
                        <li><a href="/admin-developer/index">Документация</a></li>

                    <?php } ?>

                    <li><a href="#">PZM</a></li>
                    <li><a href="<?= Url::to(['auth/logout']) ?>" data-method="post">Выйти</a></li>
                </ul>
            </div>
        </div>
    </div>
</div>

