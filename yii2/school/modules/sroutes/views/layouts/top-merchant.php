<?php
/**
 * Created by PhpStorm.
 * User: Бог-Творец
 * Date: 11.11.2016
 * Time: 1:44
 */

/** @var $this \yii\web\View */

use yii\helpers\Html;
use yii\helpers\Url;

$items1 = \common\models\Language::find()->all();

$this->registerJs(<<<JS
$('.buttonLanguageSet').click(function(e){
    function setCookie2 (name, value) {
        var date = new Date();
        var now = new Date();
        var time = now.getTime();
        var expireTime = time + (1000 * 60 * 60 * 24 * 365);
        now.setTime(expireTime);
        document.cookie = name + "=" + value +
        "; expires=" + now.toGMTString() +
        "; path=/";
    }
    setCookie2('language', $(this).data('code'));
    window.location.reload();
});
JS
);


function getItems2($items)
{
    $rows = [];
    foreach ($items as $k => $item) {
        $icon = Html::tag('i', Html::img($item->image, [
            'height' => 20,
            'class'  => 'img-circle',
            'style'  => 'border: 1px solid #888',
        ]), ['style' => 'margin-right: 15px;']);
        $a = Html::a($icon . $item['name'], 'javascript:void(0)', ['class' => 'buttonLanguageSet', 'data' => ['code' => $item->code]]);
        $li = Html::tag('li', $a);
        $rows[] = $li;
    }

    return join("\n", $rows);
}

/** @var \common\models\Language $language */
$language = \common\models\Language::findOne(['code' => Yii::$app->language]);
?>
<!-- Fixed navbar -->
<nav class="navbar navbar-default navbar-fixed-top">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar"
                    aria-expanded="false" aria-controls="navbar">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a
                class="navbar-brand"
                href="/"
                style="padding: 5px 10px 5px 10px;"
                >
                <img
                    src="/images/controller/site/index/logobig.png"
                    height="40"
                    width="40"
                    data-toggle="tooltip"
                    data-placement="bottom"
                    title="Банк Аватар"
                    >
            </a>
        </div>
        <div id="navbar" class="collapse navbar-collapse">
            <ul class="nav navbar-nav navbar-right">


                <li class="dropdown" id="userBlockLi">
                    <a
                            href="#"
                            class="dropdown-toggle"
                            data-toggle="dropdown"
                            aria-expanded="false"
                            role="button"
                            style="padding: 15px 10px 15px 10px;"
                    >
                        <?= Html::img($language->image, [
                            'height' => '20',
                            'class'  => 'img-circle',
                            'style'  => 'border: 1px solid #888',
                        ]) ?>
                        <span class="caret"></span>
                    </a>

                    <ul class="dropdown-menu" role="menu">
                        <?= getItems2($items1); ?>
                    </ul>
                </li>
            </ul>
        </div>

    </div>
</nav>
