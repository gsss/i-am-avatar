<?php
/**
 * Created by PhpStorm.
 * User: Бог-Творец
 * Date: 11.11.2016
 * Time: 1:44
 */

/** @var $this \yii\web\View */

use yii\helpers\Html;
use yii\helpers\Url;

$asset = Yii::$app->assetManager->getBundle('\school\modules\sroutes\assets\App\Asset');
$baseUrl = $asset->baseUrl;

?>


<div class="tabs">
    <div class="tabs-header">
        <a href="/" class="logo"><img src="http://s-routes.com/images/controller/site/index/logo2.png" data-toggle="tooltip" data-placement="bottom"></a>

        <div class="burg"><span></span></div>
        <div class="menu_lk">
            <div class="menu">
                <div class="burg_open"><span></span></div>
                <ul>
                    <li><a href="/page/card"><div class="icon"><i class="far fa-credit-card"></i></div><div class="text">Карта</div></a></li>
                    <li><a href="/exchange/index"><div class="icon"><i class="glyphicon glyphicon-random"></i></div><div class="text">Обменник</div></a></li>
                    <li><a href="/shop/index"><div class="icon"><i class="glyphicon glyphicon-globe"></i></div><div class="text">Магазин</div></a></li>
                    <li><a href="/page/activate"><div class="icon"><i class="fa fa-bullseye" aria-hidden="true"></i></div><div class="text">Активировать карту</div></a></li>
                    <li><a href="/page/news"><div class="icon"><i class="far fa-newspaper"></i></div><div class="text">Новости</div></a></li>
                    <li><a href="/page/blog"><div class="icon"><i class="far fa-handshake"></i></div><div class="text">Блог</div></a></li>
                    <li><a href="/page/statistic"><div class="icon"><i class="fas fa-file-alt"></i></div><div class="text">Статистика</div></a></li>
                    <li><a href="/page/about"><div class="icon"><i class="fas fa-user-alt"></i></div><div class="text">О нас</div></a></li>
                    <li><a href="/page/contact"><div class="icon"><i class="fas fa-address-card"></i></div><div class="text">Контакты</div></a></li>
                </ul>
            </div>

            <?php if (Yii::$app->user->isGuest)  {?>
                <div class="lk">
                    <div class="img">
                        <img src="<?= $baseUrl ?>/images/cabinet/index/def_photo_polz.png">
                    </div>
                    <div class="icon">
                        <i class="fas fa-angle-down"></i>
                    </div>
                    <ul>
                        <li><a href="/auth/login">Войти</a></li>
                        <li><a href="/auth/registration">Зарегистрироваться</a></li>
                        <li><a href="/auth/password-recover">Восстановить пароль</a></li>
                    </ul>
                </div>
            <?php } else {?>
                <div class="lk">
                    <div class="img">
                        <img src="<?= Yii::$app->user->identity->getAvatar() ?>">
                    </div>
                    <div class="icon">
                        <i class="fas fa-angle-down"></i>
                    </div>
                    <ul>
                        <li><a href="/cabinet/index">Мой кабинет</a></li>
                        <li><a href="/cabinet-bills/index">Мои счета</a></li>
                        <li><a href="/cabinet/profile">Мой профиль</a></li>
                        <li><a href="/cabinet/ball">Мои баллы</a></li>
                        <li><a href="/cabinet-exchange/my-deals">Мои сделки</a></li>
                        <li><a href="/cabinet-exchange/offer">Мои предложения</a></li>
                        <li><a href="/cabinet-pif/index">ПИФ</a></li>
                        <li><a href="#">PZM</a></li>
                        <li><a href="<?= Url::to(['auth/logout']) ?>" data-method="post">Выйти</a></li>
                    </ul>
                </div>
            <?php }?>

        </div>
    </div>
</div>

<div style="flex-grow: 1;">
