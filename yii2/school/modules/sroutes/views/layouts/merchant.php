<?php
/**
 * Created by PhpStorm.
 * User: Бог-Творец
 * Date: 01.11.2016
 * Time: 9:42
 */

/** @var $this \yii\web\View */

\avatar\assets\App\Asset::register($this);
\avatar\assets\LayoutMenu\Asset::register($this);
\avatar\assets\FontAwesome::register($this);

use yii\helpers\Html;
use yii\helpers\Url;

$this->registerJs(<<<JS
$('[data-toggle="tooltip"]').tooltip();
JS
);
?>
<?php $this->beginPage(); ?>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?= \Yii::t('c.FJivP9kpZN', 'Аватар Банк') ?></title>
    <link rel="shortcut icon" href="/images/favicon.ico">
    <?= Html::csrfMetaTags() ?>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody(); ?>
<?= $this->render('top-merchant') ?>

<?= $content ?>
<?php if (\Yii::$app->user->isGuest) : ?>
    <?= $this->render('_modalLogin') ?>
<?php endif; ?>

<footer class="footer">
    <div class="container">
        <div class="row" style="margin-top: 20px;">
            <div class="col-lg-4">
                <p>&copy; <span title="<?= \Yii::t('c.FJivP9kpZN', 'Децентрализованная Автономная Организация') ?>" data-toggle="tooltip">DAO</span> <?= \Yii::t('c.FJivP9kpZN', 'Аватар Банк') ?></p>
            </div>
            <div class="col-lg-4">

            </div>
            <div class="col-lg-4">

            </div>
        </div>

    </div>
</footer>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>