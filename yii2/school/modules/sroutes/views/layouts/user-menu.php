<?php
/**
 * Created by PhpStorm.
 * User: Бог-Творец
 * Date: 18.05.2016
 * Time: 7:45
 *
 */

use yii\helpers\Url;
use yii\helpers\Html;

/** @var $this \yii\web\View */
?>
<li class="dropdown" id="userBlockLi">
    <a
        href="#"
        class="dropdown-toggle"
        data-toggle="dropdown"
        aria-expanded="false"
        role="button"
        style="padding: 5px 10px 5px 10px;"
        >
        <?= Html::img(Yii::$app->user->identity->getAvatar(), [
            'height' => '40px',
            'class' => 'img-circle'
        ]) ?>
        <span class="caret"></span>
    </a>
    <ul class="dropdown-menu" role="menu">
        <li><a href="<?= Url::to(['cabinet/index']) ?>"><?= \Yii::t('c.qvYMPl9ABq', 'Мой кабинет') ?></a></li>

        <li class="divider"></li>
        <li><a href="<?= Url::to(['cabinet-bills/index']) ?>"><i class="glyphicon glyphicon-btc" style="padding-right: 5px;"></i><?= \Yii::t('c.qvYMPl9ABq', 'Мои счета') ?></a></li>
        <li><a href="<?= Url::to(['cabinet/profile']) ?>"><i class="glyphicon glyphicon-cog" style="padding-right: 5px;"></i><?= \Yii::t('c.qvYMPl9ABq', 'Мой профиль') ?></a></li>
        <li><a href="<?= Url::to(['cabinet/ball']) ?>"><i class="glyphicon glyphicon-cog" style="padding-right: 5px;"></i>Мои баллы</a></li>
        <li><a href="<?= Url::to(['cabinet-cards/index']) ?>"><i class="glyphicon glyphicon-credit-card" style="padding-right: 5px;"></i><?= Yii::t('c.D2Ye4F2K0u', 'Карты') ?></a></li>
        <li><a href="<?= Url::to(['cabinet-projects/index']) ?>"><i class="glyphicon glyphicon-list-alt" style="padding-right: 5px;"></i><?= \Yii::t('c.qvYMPl9ABq', 'Мои проекты') ?></a></li>
        <li><a href="<?= Url::to(['cabinet-ico/index']) ?>"><i class="glyphicon glyphicon-list-alt" style="padding-right: 5px;"></i>Мои заявки на токены</a></li>
        <li><a href="<?= Url::to(['cabinet-contract-list/index']) ?>"><i class="glyphicon glyphicon-list-alt" style="padding-right: 5px;"></i>Мои контракты</a></li>
        <li><a href="<?= Url::to(['cabinet-pif/index']) ?>"><i class="glyphicon glyphicon-list-alt" style="padding-right: 5px;"></i>ПИФ</a></li>

        <?php if (Yii::$app->user->can('permission_admin')) { ?>
            <li class="divider"></li>
            <li><a href="<?= Url::to(['admin-developer/index']) ?>"><i class="glyphicon glyphicon-btc" style="padding-right: 5px;"></i>Разработка</a></li>
            <li><a href="<?= Url::to(['admin/index']) ?>"><i class="glyphicon glyphicon-btc" style="padding-right: 5px;"></i>Админка</a></li>
            <li><a href="<?= Url::to(['admin-monitoring/index']) ?>"><i class="glyphicon glyphicon-btc" style="padding-right: 5px;"></i>Мониторинг</a></li>
            <li><a href="<?= Url::to(['admin-currency/index']) ?>"><i class="glyphicon glyphicon-btc" style="padding-right: 5px;"></i>Валюты</a></li>
            <li><a href="<?= Url::to(['admin-paysystem/index']) ?>"><i class="glyphicon glyphicon-btc" style="padding-right: 5px;"></i>Платежные системы</a></li>
            <li><a href="<?= Url::to(['admin-paysystem-config/index']) ?>"><i class="glyphicon glyphicon-btc" style="padding-right: 5px;"></i>Платежные настройки</a></li>
            <li><a href="<?= Url::to(['admin-users/index']) ?>"><i class="glyphicon glyphicon-user" style="padding-right: 5px;"></i>Пользователи</a></li>
        <?php } ?>
        <?php if (Yii::$app->user->can('permission_rai-akcioneri')) { ?>
            <li class="divider"></li>
            <li role="presentation" class="dropdown-header">РАЙ</li>
            <li><a href="<?= Url::to(['admin-akcioneri/index']) ?>"><i class="glyphicon glyphicon-btc" style="padding-right: 5px;"></i>Акционеры</a></li>
        <?php } ?>
        <?php if (Yii::$app->user->can('permission_languages')) { ?>
            <li class="divider"></li>
            <li role="presentation" class="dropdown-header">Языки</li>
            <li><a href="<?= Url::to(['languages/category']) ?>"><i class="glyphicon glyphicon-btc" style="padding-right: 5px;"></i>Категории</a></li>
        <?php } ?>
        <?php if (Yii::$app->user->can('permission_languages-admin')) { ?>
            <li><a href="<?= Url::to(['languages-admin/config']) ?>"><i class="glyphicon glyphicon-btc" style="padding-right: 5px;"></i>Config</a></li>
        <?php } ?>
        <?php if (Yii::$app->user->can('permission_developer')) { ?>
            <li><a href="<?= Url::to(['development/index']) ?>"><i class="glyphicon glyphicon-book" style="padding-right: 5px;"></i>Разработка</a></li>
        <?php } ?>
        <?php if (Yii::$app->user->can('permission_content')) { ?>
            <li class="divider"></li>
            <li role="presentation" class="dropdown-header">Контент</li>
            <li><a href="<?= Url::to(['admin-news/index']) ?>"><i class="glyphicon glyphicon-book" style="padding-right: 5px;"></i>Новости</a></li>
            <li><a href="<?= Url::to(['admin-blog/index']) ?>"><i class="glyphicon glyphicon-file" style="padding-right: 5px;"></i>Блог</a></li>
        <?php } ?>
        <?php if (Yii::$app->user->can('permission_arbitrator')) { ?>
            <li class="divider"></li>
            <li role="presentation" class="dropdown-header">Арбитраж</li>
            <li><a href="<?= Url::to(['cabinet-arbitrator/index']) ?>"><i class="glyphicon glyphicon-book" style="padding-right: 5px;"></i>Сделки</a></li>
        <?php } ?>
        <li class="divider"></li>

        <li><a href="<?= Url::to(['auth/logout']) ?>" data-method="post"><i class="glyphicon glyphicon-off" style="padding-right: 5px;"></i><?= \Yii::t('c.qvYMPl9ABq', 'Выйти') ?></a></li>
    </ul>
</li>