<?php

/** $this \yii\web\View  */

use common\models\avatar\UserBill;
use yii\helpers\Url;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;

$this->title = 'Кошелек';


?>

<div class="container">
    <div class="col-lg-12">
        <h1 class="page-header"><?= Html::encode($this->title) ?></h1>
    </div>

    <div class="col-lg-12">

        <?php
        $this->registerJS(<<<JS
$('.buttonAdd').click(function() {
    $('#modalAdd').modal();
});
JS
        );
        ?>
        <p>
            <button class="btn btn-default buttonAdd">Добавить</button>
        </p>


        <?php \yii\widgets\Pjax::begin(); ?>
        <?php
        $this->registerJS(<<<JS
$('[data-toggle="tooltip"]').tooltip();

$('.rowTable').click(function(e) {
    window.location = '/cabinet-wallet/item' + '?' + 'id' + '=' + $(this).data('id');
});
$('.buttonCard').click(function(e) {
    e.preventDefault();
    e.stopPropagation();
    window.location = '/cabinet-cards/card' + '?' + 'id' + '=' + $(this).data('id');
});

$('.buttonDelete').click(function (e) {
    e.preventDefault();
    e.stopPropagation();
    if (confirm('Подтвердите удаление')) {
        var button = $(this);
        var id = $(this).data('id');
        ajaxJson({
            url: '/cabinet-wallet/delete',
            data: {id: id},
            success: function (ret) {
                $('#modalInfo').on('hidden.bs.modal', function() {
                    button.parent().parent().remove();
                }).modal();
            },
            errorScript: function(ret) {
                if (ret.id == 2) {
                    $('.buttonDeleteAgree').attr('data-id', id);
                    
                    // Кошелек не пустой
                    $('#modalQuestion').modal();
                }
            }
        });
    }
});
JS
        );
        ?>
        <?= \yii\grid\GridView::widget([
            'dataProvider' => new \yii\data\ActiveDataProvider([
                'query'      => UserBill::find()
                    ->where(['user_id' => Yii::$app->user->id])
                    ->andWhere(['mark_deleted' => 0])
                ,
                'pagination' => [
                    'pageSize' => 20,
                ],
            ]),
            'summary' => '',
            'tableOptions' => [
                'class' => 'table table-striped table-hover',
            ],
            'rowOptions'   => function ($item) {
                $data = [
                    'data'  => ['id' => $item['id']],
                    'role'  => 'button',
                    'class' => 'rowTable',
                ];
                return $data;
            },
            'columns'      => [
                [
                    'header'    => 'WID',
                    'content'   => function (\common\models\avatar\UserBill $item) {
                        $currency = $item->currency;
                        if (!\common\models\avatar\CurrencyLink::find()->where(['currency_ext_id' => $currency])->exists()) {
                            return '';
                        }
                        $v = \yii\helpers\ArrayHelper::getValue($item, 'address');
                        if (is_null($v)) return '';
                        $w = new \common\models\piramida\Wallet(['id' => $v]);
                        $addressShort = $w->getAddressShort();

                        return Html::tag('code', $addressShort);
                    },
                ],
                [
                    'header'  => 'Картинка',
                    'content' => function ($item) {
                        $i = ArrayHelper::getValue($item, 'currency', '');
                        if ($i == '') return '';
                        $currency = \common\models\avatar\Currency::findOne($i);
                        if (is_null($currency)) return '';

                        return Html::img(\common\widgets\FileUpload7\FileUpload::getFile($currency->image, 'crop'), [
                            'class'  => "img-circle",
                            'width'  => 60,
                            'height' => 60,
                            'style'  => 'margin-bottom: 0px;',
                        ]);
                    },
                ],
                [
                    'header'  => 'Название',
                    'content' => function ($item) {
                        return $item['name'];
                    },
                ],
                [
                    'header'  => 'Валюта',
                    'content' => function ($item) {
                        $i = ArrayHelper::getValue($item, 'currency', '');
                        if ($i == '') return '';
                        $currency = \common\models\avatar\Currency::findOne($i);
                        if (is_null($currency)) return '';

                        return Html::tag('span', $currency->code, ['class' => 'label label-info']);
                    },
                ],
                [
                    'header'         => 'Монет',
                    'headerOptions'  => ['class' => 'text-right'],
                    'contentOptions' => ['class' => 'text-right'],
                    'content'        => function ($item) {
                        $i = ArrayHelper::getValue($item, 'currency', '');
                        if ($i == '') return '';
                        $currency = \common\models\avatar\Currency::findOne($i);
                        if (is_null($currency)) return '';

                        if (in_array($currency->id, \common\models\avatar\CurrencyLink::find()->select(['currency_ext_id'])->column())) {
                            $wallet = \common\models\piramida\Wallet::findOne($item['address']);
                            if (is_null($wallet)) return '';

                            return Yii::$app->formatter->asDecimal($wallet->getAmountWithDecimals(), 2);
                        }

                        return '';
                    },
                ],
                [
                    'header'  => 'Карта',
                    'content' => function ($item) {
                        $i = ArrayHelper::getValue($item, 'card_id', '');
                        if ($i == '') return '';
                        return Html::button('Карта', [
                            'class' => 'btn btn-primary buttonCard',
                            'data' => [
                                'id' => $item['card_id'],
                            ],
                        ]);
                    },
                ],
                [
                    'header'  => 'По умолчанию?',
                    'content' => function ($item) {
                        if ($item['is_default'] == 1) return Html::tag('span', 'Да', ['class' => 'label label-success']);
                        return Html::tag('span', 'Нет', ['class' => 'label label-default']);
                    },
                ],
                [
                    'header'  => 'Удалить',
                    'content' => function ($item) {
                        return Html::button('Удалить', [
                            'class' => 'btn btn-danger btn-xs buttonDelete',
                            'data'  => [
                                'id' => $item['id'],
                            ],
                        ]);
                    },
                ], ],
        ]) ?>
        <?php \yii\widgets\Pjax::end(); ?>
    </div>
</div>

<div class="modal fade" id="modalAdd" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Информация</h4>
            </div>
            <div class="modal-body">
                <form id="form_1">
                    <div class="form-group field-name">
                        <label class="control-label">Наименование</label>
                        <input type="text" class="form-control text-input" name="name" aria-required="true" aria-invalid="true">
                        <p class="help-block help-block-error" style="display: none;"></p>
                    </div>
                    <div class="form-group field-currency_id">
                        <label class="control-label">Валюта</label>
                        <select class="form-control" name="currency_id">
                            <option value="">- Ничего не выбрано -</option>
                            <?php $items = \common\models\avatar\Currency::find()
                                ->where(['id' => [9, 10]])
                                ->all() ?>
                            <?php foreach ($items as $c) { ?>
                                <option value="<?= $c->id ?>"><?= $c->title ?> (<?= $c->code ?>)</option>
                            <?php } ?>
                        </select>
                        <p class="help-block help-block-error" style="display: none;"></p>
                    </div>

                </form>
            </div>
            <div class="modal-footer">

                <?php
                $this->registerJS(<<<JS

var buttonAddExecute = function(e) {
    e.preventDefault();
    e.stopPropagation();
    var b = $(this);
    var bText = b.html();
    b.off('click');
    b.html($('<i>', {class: 'fa fa-spinner fa-spin fa-fw'}));
    b.attr('disabled', 'disabled');
    
    ajaxJson({
        url: '/cabinet-wallet/add',
        data: $('#form_1').serializeArray(),
        success: function (ret) {
            b.on('click', buttonAddExecute);
            b.html(bText);
            b.removeAttr('disabled');
            
            window.location.reload();
        },
        errorScript: function(ret) {
            switch (ret.id) {
                case 102:
                    var f = $('#form_1');
                    $.each(ret.data,function(i,v) {
                        var name = v.name;
                        var value = v.value;
                        var t = f.find('.field-' + name);
                        t.addClass('has-error');
                        t.find('p.help-block-error').html(value.join('<br>')).show();
                    });
                    
                    break;
            }
            
            b.on('click', buttonAddExecute);
            b.html(bText);
            b.removeAttr('disabled');
        }
    });
};

$('.buttonAddExecute').click(buttonAddExecute);
$('#form_1 .form-control').on('focus', function() {
    var o = $(this);
    var p = o.parent();
    if (p.hasClass('input-group')) {
        p = p.parent();
    }
    p.removeClass('has-error');
    p.find('p.help-block-error').hide();
});
JS
                );
                ?>
                <button type="button" class="btn btn-primary buttonAddExecute" >Добавить</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modalQuestion" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="myModalLabel">Информация</h4>
            </div>
            <div class="modal-body">
                Кошелек не пустой, подтвердите удаление
            </div>
            <div class="modal-footer">
                <?php
                $this->registerJS(<<<JS
$('.buttonDeleteAgree').click(function(e) {
    e.preventDefault();
    e.stopPropagation();
    ajaxJson({
        url: '/cabinet-wallet/delete-agree',
        data: {id: $(this).data('id')},
        success: function (ret) {
            window.location.reload();
        }
    });
});
JS
                );
                ?>
                <button type="button" class="btn btn-danger buttonDeleteAgree">Удалить</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modalInfo" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Информация</h4>
            </div>
            <div class="modal-body">
                Успешно!
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
            </div>
        </div>
    </div>
</div>