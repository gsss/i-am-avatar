<?php

/** @var $this \yii\web\View */
/** @var $bill \common\models\avatar\UserBill */

use common\models\avatar\UserBill;
use yii\helpers\Url;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;

$this->title = $bill->name;

\avatar\assets\Notify::register($this);
\avatar\assets\Clipboard::register($this);

?>
<?php
$this->registerJs(<<<JS

var clipboard = new Clipboard('.buttonCopy');
clipboard.on('success', function(e) {

    new Noty({
        timeout: 1000,
        theme: 'sunset',
        type: 'success',
        layout: 'bottomLeft',
        text: 'Скопировано'
    }).show();

});

JS
);
?>

<div class="container">
    <div class="col-lg-12">
        <h1 class="page-header text-center"><?= Html::encode($this->title) ?></h1>
    </div>

    <div class="col-lg-12">

        <?php
        /** @var  $currencyELXT \common\models\avatar\Currency */
        $currencyELXT = $bill->getCurrencyObject();

        $userWallet = $bill;
        $wallet = \common\models\piramida\Wallet::findOne($userWallet->address);
        $currency = $wallet->getCurrency();
        Yii::$app->session->set('currency.decimals', $currency->decimals);
        ?>

        <p class="text-center">
            <img src="<?= $currencyELXT->image ?>" width="300" style="margin-top: 30px;" class="img-circle">
        </p>
        <?php
        $address = $wallet->getAddress();
        $options = [
            'data'  => [
                'toggle'         => 'tooltip',
                'clipboard-text' => $address,
            ],
            'class' => 'buttonCopy',
            'title' => 'Адрес кошелька. Нажми чтобы скопировать',
        ];
        ?>
        <p class="text-center"><code <?= Html::renderTagAttributes($options) ?>><?= $address ?></code></p>
        <p class="text-center"><?= $currencyELXT->title ?></p>
        <h3 class="page-header text-center"><?= Yii::$app->formatter->asDecimal($wallet->getAmountWithDecimals(), $currency->decimals) ?></h3>
        <p class="text-center"><span class="label label-info"><?= $currencyELXT->code ?></span></p>
        <p><a href="<?= Url::to(['cabinet-wallet/send', 'id' => $bill->id]) ?>" class="btn btn-default">Отправить</a></p>
        <hr>
        <?php \yii\widgets\Pjax::begin(); ?>

        <?php
        $this->registerJs(<<<JS

$('[data-toggle="tooltip"]').tooltip();

JS
        );
        ?>
        <?= \yii\grid\GridView::widget([
            'dataProvider' => new \yii\data\ActiveDataProvider([
                'query'      => \common\models\piramida\Operation::find()
                    ->where(['wallet_id' => $userWallet->address])
                    ->orderBy(['datetime' => SORT_DESC])
                ,
                'pagination' => [
                    'pageSize' => 20,
                ],
            ]),
            'tableOptions' => [
                'class' => 'table table-striped table-hover',
            ],
            'rowOptions'   => function ($item) {
                $data = [
                    'data'  => ['id' => $item['id']],
                    'role'  => 'button',
                    'class' => 'rowTable',
                ];
                return $data;
            },
            'columns'      => [
                [
                    'header'        => 'OID',
                    'attribute'     => 'id',
                    'headerOptions' => [
                        'style' => Html::cssStyleFromArray([
                            'width' => '12%',
                        ]),
                    ],
                    'content'       => function (\common\models\piramida\Operation $item) {
                        $address = $item->getAddress();
                        $addressShort = $item->getAddressShort();

                        return Html::tag('code', $addressShort, [
                            'data'  => [
                                'toggle'         => 'tooltip',
                                'clipboard-text' => $address,
                            ],
                            'class' => 'buttonCopy',
                            'title' => 'Операция. Нажми чтобы скопировать',
                        ]);
                    },
                ],
                [
                    'header'        => 'TID',
                    'attribute'     => 'id',
                    'headerOptions' => [
                        'style' => Html::cssStyleFromArray([
                            'width' => '12%',
                        ]),
                    ],
                    'content'       => function (\common\models\piramida\Operation $item) {
                        $v = \yii\helpers\ArrayHelper::getValue($item, 'transaction_id');
                        if (is_null($v)) return '';
                        $transaction = new \common\models\piramida\Transaction(['id' => $v]);
                        $address = $transaction->getAddress();
                        $addressShort = $transaction->getAddressShort();

                        return Html::tag('code', $addressShort, [
                            'data'  => [
                                'toggle'         => 'tooltip',
                                'clipboard-text' => $address,
                            ],
                            'class' => 'buttonCopy',
                            'title' => 'Транзакция. Нажми чтобы скопировать',
                        ]);
                    },
                ],
                [
                    'header'        => 'Тип',
                    'attribute'     => 'type',
                    'headerOptions' => [
                        'style' => Html::cssStyleFromArray([
                            'width' => '5%',
                        ]),
                    ],
                    'content'       => function ($item) {
                        $v = \yii\helpers\ArrayHelper::getValue($item, 'type', 0);
                        if ($v == 0) return '';
                        if ($v == 2) return Html::tag('span', null, ['class' => 'glyphicon glyphicon-save', 'style' => 'color: #57b257']);
                        if ($v == 1) return Html::tag('span', null, ['class' => 'glyphicon glyphicon-import', 'style' => 'color: #57b257']);
                        if ($v == -1) return Html::tag('span', null, ['class' => 'glyphicon glyphicon-export', 'style' => 'color: #d54d49']);
                        if ($v == -2) return Html::tag('span', null, ['class' => 'glyphicon glyphicon-open', 'style' => 'color: #d54d49']);
                        return '';
                    },
                ],
                [
                    'header'        => 'Польз',
                    'headerOptions' => [
                        'style' => Html::cssStyleFromArray([
                            'width' => '5%',
                        ]),
                    ],
                    'content'       => function (\common\models\piramida\Operation $item) {
                        if (is_null($item->transaction_id)) return '';
                        $t = \common\models\piramida\Transaction::findOne($item->transaction_id);
                        if (is_null($t)) return '';
                        if ($item->type == \common\models\piramida\Operation::TYPE_IN) {
                            $wallet = \common\models\piramida\Wallet::findOne($t->from);
                        } else {
                            $wallet = \common\models\piramida\Wallet::findOne($t->to);
                        }
                        try {
                            $link = UserBill::findOne(['address' => $wallet->id]);
                        } catch (Exception $e) {
                            return '';
                        }
                        if (is_null($link)) return '';

                        try {
                            $user = \common\models\UserAvatar::findOne($link->user_id);
                        } catch (Exception $e) {
                            return '';
                        }
                        $avatar = $user->getAvatar(true);
                        return Html::img($avatar, ['class' => 'img-circle', 'width' => 30, 'data' => ['toggle' => 'tooltip'], 'title' => $user->getName2()]);
                    },
                ],
                [
                    'header'         => 'сумма',
                    'attribute'      => 'amount',
                    'headerOptions'  => [
                        'style' => Html::cssStyleFromArray([
                            'width'      => '10%',
                            'text-align' => 'right',
                        ]),
                    ],
                    'contentOptions' => [
                        'style' => Html::cssStyleFromArray([
                            'text-align' => 'right',
                        ]),
                    ],
                    'content'        => function ($item) {
                        $v = \yii\helpers\ArrayHelper::getValue($item, 'amount', 0);
                        if ($v == 0) return 0;
                        $v = $v / pow(10, Yii::$app->session->get('currency.decimals'));
                        $color = 1;
                        $prefix = '';
                        if (in_array($item['type'], [1, 2])) {
                            $prefix = '+';
                            $color = 'green';
                        }
                        if (in_array($item['type'], [-1, -2])) {
                            $prefix = '-';
                            $color = 'red';
                        }

                        return
                            Html::tag('span', $prefix . Yii::$app->formatter->asDecimal($v, 2), ['style' => 'color: ' . $color]);
                    },
                ],
                [
                    'header'        => 'Время',
                    'headerOptions' => [
                        'style' => Html::cssStyleFromArray([
                            'width' => '10%',
                        ]),
                    ],
                    'content'       => function ($item) {
                        $v = \yii\helpers\ArrayHelper::getValue($item, 'datetime', 0);
                        if ($v == 0) return '';
                        $v = (int)$v;

                        return Html::tag('abbr', \cs\services\DatePeriod::back($v, ['isShort' => true]), ['data' => ['toggle' => 'tooltip'], 'title' => Yii::$app->formatter->asDatetime($v)]);
                    },
                ],
                [
                    'header'        => 'Комментарий',
                    'attribute'     => 'comment',
                    'headerOptions' => [
                        'style' => Html::cssStyleFromArray([
                            'width' => '55%',
                        ]),
                    ],
                ],
            ],
        ]) ?>
        <?php \yii\widgets\Pjax::end(); ?>
    </div>
</div>
