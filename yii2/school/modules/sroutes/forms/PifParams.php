<?php

namespace school\modules\sroutes\forms;

use common\models\avatar\UserBill;
use common\models\SendLetter;
use common\models\UserSeed;
use common\services\Security;
use cs\Application;
use cs\services\Str;
use Yii;
use yii\base\Model;
use yii\helpers\ArrayHelper;
use yii\helpers\VarDumper;
use yii\web\UploadedFile;

/**
 */
class PifParams extends Model
{
    public $currency;
    public $amount;
    public $percent;
    public $period;
    public $sostav;

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            ['currency', 'required'],
            ['currency', 'integer'],

            ['amount', 'required'],
            ['amount', 'integer'],

            ['percent', 'required'],
            ['percent', 'integer'],

            ['period', 'required'],
            ['period', 'integer'],

            ['sostav', 'required'],
            ['sostav', 'integer'],
        ];
    }

    /**
     * @return array
     */
    public function action()
    {
        // Алгоритм конвертации

        // исполлнение контракта
        $contract = '0x42041249503A89950FcaAB2777A8E1C7E9FE78d2';
        $abi = '[{"constant":true,"inputs":[{"internalType":"uint256","name":"","type":"uint256"}],"name":"portfolioTokensFractions","outputs":[{"internalType":"uint256","name":"","type":"uint256"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":true,"inputs":[],"name":"VIMExchangeAddress","outputs":[{"internalType":"address","name":"","type":"address"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":true,"inputs":[],"name":"TTL","outputs":[{"internalType":"uint256","name":"","type":"uint256"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":true,"inputs":[{"internalType":"address","name":"","type":"address"}],"name":"expirations","outputs":[{"internalType":"uint256","name":"","type":"uint256"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":true,"inputs":[{"internalType":"uint256","name":"","type":"uint256"}],"name":"portfolioTokensExchanges","outputs":[{"internalType":"contractUniswapExchange","name":"","type":"address"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":false,"inputs":[{"internalType":"uint256[]","name":"_minTokensWanted","type":"uint256[]"},{"internalType":"uint256","name":"_deadline","type":"uint256"}],"name":"sellTokensVIM","outputs":[{"internalType":"uint256[]","name":"","type":"uint256[]"}],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":false,"inputs":[{"internalType":"uint256[]","name":"_minEthWanted","type":"uint256[]"},{"internalType":"uint256","name":"_deadline","type":"uint256"}],"name":"sellTokensETH","outputs":[{"internalType":"uint256[]","name":"","type":"uint256[]"}],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":false,"inputs":[{"internalType":"uint256","name":"_tokensAmountSold","type":"uint256"},{"internalType":"uint256","name":"_deadline","type":"uint256"},{"internalType":"uint256[]","name":"_wantedMinAmounts","type":"uint256[]"}],"name":"investWithVIM","outputs":[],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":true,"inputs":[{"internalType":"address","name":"","type":"address"},{"internalType":"uint256","name":"","type":"uint256"}],"name":"investorsTokens","outputs":[{"internalType":"uint256","name":"","type":"uint256"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":false,"inputs":[{"internalType":"uint256[]","name":"_minTokensWanted","type":"uint256[]"},{"internalType":"uint256","name":"_deadline","type":"uint256"}],"name":"sellTokensUSDC","outputs":[{"internalType":"uint256[]","name":"","type":"uint256[]"}],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":true,"inputs":[],"name":"USDCExchangeAddress","outputs":[{"internalType":"address","name":"","type":"address"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":true,"inputs":[{"internalType":"uint256","name":"_amount","type":"uint256"}],"name":"calculateTokensForGivenETHAmount","outputs":[{"internalType":"uint256[]","name":"","type":"uint256[]"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":false,"inputs":[{"internalType":"uint256","name":"_deadline","type":"uint256"},{"internalType":"uint256[]","name":"_wantedMinAmounts","type":"uint256[]"}],"name":"investWithETH","outputs":[],"payable":true,"stateMutability":"payable","type":"function"},{"constant":false,"inputs":[{"internalType":"uint256","name":"_tokensAmountSold","type":"uint256"},{"internalType":"uint256","name":"_deadline","type":"uint256"},{"internalType":"uint256[]","name":"_wantedMinAmounts","type":"uint256[]"}],"name":"investWithUSDC","outputs":[],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":true,"inputs":[{"internalType":"address","name":"_investor","type":"address"}],"name":"getPortfolio","outputs":[{"internalType":"uint256[]","name":"","type":"uint256[]"}],"payable":false,"stateMutability":"view","type":"function"},{"inputs":[{"internalType":"address[]","name":"_exchangeAddresses","type":"address[]"},{"internalType":"uint256[]","name":"_eachTokenShare","type":"uint256[]"},{"internalType":"uint256","name":"_TTL","type":"uint256"},{"internalType":"address","name":"_VIMExchangeAddress","type":"address"},{"internalType":"address","name":"_USDCExchangeAddress","type":"address"}],"payable":false,"stateMutability":"nonpayable","type":"constructor"}]';

        if (isset($this->params[$this->functionName])) {
            $params = array_values($this->params[$this->functionName]);
        } else {
            $params = [];
        }

        $userBill = UserBill::findOne(1);
        $walletETH = $userBill->getWalletETH();

        $currency = $this->currency;
        $amount = $this->amount;

        $txid = $walletETH->contract(
            $this->password,
            $contract,
            $abi,
            'investWithETH',
            ['deadline', ['wantedMinAmounts']]
        );

        return ['address' => $txid];
    }
}
