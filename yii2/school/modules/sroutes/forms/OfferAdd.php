<?php

namespace school\modules\sroutes\forms;

use common\models\avatar\Currency;
use common\models\avatar\UserBill;
use common\models\piramida\Wallet;
use school\modules\sroutes\models\Deal;
use school\modules\sroutes\models\Offer;
use school\modules\sroutes\models\OfferPayLink;
use cs\base\FormActiveRecord;
use cs\services\VarDumper;
use Yii;
use yii\base\Model;
use yii\base\NotSupportedException;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\helpers\Json;
use yii\web\IdentityInterface;

/**
 *
 * @property integer id
 * @property string foto
 */
class OfferAdd extends Model
{
    public $name;
    public $type_id;
    public $pay_id;

    /** @var  array */
    public $pay_list;

    public $currency_id;

    /** @var  Currency */
    public $currency;

    /** @var  int цена в монетах */
    public $price;

    public $condition;
    public $volume_start;
    public $volume_end;

    public function rules()
    {
        return [
            ['name', 'string', 'max' => 255],

            ['type_id', 'required'],
            ['type_id', 'integer'],
            ['type_id', 'compare', 'operator' => '!=', 'compareValue' => 0, 'message' => 'Это поле обязательно для заполнения'],

            ['pay_list', 'required'],

            ['price', 'required'],
            ['price', 'integer'],
            ['price', 'compare', 'operator' => '!=', 'compareValue' => 0, 'message' => 'Это поле обязательно для заполнения'],

            ['currency_id', 'required'],
            ['currency_id', 'integer'],
            ['currency_id', 'compare', 'operator' => '!=', 'compareValue' => 0, 'message' => 'Это поле обязательно для заполнения'],

            ['condition', 'string'],

            ['volume_start', 'required'],
            ['volume_start', 'integer'],

            ['volume_end', 'required'],
            ['volume_end', 'integer'],
            ['volume_end', 'validateEnd'],
            ['volume_end', 'validateWallet'], // Только для продажи. Проверяет достаточно ли монет у меня на счету
        ];
    }

    public function validateEnd($attribute, $params)
    {
        if (!$this->hasErrors()) {
            if ($this->volume_end < $this->volume_start) {
                $this->addError($attribute, 'Объем конечный должен быть больше объема начального');
                return;
            }

        }
    }

    public function attributeLabels()
    {
        return [
            'name'         => 'Заголовок',
            'type_id'      => 'Тип сделки (Я хочу ...)',
            'pay_id'       => 'Платежная система',
            'price'        => 'Цена',
            'currency_id'  => 'Валюта',
            'user_id'      => 'Пользователь',
            'condition'    => 'Условия сделки',
            'volume_start' => 'Объем от',
            'volume_end'   => 'Объем до',
        ];
    }

    public function validateWallet($attribute, $params)
    {
        if (!$this->hasErrors()) {
            if ($this->type_id == Offer::TYPE_ID_SELL) {
                // получаю кошелек VVB
                /** @var \common\models\avatar\UserBill $bill */
                $bill = UserBill::find()->where(['user_id' => Yii::$app->user->id, 'currency' => \common\models\avatar\Currency::VVB])->one();
                $wallet = Wallet::findOne($bill->address);

                $currencyVVB = \common\models\piramida\Currency::findOne(10);

                $currencyRUB = Currency::findOne(1);
                $currencyPZM = Currency::findOne(172);
                $currencyUSD = Currency::findOne(2);
                $currencyVVB = Currency::findOne(227);

                // RUB
                if ($this->currency_id == 1) {
                    $this->currency = $currencyRUB;

                    // перевожу баланс баллов в рубли
                    $amount_rub = ($wallet->amount / pow(10, $currencyVVB->decimals)) * $this->price;
                    if ($amount_rub < $this->volume_end) {
                        $this->addError($attribute, 'У вас недостаточно монет на счету');
                        return;
                    }
                }
                // USD
                if ($this->currency_id == 2) {
                    $this->currency = $currencyUSD;
                    // перевожу баланс баллов в рубли
                    $amount_rub = ($wallet->amount / pow(10, $currencyVVB->decimals)) * $this->price;
                    $amount_usd = $amount_rub / $currencyUSD->price_rub;
                    if ($amount_usd < $this->volume_end) {
                        $this->addError($attribute, 'У вас недостаточно монет на счету');
                        return;
                    }
                }
            } else {
                $currencyRUB = Currency::findOne(1);
                $currencyUSD = Currency::findOne(2);

                // RUB
                if ($this->currency_id == 1) {
                    $this->currency = $currencyRUB;
                }
                // USD
                if ($this->currency_id == 2) {
                    $this->currency = $currencyUSD;
                }
            }
        }
    }

    public function attributeHints()
    {
        return [
            'volume_start' => 'Объемы указываются в валюте продажи или покупки',
            'volume_end'   => 'Объемы указываются в валюте продажи или покупки',
        ];
    }

    /**
     * @return Offer
     */
    public function action()
    {
        $offer = new Offer();
        $offer->name = $this->name;
        $offer->user_id = Yii::$app->user->id;
        $offer->type_id = $this->type_id;
        $offer->price = $this->price * pow(10, $this->currency->decimals);
        $offer->currency_id = $this->currency_id;
        $offer->condition = $this->condition;
        $offer->volume = Json::encode([$this->volume_start * 100, $this->volume_end * 100]);
        $ret = $offer->save();
        $offer->trigger(Deal::EVENT_AFTER_OPEN);

        foreach ($this->pay_list as $item) {
            OfferPayLink::add(['offer_id' => $offer->id, 'pay_id' => $item]);
        }

        return $offer;
    }

}
