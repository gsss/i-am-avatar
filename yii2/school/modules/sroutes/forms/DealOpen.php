<?php

namespace school\modules\sroutes\forms;

use common\models\avatar\Currency;
use common\models\avatar\UserBill;
use school\modules\sroutes\models\Deal;
use school\modules\sroutes\models\DealStatus;
use school\modules\sroutes\models\Offer;
use cs\base\FormActiveRecord;
use cs\services\VarDumper;
use Yii;
use yii\base\Model;
use yii\base\NotSupportedException;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\helpers\Json;
use yii\web\IdentityInterface;

/**
 *
 */
class DealOpen extends Model
{
    public $volume;
    public $volume_vvb;

    public $message;

    /** @var  Offer */
    public $offer;

    public function rules()
    {
        return [
            ['volume', 'required'],
            ['volume', 'validateVolume'], // При открытии сделки нужно проверить что покупаемый объем больше чем указано в предложении

            ['volume_vvb', 'required'],

            ['message', 'string'],
        ];
    }

    public function attributeLabels()
    {
        $currency = Currency::findOne($this->offer->currency_id);

        return [
            'volume'     => 'Объем ' . $currency->code,
            'volume_vvb' => 'Объем VVB',
        ];
    }

    /**
     * При открытии сделки нужно проверить что покупаемый объем больше чем указано в предложении
     *
     * @param $attribute
     * @param $params
     */
    public function validateVolume($attribute, $params)
    {
        if (!$this->hasErrors()) {
            $offer = $this->offer;
            $dataVolume = Json::decode($offer->volume);
            $currency = Currency::findOne($offer->currency_id);
            $v = pow(10, $currency->decimals) * $this->volume;
            if ($dataVolume[0] > $v) {
                $this->addError($attribute, 'объем сделки должен превышать Минимального объем предложения');
                return;
            }
            if ($dataVolume[1] < $v) {
                $this->addError($attribute, 'объем сделки должен быть меньше Максимального объема предложения');
                return;
            }
        }
    }

    public function attributeHints()
    {
        return [];
    }

    /**
     * @return Deal
     */
    public function action()
    {
        $currencyVVB = Currency::findOne(Currency::VVB);
        $currency = Currency::findOne($this->offer->currency_id);

        $Deal = Deal::add([
            'user_id'     => Yii::$app->user->id,
            'price'       => $this->offer->price,
            'currency_id' => $this->offer->currency_id,
            'offer_id'    => $this->offer->id,
            'type_id'     => ($this->offer->type_id == Offer::TYPE_ID_SELL) ? Deal::TYPE_ID_BUY : Deal::TYPE_ID_SELL,
            'volume'      => (int)$this->volume * pow(10, $currency->decimals),
            'volume_vvb'  => (int)$this->volume_vvb * pow(10, $currencyVVB->decimals),
            'status'      => Deal::STATUS_OPEN,
        ]);

        DealStatus::add(['status' => Deal::STATUS_OPEN, 'deal_id' => $Deal->id]);

        $Deal->trigger(Deal::EVENT_AFTER_OPEN);

        return $Deal;
    }

}
