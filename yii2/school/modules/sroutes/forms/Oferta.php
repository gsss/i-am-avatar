<?php
namespace school\modules\sroutes\forms;

use common\models\school\File;
use cs\base\FormActiveRecord;
use cs\services\Url;
use cs\services\VarDumper;
use Yii;
use yii\base\NotSupportedException;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\web\IdentityInterface;

/**
 */
class Oferta extends FormActiveRecord
{


    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'oferta';
    }

    public static function getDb()
    {
        $module = \Yii::$app->getModule('sroutes');

        return $module->db;

    }

    public function formAttributes()
    {
        return [
            [
                'name',
                'Название',
                1,
                'string',
                ['max' => 255],
            ],
            [
                'content',
                'Полный текст',
                0,
                'string',
                'widget' => [
                    '\common\widgets\HtmlContent\HtmlContent',
                ]
            ],
        ];
    }
}
