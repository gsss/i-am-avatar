<?php

namespace school\modules\sroutes\forms;

use common\models\avatar\Currency;
use common\models\avatar\UserBill;
use common\models\BillingMain;
use common\models\PaySystemConfig;
use common\models\piramida\Wallet;
use common\models\shop\Basket;
use common\models\shop\Product;
use common\models\shop\Request;
use school\modules\sroutes\models\Deal;
use school\modules\sroutes\models\DealStatus;
use school\modules\sroutes\models\Offer;
use cs\base\FormActiveRecord;
use cs\services\VarDumper;
use school\modules\sroutes\models\ProductExt;
use Yii;
use yii\base\Model;
use yii\base\NotSupportedException;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\helpers\Json;
use yii\web\IdentityInterface;

/**
 *
 */
class Product1 extends Model
{
    public $id;
    public $name;
    public $content;
    public $price;
    public $currency_id;
    public $image;
    public $oferta_id;
    public $is_send_vvb;
    public $tree_node_id;

    public function rules()
    {
        return [
            ['id', 'integer'],

            ['name', 'required'],
            ['name', 'string'],

            ['image', 'string'],
            ['content', 'string'],
            ['price', 'integer'],

            ['currency_id', 'required'],
            ['currency_id', 'integer'],

            ['oferta_id', 'integer'],
            ['is_send_vvb', 'integer'],
            ['tree_node_id', 'integer'],
        ];
    }

    public static function findOne($id)
    {
        $i = new self();

        $p = Product::findOne($id);

        $i->id = $p->id;
        $i->name = $p->name;
        $i->image = $p->image;
        $i->content = $p->content;
        $i->currency_id = $p->currency_id;
        $i->tree_node_id = $p->tree_node_id;
        $c = Currency::findOne($p->currency_id);
        $i->price = $p->price / pow(10, $c->decimals);

        $pe = ProductExt::findOne(['product_id' => $id]);
        if (!is_null($pe)) {
            $i->oferta_id = $pe->oferta_id;
            $i->is_send_vvb = $pe->is_send_vvb;
        }

        return $i;
    }

    public function insert($id)
    {
        $c = Currency::findOne($this->currency_id);

        $p = Product::add([
            'school_id'   => $id,
            'name'        => $this->name,
            'image'       => $this->image,
            'content'     => $this->content,
            'price'       => $this->price * pow(10, $c->decimals),
            'currency_id' => $this->currency_id,
            'date_insert' => time(),
            'tree_node_id' => $this->tree_node_id,
        ]);
        $e = ProductExt::add([
            'is_send_vvb' => $this->is_send_vvb,
            'oferta_id'   => $this->oferta_id,
            'product_id'  => $p->id,
        ]);

        return $p;
    }

    public function attributeLabels()
    {
        return [
            'name'        => 'Наименование',
            'image'       => 'Изображение',
            'content'     => 'Описание',
            'price'       => 'Стоимость',
            'currency_id' => 'Валюта',
            'oferta_id'   => 'Оферта',
            'is_send_vvb' => 'Отправлять баллы после покупки?',
            'tree_node_id' => 'Раздел каталога',
        ];
    }

    public function update($id)
    {
        $p = Product::findOne($id);
        $c = Currency::findOne($this->currency_id);

        $p->name = $this->name;
        $p->image = $this->image;
        $p->content = $this->content;
        $p->price = $this->price * pow(10, $c->decimals);
        $p->currency_id = $this->currency_id;
        $p->tree_node_id = $this->tree_node_id;
        $p->save();

        $pe = ProductExt::findOne(['product_id' => $id]);
        if (!is_null($pe)) {
            $pe->oferta_id = $this->oferta_id;
            $pe->is_send_vvb = $this->is_send_vvb;
            $pe->save();
        } else {
            ProductExt::add([
                'product_id'  => $id,
                'oferta_id'   => $this->oferta_id,
                'is_send_vvb' => $this->is_send_vvb,
            ]);
        }

        return true;
    }
}
