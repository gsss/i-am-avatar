<?php

namespace school\modules\sroutes\forms;

use common\models\avatar\Currency;
use common\models\avatar\UserBill;
use common\models\BillingMain;
use common\models\PaySystemConfig;
use common\models\piramida\Wallet;
use common\models\shop\Request;
use school\modules\sroutes\models\Deal;
use school\modules\sroutes\models\DealStatus;
use school\modules\sroutes\models\Offer;
use cs\base\FormActiveRecord;
use cs\services\VarDumper;
use Yii;
use yii\base\Model;
use yii\base\NotSupportedException;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\helpers\Json;
use yii\web\IdentityInterface;

/**
 *
 */
class ValidateCode extends Model
{
    public $code;

    public function rules()
    {
        return [
            ['code', 'required'],
            ['code', 'string'],
            ['code', 'validateCode'],
        ];
    }

    public function validateCode($a, $p)
    {
        if (!$this->hasErrors()) {
            $inernalCode = Yii::$app->session->get('dogovor_code');
            $externalCode = $this->code;
            if ($inernalCode != $externalCode) {
                $this->addError($a, 'Не верный код');
                return;
            }
        }
    }


}
