<?php

namespace school\modules\sroutes\forms;

use common\models\avatar\Currency;
use common\models\avatar\UserBill;
use common\models\BillingMain;
use common\models\PaySystemConfig;
use common\models\piramida\Wallet;
use common\models\shop\Request;
use school\modules\sroutes\models\Deal;
use school\modules\sroutes\models\DealStatus;
use school\modules\sroutes\models\Offer;
use cs\base\FormActiveRecord;
use cs\services\VarDumper;
use Yii;
use yii\base\Model;
use yii\base\NotSupportedException;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\helpers\Json;
use yii\web\IdentityInterface;

/**
 *
 */
class PaySb extends Model
{
    public $school;
    public $billing_id;
    public $wallet_id;

    public function rules()
    {
        return [
            ['billing_id', 'required'],
            ['billing_id', 'integer'],

            ['wallet_id', 'required'],
            ['wallet_id', 'integer'],
        ];
    }

    /**
     *
     *
     * @return \common\models\Piramida\Transaction
     */
    public function action()
    {
        $walletSource = Wallet::findOne($this->wallet_id);

        $billing = BillingMain::findOne($this->billing_id);
        $config_id  = $billing->config_id;
        $config = PaySystemConfig::findOne($config_id);
        $data = Json::decode($config->config);
        $walletAddress = $data['wallet'];
        $wallet_id = (int) substr($walletAddress, 2);
        $walletDest = Wallet::findOne($wallet_id);
        $request = Request::findOne(['billing_id' => $billing->id]);

        $t = $walletSource->move($walletDest, $billing->sum_after, 'Оплата счета #'.$billing->id.' для заказа #'. $request->id);

        return $t;

    }

}
