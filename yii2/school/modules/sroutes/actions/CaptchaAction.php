<?php
namespace school\modules\sroutes\actions;

/**
 * Created by PhpStorm.
 * User: Ra-m-ha
 * Date: 30.10.2019
 * Time: 20:36
 */
class CaptchaAction extends \yii\captcha\CaptchaAction
{
    public function getSessionKey()
    {
        return '__captcha/site/captcha';
    }
}