<?php
/**
 * Created by PhpStorm.
 * User: Бог-Творец
 * Date: 08.12.2016
 * Time: 9:33
 */

namespace school\modules\sroutes\commands;

use school\modules\sroutes\models\Deal;
use common\models\piramida\Wallet;
use cs\services\VarDumper;
use Yii;

class ExchangeController extends \console\base\Controller
{


    public function actionCloseDeals()
    {
        // Если сделка открыта и прошло более 1 ч 30 мин с момента открытия
        $dealList = Deal::find()->where(['<', 'created_at', time() - 60 * 90])
            ->andWhere([
                'in', 'status', [
                    Deal::STATUS_OPEN,
                    Deal::STATUS_BLOCK,
                    Deal::STATUS_MONEY_SENDED,
                    Deal::STATUS_MONEY_RECEIVED,
                ],
            ])
            ->all();

        /** @var \school\modules\sroutes\models\Deal $deal */
        foreach ($dealList as $deal) {
            $deal->trigger(Deal::EVENT_BEFORE_HIDE);

            // Если деньги заблочены то перевести их в кошелек walletLost
            if (in_array($deal->status, [
                Deal::STATUS_BLOCK,
                Deal::STATUS_MONEY_SENDED,
            ])) {
                $walletEscrow = Wallet::findOne(Yii::$app->params['walletEscrow']);
                $walletLost = Wallet::findOne(Yii::$app->params['walletLost']);
                $t = $walletEscrow->move($walletLost, $deal->volume_vvb, 'Сделка ' . $deal->id . ' так и не завершилась. Монеты перешли из блока в резерв');
                echo('Сделка ' . $deal->id . ' так и не завершилась. Монеты перешли из блока в резерв. $t=' . $t->getAddress());
                echo("\n");
            }

            $deal->status = Deal::STATUS_HIDE;
            $deal->save();

            echo('Сделка ' . $deal->id . ' закрыта');
            echo("\n");

            $deal->trigger(Deal::EVENT_AFTER_HIDE);
        }
    }

}