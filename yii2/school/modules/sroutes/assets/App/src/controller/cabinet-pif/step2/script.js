var n = function(){
	var data = $(".news_one").attr("data-accordion");

	$(".btn_n").bind("click", function(){
		if(data == "close"){
			$(".news_text").slideUp();
			if($(this).hasClass("active")){
				$(this).toggleClass("active");
			}
			else{
				(".news_title").removeClass("active");
			}
		}
		else{
			$(this).toggleClass("active");
		}
		$(this).next(".news_text").not(":animated").slideToggle();
	})
}

n();




var speedCanvas = document.getElementById("speedChart");
Chart.defaults.global.defaultFontSize = 15;

var dataFirst = {
    label: "Команда А",
    data: [0, 70, 10, 75, 20, 65, 40],
    lineTension: 0.3,
    fill: true,
    borderColor: '#00effc',
    pointRadius: 0,
    backgroundColor: 'rgba(0, 239, 252, 0.3)',
    borderWidth: 0.5
  };

var dataSecond = {
    label: "Команда Б",
    data: [20, 15, 60, 60, 65, 30, 70],
    lineTension: 0.3,
    fill: true,
  	borderColor: '0, 252, 160',
  	pointRadius: 0,
  	pointRotation: 7,
  	backgroundColor: 'rgba(0, 252, 160, 0.3)',
  	borderWidth: 0.5
  };

var speedData = {
  labels: ["10 дней", "20 дней", "месяц", "0.5 года", "1 год", "2 года", "3 года"],
  datasets: [dataFirst, dataSecond]
};

var chartOptions = {
  legend: {
    display: true,
    position: 'top',
    labels: {
      boxWidth: 30,
      fontColor: '#1973fb',
      fontFamily: "'Exo 2', sans-serif"
    },
  },scales: {
		    yAxes: [{
		        ticks: {
		            display: true,
		            fontColor: '#1973fb',
		            fontSize: 12,
		            fontFamily: "'Exo 2', sans-serif"
		        },
		        gridLines: {
		            display: true,
		            drawBorder: true,
					color:"rgba(0, 239, 252, 0.2)",
		        	zeroLineColor: "rgba(0, 239, 252, 0)",
		        	zeroLineWidth: 1
		        },
		    }],
		    xAxes: [{
		        ticks: {
		            fontSize: 14,
		            fontColor: '#1973fb',
		            fontFamily: "'Exo 2', sans-serif",
		        },
		        gridLines: {
		            display: true,
		            drawBorder: true,
		            color:"rgba(0, 239, 252, 0.2)",
		        	zeroLineColor: "rgba(0, 239, 252, 0)",
		        	zeroLineWidth: 1
		        }
		    }],
		    onclick: function(){}
		}
};

var lineChart = new Chart(speedCanvas, {
  type: 'line',
  data: speedData,
  options: chartOptions
});








var oilCanvas = document.getElementById("oilChart");
Chart.defaults.global.defaultFontFamily = "'Exo 2', sans-serif";
Chart.defaults.global.defaultFontSize = 14;

var oilData = {
    labels: [
        "активы",
        "накопления",
        "страховой",
        "прибыль",
        "к выплате"
    ],
    datasets: [
        {
            data: [30, 10, 20, 15, 30],
            backgroundColor: [
                "#7cf77c",
                "#e9f77c",
                "#f7ce7c",
                "#42f5e6",
                "#dad1ff"
            ],
            borderColor: "white",
            borderWidth: 5,
            hoverBorderColor: "white"
        }],

};

var optionsPie = {
	animation: {
		duration: 500,
		animateScale: true,
		animateRotate: true
	},
	legend: {
	    display: true,
	    position: 'left',
	    labels: {
	      boxWidth: 15,
	      fontColor: '#1973fb',
	      fontFamily: "'Exo 2', sans-serif"
	    }
	}
}

var pieChart = new Chart(oilCanvas, {
  type: 'doughnut',
  data: oilData,
  options: optionsPie
});