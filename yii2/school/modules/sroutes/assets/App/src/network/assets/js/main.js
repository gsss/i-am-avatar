(function ($) {

    var decisionSlider = $('.decision-row'),
        teamSlider = $('.team-slider'),
        realizationSlider = $('.bank-realization__slider'),
        currencySlider = $('.currency__slider');

     window.sr =  ScrollReveal({easing: 'ease-in-out', scale:1, duration:800, distance:'40px'})
     sr.reveal('.reveal');

    $(window).scroll(function (){
        $.each($('.desicion-svg'), function () {
            if($(this).offset().top < $(window).scrollTop() + $(window).height()){
                $(this).find('path').animate({'stroke-dashoffset': 0},2000,function () {
                    $(this).removeClass('fill-none');
                    $(this).css('stroke', 'transparent');
                });
            }
        });
    });
    $('.lang-switcher').click(function (e) {
        e.preventDefault();
        $('.lang-choose-layer').addClass('active');
    });
    $('body').on('click', function (e) {
        if(!$(e.target).closest('.lang-choose-layer').length && !$(e.target).closest('.lang-switcher').length){
            $('.lang-choose-layer').removeClass('active');
        }
    });
     $('.menu-btn').click(function (e) {
         e.preventDefault();
         $('.lang-switcher, .menu-btn').toggleClass('hidden');
         toggleMenu();
     });
     $('.menu a.inner-link').click(function (e) {
         e.preventDefault();
         var to = $(this).attr('href');
         $('html, body').animate({
             scrollTop: $(to).offset().top
         }, 1500);
     });
     $('.menu-close').click(function (e) {
         e.preventDefault();
         $('.lang-switcher, .menu-btn').toggleClass('hidden');
         toggleMenu();
     });
    $('.card-slider').slick({
        slidesToShow: 1,
        dots:true,
        infinite: true,
        prevArrow: '<span class="avatar-slider-arrow--prev avatar-slider-arrow"></span>',
        nextArrow: '<span class="avatar-slider-arrow--next avatar-slider-arrow"></span>'
    });
    $('.roadmap-slider').slick({
        slidesToShow: 1,
        dots:false,
        infinite: true,
        prevArrow: '<span class="avatar-slider-arrow--prev avatar-slider-arrow"></span>',
        nextArrow: '<span class="avatar-slider-arrow--next avatar-slider-arrow"></span>'
    });
    $('.partners__slider').slick({
        slidesToShow: 4,
        dots:false,
        infinite: true,
        prevArrow: '<span class="avatar-slider-arrow--prev avatar-slider-arrow"></span>',
        nextArrow: '<span class="avatar-slider-arrow--next avatar-slider-arrow"></span>',
        responsive: [
            {
                breakpoint: 1200,
                settings: {
                    slidesToShow: 3
                }
            },
            {
                breakpoint: 768,
                settings: {
                    slidesToShow: 2
                }
            },
            {
                breakpoint: 512,
                settings: {
                    slidesToShow: 1
                }
            },
        ]
    });

    teamSlider.on('init', function () {
       // var slideIndex = teamSlider.find('.slick-slide:not(.slick-cloned)').length;
        setTimeout(function () {
            teamSlider.slick('slickRemove',7);
        },200);
    });

    if($(window).width() < 769){
        if(!teamSlider.hasClass('slick-initialized')){
            teamSliderInit();
        }
        if(!decisionSlider.hasClass('slick-initialized')){
            decisionSliderInit();
        }
        if(!realizationSlider.hasClass('slick-initialized')){
            realizationSliderInit();
        }
    }
    if($(window).width() < 993){
        if(!currencySlider.hasClass('slick-initialized')){
            currencySliderInit();
        }
    }
    $(window).resize(function () {
        if($(window).width() < 769){
            if(!teamSlider.hasClass('slick-initialized')){
                teamSliderInit();
            }
            if(!decisionSlider.hasClass('slick-initialized')){
                decisionSliderInit();
            }
            if(!realizationSlider.hasClass('slick-initialized')){
                realizationSliderInit();
            }
        }
        else{
            if(teamSlider.hasClass('slick-initialized')){
                teamSlider.slick('unslick');
                var lastSlide = '<div class="col-12 col-md-3 team popup-open" data-form="team">'+
                    '<figure class="team__image">'+
                    '<img src="./assets/images/team/team-add.png" alt="team-member">'+
                    '</figure>'+
                    '<p class="team__info">Станьте членом нашей команды</p>'+
                    '</div>';
                teamSlider.append(lastSlide);
            }
            if(decisionSlider.hasClass('slick-initialized')){
                decisionSlider.slick('unslick');
            }
            if(realizationSlider.hasClass('slick-initialized')){
                realizationSlider.slick('unslick');
            }
            if(currencySlider.hasClass('slick-initialized')){
                currencySlider.slick('unslick');
            }
        }

        if($(window).width() < 993){
            if(!currencySlider.hasClass('slick-initialized')){
                currencySliderInit();
            }
        }
        else if($(window).width() > 993){
            if(currencySlider.hasClass('slick-initialized')){
                currencySlider.slick('unslick');
            }
        }
    });


    $('.avatar__overlay').click(function () {
        closePopup();
    });

    $('body').on('click','.popup-open', function (e) {
        e.preventDefault();

        var form = $(this).data('form'),
            posYTop = $(this).offset().top,
            posX = $(this).offset().left,
            bodyH = $('body').height();
        eW = $(this).width();
        $(this).addClass('active');
        $('.avatar__overlay').addClass('active');
        $('.currency__popup')
            .addClass('active')
            .css('bottom', bodyH - posYTop + 35)
            .css('left', posX + eW/2);
        $('.currency__popup-inner').load('/landing/form?id=' + form);
    });

    $('body').on('click', '.popup-open-mail', function (e) {
        e.preventDefault();

        var t = $(this);
        var form = t.data('form'),
            posYTop = t.offset().top,
            posX = t.offset().left,
            bodyH = $('body').height();
        var eW = t.width();


        $.ajax({
            url: '/landing/mail',
            data: $('form.contacts__form').serializeArray(),
            dataType: 'json',
            type: 'post',
            success: function (ret) {

                t.addClass('active');
                $('.avatar__overlay').addClass('active');
                $('.currency__popup')
                    .addClass('active')
                    .css('bottom', bodyH - posYTop + 35)
                    .css('left', posX + eW/2);

                if (ret.success) {
                    $('form.contacts__form input[name="name"]').val('');
                    $('form.contacts__form input[name="email"]').val('');
                    $('form.contacts__form textarea').val('');
                    $('.currency__popup-inner').load('/landing/form?id=' + form);
                } else {
                    $('.currency__popup-inner').html(ret.data.data[0].value[0]);
                }
            },
            error: function (ret) {

            }
        });


    });

    function teamSliderInit() {
        teamSlider.slick({
            slidesToShow: 1,
            dots:false,
            infinite: true,
            prevArrow: '<span class="avatar-slider-arrow--prev avatar-slider-arrow"></span>',
            nextArrow: '<span class="avatar-slider-arrow--next avatar-slider-arrow"></span>'
        });
    }
    function decisionSliderInit() {
        decisionSlider.slick({
            slidesToShow: 1,
            dots:false,
            infinite: true,
            prevArrow: '<span class="avatar-slider-arrow--prev avatar-slider-arrow"></span>',
            nextArrow: '<span class="avatar-slider-arrow--next avatar-slider-arrow"></span>'
        });
    }
    function realizationSliderInit() {
        realizationSlider.slick({
            slidesToShow: 1,
            dots:true,
            infinite: true,
            arrows: false
        });
    }
    function currencySliderInit() {
        currencySlider.slick({
            slidesToShow: 8,
            dots:false,
            infinite: true,
            prevArrow: '<span class="avatar-slider-arrow--prev avatar-slider-arrow"></span>',
            nextArrow: '<span class="avatar-slider-arrow--next avatar-slider-arrow"></span>',
            responsive: [

                {
                    breakpoint: 768,
                    settings: {
                        slidesToShow: 6
                    }
                },
                {
                    breakpoint: 512,
                    settings: {
                        slidesToShow: 2
                    }
                }
            ]
        });
    }
    function closePopup() {
        $('.currency__popup, .avatar__overlay, .currency, .popup-open').removeClass('active');
    }
    function toggleMenu() {
        $('.menu-container').toggleClass('active');
        $('.menu-container').css('visibility','visible');
    }


})(jQuery);
