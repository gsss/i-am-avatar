<?php


namespace school\modules\sroutes\assets\Prizmology;

use yii\web\AssetBundle;

/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since  2.0
 */
class Asset extends AssetBundle
{
    public $sourcePath = '@school/modules/sroutes/assets/Prizmology/src';
    public $css      = [
        'css/index1.css'
    ];
    public $js       = [
        'js/index.js',
    ];
    public $depends  = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapThemeAsset',
        'yii\bootstrap\BootstrapPluginAsset',
    ];
}
