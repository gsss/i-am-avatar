<?php
/**
 * Created by PhpStorm.
 * User: Ra-m-ha
 * Date: 23.10.2019
 * Time: 17:45
 */

namespace school\modules\zdravo;

use cs\services\VarDumper;
use yii\base\BootstrapInterface;
use Yii;
use yii\base\Application;

class Module extends \yii\base\Module implements BootstrapInterface
{

//    public $defaultRoute = 'site/index';

    public function init()
    {
        parent::init();

        // инициализация модуля с помощью конфигурации, загруженной из config.php
        \Yii::configure($this, require __DIR__ . '/config.php');

        if (Yii::$app instanceof \yii\console\Application) {
            $this->controllerNamespace = 'school\modules\zdravo\commands';
        } else {
            $this->controllerNamespace = 'school\modules\zdravo\controllers';
        }
    }

    /**
     * @param Application $app
     */
    public function bootstrap($app)
    {
        $app->urlManager->addRules([
            '/'                                                        => 'page/root',
            'event'                                                    => 'event/item',
            'event/<action>'                                           => 'event/<action>',
            'auth/<action>'                                            => 'auth/<action>',
            'site/<action>'                                            => 'site/<action>',
            'cabinet/<action>'                                         => 'cabinet/<action>',
            'cabinet-wallet/<action>'                                  => 'cabinet-wallet/<action>',
            'cabinet-task-list/<action>'                               => 'cabinet-task-list/<action>',
            'koop/registration'                                        => 'koop/registration',

            // Блог
            'blog'                                                     => 'blog/index',
            'blog/<year:\\d{4}>/<month:\\d{2}>/<day:\\d{2}>/<id:\\w+>' => 'blog/item',
            'blog/<action>'                                            => 'blog/<action>',

            // Новости
            'news'                                                     => 'news/index',
            'news/<year:\\d{4}>/<month:\\d{2}>/<day:\\d{2}>/<id:\\w+>' => 'news/item',
            'news/<action>'                                            => 'news/<action>',

            // Комментарии
            'comments/<action>'                                        => 'comments/<action>',

            // Магазин
            'shop/<action>'                                            => 'shop/<action>',
            'cabinet-billing/<action>'                                 => 'cabinet-billing/<action>',

            // событие
            'event-room/<action>'                                      => 'event-room/<action>',

            // вывод реферальных начислений
            'cabinet-referal-request/<action>'                         => 'cabinet-referal-request/<action>',

            'pages/<action>' => 'pages/<action>',
            'test/<action>'  => 'test/<action>',
            'kurs/<action>'  => 'kurs/<action>',

            '<controller>'          => 'page/controller',
            '<controller>/<action>' => 'page/controller-action',
        ]);
    }

}