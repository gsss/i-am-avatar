<?php
/**
 * Created by PhpStorm.
 * User: s.arhangelskiy
 * Date: 12.04.2016
 * Time: 16:13
 */

namespace school\modules\avr3\models\form;

use common\models\BillingMain;
use common\models\UserAvatar;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;
use yii\helpers\VarDumper;

/**
 * @property int    id                      Идентификатор записи
 * @property string short
 * @property string link
 * @property int    created_at
 */
class Link extends ActiveRecord
{
    public function rules()
    {
        return [
            ['link', 'required'],
            ['link', 'string'],
            ['link', 'url', 'enableIDN' => true, 'validSchemes' => ['http', 'https', 'tg'], 'message' => 'Доступные протоколы в ссылке http, https, tg'],
            ['short', 'string'],
            ['created_at', 'integer'],
        ];
    }

    public function behaviors()
    {
        return [
            [
                'class'      => '\yii\behaviors\TimestampBehavior',
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at'],
                ],
                // если вместо метки времени UNIX используется datetime:
                // 'value' => new Expression('NOW()'),
            ],
        ];
    }

    /**
     * @param $fields
     *
     * @return Link
     */
    public static function add($fields)
    {
        $i = new self($fields);
        $i->save();
        $i->id = $i::getDb()->lastInsertID;

        return $i;
    }

    public static function getDb()
    {
        $module = \Yii::$app->getModule('avr3');

        return $module->db;
    }
}