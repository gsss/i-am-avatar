<?php

namespace school\modules\avr3\controllers;

use common\services\Security;
use Yii;
use yii\helpers\Url;

class SiteController extends \avatar\base\BaseController
{
    public function actions()
    {
        return [
            'error'   => [
                'class' => 'yii\web\ErrorAction',
                'view'  => '@school/views/site/error.php',
            ],
        ];
    }

    public function actionIndex()
    {
        $model = new \school\modules\avr3\models\form\Link();

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $model->short = Security::generateRandomString(6, Security::ALGORITM_BASE58);
            $model->save();
            Yii::$app->session->setFlash('form', Url::to('/'.$model->short, true));
        }
        return $this->render([
            'model' => $model,
        ]);
    }
}
