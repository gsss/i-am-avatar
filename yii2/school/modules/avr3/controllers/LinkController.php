<?php

namespace school\modules\avr3\controllers;

use cs\web\Exception;
use school\modules\avr3\models\Link;

class LinkController extends \avatar\base\BaseController
{
    public function actions()
    {
        return [
            'error'   => [
                'class' => 'yii\web\ErrorAction',
                'view'  => '@school/views/site/error.php',
            ],
        ];
    }

    public function actionIndex($id)
    {
        $link = Link::findOne(['short' => $id]);
        if (is_null($link)) {
            throw new Exception('Нет ссылки');
        }

        return $this->redirect($link->link);
    }
}
