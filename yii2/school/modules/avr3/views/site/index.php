<?php
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
$this->title = 'Сервис коротких ссылок ЯАватар';
?>

<div class="container" style="padding-bottom: 70px;">
    <div class="col-lg-12">
        <h1 class="page-header text-center">
            <?= $this->title ?>
        </h1>
    </div>
    <div class="col-lg-8 col-lg-offset-2">
        <?php if (Yii::$app->session->hasFlash('form')): ?>

        <?php
            $link = Yii::$app->session->getFlash('form');
            \avatar\assets\Notify::register($this);
            \avatar\assets\Clipboard::register($this);

            ?>
            <?php
            $this->registerJs(<<<JS

var clipboard = new Clipboard('.buttonCopy');
clipboard.on('success', function(e) {

    new Noty({
        timeout: 1000,
        theme: 'relax',
        type: 'success',
        layout: 'bottomLeft',
        text: 'Скопировано'
    }).show();

});

$('.buttonCopy').click();

JS
            );
            ?>
            <p class="alert alert-success">
                Успешно добавлено.
            </p>
            <pre><?= $link ?></pre>
            <p>
                <button
                        class="btn btn-success buttonCopy"
                        title="Скопировать"
                        data-toggle="tooltip"
                        data-clipboard-text="<?= $link ?>">Скопировать</button>
                <a href="/" class="btn btn-default">Назад </a>
            </p>

        <?php else: ?>

            <?php $form = \yii\bootstrap\ActiveForm::begin([
                'id'      => 'contact-form',
                'options' => ['enctype' => 'multipart/form-data'],
            ]); ?>

            <?= $form->field($model, 'link', ['inputOptions' => ['placeholder' => 'Введите ссылку ...']])->label('Ссылка') ?>

            <hr class="featurette-divider">

            <div class="form-group">
                <?= Html::submitButton('Получить короткую ссылку', [
                    'class' => 'btn btn-success',
                    'name'  => 'contact-button',
                    'style' => 'width: 100%;',
                ]) ?>
            </div>
            <?php \yii\bootstrap\ActiveForm::end(); ?>

            <p class="lead" style="margin-top: 100px;">Этот сервис предназначен для преобразования ссылки в короткую.</p>
            <p class="lead">Короткая ссылка формируется по стандарту <a href="https://ru.wikipedia.org/wiki/Base58" target="_blank">BASE58</a>, который исключает неоднозначное его понимание при разборе пользователем с экрана или с бумаги.</p>
            <p class="lead">Есть поддержка интернационализованных доменных имен (<a href="https://ru.wikipedia.org/wiki/IDN" target="_blank">IDN</a>).</p>

            <?= $this->render('../blocks/share', [
                'image'       => 'https://img2.freepng.ru/20180426/hge/kisspng-hyperlink-computer-icons-web-design-5ae1c9db6b4b47.2811490415247467154395.jpg',
                'title'       => $this->title,
                'url'         => Url::to('/', true),
                'description' => 'Этот сервис предназначен для преобразования ссылки в короткую. Короткая ссылка формируется по стандарту BASE58, который исключает неоднозначное его понимание при разборе пользователем с экрана или с бумаги.',
            ]) ?>
        <?php endif; ?>
    </div>
</div>






