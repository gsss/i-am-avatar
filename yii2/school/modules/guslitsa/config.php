<?php
/**
 * Created by PhpStorm.
 * User: Ra-m-ha
 * Date: 28.10.2019
 * Time: 16:42
 */

return [
    'components' => [
        // список конфигураций компонентов
        'db'     => [
            'class'               => 'yii\db\Connection',
            'charset'             => 'utf8',
            'dsn'                 => 'mysql:host=localhost;dbname=school_guslitsa',
            'username'            => 'root',
            'password'            => 'a546066',

            // Enable cache schema
            'enableSchemaCache'   => true,

            // Duration of schema cache.
            'schemaCacheDuration' => 3600,

            // Name of the cache component used to store schema information
            'schemaCache'         => 'cache',
        ],
    ],
    'params'     => [

    ],
];