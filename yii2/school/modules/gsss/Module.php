<?php
/**
 * Created by PhpStorm.
 * User: Ra-m-ha
 * Date: 23.10.2019
 * Time: 17:45
 */

namespace school\modules\gsss;

use cs\services\VarDumper;
use yii\base\BootstrapInterface;
use Yii;
use yii\base\Application;

class Module extends \yii\base\Module implements BootstrapInterface
{

//    public $defaultRoute = 'site/index';

    public function init()
    {
        parent::init();

        // инициализация модуля с помощью конфигурации, загруженной из config.php
        \Yii::configure($this, require __DIR__ . '/config.php');

        if (Yii::$app instanceof \yii\console\Application) {
            $this->controllerNamespace = 'school\modules\gsss\commands';
        } else {
            $this->controllerNamespace = 'school\modules\gsss\controllers';
        }
    }

    /**
     * @param Application $app
     */
    public function bootstrap($app)
    {
        $app->urlManager->addRules([
            'api/<controller>/<action>'                                                            => 'api-<controller>/<action>',

            'blog/<year:\\d{4}>/<month:\\d{2}>/<day:\\d{2}>/<id:\\w+>'                             => 'blog/item',
            'chenneling/<year:\\d{4}>/<month:\\d{2}>/<day:\\d{2}>/<id:\\w+>'                       => 'page/chenneling_item',
            'category/<category:\\w+>/article/<year:\\d{4}>/<month:\\d{2}>/<day:\\d{2}>/<id:\\w+>' => 'page/article',
            'news/<year:\\d{4}>/<month:\\d{2}>/<day:\\d{2}>/<id:\\w+>'                             => 'news/item',
            'praktice/<year:\\d{4}>/<month:\\d{2}>/<day:\\d{2}>/<id:\\w+>'                         => 'page/praktice_item',

            // Возврат из Яндекс Маркета
            'site_cabinet/request'                                                                 => 'cabinet_shop_client/request',


            'checkBoxTreeMask/add'                                                                 => 'check_box_tree_mask/add',
            'checkBoxTreeMask/addInto'                                                             => 'check_box_tree_mask/add_into',
            'checkBoxTreeMask/delete'                                                              => 'check_box_tree_mask/delete',

            '_widget/RadioListTree/add'                                                            => 'widget_radio_list_tree/add',
            '_widget/RadioListTree/addInto'                                                        => 'widget_radio_list_tree/add_into',
            '_widget/RadioListTree/delete'                                                         => 'widget_radio_list_tree/delete',

            'upload/upload'                                                                        => 'upload/upload',
            'upload2/upload'                                                                       => 'upload2/upload',
            'upload3/upload'                                                                       => 'upload3/upload',
            'upload/HtmlContent2'                                                                  => 'html_content/upload',
            'upload/HtmlContent2_common'                                                           => 'html_content_common/upload',

            'password/recover'                                                                     => 'auth/password_recover',
            'password/recover/activate/<code:\\w+>'                                                => 'auth/password_recover_activate',

            'registration'                                                                         => 'auth/registration',
            'registrationActivate/<code:\\w+>'                                                     => 'auth/registration_activate',
            'login'                                                                                => 'auth/login',
            'loginAjax'                                                                            => 'auth/login_ajax',
            'logout'                                                                               => 'auth/logout',
            'auth'                                                                                 => 'auth/auth',

            'rent'                                                                                 => 'site/rent',

            'key'                                                                                  => 'site/key',

            'direction'                                                                            => 'direction/index',
            'semya'                                                                                => 'direction/semya',

            'help'                                                                                 => 'help/index',
            'help/<id:\\d+>'                                                                       => 'help/item',

            'category/<id:\\w+>'                                                                   => 'page/category',
            '/'                                                                                    => 'site/index',
            'contact'                                                                              => 'site/contact',
            'clear'                                                                                => 'site/clear',
            'about'                                                                                => 'site/about',
            'captcha'                                                                              => 'site/captcha',
            'log'                                                                                  => 'site/log',
            'logDb'                                                                                => 'site/log_db',
            'siteUpdate'                                                                           => 'site/site_update',
            'siteUpdateAjax'                                                                       => 'site/site_update_ajax',
            'test'                                                                                 => 'site/test',
            'test/ajax'                                                                            => 'site/test_ajax',
            'errorAjaxReport'                                                                      => 'site/error_ajax_report',
            'stat'                                                                                 => 'site/statistic',
            'service'                                                                              => 'site/service',
            'thankyou'                                                                             => 'site/thankyou',
            'moneyBack'                                                                            => 'site/money',
            'thank'                                                                                => 'site/thank',
            'logo'                                                                                 => 'site/logo',
            'ministration'                                                                         => 'site/ministration',
            'teslaGen'                                                                             => 'site/teslagen',
            'hologram'                                                                             => 'site/hologram',

            'video'                                                                                => 'video/index',
            'video/<id:\\d+>'                                                                      => 'video/item',
            'video/category/<id:\\d+>'                                                             => 'video/category',

            'humanDesign'                                                                          => 'human_design/index',
            'humanDesign/genKeys/<id:\\d+>'                                                        => 'human_design/gen_keys_item',
            'humanDesign/about'                                                                    => 'human_design/about',
            'humanDesign/cross'                                                                    => 'human_design/cross',
            'humanDesign/keys'                                                                     => 'human_design/keys',
            'humanDesign/ajax'                                                                     => 'human_design/ajax',
            'humanDesign/ajax-delete'                                                              => 'human_design/ajax-delete',

            'vasudev'                                                                              => 'busines_club/index',
            'vasudev/login'                                                                        => 'busines_club/login',

            'user/<id:\\d+>'                                                                       => 'site/user',
            'user/<id:\\d+>/rod'                                                                   => 'site/user_rod_list',
            'user/<user_id:\\d+>/rod/<rod_id:\\d+>'                                                => 'site/user_rod',
            'user/<user_id:\\d+>/rod/<rod_id:\\d+>/edit'                                           => 'site/user_rod_edit',

            'subscribe/unsubscribe'                                                                => 'subscribe/unsubscribe',
            'subscribe/mail'                                                                       => 'subscribe/mail',
            'subscribe/activate'                                                                   => 'subscribe/activate',

            'calendar'                                                                             => 'calendar/index',
            'calendar/town'                                                                        => 'calendar/town',
            'calendar/friends'                                                                     => 'calendar/friends',
            'calendar/friends/vkontakte'                                                           => 'calendar/friends_vkontakte',
            'calendar/save'                                                                        => 'calendar/save',
            'calendar/widget'                                                                      => 'calendar/widget',
            'calendar/cache/show'                                                                  => 'calendar/cache_show',
            'calendar/cache/delete'                                                                => 'calendar/cache_delete',
            'calendar/moon'                                                                        => 'calendar/moon',
            'calendar/colkin'                                                                      => 'calendar/colkin',
            'calendar/colkin/more'                                                                 => 'calendar/colkin_more',
            'calendar/spyral'                                                                      => 'calendar/spyral',
            'calendar/orakul'                                                                      => 'calendar/orakul',
            'calendar/13yasnihZnakov'                                                              => 'calendar/yasnih_znakov',

            'moderator'                                                                            => 'moderator/index',

            'moderator/channeling'                                                                 => 'moderator_channeling/index',
            'moderator/channeling/<id:\\d+>/accept'                                                => 'moderator_channeling/accept',
            'moderator/channeling/<id:\\d+>/reject'                                                => 'moderator_channeling/reject',
            'moderator/channeling/<id:\\d+>/view'                                                  => 'moderator_channeling/view',

            'moderator/unionList'                                                                  => 'moderator_unions/index',
            'moderator/unionList/<id:\\d+>/accept'                                                 => 'moderator_unions/accept',
            'moderator/unionList/<id:\\d+>/reject'                                                 => 'moderator_unions/reject',
            'moderator/unionList/<id:\\d+>/delete'                                                 => 'moderator_unions/delete',

            'moderator/shop/productList'                                                           => 'moderator_shop/product_list',
            'moderator/shop/productList/<id:\\d+>/accept'                                          => 'moderator_shop/accept',
            'moderator/shop/productList/<id:\\d+>/reject'                                          => 'moderator_shop/reject',
            'moderator/shop/productList/<id:\\d+>/view'                                            => 'moderator_shop/view',

            'moderator/shop/imageList'                                                             => 'moderator_shop/image_list',
            'moderator/shop/imageList/<id:\\d+>/accept'                                            => 'moderator_shop/image_list_accept',
            'moderator/shop/imageList/<id:\\d+>/reject'                                            => 'moderator_shop/image_list_reject',


            'admin'                                                                                => 'admin/index',

            'admin/pictures'                                                                       => 'admin_pictures/index',
            'admin/pictures/add'                                                                   => 'admin_pictures/add',
            'admin/pictures/<id:\\d+>'                                                             => 'admin_pictures/view',

            'admin/serviceList'                                                                    => 'admin_service/index',
            'admin/serviceList/add'                                                                => 'admin_service/add',
            'admin/serviceList/<id:\\d+>/delete'                                                   => 'admin_service/delete',
            'admin/serviceList/<id:\\d+>/edit'                                                     => 'admin_service/edit',
            'admin/serviceList/<id:\\d+>/subscribe'                                                => 'admin_service/subscribe',

            'admin/shopCatalog'                                                                    => 'admin-shop-catalog/index',
            'admin/shopCatalog/add'                                                                => 'admin-shop-catalog/add',
            'admin/shopCatalog/<id:\\d+>/delete'                                                   => 'admin-shop-catalog/delete',
            'admin/shopCatalog/sort'                                                               => 'admin-shop-catalog/sort',
            'admin/shopCatalog/<id:\\d+>/sort'                                                     => 'admin-shop-catalog/sort-category',
            'admin/shopCatalog/<id:\\d+>/ajax'                                                     => 'admin-shop-catalog/sort-ajax',
            'admin/shopCatalog/<id:\\d+>/edit'                                                     => 'admin-shop-catalog/edit',

            'admin/sertRed'                                                                        => 'admin_sert_red/index',
            'admin/sertRed/add'                                                                    => 'admin_sert_red/add',
            'admin/sertRed/<id:\\d+>/delete'                                                       => 'admin_sert_red/delete',
            'admin/sertRed/<id:\\d+>/edit'                                                         => 'admin_sert_red/edit',

            'admin/unions'                                                                         => 'admin_unions/index',
            'admin/unions/add'                                                                     => 'admin_unions/add',
            'admin/unions/<id:\\d+>/delete'                                                        => 'admin_unions/delete',
            'admin/unions/<id:\\d+>/edit'                                                          => 'admin_unions/edit',
            'admin/unions/<id:\\d+>/shop'                                                          => 'admin_unions_shop/index',

            'admin/gods'                                                                           => 'admin-gods/index',
            'admin/gods/add'                                                                       => 'admin-gods/add',
            'admin/gods/<id:\\d+>/delete'                                                          => 'admin-gods/delete',
            'admin/gods/<id:\\d+>/edit'                                                            => 'admin-gods/edit',

            'admin/smeta'                                                                          => 'admin_smeta/index',
            'admin/smeta/add'                                                                      => 'admin_smeta/add',
            'admin/smeta/<id:\\d+>/delete'                                                         => 'admin_smeta/delete',
            'admin/smeta/<id:\\d+>/edit'                                                           => 'admin_smeta/edit',

            'admin/help'                                                                           => 'admin_help/index',
            'admin/help/add'                                                                       => 'admin_help/add',
            'admin/help/<id:\\d+>/delete'                                                          => 'admin_help/delete',
            'admin/help/<id:\\d+>/edit'                                                            => 'admin_help/edit',

            'admin/news'                                                                           => 'admin-news/index',
            'admin/news/add'                                                                       => 'admin-news/add',
            'admin/news/<id:\\d+>/delete'                                                          => 'admin-news/delete',
            'admin/news/<id:\\d+>/edit'                                                            => 'admin-news/edit',
            'admin/news/<id:\\d+>/subscribe'                                                       => 'admin-news/subscribe',

            'admin/events'                                                                         => 'admin_events/index',
            'admin/events/add'                                                                     => 'admin_events/add',
            'admin/events/<id:\\d+>/delete'                                                        => 'admin_events/delete',
            'admin/events/<id:\\d+>/edit'                                                          => 'admin_events/edit',
            'admin/events/<id:\\d+>/subscribe'                                                     => 'admin_events/subscribe',

            'admin/video'                                                                          => 'admin_video/index',
            'admin/video/add'                                                                      => 'admin_video/add',
            'admin/video/<id:\\d+>/delete'                                                         => 'admin_video/delete',
            'admin/video/<id:\\d+>/edit'                                                           => 'admin_video/edit',

            'admin/praktice'                                                                       => 'admin_praktice/index',
            'admin/praktice/add'                                                                   => 'admin_praktice/add',
            'admin/praktice/<id:\\d+>/delete'                                                      => 'admin_praktice/delete',
            'admin/praktice/<id:\\d+>/edit'                                                        => 'admin_praktice/edit',

            'admin/blog'                                                                           => 'admin_blog/index',
            'admin/blog/add'                                                                       => 'admin_blog/add',
            'admin/blog/addFromPage'                                                               => 'admin_blog/add_from_page',
            'admin/blog/<id:\\d+>/delete'                                                          => 'admin_blog/delete',
            'admin/blog/<id:\\d+>/edit'                                                            => 'admin_blog/edit',
            'admin/blog/<id:\\d+>/subscribe'                                                       => 'admin_blog/subscribe',
            'admin/blog/<id:\\d+>/subscribeSuccess'                                                => 'admin_blog/subscribe-success',

            'admin/hdGenKeys'                                                                      => 'admin_hd_gen_keys/index',
            'admin/hdGenKeys/add'                                                                  => 'admin_hd_gen_keys/add',
            'admin/hdGenKeys/<id:\\d+>/delete'                                                     => 'admin_hd_gen_keys/delete',
            'admin/hdGenKeys/<id:\\d+>/edit'                                                       => 'admin_hd_gen_keys/edit',

            'admin/channelingPower'                                                                => 'admin_channeling_power/index',
            'admin/channelingPower/add'                                                            => 'admin_channeling_power/add',
            'admin/channelingPower/<id:\\d+>/delete'                                               => 'admin_channeling_power/delete',
            'admin/channelingPower/<id:\\d+>/edit'                                                 => 'admin_channeling_power/edit',

            'admin/channelingAuthor'                                                               => 'admin_channeling_author/index',
            'admin/channelingAuthor/add'                                                           => 'admin_channeling_author/add',
            'admin/channelingAuthor/<id:\\d+>/delete'                                              => 'admin_channeling_author/delete',
            'admin/channelingAuthor/<id:\\d+>/edit'                                                => 'admin_channeling_author/edit',


            'admin/articleList'                                                                    => 'admin_article/index',
            'admin/articleList/add'                                                                => 'admin_article/add',
            'admin/articleList/addFromPage'                                                        => 'admin_article/add_from_page',
            'admin/articleList/<id:\\d+>/delete'                                                   => 'admin_article/delete',
            'admin/articleList/<id:\\d+>/edit'                                                     => 'admin_article/edit',
            'admin/articleList/<id:\\d+>/subscribe'                                                => 'admin_article/subscribe',
            'admin/articleList/<id:\\d+>/subscribeSuccess'                                         => 'admin_article/subscribe-success',
            'admin/categoryList'                                                                   => 'admin_category/index',
            'admin/categoryList/add'                                                               => 'admin_category/add',
            'admin/categoryList/<id:\\d+>/delete'                                                  => 'admin_category/delete',
            'admin/categoryList/<id:\\d+>/edit'                                                    => 'admin_category/edit',
            'admin/investigator'                                                                   => 'admin_investigator/index',

            'admin/vozdayanie'                                                                     => 'admin_vozdayanie/index',
            'admin/vozdayanie/add'                                                                 => 'admin_vozdayanie/add',
            'admin/vozdayanie/<id:\\d+>/delete'                                                    => 'admin_vozdayanie/delete',
            'admin/vozdayanie/<id:\\d+>/edit'                                                      => 'admin_vozdayanie/edit',

            'admin/users'                                                                          => 'admin_users/index',
            'admin/users/add'                                                                      => 'admin_users/add',
            'admin/users/<id:\\d+>/delete'                                                         => 'admin_users/delete',
            'admin/users/<id:\\d+>/edit'                                                           => 'admin_users/edit',

            // редактор
            'editor/channeling'                                                                    => 'editor-channeling/index',
            'editor/channeling/add'                                                                => 'editor-channeling/add',
            'editor/channeling/addFromPage'                                                        => 'editor-channeling/add-from-page',
            'editor/channeling/<id:\\d+>/edit'                                                     => 'editor-channeling/edit',
            'editor/channeling/<id:\\d+>/subscribe'                                                => 'editor-channeling/subscribe',

            'penalty/<id:\\d+>'                                                                    => 'penalty/pay',

            'editor/blog'                                                                          => 'editor-blog/index',
            'editor/blog/add'                                                                      => 'editor-blog/add',
            'editor/blog/addFromPage'                                                              => 'editor-blog/add-from-page',
            'editor/blog/<id:\\d+>/edit'                                                           => 'editor-blog/edit',
            'editor/blog/<id:\\d+>/subscribe'                                                      => 'editor-blog/subscribe',

            // магазин
            'shop'                                                                                 => 'shop/index',
            'shop/loginAjax'                                                                       => 'shop/login_ajax',
            'shop/registrationAjax'                                                                => 'shop/registration_ajax',
            'shop/category/<id:\\d+>'                                                              => 'shop/category',
            'shop/product/<id:\\d+>'                                                               => 'shop/product',

            'shop/basket'                                                                          => 'shop/basket',
            'shop/basket/delete'                                                                   => 'shop/basket_delete',
            'shop/basket/update'                                                                   => 'shop/basket_update',
            'shop/basket/add'                                                                      => 'shop/basket_add',
            'shop/order'                                                                           => 'shop/order',
            'shop/order/ajax'                                                                      => 'shop/order_ajax',
            'shop/order/ajaxNalozh'                                                                => 'shop/order-ajax-nalozh',
            'shop/order/success'                                                                   => 'shop/order_success',
            'shop/order/success/ok-pay'                                                            => 'shop/order-success-ok-pay',
            'shop/order/buy/<id:\\d+>'                                                             => 'shop/order_buy',
            'shop/order/buy-ok-pay/<id:\\d+>'                                                      => 'shop/order-buy-ok-pay',
            'shop/search'                                                                          => 'shop/search',
            'shop/setNoHelpWindow'                                                                 => 'shop/set_no_help_window',


            // /Магазин
            'praktice'                                                                             => 'page/praktice',
            'house'                                                                                => 'page/house',
            'mission'                                                                              => 'page/mission',
            'medical'                                                                              => 'page/medical',
            'up'                                                                                   => 'page/up',
            'study'                                                                                => 'page/study',
            'study/add'                                                                            => 'page_add/study',
            'time'                                                                                 => 'page/time',
            'time/arii'                                                                            => 'page/time_arii',
            'language'                                                                             => 'page/language',
            'language/article/<id:\\w+>'                                                           => 'page/language_article',
            'energy'                                                                               => 'page/energy',
            'money'                                                                                => 'page/money',
            'food'                                                                                 => 'page/food',
            'food/<id:\\d+>'                                                                       => 'page/food_item',
            'category/<category:\\w+>/<id:\\d+>'                                                   => 'page/union_item',
            'forgive'                                                                              => 'page/forgive',
            'tv'                                                                                   => 'page/tv',

            'category/<category:\\w+>/<id:\\d+>/shop'                                              => 'union_shop/index',
            'category/<category:\\w+>/<id:\\d+>/shop/<shop_category:\\d+>'                         => 'union_shop/category',
            'category/<category:\\w+>/<union_id:\\d+>/shop/product/<id:\\d+>'                      => 'union_shop/product',

            'newEarth'                                                                             => 'new_earth/index',
            'newEarth/order'                                                                       => 'new_earth/nmp',
            'newEarth/const'                                                                       => 'new_earth/const',
            'newEarth/game'                                                                        => 'new_earth/game',
            'newEarth/piramida'                                                                    => 'new_earth/piramida',
            'newEarth/eva'                                                                         => 'new_earth/eva',
            'newEarth/order/success'                                                               => 'new_earth/nmp_success',
            'newEarth/price'                                                                       => 'new_earth/price',
            'newEarth/flag'                                                                        => 'new_earth/flag',
            'newEarth/kon'                                                                         => 'new_earth/kon',
            'newEarth/noosfera'                                                                    => 'new_earth/noosfera',
            'newEarth/service/sert'                                                                => 'new_earth/service_cert',
            'newEarth/service/vozdayanie'                                                          => 'new_earth/service_vozdayanie',
            'newEarth/service/vozdayanie/ring'                                                     => 'new_earth/service_vozdayanie_ring',
            'newEarth/chakri'                                                                      => 'new_earth/chakri',
            'newEarth/civilizations'                                                               => 'new_earth/civilizations',
            'newEarth/civilizations/<name:\\w+>'                                                   => 'new_earth/civilizations_item',
            'newEarth/conditions'                                                                  => 'new_earth/conditions',
            'newEarth/formulaEnter'                                                                => 'new_earth/formula_enter',
            'declaration'                                                                          => 'new_earth/declaration',
            'residence'                                                                            => 'new_earth/residence',
            'manifest'                                                                             => 'new_earth/manifest',
            'history'                                                                              => 'new_earth/history',
            'pledge'                                                                               => 'new_earth/pledge',
            'program'                                                                              => 'new_earth/program',
            'hymn'                                                                                 => 'new_earth/hymn',
            'hymn2'                                                                                => 'new_earth/hymn2',
            'codex'                                                                                => 'new_earth/codex',

            'events'                                                                               => 'calendar/events',
            'events/<id:\\d+>'                                                                     => 'calendar/events_item',

            'conditions'                                                                           => 'site/conditions',
            'clothes'                                                                              => 'page/clothes',
            'portals'                                                                              => 'page/portals',
            'arts'                                                                                 => 'page/arts',
            'idea'                                                                                 => 'page/idea',
            'music'                                                                                => 'page/music',

            'news'                                                                                 => 'news/index',
            'news/ajax'                                                                            => 'news/ajax',

            'blog'                                                                                 => 'blog/index',
            'blog/ajax'                                                                            => 'blog/ajax',

            'cabinet-graal-finance/in-requests-messages/<id:\\d+>/confirm'                         => 'cabinet-graal-finance/confirm',
            'cabinet-graal-finance/in-requests-messages/<id:\\d+>/message'                         => 'cabinet-graal-finance/message',
            'cabinet-graal/in-requests-messages/<id:\\d+>/message'                                 => 'cabinet-graal/message',

            'cabinet-finance/out-messages/<id:\\d+>/confirm'                                       => 'cabinet-finance/confirm',
            'cabinet-finance/out-messages/<id:\\d+>/message'                                       => 'cabinet-finance/message',


            'chenneling'                                                                           => 'page/chenneling',
            'chenneling/search'                                                                    => 'page/chenneling_search',
            'chenneling/search_ajax'                                                               => 'page/chenneling_search_ajax',
            'chenneling/ajax'                                                                      => 'page/chenneling_ajax',
            'services'                                                                             => 'page/services',
            'services/<id:\\d+>'                                                                   => 'page/services_item',
            'page/<action>'                                                                        => 'page/<action>',

            'comment/send'                                                                         => 'comment/send',

            'cabinet/shop/<id:\\d+>'                                                               => 'cabinet_shop_shop/index',


            'cabinet/officeList/<unionId:\\d+>'                                                    => 'cabinet_office/index',
            'cabinet/officeList/<unionId:\\d+>/add'                                                => 'cabinet_office/add',
            'cabinet/officeList/<id:\\d+>/delete'                                                  => 'cabinet_office/delete',
            'cabinet/officeList/<id:\\d+>/edit'                                                    => 'cabinet_office/edit',

            'cabinet/channeling'                                                                   => 'cabinet_channeling/index',
            'cabinet/channeling/add'                                                               => 'cabinet_channeling/add',
            'cabinet/channeling/<id:\\d+>/edit'                                                    => 'cabinet_channeling/edit',
            'cabinet/channeling/<id:\\d+>/delete'                                                  => 'cabinet_channeling/delete',
            'cabinet/channeling/<id:\\d+>/subscribe'                                               => 'cabinet_channeling/subscribe',
            'cabinet/channeling/<id:\\d+>/view'                                                    => 'cabinet_channeling/view',
            'cabinet/channeling/<id:\\d+>/sendModeration'                                          => 'cabinet_channeling/send_moderation',

            'cabinet/shop/<id:\\d+>/paysystemList'                                                 => 'cabinet-shop-shop-paysystem-list/index',
            'cabinet/shop/<id:\\d+>/paysystemList/add'                                             => 'cabinet-shop-shop-paysystem-list/add',
            'cabinet/shop/paysystemList/<id:\\d+>/edit'                                            => 'cabinet-shop-shop-paysystem-list/edit',
            'cabinet/shop/paysystemList/<id:\\d+>/delete'                                          => 'cabinet-shop-shop-paysystem-list/delete',

            'cabinet/shop/<id:\\d+>/productList'                                                   => 'cabinet_shop_shop/product_list',
            'cabinet/shop/<id:\\d+>/productList/add'                                               => 'cabinet_shop_shop/product_list_add',
            'cabinet/shop/productList/<id:\\d+>/edit'                                              => 'cabinet_shop_shop/product_list_edit',
            'cabinet/shop/productList/<id:\\d+>/delete'                                            => 'cabinet_shop_shop/product_list_delete',
            'cabinet/shop/productList/<id:\\d+>/sendModeration'                                    => 'cabinet_shop_shop/product_list_send_moderation',
            'cabinet/shop/productList/<id:\\d+>/images'                                            => 'cabinet_shop_shop_images/index',
            'cabinet/shop/productList/<id:\\d+>/images/add'                                        => 'cabinet_shop_shop_images/add',
            'cabinet/shop/productList/images/<id:\\d+>/edit'                                       => 'cabinet_shop_shop_images/edit',
            'cabinet/shop/productList/images/<id:\\d+>/delete'                                     => 'cabinet_shop_shop_images/delete',

            'cabinet/shop/<id:\\d+>/requestList'                                                   => 'cabinet_shop_shop/request_list',
            'cabinet/shop/<id:\\d+>/requestList/all'                                               => 'cabinet_shop_shop/request_list_all',
            'cabinet/shop/requestList/<id:\\d+>/delete'                                            => 'cabinet_shop_shop/request_list_delete',
            'cabinet/shop/requestList/<id:\\d+>/hide'                                              => 'cabinet_shop_shop/request_list_hide',
            'cabinet/shop/requestList/<id:\\d+>'                                                   => 'cabinet_shop_shop/request_list_item',
            'cabinet/shop/requestList/<id:\\d+>/message'                                           => 'cabinet_shop_shop/request_list_item_message',
            'cabinet/shop/requestList/<id:\\d+>/pay-client'                                        => 'cabinet_shop_shop/request-list-item-pay-client',
            'cabinet/shop/requestList/<id:\\d+>/nalozh-prepared'                                   => 'cabinet_shop_shop/request-list-item-nalozh-prepared',
            'cabinet/shop/requestList/<id:\\d+>/nalozh-sended'                                     => 'cabinet_shop_shop/request-list-item-nalozh-sended',
            'cabinet/shop/requestList/<id:\\d+>/nalozh-paid'                                       => 'cabinet_shop_shop/request-list-item-nalozh-paid',
            'cabinet/shop/requestList/<id:\\d+>/nalozh-canceled'                                   => 'cabinet_shop_shop/request-list-item-nalozh-canceled',
            'cabinet/shop/requestList/<id:\\d+>/answerPay'                                         => 'cabinet_shop_shop/request_list_item_answer_pay',
            'cabinet/shop/requestList/<id:\\d+>/samovivozDone'                                     => 'cabinet_shop_shop/request_list_item_samovivoz_done',
            'cabinet/shop/requestList/<id:\\d+>/send'                                              => 'cabinet_shop_shop/request_list_item_send',
            'cabinet/shop/requestList/<id:\\d+>/prepared'                                          => 'cabinet_shop_shop/request_list_item_prepared',
            'cabinet/shop/<id:\\d+>/dostavkaList'                                                  => 'cabinet_shop_shop/dostavka_list',
            'cabinet/shop/dostavkaList/<id:\\d+>/edit'                                             => 'cabinet_shop_shop/dostavka_list_edit',
            'cabinet/shop/<id:\\d+>/dostavkaList/add'                                              => 'cabinet_shop_shop/dostavka_list_add',

            'cabinet/shop/checkBoxTreeMask/add'                                                    => 'cabinet_shop_check_box_tree_mask/add',
            'cabinet/shop/checkBoxTreeMask/addInto'                                                => 'cabinet_shop_check_box_tree_mask/add_into',
            'cabinet/shop/checkBoxTreeMask/delete'                                                 => 'cabinet_shop_check_box_tree_mask/delete',


            'cabinet/orders'                                                                       => 'cabinet_shop_client/orders',
            'cabinet/order/<id:\\d+>'                                                              => 'cabinet_shop_client/order_item',
            'cabinet/order/<id:\\d+>/cancel'                                                       => 'cabinet_shop_client/order_item_cancel',
            'cabinet/order/<id:\\d+>/message'                                                      => 'cabinet_shop_client/order_item_message',
            'cabinet/order/<id:\\d+>/answerPay'                                                    => 'cabinet_shop_client/order_item_answer_pay',
            'cabinet/order/<id:\\d+>/doneSomovivoz'                                                => 'cabinet_shop_client/order_item_done_somovivoz',
            'cabinet/order/<id:\\d+>/doneAddress'                                                  => 'cabinet_shop_client/order_item_done_address',

            'objects'                                                                              => 'cabinet/objects',
            'objects/<id:\\d+>/edit'                                                               => 'cabinet/objects_edit',
            'objects/add'                                                                          => 'cabinet/objects_add',
            'objects/<id:\\d+>/delete'                                                             => 'cabinet/objects_delete',
            'objects/<id:\\d+>/subscribe'                                                          => 'cabinet/objects_subscribe',
            'objects/<id:\\d+>/sendModeration'                                                     => 'cabinet/objects_send_moderation',
            'cabinet/passwordChange'                                                               => 'cabinet/password_change',
            'cabinet/profile'                                                                      => 'cabinet/profile',
            'cabinet/profile/possibilities'                                                        => 'cabinet/profile_possibilities',
            'cabinet/profile/humanDesign'                                                          => 'cabinet/profile_human_design',
            'cabinet/profile/humanDesign/ajax'                                                     => 'cabinet/profile_human_design_ajax',
            'cabinet/profile/humanDesign/delete'                                                   => 'cabinet/profile_human_design_delete',
            'cabinet/profile/humanDesign/genKeys'                                                  => 'cabinet/profile_human_design_gen_keys',
            'cabinet/profile/humanDesign/genKeys/<id:\\d+>'                                        => 'cabinet/profile_human_design_gen_keys_item',

            'cabinet/profile/walletBtc/in'                                                         => 'cabinet-wallet-btc/in',
            'cabinet/profile/walletBtc/send'                                                       => 'cabinet-wallet-btc/send',
            'cabinet/profile/walletBtc/create'                                                     => 'cabinet-wallet-btc/create',
            'cabinet/profile/walletBtc/save-address'                                               => 'cabinet-wallet-btc/save-address',
            'cabinet/profile/walletBtc/transactions'                                               => 'cabinet-wallet-btc/transactions',
            'cabinet/profile/walletBtc/balanceExternal'                                            => 'cabinet-wallet-btc/balance-external',
            'cabinet/profile/walletBtc/balanceInternal'                                            => 'cabinet-wallet-btc/balance-internal',

            'cabinet/profile/unLinkSocialNetWork'                                                  => 'cabinet/profile_unlink_social_network',
            'cabinet/profile/zvezdnoe'                                                             => 'cabinet/profile_zvezdnoe',
            'cabinet/profile/time'                                                                 => 'cabinet/profile_time',
            'cabinet/profile/wallet'                                                               => 'cabinet_wallet/index',
            'cabinet/profile/wallet/referalLink'                                                   => 'cabinet_wallet/referal_link',
            'cabinet/profile/wallet/referalDocument'                                               => 'cabinet_wallet/referal-document',
            'cabinet/profile/wallet/phoneEdit'                                                     => 'cabinet_wallet/phone-edit',
            'cabinet/profile/wallet/phoneEditSave'                                                 => 'cabinet_wallet/phone-edit-save',
            'cabinet/profile/wallet/facebookFriends'                                               => 'cabinet_wallet/facebook-friends',
            'cabinet/profile/wallet/childs'                                                        => 'cabinet_wallet/childs',

            'cabinet/profile/wallet/in'                                                            => 'cabinet_wallet/in',
            'cabinet/profile/wallet/inAjax'                                                        => 'cabinet_wallet/in-ajax',
            'cabinet/profile/wallet/inSuccess'                                                     => 'cabinet_wallet/in-success',

            'cabinet/profile/wallet/out'                                                           => 'cabinet_wallet/out',
            'cabinet/profile/wallet/out/requests'                                                  => 'cabinet_wallet/out-requests',
            'cabinet/profile/wallet/out/<id:\\d+>'                                                 => 'cabinet_wallet/out-item',
            'cabinet/profile/wallet/out/<id:\\d+>/message'                                         => 'cabinet_wallet/out-item-message',

            'cabinet/profile/wallet/move'                                                          => 'cabinet_wallet/move',

            'cabinet/profile/subscribe'                                                            => 'cabinet/profile_subscribe',
            'cabinet/mindMap'                                                                      => 'cabinet/mind_map',
            'cabinet/poseleniya'                                                                   => 'cabinet/poseleniya',
            'cabinet/poseleniya/add'                                                               => 'cabinet/poseleniya_add',
            'cabinet/poseleniya/<id:\\d+>/edit'                                                    => 'cabinet/poseleniya_edit',
            'cabinet/poseleniya/<id:\\d+>/delete'                                                  => 'cabinet/poseleniya_delete',


            '<controller>/<action>'                                                                => '<controller>/<action>',
            '<controller>/<action>/<id:\\d+>'                                                      => '<controller>/<action>',
        ]);
    }

}