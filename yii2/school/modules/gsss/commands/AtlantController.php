<?php

namespace app\commands;

use app\models\Piramida\InRequest;
use app\models\SubscribeMailItem;
use yii\console\Controller;
use yii\console\Response;
use yii\helpers\ArrayHelper;
use yii\helpers\VarDumper;

/**
 * Занимается обслуживанием авторизации
 */
class AtlantController extends Controller
{
    /**
     * Делает рассылку писем из списка рассылки
     */
    public function actionClearInRequests()
    {
        $i = InRequest::deleteAll('datetime < :time', [':time' => time() - 60*60*24]);
        echo('Удалено: '.$i);
    }
}
