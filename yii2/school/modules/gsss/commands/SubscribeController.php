<?php

namespace app\commands;

use app\models\Subscribe;
use app\models\SubscribeMail;
use app\models\SubscribeMailItem;
use cs\Application;
use yii\console\Controller;
use yii\console\Response;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\helpers\VarDumper;
use app\models\SubscribeStat;

/**
 * Занимается рассылкой писем
 */
class SubscribeController extends Controller
{
    /**
     * Делает рассылку писем из списка рассылки
     */
    public function actionSend()
    {
        $limit = 8; // писем в минуту
        $limitPerDay = 1900; // писем в день

        $counterDay = \common\models\Config::get('mailCounterPerDay');
        if ($counterDay + $limit > $limitPerDay) {
            echo 'limit end $counterDay=' . $counterDay;
            echo "\n";
            return;
        }


        $time = microtime(true);
        $idsSuccess = [];
        $rows = SubscribeMail::find()
            ->limit($limit)
            ->select(['id', 'subscribe_id', 'mail'])
            ->all();
        if (count($rows) > 0) {
            $subscribeIds = ArrayHelper::map($rows, 'id', 'mail', 'subscribe_id');
            foreach ($subscribeIds as $subscribeId => $mailList) {
                /** @var Subscribe $s */
                $s = Subscribe::findOne($subscribeId);
                if ($s) {
                    $scheme = true;
                    if (!empty($s->un_subscribe_url)) {
                        $scheme = $s->un_subscribe_url;
                    }
                    foreach ($mailList as $id => $mail) {
                        $urlUnSubscribe = Url::to(['subscribe/unsubscribe', 'mail' => $mail, 'type' => $s->type, 'hash' => \common\services\Subscribe::hashGenerate($mail, $s->type)], $scheme);
                        $html = str_replace('{linkUnsubscribe}', $urlUnSubscribe, $s->html);
                        try {
                            $mailO = \Yii::$app->mailer
                                ->compose()
                                ->setFrom(\Yii::$app->params['mailer']['from'])
                                ->setTo($mail)
                                ->setSubject($s->subject)
                                ->setHtmlBody($html);
                        } catch (\Exception $e) {
                            \Yii::info($e->getMessage(), 'avatar\\app\\commands\\SubscribeController::actionSend2');
                        }

                        echo $mail;
                        echo ' ';
                        try {
                            $result = $mailO->send();
                            if ($result == false) {
                                \Yii::info('Не удалось доствить: ' . $mail, 'avatar\\app\\commands\\SubscribeController::actionSend');
                                echo 'Не удалось доствить: ' . $mail;
                            }
                        } catch (\Exception $e) {
                            \Yii::info('Не удалось доствить: ' . $mail . ' ' . $e->getMessage(), 'avatar\\app\\commands\\SubscribeController::actionSend');
                            echo $e->getMessage();
                        }
                        echo "\n";
                        $idsSuccess[] = $id;
                    }
                } else {
                    SubscribeMail::deleteAll(['subscribe_id' => $subscribeId]);
                }
            }
            (new SubscribeStat([
                'time'    => time(),
                'counter' => count($idsSuccess),
                'delay'   => microtime(true) - $time,
            ]))->save();

            // Удаляю все старые письма
            SubscribeMail::deleteAll(['in', 'id', $idsSuccess]);

            // Устанавливаю счетчик отправленных писем
            \common\models\Config::set('mailCounterPerDay', $counterDay + count($idsSuccess));

        } else {
            Subscribe::deleteAll();
        }
    }
    /**
     * Делает рассылку писем из списка рассылки
     */
    public function actionSend2()
    {
        $limit = 20;
        try {
            $time = microtime(true);
            $list = SubscribeMailItem::query()
                ->limit($limit)
                ->orderBy(['date_insert' => SORT_ASC])
                ->all();
            $idsSuccess = [];
            if (count($list) > 0) {
                foreach ($list as $mailItem) {
                    $mail = \Yii::$app->mailer
                        ->compose()
                        ->setFrom(\Yii::$app->params['mailer']['from'])
                        ->setTo($mailItem['mail'])
                        ->setSubject($mailItem['subject'])
                        ->setHtmlBody($mailItem['html']);
                    if (isset($mailItem['text'])) {
                        if ($mailItem['text'] != '') {
                            $mail->setTextBody($mailItem['text']);
                        }
                    }
                    $result = $mail->send();
                    if ($result == false) {
                        \Yii::info('Не удалось доствить: ' . VarDumper::dumpAsString($mailItem, 2), 'gs\\app\\commands\\SubscribeController::actionSend');
                    } else {
                        $idsSuccess[] = $mailItem['id'];
                    }
                }
                if ($idsSuccess) {
                    $i = new \app\models\SubscribeStat([
                        'time'    => time(),
                        'counter' => count($idsSuccess),
                    ]);
                    $i->save();
                    $result = SubscribeMailItem::deleteByCondition([
                        'in', 'id', $idsSuccess
                    ]);
                }

                $time = microtime(true) - $time;
            } else {
                $rows = SubscribeMail::find()
                    ->limit($limit)
                    ->select(['id', 'subscribe_id', 'mail'])
                    ->all();
                if (count($rows) > 0) {
                    $subscribeIds = ArrayHelper::map($rows, 'id', 'mail', 'subscribe_id');
                    foreach ($subscribeIds as $subscribeId => $mailList) {
                        /** @var \app\models\Subscribe $s */
                        $s = Subscribe::findOne($subscribeId);
                        if ($s) {
                            foreach ($mailList as $id => $mail) {
                                $urlUnSubscribe = Url::to(['subscribe/unsubscribe', 'mail' => $mail, 'type' => $s->type, 'hash' => \app\services\Subscribe::hashGenerate($mail, $s->type)], true);
                                $html = str_replace('{linkUnsubscribe}', $urlUnSubscribe, $s->html);
                                $mail = \Yii::$app->mailer
                                    ->compose()
                                    ->setFrom(\Yii::$app->params['mailer']['from'])
                                    ->setTo($mail)
                                    ->setSubject($s->subject)
                                    ->setHtmlBody($html);
                                if (isset($s->text)) {
                                    if ($s->text != '') {
                                        $text = str_replace('{linkUnsubscribe}', $urlUnSubscribe, $s->text);
                                        $mail->setTextBody($text);
                                    }
                                }
                                $result = $mail->send();
                                if ($result == false) {
                                    \Yii::warning('Не удалось доствить: ' . VarDumper::dumpAsString($mail, 2), 'gs\\app\\commands\\SubscribeController::actionSend');
                                } else {
                                    $idsSuccess[] = $id;
                                }
                            }
                        }
                    }
                    (new \app\models\SubscribeStat([
                        'time'    => time(),
                        'counter' => count($idsSuccess),
                        'delay'   => microtime(true) - $time,
                    ]))->save();
                    SubscribeMail::deleteAll(['in', 'id', $idsSuccess]);
                } else {
                    Subscribe::deleteAll();
                }
            }
        } catch (\Exception $e) {
            \Yii::info($e->getMessage(), 'gs\\send');
            echo $e->getMessage();
        }
    }

    public function actionGet()
    {
        echo \common\models\Config::get('mailCounterPerDay');
        echo "\n";
    }

    public function actionSetMax()
    {
        \common\models\Config::set('mailCounterPerDay', 1900);
        echo "success\n";
    }

    public function actionResetCounter()
    {
        \common\models\Config::set('mailCounterPerDay', 0);
        echo "success\n";
    }
}
