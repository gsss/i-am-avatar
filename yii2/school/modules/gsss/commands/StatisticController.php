<?php

namespace app\commands;

use app\common\components\Monitoring;
use app\common\components\OnlineManager;
use app\common\models\StatisticOnline;
use app\common\models\StatisticRoute;
use app\models\ServerRequest;
use Yii;
use yii\helpers\ArrayHelper;
use app\common\models\StatisticDb;
use yii\helpers\VarDumper;

class StatisticController extends \app\common\base\Controller
{
    /**
     * Записывает кол-во пользователей online каждую минуту в таблицу statistic_online
     */
    public function actionCalculate($isLog = 'logTrue')
    {
        $this->useEcho = ($isLog == 'logFalse')? false : true;
        $this->saveOnline();
        $this->saveMail();
        $this->saveDb();
    }

    private function saveOnline()
    {
        $o = new OnlineManager();
        $m = new Monitoring();
        $fields = [
            'time'   => time(),
            'online' => $o->count(),
        ];
        $new = $m->calculate();
        $new['request_per_second'] = round($new['request_per_second'] * 60, 2);
        $fields = ArrayHelper::merge($fields, $new);
        (new \app\common\models\StatisticOnline($fields))->save();
        $m->reset();
    }

    private function saveMail()
    {
        $values = Yii::$app->MonitoringParams->get();
        $fields = [
            'time' => time(),
            'mail' => $values['mail'],
        ];
        \common\models\statistic\StatisticMail::add($fields);
        Yii::$app->MonitoringParams->reset();
    }

    private function saveDb()
    {
        $dbStatisticValues = Yii::$app->monitoringDb->calculate();
        $fields = [
            'time'               => time(),
            'main_master_select' => $dbStatisticValues['master']['select'],
            'main_master_insert' => $dbStatisticValues['master']['insert'],
            'main_master_update' => $dbStatisticValues['master']['update'],
            'main_master_delete' => $dbStatisticValues['master']['delete'],
//            'main_slave_select'  => (count($dbStatisticValues['slaveArray']) > 0) ? $dbStatisticValues['slaveArray'][0]['select'] : 0,
            'statistic_select'   => $dbStatisticValues['statistic']['select'],
            'statistic_insert'   => $dbStatisticValues['statistic']['insert'],
            'statistic_update'   => $dbStatisticValues['statistic']['update'],
            'statistic_delete'   => $dbStatisticValues['statistic']['delete'],
        ];
        $result = (new \app\common\models\StatisticDb($fields))->save();
        if ($result) {
            Yii::$app->monitoringDb->reset();
        } else {
            $this->logDanger('Coudnt save to StatisticDb');
        }
    }

    /**
     * Удаляет старые записи старше чем 1 день
     * Желательно запускать 1 раз в день
     */
    public function actionClear($isLog = 'logTrue')
    {
        $this->useEcho = ($isLog == 'logFalse')? false : true;
        $day = 60*60*24;
        StatisticOnline::deleteAll(['<', 'time', time() - $day * 30]);
        StatisticRoute::deleteAll(['<', 'time', time() - $day]);
        StatisticDb::deleteAll(['<', 'time', time() - $day]);
        ServerRequest::deleteAll(['<', 'log_time', time() - ($day * 2)]);
    }

    public function actionShow()
    {
        $dbStatisticValues = Yii::$app->monitoringDb->calculate();

        echo VarDumper::dumpAsString($dbStatisticValues);
    }
}