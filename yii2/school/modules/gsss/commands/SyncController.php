<?php

namespace app\commands;

use app\models\ActionsRange;
use app\models\Certificates;
use app\models\CertificatesDocuments;
use backend\components\Controller;
use backend\models\PaymentSystemsForm;
use common\extensions\KendoAdapter\KendoDataProvider;
use common\models\Config;
use common\models\Currency;
use common\models\CurrencyLog;
use common\models\Language;
use common\models\language\Category;
use common\models\language\Message;
use common\models\language\SourceMessage;
use common\models\Orders;
use common\models\PaymentBitCoin;
use common\models\PaymentSystem;
use common\models\rbac\AuthAssignment;
use common\models\rbac\AuthItemChild;
use common\services\Debugger;
use cs\services\Str;
use frontend\base\JsonController;
use GuzzleHttp\Query;
use Yii;
use yii\base\Exception;
use yii\db\Connection;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\helpers\VarDumper;
use yii\httpclient\Client;
use yii\web\HttpException;
use common\models\User;
use common\extensions\KendoAdapter\KendoFiltersCollection;
use app\models\CrmLog;
use common\models\Billing;
use common\models\Banners;
use app\models\UserPaymentAccountsTypes;
use common\models\SmartresponderHistory;
use yii\web\Request;


/**
 * Выполняет воссоздание базы данных с оригинала
 *
 * Class SyncController
 * @package console\controllers
 */
class SyncController extends \cs\base\ConsoleController
{
    public $origin = 'https://www.galaxysss.com';

    public $key = 'YIslvjZCMicGwFiN7ClX762lusQ0451M';

    public $updateData = [
        'bigData' => [
            'certificates',
            'user',
            'currency',
            'orders',       // currency_id
            'billing',      // amoumnt_currency_id
            'transaction',  // transaction.billing_id = billing.id
        ],
        'other'   => [
//            'actions_range',
//            'auth_assignment',
//            'auth_item',
//            'auth_item_child',
//            'auth_rule',
//            'banners',
//            'bonuses',
//            'brandshop_categories',
//            'brandshop_delivery',
//            'brandshop_delivery_contacts',
//            'brandshop_items',
//            'brandshop_pages',
//            'brandshop_users',
//            'certificate_delivery_document',
//            'certificate_delivery_order',
//            'certificate_delivery_order_transaction',
//            'certificates_agreement_statuses',
//            'certificates_documents',
//            'certificates_statuses',
//            'certificates_transfer_act',
//            'certificates_transfer_act_status',
//            'cities',
//            'city_type',
//            'compensation_documents',
//            'compensation_request',
//            'configs',
//            'conversations',
//            'conversations_to_users',
//            'orders',
//            'orders_detail',
//            'packet_description',
//            'packet_properties',
//            'packets',
//            'payment_systems',
//            'payments_bit_coin',
//            'promo_codes3',
//            'safety_ips_data',
//            'safety_lists',
//
//            'countries',
//
//            'source_message',
//            'message',
//            'languages',
//            'languages_access',
//            'languages_category_tree',
//
//            'user_personal_data',
//            'user_personal_data_document',
//
//            'regions',
//            'street_type',
//
//            'jurmala',
//            'login_history',
//            'mail_sender_groups',
//            'mail_sender_receivers',
//            'mail_sender_types',
//
//            'usertype',
//            'usertype_percent',
//            'usertype_rules',
//            'vouchers',
//            'identity_document_type',
//
//            // с зависимостями
//            'certificates',
//            'crm_log',
//            'user',
            'news',
            'news_text',
        ],
    ];

    public function actionUpdate()
    {
        $t1 = microtime(true);

        $options = [
            'isReCreateTable' => true,
            'itemsPerPage'    => 100,
        ];
        $options['key'] = $this->key;
        $except = [
//            'bog_counter',
//            'bog_pictures',
//            'bog_pictures_tree',
//            'bog_shop_payments',
//            'bog_shop_product',
//            'bog_shop_requests',
//            'bog_shop_requests_messages',
//            'bog_shop_requests_tickets',
//            'graal_in_requests',
//            'graal_in_requests_messages',
//            'gs_arayampa_article',
//            'gs_arayampa_packets',
//            'gs_arayampa_requests',
//            'gs_arayampa_requests_messages',
//            'gs_article_list',
//            'gs_article_tree',
//            'gs_blog',
//            'gs_blog_tree',
//            'gs_channeling_investigator',
//            'gs_chat_message',
//            'gs_chat_view',
//            'gs_cheneling_list',
//            'gs_cheneling_list_author_id',
//            'gs_cheneling_list_power_id',
//            'gs_cheneling_tree',
//            'gs_comments',
//            'gs_comments_cache',
//            'gs_config',
//            'gs_country_list',
//            'gs_events',
//            'gs_events_messages',
//            'gs_events_price_list',
//            'gs_events_requests',
//            'gs_events_status',
//            'gs_events_tickets',
//            'gs_events_tickets_link',
//            'gs_gods',
//            'gs_hd',
//            'gs_hd_gen_keys',
//            'gs_hd_town',
        ];
        $tables = Yii::$app->dbWallet->schema->getTableNames();
        $client = new Client(['baseUrl' => $this->origin]);
        if (count($tables) > 0) {
            foreach ($tables as $table) {
                if (!in_array($table, $except)) {
                    $t = microtime(true);
                    $this->clearTable($table);
                    $this->updateTable($client, $table, $options);
                    self::logTime($table . ' ' . 'success. t = ', $t);
                }
            }
        } else {
            $response = $client->get('sync/tables')->send();
            $ret = $response->data;
            if ($ret['status']) {
                $data = $ret['data'];
                foreach ($data as $table) {
                    if (!in_array($table, $except)) {
                        $t = microtime(true);
                        $this->clearTable($table);
                        $this->updateTable($client, $table, $options);
                        self::logTime($table . ' ' . 'success. t = ', $t);
                    }
                }
            } else {
                throw new Exception('нет данных');
            }
        }

        self::successTime('All done. t = ', $t1);
    }

    /**
     * Загружает все даннные в таблицу
     *
     * @param \yii\httpclient\Client $client
     * @param string $name название таблицы
     */
    private function updateTable($client, $name, $options)
    {
        ini_set('memory_limit', -1);
        $page = 1;
        $c = 0;
        do {
            try {
                $t = microtime(true);
                // получаю порцию данных
                $response = $client->get('sync/table', [
                    'name'         => $name,
                    'itemsPerPage' => $options['itemsPerPage'],
                    'page'         => $page,
                    'db'           => 'dbWallet',
                ])->send();
                $ret = ArrayHelper::toArray(json_decode(Str::sub($response->content, 1)));
                if ($ret['success']) {
                    $data = $ret['success'];
                    $columns = array_keys($data['columns']);
                    if (count($data['rows']) > 0) {
                        (new \yii\db\Query())->createCommand(Yii::$app->dbWallet)->batchInsert($name, $columns, $data['rows'])->execute();
                    }
                    $page++;
                    if (count($data['rows']) == $options['itemsPerPage']) {
                        $c += $options['itemsPerPage'];
                        self::logTime($c . ' t = ', $t);
                    } else {
                        $c += count($data['rows']);
                        self::logTime($c . ' t = ', $t);
                    }
                } else {
                    self::logDanger('Error while import table = ' . $name);
                    break;
                }
            } catch (\Exception $e) {
                $c += $options['itemsPerPage'];
                self::logDanger('Error ' . $e->getMessage());
                break;
            }
        } while (count($data['rows']) == $options['itemsPerPage']);
    }

    /**
     * Очищает таблицу
     *
     * @param string $name название таблицы
     *
     * @return int кол-во удаленных строк
     */
    private function clearTable($name)
    {
        $n = (new \yii\db\Query())->createCommand(Yii::$app->dbWallet)->delete($name)->execute();
        self::log('Clear complete. Table = ' . $name);

        return $n;
    }

    /**
     * Удаляет таблицу если существует
     * Создает таблицу
     * @param \yii\db\Connection $db
     * @param array $data
     * [
     *
     * ]
     *
     * @return bool
     */
    public function createTable($db, $data)
    {
        $tables = $db->schema->getTableNames();

        if (true) {
            // очищаю таблицу
            (new \yii\db\Query())->createCommand($db)->delete($data['name'], null)->execute();
        } else {
            // удаляю таблицу
            if (in_array($data['name'], $tables)) {
                (new \yii\db\Query())->createCommand($db)->dropTable($data['name'])->execute();
            }
            // создаю таблицу
        }

        return true;
    }

    /**
     * Удаляет не нужные колонки
     * Создает недостающие
     *
     * @param \yii\db\Connection $db
     * @param array $data
     * [
     *
     * ]
     *
     * @return bool
     */
    private function syncColumns($db, $data)
    {

        return true;
    }

    public function getTables()
    {

    }

    public function actionUpdateFolders()
    {
        $folders = [
            '/verificate_scans',
            '/cabinet/web/uploads/user_images',
            '/cabinet/web/uploads/banners',
        ];

        foreach ($folders as $folder) {
            $this->updateFolder($folder);
        }
    }

    /**
     * Затягивает папку себе
     *
     * @param string $folder путь к папке от корня проекта, например `/cabinet/web/uploads/banners`
     */
    private function updateFolder($folder)
    {
        $client = new Client(['baseUrl' => $this->origin]);

        $page = 1;
        $c = 0;
        $t = microtime(true);
        // получаю порцию данных
        $response = $this->send('sync/folder', [
            'path' => $folder,
        ]);
        $ret = $response->data;
        if ($ret['status']) {
            $data = $ret['data'];
            $files = $data;
            $c = count($files);
            for ($i = 0; $i < $c; $i++) {
                $this->getFile($folder, $i);
            }
        } else {
            self::logDanger('Error. Coudnt import folder = ' . $folder);
            return;
        }
    }

    /**
     * Получает файл и сохранаяет в файловую систему
     *
     * @param $folder
     * @param $index
     */
    private function getFile($folder, $index)
    {
        $response = $this->send('sync/file', [
            'path'   => $folder,
            'offset' => $index,
        ]);
        $ret = $response->data;
        if ($ret['status']) {
            /**
             * {
             *      'name':     string - только имя
             *      'content':  array (base64) - закодированный в Base64 данные файла разбитые на строки по 1000 символов
             * }
             */
            $data = $ret['data'];
            $name = $data['name'];
            $contentArray = $data['content'];
            $contentString = '';
            foreach ($contentArray as $row) {
                $contentString .= $row;
            }
            $content = base64_decode($contentString);
            $path = $folder . '/' . $name;
            $fullPath = Yii::getAlias('@cabinet/..' . $path);
            if (file_exists($fullPath)) {
                unlink($fullPath);
            }
            file_put_contents($fullPath, $content);
            $this->log('file created = ' . $path);
        } else {
            self::logDanger('Error. Coudnt get fileIndex = ' . $index);
            return;
        }
    }

    private function send($action, $data)
    {
        $client = new Client(['baseUrl' => $this->origin]);
        $data['key'] = $this->key;

        return $client->get($action, $data)->send();
    }
}
