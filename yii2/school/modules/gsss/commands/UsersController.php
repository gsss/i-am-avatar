<?php

namespace app\commands;

use app\models\Piramida\InRequest;
use app\models\SubscribeMailItem;
use app\models\User;
use cs\services\SitePath;
use yii\base\Exception;
use yii\console\Controller;
use yii\console\Response;
use yii\db\Query;
use yii\helpers\ArrayHelper;
use yii\helpers\VarDumper;
use yii\imagine\Image;

/**
 * Занимается обслуживанием авторизации
 */
class UsersController extends \cs\base\ConsoleController
{
    /**
     * Делает рассылку писем из списка рассылки
     */
    public function actionClearAvatars()
    {
        $rows = User::query(['not', ['avatar' => null]])
            ->select([
                'id',
                'avatar',
            ])
            ->all();
        foreach ($rows as $row) {
            $user = new User($row);
            if ($row['avatar'] != '') {
                $file = new SitePath($row['avatar']);
                try {
                    $i = Image::getImagine()->open($file->getPathFull());
                } catch (\Exception $e) {
                    $this->log($file->getPath());
                }
            }
        }
    }

    /**
     *  Проставляет всем пользователям аватарки исходя из наличия файла в системе
     */
    public function actionRemake()
    {
        $ids = User::query()
            ->select([
                'id',
            ])
            ->column();
        foreach ($ids as $id) {
            $id = str_repeat('0', (8 - strlen($id))) . $id;
            $isChange = false;
            $file = new SitePath('/upload/FileUpload2/gs_users/' . $id  . '/small/avatar.jpg');

            if (file_exists($file->getPathFull())) {
                $path = $file->getPath();
                $isChange = true;
            }
            $file = new SitePath('/upload/FileUpload2/gs_users/' . $id  . '/small/avatar.png');
            if (file_exists($file->getPathFull())) {
                $path = $file->getPath();
                $isChange = true;
            }
            $file = new SitePath('/upload/FileUpload2/gs_users/' . $id  . '/small/avatar');
            if (file_exists($file->getPathFull())) {
                $path = $file->getPath();
                $isChange = true;
            }
            $file = new SitePath('/upload/FileUpload3/gs_users/' . $id  . '/small/avatar.jpg');
            if (file_exists($file->getPathFull())) {
                $path = $file->getPath();
                $isChange = true;
            }
            $file = new SitePath('/upload/FileUpload3/gs_users/' . $id  . '/small/avatar.png');
            if (file_exists($file->getPathFull())) {
                $path = $file->getPath();
                $isChange = true;
            }
            $file = new SitePath('/upload/FileUpload3/gs_users/' . $id  . '/small/avatar');
            if (file_exists($file->getPathFull())) {
                $path = $file->getPath();
                $isChange = true;
            }
            if ($isChange) {
                (new Query())
                    ->createCommand()
                    ->update('gs_users',
                        ['avatar' => $path],
                        ['id' => $id])
                    ->execute()
                ;
                $this->log('Updated id = ' . $id . ' $path = ' . $path);
            }
        }
    }
}
