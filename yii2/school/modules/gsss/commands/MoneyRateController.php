<?php

namespace app\commands;

use common\models\Config;
use cs\services\VarDumper;
use yii\console\Controller;
use \common\models\User;
use \common\models\PartnerPeriodsInfo;
use \common\models\PartnerPeriods;
use yii\helpers\Json;
use yii\httpclient\Client;

/**
 * Test controller
 */
class MoneyRateController extends \cs\base\ConsoleController
{
    const RATE_BTC_USD = 'rate.BTC/USD';
    const RATE_BTC_RUB = 'rate.BTC/RUB';

    public function actionUpdate()
    {
        // Обновляю курсы для USD EUR
        $url = 'https://min-api.cryptocompare.com';

        $client = new \yii\httpclient\Client(['baseUrl' => $url]);
        $response = $client->get('data/price', [
            'fsym'  => 'USD',
            'tsyms' => 'RUB,EUR',
        ])->send();
        /** @var array $data
         * {
         * "RUB": 58.69,
         * "EUR": 0.8333
         * }
         */
        $data = \yii\helpers\Json::decode($response->content);

        $usd = $data['RUB'];

        $provider = new \app\modules\BitFinex\BitFinex();
        $btc = $provider->getBtc();

        $btcrub = $btc['mid'] * $usd;
        Config::set(self::RATE_BTC_USD, $btc['mid']);
        Config::set(self::RATE_BTC_RUB, $btcrub);

        self::log('btc = ' . $btcrub . ' RUB');
        self::log('btc = ' . $btc['mid'] . ' USD');

        // сохраняю в БД
        (new \common\models\piramida\BitCoinKurs([
            'time' => time(),
            'rub'  => $btcrub,
            'usd'  => $btc['mid'],
        ]))->save();
    }

}