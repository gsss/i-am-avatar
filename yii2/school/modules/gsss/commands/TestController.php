<?php

namespace app\commands;

use app\models\Investigator;
use app\models\SubscribeMailItem;
use app\services\investigator\Collection;
use cs\Application;
use yii\console\Controller;
use yii\console\Response;
use yii\helpers\ArrayHelper;
use yii\helpers\VarDumper;

/**
 * Занимается тестированием
 */
class TestController extends Controller
{

    /**
     * Делает рассылку писем из списка рассылки
     */
    public function actionIndex($isEcho = 1)
    {
        \cs\services\VarDumper::dump(\Yii::$app->requestedParams);
        $this->isEcho = $isEcho;
        $items = [];
        $list = Collection::getList();
        $c = $this->getIndex();
        $c++;

        if ($c >= count($list)) {
            $c = 0;
        }
        $item = $list[ $c ];
        $class = $item['class'];
        self::log('класс = ', $class);
        /** @var \app\services\investigator\InvestigatorInterface $class */
        $class = new $class();
        $className = $class->className();
        try {
            $new = $class->getNewItems();
            foreach ($new as $i) {
                $i['class'] = $className;
                $i['id'] = $c;
                $items[] = $i;
            }
        } catch (\Exception $e) {

        }

        if (count($items) > 0) {
            Application::mail('god@galaxysss.ru', 'Появились новые послания', 'new_channeling', [
                'items' => $items
            ]);

            self::log('новые = ', $items);
        } else {
            self::log('Нет ничего');
        }

        // доавляю
        {
            // получаю какие есть
            $existList = Investigator::query([
                'class_name' => $className,
                'status'     => \app\models\Investigator::STATUS_NEW,
            ])->select('url')->column();
            // добавляю свежие
            $rows = [];
            foreach ($items as $i) {
                if (!in_array($i['url'], $existList)) {
                    $rows[] = [
                        $i['class'],
                        $i['url'],
                        $i['name'],
                        time(),
                        \app\models\Investigator::STATUS_NEW
                    ];
                }
            }
            // добавляю в БД
            if (count($rows) > 0) {
                Investigator::batchInsert([
                    'class_name',
                    'url',
                    'name',
                    'date_insert',
                    'status',
                ], $rows);
            }
        }

        $this->setIndex($c);
    }

    public function actionSession()
    {
        $arr = ini_get_all();
        $v = ArrayHelper::getValue($arr, 'session.save_path', null);
        if (is_null($v)) {
            return;
        }
        if ($v['local_value'] != '/var/www/galaxysss/data/mod-tmp') {
            Application::mail('sotis@sotis.org', 'На GALAXYSSS.COM не работает SESSION', 'test/session', []);
        }
    }
}
