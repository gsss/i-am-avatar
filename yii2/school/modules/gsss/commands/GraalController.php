<?php

namespace app\commands;

use app\models\Piramida\InRequest;
use app\models\Piramida\Wallet;
use app\models\SubscribeMailItem;
use app\modules\Graal\components\Core;
use yii\console\Controller;
use yii\console\Response;
use yii\helpers\ArrayHelper;
use yii\helpers\VarDumper;

/**
 * Проект Грааль
 */
class GraalController extends \app\common\base\Controller
{
    /**
     * Делает пересылку денег всем учредителям и всему командному составу
     */
    public function actionIndex()
    {
        /** @var \app\modules\Graal\components\Core $graal */
        $graal = \Yii::$app->graal;
        $walletUchreditel = $graal->getWallet(Core::TYPE_UCHREDITEL);
        $uchreditelArray = \Yii::$app->authManager->getUserIdsByRole('graal-uchreditel');
        $sum = round($walletUchreditel->value / count($uchreditelArray), 2, PHP_ROUND_HALF_DOWN);
        self::log('Начинаю переводить для учредителей ...');
        foreach($uchreditelArray as $id) {
            $walletUchreditel->move([
                'user_id' => $id,
                'type'    => Wallet::TYPE_GRALL
            ], $sum, [
                'transaction' => 'Перевод для учредителя за ' . date('m.Y'),
                'from'        => 'Перевод для учредителя за ' . date('m.Y') . ' в пользу ' . $id,
                'to'          => 'Перевод для учредителя за ' . date('m.Y'),
            ]);
            self::log('Перевел для uid = '. $id . ' sum = ' . $sum);
        }
        self::logSuccess('Завершил раздачу. В кошельке осталось = ' . \Yii::$app->formatter->asDecimal($walletUchreditel->value,2));

        self::log('Начинаю переводить для командного состава ...');
        $walletCommand = $graal->getWallet(Core::TYPE_COMMAND);
        $commandArray = \Yii::$app->authManager->getUserIdsByRole('graal-command');
        $sum = round($walletCommand->value / count($commandArray), 2, PHP_ROUND_HALF_DOWN);
        foreach($commandArray as $id) {
            $walletCommand->move([
                'user_id' => $id,
                'type'    => Wallet::TYPE_GRALL
            ], $sum, [
                'transaction' => 'Перевод для командного состава за ' . date('m.Y'),
                'from'        => 'Перевод для командного состава за ' . date('m.Y') . ' в пользу ' . $id,
                'to'          => 'Перевод для командного состава за ' . date('m.Y'),
            ]);
            self::log('Перевел для uid = '. $id . ' sum = ' . $sum);
        }
        self::logSuccess('Завершил раздачу. В кошельке осталось = ' . \Yii::$app->formatter->asDecimal($walletCommand->value,2));
    }


}
