<?php

namespace school\modules\gsss\controllers;

use app\models\Article;
use app\models\SiteUpdate;
use app\services\Subscribe;
use cs\services\VarDumper;
use cs\web\Exception;
use Yii;
use yii\base\Event;
use yii\base\UserException;
use \app\models\Penalty;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;

class AdminPenaltyController extends AdminBaseController
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@', 'agency-vozdayanie2'],
                    ],
                ],
            ],
        ];
    }

    public function actionIndex()
    {
        return $this->render();
    }

    public function actionAdd()
    {
        Event::on(Penalty::className(), Penalty::EVENT_BEFORE_INSERT, function($event) {
            /** @var \app\models\Penalty $p */
            $p = $event->sender;
            $p->date_insert = time();
            $p->user_id = Yii::$app->user->id;

            return true;
        });
        
        $model = new Penalty();
        if ($model->load(Yii::$app->request->post()) && $model->insert()) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        } else {
            return $this->render([
                'model' => $model,
            ]);
        }
    }

    public function actionEdit($id)
    {
        /** @var \app\models\Penalty $model */
        $model = Penalty::findOne($id);
//        $model->load(Yii::$app->request->post());
//        $model->validate();
//        VarDumper::dump($model->errors);
        if ($model->load(Yii::$app->request->post()) && $model->update()) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        } else {
            return $this->render([
                'model' => $model,
            ]);
        }
    }

    public function actionDelete($id)
    {
        Penalty::findOne($id)->delete();

        return self::jsonSuccess();
    }
}
