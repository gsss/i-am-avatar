<?php

namespace school\modules\gsss\controllers;

use app\models\Article;
use app\models\Shop\TreeNodeGeneral;
use app\models\SiteUpdate;
use app\services\Subscribe;
use cs\services\VarDumper;
use cs\web\Exception;
use Yii;
use yii\base\UserException;
use yii\helpers\ArrayHelper;
use app\models\Shop\TreeNodeLink;

class AdminShopCatalogController extends AdminBaseController
{

    public function actionIndex()
    {
        return $this->render([]);
    }

    public function actionSort()
    {
        return $this->render([]);
    }

    /**
     * @param int $id идентификатор категории в которой нужно отсортировать ветви
     * @return string
     * @throws \cs\web\Exception
     */
    public function actionSortCategory($id)
    {
        $treeNode = TreeNodeGeneral::findOne($id);
        if (is_null($treeNode)) {
            throw new Exception('Нет такой ветки');
        }
        return $this->render('sort-category', [
            'treeNode' => $treeNode
        ]);
    }

    /**
     * AJAX
     * Обновлет сортировочный индекс
     * REQUEST:
     * - ids - array - идентификаторы веток в порядке сортировки
     *
     * @param int $id родительская ветка
     *
     * @return \yii\web\Response
     */
    public function actionSortAjax($id)
    {
        $c = 1;
        foreach (self::getParam('ids') as $id) {
            TreeNodeGeneral::updateAll(['sort_index' => $c], ['id' => $id]);
            $c++;
        }

        return self::jsonSuccess();
    }

    /**
     * AJAX
     * удаляет указанную ветку
     *
     * @param int $id идентификатор ветки для удаления
     *
     * @return \yii\web\Response
     */
    public function actionDelete($id)
    {
        /** @var \app\models\Shop\TreeNodeGeneral $treeNode */
        $treeNode = TreeNodeGeneral::findOne($id);
        $ids = $treeNode->getAllChilds();
        $ids = ArrayHelper::merge([$treeNode->id], $ids);
        TreeNodeLink::deleteAll(['in', 'tree_node_id', $ids]);
        TreeNodeGeneral::deleteAll(['in', 'id', $ids]);

        return self::jsonSuccess($ids);
    }

    public function actionAdd()
    {
        $model = new TreeNodeGeneral();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $model->save();
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        } else {
            return $this->render([
                'model' => $model,
            ]);
        }
    }

    public function actionEdit($id)
    {
        /** @var \app\models\Shop\TreeNodeGeneral $model */
        $model = TreeNodeGeneral::findOne($id);
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $model->save();
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        } else {
            return $this->render([
                'model' => $model,
            ]);
        }
    }
}
