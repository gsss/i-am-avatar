<?php

namespace school\modules\gsss\controllers;

use app\services\Subscribe;
use cs\services\VarDumper;
use cs\web\Exception;
use Yii;
use yii\base\UserException;
use yii\filters\AccessControl;
use yii\helpers\Url;
use yii\web\Controller;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use app\models\NewsItem;
use app\models\Chenneling;
use cs\base\BaseController;
use yii\web\Response;
use yii\widgets\ActiveForm;
use app\models\SiteUpdate;

class EditorChannelingController extends EditorBaseController
{
    public function actionIndex()
    {
        return $this->render([]);
    }

    /**
     * AJAX
     * Добавляет site_update
     * Делает рассылку
     *
     * @param integer $id - идентификатор послания
     *
     * @return string
     */
    public function actionSubscribe($id)
    {
        $item = Chenneling::find($id);
        if (is_null($item)) {
            return self::jsonErrorId(101, 'Не найдено послание');
        }
        Subscribe::add($item);
        SiteUpdate::add($item);
        $item->update(['is_added_site_update' => 1]);

        return self::jsonSuccess();
    }

    public function actionAdd()
    {
        $model = new \app\models\Form\Chenneling();

        if ($model->load(Yii::$app->request->post()) && ($item = $model->insert())) {
            Yii::$app->session->setFlash('contactFormSubmitted', $item->getId());
            \app\models\Chenneling::clearCache();

            return $this->refresh();
        } else {
            return $this->render([
                'model' => $model,
            ]);
        }
    }

    public function actionAddFromPage()
    {
        $model = new \app\models\Form\ChennelingFromPage();
        if ($model->load(Yii::$app->request->post()) && $model->insert()) {
            Yii::$app->session->setFlash('contactFormSubmitted');
            \app\models\Chenneling::clearCache();

            return $this->refresh();
        } else {
            return $this->render('add-from-page', [
                'model' => $model,
            ]);
        }
    }

    public function actionEdit($id)
    {
        $model = \app\models\Form\Chenneling::find($id);
        $model->is_add_image = false;
        if ($model->load(Yii::$app->request->post()) && $model->update()) {
            Yii::$app->session->setFlash('contactFormSubmitted');
            \app\models\Chenneling::clearCache();

            return $this->refresh();
        } else {
            return $this->render([
                'model' => $model,
            ]);
        }
    }

    public function actionDelete($id)
    {
        $model = \app\models\Form\Chenneling::find($id);
        $model->delete();

        return self::jsonSuccess();
    }
}
