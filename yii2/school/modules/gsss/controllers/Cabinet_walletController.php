<?php

namespace school\modules\gsss\controllers;

use app\models\Form\PiramidaSend;
use app\models\Form\Union;
use app\models\HD;
use app\models\HdGenKeys;
use app\models\HDtown;
use app\models\Piramida\Billing;
use app\models\Piramida\InRequest;
use app\models\Piramida\OutRequest;
use app\models\Piramida\Wallet;
use app\models\Piramida\WalletSource\Yandex;
use app\models\SiteUpdate;
use app\models\User;
use app\services\Subscribe;
use common\payment\BitCoinBlockTrailPayment;
use cs\Application;
use cs\services\SitePath;
use cs\services\VarDumper;
use cs\web\Exception;
use Yii;
use yii\base\UserException;
use yii\bootstrap\Html;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;
use yii\helpers\Url;
use yii\web\Controller;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use app\models\NewsItem;
use cs\base\BaseController;
use yii\web\Response;
use yii\widgets\ActiveForm;

class Cabinet_walletController extends BaseController
{
    public $layout = 'menu';

    public function actionGetQrCode()
    {
        /** @var \app\models\User $user */
        $user = \Yii::$app->user->identity;
        $address = $user->getBitCoinWallet()->address;
        Yii::setAlias('@Endroid/QrCode', '@vendor/endroid/QrCode/src');
        $content = (new \Endroid\QrCode\QrCode())
            ->setText('bitcoin:' . $address . '?url=avatar-bank.com' )
            ->setSize(200)
            ->setPadding(10)
            ->setErrorCorrection('high')
            ->setForegroundColor(['r' => 0, 'g' => 0, 'b' => 0, 'a' => 0])
            ->setBackgroundColor(['r' => 255, 'g' => 255, 'b' => 255, 'a' => 0])
            ->setLabelFontSize(16)
            ->setImageType(\Endroid\QrCode\QrCode::IMAGE_TYPE_PNG)
            ->get('png');

        return Yii::$app->response->sendContentAsFile($content, $address.'.png', ['mimeType' => 'image/png']);
    }

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    public function init()
    {
        if (\yii\helpers\ArrayHelper::getValue(Yii::$app->params, 'isTransfere', false) == true) {
            if ($this->action != 'logout') {
                throw new Exception(Yii::$app->params['isTransfere_string']);
            }
        }
    }

    public function actions()
    {
        return [
            'transactions-list' => [
                'class' => 'app\controllers\actions\Cabinet_wallet\TransactionsList'
            ]
        ];
    }

    /**
     * AJAX
     * Создает кошелек
     *
     * @return Response
     */
    public function actionCreate()
    {
        $provider = new BitCoinBlockTrailPayment();
        $id = 'user_' . Yii::$app->user->id;
        $password = 'i-am-avatar';
        /** @var \Blocktrail\SDK\WalletInterface $wallet */
        list($wallet, $primaryMnemonic, $backupMnemonic, $blocktrailPublicKeys) = $provider->getClient()->createNewWallet($id, $password);
        $address = $wallet->getNewAddress();
        /** @var \app\models\User $user */
        $user = Yii::$app->user->identity;
        $walletConfig = $user->getBitCoinWallet();
        $walletConfig->config = json_encode([
            'id'                   => $id,
            'password'             => $password,
            'address'              => $address,
            'primaryMnemonic'      => $primaryMnemonic,
            'backupMnemonic'       => $backupMnemonic,
            'blocktrailPublicKeys' => \yii\helpers\VarDumper::dumpAsString($blocktrailPublicKeys),
            'backup'               => \yii\helpers\VarDumper::dumpAsString($wallet),
        ]);
        $walletConfig->address = $address;
        $walletConfig->identity = $id;
        $walletConfig->password = $password;
        $walletConfig->setPin('0000');
        $ret = $walletConfig->save();
        if (!$ret) {
            return self::jsonErrorId(101, 'Не удалось сохранить ' . \yii\helpers\VarDumper::dumpAsString($walletConfig->errors));
        }

        return self::jsonSuccess();
    }

    /**
     * AJAX
     * Отправляет деньги
     * REQUEST:
     * - address - string - адрес
     * - amount - string - сколько (разделитель - точка)
     * - comment - string
     * - password - string
     *
     * @return Response
     */
    public function actionSend()
    {
        $model = new PiramidaSend();
        $model->load(Yii::$app->request->post(), '');
        if (!$model->validate()) {
            return self::jsonErrorId(101, $this->convert($model->errors));
        }
        $transaction = $model->send();
        if ($transaction === false) {
            return self::jsonErrorId(101, $this->convert($model->errors));
        }

        return self::jsonSuccess([
            'address' => $transaction,
        ]);
    }

    public function actionKurs()
    {
        return $this->render();
    }

    /**
     * @param array $params
     * [
     *      'amount' => ['Не верное число',...],
     * ]
     * @return array
     * [
     *      'name'  => 'amount',
     *      'value' => ['Не верное число',...],
     * ]
     */
    private function convert($params)
    {
        $ret = [];
        foreach($params as $name => $arr) {
            $ret[] = [
                'name' => $name,
                'value' => $arr,
            ];
        }
        return $ret;
    }

    public function actionIn()
    {
        return $this->render();
    }


    public function actionIndex()
    {
        return $this->render();
    }

    /**
     * REQUEST:
     * - phone: string
     *
     * @return Response
     */
    public function actionPhoneEditSave()
    {
        $phone = self::getParam('phone');
        /** @var \app\models\User $user */
        $user = Yii::$app->user->identity;
        $user->update(['phone_invite' => $phone]);

        return self::jsonSuccess();
    }

    public function actionPhoneEdit()
    {
        return $this->render('phone-edit');
    }

    /**
     * Выдает на скачивание Билет на новую Землю
     *
     * @return $this
     * @throws \yii\web\HttpException
     */
    public function actionReferalDocument()
    {
        $contentHtml = $this->renderFile('@app/views/cabinet_wallet/_referal-document.php', ['user' => Yii::$app->user->identity]);

        require \Yii::getAlias('@vendor/mpdf/mpdf/src/Mpdf.php');
        $pdfDriver = new \Mpdf\Mpdf();
        $pdfDriver->WriteHTML($contentHtml);
        $contentPdf = $pdfDriver->Output('', 'S');

        return Yii::$app->response->sendContentAsFile($contentPdf, 'ticket_to_the_new_earth.pdf');
    }



    public function actionMove()
    {
        $model = new \app\models\Form\WalletMove();

        if (Yii::$app->request->isAjax) {
            if ($model->load(Yii::$app->request->post())) {
                Yii::$app->response->format = Response::FORMAT_JSON;
                return ActiveForm::validate($model);
            }
        }

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $model->move();
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        } else {
            return $this->render([
                'model' => $model,
            ]);
        }
    }

    public function actionOut()
    {
        $model = new \app\models\Form\WalletOut();

        if (Yii::$app->request->isAjax) {
            if ($model->load(Yii::$app->request->post())) {
                Yii::$app->response->format = Response::FORMAT_JSON;
                return ActiveForm::validate($model);
            }
        }

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $model->makeRequest();
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        } else {
            return $this->render([
                'model' => $model,
            ]);
        }
    }

    public function actionOutRequests()
    {
        return $this->render('out-requests', []);
    }

    /**
     * Отображает одну заявку на вывод средств
     *
     * @param int $id идентификатор заявки на вывод
     *
     * @return string
     * @throws \cs\web\Exception
     */
    public function actionOutItem($id)
    {
        $request = OutRequest::findOne($id);
        /** @var \app\models\User $user */
        $user = Yii::$app->user->identity;
        if (!in_array($request->wallet_id, [
            $user->getWallet('A')->id,
            $user->getWallet('B')->id,
        ])
        ) {
            throw new Exception('Это не ваш кошелек. Не красиво так делать');
        }

        return $this->render('out-item', [
            'request' => $request,
        ]);
    }

    /**
     * Отправляет сообщение в чат
     *
     * REQUEST:
     * - message- string - сообщение
     *
     * @param int $id идентификатор заявки на вывод
     *
     * @return string
     * @throws \cs\web\Exception
     */
    public function actionOutItemMessage($id)
    {
        $request = OutRequest::findOne($id);
        if (is_null($request)) {
            throw new Exception('Не найден запрос на вывод');
        }
        $message = self::getParam('message');
        $messageObject = $request->addMessageToShop($message);
        Yii::info('$messageObject=' . \yii\helpers\VarDumper::dumpAsString($messageObject), 'gs\\actionOutItemMessage');

        return self::jsonSuccess([
            'html' => $this->renderPartial('@app/views/' . $this->id . '/out-item-message.php', [
                'message' => $messageObject,
                'user'    => \app\services\UsersInCache::find(Yii::$app->user->id),
            ]),
        ]);
    }

    public function actionChilds()
    {
        return $this->render();
    }

    /**
     * AJAX
     * Выдает список друзей у которых указана полная дата рождения
     *
     * @return string json
     *                [ 'friends' => [
     *                  [
     *                      'id' => int
     *                      'id' => int
     *                      'id' => int
     *                      'id' => int
     * ], ...
     * ]]
     *
     */
    public function actionFacebookFriends()
    {
        /** @var \app\services\authclient\VKontakte $client */
        $client = Yii::$app->authClientCollection->clients['facebook'];
        $data = $client->api('me', 'GET');
        VarDumper::dump($data);
        return self::jsonSuccess($data);
    }


    /**
     * AJAX
     *
     * REQUEST:
     * - value - float - сумма для начисления в кошелек (sum_after), с плавающей точкой '.'
     *
     * @return string
     */
    public function actionInAjax()
    {
        $value = self::getParam('value');
        if ($value) {
            if ($value <= 0) {
                return self::jsonErrorId(101, 'Сумма начисления не может быть 0 или отрицательной');
            }
        } else {
            return self::jsonErrorId(101, 'Сумма начисления не может быть 0 или отрицательной');
        }

        /** @var \app\models\User $user */
        $user = Yii::$app->user->identity;
        $billing = Billing::add([
            'wallet_id'  => $user->getWallet()->id,
            'sum_before' => round($value / (1 - (\app\models\Piramida\WalletSource\Yandex::getTax() / 100)), 2),
            'sum_after'  => $value,
            'type'       => Billing::TYPE_IN,
        ]);
        $request = InRequest::add([
            'billing_id' => $billing->id,
        ]);

        return self::jsonSuccess($request->id);
    }

    public function actionInSuccess()
    {
        $id = Yii::$app->request->get('id');
        $request = InRequest::findOne($id);
        if (is_null($request)) {
            return self::jsonErrorId(101, 'Не найден запрос');
        }
        $request->success(new Yandex(['transaction' => [
            'transaction_id' => 13,
            'operation'      => 'IN',
            'amount'         => '123',
        ]]));

        return self::jsonSuccess();
    }

    public function actionReferal_link()
    {
        $fields = [];
        if ($url = self::getParam('url')) {
            $fields['link'] = $url;
        }
        $model = new \app\models\Form\WalletInvite($fields);
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
//            $model->send();
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        }

        return $this->render([
            'model' => $model,
        ]);
    }
}
