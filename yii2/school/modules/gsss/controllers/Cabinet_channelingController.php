<?php

namespace school\modules\gsss\controllers;

use app\models\Article;
use app\models\Chenneling;
use app\models\SiteUpdate;
use app\models\User;
use app\services\Subscribe;
use cs\Application;
use cs\services\VarDumper;
use cs\web\Exception;
use Yii;
use yii\base\UserException;

class Cabinet_channelingController extends CabinetBaseController
{

    public function init()
    {
        if (\yii\helpers\ArrayHelper::getValue(Yii::$app->params, 'isTransfere', false) == true) {
            throw new Exception(Yii::$app->params['isTransfere_string']);
        }
    }

    public function actionIndex()
    {
        return $this->render([]);
    }

    public function actionAdd()
    {
        $model = new \app\models\Form\Cabinet\Channeling();
        if ($model->load(Yii::$app->request->post()) && ($item = $model->insert())) {
            Yii::$app->session->setFlash('contactFormSubmitted', $item->getId());

            return $this->refresh();
        } else {
            return $this->render([
                'model' => $model,
            ]);
        }
    }

    /**
     * AJAX
     * Добавляет site_update
     * Делает рассылку
     *
     * @param integer $id - идентификатор новости
     *
     * @return string
     */
    public function actionSubscribe($id)
    {
        if (Yii::$app->user->identity->get('mode_chenneling_is_subscribe',0) != 1) {
            return self::jsonErrorId(102, 'Вам нельзя делать рассылки');
        }
        $item = Chenneling::find($id);
        if (is_null($item)) {
            return self::jsonError(101, 'Не найдено послание');
        }
        Subscribe::add($item);
        SiteUpdate::add($item);
        $item->update(['is_added_site_update' => 1]);

        return self::jsonSuccess();
    }


    public function actionEdit($id)
    {
        $model = \app\models\Form\Cabinet\Channeling::find($id);
        if ($model->load(Yii::$app->request->post()) && $model->update()) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        } else {
            return $this->render([
                'model' => $model,
            ]);
        }
    }

    public function actionDelete($id)
    {
        \app\models\Form\Cabinet\Channeling::find($id)->delete();

        return self::jsonSuccess();
    }

    public function actionView($id)
    {
        $i = Chenneling::find($id);
        if (is_null($i)) {
            throw new Exception('Нет такого послания');
        }

        return $this->render([
            'item' => $i
        ]);
    }

    /**
     * AJAX
     * Отправка послания на модерацию
     *
     * @param int $id идентификатор послания
     *
     * @return string json
     */
    public function actionSend_moderation($id)
    {
        $ch = Chenneling::find($id);
        if ($ch->getField('user_id') != Yii::$app->user->id) {
            return self::jsonErrorId(103, 'Это не ваше послание');
        }
        if (Yii::$app->user->identity->get('mode_chenneling', 0) == 1) {
            // нужна модерация
            $ch->update(['moderation_status' => 2]);
            foreach(User::getQueryByRole(User::USER_ROLE_MODERATOR)
                        ->select(['email'])
                        ->column() as $moderatorMail) {
                Application::mail($moderatorMail, 'Послание отправлено на модерацию', 'moderator/channeling_mod', [
                    'item'          => $ch,
                    'user'          => Yii::$app->user->identity
                ]);
            }

            return self::jsonSuccess();
        } else {
            return self::jsonSuccess([102, 'Модерация у пользователя отключена']);
        }
    }
}
