<?php

namespace school\modules\gsss\controllers;

use app\models\Article;
use app\models\SiteUpdate;
use app\models\Smeta;
use app\services\Subscribe;
use cs\services\VarDumper;
use cs\web\Exception;
use Yii;
use yii\base\UserException;

class AdminShopController extends AdminBaseController
{

    public function actionMoneyIncome()
    {
        return $this->render();
    }
}
