<?php

namespace school\modules\gsss\controllers;

use app\common\components\Piramida;
use app\models\Form\Event;
use app\models\Piramida\Billing;
use app\models\Piramida\Transaction;
use app\models\Piramida\WalletSource\BitCoin;
use app\models\Piramida\WalletSource\Yandex;
use app\models\Shop\Request;
use app\models\Article;
use app\models\Blog;
use app\models\Form\Shop\Order;
use app\models\Piramida\InRequest;
use app\models\Piramida\Wallet;
use app\models\Praktice;
use app\models\Service;
use app\models\Shop;
use app\models\Shop\Payments;
use app\models\Shop\Product;
use app\models\Shop\TreeNodeGeneral;
use app\models\Union;
use app\models\UnionCategory;
use app\models\User;
use app\modules\Shop\services\Basket;
use common\models\PaySystem;
use common\models\shop\DostavkaItem;
use common\models\shop\PaymentConfig;
use cs\Application;
use cs\services\Str;
use cs\services\VarDumper;
use cs\web\Exception;
use cs\Widget\FileUploadMany\FileUploadMany;
use Yii;
use yii\base\UserException;
use yii\filters\AccessControl;
use yii\grid\DataColumn;
use yii\helpers\ArrayHelper;
use yii\helpers\StringHelper;
use yii\helpers\Url;
use yii\rbac\Role;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use app\models\NewsItem;
use app\models\Chenneling;
use cs\base\BaseController;
use yii\web\HttpException;


class ShopController extends BaseController
{
    public $layout = 'menu';

    public function actionIndex()
    {
        // сохраняю $parent_id
        {
            $parent_id = Yii::$app->request->get('parent_id', null);
            if ($parent_id) {
                Yii::$app->session->set('parent_id', $parent_id);
            }
        }
        return $this->render([
            'tree' => TreeNodeGeneral::getTree(1)
        ]);
    }

    public function actionPartnerProgram()
    {
        return $this->render('partner-program');
    }
    
    public function actionCategory($id)
    {
        // сохраняю $parent_id
        {
            $parent_id = Yii::$app->request->get('parent_id', null);
            if ($parent_id) {
                Yii::$app->session->set('parent_id', $parent_id);
            }
        }
        $treeNode = TreeNodeGeneral::findOne($id);
        if (is_null($treeNode)) {
            throw new Exception('Не найдена категория');
        }
        $items = Product::query()
            ->select([
                'gs_unions_shop_product.*',
            ])
            ->andWhere(['gs_unions_shop_product.is_deleted' => 0])
            ->andWhere(['gs_unions_shop_product.moderation_status' => 1])
            ->andWhere(['gs_shop_tree_products_link.tree_node_id' => $treeNode->id])
            ->innerJoin('gs_shop_tree_products_link', 'gs_shop_tree_products_link.product_id = gs_unions_shop_product.id')
            ->all();

        return $this->render([
            'treeNode' => $treeNode,
            'items'    => $items,
        ]);
    }

    /**
     * Показывает продукт
     *
     * @param int $id идентификатор продукта
     *
     * @return string
     *
     * @throws Exception
     */
    public function actionProduct($id)
    {
        // сохраняю $parent_id
        {
            $parent_id = Yii::$app->request->get('parent_id', null);
            if ($parent_id) {
                Yii::$app->session->set('parent_id', $parent_id);
            }
        }
        $product = Product::find($id);
        if (is_null($product)) {
            throw new Exception('Не найден продукт');
        }
        if ($product->get('moderation_status', 0) == 0) {
            throw new Exception('Продукт не прошел модерацию');
        }

        return $this->render([
            'product' => $product,
        ]);
    }

    /**
     * Выводит результаты посика товаров
     *
     * REQUEST:
     * - text - string - строка поиска
     *
     * @return string
     *
     * @throws Exception
     */
    public function actionSearch()
    {
        $text = self::getParam('text');

        $items = Product::query()
            ->select([
                'gs_unions_shop_product.*',
            ])
            ->where(['like', 'gs_unions_shop_product.name', $text])
            ->all();

        return $this->render([
            'text'  => $text,
        ]);
    }

    /**
     * AJAX
     * Добавление товара в корзину
     * REQUEST
     * - id - int - идентификатор товара gs_unions_shop_product.id
     *
     * @return string
     */
    public function actionBasket_add()
    {
        $id = self::getParam('id');
        $product = Product::find($id);
        if (is_null($product)) {
            return self::jsonErrorId(101, 'Не найден товар');
        }
        $count = Basket::add($product);
        Yii::$app->session->set('shop.help_window_already_showed', 1);

        return self::jsonSuccess($count);
    }

    /**
     * AJAX
     * Добавление товара в корзину
     *
     * REQUEST:
     * - product - int - идентификатор товара gs_unions_shop_product.id
     * - count - int - кол-во товаров
     *
     * @return string
     */
    public function actionBasket_update()
    {
        Basket::setCount(self::getParam('product'), self::getParam('count'));

        return self::jsonSuccess();
    }

    public function actionSet_no_help_window()
    {
        if (Yii::$app->user->isGuest) {
            return self::jsonErrorId(101, 'Пользователь не авторизован');
        }
        Yii::$app->user->identity->update(['is_show_shop_help_window' => 1]);

        return self::jsonSuccess();
    }

    public function actionBasket()
    {
        return $this->render([
            'items' => Basket::get()
        ]);
    }

    /**
     * Удаляет товар из корзины
     *
     * AJAX
     *
     * REQUEST:
     * - id - integer - идентификатор товара
     *
     * @return string
     */
    public function actionBasket_delete()
    {
        $id = self::getParam('id');
        Basket::delete($id);

        return self::jsonSuccess();
    }

    public function actionOrder()
    {
        if (\yii\helpers\ArrayHelper::getValue(Yii::$app->params, 'isTransfere', false) == true) {
            throw new Exception(Yii::$app->params['isTransfere_string']);
        }
        if (count(Basket::get()) == 0) {
            if (is_null(Yii::$app->request->get('product_id'))) {
                return $this->redirect('/');
            }
        }
        $model = new Order();
        if ($model->load(Yii::$app->request->post()) && $model->add()) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        } else {
            return $this->render([
                'model' => $model,
            ]);
        }
    }

    /**
     * Выдает форму для оплаты заказа
     *
     * @param int $id bog_shop_requests.id
     *
     * @return string|Response
     * @throws \cs\web\Exception
     */
    public function actionOrder_buy($id)
    {
        if (\yii\helpers\ArrayHelper::getValue(Yii::$app->params, 'isTransfere', false) == true) {
            throw new Exception(Yii::$app->params['isTransfere_string']);
        }
        $request = \app\models\Shop\Request::find($id);
        if (is_null($request)) {
            throw new Exception('Нет такого заказа');
        }
        if ($request->getField('is_paid', 0) == 1) return $this->redirect(['site_cabinet/request', 'id' => $request->getId()]);

        return $this->render([
            'request' => $request,
        ]);
    }

    /**
     * Выдает форму для оплаты заказа
     *
     * @param int $id bog_shop_requests.id
     *
     * @return string|Response
     * @throws \cs\web\Exception
     */
    public function actionOrderBuyOkPay($id)
    {
        if (\yii\helpers\ArrayHelper::getValue(Yii::$app->params, 'isTransfere', false) == true) {
            throw new Exception(Yii::$app->params['isTransfere_string']);
        }
        $request = \app\models\Shop\Request::find($id);
        if (is_null($request)) {
            throw new Exception('Нет такого заказа');
        }
        if ($request->getField('is_paid', 0) == 1) return $this->redirect(['site_cabinet/request', 'id' => $request->getId()]);

        return $this->render('order-buy-ok-pay', [
            'request' => $request,
        ]);
    }

    /**
     * AJAX
     *
     * RESPONSE:
     * - login - string
     * - name - string
     *
     * @return array|string|Response
     */
    public function actionRegistration_ajax()
    {
        $login = strtolower(self::getParam('login'));
        $name = self::getParam('name');
        if (!filter_var($login, FILTER_VALIDATE_EMAIL)) {
            return self::jsonErrorId(106, 'Не верный формат email');
        }
        if (User::query(['email' => $login])->exists()) {
            return self::jsonErrorId(101, 'Пользователь уже существует');
        }
        $user = User::registration_password($login, substr(str_shuffle("01234567890123456789"), 0, 4), [
            'name_first' => $name,
        ]);
        if (Yii::$app->session->get('parent_id')) {
            $user->update(['parent_id' => Yii::$app->session->get('parent_id')]);
        }
        Yii::$app->user->login($user);
        Yii::info(\yii\helpers\VarDumper::dumpAsString(Yii::$app->session->get('parent_id')),'gs\\order\\actionRegistration_ajax');

        return self::jsonSuccess([
            'wallet' => [
                'a' => 0,
                'b' => 0,
            ]
        ]);
    }

    /**
     * AJAX
     *
     * RESPONSE:
     * - login - string
     * - password - string
     *
     * @return array|string|Response
     */
    public function actionLogin_ajax()
    {
        $login = strtolower(self::getParam('login'));
        $password = self::getParam('password');
        if (!filter_var($login, FILTER_VALIDATE_EMAIL)) {
            return self::jsonErrorId(106, 'Не верный формат email');
        }
        $user = User::find(['email' => $login]);
        if (is_null($user)) {
            return self::jsonErrorId(101, 'Пользователь не найден');
        }
        if (!$user->validatePassword($password)) {
            return self::jsonErrorId(102, 'Не верный пароль');
        }
        if (Yii::$app->session->get('parent_id')) {
            if (is_null($user->get('parent_id'))) {
                $user->update(['parent_id' => Yii::$app->session->get('parent_id')]);
            }
        }
        Yii::$app->user->login($user);

        return self::jsonSuccess([
            'wallet' => [
                'a' => $user->getWallet('A')->value,
                'b' => $user->getWallet('B')->value,
            ]
        ]);
    }


    /**
     * AJAX
     *
     * REQUEST
     * + mode        - string - 'product' | 'union'
     * + comment     - string - комментарий к заказу
     * + price       - int - полная цена заказа с учетом доставки
     * + phone       - string - телефон
     * - paymentType - int - способ оплаты gs_paysystems.id
     * + wallet      - array -
     *    - a        - double - оплата с кошелька А, может быть пустым
     *    - b        - double - оплата с кошелька B, может быть пустым
     *    - ps       - double - оплата с платежной системы, может быть пустым
     *
     * - id          - int - идентификатор продукта
     * - product_id  - идентификатор продукта,  если указан mode='product'
     * - union_id    - идентификатор объединения, если указан mode='union'
     * - dostavka    - int - идентификатор доставки gs_unions_shop_dostavka.id, не обязательное. Если Все товары электронные то этого поля не будет.
     * - address     - string - адрес доставки
     *
     * @return string|Response
     */
    public function actionOrder_ajax()
    {
        Yii::info(\yii\helpers\VarDumper::dumpAsString(Yii::$app->request->post()), 'gs\\order-ajax');
        // валидирую входные данные
        $model = new \app\models\validate\OrderAjax();
        $model->load(Yii::$app->request->post(), '');
        if (!$model->validate()) {
            Yii::info('validate=false '. \yii\helpers\VarDumper::dumpAsString($model->errors), 'gs\\actionOrder_ajax');
            return self::jsonErrorId(10, $model->errors);
        }

        Yii::info('request='.\yii\helpers\VarDumper::dumpAsString(Yii::$app->request->post()), 'gs\\actionOrder_ajax');
        $mode = self::getParam('mode');
        if ($mode == 'product') {
            $productId = self::getParam('product_id');
            $product = Product::find($productId);
            $fields['union_id'] = $product->get('union_id');
            $productList = [[
                'id'    => $productId,
                'count' => 1,
            ]];
        } else {
            $union_id = self::getParam('union_id');
            $fields['union_id'] = $union_id;
            $productList = Basket::get($union_id);
        }
        // если в сессии есть праемтр parent_id это значит что  пользователь покупает товар по реферальной ссылке, поэтому добавляю его к заказу. Если он будет оплачен то этот родитель получит деньги.
        if ($parent_id = Yii::$app->session->get('parent_id')) {
            $fields['parent_id'] = $parent_id;
        }
        $fields['comment'] = self::getParam('comment');
        $fields['address'] = self::getParam('address', '');
        $fields['dostavka'] = self::getParam('dostavka', null);
        $fields['price'] = self::getParam('price');
        $fields['phone'] = self::getParam('phone');
        $fields['paid_ps'] = $model->wallet['ps'];
        $paymentType = self::getParam('paymentType');

        /** @var \app\common\components\Piramida $piramida */
        $piramida = Yii::$app->piramida;
        $piramidaWallet = $piramida->getWallet();

        if (!is_null($paymentType) && $paymentType != '') {
            /** @var \common\models\PaySystem $paySystem */
            $paySystem = PaySystem::findOne($paymentType);
            $classObject = $paySystem->getClass();

            $sum_before = $classObject->getPriceWithTax($fields['paid_ps']);
            $sum_after  = $fields['paid_ps'];

            // Создаю заявку на оплату
            {
                $price = $model->wallet['ps'];
                if ($price > 0) {
                    $billing = Billing::add([
                        'wallet_id'  => $piramidaWallet->id,
                        'sum_before' => $sum_before,
                        'sum_after'  => $sum_after,
                        'type'       => Billing::TYPE_PAY,
                        'source_id'  => $paymentType,
                    ]);
                    $fields['billing_id'] = $billing->id;
                }
            }
        }

        $request = \app\models\Shop\Request::add($fields, $productList);
        $union = $request->getUnion();
        $shop = $union->getShop();
        $dostavkaList = $shop->getDostavkaRows();
        $dostavkaText = '';
        $isAddress = 0;

        foreach ($dostavkaList as $i) {
            if ($i['id'] == $fields['dostavka']) {
                $dostavkaText = $i['name'];
                $isAddress = $i['is_need_address'];
                break;
            }
        }
        $message = [
            'Стоимость с учетом доставки: ' . Yii::$app->formatter->asDecimal($fields['price']),
            'Доставка: ' . $dostavkaText,
            ($isAddress) ? 'Адрес: ' . $fields['address'] : '',
        ];
        $request->addStatusToClient([
            'status'  => \app\models\Shop\Request::STATUS_ORDER_DOSTAVKA,
            'message' => join("\n", $message),
        ]);

        if (!$request->onlyElectronic()) {
            $dostavka = $request->getDostavka();
            if ($dostavka->type == DostavkaItem::TYPE_NALOZHNII_PLATEZH) {
                $request->addStatusToClient(\app\models\Shop\Request::STATUS_DOSTAVKA_NALOZH_PREPARE);
            }
        }


        // делаю списание денежных средств
        {
            if ($model->wallet) {
                if ($model->wallet['a']) {
                    /** @var \app\models\User $user */
                    $user = Yii::$app->user->identity;
                    $t = $user->getWallet('A')->move($piramidaWallet, $model->wallet['a'], 'Оплата заказа ' . $request->getId(), Transaction::OPERATION_PAY);
                    Shop\RequestTransaction::add([
                        'request_id'     => $request->getId(),
                        'transaction_id' => $t->id,
                        'type'           => Shop\RequestTransaction::TYPE_PAID_WALLET,
                        'sum'            => $model->wallet['a'],
                    ]);
                }
                if ($model->wallet['b']) {
                    /** @var \app\models\User $user */
                    $user = Yii::$app->user->identity;
                    $t = $user->getWallet('B')->move($piramidaWallet, $model->wallet['b'], 'Оплата заказа ' . $request->getId(), Transaction::OPERATION_PAY);
                    Shop\RequestTransaction::add([
                        'request_id'     => $request->getId(),
                        'transaction_id' => $t->id,
                        'type'           => Shop\RequestTransaction::TYPE_PAID_WALLET,
                        'sum'            => $model->wallet['b'],
                    ]);
                }
            }
        }

        // письмо магазину
        $shop->mailToMe('Заказ #' . $request->getId(), 'shop/request_to_shop', [
            'request'    => $request,
        ]);

        // письмо клиенту
        $shop->mail($request->getUser()->getEmail(), 'Заказ #' . $request->getId(), 'shop/request_to_client', [
            'request'    => $request,
        ]);

        // если режим объединения то очищаю корзину
        if ($mode == 'union') {
            Basket::clear($fields['union_id']);
        }

        // если заказ был полностью оплачен с кошельков то исполняю заказ
        if ($model->wallet['ps'] == 0) {
            $request->success();
        }

        return self::jsonSuccess($request->getId());
    }

    /**
     * AJAX
     *
     * REQUEST
     * + mode        - string - 'product' | 'union'
     * + comment     - string - комментарий к заказу
     * + price       - int - полная цена заказа с учетом доставки
     * + phone       - string - телефон
     * - id          - int - идентификатор продукта
     * - product_id  - идентификатор продукта,  если указан mode='product'
     * - union_id    - идентификатор объединения, если указан mode='union'
     * - dostavka    - int - идентификатор доставки gs_unions_shop_dostavka.id, не обязательное. Если Все товары электронные то этого поля не будет.
     * - address     - string - адрес доставки
     *
     * @return string|Response
     */
    public function actionOrderAjaxNalozh()
    {
        // валидирую входные данные
        $model = new \app\models\validate\OrderAjaxNolozh();
        $model->load(Yii::$app->request->post(), '');
        if (!$model->validate()) {
            return self::jsonErrorId(10, $model->errors);
        }

        $mode = self::getParam('mode');
        if ($mode == 'product') {
            $productId = self::getParam('product_id');
            $product = Product::find($productId);
            $fields['union_id'] = $product->get('union_id');
            $productList = [[
                'id'    => $productId,
                'count' => 1,
            ]];
        } else {
            $union_id = self::getParam('union_id');
            $fields['union_id'] = $union_id;
            $productList = Basket::get($union_id);
        }
        // если в сессии есть праемтр parent_id это значит что  пользователь покупает товар по реферальной ссылке, поэтому добавляю его к заказу. Если он будет оплачен то этот родитель получит деньги.
        if ($parent_id = Yii::$app->session->get('parent_id')) {
            $fields['parent_id'] = $parent_id;
        }
        $fields['comment'] = self::getParam('comment');
        $fields['address'] = self::getParam('address', '');
        $fields['dostavka'] = self::getParam('dostavka', null);
        $fields['price'] = self::getParam('price');
        $fields['phone'] = self::getParam('phone');
        $fields['paid_ps'] = 0;

        $sum_before = $fields['price'];
        $sum_after  = $fields['price'];
        $source_id  = PaySystem::findOne(['code' => 'pochta'])->id;

        // Создаю заявку на оплату
        {
            /** @var \app\common\components\Piramida $piramida */
            $piramida = Yii::$app->piramida;
            $piramidaWallet = $piramida->getWallet();
            $price = $fields['price'];
            if ($price > 0) {
                $billing = Billing::add([
                    'wallet_id'  => $piramidaWallet->id,
                    'sum_before' => $sum_before,
                    'sum_after'  => $sum_after,
                    'type'       => Billing::TYPE_PAY,
                    'source_id'  => $source_id,
                ]);
                $fields['billing_id'] = $billing->id;
            }
        }

        $request = \app\models\Shop\Request::add($fields, $productList);
        $union = $request->getUnion();
        $shop = $union->getShop();
        $dostavka = $request->getDostavka();
        $dostavkaText = $dostavka->name;
        $message = [
            'Стоимость с учетом доставки: ' . Yii::$app->formatter->asDecimal($fields['price']),
            'Доставка: ' . $dostavkaText,
            'Адрес: ' . $fields['address'],
        ];
        $request->addStatusToClient([
            'status'  => \app\models\Shop\Request::STATUS_ORDER_DOSTAVKA,
            'message' => join("\n", $message),
        ]);
        $request->addStatusToClient(\app\models\Shop\Request::STATUS_DOSTAVKA_NALOZH_PREPARE);

        // письмо магазину
        $shop->mailToMe('Заказ #' . $request->getId(), 'shop/request_to_shop', [
            'request'    => $request,
        ]);

        // письмо клиенту
        $shop->mail($request->getUser()->getEmail(), 'Заказ #' . $request->getId(), 'shop/request_to_client', [
            'request'    => $request,
        ]);

        // если режим объединения то очищаю корзину
        if ($mode == 'union') {
            Basket::clear($fields['union_id']);
        }

        return self::jsonSuccess($request->getId());
    }


    /**
     * Принимает внешние callback от платежных систем
     * и обрабатывает их
     *
     * @return string
     * @throws \cs\web\Exception
     */
    public function actionOrder_success()
    {
        Yii::info(['get' => Yii::$app->request->get(), 'post' => Yii::$app->request->post()], 'gs\\order\\success');
        \Yii::info(\yii\helpers\VarDumper::dumpAsString(\Yii::$app->request->rawBody), 'gs\\order\\success');
        $actions = [
            'bogdan' => function($id, \app\models\Piramida\WalletSourceInterface $transaction) {
                Yii::info('bogdan action begin', 'gs\\order\\success::actionBegin');
                $mail = 'avia@galaxysss.ru';
                $request_id = $id;
                $request = \app\models\Shop\BogDan\Request::find($request_id);
                if (is_null($request)) {
                    if ($mail) {
                        Application::mail($mail, 'Ошибка. не найден заказ', 'shop/bogdan/payments/no_request', [
                            'fields' => $transaction->getTransactionInfo(),
                        ]);
                    }
                    return false;
                }
                $request->paid();

                return true;
            },


            'inRequest' => function($id, \app\models\Piramida\WalletSourceInterface $transaction){
                Yii::info('inRequest action begin', 'gs\\order\\success::actionBegin');
                $requestId = $id;
                $request = \app\models\Piramida\InRequest::findOne($requestId);
                $request->success($transaction);

                return true;
            },

            /**
             *
             * @param \app\models\Piramida\WalletSourceInterface $transaction
             */
            'gsssShop' => function($id, \app\models\Piramida\WalletSourceInterface $transaction) {
                Yii::info('gsssShop action begin', 'gs\\order\\success::actionBegin');
                $request_id = $id;
                $request = Request::find($request_id);
                if (is_null($request)) {
                    throw new Exception('Не найден заказ');
                }
                $request->beforeSuccess();
                $request->paid($transaction);

                return true;
            },
        ];


        $type = Yii::$app->request->get('type');
        if (is_null($type)) {
            return self::jsonErrorId(101, 'отсутствует обязательный параметр type');
        }
        switch($type) {
            case 'yandex':
                $class = new Yandex();
                $ret = $class->success($actions);
                break;
            case 'bitcoin':
                $class = new BitCoin();
                $ret = $class->success($actions);
                break;
        }
    }
}
