<?php

namespace school\modules\gsss\controllers;

use app\models\Article;
use app\models\Blog;
use app\models\Praktice;
use app\models\Service;
use app\models\Union;
use app\models\UnionCategory;
use app\services\HumanDesign2;
use cs\Application;
use cs\services\Str;
use cs\services\VarDumper;
use cs\web\Exception;
use Yii;
use yii\base\UserException;
use yii\filters\AccessControl;
use yii\grid\DataColumn;
use yii\helpers\StringHelper;
use yii\helpers\Url;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use app\models\NewsItem;
use app\models\Chenneling;
use cs\base\BaseController;
use yii\web\HttpException;


class ChennelingController extends BaseController
{
    public $layout = 'menu';

    /** @var int количество посланий на странице */
    public $itemsPerPage = 30;

    public function actionItem($year, $month, $day, $id)
    {
        $date = $year . $month . $day;
        $pattern = '#^[A-Za-z0-9_-]+$#';
        if (!preg_match($pattern, $id)) {
            throw new HttpException(404, 'Нет такого послания');
        }
        $item = Chenneling::find([
            'date'      => $date,
            'id_string' => $id
        ]);
        if (is_null($item)) {
            throw new HttpException(404, 'Нет такого послания');
        }
        if ($item->get('moderation_status', 0) != 1) {
            throw new HttpException(403, 'Послание не прошло модерацию');
        }
        $item->incViewCounter();
        // похожие статьи
        {
            $nearList = Chenneling::query()
                ->select([
                    'id',
                    'header',
                    'id_string',
                    'img',
                    'view_counter',
                    'description',
                    'date_created',
                    'date',
                ])
                ->orderBy(['date_created' => SORT_DESC])
                ->andWhere(['not', ['id' => $item->getId()]])
                ->andWhere(['moderation_status' => 1])
                ->limit(3)
                ->all();
        }

        return $this->render([
            'item'     => $item->getFields(),
            'nearList' => $nearList,
        ]);
    }


    /**
     * AJAX
     * Функция для обработки AutoComplete
     *
     * REQUEST:
     * - term - string - строка запроса
     */
    public function actionSearchAjax()
    {
        $term = self::getParam('term');
        if (is_null($term)) {
            return self::jsonErrorId(101, 'Нет обязательного параметра term');
        }
        $rows = Chenneling::query(['like', 'header', $term])
            ->andWhere(['moderation_status' => 1])
            ->select('id, header as value, date, id_string')
            ->orderBy(['date_created' => SORT_DESC])
            ->limit(10)
            ->all();

        return self::jsonSuccess($rows);
    }

    /**
     * Функция для вывода результатов поиска
     *
     * REQUEST:
     * - term - string - строка запроса
     */
    public function actionSearch()
    {
        $term = self::getParam('term');
        if (is_null($term)) {
            throw new Exception('Нет обязательного параметра term');
        }

        return $this->render([
            'term' => $term,
        ]);
    }

    public function actionIndex()
    {
        $itemsPerPage = $this->itemsPerPage;
        $query = Chenneling::querylist()
            ->andWhere(['moderation_status' => 1])
            ->orderBy(['date_created' => SORT_DESC]);

        if (!is_null(Yii::$app->request->get('user_id'))) {
            $query->andWhere(['user_id' => Yii::$app->request->get('user_id')]);
        }
        if (!is_null(Yii::$app->request->get('author_id'))) {
            $query->andWhere(['author_id' => Yii::$app->request->get('author_id')]);
        }
        $cache = $this->renderFile('@app/views/page/chenneling_cache.php', $this->pageCluster([
            'query'     => $query,
            'paginator' => [
                'size' => $itemsPerPage
            ]
        ]));

        return $this->render(['html' => $cache]);
    }

    /**
     * REQUEST:
     * - user_id - int
     * - author_id - int
     *
     * @return \yii\web\Response
     */
    public function actionAjax()
    {
        $query = Chenneling::querylist()
            ->andWhere(['moderation_status' => 1])
            ->orderBy(['date_created' => SORT_DESC]);
        if (self::getParam('user_id', '') != '') {
            $query->andWhere(['user_id' => self::getParam('user_id', '')]);
        }
        if (self::getParam('author_id', '') != '') {
            $query->andWhere(['author_id' => self::getParam('author_id', '')]);
        }
        $itemsPerPage = $this->itemsPerPage;
        $cache = $this->renderFile('@app/views/page/chenneling_cache_list.php', $this->pageCluster([
            'query'     => $query,
            'paginator' => [
                'size' => $itemsPerPage
            ]
        ]));

        return self::jsonSuccess($cache);
    }

    /**
     * Создает пагинацию запроса
     *
     * @param $options
     *                         [
     *                         'query' => Query
     *                         'paginator' => [
     *                         'size' => int
     *                         ]
     *                         ]
     *
     * @return array
     * [
     *    'list' =>
     *    'pages' => [
     *        'list' => [1,2,3, ...]
     *        'current' => int
     *    ]
     * ]
     */
    private function pageCluster($options)
    {
        /** @var \yii\db\Query $query */
        $query = $options['query'];
        $paginatorSize = $options['paginator']['size'];

        $page = (int)self::getParam('page', 1);
        $countAll = $query->count();

        // вычисляю количество страниц $pageCount
        $count = $countAll - $paginatorSize;
        if ($count > 0) {
            $count += $paginatorSize - 1;
            $pageCount = (int)($count / $paginatorSize);
            $pageCount++;
        } else {
            $pageCount = 1;
        }
        $offset = ($page - 1) * $paginatorSize;
        $pages = [];
        if ($pageCount >= 1) {
            for ($i = 1; $i <= $pageCount; $i++) {
                $pages[] = $i;
            }
        }

        return [
            'list'  => $query->limit($paginatorSize)->offset($offset)->all(),
            'pages' => [
                'list'    => $pages,
                'current' => $page,
            ],
        ];
    }

}
