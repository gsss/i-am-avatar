<?php

namespace school\modules\gsss\controllers;

use app\models\Chenneling;
use app\models\ChennelingAuthor;
use app\models\ChennelingTree;
use app\models\Form\Event;
use app\models\HD;
use app\models\HDtown;
use app\models\Log;
use app\models\Piramida\Piramida;
use app\models\Piramida\Wallet;
use app\models\SiteUpdate;
use app\models\Union;
use app\models\User;
use app\models\User2;
use app\models\UserRod;
use app\services\GetArticle\YouTube;
use app\services\GraphExporter;
use app\services\HumanDesign2;
use app\services\investigator\MidWay;
use app\services\LogReader;
use app\services\Subscribe;
use cs\Application;
use cs\base\BaseController;
use cs\helpers\Html;
use cs\services\BitMask;
use cs\services\SitePath;
use cs\services\Str;
use cs\services\Url;
use cs\services\VarDumper;
use Yii;
use yii\base\Exception;
use yii\base\UserException;
use yii\data\ActiveDataProvider;
use yii\data\Sort;
use yii\db\Query;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;
use yii\helpers\StringHelper;
use yii\httpclient\Client;
use yii\web\Controller;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use yii\web\ForbiddenHttpException;
use yii\web\NotFoundHttpException;
use yii\web\Response;
use yii\widgets\ActiveForm;
use app\services\RegistrationDispatcher;
use SimpleSoftwareIO\QrCode\BaconQrCodeGenerator;

class SiteController extends BaseController
{
    public $layout = 'menu';

    public function actions()
    {
        return [
            'error'   => [
                'class' => 'yii\web\ErrorAction',
                'view'  => '@app/views/site/error.php'
            ],
            'captcha' => [
                'class'           => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_DEV ? '123' : null,
            ],
        ];
    }

    public function actionTest_ajax()
    {
        $v = 1 / 0;
    }

    public function actionKey()
    {
        return $this->render();
    }


    /**
     * AJAX
     *
     * REQUEST:
     * - status: int,
     * - url: string,
     * - type: string 'get'|'post'
     * - data: mixed
     *
     * @return Response
     */
    public function actionError_ajax_report()
    {
        $mailList = Yii::$app->params['adminEmail'];
        if (!is_array($mailList)) {
            $mailList = [$mailList];
        }

        /**
         * Для этих путей не присылать извещение
         */
        $exclude = [
            '/calendar/save',
        ];
        if ($this->isGood($exclude, self::getParam('url'))) {
            foreach ($mailList as $mail) {
                Application::mail($mail, 'ОШИБКА AJAX GALAXYSSS.RU', 'error_ajax_report', [
                    'status' => self::getParam('status'),
                    'url'    => self::getParam('url'),
                    'type'   => self::getParam('type'),
                    'data'   => self::getParam('data'),
                ]);
            }
        }

        return self::jsonSuccess();
    }

    /**
     * Если $url не содержит ни одного адрса из $exclude то true
     * Иначе false
     *
     * @param array $exclude
     * @param string $url
     *
     * @return bool
     */
    private function isGood($exclude, $url)
    {
        foreach ($exclude as $e) {
            if (strpos($url, $e) !== false) {
                return false;
            }
        }

        return true;
    }

    public function actionIndex()
    {
        return $this->render([
            'events' => \app\models\Event::query()
                ->limit(3)
                ->where(['>=', 'end_date', gmdate('Ymd')])
                ->orderBy([
                    'date_insert' => SORT_DESC,
                ])
                ->all(),
        ]);
    }

    public function actionThank()
    {
        return $this->render();
    }

    public function actionMinistration()
    {
        return $this->render();
    }

    public function actionHuman_design()
    {
        return $this->render();
    }

    /**
     * AJAX
     * Расчет дизайна
     *
     * REQUEST:
     * - date - string - 'dd.mm.yyyy'
     * - time - string - 'hh:mm'
     * - londonOffset - int - смещение при летнем времени в секундах на лондоне
     * - placeOffset - int - смещение в секундах для места назначения
     * - location - string - lat,lng
     *
     * @return array
     */
    public function actionHuman_design_ajax()
    {
        $date = self::getParam('date');
        $time = self::getParam('time');
        $date = explode('.', $date);
        $placeOffset = (int)self::getParam('placeOffset');
        $londonOffset = (int)self::getParam('londonOffset');
        $location = self::getParam('location');
        $location = explode(',', $location);
        // получаю время и дату в лондоне
        {
            $t2 = new \DateTime($date[2] . '-' . $date[1] . '-' . $date[0] . ' ' . $time . ':00', new \DateTimeZone('UTC'));
            $timestamp = (int)$t2->format('U');
            $timestamp = $timestamp - $placeOffset + $londonOffset;
            $londonTime = new \DateTime('@' . $timestamp, new \DateTimeZone('UTC'));
            $dateLondon = $londonTime->format('Y-m-d H:i:s');
            $t = new \DateTime($dateLondon, new \DateTimeZone('Europe/London'));
        }

        // получаю данные Дизайна Человека
        $extractor = new \app\modules\HumanDesign\calculate\HumanDesignAmericaCom();
        $data = $extractor->calc($t->format('U') . '000', $t->format('d/m/Y'), $t->format('H:i'));

        $html = Yii::$app->view->renderFile('@app/views/site/human_design_ajax.php', [
            'human_design' => $data,
            'birth_date'   => $date[2] . '-' . $date[1] . '-' . $date[0],
            'birth_time'   => $time . ':00',
            'birth_lat'    => $location[0],
            'birth_lng'    => $location[1],
        ]);

        return self::jsonSuccess($html);
    }

    public function actionTeslagen()
    {
        return $this->redirect('/blog/2015/12/07/mavzoley__okkultnoe_stroenie');
    }

    public function actionConditions()
    {
        return $this->render();
    }

    public function actionHologram()
    {
        return $this->render();
    }

    /**
     * Выводит карточку профиля
     *
     * @param $id
     *
     * @return string
     * @throws \cs\web\Exception
     */
    public function actionUser($id)
    {
        $user = User::find($id);
        if (is_null($user)) {
            throw new \cs\web\Exception('Пользователь не найден');
        }

        return $this->render([
            'user' => $user,
        ]);
    }

    /**
     *  Прием уведомлений о платежах
     */
    public function actionMoney()
    {
        Yii::info(\yii\helpers\VarDumper::dumpAsString(Yii::$app->request->post()), 'gs\\money');

        return self::jsonSuccess();
    }

    /**
     *  Прием уведомлений о платежах
     */
    public function actionThankyou()
    {
        return $this->render();
    }

    public function actionService()
    {
        return $this->render();
    }


    /**
     * Отправляет POST запрос на $url с параметрами $params
     *
     * @param string $url
     * @param array $params ассоциативный массив пар (ключ => значение)
     *
     * @return array
     * при удачном завершении
     * [
     *     'status' => true
     *     'data' => [
     *         'body'    => array,
     *         'headers' => array,
     *     ]
     * ]
     * при не удачном завершении
     * [
     *     'status' => false
     *     'data' => [
     *         'id'      => int,
     *         'message' => string,
     *     ]
     * ]
     */
    private function requestPost($url, $params)
    {
        $curlOptions = [
            CURLOPT_HTTPHEADER     => [],
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_URL            => $url,
        ];
        $curlOptions[CURLOPT_POST] = true;
        $curlOptions[CURLOPT_HTTPHEADER] = [
            'Origin: http://www.galaxysss.ru',
            'Accept:application/json, text/javascript, */*; q=0.01',
            'Accept-Encoding:gzip, deflate',
            'Accept-Language:ru-RU,ru;q=0.8,en-US;q=0.6,en;q=0.4',
            'Connection:keep-alive',
//            'Content-Length:7',
            'Content-Type:application/x-www-form-urlencoded; charset=UTF-8',
            'Host:service.galaxysss.com',
            'Origin:http://www.galaxysss.ru',
            'Referer:http://www.galaxysss.ru/test',
            'User-Agent:Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/51.0.2704.103 Safari/537.36',
        ];
        $curlOptions[CURLOPT_POSTFIELDS] = http_build_query($params, '', '&', PHP_QUERY_RFC3986);
        $curlResource = curl_init();
        foreach ($curlOptions as $option => $value) {
            curl_setopt($curlResource, $option, $value);
        }
        $response = curl_exec($curlResource);
        $responseHeaders = curl_getinfo($curlResource);

        // check cURL error
        $errorNumber = curl_errno($curlResource);
        $errorMessage = curl_error($curlResource);

        curl_close($curlResource);

        if ($errorNumber > 0) {
            return [
                'status' => false,
                'data'   => [
                    'id'      => $errorNumber,
                    'message' => $errorMessage,
                ],
            ];
        } else {
            return [
                'status' => true,
                'data'   => [
                    'body'    => $response,
                    'headers' => $responseHeaders,
                ],
            ];
        }
    }
    /**
     * Отправляет POST запрос на $url с параметрами $params
     *
     * @param string $url
     * @param array $params ассоциативный массив пар (ключ => значение)
     *
     * @return array
     * при удачном завершении
     * [
     *     'status' => true
     *     'data' => [
     *         'body'    => array,
     *         'headers' => array,
     *     ]
     * ]
     * при не удачном завершении
     * [
     *     'status' => false
     *     'data' => [
     *         'id'      => int,
     *         'message' => string,
     *     ]
     * ]
     */
    private function requestGet($url, $params)
    {
        $u = new Url($url);
        $u->addParams($params);
        $curl = curl_init($u->__toString());
        curl_setopt($curl, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/45.0.2454.85 Safari/537.36');
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl, CURLOPT_HEADER, 0);
        curl_setopt($curl, CURLOPT_CONNECTTIMEOUT, 10);
        $body = curl_exec($curl);

        $result = new \StdClass();
        $result->headers = curl_getinfo($curl);
        $result->body = $body;

        // check cURL error
        $errorNumber = curl_errno($curl);
        $errorMessage = curl_error($curl);

        curl_close($curl);

        if ($errorNumber > 0) {
            return [
                'status' => false,
                'data'   => [
                    'id'      => $errorNumber,
                    'message' => $errorMessage,
                ],
            ];
        } else {
            return [
                'status' => true,
                'data'   => [
                    'body'    => $body,
                    'headers' => $result->headers,
                ],
            ];
        }
    }

    public function actionTest()
    {
        VarDumper::dump(date('Y-m-d H-i-s', 1539450361));
    }

    public function actionTest1()
    {
        return $this->render();
    }

    /**
     * Возвращает элементы списка
     * @return array
     * [[
     *  'id' =>
     *  'name' =>
     *  'nodes' => array
     * ], ... ]
     */
    public function getRows($parentId = null)
    {
        $rows = (new Query())
            ->select('id, header as name')
            ->from('gs_unions_tree')
            ->where(['parent_id' => $parentId])
            ->orderBy(['sort_index' => SORT_ASC])
            ->all();
        for ($i = 0; $i < count($rows); $i++) {
            $item = &$rows[$i];
            $rows2 = $this->getRows($item['id']);
            if (count($rows2) > 0) {
                $item['nodes'] = $rows2;
            }
            $item['unions'] = $unions = Union::query(['tree_node_id' => $item['id']])->all();
        }

        return $rows;
    }


    public function actionStatistic()
    {
        return $this->render();
    }

    public function actionSite_update()
    {
        \app\services\SiteUpdateItemsCounter::clear();
        \app\services\UserLastActive::updateNow();

        return $this->render([
            'list' => SiteUpdate::query()
                ->orderBy(['date_insert' => SORT_DESC])
                ->limit(50)
                ->all()
        ]);
    }

    public function actionSite_update_ajax()
    {
        $typeId = self::getParam('id');

        return self::jsonSuccess($this->renderFile('@app/views/site/site_update_ajax.php', [
            'list' => SiteUpdate::query(['type' => $typeId])->orderBy(['date_insert' => SORT_DESC])->limit(50)->all()
        ]));
    }

    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $model->contact(Yii::$app->params['mailList']['contact']);
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        } else {
            return $this->render('contact', [
                'model' => $model,
            ]);
        }
    }

    public function actionUser_rod($user_id, $rod_id)
    {
        $user = UserRod::find(['user_id' => $user_id, 'rod_id' => $rod_id]);
        if (is_null($user)) {
            $user = new UserRod([
                'user_id' => $user_id,
                'rod_id'  => $rod_id,
            ]);
        }
        $path = $user->getRodPath();
        $breadcrumbs = [];
        foreach ($path as $i) {
            $breadcrumbs[] = [
                'label' => (is_null($i['name'])) ? '?' : $i['name'],
                'url'   => [
                    'site/user_rod',
                    'user_id' => $user_id,
                    'rod_id'  => $i['id'],
                ],
            ];
        }

        return $this->render([
            'userRod'     => $user,
            'breadcrumbs' => $breadcrumbs,
        ]);
    }

    /**
     * Выводит весь род человека
     *
     * @param int $id идентификатор пользователя
     *
     * @return string
     * @throws \cs\web\Exception
     */
    public function actionUser_rod_list($id)
    {
        $user = User::find($id);
        if (is_null($user)) {
            throw new \cs\web\Exception('Не найден пользователь');
        }
        $rod = UserRod::query(['user_id' => $id])->all();
        $rows = [];
        for ($i = 1; $i < 127; $i++) {
            $new = null;
            foreach ($rod as $item) {
                if ($item['rod_id'] == $i) {
                    $new = $item;
                }
            }
            if (is_null($new)) {
                $new = [
                    'rod_id' => $i
                ];
            }
            $rows[$i] = $new;
        }

        return $this->render([
            'items' => $rows,
            'user'  => $user,
        ]);
    }

    public function actionUser_rod_edit($user_id, $rod_id)
    {
        if (Yii::$app->user->isGuest) {
            throw new \cs\web\Exception('Вы не можете редактировать данные');
        }
        if (Yii::$app->user->id != $user_id) {
            throw new \cs\web\Exception('Вы не можете редактировать чужие данные');
        }

        $user = UserRod::find(['user_id' => $user_id, 'rod_id' => $rod_id]);
        if (is_null($user)) {
            $user = UserRod::insert([
                'user_id' => $user_id,
                'rod_id'  => $rod_id,
            ]);
        }
        $path = $user->getRodPath();
        $breadcrumbs = [];
        foreach ($path as $i) {
            $breadcrumbs[] = [
                'label' => (is_null($i['name'])) ? '?' : $i['name'],
                'url'   => [
                    'site/user_rod_edit',
                    'user_id' => $user_id,
                    'rod_id'  => $i['id'],
                ],
            ];
        }
        $model = new \app\models\Form\UserRod($user->getFields());
        if ($model->load(Yii::$app->request->post()) && $model->update()) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        } else {
            return $this->render([
                'model'       => $model,
                'breadcrumbs' => $breadcrumbs,
            ]);
        }
    }

    public function actionLogo()
    {
        return $this->render();
    }

    public function actionRent()
    {
        return $this->render();
    }

    public function actionLog()
    {
        if (!Yii::$app->user->can('admin2')) throw new NotFoundHttpException();
        $first = self::getParam('first');

        return $this->render([
            'log'   => LogReader::file('@runtime/logs/app.log')->readLast(
                ArrayHelper::merge(
                    ['maxStrings' => 10],
                    Yii::$app->request->get()
                )
            ),
            'first' => ($first) ? $first : 0,
        ]);
    }

    public function actionLog_db()
    {
        if (!Yii::$app->user->can('admin2')) throw new NotFoundHttpException();
        return $this->render();
    }
}
