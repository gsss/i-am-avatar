<?php

namespace school\modules\gsss\controllers;

use app\models\Article;
use app\models\Blog;
use app\models\Praktice;
use app\models\Service;
use app\models\Union;
use app\models\UnionCategory;
use app\services\HumanDesign2;
use cs\Application;
use cs\services\Str;
use cs\services\VarDumper;
use cs\web\Exception;
use Yii;
use yii\base\UserException;
use yii\filters\AccessControl;
use yii\grid\DataColumn;
use yii\helpers\StringHelper;
use yii\helpers\Url;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use app\models\NewsItem;
use app\models\Chenneling;
use cs\base\BaseController;
use yii\web\HttpException;


class PageController extends BaseController
{
    public $layout = 'menu';

    /** @var int количество посланий на странице */
    public $itemsPerPage = 30;

    public function actionHouse()
    {
        return $this->render([
            'articleList' => Article::getByTreeNodeId(3),
        ]);
    }

    public function actionMedical()
    {
        return $this->render([
            'articleList' => Article::getByTreeNodeId(5),
        ]);
    }

    public function actionTest()
    {
        return $this->render();
    }

    public function actionHymn()
    {
        return $this->render();
    }

    public function actionManifest()
    {
        return $this->render();
    }

    public function actionServices()
    {
        return $this->render([
            'list' => Service::query()->select([
                'id',
                'header',
                'description',
                'if(length(ifnull(content, "")) > 0, 1, 0) as is_content',
                'image',
                'link',
                'date_insert',
            ])->all(),
        ]);
    }

    public function actionServices_item($id)
    {
        return $this->render([
            'item' => Service::find($id)
        ]);
    }

    public function actionColkin()
    {
        return $this->render();
    }

    public function actionMission()
    {
        return $this->render();
    }

    public function actionArts()
    {
        return $this->render([
            'articleList' => Article::getByTreeNodeId(12),
        ]);
    }

    public function actionUp()
    {
        return $this->render();
    }

    public function actionStudy()
    {
        return $this->render([
            'articleList' => Article::getByTreeNodeId(11),
        ]);
    }

    public function actionGfs()
    {
        return $this->render();
    }

    public function actionTime()
    {
        return $this->render([
            'articleList' => Article::getByTreeNodeId(2),
        ]);
    }

    public function actionTime_arii()
    {
        return $this->render([
        ]);
    }

    /**
     * Выводит подкатегорию
     *
     * @param $id
     *
     * @return string
     * @throws Exception
     */
    public function actionCategory($id)
    {
        $category = UnionCategory::find(['id_string' => $id]);
        if (is_null($category)) {
            throw new Exception('Нет такой категории');
        }

        return $this->render([
            'item'        => $category,
            'unionList'   => $category->getUnions(),
            'articleList' => Article::getByTreeNodeId($category->getId()),
            'breadcrumbs' => [
                'label' => $category->getField('header'),
                'url'   => '/' . $id,
            ],
            'idString'    => $id
        ]);
    }

    public function actionLanguage()
    {
        return $this->render([
            'articleList' => Article::getByTreeNodeId(1),
        ]);
    }

    public function actionEnergy()
    {
        return $this->render([
            'articleList' => Article::getByTreeNodeId(7),
        ]);
    }

    public function actionMoney()
    {
        return $this->render([
            'articleList' => Article::getByTreeNodeId(6),
        ]);
    }

    public function actionFood()
    {
        return $this->render([
            'articleList' => Article::getByTreeNodeId(4),
        ]);
    }

    public function actionFood_item($id)
    {
        $item = Union::find($id);

        return $this->render([
            'item'       => $item,
            'officeList' => $item->getOfficeList([
                'point_lat as lat',
                'point_lng as lng',
                'concat("<h5>",name,"</h5><p>",point_address,"</p>") as html',
            ])
        ]);
    }

    public function actionUnion_item($category, $id)
    {
        $item = Union::find($id);
        if (is_null($item)) {
            throw new Exception('Нет такого объединения');
        }
        $categoryObject = UnionCategory::find(['id_string' => $category]);
        if (is_null($categoryObject)) {
            throw new Exception('Не найдена категория');
        }

        return $this->render([
            'item'        => $item,
            'officeList'  => $item->getOfficeList([
                'id',
                'point_lat as lat',
                'point_lng as lng',
                'concat("<h5>",name,"</h5><div>",ifnull(content,""),"</div><p>",point_address,"</p>") as html',
            ]),
            'breadcrumbs' => $categoryObject->getBreadCrumbs([$item->getField('name')]),
            'category'    => $categoryObject,
        ]);
    }

    public function actionForgive()
    {
        return $this->render([
            'articleList' => Article::getByTreeNodeId(8),
        ]);
    }

    public function actionTv()
    {
        return $this->render([
            'articleList' => Article::getByTreeNodeId(10),
        ]);
    }

    public function actionClothes()
    {
        return $this->render([
            'articleList' => Article::getByTreeNodeId(9),
        ]);
    }

    public function actionPortals()
    {
        return $this->render();
    }

    public function actionMusic()
    {
        return $this->render([
            'articleList' => Article::getByTreeNodeId(13),
        ]);
    }

    public function actionIdea()
    {
        return $this->render();
    }

    public function actionHistory()
    {
        return $this->render();
    }

    /**
     * Страница Практик
     *
     * @return string
     */
    public function actionPraktice()
    {
        return $this->render([
            'items' => Praktice::queryList()->orderBy(['date_insert' => SORT_DESC])->all()
        ]);
    }

    public function actionPraktice_item($year, $month, $day, $id)
    {
        $date = $year . $month . $day;
        $newsItem = Praktice::find([
            'date'      => $date,
            'id_string' => $id
        ]);
        if (is_null($newsItem)) {
            throw new Exception('Нет такой прктики');
        }
        $newsItem->incViewCounter();

        $row = $newsItem->getFields();

        return $this->render([
            'item'     => $newsItem->getFields(),
            'lastList' => Praktice::queryList()->where([
                'not',
                ['id' => $row['id']]
            ])->orderBy(['date_insert' => SORT_DESC])->limit(3)->all(),
        ]);
    }

    public function actionLanguage_article($id)
    {
        $item = Article::find([
            'id_string' => $id
        ]);
        if (is_null($item)) {
            throw new Exception('Нет такой статьи');
        }
        $item->incViewCounter();

        return $this->render([
            'item' => $item->getFields()
        ]);
    }

    public function actionArticle($category, $year, $month, $day, $id)
    {
        $pattern = '#^[A-Za-z0-9_-]+$#';
        if (!preg_match($pattern, $id)) {
            throw new HttpException(404, 'Нет такой статьи');
        }
        $item = Article::find([
            'id_string'         => $id,
            'DATE(date_insert)' => $year . $month . $day
        ]);
        if (is_null($item)) {
            throw new Exception('Нет такой статьи');
        }
        $categoryObject = UnionCategory::find(['id_string' => $category]);
        if (is_null($categoryObject)) {
            throw new Exception('Нет такой Категории');
        }
        $item->incViewCounter();
        // похожие статьи
        {
            $nearList = Article::getByTreeNodeIdQuery($categoryObject->getId())
                ->andWhere(['not', ['id' => $item->getId()]])
                ->limit(3)
                ->all();
            if (count($nearList) == 0) {
                $nearList = Article::query()
                    ->select('id,header,id_string,image,view_counter,description,content,date_insert')
                    ->orderBy(['date_insert' => SORT_DESC])
                    ->andWhere(['not', ['id' => $item->getId()]])
                    ->limit(3)
                    ->all();
            }
        }

        return $this->render([
            'item'        => $item->getFields(),
            'category'    => $category,
            'nearList'    => $nearList,
            'breadcrumbs' => $categoryObject->getBreadCrumbs([$item->getField('header')]),
        ]);
    }

    public function actionChenneling_item($year, $month, $day, $id)
    {
        $date = $year . $month . $day;
        $pattern = '#^[A-Za-z0-9_-]+$#';
        if (!preg_match($pattern, $id)) {
            throw new HttpException(404, 'Нет такого послания');
        }
        $item = Chenneling::find([
            'date'      => $date,
            'id_string' => $id
        ]);
        if (is_null($item)) {
            throw new HttpException(404, 'Нет такого послания');
        }
        if ($item->get('moderation_status', 0) != 1) {
            throw new HttpException(403, 'Послание не прошло модерацию');
        }
        $item->incViewCounter();
        // похожие статьи
        {
            $nearList = Chenneling::query()
                ->select([
                    'id',
                    'header',
                    'id_string',
                    'img',
                    'view_counter',
                    'description',
                    'date_created',
                    'date',
                ])
                ->orderBy(['date_created' => SORT_DESC])
                ->andWhere(['not', ['id' => $item->getId()]])
                ->andWhere(['moderation_status' => 1])
                ->limit(3)
                ->all();
        }

        return $this->render([
            'item'     => $item->getFields(),
            'nearList' => $nearList,
        ]);
    }


    /**
     * AJAX
     * Функция для обработки AutoComplete
     *
     * REQUEST:
     * - term - string - строка запроса
     */
    public function actionChenneling_search_ajax()
    {
        $term = self::getParam('term');
        if (is_null($term)) {
            return self::jsonErrorId(101, 'Нет обязательного параметра term');
        }
        $rows = Chenneling::query(['like', 'header', $term])
            ->andWhere(['moderation_status' => 1])
            ->select('id, header as value, date, id_string')
            ->orderBy(['date_created' => SORT_DESC])
            ->limit(10)
            ->all();

        return self::jsonSuccess($rows);
    }

    /**
     * Функция для вывода результатов поиска
     *
     * REQUEST:
     * - term - string - строка запроса
     */
    public function actionChenneling_search()
    {
        $term = self::getParam('term');
        if (is_null($term)) {
            throw new Exception('Нет обязательного параметра term');
        }

        return $this->render([
            'term' => $term,
        ]);
    }

    public function actionChenneling()
    {
        $itemsPerPage = $this->itemsPerPage;
        $query = Chenneling::querylist()
            ->andWhere(['moderation_status' => 1])
            ->orderBy(['date_created' => SORT_DESC]);

        if (!is_null(Yii::$app->request->get('user_id'))) {
            $query->andWhere(['user_id' => Yii::$app->request->get('user_id')]);
        }
        if (!is_null(Yii::$app->request->get('author_id'))) {
            $query->andWhere(['author_id' => Yii::$app->request->get('author_id')]);
        }
        $cache = $this->renderFile('@app/views/page/chenneling_cache.php', $this->pageCluster([
            'query'     => $query,
            'paginator' => [
                'size' => $itemsPerPage
            ]
        ]));

        return $this->render(['html' => $cache]);
    }

    /**
     * REQUEST:
     * - user_id - int
     * - author_id - int
     *
     * @return \yii\web\Response
     */
    public function actionChenneling_ajax()
    {
        $query = Chenneling::querylist()
            ->andWhere(['moderation_status' => 1])
            ->orderBy(['date_created' => SORT_DESC]);
        if (self::getParam('user_id', '') != '') {
            $query->andWhere(['user_id' => self::getParam('user_id', '')]);
        }
        if (self::getParam('author_id', '') != '') {
            $query->andWhere(['author_id' => self::getParam('author_id', '')]);
        }
        $itemsPerPage = $this->itemsPerPage;
        $cache = $this->renderFile('@app/views/page/chenneling_cache_list.php', $this->pageCluster([
            'query'     => $query,
            'paginator' => [
                'size' => $itemsPerPage
            ]
        ]));

        return self::jsonSuccess($cache);
    }

    /**
     * Создает пагинацию запроса
     *
     * @param $options
     *                         [
     *                         'query' => Query
     *                         'paginator' => [
     *                         'size' => int
     *                         ]
     *                         ]
     *
     * @return array
     * [
     *    'list' =>
     *    'pages' => [
     *        'list' => [1,2,3, ...]
     *        'current' => int
     *    ]
     * ]
     */
    public function pageCluster($options)
    {
        /** @var \yii\db\Query $query */
        $query = $options['query'];
        $paginatorSize = $options['paginator']['size'];

        $page = (int)self::getParam('page', 1);
        $countAll = $query->count();

        // вычисляю количество страниц $pageCount
        $count = $countAll - $paginatorSize;
        if ($count > 0) {
            $count += $paginatorSize - 1;
            $pageCount = (int)($count / $paginatorSize);
            $pageCount++;
        } else {
            $pageCount = 1;
        }
        $offset = ($page - 1) * $paginatorSize;
        $pages = [];
        if ($pageCount >= 1) {
            for ($i = 1; $i <= $pageCount; $i++) {
                $pages[] = $i;
            }
        }

        return [
            'list'  => $query->limit($paginatorSize)->offset($offset)->all(),
            'pages' => [
                'list'    => $pages,
                'current' => $page,
            ],
        ];
    }

}
