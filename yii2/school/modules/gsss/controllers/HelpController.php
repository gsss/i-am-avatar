<?php

namespace school\modules\gsss\controllers;

use app\models\Form\Event;
use app\models\Form\Help;
use app\models\HD;
use app\models\HDtown;
use app\models\Log;
use app\models\SiteUpdate;
use app\models\Union;
use app\models\User;
use app\models\UserRod;
use app\services\GetArticle\YouTube;
use app\services\GraphExporter;
use app\services\HumanDesign2;
use app\services\investigator\MidWay;
use cs\Application;
use cs\base\BaseController;
use cs\helpers\Html;
use cs\services\SitePath;
use cs\services\Str;
use cs\services\VarDumper;
use Yii;
use yii\base\Exception;
use yii\base\UserException;
use yii\data\ActiveDataProvider;
use yii\db\Query;
use yii\filters\AccessControl;
use yii\helpers\Json;
use yii\helpers\StringHelper;
use yii\web\Controller;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use yii\web\Response;
use yii\widgets\ActiveForm;
use app\services\RegistrationDispatcher;

class HelpController extends BaseController
{
    public $layout = 'menu';

    public function actionIndex()
    {
        return $this->render([
            'rows' => $this->getRows()

        ]);
    }

    public function actionItem($id)
    {
        $i = \app\models\Help::find($id);
        if (is_null($i)) {
            throw new \cs\web\Exception('Не найден элемент');
        }

        return $this->render([
            'item' => $i,
        ]);
    }


    /**
     * Возвращает элементы списка
     *
     * @param int | null $parentId идентификатор родительского элемента
     *
     * @return array
     * [[
     *  'id' =>
     *  'name' =>
     *  'nodes' => array
     * ], ... ]
     */
    public function getRows($parentId = null)
    {
        $rows = (new Query())
            ->select('id, name')
            ->from('gs_help_tree')
            ->where(['parent_id' => $parentId])
            ->orderBy(['sort_index' => SORT_ASC])
            ->all();
        for ($i = 0; $i < count($rows); $i++) {
            $item = &$rows[ $i ];
            $item['articleList'] = \app\models\Help::query(['tree_node_id' => $item['id']])->select('id,name')->orderBy(['sort_index' => SORT_ASC])->all();
            $rows2 = $this->getRows($item['id']);
            if (count($rows2) > 0) {
                $item['nodes'] = $rows2;
            }
        }

        return $rows;
    }

}
