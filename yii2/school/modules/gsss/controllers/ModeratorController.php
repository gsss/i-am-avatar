<?php

namespace school\modules\gsss\controllers;

use app\models\Article;
use app\models\Shop\Product;
use app\models\SiteUpdate;
use app\models\Union;
use app\services\Subscribe;
use cs\Application;
use cs\services\VarDumper;
use cs\web\Exception;
use Yii;
use yii\base\UserException;

class ModeratorController extends AdminBaseController
{

    public function actionIndex()
    {
        return $this->render([]);
    }

}
