<?php

namespace school\modules\gsss\controllers;

use app\models\Article;
use app\models\SiteUpdate;
use app\services\Subscribe;
use cs\services\VarDumper;
use cs\web\Exception;
use Yii;
use yii\base\UserException;

class AdminDeveloperController extends AdminBaseController
{
    public function actions()
    {
        $rows = [];
        $path = dir(Yii::getAlias('@app/views/admin-developer'));
        while (($file = $path->read()) !== false){
            if (!in_array($file, ['.', '..'])) {
                $key = substr($file,0, strlen($file)-4);
                $rows[$key] = '\app\controllers\actions\AdminDeveloperAction';
            }
        }
        $path->close();

        return $rows;
    }
}
