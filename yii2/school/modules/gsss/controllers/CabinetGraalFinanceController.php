<?php

namespace school\modules\gsss\controllers;

use app\models\Article;
use app\models\Graal\InRequest;
use app\models\Piramida\Billing;
use app\models\SiteUpdate;
use app\models\validate\PenaltyGet;
use app\services\Subscribe;
use cs\services\VarDumper;
use cs\web\Exception;
use Yii;
use yii\base\Event;
use yii\base\UserException;
use \app\models\Penalty;
use yii\bootstrap\ActiveForm;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use yii\rbac\Role;

class CabinetGraalFinanceController extends CabinetBaseController
{


    public static function getModeratorUser()
    {
        return [
            'name'   => 'Небесный Храм',
            'avatar' => Yii::$app->view->getAssetManager()->getBundle('\app\modules\Piramida\assets\main\Asset')->baseUrl . '/main.jpg',
        ];
    }

    /**
     * Показывает все заявки на инвестиции
     *
     * @return string
     */
    public function actionIndex()
    {
        return $this->render();
    }

    /**
     * Показывает все заявки на инвестиции
     *
     * @return string
     */
    public function actionInRequests()
    {
        return $this->render('in-requests');
    }

    /**
     * Инвесторы
     *
     * @return string
     */
    public function actionUsersInvest()
    {
        return $this->render('users-invest');
    }

    /**
     * AJAX
     * Присвоение роли graal-kon
     *
     * @param int $id идентификатор пользователя
     *
     * @return string
     */
    public function actionUsersInvestKon($id)
    {
        $role = Yii::$app->authManager->getRole('graal-kon');
        Yii::$app->authManager->assign($role, $id);

        return self::jsonSuccess();
    }

    /**
     * Командный состав
     *
     * @return string
     */
    public function actionUsersCommand()
    {
        return $this->render('users-command');
    }

    /**
     * Учредители
     *
     * @return string
     */
    public function actionUsersUchreditel()
    {
        return $this->render('users-uchreditel');
    }

    /**
     * Соратники
     *
     * @return string
     */
    public function actionUsersKon()
    {
        return $this->render('users-kon');
    }

    /**
     * Показывает все заявки на инвестиции
     *
     * @param int $id идентификатор заявки на инвестицию
     *
     * @return string
     */
    public function actionInRequestsMessages($id)
    {
        $request = \app\modules\Graal\models\InRequest::findOne($id);

        return $this->render('in-requests-messages', [
            'request' => $request
        ]);
    }

    /**
     * Подтверждает вывод средств
     *
     * @param int $id идентификатор заявки на инвестицию graal_in_requests.id
     *
     * @return string
     *
     * @throws \cs\web\Exception
     */
    public function actionConfirm($id)
    {
        $model = new \app\modules\Graal\models\forms\InRequestSuccess(['id' => $id]);
        if (Yii::$app->request->isAjax) {
            if ($model->load(Yii::$app->request->post())) {
                Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

                return ActiveForm::validate($model);
            }
        }
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $model->success($id);
            return $this->redirect(['cabinet-graal-finance/in-requests']);
        } else {
            return $this->render([
                'model' => $model,
                'id'    => $id,
            ]);
        }
    }

    /**
     * Отправляет сообщение в чат
     *
     * REQUEST:
     * - message - string - сообщение
     *
     * @param int $id идентификатор заявки на вывод
     *
     * @return string
     * @throws \cs\web\Exception
     */
    public function actionMessage($id)
    {
        $request = \app\modules\Graal\models\InRequest::findOne($id);
        $message = self::getParam('message');
        $messageObject = $request->addMessageToClient($message);

        \cs\Application::mail($request->getOwner()->getEmail(), 'Сообщение по инвестиции #'.$request->id, 'graal/out-request-to-client', [
            'request' => $this,
            'message' => $messageObject,
        ]);

        return self::jsonSuccess([
            'html' => $this->renderPartial('@app/views/cabinet-graal-finance/in-requests-message.php', [
                'message' => $messageObject,
                'user'    => self::getModeratorUser(),
            ]),
        ]);
    }
}
