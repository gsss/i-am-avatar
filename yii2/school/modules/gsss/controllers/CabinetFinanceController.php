<?php

namespace school\modules\gsss\controllers;

use app\models\Form\Union;
use app\models\HD;
use app\models\HdGenKeys;
use app\models\HDtown;
use app\models\Piramida\OutRequest;
use app\models\Piramida\OutRequestMessage;
use app\models\SiteUpdate;
use app\models\User;
use app\services\Subscribe;
use cs\Application;
use cs\services\SitePath;
use cs\services\VarDumper;
use cs\web\Exception;
use Yii;
use yii\base\UserException;
use yii\filters\AccessControl;
use yii\helpers\Url;
use yii\web\Controller;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use app\models\NewsItem;
use cs\base\BaseController;
use yii\web\Response;
use yii\widgets\ActiveForm;

class CabinetFinanceController extends BaseController
{
    public $layout = 'menu';

    public static function getModeratorUser()
    {
        return [
            'name'   => 'Галактический Союз Сил Света',
            'avatar' => Yii::$app->view->getAssetManager()->getBundle('\app\modules\Piramida\assets\main\Asset')->baseUrl . '/main.jpg',
         ];
    }

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@', 'finance-manager2'],
                    ],
                ],
            ],
        ];
    }

    public function actionOut()
    {
        return $this->render();
    }

    /**
     * @param int $id идентификатор заявки на вывод nw_out_requests.id
     *
     * @return string
     *
     * @throws \cs\web\Exception
     */
    public function actionOutMessages($id)
    {
        $outRequest = OutRequest::findOne($id);

        return $this->render('out-messages', [
            'request' => $outRequest,
        ]);
    }

    /**
     * Подтверждает вывод средств
     *
     * @param int $id идентификатор заявки на вывод nw_out_requests.id
     *
     * REQUEST:
     * - message - string - текстовая инфо
     *
     * @return string
     *
     * @throws \cs\web\Exception
     */
    public function actionConfirm($id)
    {
        $outRequest = OutRequest::findOne($id);
        if ($outRequest->getWallet()->value < $outRequest->price) {
            return self::jsonErrorId(101, 'В кошельке денег меньше чем указано в заявке на вывод');
        }
        $return = $outRequest->success(new \app\models\Piramida\WalletDestination\Yandex([
            'transaction' => [
                'message' => self::getParam('message'),
            ]
        ]));

        Application::mail($outRequest->getOwner()->getEmail(), 'Сообщение по выводу средств', 'piramida/out-request-to-client-confirm', [
            'request'     => $outRequest,
            'message'     => $return['message'],
            'transaction' => $return['transaction'],
        ]);

        return self::jsonSuccess();
    }

    /**
     * крывает заказ
     *
     * REQUEST:
     * - id - int - идентификатор заявки на вывод nw_out_requests.id
     *
     * @return string
     *
     * @throws \cs\web\Exception
     */
    public function actionOutMessagesHide()
    {
        $outRequest = OutRequest::findOne(self::getParam('id'));
        $outRequest->is_hide = 1;
        $outRequest->save();

        return self::jsonSuccess();
    }

    /**
     * Отправляет сообщение в чат
     *
     * REQUEST:
     * - message - string - сообщение
     *
     * @param int $id идентификатор заявки на вывод
     *
     * @return string
     * @throws \cs\web\Exception
     */
    public function actionMessage($id)
    {
        $request = OutRequest::findOne($id);
        $message = self::getParam('message');
        $messageObject = $request->addMessageToClient($message);

        Application::mail($request->getOwner()->getEmail(), 'Сообщение по выводу средств', 'piramida/out-request-to-client', [
            'request' => $request,
            'message' => $messageObject,
        ]);

        return self::jsonSuccess([
            'html' => $this->renderPartial('@app/views/cabinet_wallet/out-item-message.php', [
                'message' => $messageObject,
                'user'    => self::getModeratorUser(),
            ]),
        ]);
    }
}
