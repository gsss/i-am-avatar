<?php

namespace school\modules\gsss\controllers;

use app\exceptions\ApiException;
use app\models\Article;
use app\models\Form\UserRegistration;
use app\models\SiteUpdate;
use app\models\User3;
use app\services\Subscribe;
use cs\base\BaseController;
use cs\base\Controller;
use cs\services\UploadFolderDispatcher;
use cs\services\VarDumper;
use cs\web\Exception;
use Yii;
use yii\base\UserException;
use yii\db\Connection;
use yii\db\Query;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use yii\helpers\FileHelper;
use yii\web\Response;
use app\models\User2 as User;

class ApiAuthController extends Controller
{
    public $enableCsrfValidation = false;

    // https://learn.javascript.ru/xhr-crossdomain
    public function behaviors()
    {
        return [
            'corsFilter' => [
                'class'   => \yii\filters\Cors::className(),
                'cors'    => [
                    'Origin'                           => ['*'],
                    'Access-Control-Request-Method'    => ['GET', 'POST', 'PUT', 'PATCH', 'DELETE', 'HEAD', 'OPTIONS'],
                    'Access-Control-Request-Headers'   => ['*'],
                    'Access-Control-Allow-Credentials' => null,
                    'Access-Control-Max-Age'           => 86400,
                    'Access-Control-Expose-Headers'    => [],
                ],
            ],
        ];
    }


    public function init()
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

        $key = Yii::$app->request->get('key', '');
        if ($key == '') {
            throw new ApiException('Нет ключа');
        }
        if ($key != Yii::$app->params['api']['key']) {
            throw new ApiException('Ключ не верный');
        }
    }

    /**
     * Провряет логин
     * REQUEST:
     * - email - string - почта/логин
     * - password - string - пароль
     * - is_stay - int - оставаться в системе
     * 0 - не запоминать меня
     * 1 - запомнить меня на этом компьютере
     *
     * @return string
     * errors
     * 101, 'Пользователь не найден'
     * 102, 'Пользователь не активирован'
     * 103, 'Пользователь заблокирован'
     * 104, 'Не верный пароль'
     * 105, 'Вы  не завели себе пароль для аккаунта. Зайдите в восстановление пароля'
     * 106, 'Email может содержать только буквы латинского алфавита'
     * 107, 'Вы уже авторизованы'
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return self::errorId(107, 'Вы уже авторизованы');
        }
        $email = strtolower(self::getParam('email'));
        $password = self::getParam('password');
        if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
            return self::errorId(106, 'Не верный формат email'); 
        }
        $user = \app\models\User::find([
            'email' => $email,
        ]);
        if (is_null($user)) {
            return self::errorId(101, 'Пользователь не найден');
        }
        if ($user->get('is_confirm', 0) != 1) {
            return self::errorId(102, 'Пользователь не активирован');
        }
        if ($user->get('is_active', 0) != 1) {
            return self::errorId(103, 'Пользователь заблокирован');
        }
        if ($user->get('password', '') == '') {
            return self::errorId(105, 'Вы  не завели себе пароль для аккаунта. Зайдите в восстановление пароля');
        }
        if ($user->validatePassword($password)) {
            $duration = 0;
            if (self::getParam('is_stay', 0) == 1) {
                $duration = 60 * 60 * 24 * 365 * 10;
            }
            Yii::$app->user->login($user, $duration);

            return self::success();
        }
        else {
            return self::errorId(104, 'Не верный пароль');
        }
    }

    /**
     * REQUEST:
     *
     * - email - string -
     * - password - string -
     * - key -
     *
     */
    public function actionRegistration()
    {
        $model = new UserRegistration();
        if ($user = $model->register(Yii::$app->request->post())) {
            return $this->errorId(10, $model->errors);
        }

        return $this->success(['id' => $user->id]);
    }


    /**
     * Страница активации для подтверждения сменя email
     *
     * @param string $code код подтверждения
     *
     * @return string|\yii\web\Response
     *
     * @throws \cs\web\Exception
     */
    public function actionChange_email_activate($code)
    {

    }

}
