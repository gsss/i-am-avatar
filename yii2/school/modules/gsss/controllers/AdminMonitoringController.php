<?php

namespace school\modules\gsss\controllers;

use app\services\Subscribe;
use cs\services\VarDumper;
use cs\web\Exception;
use Yii;
use yii\base\UserException;
use yii\filters\AccessControl;
use yii\helpers\Url;
use yii\web\Controller;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use app\models\NewsItem;
use app\models\Chenneling;
use cs\base\BaseController;
use yii\web\Response;
use yii\widgets\ActiveForm;
use app\models\SiteUpdate;

class AdminMonitoringController extends AdminBaseController
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@', 'monitoring'],
                    ],
                ],
            ],
        ];
    }
    
    public function actionIndex()
    {
        return $this->render([]);
    }

    public function actionMails()
    {
        return $this->render([]);
    }

    public function actionOnline()
    {
        return $this->render([]);
    }

    public function actionTimer()
    {
        return $this->render([]);
    }

    public function actionDb()
    {
        return $this->render([]);
    }

    public function actionErrors()
    {
        return $this->render([]);
    }

    public function actionError()
    {
        $category = self::getParam('category');
        if (is_null($category)) {
            throw new Exception('Нет параметра error');
        }
        return $this->render([
            'category' => $category
        ]);
    }

    /**
     *
     */
    public function actionRegistration()
    {
        return $this->render([]);
    }
    /**
     * @return array
     */
    public function actionAjax()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        return [
            'online' => Yii::$app->onlineManager->count(),
            'rpm'    => (float)Yii::$app->formatter->asDecimal(((Yii::$app->monitoring->calculate()['request_per_second']) * 60), 1),
        ];
    }

    /**
     * Обноляет лимит пользователей онлайн
     *
     * REQUEST:
     * - limit - int
     *
     * @return array
     */
    public function actionIndexUpdateLimit()
    {
        $limit = Yii::$app->request->post('limit', '');
        if ($limit != '') {
            Yii::$app->onlineManager->setLimit($limit);
        } else {
            Yii::$app->onlineManager->setLimit(null);
        }

        return self::jsonSuccess();
    }

    /**
     *
     */
    public function actionReset()
    {
        Yii::$app->monitoring->reset();
        VarDumper::dump('Счетчик мониторинга сброшен');
    }

}
