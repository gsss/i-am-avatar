<?php

namespace school\modules\gsss\controllers;

use app\models\Article;
use app\models\Event;
use app\models\Union;
use cs\services\VarDumper;
use cs\web\Exception;
use Yii;
use yii\base\UserException;
use app\models\SiteUpdate;
use app\services\Subscribe;
class CabinetEventsController extends CabinetBaseController
{
    public function actionIndex()
    {
        return $this->render();
    }

    /**
     *
     * REQUEST :
     * -  $id int объединение gs_unions.id
     *
     *
     * @return string|\yii\web\Response
     * @throws \cs\web\Exception
     */
    public function actionAdd()
    {
        $id = self::getParam('id');
        $union = Union::find($id);
        if (is_null($union)) {
            $model = new \common\models\form\Event();
        } else {
            $model = new \common\models\form\Event(['union_id' => $union->getId()]);
        }

        if ($model->load(Yii::$app->request->post()) && $model->insert()) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        } else {
            return $this->render([
                'model' => $model,
            ]);
        }
    }

    public function actionEdit($id)
    {
        $model = \common\models\form\Event::findOne($id);
        if ($model->user_id != Yii::$app->user->id) {
            throw new UserException('Это не ваше событие');
        }

        $model->load(Yii::$app->request->post());
        if ($model->load(Yii::$app->request->post()) && $model->update()) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        } else {
            return $this->render([
                'model' => $model,
            ]);
        }
    }

    public function actionDelete($id)
    {
        $item = \common\models\form\Event::findOne($id);
        if ($item->user_id != Yii::$app->user->id) {
            throw new UserException('Это не ваше событие');
        }
        $item->delete();

        return self::jsonSuccess();
    }
}
