<?php

namespace school\modules\gsss\controllers;

use app\models\Article;
use app\models\Form\admin\User;
use app\models\Penalty;
use app\models\Penalty2;
use app\models\SiteUpdate;
use app\services\Subscribe;
use cs\Application;
use cs\services\VarDumper;
use cs\web\Exception;
use Yii;
use yii\base\UserException;

class TestController extends \cs\base\BaseController
{

    public function actionTest2()
    {
        VarDumper::dump(Application::mail('dram1008@yandex.ru','1','ss'));
    }

    public function actionIndex()
    {
        return $this->render('index');
    }

    public function actionIndex2()
    {
        VarDumper::dump(\Gsss::$app->MonitoringParams->get());
    }
}
