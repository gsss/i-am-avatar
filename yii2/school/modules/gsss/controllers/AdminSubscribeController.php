<?php

namespace school\modules\gsss\controllers;

use app\models\Subscribe;
use app\models\SubscribeHistory;
use yii\db\Query;
use yii\filters\AccessControl;
use Yii;

class AdminSubscribeController extends AdminBaseController
{
    public function actionIndex()
    {
        return $this->render([
            'items' => SubscribeHistory::query()->orderBy(['date_insert' => SORT_DESC])->all(),
        ]);
    }

    public function actionAdd()
    {
        $model = new \app\models\Form\SubscribeHistory();
        if ($model->load(Yii::$app->request->post()) && $model->insert()) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        } else {
            return $this->render([
                'model' => $model,
            ]);
        }
    }

    public function actionAddSimple()
    {
        $model = new \app\models\Form\SubscribeHistorySimple();
        if ($model->load(Yii::$app->request->post()) && $model->insert()) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        } else {
            return $this->render([
                'model' => $model,
            ]);
        }
    }

    public function actionView($id)
    {
        $item = \app\models\SubscribeHistory::find($id);

        return $this->render([
            'item' => $item->getFields(),
        ]);
    }

    public function actionEdit($id)
    {
        $model = \app\models\Form\SubscribeHistory::find($id);
        if ($model->load(Yii::$app->request->post()) && $model->update2($id)) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        } else {
            return $this->render([
                'model' => $model,
            ]);
        }
    }

    /**
     * AJAX
     * Делает рассылку
     *
     * REQUEST:
     * - id - int - идентификатор рассылки
     *
     * @return string json
     */
    public function actionSend()
    {
        $subscribe = \common\models\SubscribeHistory::findOne(self::getParam('id'));
        if (is_null($subscribe)) {
            return self::jsonErrorId(101, 'Не найдена рассылка');
        }

        /** @var \yii\swiftmailer\Mailer $mailer */
        $mailer = \Yii::$app->mailer;
        $html = $mailer->render('html/subscribe/subscribe', ['subscribeHistory' => $subscribe], 'layouts/html/subscribe');

        $s = new Subscribe([
            'html'        => $html,
            'date_insert' => time(),
            'subject'     => $subscribe->subject,
            'type'        => 2,
        ]);
        $s->save();
        $s->id = $s::getDb()->lastInsertID;

        $data = \common\models\UserSubscribe::find()->select(['email'])->column();
        $rows = [];
        foreach($data as $mail){
            $rows[] = [$mail, $s->id];
        }
        (new Query())->createCommand()->batchInsert('gs_subscribe_mails', ['mail', 'subscribe_id'],$rows)->execute();

        return self::jsonSuccess();
    }

    /**
     * AJAX
     * Делает рассылку
     *
     * REQUEST:
     * - id - int - идентификатор рассылки
     *
     * @return string json
     */
    public function actionSend2()
    {
        $subscribe = SubscribeHistory::find(self::getParam('id'));
        if (is_null($subscribe)) {
            return self::jsonErrorId(101, 'Не найдена рассылка');
        }

        /** @var \yii\swiftmailer\Mailer $mailer */
        $mailer = Yii::$app->mailer;
        $options = [
            'subscribeHistory' => $subscribe
        ];

        $provider = new \app\modules\UniSender\UniSender();
        $type = 'gsss';
        if ($type == 'gsss') {
            $sender_name = 'Галактический Союз Сил Света';
            $sender_email = 'god@galaxysss.ru';
            $list_id = 14899817;
//            $list_id = 14900005;
            $view = 'subscribe/subscribe';
            $layouts = 'layouts/html/subscribe';
            $apiKey = '6wtoqhncj5nrh4qj5zxo9tzfe1zgo9yx17dzsjro';
        } else {
            $sender_name = 'Проект «РАЙ»';
            $sender_email = 'sacred-home@new-earth.space';
            $list_id = 14870361; // Участники проекта РАЙ
            $view = 'subscribe/new-earth';
            $layouts = 'layouts/html/new-earth';
            $apiKey = '6g4p3wba1qk14u4yc84z4i7k39yqnrnfzh8z7e9a';
        }
        $html = $mailer->render('html/' . $view, $options, $layouts);
        try {
            $provider->token = $apiKey;
            $result = $provider->_call('createEmailMessage', [
                'sender_name'   => $sender_name,
                'sender_email'  => $sender_email,
                'subject'       => $subscribe->getField('subject'),
                'body'          => $html,
                'list_id'       => $list_id,
                'generate_text' => 1,
            ]);
        } catch (\Exception $e) {
            return self::jsonErrorId(101, $e->getMessage());
        }

        $message_id = $result['message_id'];
        $result = $provider->_call('createCampaign', [
            'message_id' => $message_id,
            'track_read' => 1
        ]);

        return self::jsonSuccess($result);
    }

    /**
     * AJAX
     * Делает рассылку
     *
     * REQUEST:
     * - id - int - идентификатор рассылки
     *
     * @return string json
     */
    public function actionSendMe()
    {
        $subscribe = \common\models\SubscribeHistory::findOne(self::getParam('id'));
        if (is_null($subscribe)) {
            return self::jsonErrorId(101, 'Не найдена рассылка');
        }

        /** @var \yii\swiftmailer\Mailer $mailer */
        $mailer = \Yii::$app->mailer;
        $html = $mailer->render('html/subscribe/subscribe', ['subscribeHistory' => $subscribe], 'layouts/html/subscribe');

        $s = new Subscribe([
            'html'        => $html,
            'date_insert' => time(),
            'subject'     => $subscribe->subject,
            'type'        => 2,
        ]);
        $s->save();
        $s->id = $s::getDb()->lastInsertID;

        $data = ['dram1008@yandex.ru'];
        $rows = [];
        foreach($data as $mail){
            $rows[] = [$mail, $s->id];
        }
        (new Query())->createCommand()->batchInsert('gs_subscribe_mails', ['mail', 'subscribe_id'],$rows)->execute();

        return self::jsonSuccess();
    }

    /**
     * AJAX
     * Делает рассылку
     *
     * REQUEST:
     * - id - int - идентификатор рассылки
     *
     * @return string json
     */
    public function actionSendRai()
    {
        $subscribe = SubscribeHistory::find(self::getParam('id'));
        if (is_null($subscribe)) {
            return self::jsonErrorId(101, 'Не найдена рассылка');
        }

        /** @var \yii\swiftmailer\Mailer $mailer */
        $mailer = Yii::$app->mailer;
        $options = [
            'subscribeHistory' => $subscribe
        ];

        $provider = new \app\modules\UniSender\UniSender();
        $sender_name = 'Проект «РАЙ»';
        $sender_email = 'sacred-home@new-earth.space';
        $list_id = 14870361; // Участники проекта РАЙ
        $view = 'subscribe/new-earth';
        $layouts = 'layouts/html/new-earth';
        $apiKey = '6g4p3wba1qk14u4yc84z4i7k39yqnrnfzh8z7e9a';


        $html = $mailer->render('html/' . $view, $options, $layouts);
        try {
            $provider->token = $apiKey;
            $result = $provider->_call('createEmailMessage', [
                'sender_name'   => $sender_name,
                'sender_email'  => $sender_email,
                'subject'       => $subscribe->getField('subject'),
                'body'          => $html,
                'list_id'       => $list_id,
                'generate_text' => 1,
            ]);
        } catch (\Exception $e) {
            return self::jsonErrorId(101, $e->getMessage());
        }

        $message_id = $result['message_id'];
        $result = $provider->_call('createCampaign', [
            'message_id' => $message_id,
            'track_read' => 1
        ]);

        return self::jsonSuccess($result);
    }


    /**
     * AJAX
     * Удаляет рассылку
     *
     * REQUEST:
     * - id - int - идентификатор рассылки
     *
     * @return string json
     */
    public function actionDelete()
    {
        $subscribe = \app\models\Form\SubscribeHistory::find(self::getParam('id'));
        if (is_null($subscribe)) {
            return self::jsonErrorId(101, 'Не найдена рассылка');
        }
        $subscribe->delete();

        return self::jsonSuccess();
    }
}
