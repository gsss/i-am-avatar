<?php

namespace school\modules\gsss\controllers;

use app\models\Form\Blog;
use app\models\Form\BlogFromPage;
use app\services\Subscribe;
use cs\services\VarDumper;
use cs\web\Exception;
use Yii;
use yii\base\UserException;
use yii\filters\AccessControl;
use yii\helpers\Url;
use yii\web\Controller;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use app\models\NewsItem;
use app\models\Chenneling;
use cs\base\BaseController;
use yii\web\Response;
use yii\widgets\ActiveForm;
use app\models\SiteUpdate;

class EditorBlogController extends EditorBaseController
{
    public function actionIndex()
    {
        return $this->render([]);
    }

    /**
     * AJAX
     * Добавляет site_update
     * Делает рассылку
     *
     * @param integer $id - идентификатор элемента блога
     *
     * @return string
     */
    public function actionSubscribe($id)
    {
        $item = \app\models\Blog::find($id);
        if (is_null($item)) {
            return self::jsonError(101, 'Не найдена статья');
        }
        Subscribe::add($item);
        SiteUpdate::add($item);
        $item->update(['is_added_site_update' => 1]);

        return self::jsonSuccess();
    }

    public function actionAdd()
    {
        $model = new Blog();

        if ($model->load(Yii::$app->request->post()) && ($item = $model->insert())) {
            Yii::$app->session->setFlash('contactFormSubmitted', $item->getId());

            return $this->refresh();
        } else {
            return $this->render([
                'model' => $model,
            ]);
        }
    }

    public function actionAddFromPage()
    {
        $model = new BlogFromPage();
        if ($model->load(Yii::$app->request->post()) && $model->insert()) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        } else {
            return $this->render('add-from-page', [
                'model' => $model,
            ]);
        }
    }

    public function actionEdit($id)
    {
        $model = Blog::find($id);
        $model->is_add_image = false;
        if ($model->load(Yii::$app->request->post()) && $model->update()) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        } else {
            return $this->render([
                'model' => $model,
            ]);
        }
    }

    public function actionDelete($id)
    {
        $model = Blog::find($id);
        $model->delete();

        return self::jsonSuccess();
    }
}
