<?php

namespace school\modules\gsss\controllers;

use app\models\Article;
use app\models\SiteUpdate;
use app\services\Subscribe;
use cs\services\VarDumper;
use cs\web\Exception;
use Yii;
use yii\base\UserException;

class Admin_usersController extends AdminBaseController
{

    public function actionIndex()
    {
        return $this->render([]);
    }

    public function actionEdit($id)
    {
        /** @var \app\models\User2 $model */
        $model = \app\models\User2::findOne($id);
        if ($model->load(Yii::$app->request->post()) && $model->update()) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        } else {
            return $this->render([
                'model' => $model,
            ]);
        }
    }

    public function actionEdit2($id)
    {
        /** @var \app\models\User2 $model */
        $model = \app\models\Form\Profile::find($id);
        if ($model->load(Yii::$app->request->post()) && $model->update()) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        } else {
            return $this->render([
                'model' => $model,
            ]);
        }
    }

    public function actionDelete($id)
    {
        $user = \app\models\User::find($id);
        $user->delete();

        return self::jsonSuccess();
    }
}
