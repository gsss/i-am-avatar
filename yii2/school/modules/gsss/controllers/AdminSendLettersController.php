<?php

namespace school\modules\gsss\controllers;

use app\services\Subscribe;
use common\models\SendLetter;
use cs\services\VarDumper;
use cs\web\Exception;
use Yii;
use yii\base\UserException;
use yii\filters\AccessControl;
use yii\helpers\Url;
use yii\web\Controller;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use app\models\NewsItem;
use app\models\Chenneling;
use cs\base\BaseController;
use yii\web\Response;
use yii\widgets\ActiveForm;
use app\models\SiteUpdate;


class AdminSendLettersController extends AdminBaseController
{

    public function actionIndex()
    {
        return $this->render([]);
    }

    /**
     * Выводит письмо
     *
     * @param integer $id - идентификатор послания
     *
     * @return string
     */
    public function actionView($id)
    {
        $item = SendLetter::findOne($id);
        if (is_null($item)) {
            return self::jsonError(101, 'Не найдено письмо');
        }

        return $this->render([
            'item' => $item,
        ]);
    }
}
