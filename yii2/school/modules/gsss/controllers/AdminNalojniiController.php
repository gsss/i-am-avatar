<?php

namespace school\modules\gsss\controllers;

use app\models\Shop\Nalojnii;
use Yii;
use yii\web\Response;

class AdminNalojniiController extends AdminBaseController
{
    public function actionIndex()
    {
        return $this->render([]);
    }

    public function actionAdd()
    {
        $model = new Nalojnii();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $model->time = time();
            $model->save();
            Yii::$app->session->setFlash('form');

            return $this->refresh();
        } else {
            return $this->render([
                'model' => $model,
            ]);
        }
    }

    public function actionEdit($id)
    {
        $model = Nalojnii::findOne($id);
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            Yii::$app->session->setFlash('form');
            $model->save();

            return $this->refresh();
        } else {
            return $this->render([
                'model' => $model,
            ]);
        }
    }

    public function actionDelete($id)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        Nalojnii::findOne($id)->delete();

        return self::jsonSuccess();
    }


    /**
     * Ставит флаг "выполнено"
     *
     * @param $id
     * @return \yii\web\Response
     */
    public function actionSuccess($id)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        $item = Nalojnii::findOne($id);
        if (is_null($item)) return self::jsonErrorId(101, 'не найден');
        $item->is_done = 1;
        $item->save();

        return self::jsonSuccess();
    }

    /**
     * Ставит флаг "скрыто"
     *
     * @param $id
     * @return \yii\web\Response
     */
    public function actionHide($id)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        $item = Nalojnii::findOne($id);
        if (is_null($item)) return self::jsonErrorId(101, 'не найден');
        $item->is_hide = 1;
        $item->save();

        return self::jsonSuccess();
    }

}
