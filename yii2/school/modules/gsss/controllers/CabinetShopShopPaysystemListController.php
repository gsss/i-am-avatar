<?php

namespace school\modules\gsss\controllers;

use app\common\components\Piramida;
use app\models\Article;
use app\models\Form\Shop\Order;
use app\models\Piramida\WalletSource\Custom;
use app\models\Piramida\WalletSource\Pochta;
use app\models\Piramida\WalletSource\Yandex;
use app\models\Shop;
use app\models\Shop\Product;
use app\models\Shop\Request;
use app\models\Shop\RequestMessage;
use app\models\Shop\RequestProduct;
use app\models\Union;
use app\models\User;
use app\modules\Shop\services\Basket;
use common\models\shop\PaymentConfig;
use cs\Application;
use cs\services\VarDumper;
use cs\web\Exception;
use Yii;
use yii\base\UserException;
use yii\bootstrap\Html;
use yii\db\Query;


/**
 * Обслуживает действия магазина
 *
 * Class Cabinet_shop_shopController
 * @package app\controllers
 */
class CabinetShopShopPaysystemListController extends CabinetBaseController
{
    /**
     * Выводит список доставки
     *
     * @param int $id идентификатор объединения
     * @return string
     *
     * @throws \cs\web\Exception
     */
    public function actionIndex($id)
    {
        $union = Union::find($id);
        if (is_null($union)) {
            throw new Exception('Не найдено такое объединение');
        }
        if ($union->getField('user_id') != Yii::$app->user->id) {
            throw new Exception('Вы не можете редактировать товары чужого магазина');
        }

        return $this->render([
            'union'    => $union,
        ]);
    }

    /**
     * Добавление доставки
     *
     * REQUEST:
     * - union_id - int - идентификатор объединения gs_unions.id
     * - paysystem_id - int - идентификатор платежной системы gs_unions.id
     *
     * @return string|\yii\web\Response
     * @throws \cs\web\Exception
     */
    public function actionEdit()
    {
        $union_id = self::getParam('union_id');
        $paysystem_id = self::getParam('paysystem_id');
        $fields = [
            'union_id'     => $union_id,
            'paysystem_id' => $paysystem_id,
        ];
        $union = Union::find($union_id);
        if ($union->getField('user_id') != Yii::$app->user->id) {
            throw new Exception('Вы не можете редактировать товары чужого магазина');
        }
        /** @var \common\models\shop\PaymentConfig $paymentConfig */
        $paymentConfig = PaymentConfig::findOne($fields);
        if (is_null($paymentConfig)) {
            $paymentConfig = new PaymentConfig($fields);
        }
        /** @var \common\models\PaySystem $paySystem */
        $paySystem = $paymentConfig->getPaySystem();
        $className = 'app\\modules\\Piramida\\PaySystems\\' . $paySystem->class_name . '\\Model';
        /** @var \yii\base\Model $model */
        $model = new $className;
        if ($paymentConfig->config) {
            $model->setConfig($paymentConfig->config);
        }
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $paymentConfig->config = $model->getConfig();
            $ret = $paymentConfig->save();
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        } else {
            return $this->render([
                'model' => $model,
                'union' => $union,
                'title' => $paySystem->title,
                'class' => $paySystem->class_name,
            ]);
        }
    }
}
