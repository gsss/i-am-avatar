<?php

namespace school\modules\gsss\controllers;

use app\models\Article;
use app\models\Blog;
use app\models\Praktice;
use app\models\Service;
use app\models\Union;
use app\models\UnionCategory;
use app\services\HumanDesign2;
use cs\Application;
use cs\services\Str;
use cs\services\VarDumper;
use cs\web\Exception;
use Yii;
use yii\base\UserException;
use yii\filters\AccessControl;
use yii\grid\DataColumn;
use yii\helpers\StringHelper;
use yii\helpers\Url;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use app\models\NewsItem;
use app\models\Chenneling;
use cs\base\BaseController;
use yii\web\HttpException;


class BlogController extends BaseController
{
    public $layout = 'menu';

    /** @var int количество посланий на странице */
    public $itemsPerPage = 30;

    public function actionItem($year, $month, $day, $id)
    {
        $date = $year . $month . $day;
        $pattern = '#^[a-zA-Z\d_-]+$#';
        if (!preg_match($pattern, $id)) {
            throw new BadRequestHttpException('Имеются запрещенные символы');
        }
        $item = Blog::find([
            'date'      => $date,
            'id_string' => $id
        ]);
        if (is_null($item)) {
            throw new HttpException(404, 'Нет такой статьи');
        }
        $item->incViewCounter();
        // похожие статьи
        {
            $nearList = Blog::query()
                ->select('id,header,id_string,image,view_counter,description,date_insert,date')
                ->orderBy(['date_insert' => SORT_DESC])
                ->andWhere(['not', ['id' => $item->getId()]])
                ->limit(3)
                ->all()
            ;
        }

        return $this->render([
            'item'     => $item,
            'nearList' => $nearList,
        ]);
    }

    public function actionIndex()
    {
        $itemsPerPage = $this->itemsPerPage;
        $cache = $this->renderFile('@app/views/blog/cache.php', $this->pageCluster([
            'query'     => Blog::query()
                ->orderBy(['date_insert' => SORT_DESC]),
            'paginator' => [
                'size' => $itemsPerPage
            ]
        ]));

        return $this->render(['html' => $cache]);
    }

    public function actionAjax()
    {
        $query = Blog::query()
            ->orderBy(['date_insert' => SORT_DESC]);
        $itemsPerPage = $this->itemsPerPage;
        $cache = $this->renderFile('@app/views/blog/cache_list.php', $this->pageCluster([
            'query'     => $query,
            'paginator' => [
                'size' => $itemsPerPage
            ]
        ]));

        return self::jsonSuccess($cache);
    }

    /**
     * Создает пагинацию запроса
     *
     * @param $options
     *                         [
     *                         'query' => Query
     *                         'paginator' => [
     *                         'size' => int
     *                         ]
     *                         ]
     *
     * @return array
     * [
     *    'list' =>
     *    'pages' => [
     *        'list' => [1,2,3, ...]
     *        'current' => int
     *    ]
     * ]
     */
    public function pageCluster($options)
    {
        /** @var \yii\db\Query $query */
        $query = $options['query'];
        $paginatorSize = $options['paginator']['size'];

        $page = (int)self::getParam('page', 1);
        $countAll = $query->count();

        // вычисляю количество страниц $pageCount
        $count = $countAll - $paginatorSize;
        if ($count > 0) {
            $count += $paginatorSize - 1;
            $pageCount = (int)($count / $paginatorSize);
            $pageCount++;
        }
        else {
            $pageCount = 1;
        }
        $offset = ($page - 1) * $paginatorSize;
        $pages = [];
        if ($pageCount >= 1) {
            for ($i = 1; $i <= $pageCount; $i++) {
                $pages[] = $i;
            }
        }

        return [
            'list'  => $query->limit($paginatorSize)->offset($offset)->all(),
            'pages' => [
                'list'    => $pages,
                'current' => $page,
            ],
        ];
    }
}
