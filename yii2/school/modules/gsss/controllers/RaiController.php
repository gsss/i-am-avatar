<?php

namespace school\modules\gsss\controllers;

use app\models\Article;
use app\models\Blog;
use app\models\Praktice;
use app\models\Service;
use app\models\Union;
use app\models\UnionCategory;
use app\services\HumanDesign2;
use cs\Application;
use cs\services\Str;
use cs\services\VarDumper;
use cs\web\Exception;
use Yii;
use yii\base\UserException;
use yii\filters\AccessControl;
use yii\grid\DataColumn;
use yii\helpers\StringHelper;
use yii\helpers\Url;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use app\models\NewsItem;
use app\models\Chenneling;
use cs\base\BaseController;
use yii\web\HttpException;


class RaiController extends BaseController
{
    public $layout = 'menu';

    public function actionSubscribe()
    {
        $model = new \app\models\Form\rai\Subscribe();

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            try {
                $model->action();
                Yii::$app->session->setFlash('anketa');
            } catch (\Exception $e) {
                $model->addError('name', $e->getMessage());
            }
        }

        return $this->render([
            'model' => $model,
        ]);

    }


}
