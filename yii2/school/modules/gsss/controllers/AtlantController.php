<?php

namespace school\modules\gsss\controllers;

use app\models\Article;
use app\models\Piramida\Wallet;
use app\models\SiteUpdate;
use app\models\User;
use app\services\Subscribe;
use cs\base\BaseController;
use cs\services\VarDumper;
use cs\web\Exception;
use Yii;
use yii\base\UserException;
use app\models\Piramida\Piramida;
use app\models\Piramida\WalletSource\God;
use yii\helpers\ArrayHelper;
use \yii\widgets\ActiveForm;
use \yii\web\Response;

class AtlantController extends BaseController
{
    public $layout = 'menu';

    /**
     * Форма регистрации
     *
     * @param string $code строка в формате `0000-0000-0000-0041`
     * @return mixed
     *
     * @throws \cs\web\Exception
     */
    public function actionRegistration($code)
    {
        // полуаю ID
        {
            $code = str_replace('-', '', $code);
            $code = str_replace('-', '', $code);
            $code = str_replace('-', '', $code);
            $id = (int)$code;
        }
        if (ArrayHelper::getValue(Yii::$app->params, 'isTransfere', false) == true) {
            throw new Exception(Yii::$app->params['isTransfere_string']);
        }
        $model = new \app\models\Form\Registration();

        if (Yii::$app->request->isAjax) {
            $model->setScenario('ajax');
            if ($model->load(Yii::$app->request->post())) {
                Yii::$app->response->format = Response::FORMAT_JSON;

                return ActiveForm::validate($model);
            }
        }
        $model->setScenario('insert');
        $user = User::find($id);
        if ($model->load(Yii::$app->request->post()) && $model->register($user)) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        }
        else {
            return $this->render([
                'model' => $model,
                'user'  => $user,
            ]);
        }
    }
}
