<?php

namespace school\modules\gsss\controllers;

use app\services\Subscribe;
use cs\services\VarDumper;
use cs\web\Exception;
use Yii;
use yii\base\UserException;
use yii\filters\AccessControl;
use yii\helpers\Url;
use yii\web\Controller;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use app\models\NewsItem;
use app\models\Chenneling;
use cs\base\BaseController;
use yii\web\Response;
use yii\widgets\ActiveForm;
use app\models\SiteUpdate;

class AdminChannelingController extends AdminBaseController
{

    public function actionIndex()
    {
        return $this->render([]);
    }

    /**
     * AJAX
     * Формирует данные для рассылки
     *
     * @param integer $id - идентификатор послания
     *
     * @return string
     */
    public function actionSubscribe($id)
    {
        $item = Chenneling::find($id);
        if (is_null($item)) {
            return self::jsonErrorId(101, 'Не найдено послание');
        }
        Subscribe::add($item);
        $item->update(['is_added_site_update' => 1]);
        SiteUpdate::add($item);

        return self::jsonSuccess();
    }

    /**
     * @return string|Response
     */
    public function actionAdd()
    {
        $model = new \app\models\Form\Chenneling();

        if ($model->load(Yii::$app->request->post()) && ($item = $model->insert())) {
            Yii::info(\yii\helpers\VarDumper::dumpAsString($model), 'gs\app\controllers\AdminChannelingController::actionAdd');
            Yii::$app->session->setFlash('contactFormSubmitted', $item->getId());
            \app\models\Chenneling::clearCache();

            return $this->refresh();
        } else {
            return $this->render([
                'model' => $model,
            ]);
        }
    }

    public function actionAdd_from_page()
    {
        $model = new \app\models\Form\ChennelingFromPage();
        if ($model->load(Yii::$app->request->post()) && $model->insert()) {
            Yii::$app->session->setFlash('contactFormSubmitted');
            \app\models\Chenneling::clearCache();

            return $this->refresh();
        } else {
            return $this->render([
                'model' => $model,
            ]);
        }
    }

    public function actionEdit($id)
    {
        $model = \app\models\Form\Chenneling::find($id);
        $model->is_add_image = false;

        if ($model->load(Yii::$app->request->post()) && $model->update()) {
            Yii::$app->session->setFlash('contactFormSubmitted');
            \app\models\Chenneling::clearCache();

            return $this->refresh();
        } else {
            return $this->render([
                'model' => $model,
            ]);
        }
    }

    public function actionDelete($id)
    {
        \app\models\Form\Chenneling::find($id)->delete();
        \app\models\Chenneling::clearCache();

        return self::jsonSuccess();
    }
}
