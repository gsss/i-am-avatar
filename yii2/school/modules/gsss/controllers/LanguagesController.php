<?php

namespace school\modules\gsss\controllers;

use app\controllers\AdminBaseController;
use common\models\Language;
use common\models\language\Category;
use common\models\language\Message;
use common\models\language\SourceMessage;
use cs\services\VarDumper;
use GuzzleHttp\Query;
use Yii;
use yii\base\Exception;
use yii\helpers\ArrayHelper;
use yii\web\Request;

class LanguagesController extends AdminBaseController
{
    public function actionIndex()
    {
        return $this->render('index');
    }

    public function insertCategory($category, $languages)
    {
        $sourceList = [];

        /** @var string $language */
        foreach ($languages as $language) {
            $file = Yii::getAlias('@frontend/messages/' . $language . '-' . strtoupper($language) . '/' . $category . '.php');
            if (file_exists($file)) {
                $sourceList[$language] = require($file);
            }
        }
        $index = [];
        foreach ($sourceList as $source) {
            $index = ArrayHelper::merge($index, array_keys($source));
            $index = array_unique($index);
        }
        $rows = [];
        foreach ($index as $name) {
            $ret = (new \yii\db\Query())->createCommand(Yii::$app->dbStatistic)->insert('source_message', [
                'category' => $category,
                'message'  => $name,
            ])->execute();
            if (!$ret) {
                throw new Exception('Не удалось сохранить');
            }
            $id = Yii::$app->dbStatistic->getLastInsertID();

            foreach ($languages as $language) {
                if (isset($sourceList[$language][$name])) {
                    $rows[] = [
                        $id,
                        $language,
                        $sourceList[$language][$name],
                    ];
                }
            }
        }
        (new \yii\db\Query())->createCommand(Yii::$app->dbStatistic)->batchInsert('message', ['id', 'language', 'translation'], $rows)->execute();
    }

    public function actionCategoryAdd()
    {
        $model = new Category();
        $model->load(Yii::$app->request->post());
        if ($model->load(Yii::$app->request->post())) {
            if (empty($model->code)) {
                $model->code = 'c' . Category::find()->select(['max(id) + 1 as m1'])->scalar();
            }
            if ($model->save()) {
                Yii::$app->session->setFlash('form', 1);
                return $this->refresh();
            }
        } else {
            if (empty($model->code)) {
                $model->code = 'c' . Category::find()->select(['max(id) + 1 as m1'])->scalar();
            }
        }

        return $this->render('category-add', [
            'model' => $model
        ]);
    }

    /**
     * AJAX
     * REQUEST:
     * - id - int - идентификатор категории languages_category_tree.id
     *
     * @return string|\yii\web\Response
     */
    public function actionCategoryDelete()
    {
        $id = self::getParam('id');
        $category = Category::findOne($id);
        if (is_null($category)) {
            return self::jsonErrorId(101, 'Не найдена категория');
        }
        $category->delete();

        return self::jsonSuccess();
    }

    /**
     * AJAX
     * REQUEST:
     * - int $id - идентификатор категории languages_category_tree.id
     *
     * @return string|\yii\web\Response
     */
    public function actionCategoryExport()
    {
        $id = self::getParam('id');
        /** @var \common\models\language\Category $category */
        $category = Category::findOne($id);
        if (is_null($category)) {
            return self::jsonErrorId(101, 'Не найдена категория');
        }

        $ids = $category->getMessages(true)->select('source_message.id')->column();

        $file = Yii::getAlias('@backend/views/languages/_migration.php');
        $className = 'm' . date('ymd_His') . '_lang';
        $file = Yii::$app->view->renderFile($file, [
            'className'    => $className,
            'ids'          => $ids,
            'categoryTree' => [
                'id' => $id,
                'name' => $category->name,
                'code' => $category->code,
                'nodes' => Category::getRows(['parentId' => $id, 'selectFields' => 'id, name, code']),
                'path'  => $category->getPath(),
            ],
        ]);

        return Yii::$app->response->sendContentAsFile($file, $className . '.php');
    }

    /**
     * Делает экспорт строк
     * REQUEST:
     * - ids - array - массив для экспорта source_message.id
     *
     * @return \yii\web\Response файл на скачивание
     */
    public function actionExport()
    {
        $ids = self::getParam('ids');
        $ids = explode(',', $ids);

        $file = Yii::getAlias('@app/views/languages/_migration.php');
        $className = 'm' . date('ymd_His') . '_lang';
        $file = Yii::$app->view->renderFile($file, [
            'className' => $className,
            'ids'       => $ids,
        ]);

        return Yii::$app->response->sendContentAsFile($file, $className . '.php');
    }

    /**
     * AJAX
     * Добавляет ключ (новую строку)
     * REQUEST:
     * - value - string - эта строка будет добавлена source_message.message
     * - category - string - languages_category_tree.code = source_message.category
     *
     * @return string json
     * [
     *      'status' => bool,
     *      'data'   => int - идентификатор строки добавленной,
     * ]
     */
    public function actionAddKey()
    {
        $value = self::getParam('value');
        $category = self::getParam('category');
        if (is_null($category)) {
            return self::jsonErrorId(101, 'Нет параметра category');
        }
        if (is_null($value)) {
            return self::jsonErrorId(102, 'Нет параметра value');
        }
        if (!Category::find()->where(['code' => $category])->exists()) {
            return self::jsonErrorId(103, 'Нет такой категории');
        }
        if (SourceMessage::find()->where(['category' => $category, 'message' => $value])->exists()) {
            return self::jsonErrorId(104, 'Такой ключ уже есть');
        }

        $item = new SourceMessage([
            'category' => $category,
            'message'  => $value,
        ]);
        $ret = $item->save();
        if (!$ret) {
            return self::jsonErrorId(104, 'Не удалось сохранить');
        }

        return self::jsonSuccess($item->getDb()->getLastInsertID());
    }

    /**
     * AJAX
     * Удаляет строку
     * REQUEST:
     * - id - int - идентификатор строки исходной source_message.id
     *
     * @return string json
     * [
     *      'status' => bool,
     * ]
     */
    public function actionDelete()
    {
        $id = self::getParam('id');
        $message = SourceMessage::findOne($id);
        if (is_null($message)) {
            return self::jsonErrorId(101, 'Нет такой строки');
        }
        Message::deleteAll(['id' => $message->id]);
        $message->delete();

        return self::jsonSuccess();
    }

    public function actionCopyMessages()
    {
        SourceMessage::deleteAll();
        Message::deleteAll();
        Category::deleteAll();
        $categoryList = [
            'app',
            'index',
            'menu',
            'country',
        ];
        $c = new Category();
        $c->name = 'root';
        $c->code = 'root';
        $c->save();
        $id = $c->getDb()->getLastInsertID();
        $languages = Language::find()->select(['code'])->column();
        foreach ($categoryList as $category) {
            $c = new Category();
            $c->name = $category;
            $c->code = $category;
            $c->parent_id = $id;
            $c->save();
            $this->insertCategory($category, $languages);
        }
    }

    public function actionCategory()
    {
        return $this->render('category', [
            'id' => Yii::$app->request->get('id')
        ]);
    }

    /**
     * AJAX
     * Обновляет строку
     * REQUEST:
     * - id - int - идентификатор строки `message.id`
     * - language - string - идентификатор `languages.code`
     * - value - string - новое значение для строки
     */
    public function actionUpdateString()
    {
        Yii::$app->response->format = 'json';

        $id = Yii::$app->request->post('id');
        if (is_null($id)) {
            return self::jsonErrorId(101, 'Не найдена строка');
        }
        $language = Yii::$app->request->post('language');
        if (is_null($language)) {
            return self::jsonErrorId(102, 'Не указан язык');
        }
        $value = Yii::$app->request->post('value');
        if (is_null($value)) {
            return self::jsonErrorId(103, 'Не указана строка');
        }

        $languageObject = Language::find()->where(['code' => $language])->one();
        $message = Message::find()->where([
            'id'       => $id,
            'language' => $languageObject->code,
        ])->one();
        if (is_null($message)) $message = new Message([
            'id'       => $id,
            'language' => $languageObject->code,
        ]);
        $message->translation = $value;
        $ret = $message->save();

        // сбрасываю кеш
        {
//            $sourceMessage = SourceMessage::find()->where(['id' => $id])->one();
            $sourceMessage = SourceMessage::findOne($id);
            $key = [
                'yii\\i18n\\DbMessageSource',
                $sourceMessage->category,
                $languageObject->code,
            ];
            Yii::$app->cache->delete($key);
        }

        return self::jsonSuccess();
    }

    /**
     * AJAX
     * Обновляет категорию
     *
     * REQUEST:
     * - id - integer - идентификатор категории languages_category_tree.id
     * - name - string - название languages_category_tree.name
     */
    public function actionCategorySave()
    {
        Yii::$app->response->format = 'json';

        $id = Yii::$app->request->post('id');
        if (is_null($id)) {
            return self::jsonErrorId(101, 'Нет обязательного параметра id');
        }
        $name = Yii::$app->request->post('name');
        if (is_null($name)) {
            return self::jsonErrorId(101, 'Нет обязательного параметра name');
        }
        /** @var \common\models\language\Category $category */
        $category = Category::findOne($id);
        if (is_null($category)) {
            return self::jsonErrorId(102, 'Не найдена категория');
        }
        $category->name = $name;
        $category->save();

        return self::jsonSuccess();
    }
}
