<?php

namespace school\modules\gsss\controllers;

use app\models\Article;
use app\models\Form\Shop\Order;
use app\models\Shop;
use app\models\Shop\Product;
use app\models\Shop\Request;
use app\models\Shop\RequestMessage;
use app\models\Shop\RequestProduct;
use app\modules\Shop\services\Basket;
use cs\Application;
use cs\services\VarDumper;
use cs\web\Exception;
use Yii;
use yii\base\UserException;

/**
 * Обслуживает действия клиента
 *
 * Class Cabinet_shop_clientController
 * @package app\controllers
 */
class Cabinet_shop_clientController extends CabinetBaseController
{
    public function init()
    {
        if (\yii\helpers\ArrayHelper::getValue(Yii::$app->params, 'isTransfere', false) == true) {
            throw new Exception(Yii::$app->params['isTransfere_string']);
        }
    }


    /**
     * Возврат из Яндекс Маркета
     * Редиректим на заказ
     *
     * REQUEST:
     * - id - int - идентификатора заказа
     *
     */
    public function actionRequest()
    {
        $id = self::getParam('id');

        return $this->redirect(['cabinet_shop_client/order_item', 'id' => $id]);
    }

    public function actionOrder_item($id)
    {
        $r = Request::find($id);
        if (is_null($r)) {
            throw new Exception('Заказ не найден');
        }
        if ($r->get('user_id') != Yii::$app->user->id) {
            throw new Exception('Это не ваш заказ');
        }

        return $this->render([
            'request' => $r,
        ]);
    }

    public function actionOrders()
    {
        return $this->render([
        ]);
    }

    /**
     * AJAX
     * Отправляет сообщение к заказу для клиента
     *
     * REQUEST:
     * - text - string - текст сообщения
     *
     * @param int $id идентификатор заказа gs_users_shop_requests.id
     *
     * @return \yii\web\Response json
     */
    public function actionOrder_item_message($id)
    {
        $text = self::getParam('text');
        $request = Request::find($id);
        if (is_null($request)) {
            return self::jsonErrorId(102, 'Не найден заказ');
        }
        if ($request->getField('user_id') != Yii::$app->user->id) {
            return self::jsonErrorId(101, 'Это не ваш заказ');
        }
        $request->addMessageToShop($text);
        // отправляю письмо
        Shop::mail($request->getUnion()->getShop()->getAdminEmail(), 'Заказ #'.$request->getId().'. Новое сообщение', 'shop/request_to_shop_message', [
            'request' => $request,
            'text'    => $text,
        ]);

        return self::jsonSuccess();
    }

    /**
     * AJAX
     * Отменяет заказ
     *
     * @param int $id идентификатор заказа gs_users_shop_requests.id
     *
     * @return \yii\web\Response json
     */
    public function actionOrder_item_cancel($id)
    {
        $request = Request::find($id);
        if (is_null($request)) {
            return self::jsonErrorId(102, 'Не найден заказ');
        }
        if ($request->getField('user_id') != Yii::$app->user->id) {
            return self::jsonErrorId(101, 'Это не ваш заказ');
        }
        $request->addStatusToShop(Request::STATUS_REJECTED);
        $request->update(['is_canceled' => 1]);

        return self::jsonSuccess();
    }

    /**
     * Заготовка для отправки статуса с сообщением
     *
     * REQUEST:
     * - text - string - текст сообщения
     *
     * @param int $id идентификатор заказа gs_users_shop_requests.id
     * @param int $status статус
     *
     * @return \yii\web\Response json
     */
    private function sendStatus($id, $status, \Closure $callback = null)
    {
        $text = self::getParam('text');
        $request = Request::find($id);
        if (is_null($request)) {
            return self::jsonErrorId(102, 'Не найден заказ');
        }
        if ($request->getField('user_id') != Yii::$app->user->id) {
            return self::jsonErrorId(101, 'Это не ваш заказ');
        }
        $request->addStatusToShop([
            'message' => $text,
            'status'  => $status,
        ]);
        $callback($request, $text);

        return self::jsonSuccess();
    }

    /**
     * AJAX
     * Отправляет сообщение для клиента: Заказ получен
     *
     * REQUEST:
     * - text - string - текст сообщения
     *
     * @param int $id идентификатор заказа gs_users_shop_requests.id
     *
     * @return \yii\web\Response json
     */
    public function actionOrder_item_done_somovivoz($id)
    {
        return $this->sendStatus($id, Request::STATUS_DOSTAVKA_SAMOVIVOZ_FINISH_CLIENT, function (Request $request, $text) {
            // отправляю письмо
            Shop::mail($request->getUnion()->getShop()->getAdminEmail(), 'Заказ #'.$request->getId().'. Клиент получил заказ', 'shop/request_to_shop_done_somovivoz', [
                'request' => $request,
                'text'    => $text,
            ]);
        });
    }

    /**
     * AJAX
     * Отправляет сообщение для клиента: Заказ получен
     *
     * REQUEST:
     * - text - string - текст сообщения
     *
     * @param int $id идентификатор заказа gs_users_shop_requests.id
     *
     * @return \yii\web\Response json
     */
    public function actionOrder_item_done_address($id)
    {
        return $this->sendStatus($id, Request::STATUS_DOSTAVKA_ADDRESS_FINISH_CLIENT, function (Request $request, $text) {
            // отправляю письмо
            Shop::mail($request->getUnion()->getShop()->getAdminEmail(), 'Заказ #'.$request->getId().'. Клиент получил заказ', 'shop/request_to_shop_done_address', [
                'request' => $request,
                'text'    => $text,
            ]);
        });
    }

    /**
     * AJAX
     * Отправляет сообщение для клиента: Оплата сделана
     *
     * REQUEST:
     * - text - string - текст сообщения
     *
     * @param int $id идентификатор заказа gs_users_shop_requests.id
     *
     * @return \yii\web\Response json
     */
    public function actionOrder_item_answer_pay($id)
    {
        return $this->sendStatus($id, Request::STATUS_PAID_CLIENT, function (Request $request, $text) {
            // отправляю письмо
            $request->getUnion()->getShop()->mailToMe(
                'Заказ #' . $request->getId() . '. Клиент подтвердил оплату',
                'shop/request_to_shop_answer_pay',
                [
                    'request' => $request,
                    'text'    => $text,
                ]
            );
        });
    }

}
