<?php

namespace school\modules\gsss\controllers;

use app\models\Form\PiramidaSend;
use app\models\Form\Union;
use app\models\HD;
use app\models\HdGenKeys;
use app\models\HDtown;
use app\models\Piramida\Billing;
use app\models\Piramida\InRequest;
use app\models\Piramida\OutRequest;
use app\models\Piramida\Wallet;
use app\models\Piramida\WalletSource\Yandex;
use app\models\SiteUpdate;
use app\models\User;
use app\services\Subscribe;
use common\models\piramida\BitCoinUserAddress;
use common\payment\BitCoinBlockTrailPayment;
use cs\Application;
use cs\services\SitePath;
use cs\services\VarDumper;
use cs\web\Exception;
use Yii;
use yii\base\UserException;
use yii\bootstrap\Html;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;
use yii\helpers\Url;
use yii\web\Controller;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use app\models\NewsItem;
use cs\base\BaseController;
use yii\web\Response;
use yii\widgets\ActiveForm;

class CabinetWalletBtcController extends BaseController
{
    public $layout = 'menu';

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    public function init()
    {
        if (\yii\helpers\ArrayHelper::getValue(Yii::$app->params, 'isTransfere', false) == true) {
            if ($this->action != 'logout') {
                throw new Exception(Yii::$app->params['isTransfere_string']);
            }
        }
    }

    /**
     * AJAX
     * Создает кошелек
     *
     * @return Response
     */
    public function actionCreate()
    {
        $provider = new BitCoinBlockTrailPayment();
        $id = 'user_' . Yii::$app->user->id;
        $password = 'i-am-avatar';
        /** @var \Blocktrail\SDK\WalletInterface $wallet */
        list($wallet, $primaryMnemonic, $backupMnemonic, $blocktrailPublicKeys) = $provider->getClient()->createNewWallet($id, $password);
        $address = $wallet->getNewAddress();

        // Сохраняю в список адресов
        (new BitCoinUserAddress([
            'user_id' => Yii::$app->user->id,
            'address' => $address,
        ]))->save();
        /** @var \app\models\User $user */
        $user = Yii::$app->user->identity;
        $walletConfig = $user->getBitCoinWallet();
        $walletConfig->config = json_encode([
            'id'                   => $id,
            'password'             => $password,
            'address'              => $address,
            'primaryMnemonic'      => $primaryMnemonic,
            'backupMnemonic'       => $backupMnemonic,
            'blocktrailPublicKeys' => \yii\helpers\VarDumper::dumpAsString($blocktrailPublicKeys),
            'backup'               => \yii\helpers\VarDumper::dumpAsString($wallet),
        ]);
        $walletConfig->address = $address;
        $walletConfig->identity = $id;
        $walletConfig->password = $password;
        $walletConfig->setPin('0000');
        $ret = $walletConfig->save();
        if (!$ret) {
            return self::jsonErrorId(101, 'Не удалось сохранить ' . \yii\helpers\VarDumper::dumpAsString($walletConfig->errors));
        }

        return self::jsonSuccess();
    }

    /**
     * AJAX
     * Отправляет деньги
     * REQUEST:
     * - address - string - адрес
     * - amount - string - сколько (разделитель - точка)
     * - comment - string
     * - password - string
     *
     * @return Response
     */
    public function actionSend()
    {
        $model = new PiramidaSend();
        $model->load(Yii::$app->request->post(), '');
        if (!$model->validate()) {
            return self::jsonErrorId(101, $this->convert($model->errors));
        }
        $transaction = $model->send();
        if ($transaction === false) {
            return self::jsonErrorId(101, $this->convert($model->errors));
        }

        return self::jsonSuccess([
            'address' => $transaction,
        ]);
    }

    /**
     * Отдает на скачивание чек PDF
     * REQUEST:
     * - hash - string - адрес транзакции
     *
     * @return Response
     */
    public function actionDownloadBill()
    {
        $hash = self::getParam('hash');
        $provider = new BitCoinBlockTrailPayment();
        $transaction = $provider->getClient()->transaction($hash);
        $file = Yii::getAlias('@app/views/cabinet-wallet-btc/download-bill.php');

        $content = $this->renderFile($file, ['transaction' => $transaction]);

        require \Yii::getAlias('@app/common/components/mpdf60/mpdf.php');
        $pdfDriver = new \mPDF();
        $pdfDriver->WriteHTML($content);
        $contentPdf = $pdfDriver->Output('', 'S');

        return Yii::$app->response->sendContentAsFile($contentPdf, 'atlantida_' . $hash . '.pdf');
    }

    /**
     * Конвертирует массив ошибок от формы чтобы их можно было выдать через JSON
     *
     * @param array $params
     * [
     *      'amount' => ['Не верное число',...],
     * ]
     * @return array
     * [
     *      'name'  => 'amount',
     *      'value' => ['Не верное число',...],
     * ]
     */
    private function convert($params)
    {
        $ret = [];
        foreach ($params as $name => $arr) {
            $ret[] = [
                'name'  => $name,
                'value' => $arr,
            ];
        }
        return $ret;
    }

    /**
     * AJAX
     * Создает адрес временный для приема платежа
     * REQUEST:
     *
     * @return Response json
     * [
     *      'html' => string
     * ]
     */
    public function actionIn()
    {
        /** @var \app\models\User $user */
        $user = Yii::$app->user->identity;
        $wallet = $user->getBitCoin();
        $address = $wallet->getNewAddress();
        (new BitCoinUserAddress([
            'user_id' => Yii::$app->user->id,
            'address' => $address,
        ]))->save();
        $src = 'data:image/png;base64,' . base64_encode(
                (new \Endroid\QrCode\QrCode())
                    ->setText('bitcoin:' . $address)
                    ->setSize(180)
                    ->setPadding(0)
                    ->setErrorCorrection('high')
                    ->setForegroundColor(['r' => 0, 'g' => 0, 'b' => 0, 'a' => 0])
                    ->setBackgroundColor(['r' => 255, 'g' => 255, 'b' => 255, 'a' => 0])
                    ->setLabelFontSize(16)
                    ->setImageType(\Endroid\QrCode\QrCode::IMAGE_TYPE_PNG)
                    ->get('png')
            );
        $rows = [];
        $rows[] = Html::tag('p', Html::img($src));
        $rows[] = Html::tag('p', Html::tag('code', $address));

        return self::jsonSuccess([
            'html' => join("\n", $rows),
        ]);
    }

    /**
     * Сохраняет BitCoin адрес в профиль
     *
     * REQUEST:
     * - address: string строка адреса
     *
     * @return string JSON
     */
    public function actionSaveAddress()
    {
        $address = self::getParam('address');
        /** @var \app\models\User $user */
        $user = Yii::$app->user->identity;
        $user->update(['bitcoin_address' => $address]);

        return self::jsonSuccess();
    }

    /**
     * AJAX
     * REQUEST:
     * - balance - string кол-во биткойнов в кошельке в mBTC
     */
    public function actionTransactions()
    {
        $balance = self::getParam('balance');
        if (is_null($balance)) {
            return self::jsonErrorId(101, 'Не указан параметр balance');
        }
        if ($balance == '') {
            return self::jsonErrorId(101, 'Не указан параметр balance');
        }

        /** @var \app\models\User $user */
        $user = Yii::$app->user->identity;
        $wallet = $user->getBitCoin();
        $transactions = $wallet->transactions(1, 20, 'desc');
        $file = Yii::getAlias('@app/views/cabinet-wallet-btc/transactions.php');
        $html = $this->renderFile($file, [
            'transactions'     => $transactions,
            'confirmedBalance' => $balance
        ]);

        return self::jsonSuccess([
            'html' => $html
        ]);
    }

    /**
     * AJAX
     * REQUEST:
     * -
     *
     * @return string json response
     * [
     *      'balance' => BTC
     * ]
     */
    public function actionBalanceExternal()
    {
        /** @var \app\models\User $user */
        $user = Yii::$app->user->identity;
        $provider = new BitCoinBlockTrailPayment();
        $address = $user->get('bitcoin_address');
        if (is_null($address)) {
            return self::jsonErrorId(101, 'Нет адреса');
        }
        if ($address == '') {
            return self::jsonErrorId(101, 'Нет адреса');
        }
        try {
            $addressObject = $provider->getClient()->address($address);
        } catch(\Exception $e) {
            return self::jsonErrorId(102, $e->getMessage());
        }
        $b = $addressObject['balance'] / 100000000;

        return self::jsonSuccess([
            'balance' => Yii::$app->formatter->asDecimal($b, 8)
        ]);
    }

    /**
     * AJAX
     * REQUEST:
     *
     * @return string json response
     * [
     *      'confirmedBalance'   => BTC
     *      'unconfirmedBalance' => BTC
     *      'all'                => mBTC
     * ]
     */
    public function actionBalanceInternal()
    {
        /** @var \app\models\User $user */
        $user = Yii::$app->user->identity;
        $wallet = $user->getBitCoin();
        list($confirmedBalance, $unconfirmedBalance) = $wallet->getBalance();
        $confirmedBalance1 = ($confirmedBalance + $unconfirmedBalance) / 100000;

        return self::jsonSuccess([
            'confirmedBalance'   => $confirmedBalance  / 100000000,
            'unconfirmedBalance' => $unconfirmedBalance  / 100000000,
            'all'                => $confirmedBalance1
        ]);
    }
}
