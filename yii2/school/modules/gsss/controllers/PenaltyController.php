<?php

namespace school\modules\gsss\controllers;

use app\models\Article;
use app\models\Penalty;
use app\models\SiteUpdate;
use app\services\Subscribe;
use cs\services\VarDumper;
use cs\web\Exception;
use Yii;
use yii\base\UserException;
use cs\base\BaseController;

class PenaltyController extends BaseController
{
    public $layout = 'menu';

    /**
     * @param int $id идентификатор квитанции gs_penalty.id_penalty
     *
     * @return string|\yii\web\Response
     * @throws \cs\web\Exception
     */
    public function actionPay($id)
    {
        $penalty = Penalty::findOne(['id_penalty' => $id]);
        if (is_null($penalty)) {
            throw new Exception('Не найдена квитанция');
        }

        return $this->render([
            'penalty' => $penalty,
        ]);
    }
}
