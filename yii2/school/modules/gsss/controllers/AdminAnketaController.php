<?php

namespace school\modules\gsss\controllers;

use app\models\rai\Anketa;
use app\models\Shop\Nalojnii;
use Yii;
use yii\web\Response;

class AdminAnketaController extends AdminBaseController
{
    public function actionIndex()
    {
        return $this->render([]);
    }

    public function actionView($id)
    {
        $model = Anketa::findOne($id);

        return $this->render([
            'model' => $model,
        ]);
    }
}
