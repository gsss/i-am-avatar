<?php

namespace school\modules\gsss\controllers;

use app\models\Article;
use app\models\SiteUpdate;
use app\models\validate\PenaltyGet;
use app\services\Subscribe;
use cs\services\VarDumper;
use cs\web\Exception;
use Yii;
use yii\base\Event;
use yii\base\UserException;
use \app\models\Penalty;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;

class CabinetPenaltyController extends CabinetBaseController
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@', 'agency-vozdayanie2'],
                    ],
                ],
            ],
        ];
    }

    public function actionIndex()
    {
        return $this->render();
    }

    /**
     * AJAX
     * Закрывает заявку
     *
     * REQUEST
     * - id - int - идентификатор заявки id_penalty
     *
     *
     * @return string
     *
     */
    public function actionClose()
    {
        $id = self::getParam('id');
        $item = \common\models\Penalty::findOne(['id_penalty' => $id]);
        $item->is_closed = 1;
        $item->save();

        return self::jsonSuccess();
    }

    public function actionAdd()
    {
        Event::on(Penalty::className(), Penalty::EVENT_BEFORE_INSERT, function ($event) {
            /** @var \app\models\Penalty $p */
            $p = $event->sender;
            $p->date_insert = time();
            $p->user_id = Yii::$app->user->id;

            return true;
        });

        $model = new Penalty();
        if ($model->load(Yii::$app->request->post()) && $model->insert()) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        } else {
            return $this->render([
                'model' => $model,
            ]);
        }
    }

    public function actionGet()
    {
        $model = new PenaltyGet();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $contentPdf = $model->add();

            return Yii::$app->response->sendContentAsFile($contentPdf, 'penalty.pdf', [
                'mimeType' => 'application/pdf',
            ]);
        } else {
            return $this->render([
                'model' => $model,
            ]);
        }
    }

    /**
     * Выдает на скачивание вновь сгенерированую квитанцию (пустую но с номером)
     *
     * @param int $id номер квитанции gs_penalty.penalty_id
     *
     * @return string Response FILE
     *
     * @throws Exception
     */
    public function actionDownload($id)
    {
        $item = \common\models\Penalty::findOne(['id_penalty' => $id]);
        if (is_null($item)) throw new Exception('Не найдена квитанция ' . $id);

        require \Yii::getAlias('@app/common/components/mpdf60/mpdf.php');
        $pdfDriver = new \mPDF();
        $filePath = \Yii::getAlias('@app/views/cabinet-penalty/get-html.php');
        $contentHtml = \Yii::$app->view->renderFile($filePath, ['ids' => [$id]]);
        $pdfDriver->WriteHTML($contentHtml);
        $contentPdf = $pdfDriver->Output('', 'S');

        return Yii::$app->response->sendContentAsFile($contentPdf, 'penalty' . '_' . $id . '.pdf', [
            'mimeType' => 'application/pdf',
        ]);
    }

    public function actionEdit($id)
    {
        /** @var \app\models\Penalty $model */
        $model = Penalty::findOne($id);
        if ($model->user_id != Yii::$app->user->id) {
            throw new Exception('Это не ваша квитанция');
        }

        if ($model->load(Yii::$app->request->post()) && $model->update()) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        } else {
            return $this->render([
                'model' => $model,
            ]);
        }
    }

    public function actionDelete($id)
    {
        Penalty::findOne($id)->delete();

        return self::jsonSuccess();
    }
}
