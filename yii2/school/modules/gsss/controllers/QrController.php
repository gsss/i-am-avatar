<?php

namespace school\modules\gsss\controllers;

use app\common\components\Piramida;
use app\models\Form\Event;
use app\models\Piramida\Billing;
use app\models\Piramida\Transaction;
use app\models\Piramida\WalletSource\BitCoin;
use app\models\Piramida\WalletSource\Yandex;
use app\models\Shop\Request;
use app\models\Article;
use app\models\Blog;
use app\models\Form\Shop\Order;
use app\models\Piramida\InRequest;
use app\models\Piramida\Wallet;
use app\models\Praktice;
use app\models\Service;
use app\models\Shop;
use app\models\Shop\Payments;
use app\models\Shop\Product;
use app\models\Shop\TreeNodeGeneral;
use app\models\Union;
use app\models\UnionCategory;
use app\models\User;
use app\modules\Shop\services\Basket;
use common\models\PaySystem;
use common\models\shop\DostavkaItem;
use common\models\shop\PaymentConfig;
use cs\Application;
use cs\services\Str;
use cs\services\VarDumper;
use cs\web\Exception;
use cs\Widget\FileUploadMany\FileUploadMany;
use Yii;
use yii\base\UserException;
use yii\filters\AccessControl;
use yii\grid\DataColumn;
use yii\helpers\ArrayHelper;
use yii\helpers\StringHelper;
use yii\helpers\Url;
use yii\rbac\Role;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use app\models\NewsItem;
use app\models\Chenneling;
use cs\base\BaseController;
use yii\web\HttpException;


class QrController extends BaseController
{
    public $layout = 'menu';

    public function actionAvatar()
    {
        return $this->redirect(['shop/product', 'id' => 58]);
    }

}
