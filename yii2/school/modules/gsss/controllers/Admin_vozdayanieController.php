<?php

namespace school\modules\gsss\controllers;

use app\models\Article;
use app\models\SiteUpdate;
use app\models\VozdayanieItem;
use app\services\Subscribe;
use cs\services\VarDumper;
use cs\web\Exception;
use Yii;
use yii\base\UserException;

class Admin_vozdayanieController extends AdminBaseController
{

    public function actionIndex()
    {
        return $this->render([
            'items' => VozdayanieItem::query()->orderBy(['sort_index' => SORT_DESC])->all(),
        ]);
    }

    public function actionAdd()
    {
        $model = new \app\models\Form\VozdayanieItem();
        if ($model->load(Yii::$app->request->post()) && $model->insert()) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        } else {
            return $this->render([
                'model' => $model,
            ]);
        }
    }

    public function actionEdit($id)
    {
        $model = \app\models\Form\VozdayanieItem::find($id);
        if ($model->load(Yii::$app->request->post()) && $model->update()) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        } else {
            return $this->render([
                'model' => $model,
            ]);
        }
    }

    public function actionDelete($id)
    {
        \app\models\Form\VozdayanieItem::find($id)->delete();

        return self::jsonSuccess();
    }

    /**
     * AJAX
     * Добавляет site_update
     * Делает рассылку
     *
     * @param integer $id - идентификатор новости
     *
     * @return string
     */
    public function actionSubscribe($id)
    {
        $item = Article::find($id);
        if (is_null($item)) {
            return self::jsonError(101, 'Не найдена статья');
        }
        Subscribe::add($item);
        SiteUpdate::add($item);
        $item->update(['is_added_site_update' => 1]);

        return self::jsonSuccess();
    }


    public function actionAdd_from_page()
    {
        $model = new \app\models\Form\ArticleFromPage();
        if ($model->load(Yii::$app->request->post()) && $model->insert()) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        } else {
            $model->provider = self::getParam('page');
            return $this->render([
                'model' => $model,
            ]);
        }
    }

}
