<?php

namespace school\modules\gsss\controllers;

use app\services\Subscribe;
use cs\services\VarDumper;
use cs\web\Exception;
use Yii;
use yii\base\UserException;
use yii\filters\AccessControl;
use yii\helpers\Url;
use yii\web\Controller;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use app\models\NewsItem;
use app\models\Chenneling;
use cs\base\BaseController;
use yii\web\Response;
use yii\widgets\ActiveForm;
use app\models\SiteUpdate;

class AdminGodsController extends AdminBaseController
{
    public function actionIndex()
    {
        return $this->render([]);
    }

    public function actionAdd()
    {
        $model = new \app\models\God();
        if ($model->load(Yii::$app->request->post()) && $model->insert()) {
            $model->date = gmdate('Y-m-d');
            $model->save();
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        } else {
            return $this->render([
                'model' => $model,
            ]);
        }
    }

    public function actionEdit($id)
    {
        /** @var \app\models\God $model */
        $model = \app\models\God::findOne($id);
        if ($model->load(Yii::$app->request->post()) && $model->update()) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        } else {
            return $this->render([
                'model' => $model,
            ]);
        }
    }

    public function actionDelete($id)
    {
        \app\models\God::findOne($id)->delete();

        return self::jsonSuccess();
    }

}
