<?php

namespace school\modules\gsss\controllers;

use app\models\Form\Blog;
use app\models\Form\BlogFromPage;
use app\services\Subscribe;
use common\models\form\Event;
use cs\services\VarDumper;
use cs\web\Exception;
use Yii;
use yii\base\UserException;
use yii\filters\AccessControl;
use yii\helpers\Url;
use yii\web\Controller;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use app\models\NewsItem;
use app\models\Chenneling;
use cs\base\BaseController;
use yii\web\Response;
use yii\widgets\ActiveForm;
use app\models\SiteUpdate;

class EditorEventsController extends EditorBaseController
{
    public function actionIndex()
    {
        return $this->render([]);
    }

    public function actionAdd()
    {
        $model = new Event();

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $model->insert();
            Yii::$app->session->setFlash('contactFormSubmitted', 1);

            return $this->refresh();
        } else {
            return $this->render([
                'model' => $model,
            ]);
        }
    }

    public function actionEdit($id)
    {
        $model = Event::findOne($id);
        if ($model->load(Yii::$app->request->post()) && $model->update()) {
            Yii::$app->session->setFlash('contactFormSubmitted', 1);

            return $this->refresh();
        } else {
            return $this->render([
                'model' => $model,
            ]);
        }
    }

    public function actionDelete($id)
    {
        $model = Event::findOne($id);
        $model->delete();

        return self::jsonSuccess();
    }
}
