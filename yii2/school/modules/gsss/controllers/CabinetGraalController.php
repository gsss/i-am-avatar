<?php

namespace school\modules\gsss\controllers;

use app\models\Article;
use app\models\Graal\InRequest;
use app\models\Piramida\Billing;
use app\models\SiteUpdate;
use app\models\validate\PenaltyGet;
use app\modules\Graal\components\Core;
use app\services\Subscribe;
use app\services\UsersInCache;
use cs\services\VarDumper;
use cs\web\Exception;
use Yii;
use yii\base\Event;
use yii\base\Response;
use yii\base\UserException;
use \app\models\Penalty;
use yii\bootstrap\ActiveForm;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;

class CabinetGraalController extends CabinetBaseController
{
    public function actionIndex()
    {
        return $this->render();
    }

    public function actionJoin()
    {
        return $this->render();
    }

    /**
     * Показывает все заявки на инвестиции
     *
     * @param int $id идентификатор заявки на инвестицию
     *
     * @return string
     *
     * @throws \cs\web\Exception
     */
    public function actionInRequestsMessages($id)
    {
        $request = \app\modules\Graal\models\InRequest::findOne($id);
        if ($request->user_id != Yii::$app->user->id) {
            throw new Exception('Это не ваша заявка');
        }

        return $this->render('in-requests-messages', [
            'request' => $request
        ]);
    }

    /**
     * AJAX
     * Формирует заявку на инвестицию
     *
     * REQUEST:
     * - value - float - размер инвестиции
     * - type - string - тип оплаты 'PC' / 'AC' / 'DI'
     *
     * @return string
     */
    public function actionJoinAjax()
    {
        $price = self::getParam('value');
        /** @var \app\modules\Graal\components\Core $graal */
        $graal = Yii::$app->graal;
        $request = \app\modules\Graal\models\InRequest::add([
            'user_id'    => Yii::$app->user->id,
            'price'      => $price,
            'billing_id' => Billing::add([
                'wallet_id'  => $graal->getWalletId(Core::TYPE_MAIN),
                'sum_before' => $price,
                'type'       => Billing::TYPE_IN_GRALL,
            ])->id,
        ]);
        $request->addStatusToShop(\app\modules\Graal\models\InRequest::STATUS_SEND);
        if (self::getParam('type') == 'DI') {
            $request->addMessageToClient(join("\n", Yii::$app->graal->getPayments()));
        }

        return self::jsonSuccess($request->id);
    }

    /**
     * Показывает все заявки на инвестиции
     *
     * @return string
     */
    public function actionInRequests()
    {
        return $this->render('in-requests');
    }


    /**
     * Отправляет сообщение в чат
     *
     * REQUEST:
     * - message - string - сообщение
     *
     * @param int $id идентификатор заявки на инвестицию
     *
     * @return string
     * @throws \cs\web\Exception
     */
    public function actionMessage($id)
    {
        $request = \app\modules\Graal\models\InRequest::findOne($id);
        if ($request->user_id != Yii::$app->user->id) {
            throw new Exception('Это не ваша заявка');
        }
        $message = self::getParam('message');
        $messageObject = $request->addMessageToShop($message);

//        \cs\Application::mail($request->getOwner()->getEmail(), 'Сообщение по инвестиции #'.$request->id, 'graal/out-request-to-client', [
//            'request' => $this,
//            'message' => $messageObject,
//        ]);

        return self::jsonSuccess([
            'html' => $this->renderPartial('@app/views/cabinet-graal-finance/in-requests-message.php', [
                'message' => $messageObject,
                'user'    => UsersInCache::find(Yii::$app->user->id),
            ]),
        ]);
    }
}
