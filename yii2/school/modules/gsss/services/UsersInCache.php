<?php
/**
 * Класс отвечает за хранение часто необходимых данных пользователей в кеше
 * Хранимые поля:
 * - id
 * - name
 * - avatar
 * - email
 *
 * Хранимые поля задаются в self::getUserData()
 *
 * Лучшая функция self::find($id) Возвращает данные пользователя. Если их нет в кеше то они будут найдены в БД и закешированы
 */

namespace app\services;

use Yii;
use app\models\User;
use yii\base\Exception;

class UsersInCache
{
    const CACHE_PREFIX = 'UsersInCache/';

    public static $fields = [
        'id',
        'avatar',
        'email',
        'name_first',
        'name_last',
    ];

    /**
     * Возвращает данные пользователя если они есть в кеше
     *
     * @param $id
     *
     * @return false|array
     * [
     *      'id' => идентификатор пользователя
     *      'name' => Имя и фамилия
     *      'avatar' => '/' относительный путь к аватару
     * ]
     */
    public static function get($id)
    {
        return Yii::$app->cache->get(self::CACHE_PREFIX . $id);
    }

    /**
     * Возвращает данные пользователя. Если их нет в кеше то они будут найдены в БД и закешированы
     *
     * @param $id
     *
     * @return array
     * [
     *      'id' => идентификатор пользователя
     *      'name' => Имя и фамилия
     *      'avatar' => '/' относительный путь к аватару
     * ]
     *
     * @throws \yii\base\Exception
     */
    public static function find($id)
    {
        $data = Yii::$app->cache->get(self::CACHE_PREFIX . $id);
        if ($data !== false) return $data;
        $user = User::find($id);
        if (is_null($user)) {
            return null;
        }
        $data = self::saveUser($user);

        return $data;
    }

    /**
     * Сохраняет текущего пользователя в кеш
     */
    public static function saveMe()
    {
        /** @var \app\models\User $user */
        $user = Yii::$app->user->identity;
        self::saveUser($user);
    }

    /**
     * @param \app\models\User $user
     * @return array
     * [
     *      'id' => идентификатор пользователя
     *      'name' => Имя и фамилия
     *      'avatar' => '/' относительный путь к аватару
     * ]
     */
    public static function getUserData($user)
    {
        return [
            'id'     => $user->getId(),
            'name'   => $user->getField('name_first') . ' ' . $user->getField('name_last'),
            'avatar' => $user->getAvatar(),
            'email'  => $user->getEmail(),
        ];
    }

    /**
     * Сохраняет пользователя в кеше
     * @param array $data
     */
    public static function save($data)
    {
        Yii::$app->cache->set(self::CACHE_PREFIX . $data['id'], $data);
    }

    /**
     * Сохраняет пользователя в кеше
     * @param \app\models\User $user
     * @return array
     * [
     *      'id' => идентификатор пользователя
     *      'name' => Имя и фамилия
     *      'avatar' => '/' относительный путь к аватару
     * ]
     */
    public static function saveUser($user)
    {
        $data = self::getUserData($user);
        self::save($data);

        return $data;
    }

    /**
     *
     * @param int $id
     */
    public static function clear($id)
    {
        Yii::$app->cache->delete(self::CACHE_PREFIX . $id);
    }
}