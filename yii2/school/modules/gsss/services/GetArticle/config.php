<?php


return [
    [
        'name'  => 'tasachena',
        'title' => 'Тасачена.org',
        'class' => 'app\services\GetArticle\Tasachena',
        'find'  => 'tasachena.org',
    ],
    [
        'name'  => 'aum-news',
        'title' => 'aum.news',
        'class' => 'app\services\GetArticle\AumNews',
        'find'  => 'aum.news',
    ],
    [
        'name'  => 'channeling-vsem',
        'title' => 'channelingvsem.com',
        'class' => 'app\services\GetArticle\ChannelingVsem',
        'find'  => 'channelingvsem.com',
    ],
    [
        'name'  => 'verhosvet',
        'title' => 'Verhosvet.org',
        'class' => 'app\services\GetArticle\Verhosvet',
        'find'  => 'verhosvet.org',
    ],
    [
        'name'  => 'youtube',
        'title' => 'YouTube.com',
        'class' => 'app\services\GetArticle\YouTube',
        'find'  => 'youtube.com',
    ],
    [
        'name'  => 'vk-midway',
        'title' => 'vk.com Midway',
        'class' => 'app\services\GetArticle\VkMidway',
        'find'  => 'vk.com',
    ],
    [
        'name'  => 'chenneling',
        'title' => 'chenneling.net',
        'class' => 'app\services\GetArticle\Chenneling',
        'find'  => 'chenneling.net',
    ],
    [
        'name'  => 'bcoreanda',
        'title' => 'bcoreanda.com',
        'class' => 'app\services\GetArticle\Bcoreanda',
        'find'  => 'bcoreanda.com',
    ],
    [
        'name'  => 'otkroveniya',
        'title' => 'Otkroveniya',
        'class' => 'app\services\GetArticle\Otkroveniya',
        'find'  => 'otkroveniya.ru',
    ],
];