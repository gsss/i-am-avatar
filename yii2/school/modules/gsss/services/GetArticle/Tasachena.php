<?php

namespace app\services\GetArticle;

use cs\services\VarDumper;
use Yii;

use yii\helpers\Html;
use cs\services\Security;
use yii\helpers\StringHelper;
use cs\services\Str;
use app\services\GsssHtml;

/**
 * Class VkMidway
 * @package app\services\GetArticle
 *
 * ```php
 * $array = (new \app\services\GetArticle\VkMidway('https://vk.com/wall-84190266_337'))->extract();
 * ```
 */
class Tasachena extends Base implements ExtractorInterface
{
    /** @var  string */
    private $image;

    /** @var  string */
    private $content;

    /**
     * Возвращает информацию о статье
     *
     * @return array
     * [
     *     'image'   => $this->getImage(),
     *     'header'  => $this->getHeader(),
     *     'content' => $this->getContent(),
     * ];
     */
    public function extract()
    {
        return [
            'image'       => $this->getImage(),
            'header'      => $this->getHeader(),
            'content'     => $this->getContent(),
            'description' => $this->getDescription(),
        ];
    }

    public function html()
    {
        return $this->getContent();
    }

    public function getContent()
    {
        if (is_null($this->content)) {
            $this->content = $this->_getContent();
        }

        return $this->content;
    }

    public function _getContent()
    {
        $html = $this->getDocument()->find('article/div.item-content')[0];

        $arr = [];
        $arr[] = Html::tag('p', Html::img($this->getImage(), [
            'width' => '100%',
            'class' => 'thumbnail',
        ]));
        foreach ($html->find('*') as $item) {
            if ($item instanceof \simple_html_dom_node) {
                $isAdd = true;
                if ($item->tag == 'div') {
                    if (strpos($item->attr['class'], 'essb_links') !== false) {
                        break;
                    }
                    if ($item->attr['class'] == 'advads-begunu' || $item->attr['class'] == 'advads-statye') {
                        $isAdd = false;
                    }
                }
                if (StringHelper::startsWith($item->text(), 'Публикация: ')) $isAdd = false;
                if ($isAdd) $arr[] = $item->outertext();
            }
        }

        return join("\n", $arr);
    }

    /**
     * Возвращает название статьи
     *
     * @return string
     */
    public function getHeader()
    {
        $a = $this->getDocument()->find('div.blog-heading/h1');

        return $a[0]->plaintext;
    }

    /**
     * Возвращает картинку для статьи
     * Если картинки нет то будет возвращено null
     *
     * @return string | null
     */
    public function _getImage()
    {
        $a = $this->getDocument()->find('div#body')[0];
        $a = $a->find('div#post-thumb');
        if (count($a) == 0) return null;
        $a = $a[0];
        $a = $a->find('img');

        return $a[0]->attr['src'];
    }

    /**
     * Возвращает картинку для статьи
     * Если картинки нет то будет возвращено null
     *
     * @return string | null
     */
    public function getImage()
    {
        if (is_null($this->image)) {
            $this->image = $this->_getImage();
        }

        return $this->image;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return GsssHtml::getMiniText($this->getContent());
    }

} 