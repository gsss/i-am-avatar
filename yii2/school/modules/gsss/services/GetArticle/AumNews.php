<?php

namespace app\services\GetArticle;

use cs\services\Url;
use cs\services\VarDumper;
use Yii;

use yii\helpers\Html;
use cs\services\Security;
use yii\helpers\StringHelper;
use cs\services\Str;
use app\services\GsssHtml;

/**
 * Class Bcoreanda
 * @package app\services\GetArticle
 *
 * ```php
 * $array = (new \app\services\GetArticle\Bcoreanda('http://chenneling.net/chennelingi/poslanie-salusa-s-siriusa-ot-4-sentyabrya-2015-goda.html'))->extract();
 * ```
 */
class AumNews extends Base implements ExtractorInterface
{
    /** @var  string */
    private $content;

    /**
     * Возвращает информацию о статье
     *
     * @return array
     * [
     *     'image'   => $this->getImage(),
     *     'header'  => $this->getHeader(),
     *     'content' => $this->getContent(),
     * ];
     */
    public function extract()
    {
        return [
            'image'       => $this->getImage(),
            'header'      => $this->getHeader(),
            'content'     => $this->getContent(),
            'description' => $this->getDescription(),
        ];
    }

    public function html()
    {
        return $this->getContent();
    }

    public function getContent()
    {
        if (is_null($this->content)) {
            $this->content = $this->_getContent();
        }

        return $this->content;
    }

    private function _getContent()
    {
        $html = $this->getDocument()->find('section.article-content/*');
        $ret = [];
        foreach ($html as $i) {
            if ($i->tag == 'div') {
                if (strpos($i->attr['class'], 'jllikeproSharesContayner') !== false) continue;
            }
            $ret[] = $i->outertext();
        }
        $html = join("\n", $ret);

        $contentObject = str_get_html($html);
        foreach ($contentObject->find('img') as $item) {
            $item->attr['width'] = '100%';
            $src = $item->attr['src'];
            if (!StringHelper::startsWith($src, 'http')) {
                $src = 'http://www.aum.news' . $src;
                $item->attr['src'] = $src;
            }
            unset($item->attr['height']);
        }

        return $contentObject->find('root')[0]->innertext();
    }

    /**
     * Возвращает название статьи
     *
     * @return string
     */
    public function getHeader()
    {
        $a = $this->getDocument()->find('header');
        Yii::info(\yii\helpers\VarDumper::dumpAsString($a, 2), 'gs\\aum::getHeader()::article');
        $h1 = $a[0]->children[0];
        $text = $h1->nodes[0];
        Yii::info(\yii\helpers\VarDumper::dumpAsString($text, 2), 'gs\\aum::getHeader()::article');
        $arr = $text->_;
        Yii::info(\yii\helpers\VarDumper::dumpAsString($arr), 'gs\\aum::getHeader()::article');
        $text = join(' ', $arr);
        $text = trim($text);

        return $text;
    }

    /**
     * Возвращает картинку для статьи
     * Если картинки нет то будет возвращено null
     *
     * @return string | null
     */
    public function getImage()
    {
        $html = $this->getDocument()->find('.article-image-full');
        $span = $html[0]->children[0];
        $img = $span->children[0];
        $src = $img->attr['src'];
        $src = 'http://www.aum.news/' . $src;

        return $src;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return GsssHtml::getMiniText($this->getContent());
    }

} 