<?php

namespace app\services\GetArticle;

use cs\services\VarDumper;
use Yii;

use yii\helpers\Html;
use cs\services\Security;
use yii\helpers\StringHelper;
use cs\services\Str;
use app\services\GsssHtml;

/**
 * Class Verhosvet
 * @package app\services\GetArticle
 *
 * ```php
 * $array = (new \app\services\GetArticle\Verhosvet('http://verhosvet.org/2015/05/poslanie-arkturianskoj-gruppy-ot-10-maya-2015-goda/'))->extract();
 * ```
 */
class Verhosvet extends Base implements ExtractorInterface
{
    /** @var  \simple_html_dom_node */
    private $article;

    /** @var  string */
    private $content;

    /**
     * Возвращает информацию о статье
     *
     * @return array
     * [
     *     'image'   => $this->getImage(),
     *     'header'  => $this->getHeader(),
     *     'content' => $this->getContent(),
     * ];
     */
    public function extract()
    {
        return [
            'image'       => $this->getImage(),
            'header'      => $this->getHeader(),
            'content'     => $this->getContent(),
            'description' => $this->getDescription(),
        ];
    }

    public function html()
    {
        return $this->getContent();
    }

    public function getContent()
    {
        if (is_null($this->content)) {
            $arr = $this->getContentAsArray();

            $this->content = join("\r\n", $arr);
        }

        return $this->content;
    }

    /**
     * Возвращает массив чистых строк без тегов
     *
     * @return array
     */
    public function getContentAsArray()
    {
        $html = $this->getObjArticle();
        $ret = [];
        // добавление видео
        {
            $result = $html->find('div.wpb_video_wrapper');
            if (count($result) > 0) {
                if (count($result[0]->children) > 0) {
                    $tagObject = $result[0]->children[0];
                    if ($tagObject->tag == 'iframe') {
                        $tagObject->attr['src'] = str_replace('http://', 'https://', $tagObject->attr['src']);
                        $ret[] = Html::tag('p', Html::tag('iframe', null, array_merge($tagObject->attr, ['width' => '100%'])));
                    }
                }
            }
        }
        // добавление текста
        {
            foreach ($html->find('div.td-post-text-content/*') as $tag) {
                if ($tag instanceof \simple_html_dom_node) {
                    if ($tag->tag == 'div') {
                        if (strpos($tag->attr['class'], 'td-a-rec-id-content_inline') !== false) {
                            continue;
                        }
                        if (strpos($tag->attr['class'], 'td-g-rec-id-content_bottom') !== false) {
                            continue;
                        }
                        if (strpos($tag->attr['class'], 'td-g-rec-id-content_top') !== false) {
                            continue;
                        }
                        if (strpos($tag->attr['id'], 'jp-relatedposts') !== false) {
                            continue;
                        }
                    }
                    if ($tag->tag == 'script') continue;
                    // убирает первые &nbsp;
                    if ($tag->tag == 'p') {
                        if (count($tag->children) > 0) {
                            $tag1 = $tag->children[0];
                            $innerText = trim($tag1->text());
                            if (substr($innerText, 0, 6) == '&nbsp;') {
                                if ($tag1->nodes) {
                                    $tag1 = $tag1->nodes[0];
                                    $indexArray = array_keys($tag1->_);
                                }
                                $new = [];
                                foreach ($indexArray as $index) {
                                    $str = $tag1->_[$index];
                                    if (substr($str, 0, 6) == '&nbsp;') {
                                        $str = substr($str, 6);
                                    }
                                    $new[$index] = $str;
                                }
                                $tag1->_ = $new;
                            }
                        }
                    }
                    if (count($tag->children) > 0) {
                        if ($tag->children[0]->tag == 'noindex') continue;
                        if ($tag->children[0]->tag == 'span') {
                            $tag->children[0]->attr['style'] = "";
                        }
                    }
                    $ret[] = $tag->outertext();
                }
            }
        }

        return $ret;
    }

    /**
     * Возвращает объект статьи
     *
     * @return \simple_html_dom_node
     */
    public function getObjArticle()
    {
        if (is_null($this->article)) {
            $this->article = $this->getDocument()->find('article')[0];
        }

        return $this->article;
    }

    /**
     * Возвращает название статьи
     *
     * @return string
     */
    public function getHeader()
    {
        $html = $this->getObjArticle();

        return $html->find('header/h1')[0]->plaintext;
    }

    /**
     * Возвращает картинку для статьи
     * Если картинки нет то будет возвращено null
     *
     * @return string | null
     */
    public function getImage()
    {
        $meta = $this->getDocument()->find('html/head/meta[property="og:image"]');
        if (is_null($meta)) return null;
        $image = $meta[0]->attr['content'];

        return $image;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return GsssHtml::getMiniText($this->getContent());
    }

} 