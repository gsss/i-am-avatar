<?php

namespace app\services\GetArticle;

use cs\services\Url;
use cs\services\VarDumper;
use Yii;

use yii\helpers\Html;
use cs\services\Security;
use yii\helpers\StringHelper;
use cs\services\Str;
use app\services\GsssHtml;

/**
 * Class ChannelingVsem
 * @package app\services\GetArticle
 *
 * ```php
 * $array = (new \app\services\GetArticle\ChannelingVsem('http://channelingvsem.com/2017/01/15/akashi-dostupny-dlya-vsex/'))->extract();
 * ```
 */
class ChannelingVsem extends Base implements ExtractorInterface
{
    /** @var  string */
    private $content;

    /**
     * Возвращает информацию о статье
     *
     * @return array
     * [
     *     'image'   => $this->getImage(),
     *     'header'  => $this->getHeader(),
     *     'content' => $this->getContent(),
     * ];
     */
    public function extract()
    {
        return [
            'image'       => $this->getImage(),
            'header'      => $this->getHeader(),
            'content'     => $this->getContent(),
            'description' => $this->getDescription(),
        ];
    }

    public function html()
    {
        return $this->getContent();
    }

    public function getContent()
    {
        if (is_null($this->content)) {
            $this->content = $this->_getContent();
        }

        return $this->content;
    }

    private function _getContent()
    {
        $firstIsImg = false;
        $html = $this->getDocument()->find('article/div.entry-content/*');
        /** @var \simple_html_dom_node $n */
        $n = $html[0];
        $result = $n->find('img.size-full');
        if (count($result) == 1) $firstIsImg = true;
        $ret = [];
        $c = 1;
        foreach ($html as $i) {
            if ($c == 1) {
                if ($firstIsImg) {
                    $c++;
                    continue;
                }
            }
            $html = $i->outertext();
            if (StringHelper::startsWith($html, '<p>Источник:')) break;
            if (StringHelper::startsWith($html, '<p>Публикация:')) break;
            $ret[] = $html;
            $c++;
        }
        $html = join("\n", $ret);

        return $html;
    }

    /**
     * Возвращает название статьи
     *
     * @return string
     */
    public function getHeader()
    {
        $a = $this->getDocument()->find('header/h1');
        $h1 = $a[0]->nodes[0];
        $arr = $h1->_;
        $text = join(' ', $arr);
        $text = trim($text);
        $text = rtrim($text, '.');

        return $text;
    }

    /**
     * Возвращает картинку для статьи
     * Если картинки нет то будет возвращено null
     *
     * @return string | null
     */
    public function getImage()
    {
        $html = $this->getDocument()->find('article/div.entry-content/img.size-full');
        $img = $html[0];
        $src = $img->attr['src'];

        return $src;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return GsssHtml::getMiniText($this->getContent());
    }

} 