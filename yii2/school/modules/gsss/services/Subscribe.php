<?php


namespace app\services;

use app\models\SiteContentInterface;
use app\models\SubscribeMail;
use app\models\SubscribeMailItem;
use app\models\User;
use cs\services\VarDumper;
use yii\db\Query;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;
use yii\helpers\Url;
use yii\httpclient\Client;

class Subscribe
{
    const TYPE_NEWS        = 1;
    const TYPE_SITE_UPDATE = 2;
    const TYPE_MANUAL      = 3;

    /** @var array Список полей для хранения информации о типах подписки */
    public static $userFieldList = [
        'subscribe_is_news',
        'subscribe_is_manual',
        'subscribe_is_after_insert',
    ];

    /**
     * Добавляет записи для рассылки в таблицу рассылки
     *
     * @param SiteContentInterface | \app\models\SubscribeItem $item тема письма
     *
     * @return array
     * /docs/subscribe-index.md # Рассылка через внешний сервер
     */
    public static function add($item)
    {
        if ($item instanceof SiteContentInterface) {
            $subscribeItem = $item->getMailContent();
        } else {
            $subscribeItem = $item;
        }
        $where = [];
        switch ($subscribeItem->type) {
            case self::TYPE_NEWS:
                $where = ['subscribe_is_news' => 1];
                break;
            case self::TYPE_SITE_UPDATE:
                $where = ['subscribe_is_after_insert' => 1];
                break;
            case self::TYPE_MANUAL:
                $where = ['subscribe_is_manual' => 1];
                break;
        }

        $emailList = User::query($where)
            ->select('email')
            ->andWhere(['not', ['email' => null]])
            ->andWhere([
                'is_active'  => 1,
                'is_confirm' => 1,
            ])
            ->column();

        $data = [
            'mailList'       => $emailList,
            'from'           => [
                'name' => 'Галактический Союз Сил Света',
                'mail' => 'no-reply@galaxysss.com',
            ],
            'subject'        => $subscribeItem->subject,
            'unSubscribeUrl' => 'https://www.galaxysss.com',
            'type'           => $subscribeItem->type,
            'mail'           => [],
        ];
        if ($subscribeItem->html) {
            $data['mail']['html'] = $subscribeItem->html;
        }
        if ($subscribeItem->html) {
            $data['mail']['text'] = $subscribeItem->text;
        }

        (new Client(['baseUrl' => 'http://service.galaxysss.com']))->post('subscribe/add', [ 'data' => Json::encode($data) ])->send();

        return $data;
    }

    /**
     * Генерирует hash для ссылки отписки
     *
     * @param string $email почта клиента
     * @param integer $type тип рассылки \app\services\Subscribe::TYPE_*
     *
     * @return string
     *
     */
    public static function hashGenerate($email, $type)
    {
        return md5($email . '_' . $type);
    }

    /**
     * Проверяет hash для ссылки отписки
     *
     * @param string $email почта клиента
     * @param integer $type имп рассылки \app\services\Subscribe::TYPE_*
     * @param integer $hash
     *
     * @return boolean
     * true - верный
     * false - не верный
     */
    public static function hashValidate($email, $type, $hash)
    {
        return md5($email . '_' . $type) == $hash;
    }
}