<?php
/**
 * Facebook
 */

namespace app\services\authclient;


use app\models\User;
use cs\helpers\Html;
use cs\services\VarDumper;
use yii\authclient\widgets\AuthChoiceItem;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;

class FacebookWidget extends AuthChoiceItem
{
    public $options;

    public function run()
    {
        $options = ($this->options)? $this->options : [];
        $options = ArrayHelper::merge($options, [
            'class' => 'btn btn-block btn-social btn-facebook',
        ]);

        return Html::a(
            Html::tag('i', null, ['class' => 'fa fa-facebook']) . ' Войти через Facebook',
            Url::to(ArrayHelper::merge($this->authChoice->baseAuthUrl, ['authclient' => $this->client->getName()])),
            $options
        );
    }
}