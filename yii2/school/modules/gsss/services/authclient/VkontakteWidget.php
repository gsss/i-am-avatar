<?php
/**
 * Facebook
 */

namespace app\services\authclient;


use app\models\User;
use cs\helpers\Html;
use cs\services\VarDumper;
use yii\authclient\widgets\AuthChoiceItem;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;

class VkontakteWidget extends AuthChoiceItem
{
    public $options;

    public function run()
    {
        $options = ($this->options)? $this->options : [];
        $options = ArrayHelper::merge($options, [
            'class' => 'btn btn-block btn-social btn-vk',
        ]);

        return Html::a(
            Html::tag('i', null, ['class' => 'fa fa-vk']) . ' Войти через Вконтакте',
            Url::to(ArrayHelper::merge($this->authChoice->baseAuthUrl, ['authclient' => $this->client->getName()])),
            $options
        );
    }
}