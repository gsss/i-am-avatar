<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;
use app\models\UnionCategory;
use yii\db\Query;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \app\models\Form\Blog */

$this->title = 'Добавить статью в блог';
?>
<div class="container">
    <div class="col-lg-12">
        <h1 class="page-header"><?= Html::encode($this->title) ?></h1>

        <?php if (Yii::$app->session->hasFlash('contactFormSubmitted')): ?>

            <div class="alert alert-success">
                Успешно добавлено.
            </div>
            <?php
            $this->registerJs(<<<JS
$('.buttonAddSiteUpdate').click(function (e) {
    e.preventDefault();
    e.stopPropagation();
    var button = $(this);
    var id = $(this).data('id');
    ajaxJson({
        url: '/admin/blog/' + id + '/subscribe',
        success: function (ret) {
            infoWindow('Успешно', function() {
                button.remove();
            });
        }
    });
});
JS
            );
            ?>
            <button class="btn btn-info buttonAddSiteUpdate" data-id="<?= $model->id ?>">Сделать рассылку</button>
            <a class="btn btn-info" href="/admin/blog/<?= $model->id ?>/edit">Вернуться к редактированию</a>
            <a class="btn btn-info" href="/admin/blog">Список статей</a>
        <?php else: ?>


            <div class="row">
                <div class="col-lg-5">
                    <?php $form = ActiveForm::begin([
                        'id'      => 'contact-form',
                        'options' => ['enctype' => 'multipart/form-data']
                    ]); ?>
                    <?= $model->field($form, 'header') ?>
                    <?= $model->field($form, 'source') ?>
                    <?= $model->field($form, 'description')->textarea(['rows' => 5]) ?>
                    <?= $model->field($form, 'content') ?>
                    <?= $model->field($form, 'image') ?>
                    <?= $model->field($form, 'is_add_image') ?>
                    <?= $model->field($form, 'tree_node_id_mask') ?>

                    <hr>
                    <div class="form-group">
                        <?= Html::submitButton('Добавить', [
                            'class' => 'btn btn-default',
                            'name'  => 'contact-button',
                            'style' => 'width:100%',
                        ]) ?>
                    </div>
                    <?php ActiveForm::end(); ?>
                </div>
            </div>
        <?php endif; ?>
    </div>
</div>
