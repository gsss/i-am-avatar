<?php

/** $this \yii\web\View  */

use yii\helpers\Url;
use app\services\GsssHtml;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use app\services\GetArticle\Collection;

$this->title = 'Блог';

?>

<div class="container">
    <div class="col-lg-12">
        <h1 class="page-header"><?= Html::encode($this->title) ?></h1>
        <div class="btn-group" style="margin-bottom: 20px;">
            <a href="<?= Url::to(['admin_blog/add']) ?>" class="btn btn-default">Добавить</a>
            <a href="<?= Url::to(['admin_blog/add_from_page']) ?>" class="btn btn-default">со страницы</a>
        </div>

        <?php \yii\widgets\Pjax::begin(); ?>

        <?php
        $this->registerJS(<<<JS

$('.gsssTooltip').tooltip();

$('.buttonDelete').click(function (e) {
    e.preventDefault();
    e.stopPropagation();
    if (confirm('Подтвердите удаление')) {
        var button = $(this);
        var id = $(this).data('id');
        ajaxJson({
            url: '/admin/blog/' + id + '/delete',
            success: function (ret) {
                infoWindow('Успешно', function() {
                    button.parent().parent().remove();
                });
            }
        });
    }
});
$('.buttonAddSiteUpdate').click(function (e) {
    e.preventDefault();
    e.stopPropagation();
    var button = $(this);
    var id = $(this).data('id');
    ajaxJson({
        url: '/admin/blog/' + id + '/subscribe',
        success: function (ret) {
            infoWindow('Успешно', function() {
                button.remove();
            });
        }
    });
});

$('.rowTable').click(function() {
    window.location = '/admin/blog/' + $(this).data('id') + '/edit';
});
JS
        );

        $sort = new \yii\data\Sort([
            'attributes'   => [
                'id'          => [
                    'asc'     => ['id' => SORT_ASC],
                    'desc'    => ['id' => SORT_DESC],
                    'default' => SORT_ASC,
                    'label'   => 'ID',
                ],
                'date_insert' => [
                    'asc'     => ['date_insert' => SORT_ASC],
                    'desc'    => ['date_insert' => SORT_DESC],
                    'default' => SORT_ASC,
                    'label'   => 'Создано',
                ],
                'header'      => [
                    'asc'     => ['header' => SORT_ASC],
                    'desc'    => ['header' => SORT_DESC],
                    'default' => SORT_ASC,
                    'label'   => 'Header',
                ],
            ],
            'defaultOrder' => [
                'date_insert' => SORT_DESC,
            ],
        ]);
        $model = new \app\models\search\Blog();
        $provider = $model->search(Yii::$app->request->get(), null, $sort)
        ?>
        <?= \yii\grid\GridView::widget([
            'dataProvider' => $provider,
            'filterModel'  => $model,
            'tableOptions' => [
                'class' => 'table table-striped table-hover',
            ],
            'rowOptions'   => function ($item) {
                return [
                    'data'  => ['id' => $item['id']],
                    'role'  => 'button',
                    'class' => 'rowTable',
                ];
            },
            'columns'      => [
                [
                    'attribute'     => 'id',
                    'headerOptions' => [
                        'style' => 'width: 10%',
                    ],
                ],
                [
                    'header'        => 'Картинка',
                    'headerOptions' => [
                        'style' => 'width: 10%',
                    ],
                    'content'       => function ($item) {
                        $i = ArrayHelper::getValue($item, 'image', '');
                        if ($i == '') return '';

                        return Html::img($i, [
                            'class'  => "thumbnail",
                            'width'  => 80,
                            'height' => 80,
                            'style'  => 'margin-bottom: 0px;',
                        ]);
                    },
                ],
                [
                    'attribute'     => 'header',
                    'headerOptions' => [
                        'style' => 'width: 40%',
                    ],
                ],
                [
                    'header'        => 'Создано',
                    'headerOptions' => [
                        'style' => Html::cssStyleFromArray([
                            'width' => '20%',
                        ]),
                    ],
                    'attribute'     => 'date_insert',
                    'content'       => function ($item) {
                        $v = \yii\helpers\ArrayHelper::getValue($item, 'date_insert', 0);
                        if ($v == 0) return '';

                        return Html::tag('abbr', \cs\services\DatePeriod::back($v, ['isShort' => true]), ['class' => 'gsssTooltip', 'title' => Yii::$app->formatter->asDatetime($v)]);
                    },
                ],
                [
                    'attribute'      => 'view_counter',
                    'headerOptions'  => [
                        'style' => Html::cssStyleFromArray([
                            'width'      => '10%',
                            'text-align' => 'right',
                        ]),
                    ],
                    'header'         => 'Счетчик',
                    'format'         => ['decimal', 0],
                    'contentOptions' => [
                        'style' => 'text-align: right;',
                    ],
                ],
                [
                    'header'        => 'Удалить',
                    'headerOptions' => [
                        'style' => 'width: 10%',
                    ],
                    'content'       => function ($item) {
                        return Html::button('Удалить', [
                            'class' => 'btn btn-danger btn-xs buttonDelete',
                            'data'  => [
                                'id' => $item['id'],
                            ],
                        ]);
                    },
                ],
            ],
        ]) ?>
        <?php \yii\widgets\Pjax::end(); ?>

    </div>
</div>