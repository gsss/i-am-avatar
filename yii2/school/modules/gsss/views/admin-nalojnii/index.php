<?php

/** $this \yii\web\View  */

use yii\helpers\Url;
use app\services\GsssHtml;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use app\services\GetArticle\Collection;

$this->title = 'Наложный платеж';


?>

<div class="container">
    <div class="col-lg-12">
        <h1 class="page-header"><?= Html::encode($this->title) ?></h1>
        <div class="btn-group" style="margin-bottom: 20px;" role="group" aria-label="...">
            <a href="<?= Url::to(['admin-nalojnii/add']) ?>" class="btn btn-default">Добавить</a>
        </div>
        <?php \yii\widgets\Pjax::begin(); ?>
        <?php
        $this->registerJS(<<<JS
$('.buttonDelete').click(function (e) {
    e.preventDefault();
    e.stopPropagation();
    if (confirm('Подтвердите удаление')) {
        var button = $(this);
        var id = $(this).data('id');
        ajaxJson({
            url: '/admin-nalojnii/delete?id=' + id,
            success: function (ret) {
                infoWindow('Успешно', function() {
                    button.parent().parent().remove();
                });
            }
        });
    }
});
$('.buttonSuccess').click(function (e) {
    e.preventDefault();
    e.stopPropagation();
    if (confirm('Подтвердите дйствие')) {
        var button = $(this);
        var id = $(this).data('id');
        ajaxJson({
            url: '/admin-nalojnii/success?id=' + id,
            success: function (ret) {
                infoWindow('Успешно', function() {
                    button.parent().parent().remove();
                });
            }
        });
    }
});
$('.buttonHide').click(function (e) {
    e.preventDefault();
    e.stopPropagation();
    if (confirm('Подтвердите дйствие')) {
        var button = $(this);
        var id = $(this).data('id');
        ajaxJson({
            url: '/admin-nalojnii/hide?id=' + id,
            success: function (ret) {
                infoWindow('Успешно', function() {
                    button.parent().parent().remove();
                });
            }
        });
    }
});

$('.rowTable').click(function() {
    window.location = '/admin-nalojnii/edit?id=' + $(this).data('id');
});
JS
        );
        $sort = new \yii\data\Sort([
            'attributes' => [
                'id',
            ],
            'defaultOrder' => [
                'id' => SORT_ASC,
            ],
        ]);
        ?>
        <?= \yii\grid\GridView::widget([
            'dataProvider' => new \yii\data\ActiveDataProvider([
                'query'      => \app\models\Shop\Nalojnii::find()
                ->where([
                    'and',
                    ['is_done' => 0],
                    ['is_hide' => 0],
                ]),
                'pagination' => [
                    'pageSize' => 20,
                ],
                'sort' => $sort,
            ]),
            'tableOptions' => [
                'class' => 'table table-striped table-hover',
            ],
            'rowOptions'   => function ($item) {
                $data = [
                    'data'  => ['id' => $item['id']],
                    'role'  => 'button',
                    'class' => 'rowTable'
                ];

                return $data;
            },
            'columns'      => [
                'id',
                'link',
                'phone',
                'price',
                [
                    'header'  => 'Адрес',
                    'content' => function ($item) {
                        return \cs\services\Str::sub($item['address'], 100);
                    }
                ],
                [
                    'header'  => 'Выполнено',
                    'content' => function ($item) {
                        return Html::button('Выполнено', [
                            'class' => 'btn btn-success btn-xs buttonSuccess',
                            'data'  => [
                                'id' => $item['id'],
                            ]
                        ]);
                    }
                ],
                [
                    'header'  => 'Скрыть',
                    'content' => function ($item) {
                        return Html::button('Скрыть', [
                            'class' => 'btn btn-default btn-xs buttonHide',
                            'data'  => [
                                'id' => $item['id'],
                            ]
                        ]);
                    }
                ],
                [
                    'header'  => 'Удалить',
                    'content' => function ($item) {
                        return Html::button('Удалить', [
                            'class' => 'btn btn-danger btn-xs buttonDelete',
                            'data'  => [
                                'id' => $item['id'],
                            ]
                        ]);
                    }
                ],
            ],
        ]) ?>
        <?php \yii\widgets\Pjax::end(); ?>
    </div>
</div>