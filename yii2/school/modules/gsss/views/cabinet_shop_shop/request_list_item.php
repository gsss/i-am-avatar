<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;
use app\models\UnionCategory;
use yii\db\Query;
use yii\helpers\Url;
use app\models\Shop\Request;
use cs\services\DatePeriod;

/* @var $this yii\web\View */
/* @var $request app\models\Shop\Request */

$this->title = 'Заказ #' . $request->getId();
$union = $request->getUnion();
$client = $request->getClient();
$dostavkaItem = $request->getDostavkaItem();


?>
<style>
    .timeline {
        list-style: none;
        padding: 20px 0 20px;
        position: relative;
    }

    .timeline:before {
        top: 0;
        bottom: 0;
        position: absolute;
        content: " ";
        width: 3px;
        background-color: #eeeeee;
        left: 50%;
        margin-left: -1.5px;
    }

    .timeline > li {
        margin-bottom: 20px;
        position: relative;
    }

    .timeline > li:before,
    .timeline > li:after {
        content: " ";
        display: table;
    }

    .timeline > li:after {
        clear: both;
    }

    .timeline > li:before,
    .timeline > li:after {
        content: " ";
        display: table;
    }

    .timeline > li:after {
        clear: both;
    }

    .timeline > li > .timeline-panel {
        width: 46%;
        float: left;
        border: 1px solid #d4d4d4;
        border-radius: 2px;
        padding: 20px;
        position: relative;
        -webkit-box-shadow: 0 1px 6px rgba(0, 0, 0, 0.175);
        box-shadow: 0 1px 6px rgba(0, 0, 0, 0.175);
    }

    .timeline > li > .timeline-panel:before {
        position: absolute;
        top: 26px;
        right: -15px;
        display: inline-block;
        border-top: 15px solid transparent;
        border-left: 15px solid #ccc;
        border-right: 0 solid #ccc;
        border-bottom: 15px solid transparent;
        content: " ";
    }

    .timeline > li > .timeline-panel:after {
        position: absolute;
        top: 27px;
        right: -14px;
        display: inline-block;
        border-top: 14px solid transparent;
        border-left: 14px solid #fff;
        border-right: 0 solid #fff;
        border-bottom: 14px solid transparent;
        content: " ";
    }

    .timeline > li > .timeline-badge {
        color: #fff;
        width: 50px;
        height: 50px;
        line-height: 50px;
        font-size: 1.4em;
        text-align: center;
        position: absolute;
        top: 16px;
        left: 50%;
        margin-left: -25px;
        background-color: #999999;
        z-index: 100;
        border-top-right-radius: 50%;
        border-top-left-radius: 50%;
        border-bottom-right-radius: 50%;
        border-bottom-left-radius: 50%;
    }

    .timeline > li.timeline-inverted > .timeline-panel {
        float: right;
    }

    .timeline > li.timeline-inverted > .timeline-panel:before {
        border-left-width: 0;
        border-right-width: 15px;
        left: -15px;
        right: auto;
    }

    .timeline > li.timeline-inverted > .timeline-panel:after {
        border-left-width: 0;
        border-right-width: 14px;
        left: -14px;
        right: auto;
    }

    .timeline-badge.primary {
        background-color: #2e6da4 !important;
    }

    .timeline-badge.success {
        background-color: #3f903f !important;
    }

    .timeline-badge.warning {
        background-color: #f0ad4e !important;
    }

    .timeline-badge.danger {
        background-color: #d9534f !important;
    }

    .timeline-badge.info {
        background-color: #5bc0de !important;
    }

    .timeline-title {
        margin-top: 0;
        color: inherit;
    }

    .timeline-body > p,
    .timeline-body > ul {
        margin-bottom: 0;
    }

    .timeline-body > p + p {
        margin-top: 5px;
    }
</style>
<div class="container">
<h1 class="page-header"><?= Html::encode($this->title) ?></h1>
<?= \cs\Widget\BreadCrumbs\BreadCrumbs::widget([
    'items' => [
        [
            'label' => 'Объединения',
            'url'   => Url::to(['cabinet/objects']),
        ],
        [
            'label' => $union->getName(),
            'url'   => Url::to(['cabinet/objects_edit', 'id' => $union->getId()]),
        ],
        [
            'label' => 'Магазин',
            'url'   => Url::to(['cabinet_shop_shop/index', 'id' => $union->getId()]),
        ],
        [
            'label' => 'Заказы',
            'url'   => Url::to(['cabinet_shop_shop/request_list', 'id' => $union->getId()]),
        ],
        $this->title
    ],
    'home'  => [
        'name' => 'я',
        'url'  => Url::to(['site/user', 'id' => Yii::$app->user->id])
    ]
]) ?>
<hr>

<div class="row">
    <div class="col-sm-4">
        <div class="panel panel-default">
            <div class="panel-heading">Клиент</div>
            <div class="panel-body">
                <a href="<?= $client->getLink() ?>">
                    <img src="<?= $client->getAvatar() ?>" class="thumbnail" width="100%";>
                </a>

                <p><a href="<?= $client->getLink() ?>"><?= $client->getName2() ?></a></p>

                <p>Email: <?= $request->getUser()->getEmail() ?></p>

                <p>Телефон: <?= $request->getField('phone') ?></p>
            </div>
        </div>


    </div>
    <div class="col-sm-8">
        <div class="panel panel-default">
            <div class="panel-heading">Заказ</div>
            <div class="panel-body">
                <p>Комментарий: <?= $request->getField('comment') ?></p>

                <?php if (!is_null($dostavkaItem)) { ?>
                    <p>Доставка: <?= $dostavkaItem->getField('name') ?></p>
                    <?php if ($dostavkaItem->getField('is_need_address', 0) == 1) { ?>
                        <p>Адрес: <?= $request->getField('address') ?></p>
                    <?php } ?>
                <?php } ?>
                <p>Сумма: <?= Yii::$app->formatter->asCurrency($request->getField('price')) ?></p>

                <hr>
                <?= \yii\grid\GridView::widget([
                    'tableOptions' => [
                        'class' => 'table table-striped table-hover',
                        'width' => 'auto',
                        'style' => 'width: auto;'
                    ],
                    'summary'      => '',
                    'dataProvider' => new \yii\data\ActiveDataProvider([
                        'query'      => $request->getProductList()
                        ,
                        'pagination' => [
                            'pageSize' => 50,
                        ],
                    ]),
                    'columns'      => [
                        ['class' => 'yii\grid\SerialColumn'],
                        [
                            'header'  => 'Картинка',
                            'content' => function ($item) {
                                $i = \yii\helpers\ArrayHelper::getValue($item, 'image', '');
                                if ($i == '') return '';
                                $p = new \app\models\Shop\Product($item);

                                return
                                    Html::a(
                                        Html::img($i, ['width' => 100, 'class' => 'thumbnail', 'style' => 'margin-bottom: 0px;'])
                                        , $p->getLink());
                            }
                        ],
                        [
                            'header'  => 'Наименование',
                            'content' => function ($item) {
                                $p = new \app\models\Shop\Product($item);

                                return
                                    Html::a(
                                        $item['name']
                                        , $p->getLink());
                            }
                        ],
                        [
                            'header'  => 'Цена',
                            'content' => function ($item) {
                                return Yii::$app->formatter->asDecimal($item['price'], 0);
                            }
                        ],
                        'count:integer:Кол-во',
                    ]
                ]) ?>
            </div>
        </div>
    </div>
</div>


<h2 class="page-header">История заказа</h2>
<?php $this->registerJs('$(".timeBack").tooltip()'); ?>
<ul class="timeline">
    <?php
    $requestStatus = $request->getStatus();
    foreach ($request->getMessages()->all() as $item) {
        ?>
        <?php
        $side = 'shop';
        $liSuffix = '';
        $itemStatus = \yii\helpers\ArrayHelper::getValue($item, 'status', 0);
        if ($item['direction'] == (($side == 'client') ? Request::DIRECTION_TO_SHOP : Request::DIRECTION_TO_CLIENT)) {
            $liSuffix = ' class="timeline-inverted"';
        }
        if ($itemStatus == 0) {
            $type = 'message';
            $header = 'Сообщение';
            $icon = 'glyphicon-envelope';
            $color = 'info';
            $message = $item['message'];
        } else {
            $type = 'status';
            $header = Request::$statusList[$item['status']][$side];
            $icon = Request::$statusList[$item['status']]['timeLine']['icon'];
            $color = Request::$statusList[$item['status']]['timeLine']['color'];
            $message = \yii\helpers\ArrayHelper::getValue($item, 'message', '');
        }
        ?>
        <li<?= $liSuffix ?>>
            <div class="timeline-badge <?= $color ?>"><i class="glyphicon <?= $icon ?>"></i></div>
            <div class="timeline-panel">
                <div class="timeline-heading">
                    <h4 class="timeline-title"><?= $header ?></h4>

                    <p>
                        <small class="text-muted"><i class="glyphicon glyphicon-time"></i> <abbr
                                title="<?= Yii::$app->formatter->asDatetime($item['datetime']) ?>"
                                class="timeBack"><?= DatePeriod::back($item['datetime']) ?></abbr></small>
                    </p>
                </div>
                <?php if ($message != '') { ?>
                    <div class="timeline-body">
                        <?= nl2br($message); ?>
                    </div>
                <?php } ?>

                <?php if (($requestStatus == Request::STATUS_ORDER_DOSTAVKA and $itemStatus == Request::STATUS_ORDER_DOSTAVKA) or ($requestStatus == Request::STATUS_PAID_CLIENT and $itemStatus == Request::STATUS_PAID_CLIENT)) { ?>
                    <hr>
                    <button class="btn btn-info" id="buttonAnswerPay">Подтвердить оплату</button>
                    <?php if (Yii::$app->user->identity->isAdmin()) { ?>
                        <?php
                        $this->registerJs(<<<JS
    $('#buttonPayClient').click(function() {
        ajaxJson({
            url: '/cabinet/shop/requestList/{$request->getId()}/pay-client',
            success: function(ret) {
                window.location.reload();
            }
        });
    });

JS
                        );
                        ?>
                        <button class="btn btn-info" id="buttonPayClient">Оплатить со счетов клиента</button>
                    <?php } ?>
                <?php } ?>

                <?php if ($requestStatus == Request::STATUS_DOSTAVKA_ADDRESS_PREPARE and $itemStatus == Request::STATUS_DOSTAVKA_ADDRESS_PREPARE) { ?>
                    <hr>
                    <button class="btn btn-default btn-xs buttonPrepared">Заказ сформирован</button>
                    <button class="btn btn-default btn-xs buttonSend">Заказ отправлен</button>
                <?php } ?>

                <?php if ($requestStatus == Request::STATUS_DOSTAVKA_ADDRESS_PREPARE_DONE and $itemStatus == Request::STATUS_DOSTAVKA_ADDRESS_PREPARE_DONE) { ?>
                    <hr>
                    <button class="btn btn-default btn-xs buttonSend">Заказ отправлен</button>
                <?php } ?>

                <?php if ($requestStatus == Request::STATUS_DOSTAVKA_SAMOVIVOZ_WAIT and $itemStatus == Request::STATUS_DOSTAVKA_SAMOVIVOZ_WAIT) { ?>
                    <hr>
                    <button class="btn btn-info buttonSamovivozDone">Заказ вручен</button>
                <?php } ?>

                <?php if ($requestStatus == Request::STATUS_DOSTAVKA_NALOZH_PREPARE and $itemStatus == Request::STATUS_DOSTAVKA_NALOZH_PREPARE) { ?>
                    <hr>
                    <button class="btn btn-info buttonNalozhniiPrepared">Сфомирован</button>
                    <button class="btn btn-info buttonNalozhniiSended">Отправлен</button>
                <?php } ?>

                <?php if ($requestStatus == Request::STATUS_DOSTAVKA_NALOZH_PREPARE_DONE and $itemStatus == Request::STATUS_DOSTAVKA_NALOZH_PREPARE_DONE) { ?>
                    <hr>
                    <button class="btn btn-info buttonNalozhniiSended">Отправлен</button>
                <?php } ?>

                <?php if ($requestStatus == Request::STATUS_DOSTAVKA_NALOZH_SEND_TO_USER and $itemStatus == Request::STATUS_DOSTAVKA_NALOZH_SEND_TO_USER) { ?>
                    <hr>
                    <button class="btn btn-success buttonNalozhniiPaid">Оплачено</button>
                    <button class="btn btn-danger buttonNalozhniiCanceled">Отменен</button>
                <?php } ?>
            </div>
        </li>
    <?php } ?>
</ul>




<?php
$this->registerJs(<<<JS
$('#buttonSendMessage').click(function() {
    $('#messageModal').modal('show');
    $('#buttonSendMessageForm').click(function() {
        var text = $('#messageModal textarea').val();
        ajaxJson({
            url: '/cabinet/shop/requestList/{$request->getId()}/message',
            data: {
                text: text
            },
            success: function(ret) {
                $('#messageModal').modal('hide');
                window.location.reload();
            }
        });
    });
});
$('#buttonAnswerPay').click(function() {
    $('#messageModal').modal('show');
    $('#buttonSendMessageForm').click(function() {
        var text = $('#messageModal textarea').val();
        ajaxJson({
            url: '/cabinet/shop/requestList/{$request->getId()}/answerPay',
            data: {
                text: text
            },
            success: function(ret) {
                $('#messageModal').modal('hide');
                window.location.reload();
            }
        });
    });
});
$('.buttonSamovivozDone').click(function() {
    $('#messageModal').modal('show');
    $('#buttonSendMessageForm').click(function() {
        var text = $('#messageModal textarea').val();
        ajaxJson({
            url: '/cabinet/shop/requestList/{$request->getId()}/samovivozDone',
            data: {
                text: text
            },
            success: function(ret) {
                $('#messageModal').modal('hide');
                window.location.reload();
            }
        });
    });
});
$('.buttonSend').click(function() {
    $('#messageModal').modal('show');
    $('#buttonSendMessageForm').click(function() {
        var text = $('#messageModal textarea').val();
        ajaxJson({
            url: '/cabinet/shop/requestList/{$request->getId()}/send',
            data: {
                text: text
            },
            success: function(ret) {
                $('#messageModal').modal('hide');
                window.location.reload();
            }
        });
    });
});
$('.buttonPrepared').click(function() {
    $('#messageModal').modal('show');
    $('#buttonSendMessageForm').click(function() {
        var text = $('#messageModal textarea').val();
        ajaxJson({
            url: '/cabinet/shop/requestList/{$request->getId()}/prepared',
            data: {
                text: text
            },
            success: function(ret) {
                $('#messageModal').modal('hide');
                window.location.reload();
            }
        });
    });
});

$('.buttonNalozhniiPrepared').click(function() {
    ajaxJson({
        url: '/cabinet/shop/requestList/{$request->getId()}/nalozh-prepared',
        success: function(ret) {
            window.location.reload();
        }
    });
});
$('.buttonNalozhniiSended').click(function() {
    $('#messageModalPaid').modal('show');
    $('#messageModalPaid .buttonSend1').click(function() {
        var text = $('#messageModalPaid textarea').val();
        var number = $('#messageModalPaid #number').val();
        ajaxJson({
            url: '/cabinet/shop/requestList/{$request->getId()}/nalozh-sended',
            data: {
                text: text,
                number: number
            },
            success: function(ret) {
                $('#messageModalPaid').modal('hide');
                window.location.reload();
            }
        });
    });
});
$('.buttonNalozhniiPaid').click(function() {
    $('#messageModal').modal('show');
    $('#buttonSendMessageForm').click(function() {
        var text = $('#messageModal textarea').val();
        ajaxJson({
            url: '/cabinet/shop/requestList/{$request->getId()}/nalozh-paid',
            data: {
                text: text
            },
            success: function(ret) {
                $('#messageModal').modal('hide');
                window.location.reload();
            }
        });
    });
});
$('.buttonNalozhniiCanceled').click(function() {
    ajaxJson({
        url: '/cabinet/shop/requestList/{$request->getId()}/nalozh-canceled',
        success: function(ret) {
            window.location.reload();
        }
    });
});
JS
);
?>

<div class="modal fade" id="messageModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
     aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                        aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Сообщение</h4>
            </div>
            <div class="modal-body">
                <textarea class="form-control" rows="10"></textarea>
                <hr>
                <button class="btn btn-primary" style="width:100%;" id="buttonSendMessageForm">Отправить</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="messageModalPaid" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
     aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                        aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Сообщение</h4>
            </div>
            <div class="modal-body">
                <p>Идентификатор отслеживания</p>
                <input class="form-control" id="number">
                <p>Сообщение</p>
                <textarea class="form-control" rows="10"></textarea>
                <hr>
                <button class="btn btn-primary buttonSend1" style="width:100%;">Отправить</button>
            </div>
        </div>
    </div>
</div>
<hr>
<button class="btn btn-info" id="buttonSendMessage"><i class="glyphicon glyphicon-envelope"></i> Отправить сообщение
</button>
<a class="btn btn-info" href="<?= Url::to(['cabinet_shop_shop/request-list-item-address', 'id' => $request->getId()])?>" target="_blank"> Распечатать адрес
</a>
</div>
