<?php
/**
 * Created by PhpStorm.
 * User: Бог-Творец
 * Date: 18.08.2016
 * Time: 10:10
 */
/** @var $request \app\models\Shop\Request */
$address = $request->get('address');
$from_name = $request->getUnion()->getShop()->get('from_name', '');
$from_address = $request->getUnion()->getShop()->get('from_address', '');
$arr = explode(', ', $address);
?>

<table>
    <tr>
        <td style="width: 50%;">
            <p>От кого: <?= $from_name ?></p>
            <p>От куда: <?= $from_address ?></p>
        </td>
        <td style="padding-left: 40px;width: 50%;">
            <p>Кому: <?= $arr[count($arr) - 1] ?></p>
            <?php unset($arr[count($arr) - 1]); ?>
            <p>Куда: <?= join(', ', $arr) ?></p>
        </td>
    </tr>
</table>