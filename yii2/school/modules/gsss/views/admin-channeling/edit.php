<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;
use app\models\UnionCategory;
use yii\db\Query;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \app\models\Form\Chenneling */

$this->title = $model->header;
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="container">
    <div class="col-lg-12">
        <div class="page-header">
            <h1><?= Html::encode($this->title) ?></h1>
        </div>

        <?php if (Yii::$app->session->hasFlash('contactFormSubmitted')): ?>

            <div class="alert alert-success">
                Успешно обновлено.
            </div>
            <?php if ($model->is_added_site_update != 1) { ?>
                <?php
                $this->registerJs(<<<JS
$('.buttonAddSiteUpdate').click(function (e) {
    e.preventDefault();
    e.stopPropagation();
    var button = $(this);
    var id = $(this).data('id');
    ajaxJson({
        url: '/admin-channeling/subscribe?id=' + id,
        success: function (ret) {
            infoWindow('Успешно', function() {
                button.remove();
            });
        }
    });
});
JS
                );
                ?>
                <button class="btn btn-info buttonAddSiteUpdate" data-id="<?= $model->id ?>">Сделать рассылку</button>
            <?php } ?>
            <a class="btn btn-info" href="/admin-channeling/edit?id=<?= $model->id ?>">Вернуться к редактированию</a>
            <a class="btn btn-info" href="/admin-channeling/index">Список посланий</a>
        <?php else: ?>


            <div class="row">
                <div class="col-lg-5">
                    <?php $form = ActiveForm::begin([
                        'id'      => 'contact-form',
                        'options' => ['enctype' => 'multipart/form-data']
                    ]); ?>
                    <?= $model->field($form, 'header') ?>
                    <?= $model->field($form, 'source') ?>
                    <?= $model->field($form, 'description')->textarea(['rows' => 5]) ?>
                    <?= $model->field($form, 'content') ?>
                    <?= $model->field($form, 'img') ?>
                    <?= $model->field($form, 'is_add_image') ?>
                    <?= $model->field($form, 'tree_node_id_mask') ?>
                    <?= $model->field($form, 'share_title_type')->dropDownList(
                        [
                            'Ничего не выбрано',
                            1 => 'Заголовок',
                            2 => 'Силы. Заголовок',
                            3 => 'Автор. Заголовок',
                            4 => 'Силы через Автора. Заголовок',
                        ]
                    ) ?>
                    <?= $model->field($form, 'author') ?>
                    <?= $model->field($form, 'author_id') ?>
                    <?= $model->field($form, 'power_id') ?>

                    <hr>
                    <div class="form-group">
                        <?= Html::submitButton('Обновить', [
                            'class' => 'btn btn-default',
                            'name'  => 'contact-button',
                            'style' => 'width:100%',
                        ]) ?>
                    </div>
                    <?php ActiveForm::end(); ?>
                </div>
            </div>
        <?php endif; ?>
    </div>
</div>
