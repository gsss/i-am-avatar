<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;
use app\models\UnionCategory;
use yii\db\Query;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \app\models\Form\ChennelingFromPage */

$this->title = 'Добавить ченнелинг';
?>
<div class="container">
    <div class="col-lg-12">
        <div class="page-header">
            <h1><?= Html::encode($this->title) ?></h1>
        </div>

        <?php if (Yii::$app->session->hasFlash('contactFormSubmitted')): ?>

            <div class="alert alert-success">
                Успешно добавлено.
            </div>
            <a class="btn btn-info" href="/admin/channelingList/<?= \common\models\Channeling::find()->orderBy(['id' => SORT_DESC])->select('id')->scalar() ?>/edit">Вернуться к редактированию</a>

        <?php else: ?>


            <div class="row">
                <div class="col-lg-5">
                    <?php $form = ActiveForm::begin([
                        'id'      => 'contact-form',
                        'options' => ['enctype' => 'multipart/form-data']
                    ]); ?>
                    <?= $form->field($model, 'url')->label('Ссылка') ?>
                    <?= $form->field($model, 'provider')->hiddenInput()->label('', ['class' => 'hide']) ?>
                    <?= $form->field($model, 'tree_node_id_mask')->label('Категории')->widget('cs\Widget\CheckBoxTreeMask\CheckBoxTreeMask', [
                        'tableName' => 'gs_cheneling_tree'
                    ]) ?>
                    <?= $form->field($model, 'author_id')->dropDownList(
                        \yii\helpers\ArrayHelper::merge(
                            [null => 'Ничего не выбрано'],
                            \yii\helpers\ArrayHelper::map(\app\models\ChennelingAuthor::query()->select('id,name')->orderBy(['name' => SORT_ASC])->all(),'id', 'name')
                        )
                    ) ?>
                    <?= $form->field($model, 'power_id')->dropDownList(
                        \yii\helpers\ArrayHelper::merge(
                            [null => 'Ничего не выбрано'],
                            \yii\helpers\ArrayHelper::map(\app\models\ChennelingPower::query()->select('id,name')->orderBy(['name' => SORT_ASC])->all(),'id', 'name')
                        )
                    ) ?>

                    <hr>
                    <div class="form-group">
                        <?= Html::submitButton('Добавить', [
                            'class' => 'btn btn-default',
                            'name' => 'contact-button',
                            'style' => 'width:100%',
                        ]) ?>
                    </div>
                    <?php ActiveForm::end(); ?>
                </div>
            </div>
        <?php endif; ?>
    </div>
</div>
