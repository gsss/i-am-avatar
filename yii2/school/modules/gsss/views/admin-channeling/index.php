<?php

/** $this \yii\web\View  */

use yii\helpers\Url;
use app\services\GsssHtml;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use app\services\GetArticle\Collection;

$this->title = 'Все послания';


?>

<div class="container">
    <div class="col-lg-12">
        <h1 class="page-header"><?= Html::encode($this->title) ?></h1>

        <div class="btn-group" style="margin-bottom: 20px;" role="group" aria-label="...">
            <a href="<?= Url::to(['admin-channeling/add']) ?>" class="btn btn-default">Добавить</a>
            <a href="<?= Url::to(['admin-channeling/add_from_page']) ?>" type="button" class="btn btn-default">со
                страницы</a>
        </div>
        <?php \yii\widgets\Pjax::begin(); ?>
        <?php
        $this->registerJS(<<<JS
$('.buttonDelete').click(function (e) {
    e.preventDefault();
    e.stopPropagation();
    if (confirm('Подтвердите удаление')) {
        var button = $(this);
        var id = $(this).data('id');
        ajaxJson({
            url: '/admin-channeling/delete?id=' + id,
            success: function (ret) {
                infoWindow('Успешно', function() {
                    button.parent().parent().remove();
                });
            }
        });
    }
});

$('.buttonAddSiteUpdate').click(function (e) {
    e.preventDefault();
    e.stopPropagation();
    var button = $(this);
    var id = $(this).data('id');
    ajaxJson({
        url: '/admin-channeling/subscribe?id=' + id,
        success: function (ret) {
            infoWindow('Успешно', function() {
                button.remove();
            });
        }
    });
});

$('.gsssTooltip').tooltip();

$('.rowTable').click(function() {
    window.location = '/admin-channeling/edit?id=' + $(this).data('id');
});
JS
        );
        $sort = new \yii\data\Sort(
            [
                'attributes'   => [
                    'id'           => ['label' => "ID"],
                    'header'       => ['label' => "Название"],
                    'date_created' => ['label' => "Создано"],
                ],
                'defaultOrder' => [
                    'date_created' => SORT_DESC,
                ],
            ]
        );
        $model = new \app\models\search\Channeling();
        $provider = $model->search(Yii::$app->request->get(), null, $sort);
        ?>
        <?= \yii\grid\GridView::widget([
            'dataProvider' => $provider,
            'filterModel'  => $model,
            'tableOptions' => [
                'class' => 'table table-striped table-hover',
            ],
            'rowOptions'   => function ($item) {
                $data = [
                    'data'  => ['id' => $item['id']],
                    'role'  => 'button',
                    'class' => 'rowTable'
                ];
                if ($item['moderation_status'] != 1) {
                    $data['style'] = 'opacity: 0.5';
                }
                return $data;
            },
            'columns'      => [
                [
                    'attribute'  => 'id',
                    'header'     => $sort->link('id'),
                    'headerOptions' => [
                        'style' => 'width: 10%',
                    ],
                ],
                [
                    'header'  => 'Картинка',
                    'headerOptions' => [
                        'style' => 'width: 10%',
                    ],
                    'content' => function ($item) {
                        $i = ArrayHelper::getValue($item, 'img', '');
                        if ($i == '') return '';

                        return Html::img($i, [
                            'class'  => "thumbnail",
                            'width'  => 80,
                            'height' => 80,
                            'style'  => 'margin-bottom: 0px;',
                        ]);
                    }
                ],
                [
                    'attribute'  => 'header',
                    'header'     => $sort->link('header'),
                    'headerOptions' => [
                        'style' => 'width: 30%',
                    ],
                ],
                [
                    'header'  => 'Автор',
                    'headerOptions' => [
                        'style' => 'width: 15%',
                    ],
                    'content' => function ($item) {
                        $u = \app\services\UsersInCache::find($item['user_id']);
                        if (is_null($u)) return '';
                        $arr = [];
                        $arr[] = Html::img($u['avatar'], [
                            'width' => 50,
                            'class' => 'thumbnail',
                            'style' => Html::cssStyleFromArray([
                                'margin-bottom' => '0px',
                                'margin-right'  => '10px',
                                'float'         => 'left',
                            ]),
                        ]);
                        $arr[] = Html::a($u['name'], '/user/' . $u['id']);
                        $arr[] = '<br>';
                        $arr[] = '<small style="color: #888;">' . $u['email'] . '</small>';

                        return join('', $arr);
                    }
                ],
                [
                    'header'  => 'Создано',
                    'attribute'      => 'date_created',
                    'headerOptions' => [
                        'style' => 'width: 10%',
                    ],
                    'content' => function ($item) {
                        $v = \yii\helpers\ArrayHelper::getValue($item, 'date_created', 0);
                        if ($v == 0) return '';

                        return Html::tag('abbr', \cs\services\DatePeriod::back($v, ['isShort' => true]), ['class' => 'gsssTooltip', 'title' => Yii::$app->formatter->asDatetime($v)]);
                    }
                ],
                [
                    'attribute'      => 'view_counter',
                    'headerOptions' => [
                        'style' => 'width: 10%',
                    ],
                    'header'         => 'Счетчик',
                    'format'         => ['decimal', 0],
                    'contentOptions' => [
                        'style' => 'text-align: right;',
                    ],
                ],
                [
                    'header'  => 'Статус',
                    'headerOptions' => [
                        'style' => 'width: 5%',
                    ],
                    'content' => function ($item) {
                        switch ($item['moderation_status']) {
                            case 0: // отклонено
                                $class = 'glyphicon glyphicon-remove';
                                break;
                            case 1: // одобрено
                                $class = 'glyphicon glyphicon-ok';
                                break;
                            case 2: // оправлено на утверждение модератором
                                $class = 'glyphicon glyphicon-eye-open';
                                break;
                            default: // на стадии редактрования
                                $class = 'glyphicon glyphicon-time';
                                break;
                        }
                        return Html::tag('span', Html::tag('span', null, ['class' => $class]), ['label label-default']);
                    }
                ],
                [
                    'header'  => 'Удалить',
                    'headerOptions' => [
                        'style' => 'width: 10%',
                    ],
                    'content' => function ($item) {
                        return Html::button('Удалить', [
                            'class' => 'btn btn-danger btn-xs buttonDelete',
                            'data'  => [
                                'id' => $item['id'],
                            ]
                        ]);
                    }
                ],
            ],
        ]) ?>
        <?php \yii\widgets\Pjax::end(); ?>
    </div>
</div>