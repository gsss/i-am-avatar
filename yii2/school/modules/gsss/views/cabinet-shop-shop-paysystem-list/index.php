<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;
use app\models\UnionCategory;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $union \app\models\Union */

$this->title = 'Платежные системы';

$this->registerJS(<<<JS
    $('.rowTable').click(function() {
        var paysystem_id = $(this).data('paysystem_id');
        var union_id = $(this).data('union_id');
        window.location = '/cabinet-shop-shop-paysystem-list/edit?' +
         'paysystem_id' + '=' + paysystem_id
         + '&' +
         'union_id' + '=' + union_id
         ;
    });

JS
);
Yii::$app->session->set('/cabinet-shop-shop-paysystem-list/index.php::union_id', $union->getId());
?>
<div class="container">
    <h1 class="page-header"><?= Html::encode($this->title) ?></h1>
    <?= \cs\Widget\BreadCrumbs\BreadCrumbs::widget([
        'items' => [
            [
                'label' => 'Объединения',
                'url'   => Url::to(['cabinet/objects']),
            ],
            [
                'label' => $union->getName(),
                'url'   => Url::to(['cabinet/objects_edit', 'id' => $union->getId()]),
            ],
            [
                'label' => 'Магазин',
                'url'   => Url::to(['cabinet_shop_shop/index', 'id' => $union->getId()]),
            ],
            $this->title
        ],
        'home'  => [
            'name' => 'я',
            'url'  => Url::to(['site/user', 'id' => Yii::$app->user->id])
        ]
    ]) ?>
    <hr>
    <style>
        .tableMy .right
        {
            text-align: right;
        }
    </style>
    <?= \yii\grid\GridView::widget([
        'tableOptions' => [
            'class' => 'table tableMy table-striped table-hover',
        ],
        'rowOptions'   => function ($item) {
            return [
                'data' => [
                    'paysystem_id' => $item['paysystem_id'],
                    'union_id'     => Yii::$app->session->get('/cabinet-shop-shop-paysystem-list/index.php::union_id')
                ],
                'role'  => 'button',
                'class' => 'rowTable'
            ];
        },
        'dataProvider' => new \yii\data\ActiveDataProvider([
            'query'      => \common\models\shop\PaymentConfig::find()
                ->rightJoin('gs_paysystems', 'gs_paysystems.id = gs_unions_shop_payments.paysystem_id and gs_unions_shop_payments.union_id='.$union->getId())
                ->select([
                    'gs_unions_shop_payments.id',
                    'gs_unions_shop_payments.config',
                    'gs_paysystems.title',
                    'gs_paysystems.image',
                    'gs_paysystems.class_name',
                    'gs_paysystems.id as paysystem_id',
                ])
                ->asArray()
            ,
            'pagination' => [
                'pageSize' => 50,
            ],
        ]),
        'columns' => [
            'id',
            [
                'header'  => 'Задана?',
                'content' => function ($item) {
                    $i = \yii\helpers\ArrayHelper::getValue($item, 'id', '');
                    if ($i == '') return Html::tag('span', 'Нет', ['class' => 'label label-danger']);

                    return Html::tag('span', 'Да', ['class' => 'label label-success']);
                }
            ],
            [
                'header'  => 'Картинка',
                'content' => function ($item) {
                    $i = \yii\helpers\ArrayHelper::getValue($item, 'image', '');
                    if ($i == '') return '';

                    return Html::img($i, [
                        'class' => "thumbnail",
                        'width' => 80,
                        'style' => 'margin-bottom: 0px;',
                    ]);
                }
            ],
            'title:text:Название',
            [
                'header'  => 'Конфиг',
                'content' => function ($item) {
                    return \app\services\GsssHtml::getMiniText(\yii\helpers\ArrayHelper::getValue($item, 'config'), 50);
                }
            ],
        ]
    ]) ?>
</div>
