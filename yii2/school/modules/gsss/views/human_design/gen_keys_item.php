<?php
use yii\helpers\Html;
use yii\bootstrap\ButtonDropdown;
use yii\helpers\Url;
use cs\services\Url as csUrl;
use cs\services\Str;
use yii\widgets\Breadcrumbs;

/** @var $this yii\web\View */
/** @var $item \app\models\HdGenKeys */

$this->title = 'Генный ключ '. $item->getField('num') . '. Дизайн Человека';

function cc($v)
{
    return $v;
}
?>
<div class="container">
    <div class="col-lg-12">
        <h1 class="page-header"><?= \yii\helpers\Html::encode($this->title) ?></h1>
        <?= \cs\Widget\BreadCrumbs\BreadCrumbs::widget([
            'items' => [
                [
                    'label' => 'Дизайн Человека',
                    'url'   => ['human_design/index'],
                ],
                $this->title
            ],
        ]) ?>
        <hr>
    </div>
    <div class="col-lg-8">
        <?php if (
            $item->get('num') &&
            $item->get('ten') &&
            $item->get('dar') &&
            $item->get('siddhi') &&
            $item->get('name') &&
            $item->get('content_ten') &&
            $item->get('content_dar') &&
            $item->get('content_siddhi') &&
            $item->get('codon') &&
            $item->get('amin') &&
            $item->get('patern') &&
            $item->get('fiz')
        ) { ?>
            <table class="table table-hover table-striped" style="width: auto;">
                <tr>
                    <td>
                        Кодон
                    </td>
                    <td>
                        <?php
                        $img = $item->get('image_codon', '');
                        if ($img) {
                            echo Html::img($img, ['width' => 100, 'height' => 100, 'class' => 'thumbnail', 'margin-bottom: 0px;']);
                        } else {
                            echo '';
                        }
                        ?>
                    </td>
                </tr>
                <tr>
                    <td>
                        Граф
                    </td>
                    <td>
                        <?php
                        $img = $item->get('image_graph', '');
                        if ($img) {
                            echo Html::img($img, ['width' => 100, 'height' => 100, 'class' => 'thumbnail', 'margin-bottom: 0px;']);
                        } else {
                            echo '';
                        }
                        ?>
                    </td>
                </tr>
                <tr>
                    <td>
                        Название
                    </td>
                    <td>
                        <?= $item->get('name')  ?>
                    </td>
                </tr>
                <tr>
                    <td>
                        Тень
                    </td>
                    <td>
                        <?= $item->get('ten')  ?>
                    </td>
                </tr>
                <tr>
                    <td>
                        Дар
                    </td>
                    <td>
                        <?= $item->get('dar')  ?>
                    </td>
                </tr>
                <tr>
                    <td>
                        Сиддхи
                    </td>
                    <td>
                        <?= $item->get('siddhi')  ?>
                    </td>
                </tr>
                <tr>
                    <td>
                        Кодоновое кольцо
                    </td>
                    <td>
                        <?php
                        $text = $item->get('codon');
                        $arr = explode('(', $text);
                        $text = trim($arr[0]);
                        $arr = explode(')', $arr[1]);
                        $arr = explode(',', $arr[0]);
                        $t = [];
                        foreach($arr as $i) {
                            $i = trim($i);
                            $t[] = Html::a($i, ['human_design/gen_keys_item', 'id' => $i], ['target' => '_blank']);
                        }
                        ?>

                        <?= $text . ' (' . join(', ', $t) . ')'  ?>
                    </td>
                </tr>
                <tr>
                    <td>
                        Физиология
                    </td>
                    <td>
                        <?= $item->get('fiz')  ?>
                    </td>
                </tr>
                <tr>
                    <td>
                        Аминокислота
                    </td>
                    <td>
                        <?= $item->get('amin')  ?>
                    </td>
                </tr>
                <tr>
                    <td>
                        Программный паттерн
                    </td>
                    <td>
                        <a href="<?= Url::to(['human_design/gen_keys_item', 'id' => $item->get('patern')])?>" target="_blank"><?= $item->get('patern')  ?></a>
                    </td>
                </tr>
            </table>
            <h2 class="page-header">Тень</h2>
            <?= cc($item->get('content_ten'))  ?>

            <h2 class="page-header">Дар</h2>
            <?= cc($item->get('content_dar'))  ?>

            <h2 class="page-header">Сиддхи</h2>
            <?= cc($item->get('content_siddhi'))  ?>

        <?php } else { ?>
            <?= cc($item->get('content'))  ?>
        <?php } ?>

        <hr>
        <?= $this->render('../blocks/share', [
            'image'       => \yii\helpers\Url::to('/images/new_earth/chakri/1444980126_XlhMUkP7z3.jpg', true),
            'url'         => Url::current([], true),
            'title'       => $this->title,
            'description' => trim(Str::sub(strip_tags($item->getField('content')), 0, 200)),
        ]) ?>


    </div>
    <div class="col-lg-4">
        <?php if (\yii\helpers\ArrayHelper::getValue(Yii::$app->params, 'isMarketing', 0) == 1) { ?>
            <?= $this->render('../blocks/reklama') ?>
        <?php } ?>

        <?= $this->render('../blocks/reklama/block2') ?>
    </div>

</div>