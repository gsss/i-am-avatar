<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;
use app\controllers\Human_designController;
use cs\services\Security;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */

$this->title = 'Генные ключи';

?>

<div class="container">
    <div class="col-lg-12">
        <h1 class="page-header text-center"><?= Html::encode($this->title) ?></h1>


        <p class="text-center" style="margin-top: 50px;">
            <img src="/images/site/human_design/keys/DDm0Hhbe35w.jpg"
                 width="100%" style="border-radius: 30px; max-width: 604px;">
        </p>
        <?php
        $this->registerJs(<<<JS
$('.rowTable').click(function() {
    window.location = '/humanDesign/genKeys/' + $(this).data('num');
})
JS
        );
        ?>
        <?= \yii\grid\GridView::widget([
            'dataProvider' => new \yii\data\ActiveDataProvider([
                'query'      => \app\models\HdGenKeys::query()->orderBy(['num' => SORT_ASC]),
                'pagination' => [
                    'pageSize' => 70,
                ],
            ]),
            'summary'      => '',
            'rowOptions'   => function ($item) {
                return [
                    'class'    => 'rowTable',
                    'role'     => 'button',
                    'data-num' => $item['num'],
                ];
            },
            'tableOptions' => [
                'class' => 'table table-striped table-hover',
            ],
            'columns'      => [
                'num:text:#',
                [
                    'header'  => 'Кодон',
                    'content' => function ($item) {
                        $img = \yii\helpers\ArrayHelper::getValue($item, 'image_codon', '');
                        if ($img) {
                            return Html::img($img, ['width' => 50, 'height' => 50, 'style' => Html::cssStyleFromArray([
                                'border'        => '1px solid #ccc',
                                'border-radius' => '4px',
                            ])]);
                        } else {
                            return '';
                        }
                    },
                ],
                [
                    'header'  => 'Граф',
                    'content' => function ($item) {
                        $img = \yii\helpers\ArrayHelper::getValue($item, 'image_graph', '');
                        if ($img) {
                            return Html::img($img, ['width' => 50, 'height' => 50, 'style' => Html::cssStyleFromArray([
                                'border'        => '1px solid #ccc',
                                'border-radius' => '4px',
                            ])]);
                        } else {
                            return '';
                        }
                    },
                ],
                'name:text:Название',
                'ten:text:Тень',
                'dar:text:Дар',
                'siddhi:text:Сиддхи',
            ],
        ]) ?>

        <hr>
        <?= $this->render('../blocks/share', [
            'image'       => \yii\helpers\Url::to('/images/site/human_design/keys/DDm0Hhbe35w.jpg', true),
            'url'         => 'Дизайн Человека' . \yii\helpers\Url::current([], true),
            'title'       => $this->title,
            'description' => 'Вaш Хологенетический Профиль приглaшaет вaс в сугубо персонaльный приключение через Генные Ключи. Определенные
    последовaтельности Генных Ключей, отпечaтaнных в момент вaшего рождения, открывaют внутри вaс пути к пробуждению.
    Через их созерцaние и применение этого учения к вaшей ежедневной жизни, вы можете почувствовaть, кaк в вaс
    просыпaется новый дух.',
        ]) ?>
    </div>
</div>
