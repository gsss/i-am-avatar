<?php

use yii\helpers\Url;
use yii\helpers\Html;

/** @var $human_design \app\models\HumanDesign */

/**
 * 'human_design'  => $data,
 * 'birth_date'    => $date[2].'-'.$date[1].'-'.$date[0],
 * 'birth_time'    => $time.':00',
 * 'birth_lat'     => $location[0],
 * 'birth_lng'     => $location[1],
 */
$humanDesign = $human_design;
?>


<div class="panel panel-default">
    <div class="panel-heading">Дизайн Человека

        <div class="btn-group pull-right">
            <button type="button" class="btn btn-danger btn-xs buttonDelete">
                Удалить и пересчитать
            </button>
        </div>
    </div>
    <div class="panel-body">
        <img src="data:image/png;base64,<?= base64_encode($humanDesign->image) ?>" style="width: 100%;">
        <table class="table table-striped table-hover" style="width: auto;" align="center">
            <tr>
                <td>Тип</td>
                <td>
                    <a href="<?= \app\modules\HumanDesign\calculate\YourHumanDesignRu::$links['type'][$humanDesign->type['href']] ?>">
                        <?= $humanDesign->type['text'] ?>
                    </a>
                </td>
            </tr>
            <tr>
                <td>Профиль</td>
                <td>
                    <a href="<?= \app\modules\HumanDesign\calculate\YourHumanDesignRu::$links['profile'][$humanDesign->profile['href']] ?>">
                        <?= $humanDesign->profile['text'] ?>
                    </a>
                </td>
            </tr>
            <tr>
                <td>Определение</td>
                <td><?= $humanDesign->definition['text'] ?></td>
            </tr>
            <tr>
                <td>Внутренний Авторитет</td>
                <td><?= $humanDesign->inner['text'] ?></td>
            </tr>
            <tr>
                <td>
                    Стратегия
                </td>
                <td>
                    <?= $humanDesign->strategy['text'] ?>
                </td>
            </tr>
            <tr>
                <td>
                    Тема ложного Я
                </td>
                <td>
                    <?= $humanDesign->theme['text'] ?>
                </td>
            </tr>
            <tr>
                <td>Подпись</td>
                <td><?= (is_null($humanDesign->signature)) ? '' : $humanDesign->signature['text'] ?></td>
            </tr>
            <tr>
                <td>Инкарнационный крест</td>
                <?php
                $text = $humanDesign->cross["text"];
                $arr = explode('(', $text);
                $text = trim($arr[0]);
                $isError = false;
                if (count($arr) == 1) {
                    $isError = true;
                } else {
                    $arr = explode(')', $arr[1]);
                    $arr = explode('|', $arr[0]);
                    $new = [];
                    foreach ($arr as $i) {
                        $a = explode('/', $i);
                        $new[] = trim($a[0]);
                        $new[] = trim($a[1]);
                    }
                    $route = 'human_design/gen_keys_item';
                    $a1 = Html::a($new[0], [$route, 'id' => $new[0]]);
                    $a2 = Html::a($new[1], [$route, 'id' => $new[1]]);
                    $a3 = Html::a($new[2], [$route, 'id' => $new[2]]);
                    $a4 = Html::a($new[3], [$route, 'id' => $new[3]]);
                }
                ?>
                <td><?= $text ?>
                    <?php if (!$isError) { ?>
                        (<?= $a1 ?>/<?= $a2 ?> | <?= $a3 ?>/<?= $a4 ?>)
                    <?php } ?>
                </td>
            </tr>
        </table>
        <h3 class="page-header text-center">Генные ключи</h3>
        <table class="table table-striped table-hover">
            <tr>
                <td>
                    #
                </td>
                <td>
                    Название
                </td>
                <td>
                    Тень
                </td>
                <td>
                    Дар
                </td>
                <td>
                    Сиддхи
                </td>
            </tr>
            <?php if (!$isError) { ?>
                <?php foreach ($new as $i) { ?>
                    <?php $genKey = \app\models\HdGenKeys::find(['num' => $i]) ?>
                    <tr class="rowTable" data-num="<?= $genKey->getField('num') ?>">
                        <td>
                            <?= $genKey->getField('num') ?>
                        </td>
                        <td>
                            <a href="<?= Url::to(['human_design/gen_keys_item', 'id' => $genKey->getField('num')]) ?>">
                                <?= $genKey->getField('name') ?>
                            </a>
                        </td>
                        <td>
                            <?= $genKey->getField('ten') ?>
                        </td>
                        <td>
                            <?= $genKey->getField('dar') ?>
                        </td>
                        <td>
                            <?= $genKey->getField('siddhi') ?>
                        </td>
                    </tr>
                <?php } ?>
            <?php } ?>
        </table>
    </div>
</div>