<?php
/**
 * Created by PhpStorm.
 * User: Бог-Творец
 * Date: 29.09.2016
 * Time: 19:49
 */
use yii\helpers\Html;

/** @var array $transactions */
/** @var array $confirmedBalance mBTC */

Yii::$app->session->set('$confirmedBalance', $confirmedBalance);


$columns = [
    [
        'header'  => 'ID',
        'content' => function ($item) {
            return
                Html::tag(
                    'abbr',
                    substr($item['hash'], 0, 8) . '...',
                    [
                        'class' => 'js-buttonTransactionInfo gsssTooltip',
                        'style' => 'font-family: "Courier New", Courier, monospace;',
                        'role'  => 'button',
                        'title' => 'Подробнее',
                        'data'  => [
                            'html'      => $item,
                            'placement' => 'bottom',
                        ],
                    ]
                );
        }
    ],
    [
        'header'         => 'Подтверждений',
        'headerOptions'  => [
            'style' => 'text-align:right',
        ],
        'contentOptions' => [
            'style' => 'text-align:right',
        ],
        'content'        => function ($item) {
            $confirmations = $item['confirmations'];
            if ($confirmations == 0) {
                $class = 'label label-danger';
            } else
                if ($confirmations < 6) {
                    $class = 'label label-warning';
                } else {
                    $class = 'label label-success';
                }
            return
                Html::tag(
                    'span',
                    Yii::$app->formatter->asDecimal($confirmations),
                    ['class' => $class]
                );
        }
    ],

    [
        'header'         => 'mBTC',
        'headerOptions'  => [
            'style' => 'text-align:right',
        ],
        'contentOptions' => function ($item) {
            $style = [];
            if ($item['wallet_value_change'] < 0) {
                $style[] = 'color: #F45F5F';
            } else {
                $style[] = 'color: #6ED098';
            }
            return [
                'style' => join(';', $style),
                'class' => 'code',
            ];
        },
        'content'        => function ($item) {
            $v = $item['wallet_value_change'];
            if ($v < 0) {
                $v += $item['total_fee'];
            }
            return Yii::$app->formatter->asDecimal($v / 100000, 5);
        }
    ],
    [
        'header'         => 'Комиссия mBTC',
        'headerOptions'  => [
            'style' => 'text-align:right',
        ],
        'contentOptions' => [
            'style' => 'color: #F45F5F;',
            'class' => 'code',
        ],
        'content'        => function ($item) {
            if ($item['wallet_value_change'] > 0) {
                return '';
            }

            return '-' . Yii::$app->formatter->asDecimal($item['total_fee'] / 100000, 5);
        }
    ],
    [
        'header'    => 'Время',
        'content'   => function ($item) {
            $t = $item['time'];
            $t = str_replace('+00:00', '', $t);
            $t = str_replace('T', ' ', $t);

            return Html::tag('abbr', \cs\services\DatePeriod::back($t, ['isShort' => true]), [
                'title' => Yii::$app->formatter->asDatetime($t),
                'class' => 'gsssTooltip',
            ]);
        },
        'attribute' => 'time'
    ],
    [
        'header'  => 'Баланс после',
        'headerOptions'  => [
            'style' => 'text-align:right',
        ],
        'contentOptions' => [
            'class' => 'code',
        ],
        'content' => function ($item) {
            $comission = 0;
            if ($item['wallet_value_change'] <= 0) {
                $comission = $item['total_fee'] / 100000;
            }


            $v = $item['wallet_value_change'];
            if ($v < 0) {
                $v += $item['total_fee'];
            }
            $increment = $v / 100000;

            $confirmedBalance = Yii::$app->session->get('$confirmedBalance');
            $confirmedBalance1 = $confirmedBalance;
            if ($comission > 0) {
                $confirmedBalance1 += $comission + abs($increment);
            } else {
                $confirmedBalance1 -= $increment;
            }
            Yii::$app->session->set('$confirmedBalance', $confirmedBalance1);

            return Yii::$app->formatter->asDecimal($confirmedBalance, 5);
        },
    ],
    [
        'header'  => 'Баланс до',
        'headerOptions'  => [
            'style' => 'text-align:right',
        ],
        'contentOptions' => [
            'class' => 'code',
        ],
        'content' => function ($item) {
            $confirmedBalance = Yii::$app->session->get('$confirmedBalance');

            return Yii::$app->formatter->asDecimal($confirmedBalance, 5);
        },
    ],
];

if (Yii::$app->deviceDetect->isMobile()) {
    $columns = [
        $columns[4],
        $columns[2],
        $columns[3],
    ];
}
?>

<?= \yii\grid\GridView::widget([
    'dataProvider' => new \yii\data\ArrayDataProvider([
        'allModels' => $transactions['data'],
    ]),
    'tableOptions' => [
        'class' => 'table table-hover table-striped',
        'id' => 'tableTransaction'
    ],
    'columns'      => $columns

]);
?>