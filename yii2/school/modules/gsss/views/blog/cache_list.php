<?php
use yii\helpers\Html;
use yii\bootstrap\ButtonDropdown;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $list array gs_chaneling_list.* */

foreach ($list as $item) {
    echo $this->render('../blocks/blog_item', [
        'item' => new \app\models\Blog($item),
    ]);
}
