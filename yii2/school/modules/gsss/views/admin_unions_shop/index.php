<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;
use app\models\UnionCategory;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model cs\base\BaseForm */
/* @var $union \app\models\Union */

$this->title = 'Магазин';
?>
<div class="container">
    <h1 class="page-header"><?= Html::encode($this->title) ?></h1>
    <?= \cs\Widget\BreadCrumbs\BreadCrumbs::widget([
        'items' => [
            [
                'label' => 'Объединения',
                'url'   => Url::to(['cabinet/objects']),
            ],
            [
                'label' => $union->getName(),
                'url'   => Url::to(['cabinet/objects_edit', 'id' => $union->getId()]),
            ],
            $this->title
        ],
    ]) ?>
    <hr>

    <?php if (Yii::$app->session->hasFlash('contactFormSubmitted')): ?>

        <div class="alert alert-success">
            Успешно обновлено.
        </div>

    <?php else: ?>


        <div class="row">
            <div class="col-lg-6 col-lg-offset-3">
                <?php $form = ActiveForm::begin([
                    'id'      => 'contact-form',
                    'options' => ['enctype' => 'multipart/form-data']
                ]); ?>
                <?= $model->field($form, 'name') ?>
                <?= $model->field($form, 'admin_email') ?>
                <?= $model->field($form, 'rekvizit')->textarea(['rows' => 10]) ?>
                <?= $model->field($form, 'mail_image') ?>
                <?= $model->field($form, 'signature')->textarea(['rows' => 3]) ?>
                <?= $model->field($form, 'contact')->textarea(['rows' => 3]) ?>

                <?= $model->field($form, 'yandex_id') ?>
                <?= $model->field($form, 'secret') ?>
                <?= $model->field($form, 'moderation_mode') ?>
                <?= $model->field($form, 'is_active') ?>
                <?= $model->field($form, 'is_piramida') ?>

                <hr>
                <div class="form-group">
                    <?= Html::submitButton('Обновить', [
                        'class' => 'btn btn-default',
                        'name'  => 'contact-button',
                        'style' => 'width:100%',
                    ]) ?>
                </div>
                <?php ActiveForm::end(); ?>
            </div>
        </div>

    <?php endif; ?>
</div>
