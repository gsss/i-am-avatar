<?php

use yii\helpers\Url;
use app\services\GsssHtml;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;

$this->title = 'Все новости';

$this->registerJs(<<<JS
$('.buttonDelete').click(function (e) {
    e.preventDefault();
    if (confirm('Подтвердите удаление')) {
        var id = $(this).data('id');
        ajaxJson({
            url: '/admin/news/' + id + '/delete',
            success: function (ret) {
                showInfo('Успешно', function() {
                    $('#newsItem-' + id).remove();
                });
            }
        });
    }
});

$('.rowTable').click(function() {
    window.location = '/admin/news/' + $(this).data('id') + '/edit';
});
JS
);

$sort = new \yii\data\Sort([
    'attributes'   => [
        'id',
        'header'       => ['label' => 'Название'],
        'date_insert'  => ['label' => 'Дата'],
        'view_counter' => ['label' => 'Счетчик'],
    ],
    'defaultOrder' => [
        'date_insert' => SORT_DESC,
    ],
])
?>

<div class="container">
    <div class="col-lg-12">
        <h1 class="page-header">Все Новости</h1>

        <div class="btn-group" style="margin-bottom: 20px;">
            <a href="<?= Url::to(['admin-news/add']) ?>" class="btn btn-default">Добавить</a>
        </div>


        <?= \yii\grid\GridView::widget([
            'dataProvider' => new \yii\data\ActiveDataProvider([
                'query'      => \app\models\NewsItem::query(),
                'pagination' => [
                    'pageSize' => 20,
                ],
                'sort'       => $sort,
            ]),
            'tableOptions' => [
                'class' => 'table table-striped table-hover',
            ],
            'rowOptions'   => function ($item) {
                return [
                    'data'  => ['id' => $item['id']],
                    'role'  => 'button',
                    'class' => 'rowTable',
                ];
            },
            'columns'      => [
                [
                    'attribute'     => 'id',
                    'header'        => $sort->link('id'),
                    'headerOptions' => [
                        'style' => 'width: 10%',
                    ],
                ],
                [
                    'header'        => 'Картинка',
                    'headerOptions' => [
                        'style' => 'width: 10%',
                    ],
                    'content'       => function ($item) {
                        $i = ArrayHelper::getValue($item, 'img', '');
                        if ($i == '') return '';

                        return Html::img($i, [
                            'class' => "thumbnail",
                            'width' => 80,
                            'style' => 'margin-bottom: 0px;',
                        ]);
                    },
                ],
                [
                    'attribute'     => 'header',
                    'header'        => $sort->link('header'),
                    'headerOptions' => [
                        'style' => 'width: 50%',
                    ],
                ],
                [
                    'attribute'     => 'date_insert',
                    'headerOptions' => [
                        'style' => 'width: 10%',
                    ],
                    'header'        => $sort->link('date_insert'),
                    'content'       => function ($item) {
                        $v = \yii\helpers\ArrayHelper::getValue($item, 'date_insert', 0);
                        if ($v == 0) return '';

                        return Html::tag('abbr', \cs\services\DatePeriod::back($v, ['isShort' => true]), ['class' => 'gsssTooltip', 'title' => Yii::$app->formatter->asDatetime($v)]);
                    },
                ],
                [
                    'attribute'      => 'view_counter',
                    'header'         => $sort->link('view_counter'),
                    'headerOptions'  => [
                        'style' => 'width: 10%',
                    ],
                    'format'         => ['decimal', 0],
                    'contentOptions' => [
                        'style' => 'text-align: right;',
                    ],
                ],
                [
                    'header'        => 'Удалить',
                    'headerOptions' => [
                        'style' => 'width: 10%',
                    ],
                    'content'       => function ($item) {
                        return Html::button('Удалить', [
                            'class' => 'btn btn-danger btn-xs buttonDelete',
                            'data'  => [
                                'id' => $item['id'],
                            ],
                        ]);
                    },
                ],
            ],
        ]) ?>
    </div>
</div>