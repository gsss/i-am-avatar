<?php
use yii\helpers\Html;
use yii\bootstrap\ButtonDropdown;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $html string код html */

$this->title = 'Послания';

$userId = Yii::$app->request->get('user_id', 0);
$author_id = Yii::$app->request->get('author_id', 0);
$this->registerJs(<<<JS
    var page = 1;
    var user_id = {$userId};
    var author_id = {$author_id};
    var isStartLoad = false;
    var isFinished = false;
    $(window).on('scroll', function() {

        var sizeBottom = $('#layoutMenuFooter').height() + $('#layoutMenuFooter').find('.container').height() + 420;
        if ($(window).scrollTop() > $(document).height() - sizeBottom) {

            if (isFinished == false) {
                if (isStartLoad === false) {
                    isStartLoad = true;
                    page++;
                    $('#chennelingPages').remove();
                    var data = {
                            page: page
                        };
                    if (user_id != 0) {
                        data.user_id = user_id;
                    }
                    if (author_id != 0) {
                        data.author_id = author_id;
                    }
                    ajaxJson({
                        url: '/chenneling/ajax',
                        data: data,
                        beforeSend: function() {
                            $('#channelingList').append(
                                $('<div>', {
                                    id: 'channelingListLoading',
                                    class: 'col-lg-12'
                                }).append(
                                    $('<img>', {src: pathLayoutMenu + '/images/ajax-loader.gif'})
                                )
                            );
                        },
                        success: function(ret) {
                            if (ret == '') isFinished = true;
                            $('#channelingListLoading').remove();
                            $('#channelingList').append(ret);
                            isStartLoad = false;
                        }
                    });
                }
            }
        }

    });
JS
);

?>

<?= $html ?>