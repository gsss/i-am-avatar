<?php
use yii\helpers\Html;
use yii\bootstrap\ButtonDropdown;
use yii\helpers\Url;
use cs\services\Url as csUrl;
use cs\services\Str;

/* @var $this yii\web\View */
/* @var $item array поля послания */
/* @var $nearList array похожие послания */

$this->title = $item['header'];

$channeling = new \app\models\Chenneling($item);

/**
 * Выдает заголовок для расшаривания
 *
 * @param array $item поля послания
 * @return string заголовок для расшанивания
 */
function getShareName($item)
{
    if ($item['share_title_type']) {
        switch($item['share_title_type']) {
            case 1: // заголовок
                return $item['header'];
                break;
            case 2: // Силы. Заголовок
                $power = $item['power_id'];
                if ($power) {
                    $p = \app\models\ChennelingPower::find($power);
                    $powerName = $p->get('name');
                    return $powerName . '. ' . $item['header'];
                } else {
                    return $item['header'];
                }
                break;
            case 3: // Автор. Заголовок
                $author_id = $item['author_id'];
                $author = $item['author'];
                if ($author_id) {
                    $a = \app\models\ChennelingAuthor::find($author_id);
                    $authorName = $a->get('name');
                    return $authorName . '. ' . $item['header'];
                } else if ($author) {
                    return $author . '. ' . $item['header'];
                } else {
                    return $item['header'];
                }
                break;
            case 4: // Силы через Автора. Заголовок
                /**
                 * 0 индекс - сила
                 * 1 индекс - автор
                 * 2 индекс - название
                 */
                $arr = [];
                $power = $item['power_id'];
                if ($power) {
                    $p = \app\models\ChennelingPower::find($power);
                    $powerName = $p->get('name');
                    $arr[0] = $powerName;

                    $author_id = $item['author_id'];
                    $author = $item['author'];
                    if ($author_id) {
                        $a = \app\models\ChennelingAuthor::find($author_id);
                        $authorName = $a->get('name_genitivus');
                        if ($authorName) {
                            $arr[1] = $authorName;
                        } else {
                            $authorName = $a->get('name');
                            if ($authorName) {
                                $arr[1] = $authorName;
                            } else {
                                if ($author) {
                                    $arr[1] = $author;
                                } else {
                                    return $item['header'];
                                }
                            }
                        }

                        return $arr[0] . ' через ' . $arr[1] . '. ' . $item['header'];
                    } else {
                        return $item['header'];
                    }
                } else {
                    return $item['header'];
                }
                break;
        }
    } else {
        return $item['header'];
    }

    return '';
}

?>
<div class="container">

    <div class="col-lg-12">
        <h1 class="page-header"><?= \yii\helpers\Html::encode($this->title) ?></h1>
    </div>

    <div class="col-lg-8" style="padding-bottom: 50px;">
        <p style="color: #ccc; font-family: 'Courier New', Monospace; font-size: 80%"><span class="glyphicon glyphicon-calendar" style=" margin-right: 10px;"></span><?= \app\services\GsssHtml::dateString($item['date_created']) ?> |
            <span class="glyphicon glyphicon-eye-open" style=" margin-right: 10px;"></span><?= Yii::$app->formatter->asDecimal($item['view_counter'], 0) ?></p>
        <hr>
        <?= $item['content'] ?>

<!--        Показ пользователя -->
        <?php if (\yii\helpers\ArrayHelper::getValue($item, 'user_id', '') != '') { ?>
            <hr>
            <?php
            $author = \app\models\User::find($item['user_id']);
            ?>
            <p>Автор: <img src="<?= $author->getAvatar() ?>" width="50" class="img-circle"> <a href="<?= $author->getLink() ?>"><?= $author->getName2() ?></a>
                <a href="<?=Url::to(['page/chenneling', 'user_id' => $author->getId()])?>" style="margin-left: 20px;">Все послания</a>
        <?php } ?>
        <?php if (\yii\helpers\ArrayHelper::getValue($item, 'author_id', '') != '') { ?>
            <hr>
            <?php
            $author = \common\models\ChannelingAuthor::findOne($item['author_id']);
            ?>
            <p>Автор: <img src="<?= $author->image ?>" width="50" class="img-circle"> <?= $author->name ?>
                <a href="<?=Url::to(['page/chenneling', 'author_id' => $author->id])?>" style="margin-left: 20px;">Все послания</a>
        <?php } ?>
        <hr>
        <?php if (isset($item['source'])): ?>
            <?php if ($item['source'] != ''): ?>
                <?= Html::a('Ссылка на источник »', $item['source'], [
                    'class'  => 'btn btn-primary',
                    'target' => '_blank',
                    'style'  => 'margin-bottom:10px;',

                ]) ?>
            <?php endif; ?>
        <?php endif;
        $image = $item['img'] . '';
        ?>

        <?= $this->render('../blocks/share', [
            'image'       => ($image != '')? Url::to(\cs\Widget\FileUpload2\FileUpload::getOriginal($item['img']), true) : '' ,
            'url'         => Url::current([], true),
            'title'       => $channeling->getShareName(),
            'description' => $item['description'],
        ]) ?>

        <?= $this->render('../blocks/yandexMoney2') ?>
        <?= $this->render('../blocks/fbLike') ?>
    </div>
    <div class="col-lg-4">
        <?php if (\yii\helpers\ArrayHelper::getValue(Yii::$app->params, 'isMarketing', 0) == 1) { ?>
            <?= $this->render('../blocks/reklama') ?>
        <?php } ?>
        <?php foreach ($nearList as $item) { ?>
            <?= $this->render('../blocks/chenneling', [
                'item'     => $item,
            ]) ?>
        <?php } ?>
        <?= $this->render('../blocks/reklama/block2') ?>
    </div>
</div>