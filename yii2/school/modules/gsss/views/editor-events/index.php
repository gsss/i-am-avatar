<?php

/** $this \yii\web\View  */

use yii\helpers\Url;
use app\services\GsssHtml;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use app\services\GetArticle\Collection;

$this->title = 'События';


?>

<div class="container">
    <div class="col-lg-12">
        <h1 class="page-header"><?= Html::encode($this->title) ?></h1>
        <a href="<?= Url::to(['editor-events/add']) ?>" class="btn btn-default">Добавить</a>


        <?php \yii\widgets\Pjax::begin(); ?>
        <?php
        $this->registerJS(<<<JS

    $('.buttonDelete').click(function (e) {
        e.preventDefault();
        e.stopPropagation();
        if (confirm('Подтвердите удаление')) {
            var button = $(this);
            var id = $(this).data('id');
            ajaxJson({
                url: '/editor-events/delete/' + id,
                success: function (ret) {
                    infoWindow('Успешно', function() {
                        button.parent().parent().remove();
                    });
                }
            });
        }
    });

    $('.rowTable').click(function() {
        window.location = '/editor-events/edit/' + $(this).data('id');
    });

JS
        );
        ?>
        <?= \yii\grid\GridView::widget([
            'dataProvider' => new \yii\data\ActiveDataProvider([
                'query'      => \app\models\Event::query()->orderBy(['start_date' => SORT_DESC]),
                'pagination' => [
                    'pageSize' => 20,
                ],
            ]),
            'tableOptions' => [
                'class' => 'table table-striped table-hover',
            ],
            'rowOptions'   => function ($item) {
                return [
                    'data' => ['id' => $item['id']],
                    'role' => 'button',
                    'class' => 'rowTable'
                ];
            },
            'columns'      => [
                'id',
                [
                    'header'  => 'Картинка',
                    'content' => function ($item) {
                        $i = ArrayHelper::getValue($item, 'image', '');
                        if ($i == '') return '';

                        return Html::img($i, [
                            'class' => "thumbnail",
                            'width' => 80,
                            'height' => 80,
                            'style' => 'margin-bottom: 0px;',
                        ]);
                    }
                ],
                'name:text:Название',
                'start_date:date:Дата начала',
                'end_date:date:Дата окончания',
                [
                    'header'  => 'Дата добавления',
                    'content' => function ($item) {
                        $v = \yii\helpers\ArrayHelper::getValue($item, 'date_insert', 0);
                        if ($v == 0) return '';

                        return Html::tag('abbr', \cs\services\DatePeriod::back($v, ['isShort' => true]), ['class' => 'gsssTooltip', 'title' => Yii::$app->formatter->asDatetime($v)]);
                    }
                ],
                [
                    'header'  => 'Удалить',
                    'content' => function ($item) {
                        return Html::button('Удалить', [
                            'class' => 'btn btn-danger btn-xs buttonDelete',
                            'data'  => [
                                'id' => $item['id'],
                            ]
                        ]);
                    }
                ],
            ],
        ]) ?>
        <?php \yii\widgets\Pjax::end(); ?>

    </div>
</div>