<?php

/** $this \yii\web\View  */

use yii\helpers\Url;
use app\services\GsssHtml;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use app\services\GetArticle\Collection;

$this->title = 'Все послания';

?>

<div class="container">
    <div class="col-lg-12">
        <h1 class="page-header"><?= Html::encode($this->title) ?></h1>

        <div class="btn-group" style="margin-bottom: 20px;" role="group" aria-label="...">
            <a href="<?= Url::to(['editor-channeling/add']) ?>" class="btn btn-default">Добавить</a>
            <a href="<?= Url::to(['editor-channeling/add-from-page']) ?>" type="button" class="btn btn-default">со
                страницы</a>
        </div>
        <?php \yii\widgets\Pjax::begin(); ?>
        <?php
        $this->registerJs(<<<JS
    $('.gsssTooltip').tooltip();

    $('.buttonDelete').click(function (e) {
        e.preventDefault();
        e.stopPropagation();
        if (confirm('Подтвердите удаление')) {
            var button = $(this);
            var id = $(this).data('id');
            ajaxJson({
                url: '/editor/channeling/' + id + '/delete',
                success: function (ret) {
                    infoWindow('Успешно', function() {
                        button.parent().parent().remove();
                    });
                }
            });
        }
    });

    // Сделать рассылку
    $('.buttonAddSiteUpdate').click(function (e) {
        e.preventDefault();
        e.stopPropagation();
        if (confirm('Подтведите свое намерение')) {
            var buttonSubscribe = $(this);
            var id = $(this).data('id');
            ajaxJson({
                url: '/editor/channeling/' + id + '/subscribe',
                success: function (ret) {
                    infoWindow('Успешно', function() {
                        buttonSubscribe.remove();
                    });
                }
            });
        }
    });

    $('.rowTable').click(function() {
        window.location = '/editor/channeling/' + $(this).data('id') + '/edit';
    });
JS
);
        ?>
        <?= \yii\grid\GridView::widget([
            'dataProvider' => new \yii\data\ActiveDataProvider([
                'query'      => \app\models\Chenneling::query()
                    ->orderBy(['date_created' => SORT_DESC])
                    ->where(['user_id' => null])
                ,
                'pagination' => [
                    'pageSize' => 20,
                ],
            ]),
            'tableOptions' => [
                'class' => 'table table-striped table-hover',
            ],
            'rowOptions'   => function ($item) {
                $data = [
                    'data'  => ['id' => $item['id']],
                    'role'  => 'button',
                    'class' => 'rowTable'
                ];
                if ($item['moderation_status'] != 1) {
                    $data['style'] = 'opacity: 0.5';
                }
                return $data;
            },
            'columns'      => [
                [
                    'attribute' => 'id',
                    'headerOptions' => [
                        'style' => 'width: 10%',
                    ],
                ],
                [
                    'header'  => 'Картинка',
                    'headerOptions' => [
                        'style' => 'width: 15%',
                    ],
                    'content' => function ($item) {
                        $i = ArrayHelper::getValue($item, 'img', '');
                        if ($i == '') return '';

                        return Html::img($i, [
                            'class'  => "thumbnail",
                            'width'  => 80,
                            'height' => 80,
                            'style'  => 'margin-bottom: 0px;',
                        ]);
                    }
                ],
                'header:text:Название',
                [
                    'header'  => 'Создано',
                    'headerOptions' => [
                        'style' => 'width: 15%',
                    ],
                    'content' => function ($item) {
                        $v = \yii\helpers\ArrayHelper::getValue($item, 'date_created', 0);
                        if ($v == 0) return '';

                        return Html::tag('abbr', \cs\services\DatePeriod::back($v, ['isShort' => true]), ['class' => 'gsssTooltip', 'title' => Yii::$app->formatter->asDatetime($v)]);
                    }
                ],
                [
                    'header'  => 'Удалить',
                    'content' => function ($item) {
                        return Html::button('Удалить', [
                            'class' => 'btn btn-danger btn-xs buttonDelete',
                            'data'  => [
                                'id' => $item['id'],
                            ]
                        ]);
                    }
                ],
                [
                    'header'  => 'Рассылка',
                    'headerOptions' => [
                        'style' => 'width: 10%',
                    ],
                    'content' => function ($item) {
                        if (ArrayHelper::getValue($item, 'is_added_site_update', 0) == 1) {
                            return '';
                        }
                        if ($item['moderation_status'] != 1) {
                            return '';
                        }
                        return Html::button('Рассылка', [
                            'class' => 'btn btn-success btn-xs buttonAddSiteUpdate',
                            'data'  => [
                                'id' => $item['id'],
                            ]
                        ]);
                    }
                ]
            ],
        ]) ?>
        <?php \yii\widgets\Pjax::end(); ?>
    </div>
</div>