<?php

/** @var \yii\web\View $this */
/** @var integer $id */

$this->title = 'Переводы';

function getLink($params = [])
{
    $options = Yii::$app->request->get();
    $options[] = 'languages/category';
    $options = \yii\helpers\ArrayHelper::merge($options, $params);

    return \yii\helpers\Url::to($options);
}

/**
 * В этом языке вывожу все данные
 */
$language = Yii::$app->request->get('language', Yii::$app->language);
$languageId = null;

$languages = \common\models\Language::find()->where(['not', ['code' => 'ru']])->asArray()->all();

$languageInfo = [
    'languages' => $languages,
    'default'   => 0,
    'count'     => count($languages),
];
Yii::$app->cache->set('languages.cache', $languageInfo);
/**
 * в переменной хранится список языков
 * переменная default говорит о том какой язык нужно вывести в текущей рисуемой ячейке
 * после заканчивания рисования всех ячеек в одной строке я возвращаюсь в начало
 * русский выводится первым и с запросом
 * следовательно в массиве все языки кроме русского и индексы расставляются начиная с 0
 */
$category = \common\models\language\Category::findOne(Yii::$app->request->get('id'));
?>

<style>
    a.asc:after {
        content: ' ↓';
        display: inline;
    }

    a.desc:after {
        content: ' ↑';
        display: inline;
    }
</style>
<div class="container">


<h2>Переводы</h2>

<p>Категории</p>

<?php \yii\widgets\Pjax::begin(); ?>
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                        aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Добавление новой строки</h4>
            </div>
            <div class="modal-body">
                <div class="col-lg-12" style="margin: 40px 0px 40px 0px;">
                    <p>Индекс:</p>
                    <input class="form-control" style="margin-bottom: 20px;">

                    <p class="label label-danger labelError" style="display: none;">Индекс:</p>
                </div>
            </div>
            <div class="modal-footer">
                <?php
                $this->registerJs(<<<JS
                    $('#myModal .modal-body input').on('focus', function(e) {
                        $('.labelError').hide();
                    });
                    $('.buttonSave').click(function(e) {
                        ajaxJson({
                            url: '/languages/add-key',
                            data: {
                                value: $('#myModal .modal-body input').val(),
                                category: $('#category').html()
                            },
                            success:function(ret) {
                                var id = ret;
                                $('#tableMessages').find('tbody')
                                .append(
                                    $('<tr>')
                                    .append(
                                        $('<td>').html(
                                            $('<input>', {type:'checkbox', name: 'selection[]', value:id})
                                        )
                                    )
                                    .append(
                                        $('<td>').html(id)
                                    )
                                    .append(
                                        $('<td>').html($('#myModal .modal-body input').val())
                                    )
                                    .append(
                                        $('<td>').html(
                                            $('<div>', {class: 'input-group', style: 'width: 100%'})
                                            .append($('<input>', {
                                                type:      'text',
                                                id:        'value_ru_' + id,
                                                class:     'form-control',
                                                name:      'message',
                                                'data-id': ret.data,
                                                'style':   'width: 200px'
                                            }))
                                            .append(
                                                $('<span>', {
                                                    class:     'input-group-btn'
                                                })
                                                .append(
                                                    $('<button>', {
                                                        class: 'btn btn-default buttonUpdate',
                                                        'data-language': 'ru',
                                                        'data-id': id
                                                    }).click(functionUpdate).on('mouseover', function() {
                                                        var b = $(this);
                                                        b.removeClass('btn-default').addClass('btn-info');
                                                    }).on('mouseout', function() {
                                                      var b = $(this);
                                                        b.removeClass('btn-info').removeClass('btn-success').addClass('btn-default');
                                                    })
                                                    .append($('<span>', {class: 'glyphicon glyphicon-ok'}))
                                                )
                                            )
                                        )
                                    )
                                )
                                ;
                                $('#myModal').modal('hide');
                            },
                            errorScript: function(ret) {
                                $('.labelError').html(ret.data).show();
                            }
                        });
                    });
JS
                );
                ?>
                <button type="button" class="btn btn-primary buttonSave">Сохранить</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Отмена</button>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="modalInfo" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                        aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Успешно</h4>
            </div>
            <div class="modal-body">
                <div class="col-lg-12" style="margin: 40px 0px 40px 0px;">
                    <p>Успешно скопировано в буфер.</p>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="modalEdit" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                        aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Успешно</h4>
            </div>
            <div class="modal-body">
                <div class="col-lg-12" style="margin: 40px 0px 40px 0px;">
                    <input class="form-control">
                </div>
            </div>
            <div class="modal-footer">
                <?php
                $this->registerJs(<<<JS
$('.buttonSaveCategory').click(function(ret){
    var id = $('#modalEdit').data('id');
    ajaxJson({
        url: '/languages/category-save',
        data: {
            id: id,
            name: $('#modalEdit .modal-body input').val()
        },
        success: function(ret) {
            $('#categoryName_' + id).html($('#modalEdit .modal-body input').val());
            $('#modalEdit').modal('hide');
        }
    });
});
JS
                );
                ?>
                <button type="button" class="btn btn-default buttonSaveCategory">Сохранить</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="modalError" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                        aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Ошибка</h4>
            </div>
            <div class="modal-body">
                <div class="col-lg-12" style="margin: 40px 0px 40px 0px;">
                    <p>Выберите строки для экспорта</p>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
            </div>
        </div>
    </div>
</div>

<div class="col-lg-3">
    <?= $this->render('_tree', [
        'rows'    => \common\models\language\Category::getRows(),
        'options' => ['id' => $id],
    ]); ?>
    <p style="margin-top: 20px;">
        <a href="/languages/category-add" data-pjax="0" style="width: 100%;" class="btn btn-success">Добавить</a>
    </p>
<?php
$this->registerJs(<<<JS
$('.buttonEdit').click(function(e) {
    var id = $(this).data('id');

    $('#modalEdit input').val($('#categoryName_'+id).html().trim());
    $('#modalEdit').data('id', $(this).data('id')).modal();
});
$('.buttonCategoryDelete').click(function(e) {
    var o = $(this);
    if (confirm('Вы уверены?')) {
        ajaxJson({
            url: '/languages/category-delete',
            data: {
                id: o.data('id')
            },
            success: function(ret) {
                $('#categoryName_' + o.data('id')).parent().remove();
                alert('Успешно');
                window.location = '/languages/category';
            }
        });
    }
});
$('.buttonCategoryExport').click(function(e) {
    var o = $(this);
    window.location = '/languages/category-export?id=' + o.data('id');
});
JS
);

?>
</div>

<div class="col-lg-9">

<?php if (Yii::$app->request->get('id')) { ?>
    <div class="hide" id="category" data-id="<?= $category->id ?>"><?= $category->code ?></div>
    <?php
    $languageInfo['category'] = $category;
    Yii::$app->cache->set('languages.cache', $languageInfo);

    $this->registerJs(<<<JS
$('.buttonCopy').on('mouseover', function() {
    var b = $(this);
    b.removeClass('btn-default').addClass('btn-info');
}).on('mouseout', function() {
    var b = $(this);
    b.removeClass('btn-info').removeClass('btn-success').addClass('btn-default');
});
var language = 1;
var functionUpdate = function() {
    var b = $(this);
    var id = b.data('id');
    var language = b.data('language');
    var value = $('#value_' + language + '_' + id).val();
    b.click(null);
    ajaxJson({
        url: '/languages/update-string',
        data: {
            id: id,
            value: value,
            language: language
        },
        success: function(ret) {
            b.removeClass('btn-default').removeClass('btn-info').addClass('btn-success');
            b.parent().parent().addClass('has-success');
            window.setTimeout(function() {
                b.removeClass('btn-success').addClass('btn-default');
                b.parent().parent().removeClass('has-success');
            }, 1000);
        },
        errorScript: function(ret) {
            alert(ret.data.data);
        }
    });
};
$('.buttonUpdate').click(functionUpdate).on('mouseover', function() {
    var b = $(this);
    b.removeClass('btn-default').addClass('btn-info');
}).on('mouseout', function() {
  var b = $(this);
    b.removeClass('btn-info').removeClass('btn-success').addClass('btn-default');
});
JS
    );

    $sort = new \yii\data\Sort([
        'attributes' => [
            'id' => [
                'default' => SORT_ASC,
                'label'   => 'id',
            ],
            'message',
            'translation',
        ],
    ]);
    $searchModel = new \app\models\Form\MessageSearch();
    $dataProvider = $searchModel->search($category, 'ru', $sort, Yii::$app->request->get());
    ?>
    <style>
        .pagination {
            margin: 20px 0px 20px 0px;
        }
    </style>
    <?php
    \app\assets\ZeroClipboard\Asset::register($this);
    $this->registerJs(<<<JS

        // Сюда указываем элемент, по которому будет запускаться процедура копирования.
		var target = $(".buttonCopy");
		var client = new ZeroClipboard(target);

		target.click(function (e) {
			e.preventDefault();
		});

		client.on("copy", function (event) {
			// Текст, который вставится в буфер.
			var textToCopy = $(event.target).data('message');
			var clipboard = event.clipboardData;

			clipboard.setData("text/plain", textToCopy);
		});

		// Событие, которое вызывается после копирования. Можно, например, вывести какое-то окошко.
		client.on('aftercopy', function (event) {
			$('#modalInfo').modal()
		});
JS
    );
    $columns = [
        [
            'class' => 'yii\grid\CheckboxColumn', // <-- here
            // you may configure additional properties here
        ],
        [
            'header'        => $sort->link('id'),
            'headerOptions' => [
                'style' => 'width:100px;',
                'role'  => 'button',
            ],
            'attribute'     => 'id',
        ],
        [
            'header'         => $sort->link('message'),
            'headerOptions'  => [
                'style' => 'width:300px;',
                'role'  => 'button',
            ],
            'contentOptions' => [
                'style' => 'width:200px;',
            ],
            'attribute'      => 'message',
            'content'        => function ($item) {
                $l = 30;
                $message = \yii\helpers\ArrayHelper::getValue($item, 'message');
                if (\cs\services\Str::length($message) > $l) {
                    $message = \cs\services\Str::sub($message, 0, $l) . ' ...';
                }
                return \yii\helpers\Html::tag(
                    'div'
                    ,
                    \yii\helpers\Html::encode($message)
                    ,
                    [
                        'style' => 'width: 200px',
                    ]
                );
            }
        ],
        [
            'header'  => 'Копировать',
            'content' => function ($item) {
                $class = 'glyphicon glyphicon-copyright-mark';

                return \yii\helpers\Html::button(\yii\helpers\Html::tag('i', null, ['class' => $class]), [
                    'class' => 'btn btn-default buttonCopy',
                    'data'  => [
                        'message' => $item['message'],
                        'id'      => $item['id'],
                    ],
                ]);
            }
        ],
        [
            'header'  => 'Копировать',
            'content' => function ($item) {
                $category = $item['category'];
                $message = $item['message'];
                $message = \yii\helpers\VarDumper::export($message);
                $string = "Yii::t('{$category}', {$message})";

                return \yii\helpers\Html::button('php', [
                    'class' => 'btn btn-default buttonCopy',
                    'data'  => [
                        'message' => $string,
                        'id'      => $item['id'],
                    ],
                ]);
            }
        ],
        [
            'header'        => 'Русский',
            'headerOptions' => [
                'style' => 'width:400px;',
                'role'  => 'button',
            ],
            'attribute'     => 'translation',
            'content'       => function ($item) {
                $arr = [];
                $arr[] = \yii\helpers\Html::input('text', 'message', $item['translation'], [
                    'class'   => 'form-control',
                    'id'      => 'value_ru_' . $item['id'],
                    'data-id' => $item['id'],
                    'style'   => 'width: 200px',
                ]);
                $arr[] = \yii\helpers\Html::tag('span',
                    \yii\helpers\Html::button(
                        \yii\helpers\Html::tag('span', null, [
                            'class' => 'glyphicon glyphicon-ok',
                        ])
                        , [
                            'class' => 'btn btn-default buttonUpdate',
                            'data'  => [
                                'language' => 'ru',
                                'id'       => $item['id'],
                            ],
                        ])
                    , [
                        'class' => 'input-group-btn',
                    ]);

                return \yii\helpers\Html::tag("div", join("\n", $arr), [
                    'class' => 'input-group',
                    'style' => 'width: 100%',
                ]);
            }
        ],
    ];
    /** @var \common\models\Language $language */
    foreach ($languageInfo['languages'] as $language) {
        $columns[] = [
            'header'        => $language['name'],
            'headerOptions' => [
                'style' => 'width:400px;',
                'role'  => 'button',
            ],
            'content'       => function ($item) {
                $languageInfo = Yii::$app->cache->get('languages.cache');
                $default = $languageInfo['default'];
                /** @var \common\models\language\Category $category */
                $category = $languageInfo['category'];
                $language = $languageInfo['languages'][$default];
                $arr = [];

                // перевод
                {
                    $t = \common\models\language\Message::find()->where([
                        'id'       => $item['id'],
                        'language' => $language['code'],
                    ])->one();
                    $t = (is_null($t)) ? '' : $t->translation;
                }

                $arr[] = \yii\helpers\Html::input('text', 'message', $t, [
                    'class'   => 'form-control',
                    'id'      => 'value_' . $language['code'] . '_' . $item['id'],
                    'data-id' => $item['id'],
                    'style'   => 'width: 200px',
                ]);
                $arr[] = \yii\helpers\Html::tag('span',
                    \yii\helpers\Html::button(
                        \yii\helpers\Html::tag('span', null, [
                            'class' => 'glyphicon glyphicon-ok',
                        ])
                        , [
                            'class' => 'btn btn-default buttonUpdate',
                            'data'  => [
                                'language' => $language['code'],
                                'id'       => $item['id'],
                            ],
                        ])
                    , [
                        'class' => 'input-group-btn',
                    ]);
                $default++;
                if ($default >= $languageInfo['count']) {
                    $default = 0;
                }
                $languageInfo['default'] = $default;
                Yii::$app->cache->set('languages.cache', $languageInfo);

                return \yii\helpers\Html::tag("div", join("\n", $arr), [
                    'class' => ($t == '') ? 'input-group has-error' : 'input-group',
                    'style' => 'width: 100%',
                ]);
            }
        ];
    }
    $this->registerJs(<<<JS
        $('.buttonDelete').click(function(e) {
            if (confirm('Вы уверены?')) {
                var b = $(this);
                    ajaxJson({
                        url: '/languages/delete',
                        data: {id: b.data('id')},
                        success: function(ret) {
                            b.parent().parent().remove();
                            alert('Успешно');
                        }
                    });
            }
        });
JS
    );
    $columns[] = [
        'header'  => 'Удалить',
        'content' => function ($item) {
            return \yii\helpers\Html::button(
                \yii\helpers\Html::tag(
                    'i',
                    null,
                    ['class' => 'glyphicon glyphicon-remove']
                ),
                [
                    'class' => 'btn btn-danger buttonDelete',
                    'data'  => [
                        'id' => $item['id']
                    ]
                ]
            );
        }
    ];

    ?>
    <?= \yii\grid\GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel'  => $searchModel,
        'tableOptions' => [
            'class' => 'table table-hover table-striped',
            'style' => 'width: auto;',
            'id'    => 'tableMessages'
        ],
        'columns'      => $columns,
        'summary'      => '',
    ]); ?>
    <?php
    $this->registerJs(<<<JS
        $('#buttonExport').click(function(e) {
            var ids = [];
            $('input[name="selection[]"]').each(function(i,v) {
                if ($(v).is(':checked')) {
                    ids.push(parseInt($(v).attr('value')));
                }
            });
            if (ids.length == 0) {
                $('#modalError').modal();
                return false;
            }
            window.location = '/languages/export?ids=' + ids.join(',');
        });
JS
    );
    ?>
    <button class="btn btn-default" id="buttonExport" style="width:200px; margin-top: 20px;">Экспортировать</button>
    <button class="btn btn-default" data-toggle="modal" data-target="#myModal" style="width:600px; margin-top: 20px;">Добавить</button>
<?php } ?>
</div>
<?php \yii\widgets\Pjax::end(); ?>
</div>