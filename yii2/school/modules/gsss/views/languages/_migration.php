<?php
/**
 * Created by PhpStorm.
 * User: s.arhangelskiy
 * Date: 26.09.2016
 * Time: 15:51
 */

/** @var \yii\web\View $this */
/** @var array $ids идентификаторы строк для экспорта */
/** @var string $className */

echo "<?php\n";


$ret = [];
$rows = \common\models\language\SourceMessage::find()->where(['in', 'id', $ids])->all();
/** @var \common\models\language\SourceMessage $message */
foreach ($rows as $message) {
    $row = \yii\helpers\ArrayHelper::toArray($message);
    $translations = \yii\helpers\ArrayHelper::map(\common\models\language\Message::find()
        ->where(['id' => $message->id])
        ->select([
            'language',
            'translation',
        ])
        ->asArray()
        ->all(), 'language', 'translation')
    ;
    $row['translations'] = $translations;
    $ret[] = $row;
}


if (isset($categoryTree)) {
    $ret = [
        'categoryTree'  => $categoryTree,
        'messages'      => $ret,
    ];
} else {
    $ret = [
        'messages' => $ret,
    ];
}

?>

use yii\db\Schema;
use yii\db\Migration;

class <?= $className ?> extends \common\components\Migration
{

    public function up()
    {
        $strings = <?= \yii\helpers\VarDumper::export($ret) ?>;
        $this->updateStrings($strings);
    }

    public function down()
    {
        echo "<?= $className ?> cannot be reverted.\n";

        return false;
    }
}