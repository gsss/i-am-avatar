<?php
/**
 * Created by PhpStorm.
 * User: s.arhangelskiy
 * Date: 27.09.2016
 * Time: 17:08
 */

/** @var \yii\web\View $this */
/** @var \common\models\language\Category $model */

?>
<div class="container">

<h1>Добавить категорию</h1>

<?php if (Yii::$app->session->hasFlash('form')) { ?>
    <p class="alert alert-success">Удачно</p>
<?php } else  { ?>
    <?php $form = \yii\bootstrap\ActiveForm::begin([]) ?>
    <?= $form->field($model, 'name') ?>
    <?= $form->field($model, 'code') ?>
    <?= $form->field($model, 'parent_id')->widget('app\common\widgets\Tree\Tree', ['className' => 'common\models\language\Category']) ?>
    <?= \yii\helpers\Html::submitButton('Добавить', ['class' => 'btn btn-success'])?>
    <?php \yii\bootstrap\ActiveForm::end() ?>
<?php } ?>
    </div>
