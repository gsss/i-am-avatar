<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;
use app\models\UnionCategory;
use yii\db\Query;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $item \common2\models\SendLetter */

$this->title = $item->name . ' ' . $item->name . ' от ' . Yii::$app->formatter->asDatetime($item->time);

?>
<div class="container">
    <div class="col-lg-12">
        <div class="page-header">
            <h1><?= Html::encode($this->title) ?></h1>
        </div>

        <?= \yii\widgets\DetailView::widget([
            'model'      => $item,
            'attributes' => [
                'id',
                'name',
                'email',
                [
                    'attribute' => 'text',
                    'format'    => 'raw',
                    'value'     => nl2br(Html::encode($item->text)),
                ],
                [
                    'attribute' => 'time',
                    'format'    => 'datetime',
                ],
            ],
        ]) ?>
    </div>
</div>
