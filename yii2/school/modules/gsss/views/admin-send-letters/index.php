<?php

/** $this \yii\web\View  */

use yii\helpers\Url;
use app\services\GsssHtml;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use app\services\GetArticle\Collection;

$this->title = 'Все письма';

?>

<div class="container">
    <div class="col-lg-12">
        <h1 class="page-header"><?= Html::encode($this->title) ?></h1>
        <style>
            a.asc:after {
                content: ' ↓';
                display: inline;
            }
            a.desc:after {
                content: ' ↑';
                display: inline;
            }
        </style>
        <?php \yii\widgets\Pjax::begin(); ?>
        <?php
        $this->registerJS(<<<JS
    $('.rowTable').click(function() {
        window.location = '/admin-send-letters/view/' + $(this).data('id');
    });
JS
        );
        $sort = new \yii\data\Sort([
            'attributes' => [
                'id',
                'name',
                'email',
                'time',
            ],
            'defaultOrder' => [
                'time' => SORT_DESC
            ],
        ]);
        ?>

        <?= \yii\grid\GridView::widget([
            'dataProvider' => new \yii\data\ActiveDataProvider([
                'query'      => \common\models\SendLetter::find(),
                'pagination' => [
                    'pageSize' => 20,
                ],
                'sort' => $sort,
            ]),
            'tableOptions' => [
                'class' => 'table table-striped table-hover',
            ],
            'rowOptions'   => function ($item) {
                $data = [
                    'data'  => ['id' => $item['id']],
                    'role'  => 'button',
                    'class' => 'rowTable'
                ];
                return $data;
            },
            'columns'      => [
                'id',
                'name:text:От кого',
                'email:text:email',
                [
                    'header'  => 'Текст',
                    'content' => function ($item) {
                        $s = ArrayHelper::getValue($item, 'text');
                        $length = 50;
                        if (\cs\services\Str::length($s) > $length) {
                            $s = \cs\services\Str::sub($s, 0, $length) . ' ...';
                        }

                        return Html::encode($s);
                    }
                ],
                'time:datetime:Создано',
            ],
        ]) ?>
        <?php \yii\widgets\Pjax::end(); ?>
    </div>
</div>