<?php

/** $this \yii\web\View  */

use yii\helpers\Url;
use app\services\GsssHtml;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use app\services\GetArticle\Collection;

$this->title = 'Штрафные квитанции';

$controller = 'admin-penalty'
?>

<div class="container">
    <div class="col-lg-12">
        <h1 class="page-header">
            <?= Html::encode($this->title) ?>
        </h1>
        <div class="btn-group" style="margin-bottom: 20px;" role="group" aria-label="...">
            <a href="<?= Url::to([$controller . '/add']) ?>" class="btn btn-default">Добавить</a>
        </div>
        <?php \yii\widgets\Pjax::begin(); ?>
        <?php
        $this->registerJS(<<<JS
    $('.buttonDelete').click(function (e) {
        e.preventDefault();
        e.stopPropagation();
        if (confirm('Подтвердите удаление')) {
            var button = $(this);
            var id = $(this).data('id');
            ajaxJson({
                url: '/{$controller}/' + 'delete/' + id,
                success: function (ret) {
                    infoWindow('Успешно', function() {
                        button.parent().parent().remove();
                    });
                }
            });
        }
    });

    $('.rowTable').click(function() {
        window.location = '/{$controller}' + '/edit/' + $(this).data('id');
    });
JS
        );
        ?>
        <?= \yii\grid\GridView::widget([
            'dataProvider' => new \yii\data\ActiveDataProvider([
                'query'      => \app\models\Penalty::find(),
                'pagination' => [
                    'pageSize' => 20,
                ],
                'sort' => new \yii\data\Sort([
                    'defaultOrder' => [
                        'date_insert' => SORT_DESC
                    ],
                ])
            ]),
            'tableOptions' => [
                'class' => 'table table-striped table-hover',
            ],
            'rowOptions'   => function ($item) {
                $data = [
                    'data'  => ['id' => ArrayHelper::getValue($item, 'id')],
                    'role'  => 'button',
                    'class' => 'rowTable'
                ];
                return $data;
            },
            'columns'      => [
                'id',
                'id_penalty',
                [
                    'header'  => 'Архангел',
                    'content' => function ($item) {
                        $user_id = ArrayHelper::getValue($item, 'user_id');
                        $u = \app\services\UsersInCache::find($user_id);
                        if (is_null($u)) return '';
                        $arr = [];
                        $arr[] = Html::img($u['avatar'], ['width' => 50, 'class' => 'thumbnail', 'style' => 'margin-bottom: 0px; margin-right: 10px;float: left;']);
                        $arr[] = Html::a($u['name'], '/user/' . $u['id']);
                        $arr[] = '<br>';
                        $arr[] = '<small style="color: #888;">' . $u['email'] . '</small>';

                        return join('', $arr);
                    },
                    'attribute' => 'user_id',
                ],
                'identy:text:Виновник',
                'reason:text:Причина',
                'date_insert:datetime:Время добавления',
                'date:date:Время события',
                [
                    'header'  => 'Оплачена?',
                    'content' => function ($item) {
                        $v = \yii\helpers\ArrayHelper::getValue($item, 'is_paid', 0);
                        if ($v == 0) return Html::tag('span', 'Нет', ['class' => 'label label-default']);

                        return Html::tag('span', 'Да', ['class' => 'label label-success']);
                    }
                ],
                [
                    'header'  => 'Удалить',
                    'content' => function ($item) {
                        return Html::button('Удалить', [
                            'class' => 'btn btn-danger btn-xs buttonDelete',
                            'data'  => [
                                'id' => $item['id'],
                            ]
                        ]);
                    }
                ],
            ],
        ]) ?>
        <?php \yii\widgets\Pjax::end(); ?>
    </div>
</div>