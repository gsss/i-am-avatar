<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;
use app\models\UnionCategory;
use yii\db\Query;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model cs\base\BaseForm */

$this->title = $model->name;

?>
<div class="container">
    <div class="page-header text-center">
        <h1><?= Html::encode($this->title) ?></h1>
    </div>

    <?php if (Yii::$app->session->hasFlash('contactFormSubmitted')): ?>

        <div class="alert alert-success">
            Успешно обновлено.
        </div>

    <?php else: ?>


        <div class="row">
            <div class="col-lg-6 col-lg-offset-3">
                <?php $form = ActiveForm::begin([
                    'id'      => 'contact-form',
                    'options' => ['enctype' => 'multipart/form-data']
                ]); ?>
                <?= $model->field($form, 'name') ?>
                <?= $model->field($form, 'start_date') ?>
                <?= $model->field($form, 'start_time') ?>
                <?= $model->field($form, 'end_date') ?>
                <?= $model->field($form, 'end_time') ?>
                <?= $model->field($form, 'content') ?>
                <?= $model->field($form, 'image') ?>
                <?= $model->field($form, 'link') ?>
                <?= $model->field($form, 'date') ?>
                <hr>

                <div class="form-group">
                    <?= Html::submitButton('Обновить', [
                        'class' => 'btn btn-primary btn-lg',
                        'name'  => 'contact-button',
                        'style' => 'width:100%',
                    ]) ?>
                </div>
                <?php ActiveForm::end(); ?>
            </div>
        </div>

    <?php endif; ?>
</div>
