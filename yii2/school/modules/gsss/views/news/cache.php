<?php
use yii\helpers\Html;
use yii\bootstrap\ButtonDropdown;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $items array gs_chaneling_list.* */

?>
<div class="container">
    <div class="col-sm-12">
        <h1 class="page-header">Новости планеты Земля</h1>
    </div>
    <!-- /.row -->
    <div id="channelingList">
        <?= $this->render('cache_list', ['list' => $list]); ?>
    </div>
    <div class="row col-lg-12">
        <div id="chennelingPages">
            <?php foreach ($pages['list'] as $num) { ?>
                <a href="<?= Url::current(['page' => $num]) ?>"><?= $num ?></a>
            <?php } ?>
        </div>
    </div>
</div>