<?php

/** @var $item array article */
/** @var $category string */

use yii\helpers\Html;
use yii\bootstrap\ButtonDropdown;
use yii\helpers\Url;
use cs\services\Url as csUrl;

$id = \common\models\Config::get('reklama', 1);

?>

<?php if ($id == 1) { ?>
    <?php $item = \app\models\Service::find(11); ?>
    <div class="thumbnail">
        <a href="/category/medical/344/shop/product/1">
            <img
                    alt="Получить Акселератор Мозга в подарок"
                    src="<?= $item->getImage() ?>"
                    style="width: 100%; display: block;"
            >
        </a>

        <div class="caption">
            <h3><?= $item->getName() ?></h3>

            <p><?= $item->getDescription() ?></p>
        </div>
    </div>
<?php } ?>

<?php if ($id == 2) { ?>
    <div class="thumbnail">
        <a href="/shop/product/17">
            <img alt="Купить Ключ к Сердцу"
                 src="/upload/FileUpload3/gs_unions_shop_product/00000017/small/image.jpg"
                 style="width: 100%; display: block;">
        </a>

        <div class="caption">
            <h3>Купить Ключ к Сердцу</h3>

            <p>Это уникальная разработка от Лаборатории «Аватар», завоевавшая и открывшая сердца всех, кто ею
                пользовался. Этот ключ универсальный, подходит как для мужского, так и для женского сердца. Открывает
                даже самые скрипучие и тяжело заваленные камнями сердца. Поставляется с инструкцией по применению и
                открытию сердца.</p>
        </div>
    </div>
<?php } ?>


<?php if ($id == 4) { ?>
    <div class="thumbnail">
        <a href="/shop/product/58">
            <img alt="Квантовая Голограмма Аватар Свободы"
                 src="/upload/FileUpload3/gs_unions_shop_product/00000058/small/image.jpg"
                 style="width: 100%; display: block;"
            >
        </a>

        <div class="caption">
            <h3>Квантовая Голограмма Аватар Свободы</h3>
            <p>Будь хозяином своей жизни!</p>
            <p>Сегодня это возможно для Тебя!</p>
            <p>Все начинается с Веры в Себя!</p>
            <p>Все в твоих руках!</p>
        </div>
    </div>
<?php } ?>
<?php if ($id == 5) { ?>
    <div class="thumbnail">
        <a href="http://www.i-am-avatar.com/page/new-earth" target="_blank">
            <img alt="23 февраля 20:00 МСК. Видеопередача Новая Земля"
                 src="/images/nw75280223.png"
                 style="width: 100%; display: block;"
            >
        </a>

        <div class="caption">
            <h3>Видеопередача Новая Земля</h3>
            <p>23 февраля 20:00 МСК</p>

            <p>О чем передача:<br>
                - Идентификация и Самоопределение<br>
                - Важность Имени и как ее нужно определять<br>
                - Что такое священное Имя Высшего Я<br>
                - Зачем это нужно<br>
                - Какие составляющие самоидентификации<br>
                - Как она влияет на способность добавиться результата<br>
                - Как нам всем объединиться после самоопределения<br>
                - Цифровая идентификация</p>

            <p class="text-center"><a href="http://www.i-am-avatar.com/page/new-earth" target="_blank" class="btn btn-success" style="width: 100%; ">Подробнее</a></p>
        </div>
    </div>
<?php } ?>

