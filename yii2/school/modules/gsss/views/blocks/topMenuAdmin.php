<?php
use yii\helpers\Url;
use yii\helpers\Html;

function liMenu($route, $name = null, $options = [])
{
    if (is_array($route)) {
        $arr = [];
        foreach ($route as $route1 => $name1) {
            $arr[] = liMenu($route1, $name1);
        }

        return join("\n", $arr);
    }
    if (Yii::$app->requestedRoute == $route) {
        $options['class'] = 'active';
    }

    return \yii\helpers\Html::tag('li', \cs\helpers\Html::a($name, [$route]), $options);
}

$menu = [
    [
        'label' => 'Мониторинг',
        'items' => [
            [
                'label' => 'Статистика',
                'route' => 'admin-monitoring/index',
            ],
            [
                'label' => 'Онлайн список',
                'route' => 'admin-monitoring/online',
            ],
            [
                'label' => 'Регистраций в день',
                'route' => 'admin-monitoring/registration',
            ],
            [
                'label' => 'Отправленные письма',
                'route' => 'admin-monitoring/mails',
            ],
            [
                'label' => 'Время исполнения',
                'route' => 'admin-monitoring/timer',
            ],
            [
                'label' => 'База данных',
                'route' => 'admin-monitoring/db',
            ],
            [
                'label' => 'Ошибки',
                'route' => 'admin-monitoring/errors',
            ],
        ],
    ],
    [
        'label'   => 'Послания',
        'urlList' => [
            ['controller', 'admin-channeling'],
            ['controller', 'admin_channeling_author'],
            ['controller', 'admin_channeling_power'],
        ],
        'items'   => [
            [
                'label' => 'Все',
                'route' => 'admin-channeling/index',
            ],
            [
                'label' => 'Автор',
                'route' => 'admin_channeling_author/index',
            ],
            [
                'label' => 'Силы',
                'route' => 'admin_channeling_power/index',
            ],
        ],
    ],
    [
        'label' => 'Магазин',
        'items' => [
            [
                'label' => 'Входящие платежи',
                'route' => 'admin-shop/money-income',
            ],
            [
                'label' => 'Каталог общий',
                'route' => 'admin-shop-catalog/index',
            ],
            [
                'label' => 'Наложный платеж',
                'route' => 'admin-nalojnii/index',
            ],
        ],
    ],
    [
        'label' => 'Агентство Сертификации',
        'items' => [
            [
                'label' => 'Записи об экспертизах',
                'route' => 'admin_sert_red/index',
            ],
        ],
    ],
    [
        'label' => 'Все',
        'items' => [
            [
                'label' => 'Генные ключи',
                'route' => 'admin_hd_gen_keys/index',
            ],
            [
                'label' => 'Практики',
                'route' => 'admin_praktice/index',
            ],
            [
                'label' => 'Штрафы',
                'route' => 'admin-penalty/index',
            ],
            [
                'label' => 'Новости',
                'route' => 'admin/news',
            ],
            [
                'label' => 'Статьи',
                'route' => 'admin_article/index',
            ],
            [
                'label' => 'События',
                'route' => 'admin_events/index',
            ],
            [
                'label' => 'Блог',
                'route' => 'admin_blog/index',
            ],
            [
                'label' => 'Пользователи',
                'route' => 'admin_users/index',
            ],
            [
                'label' => 'Помощь',
                'route' => 'admin_help/index',
            ],
            [
                'label' => 'Подписка',
                'route' => 'admin-subscribe/index',
            ],
            [
                'label' => 'Смета на Новую Землю',
                'route' => 'admin_smeta/index',
            ],
            [
                'label' => 'Услуги',
                'route' => 'admin_service/index',
            ],
            [
                'label' => 'Картинки',
                'route' => 'admin_pictures/index',
            ],
            [
                'label' => 'Объединения',
                'route' => 'admin_unions/index',
            ],
            [
                'label' => 'Видео',
                'route' => 'admin_video/index',
            ],
            [
                'label' => 'Видео',
                'route' => 'admin_video/index',
            ],
            [
                'label' => 'Штрафные квитанции',
                'route' => 'admin-penalty/index',
            ],
            [
                'label' => 'Бого-люди',
                'route' => 'admin-gods/index',
            ],
            [
                'label' => 'Платежные системы',
                'route' => 'admin-pay-system/index',
            ],
            [
                'label' => 'Анкеты',
                'route' => 'admin-anketa/index',
            ],
        ],
    ],
    [
        'label'   => 'Разработка',
        'route'   => 'admin-developer/index',
        'urlList' => [
            ['controller', 'admin-developer'],
        ],
    ],
];
function isSelected($item, $route)
{
    if (isset($item['route'])) {
        if ($item['route'] == $route) return true;
    }

    if (isset($item['items'])) {
        foreach ($item['items'] as $i) {
            if ($i['route'] == $route) {
                return true;
            }
        }
    }

    if (\yii\helpers\ArrayHelper::keyExists('urlList', $item)) {
        foreach ($item['urlList'] as $item) {
            $i = $item[0];
            $v = $item[1];
            switch ($i) {
                case 'startsWith':
                    if (\yii\helpers\StringHelper::startsWith($route, $v)) return true;
                    break;
                case 'controller':
                    $arr = explode('/', $route);
                    if ($arr[0] == $v) return true;
                    break;
            }
        }
    }

    return false;
}

?>

<?php
$rows = [];
foreach ($menu as $item) {
    $class = ['dropdown'];
    if (isSelected($item, Yii::$app->requestedRoute)) {
        $class[] = 'active';
    }
    if (\yii\helpers\ArrayHelper::keyExists('items', $item)) {
        $html = Html::tag(
            'li',
            Html::a(
                $item['label']
                .
                ' '
                .
                Html::tag('span', null, ['class' => 'caret'])
                ,
                '#'
                ,
                [
                    'class'         => "dropdown-toggle",
                    'data-toggle'   => "dropdown",
                    'role'          => "button",
                    'aria-expanded' => "false",
                ]
            )
            .
            Html::tag('ul',
                liMenu(\yii\helpers\ArrayHelper::map($item['items'], 'route', 'label'))
                , [
                    'class' => "dropdown-menu",
                    'role'  => "menu",
                ])
            ,
            ['class' => join(' ', $class)]);
    } else {
        $html = Html::tag(
            'li',
            Html::a($item['label'], [$item['route']]),
            ['class' => join(' ', $class)]
        );
    }

    $rows[] = $html;
}

?>
<?= join("\n", $rows) ?>
