<?php
use yii\helpers\Url;
use yii\helpers\Html;

function liMenu($route, $name = null, $options = [])
{
    if (is_array($route)) {
        $arr = [];
        foreach ($route as $route1 => $name1) {
            $arr[] = liMenu($route1, $name1);
        }

        return join("\n", $arr);
    }
    if (Yii::$app->requestedRoute == $route) {
        $options['class'] = 'active';
    }

    return \yii\helpers\Html::tag('li', \cs\helpers\Html::a($name, [$route]), $options);
}

$menu = [
    [
        'label' => 'Сферы жизни',
        'items' => [
            [
                'label' => 'Все',
                'route' => 'direction/index',
            ],
            [
                'label' => 'Язык',
                'route' => 'page/language',
            ],
            [
                'label' => 'Энергия',
                'route' => 'page/energy',
            ],
            [
                'label' => 'Время',
                'route' => 'page/time',
            ],
            [
                'label' => 'Пространство',
                'route' => 'page/house',
            ],
            [
                'label' => 'Обучение',
                'route' => 'page/study',
            ],
            [
                'label' => 'Прощающая система',
                'route' => 'page/forgive',
            ],
            [
                'label' => 'Деньги',
                'route' => 'page/money',
            ],
            [
                'label' => 'Здоровье',
                'route' => 'page/medical',
            ],
            [
                'label' => 'Питание',
                'route' => 'page/food',
            ],
            [
                'label' => 'Идеологические группы',
                'route' => 'page/idea',
            ],
            [
                'label' => 'ТВ',
                'route' => 'page/tv',
            ],
            [
                'label' => 'Одежда',
                'route' => 'page/clothes',
            ],
            [
                'label' => 'Художники',
                'route' => 'page/arts',
            ],
            [
                'label' => 'Музыка',
                'route' => 'page/music',
            ],
            [
                'label' => 'Семья',
                'route' => 'direction/semya',
            ],
        ],
    ],
    [
        'label' => 'Земля 5D',
        'items' => [
            [
                'label' => 'Введение',
                'route' => 'new_earth/index',
            ],
            [
                'label' => 'Новый Порядок Мира',
                'route' => 'new_earth/nmp',
            ],
            [
                'label' => 'Ноосферный Контакт',
                'route' => 'new_earth/noosfera',
            ],
            [
                'label' => 'Дизайн Человека',
                'route' => 'human_design/index',
            ],
            [
                'label' => 'Игра',
                'route' => 'new_earth/game',
            ],
            [
                'label' => 'Декларация Суверенитета',
                'route' => 'new_earth/dec',
            ],
            [
                'label' => 'Декларация Самоопределения',
                'route' => 'new_earth/declaration',
            ],
            [
                'label' => 'Вселенская Конституция',
                'route' => 'new_earth/const',
            ],
            [
                'label' => 'Манифест',
                'route' => 'new_earth/manifest',
            ],
            [
                'label' => 'Кодекс',
                'route' => 'new_earth/codex',
            ],
            [
                'label' => 'Флаг',
                'route' => 'new_earth/flag',
            ],
            [
                'label' => 'Соглашение о наблюдении',
                'route' => 'new_earth/conditions',
            ],
            [
                'label' => 'Резиденция',
                'route' => 'new_earth/residence',
            ],
            [
                'label' => 'Карта чакр',
                'route' => 'new_earth/chakri',
            ],
            [
                'label' => 'Гимн',
                'route' => 'new_earth/hymn',
            ],
            [
                'label' => 'История Человечества',
                'route' => 'new_earth/history',
            ],
            [
                'label' => 'Каноны',
                'route' => 'new_earth/kon',
            ],
            [
                'label' => 'Агентство Сертификации',
                'route' => 'new_earth/service_cert',
            ],
            [
                'label' => 'Агентство Воздаяния',
                'route' => 'new_earth/service_vozdayanie',
            ],
        ],
    ],
    [
        'label' => 'Послания',
        'route' => 'page/chenneling',
        'urlList' => [
            'startsWith' => '/chenneling',
        ],
    ],
    [
        'label'   => 'Новости',
        'route'   => 'news/index',
        'urlList' => [
            'controller' => 'news',
        ],
    ],
    [
        'label' => 'События',
        'route' => 'calendar/events',
    ],
    [
        'label'   => 'Блог',
        'route'   => 'blog/index',
        'urlList' => [
            'controller' => 'blog',
        ],
    ],
    [
        'label' => 'Магазин',
        'route' => 'shop/index',
        'urlList' => [
            'controller' => 'shop',
        ],
    ],
    [
        'label' => 'О нас',
        'route' => 'page/mission',
    ],
    [
        'label' => 'Контакты',
        'route' => 'site/contact',
    ],

];

function isSelected($item, $route)
{
    if (isset($item['route'])) {
        if ($item['route'] == $route) return true;
    }

    if (isset($item['items'])) {
        foreach ($item['items'] as $i) {
            if ($i['route'] == $route) {
                return true;
            }
        }
    }

    if (\yii\helpers\ArrayHelper::keyExists('urlList', $item)) {
        foreach ($item['urlList'] as $i => $v) {
            switch ($i) {
                case 'startsWith':
                    if (\yii\helpers\StringHelper::startsWith($_SERVER['REQUEST_URI'], $v)) return true;
                    break;
                case 'controller':
                    $arr = explode('/', $route);
                    if ($arr[0] == $v) return true;
                    break;
            }
        }
    }

    return false;
}

?>

<?php
$rows = [];
foreach ($menu as $item) {
    $class = ['dropdown'];
    if (isSelected($item, Yii::$app->requestedRoute)) {
        $class[] = 'active';
    }
    if (\yii\helpers\ArrayHelper::keyExists('items', $item)) {
        $html = Html::tag(
            'li',
            Html::a(
                $item['label']
                .
                ' '
                .
                Html::tag('span', null, ['class' => 'caret'])
                ,
                '#'
                ,
                [
                    'class'         => "dropdown-toggle",
                    'data-toggle'   => "dropdown",
                    'role'          => "button",
                    'aria-expanded' => "false",
                ]
            )
            .
            Html::tag('ul',
                liMenu(\yii\helpers\ArrayHelper::map($item['items'], 'route', 'label'))
                , [
                    'class' => "dropdown-menu",
                    'role'  => "menu",
                ])
            ,
            ['class' => join(' ', $class)]);
    } else {
        $html = Html::tag(
            'li',
            Html::a($item['label'], [$item['route']]),
            ['class' => join(' ', $class)]
        );
    }

    $rows[] = $html;
}


?>
<?= join("\n", $rows) ?>
