<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;
use app\models\UnionCategory;
use yii\db\Query;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \app\models\God */

$this->title = '30 августа в 20:00 МСК вебинар проекта РАЙ от Камаллая Хефорс «Инвестиции нового времени»';

?>

<div class="parallax-window" data-parallax="scroll" data-image-src="/images/site/index/rai5.jpg">


    <h1 class="text-center" style="padding-top: 150px; font-size: 400%; color: white;text-shadow: 2px 2px 7px hsla(37, 11%, 14%, 0.5);">30 августа в 20:00 МСК</h1>
    <h3 class="text-center" style="padding-top: 35px; font-size: 300%; color: white;text-shadow: 2px 2px 7px hsla(37, 11%, 14%, 0.5);">вебинар проекта РАЙ от Камаллая Хефорс</h3>
    <h3 class="text-center" style="padding-top: 50px; font-size: 400%; color: white;text-shadow: 2px 2px 7px hsla(37, 11%, 14%, 0.5);">Инвестиции нового времени</h3>

</div>
<style>
    .parallax-window {
        min-height: 650px;
        background: transparent;
    }
</style>
<?php $this->registerJsFile('/js/parallax.js-1.5.0/parallax.js', ['depends' => ['yii\web\JqueryAsset']]); ?>

<div class="container">

    <div class="col-lg-8 col-lg-offset-2">

        <p style="color: red; font-size: 200%;" class="text-center lead">❤❤❤❤❤</p>
        <p class="lead">Дорогая Небесная Семья Света! О золотые Ангелы! Приветствуем вас сиянием небесной чудной новости любимые и прекрасные!</p>

        <p class="lead">В 2019 году произойдет эволюция в истории инвестиций и строительства! Уже сегодня строится фундамент инвестиций нового времени, который даст мощный толчок в развитие нашего будущего.</p>

        <p class="lead">● Какие тренды неизбежно наступают и о которых молчат самые крупные инвесторы?<br>
        ● Что станет основой прибыльности в недвижимости уже с 2019г, о чем еще не говорит ни один тренер по недвижимости?<br>
        ● Почему в 2020 году финансовая мировая система перевернется? И почему для большинства это будет кризис, а для тех, кто сейчас займется недвижимостью Нового Времени наступит Золотой Век?<br>
        ● Какие Тайны хранят истинные Инвестора-Миллионеры Нового Времени?<br>
        ● Как мы можем повлиять на Изменения Строительства Нового времени?<br>
        ● Что значит “Новое Время” и какие тут правила?<br>
        ● Как Карма связана со строительством недвижимости Нового Времени?<br>
        ● Почему правительство РФ поддерживает проекты Нового Времени, выбирая их из сотен тысяч других?<br>
        ● Как слово “пенсия” перестанет приносить боль, а начнет приносить радость и достойный уровень жизни.<br>
            ● Как увеличить свои инвестиции в 3 раза за 2 года?</p>

        <p class="lead">На все эти вопросы и многие другие в этот четверг 30 августа в 20:00 Мск ответит основатель проекта “Город света” Камаллая Хефорс.</p>

        <p class="lead">Ссылка <a href="https://www.youtube.com/watch?v=hxGGRwxzvGk" target="_blank">https://www.youtube.com/watch?v=hxGGRwxzvGk</a>. Нажмите на кнопку "Напомнить о трансляции" и Google вам напомнит о ней.</p>
        <h3 class="page-header text-center">Вебинар ведет</h3>
        <p class="text-center"><img src="/images/rai/vebinar30aug/file.png" style="border-radius: 20px; width: 200px;"></p>
        <p class="text-center lead">Камаллая Хефорс</p>
        <ul>
            <li>"Основатель" и идейный вдохновитель проектов РАЙ и Города Света. Тот кто прокладывает дорогу, соединяет и призывает.</li>
            <li>Президент Фонда развития инновационных проектов "Планета света".</li>
            <li>Директор АО "Золотая платформа"</li>
            <li>Вице Президент компании “Мир и Недвижимость”, г. Москва</li>
            <li>Управляющий агентства недвижимости “Бизнес Недвижимость”, г.Москва</li>
            <li>Кандидат в депутаты РФ</li>
            <li>Помощник депутата ГосДумы РФ</li>
            <li>Генеральный директор компании “КрымПромЛюкс” застройщик рынков промышленных товаров.</li>
            <li>Цель жизни: общество нового времени, которое вспомнит что все есть Любовь, а все остальное лишь инструмент для ее размножения. Единство во всем без разделения на религии и расовую принадлежность. Если мы сможем прийти к этому в количестве и объединим хотя бы 1000 человек, то этого будет достаточно чтобы свершить глобальный квантовый скачок в эпоху нового времени, новых частот и новой самой совершенной истории на этой планете. Любовь размножится по нейросети нашей свами связи. И речь не о той любви, которую знали люди, эгоистической с красивыми словами, речь идет о подлинной Любви, которую еще предстоит постичь в Единстве.</li>
        </ul>

        <p class="lead">Смотреть трансляцию вы можете как здесь так и на самом YouTube канале.</p>

        <p class="lead"><iframe width="100%" height="315" src="https://www.youtube.com/embed/hxGGRwxzvGk" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen=""></iframe></p>

        <p class="lead">Чтобы мы вам напомнили по почте подпишитесь пожалуйста по форме ниже.</p>
    </div>

        <div class="row">
            <div class="col-lg-3">
            </div>
            <div class="col-lg-6">
                <?php if (!is_null($id = Yii::$app->session->getFlash('anketa'))) : ?>

                    <div class="alert alert-success">
                        Успешно добавлено. Перейдите на почту и подтвердите пожалуйста свою почту.
                    </div>

                <?php else: ?>


                    <?php $form = ActiveForm::begin([
                        'id'        => 'contact-form',
                        'options'   => ['enctype' => 'multipart/form-data']
                    ]); ?>
                    <?= $form->field($model, 'name')->label('Имя') ?>
                    <?= $form->field($model, 'email')->label('Почта') ?>
                    <?= $form->field($model, 'phone')->label('Телефон') ?>

                    <hr>
                    <div class="form-group">
                        <?= Html::submitButton('Подписаться', [
                            'class' => 'btn btn-success',
                            'name'  => 'contact-button',
                            'style' => 'width:100%',
                        ]) ?>
                    </div>
                    <?php ActiveForm::end(); ?>

                <?php endif; ?>
            </div>
        </div>

    <div class="col-lg-8 col-lg-offset-2" style="margin-bottom: 50px;">

        <p class="lead">Знакомьтесь с нашим проектом более подробно на сайте <a href="http://gorod-sveta.com/" target="_blank">http://gorod-sveta.com/</a>.</p>

        <p class="lead">А также напоминаем вам, что вы можете подписаться на группу в Vkontakte: <a href="https://vk.com/goldencitylight" target="_blank">https://vk.com/goldencitylight</a>.</p>

        <p class="lead">на группу в Facebook <a href="https://www.facebook.com/paradiseuniverse999/" target="_blank">https://www.facebook.com/paradiseuniverse999/</a>.</p>

        <p class="lead">на наш видеоканал в YouTube <a href="https://www.youtube.com/channel/UCQHo_sBzqgmr7ZWrscV8djg" target="_blank">https://www.youtube.com/channel/UCQHo_sBzqgmr7ZWrscV8djg</a>.</p>

        <p class="lead">Подпишитесь на нашу почтовую рассылку проекта, для этого зайдите на сайт <a href="http://gorod-sveta.com/" target="_blank">http://gorod-sveta.com/</a> и в конце сайта заполните форму на подписку.  Или подпишитесь на странице <a href="https://www.galaxysss.com/rai/subscribe">https://www.galaxysss.com/rai/subscribe</a>.</p>
        <p class="lead">Если вы хотите быть в нашей команде то заполните пожалуйста анкету <a href="https://www.galaxysss.com/anketa/new">https://www.galaxysss.com/anketa/new</a> и мы с вами свяжемся.</p>

        <p class="lead">Если вы хотите быть спонсором или инвестором то звоните по номеру +7 (499) 992-25-82 или пишите на почту <a href="mailto:kamallaya999@gmail.com">kamallaya999@gmail.com</a>.</p>

        <p class="lead">Если вы хотите стать нашим информационным партнером или вы готовы сотрудничать то звоните по номеру +7 (499) 992-25-82 или пишите на почту <a href="mailto:kamallaya999@gmail.com">kamallaya999@gmail.com</a>.</p>


        <p class="lead">С Любовью и Светом команда проекта РАЙ!</p>

        <p style="color: red; font-size: 200%;" class="text-center">❤❤❤❤❤</p>


        <hr>
        <center>
            <?= $this->render('../blocks/share', [
                'image'       => \yii\helpers\Url::to('/images/rai/vebinar30aug/2018-08-27_23-54-39.png', true),
                'url'         => \yii\helpers\Url::current([], true),
                'title'       => '30 августа в 20:00 Мск вебинар проекта РАЙ от Камаллая Хефорс «Инвестиции нового времени»',
                'description' => 'В 2019 году произойдет эволюция в истории инвестиций и строительства! Уже сегодня строится фундамент инвестиций нового времени, который даст мощный толчок в развитие нашего будущего.',
            ]) ?>
        </center>
    </div>


</div>
<div class="parallax-window" style="min-height: 450px;" data-parallax="scroll" data-image-src="/images/site/index/bg123.png">
    <h1 class="text-center" style="padding-top: 150px; font-size: 200%; color: white;text-shadow: 2px 2px 7px hsla(37, 11%, 14%, 0.5);">В 2019 году произойдет эволюция<br>в истории инвестиций и строительства!<br>Уже сегодня строится фундамент инвестиций нового времени,<br>который даст мощный толчок в развитие нашего будущего.</h1>
</div>
<div class="container">
</div>
