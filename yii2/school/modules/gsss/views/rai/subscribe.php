<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;
use app\models\UnionCategory;
use yii\db\Query;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \app\models\Form\rai\Subscribe */

$this->title = 'Подписка на новости проекта РАЙ';

?>
<div class="container">
    <div class="page-header">
        <h1 class="text-center"><?= Html::encode($this->title) ?></h1>
    </div>
    <p><img src="/images/anketa/new/211.jpg"  width="100%" class="thumbnail"></p>




        <div class="row">
            <div class="col-lg-3">
            </div>
            <div class="col-lg-6" style="margin-bottom: 200px;">
                <?php if (!is_null($id = Yii::$app->session->getFlash('anketa'))) : ?>

                    <div class="alert alert-success">
                        Успешно добавлено. Перейдите на почту и подтвердите пожалуйста свою почту.
                    </div>

                <?php else: ?>


                    <?php $form = ActiveForm::begin([
                        'id' => 'contact-form',
                        'options' => ['enctype' => 'multipart/form-data']
                    ]); ?>
                    <?= $form->field($model, 'name')->label('Имя') ?>
                    <?= $form->field($model, 'email')->label('Почта') ?>
                    <?= $form->field($model, 'phone')->label('Телефон') ?>

                    <hr>
                    <div class="form-group">
                        <?= Html::submitButton('Подписаться', [
                            'class' => 'btn btn-success',
                            'name'  => 'contact-button',
                            'style' => 'width:100%',
                        ]) ?>
                    </div>
                    <?php ActiveForm::end(); ?>

                <?php endif; ?>
            </div>
        </div>

</div>
