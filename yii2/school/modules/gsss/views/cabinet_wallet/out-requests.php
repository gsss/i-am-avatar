<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;
use app\models\UnionCategory;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model cs\base\BaseForm */

$this->title = 'Снять деньги';
?>
<div class="container">
    <div class="col-lg-12">
        <h1 class="page-header"><?= Html::encode($this->title) ?></h1>
        <?= \cs\Widget\BreadCrumbs\BreadCrumbs::widget([
            'items' => [
                $this->title
            ],
            'home'  => [
                'name' => 'я',
                'url'  => \yii\helpers\Url::to(['site/user', 'id' => Yii::$app->user->id])
            ]
        ]) ?>
        <hr>


        <div class="col-lg-8">
            <?php if (Yii::$app->session->hasFlash('contactFormSubmitted')) {?>
                <div class="alert alert-success">
                    Заявка на вывод успешно создана.
                </div>
            <?php } else { ?>
                <?php $form = ActiveForm::begin([
                    'enableAjaxValidation' => true,
                ]) ?>
                <?php
                /** @var \app\models\User $me */
                $me = Yii::$app->user->identity;
                $walletA = $me->getWallet('A');
                $walletB = $me->getWallet('B');
                ?>
                <?= $form->field($model, 'walletId')->radioList([
                    $walletA->id => 'A' . ' ' . Yii::$app->formatter->asDecimal($walletA->value),
                    $walletB->id => 'B' . ' ' . Yii::$app->formatter->asDecimal($walletB->value),
                ], ['unselect' => $walletA->id])?>
                <?= $form->field($model, 'value')?>
                <?= $form->field($model, 'destination')->textarea(['rows' => 5])?>
                <?= Html::submitButton('Перевести', [
                    'class' => 'btn btn-success',
                    'style' => 'width:100%;',
                ]) ?>
                <?php ActiveForm::end() ?>
            <?php }?>
        </div>
        <div class="col-lg-4">
            <?= $this->render('../cabinet/profile_menu/profile_menu') ?>
        </div>

    </div>
</div>
