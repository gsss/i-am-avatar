<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;
use app\models\UnionCategory;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model cs\base\BaseForm */

$this->title = 'Положить деньги';
?>
<div class="container">
    <div class="col-lg-12">
        <h1 class="page-header"><?= Html::encode($this->title) ?></h1>
        <?= \cs\Widget\BreadCrumbs\BreadCrumbs::widget([
            'items' => [
                $this->title
            ],
            'home'  => [
                'name' => 'я',
                'url'  => \yii\helpers\Url::to(['site/user', 'id' => Yii::$app->user->id])
            ]
        ]) ?>
        <hr>

        <div class="col-lg-8">
            <div id="step1">
                <?php
                /** @var \app\models\User $user */
                $user = Yii::$app->user->identity;
                /** @var \app\models\Piramida\Wallet $wallet */
                $wallet = $user->getWallet('A');
                ?>
                <p>Сколько:</p>
                <div class="form-group">
                    <input class="form-control" value="" id="inputValue"/>
                    <p class="hide errorComment error"></p>
                    <p class="helpParagraph"></p>
                </div>
                <?php
                $this->registerJs(<<<JS
                /**
                *
                * @param val
                * @param separator string
                * @param lessOne int кол-во символов после точки, если 0 то точка не выводится, добавляются задние нули работает пока только для 0..2
                * @returns {string}
                */
                function formatAsDecimal (val, separator, lessOne)
                {
                    if (typeof separator == 'undefined') separator = ',';
                    if (typeof lessOne == 'undefined') lessOne = 2;

                    var pStr = '';
                    var original = val;
                    val = parseInt(val);
                    if (val >= 1000000) {
                        var t1 = parseInt(val/1000000);
                        var ost1 = (val - t1 * 1000000);
                        if (ost1 >= 1000) {
                            var t2 = parseInt(ost1/1000);
                            var ost2 = (ost1 - t2 * 1000);
                            var ostStr2 = ost2;
                            if  (t2 == 0) {
                                t2 = '000';
                            } else {
                                if (t2 < 10) {
                                    t2 = '00' + t2;
                                } else if (t2 < 100) {
                                    t2 = '0' + t2;
                                }
                            }
                        } else {
                            t2 = '000';
                            ost2 = ost1;
                        }
                        if  (ost2 == 0) {
                            ostStr2 = '000';
                        } else {
                            if (ost2 < 10) {
                                ostStr2 = '00' + ost2;
                            } else if (ost2 < 100) {
                                ostStr2 = '0' + ost2;
                            }
                        }
                        pStr = t1 + separator + t2 + separator + ostStr2;
                    } else {
                        if (val >= 1000) {
                            var t = parseInt(val/1000);
                            var ost = (val - t * 1000);
                            var ostStr = ost;
                            if  (ost == 0) {
                                ostStr = '000';
                            } else {
                                if (ost < 10) {
                                    ostStr = '00' + ost;
                                } else if (ost < 100) {
                                    ostStr = '0' + ost;
                                }
                            }
                            pStr = t + separator + ostStr;
                        } else {
                            pStr = val;
                        }
                    }

                    var oStr = '';
                    if (lessOne > 0) {
                        oStr = '.';
                        var d = original - parseInt(original);
                        d = d * Math.pow(10, lessOne);
                        d = parseInt(d);
                        if (d == 0) {
                            for (var i = 0; i < lessOne; i++) {
                                oStr = oStr + '0';
                            }
                        } else {
                            if (lessOne == 1) {
                                oStr = d;
                            } else {
                                if (d < 10) {
                                    oStr = oStr + '0' + d;
                                } else {
                                    oStr = oStr + d;
                                }
                            }
                        }
                    }

                    return pStr + oStr;
                }

                /**
                * Вычисляет сумму для поплаты счета пополнения кошелька
                *
                * @param value
                * @returns {number}
                */
                function getPaySum(value)
                {
                    return Math.round(value / (1 - (2 / 100)));
                }

                $('#inputValue').on('input', function() {
                    var i = $(this);
                    if (i.val() != '') {
                        $('.helpParagraph').html('Сумма для оплаты: ' +
                            formatAsDecimal(
                                getPaySum(
                                    $(this).val()
                                )
                            )
                        );
                    } else {
                        $('.helpParagraph').html('Сумма для оплаты: ' + 0);
                    }
                });
                $('.buttonAdd').click(function() {
                    if ($('#inputValue').val() == '') {
                        $('#inputValue').parent().addClass('has-error');
                        $('.errorComment').removeClass('hide').html('Это поле обязательно надо заполнить!').show();
                        return;
                    }
                    ajaxJson({
                        url: '/cabinet/profile/wallet/inAjax',
                        data: {
                            value: $('#inputValue').val()
                        },
                        success: function(ret) {
                            $('#formPayLabel').val('inRequest.' + ret);
                            $('#formPay input[name="short-dest"]').val('Счет #' + ret);
                            $('#formPay input[name="targets"]').val('Счет #' + ret);
                            $('#productPriceForm').val(
                            getPaySum(
                                    $('#inputValue').val()
                                )
                            );
                            $('#formPay').submit();
                        }
                    });
                });
                $('#inputValue').on('focus', function() {
                    $('#inputValue').parent().removeClass('has-error');
                    $('.errorComment').hide();
                });
JS
                );
                ?>
                <div class="col-lg-12 row" style="margin-top: 10px; margin-bottom: 20px;">
                    <div class="btn-group" role="group" aria-label="...">
                        <?php
                        $this->registerJs(<<<JS
    $('.paymentType').click(function () {
        $('#formPay input[name="paymentType"]').attr('value', $(this).data('value'));
        if (!$(this).hasClass('active')) {
            $('.paymentType').removeClass('active');
            $(this).addClass('active');
        }
    });
JS
                        );
                        ?>

                        <button type="button" class="btn btn-default paymentType" data-value="PC">
                            <i class="glyphicon glyphicon-yen" style="margin-right: 4px;"></i> Яндекс.Деньгами
                        </button>
                        <button type="button" class="btn btn-default paymentType active" data-value="AC">
                            <i class="glyphicon glyphicon-credit-card" style="margin-right: 4px;"></i> Банковской картой
                        </button>
                        <button type="button" class="btn btn-default paymentType gsssTooltip" data-value="DI" title="Реквизиты будут высланы ответом после оформления заказа">
                            <i class="glyphicon glyphicon-question-sign" style="margin-right: 4px;"></i> Другие способы
                        </button>
                    </div>
                </div>

                <button class="btn btn-success buttonAdd">Пополнить</button>
            </div>
            <div id="step2" style="display: none;">
                <h2 class="text-center">Идет перенаправление на оплату...</h2>

                <form method="POST" action="https://money.yandex.ru/quickpay/confirm.xml" name="form2" id="formPay">
                    <input type="hidden" name="receiver" value="410011473018906">
                    <input type="hidden" name="formcomment" value="<?= Yii::$app->user->identity->getName2() ?>">
                    <input type="hidden" name="short-dest" value="Счет #<?= $wallet->id ?>">
                    <input type="hidden" name="label" value="inRequest." id="formPayLabel">
                    <input type="hidden" name="quickpay-form" value="donate">
                    <input type="hidden" name="targets" value="Пополнение кошелька #<?= $wallet->id ?>">
                    <input type="hidden" name="sum" value="" data-type="number"
                           id="productPriceForm">
                    <input type="hidden" name="comment" value="">
                    <input type="hidden" name="need-fio" value="false">
                    <input type="hidden" name="need-email" value="false">
                    <input type="hidden" name="need-phone" value="false">
                    <input type="hidden" name="need-address" value="false">
                    <input type="hidden" name="paymentType" value="AC" id="paymentType">

                    <div class="row">
                        <div class="col-lg-12" style="margin-top: 30px;">
                            <div class="btn-group" role="group" aria-label="...">
                                <input type="button" value="Перейти к оплате"
                                       class="btn btn-success btn-lg buttonSubmit" style="width: 400px; border: 2px solid white; border-radius: 20px;">
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <div class="col-lg-4">
            <?= $this->render('../cabinet/profile_menu/profile_menu') ?>
        </div>
    </div>

</div>
