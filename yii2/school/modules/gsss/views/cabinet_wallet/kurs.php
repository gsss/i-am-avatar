<?php

/**
 * @var $this \yii\web\View
 */

use yii\widgets\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;

$this->title = 'Курс Биткойна';
?>

<style>
    a.asc:after {
        content: ' ↓';
        display: inline;
    }

    a.desc:after {
        content: ' ↑';
        display: inline;
    }
    .tableStat td {
        font-family: "Courier New", Courier, monospace;
    }
</style>

<div class="container">
    <h1 class="page-header text-center">Курс Биткойна</h1>

    <?php
    $this->registerJs(<<<JS

    /**
    *
    * @param val
    * @param separator
    * @param int lessOne кол-во символов после точки, если 0 то точка не выводится, добавляются задние нули работает пока только для 0..2
    * @returns {string}
    */
    function formatAsDecimal(val, separator = ',', lessOne = 0)
    {
        var pStr = '';
        var original = val;
        val = parseInt(val);
        if (val >= 1000) {
            var t = parseInt(val/1000);
            var ost = (val - t * 1000);
            var ostStr = ost;
            if  (ost == 0) {
                ostStr = '000';
            } else {
                if (ost < 10) {
                    ostStr = '00' + ost;
                } else if (ost < 100) {
                    ostStr = '0' + ost;
                }
            }
            pStr = t + separator + ostStr;
        } else {
            pStr = val;
        }
        var oStr = '';
        if (lessOne > 0) {
            oStr = '.';
            var d = original - parseInt(original);
            d = d * Math.pow(10, lessOne);
            d = parseInt(d);
            if (d == 0) {
                for (var i=0;i<lessOne;i++){
                    oStr = oStr + '0';
                }
            } else {
                if (lessOne == 1) {
                    oStr = d;
                } else {
                    if (d < 10) {
                        oStr = oStr + '0' + d;
                    } else {
                        oStr = oStr + d;
                    }
                }
            }
        }

        return pStr + oStr;
    }
JS
    );

    ?>


    <?php
    $period = 24 * 2;

    $rows = \common\models\piramida\BitCoinKurs::find()
        ->select([
        'rub',
        'time',
    ])
        ->where(['>', 'time', time() - (60*60*24*30)])
        ->all()
    ;
    $rowsJson = \yii\helpers\Json::encode($rows);
    $this->registerJs(<<<JS
        Highcharts.setOptions({
            global: {
                timezoneOffset: -3 * 60
            }
        });
        var rows = {$rowsJson};
        var newRows = [];
        for(i = 0; i < rows.length; i++)
        {
            var item = rows[i];
            newRows.push({
                x: new Date(item.time * 1000),
                y: item.rub
            });
        }
JS
    );
    ?>
    <?= HighCharts::widget([
        'chartOptions' => [
            'chart' => [
                'zoomType' => 'x',
                'type'     => 'spline',
            ],
            'title' => [
                'text' => 'График',
            ],
            'subtitle' => [
                'text' => 'Выделите область для изменения масштаба',
            ],
            'xAxis' => [
                'type' => 'datetime',
            ],
            'yAxis' => [
                [
                    'title' => [
                        'text' => 'Количество',
                    ],
                ]
            ],
            'legend' => [
                'enabled' => true
            ],
            'tooltip' => [
                'crosshairs' => true,
                'shared' => true,
            ],
            'plotOptions' => [
                'series' => [
                    'turboThreshold' => 0,
                ],
            ],
            'series' => [
                [
                    'type' => 'spline',
                    'name' => 'RUB',
                    'data' => new \yii\web\JsExpression('newRows'),
                ],
            ],
        ],
    ]);

    ?>

</div>