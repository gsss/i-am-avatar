<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;
use app\models\UnionCategory;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model cs\base\BaseForm */

$this->title = 'Кошелек';
Yii::setAlias('@Endroid/QrCode', '@vendor/endroid/QrCode/src');

?>
<div class="container">
<div class="col-lg-12">
<h1 class="page-header"><?= Html::encode($this->title) ?></h1>
<?= \cs\Widget\BreadCrumbs\BreadCrumbs::widget([
    'items' => [
        $this->title
    ],
    'home'  => [
        'name' => 'я',
        'url'  => \yii\helpers\Url::to(['site/user', 'id' => Yii::$app->user->id])
    ]
]) ?>
<hr>
<div class="col-lg-8">
<div>

<!-- Nav tabs -->
<ul class="nav nav-tabs" role="tablist">
    <li role="presentation" class="active"><a href="#tab-rub" aria-controls="home" role="tab" data-toggle="tab"><i
                class="fa fa-rub"></i> Рубли</a></li>
    <li role="presentation"><a href="#tab-bit-coin" aria-controls="profile" role="tab" data-toggle="tab"><i
                class="fa fa-btc"></i> BitCoin</a></li>
</ul>

<!-- Tab panes -->
<div class="tab-content">
<div role="tabpanel" class="tab-pane fade in active" id="tab-rub" style="padding: 20px 0px 20px 0px;">
    <?php
    /** @var \app\models\User $user */
    $user = Yii::$app->user->identity;
    /** @var \app\models\Piramida\Wallet $wallet */
    $walletA = $user->getWallet('A');
    $walletB = $user->getWallet('B');
    $sort = new \yii\data\Sort([
        'attributes'   => [
            'datetime' => [
                'label' => 'Время',
            ],
        ],
        'defaultOrder' => [
            'datetime' => SORT_DESC,
        ],
    ]);
    ?>
    <p>В вашем кошельке:</p>

    <div class="row">
        <div class="col-sm-6">
            <p class="alert alert-success" style="font-weight: bold;font-size: 300%; font-family: Impact;">
                <?= Yii::$app->formatter->asDecimal($walletA->value + $walletB->value) ?> р.
            </p>
        </div>
        <div class="col-sm-3">
            <p class="alert alert-info" style="font-size: 200%; font-family: Impact;">
                <?= Yii::$app->formatter->asDecimal($walletA->value) ?> р.
            </p>
        </div>
        <div class="col-sm-3">
            <p class="alert alert-info" style="font-size: 200%; font-family: Impact;">
                <?= Yii::$app->formatter->asDecimal($walletB->value) ?> р.
            </p>
        </div>
    </div>
    <p>
        <a href="/cabinet/profile/wallet/in" class="btn btn-info btn-lg">
            <i class="glyphicon glyphicon-import"></i>
            Пополнить ...
        </a>
        <a href="/cabinet/profile/wallet/out" class="btn btn-info btn-lg">
            <i class="glyphicon glyphicon-export"></i>
            Снять ...
        </a>
        <a href="/cabinet/profile/wallet/move" class="btn btn-info btn-lg">
            <i class="glyphicon glyphicon-transfer"></i>
            Перевести ...
        </a>
    </p>

    <h3 class="page-header">Транзакции</h3>
    <style>
        a.asc:after {
            content: ' ↓';
            display: inline;
        }

        a.desc:after {
            content: ' ↑';
            display: inline;
        }
        #tableTransaction .code {
            font-family: "Courier New", Courier, monospace;
            text-align: right;
        }
    </style>
    <?php \yii\widgets\Pjax::begin(); ?>
    <?php
    $path = Yii::$app->assetManager->getBundle('\app\assets\LayoutMenu\Asset')->baseUrl . '/images/ajax-loader.gif';
    $this->registerJs(<<<JS
    $('.gsssTooltip').tooltip();
JS
    );
    $query = \app\models\Piramida\Operation::find()
        ->select([
            'nw_operations.*',
            'nw_transactions.operation',
            'nw_transactions.comment',
            'if(nw_operations.wallet_id=' . $walletA->id . ',"A","B") as wallet_name'
        ])
        ->where(['nw_operations.wallet_id' => [$walletA->id, $walletB->id]])
        ->innerJoin('nw_transactions', 'nw_transactions.id = nw_operations.transaction_id')
        ->asArray();
    ?>
    <?= \yii\grid\GridView::widget([
        'dataProvider' => new \yii\data\ActiveDataProvider([
            'query'      => $query,
            'sort'       => $sort,
            'pagination' => [
                'pageSize' => 50,
            ],
        ]),
        'tableOptions' => [
            'class' => 'table table-striped table-hover',
        ],
        'rowOptions'   => function ($item) {
            $class = ['rowTable'];
            if (\yii\helpers\ArrayHelper::getValue($item, 'type', 0) == 1) $class[] = 'success';

            return [
                'data'  => ['id' => $item['id']],
                'role'  => 'button',
                'class' => join(' ', $class),
            ];
        },
        'columns'      => [
            [
                'header'  => 'Операция',
                'content' => function ($item) {
                    $class = 'glyphicon glyphicon-question-sign';
                    switch ($item['operation']) {
                        case 0:
                            $class = 'glyphicon glyphicon-transfer';
                            break;
                        case 1:
                            $class = 'glyphicon glyphicon-import';
                            break;
                        case -1:
                            $class = 'glyphicon glyphicon-export';
                            break;
                        case 2:
                            $class = 'glyphicon glyphicon-heart';
                            break;
                        case 3:
                            $class = 'fa fa-credit-card';
                            break;
                    }

                    return Html::tag('span', null, ['class' => $class]);
                }
            ],
            [
                'header'  => $sort->link('datetime'),
                'content' => function ($item) {
                    $v = (int)\yii\helpers\ArrayHelper::getValue($item, 'datetime', 0);

                    return Html::tag('abbr', \cs\services\DatePeriod::back($v, ['isShort' => true]), ['class' => 'gsssTooltip', 'title' => Yii::$app->formatter->asDatetime($v)]);
                }
            ],
            [
                'header'         => 'Сумма',
                'headerOptions'  => [
                    'style' => 'text-align:right;'
                ],
                'contentOptions' => [
                    'style'  => 'text-align:right;',
                    'nowrap' => 'nowrap',
                ],
                'content'        => function ($item) {
                    $type = \yii\helpers\ArrayHelper::getValue($item, 'type', 0);
                    $type = ($type > 0) ? '+' : '-';
                    $v = \yii\helpers\ArrayHelper::getValue($item, 'summa', 0);
                    $string = $type . ' ' . Yii::$app->formatter->asDecimal($v);
                    if ($type == '+') {
                        $string = Html::tag('abbr', $string, []);
                    }

                    return $string;
                }
            ],
            'comment:text:Комментарий',
            'wallet_name:text:Кошелек',
        ]
    ]) ?>
    <?php \yii\widgets\Pjax::end(); ?>
</div>

<div class="modal fade" id="modalInfo" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                        aria-hidden="true">&times;</span></button>
                <h4 class="modal-title text-center" id="myModalLabel">Информация</h4>
            </div>
            <div class="modal-body">

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" style="width: 100%;" data-dismiss="modal">Закрыть</button>
            </div>
        </div>
    </div>
</div>
<div class="modal fade bs-example-modal-lg" id="modalTransactionInfo" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                        aria-hidden="true">&times;</span></button>
                <h4 class="modal-title text-center" id="myModalLabel">Транзакция</h4>
            </div>
            <div class="modal-body">
                <table class="table table-hover table-striped">
                    <tbody>
                    <tr>
                        <td>Хеш</td>
                        <td><code  class="js-hash"></code></td>
                    </tr>
                        <tr>
                            <td>Время</td>
                            <td class="js-time"></td>
                        </tr>
                        <tr>
                            <td>Подтверждений</td>
                            <td class="js-confirmations"></td>
                        </tr>
                        <tr>
                            <td>Блок №</td>
                            <td class="js-block_height"></td>
                        </tr>
                        <tr>
                            <td>Блок Хеш</td>
                            <td><code class="js-block_hash"></code></td>
                        </tr>
                        <tr>
                            <td>Всего входов</td>
                            <td class="js-total_input_value"></td>
                        </tr>
                        <tr>
                            <td>Всего выходов</td>
                            <td class="js-total_output_value"></td>
                        </tr>
                    <tr>
                            <td>Комиссия</td>
                            <td class="js-total_fee"></td>
                        </tr>
                    </tbody>
                </table>
        <?php
        $this->registerJs(<<<JS
$('.js-download-bill').click(function() {
    window.location = '/cabinet-wallet-btc/download-bill?hash=' + $(this).data('hash');
    });
JS
);
        ?>
                <button class="btn btn-info center-block js-download-bill" href=""><i class="glyphicon glyphicon-download-alt"></i> Скачать чек PDF</button>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" style="width: 100%;" data-dismiss="modal">Закрыть</button>
            </div>
        </div>
    </div>
</div>

<div role="tabpanel" class="tab-pane fade" id="tab-bit-coin">
<div class="row" style="margin: 20px 0px 20px 0px">


<div>

<?php
$this->registerJs(<<<JS
$('.gsssTooltip').tooltip();

/**
*
* @param val
* @param separator
* @param int lessOne кол-во символов после точки, если 0 то точка не выводится, добавляются задние нули работает пока только для 0..2
* @returns {string}
*/
function formatAsDecimal (val, separator, lessOne)
{
    if (typeof separator == 'undefined') separator = ',';
    if (typeof lessOne == 'undefined') lessOne = 0;
    var pStr = '';
    var original = val;
    val = parseInt(val);
    if (val >= 1000) {
        var t = parseInt(val/1000);
        var ost = (val - t * 1000);
        var ostStr = ost;
        if  (ost == 0) {
            ostStr = '000';
        } else {
            if (ost < 10) {
                ostStr = '00' + ost;
            } else if (ost < 100) {
                ostStr = '0' + ost;
            }
        }
        pStr = t + separator + ostStr;
    } else {
        pStr = val;
    }
    var oStr = '';
    if (lessOne > 0) {
        oStr = '.';
        var d = original - parseInt(original);
        d = d * Math.pow(10, lessOne);
        d = parseInt(d);
        if (d == 0) {
            console.log(d);

            for (var i=0;i<lessOne;i++){
                oStr = oStr + '0';
            }
        } else {
            if (lessOne == 1) {
                oStr = d;
            } else {
                if (d < 10) {
                    oStr = oStr + '0' + d;
                } else {
                    oStr = oStr + d;
                }
            }
        }
    }

    return pStr + oStr;
}

var functionPageClick = function(e) {
    var o = $(this);
    var href1 = o.data('href');
    ajaxJson({
        url: href1,
        success: function(ret) {
            var o = $(ret.html);
            o.find('.js-buttonTransactionInfo').click(function(e) {
                var data = $(this).data('html');
                $('.js-hash').html(data.hash);
                $('.js-time').html(data.time);
                $('.js-confirmations').html(data.confirmations);
                $('.js-block_height').html(data.block_height);
                $('.js-block_hash').html(data.block_hash);
                $('.js-total_input_value').html(data.total_input_value);
                $('.js-total_output_value').html(data.total_output_value);
                $('.js-total_fee').html(data.total_fee);
                $('.js-download-bill').data('hash', data.hash);
                $('#modalTransactionInfo').modal();
            });
            o.find('.paginationItem').each(function(i, v) {
                var o = $(v);
                var href1 = o.attr('href');
                o.attr('href', 'javascript:void(0)');
                o.attr('data-href', href1);
                o.click(functionPageClick);
            });
            o.find('.gsssTooltip').tooltip();
            $('#transactions').html(o);
        }
    });
};

var operationList = [
    {
        url: '/cabinet/profile/walletBtc/balanceExternal',
        success: function(ret) {
            $('#balance-external').html(ret.balance);
            $('#balance-external').parent().show();
            return true;
        },
        onErrorRepeat: true
    },
    {
        url: '/cabinet/profile/walletBtc/balanceInternal',
        success: function(ret) {
            $('#internal-balance').val(ret.confirmedBalance);
            $('#external-wallet-confirmed').html(ret.confirmedBalance);
            $('#internal-balance').attr('data-balance', ret.all);
            $('#external-wallet-confirmed-rub').html(
                formatAsDecimal(
                    ret.confirmedBalance * $('#kurs-rub-btc').data('value'), ',', 2
                )
            );

            if (ret.unconfirmedBalance != 0) {
                $('#external-wallet-unconfirmed').html(ret.unconfirmedBalance);
                $('#external-wallet-unconfirmed').parent().parent().show();
            }
            return true;
        }
    },
    {
        url: '/cabinet_wallet/transactions-list',
        data: function() {
            return {
                balance: $('#internal-balance').attr('data-balance')
            };
        },
        beforeSend: function() {
            $('#transactions').html(
                $('<img>', {src: '{$path}' })
            );
        },
        success: function(ret) {
            var o = $(ret.html);
            o.find('.js-buttonTransactionInfo').click(function(e) {
                var data = $(this).data('html');
                $('.js-hash').html(data.hash);
                $('.js-time').html(data.time);
                $('.js-confirmations').html(data.confirmations);
                $('.js-block_height').html(data.block_height);
                $('.js-block_hash').html(data.block_hash);
                $('.js-total_input_value').html(data.total_input_value);
                $('.js-total_output_value').html(data.total_output_value);
                $('.js-total_fee').html(data.total_fee);
                $('.js-download-bill').data('hash', data.hash);
                $('#modalTransactionInfo').modal();
            });

            o.find('.paginationItem').each(function(i, v) {
                var o = $(v);
                var href1 = o.attr('href');
                o.attr('href', 'javascript:void(0)');
                o.attr('data-href', href1);
                o.click(functionPageClick);
            });

            o.find('.gsssTooltip').tooltip();
            $('#transactions').html(o);
            return true;
        }
    }
];
var i = 0;

var functionCalc = function(i, arr) {
    var item = arr[i];
    var options = {
        url: item.url,
        success: function(ret) {
            i++;
            ret =  item.success(ret);
            if (i < operationList.length) {
                if (ret) functionCalc(i, arr);
            }
        },
        errorScript: function(ret) {
            if (typeof(item.onErrorRepeat) != "undefined") {
                if (item.onErrorRepeat) {
                    i++;
                    ret =  item.success(ret);
                    if (i <= operationList.length) {
                        if (ret) functionCalc(i, arr);
                    }
                }
            }
        }
    };
    if (typeof(item.beforeSend) != "undefined") {
        options.beforeSend = item.beforeSend;
    }
    if (typeof(item.data) != "undefined") {
        options.data = item.data();
    }

    ajaxJson(options);
};
functionCalc(i, operationList);

JS
);
?>
<!-- Nav tabs -->
<ul class="nav nav-tabs" role="tablist">
    <li role="presentation"><a href="#external" aria-controls="home" role="tab" data-toggle="tab">Внешний</a></li>
    <li role="presentation" class="active"><a href="#internal" aria-controls="profile" role="tab" data-toggle="tab">Внутренний</a>
    </li>
</ul>

<!-- Tab panes -->
<div class="tab-content">
<div role="tabpanel" class="tab-pane fade" style="padding: 40px 0px 40px 0px;" id="external">
    <p>Ваш биткойн кошелек для бонусов:</p>

    <div class="input-group">
        <input type="text" class="form-control" placeholder="Биткойн адрес" id="bitCoinAddress"
               value="<?= $user->get('bitcoin_address', '') ?>">
    <span class="input-group-btn">
        <?php
        $this->registerJs(<<<JS
            $('#saveBitCoinAddress').click(function() {
                if ($('#bitCoinAddress').val().length > 40) {
                    infoWindow('Кошелек может быть длиной не более 40 символов');
                    return;
                }

                var b = $(this);
                ajaxJson({
                    url: '/cabinet/profile/walletBtc/save-address',
                    data: {
                        address: $('#bitCoinAddress').val()
                    },
                    success: function(ret) {
                        b.parent().parent().addClass('has-success');
                        setTimeout(function() {
                            b.parent().parent().removeClass('has-success');
                        }, 1000);
                    }
                });
            });
JS
        );
        ?>
        <button class="btn btn-default" type="button" id="saveBitCoinAddress">Сохранить</button>
      </span>

    </div>

    <?php if ($user->get('bitcoin_address', '') != '') { ?>
        <p style="margin-top: 40px;">
            <img src="<?= 'data:image/png;base64,' . base64_encode(
                (new \Endroid\QrCode\QrCode())
                    ->setText('bitcoin:' . $user->get('bitcoin_address', ''))
                    ->setSize(180)
                    ->setPadding(0)
                    ->setErrorCorrection('high')
                    ->setForegroundColor(['r' => 0, 'g' => 0, 'b' => 0, 'a' => 0])
                    ->setBackgroundColor(['r' => 255, 'g' => 255, 'b' => 255, 'a' => 0])
                    ->setLabelFontSize(16)
                    ->setImageType(\Endroid\QrCode\QrCode::IMAGE_TYPE_PNG)
                    ->get('png')
            )
            ?>" class="thumbnail">
        </p>
        <h2 class="alert alert-success" style="display: none;">
            Баланс: <span id="balance-external"><img src="<?= $path ?>"></span> BTC</h2>
    <?php } ?>


</div>
<div role="tabpanel" class="tab-pane fade in active" style="padding: 40px 0px 40px 0px;" id="internal">

<p class="text-center">
    RUB/BTC = <b id="kurs-rub-btc" data-value="<?= \common\models\Config::get(\app\commands\MoneyRateController::RATE_BTC_RUB) ?>"><?= \Yii::$app->formatter->asDecimal(\common\models\Config::get(\app\commands\MoneyRateController::RATE_BTC_RUB), 2) ?></b> <span style="color: #ccc;">руб.</span>
</p>
<p class="text-center">
    <img src="/images/cabinet_wallet/index/bitcoin2.jpg" title="Свобода Истина Справедливость (В криптографию МЫ верим)" class="gsssTooltip" style="width: 100%;max-width: 308px;">
</p>
<?php if (Yii::$app->user->identity->hasBitCoin()) { ?>
    <div class="input-group">
        <span class="input-group-btn">

                                <?php
                                \app\assets\ZeroClipboard\Asset::register($this);
                            $this->registerJs(<<<JS

        // Сюда указываем элемент, по которому будет запускаться процедура копирования.
		var target = $(".buttonCopy");
		var client = new ZeroClipboard(target);

		target.click(function (e) {
			e.preventDefault();
		});

		client.on("copy", function (event) {
			// Текст, который вставится в буфер.
			var textToCopy = $('#internalAddress').val();
			var clipboard = event.clipboardData;

			clipboard.setData("text/plain", textToCopy);
		});

		// Событие, которое вызывается после копирования. Можно, например, вывести какое-то окошко.
		client.on('aftercopy', function (event) {
			infoWindow('Скопировано!');
		});
JS
                                );

                                ?>
                                <button type="button" class="btn btn-default buttonCopy">
                                    <i class="fa fa-files-o " style="margin-right: 5px;"></i> Скопировать в буфер
                                </button>
                            </span>
        <?php
        $b = $user->getBitCoinWallet();
        ?>
        <input type="text" class="form-control" placeholder="Адрес кошелька"
               style="font-family: Consolas, Courier New, monospace;"
               value="<?= $b->address ?>"
               id="internalAddress"
            >

        <span class="input-group-btn">
            <a href="/cabinet_wallet/get-qr-code" class="btn btn-default gsssTooltip" title="Скачать QR код">
                <i class="fa fa-qrcode"></i>
            </a>
        </span>
    </div>
    <div class="modal fade" id="modalIn" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title text-center" id="exampleModalLabel">Принять на счет</h4>
                </div>
                <div class="modal-body" style="text-align: center;">

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal" style="width: 100%">Закрыть
                    </button>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="modalOut" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="exampleModalLabel">Отправить на счет</h4>
                </div>
                <div class="modal-body">
                    <form id="formOut">
                        <div class="form-group recipient-field-address">
                            <label for="recipient-name" class="control-label">Кому:</label>
                            <input type="text" class="form-control" id="recipient-address" name="address"
                                   style="font-family: Consolas, Courier New, monospace"
                                >
                            <p class="help-block help-block-error" style="display: none;"></p>
                        </div>
                        <div class="form-group recipient-field-amount">
                            <label for="recipient-name" class="control-label">Сколько:</label>

                            <div class="input-group">
                                <span class="input-group-addon" id="basic-addon1"><i class="glyphicon glyphicon-btc"
                                                                                     style="padding-right: 7px; padding-left: 2px;"></i></span>
                                <input
                                    type="text"
                                    class="form-control"
                                    placeholder="0.00000001"
                                    name="amount"
                                    style="font-family: Consolas, Courier New, monospace"

                                    aria-describedby="basic-addon1" autocomplete="off" id="recipient-amount">
                            </div>
                            <p class="help-block help-block-error" style="display: none;"></p>
                        </div>
                        <div class="form-group recipient-field-password">
                            <label for="recipient-name" class="control-label">Пароль:</label>
                            <input type="password" class="form-control" id="recipient-password" name="password">
                            <p class="help-block help-block-error" style="display: none;"></p>
                        </div>
                        <div class="form-group recipient-field-comment">
                            <label for="message-text" class="control-label">Комментарий:</label>
                            <textarea class="form-control" id="recipient-amount" name="comment"></textarea>
                            <p class="help-block help-block-error" style="display: none;"></p>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <?php
                    $this->registerJs(<<<JS
$('#formOut .form-control').on('focus', function() {
    var o = $(this);
    var p = o.parent();
    if (p.hasClass('input-group')) {
        p = p.parent();
    }
    p.removeClass('has-error');
    p.find('p.help-block-error').hide();
});
$('.buttonSend').click(function() {
    ajaxJson({
        url: '/cabinet/profile/walletBtc/send',
        data: $('#formOut').serializeArray(),
        success: function(ret) {
            $('#modalOut').modal('hide').on('hidden.bs.modal', function(e) {
                $('#modalInfo .modal-body')
                .append(
                    $('<p>').html('Транзакция успешно отправлена')
                )
                .append(
                    $('<p>').append(
                        $('<code>').html(ret.address)
                    )
                )
                ;
                $('#modalInfo').modal();
            });
        },
        errorScript: function(ret) {
            switch (ret.id) {
                case 101:
                    $.each(ret.data,function(i,v) {
                        var name = v.name;
                        var value = v.value;
                        var o = $('#recipient-'+ name);
                        var t = $('.recipient-field-' + name);
                        t.addClass('has-error');
                        t.find('p.help-block-error').html(value.join('<br>')).show();
                    });

                    break;
            }
        }
    });
});
JS
                    );
                    ?>
                    <button type="button" class="btn btn-success buttonSend" style="width: 100%">Оплатить</button>
                </div>
            </div>
        </div>
    </div>
    <h2 class="alert alert-success text-center">
        <span id="external-wallet-confirmed"><img src="<?= $path ?>"></span><br>
        <small>BTC</small>
    </h2>
    <h2 class="alert alert-success text-center">
        <span id="external-wallet-confirmed-rub"><img src="<?= $path ?>"></span><br>
        <small>RUB</small>
    </h2>

    <?php if (Yii::$app->deviceDetect->isDesktop()) { ?>
    <div id="kursGraf">
        <?php
        $this->registerJs(<<<JS

    /**
    *
    * @param val
    * @param separator
    * @param int lessOne кол-во символов после точки, если 0 то точка не выводится, добавляются задние нули работает пока только для 0..2
    * @returns {string}
    */
    function formatAsDecimal(val, separator = ',', lessOne = 0)
    {
        var pStr = '';
        var original = val;
        val = parseInt(val);
        if (val >= 1000) {
            var t = parseInt(val/1000);
            var ost = (val - t * 1000);
            var ostStr = ost;
            if  (ost == 0) {
                ostStr = '000';
            } else {
                if (ost < 10) {
                    ostStr = '00' + ost;
                } else if (ost < 100) {
                    ostStr = '0' + ost;
                }
            }
            pStr = t + separator + ostStr;
        } else {
            pStr = val;
        }
        var oStr = '';
        if (lessOne > 0) {
            oStr = '.';
            var d = original - parseInt(original);
            d = d * Math.pow(10, lessOne);
            d = parseInt(d);
            if (d == 0) {
                for (var i=0;i<lessOne;i++){
                    oStr = oStr + '0';
                }
            } else {
                if (lessOne == 1) {
                    oStr = d;
                } else {
                    if (d < 10) {
                        oStr = oStr + '0' + d;
                    } else {
                        oStr = oStr + d;
                    }
                }
            }
        }

        return pStr + oStr;
    }
JS
        );

        ?>


        <?php
        $period = 24 * 2;

        $rows = \common\models\piramida\BitCoinKurs::find()
            ->select([
                'rub',
                'time',
            ])
            ->where(['>', 'time', time() - (60*60*24*60)])
            ->all()
        ;
        $rowsJson = \yii\helpers\Json::encode($rows);
        $this->registerJs(<<<JS
        Highcharts.setOptions({
            global: {
                timezoneOffset: -3 * 60
            }
        });
        var rows = {$rowsJson};
        var newRows = [];
        for(i = 0; i < rows.length; i++)
        {
            var item = rows[i];
            newRows.push({
                x: new Date(item.time * 1000),
                y: item.rub
            });
        }
JS
        );
        ?>
        <?= \cs\Widget\HighCharts\HighCharts::widget([
            'chartOptions' => [
                'chart' => [
                    'zoomType' => 'x',
                    'type'     => 'spline',
                ],
                'title' => [
                    'text' => 'График',
                ],
                'subtitle' => [
                    'text' => 'Выделите область для изменения масштаба',
                ],
                'xAxis' => [
                    'type' => 'datetime',
                ],
                'yAxis' => [
                    [
                        'title' => [
                            'text' => 'Количество',
                        ],
                    ]
                ],
                'legend' => [
                    'enabled' => true
                ],
                'tooltip' => [
                    'crosshairs' => true,
                    'shared' => true,
                ],
                'plotOptions' => [
                    'series' => [
                        'turboThreshold' => 0,
                    ],
                ],
                'series' => [
                    [
                        'type' => 'spline',
                        'name' => 'RUB',
                        'data' => new \yii\web\JsExpression('newRows'),
                    ],
                ],
            ],
        ]);

        ?>
    </div>
    <?php } ?>


    <p style="display: none;">Неподтвержденне транзакции:<br>
        <code><span id="external-wallet-unconfirmed"></span></code>
    </p>
    <?php
    $this->registerJs(<<<JS
$('.buttonIn').click(function(e) {

    ajaxJson({
        url: '/cabinet/profile/walletBtc/in',
        success: function(ret) {
            $('#modalIn .modal-body').html(ret.html);
            $('#modalIn').modal();
        }
    });
});
$('.buttonOut').click(function(e) {
    $('#modalOut').modal();
});
JS
    );
    ?>
    <div class="row">
        <div class="col-lg-10 col-lg-offset-1">
            <center>
                <div class="input-group">
                        <span class="input-group-btn">
                            <a href="https://localbitcoins.net/ru/buy_bitcoins" class="btn btn-default" target="_blank">
                                <i class="glyphicon glyphicon-import" style="margin-right: 5px;"></i>Купить
                            </a>
                            <button type="button" class="btn btn-default buttonIn">
                                <i class="glyphicon glyphicon-import" style="margin-right: 5px;"></i>Принять
                            </button>
                        </span>
                    <input type="text" class="form-control" placeholder="0.00000001"
                           value=""
                           id="internal-balance"
                           data-balance=""
                        >

                            <span class="input-group-btn">
                                <button type="button" class="btn btn-default buttonOut">
                                    <i class="glyphicon glyphicon-export" style="margin-right: 5px;"></i>Перевести
                                </button>
                            <a href="https://localbitcoins.net/ru/sell_bitcoins" class="btn btn-default" target="_blank">
                                <i class="glyphicon glyphicon-export" style="margin-right: 5px;"></i>Продать
                            </a>
                            </span>
                </div>
                <!-- /input-group -->
            </center>
        </div>
    </div>
    <h4 class="page-header text-center">Транзакции</h4>
    <div id="transactions">

    </div>
<?php } else { ?>
    <?php
    $this->registerJs(<<<JS
        $('.buttonCreate').click(function(e) {
            ajaxJson({
                url: '/cabinet/profile/walletBtc/create',
                success: function(ret) {
                    infoWindow('Кошлек успешно создан');
                    window.location.reload();
                }
            });
        });
JS
    );
    ?>
    <div style="text-align: center">
        <button class="btn btn-success btn-lg buttonCreate">Создать кошелек</button>
    </div>
<?php } ?>
</div>
</div>
</div>


</div>
<!-- /.row -->

</div>
</div>

</div>

</div>
<div class="col-lg-4">
    <?= $this->render('../cabinet/profile_menu/profile_menu') ?>
</div>
</div>
</div>
