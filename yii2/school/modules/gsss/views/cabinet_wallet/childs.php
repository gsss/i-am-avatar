<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;
use app\models\UnionCategory;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model cs\base\BaseForm */

$this->title = 'Мои дети';

/** @var \app\models\User $user */
$user = Yii::$app->user->identity;
$tree = $user->getTree();
?>

<div class="container">
    <div class="col-lg-12">
        <h1 class="page-header"><?= Html::encode($this->title) ?></h1>
        <?= \cs\Widget\BreadCrumbs\BreadCrumbs::widget([
            'items' => [
                $this->title
            ],
            'home'  => [
                'name' => 'я',
                'url'  => \yii\helpers\Url::to(['site/user', 'id' => Yii::$app->user->id])
            ]
        ]) ?>
        <hr>
        <div class="col-lg-8">

            <ul class="media-list">
                <li class="media">
                    <div class="media-left">
                        <a href="#">
                            <img class="media-object"
                                 src="<?= Yii::$app->user->identity->getAvatar() ?>"
                                 style="width: 64px; height: 64px; border-radius: 10px; border: 1px solid #ccc"
                                >
                        </a>
                    </div>
                    <div class="media-body">
                        <h4 class="media-heading"><?= Yii::$app->user->identity->getName2() ?></h4>
                        <span
                            class="label label-success"><?= Yii::$app->formatter->asCurrency(Yii::$app->user->identity->getWallet('A')->value) ?></span>
                        | <span
                            class="label label-info"><?= Yii::$app->formatter->asCurrency(Yii::$app->user->identity->getWallet('B')->value) ?></span>

                        <?= $this->render('_tree', ['rows' => $tree]) ?>
                    </div>
                </li>
            </ul>

        </div>
        <div class="col-lg-4">
            <?= $this->render('../cabinet/profile_menu/profile_menu') ?>
        </div>
    </div>
</div>
