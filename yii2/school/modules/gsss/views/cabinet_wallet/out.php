<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;
use app\models\UnionCategory;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model cs\base\BaseForm */

$this->title = 'Снять деньги';

/** @var \app\models\User $me */
$me = Yii::$app->user->identity;
$walletA = $me->getWallet('A');
$walletB = $me->getWallet('B');

?>
<div class="container">
    <div class="col-lg-12">
        <h1 class="page-header"><?= Html::encode($this->title) ?></h1>
        <?= \cs\Widget\BreadCrumbs\BreadCrumbs::widget([
            'items' => [
                $this->title
            ],
            'home'  => [
                'name' => 'я',
                'url'  => \yii\helpers\Url::to(['site/user', 'id' => Yii::$app->user->id])
            ]
        ]) ?>
        <hr>


        <div class="col-lg-8">
            <?php if (Yii::$app->session->hasFlash('contactFormSubmitted')) {?>
                <div class="alert alert-success">
                    Заявка на вывод успешно создана.
                </div>
            <?php } else { ?>
                <?php $form = ActiveForm::begin([
                    'enableAjaxValidation' => true,
                ]) ?>
                <?= $form->field($model, 'walletId')->radioList([
                    $walletA->id => 'A' . ' ' . Yii::$app->formatter->asDecimal($walletA->value),
                    $walletB->id => 'B' . ' ' . Yii::$app->formatter->asDecimal($walletB->value),
                ], ['unselect' => $walletA->id])->label('Кошелек') ?>
                <?= $form->field($model, 'value')->label('Сумма') ?>
                <?= $form->field($model, 'destination')->textarea(['rows' => 5])->label('Реквизиты') ?>
                <?= Html::submitButton('Оформить заявку', [
                    'class' => 'btn btn-success',
                    'style' => 'width:100%;',
                ]) ?>
                <?php ActiveForm::end() ?>
            <?php }?>

            <h3 class="page-header">Заявки на вывод</h3>
            <style>
                a.asc:after {
                    content: ' ↓';
                    display: inline;
                }
                a.desc:after {
                    content: ' ↑';
                    display: inline;
                }
            </style>
            <?php
            $sort = new \yii\data\Sort([
                'attributes' => [
                    'id',
                    'is_paid' => [
                        'label' => 'Оплачен?',
                    ],
                    'datetime' => [
                        'label' => 'Создана',
                    ],
                    'price' => [
                        'label' => 'Сумма',
                    ],
                ],
                'defaultOrder' => [
                    'datetime' => SORT_DESC,
                ],
            ]);
            ?>
            <?php \yii\widgets\Pjax::begin()?>
            <?php
            $this->registerJs(<<<JS
            $('.rowTable').click(function() {
                window.location = '/cabinet/profile/wallet/out/' + $(this).data('id');
            });
JS
            );
            ?>
            <?= \yii\grid\GridView::widget([
                'dataProvider' => new \yii\data\ActiveDataProvider([
                    'query' => \app\models\Piramida\OutRequest::find()
                    ->andWhere(['wallet_id' => [$walletA->id, $walletB->id]])
                    ->andWhere(['not', ['is_paid' => 1]])
                    ,

                    'sort' => $sort
                ]),
                'tableOptions' => [
                    'class' => 'table table-hover table-striped',
                ],
                'rowOptions' => function($item) {
                    return [
                        'class' => 'rowTable',
                        'data' => [
                            'id' => \yii\helpers\ArrayHelper::getValue($item, 'id', 0),
                        ],
                        'role' => 'button',
                    ];
                },
                'columns' => [
                    'id',
                    'price:currency:Сумма',
                    'destination:text:Назначение',
                    [
                        'header'  => $sort->link('datetime'),
                        'content' => function ($item) {
                            $v = \yii\helpers\ArrayHelper::getValue($item, 'datetime', 0);
                            if ($v == 0) return '';

                            return Html::tag('abbr', \cs\services\DatePeriod::back($v, ['isShort' => true]), ['class' => 'gsssTooltip', 'title' => Yii::$app->formatter->asDatetime($v)]);
                        },
                        'attribute' => 'datetime'
],
                    [
                        'header'  => 'Оплачена?',
                        'content' => function ($item) {
                            $v = \yii\helpers\ArrayHelper::getValue($item, 'is_paid', 0);
                            if ($v == 0) return Html::tag('span', 'Нет', ['class' => 'label label-default']);

                            return Html::tag('span', 'Да', ['class' => 'label label-success']);
                        },
                        'attribute' => 'is_paid'
                    ],
                ]
            ])?>
            <?php \yii\widgets\Pjax::end()?>
        </div>
        <div class="col-lg-4">
            <?= $this->render('../cabinet/profile_menu/profile_menu') ?>
        </div>

    </div>
</div>
