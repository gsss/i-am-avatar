<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;
use app\models\UnionCategory;

/* @var $this yii\web\View */
/* @var $user \app\models\User */
$this->title = 'Снять деньги';
?>
<p>
    <img src="/images/cabinet_wallet/referal-document/top.jpg" width="100%">
</p>
<h1><?= $user->getName2() ?></h1>
<hr>
<style>
    .cell1 {
        padding: 10px;
        vertical-align: top;
    }

    .cell2 {
        padding: 10px;
        vertical-align: top;
    }
</style>
<table>
    <tr>
        <td class="cell1">
            <img src="<?= $user->getAvatar() ?>" width="180" style="border-radius: 20px;">
        </td>
        <td class="cell2" style="font-size: 200%">
            Здравствуйте! Я ваш проводник в Новый Мир на Новую Землю!
        </td>
    </tr>
    <tr>
        <td class="cell1" style="text-align: right;">
            Тел:
        </td>
        <td class="cell2">
            <?= $user->get('phone_invite', '') ?>
        </td>
    </tr>
    <tr>
        <td class="cell1" style="text-align: right;">
            Почта:
        </td>
        <td class="cell2">
            <?= $user->getEmail() ?>
        </td>
    </tr>
    <tr>
        <td class="cell1">
            <img src="<?= 'data:image/png;base64,' . base64_encode(
                (new \Endroid\QrCode\QrCode())
                    ->setText('https://www.galaxysss.com' . Url::to(['shop/index', 'parent_id' => $user->getId()]))
                    ->setSize(180)
                    ->setPadding(0)
                    ->setErrorCorrection('high')
                    ->setForegroundColor(['r' => 0, 'g' => 0, 'b' => 0, 'a' => 0])
                    ->setBackgroundColor(['r' => 255, 'g' => 255, 'b' => 255, 'a' => 0])
                    ->setLabelFontSize(16)
                    ->setImageType(\Endroid\QrCode\QrCode::IMAGE_TYPE_PNG)
                    ->get('png')
            )
            ?>" class="thumbnail">
        </td>
        <td class="cell2">
            <p>Реферальная ссылка:</p>

            <p class="alert alert-info"
               style="font-family: 'Courier New', Monospace;"><?= 'https://www.galaxysss.com' . Url::to(['shop/index', 'parent_id' => $user->getId()]); ?></p>
        </td>
    </tr>
</table>
<hr>
С Любовью и Светом на Новой Земле!