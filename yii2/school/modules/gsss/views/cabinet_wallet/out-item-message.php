<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;
use app\models\UnionCategory;

/* @var $this yii\web\View */
/* @var $message \app\models\Piramida\OutRequestMessage */
/* @var $user array
 * [
 *      'name'
 *      'avatar'
 * ]
 */

$userInCache = $user;
?>
<div class="row">
    <div class="col-sm-2">
        <div class="thumbnail">
            <img class="img-responsive user-photo" src="<?= $userInCache['avatar'] ?>">
        </div><!-- /thumbnail -->
    </div><!-- /col-sm-1 -->
    <div class="col-sm-10">
        <div class="panel panel-default">
            <div class="panel-heading">
                <strong><?= $userInCache['name'] ?></strong>
                <span class="text-muted">прокомментировано</span> <abbr class="gsssTooltip" title="<?= Yii::$app->formatter->asDatetime($message->datetime) ?>">
                    <?= \cs\services\DatePeriod::back($message->datetime, ['isShort' => false]) ?>
                </abbr>
            </div>
            <div class="panel-body">
                <?php if ($message->status) { ?>
                    <?php
                    $html = '';
                    switch($message->status) {
                        case \app\models\Piramida\OutRequest::STATUS_CONFIRM_SHOP:
                            $html = Html::tag('span', 'Оплата подтверждена', ['class' => 'label label-success']);
                    }
                    ?>
                    <?php if ($html) { ?>
                        <?= $html ?>
                    <?php } ?>
                <?php } ?>
                <?= nl2br($message->message) ?>
            </div><!-- /panel-body -->
        </div><!-- /panel panel-default -->
    </div><!-- /col-sm-5 -->
</div>
