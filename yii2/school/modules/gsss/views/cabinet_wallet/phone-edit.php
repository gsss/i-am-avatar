<?php
/**
 * Created by PhpStorm.
 * User: Бог-Творец
 * Date: 11.09.2016
 * Time: 0:26
 */
$this->title = 'Телефон для контактных данных';
$user = Yii::$app->user->identity;
?>

    <div class="container">
        <div class="col-lg-12">
            <h1 class="page-header"><?= \cs\helpers\Html::encode($this->title) ?></h1>
            <?= \cs\Widget\BreadCrumbs\BreadCrumbs::widget([
                'items' => [
                    $this->title
                ],
                'home'  => [
                    'name' => 'я',
                    'url'  => \yii\helpers\Url::to(['site/user', 'id' => Yii::$app->user->id])
                ]
            ]) ?>
            <hr>


            <div class="col-lg-8">
                <p>Телефон для контактных данных:</p>

                <div class="input-group">
                    <input type="text" class="form-control" placeholder="Телефон" id="bitCoinAddress"
                           value="<?= $user->get('phone_invite', '') ?>">
    <span class="input-group-btn">
        <?php
        $this->registerJs(<<<JS
            $('#saveBitCoinAddress').click(function() {
                if ($('#bitCoinAddress').val().length > 20) {
                    infoWindow('Кошелек может быть длиной не более 20 символов');
                    return;
                }

                var b = $(this);
                ajaxJson({
                    url: '/cabinet/profile/wallet/phoneEditSave',
                    data: {
                        phone: $('#bitCoinAddress').val()
                    },
                    success: function(ret) {
                        b.parent().parent().addClass('has-success');
                        setTimeout(function() {
                            b.parent().parent().removeClass('has-success');
                        }, 1000);
                        window.location.href = '/cabinet/profile/wallet/referalDocument';
                    }
                });
            });
JS
        );
        ?>
        <button class="btn btn-default" type="button" id="saveBitCoinAddress">Сохранить и получить PDF</button>
      </span>
                </div>
                <!-- /input-group -->
            </div>
            <!-- /.col-lg-6 -->
            <div class="col-lg-4">
                <?= $this->render('../cabinet/profile_menu/profile_menu') ?>
            </div>

        </div>
    </div>
