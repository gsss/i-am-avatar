<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;
use app\models\UnionCategory;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \app\models\Form\WalletInvite */

$this->title = 'Реферальная ссылка';
$user = Yii::$app->user->identity;
?>
<div class="container">
    <div class="col-lg-12">
        <h1 class="page-header"><?= Html::encode($this->title) ?></h1>
        <?= \cs\Widget\BreadCrumbs\BreadCrumbs::widget([
            'items' => [
                $this->title
            ],
            'home'  => [
                'name' => 'я',
                'url'  => \yii\helpers\Url::to(['site/user', 'id' => Yii::$app->user->id])
            ]
        ]) ?>
        <hr>
        <div class="col-lg-8">
            <div class="btn-group">
                <a href="<?= ($user->get('phone_invite', '') == '')? '/cabinet/profile/wallet/phoneEdit' : '/cabinet/profile/wallet/referalDocument' ?>" class="btn btn-default">Получить PDF</a>
                <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <span class="caret"></span>
                    <span class="sr-only">Toggle Dropdown</span>
                </button>
                <ul class="dropdown-menu">
                    <li><a href="/cabinet/profile/wallet/phoneEdit">Редактировать телефон</a></li>
                </ul>
            </div>

            <hr>
            <?php if (Yii::$app->session->hasFlash('contactFormSubmitted')) {?>
                <div class="alert alert-success">
                    Рассылка сделана.
                </div>

            <?php } else {?>
                <?php $form = ActiveForm::begin([
                    'id'      => 'contact-form',
                    'options' => ['enctype' => 'multipart/form-data']
                ]); ?>
                <?= $form->field($model, 'link', ['inputOptions' => ['class' => 'form-control']])
                    ->label('Ваша реферальная ссылка')
                ?>
                <?php ActiveForm::end(); ?>

            <?php } ?>



        </div>
        <div class="col-lg-4">
            <?= $this->render('../cabinet/profile_menu/profile_menu') ?>
        </div>
    </div>
</div>
