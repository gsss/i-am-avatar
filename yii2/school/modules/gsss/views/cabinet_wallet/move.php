<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;
use app\models\UnionCategory;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model cs\base\BaseForm */

$this->title = 'Перевести деньги';
?>
<div class="container">
    <div class="col-lg-12">
        <h1 class="page-header"><?= Html::encode($this->title) ?></h1>
        <?= \cs\Widget\BreadCrumbs\BreadCrumbs::widget([
            'items' => [
                $this->title
            ],
            'home'  => [
                'name' => 'я',
                'url'  => \yii\helpers\Url::to(['site/user', 'id' => Yii::$app->user->id])
            ]
        ]) ?>
        <hr>

        <div class="col-lg-8">
            <?php if (Yii::$app->session->hasFlash('contactFormSubmitted')) {?>
                <div class="alert alert-success">
                    Перевод выполнен успешно.
                </div>

            <?php } else {?>
                <?php $form = ActiveForm::begin([
                    'enableAjaxValidation' => true,
                ]) ?>
                <?= $form->field($model, 'a')->label('Сумма списания с кошелька A') ?>
                <?= $form->field($model, 'b')->label('Сумма списания с кошелька B') ?>
                <?= $form->field($model, 'emailDestination')->label('Получатель')->hint('Введите полный email получателя') ?>
                <?= Html::submitButton('Перевести', [
                    'class' => 'btn btn-success',
                    'style' => 'width:100%;',
                ]) ?>
                <?php ActiveForm::end() ?>
            <?php }?>
        </div>
        <div class="col-lg-4">
            <?= $this->render('../cabinet/profile_menu/profile_menu') ?>
        </div>
    </div>

</div>
