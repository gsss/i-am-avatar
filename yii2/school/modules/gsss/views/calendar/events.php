<?php
/** @var array $items */
/** @var $this yii\web\View */

use cs\helpers\Html;
use app\services\GsssHtml;
use cs\models\Calendar\Maya;

$this->title = 'События на Планете Земля';

?>
<div class="container">

    <div class="col-lg-12">
        <h1 class="page-header">События</h1>
    </div>

    <?php if (count($items) > 0) { ?>
        <?php foreach ($items as $event) {
            $link = '/events/' . $event['id'];
            ?>
            <div class="col-lg-4">
                <h3 style="height: 70px;"><?= $event['name'] ?></h3>

                <p><?= $event['date'] ?></p>

                <p style="margin-bottom: 0px;padding-bottom: 0px;">
                    <a href="<?= $link ?>">
                        <img
                            src="<?= $event['image'] ?>"
                            width="100%"
                            alt=""
                            class="thumbnail"
                            >
                    </a>
                </p>
            </div>
        <?php } ?>
    <?php } ?>

    <div class="col-lg-12">
        <hr>
        <?= $this->render('../blocks/share', [
            'image'       => \yii\helpers\Url::to('/images/calendar/events/header.jpg', true),
            'url'         => \yii\helpers\Url::current([], true),
            'title'       => $this->title,
            'description' => 'Предстоящие события на Планете Земля',
        ]) ?>
    </div>


</div>