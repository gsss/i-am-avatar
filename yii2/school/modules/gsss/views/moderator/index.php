<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\ContactForm */

$this->title = 'Модерация';
?>
<div class="container">

    <h1 class="page-header">Модерация</h1>

    <p>
        <a href="/moderator/unionList">
            Объединения
        </a> (<?= \app\models\Union::query(['moderation_status' => 2])->count() ?>)
    </p>
    <p>
        <a href="/moderator/shop/productList">
            Товары
        </a> (<?= \app\models\Shop\Product::query(['moderation_status' => 2])->count() ?>)
    </p>
    <p>
        <a href="/moderator/shop/imageList">
            Изображния
        </a> (<?= \app\models\Shop\ProductImage::query(['moderation_status' => 2])->count() ?>)
    </p>
    <p>
        <a href="/moderator/channeling">
            Послания
        </a> (<?= \app\models\Chenneling::query(['moderation_status' => 2])->count() ?>)
    </p>

</div>