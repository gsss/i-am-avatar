<?php

use yii\helpers\Url;

/**
 * params:
 * [
 *      'rows'     => [
 *                       [
 *                            'id'                 => '524'
 *                            'name'               => '123456'
 *                            ...
 *                            'selected'           => bool
 *                        ], ...
 *                    ]
 *     'formName'          => $this->model->formName(), // string
 *     'model'             => $this->model,             // \yii\base\Model
 *     'attrId'            => $this->attrId,            // attribute id
 *     'attrName'          => $this->attrName,          // attribute name
 *     'templateVariables' => []
 * ]
 */

/** @var $rows array */

?>



<ul>
    <?php
    foreach ($rows as $item) {
        ?>
        <li class="treeNode">
            <a href="<?= Url::to(['admin-shop-catalog/edit',
                'id' => $item['id']
            ]) ?>">
                <?= $item['name'] ?>
            </a>
            <a data-id="<?= $item['id'] ?>" class="buttonDelete function" role="button">
                d
            </a>
            <?php if (isset($item['nodes'])) { ?>
                <?php if (count($item['nodes']) > 1) { ?>
                    <a class="function" href="<?= Url::to(['admin-shop-catalog/sort-category',
                        'id' => $item['id']
                    ]) ?>">
                        s
                    </a>
                <?php } ?>
            <?php } ?>
        </li>
        <?php
        if (isset($item['nodes'])) {
            echo $this->render('_tree', [
                'rows' => $item['nodes'],
            ]);
        }
        ?>

    <?php } ?>
</ul>
