<?php

/** @var $this \yii\web\View */
/** @var $treeNode \app\models\Shop\TreeNodeGeneral */

use yii\helpers\Url;
use app\services\GsssHtml;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use app\services\GetArticle\Collection;

$this->title = 'Каталог Магазина';

$items = \app\models\Shop\TreeNodeGeneral::find()
    ->select([
        'name',
        'id',
    ])
    ->where(['parent_id' => $treeNode->id])
    ->orderBy(['sort_index' => SORT_ASC])
    ->asArray()
    ->all();
$arr = [];
$id = \cs\services\Security::generateRandomString();
$parentId = $treeNode->id;
foreach ($items as $item) {
    $arr[] = ['content' => Html::tag('span', $item['name'], ['data-id' => $item['id'], 'class' => 'sortable_' . $id])];
}
$items = $arr;
?>

<div class="container">
    <div class="col-lg-12">
        <h1 class="page-header"><?= Html::encode($this->title) ?></h1>
        <?= \kartik\sortable\Sortable::widget([
            'showHandle'   => true,
            'pluginEvents' => [
                'sortupdate' => <<<JS
            function(e, ui) {
                var i;
                var newIds = [];
                for(i = 0; i < ui.endparent.length; i++) {
                    $(ui.endparent[i]).find('li').each(function(i,v) {
                        $(v).find('span.sortable_' + '{$id}').each(function(i2,v2) {
                            newIds.push($(v2).data('id'));
                        });
                    });
                }
                ajaxJson({
                    url: '/admin/shopCatalog/{$parentId}/ajax',
                    data: {
                        ids: newIds
                    },
                    success: function(ret) {
                        alert('Удачно');
                    }
                });
            }
JS
                ,
            ],
            'items'        => $items
        ]); ?>
    </div>
</div>