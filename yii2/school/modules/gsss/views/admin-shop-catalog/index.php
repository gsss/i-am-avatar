<?php

/** $this \yii\web\View  */

use yii\helpers\Url;
use app\services\GsssHtml;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use app\services\GetArticle\Collection;

$this->title = 'Каталог Магазина';

$this->registerJS(<<<JS
    $('.function').hide();
    $('.treeNode')
    .on('mouseover', function(e) {
        $(this).find('.function').show();
    })
    .on('mouseout', function(e) {
        $(this).find('.function').hide();
    })
    ;
    $('.buttonDelete').click(function (e) {
        if (confirm('Подтвердите удаление')) {
            var button = $(this);
            var id = $(this).data('id');
            ajaxJson({
                url: '/admin/shopCatalog/' + id + '/delete',
                success: function (ret) {
                    infoWindow('Успешно', function() {
                        button.parent()[0].nextElementSibling.remove();
                        button.parent().remove();
                    });
                }
            });
        }
    });

JS
);
?>

<div class="container">
    <div class="col-lg-12">
        <h1 class="page-header"><?= Html::encode($this->title) ?></h1>
        <div class="btn-group" style="margin-bottom: 20px;">
            <a href="<?= Url::to(['admin-shop-catalog/add']) ?>" class="btn btn-default">Добавить</a>
        </div>
        <?= $this->render('_tree', ['rows' => \app\models\Shop\TreeNodeGeneral::getTree()]); ?>
    </div>
</div>