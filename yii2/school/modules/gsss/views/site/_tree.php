<?php

/**
 * params:
 * [
 *      'rows'     => [
 *                       [
 *                            'id'                 => '524'
 *                            'name'               => '123456'
 *                            ...
 *                            'selected'           => bool
 *                        ], ...
 *                    ]
 *     'formName'          => $this->model->formName(), // string
 *     'model'             => $this->model,             // \yii\base\Model
 *     'attrId'            => $this->attrId,            // attribute id
 *     'attrName'          => $this->attrName,          // attribute name
 *     'templateVariables' => []
 * ]
 */

/** @var $rows array */

?>



    <?php
    foreach ($rows as $item) { ?>
        <h3 class="page-header">
            <?= $item['name'] ?>
        </h3>

        <?php foreach($item['unions'] as $union) {?>
            <p><b><?= $union['name'] ?></b>: <a href="<?= $union['link']?>"><?= $union['link']?></a></p>
        <?php }?>

        <?php
            if (isset($item['nodes'])) {
                echo $this->render('_tree', [
                    'rows'              => $item['nodes'],
                ]);
            }
        ?>

    <?php } ?>
