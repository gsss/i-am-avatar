<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\ContactForm */

$this->title = 'Обратная связь';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="container">
    <h1 class="page-header text-center"><?= Html::encode($this->title) ?></h1>

    <?php if (Yii::$app->session->hasFlash('contactFormSubmitted')): ?>

    <div class="alert alert-success">
        Благодарим вас за обращение.
    </div>

    <?php else: ?>


    <div class="row">
        <div class="col-lg-6 col-lg-offset-3">
            <?php $form = ActiveForm::begin(['id' => 'contact-form']); ?>
                <?= $form->field($model, 'name', ['inputOptions' => ['class' => 'form-control', 'placeholder' => 'Ваше имя']])->label('Ваше имя', ['class' => 'hide']) ?>
                <?= $form->field($model, 'email', ['inputOptions' => ['class' => 'form-control', 'placeholder' => 'Email']])->label('Email', ['class' => 'hide']) ?>
                <?= $form->field($model, 'subject', ['inputOptions' => ['class' => 'form-control', 'placeholder' => 'Тема']])->label('Тема', ['class' => 'hide']) ?>
                <?= $form->field($model, 'body')->textArea(['rows' => 6])->label('Сообщение') ?>
                <?= $form->field($model, 'verifyCode')->widget(Captcha::className(), [
                    'options' => ['class' => 'form-control', 'placeholder' => 'Введите код указанный на картинке']
                ]) ?>
                <hr>
                <div class="form-group">
                    <?= Html::submitButton('Отправить', [
                        'class' => 'btn btn-primary',
                        'name' => 'contact-button',
                        'style' => 'width: 100%',
                    ]) ?>
                </div>
            <?php ActiveForm::end(); ?>
        </div>
    </div>

    <?php endif; ?>
</div>
