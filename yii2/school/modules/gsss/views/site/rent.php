<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;

/* @var $this yii\web\View */

$this->title = 'Сдаются помещения в аренду под проведение лекций, мастерклассов, конференций, презентаций.';
?>
<style xmlns="http://www.w3.org/1999/html">
    body {
        background: url('/images/new_earth/nmp/bg5.jpg') repeat-y center top;
    }

    .maxMargin {
        margin-top: 30px;
        margin-bottom: 30px;
    }
</style>

<div class="container">
    <div class="col-lg-12">
        <h1 class="page-header text-center">Сдаются помещения в аренду</h1>

        <p class="lead text-center">
            <img src="/images/site/rent/photo_2016-09-09_02-07-032.jpg" style="width: 100%; max-width: 800px;"/>
        </p>


        <p class="lead text-center">
            У нас для вас есть три помещения!
        </p>

        <p class="lead text-center">
            <img src="/images/site/rent/M8m.gif" style="width: 500px;"/>
        </p>

        <p class="lead text-center">
            Цели:<br>
            - Лекции<br>
            - Мастер-классы<br>
            - Конференции<br>
            - Презентации<br>
            - Уроки<br>
        </p>
        <p class="lead text-center">
            К вашим услугам:<br>
            - Wi-Fi<br>
            - Стулья<br>
            - Кофе-брейк<br>
            - Проектор<br>
            - Кондиционер<br>
            - Флип-чарт<br>
        </p>

        <h2 class="page-header text-center" style="margin-top: 50px;">
            25 м.кв.
        </h2>

        <div class="row">
            <div class="col-lg-4">
                <?= \app\assets\SlideShow\Core::item('/images/site/rent/1/1.JPG', '/images/site/rent/1/1b.JPG', ['img' => ['width' => '100%']]); ?>
            </div>
            <div class="col-lg-4">
                <?= \app\assets\SlideShow\Core::item('/images/site/rent/1/2.JPG', '/images/site/rent/1/2b.JPG', ['img' => ['width' => '100%']]); ?>
            </div>
            <div class="col-lg-4">
                <?= \app\assets\SlideShow\Core::item('/images/site/rent/1/3.JPG', '/images/site/rent/1/3b.JPG', ['img' => ['width' => '100%']]); ?>
            </div>
        </div>

        <h2 class="page-header text-center" style="margin-top: 50px;">
            30 м.кв.
        </h2>

        <div class="row">
            <div class="col-lg-4">
                <?= \app\assets\SlideShow\Core::item('/images/site/rent/2/1.JPG', '/images/site/rent/2/1b.JPG', ['img' => ['width' => '100%']]); ?>
            </div>
            <div class="col-lg-4">
                <?= \app\assets\SlideShow\Core::item('/images/site/rent/2/2.JPG', '/images/site/rent/2/2b.JPG', ['img' => ['width' => '100%']]); ?>
            </div>
            <div class="col-lg-4">
                <?= \app\assets\SlideShow\Core::item('/images/site/rent/2/3.JPG', '/images/site/rent/2/3b.JPG', ['img' => ['width' => '100%']]); ?>
            </div>
        </div>

        <h2 class="page-header text-center" style="margin-top: 50px;">
            48 м.кв.
        </h2>

        <div class="row">
            <div class="col-lg-4 col-lg-offset-2">
                <?= \app\assets\SlideShow\Core::item('/images/site/rent/3/1.JPG', '/images/site/rent/3/1b.JPG', ['img' => ['width' => '100%']]); ?>
            </div>
            <div class="col-lg-4">
                <?= \app\assets\SlideShow\Core::item('/images/site/rent/3/2.JPG', '/images/site/rent/3/2b.JPG', ['img' => ['width' => '100%']]); ?>
            </div>
        </div>

        <h2 class="page-header text-center" style="margin-top: 50px;">
            Дизайн
        </h2>

        <div class="row">
            <div class="col-lg-4">
                <?= \app\assets\SlideShow\Core::item('/images/site/rent/4/8.JPG', '/images/site/rent/4/8b.JPG', ['img' => ['width' => '100%']]); ?>
            </div>
            <div class="col-lg-4">
                <?= \app\assets\SlideShow\Core::item('/images/site/rent/4/2.JPG', '/images/site/rent/4/2b.JPG', ['img' => ['width' => '100%']]); ?>
            </div>
            <div class="col-lg-4">
                <?= \app\assets\SlideShow\Core::item('/images/site/rent/4/3.JPG', '/images/site/rent/4/3b.JPG', ['img' => ['width' => '100%']]); ?>
            </div>
        </div>
        <p>&nbsp;</p>
        <div class="row">
            <div class="col-lg-4 col-lg-offset-2">
                <?= \app\assets\SlideShow\Core::item('/images/site/rent/4/4.JPG', '/images/site/rent/4/4b.JPG', ['img' => ['width' => '100%']]); ?>
            </div>
            <div class="col-lg-4">
                <?= \app\assets\SlideShow\Core::item('/images/site/rent/4/1.JPG', '/images/site/rent/4/1b.JPG', ['img' => ['width' => '100%']]); ?>
            </div>
        </div>
        <p>&nbsp;</p>
        <div class="row">
            <div class="col-lg-4 col-lg-offset-2">
                <?= \app\assets\SlideShow\Core::item('/images/site/rent/4/5.JPG', '/images/site/rent/4/5b.JPG', ['img' => ['width' => '100%']]); ?>
            </div>
            <div class="col-lg-4">
                <?= \app\assets\SlideShow\Core::item('/images/site/rent/4/9.JPG', '/images/site/rent/4/9b.JPG', ['img' => ['width' => '100%']]); ?>
            </div>
        </div>

        <h2 class="page-header text-center" style="margin-top: 50px;">
            Цена
        </h2>
        <p class="lead text-center">
            800 руб. час до 18 часов<br>1200 руб. час с 18 до 22 часов
        </p>

        <h2 class="page-header text-center" style="margin-top: 50px;">
            Расположение
        </h2>

        <div class="row">
            <div class="row">
                <div class="col-lg-12">
                <center>
                    <script type="text/javascript" charset="utf-8" async src="https://api-maps.yandex.ru/services/constructor/1.0/js/?sid=rtn570sy5HirMXoyAEtVtexJsGwFx09v&amp;width=800&amp;height=478&amp;lang=ru_RU&amp;sourceType=constructor&amp;scroll=true"></script>
                </center>
            </div>
            </div>
            <p class="lead text-center" style="margin-top: 20px;">Москва, Большая Полянка 51а/9, 5 этаж, м. Полянка, м. Добрынинская</p>
            <div class="row">
                <div class="col-lg-8 col-lg-offset-2">
                    <img src="/images/site/rent/2016-09-09_01-49-09.png" class="thumbnail" width="100%"/>
                </div>
            </div>

        </div>


        <h2 class="page-header text-center" style="margin-top: 50px;">
            Контакты
        </h2>

        <p class="lead text-center">
            +7-916-236-07-57
        </p>

        <center>
            <?= $this->render('../blocks/share', [
                'image'       => \yii\helpers\Url::to('/images/site/rent/photo_2016-09-09_02-07-032.jpg', true),
                'url'         => \yii\helpers\Url::current([], true),
                'title'       => 'Академия Личности. Сдаются помещения в аренду',
                'description' => 'Большая Полянка 51а/9. Три помещения для занятий',
            ]) ?>
        </center>
    </div>
</div>
