<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\ContactForm */

$this->title = 'Ключ к Сердцу';
?>
<div class="container">
    <div class="col-lg-8 col-lg-offset-2">
        <h1 class="page-header text-center">Ключ к Сердцу</h1>

        <h2 class="page-header text-center">Активатор №1</h2>
        <h3 class="text-center">Задача Аватара Свободы</h3>
        <p><iframe width="100%" height="315" src="https://www.youtube.com/embed/u-b_I_UWSIM" frameborder="0" allowfullscreen></iframe></p>
        <h2 class="page-header text-center">Активатор №2</h2>
        <h3 class="text-center">Исцеление отношений с родителями </h3>
        <p><iframe width="100%" height="315" src="https://www.youtube.com/embed/iU6GR2X8PjA" frameborder="0" allowfullscreen></iframe></p>
        <h2 class="page-header text-center">Активатор №3</h2>
        <h3 class="text-center">Активация Двенадцатичакровой Системы</h3>
        <p><iframe width="100%" height="450" scrolling="no" frameborder="no" src="https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/tracks/259017229&amp;auto_play=false&amp;hide_related=false&amp;show_comments=true&amp;show_user=true&amp;show_reposts=false&amp;visual=true"></iframe></p>
        <h2 class="page-header text-center">Активатор №4</h2>
        <h3 class="text-center">Меркаба продвинутая</h3>
        <p><iframe width="100%" height="315" src="https://www.youtube.com/embed/dzCRSwHUi50" frameborder="0" allowfullscreen></iframe></p>
    </div>
</div>
