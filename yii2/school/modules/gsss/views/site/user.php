<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;
use yii\helpers\ArrayHelper;
use \yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $user \app\models\User */

$name = $user->getName2();

$this->title = $name;
?>
<div class="container">
<div class="col-lg-12">
<h1 class="page-header text-center">
    Карта Жизни Гражданина Галактики
    <small>
        <span class="glyphicon glyphicon-question-sign buttonInfo" role="button"></span>
    </small>
</h1>

<?php $this->registerJs('$(".buttonInfo").click(function() { $("#infoModal").modal("show"); } )'); ?>
<!-- Modal -->
<div class="modal fade" id="infoModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                        aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Карта Жизни Гражданина Галактики</h4>
            </div>
            <div class="modal-body">
                <p>
                    Гороскоп - карта, или изображение небес, составленная для даты, времени и места вашего рождения.
                    Положение Солнца, Луны, планет, а также знак над горизонтом размещаются по кольцу Зодиака и
                    раскрывают сложные математические взаимосвязи, рисующие вашу персональную схему и потенциал
                    развития. Эта карта раскрывает ваши физические, ментальные, эмоциональные и духовные способности и
                    затруднения, и вы всегда вольны расти и меняться по своему собственному желанию.
                </p>

                <p>
                    Примечательны также точки пересечения траекторий Земли и Луны, образующие так называемые голову и
                    хвост небесного дракона, или северный и южный узлы лунной орбиты. Расположение «небесного дракона»
                    на карте чрезвычайно значимо, поскольку оно указывает направление, в котором вы движетесь, достигая
                    исполнения предначертанной вам цели, а также то место в прошлом, где вы изначально возникли.
                    Поскольку вы родились в физической реальности, ваша жизнь несет на себе отпечаток космической
                    энергии, воплощающей план ваших целей и намерений, план, составленный и утвержденный вами лично.
                </p>

                <p>
                    В анналах творения Земля рассматривается как монументальный архив точной информации, живая
                    библиотека, хранящая множество слоев генетических кодов ее органической жизни. Ваша наука с большим
                    энтузиазмом стремится разгадать генетические коды жизни, не понимая, где и как возник этот
                    великолепный порядок. Вы - наследники звезд.
                </p>

                <p>
                    Как часто ваш взгляд устремлялся в ночное небо? Сколько вопросов и молитв вознесли вы к небесному
                    своду? Сколько раз вы пересчитывали звезды или размышляли об энергии Солнца? Ваши глубочайшие
                    внутренние желания - это закономерные вибрации воспоминаний, стремящихся разгореться и пробудиться,
                    вновь стать едиными с непрерывной целью, охватывающей весь Космос. Вы пришли на Землю, чтобы
                    закрепить это древнее знание в человеческом облике, зная, что жизнь здесь преподносит столько
                    возможностей и даров, сколько вы способны выбрать, чтобы пережить их.
                </p>

                <p>
                    <i>Путь силы. Мудрость плеяд для мира в хаосе</i>
                </p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
            </div>
        </div>
    </div>
</div>


<h2 class="page-header text-center"><?= $user->getName2() ?>
    <?php if (!\Yii::$app->user->isGuest) { ?>
        <?php if (\Yii::$app->user->id == $user->getId()) { ?>
            <a href="<?= \yii\helpers\Url::to(['cabinet/profile']) ?>" class="btn btn-default btn-sm"><span
                    class="glyphicon glyphicon-edit"></span> Редактировать</a>
        <?php } ?>
    <?php } ?>
</h2>

<div class="row">
    <div class="col-lg-12">
        <p class="text-center">
            <img src="<?= $user->getAvatar() ?>" class="img-circle" style="width: 150px;"/>
        </p>
    </div>
</div>
<div class="row">
    <div class="col-lg-12">
        <p class="text-center">
            <img src="https://www.galaxysss.com/images/site/human_design/keys/DDm0Hhbe35w.jpg" class="img-circle" style="width: 500px;"/>
        </p>
    </div>
</div>

<div class="row">
<div class="col-lg-8 col-lg-offset-2">

<div class="panel panel-default">
    <div class="panel-heading"><?= (is_null($user->get('gender'))? 'Пришел' : (($user->get('gender') == 1)? 'Пришел' : 'Пришла')) ?> на планету Земля</div>
    <div class="panel-body">
        <?php if ($user->getField('birth_date')) { ?>
            <div><b>Дата</b>: <?= \Yii::$app->formatter->asDate($user->getField('birth_date')) ?></div>
        <?php } ?>
        <?php if ($user->hasBirthPlace()) { ?>
            <div><b>Место</b>: <?= $user->getBirthPlace() ?></div>
        <?php } ?>
    </div>
</div>


<div class="panel panel-default">
    <?php $humanDesign = $user->getHumanDesign(); ?>
    <?php $humanDesign = false; ?>
    <div class="panel-heading">Дизайн Человека
        <?php if ($humanDesign) { ?>
            <?php if (!\Yii::$app->user->isGuest) { ?>
                <?php if (\Yii::$app->user->id == $user->getId()) {
                    $this->registerJs(<<<JS
$('.buttonDelete').click(function(){
    ajaxJson({
            url: '/cabinet/profile/humanDesign/delete',
            success: function(){
                window.location = '/cabinet/profile/humanDesign'
            }
        })
    }
);
JS
                    );
                    ?>
                    <div class="btn-group pull-right">
                        <button type="button" class="btn btn-default btn-xs buttonDelete"
                        ">
                        Удалить и пересчитать
                        </button>
                    </div>
                <?php } ?>
            <?php } ?>
        <?php } ?>
    </div>
    <div class="panel-body">
        <?php if ($humanDesign) { ?>
            <img src="<?= $humanDesign->getImage() ?>" style="width: 100%;">
            <table class="table table-striped table-hover" style="width: auto;" align="center">
                <tr>
                    <td>Тип</td>
                    <td>
                        <a href="<?= \app\modules\HumanDesign\calculate\YourHumanDesignRu::$links['type'][ $humanDesign->type->href ] ?>">
                            <?= $humanDesign->type->text ?>
                        </a>
                    </td>
                </tr>
                <tr>
                    <td>Профиль</td>
                    <td>
                        <a href="<?= \app\modules\HumanDesign\calculate\YourHumanDesignRu::$links['profile'][ $humanDesign->profile->href ] ?>">
                            <?= $humanDesign->profile->text ?>
                        </a>
                    </td>
                </tr>
                <tr>
                    <td>Определение</td>
                    <td><?= $humanDesign->definition->text ?></td>
                </tr>
                <tr>
                    <td>Внутренний Авторитет</td>
                    <td><?= $humanDesign->inner->text ?></td>
                </tr>
                <tr>
                    <td>Стратегия</td>
                    <td><?= $humanDesign->strategy->text ?></td>
                </tr>
                <tr>
                    <td>Тема ложного Я</td>
                    <td><?= $humanDesign->theme->text ?></td>
                </tr>
                <tr>
                    <td>Подпись</td>
                    <td><?= (is_null($humanDesign->signature)) ? '' : $humanDesign->signature->text ?></td>
                </tr>
                <tr>
                    <td>Инкарнационный крест</td>
                    <?php
                    $text = $humanDesign->cross->text;
                    $arr = explode('(', $text);
                    $text = trim($arr[0]);
                    $isError = false;
                    if (count($arr) == 1) {
                        \cs\Application::mail('admin@galaxysss.ru', 'Ошибка', 'admin', [
                            'text' => '/galaxysss/views/site/user.php:194 humanDesign' . \yii\helpers\VarDumper::dumpAsString([$arr, $humanDesign, $user->getFields()])
                        ]);
                        $isError = true;
                    } else {
                        $arr = explode(')', $arr[1]);
                        $arr = explode('|', $arr[0]);
                        $new = [];
                        foreach ($arr as $i) {
                            $a = explode('/', $i);
                            $new[] = trim($a[0]);
                            $new[] = trim($a[1]);
                        }
                        $route = 'human_design/gen_keys_item';
                        $a1 = Html::a($new[0], [$route, 'id' => $new[0]]);
                        $a2 = Html::a($new[1], [$route, 'id' => $new[1]]);
                        $a3 = Html::a($new[2], [$route, 'id' => $new[2]]);
                        $a4 = Html::a($new[3], [$route, 'id' => $new[3]]);
                    }
                    ?>
                    <td><?= $text ?>
                        <?php if (!$isError) { ?>
                            (<?= $a1 ?>/<?= $a2 ?> | <?= $a3 ?>/<?= $a4 ?>)
                        <?php } ?>
                    </td>
                </tr>
            </table>
            <h3>Генные ключи</h3>
            <table class="table table-striped table-hover">
                <tr>
                    <td>
                        #
                    </td>
                    <td>
                        Название
                    </td>
                    <td>
                        Тень
                    </td>
                    <td>
                        Дар
                    </td>
                    <td>
                        Сиддхи
                    </td>
                </tr>
                <?php foreach ($new as $i) { ?>
                    <?php  $genKey = \app\models\HdGenKeys::find(['num' => $i]) ?>
                    <tr data-num="<?= $genKey->getField('num') ?>" role="button">
                        <td>
                            <?= $genKey->getField('num') ?>
                        </td>
                        <td>
                            <a href="<?= Url::to(['human_design/gen_keys_item', 'id' => $genKey->getField('num')])?>">
                                <?= $genKey->getField('name') ?>
                            </a>
                        </td>
                        <td>
                            <?= $genKey->getField('ten') ?>
                        </td>
                        <td>
                            <?= $genKey->getField('dar') ?>
                        </td>
                        <td>
                            <?= $genKey->getField('siddhi') ?>
                        </td>
                    </tr>
                <?php } ?>
            </table>
        <?php } else { ?>
            <?php if (!\Yii::$app->user->isGuest) { ?>
                <?php if (\Yii::$app->user->id == $user->getId()) { ?>
                    <a class="btn btn-primary"
                       href="<?= \yii\helpers\Url::to(['cabinet/profile_human_design']) ?>">
                        Расчитать
                    </a>
                <?php } else { ?>
                    <p class="alert alert-success">Нет</p>
                <?php } ?>
            <?php } else { ?>
                <p class="alert alert-success">Нет</p>
            <?php } ?>
        <?php } ?>
    </div>
</div>

<div class="panel panel-default">
    <div class="panel-heading">Древо Рода</div>
    <div class="panel-body">
        <div class="row col-lg-12">
            <?php
            $gender = $user->getGender();
            ?>
            <?php if (is_null($gender)) { ?>
                <p class="alert alert-danger">Пол не выбран</p>
                <?php $gender = 1; ?>
            <?php } ?>
            <?php
            $url = "/images/passport/tree-{$gender}.png";
            ?>
            <img src="<?= $url ?>" usemap="#Map"/>
            <?php

            function getName($point)
            {
                if (isset($point[3])) {
                    $user = $point[3];
                    $arr = [];
                    $index = 'name_first';
                    if (ArrayHelper::getValue($user, $index, '') != '') {
                        $arr[] = $user[ $index ];
                    }
                    $index = 'name_middle';
                    if (ArrayHelper::getValue($user, $index, '') != '') {
                        $arr[] = $user[ $index ];
                    }
                    $index = 'name_last';
                    if (ArrayHelper::getValue($user, $index, '') != '') {
                        $arr[] = $user[ $index ];
                    }
                    if (ArrayHelper::getValue($user, 'date_born', '') != '') {
                        if (ArrayHelper::getValue($user, 'date_death', '') != '') {
                            $start = Yii::$app->formatter->asDate(ArrayHelper::getValue($user, 'date_born', ''));
                            $end = Yii::$app->formatter->asDate(ArrayHelper::getValue($user, 'date_death', ''));
                            $arr[] = "({$start} - {$end})";
                        } else {
                            $start = Yii::$app->formatter->asDate(ArrayHelper::getValue($user, 'date_born', ''));
                            $arr[] = "({$start})";
                        }
                    }

                    return join(' ', $arr);
                } else {
                    return '';
                }
            }

            /**
             * @var array $points
             * [
             *    x,
             *    y,
             *    radius,
             * ]
             */
            $points = [
                1 => [128, 107, 25],
                2 => [128 + 256, 107, 25],
            ];
            for ($i = 0; $i < 4; $i++) {
                $points[] = [64 + (128 * $i), 155, 16];
            }
            for ($i = 0; $i < 8; $i++) {
                $points[] = [32 + (64 * $i), 195, 2];
            }
            for ($i = 0; $i < 16; $i++) {
                $points[] = [16 + (32 * $i), 204, 2];
            }
            for ($i = 0; $i < 32; $i++) {
                $points[] = [8 + (16 * $i), 213, 2];
            }
            for ($i = 0; $i < 64; $i++) {
                $points[] = [4 + (8 * $i), 221, 2];
            }

            $rod = \app\models\UserRod::query(['user_id' => $user->getId()])->all();
            foreach ($rod as $i) {
                $points[ $i['rod_id'] ][] = $i;
            }
            ?>
            <map name="Map">
                <?php foreach ($points as $key => $point) { ?>
                    <area
                        class="rectTitle"
                        shape="rect"
                        title="<?= getName($points[ $key ]) ?>"
                        coords="<?= $point[0] - $point[2] ?>,<?= $point[1] - $point[2] ?>,<?= $point[0] + $point[2] ?>,<?= $point[1] + $point[2] ?>"
                        href="<?= \yii\helpers\Url::to(['site/user_rod', 'rod_id' => $key, 'user_id' => $user->getId()]) ?>"
                        />
                <?php } ?>
            </map>

        </div>
        <div class="row col-lg-12">
            <a href="<?= \yii\helpers\Url::to(['site/user_rod_list', 'id' => $user->getId()]) ?>"
               class="btn btn-default">
                Весь род списком
            </a>

            <!-- Button trigger modal -->
            <button type="button" class="btn btn-success" data-toggle="modal" data-target="#myModal">
                Очистить Род
            </button>

            <!-- Modal -->
            <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                    aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title" id="myModalLabel">Отмаливание Рода</h4>
                        </div>
                        <div class="modal-body">
                            <p><img class="thumbnail"
                                    src="/upload/FileUpload2/gs_article_list/00000142/original/image.jpg"
                                    alt="Отмаливание Рода" style="width:100%;"></p>

                            <p><em><strong>Если вы обнаружили повторяющуюся родовую проблему и приняли решение серьезно
                                        заняться ею, то вот вам несколько советов.</strong></em></p>

                            <p>Издревле считалось, что молиться за нас, рабов Божьих, и отмаливать наших грешных предков
                                сподручнее всего служителям монастырей. Многие соборы Санкт-Петербурга принимают записки
                                на полугодовое и годичное поминание о здравии или об упокоении в монастырях
                                Ленинградской, Псковской, Новгородской областей.</p>

                            <p>Поэтому вам необходимо составить список своих родственников, членов своего прямого рода,
                                учитывая всех от первого до седьмого колена. Братья, сестры, дяди и тети в этот список
                                не входят.</p>

                            <p>Записать необходимо следующие имена:</p>

                            <p>вы – это первое колено,</p>

                            <p>ваши отец и мать – это второе колено,</p>

                            <p>ваши бабушки и дедушки – это третье колено,</p>

                            <p>ваши прадедушки и прабабушки – это четвертое колено и так далее.</p>

                            <p>Записывайте тех, чьи имена вам известны, причем заносите их в два столбика: те, кто живы,
                                и те, кого уже на этом свете нет. Ведь за одних надо подать поминание о здравии, а за
                                других – об упокоении.</p>

                            <p>Но работой монахов отмаливание вашего рода на этом не заканчивается. Непосредственное
                                ваше участие в этом тоже велико. Есть три чудодейственные молитвы, их можно найти в
                                любом молитвослове.</p>

                            <p><strong>Первая – это 90-й псалом</strong>, смысловые и звуковые вибрации которого помогут
                                почистить энергетическую структуру человека.</p>

                            <p><strong>Вторая – 50-й псалом</strong>. Она очень действенна при защите биополя и
                                окружающего пространства личности.</p>

                            <p><strong>И третья – Символ веры</strong>, при которой идет быстрое наполнение
                                высокочастотной энергией всех центров и каналов Души.</p>

                            <p>Эти молитвы необходимо прочитать за каждого члена вашего рода в определенной
                                последовательности.</p>

                            <p>— Начинать нужно с себя.</p>

                            <p>— Затем вы читаете за мать, затем – за отца.</p>

                            <p>— Переходя к третьему колену, читаете за бабушку и за дедушку по материнской линии, потом
                                за бабушку и за дедушку по отцовской линии.</p>

                            <p>— Работая с четвертым коленом, начинаете читать за прабабушку и за прадедушку – родителей
                                бабушки, потом – за прабабушку и прадедушку – родителей дедушки (это работа с предками
                                по женской линии).</p>

                            <p>— Точно так же работаете с предками по мужской линии: сначала читаете молитвы за
                                прабабушку и прадедушку – родителей бабушки, потом за прабабушку и прадедушку –
                                родителей дедушки. И так далее.</p>

                            <p><strong><em>Необходимо помнить, что при продвижении вглубь по родовому каналу, энергия
                                        молитв закручивается по часовой стрелке по правилу буравчика (слева направо).
                                        Женская часть родового канала располагается слева, а мужская –
                                        справа.</em></strong><br>
                                <br>
                                Итак, вы начинаете чтение молитв за себя.</p>

                            <p>— После третьей молитвы произнесите слова: “<em>Прошу прощения у всех, кому чаяно и
                                    нечаянно принесла зло”.</em><br>
                                — Работу с каждым из предков начинайте, например, с таких слов:&nbsp;<em>“Отдаю свой
                                    голос прадеду по материнской линии раба Божьего Терентия”.</em><br>
                                <br>
                                — Потом читаете молитвы, а в конце просите прощение за предка у всех тех, кому он принес
                                при жизни зло. Если имя вашего предка неизвестно, то назовите просто его статус по
                                вашему роду. Удобнее составить схему всех родственников до седьмого колена.</p>

                            <p>— Понятно, что такая работа займет много времени и за один подход ее не выполнить. У
                                кого-то на нее уйдет несколько дней, это не важно, главное, что проделанная вами работа
                                даст бесценный результат – ваш род будет очищен от жестких, низкочастотных,
                                разрушительных структур.</p>

                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>

<div class="panel panel-default">
<?php $birthDate = $user->getField('birth_date'); ?>

<div class="panel-heading">Персональная Галактическая Печать</div>
<div class="panel-body">
<?php if ($birthDate) { ?>
    <div class="col-lg-4" id="orakul-div">
        <?php
        $mayaAssetUrl = Yii::$app->assetManager->getBundle('app\assets\Maya\Asset')->baseUrl;
        $maya = \cs\models\Calendar\Maya::calc($birthDate);
        $analog = 19 - (($maya['stamp'] == 20) ? 0 : $maya['stamp']);
        $analog = ($analog == 0) ? 20 : $analog;
        $antipod = (($maya['stamp'] == 20) ? 0 : $maya['stamp']);
        $antipod = (int)($antipod) + (int)((($antipod > 10) ? -1 : 1) * 10);
        // Ведущий учитель
        {
            $vedun = 0;

            switch ($maya['ton'] % 5) {
                case 0:
                    // + 8 печатей
                    $vedun = $maya['stamp'] + 8;
                    if ($vedun > 20) {
                        $vedun = $vedun - 20;
                    }
                    break;
                case 1:
                    // та же печать
                    $vedun = $maya['stamp'];
                    break;
                case 2:
                    // - 8 печатей
                    $vedun = $maya['stamp'] - 8;
                    if ($vedun <= 0) {
                        $vedun = 20 + $vedun;
                    }
                    break;
                case 3:
                    // + 4 печати
                    $vedun = $maya['stamp'] + 4;
                    if ($vedun > 20) {
                        $vedun = $vedun - 20;
                    }
                    break;
                case 4:
                    // - 4 печати
                    $vedun = $maya['stamp'] - 4;
                    if ($vedun <= 0) {
                        $vedun = 20 + $vedun;
                    }
                    break;
            }
        }

        // Оккультный учитель
        {
            $okkult = 21 - $maya['stamp'];
            $okkultTon = 14 - $maya['ton'];
        }
        ?>
        <style>
            .oracul .item {
                padding: 0px 10px 10px 10px;
                text-align: center;
            }

            .glyphicon-question-sign {
                opacity: 0.3;
            }
        </style>
        <table id="orakul-table" class="oracul">
            <tr>
                <td></td>
                <td id="vedun" class="item">
                    <img src="<?= $mayaAssetUrl ?>/images/ton/<?= $maya['ton'] ?>.gif" alt="" width="20"
                         class="ton"><br>
                    <a class="popup-with-zoom-anim" href="#small-dialog">
                        <img src="<?= $mayaAssetUrl ?>/images/stamp3/<?= $vedun ?>.gif" alt="" class="stamp"
                             data-stamp="<?= $vedun ?>"
                             title="<?= \cs\models\Calendar\Maya::$stampRows[ $vedun - 1 ][0] ?>">
                    </a>
                </td>
                <td></td>
            </tr>
            <tr>
                <td id="antipod" class="item">
                    <img src="<?= $mayaAssetUrl ?>/images/ton/<?= $maya['ton'] ?>.gif" alt="" width="20"
                         class="ton"><br>
                    <a class="popup-with-zoom-anim" href="#small-dialog">
                        <img src="<?= $mayaAssetUrl ?>/images/stamp3/<?= $antipod ?>.gif" alt="" class="stamp"
                             data-stamp="<?= $antipod ?>"
                             title="<?= \cs\models\Calendar\Maya::$stampRows[ $antipod - 1 ][0] ?>">
                    </a>
                </td>
                <td id="today" class="item">
                    <img src="<?= $mayaAssetUrl ?>/images/ton/<?= $maya['ton'] ?>.gif" alt="" width="20"
                         class="ton"><br>
                    <a class="popup-with-zoom-anim" href="#small-dialog">
                        <img
                            src="<?= $mayaAssetUrl ?>/images/stamp3/<?= $maya['stamp'] ?>.gif"
                            alt=""
                            class="stamp"
                            data-stamp="<?= $maya['stamp'] ?>"
                            title="<?= \cs\models\Calendar\Maya::$stampRows[ $maya['stamp'] - 1 ][0] ?>"
                            data-date="<?= $birthDate ?>"
                            >
                    </a>
                </td>
                <td id="analog" class="item">
                    <img src="<?= $mayaAssetUrl ?>/images/ton/<?= $maya['ton'] ?>.gif" alt="" width="20"
                         class="ton"><br>
                    <a class="popup-with-zoom-anim" href="#small-dialog">
                        <img
                            src="<?= $mayaAssetUrl ?>/images/stamp3/<?= $analog ?>.gif"
                            alt=""
                            class="stamp"
                            data-stamp="<?= $analog ?>"
                            title="<?= \cs\models\Calendar\Maya::$stampRows[ $analog - 1 ][0] ?>"
                            >
                    </a>
                </td>
            </tr>
            <tr>
                <td></td>
                <td id="okkult" class="item">
                    <img src="<?= $mayaAssetUrl ?>/images/ton/<?= $okkultTon ?>.gif" alt="" width="20"
                         class="ton"><br>
                    <a class="popup-with-zoom-anim" href="#small-dialog">
                        <img
                            src="<?= $mayaAssetUrl ?>/images/stamp3/<?= $okkult ?>.gif"
                            alt=""
                            data-stamp="<?= $okkult ?>"
                            class="stamp"
                            title="<?= \cs\models\Calendar\Maya::$stampRows[ $okkult - 1 ][0] ?>"
                            >
                    </a>
                </td>
                <td></td>
            </tr>
        </table>
    </div>
    <div class="col-lg-8">
        <?php
        $path = $this->registerAssetBundle('app\assets\Maya\Asset')->baseUrl;
        $this->registerJs(<<<JS
    var magnificPopupOptions = {
        type: 'inline',

        fixedContentPos: false,
        fixedBgPos: true,

        overflowY: 'auto',

        closeBtnInside: true,
        preloader: false,

        midClick: true,
        removalDelay: 300,
        mainClass: 'my-mfp-zoom-in',

        callbacks: {
            beforeOpen: function(e,i) {
            var thisDayLink = this.items[this.index];
            var thisDayImg = $(thisDayLink).find('img')[0];
            thisDayImg = $(thisDayImg);
            thisDayImg.tooltip('hide');
            console.log(thisDayImg);
            var stamp = thisDayImg.data('stamp');
            var modalDialog = $('#small-dialog');
            modalDialog.html('');
            modalDialog.append($('<h1>', {
                class: 'page-header'
            }).html(GSSS.calendar.maya.stampList[stamp-1][0]));
            modalDialog.append(
                $('<p>').append(
                    $('<img>', {
                        src: '{$path}/images/stamp3/' + stamp + '.gif'
                    })
                )
            );
            modalDialog.append($('<p>').html(GSSS.calendar.maya.stampList[stamp-1][1]));
            modalDialog.append($('<p>').html(GSSS.calendar.maya.stampList[stamp-1][2]));
            modalDialog.append($('<p>').html(GSSS.calendar.maya.stampList[stamp-1][3]));
            modalDialog.append($('<p>').html(GSSS.calendar.maya.stampList[stamp-1][4]));
            modalDialog.append($('<p>').html(GSSS.calendar.maya.stampList[stamp-1][5]));
            modalDialog.append($('<p>').html(GSSS.calendar.maya.stampList[stamp-1][6]));
            modalDialog.append($('<p>').html(GSSS.calendar.maya.stampList[stamp-1][7]));

            }
        }
    };
    $('.popup-with-zoom-anim').magnificPopup(magnificPopupOptions);
    $('img.stamp').tooltip();
JS
        );
        ?>
        <div id="small-dialog" class="zoom-anim-dialog mfp-hide">
            <h1>Dialog example</h1>

            <p>This is dummy copy. It is not meant to be read. It has been placed here solely to demonstrate the look
                and feel of finished, typeset text. Only for show. He who searches for meaning here will be sorely
                disappointed.</p>
        </div>

        <table class="table table-striped table-hover">
            <tr>
                <?php $this->registerJs(<<<JS
        $('.glyphicon-question-sign').popover({
            trigger: 'focus',
            placement: 'right'
        });
JS
                )?>
                <td><span
                        tabindex="0"
                        class="glyphicon glyphicon-question-sign"
                        role="button"
                        data-content="Кин Судьбы, который состоит из тона и печати. Это основная энергия, которая дана человеку от рождения."
                        data-title="Кин"
                        ></span>
                </td>
                <td>Кин</td>
                <td><?= $maya['kin'] ?></td>
            </tr>
            <tr>
                <td><span tabindex="0" class="glyphicon glyphicon-question-sign" role="button"
                          data-title="ПГА"
                          data-content="Портал Галактической Активации означает, что этот Кин - день или человек имеет прямую связь с духом и космосом."></span>
                </td>
                <td>ПГА</td>
                <td><?= $maya['nearPortal'] == 0 ? 'Да' : 'Нет' ?></td>
            </tr>
            <tr>
                <td><span tabindex="0" class="glyphicon glyphicon-question-sign" role="button"
                          data-title="Главная печать"
                          data-content="Персональная Галактическая Печать, которая определяет свойства человека, рожденного в этот день. Эта энергия остается с человеком на всю жизнь."></span>
                </td>
                <td>Главная печать</td>
                <td><?= \cs\models\Calendar\Maya::$stampRows[ $maya['stamp'] - 1 ][0] ?></td>
            </tr>
            <tr>
                <td><span tabindex="0" class="glyphicon glyphicon-question-sign" role="button"
                          data-title="Ведущая печать"
                          data-content="результирующая сила, дающая обертон и движение"></span></td>
                <td>Ведущая печать</td>
                <td><?= \cs\models\Calendar\Maya::$stampRows[ $vedun - 1 ][0] ?></td>
            </tr>
            <tr>
                <td><span tabindex="0" class="glyphicon glyphicon-question-sign" role="button"
                          data-title="Аналог"
                          data-content="поддерживающая, питающая сила, планетарный партнер"></span></td>
                <td>Аналог</td>
                <td><?= \cs\models\Calendar\Maya::$stampRows[ $analog - 1 ][0] ?></td>
            </tr>
            <tr>
                <td><span tabindex="0" class="glyphicon glyphicon-question-sign" role="button"
                          data-title="Антипод"
                          data-content="сила вызова и испытания, балансирующая сила"></span></td>
                <td>Антипод</td>
                <td><?= \cs\models\Calendar\Maya::$stampRows[ $antipod - 1 ][0] ?></td>
            </tr>
            <tr>
                <td><span tabindex="0" class="glyphicon glyphicon-question-sign" role="button"
                          data-title="Оккультный учитель"
                          data-content="скрытая сила, незримая духовная поддержка"></span></td>
                <td>Оккультный учитель</td>
                <td><?= \cs\models\Calendar\Maya::$stampRows[ $okkult - 1 ][0] ?></td>
            </tr>
        </table>
    </div>
<?php } else { ?>
    <?php if (!Yii::$app->user->isGuest) { ?>
        <?php if (Yii::$app->user->id == $user->getId()) { ?>
            <a href="<?= \yii\helpers\Url::to(['cabinet/profile_time']) ?>" class="btn btn-primary">Расчитать</a>
        <?php } else { ?>
            <p class="alert alert-success">Нет данных</p>
        <?php } ?>
    <?php } else { ?>
        <p class="alert alert-success">Нет данных</p>
    <?php } ?>
<?php } ?>
</div>
</div>

<div class="panel panel-default">
    <div class="panel-heading">Звездное происхождение
        <?php if ($user->hasZvezdnoe()) { ?>
            <?php if (!Yii::$app->user->isGuest) { ?>
                <?php if (Yii::$app->user->id == $user->getId()) { ?>
                    <div class="btn-group pull-right">
                        <a
                            type="button"
                            class="btn btn-default btn-xs buttonDelete"
                            href="<?= \yii\helpers\Url::to(['cabinet/profile_zvezdnoe']) ?>"
                        ">
                        редактировать
                        </a>
                    </div>
                <?php } ?>
            <?php } ?>
        <?php } ?>
    </div>
    <div class="panel-body">
        <?php if ($user->hasZvezdnoe()) { ?>
            <?php $z = $user->getZvezdnoe(); ?>
            <div><?= nl2br(Html::encode($z->data)) ?></div>

        <?php } else { ?>
            <?php if (!Yii::$app->user->isGuest) { ?>
                <?php if (Yii::$app->user->id == $user->getId()) { ?>
                    <!-- я смотрю себя-->
                    <a href="https://vk.com/id313579738" target="_blank" class="btn btn-primary">Узнать</a>
                    <a href="<?= \yii\helpers\Url::to(['cabinet/profile_zvezdnoe']) ?>" class="btn btn-primary">Заполнить</a>
                <?php } else { ?>
                    <!-- авторизованный пользователь смотрит этот профиль -->
                    <p class="alert alert-success">Нет данных</p>
                <?php } ?>
            <?php } else { ?>
                <!-- гость смотрит этот профиль -->
                <p class="alert alert-success">Нет данных</p>
            <?php } ?>
        <?php } ?>
    </div>
</div>

<div class="panel panel-default">
    <div class="panel-heading">Миссия</div>
    <div class="panel-body">
        <?php if ($user->getField('mission')) { ?>
            <?= nl2br(Html::encode($user->getField('mission'))); ?>
        <?php } else { ?>
            <?php if (!Yii::$app->user->isGuest) { ?>
                <?php if (Yii::$app->user->id == $user->getId()) { ?>
                    <a href="<?= \yii\helpers\Url::to(['cabinet/profile']) ?>" class="btn btn-default">Заполнить</a>
                <?php } else { ?>
                    <p class="alert alert-success">Нет данных</p>
                <?php } ?>
            <?php } else { ?>
                <p class="alert alert-success">Нет данных</p>
            <?php } ?>
        <?php } ?>
    </div>
</div>

</div>

</div>

<hr>
<?php
$arr = [];
if ($user->hasHumanDesign()) {
    $hd = $user->getHumanDesign();
    $arr[] = 'Дизайн: ' . $hd->type->text . ', профиль: ' . $hd->profile->text;
}
$birthDate = $user->getField('birth_date');
if ($birthDate) {
    $maya = \cs\models\Calendar\Maya::calc($birthDate);
    $arr[] = 'Персональная Галактическая Печать: ' . \cs\models\Calendar\Maya::$stampRows[ $maya['stamp'] - 1 ][0] . ', ' . \cs\models\Calendar\Maya::$tonList[ $maya['ton'] ][0];
    $arr[] = 'Дата прихода на Землю: ' . Yii::$app->formatter->asDate($birthDate);
}
if ($user->hasZvezdnoe()) {
    $z = $user->getZvezdnoe();
    $arr[] = 'Звездное происхождение: ' . $z->data;
}
$description = join('; ', $arr);
?>

<?= $this->render('../blocks/share', [
    'image'       => \yii\helpers\Url::to($user->getAvatar(), true),
    'url'         => \yii\helpers\Url::current([], true),
    'title'       => $user->getName2() . ':' . ' ' . 'Карта Жизни Гражданина Галактики',
    'description' => $description,
]) ?>
</div>
</div>
