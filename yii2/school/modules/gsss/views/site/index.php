<?php

use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var array $events gs_events */

$this->title = 'Галактический Союз Сил Света';


$isMobile = Yii::$app->deviceDetect->isMobile();

?>

<style>
    body {
        background: url(/images/new_earth/nmp/bg5.jpg) repeat-y center top;
    }
</style>
<center>
    <?php if ($isMobile) { ?>
        <div class="row">
            <div class="col-lg-12" style="margin-top: 60px;">
                <img
                    src="/images/index/slider/1.jpg"
                    width="100%">
            </div>
        </div>

    <?php } else { ?>
        <div class="bs-example" data-example-id="carousel-with-captions" style="width: 1200px; margin-top: 80px;">
            <div id="carousel-example-captions" class="carousel slide" data-ride="carousel">
                <ol class="carousel-indicators">
                    <li data-target="#carousel-example-captions" data-slide-to="0" class="active"></li>
                    <li data-target="#carousel-example-captions" data-slide-to="1" class=""></li>
                    <li data-target="#carousel-example-captions" data-slide-to="2" class=""></li>
                    <li data-target="#carousel-example-captions" data-slide-to="3" class=""></li>
                    <li data-target="#carousel-example-captions" data-slide-to="4" class=""></li>
                    <li data-target="#carousel-example-captions" data-slide-to="5" class=""></li>
                    <li data-target="#carousel-example-captions" data-slide-to="6" class=""></li>
                    <li data-target="#carousel-example-captions" data-slide-to="7" class=""></li>
                    <li data-target="#carousel-example-captions" data-slide-to="8" class=""></li>
                    <li data-target="#carousel-example-captions" data-slide-to="9" class=""></li>
                    <li data-target="#carousel-example-captions" data-slide-to="10" class=""></li>
                </ol>
                <div class="carousel-inner" role="listbox">
                    <div class="item active">
                        <img
                            src="/images/index/slider/1.jpg"
                            data-holder-rendered="true">
                    </div>
                    <div class="item">
                        <img
                            src="/images/index/slider/2.jpg"
                            data-holder-rendered="true">
                    </div>
                    <div class="item">
                        <img
                            src="/images/index/slider/3.jpg"
                            data-holder-rendered="true">
                    </div>
                    <div class="item">
                        <img
                            src="/images/index/slider/4.jpg"
                            data-holder-rendered="true">
                    </div>
                    <div class="item">
                        <img
                            src="/images/index/slider/5.jpg"
                            data-holder-rendered="true">
                    </div>
                    <div class="item">
                        <img
                            src="/images/index/slider/6.jpg"
                            data-holder-rendered="true">
                    </div>
                    <div class="item">
                        <img
                            src="/images/index/slider/7.jpg"
                            data-holder-rendered="true">
                    </div>
                    <div class="item">
                        <img
                            src="/images/index/slider/8.jpg"
                            data-holder-rendered="true">
                    </div>
                    <div class="item">
                        <img
                            src="/images/index/slider/9.jpg"
                            data-holder-rendered="true">
                    </div>
                    <div class="item">
                        <img
                            src="/images/index/slider/10.jpg"
                            data-holder-rendered="true">
                    </div>
                    <div class="item">
                        <img
                            src="/images/index/slider/11.jpg"
                            data-holder-rendered="true">
                    </div>
                </div>
                <a class="left carousel-control" href="#carousel-example-captions" role="button"
                   data-slide="prev"> <span
                        class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span> <span
                        class="sr-only">Previous</span> </a> <a class="right carousel-control"
                                                                href="#carousel-example-captions" role="button"
                                                                data-slide="next"> <span
                        class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span> <span
                        class="sr-only">Next</span> </a>
            </div>
        </div>
    <?php } ?>
</center>


<div class="container">
<center>
    <p class="lead text-center">Галактический Союз Сил Света – это живой организм объединяющий все силы света в единое
        целое служащий всеобщему процветанию и счастью каждого элемента входящего в его состав, построенный на основе
        естественных законов мироздания.</p>
</center>

</div>

<?php $this->registerJsFile('/js/parallax.js-1.5.0/parallax.js', ['depends' => ['yii\web\JqueryAsset']]); ?>





<div class="modal fade" id="myModal1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">&nbsp;</h4>
            </div>
            <div class="modal-body">
                <img class="text-center" src="/images/new_earth/index/i1.png" width="100%"/>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" data-dismiss="modal" style="width: 100%;">Закрыть
                </button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="myModal2" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">&nbsp;</h4>
            </div>
            <div class="modal-body">
                <img class="text-center" src="/images/new_earth/index/i2.png" width="100%"/>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" data-dismiss="modal" style="width: 100%;">Закрыть
                </button>
            </div>
        </div>
    </div>
</div>
<div class="container">

    <div class="col-lg-12">

        <h2 class="page-header text-center">Измерения жизни</h2>

        <div class="col-lg-4 col-lg-offset-2"><a href="#" type="button" data-toggle="modal" data-target="#myModal1"><img
                        class="thumbnail" src="/images/new_earth/index/i1.png"
                        style="width: 100%;max-width: 300px; max-height: 400px;"/></a></div>
        <div class="col-lg-4"><a href="#" type="button" data-toggle="modal" data-target="#myModal2"><img
                        class="thumbnail" src="/images/new_earth/index/i2.png"
                        style="width: 100%;max-width: 300px; max-height: 400px;"/></a></div>
    </div>
    <div class="col-lg-8 col-lg-offset-2">

        <h2 class="page-header text-center">Отличия трехмерной Земли от пятимерной Земли</h2>

        <p>Дорогие, многие из вас интересуются, как они узнают, что оказались в этих потоках энергии, и как узнают,
            что находятся в пятом измерении.</p>

        <p>В третьем измерении вы жили в дуальном мире, и этот дуализм был основой энергетических структур и
            дисбаланса, которые использовались для управления. Например, в дуальности мужских и женских энергий
            долгое время наблюдался дисбаланс и контроль.</p>

        <p>Вначале существовало матриархальное общество, где доминировало женское начало, женщины находились при
            власти и управляли социальными структурами. Потом оно было заменено патриархальными структурами, когда
            стало доминировать мужское начало, которое взяло общество под свой контроль. В обоих случаях наблюдалось
            отсутствие баланса и доминирование одного над другим.</p>

        <p>Ни одна из этих форм не была сбалансирована и, как таковая, ни одна из них не отражала поток
            Божественного Света и Коды Света Космического Сердца.</p>

        <p>На Новой Земле эти Божественные Силы, известные как мужские и женские, будут абсолютно сбалансированы в
            обществе пятого измерения. Никто больше не будет доминировать, и не будет никаких социальных структур,
            лишающих силы многочисленные группы людей.</p>


        <h2 class="page-header text-center">Пятое Измерение – это структура Единства, Гармонии, Сплоченности и Баланса</h2>

        <p>Осознавая реальность пятого измерения, вы начинаете чувствовать, насколько глубоко «несбалансировано»
            ваше общество, и каким хаотичным оно кажется, поскольку по мере вашего приближения к равновесию, старое
            начинает отпадать.</p>

        <p>Первый признаком того, что этот процесс идет полным ходом, является то, что вы начинаете чувствовать, что
            мир сошел с ума, и вы изо всех сил стараетесь поддержать свое душевное спокойствие. Дорогие, это вызвано
            тем, что ваш «центр» перестраивается, и вы после многолетней работы приходите к реальному внутреннему
            балансу. То, что казалось «реальным» в мире, теперь кажется не больше, чем хаотическим беспорядком. И
            вам&nbsp; тяжело найти свое «место» в этом хаосе и беспорядке. Это потому, что ваше место уже на Новой
            Земле Высшего Сознания, которое начинает проявляться. Ваша задача состоит в поддержании своего душевного
            спокойствия и высшей частоты так, чтобы вы могли быть «точкой проявления» Новой Земли. Новые идеи и
            новые учения будут кристаллизоваться и проявляться вокруг вас и течь в бытие через вас, когда вы будете
            в своем&nbsp; пятимерном теле и форме.</p>

        <p>Таким образом, находясь посреди хаоса, помните, что вы - маяк или передатчик внутреннего баланса. Это
            означает, что внутренний Священный Союз может быть достигнут благодаря тому, что Коды Света Мира и
            Гармонии пятого измерения будут передаваться окружающему миру через ваше Тело Света. Степень своей&nbsp;
            работы выбираете вы сами,&nbsp; поскольку каждый из вас является Сотрудником Света или Воином Света,
            пришедшим на Землю для участия в этом процессе.</p>

    </div>

</div>

<div class="container">

<hr>
<div class="row">
    <center>
        <p class="lead" style="padding-top: 40px;">Представляем вам наш новый проект «Аватар»</p>
        <iframe width="100%" height="315" src="https://www.youtube.com/embed/u-b_I_UWSIM" frameborder="0" allowfullscreen></iframe>

        <p>Подробнее смотрите на сайте <a href="http://www.i-am-avatar.com/" target="_blank">Школы Богов</a></p>
    </center>
</div>


<hr class="featurette-divider" style="margin-bottom: 100px;">
<div class="container">
    <center>
        <p class="lead text-center">Мы строим мир гармонично развивающий все сферы организации жизни.</p>
    </center>
    <div class="row featurette">
        <center>
            <?php if ($isMobile) { ?>
                <div class="list-group">
                    <a class="list-group-item" href="/language">Язык</a>
                    <a class="list-group-item" href="/energy">Энергия</a>
                    <a class="list-group-item" href="/time">Время</a>
                    <a class="list-group-item" href="/house">Пространство</a>
                    <a class="list-group-item" href="/study">Обучение</a>
                    <a class="list-group-item" href="/forgive">Прощающая система</a>
                    <a class="list-group-item" href="/money">Деньги</a>
                    <a class="list-group-item" href="/medical">Здоровье</a>
                    <a class="list-group-item" href="/food">Питание</a>
                </div>
            <?php } else { ?>
                <p class="text-center"><img
                        src="/images/page/mission/1415014_551550491597436_590146424_o.jpg"
                        usemap="#Map"
                        width="610"
                        title="Выберите интересующую вас сферу жизни"
                        id="imageSfera"
                        data-toggle="tooltip" data-placement="right"
                        ></p>
                <map name="Map" id="Map">
                    <area shape="poly" coords="319,122,345,131,335,185,304,181" href="/language" alt="Язык">
                    <area shape="poly" coords="382,226,443,166,461,191,402,245" href="/energy" alt="Энергия">
                    <area shape="poly" coords="421,293,488,293,489,316,422,319" href="/time" alt="Время">
                    <area shape="poly" coords="385,356,482,439,453,455,359,380" href="/house" alt="Пространство">
                    <area shape="poly" coords="300,422,341,409,359,499,322,507" href="/study" alt="Обучение">
                    <area shape="poly" coords="226,378,282,416,227,509,168,477" href="/forgive" alt="Прощающая система">
                    <area shape="poly" coords="105,358,193,328,201,357,121,386" href="/money" alt="Деньги">
                    <area shape="poly" coords="116,223,213,255,198,283,101,255" href="/medical" alt="Здоровье">
                    <area shape="poly" coords="213,129,273,198,222,219,184,145" href="/food" alt="Питание">
                </map>
            <?php } ?>

        </center>
    </div>
</div>

<hr>
<!-- Направления -->
<div class="row">
    <center>
        <div class="col-lg-3">
            <a href="/tv" role="button"><img class="img-circle"
                                             src="/images/index/39282781.jpg"
                                             alt="Generic placeholder image" width="140" height="140"></a>

            <h2>ТелеВидение</h2>

            <p>Новое прогрессивное телевидение открывающее новые горизонты восприятия мира и обучающее счастью, любви и
                радости жизни.</p>

            <p><a class="btn btn-default" href="/tv" role="button">Подробнее &raquo;</a></p>
        </div>
        <!-- /.col-lg-4 -->
        <div class="col-lg-3">
            <a href="/clothes" role="button"><img class="img-circle"
                                                  src="/images/page/clothes/11149819_1670996323123198_2039843726665452928_o.jpg"
                                                  alt="" width="140" height="140"></a>

            <h2>Одежда</h2>

            <p>Одежда активирует энергетические центры и подчеркивает божественную красоту тела ангела.</p>

            <p><a class="btn btn-default" href="/clothes" role="button">Подробнее &raquo;</a></p>
        </div>
        <!-- /.col-lg-4 -->
        <div class="col-lg-3">
            <a href="<?= Url::to(['page/arts']) ?>" role="button"> <img class="img-circle"
                                                                        src="/images/index/arts.png"
                                                                        alt="Generic placeholder image" width="140"
                                                                        height="140"></a>

            <h2>Художники</h2>

            <p>Художники рисуют миры в которых мы живем.</p>

            <p><a class="btn btn-default" href="<?= Url::to(['page/arts']) ?>" role="button">Подробнее &raquo;</a></p>
        </div>
        <!-- /.col-lg-4 -->
        <div class="col-lg-3">
            <a href="<?= Url::to(['page/music']) ?>" role="button"> <img class="img-circle"
                                                                         src="/images/index/music.jpg"
                                                                         alt="Generic placeholder image" width="140"
                                                                         height="140"></a>

            <h2>Музыка</h2>

            <p>Музыка высших сфер раскрывает сердца и расширяет сознание</p>

            <p><a class="btn btn-default" href="<?= Url::to(['page/music']) ?>" role="button">Подробнее &raquo;</a></p>
        </div>
        <!-- /.col-lg-4 -->
    </center>
</div>

<a name="events"></a>
<?php if (count($events) > 0) { ?>
    <hr class="featurette-divider" style="margin-bottom: 100px;">
    <div class="container">
        <center>
            <h1>Предстоящие события</h1>
        </center>
        <div class="row featurette">
            <?php foreach ($events as $event) {
                $link = $event['link'] . '';
                if ($link == '') {
                    $link = '/events/' . $event['id'];
                }
                ?>
                <div class="col-lg-4">
                    <h3><?= $event['name'] ?></h3>

                    <p><?= $event['date'] ?></p>

                    <p style="margin-bottom: 0px;padding-bottom: 0px;">
                        <a href="<?= $link ?>" target="_blank">
                            <img
                                src="<?= $event['image'] ?>"
                                width="100%"
                                alt=""
                                class="thumbnail"
                                >
                        </a>
                    </p>
                    <!--                    <table>-->
                    <!--                        <tr>-->
                    <!--                            <td style='font-family: "courier new", "times new roman", monospace; font-size: 8pt;text-align: center;'>-->
                    <!--                                12-->
                    <!--                            </td>-->
                    <!--                            <td style='font-family: "courier new", "times new roman", monospace; font-size: 8pt;text-align: center;'>-->
                    <!--                                13-->
                    <!--                            </td>-->
                    <!--                            <td style='font-family: "courier new", "times new roman", monospace; font-size: 8pt;text-align: center;'>-->
                    <!---->
                    <!--                            </td>-->
                    <!--                            <td style='font-family: "courier new", "times new roman", monospace; font-size: 8pt;text-align: center;'>-->
                    <!--                                23-->
                    <!--                            </td>-->
                    <!--                            <td style='font-family: "courier new", "times new roman", monospace; font-size: 8pt;text-align: center;'>-->
                    <!--                                24-->
                    <!--                            </td>-->
                    <!--                        </tr>-->
                    <!--                        <tr>-->
                    <!--                            <td>-->
                    <!--                                <img src="/assets/57410080/images/ton/12.gif" width="20">-->
                    <!--                            </td>-->
                    <!--                            <td>-->
                    <!--                                <img src="/assets/57410080/images/ton/12.gif" width="20">-->
                    <!--                            </td>-->
                    <!--                            <td style="padding-right: 10px;padding-left: 5px;">-->
                    <!--                                <img src="/assets/57410080/images/ton/3.gif" width="20">-->
                    <!--                            </td>-->
                    <!--                            <td>-->
                    <!--                                <img src="/assets/57410080/images/ton/12.gif" width="20">-->
                    <!--                            </td>-->
                    <!--                            <td>-->
                    <!---->
                    <!--                                <img src="/assets/57410080/images/ton/12.gif" width="20">-->
                    <!--                            </td>-->
                    <!--                        </tr>-->
                    <!--                        <tr>-->
                    <!--                            <td style="padding-right: 5px;">-->
                    <!--                                <img src="/assets/57410080/images/stamp3/12.gif" width="20">-->
                    <!--                            </td>-->
                    <!--                            <td style="padding-right: 5px;">-->
                    <!--                                <img src="/assets/57410080/images/stamp3/12.gif" width="20">-->
                    <!--                            </td>-->
                    <!--                            <td>-->
                    <!--                            </td>-->
                    <!--                            <td style="padding-right: 5px;">-->
                    <!--                                <img src="/assets/57410080/images/stamp3/12.gif" width="20">-->
                    <!---->
                    <!--                            </td>-->
                    <!--                            <td>-->
                    <!---->
                    <!--                                <img src="/assets/57410080/images/stamp3/12.gif" width="20" id="dd1">-->
                    <!---->
                    <!--                                <div style="position: absolute; top: 508px;left: 136px; z-index: 999; ">-->
                    <!--                                    <img src="/assets/57410080/images/stamp3/13.gif" width="10" alt="dddd" onmouseover="" style="border-radius: 5px;">-->
                    <!--                                </div>-->
                    <!--                            </td>-->
                    <!--                        </tr>-->
                    <!--                    </table>-->
                </div>
            <?php } ?>
        </div>
    </div>
<?php } ?>


<!--    Наши партнеры-->
<hr class="featurette-divider">
<div class="container">
    <h1 style="text-align: center;">Наши партнеры</h1>
    <style>
        .thumbnail1 {
            padding-bottom: 20px;
            padding-top: 4px;
        }
    </style>
    <div class="row" style="margin-top: 30px;">
        <div class="col-lg-4">
            <center>
                <h4 style="text-align: center;">Проект «Новая Земля»</h4>
                <a href="http://www.new-earth-project.org/" target="_blank" style="text-align: center;">
                    <center>
                        <img
                            src="/images/index/partners/logo.png"
                            height="100"
                            class="thumbnail1"
                            >
                    </center>
                </a>
            </center>
            <iframe width="100%" height="200" src="//www.youtube.com/embed/hu7hNS--S8M" frameborder="0"
                    allowfullscreen=""></iframe>
        </div>
        <div class="col-lg-4">
            <center>
                <h4 style="text-align: center;">Проект «Резонанс»</h4>
                <a href="http://resonance.is/" target="_blank" style="text-align: center;">
                    <center>
                        <img
                            src="/images/index/partners/resonance-project-vector-equilibrium-logo-sigil.png"
                            height="100"
                            class="thumbnail1"
                            >
                    </center>
                </a>
            </center>
            <iframe width="100%" height="200" src="//www.youtube.com/embed/5fVTtsivj64" frameborder="0"
                    allowfullscreen=""></iframe>
        </div>
        <div class="col-lg-4">
            <center>
                <h4 style="text-align: center;">Проект «Я Аватар»</h4>
                <a href="http://iamavatar.org/" target="_blank" style="text-align: center;">
                    <center>
                        <img src="/images/index/partners/122250502_(1)1.png"
                             height="100"
                             class="thumbnail1"
                            >
                    </center>
                </a>
            </center>
            <iframe width="100%" height="200" src="//www.youtube.com/embed/XLmcOvCa6UM" frameborder="0"
                    allowfullscreen=""></iframe>
        </div>
    </div>
    <div class="row" style="margin-top: 60px;">
        <div class="col-lg-4">
            <center>
                <h4 style="text-align: center;">Фонд «Дети Солнца»</h4>
                <a href="http://childrenofthesun.org/" target="_blank" style="text-align: center;">
                    <center>
                        <img
                            src="/images/index/partners/122250502_(1)2.png" height="100"
                            class="thumbnail1"
                            >
                    </center>
                </a>
            </center>
            <iframe width="100%" height="200" src="//www.youtube.com/embed/Mq-c8pzCIVM" frameborder="0"
                    allowfullscreen=""></iframe>
        </div>
        <div class="col-lg-4">
            <center>
                <h4 style="text-align: center;"
                    title="Новейшие космологические представления о Вселенной и человеке">Ииссиидиология</h4>
                <a href="http://ayfaar.org/" target="_blank">
                    <center>
                        <img
                            src="/images/index/partners/issi.jpg" height="100"
                            class="thumbnail1"
                            >
                    </center>
                </a>
            </center>
            <iframe width="100%" height="200" src="https://www.youtube.com/embed/0f3IwYlUPas" frameborder="0"
                    allowfullscreen></iframe>
        </div>
        <div class="col-lg-4">
            <center>
                <h4 style="text-align: center;">Проект «Аллатра»</h4>
                <a href="http://allatra.org/" target="_blank">
                    <center>
                        <img
                            src="/images/index/partners/allatra.png" height="100"
                            class="thumbnail1"
                            >
                    </center>
                </a>
            </center>
            <iframe width="100%" height="200" src="https://www.youtube.com/embed/DQCkVTTRiNA" frameborder="0"
                    allowfullscreen></iframe>
        </div>
        <div class="row" style="margin-top: 60px;">
            <div class="col-lg-4 col-lg-offset-4">
                <center>
                    <h4 style="text-align: center;">«Мы желаем всем счастья»</h4>
                    <a href="http://vse-vmeste.info/" target="_blank">
                        <center>
                            <img
                                src="/images/index/partners/vse-vmeste.png" height="100"
                                class="thumbnail1"
                                >
                        </center>
                    </a>
                </center>
                <iframe width="100%" height="200" src="https://www.youtube.com/embed/reFN7H0_U3Y" frameborder="0"
                        allowfullscreen></iframe>
            </div>
        </div>
    </div>

</div>

<!-- Индекс счастья -->
<hr class="featurette-divider" style="margin-top: 100px;">
<div class="container">
    <div class="col-md-7">
        <h2 class="featurette-heading" style="margin-top: 50px;">Индекс счастья <span
                class="text-muted">планеты Земля</span></h2>

        <p class="lead">Индекс Общенационального Счастья рассматривается как ключевой элемент строительства экономики
            Новой Земли, который синхронизирован с изначальными законми мироздания.</p>

        <p>Министерство Счастья провело несколько международных конференций, на которые были приглашены многие западные
            экономисты (включая нобелевских лауреатов по экономике), с целью выработки методик расчета ИОС (Индекс
            Общенационального Счастья) на основе сочетания экономической ситуации в стране и удовлетворенности жизнью
            населения. Улыбка населения является одним из показателей в разработанных формулах.</p>
    </div>
    <div class="col-md-5">
        <img
            class="featurette-image img-responsive center-block"
            src="/images/index/x_83bd0a57.jpg"
            alt="Generic placeholder image"
            >

    </div>
</div>

<!-- Новости -->
<div>
    <hr class="featurette-divider">
    <div class="col-lg-12">
        <h1 class="page-header">Последние новости Планеты Земля</h1>
    </div>
    <?php
    foreach (\app\models\NewsItem::query()->orderBy(['date_insert' => SORT_DESC])->limit(3)->all() as $row) {
        echo \app\services\GsssHtml::newsItem($row);
    }
    ?>
    <div class="col-lg-12">
        <a
            class="btn btn-default"
            style="width:100%"
            href="<?= Url::to(['news/index']) ?>"
            >Все новости</a>
    </div>
</div>

<!-- share -->
<div>
    <div class="col-lg-12">
        <hr class="featurette-divider">
        <?= $this->render('../blocks/share', [
            'image'       => \yii\helpers\Url::to('/images/index/slider/1.jpg', true),
            'url'         => \yii\helpers\Url::current([], true),
            'title'       => $this->title,
            'description' => 'Галактический Союз Сил Света – это живой организм объединяющий все силы света в единое целое служащий всеобщему процветанию и счастью каждого элемента входящего в его состав, построенный на основе естественных законах мироздания.',
        ]) ?>
    </div>
</div>

</div>