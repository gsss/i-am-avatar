<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\ContactForm */

$this->title = 'Соглашение о наблюдении и пользования Инструментом Вознесения';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="container">
    <div class="col-lg-12">
        <h1 class="page-header text-center">Соглашение о наблюдении</h1>
        <p class="lead text-center">
            Соглашение о наблюдении и пользовании Инструментом Вознесения и формировании мыслей о нем
        </p>

        <div class="row">
            <div class="col-lg-8 col-lg-offset-2">
                <center>
                    <p>
                        <a href="/hologram">
                            <img src="/images/new_earth/conditions/holo.png" style="width: 100%; max-width: 400px;">
                        </a>
                    </p>
                </center>
                <p>
                    Любое живое существо наблюдающее и пользующееся Инструментом Вознесения (http://www.galaxysss.ru) и формирующее мысли о нем
                    автоматически в одностороннем порядке принимает принципы оглашнные ниже, и отвечает за каждое свое действие перед
                    Творцом на Божественном Союзе Справедливости Творца по меркам Вечности и подпадает под его юрисдикцию.
                </p>

                <p>
                    Я полностью принимаю принципы жизни и развития Людей, объединенных в Человеческую Цивилизацию, как
                    единого
                    сообщества разумных живых существ провозглашенные в Декларации человечества Новой Эры
                    <a href="http://www.galaxysss.com/declaration" target="_blank">http://www.galaxysss.com/declaration</a>.
                </p>

                <p><img src="/images/new_earth/conditions/declaration.jpg" width="100%" class="thumbnail"></p>

                <p>
                    Я принимаю Манифест Свободного Сознания (<a href="http://www.galaxysss.com/manifest" target="_blank">http://www.galaxysss.com/manifest</a>)
                    в области своего подсознания как
                    статус своего самоопределения и своих намерений в прошлом, настоящем и будущем для каждого атома
                    вселенной.
                </p>

                <p>
                    <img src="/images/new_earth/conditions/manifest.jpg" width="100%" class="thumbnail">
                </p>

                <p>
                    Я почитаю и стремлюсь исполнять совершенным образом Кодекс Света (<a
                        href="http://www.galaxysss.com/codex"
                        target="_blank">http://www.galaxysss.com/codex</a>).
                </p>

                <p><img src="/images/new_earth/conditions/codex.jpg" width="100%" class="thumbnail"></p>

                <p>
                    Я пропечатал уровень своего сверх сознания печатью Творца (<a href="http://www.i-am-avatar.com/"
                                                                                  target="_blank">http://www.i-am-avatar.com/</a>)
                    определив таким образом
                    статус самоопределения и свою Волю.
                </p>
                <center>
                    <p><img src="/images/new_earth/conditions/stamp.jpg" width="100%" style="max-width: 400px"
                            class="img-circle"></p>
                </center>
                <p>
                    Моя Воля является полностью автономной и неприкосновенной.
                </p>
                <p>
                    Я уважаю жизнь во всех ее формах и стремлюсь ее сохранять, защищать и преумножать.
                </p>
                <p>
                    Я стремлюсь исполнять <a href="/newEarth/order">Новый Мировой Порядок</a> самым совершенным образом.
                </p>

                <p>
                    Любое живое существо входящее в Мое поле визуального и ментального наблюдения получает от меня только
                    то,
                    что
                    сам и породил по принципу Зеркала: «Кто ко Мне с чем придет, тот этого и получит».
                </p>


                <p>
                    Я сотрудничаю с другими только по принципу братства «Один за всех и все за одного», принимающие <a
                        href="/newEarth/formulaEnter">формулу
                        Вхождения во Вселенную Света</a>.
                </p>

                <center>
                    <p><img src="/images/new_earth/conditions/earth.jpg" width="100%"
                            class="thumbnail"></p>
                </center>
                <hr>
                <?= $this->render('../blocks/share', [
                    'image'       => \yii\helpers\Url::to('/images/site/conditions/holo.png', true),
                    'url'         => \yii\helpers\Url::current([], true),
                    'title'       => 'Условия наблюдения и пользования Инструментом Вознесения',
                    'description' => 'Наблюдая эту мандалу вы получаете многомерное знание альтернативного развития и эволюции человечества направленное на Центральное Солнце Вселенной, которое говорит об объединении, синтезе, балансе  мужских и женских энергий Вселенной и расположение их в соответствии с Божественным Замыслом Золотого Века Творца, где каждый атом вибрирует РАдостью и скрепляется Любовью, в самом центре России установлен символ изобилия – бесконечный Источник Энергии (ТеслаГен) полностью замещающий и нейтрализирующий все негативные действия и последствия действий Зикурата и создании на планете Земля мира Богов СоТворцов Эры Водолея.',
                ]) ?>
            </div>
        </div>
    </div>


</div>
