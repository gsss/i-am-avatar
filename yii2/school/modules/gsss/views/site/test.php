<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\ContactForm */

$this->title = 'Благодарность магазину';
?>
<div class="container">
    <div class="col-lg-6 col-lg-offset-3">
        <h1 class="page-header text-center">Благодарность магазину</h1>


        <form action="https://www.avatar-bank.com/merchant/index" method="post" role="form" target="_blank">
            <div class="form-group field-loginform-username required has-success">
                <label class="control-label" for="loginform-username">Комментарий:</label>
                <input type="text" class="form-control" name="title">
                <input type="hidden" class="form-control" name="key" value="pmRV9htJ6KmQfJQmiuZHbYFKKCXXQGvJynpNddBM">
                <input type="hidden" class="form-control" name="currency" value="BTC">
                <input type="hidden" class="form-control" name="successUrl" value="http://www.galaxysss.ru/site/test1">
            </div>
            <div class="form-group field-loginform-password required has-success">
                <label class="control-label" for="loginform-password">Сколько:</label>
                <input type="text" id="loginform-password" class="form-control" name="price">
            </div>
            <hr>
            <div class="form-group">
                <button type="submit" class="btn btn-success" name="login-button" style="width:100%">Оплатить</button>
            </div>
        </form>

    </div>


</div>
