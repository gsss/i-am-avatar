<?php

/** @var $this \yii\web\View */
$this->title = 'Кольцо Огненного Архангела Галактического Союза Сил Света';
?>
<div class="container">

    <h1 class="page-header text-center">Кольцо Огненного Архангела Галактического Союза Сил Света</h1>

    <p class="lead text-center">Защитный артефакт для защиты и утверждения Закона Белых Цивилизаций на планете Земля</p>

    <div class="col-lg-8 col-lg-offset-2">

        <p>
            <img src="/images/new_earth/service_vozdayanie_ring/12735697_1008953879172672_357678034_n.jpg" class="thumbnail" width="100%">
        </p>

        <p>
            Кольцо является защитным артефактом, предназначенное для Огненных Архангелов Галактического Союза Сил Света.
            Огненные Архангелы являются <a href="/category/semya/article/2016/01/20/soznanie_avatara">аватарами</a> посланными на планету Земля для выполнения миссии утверждения <a href="/newEarth/service/vozdayanie#zakon">Законов
            Белых Цивилизаций</a> на ее территории. Оно изготовлено из чистого серебра, специальных оберегающих и
            усиливающих символов и молитв.
        </p>

        <p>
            Разработано специально для сотрудников <a href="/newEarth/service/vozdayanie">Агентства Воздаяния Творца</a>, которыми и являются Огненные Архангелы.
        </p>

        <p>
            Изготавливается специально с использованием молитв к Архангелу Михаилу и Иисусу Христу в определенное время
            и при определенных обстоятельствах, дабы снабдить его силой отражения достаточной для охраны и утверждения
            <a href="/newEarth/service/vozdayanie#zakon">Законов Белых Цивилизаций</a> на Галактическом и Вселенском уровне.
        </p>

        <p>
            Кольцо является отличным индикатором, как только кожа на пальце под кольцом почернеет, или само кольцо
            почернеет, значит на вас был направлен негативный поток. При наведенной сильной порчи кольцо, либо какой
            другой предмет-талисман может даже треснуть, приняв поток негатива на себя.
        </p>

        <p>
            Носить его является особой честью во всех уголках Вселенной, так как защита <a href="/newEarth/service/vozdayanie#zakon">Законов Белых Цивилизаций</a> дарует
            величайшую заслугу.
        </p>

        <p>
            Любой желающий может приобрести кольцо и стать сотрудником <a href="/newEarth/service/vozdayanie">Агентства Воздаяния Творца</a> для защиты и
            утверждения <a href="/newEarth/service/vozdayanie#zakon">Законов Белых Цивилизаций</a> на планете Земля.
        </p>

        <p>
            Стоимость 10 000 руб. Срок изготовления 1 мес. Высылается по почте. Для заказа пользуйтесь почтой
            god@galaxysss.ru.
        </p>
        <p>
            <img src="/images/new_earth/service_vozdayanie_ring/12722033_1008953969172663_846918493_n.jpg" class="thumbnail" width="100%">
        </p>
        <p>
            <img src="/images/new_earth/service_vozdayanie_ring/12746032_1008953882506005_1550065793_n.jpg" class="thumbnail" width="100%">
        </p>



        <hr>


        <?= $this->render('../blocks/share', [
            'image'       => \yii\helpers\Url::to('/images/new_earth/service_vozdayanie_ring/12735697_1008953879172672_357678034_n.jpg', true),
            'url'         => \yii\helpers\Url::current([], true),
            'title'       => $this->title,
            'description' => 'Защитный артефакт для защиты и утверждения Закона Белых Цивилизаций на планете Земля. Кольцо является защитным артефактом, предназначенное для Огненных Архангелов Галактического Союза Сил Света. Огненные Архангелы являются аватарами посланными на планету Земля для выполнения миссии утверждения Законов Белых Цивилизаций на ее территории. Оно изготовлено из чистого серебра, специальных оберегающих и усиливающих символов и молитв.',
        ]) ?>
    </div>
</div>
