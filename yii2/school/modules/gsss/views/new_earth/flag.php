<?php
$this->title = 'Флаг планеты Земля';
?>

<div class="container">

    <div class="col-lg-12">
        <h1 class="page-header text-center"><?= \yii\helpers\Html::encode($this->title) ?></h1>

        <p class="lead text-center">Концепт интернационального флага Земли</p>
        <p><img src="/images/new_earth/flag/header.jpg" width="100%" class="thumbnail"></p>



    </div>
    <div class="row">
        <div class="col-lg-8 col-lg-offset-2">
            <p style="margin-bottom: 40px;margin-top: 40px;">Шведский дизайнер Оскар Пернефельдт (Oskar Pernefeldt) создал проект флага планеты Земля (The International Flag of Planet Earth), основная цель которого — «напомнить населению Земли, что все мы живем на одной планете, несмотря на государственные границы». На первый взгляд все очень просто, однако за каждой деталью стоит свой символ, о чем автор подробно рассказывает в этом видео.</p>
            <p><iframe src="https://player.vimeo.com/video/127694736" width="100%" height="400" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe></p>
            <p><img src="/images/new_earth/flag/Flag_20x30.jpg" width="100%" class="thumbnail"/> </p>
            <p><img src="/images/new_earth/flag/flag_construction_w_flag.jpg" width="100%" class="thumbnail"/> </p>
            <p style="margin-top: 40px;">«Семь колец в центре флага образуют цветок — символ жизни на Земле. Кольца соединены друг с другом, что указывает на то, что все на нашей планете прямо или косвенно связано между собой. Синий фон представляет собой воду, которая имеет важное значение для жизни, а также океаны, которые покрывают большую часть поверхности нашей планеты.</p>
            <p>Внешние кольца цветка образуют круг, который можно рассматривать как символ Земли как планеты, а синий фон может представлять собой Вселенную»</p>
            <p style="margin-bottom: 40px;">Конкретный оттенок синего подобран таким образом, чтобы он был легко различим как на белых скафандрах космонавтов, так и на фоне темного космоса.</p>
            <p><img src="/images/new_earth/flag/2015-05-19_11h09_47-671x447.jpg" width="100%" class="thumbnail"/> </p>
            <p><img src="/images/new_earth/flag/Antarctica.0-671x447.jpg" width="100%" class="thumbnail"/> </p>
            <p><img src="/images/new_earth/flag/bdOHg.jpg" width="100%" class="thumbnail"/> </p>
            <p><img src="/images/new_earth/flag/Bureaucracy.0-671x447.jpg" width="100%" class="thumbnail"/> </p>
            <p><img src="/images/new_earth/flag/flag-of-earth.jpg" width="100%" class="thumbnail"/> </p>
            <p><img src="/images/new_earth/flag/Space_walk.0-671x447.jpg" width="100%" class="thumbnail"/> </p>
            <p><img src="/images/new_earth/flag/sport.jpg" width="100%" class="thumbnail"/> </p>
        </div>
    </div>




    <hr>

    <?= $this->render('../blocks/share', [
        'image'       => \yii\helpers\Url::to('/images/new_earth/flag/flag-of-earth.jpg', true) ,
        'url'         => \yii\helpers\Url::current([], true),
        'title'       => $this->title,
        'description' => 'Шведский дизайнер Оскар Пернефельдт (Oskar Pernefeldt) создал проект флага планеты Земля (The International Flag of Planet Earth), основная цель которого — «напомнить населению Земли, что все мы живем на одной планете, несмотря на государственные границы»',
    ]) ?>

</div>

