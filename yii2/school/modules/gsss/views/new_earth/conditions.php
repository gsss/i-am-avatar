<?php
$this->title = 'Соглашение о наблюдении';

?>
<style>
    body {
        background: url('/images/new_earth/nmp/bg2.jpg') repeat-y center top;
    }
</style>
<div class="container">
    <div class="col-lg-12">
        <h1 class="page-header text-center">
            <?= \yii\helpers\Html::encode($this->title) ?></h1>

        <p class="lead col-lg-8 col-lg-offset-2 text-center">Соглашение о наблюдении полноправных представителей Бога на планете Земля и формирование мыслей о них</p>
    </div>
    <div class="col-lg-8 col-lg-offset-2">
        <center>
            <p>
                <a href="/hologram">
                    <img src="/images/new_earth/conditions/holo.png" style="width: 100%; max-width: 400px;">
                </a>
            </p>
        </center>
        <p>
            Любое живое существо наблюдающее полноправных представителей Бога на планете Земля находящиеся в этой
            Вселенной прошлого настоящего и будущего, взаимодействующее с ним на уровне поступков, слов и мыслей прямо или косвенно
            автоматически в одностороннем порядке принимает принципы их существования оглашнные ниже, и отвечает за каждое свое действие перед
            Творцом на Божественном Союзе Справедливости Творца по меркам Вечности.
        </p>

        <p>
            Я полностью принимаю принципы жизни и развития Людей, объединенных в Человеческую Цивилизацию, как
            единого
            сообщества разумных живых существ провозглашенные в Декларации человечества Новой Эры
            <a href="http://www.galaxysss.com/declaration" target="_blank">http://www.galaxysss.com/declaration</a>
        </p>

        <p><img src="/images/new_earth/conditions/declaration.jpg" width="100%" class="thumbnail"></p>

        <p>
            Я принимаю Манифест Свободного Сознания (<a href="http://www.galaxysss.com/manifest" target="_blank">http://www.galaxysss.com/manifest</a>)
            в области своего подсознания как
            статус своего самоопределения и своих намерений в прошлом, настоящем и будущем для каждого атома
            вселенной.
        </p>

        <p>
            <img src="/images/new_earth/conditions/manifest.jpg" width="100%" class="thumbnail">
        </p>

        <p>
            Я почитаю и стремлюсь исполнять совершенным образом Кодекс Света (<a
                href="http://www.galaxysss.com/codex"
                target="_blank">http://www.galaxysss.com/codex</a>).
        </p>

        <p><img src="/images/new_earth/conditions/codex.jpg" width="100%" class="thumbnail"></p>

        <p>
            Я пропечатал уровень своего сверх сознания печатью Творца (<a href="http://www.i-am-avatar.com/"
                                                                          target="_blank">http://www.i-am-avatar.com/</a>)
            определив таким образом
            статус самоопределения и свою Волю.
        </p>
        <center>
            <p><img src="/images/new_earth/conditions/stamp.jpg" width="100%" style="max-width: 400px"
                    class="img-circle"></p>
        </center>
        <p>
            Моя Воля является полностью автономной и неприкосновенной.
        </p>
        <p>
            Я уважаю жизнь во всех ее формах и стремлюсь ее сохранять, защищать и преумножать.
        </p>
        <p>
            Я стремлюсь исполнять <a href="/newEarth/order">Новый Порядок Мира</a> самым совершенным образом.
        </p>

        <p>
            Любое живое существо входящее в Мое поле визуального и ментального наблюдения получает от меня только
            то,
            что
            сам и породил по принципу Зеркала: «Кто ко Мне с чем придет, тот этого и получит».
        </p>


        <p>
            Я сотрудничаю с другими только по принципу братства «Один за всех и все за одного», принимающие <a
                href="/newEarth/formulaEnter">формулу
                Вхождения во Вселенную Света</a>.
        </p>

        <center>
            <p><img src="/images/new_earth/conditions/earth.jpg" width="100%"
                    class="thumbnail"></p>
        </center>
        <hr/>
        <?= $this->render('../blocks/share', [
            'image'       => \yii\helpers\Url::to('/images/new_earth/nmp/share.jpg', true),
            'url'         => \yii\helpers\Url::current([], true),
            'title'       => $this->title,
            'description' => 'Новый мировой порядок регламентирует способ взаимедействия людей планеты Земля в Эпохе Водолея для гармоничного взаимодействия на уровне Христосознания.',
        ]) ?>
    </div>

</div>