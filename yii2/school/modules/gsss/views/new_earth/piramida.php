<?php


$this->title = 'Проект Пирамида';

?>
<style xmlns="http://www.w3.org/1999/html">
    body {
        background: url('/images/new_earth/nmp/bg2.jpg') repeat-y center top;
    }

    .maxMargin {
        margin-top: 30px;
        margin-bottom: 30px;
    }
</style>
<div class="container">
<div class="col-lg-12">
    <h1 class="page-header text-center">
        <small>Проект</small><br>
        Пирамида
    </h1>
</div>
<div class="col-lg-6 col-lg-offset-3">
    <p class="text-center">

        http://psifactor.info/alesya/propavshij-kontinent-mu-atlantida-tihogo-okeana.htm

    <p class="text-center"><img style="border-radius: 20px; width: 100%;max-width: 500px;"
                                src="/images/new_earth/piramida/piramida.gif"></p>

    <h3 class="text-center page-header">Введение</h3>

    <p>Возлюбленные, Энергии продолжают усиливаться в своей интенсивности. Многие из вас нашли время, чтобы
        насладиться красотами природного мира и эта связь дает ощущение единства со всем, что вас окружает. Вы
        знаете, что есть причина и цель вашего присутствия в этом мире, пока вы продолжаете держать мост между двумя
        сходящимися реалиями. Это подарок вам принесён в ваш мир. У вас есть сила внутри вас и вы вегда сознательно
        в курсе, вы всегда знаете, что происходит в вашем мире, что находится в потоке Божественной цели и что
        движется через вас. Вы чувствуете волнение, имеете творческий потенциал и возможности, когда вы входите в
        новый этап своей жизни и старые энергии будут очищены и снято. Ваши бывшие представления уже отсутствуют, вы
        сделали свой выбор для большего роста, осознания и расширения.</p>

    <p class="text-center"><img style="border-radius: 20px; width: 100%;max-width: 500px;"
                                src="/images/new_earth/piramida/piramida2.gif"></p>
<p>Цель проекта: Заземление Луча Аватара Свободы на планету Земля.</p>
<p><iframe width="100%" height="315" src="https://www.youtube.com/embed/74befrRPmzY" frameborder="0" allowfullscreen></iframe></p>
    <p>О мощности пирамид. Валерий Уваров</p>

    <p><iframe width="100%" height="315" src="https://www.youtube.com/embed/hgNrjNOni_w" frameborder="0" allowfullscreen></iframe></p>
    <p>Вы ничего не теряете, а получаете шанс войти в новую реальность.
    Мы являемся путешественниками во Времени и пришли из Будущего чтобы построить Настоящее и ускорить ход
    эволюционных процессов на планете и формирований Божественного измерения на планете Земля.
    Мы заземляем луч эволюции Нового Времени. Всех заинтересованных в положительном исходе событий на планете Земля приглшаем подключаться к нашей команде.
    Пирамида является приемником лучей Нового Времени и тех пакетов информации Будущего которые сейчас транслируются на Землю.
    Эти пакеты станут семенами новых Знаний в умах Человечества, которые прорастут в дальнейшем в
    гарминичные био-ноосферные технологии которые будут внедрены в ближайшее время.</p>

    <p>Пирамида является Пятым Элементов вокруг четырех Элементов Стихий.</p>
    <h2 class="page-header">Энергетическая конструкция человека</h2>

    <p><iframe allowfullscreen="" class="thumbnail" frameborder="0" height="315" src="https://www.youtube.com/embed/hoO6b6KxaoI" width="100%"></iframe></p>

    <p>Тело - это ещё не вся конструкция человека. Энергетическое строение человека. Исконные знания.&nbsp;<br>
        Человек - самое загадочное творение на планете. Тело человека кажется нам совершенной биомашиной, которое выполняет множество функций, не только внешних, но и внутренних.<br>
        <br>
        Мы перемещаемся в 3-х мерном пространстве, выполняем разного рода действия, принимаем решения, обрабатываем информацию, общаемся. Но существует еще что-то неучтенное, пока не регистрируемое современными приборами, но явно ощущаемое человеком, невидимое глазу, тонкоматериальная составляющая человеческого существа - энергетическая конструкция человека.<br>
        <br>
        Процесс познания человеком своей природы бесконечен, равно как и количество неразгаданных тайн человеческого существования. Людям еще предстоит узнать много, ведь каждый ответ рождает еще больше новых вопросов...<br>
        <br>
        Подробнее о энергетическом строении человека в данном видео и в книге "АллатРа"&nbsp;<a data-sessionlink="ei=YTOhVp1Ek6Viyaq4mAs" href="http://allatra.info/" rel="nofollow" target="_blank">http://allatra.info/</a></p>

    <h2 class="page-header">Игры</h2>

<iframe width="100%" height="315" src="https://www.youtube.com/embed/fVj650ChRR8" frameborder="0" allowfullscreen></iframe>

    <hr>
    <?= $this->render('../blocks/share', [
        'image'       => \yii\helpers\Url::to('/images/new_earth/game/13517681_1054281484657665_1594128319563039257_o.jpg', true),
        'url'         => \yii\helpers\Url::current([], true),
        'title'       => $this->title,
        'description' => 'Общество построенное по био-ноосферному типу, когда технологии служат сохранению жизни, природы и рода.
            Децентрализованная система управления, самоуправление, Копное Право, БогоДержавие. Каждый помогает друг
            другу основываясь на взаимопомощи.',
    ]) ?>
</div>

</div>