<?php
$this->title = 'Звёздный Гимн';
?>

<div class="container">

    <div class="col-lg-12">
        <h1 class="page-header text-center">Звёздный Гимн</h1>
        <p class="lead text-center">Когда Заря свободная Взойдёт над всей Землёй, Любовью озарённые Войдём мы в Мир иной!</p>
    </div>

    <div class="col-lg-8 col-lg-offset-2">
        <iframe width="100%" height="480" src="https://www.youtube.com/embed/Fq6_b_89A6A" frameborder="0" allowfullscreen></iframe>

        <p style="padding-top: 40px;" class="lead">Когда Заря свободная<br>
            Взойдёт над всей Землёй,<br>
            Любовью озарённые<br>
            Войдём мы в Мир иной!<br>
            Пройдём сквозь годы грозные,<br>
            Очнувшись ото сна,<br>
            Чтоб стать Семьёю Звёздною,<br>
            Что в Боге лишь Одна!<br>
            <br>
            Пройдём сквозь годы грозные,<br>
            Очнувшись ото сна,<br>
            Чтоб стать Семьёю Звёздною,<br>
            Что в Боге лишь Одна!<br>
            <br>
            Сияя ослепительно<br>
            Огнём Любви своей,<br>
            От злобы разрушительной<br>
            Освободим людей!<br>
            Чтобы детьми-потомками<br>
            Гордилась вся Земля,<br>
            Чтоб наши Песни звонкие<br>
            Неслись во все края!<br>
            <br>
            Чтобы детьми-потомками<br>
            Гордилась вся Земля,<br>
            Чтоб наши Песни звонкие<br>
            Неслись во все края!<br>
            <br>
            Пусть наша Сила Звёздная<br>
            Осветит злую рать,<br>
            Вливая Светоносную<br>
            В них Божью Благодать!<br>
            Чтоб все, от тьмы свободные,<br>
            Путь находили к нам,<br>
            Чтоб Звёзды Путеводные<br>
            Светили их Сердцам!<br>
            <br>
            Чтоб все, от тьмы свободные,<br>
            Путь находили к нам,<br>
            Чтоб Звёзды Путеводные<br>
            Светили их Сердцам!<br>
            <br>
            Пускай Земля засветится<br>
            Божественным Огнём<br>
            И Богу пусть доверятся<br>
            Все, кто забыл о Нём!<br>
            Чтоб нас Дорога Звёздная<br>
            Вновь к Дому привела,<br>
            Чтоб Матерью нам Крёстною<br>
            Вселенная была!<br>
            <br>
            Чтоб нас Дорога Звёздная<br>
            Вновь к Дому привела,<br>
            Чтоб Матерью нам Крёстною<br>
            Вселенная была!</p>

        <hr>

        <?= $this->render('../blocks/share', [
            'image'       => \yii\helpers\Url::to('/images/new_earth/hymn2/share.png', true) ,
            'url'         => \yii\helpers\Url::current([], true),
            'title'       => $this->title,
            'description' => 'Когда Заря свободная
Взойдёт над всей Землёй,
Любовью озарённые
Войдём мы в Мир иной!
Пройдём сквозь годы грозные,
Очнувшись ото сна,
Чтоб стать Семьёю Звёздною,
Что в Боге лишь Одна!

Пройдём сквозь годы грозные,
Очнувшись ото сна,
Чтоб стать Семьёю Звёздною,
Что в Боге лишь Одна!',
        ]) ?>
    </div>
</div>

