<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;
use app\models\UnionCategory;
use yii\db\Query;
use app\modules\Shop\services\Basket;

/**
 * Форма оплаты
 */

/* @var $this yii\web\View */
/* @var $request \app\models\Shop\Request */

$this->title = 'Оформление заказа #' . $request->getId();

$union = $request->getUnion();
$shop = $union->getShop();

$billing = $request->getBilling();
$sid = $billing->source_id;
/** @var \common\models\shop\PaymentConfig $payConfig */
$payConfig = \common\models\shop\PaymentConfig::findOne(['paysystem_id' => $sid, 'union_id' => $union->getId()]);
/** @var \app\models\Piramida\WalletSourceInterface $source */
$source = $payConfig->getClass();
Yii::info(\yii\helpers\VarDumper::dumpAsString([$_SERVER, \Yii::$app->request->referrer]), 'gs\\order\\buy::SERVER');
?>
<div class="container">
    <div class="page-header text-center">
        <h1>
            <?= Html::encode($this->title) ?>
        </h1>
    </div>


    <div class="row">
        <div class="col-lg-8 col-lg-offset-2">
            <?= $source->getForm($billing, 'Заказ #' . $request->getId(), $shop->getField('name'), $payConfig->config, ['action' => 'gsssShop.'.$request->getId()]) ?>
        </div>
    </div>
</div>
