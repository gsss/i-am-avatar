<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;
use app\models\UnionCategory;
use yii\db\Query;
use app\modules\Shop\services\Basket;

/**
 * Форма заказа
 * Если пользователь не зарегистрирован то будет предожена авторизация и регистрация
 *
 * REQUEST:
 * - unionId - int - идентификатор объединения по которому будет выборка товаров для заказа ($mode = 'union')
 *
 * REQUEST:
 * - productId - int - идентификатор товара, который покупается в кол-ве 1 шт ($mode = 'product')
 *
 * Здесь может быть несколько вариантов:
 * 1. У клиента уже есть товары в корзине.
 * 2. У пользователя пустая корзина.
 *
 * Ситуация 1
 * Формируется заказ из одного товара а корзина так и остается которую можно оформить позже
 *
 * Ситуация 2
 * Формируется заказ из одного товара
 */

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \app\models\Form\Shop\Order */

$this->title = 'Оформление заказа';

if ($productId = Yii::$app->request->get('product_id')) {
    $mode = 'product';
    $product = \app\models\Shop\Product::find($productId);
    $unionId = $product->get('union_id');
    $productPrice = $product->get('price');
    $union = \app\models\Union::find($unionId);
    $isElectron = $product->get('is_electron', 0) == 1;
    $onlyElectron = $product->get('is_electron')? 1 : -1;
} else {
    $productId = 0;
    $mode = 'union';
    $unionId = Yii::$app->request->get('unionId');
    if (!$unionId) {
        $unionId = Basket::getUnionIds()[0];
    }
    $productPrice = Basket::getPrice($unionId);
    $union = \app\models\Union::find($unionId);
    $onlyElectron = (Basket::onlyElectron($unionId))? 1 : -1;
    $isElectron = $onlyElectron > 0;
}
$shop = $union->getShop();
$dostavkaRows = $shop->getDostavkaRows();
$isPiramida = $shop->get('is_piramida', 0);
$isPiramidaJson = ($isPiramida == 1) ? 1 : -1;

$this->registerJs(<<<JS
    var isPiramida = ({$isPiramidaJson} > 0) ? true : false;
    var onlyElectron = ({$onlyElectron} > 0) ? true : false;
    var mode = '{$mode}';

    $('.field-group-address').hide();
    function formatAsDecimal (val)
    {
        var pStr = '';
        if (val >= 1000) {
            var t = parseInt(val/1000);
            var ost = (val - t * 1000);
            var ostStr = ost;
            if  (ost == 0) {
                ostStr = '000';
            } else {
                if (ost < 10) {
                    ostStr = '00' + ost;
                }
                if (ost < 100) {
                    ostStr = '0' + ost;
                }
            }
            pStr = t + ' ' + ostStr;
        } else {
            pStr = val;
        }

        return pStr;
    }
JS
);
$this->registerJs(<<<JS
    var productPrice = {$productPrice};
JS
    , \yii\web\View::POS_HEAD);

?>
<div class="container">
<h1 class="page-header text-center">
    <?= Html::encode($this->title) ?>
</h1>
<div class="col-lg-8 col-lg-offset-2" style="padding-bottom: 30px; font-size: 200%;">
    <div class="row">
        <div class="btn-group" role="group" aria-label="..." style="width: 100%;">
            <?php
            if (Yii::$app->user->isGuest) $class = 'primary';
            else $class = 'success';
            ?>
            <button type="button" class="btn btn-<?= $class ?>" id="buttonStep1" style="width: 25%;"><i class="glyphicon glyphicon-user"></i>
                Знакомство
            </button>
            <?php
            if (Yii::$app->user->isGuest) {
                $class = 'default';
            } else {
                if ($isElectron) {
                    $class = 'success';
                } else {
                    $class = 'primary';
                }
            }
            ?>
            <button type="button" class="btn btn-<?= $class ?>" id="buttonStep2" style="width: 25%;"><i class="glyphicon glyphicon-plane"></i>
                Тип доставки
            </button>
            <?php
            if (!Yii::$app->user->isGuest && ($isElectron)) $class = 'primary';
            else $class = 'default';
            ?>
            <button type="button" class="btn btn-<?= $class ?>" id="buttonStep3" style="width: 25%;"><i class="glyphicon glyphicon-globe"></i>
                Адрес
            </button>
            <button type="button" class="btn btn-default" id="buttonStep4" style="width: 25%;"><i
                    class="glyphicon glyphicon-credit-card"></i> Оплата
            </button>
        </div>
    </div>
</div>


    <div class="row">
        <div class="col-lg-8 col-lg-offset-2">

        <?php
        $class = ['collapse', 'divStep1'];
        if (Yii::$app->user->isGuest) {
            $class[] = 'in';
        }
        ?>
        <div class="<?= join(' ', $class) ?>">
            <p style="padding-bottom: 30px; font-size: 200%;">Вы не авторизованы. Если вы в первый раз на нашем сайте, то зарегистрируйтесь. Или войдите если у вас есть
                логин.</p>
            <div class="row">
                <div class="col-lg-6">
                    Вход
                    <?php
                    $form = \yii\bootstrap\ActiveForm::begin([
                        'id'                 => 'login-form',
                        'enableClientScript' => false,
                    ]);
                    $model2 = new \app\models\Auth\Login();
                    ?>

                    <?= $form->field($model2, 'username', ['inputOptions' => ['placeholder' => 'Логин']])->label('', ['class' => 'hide']) ?>
                    <?= $form->field($model2, 'password', ['inputOptions' => ['placeholder' => 'Пароль']])->label('', ['class' => 'hide'])->passwordInput() ?>

                    <?php \yii\bootstrap\ActiveForm::end(); ?>
                    <button class="btn btn-default" id="buttonLogin2" style="width: 100%;">Войти</button>

                    <?php
                    $this->registerJs(<<<JS
                        $('#buttonLogin2').click(function() {
                            if ($('#login-username').val() == '') {
                                $('.field-login-username').addClass('has-error');
                                $('.field-login-username .help-block').show().html('Нужно заполнить логин');

                                return false;
                            }
                            if ($('#login-password').val() == '') {
                                $('.field-login-password').addClass('has-error');
                                $('.field-login-password .help-block').show().html('Нужно заполнить пароль');

                                return false;
                            }
                            ajaxJson({
                                url: '/shop/loginAjax',
                                data: {
                                    login: $('#login-username').val(),
                                    password: $('#login-password').val()
                                },
                                success: function(ret) {
                                    if (isPiramida) {
                                        $('#wallet-a').data('max', ret.wallet.a);
                                        $('#wallet-b').data('max', ret.wallet.b);
                                        $('#wallet-a-text').html(ret.wallet.a);
                                        $('#wallet-b-text').html(ret.wallet.b);
                                    }
                                    $('#divPrice').show();
                                    $('.divStep1').on('hidden.bs.collapse', function() {
                                        if (!onlyElectron) {
                                            $('.divStep2').collapse('show');
                                            $('#buttonStep1').removeClass('btn-primary').addClass('btn-success');
                                            $('#buttonStep2').removeClass('btn-default').addClass('btn-primary');
                                        } else {
                                            $('.divStep3').collapse('show');
                                            $('#buttonStep1').removeClass('btn-primary').addClass('btn-success');
                                            $('#buttonStep2').removeClass('btn-default').addClass('btn-success');
                                            $('#buttonStep3').removeClass('btn-default').addClass('btn-primary');
                                        }
                                    }).collapse('hide');
                                },
                                errorScript: function(ret) {
                                    if (ret.id == 101) {
                                        $('.field-login-username').addClass('has-error');
                                        $('.field-login-username .help-block').show().html(ret.data);
                                    }
                                    if (ret.id == 102) {
                                        $('.field-login-password').addClass('has-error');
                                        $('.field-login-password .help-block').show().html(ret.data);
                                    }
                                }
                            });

                        });
                        $('#login-username').on('focus', function() {
                            $('.field-login-username').removeClass('has-error');
                            $('.field-login-username .help-block').hide();
                        });
                        $('#login-password').on('focus', function() {
                            $('.field-login-password').removeClass('has-error');
                            $('.field-login-password .help-block').hide();
                        });
JS
                    );
                    ?>
                    <div style="margin: 20px 0px 0px 0px;">
                        <?= \yii\authclient\widgets\AuthChoice::widget([
                            'baseAuthUrl' => ['auth/auth']
                        ]); ?>
                    </div>
                </div>
                <div class="col-lg-6">
                    Регистрация
                    <?php
                    $form = \yii\bootstrap\ActiveForm::begin([
                        'id'                 => 'rigistration-form',
                        'enableClientScript' => false,
                    ]);
                    $model2 = new \app\models\Auth\Regisration();
                    ?>

                    <?= $form->field($model2, 'name', ['inputOptions' => ['placeholder' => 'Имя (Фамилия)']])->label('', ['class' => 'hide']) ?>
                    <?= $form->field($model2, 'username', ['inputOptions' => ['placeholder' => 'Логин/почта']])->label('', ['class' => 'hide']) ?>

                    <?php \yii\bootstrap\ActiveForm::end(); ?>
                    <button class="btn btn-default" id="buttonRegister" style="width: 100%;">Войти</button>
                    <?php
                    $this->registerJs(<<<JS
                        $('#buttonRegister').click(function(){
                            if ($('#regisration-name').val() == '') {
                                $('.field-regisration-name').addClass('has-error');
                                $('.field-regisration-name .help-block').show().html('Нужно заполнить имя');

                                return false;
                            }
                            if ($('#regisration-username').val() == '') {
                                $('.field-regisration-username').addClass('has-error');
                                $('.field-regisration-username .help-block').show().html('Нужно заполнить логин');

                                return false;
                            }

                            ajaxJson({
                                url: '/shop/registrationAjax',
                                data: {
                                    login: $('#regisration-username').val(),
                                    name: $('#regisration-name').val()
                                },
                                success: function(ret) {
                                    if (isPiramida) {
                                        $('#wallet-a').data('max', ret.wallet.a);
                                        $('#wallet-b').data('max', ret.wallet.b);
                                        $('#wallet-a-text').html(ret.wallet.a);
                                        $('#wallet-b-text').html(ret.wallet.b);
                                    }
                                    $('#divPrice').show();
                                    $('.divStep1').on('hidden.bs.collapse', function() {
                                        if (!onlyElectron) {
                                            $('.divStep2').collapse('show');
                                            $('#buttonStep1').removeClass('btn-primary').addClass('btn-success');
                                            $('#buttonStep2').removeClass('btn-default').addClass('btn-primary');
                                        } else {
                                            $('.divStep3').collapse('show');
                                            $('#buttonStep1').removeClass('btn-primary').addClass('btn-success');
                                            $('#buttonStep2').removeClass('btn-default').addClass('btn-success');
                                            $('#buttonStep3').removeClass('btn-default').addClass('btn-primary');
                                        }
                                    }).collapse('hide');
                                },
                                errorScript: function(ret) {
                                    $('.field-regisration-username').addClass('has-error');
                                    $('.field-regisration-username .help-block').show().html(ret.data);
                                }
                            });

                        });
                        $('#regisration-username').on('focus', function() {
                            $('.field-regisration-username').removeClass('has-error');
                            $('.field-regisration-username .help-block').hide();
                        });
                        $('#regisration-name').on('focus', function() {
                            $('.field-regisration-name').removeClass('has-error');
                            $('.field-regisration-name .help-block').hide();
                        });
JS
                    );
                    ?>
                </div>
            </div>

        </div>


        <p id="divPrice" <?php if (Yii::$app->user->isGuest) echo ' style="display:none;"' ?>>Сумма заказа: <span id="requestPrice"
                               data-price="<?= $productPrice ?>"><?= Yii::$app->formatter->asDecimal($productPrice, 0) ?></span>
        </p>

        <?php
        $class = ['collapse', 'divStep2'];
        if ((Yii::$app->user->isGuest == false) && ($onlyElectron == -1)) {
            $class[] = 'in';
        } else {
            $this->registerJs(<<<JS
$('#buttonSuccessStep3').click(function() {
    var dostavka = '';
    var dostavkaType = '';
    var dostavkaObject;
    $('input[name="Order[dostavka]"]').each(function() {
        if ($(this).is(':checked')) {
            dostavkaObject = $(this);
            dostavka = dostavkaObject.attr('value');
        }
    });
    if (!onlyElectron) {
        if (dostavkaObject.data('is_need_address') == 1) {
            if ($('#order-address').val() == '') {
                infoWindow('Нужно обязательно заполнить адрес доставки');
                return false;
            }
        }
    }
    var price = $('#requestPrice').data('price');
    $('#wallet-ps').val(price);
    $('#wallet-ps').attr('data-value', price);
    $('.divStep3').collapse('hide').on('hidden.bs.collapse', function() {
        $('.divStep4').collapse('show');
        $('#buttonStep3').removeClass('btn-primary').addClass('btn-success');
        $('#buttonStep4').removeClass('btn-default').addClass('btn-primary');
    });

    return false;
});
JS
);
        }
        ?>
        <div class="<?= join(' ', $class) ?>">
            <?= $form
                ->field($model, 'dostavka', [
                    'template' => "{label}\n{input}\n{hint}\n{error}<p><a href='javascript:void(0);' class='buttonDostavkaMore'>Подробнее</a></p>",
                ])
                ->widget('app\modules\Shop\widget\RadioList', ['rows' => $dostavkaRows])
                ->label('Доставка')
            ?>
            <?php
            $this->registerJs(<<<JS
$('#w0').on('submit', function() {return false});
$('input[name="Order[dostavka]"]').on('change', function() {
    var newPrice;
    var dostavka_price = $(this).data('price');
    var dostavka_is_need_address = $(this).data('is_need_address');

    newPrice = productPrice + dostavka_price;
    if (dostavka_is_need_address == 1) {
        $('.field-group-address').show();
    } else {
        $('.field-group-address').hide();
    }
    if (isPiramida) {
    $('#wallet-ps').val(newPrice);
    $('#wallet-a').val('');
    $('#wallet-b').val('');
    }
    $('#requestPrice').html(formatAsDecimal(newPrice));
    $('#requestPrice').attr('data-price', newPrice);
});
$('.buttonDostavkaMore').click(function() {
$('#myModal').modal('show');
});

$('#buttonSuccessStep2').click(function() {
    var isNaloznii;
    var dostavka = '';
    var dostavkaType = '';
    var dostavkaObject;
    $('input[name="Order[dostavka]"]').each(function() {
    if ($(this).is(':checked')) {
        dostavkaObject = $(this);
        dostavka = dostavkaObject.attr('value');
        isNaloznii = dostavkaObject.data('type') == 3;
    }
    });

    if (isNaloznii) {
        $('#buttonSuccessStep3').click(function() {
            var dostavka = '';
            var dostavkaType = '';
            var dostavkaObject;
            $('input[name="Order[dostavka]"]').each(function() {
                if ($(this).is(':checked')) {
                    dostavkaObject = $(this);
                    dostavka = dostavkaObject.attr('value');
                }
            });
            var address = [];
            if ($('#order-address_index').val() != '') address.push($('#order-address_index').val());
            if ($('#order-address').val() != '') address.push($('#order-address').val());
            if ($('#order-address_fio').val() != '') address.push($('#order-address_fio').val());
            var data = {
                mode: mode,
                comment: $('#order-comment').val(),
                price: $('#requestPrice').data('price'),
                phone: $('#order-phone').val(),
                type: dostavkaType,
                dostavka: dostavka
            };
            data.address = address.join(', ');
            if (mode == 'product') {
                data.product_id = {$productId};
            } else {
                data.union_id = {$union->getId()};
            }
            ajaxJson({
                url: '/shop/order/ajaxNalozh',
                data: data,
                success: function(ret) {
                    window.location = '/cabinet/order/' + ret;
                }
            });
        }).html('Оформить');
    } else {
        $('#buttonSuccessStep3').click(function() {
            var dostavka = '';
            var dostavkaType = '';
            var dostavkaObject;
            $('input[name="Order[dostavka]"]').each(function() {
                if ($(this).is(':checked')) {
                    dostavkaObject = $(this);
                    dostavka = dostavkaObject.attr('value');
                }
            });
            if (!onlyElectron) {
                if (dostavkaObject.data('is_need_address') == 1) {
                    if ($('#order-address').val() == '') {
                        infoWindow('Нужно обязательно заполнить адрес доставки');
                        return false;
                    }
                }
            }
            var price = $('#requestPrice').data('price');
            $('#wallet-ps').val(price);
            $('#wallet-ps').attr('data-value', price);
            $('.divStep3').collapse('hide').on('hidden.bs.collapse', function() {
                $('.divStep4').collapse('show');
                $('#buttonStep3').removeClass('btn-primary').addClass('btn-success');
                $('#buttonStep4').removeClass('btn-default').addClass('btn-primary');
            });

            return false;
        });
    }
    if (dostavka == '') {
        infoWindow('Вам нужно выбрать способ Доставки');
        return false;
    }
    $('.divStep2').collapse('hide').on('hidden.bs.collapse', function() {
        $('.divStep3').collapse('show');
        $('#buttonStep2').removeClass('btn-primary').addClass('btn-success');
        $('#buttonStep3').removeClass('btn-default').addClass('btn-primary');
    });

    return false;
});

JS
            );
            ?>
            <button class="btn btn-success" style="width: 100%;" id="buttonSuccessStep2">Далее</button>
        </div>
        <?php
        $class = ['collapse', 'divStep3'];
        if ($isElectron && !Yii::$app->user->isGuest) {
            $class[] = 'in';
        }
        ?>
        <div class="<?= join(' ', $class) ?>">
            <?php if (!$isElectron) { ?>
                <div class="field-group-address">
                    <?= $form->field($model, 'address')->textarea(['rows' => 2])->label('Адрес доставки') ?>
                    <?= $form->field($model, 'address_index')->label('Индекс') ?>
                    <?= $form->field($model, 'address_fio')->label('ФИО получателя') ?>
                </div>
            <?php } ?>
            <?= $form->field($model, 'comment')->textarea(['rows' => 5])->label('Комментарий к заказу') ?>
            <?= $form->field($model, 'phone')->label('Телефон') ?>
            <button class="btn btn-success" style="width: 100%;" id="buttonSuccessStep3">Далее</button>
        </div>

        <div class="collapse divStep4">
            <div style="display:none;">
                <?php
                $this->registerJs(<<<JS
    $('#wallet-a').on('input', function() {
        var price = parseFloat($('#requestPrice').data('price'));
        var a = $('#wallet-a').val();
        a = (a == '') ? 0 : parseFloat(a);
        var b = $('#wallet-b').val();
        b = (b == '') ? 0 : parseFloat(b);
        var ps = $('#wallet-ps').data('value');
        ps = (ps == '') ? 0 : parseFloat(ps);
        if (a < 0) {
            alert('Нельзя вводить отрицательную сумму');
            return false;
        }
        if (a > $('#wallet-a').data('max')) {
            // ошибка невозможно оплатить суммой большей чем есть на счете
            alert('ошибка невозможно оплатить суммой большей чем есть на счете');
            return false;
        }
        if (a > price) {
            // ошибка сумма списания не может быть выше суммы заказа
            alert('ошибка сумма списания не может быть выше суммы заказа');
            return false;
        }
        if (a > (price - b)) {
            // ошибка
            alert('ошибка a > (price - b)');
            return false;
        }
        ps = price - b - a;
        $('#wallet-ps').val(ps).data('value', ps);
    });
    $('#wallet-b').on('input', function() {
        var price = parseFloat($('#requestPrice').data('price'));
        var a = $('#wallet-a').val();
        a = (a == '') ? 0 : parseFloat(a);
        var b = $('#wallet-b').val();
        b = (b == '') ? 0 : parseFloat(b);
        var ps = $('#wallet-ps').data('value');
        ps = (ps == '') ? 0 : parseFloat(ps);
        if (b < 0) {
            alert('Нельзя вводить отрицательную сумму');
            return false;
        }
        if (b > $('#wallet-b').data('max')) {
            // ошибка невозможно оплатить суммой большей чем есть на счете
            alert('ошибка невозможно оплатить суммой большей чем есть на счете');
            return false;
        }
        if (b > price) {
            // ошибка сумма списания не может быть выше суммы заказа
            alert('ошибка сумма списания не может быть выше суммы заказа');
            return false;
        }
        if (b > (price - a)) {
            // ошибка
           alert('ошибка b > (price - a)');
            return false;
        }
        ps = price - b - a;
        $('#wallet-ps').val(ps).data('value', ps);
    });
JS
                );
                ?>
                <div class="col-lg-4">
                    <p id="wallet-a-text">В кошельке:<?= (Yii::$app->user->isGuest)? '' : Yii::$app->user->identity->getWallet('A')->value ?></p>
                    <input class="form-control" type="text" id="wallet-a" placeholder="Оплата с кошелька А" data-max="<?= (Yii::$app->user->isGuest)? '0' : Yii::$app->user->identity->getWallet('A')->value ?>">
                </div>
                <div class="col-lg-4">
                    <p id="wallet-b-text">В кошельке:<?= (Yii::$app->user->isGuest)? '' : Yii::$app->user->identity->getWallet('B')->value ?></p>
                    <input class="form-control" type="text" id="wallet-b" placeholder="Оплата с кошелька B" data-max="<?= (Yii::$app->user->isGuest)? '0' : Yii::$app->user->identity->getWallet('B')->value ?>">
                </div>
                <div class="col-lg-4">
                    <p>К оплате платежной системе:</p>
                    <input class="form-control" type="text" disabled id="wallet-ps" placeholder="Оплата с Платежной системы" value="" data-value="">
                </div>
            </div>
            <div class="col-lg-12" style="margin-top: 20px;">
                <?php
                $this->registerJs(<<<JS
    $('.paymentType').click(function () {
        $('#paymentType').attr('value', $(this).data('value'));
        if (!$(this).hasClass('active')) {
            $('.paymentType').removeClass('active');
            $(this).addClass('active');
        }
    });
JS
                );
                ?>
                <h4 class="page-header">С помощью платежной системы</h4>

                <p>
                    <?php $this->registerJs("$('.gsssPopup').popover()");?>
                    <img
                        width="200"
                        style="margin: 20px 0px 20px 64px"
                        src="/images/shop/order/security-seal-2x.png"
                        data-toggle="popup"
                        data-html="enabled"
                        data-placement="bottom"
                        data-trigger="hover"
                        class="gsssPopup"
                        data-content="<b>Вся информация на этой странице защищена SSL. <br> Почему это так важно?</b><br><br>SSL (англ. Secure Sockets Layer — уровень защищенных сокетов) — это технология, которая обеспечивает безопасность обмена данными через интернет. Эта ссылка гарантирует, что все данные, передаваемые между веб-сервером и браузерами, защищены и останутся конфиденциальными. <br><br>SSL является отраслевым стандартом и используется миллионами сайтов для защиты онлайн-транзакций с клиентами."/>
                </p>

                <input type="hidden" value="" id="paymentType">
                <?php /** @var \common\models\PaySystem $paySystem */?>
                <?php
                $this->registerJs(<<<JS
                            $('.rowTable').click(function() {
                                $('#paymentType').val($(this).data('id'));
                                $(this).parent().find('.js-radio').prop('checked','');
                                $(this).find('.js-radio').prop('checked', 'checked');
                            });
JS
                );
                ?>
                <?= \yii\grid\GridView::widget([
                    'showHeader' => false,
                    'summary' => '',
                    'dataProvider' => new \yii\data\ActiveDataProvider([
                        'query' => \common\models\PaySystem::find()
                            ->innerJoin('gs_unions_shop_payments', 'gs_unions_shop_payments.paysystem_id = gs_paysystems.id')
                            ->andWhere(['union_id' => $union->getId()])
                    ]),
                    'tableOptions' => [
                        'class' => 'table table-hover table-striped',
                    ],
                    'rowOptions'   => function ($item) {
                        $data = [
                            'data'  => ['id' => $item['id']],
                            'role'  => 'button',
                            'class' => 'rowTable'
                        ];

                        return $data;
                    },
                    'columns' => [
                        [
                            'content' => function($item) {
                                return Html::radio(null,false,[
                                    'value' => \yii\helpers\ArrayHelper::getValue($item, 'id'),
                                    'class' => 'js-radio',
                                ]);
                            }
                        ],
                        [
                            'attribute' => 'image',
                            'format' => ['image', ['width' => 100, 'class' => 'thumbnail','style' => 'margin-bottom:0px;']]
                        ],
                        'title',
                    ]
                ])?>
            </div>

            <?php
                $this->registerJs(<<<JS

    $('#buttonSuccessStep4').click(function() {
        if ($('#wallet-ps').data('value') > 0) {
            if ($('#paymentType').val() == '') {
                alert('Выберите платежную систему');
                return false;
            }
        }
        var dostavka = '';
        var dostavkaType = '';
        var dostavkaObject;
        $('input[name="Order[dostavka]"]').each(function() {
            if ($(this).is(':checked')) {
                dostavkaObject = $(this);
                dostavka = dostavkaObject.attr('value');
            }
        });
        var data = {
            mode: mode,
            comment: $('#order-comment').val(),
            price: $('#requestPrice').data('price'),
            phone: $('#order-phone').val(),
            paymentType: $('#paymentType').val(),
            wallet: {
                a: $('#wallet-a').val(),
                b: $('#wallet-b').val(),
                ps: $('#wallet-ps').data('value')
            }
        };
        if (mode == 'product') {
            data.product_id = {$productId};
        } else {
            data.union_id = {$union->getId()};
        }
        if (!onlyElectron) {
            data.dostavka = dostavka;
            var address = [];
            if ($('#order-address_index').val() != '') address.push($('#order-address_index').val());
            if ($('#order-address').val() != '') address.push($('#order-address').val());
            if ($('#order-address_fio').val() != '') address.push($('#order-address_fio').val());
            data.address = address.join(', ');
        }
        ajaxJson({
            url: '/shop/order/ajax',
            data: data,
            success: function(ret) {
                if ($('#wallet-ps').val() == 0) {
                    window.location = '/cabinet/order/' + ret;
                } else {
                    window.location = '/shop/order/buy/' + ret;
                }
            }
        });
    })
JS
);
                ?>
                <button class="btn btn-success" style="width: 100%; margin-top: 20px;" id="buttonSuccessStep4" >перейти к оплате</button>
            </div>

        </div>
    </div>


<?php if ($onlyElectron < 0) { ?>
    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Доставка</h4>
                </div>
                <div class="modal-body">
                    <?php foreach ($dostavkaRows as $row) { ?>
                        <p><b><?= $row['name'] ?></b> - <?= $row['description'] ?>. Стоимость
                            = <?= is_null($row['price']) ? 0 : Yii::$app->formatter->asDecimal($row['price'], 0) ?> руб.
                        </p>
                    <?php } ?>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
                </div>
            </div>
        </div>
    </div>
<?php } ?>




</div>
