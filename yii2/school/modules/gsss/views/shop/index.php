<?php

/** $this yii\web\View */
/** $tree array */

use yii\helpers\Url;

$this->title = 'Магазин';
?>
<div class="container">

    <div class="col-lg-12">
        <h1 class="page-header"><?= \yii\helpers\Html::encode($this->title) ?></h1>
        <?= \cs\Widget\BreadCrumbs\BreadCrumbs::widget([
            'items' => [
                $this->title
            ]
        ]) ?>
        <hr>
    </div>
    <div class="col-lg-4">
        <div class="panel panel-default">
            <div class="panel-heading">Магазины</div>
            <div class="panel-body">
                <?php foreach (\app\models\Shop::query()->andWhere(['gs_unions_shop.is_active' => 1])->all() as $shop) { ?>
                    <?php
                    $s = new \app\models\Shop($shop);
                    ?>
                    <p><a href="<?= $s->getLink() ?>"><?= $s->get('name') ?></a></p>
                <?php } ?>
            </div>
        </div>
    </div>
    <div class="col-lg-8">
        <div class="panel panel-default">
            <div class="panel-heading">Каталог товаров</div>
            <div class="panel-body">
                <?= $this->render('_tree', ['rows' => $tree]) ?>
            </div>
        </div>
    </div>

    <form method="get" action="/shop/search">
        <div class="col-lg-12">

            <div class="input-group">
                <input type="text" class="form-control" size="20" name="text" placeholder="Поиск..."/>
              <span class="input-group-btn">
                <button class="btn btn-default" id="buttonSearch" type="submit">Найти</button>
              </span>
            </div>
            <!-- /input-group -->


        </div>
    </form>

    <?php
    /** @var array $shopList магазины в которых есть товары и которые активны is_active = 1 */
    $shopList = \app\models\Shop\Product::query()
        ->groupBy('gs_unions_shop.id')
        ->innerJoin('gs_unions_shop_tree', 'gs_unions_shop_tree.id = gs_unions_shop_product.tree_node_id')
        ->innerJoin('gs_unions_shop', 'gs_unions_shop.id = gs_unions_shop_tree.shop_id')
        ->select([
            'gs_unions_shop.*'
        ])

        ->all();

    ?>
    <?php foreach ($shopList as $i) { ?>
        <p><?= $shopList['name'] ?></p>
    <?php } ?>

    <div class="col-lg-12">
        <hr>
        <?= $this->render('../blocks/share', [
            'image'       => Url::to('/images/page/energy/1405027_571610319578558_903459749_o1.jpg', true),
            'url'         => Url::current([], true),
            'title'       => $this->title,
            'description' => 'В каждой точке вселенной находится сверхизбыток энергии, а значит на Земле присутствует
                Богатство Чистейшей Энергии.',
        ]) ?>
    </div>
</div>