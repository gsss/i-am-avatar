<?php
use yii\helpers\Html;
use yii\bootstrap\ButtonDropdown;
use yii\helpers\Url;
use cs\services\Url as csUrl;
use cs\services\Str;
use yii\widgets\Breadcrumbs;

/** @var $this yii\web\View */
/** @var $product     \app\models\Shop\Product */

$this->title = $product->getField('name');
$union = $product->getUnion();
$shop = $union->getShop();


$isShowWindow = 1;
if (Yii::$app->user->isGuest) {
    if (Yii::$app->session->get('shop.help_window_already_showed', 0) == 1) {
        $isShowWindow = 0;
    }
} else {
    if (Yii::$app->user->identity->get('is_show_shop_help_window', 0) == 0) {
        if (Yii::$app->session->get('shop.help_window_already_showed', 0) == 1) {
            $isShowWindow = 0;
        }
    } else {
        $isShowWindow = 0;
    }
}

$url = Url::to(['shop/basket']);
$urlAdd = Url::to(['shop/basket_add']);
$this->registerJs(<<<JS
    var isShowedWindow = false;
    $('.buttonAddToCart').click(function() {
        var id = $(this).data('id');
        ajaxJson({
            url: '{$urlAdd}',
            data: {
                id: id
            },
            success: function(ret) {
                var basket = $('#basketCount');
                var i2 = $('<i>', {class: 'fa fa-shopping-basket', style: 'margin-right:5px;'}).html();
                console.log(i);
                var span;
                if (basket.length > 0) {
                    var count = basket.html();
                    basket.html(i2 + ret);
                    span = $('#basketCount').popover({
                        content: 'Товар добавлен',
                        placement: 'bottom'
                    });
                    span.popover('show');
                    window.setTimeout(function () {
                        span.popover('hide');
                        if (!isShowedWindow) {
                            if ({$isShowWindow} == 1) {
                                GSSS.modal('#modalHelpShop2', {
                                    callbacks: {
                                        close: function() {
                                            if (!$('#isShowModalHelp').is(':checked')) {
                                                ajaxJson({
                                                    url: '/shop/setNoHelpWindow'
                                                });
                                            }
                                        }
                                    }
                                });
                            }
                        }
                        isShowedWindow = true;
                    }, 2000);
                } else {
                    var userMenu = $('#userBlockLi');
                    span = $('<span>', {
                                id: 'basketCount',
                                class: 'label label-success',
                                title: 'Корзина'
                            }).tooltip({placement:"bottom"}).html(i2 + ret).popover({
                        content: 'Товар добавлен',
                        placement: 'bottom'
                    });
                    var liBasket = $('<li>').append(
                        $('<a>', {
                            href: '{$url}',
                            style: 'padding-right: 0px;  padding-bottom: 0px;'
                        }). append(
                            span
                        )
                    );
                    liBasket.insertBefore(userMenu);
                    span.popover('show');
                    window.setTimeout(function () {
                        span.popover('hide');
                        if (!isShowedWindow) {
                            if ({$isShowWindow} == 1) {
                                GSSS.modal('#modalHelpShop2', {
                                    callbacks: {
                                        close: function() {
                                            if (!$('#isShowModalHelp').is(':checked')) {
                                                ajaxJson({
                                                    url: '/shop/setNoHelpWindow'
                                                });
                                            }
                                        }
                                    }
                                });
                            }
                        }
                        isShowedWindow = true;
                    }, 2000);
                }
            }
        });
    });
JS
);
?>
<div class="container">
    <div class="col-lg-12">
        <h1 class="page-header"><?= \yii\helpers\Html::encode($this->title) ?></h1>
        <?= \cs\Widget\BreadCrumbs\BreadCrumbs::widget([
            'items' => [
                [
                    'url' => 'shop/index',
                    'label' => 'Магазин'
                ],
                $product->getField('name'),
            ]
        ]) ?>
        <hr>
    </div>

    <div class="col-lg-4">
        <img class="img-thumbnail" src="<?= $product->getImage() ?>">
        <hr>
        <p>Цена: <?= Yii::$app->formatter->asDecimal($product->getField('price'), 0) ?> руб</p>
        <hr>
        <p><button class="btn btn-default buttonAddToCart" data-id="<?= $product->getId() ?>">В корзину</button></p>
        <hr>
        <p>Магазин:
            <a href="<?= $shop->getLink()?>">
                <?= $shop->getName() ?>
            </a>
        </p>
    </div>
    <div class="col-lg-8">
        <p><?= $product->getField('content') ?></p>

        <a
            class="btn btn-success btn-lg gsssTooltip"
            title="Быстрая покупка"
            href="<?= Url::to(['shop/order', 'product_id' => $product->getId()]) ?>"
            style="width: 100%; margin-bottom: 50px;margin-top: 50px;"
            >
            Купить
        </a>
        <?php if ($product->getImages()->exists()) { ?>
            <div class="row">
                <h2 class="page-header">Изображения</h2>
                <?php
                \app\assets\SlideShow\Asset::register($this);
                ?>
                <?php foreach ($product->getImages()->andWhere(['moderation_status' => 1])->all() as $item) { ?>
                    <div class="col-lg-4" style="margin-bottom: 20px;">
                        <a href="<?= \cs\Widget\FileUpload3\FileUpload::getOriginal($item['image']) ?>"
                           rel="lightbox[example]" class="highslide" onclick="return hs.expand(this)">
                            <img src="<?= $item['image'] ?>" width="100%">
                        </a>
                    </div>
                <?php } ?>
            </div>
        <?php } ?>



        <?php if ($product->getUnion()->getShop()->get('is_piramida', 0)) { ?>
            <hr>
            <p>
                Этот продукт участвует в партнерской программе.
                Зарабатывай на партнерской программе.
                Поделись ссылкой на товар и получи % с продаж.
            </p>
            <?php if (!Yii::$app->user->isGuest) { ?>
                <p>
                    <?= $this->render('../blocks/share', [
                        'image'       => Url::to(\cs\Widget\FileUpload3\FileUpload::getOriginal($product->getImage()), true),
                        'url'         => Url::current(['parent_id' => Yii::$app->user->id], true),
                        'title'       => $product->getField('name'),
                        'description' => \app\services\GsssHtml::getMiniText($product->getField('content')),
                        'text'        => 'Поделиться партнерской ссылкой',
                    ]) ?>
                </p>
                <p>Партнерская ссылка на этот товар:</p>
                <div class="input-group">
                    <input type="text" class="form-control"  value="<?= Url::current(['parent_id' => Yii::$app->user->id], true) ?>" id="link">
                        <span class="input-group-btn">
                            <a class="btn btn-default" href="<?= Url::to(['/cabinet/profile/wallet/referalLink', 'url' => Url::current(['parent_id' => Yii::$app->user->id], true)])?>">Сделать рассылку адреса</a>
                            <?php
                            \cs\assets\ZClip\Asset::register($this);
                            $p = \Yii::$app->assetManager->getBundle('cs\assets\ZClip\Asset')->baseUrl;
                            $this->registerJs(<<<JS
                                $(".buttonCopy").zclip({
                                    path: '{$p}' + '/ZeroClipboard.swf',
                                    copy: $('#link').val(),
                                    beforeCopy: function () {
                                    },
                                    afterCopy: function () {
                                        infoWindow('Скопировано');
                                    }
                                });
JS
                            );

                            ?>
                            <button class="btn btn-default buttonCopy">Копировать</button>
                        </span>
                </div><!-- /input-group -->
            <?php } else { ?>
                <p>Чтобы получить ссылку вам необходимо <a href="javascript:void(0);" class="buttonLogin">Войти</a></p>
            <?php } ?>

        <?php } else { ?>
            <hr>
            <?= $this->render('../blocks/share', [
                'image'       => \cs\Widget\FileUpload3\FileUpload::getOriginal(Url::to($product->getImage(), true), false),
                'url'         => Url::current([], true),
                'title'       => $product->getField('name'),
                'description' => $product->getField('description'),
            ]) ?>
        <?php } ?>
    </div>


</div>
<?php if ($isShowWindow) { ?>
    <?php
    \app\assets\ModalBoxNew\Asset::register($this);
    ?>
    <div id="modalHelpShop2" class="zoom-anim-dialog mfp-hide mfp-dialog">
        <h2>Как совершить покупку</h2>

        <div style="margin-top: 50px;margin-bottom: 50px;">
            <p>Сейчас выбранный вами товар попал в корзину.</p>
            <img src="/images/union_shop/product/modal_basket.png" class="thumbnail" style="width: 100%;">
            <p>Вы можете продолжить выбирать товары или перейти к <a href="/shop/order">оформлениею заказа</a>.</p>
            <p>Просмотреть <a href="/shop/basket">содержимое корзины</a> вы можете кликнув по зеленому значку.</p>
        </div>
        <?php
        \cs\assets\CheckBox\Asset::register($this);
        ?>
        <table>
            <tr>
                <td style="vertical-align:  center; padding-right: 10px;">
                    Показывать это окно в дальнейшем?
                </td>
                <td>
                    <input type="checkbox" data-toggle="toggle" data-on="Да" data-off="Нет" id="isShowModalHelp"
                           checked="checked">
                </td>
            </tr>
        </table>
    </div>
<?php } ?>
