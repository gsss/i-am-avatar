<?php
use yii\helpers\Html;
use yii\bootstrap\ButtonDropdown;
use yii\helpers\Url;
use cs\services\Url as csUrl;
use cs\services\Str;
use yii\widgets\Breadcrumbs;
use yii\helpers\ArrayHelper;

/** @var $this yii\web\View */
/** @var $text    string */
/** @var $items   array gs_unions_shop_product.* */

$this->title = 'Результат поиска';

$isShowWindow = 1;
if (Yii::$app->user->isGuest) {
    if (Yii::$app->session->get('shop.help_window_already_showed', 0) == 1) {
        $isShowWindow = 0;
    }
} else {
    if (Yii::$app->user->identity->get('is_show_shop_help_window', 0) == 0) {
        if (Yii::$app->session->get('shop.help_window_already_showed', 0) == 1) {
            $isShowWindow = 0;
        }
    } else {
        $isShowWindow = 0;
    }
}

$url = Url::to(['shop/basket']);
$urlAdd = Url::to(['shop/basket_add']);
$this->registerJs(<<<JS
    var isShowedWindow = false;
    $('.buttonAddToCart').click(function() {
        var id = $(this).data('id');
        ajaxJson({
            url: '{$urlAdd}',
            data: {
                id: id
            },
            success: function(ret) {
                var basket = $('#basketCount');
                var span;
                if (basket.length > 0) {
                    var count = basket.html();
                    basket.html(ret);
                    span = $('#basketCount').popover({
                        content: 'Товар добавлен',
                        placement: 'bottom'
                    });
                    span.popover('show');
                    window.setTimeout(function () {
                        span.popover('hide');
                        if (!isShowedWindow) {
                            if ({$isShowWindow} == 1) {
                                GSSS.modal('#modalHelpShop2', {
                                    callbacks: {
                                        close: function() {
                                            if (!$('#isShowModalHelp').is(':checked')) {
                                                ajaxJson({
                                                    url: '/shop/setNoHelpWindow'
                                                });
                                            }
                                        }
                                    }
                                });
                            }
                        }
                        isShowedWindow = true;
                    }, 2000);
                } else {
                    var userMenu = $('#userBlockLi');
                    span = $('<span>', {
                                id: 'basketCount',
                                class: 'label label-success',
                                title: 'Корзина'
                            }).tooltip({placement:"left"}).html(ret).popover({
                        content: 'Товар добавлен',
                        placement: 'bottom'
                    });
                    var liBasket = $('<li>').append(
                        $('<a>', {
                            href: '{$url}',
                            style: 'padding-right: 0px;  padding-bottom: 0px;'
                        }). append(
                            span
                        )
                    );
                    liBasket.insertBefore(userMenu);
                    span.popover('show');
                    window.setTimeout(function () {
                        span.popover('hide');
                        if (!isShowedWindow) {
                            if ({$isShowWindow} == 1) {
                                GSSS.modal('#modalHelpShop2', {
                                    callbacks: {
                                        close: function() {
                                            if (!$('#isShowModalHelp').is(':checked')) {
                                                ajaxJson({
                                                    url: '/shop/setNoHelpWindow'
                                                });
                                            }
                                        }
                                    }
                                });
                            }
                        }
                        isShowedWindow = true;
                    }, 2000);
                }
            }
        });
    });
JS
);
?>
<div class="container">

    <div class="col-lg-12">
        <h1 class="page-header"><?= \yii\helpers\Html::encode($this->title) ?></h1>
        <?= \cs\Widget\BreadCrumbs\BreadCrumbs::widget([
            'items' => [
                [
                    'label' => 'Магазин',
                    'url'   => ['shop/index'],
                ],
                $this->title
            ]
        ]) ?>
        <hr>

        <?= \yii\grid\GridView::widget([
            'dataProvider' => new \yii\data\ActiveDataProvider([
                'query'      => \app\models\Shop\Product::query()
                    ->select([
                        'gs_unions_shop_product.*',
                    ])
                    ->where(['like', 'gs_unions_shop_product.name', $text]),
                'pagination' => [
                    'pageSize' => 20,
                ],
            ]),
            'tableOptions' => [
                'class' => 'table table-striped table-hover',
            ],
            'rowOptions'   => function ($item) {
                return [
                    'data' => ['id' => $item['id']],
                    'role' => 'button',
                    'class' => 'rowTable'
                ];
            },
            'columns'      => [
                'id',
                [
                    'header'  => 'Картинка',
                    'content' => function ($item) {
                        $i = \yii\helpers\ArrayHelper::getValue($item, 'image', '');
                        if ($i == '') return '';

                        return Html::img($i, [
                            'class' => "thumbnail",
                            'width' => 80,
                            'style' => 'margin-bottom: 0px;',
                        ]);
                    }
                ],
                [
                    'header'  => 'Магазин',
                    'content' => function ($item) {
                        $uid = ArrayHelper::getValue($item, 'union_id', '');
                        if ($uid == '') return '';
                        $u = \app\models\Union::find($uid);
                        $shop = $u->getShop();

                        return \yii\bootstrap\Html::a($u->getName(), $u->getLink()) ;
                    }
                ],
                [
                    'header'  => 'Купить',
                    'content' => function ($item) {

                        return Html::button('Купить', ['class' => 'btn btn-success buttonAddToCart', 'data-id' => $item['id']]);
                    }
                ],
                'name:text:Название',
            ],
        ]) ?>
    </div>
</div>
<?php if ($isShowWindow) { ?>
    <?php
    \app\assets\ModalBoxNew\Asset::register($this);

    ?>
    <div id="modalHelpShop2" class="zoom-anim-dialog mfp-hide mfp-dialog">
        <h2>Как совершить покупку</h2>

        <div style="margin-top: 50px;margin-bottom: 50px;">
            <p>Сейчас выбранный вами товар попал в корзину.</p>
            <img src="/images/union_shop/product/modal_basket.png" class="thumbnail" style="width: 100%;">
            <p>Вы можете продолжить выбирать товары или перейти к <a href="/shop/order">оформлениею заказа</a>.</p>
            <p>Просмотреть <a href="/shop/basket">содержимое корзины</a> вы можете кликнув по зеленому значку.</p>
        </div>
        <?php
        \cs\assets\CheckBox\Asset::register($this);
        ?>
        <table>
            <tr>
                <td style="vertical-align:  center; padding-right: 10px;">
                    Показывать это окно в дальнейшем?
                </td>
                <td>
                    <input type="checkbox" data-toggle="toggle" data-on="Да" data-off="Нет" id="isShowModalHelp"
                           checked="checked">
                </td>
            </tr>
        </table>
    </div>
<?php } ?>
