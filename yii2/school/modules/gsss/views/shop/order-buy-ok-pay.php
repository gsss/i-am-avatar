<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;
use app\models\UnionCategory;
use yii\db\Query;
use app\modules\Shop\services\Basket;

/**
 * Форма оплаты
 */

/* @var $this yii\web\View */
/* @var $request \app\models\Shop\Request */

$this->title = 'Оформление заказа #' . $request->getId();

$union = $request->getUnion();
$shop = $union->getShop();

$this->registerJs(<<<JS
    $('#formPay').submit();
JS
);

?>
<div class="container">
    <div class="page-header">
        <h1><?= Html::encode($this->title) ?></h1>
    </div>


    <div class="row">
        <div class="col-lg-8 col-lg-offset-2">
            <h2 class="text-center">Идет перенаправление на оплату...</h2>

            <form method="POST" action="https://checkout.okpay.com/" name="form2" id="formPay">
                <input type="hidden" name="ok_receiver" value="OK414559097"/>
                <input type="hidden" name="ok_item_1_name" value="Заказ #<?= $request->getId() ?>"/>
                <input type="hidden" name="ok_item_1_price" value="<?= $request->getField('price') ?>"/>
                <input type="hidden" name="ok_currency" value="RUB"/>
                <input type="hidden" name="ok_invoice" value="<?= $request->getId() ?>"/>

                <div class="row">
                    <div class="col-lg-12" style="margin-top: 30px;">
                        <div class="btn-group" role="group" aria-label="...">
                            <input type="button" value="Перейти к оплате"
                                   class="btn btn-success btn-lg buttonSubmit" style="width: 400px; border: 2px solid white; border-radius: 20px;">
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>

</div>
