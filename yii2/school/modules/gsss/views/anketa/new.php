<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;
use app\models\UnionCategory;
use yii\db\Query;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \app\models\God2 */

$this->title = 'Анкета';

?>
<div class="container">
    <div class="page-header">
        <h1 class="text-center"><?= Html::encode($this->title) ?></h1>
    </div>
    <p>
        <img src="https://www.galaxysss.com/images/new_earth/index/header.jpg"  width="100%" class="thumbnail">
    </p>
        <div class="row">
            <div class="col-lg-3">
            </div>
            <div class="col-lg-6" style="margin-bottom: 200px;">
                <?php if (!is_null($id = Yii::$app->session->getFlash('anketa'))) : ?>

                    <div class="alert alert-success">
                        Успешно добавлено.
                    </div>

                <?php else: ?>

                    <?php $form = ActiveForm::begin([
                        'id'        => 'contact-form',
                        'options'   => ['enctype' => 'multipart/form-data']
                    ]); ?>
                    <?= $model->field($form, 'name_first') ?>
                    <?= $model->field($form, 'name_last') ?>
                    <?= $model->field($form, 'name_middle') ?>
                    <?= $model->field($form, 'contact_email') ?>
                    <?= $model->field($form, 'contact_phone') ?>
                    <?= $model->field($form, 'contact_vk') ?>
                    <?= $model->field($form, 'contact_fb') ?>
                    <?= $model->field($form, 'contact_telegram') ?>
                    <?= $model->field($form, 'contact_whatsapp') ?>
                    <?= $model->field($form, 'contact_skype')->hint('') ?>
                    <?= $model->field($form, 'age') ?>
                    <?= $model->field($form, 'place') ?>
                    <?= $model->field($form, 'vozmojnosti')->textarea(['rows' => 10]) ?>
                    <?= $model->field($form, 'opit')->textarea(['rows' => 10]) ?>

                    <hr>
                    <div class="form-group">
                        <?= Html::submitButton('Добавить', [
                            'class' => 'btn btn-success',
                            'name'  => 'contact-button',
                            'style' => 'width:100%',
                        ]) ?>
                    </div>
                    <?php ActiveForm::end(); ?>

                <?php endif; ?>
            </div>
        </div>

</div>
