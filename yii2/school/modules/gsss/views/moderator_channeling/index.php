<?php

use yii\helpers\Url;
use yii\helpers\Html;

$this->title = 'Послания';
$this->registerJs(<<<JS
    $('.buttonAccept').click(function(e) {
        e.stopPropagation();
        e.preventDefault();
        var button = $(this);
        ajaxJson({
            url: '/moderator/channeling/' + button.data('id') + '/accept',
            success: function(ret) {
                button.parent().parent().remove();
                infoWindow('Успешно');
            }
        })
    });
    $('.buttonReject').click(function(e) {
        e.stopPropagation();
        e.preventDefault();
        var button = $(this);
        $('#messageModal').modal('show');
        $('#buttonSendMessageForm').click(function() {
            ajaxJson({
                url: '/moderator/channeling/' + button.data('id') + '/reject',
                data: {
                    text: $('#messageModal textarea').val()
                },
                success: function(ret) {
                    button.parent().parent().remove();
                    $('#messageModal').modal('hide');
                    infoWindow('Успешно');
                }
            })
        });
    });
    $('.rowRow').click(function() {
        window.location = '/moderator/channeling/' + $(this).data('id') + '/view';
    });
JS
);

?>
<div class="container">
    <div class="col-lg-12">
        <h1 class="page-header">Послания</h1>
    </div>


    <div class="col-lg-12">
    <?= \yii\grid\GridView::widget([
        'tableOptions' => [
            'class' => 'table table-striped table-hover',
        ],
        'rowOptions' => function($item){
            return [
                'role' => 'button',
                'data-id' => $item['id'],
                'class' => 'rowRow',
            ];
        },
        'dataProvider' => new \yii\data\ActiveDataProvider([
            'query'      => \app\models\Chenneling::query([
                'or',
                ['gs_cheneling_list.moderation_status' => 2],
                ['gs_cheneling_list.moderation_status' => null],
            ])
                ->orderBy(['gs_cheneling_list.date_created' => SORT_DESC])
            ,
            'pagination' => [
                'pageSize' => 50,
            ],
        ]),
        'columns' => [
            'id',
            [
                'header' => 'Картинка',
                'content' => function($item) {
                    $i = \yii\helpers\ArrayHelper::getValue($item, 'img', '');
                    if ($i == '') return '';
                    return Html::img($i, ['width' => 100]);
                }
            ],
            [
                'header' => 'Пользователь',
                'content' => function($item) {
                    $template = '
<table>
    <tr>
        <td>
            {image}
        </td>
        <td style="padding-left: 20px;">
            {name}
        </td>
    </tr>
</table>
';
                    $i = \yii\helpers\ArrayHelper::getValue($item, 'user_id', '');
                    if ($i == '') return '';
                    $u = (new \app\services\UsersInCache())->find($i);
                    if (is_null($u)) return '';
                    if ($u == '') return '';

                    $image = Html::img($u['avatar'], [
                        'class' => 'img-circle',
                        'width' => '50',
                        'style' => 'margin-bottom:0px;',
                    ]);
                    $i = \yii\helpers\ArrayHelper::getValue($item, 'user_id', '');
                    if ($i == '') return '';
                    $u = (new \app\services\UsersInCache())->find($i);

                    $name = Html::a($u['name'], ['site/user', 'id' => $u['id']]);

                    $template = str_replace('{name}', $name,$template);
                    $template = str_replace('{image}', $image,$template);

                    return $template;
                }
            ],
            'name:text:Название',
            [
                'header'  => 'Дата',
                'content' => function ($item) {
                    return Html::tag('span', \app\services\GsssHtml::dateString($item['date_created']), ['style' => 'font-size: 80%; margin-bottom:10px; color: #c0c0c0;']);
                }
            ],
            [
                'header'  => 'Принять',
                'content' => function($item) {
                    return Html::button('Принять', ['class' => 'btn btn-success buttonAccept', 'data-id' => $item['id']]);
                }
            ],
            [
                'header'  => 'Отклонить',
                'content' => function($item) {
                    return Html::button('Отклонить', ['class' => 'btn btn-danger buttonReject', 'data-id' => $item['id']]);
                }
            ],
        ]
    ]) ?>
</div>
</div>

<div class="modal fade" id="messageModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Причина отклонения</h4>
            </div>
            <div class="modal-body">
                <textarea class="form-control" rows="10"></textarea>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" id="buttonSendMessageForm">Отправить</button>
            </div>
        </div>
    </div>
</div>