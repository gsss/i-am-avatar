<?php
use yii\helpers\Html;
use yii\bootstrap\ButtonDropdown;
use yii\helpers\Url;
use cs\services\Url as csUrl;
use cs\services\Str;

/* @var $this yii\web\View */
/* @var $item \app\models\Chenneling поля послания */

$this->title = $item->get('header');
$this->registerJs(<<<JS
    $('.buttonAccept').click(function(e) {
        e.stopPropagation();
        e.preventDefault();
        var button = $(this);
        ajaxJson({
            url: '/moderator/channeling/' + button.data('id') + '/accept',
            success: function(ret) {
                button.parent().parent().remove();
                infoWindow('Успешно');
            }
        })
    });
    $('.buttonReject').click(function(e) {
        e.stopPropagation();
        e.preventDefault();
        var button = $(this);
        $('#messageModal').modal('show');
        $('#buttonSendMessageForm').click(function() {
            ajaxJson({
                url: '/moderator/channeling/' + button.data('id') + '/reject',
                data: {
                    text: $('#messageModal textarea').val()
                },
                success: function(ret) {
                    button.parent().parent().remove();
                    $('#messageModal').modal('hide');
                    infoWindow('Успешно');
                }
            })
        });
    });
JS
);
?>
<div class="container">

    <div class="col-lg-12">
        <h1 class="page-header"><?= \yii\helpers\Html::encode($this->title) ?></h1>
    </div>

    <div class="col-lg-8" style="padding-bottom: 50px;">
        <?= $item->get('content') ?>

<!--        Показ пользователя -->
        <?php $author = $item->getUser(); ?>
        <?php if (is_null($author)) { ?>
            <hr>
            <p>Автор: <img src="<?= $author->getAvatar() ?>" width="100" class="img-center"> <a href="<?= $author->getLink() ?>"><?= $author->getName2() ?></a>
        <?php } ?>
        <hr>
        <?php if ($item->get('source')): ?>
            <?php if ($item->get('source') != ''): ?>
                <?= Html::a('Ссылка на источник »', $item->get('source'), [
                    'class'  => 'btn btn-primary',
                    'target' => '_blank',
                ]) ?>
            <?php endif; ?>
        <?php endif;
        $image = $item->get('img') . '';
        ?>

        <button class="btn btn-success buttonAccept btn-lg" data-id="<?= $item->getId() ?>">Одобрить</button>
        <button class="btn btn-danger buttonReject btn-lg" data-id="<?= $item->getId() ?>">Отклонить</button>
    </div>
</div>

<div class="modal fade" id="messageModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Причина отклонения</h4>
            </div>
            <div class="modal-body">
                <textarea class="form-control" rows="10"></textarea>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" id="buttonSendMessageForm">Отправить</button>
            </div>
        </div>
    </div>
</div>