<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;
use app\models\UnionCategory;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $query \yii\db\Query */
/* @var $union \app\models\Union */
/* @var $product \app\models\Shop\Product */

$this->title = 'Изображения';
?>
<div class="container">
    <h1 class="page-header"><?= Html::encode($this->title) ?></h1>
    <?= \cs\Widget\BreadCrumbs\BreadCrumbs::widget([
        'items' => [
            [
                'label' => 'Объединения',
                'url'   => Url::to(['cabinet/objects']),
            ],
            [
                'label' => $union->getName(),
                'url'   => Url::to(['cabinet/objects_edit', 'id' => $union->getId()]),
            ],
            [
                'label' => 'Магазин',
                'url'   => Url::to(['cabinet_shop_shop/index', 'id' => $union->getId()]),
            ],
            [
                'label' => 'Товары',
                'url'   => Url::to(['cabinet_shop_shop/product_list', 'id' => $union->getId()]),
            ],
            [
                'label' => $product->getField('name'),
                'url'   => Url::to(['cabinet_shop_shop/product_list_edit', 'id' => $product->getId()]),
            ],
            $this->title
        ],
        'home'  => [
            'name' => 'я',
            'url'  => Url::to(['site/user', 'id' => Yii::$app->user->id])
        ]
    ]) ?>
    <hr>

    <?= \yii\grid\GridView::widget([
        'tableOptions' => [
            'class' => 'table table-striped table-hover',
        ],
        'rowOptions'   => function ($item) {
            return [
                'data' => ['id' => $item['id']],
                'role' => 'button',
                'class' => 'rowTable'
            ];
        },
        'dataProvider' => new \yii\data\ActiveDataProvider([
            'query'      => $query
                ->select([
                    'id',
                    'name',
                    'image',
                ])
            ->orderBy(['sort_index' => SORT_ASC])
            ,
            'pagination' => [
                'pageSize' => 50,
            ],
        ]),
        'columns' => [
            'id',
            [
                'header' => 'Картинка',
                'content' => function($item) {
                    $i = \yii\helpers\ArrayHelper::getValue($item, 'image', '');
                    if ($i == '') return '';
                    return Html::img($i, ['width' => 100, 'style' => 'margin-top: 0px;']);
                }
            ],
            'name',
            'moderation_status',
            [
                'header' => 'Удалить',
                'content' => function($item) {
                    return Html::button('Удалить', ['class' => 'btn btn-danger btn-xs buttonDelete', 'data-id' => $item['id']]);
                }
            ],
        ]
    ]) ?>
    <?php
    $this->registerJs(<<<JS
    $('.buttonDelete').click(function (e) {
        e.preventDefault();
        e.stopPropagation();
        if (confirm('Подтвердите удаление')) {
            var button = $(this);
            var id = $(this).data('id');
            ajaxJson({
                url: '/cabinet/shop/productList/images/' + id + '/delete',
                success: function (ret) {
                    infoWindow('Успешно', function() {
                        button.parent().parent().remove();
                    });
                }
            });
        }
    });
    $('.rowTable').click(function() {
        window.location = '/cabinet/shop/productList/images/' + $(this).data('id') + '/edit';
    });
JS
);
    ?>
    <hr>
    <a href="<?= \yii\helpers\Url::to(['cabinet_shop_shop_images/add', 'id' => $product->getId()]) ?>" class="btn btn-default">Добавить</a>
</div>
