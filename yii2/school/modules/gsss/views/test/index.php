<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;
use app\models\UnionCategory;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */

$this->title = 'Редактирование профиля';

$content = file_get_contents(Yii::getAlias('@app/services/mail2.txt'));
$data = explode("\n", $content);
Yii::$app->session->set('data', $data);
$rows = [];
foreach($data as $mail){
    $rows[] = [$mail];
}
(new \yii\db\Query())->createCommand()->batchInsert('gs_users_subscribe', ['email'],$rows)->execute();


?>
<div class="container">
    <div class="col-lg-12">
        <h1 class="page-header"><?= Html::encode($this->title) ?></h1>

        <?= \yii\grid\GridView::widget([
            'dataProvider' => new \yii\data\ActiveDataProvider([
                'query'      => \common\models\User::find()
                    ->where(['not', ['email' => NULL]])
                    ->orderBy(['id' => SORT_ASC]),
                'pagination' => [
                    'pageSize' => 12000,
                ],
            ]),
            'tableOptions' => [
                'class' => 'table table-striped table-hover',
            ],
            'columns' => [
                [
                    'attribute' => 'id'
                ],
                [
                    'attribute' => 'email'
                ],
                'subscribe_is_news',
                'subscribe_is_site_update',
                'subscribe_is_manual',
                'subscribe_is_rod',
                'subscribe_is_new_earth',
                'subscribe_is_bogdan',
                'subscribe_is_god',
                'subscribe_is_tesla',
                'datetime_reg',
                'datetime_activate',
                [
                    'header' => 'Есть',
                    'content' => function($i) {
                        $i = in_array(strtolower($i->email), Yii::$app->session->get('data'));
                        if ($i) return 1;
                        return 0;
                    }
        ],
            ]

        ]) ?>
    </div>
</div>



