<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;
use app\models\UnionCategory;
use yii\db\Query;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model cs\base\BaseForm */

$this->title = 'Добавить генный ключ';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="container">
    <div class="page-header">
        <h1><?= Html::encode($this->title) ?></h1>
    </div>

    <?php if (Yii::$app->session->hasFlash('contactFormSubmitted')): ?>

        <div class="alert alert-success">
            Успешно добавлено.
        </div>

    <?php else: ?>


        <div class="row">
            <div class="col-lg-5">
                <?php $form = ActiveForm::begin([
                    'id'      => 'contact-form',
                    'options' => ['enctype' => 'multipart/form-data']
                ]); ?>
                <?= $model->field($form, 'num') ?>
                <?= $model->field($form, 'name') ?>
                <?= $model->field($form, 'ten') ?>
                <?= $model->field($form, 'dar') ?>
                <?= $model->field($form, 'siddhi') ?>
                <?= $model->field($form, 'codon') ?>
                <?= $model->field($form, 'fiz') ?>
                <?= $model->field($form, 'amin') ?>
                <?= $model->field($form, 'patern') ?>
                <?= $model->field($form, 'image') ?>
                <?= $model->field($form, 'image_codon') ?>
                <?= $model->field($form, 'image_graph') ?>
                <?= $model->field($form, 'content') ?>
                <?= $model->field($form, 'content_ten') ?>
                <?= $model->field($form, 'content_dar') ?>
                <?= $model->field($form, 'content_siddhi') ?>

                <hr>
                <div class="form-group">
                    <?= Html::submitButton('Добавить', [
                        'class' => 'btn btn-default',
                        'name'  => 'contact-button',
                        'style' => 'width:100%',
                    ]) ?>
                </div>
                <?php ActiveForm::end(); ?>
            </div>
        </div>

    <?php endif; ?>
</div>
