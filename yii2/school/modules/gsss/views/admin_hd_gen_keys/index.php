<?php

use yii\helpers\Url;
use app\services\GsssHtml;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;

$this->title = 'Генные Ключи';

$this->registerJs(<<<JS
$('.buttonDelete').click(function (e) {
        e.preventDefault();
        if (confirm('Подтвердите удаление')) {
            var id = $(this).data('id');
            ajaxJson({
                url: '/admin/hdGenKeys/' + id + '/delete',
                success: function (ret) {
                    infoWindow('Успешно', function() {
                        $('#newsItem-' + id).remove();
                    });
                }
            });
        }
    });

    $('.rowTable').click(function() {
        window.location = '/admin/hdGenKeys/' + $(this).data('id') + '/edit';
    });

JS
);
?>
<div class="container">
    <div class="col-lg-12">
        <h1 class="page-header"><?= Html::encode($this->title) ?></h1>
        <a href="<?= Url::to(['admin_hd_gen_keys/add']) ?>" class="btn btn-default" style="margin-bottom: 20px;">Добавить</a>

        <?= \yii\grid\GridView::widget([
            'dataProvider' => new \yii\data\ActiveDataProvider([
                'query'      => \app\models\HdGenKeys::query()->orderBy(['num' => SORT_ASC]),
            ]),
            'tableOptions' => [
                'class' => 'table table-striped table-hover',
            ],
            'rowOptions'   => function ($item) {
                return [
                    'data' => ['id' => $item['id']],
                    'role' => 'button',
                    'class' => 'rowTable'
                ];
            },
            'columns'      => [
                'num',
                [
                    'header'  => 'Картинка',
                    'content' => function ($item) {
                        $i = ArrayHelper::getValue($item, 'img', '');
                        if ($i == '') return '';

                        return Html::img($i, [
                            'class' => "thumbnail",
                            'width' => 80,
                            'style' => 'margin-bottom: 0px;'
                        ]);
                    }
                ],
                'ten:text:Тень',
                'dar:text:Дар',
                'siddhi:text:Сиддхи',
                [
                    'header'  => 'Удалить',
                    'content' => function ($item) {
                        return Html::button('Удалить', [
                            'class' => 'btn btn-danger btn-xs buttonDelete',
                            'data'  => [
                                'id' => $item['id'],
                            ]
                        ]);
                    }
                ],
            ],
        ]) ?>
    </div>
</div>