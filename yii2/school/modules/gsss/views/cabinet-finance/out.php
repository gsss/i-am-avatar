<?php

/** $this \yii\web\View  */

use yii\helpers\Url;
use app\services\GsssHtml;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use app\services\GetArticle\Collection;

$this->title = 'Заявки на вывод средств';

$this->registerJS(<<<JS
    $('.buttonAnswer').click(function (e) {
         e.preventDefault();
         e.stopPropagation();
         $('#buttonSendMessageForm').data('id', $(this).data('id'));
         $('#messageModal').modal();
    });
    $('.buttonHide').click(function (e) {
         e.preventDefault();
         e.stopPropagation();
         var b = $(this);
         if (confirm('Вы подтверждаете свое действие?')) {
             ajaxJson({
                url: '/cabinet-finance/out-messages-hide',
                data: {
                    id: b.data('id')
                },
                success: function(ret) {
                    b.parent().parent().remove();
                }
             });
         }
    });

    $('.rowTable').click(function() {
        window.location = '/cabinet-finance/out-messages/' + $(this).data('id');
    });

JS
);
?>

<div
    class="modal fade"
    id="messageModal"
    tabindex="-1"
    role="dialog"
    aria-labelledby="myModalLabel"
    aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                        aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Сообщение</h4>
            </div>
            <div class="modal-body">
                <textarea class="form-control" rows="10"></textarea>
                <hr>
                <?php
                $this->registerJs(<<<JS
                    $('#buttonSendMessageForm').click(function() {
                        var requestId = $(this).data('id');
                        var text = $('#messageModal textarea').val();
                        ajaxJson({
                            url: '/cabinet-finance/out-messages/'+requestId+'/confirm',
                            data: {
                                message: text
                            },
                            success: function(ret) {
                                $('#messageModal').modal('hide');
                                window.location.reload();
                            }
                        });
                    });
JS
                );
                ?>
                <button class="btn btn-primary" style="width:100%;" id="buttonSendMessageForm">Отправить</button>
            </div>
        </div>
    </div>
</div>
<style>
    a.asc:after {
        content: ' ↓';
        display: inline;
    }

    a.desc:after {
        content: ' ↑';
        display: inline;
    }
</style>

<div class="container">
    <div class="col-lg-12">
        <h1 class="page-header">
            <?= Html::encode($this->title) ?>
        </h1>
        <?php
        $sort = new \yii\data\Sort([
            'attributes'   => [
                'datetime' => [
                    'default' => SORT_DESC,
                    'label'   => 'Создана',
                ],
                'last_message_time' => [
                    'default' => SORT_DESC,
                    'label'   => 'Последнее сообщение',
                ],
                'price'    => [
                    'default' => SORT_ASC,
                ],
                'id'       => [
                    'default' => SORT_ASC,
                ],
                'is_paid'  => [
                    'default' => SORT_DESC,
                    'label'   => 'Оплачена?',
                ],
            ],
            'defaultOrder' => [
                'is_paid'  => SORT_ASC,
                'datetime' => SORT_DESC,
            ],
        ]);
        $model = new \app\models\search\PiramidaOutRequest();
        $w = [
         'and',
            ['not', ['is_paid' => 1]],
            [
                'or',
                ['nw_out_requests.is_hide' => 0],
                [
                    'nw_out_requests.is_hide'               => 1,
                    'nw_out_requests.is_answer_from_client' => 1,
                ]
            ]
        ];
        $provider = $model->search(Yii::$app->request->get(), $w, $sort);
        ?>
        <?php \yii\widgets\Pjax::begin() ?>
        <?= \yii\grid\GridView::widget([
            'dataProvider' => $provider,
            'filterModel'  => $model,
            'tableOptions' => [
                'class' => 'table table-striped table-hover',
            ],
            'rowOptions'   => function ($item) {
                return [
                    'data'  => ['id' => $item['id']],
                    'role'  => 'button',
                    'class' => 'rowTable'
                ];
            },
            'columns'      => [
                [
                    'headerOptions' => [
                        'style' => Html::cssStyleFromArray([
                            'width' => '5%',
                        ])
                    ],
                    'attribute'     => 'id',
                    'header'        => $sort->link('id'),
                ],
                [
                    'header'  => 'Пользователь',
                    'content' => function ($item) {
                        $user_id = ArrayHelper::getValue($item, 'user_id');
                        $u = \app\services\UsersInCache::find($user_id);
                        if (is_null($u)) return '';
                        $arr = [];
                        $arr[] = Html::img($u['avatar'], ['width' => 50, 'class' => 'thumbnail', 'style' => 'margin-bottom: 0px; margin-right: 10px;float: left;']);
                        $arr[] = Html::a($u['name'], '/user/' . $u['id']);
                        $arr[] = '<br>';
                        $arr[] = '<small style="color: #888;">' . $u['email'] . '</small>';

                        return join('', $arr);
                    },
                    'attribute' => 'user',
                ],
                'price:currency:Сумма',
                [
                    'header'  => $sort->link('datetime'),
                    'content' => function ($item) {
                        $v = \yii\helpers\ArrayHelper::getValue($item, 'datetime', 0);
                        if ($v == 0) return '';

                        return Html::tag('abbr', \cs\services\DatePeriod::back($v, ['isShort' => true]), ['class' => 'gsssTooltip', 'title' => Yii::$app->formatter->asDatetime($v)]);
                    }
                ],
                [
                    'header'  => $sort->link('last_message_time'),
                    'content' => function ($item) {
                        $v = \yii\helpers\ArrayHelper::getValue($item, 'last_message_time', 0);
                        if ($v == 0) return '';

                        return Html::tag('abbr', \cs\services\DatePeriod::back($v, ['isShort' => true]), ['class' => 'gsssTooltip', 'title' => Yii::$app->formatter->asDatetime($v)]);
                    }
                ],
                [
                    'header'  => $sort->link('is_paid'),
                    'filter'        => [
                        0 => 'Не оплачена',
                        1 => 'Оплачена',
                    ],
                    'content' => function ($item) {
                        $v = ArrayHelper::getValue($item, 'is_paid', 0);
                        if ($v == 0) return Html::tag('span', 'Нет', ['class' => 'label label-default']);

                        return Html::tag('span', 'Да', ['class' => 'label label-success']);
                    },
                    'attribute' => 'is_paid',
                ],
                [
                    'header'  => 'Подтвердить оплату',
                    'content' => function ($item) {
                        return Html::button('Подтвердить', [
                            'class'   => 'btn btn-success buttonAnswer',
                            'data-id' => ArrayHelper::getValue($item, 'id')
                        ]);
                    },
                ],
                [
                    'header'  => 'Скрыть',
                    'content' => function ($item) {
                        return Html::button('Скрыть', [
                            'class'   => 'btn btn-default btn-xs buttonHide',
                            'data-id' => ArrayHelper::getValue($item, 'id')
                        ]);
                    },
                ],
            ],
        ]) ?>
        <?php \yii\widgets\Pjax::end() ?>

    </div>
</div>