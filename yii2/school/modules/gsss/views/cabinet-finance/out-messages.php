<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;
use app\models\UnionCategory;

/* @var $this yii\web\View */
/* @var $request \app\models\Piramida\OutRequest */

$this->title = 'Заявка на вывод';
?>
<div class="container">
    <div class="col-lg-8 col-lg-offset-2">
        <h1 class="page-header"><?= Html::encode($this->title) ?></h1>
        <?= \yii\widgets\DetailView::widget([
            'model' => $request,
            'attributes' => [
                'id',
                'datetime:datetime:Создано',
                'destination:text:Назначение',
                [
                    'label'  => 'Оплачен?',
                    'value' => (\yii\helpers\ArrayHelper::getValue($request, 'is_paid', 0) == 1) ? 'Да':'Нет',
                    'attribute' => 'is_paid',
                ],
                'price:currency:Сумма',
            ],
        ])?>


        <h3 class="page-header">Комментарии</h3>

        <style>
            .thumbnail {
                padding:0px;
            }
            .panel {
                position:relative;
            }
            .panel>.panel-heading:after,.panel>.panel-heading:before{
                position:absolute;
                top:11px;left:-16px;
                right:100%;
                width:0;
                height:0;
                display:block;
                content:" ";
                border-color:transparent;
                border-style:solid solid outset;
                pointer-events:none;
            }
            .panel>.panel-heading:after{
                border-width:7px;
                border-right-color:#f7f7f7;
                margin-top:1px;
                margin-left:2px;
            }
            .panel>.panel-heading:before{
                border-right-color:#ddd;
                border-width:8px;
            }
        </style>

        <div id="messageList">
            <?php
            $messages = $request->getMessages()
                ->orderBy(['datetime' => SORT_DESC])
                ->limit(20)
                ->all()
            ;
            $messages = array_reverse($messages);
            /** @var $message \app\models\Piramida\OutRequestMessage */
            ?>
            <?php foreach($messages as $message) { ?>
                <?= $this->render('../cabinet_wallet/out-item-message', [
                    'message' => $message,
                    'user'    => ($message->direction == \app\models\Piramida\OutRequestMessage::DIRECTION_TO_SHOP) ? \app\services\UsersInCache::find($message->user_id) : \app\controllers\CabinetFinanceController::getModeratorUser(),
                ]) ?>
            <?php } ?>
        </div>

        <div class="row">
            <div class="col-lg-10 col-lg-offset-2">
                <textarea class="form-control" rows="4" id="formMessage"></textarea>
                <?php
                $requestId = $request->id;
                $this->registerJs(<<<JS
                        $('#buttonSend').click(function() {
                            if ($('#formMessage').val() == '') {
                                infoWindow('Необходимо ввести текст');
                                return false;
                            }
                            ajaxJson({
                                url: '/cabinet-finance/out-messages/{$requestId}/message',
                                data: {
                                    message: $('#formMessage').val()
                                },
                                success: function(ret) {
                                    var html = $(ret.html);
                                    html.find('.gsssTooltip').tooltip();
                                    $('#messageList').append(html);
                                    $('#formMessage').val('');
                                }
                            })
                        });
JS
                );
                ?>
                <button class="btn btn-success" style="width: 100%; margin-top: 10px;" id="buttonSend">Ответить</button>
            </div>
        </div>

    </div>
</div>

