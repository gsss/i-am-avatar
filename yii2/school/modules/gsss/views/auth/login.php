<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\LoginForm */

$this->title = 'Вход в измерение личного счастья';
$this->params['breadcrumbs'][] = $this->title;
?>
<style>
    #passwordrecover-email {

    }
</style>
<div class="container">

    <div class="col-lg-12">
        <h1 class="page-header text-center"><?= Html::encode($this->title) ?></h1>
    </div>
    <div class="col-lg-4 col-lg-offset-4">
        <p>Пожалуйста заполните следующие поля для входа:</p>

        <?php $form = ActiveForm::begin([
            'id'          => 'login-form',
        ]); ?>

        <?= $form->field($model, 'username')->label('Email') ?>

        <?= $form->field($model, 'password', [
            'inputTemplate' => '<div class="input-group input-group">
                        {input}
                            <span class="input-group-btn">
                                <a class="btn btn-default gsssTooltip" type="button" title="Восстановить пароль" href="/password/recover">
                                    <span class="glyphicon glyphicon-question-sign"></span>
                                </a>
                            </span>
                    </div>'
        ])->passwordInput()->label('Пароль') ?>

        <?= $model->field($form, 'rememberMe') ?>

        <div class="form-group">
                <?= Html::submitButton('Вход', [
                    'class' => 'btn btn-success',
                    'name'  => 'login-button',
                    'style' => 'width:100%',
                ]) ?>
        </div>
        <p><a href="/registration" class="btn btn-default" style="width: 100%;">Регистрация</a></p>
        <p>Войти через соц. сеть</p>
        <?= \yii\authclient\widgets\AuthChoice::widget([
            'baseAuthUrl' => ['auth/auth']
        ]); ?>


        <?php ActiveForm::end(); ?>
    </div>

</div>