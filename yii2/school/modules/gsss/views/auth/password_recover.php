<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \app\models\Form\Registration */

$this->title = 'Восстановление пароля';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="container">

    <h1 class="page-header text-center"><?= Html::encode($this->title) ?></h1>

    <?php if (Yii::$app->session->hasFlash('contactFormSubmitted')): ?>

        <div class="alert alert-success">
            Перейдите на почту указанную Вами. Туда были отправлены инструкции для восстановления пароля.
        </div>

    <?php else: ?>

        <div class="row">
            <div class="col-lg-4 col-lg-offset-4">
                <?php $form = ActiveForm::begin([
                    'id' => 'contact-form',
                    'enableAjaxValidation' => true,
                ]); ?>
                <?= $form->field($model, 'email', ['inputOptions' => ['placeholder' => 'email']])->label('Почта', ['class' => 'hide']) ?>
                <?= $form->field($model, 'verifyCode')->widget('\cs\Widget\Google\reCaptcha\reCaptcha') ?>
                <div class="form-group">
                    <?= Html::submitButton('Восстановить', [
                        'class' => 'btn btn-primary',
                        'name'  => 'contact-button',
                        'style' => 'width:100%'
                    ]) ?>
                </div>
                <?php ActiveForm::end(); ?>
            </div>
        </div>

    <?php endif; ?>
</div>