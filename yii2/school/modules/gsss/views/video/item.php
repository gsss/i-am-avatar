<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;

/* @var $this yii\web\View */
/* @var $item \app\models\Help */

$this->title = $item->get('name');
?>
<div class="container">
    <div class="col-lg-12">
        <h1 class="page-header text-center"><?= $this->title ?></h1>
    </div>
    <div class="col-lg-8 col-lg-offset-2">
        <?php if ($item->get('content_video')) { ?>
            <?= $item->get('content_video') ?>
        <?php } else { ?>
            <p><img src="<?= $item->get('image') ?>" width="100%" class="thumbnail"></p>
            <?php if ($item->get('url')) { ?>
                <p><a href="<?= $item->get('url') ?>" target="_blank"><?= $item->get('url') ?></a></p>
            <?php } ?>
        <?php } ?>
        <hr>
        <?= $item->get('content') ?>
        <?php if ($item->get('torrent_file', '') != '') { ?>
            <hr>
            <p>
                <a href="<?= $item->get('torrent_file', '') ?>">
                    <img src="/images/video/item/torrent.png" width="50">
                </a>
            </p>
        <?php } ?>
    </div>
</div>

<?php $this->render('../blocks/shareMeta', [
    'image'       => $item->getImage(true),
    'url'         => \yii\helpers\Url::current([], true),
    'title'       => $this->title,
    'description' => \app\services\GsssHtml::getMiniText($item->get('content')),
]) ?>
