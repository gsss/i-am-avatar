<?php

/**
 * params:
 * [
 *      'rows'     => [
 *                       [
 *                            'id'                 => '524'
 *                            'name'               => '123456'
 *                            ...
 *                            'selected'           => bool
 *                        ], ...
 *                    ]
 *     'formName'          => $this->model->formName(), // string
 *     'model'             => $this->model,             // \yii\base\Model
 *     'attrId'            => $this->attrId,            // attribute id
 *     'attrName'          => $this->attrName,          // attribute name
 *     'templateVariables' => []
 * ]
 */

use yii\helpers\ArrayHelper;

/** @var $rows array */

?>
<ul>
    <?php foreach ($rows as $item) { ?>
        <li>
            <a href="/video/category/<?= $item['id'] ?>"><?= $item['name'] ?></a>
            <?php if (ArrayHelper::getValue($item, 'count')) { ?>
                (<?= ArrayHelper::getValue($item, 'count') ?>)
            <?php } ?>
        </li>
        <?php if (isset($item['nodes'])) { ?>
            <?= $this->render('_tree', ['rows' => $item['nodes']]) ?>
        <?php } ?>
    <?php } ?>
</ul>
