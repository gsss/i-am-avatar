<?php

use yii\helpers\Url;
use app\services\GsssHtml;
use yii\helpers\Html;

$this->title = 'Входящие платежи';

?>
<style>

    a.asc:after {
        content: ' ↓';
        display: inline;
    }

    a.desc:after {
        content: ' ↑';
        display: inline;
    }
    .tableIncome td {
        font-family: "Courier New", Courier, monospace;
    }

</style>

<div class="container">
    <div class="col-lg-12">
        <h1 class="page-header"><?= $this->title ?></h1>
        <?php
        $sort = new \yii\data\Sort([
            'attributes' => [
                'id',
                'withdraw_amount' => [
                    'default' => SORT_DESC,
                    'label'   => 'Оплатил Клиент',
                ],
                'amount' => [
                    'default' => SORT_DESC,
                    'label'   => 'Пришло в компанию',
                ],
                'date_insert',
                'datetime',
            ],
            'defaultOrder' => [
                'date_insert' => SORT_DESC,
            ],
        ]);
        ?>
        <?php \yii\widgets\Pjax::begin() ?>
        <?= \yii\grid\GridView::widget([
            'dataProvider' => new \yii\data\ActiveDataProvider([
                'query'      => \app\models\Shop\Payments::find(),
                'sort'       => $sort,
                'pagination' => [
                    'pageSize' => 50,
                ],
            ]),
            'tableOptions' => [
                'class' => 'table table-striped table-hover tableIncome',
            ],
            'rowOptions'   => function ($item) {
                $class = ['rowTable'];
                if ($item['is_valid'] == 0) {
                    $class[] = 'danger';
                }

                return [
                    'data'  => ['id' => $item['id']],
                    'role'  => 'button',
                    'class' => join(' ', $class),

                ];
            },
            'columns' => [
                [
                    'attribute'      => 'id',
                    'contentOptions' => [
                        'style' => 'text-align: right;',
                    ],
                ],
                [
                    'attribute'      => 'amount',
                    'header'         => $sort->link('amount'),
                    'contentOptions' => [
                        'style'  => 'text-align: right;',
                        'nowrap' => 'nowrap',
                    ],
                    'format'         => ['decimal', 2],
                ],
                [
                    'attribute'      => 'withdraw_amount',
                    'header'         => $sort->link('withdraw_amount'),
                    'contentOptions' => [
                        'style'  => 'text-align: right;',
                        'nowrap' => 'nowrap',
                    ],
                    'format'         => ['decimal', 2],
                ],
                [
                    'attribute'      => 'operation_label',
                    'contentOptions' => [
                        'nowrap' => 'nowrap',
                    ],
                ],
                [
                    'attribute'      => 'operation_id',
                    'contentOptions' => [
                        'nowrap' => 'nowrap',
                    ],
                ],
                [
                    'attribute'      => 'label',
                    'contentOptions' => [
                        'nowrap' => 'nowrap',
                    ],
                ],



                [
                    'attribute'      => 'datetime',
                    'format'         => 'datetime',
                    'contentOptions' => [
                        'nowrap' => 'nowrap',
                    ],
                ],
                [
                    'attribute'      => 'date_insert',
                    'format'         => 'datetime',
                    'contentOptions' => [
                        'nowrap' => 'nowrap',
                    ],
                ],
            ],
        ]) ?>
        <?php \yii\widgets\Pjax::end() ?>
    </div>

</div>