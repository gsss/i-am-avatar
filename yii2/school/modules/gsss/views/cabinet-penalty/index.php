<?php

/** $this \yii\web\View  */

use yii\helpers\Url;
use app\services\GsssHtml;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use app\services\GetArticle\Collection;

$this->title = 'Штрафные квитанции';

$controller = 'cabinet-penalty'
?>

<div class="container">
    <div class="col-lg-12">
        <h1 class="page-header">
            <?= Html::encode($this->title) ?>
        </h1>
        <div class="btn-group" style="margin-bottom: 20px;" role="group" aria-label="...">
            <a href="<?= Url::to([$controller . '/get']) ?>" class="btn btn-default">Получить</a>
        </div>
        <?php \yii\widgets\Pjax::begin(); ?>
        <?php
        $this->registerJS(<<<JS
$('.rowTable').click(function() {
    window.location = '/{$controller}' + '/edit/' + $(this).data('id');
});
$('.myRow').click(function() {
    window.location = '/penalty/' + $(this).data('id_penalty');
});
$('.buttonClose').click(function(e) {
    e.preventDefault();
    e.stopPropagation();
    if (confirm('Подтвердите свое намерение')) {
        ajaxJson({
            url: '/cabinet-penalty/close',
            data: {
                id: $(this).data('id')
            },
            success: function(ret) {
                window.location.reload();
            }
            
        });
    }
});
$('.buttonDownload').click(function(e) {
    e.preventDefault();
    e.stopPropagation();
    window.location = '/cabinet-penalty/download' + '?' + 'id' + '=' + $(this).data('id')
});
JS
        );
        ?>
        <style>
            a.asc:after {
                content: ' ↓';
                display: inline;
            }

            a.desc:after {
                content: ' ↑';
                display: inline;
            }
        </style>
        <?php
        $sort = new \yii\data\Sort([
            'defaultOrder' => [
                'date_insert' => SORT_DESC,
            ],
        ]);
        ?>
        <?= \yii\grid\GridView::widget([
            'dataProvider' => new \yii\data\ActiveDataProvider([
                'query'      => \common\models\Penalty::find()
                    ->andWhere(['user_id' => Yii::$app->user->id])
                ,
                'pagination' => [
                    'pageSize' => 20,
                ],
                'sort'       => $sort,
            ]),
            'tableOptions' => [
                'class' => 'table table-striped table-hover',
            ],
            'rowOptions'   => function (\common\models\Penalty $item) {
                if ($item->is_closed) {
                    $class = 'myRow';
                } else {
                    $class = 'rowTable';
                }
                $data = [
                    'data'  => [
                        'id'         => ArrayHelper::getValue($item, 'id'),
                        'id_penalty' => ArrayHelper::getValue($item, 'id_penalty'),
                    ],
                    'role'  => 'button',
                    'class' => $class,
                ];

                return $data;
            },
            'columns'      => [
                'id_penalty:text:id',
                'identy:text:Виновник',
                [
                    'header'    => $sort->link('reason'),
                    'content'   => function (\common\models\Penalty $item) {
                        return GsssHtml::getMiniText($item->reason);
                    },
                    'attribute' => 'reason',

                ],
                'date_insert:datetime:Добавлено',
                'date:date:Дата события',
                [
                    'header'  => 'Оплачена?',
                    'content' => function (\common\models\Penalty $item) {
                        $v = \yii\helpers\ArrayHelper::getValue($item, 'is_paid', 0);
                        if ($v == 0) return Html::tag('span', 'Нет', ['class' => 'label label-default']);

                        return Html::tag('span', 'Да', ['class' => 'label label-success']);
                    },
                ],
                [
                    'header'  => 'Скачать',
                    'content' => function (\common\models\Penalty $item) {
                        return Html::button('Скачать',
                            [
                                'class' => 'btn btn-info btn-xs buttonDownload',
                                'data'  => ['id' => $item->id_penalty],
                            ]);
                    },
                ],
                [
                    'header'  => 'Закрыть',
                    'content' => function (\common\models\Penalty $item) {
                        $v = ArrayHelper::getValue($item, 'is_closed', 0);
                        if ($v == 1) return '';

                        return Html::button('Закрыть', ['class' => 'btn btn-success btn-xs buttonClose', 'data' => ['id' => $item->id_penalty]]);
                    },
                ],
            ],
        ]) ?>
        <?php \yii\widgets\Pjax::end(); ?>
    </div>
</div>