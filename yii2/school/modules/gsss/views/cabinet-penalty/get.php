<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;
use app\models\UnionCategory;
use yii\db\Query;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \app\models\validate\PenaltyGet */

$this->title = 'Получить квитанции квитанцию';

?>
<div class="container">
    <div class="page-header">
        <h1><?= Html::encode($this->title) ?></h1>
    </div>

    <?php \yii\widgets\Pjax::begin() ?>
    <?php if (!is_null($id = Yii::$app->session->getFlash('contactFormSubmitted'))) : ?>

        <div class="alert alert-success">
            Успешно добавлено.
        </div>
        
    <?php else: ?>

        <div class="row">
            <div class="col-lg-6 col-lg-offset-3">
                <?php $form = ActiveForm::begin([
                    'id'      => 'contact-form',
                    'options' => ['enctype' => 'multipart/form-data']
                ]); ?>
                <?= $form->field($model, 'count')->label('Кол-во') ?>

                <hr>
                <div class="form-group">
                    <?= Html::submitButton('Получить', [
                        'class' => 'btn btn-default',
                        'name'  => 'contact-button',
                        'style' => 'width:100%',
                    ]) ?>
                </div>
                <?php ActiveForm::end(); ?>
            </div>
        </div>
    <?php endif; ?>
    <?php \yii\widgets\Pjax::end() ?>
</div>
