<?php

/**
 * Created by PhpStorm.
 * User: Ramha
 * Date: 04.12.2018
 * Time: 1:32
 */

use yii\helpers\Html;

/** @var $model \app\models\rai\Anketa */
/** $this \yii\web\View  */

$this->title = $model->name_first;

?>

<div class="container">
    <div class="col-lg-12">
        <h1 class="page-header"><?= Html::encode($this->title) ?></h1>

        <?= \yii\widgets\DetailView::widget([
            'model'      => $model,
            'attributes' => [
                [
                    'label'          => 'Имя',
                    'value'          => $model->name_first,
                    'captionOptions' => [
                        'style' => 'width:30%',
                    ],
                ],
                'name_last',
                'name_middle',
                'contact_email',
                'contact_phone',
                'contact_phone',
                'contact_vk',
                'contact_fb',
                'contact_telegram',
                'contact_whatsapp',
                'contact_skype',
                'age',
                'place',
                [
                    'label'  => 'vozmojnosti',
                    'format' => 'html',
                    'value'  => nl2br($model->vozmojnosti),
                ],
                [
                    'label'  => 'opit',
                    'format' => 'html',
                    'value'  => nl2br($model->opit),
                ],
                'created_at:datetime', // creation date formatted as datetime
            ],
        ]) ?>
    </div>
</div>
