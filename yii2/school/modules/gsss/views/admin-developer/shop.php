<?php

/** $this \yii\web\View  */

use yii\helpers\Url;
use app\services\GsssHtml;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use app\services\GetArticle\Collection;

$this->title = 'Магазин';


?>

<div class="container">
    <div class="col-lg-12">
        <h1 class="page-header"><?= Html::encode($this->title) ?></h1>

        <p>Этот документ описывает общую структуру магазина.</p>
        <h2 class="page-header">Введение</h2>
        <p>Если пользователь набирает товары из разных магазинов, то заказы формируются и потом оплачиваются по отдельности. А если из одного то сразу идет оплата.</p>

        <p><img src="/images/controller/admin-developer/shop/shop-index_1.png"></p>
        <p><img src="/images/controller/admin-developer/shop/shop-index_2.png"></p>

        <p>Для обеспечения этого раздела используются следующие таблицы:</p>
        <?php
        $rows = [
            [
                'name'        => 'gs_shop_tree',
                'description' => 'общий каталог магазина',
                'link' => '',
            ],
            [
                'name'        => 'gs_shop_tree_products_link',
                'description' => 'связь продукта и категории магазина, то есть определяет к какой',
                'link' => '',
            ],
            [
                'name'        => 'gs_unions_shop',
                'description' => 'описание и настройки магазина для объединения',
                'link' => '',
            ],
        ];
        ?>
        <?= \yii\grid\GridView::widget([
            'dataProvider' => new \yii\data\ArrayDataProvider([
                'models' => $rows,
            ]),
            'tableOptions' => [
                'class' => 'table table-striped table-hover',
            ],
            'summary'      => '',
        ]) ?>
        <p>Каждое объединение может создать себе интернет магазин.</p>
        <p>В настройках магазина можна задать картинку для писем магазина, подпись, и кошелек Яндекс.Деньги куда будут приходить деньги от покупок.</p>


    </div>
</div>

