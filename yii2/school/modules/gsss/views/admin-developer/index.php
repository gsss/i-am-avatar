<?php

/** $this \yii\web\View  */

use yii\helpers\Url;
use app\services\GsssHtml;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use app\services\GetArticle\Collection;

$this->title = 'Разработка';


?>

<div class="container">
    <div class="col-lg-12">
        <h1 class="page-header"><?= Html::encode($this->title) ?></h1>

        <p><a href="base-form-active-record">База. FormActiveRecord</a></p>
        <p><a href="install">Установка сайта</a></p>
        <p><a href="chaneling">Послания</a></p>
        <p><a href="school">Школа</a></p>
        <p><a href="school-autovoronki">Школа. Автоворонки</a></p>
        <p><a href="school-uroki">Школа. Уроки</a></p>
        <p><a href="get-course">Школа. get-course</a></p>

        <p><a href="shop">Магазин</a></p>
        <p><a href="shop-delivery">Магазин. Доставка</a></p>
    </div>
</div>