<?php

/** $this \yii\web\View  */

use yii\helpers\Url;
use app\services\GsssHtml;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use app\services\GetArticle\Collection;

$this->title = 'Доставка';


?>

<div class="container">
    <div class="col-lg-12">
        <h1 class="page-header"><?= Html::encode($this->title) ?></h1>

        <p>Этот документ описывает все что касается доставки.</p>
        <h2 class="page-header">Описание</h2>
        <p>По виду оплаты:</p>
        <p>1. Предоплата</p>
        <p>2. Оплата при получении</p>
        <p><img src="/images/controller/admin-developer/shop-delivery/shop-delivery_1.png"></p>
        <p><a href="https://drive.google.com/file/d/0B73bzLewJ2HMRVdpN1pkNHJqYXM/view?usp=sharing" target="_blank">https://drive.google.com/file/d/0B73bzLewJ2HMRVdpN1pkNHJqYXM/view?usp=sharing</a>
        </p>

        <?= \common\services\documentation\DbTable::widget([
            'name'    => 'gs_unions_shop_dostavka',
            'columns' => [
                [
                    'name'        => 'id',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Идентификатор пользователя',
                ],
                [
                    'name' => 'name',
                ],
                [
                    'name' => 'description',
                ],
                [
                    'name' => 'content',
                ],
                [
                    'name' => 'union_id',
                    'type' => 'integer',
                ],
                [
                    'name' => 'price',
                ],
                [
                    'name'        => 'is_need_address',
                    'type'        => 'tinyint',
                    'isRequired'  => true,
                    'description' => 'Нужно ли указывать адрес в заказе при выборе этого способа доставки?',
                ],
                [
                    'name'        => 'type',
                    'type'        => 'tinyint',
                    'isRequired'  => true,
                    'description' => 'тип доставки - 1 - самовывоз 2 - доставка по почте 3 - наложный платеж',
                ],
                [
                    'name'        => 'is_pay',
                    'type'        => 'tinyint',
                    'isRequired'  => true,
                    'description' => '0 - оплачивать не обязательно, 1 - оплачивать обязательно',
                ],
            ],
        ]) ?>
        <h2 class="page-header">Наложный платеж</h2>
        <p>Этот документ описывает все что касается доставки наложным платежом.</p>
        <p><img src="/images/controller/admin-developer/shop-delivery/shop-delivery-nalozh-1.png"></p>
        <p>Есть несколько статусов:</p>
        <?php
        $rows = [
            [
                'name'        => 'STATUS_DOSTAVKA_NALOZH_PREPARE',
                'value'       => '21',
                'description' => 'заказ формируется для отправки клиенту',
            ],
            [
                'name'        => 'STATUS_DOSTAVKA_NALOZH_PREPARE_DONE',
                'value'       => '22',
                'description' => 'заказ сформирован для отправки клиенту',
            ],
            [
                'name'        => 'STATUS_DOSTAVKA_NALOZH_SEND_TO_USER',
                'value'       => '23',
                'description' => 'заказ отправлен клиенту',
            ],
            [
                'name'        => 'STATUS_DOSTAVKA_NALOZH_PIAD',
                'value'       => '24',
                'description' => 'заказ исполнен, деньги были получены',
            ],
            [
                'name'        => 'STATUS_DOSTAVKA_NALOZH_RETURN',
                'value'       => '25',
                'description' => 'заказ вернулся, отменен',
            ],
        ];
        ?>
        <?= \yii\grid\GridView::widget([
            'dataProvider' => new \yii\data\ArrayDataProvider([
                'models' => $rows,
            ]),
            'tableOptions' => [
                'class' => 'table table-striped table-hover',
            ],
            'summary'      => '',
        ]) ?>

        <p>Письмо отсылается клиенту когда ставится статус <code>STATUS_DOSTAVKA_NALOZH_SEND_TO_USER</code>.</p>


    </div>
</div>

