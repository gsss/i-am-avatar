<?php

/** $this \yii\web\View  */

use yii\helpers\Url;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;

$this->title = 'Школа. Уроки';


?>

<div class="container">
    <div class="col-lg-12">
        <h1 class="page-header"><?= Html::encode($this->title) ?></h1>

        <h2 class="page-header">Описание</h2>
        <p>Это нужно для того чтобы учитель мог создавать свои уроки и выкладывать / предоставлять доступ для других.
            Платно или на другой основе.</p>
        <p>Какие уроки могут быть?</p>
        <p>1. Видео в YouTube</p>
        <p>2. Текст HTML</p>
        <p>3. Документ DOCX, PDF</p>
        <p>Нужно к объединению добавить сущность Школа со своими параметрами</p>
        <p>Как предоставлять доступ к урокам?</p>
        <p>1. Лично для каждого клиента</p>
        <p>2. Для группы людей. Тогда нужно сделать сущность группа/класс внутри школы.</p>
        <p>3. Для Участников такого то события на который они записались/оплатили.</p>

        <h2 class="page-header">Права доступа</h2>
        <p>Например курсы состоят из уроков. Урок состоит из нескольких материатов. Для урока может быть выдано домашнее
            задание.</p>
        <p>Для урока может быть задано от одного до нескольких лекторов и ведущих.</p>
        <p>Для курса задаются уже права доступа для группы или для конкретного пользователя.</p>

        <h2 class="page-header">Сущности</h2>

        <h3 class="page-header">Школа</h3>
        <p>Основные свойства школы:</p>
        <p>- Название</p>
        <p>- Принадлежность Объединению</p>

        <?= \common\services\documentation\DbTable::widget([
            'name'    => 'gs_school',
            'columns' => [
                [
                    'name'        => 'id',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Идентификатор школы',
                ],
                [
                    'name'        => 'name',
                    'type'        => 'varchar(100)',
                    'isRequired'  => true,
                    'description' => 'Название группы',
                ],
                [
                    'name'        => 'union_id',
                    'type'        => 'integer',
                    'isRequired'  => true,
                    'description' => 'Идентификатор объединения',
                ],
            ],
        ]) ?>

        <h3 class="page-header">Класс учеников</h3>
        <p>Основные свойства класса:</p>
        <p>- Название</p>
        <p>- Принадлежность школе</p>

        <?= \common\services\documentation\DbTable::widget([
            'name'    => 'gs_school_group',
            'columns' => [
                [
                    'name'        => 'id',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Идентификатор группы',
                ],
                [
                    'name'        => 'name',
                    'type'        => 'varchar(100)',
                    'isRequired'  => true,
                    'description' => 'Название группы',
                ],
                [
                    'name'        => 'school_id',
                    'type'        => 'integer',
                    'isRequired'  => true,
                    'description' => 'Идентификатор школы',
                ],
            ],
        ]) ?>


        <h3 class="page-header">Курс</h3>
        <p>Основные свойства курса:</p>
        <p>- Название</p>
        <p>- Принадлежность Школе</p>

        <?= \common\services\documentation\DbTable::widget([
            'name'    => 'gs_school_kurs',
            'columns' => [
                [
                    'name'        => 'id',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Идентификатор курса',
                ],
                [
                    'name'        => 'name',
                    'type'        => 'varchar(100)',
                    'isRequired'  => true,
                    'description' => 'Название курса',
                ],
                [
                    'name'        => 'school_id',
                    'type'        => 'integer',
                    'isRequired'  => true,
                    'description' => 'Идентификатор школы',
                ],
            ],
        ]) ?>

        <h3 class="page-header">Урок</h3>

        <p>Курсы содержат уроки.
            Уроки могут быть доступны как сразу все так и последовательно.
            Если доступны все уроки сразу то ученик может проходить их последовательно или в разнобой не соблюдая очередность.
            Если доступ производится последовательно то значит что ученик не может перейти к прохождению следующего урока не выплнив домашнее задание и не получив положительной оценки.
        </p>
        <p>Основные свойства урока:</p>
        <p>- Название</p>
        <p>- Принадлежность курсу</p>

        <?= \common\services\documentation\DbTable::widget([
            'name'    => 'gs_school_urok',
            'columns' => [
                [
                    'name'        => 'id',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Идентификатор урока',
                ],
                [
                    'name'        => 'name',
                    'type'        => 'varchar(100)',
                    'isRequired'  => true,
                    'description' => 'Название урока',
                ],
                [
                    'name'        => 'kurs_id',
                    'type'        => 'integer',
                    'isRequired'  => true,
                    'description' => 'Идентификатор курса',
                ],
            ],
        ]) ?>

        <p>Если урок является стоповым а дз не указано?</p>
        <p>Если урок является то обязательно должно быть указано домашнее задание.</p>
        <p>А если дз указано то обязательно ли это считается то урок является стоповым? нет.</p>
        <p>Значит флаг is_stop я перенешу в домашнее задание.</p>

        <h3 class="page-header">Домашнее задание к уроку</h3>

        <p>Урок может содержать домашнее задание.
            В нем есть флаг is_stop. Если он выставлен то значит ученик не сможет перети к следующим урокам не выполнив домашнего задания.
        </p>

        <?= \common\services\documentation\DbTable::widget([
            'name'    => 'gs_school_urok_dz_request',
            'columns' => [
                [
                    'name'        => 'id',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Идентификатор домашнего задания',
                ],
                [
                    'name'        => 'content',
                    'type'        => 'text',
                    'isRequired'  => true,
                    'description' => 'Содержание домашнего задания',
                ],
                [
                    'name'        => 'sale_id',
                    'type'        => 'integer',
                    'isRequired'  => true,
                    'description' => 'Идентификатор продажи',
                ],
                [
                    'name'        => 'is_stop',
                    'type'        => 'tinyint',
                    'isRequired'  => false,
                    'description' => 'Флаг указывающий является ли этот укор стоповым. То есть если да то ученик не может перейти к следующему уроку не выполнив положительно домашнее задание для текушего урока. 0 - нет, ',
                ],
            ],
        ]) ?>
        <pre>-- auto-generated definition
create table gs_school_urok_dz_request
(
  id bigint auto_increment
    primary key,
  content text not null,
  urok_id int not null,
  is_stop tinyint not null
) ENGINE=InnoDB DEFAULT CHARSET=utf8
;</pre>

        <p>Домашнее задание считается выполненным если ученик получает оценку более чем 2.</p>
        <p>Если ученик не выполняет домашнего задания и урок является стоповым, то ученик делает ДЗ до тех пор пока не выполнит его.</p>


        <h3 class="page-header">Ответ ученика на домашнее задание.</h3>

        <?= \common\services\documentation\DbTable::widget([
            'name'    => 'gs_school_urok_dz_response',
            'columns' => [
                [
                    'name'        => 'id',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Идентификатор ответа',
                ],
                [
                    'name'        => 'dz_id',
                    'type'        => 'integer',
                    'isRequired'  => true,
                    'description' => 'Идентификатор ДЗ',
                ],
                [
                    'name'        => 'user_id',
                    'type'        => 'integer',
                    'isRequired'  => true,
                    'description' => 'Пользователь',
                ],
                [
                    'name'        => 'ocenka',
                    'type'        => 'integer',
                    'isRequired'  => true,
                    'description' => 'Осенка за домашнее задание',
                ],
            ],
        ]) ?>
        <p>Предупреждение. Оценка привязывается к домашнему заданию, а домашнее задание одно на все прохождения, то может случиться ситуация что если человек проходит курс дважды, то будет виден и предыдущий ответ а это уже некрасиво, должны быть только ответы на текущее прехождение. Поэтому чтобы избежать такой ситуации то нужно ввести такую сущность как прохождение курса группой.</p>

        <h3 class="page-header">Прохождение курса группой - поток</h3>

        <?= \common\services\documentation\DbTable::widget([
            'name'    => 'gs_school_potok',
            'columns' => [
                [
                    'name'        => 'id',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Идентификатор потока',
                ],
                [
                    'name'        => 'kurs_id',
                    'type'        => 'integer',
                    'isRequired'  => true,
                    'description' => 'Идентификатор курса',
                ],
                [
                    'name'        => 'group_id',
                    'type'        => 'integer',
                    'isRequired'  => true,
                    'description' => 'Идентификатор группы',
                ],
                [
                    'name'        => 'date_start',
                    'type'        => 'date',
                    'isRequired'  => false,
                    'description' => 'Дата старта потока (планируемая)',
                ],
                [
                    'name'        => 'date_end',
                    'type'        => 'date',
                    'isRequired'  => false,
                    'description' => 'Дата окончания потока (планируемая)',
                ],
            ],
        ]) ?>

        <p>Обязательно указываются курс к которому привязан поток и группа которая го будет проходить.</p>
        <p>Можно указать дата начала и окончания потока.</p>

        <h2 class="page-header">Продажа курса</h2>
        <p>Продать можно только курс. Если у вас один урок или вебинар то делайте курс с одним уроком.</p>
        <p>Организовать продажу курса можно через событие.</p>
        <p>Продажа курса сопровождается обязательным формированием потока и группы на него.</p>
        <p>Как назвать продажу курса? Продажа курса? Событие? Анонс? Анонс - это просто объявление о событии. А что я хочу?
            Я хочу как то обозначить именно событие связанное с продажей курса.
            Ну скорее это продажа курса.
            Выгладит в виде анонса.
            Рабочее название - событие</p>
        <p>По сути конечно это страничка - минилендинг с продажей с отдельной ссылкой.</p>

        <?= \common\services\documentation\DbTable::widget([
            'name'    => 'gs_school_sale_kurs',
            'columns' => [
                [
                    'name'        => 'id',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Идентификатор потока',
                ],
                [
                    'name'        => 'potok_id',
                    'type'        => 'integer',
                    'isRequired'  => true,
                    'description' => 'Идентификатор потока для курса',
                ],
                [
                    'name'        => 'price',
                    'type'        => 'double',
                    'isRequired'  => false,
                    'description' => 'Стоимость курса',
                ],
                [
                    'name'        => 'date_end',
                    'type'        => 'date',
                    'isRequired'  => false,
                    'description' => 'Дата окончания продаж',
                ],
                [
                    'name'        => 'is_offline',
                    'type'        => 'date',
                    'isRequired'  => false,
                    'description' => 'Флаг показывающий что это уже оффлайн курс. 0 - это онлайн курс, 1 - это офлайн курс. По умолчанию 0.',
                ],
                [
                    'name'        => 'place',
                    'type'        => 'varchar(100)',
                    'isRequired'  => false,
                    'description' => 'Мосто положение',
                ],
            ],
        ]) ?>
        <p>Еще я так подумал что лучше конечно же сделать и возможность физического обучения. Да это очень актуально.</p>

        <h2 class="page-header">Продажа курса. Местоположение</h2>

        <p>Описывает местоположение проведения курса.</p>
        <?= \common\services\documentation\DbTable::widget([
            'name'    => 'gs_school_sale_kurs_place',
            'columns' => [
                [
                    'name'        => 'id',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Идентификатор местоположения',
                ],
                [
                    'name'        => 'sale_id',
                    'type'        => 'integer',
                    'isRequired'  => true,
                    'description' => 'Идентификатор продажи',
                ],
                [
                    'name'        => 'place',
                    'type'        => 'varchar(100)',
                    'isRequired'  => false,
                    'description' => 'Мосто положение в стандарте Google, (Страна, город)',
                ],
                [
                    'name'        => 'lat',
                    'type'        => 'double',
                    'isRequired'  => false,
                    'description' => 'Мосто положение, широта',
                ],
                [
                    'name'        => 'lng',
                    'type'        => 'double',
                    'isRequired'  => false,
                    'description' => 'Мосто положение, долгота',
                ],
                [
                    'name'        => 'id_string',
                    'type'        => 'varchar(10)',
                    'isRequired'  => false,
                    'description' => 'уникальный идентификатор',
                ],
            ],
        ]) ?>

        <p>Для продажи можно продумать некое подобие конструктора лендингов как в тильде.</p>
        <p>Ну банально можно сделать заголовок, преимущества, описание, форма заказа, контакты.</p>
        <p>Кстати как формируется алгоритм заказа?</p>
        <p>1. Клиент оставляет свои контакты, Имя телефон и почту</p>
        <p>2. Он регистрируется как клиент в системе</p>
        <p>3. Он Оплачивает курс</p>
        <p>4. Ему открывается доступ</p>
        <p>5. Он получает письмо в котором все расписывается. Возможности и тд. Если это физическое мероприятие то получает QR код для прохода.</p>

        <h2 class="page-header">Бизнес план</h2>

        <h2 class="page-header">Формирование Бренда</h2>
        <p><a href="http://go.free-publicity.ru/mybrand" target="_blank">http://go.free-publicity.ru/mybrand</a></p>

        <h3 class="page-header">Описание платформы</h3>

        <p>Название англ: I-AM-AVATAR</p>
        <p>Название рус: Я - Аватар</p>
        <p>Подключен безоплатно дизайн человека. На платформе специалисты могут давать консультации. И проводить свои курсы.</p>
        <p>Карточка мастера, Фотоальбом</p>
        <p>Карточка события</p>
        <p>Автоворонки</p>
        <p>Система билетов</p>
        <p>Система сертификатов</p>
        <p>Система аттестации</p>
        <p>Система домашних заданий</p>
        <p>Блог для мастера</p>
        <p>Вебинарная комната с презентациями</p>
        <p>Интернет магазин</p>
        <p>Система ступеней</p>
        <p>Онлайн хранилище</p>
        <p>Система рассылок</p>
        <p>Сейфовая ячейка</p>
        <p>Система шифрования материалов</p>
        <p>Подключение всех сервисов</p>
        <p>Мини конструктор лендингов</p>
        <p>Обучение через виртуальную реальность на будущее</p>
        <p>Мультиязычность</p>

        <h3 class="page-header">На чем будет зарабатывать платформа</h3>
        <p>Абонентское обслуживание за платформу.</p>
        <p>Например 1000 руб в мес.</p>

        <h3 class="page-header">Маркетинговое исследование</h3>

        <h3 class="page-header">Анализ конкурентов</h3>
        <p><a href="https://4brain.ru/" target="_blank">https://4brain.ru/</a></p>
        <p><a href="https://getcourse.ru/" target="_blank">https://getcourse.ru/</a></p>
        <p><a href="https://geekbrains.ru" target="_blank">https://geekbrains.ru</a></p>
        <p><a href="https://www.udemy.com/" target="_blank">https://www.udemy.com/</a></p>

        <h3 class="page-header">Финансовый план</h3>
        <h3 class="page-header">Road Map</h3>
        <?php
        $rows = [
            [
                'date'        => '2019 Март',
                'description' => [
                    'Форкнутый AvatarNetwork',
                    'Уроки',
                    'События',
                    'Курсы',
                    'Продажа курса',
                    'Блог',
                    'Карточка мастера',
                ],
            ],
            [
                'date'        => '2019 Сентябрь',
                'description' => [
                    'Автоворонки',
                    'Система билетов',
                    'Домашнее задание',
                    'Мультиязычность',
                    'Интернет магазин',
                    'Система аттестации',
                    'Мини конструктор лендингов',
                    'Система рассылок',
                ],
            ],
            [
                'date'        => '2020',
                'description' => [
                    'Обучение через виртуальную реальность',
                    'Система шифрования материалов',
                ],
            ],
        ];
        ?>
        <?= \yii\grid\GridView::widget([
            'dataProvider' => new \yii\data\ArrayDataProvider([
                'allModels' => $rows,
            ]),
            'tableOptions' => [
                'class' => 'table table-striped table-hover',
                'style' => 'width: auto',
            ],
            'summary' => '',
            'columns' => [
                'date',
                [
                    'attribute' => 'description',
                    'content' => function($i) {
                        if (is_array($i)) {
                            return join('<br>', $i['description']);
                        } else {
                            return $i['description'];
                        }
                    }
                ]
            ],
        ]) ?>

        <h3 class="page-header">Инвестиционный план</h3>
        <p>Схема привлечения инвестиций строится по следующей схеме: 51% акций остается у основателя. 49% распределяется между инвесторами которые верят в проект и хотят получать дивиденты от его реализации.</p>
        <p>Необходимо определить стоимость всего пакета акций. Как это делается? Можно ли потом довыпускать и по какому алгоритму?</p>
        <p>Как определить стоимость всего пакета акций?</p>
        <p>1 от балды</p>
        <p>2 расчитать стоимость всех ресурсов на пике развития и от ожидаемой прибыли.</p>
        <p>* Зависит от того можно ли будет довыпускать акции</p>
        <p>Можно ли будет довыпускать акции?</p>
        <p>Пока я не могу ответить на этот вопрос,
            потому что надо проанализировать как будет меняться структура компании,
            интересы и поведение инвесторов,
            экономический план от такого довыпуска.</p>


    </div>
</div>

