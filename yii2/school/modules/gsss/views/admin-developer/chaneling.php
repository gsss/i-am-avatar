<?php

/** $this \yii\web\View  */

use yii\helpers\Url;
use app\services\GsssHtml;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use app\services\GetArticle\Collection;

$this->title = 'Послания';


?>

<div class="container">
    <div class="col-lg-12">
        <h1 class="page-header"><?= Html::encode($this->title) ?></h1>

        <h2 class="page-header">База данных</h2>
        <?= \common\services\documentation\DbTable::widget([
            'name' => 'gs_cheneling_list',
            'columns' => [
                [
                    'name'          => 'id',
                    'type'          => 'int',
                    'isRequired'    => true,
                    'description'   => 'Идентификатор пользователя',
                ],
                [
                    'name'          => 'header',
                    'type'          => 'varchar(255)',
                    'isRequired'    => true,
                    'description'   => 'Название послания',
                ],
                [
                    'name'          => 'content',
                    'type'          => 'mediumtext',
                    'isRequired'    => true,
                    'description'   => 'Содержимое послания',
                ],
                [
                    'name'          => 'source',
                    'type'          => 'varchar(255)',
                    'isRequired'    => true,
                    'description'   => 'Ссылка на источник',
                ],
                [
                    'name'          => 'date_insert',
                    'type'          => 'datetime',
                    'isRequired'    => true,
                    'description'   => 'Время добавления послания',
                ],
                [
                    'name'          => 'id_string',
                    'type'          => 'varchar(255)',
                    'isRequired'    => true,
                    'description'   => 'Идентификатор послания для URL (см раздел "URL послания")',
                ],
                [
                    'name'          => 'img',
                    'type'          => 'varchar(255)',
                    'isRequired'    => true,
                    'description'   => 'Картинка послания',
                ],
                [
                    'name'          => 'view_counter',
                    'type'          => 'int',
                    'isRequired'    => true,
                    'description'   => 'Счетчик пользователей',
                ],
                [
                    'name'          => 'description',
                    'type'          => 'varchar(1000)',
                    'isRequired'    => true,
                    'description'   => 'Описание для послания (обычно первые слова послания)',
                ],
                [
                    'name'          => 'date',
                    'type'          => 'date',
                    'isRequired'    => true,
                    'description'   => 'Дата добавления послания (см раздел "URL послания")',
                ],
                [
                    'name'          => 'tree_node_id_mask',
                    'type'          => 'bigint',
                    'isRequired'    => true,
                    'description'   => 'Поле хранящее отмеченные теги для послания',
                ],
                [
                    'name'          => 'is_added_site_update',
                    'type'          => 'tinyint',
                    'isRequired'    => false,
                    'description'   => 'Флаг "?"',
                ],
                [
                    'name'          => 'user_id',
                    'type'          => 'int',
                    'isRequired'    => false,
                    'description'   => 'Автор послания из списка gs_users (зарегистрированные пользователи)',
                ],
                [
                    'name'          => 'moderation_status',
                    'type'          => 'tinyint',
                    'isRequired'    => false,
                    'description'   => 'Статус модерации',
                ],
                [
                    'name'          => 'date_created',
                    'type'          => 'int',
                    'isRequired'    => false,
                    'description'   => 'Время добавления послания',
                ],
                [
                    'name'          => 'author_id',
                    'type'          => 'int',
                    'isRequired'    => false,
                    'description'   => 'Автор послания из списка gs_cheneling_list_author_id',
                ],
                [
                    'name'          => 'power_id',
                    'type'          => 'int',
                    'isRequired'    => false,
                    'description'   => 'Силы которые говорили через контактера послания (из списка gs_cheneling_list_author_id)',
                ],
                [
                    'name'          => 'author',
                    'type'          => 'varchar(64)',
                    'isRequired'    => false,
                    'description'   => 'Автор послания текстом',
                ],
                [
                    'name'          => 'share_title_type',
                    'type'          => 'tinyint',
                    'isRequired'    => false,
                    'description'   => 'Шаблон формирования заголовка для расшаривания, 1 - Заголовок, 2 - Силы. Заголовок, 3 - Автор. Заголовок, 4 - Силы через Автор. Заголовок',
                ],
            ]
        ])?>

        <h2 class="page-header">Страница Послания</h2>
        <p>Открывается по ссылке <code>/chenneling</code> </p>

        <p><b>Автор</b></p>
        <p><code>/chenneling?user_id=4348</code> </p>
        <p><code>/chenneling?author_id=4348</code></p>

        <h2 class="page-header">Автор</h2>
        <p>Всего может быть три варианта задания автора для послания</p>
        <p>1. после user_id</p>
        <p>2. после author_id</p>
        <p>3. после author</p>




    </div>
</div>

