<?php

/** $this \yii\web\View  */

use yii\helpers\Url;
use app\services\GsssHtml;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use app\services\GetArticle\Collection;

$this->title = 'Инсталяция сайта';


?>

<div class="container">
    <div class="col-lg-12">
        <h1 class="page-header"><?= Html::encode($this->title) ?></h1>

        <h2 class="page-header">Установка memcache</h2>
        <p>Установка memcache выполняется в несколько действий.</p>
        <p>Для начала используйте apt-get, чтобы установить memcached.</p>
        <pre>sudo apt-get install memcached</pre>
        <p>Затем нужно установить php-pear – репозиторий, в котором хранится memcache.</p>
        <pre>sudo apt-get install php-pear</pre>
        <p>Если на вашем сервере нет компилятора, скачайте инструменты build-essential:</p>
        <pre>sudo apt-get install php7.0-dev</pre>
        <pre>sudo apt-get install build-essential</pre>
        <p>В завершение используйте PECL (PHP Extension Community Library) для установки memcache:</p>
        <pre>sudo pecl install memcache</pre>
        <h3 class="page-header">Просмотр статистики</h3>
        <p>Проверить, установился ли Memcache, можно с помощью функции поиска:</p>
        <pre>ps aux | grep memcache</pre>

        <h2 class="page-header">Возможные ошибки</h2>
        <p>Если в Mysql возникают ошибка</p>
        <p class="alert alert-danger">SQLSTATE[42000]: Syntax error or access violation: 1055 Expression #3 of SELECT list is not in GROUP BY clause and contains nonaggregated</p>
        <p>То устанавливаю:</p>
        <pre>sudo mysql -u root -p</pre>
        <pre>SET GLOBAL sql_mode=(SELECT REPLACE(@@sql_mode,'ONLY_FULL_GROUP_BY',''));</pre>



    </div>
</div>

