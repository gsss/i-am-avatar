<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;
use app\models\UnionCategory;
use yii\db\Query;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model cs\base\BaseForm */

$this->title = $model->header;
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="container">
    <h1 class="page-header text-center"><?= Html::encode($this->title) ?></h1>

    <?php if (Yii::$app->session->hasFlash('contactFormSubmitted')): ?>

        <div class="alert alert-success">
            Успешно обновлено.
        </div>
        <?php if (Yii::$app->user->identity->get('mode_chenneling', 0) == 1) { ?>
            <?php
            $this->registerJs(<<<JS
            // Сделать рассылку
        $('.sendModeration').click(function (e) {
            e.preventDefault();
            e.stopPropagation();
            if (confirm('Подтведите свое намерение')) {
                var buttonSubscribe = $(this);
                var id = $(this).data('id');
                ajaxJson({
                    url: '/cabinet/channeling/' + id + '/sendModeration',
                    success: function (ret) {
                        infoWindow('Успешно', function() {
                            buttonSubscribe.remove();
                        });
                    }
                });
            }
        });

JS
            );
            ?>
            <button class="btn btn-info sendModeration" data-id="<?= $id ?>">Отправить на модерацию</button>
        <?php } ?>

        <?php if (Yii::$app->user->identity->get('mode_chenneling_is_subscribe', 0) == 1) { ?>
            <?php
            $this->registerJs(<<<JS
            // Сделать рассылку
        $('.sendSubscribe').click(function (e) {
            e.preventDefault();
            e.stopPropagation();
            if (confirm('Подтведите свое намерение')) {
                var buttonSubscribe = $(this);
                var id = $(this).data('id');
                ajaxJson({
                    url: '/cabinet/channeling/' + id + '/subscribe',
                    success: function (ret) {
                        infoWindow('Успешно', function() {
                            buttonSubscribe.remove();
                        });
                    }
                });
            }
        });

JS
            );
            ?>
            <button class="btn btn-success sendSubscribe" data-id="<?= $id ?>">Рассылка</button>
        <?php } ?>
        <a class="btn btn-info" href="/cabinet/channeling/<?= $id ?>/view">Просмотреть</a>

    <?php else: ?>


        <div class="row">
            <div class="col-lg-6 col-lg-offset-3">
                <?php $form = ActiveForm::begin([
                    'id'      => 'contact-form',
                    'options' => ['enctype' => 'multipart/form-data']
                ]); ?>
                <?= $model->field($form, 'header') ?>
                <?= $model->field($form, 'content') ?>
                <?= $model->field($form, 'img') ?>
                <?= $model->field($form, 'is_add_image') ?>

                <hr>
                <div class="form-group">
                    <?= Html::submitButton('Обновить', [
                        'class' => 'btn btn-default',
                        'name'  => 'contact-button',
                        'style' => 'width:100%',
                    ]) ?>
                </div>
                <?php ActiveForm::end(); ?>
            </div>
        </div>

    <?php endif; ?>
</div>
