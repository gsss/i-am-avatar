<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;
use app\models\UnionCategory;
use yii\db\Query;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model cs\base\BaseForm */

$this->title = $model->email;
?>
<div class="container">
    <h1 class="page-header"><?= Html::encode($this->title) ?></h1>


    <?php if (Yii::$app->session->hasFlash('contactFormSubmitted')): ?>

        <div class="alert alert-success">
            Успешно обновлено.
        </div>

    <?php else: ?>
        <div class="row">
            <div class="col-lg-8 col-lg-offset-2">
                <?php $form = ActiveForm::begin([
                    'id'      => 'contact-form',
                    'layout'  => 'horizontal',
                    'options' => ['enctype' => 'multipart/form-data']
                ]); ?>
                <?= $model->field($form, 'email') ?>
                <?= $model->field($form, 'mode_chenneling') ?>
                <?= $model->field($form, 'mode_chenneling_is_subscribe') ?>
                <h3 class="page-header">Подписки</h3>
                <?= $model->field($form, 'subscribe_is_news') ?>
                <?= $model->field($form, 'subscribe_is_site_update')->hint('еженедельно') ?>
                <?= $model->field($form, 'subscribe_is_after_insert')->hint('сразу после добавления') ?>
                <?= $model->field($form, 'subscribe_is_manual') ?>
                <?= $model->field($form, 'subscribe_is_tesla') ?>
                <?= $model->field($form, 'subscribe_is_rod') ?>
                <?= $model->field($form, 'subscribe_is_new_earth') ?>
                <?= $model->field($form, 'subscribe_is_bogdan') ?>
                <?= $model->field($form, 'subscribe_is_god') ?>
                <h3 class="page-header">Аватар</h3>
                <?= $model->field($form, 'avatar') ?>
                <?= $model->field($form, 'phone') ?>
                <h3 class="page-header">Флаги</h3>
                <?= $model->field($form, 'is_confirm') ?>
                <?= $model->field($form, 'is_active') ?>
                <h3 class="page-header">Пришел на планету Земля</h3>
                <?= $model->field($form, 'birth_lat') ?>
                <?= $model->field($form, 'birth_lng') ?>
                <?= $model->field($form, 'birth_place') ?>
                <h3 class="page-header">Другое</h3>
                <?= $model->field($form, 'time_zone') ?>
                <?= $model->field($form, 'is_accept_nmp') ?>
                <?= $model->field($form, 'referal_code') ?>
                <h3 class="page-header">Соц сети</h3>
                <?= $model->field($form, 'vk_id') ?>
                <?= $model->field($form, 'vk_link') ?>
                <?= $model->field($form, 'fb_id') ?>
                <?= $model->field($form, 'fb_link') ?>


                <hr>
                <div class="form-group">
                    <?= Html::submitButton('Обновить', [
                        'class' => 'btn btn-default',
                        'name'  => 'contact-button',
                        'style' => 'width:100%',
                    ]) ?>
                </div>
                <?php ActiveForm::end(); ?>
            </div>
        </div>

    <?php endif; ?>
</div>
