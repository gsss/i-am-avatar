<?php

/** $this \yii\web\View  */

use yii\helpers\Url;
use app\services\GsssHtml;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use app\services\GetArticle\Collection;

$this->title = 'Пользователи';

?>



<div class="container">
    <?php \yii\widgets\Pjax::begin(); ?>
    <?php
    $this->registerJS(<<<JS
    $('.buttonDelete').click(function (e) {
        e.preventDefault();
        e.stopPropagation();
        if (confirm('Подтвердите удаление')) {
            var button = $(this);
            var id = $(this).data('id');
            ajaxJson({
                url: '/admin/users/' + id + '/delete',
                success: function (ret) {
                    infoWindow('Успешно', function() {
                        button.parent().parent().remove();
                    });
                }
            });
        }
    });

    $('.rowTable').click(function() {
        window.location = '/admin/users/' + $(this).data('id') + '/edit';
    });

JS
    );
    $searchModel = new \app\models\Form\admin\UserSearch();
    $dataProvider = $searchModel->search(Yii::$app->request->get());
    $sort = new \yii\data\Sort([
        'attributes' => [
            'id'         => [
                'default' => SORT_ASC,
            ],
            'email'      => [
                'label' => 'Email',
            ],
            'name_first' => [
                'label' => 'Имя',
            ],
            'name_last'  => [
                'label' => 'Фамилия',
            ],
        ],
    ]);
    if ($sort->orders) $dataProvider->query->orderBy($sort->orders);
    ?>
    <div class="col-lg-12">
        <h1 class="page-header"><?= Html::encode($this->title) ?></h1>

        <?= \yii\grid\GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel'  => $searchModel,
            'tableOptions' => [
                'class' => 'table table-striped table-hover',
            ],
            'rowOptions'   => function ($item) {
                return [
                    'data'  => ['id' => $item['id']],
                    'role'  => 'button',
                    'class' => 'rowTable'
                ];
            },
            'columns'      => [
                [
                    'header'        => $sort->link('id'),
                    'headerOptions' => [
                        'style' => 'width: 10%;'
                    ],
                    'attribute'     => 'id',
                ],
                [
                    'header'        => 'Аватар',
                    'headerOptions' => [
                        'style' => 'width: 10%;'
                    ],
                    'filter'        => [
                        0 => 'Нет',
                        1 => 'Есть',
                    ],
                    'content'       => function ($item) {
                        return Html::img((new \app\models\User($item))->getAvatar(), [
                            'class'  => "thumbnail",
                            'width'  => 80,
                            'height' => 80,
                            'style'  => 'margin-bottom: 0px;',
                        ]);
                    },
                    'attribute'     => 'avatar',
                ],
                [
                    'header'        => $sort->link('email'),
                    'headerOptions' => [
                    ],
                    'attribute'     => 'email',
                ],
                [
                    'header'        => $sort->link('name_first'),
                    'headerOptions' => [
                    ],
                    'attribute'     => 'name_first',
                ],
                [
                    'header'        => $sort->link('name_last'),
                    'headerOptions' => [
                    ],
                    'attribute'     => 'name_last',
                ],
                [
                    'header'        => 'VK',
                    'headerOptions' => [
                    ],
                    'filter'        => [
                        0 => 'Нет',
                        1 => 'Есть',
                    ],
                    'content'       => function ($item) {
                        $vk = ArrayHelper::getValue($item, 'vk_id', '');
                        if ($vk) {
                            return Html::a(
                                Html::tag('i', null, ['class' => 'fa fa-vk']),
                                'https://vk.com/id' . $vk,
                                [
                                    'class'  => 'btn btn-info btn-sm',
                                    'target' => '_blank',
                                    'style'  => 'width:40px;',
                                ]
                            );
                        }
                        return '';
                    },
                    'attribute' => 'vk_id',
                ],
                [
                    'header'        => 'FB',
                    'headerOptions' => [
                    ],
                    'filter'        => [
                        0 => 'Нет',
                        1 => 'Есть',
                    ],
                    'attribute' => 'fb_id',
                    'content'       => function ($item) {
                        $fb = ArrayHelper::getValue($item, 'fb_id', '');
                        if ($fb) {
                            return Html::a(
                                Html::tag('i', null, ['class' => 'fa fa-facebook']),
                                'https://www.facebook.com/profile.php?id=' . $fb,
                                [
                                    'class'  => 'btn btn-info btn-sm',
                                    'target' => '_blank',
                                    'style'  => 'width:40px;',
                                ]
                            );
                        }
                        return '';
                    },
                ],
                [
                    'header'  => 'Удалить',
                    'content' => function ($item) {
                        return Html::button('Удалить', [
                            'class' => 'btn btn-danger btn-xs buttonDelete',
                            'data'  => [
                                'id' => $item['id'],
                            ]
                        ]);
                    }
                ],
            ],
        ]) ?>

    </div>
    <?php \yii\widgets\Pjax::end(); ?>
</div>
