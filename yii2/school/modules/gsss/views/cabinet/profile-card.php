<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;
use app\models\UnionCategory;
use \yii\helpers\ArrayHelper;

/* @var $this  \yii\web\View */
/* @var $model \app\models\Form\ProfileHumanDesignCalc */


$this->title = 'Карта Аватара';

/** @var \app\models\User $user */
$user = Yii::$app->user->identity;
$birthDate = $user->getField('birth_date');
\app\assets\SlideShow\Asset::register($this);

?>
<div class="container">
<div class="col-lg-12">
<h1 class="page-header"><?= Html::encode($this->title) ?></h1>
<?= \cs\Widget\BreadCrumbs\BreadCrumbs::widget([
    'items' => [
        $this->title
    ],
    'home'  => [
        'name' => 'я',
        'url'  => \yii\helpers\Url::to(['site/user', 'id' => Yii::$app->user->id])
    ]
]) ?>
<hr>

<div class="col-lg-8">
    <div class="panel panel-default">
        <div class="panel-heading">Карта Аватара
        </div>
        <div class="panel-body">
            <p><img src="/upload/FileUpload3/gs_unions_shop_product/00000051/original/image.jpg" class="thumbnail" width="100%"></p>
            <p>Карта Аватара - явяется платежным инструментом&nbsp;и кошельком.</p>    <p>Работает на Биткойн сети и является официальным&nbsp;Биткойн кошельком.</p>    <p>Счет принадлежит только вам по вашему персональному паролю.</p>    <p>Деньги хранятся в биткойн сети и доступ к ним осуществляется только через ваш персональный пароль.</p>    <p>Работает на Блокчейн сети, что означает, что воровство исключено по определению и гарантируется криптотехнологией.</p>    <p>Ваш счёт не может быть заблокирован ни в каком случае. Так как сеть биткойн действует подобно торрент системе и её невозможно остановить.</p>    <p>При помощи карты Аватара вы легко можете как принимать платежи так и оплачивать.</p>    <p>Приглашая друзей играть в нашу Игру используя нашу Партнерскую программу вы приумножаете деньги и радуетесь общему успеху друг друга.</p>    <p>Кошелек Аватара содержит в себе пять валют:</p>    <p>- Биткойн (BTC)</p>    <p>- Эфир (ETH)</p>    <p>- Энергорубли (ENR)</p>    <p>- Edinar coin (EDC)</p>    <p>- Аватары (AVR) валюта стандарта Золотого Века.</p>    <p>- EnergyCoin (ERC)</p>    <p>Встроенная биржа. Любую валюту вы можете мгновенно разменять на любую по минимальному тарифу</p>    <p>Карта Аватара снабжена <a href="http://www.galaxysss.ru/shop/product/34">квантовой голограммой Аватара Свободы</a> которая активирует Кристаллическое Сапфировое Тело Света для получения сверхспособностей.</p>    <p>Есть банк клиент, доступен с браузера и с телефона!</p>    <p>Мгновенные переводы в любою точку мира.</p>    <p>Девиз банка Аватар – Пусть Благо приумножится!</p>    <p>Это значит, что мы делаем Людей БОГАтыМИ!</p>    <p>Сайт банка Аватар: <a href="https://www.avatar-bank.com">https://www.avatar-bank.com</a></p>  <p></p>

            <a class="btn btn-success btn-lg gsssTooltip" title="" href="/shop/product/51" style="width: 100%; margin-bottom: 50px;margin-top: 50px;" data-original-title="Бысрая покупка">
                Купить
            </a>
            <h2 class="page-header text-center">Изображения</h2>
            <div class="col-lg-4" style="margin-bottom: 20px;">
                <a href="/upload/FileUpload3/gs_unions_shop_product_images/00000126/original/image.jpg" rel="lightbox[example]" class="highslide" onclick="return hs.expand(this)">
                    <img src="/upload/FileUpload3/gs_unions_shop_product_images/00000126/small/image.jpg" width="100%">
                </a>
            </div>
            <div class="col-lg-4" style="margin-bottom: 20px;">
                <a href="/upload/FileUpload3/gs_unions_shop_product_images/00000127/original/image.jpg" rel="lightbox[example]" class="highslide" onclick="return hs.expand(this)">
                    <img src="/upload/FileUpload3/gs_unions_shop_product_images/00000127/small/image.jpg" width="100%">
                </a>
            </div>
            <div class="col-lg-4" style="margin-bottom: 20px;">
                <a href="/upload/FileUpload3/gs_unions_shop_product_images/00000128/original/image.jpg" rel="lightbox[example]" class="highslide" onclick="return hs.expand(this)">
                    <img src="/upload/FileUpload3/gs_unions_shop_product_images/00000128/small/image.jpg" width="100%">
                </a>
            </div>
            <div class="col-lg-4" style="margin-bottom: 20px;">
                <a href="/upload/FileUpload3/gs_unions_shop_product_images/00000129/original/image.jpg" rel="lightbox[example]" class="highslide" onclick="return hs.expand(this)">
                    <img src="/upload/FileUpload3/gs_unions_shop_product_images/00000129/small/image.jpg" width="100%">
                </a>
            </div>
            <div class="col-lg-4" style="margin-bottom: 20px;">
                <a href="/upload/FileUpload3/gs_unions_shop_product_images/00000130/original/image.jpg" rel="lightbox[example]" class="highslide" onclick="return hs.expand(this)">
                    <img src="/upload/FileUpload3/gs_unions_shop_product_images/00000130/small/image.jpg" width="100%">
                </a>
            </div>
            <div class="col-lg-4" style="margin-bottom: 20px;">
                <a href="/upload/FileUpload3/gs_unions_shop_product_images/00000131/original/image.jpg" rel="lightbox[example]" class="highslide" onclick="return hs.expand(this)">
                    <img src="/upload/FileUpload3/gs_unions_shop_product_images/00000131/small/image.jpg" width="100%">
                </a>
            </div>
            <div class="col-lg-4" style="margin-bottom: 20px;">
                <a href="/upload/FileUpload3/gs_unions_shop_product_images/00000132/original/image.jpg" rel="lightbox[example]" class="highslide" onclick="return hs.expand(this)">
                    <img src="/upload/FileUpload3/gs_unions_shop_product_images/00000132/small/image.jpg" width="100%">
                </a>
            </div>
            <div class="col-lg-4" style="margin-bottom: 20px;">
                <a href="/upload/FileUpload3/gs_unions_shop_product_images/00000133/original/image.jpg" rel="lightbox[example]" class="highslide" onclick="return hs.expand(this)">
                    <img src="/upload/FileUpload3/gs_unions_shop_product_images/00000133/small/image.jpg" width="100%">
                </a>
            </div>
            <div class="col-lg-4" style="margin-bottom: 20px;">
                <a href="/upload/FileUpload3/gs_unions_shop_product_images/00000134/original/image.jpg" rel="lightbox[example]" class="highslide" onclick="return hs.expand(this)">
                    <img src="/upload/FileUpload3/gs_unions_shop_product_images/00000134/small/image.jpg" width="100%">
                </a>
            </div>
            <div class="col-lg-4" style="margin-bottom: 20px;">
                <a href="/upload/FileUpload3/gs_unions_shop_product_images/00000135/original/image.jpg" rel="lightbox[example]" class="highslide" onclick="return hs.expand(this)">
                    <img src="/upload/FileUpload3/gs_unions_shop_product_images/00000135/small/image.jpg" width="100%">
                </a>
            </div>
            <div class="col-lg-4" style="margin-bottom: 20px;">
                <a href="/upload/FileUpload3/gs_unions_shop_product_images/00000136/original/image.jpg" rel="lightbox[example]" class="highslide" onclick="return hs.expand(this)">
                    <img src="/upload/FileUpload3/gs_unions_shop_product_images/00000136/small/image.jpg" width="100%">
                </a>
            </div>



        </div>
    </div>
</div>
<div class="col-lg-4">
    <?= $this->render('profile_menu/profile_menu') ?>
</div>


</div>


</div>
