<?php

use yii\helpers\Url;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;

$this->title = 'Мои объединения';



?>
<div class="container">
<div class="col-lg-12">
<h1 class="page-header">Мои объединения</h1>
<?= \cs\Widget\BreadCrumbs\BreadCrumbs::widget([
    'items' => [
        $this->title
    ],
    'home'  => [
        'name' => 'я',
        'url'  => Url::to(['site/user', 'id' => Yii::$app->user->id])
    ]
]) ?>
<hr>

<a href="/objects/add" class="btn btn-default" style="margin-bottom: 20px;">Добавить</a>

<?php \yii\widgets\Pjax::begin(); ?>
<?php
$searchModel = new \app\models\Form\UnionSearch();
$dataProvider = $searchModel->search(Yii::$app->request->get(), ['user_id' => Yii::$app->user->id]);
$sort = new \yii\data\Sort([
    'attributes' => [
        'name' => [
            'label' => 'Название',
        ],
        'id',
    ]
]);
if ($sort->orders) $dataProvider->query->orderBy($sort->orders);

$this->registerJs(<<<JS
    $('.buttonDelete').click(function (e) {
        if (confirm('Подтвердите удаление')) {
            e.preventDefault();
            e.stopPropagation();
            var id = $(this).data('id');
            var a = $(this).parent().parent();
            ajaxJson({
                url: '/objects/' + id + '/delete',
                success: function (ret) {
                    a.remove();
                    infoWindow('Успешно', function() {

                    });
                }
            });
        }
    });
    $('.buttonShop').click(function (e) {
        e.preventDefault();
        e.stopPropagation();
        var id = $(this).parent().parent().data('id');
        window.location = '/cabinet/shop/' + id;
    });
    $('.buttonRequests').click(function (e) {
        e.preventDefault();
        e.stopPropagation();
        var id = $(this).parent().parent().data('id');
        window.location = '/cabinet/shop/' + id + '/requestList';
    });
    $('.buttonProducts').click(function (e) {
        e.preventDefault();
        e.stopPropagation();
        var id = $(this).parent().parent().data('id');
        window.location = '/cabinet/shop/' + id + '/productList';
    });

    // Отправить на модерацию
    $('.buttonSendModeration').click(function (e) {
        e.preventDefault();
        e.stopPropagation();
        if (confirm('Подтвердите свой выбор')) {
            var button = $(this);
            var id = $(this).data('id');
            var a = $(this).parent().parent();
            ajaxJson({
                url: '/objects/' + id + '/sendModeration',
                success: function (ret) {
                    a.remove();
                    infoWindow('Успешно', function() {
                    });
                }
            });
        }
    });

    // Сделать рассылку
    $('.buttonAddSiteUpdate').click(function (e) {
        e.preventDefault();
        e.stopPropagation();
        if (confirm('Подтвердите')) {
            var buttonSubscribe = $(this);
            var id = $(this).data('id');
            ajaxJson({
                url: '/objects/' + id + '/subscribe',
                success: function (ret) {
                    infoWindow('Успешно', function() {
                        buttonSubscribe.remove();
                    });
                }
            });
        }
    });

    $('.rowTable').click(function() {
        window.location = '/objects/' + $(this).data('id') + '/edit';
    });
JS
);
?>
<?= \yii\grid\GridView::widget([
    'dataProvider' => $dataProvider,
    'filterModel'  => $searchModel,
    'tableOptions' => [
        'class' => 'table table-striped table-hover',
    ],
    'rowOptions'   => function ($item) {
        return [
            'data'  => ['id' => $item['id']],
            'role'  => 'button',
            'class' => 'rowTable'
        ];
    },
    'columns'      => ArrayHelper::merge([
        [
            'attribute'     => 'id',
            'header'        => $sort->link('id'),
            'headerOptions' => [
                'style' => Html::cssStyleFromArray([
                    'width' => '5%',
                ])
            ],
        ],
        [
            'header'        => 'Картинка',
            'filter'        => [
                0 => 'Нет',
                1 => 'Есть',
            ],
            'headerOptions' => [
                'style' => Html::cssStyleFromArray([
                    'width' => '10%',
                ])
            ],
            'attribute'     => 'img',
            'content'       => function ($item) {
                $i = ArrayHelper::getValue($item, 'img', '');
                if ($i == '') return '';

                return Html::img($i, [
                    'class'  => "thumbnail",
                    'width'  => 80,
                    'height' => 80,
                    'style'  => Html::cssStyleFromArray(['margin-bottom' => '0px']),
                ]);
            }
        ],
        [
            'header'    => $sort->link('name'),
            'attribute' => 'name',
        ],
        [
            'header'        => 'Дата',
            'headerOptions' => [
                'style' => Html::cssStyleFromArray([
                    'width' => '10%',
                ])
            ],
            'content'       => function ($item) {
                return Html::tag('span', \app\services\GsssHtml::dateString($item['date_insert']), ['style' => 'font-size: 80%; margin-bottom:10px; color: #c0c0c0;']);
            }
        ],
        [
            'header'        => 'Удалить',
            'headerOptions' => [
                'style' => Html::cssStyleFromArray([
                    'width' => '10%',
                ])
            ],
            'content'       => function ($item) {
                return \yii\helpers\Html::button('Удалить', [
                    'class' => 'btn btn-danger btn-xs buttonDelete',
                    'data'  => [
                        'id' => $item['id'],
                    ]
                ]);
            }
        ],
        [
            'header'        => 'Модерация',
            'headerOptions' => [
                'style' => Html::cssStyleFromArray([
                    'width' => '15%',
                ])
            ],
            'filter'        => [
                0 => 'Редактируется',
                1 => 'Отклонено',
                2 => 'Утверждено',
                3 => 'На рассмотрении',
            ],
            'attribute'     => 'moderation_status',
            'content'       => function ($item) {
                $class = 'glyphicon glyphicon-time';
                if (isset($item['moderation_status'])) {
                    switch ($item['moderation_status']) {
                        case 0: // отклонено
                            $class = 'glyphicon glyphicon-remove';
                            break;
                        case 1: // одобрено
                            $class = 'glyphicon glyphicon-ok';
                            break;
                        case 2: // оправлено на утверждение модератором
                            $class = 'glyphicon glyphicon-eye-open';
                            break;
                    }
                }

                return Html::tag('span', Html::tag('span', null, ['class' => $class]), ['label label-default']);
            },
        ],
        [
            'header'        => 'Магазин',
            'headerOptions' => [
                'style' => Html::cssStyleFromArray([
                    'width' => '10%',
                ])
            ],
            'content'       => function ($item) {
                if (ArrayHelper::getValue($item, 'is_shop', 0) != 1) {
                    return '';
                }
                $arr = [];
                $arr[] = Html::button('Магазин', ['class' => 'btn btn-default btn-xs buttonShop', 'style' => Html::cssStyleFromArray(['margin-bottom' => '5px'])]);
                $arr[] = Html::button('Заказы', ['class' => 'btn btn-default btn-xs buttonRequests', 'style' => Html::cssStyleFromArray(['margin-bottom' => '5px'])]);
                $arr[] = Html::button('Товары', ['class' => 'btn btn-default btn-xs buttonProducts', 'style' => Html::cssStyleFromArray(['margin-bottom' => '5px'])]);

                return join("<br>", $arr);
            }
        ],

    ], (\Yii::$app->user->identity->isAdmin()) ? [
        [
            'header'        => 'Рассылка',
            'headerOptions' => [
                'style' => Html::cssStyleFromArray([
                    'width' => '10%',
                ])
            ],
            'content'       => function ($item) {
                if (ArrayHelper::getValue($item, 'is_added_site_update', 0) == 1) {
                    return '';
                }

                return Html::button('Рассылка', [
                    'class' => 'btn btn-success btn-xs buttonAddSiteUpdate',
                    'data'  => [
                        'id' => $item['id'],
                    ]
                ]);
            }
        ]
    ] : [])
]) ?>
<?php \yii\widgets\Pjax::end(); ?>
</div>
</div>