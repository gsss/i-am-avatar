<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;
use app\models\UnionCategory;
use yii\helpers\ArrayHelper;
use app\models\HdGenKeys;

/* @var $this  \yii\web\View */
/* @var $model \app\models\Form\ProfileHumanDesignCalc */


$this->title = 'Дизайн Аватара';

/** @var \app\models\User $user */
$user = Yii::$app->user->identity;
?>
<div class="container">
<div class="col-lg-12">
<h1 class="page-header"><?= Html::encode($this->title) ?></h1>
<?= \cs\Widget\BreadCrumbs\BreadCrumbs::widget([
    'items' => [
        $this->title
    ],
    'home'  => [
        'name' => 'я',
        'url'  => \yii\helpers\Url::to(['site/user', 'id' => Yii::$app->user->id])
    ]
]) ?>
<hr>



<div class="col-lg-8">
<?php if ($user->hasHumanDesign()): ?>
    <?php $humanDesign = $user->getHumanDesign();

    $this->registerJs(<<<JS
$('.buttonDelete').click(function(){
    ajaxJson({
            url: '/cabinet/profile/humanDesign/delete',
            success: function(){
                location.reload();
            }
        })
    }
);
JS
    );
    ?>

    <div class="panel panel-default">
        <div class="panel-heading">Дизайн Аватара

            <div class="btn-group pull-right">
                <button type="button" class="btn btn-default btn-xs buttonDelete">
                    Удалить и пересчитать
                </button>
            </div>
        </div>
        <div class="panel-body">
            <img src="<?= $humanDesign->getImage() ?>" style="width: 100%;">
            <table class="table table-striped table-hover" style="width: auto;" align="center">
                <tr>
                    <td>Тип</td>
                    <td>
                        <a href="<?= \app\modules\HumanDesign\calculate\YourHumanDesignRu::$links['type'][$humanDesign->type->href] ?>">
                            <?= $humanDesign->type->text ?>
                        </a>
                    </td>
                </tr>
                <tr>
                    <td>Профиль</td>
                    <td>
                        <a href="<?= \app\modules\HumanDesign\calculate\YourHumanDesignRu::$links['profile'][$humanDesign->profile->href] ?>">
                            <?= $humanDesign->profile->text ?>
                        </a>
                    </td>
                </tr>
                <tr>
                    <td>Определение</td>
                    <td><?= $humanDesign->definition->text ?></td>
                </tr>
                <tr>
                    <td>Внутренний Авторитет</td>
                    <td><?= $humanDesign->inner->text ?></td>
                </tr>
                <tr>
                    <td>Стратегия</td>
                    <td><?= $humanDesign->strategy->text ?></td>
                </tr>
                <tr>
                    <td>Тема ложного Я</td>
                    <td><?= $humanDesign->theme->text ?></td>
                </tr>
                <tr>
                    <td>Подпись</td>
                    <td><?= (is_null($humanDesign->signature)) ? '' : $humanDesign->signature->text ?></td>
                </tr>
                <tr>
                    <td>Инкарнационный крест</td>
                    <?php
                    $text = $humanDesign->cross->text;
                    $arr = explode('(', $text);
                    $text = trim($arr[0]);
                    $isError = false;
                    if (count($arr) == 1) {
                        \cs\Application::mail('admin@galaxysss.ru', 'Ошибка', 'admin', [
                            'text' => '/galaxysss/views/site/user.php:187 ' . \yii\helpers\VarDumper::dumpAsString([$arr, $humanDesign])
                        ]);
                        $isError = true;
                    } else {
                        $arr = explode(')', $arr[1]);
                        $arr = explode('|', $arr[0]);
                        $new = [];
                        foreach ($arr as $i) {
                            $a = explode('/', $i);
                            $new[] = trim($a[0]);
                            $new[] = trim($a[1]);
                        }
                    }
                    ?>
                    <td><?= $text ?>
                        <?php if (!$isError) { ?>
                            (<a
                                href="<?= Url::to(['human_design/gen_keys_item', 'id' => $new[0]]) ?>"><?= $new[0] ?></a>/
                            <a
                                href="<?= Url::to(['human_design/gen_keys_item', 'id' => $new[1]]) ?>"><?= $new[1] ?></a>
                            | <a
                                href="<?= Url::to(['human_design/gen_keys_item', 'id' => $new[2]]) ?>"><?= $new[2] ?></a>/
                            <a
                                href="<?= Url::to(['human_design/gen_keys_item', 'id' => $new[3]]) ?>"><?= $new[3] ?></a>)
                        <?php } ?>
                    </td>
                </tr>
            </table>
            <h3>Генные ключи</h3>
            <table class="table table-striped table-hover">
                <tr>
                    <td>
                        #
                    </td>
                    <td>
                        Название
                    </td>
                    <td>
                        Тень
                    </td>
                    <td>
                        Дар
                    </td>
                    <td>
                        Сиддхи
                    </td>
                </tr>
                <?php foreach ($new as $i) { ?>
                    <?php $genKey = HdGenKeys::find(['num' => $i]); ?>
                    <tr data-num="<?= $genKey->getField('num') ?>" role="button">
                        <td>
                            <a href="<?= Url::to(['human_design/gen_keys_item', 'id' => $genKey->getField('num')]) ?>">
                                <?= $genKey->getField('num') ?>
                            </a>
                        </td>
                        <td>
                            <?= $genKey->getField('name') ?>
                        </td>
                        <td>
                            <?= $genKey->getField('ten') ?>
                        </td>
                        <td>
                            <?= $genKey->getField('dar') ?>
                        </td>
                        <td>
                            <?= $genKey->getField('siddhi') ?>
                        </td>
                    </tr>
                <?php } ?>
            </table>
        </div>
    </div>

<?php else: ?>
    <p>Для того чтобы расчитать Дизайн Человека вам необходимо заполнить следующие данные</p>
    <div class="hide" id="loginFormLoading">
        <img style="padding-left: 10px; padding-right: 10px;"
             src="<?= \Yii::$app->assetManager->getBundle(\app\assets\App\Asset::className())->baseUrl ?>/images/ajax-loader.gif"
            >
    </div>
    <!--                Форма подключения дизайна человека -->
    <div class="col-lg-8 row">
        <?php
        $this->registerJs(<<<JS
    /**
    * проверяет наличие в массиве item.types щначений 'locality' и 'political'
    * @param item
    */
    function is_good(item)
    {
        var l = false;
        var p = false;
        for(i = 0; i < item.types.length; i++) {
            if (item.types[i] == 'locality') {
                l = true;
            }
            if (item.types[i] == 'political') {
                p = true;
            }
        }

        return l & p;
    }

    /**
    *
    * @param  latLng object {lat: '', lng: ''}
    * @param  londonOffset int kol-vo sek
    * @param  placeOffset int kol-vo sek
    * @param  location string lat + ',' + lng
    * @returns {string}
    */
    function geocodeLatLng(latLng, londonOffset, placeOffset, location) {
        var geocoder = new google.maps.Geocoder();
        var latlng = new google.maps.LatLng(parseFloat(latLng.lat), parseFloat(latLng.lng));
        geocoder.geocode({latLng: latlng}, function (results, status) {
            var ret1 = '';
            if (status == google.maps.GeocoderStatus.OK) {
                for(j = 0; j < results.length; j++){
                    if (is_good(results[j])) {
                        ret1 = results[j].formatted_address;
                        break;
                    }
                }
            }
            ajaxJson({
                url: '/cabinet/profile/humanDesign/ajax',
                data: {
                    date: $('#profilehumandesigncalc-date').val(),
                    time: $('#profilehumandesigncalc-time').val(),
                    londonOffset: londonOffset,
                    placeOffset: placeOffset,
                    location: location,
                    born: ret1
                },
                success: function(ret3) {
                    window.location.reload();
                }
            });
        });
    }

    $('#buttonNext').click(function(){
        var url = 'https://maps.googleapis.com/maps/api/timezone/json';
        var lng = $('#profilehumandesigncalc-point-lng').val();
        var lat = $('#profilehumandesigncalc-point-lat').val();
        var latLng = lat + ',' + lng;
        var date = $('#profilehumandesigncalc-date').val();
        date = date.split('.');
        var d = new Date();
        d.setFullYear(date[2]);
        d.setMonth(date[1]-1);
        d.setDate(date[0]);
        var time = $('#profilehumandesigncalc-time').val();
        time = time.split(':');
        d.setHours(time[0]);
        d.setMinutes(time[1]);
        timestamp = parseInt(d.getTime()/1000);
        $.ajax({
            url: url,
            data: {
                location: latLng,
                timestamp: timestamp
            },
            beforeSend: function () {
                $('#buttonNext').html($('#loginFormLoading').html());
            },
            dataType: 'json',
            success: function(ret) {
                if (ret.status == 'OK') {
                    $.ajax({
                        url: 'https://maps.googleapis.com/maps/api/timezone/json',
                        data: {
                            location: '51.5073509,-0.12775829999998223', // london
                            timestamp: timestamp
                        },
                        dataType: 'json',
                        success: function(ret2) {
                            geocodeLatLng({lat:lat, lng:lng}, ret2.dstOffset, ret.dstOffset + ret.rawOffset, latLng);
                        }
                    });
                }
            }
        });
    });
JS
        );
        $form = ActiveForm::begin([
            'id'                 => 'contact-form',
            'enableClientScript' => true,

        ]); ?>

        <?= $model->field($form, 'date') ?>
        <?= $model->field($form, 'time') ?>
        <?= $model->field($form, 'point') ?>

        <div class="form-group">
            <hr>
            <?= Html::button('Далее', [
                'class' => 'btn btn-primary',
                'name'  => 'contact-button',
                'style' => 'width:100%',
                'id'    => 'buttonNext'
            ]) ?>
        </div>
        <?php ActiveForm::end(); ?>
    </div>
    <!-- /Форма подключения дизайна человека -->

<?php endif; ?>
</div>
<div class="col-lg-4">
    <?= $this->render('profile_menu/profile_menu') ?>
</div>

</div>


</div>
