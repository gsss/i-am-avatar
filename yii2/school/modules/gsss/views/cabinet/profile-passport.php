<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;
use app\models\UnionCategory;
use \yii\helpers\ArrayHelper;

/* @var $this  \yii\web\View */
/* @var $model \app\models\Form\ProfileHumanDesignCalc */


$this->title = 'Паспорт Аватара';

/** @var \app\models\User $user */
$user = Yii::$app->user->identity;
$birthDate = $user->getField('birth_date');

?>
<div class="container">
    <div class="col-lg-12">
        <h1 class="page-header"><?= Html::encode($this->title) ?></h1>
        <?= \cs\Widget\BreadCrumbs\BreadCrumbs::widget([
            'items' => [
                $this->title
            ],
            'home'  => [
                'name' => 'я',
                'url'  => \yii\helpers\Url::to(['site/user', 'id' => Yii::$app->user->id])
            ]
        ]) ?>
        <hr>
        <div class="col-lg-8">

            <div class="panel panel-default">
                <div class="panel-heading">Паспорт Аватара</div>
                <div class="panel-body">
                    <p>Все в нашей игре являются Космическими Братьями и Сестрами! Каждый имеет Карту Жизни Гражданина
                        Галактики для
                        взаимодействия с другими участниками игры.</p>

                    <p><img src="/images/new_earth/game/pass/obl.jpg" class="thumbnail" width="100%"></p>

                    <?php
                    \app\assets\SlideShow\Asset::register($this);
                    $images = [
                        [
                            '/images/new_earth/game/pass/1.jpg',
                            '/images/new_earth/game/pass/1_2.jpg',
                        ],
                        [
                            '/images/new_earth/game/pass/2.jpg',
                            '/images/new_earth/game/pass/2_2.jpg',
                        ],
                        [
                            '/images/new_earth/game/pass/3.jpg',
                            '/images/new_earth/game/pass/3_2.jpg',
                        ],
                        [
                            '/images/new_earth/game/pass/4.jpg',
                            '/images/new_earth/game/pass/4_2.jpg',
                        ],
                        [
                            '/images/new_earth/game/pass/5.jpg',
                            '/images/new_earth/game/pass/5_2.jpg',
                        ],
                        [
                            '/images/new_earth/game/pass/6.jpg',
                            '/images/new_earth/game/pass/6_2.jpg',
                        ],
                        [
                            '/images/new_earth/game/pass/7.jpg',
                            '/images/new_earth/game/pass/7_2.jpg',
                        ],
                        [
                            '/images/new_earth/game/pass/8.jpg',
                            '/images/new_earth/game/pass/8_2.jpg',
                        ],
                    ];
                    ?>
                    <div class="row">
                        <?php foreach ($images as $item) { ?>
                            <div class="col-lg-4" style="margin-bottom: 20px;">
                                <a href="<?= $item[0] ?>" rel="lightbox[example]" class="highslide"
                                   onclick="return hs.expand(this)">
                                    <img src="<?= $item[1] ?>" width="100%">
                                </a>
                            </div>
                        <?php } ?>
                    </div>
                    <p>Пасспорт стоит 10,000 руб</p>

                    <p class="text-center lead page-header">Начни новую жизнь!</p>
                    <p>Заведи себе имя Аватара</p>
                    <p>Как корабль назовешь так он и поплывет</p>
                    <p>Твое тело это корабль твоего путешествия</p>
                    <p>Все в твоих руках</p>
                    <p>Звони: +7-925-237-45-01</p>
                    <hr>
                    <p><a href="http://www.galaxysss.ru/shop/product/50" class="btn btn-success btn-lg" style="width: 100%;">Купить</a></p>
                </div>

            </div>


        </div>
        <div class="col-lg-4">
            <?= $this->render('profile_menu/profile_menu') ?>
        </div>


    </div>
</div>
