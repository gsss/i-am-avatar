<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;
use app\models\UnionCategory;
use yii\db\Query;
use yii\helpers\Url;

/* @var $this yii\web\View */
$requestedRoute = Yii::$app->requestedRoute;
$menu = [
    [
        'route' => 'cabinet/profile',
        'label' => 'Профиль',
    ],
    [
        'route' => 'cabinet/profile_subscribe',
        'label' => 'Рассылки',
    ],
    [
        'route' => 'cabinet/profile_human_design',
        'label' => 'Дизайн Аватара',
    ],
    [
        'route' => 'cabinet/profile-passport',
        'label' => 'Паспорт Аватара',
    ],
    [
        'route' => 'cabinet/profile-card',
        'label' => 'Карта Аватара',
    ],
    [
        'route' => 'cabinet/profile_zvezdnoe',
        'label' => 'Звездное происхождение',
    ],
    [
        'route' => 'cabinet/profile_time',
        'label' => 'Персональная Галактическая Печать',
    ],
    [
        'route' => 'cabinet/profile_possibilities',
        'label' => 'Возможности профиля',
    ],
];
if (\yii\helpers\ArrayHelper::getValue(Yii::$app->params,'isShop', 0)) {
    $menu = \yii\helpers\ArrayHelper::merge($menu, [
        [
            'route' => 'cabinet_wallet/index',
            'label' => 'Кошелек',
        ],
        [
            'route' => 'cabinet_wallet/childs',
            'label' => 'Мои дети',
        ],
        [
            'route' => 'cabinet_wallet/referal_link',
            'label' => 'Реферальная ссылка',
        ],
    ]);
}
?>

<div class="list-group">
    <?php
    foreach($menu as $item) {
        $options = ['class' => ['list-group-item']];
        if ($item['route'] == $requestedRoute) {
            $options['class'][] = 'active';
        }
        if (\yii\helpers\ArrayHelper::keyExists('data', $item)) {
            $options['data'] = $item['data'];
        }
        $options['class'] = join(' ', $options['class']);
        echo Html::a($item['label'], [$item['route']], $options);
    }
    ?>
</div>
