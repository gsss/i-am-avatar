<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;
use app\models\UnionCategory;
use \yii\helpers\ArrayHelper;

/* @var $this  \yii\web\View */
/* @var $model \app\models\Form\ProfileHumanDesignCalc */


$this->title = 'Возможности профиля';

/** @var \app\models\User $user */
$user = Yii::$app->user->identity;
?>
<div class="container">
    <div class="col-lg-12">
        <h1 class="page-header"><?= Html::encode($this->title) ?></h1>
        <?= \cs\Widget\BreadCrumbs\BreadCrumbs::widget([
            'items' => [
                $this->title
            ],
            'home'  => [
                'name' => 'я',
                'url'  => \yii\helpers\Url::to(['site/user', 'id' => Yii::$app->user->id])
            ]
        ]) ?>
        <hr>

        <div class="col-lg-8">


            <div class="panel panel-default">
                <div class="panel-heading">Размещение посланий</div>
                <div class="panel-body">
                    <table class="table table-hover table-striped" style="width:auto;">
                        <tr>
                            <td>
                                Модерация
                            </td>
                            <td>
                                <?= (Yii::$app->user->identity->get('mode_chenneling', 0) == 1)? 'Включена' : 'Отключена'?>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Возможность рассылки
                            </td>
                            <td>
                                <?= (Yii::$app->user->identity->get('mode_chenneling_is_subscribe', 0) == 1)? 'Есть': 'Отключена'?>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
        <div class="col-lg-4">
            <?= $this->render('profile_menu/profile_menu') ?>
        </div>
    </div>
</div>
