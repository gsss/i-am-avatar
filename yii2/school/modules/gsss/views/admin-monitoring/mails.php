<?php

/**
 * @var $this \yii\web\View
 */

use yii\widgets\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;
use cs\Widget\HighCharts\HighCharts;

$this->title = 'Письма';
?>

<style>
    a.asc:after {
        content: ' ↓';
        display: inline;
    }

    a.desc:after {
        content: ' ↑';
        display: inline;
    }

    .tableStat td {
        font-family: "Courier New", Courier, monospace;
    }
</style>

<div class="container">
    <div class="col-lg-12">
        <h1 class="page-header"><?= $this->title ?></h1>


        <?php
        $period = 24 * 2 * 30;

        $rows3 = \common\models\statistic\StatisticMail::find()
            ->select([
                'time',
                'mail',
            ])
            ->where(['between', 'time', time() - 60 * 60 * $period, time()])
            ->all();
        $rows3Json = \yii\helpers\Json::encode($rows3);
        $this->registerJs(<<<JS
        var rows3 = {$rows3Json};
        var newRows3 = [];
        for(i = 0; i < rows3.length; i++)
        {
            var item3 = rows3[i];
            newRows3.push({
                x: new Date(item3.time * 1000),
                y: parseInt(item3.mail)
            });
        }

JS
        );
        ?>
        <?= HighCharts::widget([
            'chartOptions' => [
                'chart'       => [
                    'zoomType' => 'x',
                    'type'     => 'spline',
                    'events'   => [
                        'selection' => new \yii\web\JsExpression(<<<JS
function(event) {
    if (typeof(event.xAxis) == "undefined") {
        console.log(1);
    } else {
        // log the min and max of the primary, datetime x-axis
        console.log(
            Highcharts.dateFormat(
                '%Y-%m-%d %H:%M:%S',
                event.xAxis[0].min
            ),
            Highcharts.dateFormat(
                '%Y-%m-%d %H:%M:%S',
                event.xAxis[0].max
            )
        );
        // log the min and max of the y axis
        console.log(event.yAxis[0].min, event.yAxis[0].max);
    }
}
JS
                        ),
                    ],
                ],
                'title'       => [
                    'text' => 'График',
                ],
                'subtitle'    => [
                    'text' => 'Выделите область для изменения масштаба',
                ],
                'xAxis'       => [
                    'type' => 'datetime',
                ],
                'yAxis'       => [
                    [
                        'title' => [
                            'text' => 'Количество',
                        ],
                    ],
                ],
                'legend'      => [
                    'enabled' => true,
                ],
                'tooltip'     => [
                    'crosshairs' => true,
                    'shared'     => true,
                ],
                'plotOptions' => [
                    'series' => [
                        'turboThreshold' => 0,
                    ],
                ],
                'series'      => [
                    [
                        'type' => 'column',
                        'name' => 'Писем в мин',
                        'data' => new \yii\web\JsExpression('newRows3'),
                    ],
                ],
            ],
        ]);

        ?>

        <?php \yii\widgets\Pjax::begin() ?>
        <?php
        $sort = new \yii\data\Sort([
            'attributes'   => [
                'id',
                'time',
            ],
            'defaultOrder' => [
                'id' => SORT_DESC,
            ],
        ]);
        ?>
        <?= \yii\grid\GridView::widget([
            'dataProvider' => new \yii\data\ActiveDataProvider([
                'query' => \common\models\Mail::find(),
                'sort'  => $sort,
            ]),
            'tableOptions' => [
                'class' => 'table table-striped table-hover',
            ],
            'columns'      => [
                'id',
                'to',
                'from',
                [
                    'header'        => 'Отправлено',
                    'attribute'     => 'time',
                    'headerOptions' => [
                        'style' => 'width: 10%',
                    ],
                    'content'       => function ($item) {
                        $v = \yii\helpers\ArrayHelper::getValue($item, 'time', 0);
                        if ($v == 0) return '';

                        return Html::tag('abbr', \cs\services\DatePeriod::back($v, ['isShort' => true]), ['class' => 'gsssTooltip', 'title' => Yii::$app->formatter->asDatetime($v)]);
                    },
                ],
                [
                    'header'  => 'Результат?',
                    'content' => function ($item) {
                        $v = \yii\helpers\ArrayHelper::getValue($item, 'result', 0);
                        if ($v == 0) return '';

                        return Html::tag('span', Html::tag('span', null, ['class' => 'glyphicon glyphicon-ok']), ['class' => 'label label-success']);
                    },
                ],
            ],
        ]) ?>
        <?php \yii\widgets\Pjax::end() ?>

    </div>

</div>