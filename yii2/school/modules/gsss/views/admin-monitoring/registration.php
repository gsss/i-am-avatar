<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;

/* @var $this yii\web\View */
/* @var $lineArray array ['x' => array, 'y' => array[[1,2,3,..],...]] Регистрация за один день */
/* @var $lineArray2 array ['x' => array, 'y' => array[[1,2,3,..],...]] Прирост пользователей общий */

$this->title = 'Статистика';
?>
<div class="container">
    <div class="col-lg-12">
        <h1 class="page-header"><?= Html::encode($this->title) ?></h1>

        <h2 class="page-header">Регистрация за один день</h2>

        <?php
        $now = new DateTime();
        $start = (new DateTime())->sub(new DateInterval('P2Y'));


        $rows = \common\models\User::find()
            ->select([
                'count(*) as counter',
                'DATE(`datetime_reg`) as day',
            ])
            ->where(['between', 'datetime_reg', $start->format('Y-m-d H:i:s'), $now->format('Y-m-d H:i:s')])
            ->groupBy(['DATE(`datetime_reg`)'])
            ->asArray()
            ->all();

        $rowsJson = \yii\helpers\Json::encode($rows);
        $this->registerJs(<<<JS
        Highcharts.setOptions({
            global: {
                timezoneOffset: -3 * 60
            }
        });
        var rows = {$rowsJson};
        var newRows = [];
        for(i = 0; i < rows.length; i++)
        {
            var item = rows[i];
            newRows.push({
                x: new Date(item.day + 'T00:00:00'),
                y: parseInt(item.counter)
            });
        }
JS
        );
        ?>



        <?= \cs\Widget\HighCharts\HighCharts::widget([
            'chartOptions' => [
                'chart'       => [
                    'zoomType' => 'x',
                    'type'     => 'spline',
                ],
                'title'       => [
                    'text' => 'График',
                ],
                'subtitle'    => [
                    'text' => 'Выделите область для изменения масштаба',
                ],
                'xAxis'       => [
                    'type' => 'datetime',
                ],
                'yAxis'       => [
                    [
                        'title' => [
                            'text' => 'Количество',
                        ],
                    ],
                ],
                'legend'      => [
                    'enabled' => true,
                ],
                'tooltip'     => [
                    'crosshairs' => true,
                    'shared'     => true,
                ],
                'plotOptions' => [
                    'series' => [
                        'turboThreshold' => 0,
                    ],
                ],
                'series'      => [
                    [
                        'type' => 'column',
                        'name' => 'Registration',
                        'data' => new \yii\web\JsExpression('newRows'),
                    ],
                ],
            ],
        ]);

        ?>
    </div>
</div>
