<?php

/**
 * @var $this \yii\web\View
 * @var $category string
 */

use yii\widgets\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;
use app\common\assets\HighCharts\HighChartsAsset;
use app\common\models\StatisticRoute;


$this->title = $category;


?>
<style>
    a.asc:after {
        content: ' ↓';
        display: inline;
    }

    a.desc:after {
        content: ' ↑';
        display: inline;
    }

</style>

<div class="container">

    <?php \yii\widgets\Pjax::begin() ?>
    <h1 class="page-header"><?= $category ?></h1>
    <?php
    $sort = new \yii\data\Sort([
        'attributes'   => [
            'controller',
            'action',
            'counter' => [
                'default' => SORT_DESC,
            ],
            'min'     => [
                'default' => SORT_DESC,
            ],
            'avg'     => [
                'default' => SORT_DESC,
            ],
            'max'     => [
                'default' => SORT_DESC,
            ],
        ],
        'defaultOrder' => [
            'avg' => SORT_DESC,
        ]
    ]);
    ?>
    <?php
    ?>
    <style>
        .code {
            font-family: "Courier New", Courier, monospace;
        }
    </style>
    <?= \yii\grid\GridView::widget([
        'dataProvider' => new \yii\data\ActiveDataProvider([
            'query' => \app\models\Log::find()
                ->where([
                        'level' => \yii\log\Logger::LEVEL_ERROR,
                        'category' => $category,
                ])
                ->select([
                    '*',
                ])
                ->orderBy(['log_time' => SORT_DESC])
                ->asArray()
        ]),
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            // Simple columns defined by the data contained in $dataProvider.
            // Data from the model's column will be used.
            'id',
            'prefix',
            [
                'label'          => 'date',
                'contentOptions' => [
                    'nowrap' => 'nowrap',
                ],
                'content'        => function ($model, $key, $index, $column) {
                    return \Yii::$app->formatter->asDate((int)$model['log_time']);
                }
            ],
            [
                'label'          => 'time',
                'contentOptions' => [
                    'nowrap' => 'nowrap',
                ],
                'content'        => function ($model, $key, $index, $column) {
                    $isViewMilliseconds = true;

                    $suffix = '';
                    if ($isViewMilliseconds) {
                        $decimal = $model['log_time'] - (int)$model['log_time'];
                        $suffix = substr($decimal, 1, 4);
                    }
                    return \Yii::$app->formatter->asTime((int)$model['log_time']) . $suffix;
                }
            ],
            [
                'header'  => 'Сообщение',
                'content' => function ($model, $key, $index, $column) {
                    return Html::tag('pre', Html::encode($model['message'])); // $data['name'] for array data, e.g. using SqlDataProvider.
                }
            ],
        ]
    ]) ?>
    <?php \yii\widgets\Pjax::end() ?>
</div>