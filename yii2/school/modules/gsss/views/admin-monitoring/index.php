<?php

/**
 * @var $this \yii\web\View
 */

use yii\widgets\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;
use cs\Widget\HighCharts\HighCharts;

$this->title = 'Мониторинг системы';
?>

<style>
    a.asc:after {
        content: ' ↓';
        display: inline;
    }

    a.desc:after {
        content: ' ↑';
        display: inline;
    }
    .tableStat td {
        font-family: "Courier New", Courier, monospace;
    }
</style>

<div class="container">
    <div class="col-lg-12">
        <h1 class="page-header"><?= $this->title ?></h1>

        <?php
        $online = Yii::$app->onlineManager->count();
        $rpm = (float)Yii::$app->formatter->asDecimal((Yii::$app->monitoring->calculate()['request_per_second']) * 60, 1);

        $gaugeOptions = [
            'chart'       => [
                'type' => 'solidgauge',
            ],
            'title'       => null,
            'pane'        => [
                'center'     => ['50%', '85%'],
                'size'       => '140%',
                'startAngle' => -90,
                'endAngle'   => 90,
                'background' => [
                    'backgroundColor' => new \yii\web\JsExpression("(Highcharts.theme && Highcharts.theme.background2) || '#EEE'"),
                    'innerRadius'     => '60%',
                    'outerRadius'     => '100%',
                    'shape'           => 'arc',
                ],
            ],
            'tooltip'     => [
                'enabled' => true,
            ],
            'yAxis'       => [
                'stops'             => [
                    [0.1, '#55BF3B'], // green
                    [0.5, '#DDDF0D'], // yellow
                    [0.9, '#DF5353'] // red
                ],
                'lineWidth'         => 0,
                'minorTickInterval' => null,
                'tickPixelInterval' => 400,
                'tickWidth'         => 0,
                'title'             => [
                    'y' => -70,
                ],
                'labels'            => [
                    'y' => 16,
                ]
            ],
            'plotOptions' => [
                'solidgauge' => [
                    'dataLabels' => [
                        'y'           => 5,
                        'borderWidth' => 0,
                        'useHTML'     => true
                    ]
                ]
            ],
        ];
        $onlineOptions = [
            'yAxis' => [
                'min'   => 0,
                'max'   => 800,
                'title' => [
                    'text' => 'Online'
                ]
            ],
            'credits' => [
                'enabled' => false
            ],
            'series' => [[
                'name'       => 'Online',
                'data'       => [$online],
                'dataLabels' => [
                    'format' => new \yii\web\JsExpression("'<div style=\"text-align:center\"><span style=\"font-size:25px;color:' + ((Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black') + '\">{y}</span><br/>' + '<span style=\"font-size:12px;color:silver\">Шт</span></div>'")
                ],
                'tooltip'    => [
                    'valueSuffix' => ' шт',
                ]
            ]]
        ];
        $rpmOptions = [
            'yAxis' => [
                'min'   => 0,
                'max'   => 200,
                'title' => [
                    'text' => 'RPM'
                ]
            ],
            'series' => [[
                'name'       => 'RPM',
                'data'       => [$rpm],
                'dataLabels' => [
                    'format' => new \yii\web\JsExpression("'<div style=\"text-align:center\"><span style=\"font-size:25px;color:' + ((Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black') + '\">{y}</span><br/>' + '<span style=\"font-size:12px;color:silver\">request / min</span></div>'")
                ],
                'tooltip'    => [
                    'valueSuffix' => ' request/min',
                ]
            ]]
        ];
        $onlineOptions = \yii\helpers\ArrayHelper::merge($gaugeOptions, $onlineOptions);
        $rpmOptions = \yii\helpers\ArrayHelper::merge($gaugeOptions, $rpmOptions);


        $this->registerJs(<<<JS
    
    /**
    * 
    * @param val
    * @param separator
    * @param int lessOne кол-во символов после точки, если 0 то точка не выводится, добавляются задние нули работает пока только для 0..2
    * @returns {string}
    */
    function formatAsDecimal(val, separator = ',', lessOne = 0)
    {
        var pStr = '';
        var original = val;
        val = parseInt(val);
        if (val >= 1000) {
            var t = parseInt(val/1000);
            var ost = (val - t * 1000);
            var ostStr = ost;
            if  (ost == 0) {
                ostStr = '000';
            } else {
                if (ost < 10) {
                    ostStr = '00' + ost;
                } else if (ost < 100) {
                    ostStr = '0' + ost;
                }
            }
            pStr = t + separator + ostStr;
        } else {
            pStr = val;
        }
        var oStr = '';
        if (lessOne > 0) {
            oStr = '.';
            var d = original - parseInt(original);
            d = d * Math.pow(10, lessOne);
            d = parseInt(d);
            if (d == 0) {
                for (var i=0;i<lessOne;i++){
                    oStr = oStr + '0';
                }
            } else {
                if (lessOne == 1) {
                    oStr = d;
                } else {
                    if (d < 10) {
                        oStr = oStr + '0' + d;
                    } else {
                        oStr = oStr + d;
                    }
                }
            }
        }

        return pStr + oStr;
    }
    // Bring life to the dials
    setInterval(function () {
        $.ajax({
            url: '/admin-monitoring/ajax',
            dataType: 'json',
            success: function(ret) {
                // Speed
                var chart = $('#container-speed').highcharts(),
                    point,
                    newVal,
                    inc;
        
                if (chart) {
                    point = chart.series[0].points[0];
                    point.update(ret.online);
                }
        
                // RPM
                chart = $('#container-rpm').highcharts();
                if (chart) {
                    point = chart.series[0].points[0];
                    point.update(ret.rpm);
                }
            }
        })
    }, 5000);
JS
        );

        ?>

        <div class="col-lg-4">
            <?= HighCharts::widget([
                'options'      => [
                    'id'    => 'container-speed',
                    'style' => 'width: 300px; height: 200px; float: left',
                ],
                'chartOptions' => $onlineOptions,
                'modules'      => [
                    'highcharts-more',
                    'modules/solid-gauge',
                ],
            ]); ?>
        </div>
        <div class="col-lg-4">
            <?= HighCharts::widget([
                'options'      => [
                    'id'    => 'container-rpm',
                    'style' => 'width: 300px; height: 200px; float: left',
                ],
                'chartOptions' => $rpmOptions,
                'modules'      => [
                    'highcharts-more',
                    'modules/solid-gauge',
                ],
            ]); ?>
        </div>

        <?php
        $period = 24 * 2;
        $rows = \app\common\models\StatisticOnline::find()
            ->select([
                'time',
                'online',
            ])
            ->where(['between', 'time', time() - 60 * 60 * $period, time()])
            ->all()
        ;
        $rows2 = \app\common\models\StatisticOnline::find()
            ->select([
                'time',
                'request_per_second',
            ])
            ->where(['between', 'time', time() - 60 * 60 * $period, time()])
            ->all()
        ;

        $rowsJson = \yii\helpers\Json::encode($rows);
        $rows2Json = \yii\helpers\Json::encode($rows2);
        $this->registerJs(<<<JS
        Highcharts.setOptions({
            global: {
                timezoneOffset: -3 * 60
            }
        });
        var rows = {$rowsJson};
        var newRows = [];
        for(i = 0; i < rows.length; i++)
        {
            var item = rows[i];
            newRows.push({
                x: new Date(item.time * 1000),
                y: item.online
            });
        }
        var rows2 = {$rows2Json};
        var newRows2 = [];
        for(i = 0; i < rows2.length; i++)
        {
            var item2 = rows2[i];
            newRows2.push({
                x: new Date(item2.time * 1000),
                y: parseFloat(formatAsDecimal(item2.request_per_second, '', 1))
            });
        }
JS
        );
        ?>
        <?= HighCharts::widget([
            'chartOptions' => [
                'chart' => [
                    'zoomType' => 'x',
                    'type'     => 'spline',
                    'events' => [
                        'selection' => new \yii\web\JsExpression(<<<JS
function(event) {
    if (typeof(event.xAxis) == "undefined") {
        console.log(1);
    } else {
        // log the min and max of the primary, datetime x-axis
        console.log(
            Highcharts.dateFormat(
                '%Y-%m-%d %H:%M:%S',
                event.xAxis[0].min
            ),
            Highcharts.dateFormat(
                '%Y-%m-%d %H:%M:%S',
                event.xAxis[0].max
            )
        );
        // log the min and max of the y axis
        console.log(event.yAxis[0].min, event.yAxis[0].max);
    }
}
JS
                        )
                    ]
                ],
                'title' => [
                    'text' => 'График',
                ],
                'subtitle' => [
                    'text' => 'Выделите область для изменения масштаба',
                ],
                'xAxis' => [
                    'type' => 'datetime',
                ],
                'yAxis' => [
                    [
                        'title' => [
                            'text' => 'Количество',
                        ],
                    ]
                ],
                'legend' => [
                    'enabled' => true
                ],
                'tooltip' => [
                    'crosshairs' => true,
                    'shared' => true,
                ],
                'plotOptions' => [
                    'series' => [
                        'turboThreshold' => 0,
                    ],
                ],
                'series' => [
                    [
                        'type' => 'spline',
                        'name' => 'Online',
                        'data' => new \yii\web\JsExpression('newRows'),
                    ],
                    [
                        'type' => 'spline',
                        'name' => 'Запросов в мин',
                        'data' => new \yii\web\JsExpression('newRows2'),
                    ],
                ],
            ],
        ]);

        ?>

        <?php \yii\widgets\Pjax::begin() ?>
        <?php
        $sort = new \yii\data\Sort([
            'attributes' => [
                'time' => [
                    'default' => SORT_DESC,
                ],
                'online' => [
                    'default' => SORT_DESC,
                ],
                'request_per_second' => [
                    'default' => SORT_DESC,
                ],
            ],
            'defaultOrder' => [
                'time' => SORT_DESC,
            ],
        ]);
        ?>
        <?= \yii\grid\GridView::widget([
            'dataProvider' => new \yii\data\ActiveDataProvider([
                'query' => \app\common\models\StatisticOnline::find(),
                'sort'  => $sort,
            ]),
            'tableOptions' => [
                'class' => 'table table-striped table-hover'
            ],
            'columns' => [
                'time:datetime:Время',
                [
                    'attribute'      => 'online',
                    'format'         => ['decimal', 0],
                    'contentOptions' => [
                        'style' => 'text-align: right;'
                    ],
                ],
                [
                    'label'          => 'Rpm',
                    'attribute'      => 'request_per_second',
                    'format'         => ['decimal', 1],
                    'contentOptions' => [
                        'style' => 'text-align: right;'
                    ],
                ],
            ],
        ]) ?>
        <?php \yii\widgets\Pjax::end() ?>

    </div>

</div>