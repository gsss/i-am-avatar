<?php

/**
 * @var $this \yii\web\View
 */

use yii\widgets\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;
use app\common\assets\HighCharts\HighChartsAsset;
use app\common\models\StatisticRoute;


$this->title = 'Время исполнения страниц на Личном Кабинете';

$rows = StatisticRoute::find()
    ->select([
        'controller_id as controller',
        'action_id     as action',
        'count(time)   as counter',
        'min(timer)    as min',
        'avg(timer)    as avg',
        'max(timer)    as max',
    ])
    ->andWhere(['between', 'time', time() - 60 * 60, time()])
    ->groupBy(['controller_id', 'action_id'])
    ->orderBy(['avg' => SORT_DESC])
    ->limit(5)
    ->asArray()
    ->all()
;
$categories = [];
$min = [];
$avg = [];
$max = [];
$counter = [];
foreach ($rows as $row) {
    $min[] = (int)($row['min'] * 1000);
    $avg[] = (int)($row['avg'] * 1000);
    $max[] = (int)($row['max'] * 1000);
    $categories[] = $row['controller'] . '/' . $row['action'];
}

$timeOptions = [
    'chart'       => [
        'type' => 'bar',
    ],
    'colors'      => [
        '#00cc00',
        '#cccc00',
        '#cc0000',
        '#0000cc',
    ],
    'title'       => [
        'text' => 'Самые тяжелые запросы'
    ],
    'xAxis'       => [
        'categories' => $categories,
        'title'      => [
            'text' => null,
        ],
    ],
    'yAxis'       => [
        'min'    => 0,
        'title'  => [
            'text'  => 'Время выполнения, ms',
            'align' => 'high',
        ],
        'labels' => [
            'overflow' => 'justify'
        ],
    ],
    'tooltip'     => [
        'valueSuffix' => ' ms'
    ],
    'plotOptions' => [
        'bar' => [
            'dataLabels' => [
                'enabled' => true
            ],
        ],
    ],
    'legend'      => [
        'layout'          => 'vertical',
        'align'           => 'right',
        'verticalAlign'   => 'vertical',
        'x'               => -40,
        'y'               => 80,
        'floating'        => true,
        'borderWidth'     => 1,
        'backgroundColor' => new \yii\web\JsExpression("((Highcharts.theme && Highcharts.theme.legendBackgroundColor) || '#FFFFFF')"),
        'shadow'          => true,
    ],
    'credits'     => [
        'enabled' => true,
    ],
    'series'      => [
        [
            'name' => 'мин',
            'data' => $min,
        ],
        [
            'name' => 'среднее',
            'data' => $avg,
        ],
        [
            'name' => 'макс',
            'data' => $max,
        ],
    ],
];


$rows = StatisticRoute::find()
    ->select([
        'controller_id as controller',
        'action_id     as action',
        'count(time)   as counter',
    ])
    ->andWhere(['between', 'time', time() - 60 * 60, time()])
    ->groupBy(['controller_id', 'action_id'])
    ->orderBy(['counter' => SORT_DESC])
    ->limit(15)
    ->asArray()
    ->all()
;
$categories = [];
$counter = [];
foreach ($rows as $row) {
    $counter[] = (int)$row['counter'];
    $categories[] = $row['controller'] . '/' . $row['action'];
}

$counterOptions = [
    'chart'       => [
        'type' => 'bar',
    ],
    'colors'      => [
        '#cccccc',
    ],
    'title'       => [
        'text' => 'Самые популярные запросы'
    ],
    'xAxis'       => [
        'categories' => $categories,
        'title'      => [
            'text' => null,
        ],
    ],
    'yAxis'       => [
        'min'    => 0,
        'title'  => [
            'text'  => 'Кол-во, раз',
            'align' => 'high',
        ],
        'labels' => [
            'overflow' => 'justify'
        ],
    ],
    'tooltip'     => [
        'valueSuffix' => ' вызова'
    ],
    'plotOptions' => [
        'bar' => [
            'dataLabels' => [
                'enabled' => true
            ],
        ],
    ],
    'legend'      => [
        'layout'          => 'vertical',
        'align'           => 'right',
        'verticalAlign'   => 'vertical',
        'x'               => -40,
        'y'               => 80,
        'floating'        => true,
        'borderWidth'     => 1,
        'backgroundColor' => new \yii\web\JsExpression("((Highcharts.theme && Highcharts.theme.legendBackgroundColor) || '#FFFFFF')"),
        'shadow'          => true,
    ],
    'credits'     => [
        'enabled' => true,
    ],
    'series'      => [
        [
            'name' => 'Вызовов',
            'data' => $counter,
        ],
    ],
];

?>
<style>

    a.asc:after {
        content: ' ↓';
        display: inline;
    }

    a.desc:after {
        content: ' ↑';
        display: inline;
    }

</style>

<div class="container">

<h1 class="page-header">Время исполнения страниц на Личном Кабинете</h1>

    <?= \cs\Widget\HighCharts\HighCharts::widget([
        'options' => [
            'style' => 'min-width: 310px; max-width: 800px; height: 400px; margin: 0 auto',
        ],
        'chartOptions' => $timeOptions,
    ]) ?>
    <?= \cs\Widget\HighCharts\HighCharts::widget([
        'options' => [
            'style' => 'min-width: 310px; max-width: 800px; height: 400px; margin: 0 auto',
        ],
        'chartOptions' => $counterOptions,
    ]) ?>

    <p>Статистика за последний час</p>

    <?php \yii\widgets\Pjax::begin() ?>
        <?php
        $sort = new \yii\data\Sort([
            'attributes'   => [
                'controller',
                'action',
                'counter' => [
                    'default' => SORT_DESC,
                ],
                'min' => [
                    'default' => SORT_DESC,
                ],
                'avg' => [
                    'default' => SORT_DESC,
                ],
                'max' => [
                    'default' => SORT_DESC,
                ],
            ],
            'defaultOrder' => [
                'avg' => SORT_DESC,
            ]
        ]);
        ?>
    <?php
    $model = new \app\models\Form\StatisticRouteForm();
    $provider = $model->search($sort, Yii::$app->request->get());
    ?>
    <?= \yii\grid\GridView::widget([
        'dataProvider' => $provider,
        'filterModel' => $model,
        'columns' => [
            [
                'attribute' => 'controller',
                'filterOptions' => [
                    'style' => 'vertical-align: top;',
                ],
            ],
            [
                'attribute' => 'action',
                'filterOptions' => [
                    'style' => 'vertical-align: top;',
                ],
            ],
            [
                'attribute'      => 'counter',
                'format'         => ['decimal', 0],
                'contentOptions' => [
                    'style' => 'text-align: right;'
                ],
                'headerOptions'  => [
                    'style' => 'text-align: right;width:150px;'
                ],
                'filterOptions' => [
                    'style' => 'vertical-align: top;',
                ],
            ],
            [
                'attribute'      => 'min',
                'format'         => ['decimal', 0],
                'contentOptions' => [
                    'style' => 'text-align: right;'
                ],
                'headerOptions'  => [
                    'style' => 'text-align: right;width:150px;'
                ],
                'filterOptions' => [
                    'style' => 'vertical-align: top;',
                ],
            ],
            [
                'attribute'      => 'avg',
                'format'         => ['decimal', 0],
                'contentOptions' => [
                    'style' => 'text-align: right;'
                ],
                'headerOptions'  => [
                    'style' => 'text-align: right;width:150px;'
                ],
                'filterOptions' => [
                    'style' => 'vertical-align: top;',
                ],
            ],
            [
                'attribute'      => 'max',
                'format'         => ['decimal', 0],
                'contentOptions' => [
                    'style' => 'text-align: right;'
                ],
                'headerOptions'  => [
                    'style' => 'text-align: right;width:150px;'
                ],
                'filterOptions' => [
                    'style' => 'vertical-align: top;',
                ],
            ],
        ],
    ]) ?>
    <?php \yii\widgets\Pjax::end() ?>

</div>