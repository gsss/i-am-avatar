<?php

/**
 * @var $this \yii\web\View
 */

use yii\widgets\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;
use common\models\blacklist\LoginHistory;
use common\assets\HighCharts\HighChartsAsset;
use common\widgets\highCharts\HighCharts;
$this->title = 'Мониторинг кластера базы данных';

?>
<div class="container">

<h1 class="page-header">Мониторинг кластера базы данных</h1>

<?php
$rows = \app\common\models\StatisticDb::find()
    ->select([
        '*',
    ])
    ->where(['between', 'time', time() - 60 * 60 * 24 * 7, time()])
    ->all()
;
$columns = [
    ['attribute' => 'main_master_select', 'name' => 'master_select', 'data' => [], 'color' => '#ff0000', 'lineWidth' => 1],
    ['attribute' => 'main_master_insert', 'name' => 'master_insert', 'data' => [], 'color' => '#cc0000', 'lineWidth' => 1],
    ['attribute' => 'main_master_update', 'name' => 'master_update', 'data' => [], 'color' => '#990000', 'lineWidth' => 1],
    ['attribute' => 'main_master_delete', 'name' => 'master_delete', 'data' => [], 'color' => '#660000', 'lineWidth' => 1],
    ['attribute' => 'main_slave_select', 'name' => 'slave_select', 'data' => [], 'color' => '#0000ff', 'lineWidth' => 1],
    ['attribute' => 'statistic_select', 'name' => '', 'data' => [], 'color' => '#00ff00', 'lineWidth' => 1],
    ['attribute' => 'statistic_insert', 'name' => '', 'data' => [], 'color' => '#00cc00', 'lineWidth' => 1],
    ['attribute' => 'statistic_update', 'name' => '', 'data' => [], 'color' => '#009900', 'lineWidth' => 1],
    ['attribute' => 'statistic_delete', 'name' => '', 'data' => [], 'color' => '#006600', 'lineWidth' => 1],
];
$rowsJson = \yii\helpers\Json::encode($rows);
$columnsJson = \yii\helpers\Json::encode($columns);
$this->registerJs(<<<JS
        Highcharts.setOptions({
            global: {
                timezoneOffset: -3 * 60
            }
        });
   
        var rows = {$rowsJson};
        var lines = {$columnsJson};
        for(i = 0; i < rows.length; i++)
        {
            var item = rows[i];
            var now = new Date(item.time * 1000);
            lines[0].data.push({
                x: now,
                y: item.main_master_select
            });
            lines[1].data.push({
                x: now,
                y: item.main_master_insert
            });
            lines[2].data.push({
                x: now,
                y: item.main_master_update
            });
            lines[3].data.push({
                x: now,
                y: item.main_master_delete
            });
            lines[4].data.push({
                x: now,
                y: item.main_slave_select
            });
            lines[5].data.push({
                x: now,
                y: item.statistic_select
            });
            lines[6].data.push({
                x: now,
                y: item.statistic_insert
            });
            lines[7].data.push({
                x: now,
                y: item.statistic_update
            });
            lines[8].data.push({
                x: now,
                y: item.statistic_delete
            });
        }
JS
);
$series = [];
$c = 0;
foreach ($columns as $column) {
    $new = [
        'type' => 'spline',
        'name' => (\yii\helpers\ArrayHelper::getValue($column, 'name', '') == '') ? $column['attribute'] : $column['name'],
        'data' => new \yii\web\JsExpression("lines[{$c}].data"),
    ];
    if (isset($column['color'])) {
        $new['color'] = $column['color'];
    }
    if (isset($column['color'])) {
        $new['lineWidth'] = $column['lineWidth'];
    }
    $series[] = $new;
    $c++;
}

?>
<p>Указывает кол-во запросов к Базе Данных в мин</p>
<?= \cs\Widget\HighCharts\HighCharts::widget([
    'chartOptions' => [
        'chart' => [
            'zoomType' => 'x',
            'type'     => 'spline',
        ],
        'plotOptions' => [
            'series' => [
                'turboThreshold' => 0,
            ],
        ],
        'title' => [
            'text' => 'График',
        ],
        'subtitle' => [
            'text' => 'Выделите область для изменения масштаба',
        ],
        'xAxis' => [
            'type' => 'datetime',
        ],
        'yAxis' => [
            [
                'title' => [
                    'text' => 'Количество',
                ],
            ]
        ],
        'legend' => [
            'enabled' => true
        ],
        'tooltip' => [
            'crosshairs' => true,
            'shared' => true,
        ],
        'series' => $series,
    ],
]);

?>
</div>
