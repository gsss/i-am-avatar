<?php

/**
 * @var $this \yii\web\View
 */

use yii\widgets\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;
use app\common\assets\HighCharts\HighChartsAsset;
use app\common\models\StatisticRoute;


$this->title = 'Ошибки';


?>
<style>

    a.asc:after {
        content: ' ↓';
        display: inline;
    }

    a.desc:after {
        content: ' ↑';
        display: inline;
    }

</style>

<div class="container">

    <?php \yii\widgets\Pjax::begin() ?>
    <h1 class="page-header"><?= $this->title ?></h1>
    <?php
    $sort = new \yii\data\Sort([
        'attributes'   => [
            'controller',
            'action',
            'counter' => [
                'default' => SORT_DESC,
            ],
            'min'     => [
                'default' => SORT_DESC,
            ],
            'avg'     => [
                'default' => SORT_DESC,
            ],
            'max'     => [
                'default' => SORT_DESC,
            ],
        ],
        'defaultOrder' => [
            'avg' => SORT_DESC,
        ]
    ]);
    ?>
    <?php
    ?>
    <style>
        .code {
            font-family: "Courier New", Courier, monospace;
        }
    </style>
    <?= \yii\grid\GridView::widget([
        'dataProvider' => new \yii\data\ActiveDataProvider([
            'query' => \app\models\Log::find()
                ->where(['level' => \yii\log\Logger::LEVEL_ERROR])
                ->select([
                    'count(id) as count',
                    'category',
                    'max(log_time) as max_log_time',
                    'prefix',
                ])
                ->groupBy(['category'])
                ->orderBy([
                    'max(log_time)' => SORT_DESC,
                    'count'         => SORT_DESC,
                ])
                ->asArray()
        ]),
        'columns'      => [
            'count:text:Кол-во',
            [
                'header'         => 'Категория',
                'attribute'      => 'category',
                'contentOptions' => [
                    'class' => 'code',
                ],
                'content'        => function ($item) {
                    $v = \yii\helpers\ArrayHelper::getValue($item, 'category', '');
                    if ($v == '') return '';

                    return Html::a($v, ['admin-monitoring/error', 'category' => $v]);
                }
            ],
            'prefix:text:Префикс',
            [
                'header'  => 'Последний раз',
                'content' => function ($item) {
                    $v = (int)\yii\helpers\ArrayHelper::getValue($item, 'max_log_time', 0);
                    if ($v == 0) return '';

                    return Html::tag('abbr', \cs\services\DatePeriod::back($v, ['isShort' => true]), ['class' => 'gsssTooltip', 'title' => Yii::$app->formatter->asDatetime($v)]);
                }
            ],
        ]
        ,
    ]) ?>
    <?php \yii\widgets\Pjax::end() ?>
</div>