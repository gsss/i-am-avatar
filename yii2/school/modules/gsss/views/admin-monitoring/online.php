<?php

/**
 * @var $this \yii\web\View
 */

use yii\widgets\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;
use app\common\assets\HighCharts\HighChartsAsset;
use app\common\models\StatisticRoute;

$t = microtime(true);

$this->title = 'Клиенты онлайн';

$userList = Yii::$app->onlineManager->get();

$requests = \app\models\ServerRequest::find()
    ->where(['>', 'log_time', time() - 60*15])
    ->orderBy(['log_time' => SORT_DESC])
    ->all();

/**
 * @param \app\models\ServerRequest[] $requests
 * @return  \app\models\ServerRequest
 */
function find($requests, $type, $data)
{
    for ($i = 0; $i < count($requests); $i++) {
        /** @var \app\models\ServerRequest $request */
        $request = $requests[$i];
        if ($request->mode == \app\models\ServerRequest::MODE_GET) {
            if ($type == 'ip') {
                if ($request->ip == $data) {
                    return $request;
                }
            } else {
                if ($request->user_id == $data) {
                    return $request;
                }
            }
        }
    }

    return null;
}


/**
 * @var array
 * [
 * 'type' => 'id'/'ip'
 * 'data' => '41'
 * 'lastAction' => 1539602681
 * 'secondsFromLastAction' => 0
 * 'lastRequest' => app\models\ServerRequest#1
 * ]
 */
$rows = [];
$c = 0;
for ($i = 0;$i < count($userList);$i++) {
    $item['type'] = $userList[$i][0];
    $item['data'] = $userList[$i][1];
    $item['lastAction'] = $userList[$i][2];
    $item['secondsFromLastAction'] = time() - $userList[$i][2];
    $item['lastRequest'] = find($requests, $userList[$i][0], $userList[$i][1]);
    if (!is_null($item['lastRequest'])) {
        $item['isBot'] = isBot($item['lastRequest']);
        if ($item['isBot']) $c++;
    }
    $rows[] = $item;
}
\yii\helpers\ArrayHelper::multisort($rows, 'secondsFromLastAction');


/**
 * @param \app\models\ServerRequest $request
 * @return bool
 */
function isBot($request) {
    $botArray = [
        'YandexBot',
        'bingbot',
        'SemrushBot',
        'Pinterestbot',
        'Googlebot',
        'DotBot',
        'AhrefsBot',
        'Twitterbot',
        'seekport.com',
    ];
    $data = unserialize($request->server_data);
    if (isset ($data['HTTP_USER_AGENT'])) {
        $agent = $data['HTTP_USER_AGENT'];
        foreach ($botArray as $botName) {
            if (strpos($agent, $botName)) {
                return true;
            }
        }
    }
    
    return false;
}

?>
<style>

    a.asc:after {
        content: ' ↓';
        display: inline;
    }

    a.desc:after {
        content: ' ↑';
        display: inline;
    }

</style>

<div class="container">
    <div class="col-lg-12">
        <h1 class="page-header"><?= $this->title ?></h1>
        <p>Bots: <?= Yii::$app->formatter->asDecimal(($c/count($rows))*100, 2) ?>%</p>

        <?= \yii\grid\GridView::widget([
            'dataProvider' => new \yii\data\ArrayDataProvider([
                'allModels' => $rows,
                'pagination' => [
                    'pageSize' => 100,
                ],
            ]),
            'rowOptions' => function($i) {
                if (isset($i['isBot'])) {
                    if ($i['isBot']) return [
                        'class' => 'danger',
                    ];
                }
                return [];
            },
            'tableOptions' => [
                'class' => 'table table-striped table-hover'
            ],
            'columns' => [
                [
                    'class' => 'yii\grid\SerialColumn',
                ],
                [
                    'header'  => 'RequestID',
                    'content' => function ($i) {
                        if (!is_null($i['lastRequest'])) {
                            /** @var app\models\ServerRequest $request */
                            $request = $i['lastRequest'];

                            return $request->id;
                        }
                        return ''   ;
                    },
                ],
                [
                    'header'         => 'Последнее действие, сек',
                    'attribute'      => 'secondsFromLastAction',
                    'format'         => ['time', 'php:i:s'],
                    'contentOptions' => [
                        'style' => 'text-align: right;',
                    ],
                    'headerOptions' => [
                        'style' => 'text-align: right;',
                    ],
                ],
                [
                    'header'    => 'Пользователь',
                    'attribute' => 'data',
                    'content' => function($i) {
                        if ($i['type'] == 'id') {
                            $user = \common\models\User::findOne($i['data']);
                            return Html::a(
                                Html::img($user->getAvatar(), ['width' => 50, 'class' => 'img-circle', 'title' => $i['data'], 'data-toggle' => 'tooltip']),
                                '/user/' . $i['data'],
                                ['target' => '_blank']
                            );
                        }
                        if ($i['type'] == 'ip') {
                            $arr = explode('.', $i['data']);
                            $rows = [];
                            foreach ($arr as $i1) {
                                if ($i1 < 10) {
                                    $rows[] = '00' . $i1;
                                    continue;
                                }
                                if ($i1 < 100) {
                                    $rows[] = '0' . $i1;
                                    continue;
                                }
                                $rows[] = $i1;
                            }
                            return Html::tag('span', join('.', $rows), ['class' => 'label label-default', 'style' => 'font-family: Courier New;']);
                        }
                        return ''   ;
                    }
                ],
                [
                    'header'         => 'session_id',
                    'attribute'      => 'session_id',
                    'content' => function($i) {
                        if (!is_null($i['lastRequest'])) {
                            /** @var app\models\ServerRequest $request */
                            $request = $i['lastRequest'];

                            return Html::tag('span', $request->session_id, ['class' => 'label label-info', 'style' => 'font-family: Courier New;']);
                        }
                        return ''   ;
                    }
                ],
                [
                    'header'         => 'HTTP_HOST',
                    'content' => function($i) {
                        if (!is_null($i['lastRequest'])) {
                            /** @var app\models\ServerRequest $request */
                            $request = $i['lastRequest'];

                            if (isset($request['server_data'])) {
                                $server_data = unserialize($request['server_data']);
                                $arr = explode('.', strtolower($server_data['HTTP_HOST']));
                                $class = 'default';
                                if ($arr[count($arr)-1] == 'com') $class = 'success';

                                return Html::tag('span', join('.', $arr), ['class' => 'label label-'.$class]);
                            }
                        }

                        return ''   ;
                    }
                ],
                [
                    'header'         => 'URL',
                    'attribute'      => 'lastRequest',
                    'content' => function($i) {
                        if (!is_null($i['lastRequest'])) {
                            /** @var app\models\ServerRequest $request */
                            $request = $i['lastRequest'];

                            return Html::tag('span', Html::a($request->uri, 'http://www.galaxysss.com' . $request->uri, ['target' => '_blank']), ['class' => 'label label-info']);
                        }
                        return ''   ;
                    }
                ],
                [
                    'header'         => 'Страна',
                    'attribute'      => 'lastRequest',
                    'content' => function($i) {
                        if (!is_null($i['lastRequest'])) {
                            /** @var app\models\ServerRequest $request */
                            $request = $i['lastRequest'];
                            $data = unserialize($request->server_data);
                            if (isset ($data['HTTP_CF_IPCOUNTRY'])) {
                                return $data['HTTP_CF_IPCOUNTRY'];
                            } else {
                                return ''   ;
                            }
                        }
                        return ''   ;
                    }
                ],

            ],
        ]) ?>

        <p>t = <?= Yii::$app->formatter->asDecimal(microtime(true) - $t, 4)?></p>

    </div>
</div>