<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;
use app\models\UnionCategory;
use yii\helpers\Url;

/* @var $this yii\web\View */

$this->title = 'Картинки';
?>
<div class="container">
    <h1 class="page-header">
        <?= Html::encode($this->title) ?>
    </h1>


    <?php
$this->registerJs(<<<JS
    $('.buttonAccept').click(function(e) {
        e.stopPropagation();
        e.preventDefault();
        var button = $(this);
        ajaxJson({
            url: '/moderator/shop/imageList/' + button.data('id') + '/accept',
            success: function(ret) {
                button.parent().parent().remove();
                infoWindow('Успешно');
            }
        })
    });
    $('.buttonReject').click(function(e) {
        e.stopPropagation();
        e.preventDefault();
        var button = $(this);
        $('#messageModal').modal('show');
        $('#buttonSendMessageForm').click(function() {
            ajaxJson({
                url: '/moderator/shop/imageList/' + button.data('id') + '/reject',
                data: {
                    text: $('#messageModal textarea').val()
                },
                success: function(ret) {
                    button.parent().parent().remove();
                    $('#messageModal').modal('hide');
                    infoWindow('Успешно');
                }
            })
        });
    });
JS
);
    ?>
    <div class="modal fade" id="messageModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
         aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Сообщение</h4>
                </div>
                <div class="modal-body">
                    <textarea class="form-control" rows="10"></textarea>
                    <hr>
                    <button class="btn btn-primary" style="width:100%;" id="buttonSendMessageForm">Отправить</button>
                </div>
            </div>
        </div>
    </div>
    <?= \yii\grid\GridView::widget([
        'tableOptions' => [
            'class' => 'table table-striped table-hover',
        ],
        'rowOptions' => function($item){
            return [
                'role' => 'button',
                'data-id' => $item['id'],
                'class' => 'rowRow',
            ];
        },
        'dataProvider' => new \yii\data\ActiveDataProvider([
            'query'      => \app\models\Shop\ProductImage::query(['gs_unions_shop_product_images.moderation_status' => null])
                ->select([
                    'gs_unions_shop_product_images.*',
                    'gs_unions_shop_product.name as product_name',
                    'gs_unions_shop_product.image as product_image',
                    'gs_unions.name as union_name',
                ])
                ->innerJoin('gs_unions_shop_product', 'gs_unions_shop_product.id = gs_unions_shop_product_images.product_id')
                ->innerJoin('gs_unions', 'gs_unions.id = gs_unions_shop_product.union_id')
                ->orderBy(['gs_unions_shop_product.date_insert' => SORT_DESC])
            ,
            'pagination' => [
                'pageSize' => 50,
            ],
        ]),
        'columns' => [
            'id',
            [
                'header' => 'Картинка',
                'content' => function($item) {
                    $i = \yii\helpers\ArrayHelper::getValue($item, 'product_image', '');
                    if ($i == '') return '';
                    return Html::img($i, ['width' => 100]);
                }
            ],
            'product_name',
            [
                'header' => 'Картинка',
                'content' => function($item) {
                    $i = \yii\helpers\ArrayHelper::getValue($item, 'image', '');
                    if ($i == '') return '';
                    return Html::img($i, ['width' => 100]);
                }
            ],
            'union_name',
            [
                'header'  => 'Принять',
                'content' => function($item) {
                    return Html::button('Принять', ['class' => 'btn btn-success buttonAccept', 'data-id' => $item['id']]);
                }
            ],
            [
                'header'  => 'Отклонить',
                'content' => function($item) {
                    return Html::button('Отклонить',  ['class' => 'btn btn-danger buttonReject', 'data-id' => $item['id']]);
                }
            ],
        ]
    ]) ?>
</div>
