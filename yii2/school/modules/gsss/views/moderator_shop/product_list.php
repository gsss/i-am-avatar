<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;
use app\models\UnionCategory;
use yii\helpers\Url;

/* @var $this yii\web\View */

$this->title = 'Товары';
?>
<div class="container">
    <h1 class="page-header"><?= Html::encode($this->title) ?></h1>


    <?php
$this->registerJs(<<<JS
    $('.buttonAccept').click(function(e) {
        e.stopPropagation();
        e.preventDefault();
        var button = $(this);
        ajaxJson({
            url: '/moderator/shop/productList/' + button.data('id') + '/accept',
            success: function(ret) {
                button.parent().parent().remove();
                infoWindow('Успешно');
            }
        })
    });
    $('.buttonReject').click(function(e) {
        e.stopPropagation();
        e.preventDefault();
        var button = $(this);
        $('#messageModal').modal('show');
        $('#buttonSendMessageForm').click(function() {
            ajaxJson({
                url: '/moderator/shop/productList/' + button.data('id') + '/reject',
                data: {
                    text: $('#messageModal textarea').val()
                },
                success: function(ret) {
                    button.parent().parent().remove();
                    $('#messageModal').modal('hide');
                    infoWindow('Успешно');
                }
            })
        });
    });
    $('.rowRow').click(function() {
        window.location = '/moderator/shop/productList/' + $(this).data('id') + '/view';
    });
JS
);
    ?>
    <div class="modal fade" id="messageModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
         aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Сообщение</h4>
                </div>
                <div class="modal-body">
                    <textarea class="form-control" rows="10"></textarea>
                    <hr>
                    <button class="btn btn-primary" style="width:100%;" id="buttonSendMessageForm">Отправить</button>
                </div>
            </div>
        </div>
    </div>
    <?= \yii\grid\GridView::widget([
        'tableOptions' => [
            'class' => 'table table-striped table-hover',
        ],
        'rowOptions' => function($item){
            return [
                'role' => 'button',
                'data-id' => $item['id'],
                'class' => 'rowRow',
            ];
        },
        'dataProvider' => new \yii\data\ActiveDataProvider([
            'query'      => \app\models\Shop\Product::query(['gs_unions_shop_product.moderation_status' => \app\models\Shop\Product::MODERATION_STATUS_SEND])
                ->select([
                    'gs_unions_shop_product.*',
                    'gs_unions.name as union_name',
                ])
                ->innerJoin('gs_unions', 'gs_unions.id = gs_unions_shop_product.union_id')
                ->orderBy(['gs_unions_shop_product.date_insert' => SORT_DESC])
            ,
            'pagination' => [
                'pageSize' => 50,
            ],
        ]),
        'columns' => [
            'id',
            [
                'header' => 'Картинка',
                'content' => function($item) {
                    $i = \yii\helpers\ArrayHelper::getValue($item, 'image', '');
                    if ($i == '') return '';
                    return Html::img($i, ['width' => 100]);
                }
            ],
            'name',
            'union_name',
            'price:decimal:Цена',
            [
                'header'  => 'Принять',
                'content' => function($item) {
                    return Html::a('Принять', ['moderator_shop/accept', 'id' => $item['id']], ['class' => 'btn btn-success buttonAccept', 'data-id' => $item['id']]);
                }
            ],
            [
                'header'  => 'Отклонить',
                'content' => function($item) {
                    return Html::a('Отклонить', ['moderator_shop/reject', 'id' => $item['id']], ['class' => 'btn btn-danger buttonReject', 'data-id' => $item['id']]);
                }
            ],
        ]
    ]) ?>
</div>
