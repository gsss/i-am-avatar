<?php
use yii\helpers\Html;
use yii\bootstrap\ButtonDropdown;
use yii\helpers\Url;
use cs\services\Url as csUrl;
use cs\services\Str;
use yii\widgets\Breadcrumbs;

/** @var $this yii\web\View */
/** @var $product     \app\models\Shop\Product */

$this->title = $product->getField('name');
$union = $product->getUnion();

$this->registerJs(<<<JS
    $('.buttonAccept').click(function(e) {
        e.stopPropagation();
        e.preventDefault();
        var button = $(this);
        ajaxJson({
            url: '/moderator/shop/productList/' + button.data('id') + '/accept',
            success: function(ret) {
                button.parent().remove();
                infoWindow('Успешно');
            }
        })
    });
    $('.buttonReject').click(function(e) {
        e.stopPropagation();
        e.preventDefault();
        var button = $(this);
        $('#messageModal').modal('show');
        $('#buttonSendMessageForm').click(function() {
            ajaxJson({
                url: '/moderator/shop/productList/' + button.data('id') + '/reject',
                data: {
                    text: $('#messageModal textarea').val()
                },
                success: function(ret) {
                    button.parent().remove();
                    $('#messageModal').modal('hide');
                    infoWindow('Успешно');
                }
            })
        });
    });
JS
);
?>
<div class="container">
    <div class="col-lg-12">
        <h1 class="page-header"><?= \yii\helpers\Html::encode($this->title) ?></h1>
        <p>Объединение: <a href="<?= $union->getLink() ?>"><?= $union->getName() ?></a></p>
    </div>
    <div class="col-lg-4">
        <img class="img-thumbnail" src="<?= $product->getImage() ?>">
    </div>
    <div class="col-lg-8">

        <p><?= $product->getField('content') ?></p>
        <hr>
        <p>Цена: <?= Yii::$app->formatter->asDecimal($product->getField('price'), 0) ?> руб</p>
        <hr>
        <p>
            <button class="btn btn-success buttonAccept" data-id="<?= $product->getId() ?>">Одобрить</button>
            <button class="btn btn-danger buttonReject" data-id="<?= $product->getId() ?>">Отклонить</button>
        </p>





    </div>


</div>

<div class="modal fade" id="messageModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
     aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                        aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Сообщение</h4>
            </div>
            <div class="modal-body">
                <textarea class="form-control" rows="10"></textarea>
                <hr>
                <button class="btn btn-primary" style="width:100%;" id="buttonSendMessageForm">Отправить</button>
            </div>
        </div>
    </div>
</div>