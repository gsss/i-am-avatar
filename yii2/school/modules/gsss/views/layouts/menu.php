<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\modules\Shop\services\Basket;

/* @var $this \yii\web\View */
/* @var $content string */

\app\assets\App\Asset::register($this);
\app\assets\Maya\Asset::register($this);
\app\assets\LayoutMenu\Asset::register($this);
\app\assets\ModalBoxNew\Asset::register($this);

/** @var \app\assets\Maya\Asset $mayaAsset */
$mayaAsset = Yii::$app->assetManager->getBundle('app\assets\Maya\Asset');
$LayoutMenuAsset = Yii::$app->assetManager->getBundle('app\assets\LayoutMenu\Asset');
$LayoutMenuAssetPath = Yii::$app->assetManager->getBundle('app\assets\LayoutMenu\Asset')->baseUrl;
$this->registerJs('var pathMaya = \'' . $mayaAsset->baseUrl . '\';', \yii\web\View::POS_HEAD );
$this->registerJs('var pathLayoutMenu = \'' . $LayoutMenuAssetPath . '\';', \yii\web\View::POS_HEAD );
$this->registerJs("$('.gsssTooltip').tooltip();");
$this->registerMetaTag(['name' => 'fb:app_id', 'content' => '1004003993007363']);
$this->registerMetaTag(['name' => 'og:type', 'content' => 'website']);

?>
<?php $this->beginPage();
?>
<!DOCTYPE html>
<html
        xmlns="http://www.w3.org/1999/xhtml"
        xmlns:fb="http://ogp.me/ns/fb#"
        xmlns:og="http://ogp.me/ns#"
        lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>

    <title><?= $this->title ?><?php if (Yii::$app->requestedRoute != 'site/index') { ?> &middot; ♥ &middot; Галактический Союз Сил Света<?php } ?></title>
    <?php $this->head() ?>
    <link rel="shortcut icon" href="/images/ico.png">

</head>

<body>
<?php $this->beginBody() ?>

<!-- facebook plugin -->
<div id="fb-root"></div>
<script>(function(d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s); js.id = id;
        js.src = "//connect.facebook.net/ru_RU/sdk.js#xfbml=1&version=v2.5&appId=1132196000140882";
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));</script>
<!-- /facebook plugin -->

<!-- Fixed navbar -->
<nav class="navbar navbar-default navbar-fixed-top">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar"
                    aria-expanded="false" aria-controls="navbar">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a
                class="navbar-brand"
                href="/"
                style="padding: 5px 10px 5px 10px;"
                >
                <img
                    src="/images/ico40.png"
                    height="40"
                    width="40"
                    >
            </a>
        </div>
        <div id="navbar" class="collapse navbar-collapse">
            <ul class="nav navbar-nav">
                <?= $this->render('../blocks/topMenu') ?>
            </ul>
            <ul class="nav navbar-nav navbar-right">
                <?php
                $count = Basket::getCount();
                $this->registerJs('$("#basketCount").tooltip({placement:"left"})');
                ?>

                <?php if ($count > 0) { ?>
                    <li>
                        <a style="
                                padding-right: 0px;
                                padding-bottom: 0px;
                            " href="<?= Url::to(['shop/basket']) ?>">
                                <span id="basketCount" class="label label-success" title="Корзина">
                                    <?= $count ?>
                                </span>
                        </a>
                    </li>
                <?php } ?>
                <?php if (\Yii::$app->user->isGuest) { ?>
                    <li id="userBlockLi">
                        <!-- Split button -->
                        <div class="btn-group" style="margin-top: 9px; opacity: 0.5;" id="loginBarButton">
                            <button type="button" class="btn btn-default" id="modalLogin"><i class="glyphicon glyphicon-user" style="padding-right: 5px;"></i>Войти</button>
                            <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown"
                                    aria-expanded="false">
                                <span class="caret"></span>
                                <span class="sr-only">Toggle Dropdown</span>
                            </button>
                            <ul class="dropdown-menu" role="menu">
                                <li><a href="<?= Url::to(['auth/password_recover']) ?>">Напомнить пароль</a></li>
                                <li><a href="<?= Url::to(['auth/registration']) ?>">Регистрация</a></li>
                            </ul>
                        </div>
                    </li>
                <?php } else { ?>
                    <?= $this->render('user-menu') ?>
                <?php } ?>


                <?php
                Yii::$app->session->open();
                $maya = Yii::$app->cache->get(Yii::$app->session->getId() . '/maya');
                if ($maya) {
                    $this->registerJs("LayoutMenu.init({$maya['maya']});");
                } else {
                    $this->registerJs("LayoutMenu.init();");
                }

                if (Yii::$app->deviceDetect->isMobile()) {
                    $link = Url::to(['calendar/index']);
                    $options = [];
                } else {
                    $link = 'javascript:void(0);';
                    $options = [
                        'title' => 'Сегодня',
                        'id'    => 'linkCalendar',
                        'data'  => [
                            'toggle'    => 'popover',
                            'template'  => '<div class="popover" role="tooltip" style="width: 500px;"><div class="arrow"></div><h3 class="popover-title"></h3><div class="popover-content" style="padding-bottom:30px;"></div></div>',
                            'html'      => true,
                            'placement' => 'bottom',
                        ]
                    ];
                }
                ?>
                <li><?= Html::a(Html::tag('span', ($maya !== false) ?  $maya['date'] : '# ### #### г.', [
                            'style' => Html::cssStyleFromArray(['padding-right' => '10px']),
                            'id'    => 'dateThis',
                        ]) . Html::img( $mayaAsset->getStampSrc(($maya !== false) ?  $maya['stamp'] : \cs\models\Calendar\Maya::calc()['stamp']), [
                            'height' => 20,
                            'width'  => 20,
                            'id'     => 'calendarMayaStamp',
                        ]), $link, $options); ?></li>
            </ul>
        </div>

    </div>
</nav>
<div class="hide" id="calendarMayaDescription">
   <noindex>
        <h4>Красный Дракон</h4>

        <p>Откройтесь энергиям Рождения и Упования - Высшей Веры во всемогущесто бытия и стремитись найти им выражение в
            своей жизни. Фокусируйтесь на самоподдержании и принятии необходимой энергии от Вселенной, и тогда ваши
            глубинные потребности начнут восполняться самой жизнью. Позвольте энергии рождения инициировать и проявлять все
            ваши начинания!</p>

        <p>Ближайший <abbr title="День имеет прямую связь с духом и космосом">портал галактической активации</abbr>
            открывается <span class="days">через <kbd>7</kbd> <span class="days2">дней</span></span></p>
        <a class="btn btn-primary" href="<?= Url::to(['calendar/index'])?>">Подробнее</a>
        <a class="btn btn-primary" href="<?= Url::to(['page/time_arii'])?>">Арийское время</a>
   </noindex>
</div>



<?= $content ?>


<?php if (\Yii::$app->user->isGuest) : ?>
    <?= $this->render('_modalLogin') ?>
<?php endif; ?>


<?= $this->render('_footer', ['LayoutMenuAssetPath' => $LayoutMenuAssetPath]) ?>


<div id="infoModal" class="zoom-anim-dialog mfp-hide mfp-dialog">
    <h1>Dialog example</h1>
    <p>This is dummy copy. It is not meant to be read. It has been placed here solely to demonstrate the look and feel of finished, typeset text. Only for show. He who searches for meaning here will be sorely disappointed.</p>
</div>

<?= $this->render('_counters'); ?>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
