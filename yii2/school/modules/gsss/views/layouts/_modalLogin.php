<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\App\Asset;



?>
<div class="modal fade" id="loginModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
     aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                        aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Вход в измерение личного счастья</h4>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <div class="input-group input-group-lg">
                        <span class="input-group-addon" id="basic-addon1">
                            <i class="fa fa-envelope fa-fw"></i>
                        </span>
                        <input type="text" class="form-control" placeholder="Почта"
                               aria-describedby="basic-addon1" id="field-email">
                    </div>
                </div>
                <div class="form-group">
                    <div class="input-group input-group-lg">
                        <span class="input-group-addon" id="basic-addon1">
                            <i class="fa fa-key fa-fw" aria-hidden="true"></i>
                        </span>
                        <input type="password" class="form-control" placeholder="Пароль" id="field-password">
                            <span class="input-group-btn">
                                <a class="btn btn-default gsssTooltip" href="/password/recover" title="Забыли пароль? Нажмите чтобы воссановить">
                                    <i class="glyphicon glyphicon-question-sign"></i>
                                </a>
                            </span>
                    </div>
                    <div class="hide" id="loginFormLoading">
                        <img style="padding-left: 10px;padding-right: 10px;"
                             src="<?= \Yii::$app->assetManager->getBundle(Asset::className())->baseUrl ?>/images/ajax-loader.gif"
                             id="">
                    </div>
                    <p class="text-danger" style="margin-top: 10px;display: none;" id="loginFormError">Здесь выводятся ошибки</p>
                </div>
                <div class="form-group">
                    <?php
                    \cs\assets\CheckBox\Asset::register($this);
                    ?>
                    <div class="row">
                        <div class="col-lg-6">
                            <input type="checkbox" data-toggle="toggle" data-on="Да" data-off="Нет" id="loginFormIsStay" checked="checked">
                            Запомнить Вас?
                        </div>
                        <div class="col-lg-6">
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <button class="btn btn-success btn-lg" id="buttonLogin" style="width: 100%;">
                        Войти
                    </button>
                </div>
            </div>
            <div class="modal-footer">
                <a href="<?= Url::to(['auth/registration']) ?>" class="btn btn-default" style="width: 100%;">Регистрация</a>
                <div style="margin: 20px 0px 0px 0px;">
                    <?= \yii\authclient\widgets\AuthChoice::widget([
                        'baseAuthUrl' => ['auth/auth']
                    ]); ?>
                </div>
            </div>
        </div>
    </div>
</div>