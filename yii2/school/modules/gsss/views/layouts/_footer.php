<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 25.09.2018
 * Time: 13:24
 */

/** @var $LayoutMenuAssetPath */
use yii\helpers\Url;

?>

<footer class="footer" id="layoutMenuFooter">
    <div class="container">
        <div class="row" style="margin-top: 20px;">
            <div class="col-lg-4">
                <p>&copy; <a href="/category/money/article/2016/12/10/otvety_na_voprosy_po_sozdaniyu" title="Децентрализованная Автономная Организация" data-toggle="tooltip">DAO</a> Галактический Союз Сил Света, <a href="https://www.galaxysss.com/news/2018/09/19/23_sentyabrya_2018g__novoletie">7528</a> г.</p>

                <?php if (YII_ENV == 'prod') {?>
                    <center>
                        <script type="text/javascript" src="//ra.revolvermaps.com/0/0/6.js?i=0rgt0rizmb5&amp;m=0&amp;c=ff0000&amp;cr1=ffffff&amp;f=tahoma&amp;l=1&amp;bv=30&amp;lx=380&amp;ly=340" async="async"></script>
                    </center>
                <?php }?>
                <p class="text-center"><img src="<?= $LayoutMenuAssetPath ?>/images/merkaba-2nd.gif"></p>
            </div>
            <div class="col-lg-4" style="margin-bottom: 30px;">
                <a href="/newEarth/order">
                    <img src="/images/layout/menu/ramha.jpg" width="100%" class="thumbnail" style="border-radius: 20px;">
                </a>

                <p class="text-center">
                    <a href="/newEarth/game">
                        <img
                            src="/images/layout/menu/bitcoin2.jpg"
                            width="100"
                            data-toggle="tooltip"
                            title="Многомерная Социальная Игра Править Землей и Славить Богов"
                        />
                    </a>
                </p>
                <center>
                    <a href="<?= Url::to(['site/conditions']) ?>" class="text-center">
                        <img src="/images/layout/menu/holo.png" width="200" style="
    margin-top: 62px;
"><br>Условия наблюдения и пользования<br><abbr title="Инструмент Вознесения - этот сайт." class="gsssTooltip">Инструментом Вознесения</abbr>
                    </a>
                </center>

            </div>
            <div class="col-lg-4">
                <div class="fb-page" data-href="https://www.facebook.com/gsss.merkaba" data-width="360" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true" data-show-posts="false"><div class="fb-xfbml-parse-ignore"><blockquote cite="https://www.facebook.com/gsss.merkaba"><a href="https://www.facebook.com/gsss.merkaba">Галактический союз сил света</a></blockquote></div></div>

                <div style="margin-top: 30px;">
                    <hr>
                    <p><a href="<?= Url::to(['site/ministration']) ?>">Сотрудничество и служение</a></p>
                    <p><a href="http://yasobe.ru/na/galaxysss" target="_blank">Поддержать проект</a></p>
                    <p><a href="/help">Помощь</a></p>
                    <p><a href="/video">Видео библиотека</a></p>
                </div>

                <?php
                $isShowForm = false;
                if (Yii::$app->user->isGuest) {
                    if (!isset(Yii::$app->request->cookies['subscribeIsStarted'])) {
                        $isShowForm = true;
                    }
                } else if (Yii::$app->user->identity->getEmail() == '') {
                    $isShowForm = true;
                }
                if (\yii\helpers\ArrayHelper::getValue(Yii::$app->params, 'isTransfere', false) == true) {
                    $isShowForm = false;
                }
                ?>
                <?php if ($isShowForm) { ?>
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h3 class="panel-title">Подписаться на рассылку</h3>
                        </div>
                        <div class="panel-body">
                            <form id="formSubscribe">
                                <?php if (Yii::$app->user->isGuest) {?>
                                    <div class="form-group">
                                        <input type="text" class="form-control" id="formSubscribeName" placeholder="Имя">
                                        <p class="help-block help-block-error hide">Это поле должно быть заполнено обязательно</p>
                                    </div>
                                <?php }?>
                                <div class="form-group">
                                    <input type="email" class="form-control" id="formSubscribeEmail" placeholder="Email">
                                    <p class="help-block help-block-error hide">Это поле должно быть заполнено обязательно</p>
                                </div>
                                <button type="button" class="btn btn-default" style="width: 100%;" id="formSubscribeSubmit">Подписаться</button>
                            </form>
                        </div>
                    </div>
                <?php } ?>
            </div>
        </div>
    </div>
</footer>

