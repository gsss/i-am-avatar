<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\modules\Shop\services\Basket;

/* @var $this \yii\web\View */
/* @var $content string */

?>
<?php $this->beginPage();
?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name='yandex-verification' content='48c479de78d958cc' />
    <?= Html::csrfMetaTags() ?>
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->

    <title><?= $this->title ?><?php if (Url::current() != '/') { ?> &middot; ♥ &middot; Галактический Союз Сил Света<?php } ?></title>
    <?php $this->head() ?>
    <link rel="shortcut icon" href="/images/ico.png">
</head>

<body>
<?php $this->beginBody() ?>

<?= $content ?>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
