<?php
/**
 * Created by PhpStorm.
 * User: Бог-Творец
 * Date: 18.05.2016
 * Time: 7:45
 *
 */

use yii\helpers\Url;
use yii\helpers\Html;

/** @var $this \yii\web\View */
?>
<li class="dropdown" id="userBlockLi">
    <a
            href="#"
            class="dropdown-toggle"
            data-toggle="dropdown"
            aria-expanded="false"
            role="button"
            style="padding: 5px 10px 5px 10px;"
    >
        <?= Html::img(Yii::$app->user->identity->getAvatar(), [
            'height' => '40px',
            'class'  => 'img-circle'
        ]) ?>
        <span class="caret"></span>
    </a>
    <ul class="dropdown-menu" role="menu">
        <li><a href="<?= Url::to(['cabinet/objects']) ?>">Мои объединения</a></li>
        <li><a href="<?= Url::to(['cabinet/poseleniya']) ?>">Мои поселения</a></li>
        <li><a href="<?= Url::to(['cabinet_shop_client/orders']) ?>">Мои заказы</a></li>
        <li><a href="<?= Url::to(['cabinet_channeling/index']) ?>">Мои послания</a></li>
        <li><a href="/cabinet-events/index">Мои события</a></li>

        <?php if (Yii::$app->user->identity->isAdmin()) { ?>
            <li class="divider"></li>

            <li role="presentation" class="dropdown-header">Раздел админа</li>
            <li><a href="<?= Url::to(['admin-monitoring/index']) ?>">Все</a></li>
        <?php } ?>
        <?php if (Yii::$app->user->can('moderator2')) { ?>
            <li class="divider"></li>

            <li role="presentation" class="dropdown-header">Раздел модератора</li>
            <li><a href="<?= Url::to(['moderator/index']) ?>">Модерация</a></li>
        <?php } ?>
        <?php if (Yii::$app->user->can('agency-vozdayanie2')) { ?>
            <li class="divider"></li>

            <li role="presentation" class="dropdown-header">Агентство Воздаяния Творца</li>
            <li><a href="<?= Url::to(['cabinet-penalty/index']) ?>">Штрафы</a></li>
        <?php } ?>
        <?php if (Yii::$app->user->can('finance-manager2')) { ?>
            <li class="divider"></li>

            <li role="presentation" class="dropdown-header">Инвестиционная компания</li>
            <li><a href="<?= Url::to(['cabinet-finance/out']) ?>">Вывод средств</a></li>
        <?php } ?>
        <?php if (Yii::$app->user->can('content-manager2')) { ?>
            <li class="divider"></li>

            <li role="presentation" class="dropdown-header">Раздел редактора</li>
            <li><a href="/editor/channeling">Послания</a></li>
            <li><a href="/editor/blog">Блог</a></li>
            <li><a href="/editor-events/index">События</a></li>
        <?php } ?>

        <li class="divider"></li>
        <li><a href="<?= Url::to(['cabinet_wallet/index']) ?>"><i class="glyphicon glyphicon-btc"
                                                                  style="padding-right: 5px;"></i>Мой Кошелек</a></li>
        <li><a href="<?= Url::to(['site/user', 'id' => \Yii::$app->user->id]) ?>"><i class="glyphicon glyphicon-cog"
                                                                                     style="padding-right: 5px;"></i>Мой
                дизайн (профиль)</a></li>
        <li><a href="<?= Url::to(['cabinet/password_change']) ?>"><i class="glyphicon glyphicon-option-horizontal"
                                                                     style="padding-right: 5px;"></i>Сменить пароль</a>
        </li>

        <li class="divider"></li>

        <li><a href="<?= Url::to(['auth/logout']) ?>" data-method="post"><i class="glyphicon glyphicon-off"
                                                                            style="padding-right: 5px;"></i>Выйти</a>
        </li>
    </ul>
</li>