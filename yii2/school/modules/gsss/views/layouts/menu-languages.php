<?php
/**
 * Created by PhpStorm.
 * User: Бог-Творец
 * Date: 14.10.2016
 * Time: 20:40
 */

use yii\helpers\Url;
use yii\helpers\Html;

/** @var $this \yii\web\View */
?>

<li class="dropdown" id="userBlockLi">
    <a
        href="#"
        class="dropdown-toggle"
        data-toggle="dropdown"
        aria-expanded="false"
        role="button"
        style="padding: 5px 10px 5px 10px;"
            >
        <?= \yii\helpers\Html::img(Yii::$app->user->identity->getAvatar(), [
    'height' => '40px',
    'class' => 'img-circle'
]) ?>
<span class="caret"></span>
</a>
<ul class="dropdown-menu" role="menu">
    <?php foreach(\common\models\Language::find()->all() as $language) { ?>
        <li><a href="<?= \yii\helpers\Url::to(['cabinet/objects']) ?>"><?= $language->name ?></a></li>
    <?php } ?>
</ul>
</li>