<?php
/**
 * Created by PhpStorm.
 * User: Ra-m-ha
 * Date: 23.10.2019
 * Time: 17:45
 */

namespace school\modules\lesnayaopushka;

use cs\services\VarDumper;
use yii\base\BootstrapInterface;
use Yii;
use yii\base\Application;

class Module extends \yii\base\Module implements BootstrapInterface
{

//    public $defaultRoute = 'site/index';

    public function init()
    {
        parent::init();

        // инициализация модуля с помощью конфигурации, загруженной из config.php
        \Yii::configure($this, require __DIR__ . '/config.php');

        if (Yii::$app instanceof \yii\console\Application) {
            $this->controllerNamespace = 'school\modules\lesnayaopushka\commands';
        } else {
            $this->controllerNamespace = 'school\modules\lesnayaopushka\controllers';
        }
    }

    /**
     * @param Application $app
     */
    public function bootstrap($app)
    {
        $app->urlManager->addRules([
            '/'         => 'lesnayaopushka/site/index',

            'blog'                                                     => 'blog/index',
            'blog/<year:\\d{4}>/<month:\\d{2}>/<day:\\d{2}>/<id:\\w+>' => 'blog/item',
            'blog/<action>'                                            => 'blog/<action>',
        ]);
    }

}