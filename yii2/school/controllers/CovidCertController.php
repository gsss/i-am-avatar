<?php

namespace school\controllers;

use avatar\base\BaseController;
use avatar\models\UserLogin;
use avatar\models\UserRecover;
use avatar\models\UserRegistration;
use common\models\avatar\UserBill;
use common\models\avatar\UserBillDefault;
use common\models\CovCert;
use common\models\school\CommandLink;
use common\models\school\PotokUser3Link;
use common\models\school\School;
use common\models\school\SchoolDesign;
use common\models\school\UserLink;
use common\models\UserAvatar;
use common\models\UserDevice;
use common\models\UserPassword;
use common\models\UserRoot;
use common\services\Security;
use cs\Application;
use cs\services\Str;
use cs\services\VarDumper;
use Yii;
use yii\db\Query;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\helpers\StringHelper;
use yii\helpers\Url;
use yii\web\BadRequestHttpException;
use yii\web\NotFoundHttpException;
use yii\web\Request;
use yii\web\Response;
use yii\widgets\ActiveForm;
use cs\web\Exception;

class CovidCertController extends BaseController
{
    public $layout = 'main';


    public function actionStatus($uid)
    {
        $this->layout = 'empty';
        $covCert = CovCert::findOne(['uid' => $uid]);
        if (is_null($covCert)) {
            throw new NotFoundHttpException();
        }

        return $this->render(['covCert' => $covCert]);
    }

    public function actionQrCode($uid)
    {
        $this->layout = 'empty';
        $covCert = CovCert::findOne(['uid' => $uid]);
        if (is_null($covCert)) {
            throw new NotFoundHttpException();
        }

        return $this->render(['covCert' => $covCert]);
    }

    public function actionQrCode2($uid)
    {
        $this->layout = 'empty';
        $covCert = CovCert::findOne(['uid' => $uid]);
        if (is_null($covCert)) {
            throw new NotFoundHttpException();
        }

        return $this->render(['covCert' => $covCert]);
    }

    public function actionDownload($uid)
    {
        $this->layout = 'empty';
        $covCert = CovCert::findOne(['uid' => $uid]);
        if (is_null($covCert)) {
            throw new NotFoundHttpException();
        }

        return $this->render(['covCert' => $covCert]);
    }

}
