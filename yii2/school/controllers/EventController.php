<?php

namespace school\controllers;

use app\models\Article;
use app\models\SiteUpdate;
use app\services\Subscribe;
use avatar\models\forms\Contact;
use avatar\services\LogReader;
use common\components\providers\ETH;
use common\components\sms\IqSms;
use common\models\avatar\Currency;
use common\models\avatar\UserBill;
use common\models\CompanyCustomizeItem;
use common\models\PaymentBitCoin;
use common\models\school\Event;
use common\models\school\Kurs;
use common\models\school\Potok;
use common\models\UserAvatar;
use common\models\UserRegistration;
use common\payment\BitCoinBlockTrailPayment;
use cs\Application;
use cs\base\BaseController;
use cs\services\UploadFolderDispatcher;
use cs\services\VarDumper;
use cs\web\Exception;
use Yii;
use yii\base\UserException;
use yii\db\Connection;
use yii\db\Query;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use yii\helpers\FileHelper;
use yii\helpers\Url;
use yii\web\ForbiddenHttpException;
use yii\web\Response;

class EventController extends \avatar\base\BaseController
{

    /**
     * @param int $id идентификатор потока
     *
     * @return string
     */
    public function actionItem($id)
    {
        $p = Potok::findOne($id);
        $kurs = Kurs::findOne($p->kurs_id);
        $event = Event::findOne(['potok_id' => $p->id]);

        return $this->render([
            'potok' => $p,
            'kurs'  => $kurs,
            'event' => $event,
        ]);
    }

}
