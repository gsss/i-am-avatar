<?php

namespace school\controllers;

use app\models\Article;
use app\models\SiteUpdate;
use app\services\Subscribe;
use avatar\models\forms\Contact;
use avatar\services\LogReader;
use common\components\providers\ETH;
use common\components\sms\IqSms;
use common\models\avatar\Currency;
use common\models\avatar\UserBill;
use common\models\BillingMain;
use common\models\CompanyCustomizeItem;
use common\models\PaymentBitCoin;
use common\models\school\Page;
use common\models\school\School;
use common\models\shop\Basket;
use common\models\shop\CatalogItem;
use common\models\shop\Product;
use common\models\shop\Request;
use common\models\UserAvatar;
use common\models\UserRegistration;
use common\payment\BitCoinBlockTrailPayment;
use cs\Application;
use cs\base\BaseController;
use cs\services\UploadFolderDispatcher;
use cs\services\VarDumper;
use cs\web\Exception;
use Yii;
use yii\base\UserException;
use yii\db\Connection;
use yii\db\Query;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use yii\helpers\FileHelper;
use yii\helpers\Url;
use yii\web\ForbiddenHttpException;
use yii\web\Response;

class BillingController extends \avatar\controllers\ShopController
{

    /**
     * Оплата заявки
     *
     * @param int $id идентификатор заявки gs_users_shop_requests.id
     *
     * @return string Response
     * @throws
     */
    public function actionPay($id)
    {
        $BillingMain = BillingMain::findOne($id);
        if (is_null($BillingMain)) {
            throw new \Exception('Не найден счет');
        }

        return $this->render([
            'BillingMain' => $BillingMain,
        ]);
    }

}
