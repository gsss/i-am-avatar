<?php

namespace school\controllers;

use avatar\base\BaseController;
use avatar\models\UserLogin;
use avatar\models\UserRecover;
use avatar\models\UserRegistration;
use common\models\avatar\UserBill;
use common\models\avatar\UserBillDefault;
use common\models\school\CommandLink;
use common\models\school\PotokUser3Link;
use common\models\school\School;
use common\models\school\SchoolDesign;
use common\models\school\UserLink;
use common\models\UserAvatar;
use common\models\UserDevice;
use common\models\UserPassword;
use common\models\UserRoot;
use common\services\Security;
use cs\Application;
use cs\services\Str;
use cs\services\VarDumper;
use Yii;
use yii\db\Query;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\helpers\StringHelper;
use yii\helpers\Url;
use yii\web\BadRequestHttpException;
use yii\web\Request;
use yii\web\Response;
use yii\widgets\ActiveForm;
use cs\web\Exception;

class AuthController extends BaseController
{
    public $layout = 'main';

    public $timeOut = 31536000;

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only'  => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow'   => true,
                        'roles'   => ['@'],
                    ],
                ],
            ],
            'verbs'  => [
                'class'   => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }


    public function actionLogin()
    {
        if (!\Yii::$app->user->isGuest) {
            return $this->redirect(['cabinet/index']);
        }
        $model = new \avatar\models\forms\Login();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $user = $model->getUser();
            if (!empty($user->google_auth_code)) {
                Yii::$app->session->set('userLogin', $user->id);
            }
            $action = 'login';
            if ($user->is_2step_login == 0) {
                $action = 'login';
            } else {
                if ($user->is_2step_login == 1) {
                    if (Security::checkDeviceAfterLogin($model->getUser())) {
                        $action = 'login';
                    } else {
                        $action = 'confirm-device';
                    }
                } else if ($user->is_2step_login == 2) {
                    $action = 'confirm-device';
                }
            }

            if ($action == 'login') {
                if (!empty($user->google_auth_code)) {
                    return $this->redirect(['auth/google-confirm']);
                }
                $model->login($this->timeOut);
                UserLogin::add();

                if (Yii::$app->getUser()->getReturnUrl() == '/') return $this->redirect(['cabinet/index']);

                return $this->goBack();
            }
            if ($action == 'confirm-device') {
                $this->sendVerifyCode($user);
                return $this->redirect(['auth/device-confirm']);
            }
        } else {
            return $this->render('@school/views/auth/login',[
                'model' => $model,
                'school' => School::get(),
            ]);
        }
    }

    /**
     * Генерирует код, высылает его
     *
     * @param \common\models\UserAvatar $user
     *
     */
    private function sendVerifyCode($user)
    {
        $code = substr(str_shuffle('0123456789012345678901234567890123456789012345678901234567890123456789'), 0, 8);
        Yii::$app->session->set('enter', [
            'code'      => $code,
            'user_id'   => $user->id,
        ]);
        Yii::trace('$code='.$code, 'avatar\controllers\AuthController::sendVerifyCode()');

        Application::mail($user->email, 'Подтверждение устройства', 'device-confirm', [
            'code'      => $code,
            'user_id'   => $user->id,
        ]);
    }

    /**
     * Подтверждение нового устройства
     *
     * @return string
     */
    public function actionDeviceConfirm()
    {
        $model = new \avatar\models\forms\DeviceConfirm();

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $user = $model->getUser();
            if ($user->is_2step_login == UserAvatar::IS_2STEP_LOGIN_DEVICE) {
                UserDevice::add([
                    'user_id' => $user->id,
                    'name'    => Yii::$app->request->userAgent,
                ]);
            }
            if (!empty($user->google_auth_code)) {
                Yii::$app->session->set('userLogin', $user->id);
                return $this->redirect(['auth/google-confirm']);
            }
            Yii::$app->user->login($user);
            UserLogin::add();

            if (Yii::$app->getUser()->getReturnUrl() == '/') return $this->redirect(['cabinet/index']);
            return $this->goBack();
        } else {
            return $this->render('device-confirm', [
                'model' => $model
            ]);
        }
    }


    /**
     * Активация почты после магазина
     *
     * @param string $code
     *
     * @return Response
     * @throws
     */
    public function actionRegistrationShopActivate($code)
    {
        /** @var \avatar\models\UserRegistration $registration */
        $registration = UserRegistration::findOne(['code' => $code]);
        if (is_null($registration)) {
            throw new Exception('Срок ссылки истек или не верный код активации');
        }
        $userRoot = UserRoot::findOne($registration->parent_id);
        if (is_null($userRoot)) {
            throw new Exception('Пользователь не найден');
        }

        $user = UserAvatar::findOne(['user_root_id' => $userRoot->id]);
        $user->email_is_confirm = 1;
        $user->save();

        return $this->redirect(['cabinet/index']);
    }


    /**
     * Подтверждение google code
     *
     * @return string
     */
    public function actionGoogleConfirm()
    {
        $model = new \avatar\models\forms\GoogleConfirm();

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $user = $model->getUser();
            Yii::$app->user->login($user);
            UserLogin::add();

            if (Yii::$app->getUser()->getReturnUrl() == '/') return $this->redirect(['cabinet/index']);

            return $this->goBack();
        } else {
            return $this->render('google-confirm', [
                'model' => $model
            ]);
        }
    }

    /**
     * Провряет логин
     * REQUEST:
     * + email - string - логин - почта или телефон начинающийся на +7
     * + password - string - пароль
     *
     * @return string
     * errors
     * 101, 'Пользователь не найден'
     * 102, 'Пользователь не активирован'
     * 103, 'Пользователь заблокирован'
     * 104, 'Не верный пароль'
     * 105, 'Вы  не завели себе пароль для аккаунта. Зайдите в восстановление пароля'
     * 106, 'Email может содержать только буквы латинского алфавита'
     * 107, 'Нет обязательного параметра <name>'
     * 108, 'В телефоне должно быть 12 символов и никаких спецсимволов'
     * 109, 'В телефоне должны быть только цифры'
     *
     * @throws \yii\web\BadRequestHttpException
     */
    public function actionLoginAjax()
    {
        $email = strtolower(self::getParam('email'));
        $email = trim($email);
        $password = self::getParam('password');
        if (is_null($email)) {
            return self::jsonErrorId(107, 'Нет обязательного параметра mail');
        }
        if (is_null($password)) {
            return self::jsonErrorId(107, 'Нет обязательного параметра password');
        }

        if (StringHelper::startsWith($email, '+7')) {
            // Это телефон
            // +79252374501
            if (strlen($email) != 12) {
                return self::jsonErrorId(108, 'В телефоне должно быть 12 символов и никаких спецсимволов');
            }
            if (!$this->validatePhone(substr($email, 1))) {
                return self::jsonErrorId(109, 'В телефоне должны быть только цифры');
            }
            $user = UserAvatar::findOne(['phone' => substr($email,1)]);
        } else {
            if (strpos($email, '@')) {
                if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
                    return self::jsonErrorId(106, 'Не верный формат email');
                }
            } else {
                return self::jsonErrorId(107, 'Email должен содержать @');
            }
            /** @var \common\models\UserAvatar $user */
            try {
                $user = UserAvatar::findOne(['email' => $email]);
            } catch (\Exception $e) {
                return self::jsonErrorId(101, 'Пользователь не найден');
            }
        }

        if ($user->mark_deleted == 1) {
            return self::jsonErrorId(103, 'Пользователь заблокирован');
        }
        if ($user->password == '') {
            return self::jsonErrorId(105, 'Вы  не завели себе пароль для аккаунта. Зайдите в восстановление пароля');
        }
        if (!$user->validatePassword($password)) {
            return self::jsonErrorId(104, 'Не верный пароль');
        }
        if ($user->is_2step_login == 0) {
            if (!empty($user->google_auth_code)) {
                Yii::$app->session->set('userLogin', $user->id);
                return self::jsonSuccess([
                    'code'      => 201,
                    'redirect'  => Url::to(['auth/google-confirm'])
                ]);
            }
            Yii::$app->user->login($user, $this->timeOut);
            UserLogin::add();

            return self::jsonSuccess(['code' => 200]);
        } else {
            if ($user->is_2step_login == 1) {
                if (Security::checkDeviceAfterLogin($user)) {
                    if (!empty($user->google_auth_code)) {
                        Yii::$app->session->set('userLogin', $user->id);
                        return self::jsonSuccess([
                            'code'      => 201,
                            'redirect'  => Url::to(['auth/google-confirm'])
                        ]);
                    }
                    Yii::$app->user->login($user, $this->timeOut);
                    UserLogin::add();

                    return self::jsonSuccess(['code' => 200]);
                } else {
                    $this->sendVerifyCode($user);
                    return self::jsonSuccess([
                        'code'      => 201,
                        'redirect'  => Url::to(['auth/device-confirm'])
                    ]);
                }
            } else if ($user->is_2step_login == 2) {
                $this->sendVerifyCode($user);
                return self::jsonSuccess([
                    'code'      => 201,
                    'redirect'  => Url::to(['auth/device-confirm'])
                ]);
            }
        }
    }

    private function validatePhone($amount)
    {
        $symbols = Str::getChars($amount);
        foreach ($symbols as $char) {
            if (!in_array($char, ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9'])) return false;
        }
        return true;
    }

    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    public function actionPasswordRecover()
    {
        $model = new \avatar\models\forms\PasswordRecover();
        $model->setScenario('insert');

        if (Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) {
            $model->setScenario('ajax');
            Yii::$app->response->format = Response::FORMAT_JSON;

            return ActiveForm::validate($model);
        }

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $model->send();
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        }
        else {
            return $this->render('password-recover',[
                'model' => $model,
            ]);
        }

    }

    /**
     * Активация восстановления пароля
     *
     * @param string $code
     *
     * @return string
     *
     * @throws Exception
     */
    public function actionPasswordRecoverActivate($code)
    {
        $row = UserRecover::findOne(['code' => $code]);
        if (is_null($row)) {
            throw new Exception('Не найден код активации');
        }
        $user = UserAvatar::findOne($row->parent_id);
        if (is_null($user)) {
            throw new Exception('Пользователь не найден');
        }

        $model = new \avatar\models\forms\PasswordNewRecover(['code' => $code]);
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $model->action($user);
            $row->delete();

            return $this->redirect(['auth/login']);
        }

        return $this->render('password-recover-activate', [
            'model' => $model,
            'user'  => $user,
        ]);
    }

    public function actionRegistration()
    {
        $School = School::get();
        $model = new \school\models\forms\Registration(['school' => $School]);

        if (Yii::$app->request->isAjax) {
            $model->setScenario('ajax');
            if ($model->load(Yii::$app->request->post())) {
                Yii::$app->response->format = Response::FORMAT_JSON;
                return ActiveForm::validate($model);
            }
        }
        $model->setScenario('insert');

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $model->register();
            Yii::$app->session->setFlash('contactFormSubmitted');
        }

        return $this->render('@school/views/auth/registration', [
            'model'  => $model,
            'school' => $School,
        ]);
    }


    /**
     * @return string
     */
    public function actionRepareGoogleCode()
    {
        $model = new \avatar\models\forms\RepareGoogleCode();

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $model->action();
            Yii::$app->session->setFlash('form', 1);
        }

        return $this->render('repare-google-code', [
            'model' => $model,
        ]);

    }

    /**
     * Активация регистрации
     *
     * @param string $code
     *
     * @return Response
     * @throws Exception
     */
    public function actionRegistrationActivate3($code)
    {
        /** @var \avatar\models\UserRegistration $registration */
        $registration = UserRegistration::findOne(['code' => $code]);
        if (is_null($registration)) {
            throw new Exception('Срок ссылки истек или не верный код активации');
        }
        /** @var \common\models\UserAvatar $user */
        $user = UserAvatar::findOne($registration->parent_id);
        if (is_null($user)) {
            throw new Exception('Пользователь не найден');
        }

        try {
            // если уже есть счет привязанный к карте то не нужно создавать второй
            $billing = UserBill::findOne(['user_id' => $user->id]);
        } catch (\Exception $e) {

        }

        $user->activate();
        Yii::$app->user->login($user);
        $registration->delete();

        return $this->redirect(['cabinet/index']);
    }


    /**
     * Активация регистрации
     *
     * @param string $code
     *
     * @return Response
     * @throws
     */
    public function actionRegistrationActivate($code)
    {
        /** @var \avatar\models\UserRegistration $registration */
        $registration = UserRegistration::findOne(['code' => $code]);
        if (is_null($registration)) {
            throw new Exception('Срок ссылки истек или не верный код активации');
        }
        $userRoot = UserRoot::findOne($registration->parent_id);
        if (is_null($userRoot)) {
            throw new Exception('Пользователь не найден');
        }
        $userPassword = UserPassword::findOne(['user_root_id' => $userRoot->id]);

        $school = School::get();
        $userLink = UserLink::find()->where(['user_root_id' => $userRoot->id, 'school_id' => $school->id])->one();
        if (is_null($userLink)) {
            $userLink = UserLink::add(['user_root_id' => $userRoot->id, 'school_id' => $school->id]);
        }

        // Создаю пользователя
        {
            $user = UserAvatar::add([
                'user_root_id'       => $userRoot->id,
                'email'              => $userRoot->email,
                'password'           => $userPassword->password_hash,
                'registered_ad'      => time(),
                'auth_key'           => Security::generateRandomString(60),
                'password_save_type' => UserBill::PASSWORD_TYPE_OPEN_CRYPT,
            ]);

            // устанавливаю статус что пользователь создан
            $userRoot->avatar_status = 2;
            $userRoot->save();
        }

        // делаю update для ранее зарегистрированых лидов
        (new Query())->createCommand()->update(PotokUser3Link::tableName(), ['is_avatar' => 1], ['user_root_id' => $userRoot->id])->execute();

        Yii::$app->user->login($user, $this->timeOut);
        $registration->delete();
        $userPassword->delete();

        // Если стоит флаг добавлять в команду то добавлю
        $s = SchoolDesign::findOne(['school_id' => $school->id]);
        if (!is_null($s)) {
            if ($s->is_add_user_in_command_after_register) {
                CommandLink::add([
                    'user_id'    => $user->id,
                    'school_id'  => $school->id,
                    'is_only_my' => 0,
                ]);
            }
        }

        return $this->redirect(['cabinet/index']);
    }



    /**
     * Активация кода для активации карты
     *
     * @param string $code
     *
     * @return Response
     * @throws Exception
     */
    public function actionQrEnterActivate($code)
    {
        if (\yii\helpers\ArrayHelper::getValue(Yii::$app->params, 'isTransfere', false) == true) {
            throw new Exception(Yii::$app->params['isTransfere_string']);
        }
        /** @var \avatar\models\UserRegistration $registration */
        $registration = \avatar\models\UserEnter::findOne(['code_registration' => $code]);
        if (is_null($registration)) {
            throw new Exception('Не верный код активации');
        }
        /** @var \common\models\UserAvatar $user */
        $user = UserAvatar::findOne($registration->id);
        if (is_null($user)) {
            throw new Exception('Пользователь не найден');
        }
        $user->activate();
        Yii::$app->user->login($user);
        $registration->delete();

        return $this->goHome();
    }

}
