<?php

namespace school\controllers;

use app\models\Article;
use app\models\SiteUpdate;
use app\services\Subscribe;
use common\models\avatar\UserBill;
use common\models\UserAvatar;
use cs\base\BaseController;
use cs\services\UploadFolderDispatcher;
use cs\services\VarDumper;
use cs\web\Exception;
use Yii;
use yii\base\UserException;
use yii\bootstrap\ActiveForm;
use yii\db\Connection;
use yii\db\Query;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use yii\helpers\FileHelper;
use yii\web\Response;

class CabinetProfilePhoneController extends \avatar\controllers\CabinetProfilePhoneController
{

    public function actionIndex2()
    {
        /** @var \avatar\models\forms\ProfilePhone $model */
        $model = \avatar\models\forms\ProfilePhone::findOne(Yii::$app->user->id);
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $model->save();
            Yii::$app->session->setFlash('form');
        }

        return $this->render([
            'model' => $model,
        ]);
    }
}
