<?php

namespace school\controllers;

use avatar\models\forms\school\CommandLink;
use avatar\models\forms\UserAvatar;
use avatar\models\validate\CardNumberAccept;
use common\components\Card;
use common\components\providers\BTC;
use common\components\providers\ETH;
use common\models\avatar\Currency;
use common\models\avatar\UserBill;
use common\models\comment\Comment;
use common\models\comment\CommentList;
use common\models\Config;
use common\models\piramida\Wallet;
use common\models\school\ProjectManagerLink;
use common\models\school\School;
use common\models\task\Task;
use common\models\UserSeed;
use common\models\UserWallet;
use common\payment\BitCoinBlockTrailPayment;
use common\services\Security;
use common\services\Subscribe;
use console\controllers\MoneyRateController;
use cs\Application;
use cs\services\VarDumper;
use cs\web\Exception;
use cs\Widget\FileUpload4\ModelFields;
use Yii;
use yii\base\Event;
use yii\filters\AccessControl;
use yii\helpers\FileHelper;
use yii\helpers\Json;
use yii\helpers\StringHelper;
use yii\httpclient\Client;
use yii\imagine\Image;
use yii\web\Response;

class CabinetTaskListController extends \avatar\controllers\CabinetTaskListController
{


}
