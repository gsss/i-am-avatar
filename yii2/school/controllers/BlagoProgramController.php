<?php

namespace school\controllers;

use avatar\models\forms\BlagoProgram;
use avatar\models\UserEnter;
use avatar\modules\UniSender\UniSender;
use common\components\providers\ETH;
use common\models\avatar\Currency;
use common\models\avatar\UserBill;
use common\models\avatar\UserBillDefault;
use common\models\HD;
use common\models\HDtown;
use common\models\school\PotokUser3Link;
use common\models\school\School;
use common\models\subscribe\UserSubscribe;
use common\models\UserAvatar;
use common\models\UserDigitalSign;
use common\models\UserDocument;
use common\models\UserMaster;
use common\models\UserTelegramConnect;
use common\models\UserTelegramTemp;
use common\services\Security\AES;
use cs\base\BaseController;
use cs\services\SitePath;
use cs\services\UploadFolderDispatcher;
use cs\services\VarDumper;
use cs\web\Exception;
use iAvatar777\services\FormAjax\DefaultFormAjax;
use Yii;
use yii\base\UserException;
use yii\bootstrap\ActiveForm;
use yii\db\Connection;
use yii\db\Query;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use yii\helpers\FileHelper;
use yii\helpers\StringHelper;
use yii\web\Response;

class BlagoProgramController extends \avatar\base\BaseController
{
    public function actions()
    {
        return [
            'login' => [
                'class'    => '\iAvatar777\services\FormAjax\DefaultFormAjax',
                'model'    => '\school\models\forms\ShopLogin',
                'formName' => 'ShopLogin',
            ],
            'registration' => [
                'class'    => '\iAvatar777\services\FormAjax\DefaultFormAjax',
                'model'    => '\school\models\forms\ShopRegistration',
                'formName' => 'ShopRegistration',
            ],
            'registration-code' => [
                'class'    => '\iAvatar777\services\FormAjax\DefaultFormAjax',
                'model'    => '\school\models\forms\ShopRegistrationCode',
                'formName' => 'ShopRegistrationCode',
            ],
        ];
    }

    public function actionIndex($id)
    {
        $program = BlagoProgram::findOne($id);
        $school = School::get();
        if ($program->school_id != $school->id) {
            throw new \Exception('Программа не найдена' );
        }
        $model = new \school\models\validate\BlagoProgramIndexAjax(['id' => $id]);

        if (Yii::$app->request->isPost) {
            if ($model->load(Yii::$app->request->post()) && $model->validate()) {
                $s = $model->save();
                return self::jsonSuccess($s);
            } else {
                return self::jsonErrorId(102, $model->getErrors102());
            }
        }

        return $this->render([
            'model'   => $model,
            'program' => $program,
            'school'  => $school,
        ]);
    }

    public function actionIndex2($id)
    {
        $program = BlagoProgram::findOne($id);
        $school = School::get();
        if ($program->school_id != $school->id) {
            throw new \Exception('Программа не найдена' );
        }
        $model = new \school\models\validate\BlagoProgramIndexAjax(['id' => $id]);

        if (Yii::$app->request->isPost) {
            if ($model->load(Yii::$app->request->post()) && $model->validate()) {
                $s = $model->save();
                return self::jsonSuccess($s);
            } else {
                return self::jsonErrorId(102, $model->getErrors102());
            }
        }

        return $this->render([
            'model'   => $model,
            'program' => $program,
            'school'  => $school,
        ]);
    }

    public function actionSuccess($id)
    {
        $program = BlagoProgram::findOne($id);
        $school = School::get();
        if ($program->school_id != $school->id) {
            throw new \Exception('Программа не найдена' );
        }

        return $this->render([
            'program' => $program,
            'school'  => $school,
        ]);
    }

    public function actionError($id)
    {
        $program = BlagoProgram::findOne($id);
        $school = School::get();
        if ($program->school_id != $school->id) {
            throw new \Exception('Программа не найдена' );
        }

        return $this->render([
            'program' => $program,
            'school' => $school,
        ]);
    }
}
