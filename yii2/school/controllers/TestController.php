<?php

namespace school\controllers;

use avatar\models\forms\Contact;
use avatar\modules\ETH\ServiceEtherScan;
use avatar\modules\UniSender\UniSender;
use avatar\services\LogReader;
use common\components\providers\BTC;
use common\components\providers\ETH;
use common\components\sms\IqSms;
use common\models\avatar\Currency;
use common\models\avatar\QrCode;
use common\models\avatar\UserBill;
use common\models\avatar\UserBillConfig;
use common\models\Card;
use common\models\Config;
use common\models\PaymentBitCoin;
use common\models\RequestTokenCreate;
use common\models\statistic\UserBinanceStatisticItem;
use common\models\subscribe\SubscribeList;
use common\models\Token;
use common\models\UserAvatar;
use common\models\UserDocument;
use common\models\UserRegistration;
use common\models\UserSeed;
use common\payment\BitCoinBlockTrailPayment;
use common\services\CloudFlare;
use common\services\Security;
use cs\Application;
use cs\base\BaseController;
use cs\services\Str;
use cs\services\UploadFolderDispatcher;
use cs\services\VarDumper;
use cs\web\Exception;
use Yii;
use yii\base\UserException;
use yii\db\Connection;
use yii\db\Query;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use yii\helpers\FileHelper;
use yii\helpers\Json;
use yii\helpers\Url;
use yii\httpclient\Client;
use yii\web\Response;
use Blocktrail\SDK\Services\BlocktrailBatchUnspentOutputFinder;
use Blocktrail\SDK\WalletV1Sweeper;

class TestController extends \yii\web\Controller
{
    public $layout = 'menu';

    public function actionTest1()
    {
        VarDumper::dump(Yii::$app);
    }

    /**
     */
    public function actionTest2()
    {
//        /** @var \common\services\CloudFlare $CloudFlare */
//        $CloudFlare = Yii::$app->CloudFlare;
//        $d = $CloudFlare->_post('zones/061a8247a405dcf405ebbbf7544e359c/dns_records', [
//            'name'    => 'ra',
//            'type'    => 'A',
//            'content' => '89.40.117.156',
//        ]);
//        VarDumper::dump(Json::decode($d->content));
         require (Yii::getAlias('@vendor/akbarjoudi/yii2-bot-telegram/Telegram.php'));

        /** @var \aki\telegram\Telegram $telegram */
        $telegram = Yii::$app->telegram;
//        $response = $telegram->getUpdates();
//        VarDumper::dump($response);
        $telegram->sendMessage(['chat_id' => 122605414, 'text' => 'Привет Я твой Звездный Аватар! Я покажу тебе Путь! Ты готов к Игре?']);
    }

    /**
     */
    public function actionTest3()
    {
        /** @var \yii\swiftmailer\Mailer $mailer */
        $mailer = Yii::$app->mailer;
        $html = $mailer->render('html/' . 'a', []);
        $mailer->viewPath = '@avatar/mail';

        $o = \Yii::$app->mailer
            ->compose()
            ->setFrom(['info@i-am-avatar.com' => 'iAvatar'])
            ->setTo('dram1008@yandex.ru')
            ->setHtmlBody($html)
            ->setSubject('1');
        VarDumper::dump($o->send());
    }


}
