<?php

namespace school\controllers;

use avatar\models\forms\BlagoProgram;
use avatar\models\UserEnter;
use avatar\modules\UniSender\UniSender;
use common\components\providers\ETH;
use common\models\avatar\Currency;
use common\models\avatar\UserBill;
use common\models\avatar\UserBillDefault;
use common\models\HD;
use common\models\HDtown;
use common\models\school\PotokUser3Link;
use common\models\school\School;
use common\models\subscribe\UserSubscribe;
use common\models\UserAvatar;
use common\models\UserDigitalSign;
use common\models\UserDocument;
use common\models\UserMaster;
use common\models\UserTelegramConnect;
use common\models\UserTelegramTemp;
use common\services\Security\AES;
use cs\base\BaseController;
use cs\services\SitePath;
use cs\services\UploadFolderDispatcher;
use cs\services\VarDumper;
use cs\web\Exception;
use iAvatar777\services\FormAjax\DefaultFormAjax;
use Yii;
use yii\base\UserException;
use yii\bootstrap\ActiveForm;
use yii\db\Connection;
use yii\db\Query;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use yii\helpers\FileHelper;
use yii\helpers\StringHelper;
use yii\web\Response;

class KoopController extends \avatar\controllers\CabinetBaseController
{
    public function actions()
    {
        return [
            'sign-ajax' => [
                'class'    => '\avatar\controllers\actions\DefaultAjax',
                'model'    => '\school\models\validate\KoopSignAjax',
            ],
            'sign2-ajax' => [
                'class'    => '\iAvatar777\services\FormAjax\DefaultFormAjax',
                'model'    => '\school\models\validate\KoopSign2Ajax',
                'formName' => 'KoopSign2Ajax',
            ],
            'pay-ajax' => [
                'class'    => '\iAvatar777\services\FormAjax\DefaultFormAjax',
                'model'    => '\school\models\validate\KoopPayAjax',
                'formName' => 'KoopPayAjax',
            ],
        ];
    }

    public function actionIndex()
    {
        $UserEnter = \avatar\models\UserEnter::findOne(['user_id' => Yii::$app->user->id]);
        if (!is_null($UserEnter)) {
            if ($UserEnter->is_paid) {
                return $this->redirect(['koop/main']);
            }
        }

        return $this->render([]);
    }

    public function actionPayAddMoney()
    {
        $model = new \school\models\validate\KoopPayAddAjax();

        if (Yii::$app->request->isPost) {
            if ($model->load(Yii::$app->request->post()) && $model->validate()) {
                $s = $model->save();
                return self::jsonSuccess($s);
            } else {
                return self::jsonErrorId(102, $model->getErrors102());
            }
        }

        return $this->render([
            'model' => $model,
        ]);
    }

    public function actionMain()
    {

        return $this->render([]);
    }

    public function actionBlagoProgram($id)
    {
        $program = BlagoProgram::findOne($id);
        $school = School::get();
        if ($program->school_id != $school->id) {
            throw new \Exception('Программа не найдена' );
        }

        return $this->render([
            'program' => $program,
            'school' => $school,
        ]);
    }

    public function actionRegistration()
    {
        $s = School::get();
        $model = new \school\models\validate\UserEnter(['_school_id' => $s->id]);

        if (Yii::$app->request->isPost) {
            if ($model->load(Yii::$app->request->post()) && $model->validate()) {
                $s = $model->save();
                return self::jsonSuccess($s);
            } else {
                return self::jsonErrorId(102, $model->getErrors102());
            }
        }

        return $this->render([
            'model' => $model,
        ]);
    }

    public function actionPhone()
    {
        /** @var \avatar\models\forms\ProfilePhone $model */
        $model = \avatar\models\forms\ProfilePhone::findOne(Yii::$app->user->id);
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $model->save();
            Yii::$app->session->setFlash('form');
        }

        return $this->render([
            'model' => $model,
        ]);
    }

    public function actionMy()
    {
        $s = School::get();
        $item = \common\models\UserEnter::findOne(['user_id' => Yii::$app->user->id, 'school_id' => $s->id]);

        return $this->render([
            'item' => $item,
            'school' => $s,
        ]);
    }

    public function actionIsSign()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        /** @var UserAvatar $user */
        $user = \Yii::$app->user->identity;

        return self::jsonSuccess([
            'is_sign'  => UserDigitalSign::isPK($user->id)? 1 : 0,
        ]);
    }

    public function actionIsSign2()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        /** @var UserAvatar $user */
        $user = \Yii::$app->user->identity;

        return self::jsonSuccess([
            'is_sign'  => UserDigitalSign::isPK($user->id)? 1 : 0,
        ]);
    }

    public function actionGetsign()
    {
        $s = School::get();
        $model = new \school\models\validate\UserEnter(['_school_id' => $s->id]);

        if (Yii::$app->request->isPost) {
            if ($model->load(Yii::$app->request->post()) && $model->validate()) {
                $s = $model->save();
                return self::jsonSuccess();
            } else {
                return self::jsonErrorId(102, $model->getErrors102());
            }
        }

        return $this->render([
            'model' => $model,
        ]);
    }

    public function actionVideo()
    {
        $UserEnter = UserEnter::findOne(['user_id' => Yii::$app->user->id]);
        $school = School::findOne($UserEnter->school_id);
        $model = new \school\models\forms\KoopVerify(['id' => $UserEnter->id]);

        if ($model->load(Yii::$app->request->post()) && $model->validate())     {
            $model->action();
            Yii::$app->session->setFlash('form', 1);
        }

        return $this->render([
            'UserEnter' => $UserEnter,
            'school'    => $school,
            'model'     => $model,
        ]);
    }

    public function actionSign()
    {
        return $this->render([]);
    }

    public function actionPay()
    {
        return $this->render([]);
    }

    public function actionPay1()
    {
        return $this->render([]);
    }

    public function actionPay2()
    {
        return $this->render([]);
    }
}
