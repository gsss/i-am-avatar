<?php

namespace school\controllers;

use common\models\school\Potok;
use common\models\school\School;
use cs\services\Url;
use cs\services\VarDumper;
use yii\helpers\StringHelper;
use yii\web\HttpException;

class PageController extends \avatar\base\BaseController
{

    public $layout = 'blank';

    /**
     */
    public function actionRoot()
    {
        $module = \common\models\school\School::getModuleClassName();
        if (!is_null($module)) {
            /** @var \yii\base\Module $moduleObject */
            $moduleObject = $module::getInstance();
            $defaultRoute = $moduleObject->defaultRoute;

            try {
                return $moduleObject->runAction($defaultRoute);
            } catch (\Exception $e) {

            }
        }
        $school = School::get();
        $page = $school->getPage('/');

        if (is_null($page)) {
            // страница не найдена
            throw  new HttpException(404, 'Page not found');
        }
        if ($page->tilda_page_id) {
            return file_get_contents(\Yii::getAlias('@school/web/tilda/' . $school->id . '/html/' . 'file' . $page->id . '.' . 'html'));
        }
        $page->view();
        \Yii::$app->session->set('__page_id', $page->id);

        return $this->render([
            'page'   => $page,
            'school' => $school,
        ]);
    }

    /**
     */
    public function actionPage($url)
    {
        $u = explode('?', $url);
        if (count($u) > 1) {
            $url = $u[0];
        }
        $school = School::get();
        $page = $school->getPage($url);

        if (is_null($page)) {
            // страница не найдена
            throw  new HttpException(404, 'Page not found');
        }
        if (count($page->getBlocks()) == 0) {
            // страница не найдена
            throw new HttpException(404, 'Страница не имеет блоков');
        }
        $page->view();

        return $this->render([
            'page'   => $page,
            'school' => $school,
        ]);
    }

    /**
     */
    public function actionController()
    {
        $url = \Yii::$app->request->getUrl();
        $u = explode('?', $url);
        if (count($u) > 1) {
            $url = $u[0];
        }

        $school = School::get();
        $page = $school->getPage($url);
        if (is_null($page)) {
            // страница не найдена
            throw  new HttpException(404, 'Page not found');
        }
        if (count($page->getBlocks()) == 0) {
            // страница не найдена
            throw  new HttpException(404, 'Page not found');
        }
        $page->view();
        \Yii::$app->session->set('__page_id', $page->id);

        return $this->render([
            'page'   => $page,
            'school' => $school,
        ]);
    }

    /**
     */
    public function actionControllerAction()
    {
        $url = \Yii::$app->request->getUrl();
        $u = explode('?', $url);
        if (count($u) > 1) {
            $url = $u[0];
        }

        $school = School::get();
        $page = $school->getPage($url);
        if (is_null($page)) {
            // страница не найдена
            throw  new HttpException(404, 'Page not found');
        }
        if (count($page->getBlocks()) == 0) {
            // страница не найдена
            throw  new HttpException(404, 'Page not found');
        }
        $page->view();
        \Yii::$app->session->set('__page_id', $page->id);

        return $this->render('controller', [
            'page'   => $page,
            'school' => $school,
        ]);
    }
}
