<?php

namespace school\controllers;

use app\services\Subscribe;
use avatar\controllers\actions\DefaultAjax;
use avatar\controllers\AdminBaseController;
use avatar\models\forms\BlogItem;
use common\models\avatar\UserBill;
use common\models\investment\IcoRequest;
use common\models\investment\IcoRequestAddress;
use common\models\investment\IcoRequestSuccess;
use common\models\investment\Project;
use common\models\investment\ProjectIco;
use common\models\KoopPodryad;
use common\models\MapBankomat;
use common\models\PaySystemConfig;
use common\models\PaySystemListItem;
use common\models\school\School;
use cs\Application;
use cs\services\VarDumper;
use cs\web\Exception;
use iAvatar777\services\FormAjax\DefaultFormAjax;
use Yii;
use yii\base\UserException;
use yii\filters\AccessControl;
use yii\helpers\StringHelper;
use yii\helpers\Url;
use yii\web\Controller;
use yii\filters\VerbFilter;
use cs\base\BaseController;
use yii\web\Response;
use yii\widgets\ActiveForm;

class CabinetKoopPodryadController extends \avatar\controllers\CabinetProfilePhoneController
{
    public function actions()
    {
        return [

            'tz-sign'         => [
                'class' => '\avatar\controllers\actions\DefaultAjax',
                'model' => '\school\models\validate\CabinetKoopPodryadStep4TzSign',
            ],
            'tz-sign2'         => [
                'class' => '\iAvatar777\services\FormAjax\DefaultFormAjax',
                'model' => '\school\models\validate\CabinetKoopPodryadStep4TzSign2',
            ],

            'protokol-sign'         => [
                'class' => '\avatar\controllers\actions\DefaultAjax',
                'model' => '\school\models\validate\CabinetKoopPodryadStep4ProtokolSign',
            ],
            'protokol-sign2'         => [
                'class' => '\iAvatar777\services\FormAjax\DefaultFormAjax',
                'model' => '\school\models\validate\CabinetKoopPodryadStep4ProtokolSign2',
            ],

        ];
    }

    public function actionIndex()
    {
        $school = School::get();

        return $this->render(['school' => $school]);
    }


    public function actionView($id)
    {
        $item = KoopPodryad::findOne($id);
        $school = School::findOne($item->school_id);

        return $this->render([
            'item'   => $item,
            'school' => $school,
        ]);
    }

    public function actionStep4($id)
    {
        $item = KoopPodryad::findOne($id);
        $school = School::findOne($item->school_id);

        return $this->render([
            'item'   => $item,
            'school' => $school,
        ]);
    }

    public function actionStep9($id)
    {
        $item = KoopPodryad::findOne($id);
        $school = School::findOne($item->school_id);

        return $this->render([
            'item'   => $item,
            'school' => $school,
        ]);
    }

}
