<?php

namespace school\controllers;

use app\models\Article;
use app\models\SiteUpdate;
use app\services\Subscribe;
use avatar\models\forms\Contact;
use avatar\services\LogReader;
use common\components\providers\ETH;
use common\components\sms\IqSms;
use common\models\avatar\Currency;
use common\models\avatar\UserBill;
use common\models\CompanyCustomizeItem;
use common\models\PaymentBitCoin;
use common\models\school\Page;
use common\models\school\School;
use common\models\shop\Basket;
use common\models\shop\CatalogItem;
use common\models\shop\Product;
use common\models\shop\Request;
use common\models\UserAvatar;
use common\models\UserRegistration;
use common\payment\BitCoinBlockTrailPayment;
use cs\Application;
use cs\base\BaseController;
use cs\services\UploadFolderDispatcher;
use cs\services\VarDumper;
use cs\web\Exception;
use Yii;
use yii\base\UserException;
use yii\db\Connection;
use yii\db\Query;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use yii\helpers\FileHelper;
use yii\helpers\Url;
use yii\web\ForbiddenHttpException;
use yii\web\Response;

class ShopController extends \avatar\controllers\ShopController
{
    public function actionIndex()
    {
        $module = \common\models\school\School::getModuleClassName();

        if (!is_null($module)) {
            /** @var \yii\base\Module $moduleObject */
            $moduleObject = $module::getInstance();
            $module = substr($module, 0, strlen($module) - 7);
            $shopController = $module . '\\controllers\\ShopController';
            if (class_exists($shopController)) {
                $defaultRoute = 'shop/index';

                return $moduleObject->runAction($defaultRoute);
            }
        }

        return $this->render('@avatar/views/shop/index', ['school' => School::get()]);
    }

    public function actionCard($card)
    {
        return $this->render( [
            'school' => School::get(),
            'card'   => $card
        ]);
    }

    public function actionCardPay($id, $card)
    {
        return $this->render( [
            'school' => School::get(),
            'card'   => $card,
            'id'     => $id,
        ]);
    }

    /**
     */
    public function actionCatalog($id)
    {
        $catalog = CatalogItem::findOne($id);
        $school = School::findOne($catalog->school_id);

        return $this->render([
            'catalog' => $catalog,
            'school'  => $school,
        ]);
    }


    /**
     */
    public function actionItem($id)
    {
        $Product = Product::findOne($id);
        if (is_null($Product)) {
            throw new Exception('Не найден продукт');
        }

        return $this->render('@avatar/views/'.$this->id.'/' . $this->action->id, [
            'item'   => $Product,
        ]);
    }

    /**
     * Добавляет в корзину
     */
    public function actionCartAdd()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        $model = new \avatar\models\forms\shop\CartAdd();

        if (!$model->load(Yii::$app->request->post(), '')) {
            return self::jsonErrorId(400, 'Не загружены данные');
        }
        if (!$model->validate()) {
            return self::jsonErrorId(102, $model->errors);
        }

        return self::jsonSuccess(
            ['counter' => $model->action()]
        );
    }

    /**
     * Добавляет в корзину
     */
    public function actionCartDelete()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        $model = new \avatar\models\forms\shop\CartDelete();

        if (!$model->load(Yii::$app->request->post(), '')) {
            return self::jsonErrorId(400, 'Не загружены данные');
        }
        if (!$model->validate()) {
            return self::jsonErrorId(102, $model->errors);
        }

        return self::jsonSuccess($model->action());
    }

    /**
     * Показывает корзину
     */
    public function actionCart()
    {
        return $this->render();
    }

    /**
     */
    public function actionDelivery()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        if (Yii::$app->user->isGuest) {
            return self::jsonErrorId(10, 'Пользователь должен быть авторизован');
        }

        $model = new \avatar\models\forms\ShopDelivery();

        if (!$model->load(Yii::$app->request->post())) {
            return self::jsonErrorId(400, 'Не загружены данные');
        }
        if (!$model->validate()) {
            return self::jsonErrorId(102, $model->errors);
        }

        return self::jsonSuccess($model->action());
    }

    /**
     */
    public function actionAddress()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        if (Yii::$app->user->isGuest) {
            return self::jsonErrorId(10, 'Пользователь должен быть авторизован');
        }

        $model = new \avatar\models\forms\ShopAddress();

        if (!$model->load(Yii::$app->request->post())) {
            return self::jsonErrorId(400, 'Не загружены данные');
        }
        if (!$model->validate()) {
            return self::jsonErrorId(102, $model->errors);
        }

        return self::jsonSuccess($model->action());
    }

    /**
     */
    public function actionAnketa($id)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        if (Yii::$app->user->isGuest) {
            return self::jsonErrorId(10, 'Пользователь должен быть авторизован');
        }

        $anketa = \common\models\school\AnketaShop::findOne($id);

        $model = new \avatar\models\forms\ShopAnketa(['anketa' => $anketa]);

        if (!$model->load(Yii::$app->request->post())) {
            return self::jsonErrorId(400, 'Не загружены данные');
        }
        if (!$model->validate()) {
            return self::jsonErrorId(102, $model->errors);
        }

        return self::jsonSuccess($model->action());
    }

    /**
     */
    public function actionPaySystem()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        if (Yii::$app->user->isGuest) {
            return self::jsonErrorId(10, 'Пользователь должен быть авторизован');
        }

        $model = new \avatar\models\forms\ShopPaySystem();

        if (!$model->load(Yii::$app->request->post())) {
            return self::jsonErrorId(400, 'Не загружены данные');
        }
        if (!$model->validate()) {
            return self::jsonErrorId(102, $model->errors);
        }

        return self::jsonSuccess($model->action());
    }


    /**
     * Оплата заявки
     *
     * @param int $id идентификатор заявки gs_users_shop_requests.id
     *
     * @return string Response
     * @throws
     */
    public function actionPay($id)
    {
        if (Yii::$app->user->isGuest) {
            return self::jsonErrorId(10, 'Пользователь должен быть авторизован');
        }

        $request = Request::findOne($id);
        if (is_null($request)) {
            throw new \Exception('Не найдена заявка');
        }

        return $this->render([
            'request' => $request,
        ]);
    }

}
