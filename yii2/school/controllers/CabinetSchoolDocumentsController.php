<?php

namespace school\controllers;

use app\services\Subscribe;
use avatar\controllers\actions\DefaultAjax;
use avatar\modules\UniSender\UniSender;
use common\components\providers\ETH;
use common\models\avatar\Currency;
use common\models\avatar\UserBill;
use common\models\avatar\UserBillDefault;
use common\models\HD;
use common\models\HDtown;
use common\models\school\School;
use common\models\UserAvatar;
use common\models\UserDigitalSign;
use common\models\UserDocument;
use common\services\Security\AES;
use cs\base\BaseController;
use cs\services\SitePath;
use cs\services\UploadFolderDispatcher;
use cs\services\VarDumper;
use cs\web\Exception;
use iAvatar777\services\FormAjax\DefaultFormAjax;
use Yii;
use yii\base\UserException;
use yii\bootstrap\ActiveForm;
use yii\db\Connection;
use yii\db\Query;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use yii\helpers\FileHelper;
use yii\web\Response;
use BitcoinPHP\BitcoinECDSA\BitcoinECDSA;

class CabinetSchoolDocumentsController extends \avatar\controllers\CabinetBaseController
{
    public function actions()
    {
        return [
            'secret' => [
                'class' => '\iAvatar777\services\FormAjax\DefaultFormAjax',
                'model' => '\avatar\models\forms\UserDocumentSecret',
            ],
            'sign' => [
                'class' => '\avatar\controllers\actions\DefaultAjax',
                'model' => '\avatar\models\forms\UserDocumentSign',
            ],
        ];
    }

    /**
     */
    public function actionIndex()
    {
        $school = School::get();

        return $this->render(['school' => $school]);
    }

    /**
     * Добавляет документ
     *
     * @return string|Response
     */
    public function actionAdd($id)
    {
        $school = School::findOne($id);
        $model = new \avatar\models\forms\SchoolDocument(['school_id' => $school->id]);

        if (Yii::$app->request->isPost) {
            if ($model->load(Yii::$app->request->post()) && $model->validate()) {
                return self::jsonSuccess($model->save());
            } else {
                return self::jsonErrorId(102, $model->getErrors102());
            }
        }

        return $this->render('add', [
            'model' => $model,
            'school' => $school,
        ]);
    }


    /**
     * Выдает документ на скачивание
     *
     * @return string
     * @throws \cs\web\Exception
     */
    public function actionDownload($id)
    {
        $doc = UserDocument::findOne($id);
        if (is_null($doc)) {
            throw new Exception('Не найден документ');
        }
        if ($doc->user_id != Yii::$app->user->id) {
            throw new Exception('Это не ваш документ');
        }
        $info = pathinfo($doc->file);

        return Yii::$app->response->sendContentAsFile(file_get_contents(Yii::getAlias('@webroot' . $doc->file)), $doc->id . '.'  . $info['extension']);
    }

    /**
     * Выдает документ на скачивание
     *
     * @return string
     * @throws \cs\web\Exception
     */
    public function actionDownloadPdf($id)
    {
        $doc = UserDocument::findOne($id);
        if (is_null($doc)) {
            throw new Exception('Не найден документ');
        }
        if ($doc->user_id != Yii::$app->user->id) {
            throw new Exception('Это не ваш документ');
        }
        $info = pathinfo($doc->file);

        require \Yii::getAlias('@common/components/mpdf60/mpdf.php');
        $pdfDriver = new \mPDF();
        $filePath = \Yii::getAlias('@avatar/views/cabinet-documents/pdf.php');
        $contentHtml = \Yii::$app->view->renderFile($filePath, ['document' => $doc]);
        $pdfDriver->WriteHTML($contentHtml);
        $contentPdf = $pdfDriver->Output('', 'S');

        return Yii::$app->response->sendContentAsFile($contentPdf, 'document' . '_' . $id . '.pdf', [
            'mimeType' => 'application/pdf',
        ]);
    }

    /**
     * Показывает документ
     *
     * @param $id
     * @return string
     * @throws Exception
     */
    public function actionView($id)
    {
        $document = UserDocument::findOne($id);
        $school = School::findOne($document->school_id);

        if (is_null($document)) {
            throw new Exception('Нет такого документа');
        }

        return $this->render('view', [
            'document' => $document,
            'school' => $school,
        ]);
    }
}
