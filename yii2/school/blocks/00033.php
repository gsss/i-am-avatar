<?php
/**
 * Created by PhpStorm.
 * User: Metatron
 * Date: 22.12.2018
 * Time: 7:03
 */
use yii\helpers\Url;

/** @var \yii\web\View $this */
/** @var string $data json */
/** @var \common\models\school\PageBlockContent $blockContent  */

$object = \yii\helpers\Json::decode($data);
\avatar\assets\Paralax::register($this);


$logo = '/images/logo830.png';
$serverNameString = 'www.i-am-avatar.com';
$serverNameImage = '/images/domain-name.png';




?>

<div class="parallax-window" data-parallax="scroll" data-image-src="<?= $object['image'] ?>"
     style="min-height: 650px; background: transparent;" id="<?= \avatar\controllers\CabinetSchoolPagesConstructorController::getBlockId($blockContent) ?>">


    <header style="
    position: absolute;
    top: 20px;
    left: 0;
    width: 100%;
    z-index: 3;
">
        <div class="container">
            <div id="navbar" class="collapse navbar-collapse">
                <div class="col-lg-10">
                    <p>
                        <?php $menu = require_once(Yii::getAlias('@avatar/views/blocks/topMenuData.php')); ?>
                        <?php if (is_array($menu)) { ?>
                            <?php foreach ($menu as $item) { ?>
                                <a href="<?= Url::to($item['route'])?>" style="margin-right: 20px;"><?= $item['label'] ?></a>
                            <?php } ?>
                        <?php } ?>
                    </p>
                </div>
                <div class="col-lg-2">
                    <ul class="nav navbar-nav navbar-right">
                        <?php if (\Yii::$app->user->isGuest) { ?>
                            <?php \avatar\widgets\ModalLogin\Asset::register($this); ?>
                            <li id="userBlockLi">
                                <!-- Split button -->
                                <div class="btn-group"  id="loginBarButton">
                                    <button type="button" class="btn btn-info" id="modalLogin"><i
                                                class="glyphicon glyphicon-user" style="padding-right: 5px;"></i><?= \Yii::t('c.DRNtMmSPgd', 'Войти') ?>
                                    </button>

                                    <button type="button" class="btn btn-info dropdown-toggle" data-toggle="dropdown"
                                            aria-expanded="false">
                                        <span class="caret"></span>
                                        <span class="sr-only">Toggle Dropdown</span>
                                    </button>

                                    <ul class="dropdown-menu" role="menu">
                                        <li><a href="<?= Url::to(['auth/password-recover']) ?>"><?= \Yii::t('c.DRNtMmSPgd', 'Напомнить пароль') ?></a></li>
                                        <li><a href="<?= Url::to(['auth/registration']) ?>"><?= \Yii::t('c.DRNtMmSPgd', 'Регистрация') ?></a></li>
                                        <li><a href="<?= Url::to(['qr/enter']) ?>">Активировать карту</a></li>
                                    </ul>
                                </div>
                            </li>
                        <?php } else { ?>
                            <?= $this->render('@avatar/views/layouts/user-menu') ?>
                        <?php } ?>

                    </ul>
                </div>
            </div>
        </div>
    </header>

    <?= $object['content'] ?>

</div>

<?php if (Yii::$app->deviceDetect->isMobile()) { ?>
<center>
    <div class="btn-group">
        <a href="/auth/login" class="btn btn-success"><i class="glyphicon glyphicon-user" style="padding-right: 5px;"></i>Войти
        </a>
        <a href="/auth/registration" class="btn btn-default"><i class="glyphicon glyphicon-user" style="padding-right: 5px;"></i>Регистрация
        </a>
        <a href="/auth/password-recover" class="btn btn-default"><i class="glyphicon glyphicon-asterisk" style="padding-right: 5px;"></i>Напомнить пароль
        </a>
    </div>
</center>
<?php } ?>
