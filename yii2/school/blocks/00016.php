<?php
/**
 * Created by PhpStorm.
 * User: Metatron
 * Date: 22.12.2018
 * Time: 7:03
 */

/** @var string $data json */

$object = \yii\helpers\Json::decode($data);
?>
<div class="container" id="<?= \avatar\controllers\CabinetSchoolPagesConstructorController::getBlockId($blockContent) ?>">
    <div class="col-lg-4 col-lg-offset-4">
        <p><img src="<?= $object['image'] ?>" class="img-circle" width="100%"></p>
        <h2 class="page-header text-center"><?= $object['header'] ?></h2>
        <p class="lead text-center"><?= $object['text'] ?></p>
    </div>
</div>

