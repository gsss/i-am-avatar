<?php
/**
 * Created by PhpStorm.
 * User: Metatron
 * Date: 22.12.2018
 * Time: 7:03
 */
use yii\helpers\Url;

/** @var \yii\web\View $this */
/** @var string $data json */
/** @var \common\models\school\PageBlockContent $blockContent */

$object = \yii\helpers\Json::decode($data);

$this->registerJs(<<<JS

$('.flowing-scroll').on( 'click', function(){ 
    var el = $(this);
    var dest = el.attr('href'); // получаем направление
    if(dest !== undefined && dest !== '') { // проверяем существование
        $('html').animate({ 
            scrollTop: $(dest).offset().top // прокручиваем страницу к требуемому элементу
        }, 500 // скорость прокрутки
        );
    }
    return false;
});

JS
);
?>
<nav class="navbar navbar-default navbar-fixed-top">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand flowing-scroll other-class" href="<?= $object['image']['href'] ?>" style="padding: 5px 10px 5px 10px;">
                <img src="<?= \common\widgets\FileUpload7\FileUpload::getFile($object['image']['image'], 'crop') ?>" width="40" data-toggle="tooltip" data-placement="bottom" title="На главную"  class="img-circle">
            </a>
        </div>
        <div id="navbar" class="collapse navbar-collapse">
            <ul class="nav navbar-nav">
                <?php $isFirst = true; ?>
                <?php foreach ($object['items'] as $item) { ?>
                    <?php $options = []; ?>
                    <?php if ($isFirst) $options['class'] = 'active'; ?>
                    <?= \yii\helpers\Html::tag('li', \yii\helpers\Html::a($item['name'], $item['href'], ['class' => 'flowing-scroll other-class'])) ?>
                <?php } ?>
            </ul>
            <ul class="nav navbar-nav navbar-right">
                <?php if (\Yii::$app->user->isGuest) { ?>
                    <?php \avatar\widgets\ModalLogin\Asset::register($this) ?>
                    <li id="userBlockLi">
                        <!-- Split button -->
                        <div class="btn-group" style="margin-top: 9px;" id="loginBarButton">
                            <button type="button" class="btn btn-default" id="modalLogin"><i
                                        class="glyphicon glyphicon-user" style="padding-right: 5px;"></i>Войти
                            </button>
                            <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown"
                                    aria-expanded="false">
                                <span class="caret"></span>
                                <span class="sr-only">Toggle Dropdown</span>
                            </button>
                            <ul class="dropdown-menu" role="menu">
                                <li><a href="<?= Url::to(['auth/password_recover']) ?>">Напомнить пароль</a></li>
                                <li><a href="<?= Url::to(['auth/registration']) ?>">Регистрация</a></li>
                            </ul>
                        </div>
                    </li>
                <?php } else { ?>
                    <?= $this->render('@school/views/layouts/user-menu', ['school' => $school]) ?>
                <?php } ?>
            </ul>
        </div>
    </div>
</nav>
