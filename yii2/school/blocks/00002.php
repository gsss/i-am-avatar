<?php
/**
 * Created by PhpStorm.
 * User: Metatron
 * Date: 22.12.2018
 * Time: 7:03
 */

/** @var string $data json */

$object = \yii\helpers\Json::decode($data);
?>
<div class="container" id="<?= \avatar\controllers\CabinetSchoolPagesConstructorController::getBlockId($blockContent) ?>">
    <div class="col-lg-8 col-lg-offset-2">
        <iframe width="100%" height="315" src="https://www.youtube.com/embed/<?= $object['id'] ?>" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
    </div>
</div>

