<?php
/**
 * Created by PhpStorm.
 * User: Metatron
 * Date: 22.12.2018
 * Time: 7:03
 */

/** @var string $data json */

$object = \yii\helpers\Json::decode($data);

$rows = 0;
$u    = (count($object['items']) + 1) / 2;
$rows = (int)$u;

?>
<div class="container" id="<?= \avatar\controllers\CabinetSchoolPagesConstructorController::getBlockId($blockContent) ?>">

    <div class="col-lg-12">
        <h1 class="text-center page-header"><?= $object['header'] ?></h1>
    </div>


    <?php for ($i = 0; $i < $rows; $i++) { ?>
        <div class="row" style="margin-bottom: 50px;">
            <?php $c = $i * 2; ?>
            <div class="col-lg-1">
                <p style="text-align: right;"><span class="glyphicon glyphicon-ok"></span></p>
            </div>
            <div class="col-lg-5">
                <p class="lead"><?= $object['items'][$c]['header'] ?></p>
                <p><?= $object['items'][$c]['text'] ?></p>
            </div>
            <?php $c++; ?>

            <?php $isDraw = true; ?>
            <?php if ($i == $rows - 1) { ?>
                <?php if ((count($object['items']) - $rows * 2) == -1) { ?>
                    <?php $isDraw = false; ?>
                <?php } ?>
            <?php } ?>
            <?php if ($isDraw) { ?>
                <div class="col-lg-1">
                    <p style="text-align: right;"><span class="glyphicon glyphicon-ok"></span></p>
                </div>
                <div class="col-lg-5">
                    <p class="lead"><?= $object['items'][$c]['header'] ?></p>
                    <p><?= $object['items'][$c]['text'] ?></p>
                </div>
            <?php } ?>
        </div>
    <?php } ?>
</div>

