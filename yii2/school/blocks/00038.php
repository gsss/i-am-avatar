<?php
/**
 * Created by PhpStorm.
 * User: Metatron
 * Date: 22.12.2018
 * Time: 7:03
 */
use yii\helpers\Url;

/** @var \yii\web\View $this */
/** @var string $data json */
/** @var \common\models\school\PageBlockContent $blockContent */

$object = \yii\helpers\Json::decode($data);

$image = \yii\helpers\ArrayHelper::getValue($object, 'image', '//www.i-am-avatar.com/images/avatarlogo1.png');
$school = \common\models\school\School::get();
if (!\cs\Application::isEmpty($school->image)) {
    $image = \iAvatar777\widgets\FileUpload7\FileUpload::getFile($school->image, 'crop');
}

?>
<nav class="navbar navbar-default navbar-fixed-top">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="/" style="padding: 5px 10px 5px 10px;">
                <img src="<?= $image ?>" width="40" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="Главная" class="img-circle">
            </a>
        </div>
        <div id="navbar" class="collapse navbar-collapse">
            <ul class="nav navbar-nav">
                <?= $this->render('@avatar/views/blocks/topMenu') ?>
            </ul>
            <ul class="nav navbar-nav navbar-right">
                <?php if (\Yii::$app->user->isGuest) { ?>
                    <?php \avatar\widgets\ModalLogin\Asset::register($this); ?>
                    <li id="userBlockLi">
                        <!-- Split button -->
                        <div class="btn-group" style="margin-top: 9px; opacity: 0.5;" id="loginBarButton">
                            <button type="button" class="btn btn-default" id="modalLogin"><i
                                        class="glyphicon glyphicon-user" style="padding-right: 5px;"></i><?= \Yii::t('c.DRNtMmSPgd', 'Войти') ?>
                            </button>
                            <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown"
                                    aria-expanded="false">
                                <span class="caret"></span>
                                <span class="sr-only">Toggle Dropdown</span>
                            </button>
                            <ul class="dropdown-menu" role="menu">
                                <li><a href="<?= Url::to(['auth/password-recover']) ?>"><?= \Yii::t('c.DRNtMmSPgd', 'Напомнить пароль') ?></a></li>
                                <li><a href="<?= Url::to(['auth/registration']) ?>"><?= \Yii::t('c.DRNtMmSPgd', 'Регистрация') ?></a></li>
                                <li><a href="<?= Url::to(['auth/repare-google-code']) ?>"><?= \Yii::t('c.DRNtMmSPgd', 'Восстановить Google code') ?></a></li>
                            </ul>
                        </div>
                    </li>
                <?php } else { ?>
                    <?= $this->render('@avatar/views/layouts/user-menu') ?>
                <?php } ?>
            </ul>
        </div>
    </div>
</nav>
