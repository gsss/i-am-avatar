<?php
/**
 * Created by PhpStorm.
 * User: Metatron
 * Date: 22.12.2018
 * Time: 7:03
 */

/** @var string $data json */

$object = \yii\helpers\Json::decode($data);

\avatar\assets\Paralax::register($this);

?>


<div class="parallax-window" data-parallax="scroll" data-image-src="<?= $object['imageBg'] ?>"
     style="min-height: 650px; background: transparent;" id="<?= \avatar\controllers\CabinetSchoolPagesConstructorController::getBlockId($blockContent) ?>">

    <div class="container">
        <div class="col-sm-4 col-sm-offset-4">
            <p style="padding-top: 50px; margin-bottom: 0px;"><img src="<?= $object['image'] ?>" class="img-circle" width="100%" style="text-shadow: 0px 10px 20px rgba(0, 0, 0, 0.5);color:white;"></p>
            <h2 class="page-header text-center" style="text-shadow: -1px 0 1px white, 0 -1px 1px white, 0 1px 1px white, 1px 0 1px white, 0 0 8px white, 0 0 8px white, 0 0 8px white, 2px 2px 3px black;"><?= $object['header'] ?></h2>
            <p class="lead text-center" style="text-shadow: -1px 0 1px white, 0 -1px 1px white, 0 1px 1px white, 1px 0 1px white, 0 0 8px white, 0 0 8px white, 0 0 8px white, 2px 2px 3px black;"><?= $object['text'] ?></p>
        </div>
    </div>

</div>


