<?php
/**
 * Created by PhpStorm.
 * User: Metatron
 * Date: 22.12.2018
 * Time: 7:03
 */

/** @var \yii\web\View $this */
/** @var string $data json */
/** @var \common\models\school\PageBlockContent $blockContent  */


?>
<?= $this->render('@school/blocks-edit/00018', ['data' => $data, 'blockContent' => $blockContent]) ?>
