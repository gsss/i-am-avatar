<?php


/** Кто ведет Вариант 4 */

/** @var \yii\web\View $this */
/** @var string $data json */
/** @var \common\models\school\PageBlockContent $blockContent */
/** @var \common\models\school\School $school */

$object = \yii\helpers\Json::decode($data);

$query = \common\models\blog\Article::find()->where(['school_id' => $school->id]);
$recordsCount = $query->count();
$itemsPerPage = $object['rows'] * 4 ;

$page = Yii::$app->request->get('page');

?>
<div class="container"
     id="<?= \avatar\controllers\CabinetSchoolPagesConstructorController::getBlockId($blockContent) ?>">

    <?php /** @var \common\models\blog\Article $item */ ?>
    <?php foreach ($query->limit(12)->offset(($page - 1) * 12)->orderBy(['id' => SORT_DESC])->all() as $item) { ?>
        <div class="col-lg-3">
            <p style="height: 100px;"><b><?= $item->name ?></b></p>
            <p>
                <a href="<?= $item->getLink() ?>">
                    <img src="<?= \iAvatar777\widgets\FileUpload7\FileUpload::getFile($item->image, 'crop') ?>" width="100%" class="thumbnail"/>
                </a>
            </p>
            <p style="color: #ccc;height: 50px;"><?= Yii::$app->formatter->asDate($item->created_at, 'php:d.m.Y') ?></p>
            <?= $item->name ?>
        </div>
    <?php } ?>

    <div class="col-lg-12" style="margin-bottom: 100px;">
        <hr>
        <?php
        $pagesInLine = 10;
        $pagesCount = (int)(($recordsCount + ($itemsPerPage - 1)) / $itemsPerPage);
        if ($pagesCount >= $pagesInLine) { // 11 страниц >= 10
            if ($page > 5 && ($page + 4 >= $pagesInLine)) {
                if ($page + 4 <= $pagesCount) {
                    $first = $page - 5;
                    $end = $page + 4;
                } else {
                    $end = $pagesCount;
                    $first = $pagesCount - 9;
                }
            } else {
                if ($page <= 5) {
                    $first = 1;
                    $end = 10;
                } else {
                    $end = $pagesCount;
                    $first = $pagesCount - 9;
                }
            }
        } else {
            $first = 1;
            $end = 1;
        }
        ?>

        <?php if ($pagesCount > 1) { ?>
            <ul class="pagination">
                <?php if ($page == 1) { ?>
                    <li class="prev disabled"><span>«</span></li>
                <?php } else { ?>
                    <li class="prev"><a href="?page=<?= $page - 1 ?>">«</a></li>
                <?php } ?>

                <?php for ($i = $first; $i <= $end; $i++) { ?>
                    <?php if ($i == $page) { ?>
                        <li class="active"><a href="?page=<?= $i ?>"><?= $i ?></a></li>
                    <?php } else { ?>
                        <li><a href="?page=<?= $i ?>"><?= $i ?></a></li>
                    <?php } ?>
                <?php } ?>

                <?php if ($page == $pagesCount) { ?>
                    <li class="next disabled"><span>»</span></li>
                <?php } else { ?>
                    <li class="next"><a href="?page=<?= $page + 1 ?>">»</a></li>
                <?php } ?>
            </ul>
        <?php } ?>
    </div>
</div>


