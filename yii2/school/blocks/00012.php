<?php
/**
 * Created by PhpStorm.
 * User: Metatron
 * Date: 22.12.2018
 * Time: 7:03
 */

/** @var string $data json */

$object = \yii\helpers\Json::decode($data);

$rows = 0;
$u    = (count($object['items']) + 2) / 3;
$rows = (int)$u;

?>
<div class="container" id="<?= \avatar\controllers\CabinetSchoolPagesConstructorController::getBlockId($blockContent) ?>">
    <h1 class="page-header text-center"><?= $object['header'] ?></h1>

    <?php for ($i = 0; $i < $rows; $i++) { ?>
        <div class="row" style="margin-bottom: 50px;">
            <?php $c = $i * 3; ?>
            <div class="col-lg-4">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title"><?= $object['items'][$c]['header'] ?></h3>
                    </div>
                    <div class="panel-body">
                        <?= $object['items'][$c]['text'] ?>
                    </div>
                </div>
            </div>

            <?php $c++; ?>

            <?php $isDraw = true; ?>
            <?php if ($i == $rows - 1) { ?>
                <?php if ((count($object['items']) - $rows * 3) == -2) { ?>
                    <?php $isDraw = false; ?>
                <?php } ?>
            <?php } ?>
            <?php if ($isDraw) { ?>
                <div class="col-lg-4">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h3 class="panel-title"><?= $object['items'][$c]['header'] ?></h3>
                        </div>
                        <div class="panel-body">
                            <?= $object['items'][$c]['text'] ?>
                        </div>
                    </div>
                </div>
            <?php } ?>

            <?php $c++; ?>

            <?php $isDraw = true; ?>
            <?php if ($i == $rows - 1) { ?>
                <?php if ((count($object['items']) - $rows * 3) < -1) { ?>
                    <?php $isDraw = false; ?>
                <?php } ?>
            <?php } ?>
            <?php if ($isDraw) { ?>
                <div class="col-lg-4">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h3 class="panel-title"><?= $object['items'][$c]['header'] ?></h3>
                        </div>
                        <div class="panel-body">
                            <?= $object['items'][$c]['text'] ?>
                        </div>
                    </div>
                </div>
            <?php } ?>
        </div>
    <?php } ?>


</div>

