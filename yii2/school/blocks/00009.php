<?php
/**
 * Created by PhpStorm.
 * User: Metatron
 * Date: 22.12.2018
 * Time: 7:03
 */

/** @var string $data json */

$object = \yii\helpers\Json::decode($data);

$rows = 0;
$u    = (count($object['items']) + 2) / 3;
$rows = (int)$u;

?>
<div class="container" id="<?= \avatar\controllers\CabinetSchoolPagesConstructorController::getBlockId($blockContent) ?>">

    <div class="col-lg-12">
        <h1 class="text-center page-header"><?= $object['header'] ?></h1>
    </div>


    <?php for ($i = 0; $i < $rows; $i++) { ?>
        <div class="row" style="margin-bottom: 50px;">
            <?php $c = $i * 3; ?>
            <div class="col-lg-4">
                <div style="
                <?= \yii\helpers\Html::cssStyleFromArray([
                    'margin-bottom' => '20px',
                    'height'        => '360px',
                    'background'    => 'url(\'' . $object['items'][$c]['image'] . '\') center center / cover no-repeat',
                    'max-width'     => '360px',
                ]) ?>"></div>
                <p class="text-center"><?= $object['items'][$c]['text'] ?></p>
            </div>

            <?php $c++; ?>

            <?php $isDraw = true; ?>
            <?php if ($i == $rows - 1) { ?>
                <?php if ((count($object['items']) - $rows * 9) == -2) { ?>
                    <?php $isDraw = false; ?>
                <?php } ?>
            <?php } ?>
            <?php if ($isDraw) { ?>
                <div class="col-lg-4">
                    <div style="
                <?= \yii\helpers\Html::cssStyleFromArray([
                        'margin-bottom' => '20px',
                        'height'        => '360px',
                        'background'    => 'url(\'' . $object['items'][$c]['image'] . '\') center center / cover no-repeat',
                        'max-width'     => '360px',
                    ]) ?>"></div>
                    <p class="text-center"><?= $object['items'][$c]['text'] ?></p>
                </div>
            <?php } ?>

            <?php $c++; ?>

            <?php $isDraw = true; ?>
            <?php if ($i == $rows - 1) { ?>
                <?php if ((count($object['items']) - $rows * 9) == -1) { ?>
                    <?php $isDraw = false; ?>
                <?php } ?>
            <?php } ?>
            <?php if ($isDraw) { ?>
                <div class="col-lg-4">
                    <div style="
                <?= \yii\helpers\Html::cssStyleFromArray([
                        'margin-bottom' => '20px',
                        'height'        => '360px',
                        'background'    => 'url(\'' . $object['items'][$c]['image'] . '\') center center / cover no-repeat',
                        'max-width'     => '360px',
                    ]) ?>"></div>
                    <p class="text-center"><?= $object['items'][$c]['text'] ?></p>
                </div>
            <?php } ?>
        </div>
    <?php } ?>
</div>

