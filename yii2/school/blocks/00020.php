<?php
/**
 * Created by PhpStorm.
 * User: Metatron
 * Date: 22.12.2018
 * Time: 7:03
 */

/** @var \yii\web\View $this */
/** @var string $data json */
/** @var \common\models\school\PageBlockContent $blockContent */

$object = \yii\helpers\Json::decode($data);

$this->registerJs(<<<JS

$('.flowing-scroll').on( 'click', function(){ 
    var el = $(this);
    var dest = el.attr('href'); // получаем направление
    if(dest !== undefined && dest !== '') { // проверяем существование
        $('html').animate({ 
            scrollTop: $(dest).offset().top // прокручиваем страницу к требуемому элементу
        }, 500 // скорость прокрутки
        );
    }
    return false;
});

JS
);
?>
<nav class="navbar navbar-default navbar-fixed-top">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand flowing-scroll other-class" href="<?= $object['image']['href'] ?>" style="padding: 5px 10px 5px 10px;">
                <img src="<?= $object['image']['image'] ?>" width="40" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="Главная">
            </a>
        </div>
        <div id="navbar" class="collapse navbar-collapse">
            <ul class="nav navbar-nav">
                <?php $isFirst = true; ?>
                <?php foreach ($object['items'] as $item) { ?>
                    <?php $options = []; ?>
                    <?php if ($isFirst) $options['class'] = 'active'; ?>
                    <?= \yii\helpers\Html::tag('li', \yii\helpers\Html::a($item['name'], $item['href'], ['class' => 'flowing-scroll other-class'])) ?>
                <?php } ?>
            </ul>
        </div>
    </div>
</nav>
