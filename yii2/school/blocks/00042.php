<?php
/**
 *
 */

/** @var \yii\web\View $this */
/** @var string $data json */
/** @var \common\models\school\PageBlockContent $blockContent */

$object = \yii\helpers\Json::decode($data);

\avatar\assets\Notify::register($this);

$this->registerJs(<<<JS
var n = new Noty({
    text: 'Этот сайт использует Cookie подтвердите свое согласие.',
    theme: 'nest',
    type: 'success',
    layout: 'bottomLeft',
    buttons: [
        Noty.button('Да', 'btn btn-success', function () {
            console.log('button 1 clicked');
            n.close();
        })
    ]
});
n.show();
JS
);
?>

