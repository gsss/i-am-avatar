<?php
/**
 * Created by PhpStorm.
 * User: Metatron
 * Date: 22.12.2018
 * Time: 7:03
 */

/** @var \yii\web\View $this */
/** @var string $data json */
/** @var \common\models\school\PageBlockContent $blockContent */

$object = \yii\helpers\Json::decode($data);
$rows = count($object['items']);

\avatar\assets\Notify::register($this);

$this->registerJs(<<<JS

// снимает ошибочные признаки поля при фокусе
$('.text-input').on('focus', function() {
    var o = $(this);
    var p = o.parent();
    if (p.hasClass('input-group')) {
        p = p.parent();
    }
    p.removeClass('has-error');
    p.find('p.help-block-error').hide();
});

var functionButtonSendEth = function() {
    var b = $(this);
    b.off('click');
    var buttonText = b.html();
    b.html($('<i>', {class: 'fa fa-spinner fa-spin fa-fw'}));
    b.attr('disabled', 'disabled');
    var formId = b.data('form'); 
    var id = b.data('id'); 
    
    ajaxJson({
        url: '/pages/form',
        data: $('#' + formId).serializeArray(),
        success: function(ret) {
            b.on('click', functionButtonSendEth);
            b.html(buttonText);
            b.removeAttr('disabled');
            if (ret.result == 1) {
                $('#collapse_alert_' + id).find('p.text2019').html('Пройдите на свою почту и подтвердите свою регистрацию. Мы вам выслали письмо от iAvatar.');
            }
            if (ret.result == 2) {
                $('#collapse_alert_' + id).find('p.text2019').html('Вы успешно подписались на событие.');
            }
            if (ret.result == 3) {
                $('#collapse_alert_' + id).find('p.text2019').html('Вы успешно подписались на событие');
            }
            $('#collapse_form_' + id).collapse('hide');
            $('#collapse_alert_' + id).collapse('show');
            
        },
        errorScript: function(ret) {
            switch (ret.id) {
                case 101:
                    new Noty({
                        timeout: 3000,
                        theme: 'sunset',
                        type: 'error',
                        layout: 'bottomLeft',
                        text: 'Произошла ошибка'
                    }).show();
                    break;
                case 102:
                    var f = $('#' + formId);
                    $.each(ret.data,function(i,v) {
                        var name = v.name;
                        var value = v.value;
                        var t = f.find('.field-' + name);
                        t.addClass('has-error');
                        t.find('p.help-block-error').html(value.join('<br>')).show();
                    });

                    break;
            }
            b.on('click', functionButtonSendEth);
            b.html(buttonText);
            b.removeAttr('disabled');
        },
        error: function(e) {
            b.on('click', functionButtonSendEth);
            b.html(buttonText);
            b.removeAttr('disabled');
            new Noty({
                timeout: 3000,
                theme: 'sunset',
                type: 'error',
                layout: 'bottomLeft',
                text: 'Произошла ошибка'
            }).show();
        }
    });
};
$('.buttonSend').click(functionButtonSendEth);

JS
);
?>
<div class="container"
     id="<?= \avatar\controllers\CabinetSchoolPagesConstructorController::getBlockId($blockContent) ?>">

    <div class="col-lg-12">
        <h1 class="text-center page-header"><?= $object['header'] ?></h1>
    </div>

    <div class="col-lg-12">
        <div class="row" style="margin-bottom: 50px;">
            <div class="col-lg-6 col-lg-offset-3">

                <div class="collapse in" id="collapse_form_<?= $blockContent->id ?>">
                    <form id="form_<?= $blockContent->id ?>">
                        <input type="hidden" name="_csrf" value="<?= Yii::$app->request->csrfToken ?>">
                        <input type="hidden" name="block_id" value="<?= $blockContent->id ?>"/>
                        <?php for ($i = 0; $i < $rows; $i++) { ?>
                            <div class="form-group required field-<?= $object['items'][$i]['name'] ?>">
                                <label class="control-label"
                                ><?= $object['items'][$i]['placeholder'] ?></label>
                                <input type="text"
                                       id="field_<?= $blockContent->id ?>_<?= $object['items'][$i]['name'] ?>"
                                       class="form-control text-input" name="<?= $object['items'][$i]['name'] ?>"
                                       aria-required="true"
                                       aria-invalid="true">
                                <p class="help-block help-block-error"></p>
                            </div>
                        <?php } ?>
                        <hr>
                        <div class="form-group">
                            <button type="submit"
                                    class="btn btn-success buttonSend"
                                    name="contact-button"
                                    style="width:100%"
                                    data-id="<?= $blockContent->id ?>"
                                    data-form="form_<?= $blockContent->id ?>"
                            ><?= $object['button']['text'] ?></button>
                        </div>
                    </form>
                </div>
                <div class="collapse" id="collapse_alert_<?= $blockContent->id ?>">
                    <p class="text-center">
                        <img src="//www.i-am-avatar.com/images/mail.png" width="100">
                    </p>
                    <p class="text-center lead text2019">
                        Пройдите на свою почту и подтвердите свою регистрацию.<br>Мы вам выслали письмо от iAvatar.
                    </p>
                </div>
            </div>
        </div>
    </div>
</div>

