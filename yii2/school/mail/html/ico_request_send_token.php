<?php
/**
 * Created by PhpStorm.
 * User: god
 * Date: 12.08.2017
 * Time: 2:35
 */
use avatar\modules\ETH\ServiceEtherScan;
use yii\helpers\Html;
use yii\helpers\Url;

/**
 * @var $request \common\models\investment\IcoRequest
 */
/**
 * @var $txid
 */
/**
 * @var $user \common\models\UserAvatar
 */

$provider = new ServiceEtherScan();
$url = $provider->getUrl('/tx/' . $txid);

?>
<p>Вам были отправлены токены.</p>
<p>Идентификатор транзакции: <?= Html::a(substr($txid, 0, 8) . ' ...', $url) ?>.</p>
<p><?= Html::a('Заказ', Url::to(['cabinet-ico/request', 'id' => $request->id], true)) ?></p>

