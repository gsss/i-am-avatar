<?php
/**
 * @var \common\models\comment\Comment $comment
 * @var \common\models\Anketa $item
 */

$url = \yii\helpers\Url::to(['admin-anketa/view', 'id' => $item->id], true);
?>

<p>Добавлен новый коментарий к анкете</p>
<p>Идентификатор анкеты: <?= $comment->getList()->object_id ?></p>
<p>Время коментария: <?= Yii::$app->formatter->asDatetime($comment->created_at) ?></p>
<p>Ссылка на анкету: <a href="<?= $url ?>" target="_blank"><?= $url ?></a></p>

<hr>
<p><?= $comment->getHtml() ?></p>
