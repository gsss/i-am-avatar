<?php
/**
 * @var \common\models\comment\Comment $comment
 * @var \common\models\BlogItem $blog
 */

$url = \yii\helpers\Url::to(['blog/item', 'id' => $blog->id], true);
?>

<p>Добавлен новый коментарий к статье блога</p>
<p>Идентификатор статьи: <?= $blog->id ?></p>
<p>Название статьи: <?= $blog->name ?></p>
<p>Автор: <?= \yii\helpers\Html::encode($comment->getUser()->getName2()) ?></p>
<p>Время коментария: <?= Yii::$app->formatter->asDatetime($comment->created_at) ?></p>
<p>Ссылка на статью: <a href="<?= $url ?>" target="_blank"><?= $url ?></a></p>

<hr>
<p><?= $comment->getHtml() ?></p>
