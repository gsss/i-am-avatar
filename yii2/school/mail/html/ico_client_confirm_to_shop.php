<?php
/**
 * Created by PhpStorm.
 * User: god
 * Date: 12.08.2017
 * Time: 2:35
 */

/**
 * @var $request \common\models\investment\IcoRequest
 */

/**
 * @var $txid string
 */

?>
<p>Клиент подтвердил оплату по заказу #<?= $request->id ?></p>
<p>Платежная система: <?= $request->getBilling()->getPaySystem()->title ?></p>
<p>Кол-во заказанных токенов: <?= $request->tokens ?></p>
<p>К оплате: <?= Yii::$app->formatter->asDecimal($request->getBilling()->sum_after, $request->getBilling()->getPaySystem()->getCurrencyObject()->decimals) ?> <?= $request->getBilling()->getPaySystem()->getCurrencyObject()->code ?></p>
<p>Транзакция: <?= $txid ?>.</p>