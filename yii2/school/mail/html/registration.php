<?php
/**
 * @var $url string
 * @var $datetime string
 * @var $user \app\models\user
 */
/** @var $school_subscribe_image    string                              картинка с относительным путем или полным */

if (!isset($school_subscribe_image)) $school_subscribe_image = '/images/mail/new-lesson/logo_v2.png';
if (\yii\helpers\StringHelper::startsWith($school_subscribe_image, '/')) $school_subscribe_image = \yii\helpers\Url::to($school_subscribe_image, true);

?>


<p style="text-align: center">
    <img src="<?= $school_subscribe_image ?>" width="200">
</p>
<p>Вы успешно прошли регистрацию.</p>
<p>Чтобы подтвердить свою регистрацию, нажмите кнопку ниже.</p>
<p><a href="<?= $url ?>" style="
            text-decoration: none;
            color: #fff;
            background-color: #337ab7;
            border-color: #2e6da4;
             display: inline-block;
            padding: 6px 12px;
            margin-bottom: 0;
            font-size: 14px;
            font-weight: normal;
            line-height: 1.42857143;
            text-align: center;
            white-space: nowrap;
            vertical-align: middle;
            -ms-touch-action: manipulation;
            touch-action: manipulation;
            cursor: pointer;
            -webkit-user-select: none;
            -moz-user-select: none;
            -ms-user-select: none;
            user-select: none;
            background-image: none;
            border: 1px solid transparent;
            border-radius: 4px;
">Ссылка</a></p>

<p>Данная ссылка будет действительна семь дней.</p>