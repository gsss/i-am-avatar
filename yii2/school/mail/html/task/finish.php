<?php
/**
 * Created by PhpStorm.
 * User: Ra-m-ha
 * Date: 06.01.2019
 * Time: 19:08
 */

/** @var \common\models\task\Task $task */
/** @var $school_subscribe_image    string                              картинка с относительным путем или полным */

if (!isset($school_subscribe_image)) $school_subscribe_image = 'https://cloud1.cloud999.ru/upload/cloud/16227/07865_oWaEF7Ue6Q_crop.png';
if (\yii\helpers\StringHelper::startsWith($school_subscribe_image, '/')) $school_subscribe_image = \yii\helpers\Url::to($school_subscribe_image, true);

$url = \yii\helpers\Url::to(['cabinet-task-list/view', 'id' => $task->id], true);
?>


<p style="text-align: center">
    <img src="<?= $school_subscribe_image ?>" width="200">
</p>
<p>Исполнитель выполнил задачу.</p>
<p>ID: <?= $task->id ?>.</p>
<p>Задача: "<?= $task->name ?>".</p>
<p>Задачу можно посмотреть по ссылке <a href="<?= $url ?>"><?= $url ?></a></p>
