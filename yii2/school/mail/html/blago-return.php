<?php
/**
 * Created by PhpStorm.
 * User: Ra-m-ha
 * Date: 06.01.2019
 * Time: 19:08
 */

/** @var \common\models\UserAvatar $user */
/** @var \iAvatar777\services\Processing\Transaction $transaction */

$wTo = \iAvatar777\services\Processing\Wallet::findOne($transaction->to);
$c = \iAvatar777\services\Processing\Currency::findOne($wTo->currency_id);
?>


<p>Поздравляем. Вы успешно получили <?= Yii::$app->formatter->asDecimal(
        \iAvatar777\services\Processing\Currency::getValueFromAtom($transaction->amount, $c->id), $c->decimals
    ) ?> <?= $c->code ?>.</p>
