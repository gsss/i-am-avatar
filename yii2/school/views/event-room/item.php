<?php

use common\widgets\FileUpload3\FileUpload;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */
/* @var $kurs \common\models\school\Kurs */
/* @var $potok \common\models\school\Potok */
/* @var $event \common\models\school\Event */

$this->title = $potok->name;

$data = \yii\helpers\Json::decode($event->config);

?>
<div class="container">

    <div class="col-lg-12">
        <h1 class="page-header text-center"><?= Html::encode($this->title) ?></h1>
    </div>
    <div class="col-lg-4 col-lg-offset-4">

        <p><?= \Yii::t('c.Q2vFnrwWB1', 'Пожалуйста заполните следующие поля для входа') ?>:</p>

        <?php $form = ActiveForm::begin(['id' => 'formLogin']); ?>
        <?php $model = new \school\models\forms\RoomLogin(); ?>

        <?= $form->field($model, 'name', ['inputOptions' => ['placeholder' => 'Имя']])->label('Имя', ['class' => 'hide']) ?>

        <hr>
        <div class="form-group">
            <?php
            $this->registerJs(<<<JS
$('.buttonLogin').click(function(e) {
    ajaxJson({
        url: '/event-room/login',
        data: $('#formLogin').serializeArray(),
        success: function(ret) {
            
        }
    });
});
JS
)
            ?>
            <?= Html::button(Yii::t('c.Q2vFnrwWB1', 'Вход'), [
                'class' => 'btn btn-success buttonLogin',
                'name'  => 'login-button',
                'style' => 'width:100%',
            ]) ?>
        </div>

        <?php ActiveForm::end(); ?>
    </div>

</div>

