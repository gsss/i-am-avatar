<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;
use app\models\UnionCategory;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \avatar\models\forms\ProfilePhone */

$this->title = 'Телефон';
?>
<div class="container">
    <div class="col-lg-12">
        <h1 class="page-header"><?= Html::encode($this->title) ?></h1>



    </div>
    <div class="col-lg-8">
        <?php if (Yii::$app->user->identity->phone_is_confirm == 1): ?>

            <?php
            $this->registerJs(<<<JS
$('.buttonReset').click(function() {
    ajaxJson({
        url: '/cabinet-profile-phone/reset',
        success: function(ret) {
            window.location.reload();
        }
    });
});
JS
            );
            ?>
            <?php $form = ActiveForm::begin([
                'layout'      => 'horizontal',
                'enableClientScript'      => false,
                'fieldConfig' => [
                    'template'             => "{label}\n{beginWrapper}\n{input}\n{hint}\n{error}\n{endWrapper}",
                    'horizontalCssClasses' => [
                        'label'   => 'col-sm-5',
                        'offset'  => 'col-sm-offset-4',
                        'wrapper' => 'col-sm-7',
                        'error'   => '',
                        'hint'    => '',
                    ],
                ],
            ]); ?>
            <?= $form->field($model, 'phone', ['inputOptions' => ['disabled' => 'disabled']]) ?>

            <div class="form-group groupVerified" style="margin-bottom: 50px;">
                <label class="control-label col-sm-5"></label>
                <div class="col-sm-7">
                    <span class="label label-success">Подтвержден</span>
                </div>
            </div>

            <?= Html::button('Изменить', [
                'class' => 'btn btn-default buttonReset',
                'style' => 'width:100%',
            ]) ?>
            <?php ActiveForm::end(); ?>

        <?php else: ?>
            <?php
            $this->registerJs(<<<JS
var timer = 20;
$('.buttonSendFirst').click(function(e) {
    var phone = $('#profilephone-phone').val();
    if (phone.indexOf('_') >= 0) {
        alert('Нужно ввести телефон полностью, только после этог можно запросить отправку СМС');
        return;
    }

    ajaxJson({
        url: '/cabinet-profile-phone/send',
        data: {
            phone: $('#profilephone-phone').val()
        },
        success: function(ret) {
            $('.groupVerified').hide();
            $('.groupSend').removeClass('hide');
            $('.field-profilephone-sms').removeClass('hide');
            $('.buttonCheck').removeClass('hide');
            $('.buttonSendFirst').hide();
            $('#spanTimer').html(timer);
            setInterval(function() {
                timer--;
                if (timer == 0) {
                    $('.alertCounter').hide();
                    $('.buttonSendMore').removeClass('hide').show();
                } else {
                    $('#spanTimer').html(timer);
                    return false;
                }
            }, 1000);
        }
    });
});
$('.buttonCheck').click(function() {
    ajaxJson({
        url: '/cabinet-profile-phone/check',
        data: {
            phone: $('#profilephone-phone').val(),
            sms: $('#profilephone-sms').val()
        },
        success: function(ret) {
            ajaxJson({
                url: '/koop/is-sign',
                success: function(ret) {
                    console.log(ret);
                    if (ret.is_sign == 1) {
                        $('#modalInfo').on('hidden.bs.modal', function() {
                            window.location = '/koop/pay';
                        }).modal();
                    } else {
                        $('#modalInfo').on('hidden.bs.modal', function() {
                            window.location = '/koop/sign2';
                        }).modal();
                    }
                }
            });
        },
        errorScript: function(ret) {
            if (ret.id == 101) {
                $('.field-profilephone-sms .help-block-error').html(ret.data);
                $('.field-profilephone-sms .help-block-error').show();
                $('.field-profilephone-sms').addClass('has-error');
            }
        }
    })
});
$('.buttonSendMore').click(function() {
    timer = 20;
    ajaxJson({
        url: '/cabinet-profile-phone/send-more',
        data: {
            phone: $('#profilephone-phone').val()
        },
        success: function(ret) {
            $('.field-profilephone-sms').removeClass('has-error');
            $('.field-profilephone-sms .help-block-error').hide();
            $('.buttonSendMore').hide();
            $('.alertCounter').show();
            $('#spanTimer').html(timer);
        }
    });
});
$('#profilephone-sms').on('focus', function(e){
    $('.field-profilephone-sms').removeClass('has-error');
    $('.field-profilephone-sms .help-block-error').hide();
});
JS
            );
            ?>
            <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title" id="myModalLabel">Успешно</h4>
                        </div>
                        <div class="modal-body">
                            Телефон успешно подтвержден и сохранен.
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
                        </div>
                    </div>
                </div>
            </div>
            <?php $form = ActiveForm::begin([
                'layout'      => 'horizontal',
                'enableClientScript'      => false,
                'fieldConfig' => [
                    'template'             => "{label}\n{beginWrapper}\n{input}\n{hint}\n{error}\n{endWrapper}",
                    'horizontalCssClasses' => [
                        'label'   => 'col-sm-5',
                        'offset'  => 'col-sm-offset-4',
                        'wrapper' => 'col-sm-7',
                        'error'   => '',
                        'hint'    => '',
                    ],
                ],
            ]); ?>
            <div class="form-group">
                <div class="col-sm-12">
                    <p>Введите свой номер телефона в поле и нажмите кнопку "Выслать СМС". Вам на ваш указанный телефон прийдет СМС с кодом подтверждения и вам ее нужно будет указать в поле "Код из СМС"</p>
                </div>
            </div>

            <?= $form->field($model, 'phone')->widget('yii\widgets\MaskedInput', [
            'mask' => '+7(999)-999-99-99'
        ]) ?>

            <?php if (Yii::$app->user->identity->phone_is_confirm) { ?>
                <div class="form-group groupVerified" style="margin-bottom: 50px;">
                    <label class="control-label col-sm-5"></label>
                    <div class="col-sm-7">
                        <span class="label label-success">Подтвержден</span>
                    </div>
                </div>
            <?php } ?>

            <div class="form-group field-profilephone-sms hide">
                <label class="control-label col-sm-5" for="profilephone-sms">Код из СМС</label>
                <div class="col-sm-7">
                    <input type="text" id="profilephone-sms" class="form-control"
                           value="" size="4"
                    >
                    <div class="help-block help-block-error"></div>
                </div>
            </div>


            <div class="form-group groupSend hide">
                <div class="col-sm-5">
                    <p class="alert alert-success">СМС выслано на указанный телефон</p>
                </div>
                <div class="col-sm-7 ">
                    <p class="alert alert-success alertCounter">Если СМС не пришло, через <span id="spanTimer">20</span> сек можно запросить еще одну СМС, код остается прежним</p>
                    <?= Html::button('Выслать еще раз', [
                        'class' => 'btn btn-default buttonSendMore hide',
                        'style' => 'width:100%',
                    ]) ?>
                </div>
            </div>

            <?= Html::button('Выслать СМС', [
                'class' => 'btn btn-default buttonSendFirst',
                'style' => 'width:100%',
            ]) ?>
            <?= Html::button('Подтвердить', [
                'class' => 'btn btn-default hide buttonCheck',
                'style' => 'width:100%',
            ]) ?>
            <?php ActiveForm::end(); ?>
        <?php endif; ?>
    </div>
</div>

<div class="modal fade" id="modalInfo" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Информация</h4>
            </div>
            <div class="modal-body">
                Успешно!
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
            </div>
        </div>
    </div>
</div>