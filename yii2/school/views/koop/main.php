<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;
use app\models\UnionCategory;

/* @var $this yii\web\View */
/* @var $item \common\models\UserEnter */
/* @var $school \common\models\school\School */

$this->title = 'Главная - кооператива';
?>

<div class="container">
    <div class="col-lg-12">
        <h1 class="page-header"><?= Html::encode($this->title) ?></h1>
    </div>
    <div class="col-lg-12">
        <ul>
            <li>Внести взнос</li>
            <ul>
                <li>Вступительный</li>
                <li>Членский</li>
                <ul>
                    <li>Деньгами</li>
                    <li>Имуществом</li>
                </ul>
                <li>Паевой</li>
                <ul>
                    <li>Деньгами</li>
                    <li>Имуществом</li>
                </ul>
            </ul>

            <li>Вернуть взнос</li>
            <ul>
                <li>Деньгами</li>
                <li>Имуществом</li>
                <ul>
                    <li>Пайщика</li>
                    <li>Партнера</li>
                </ul>
            </ul>
        </ul>
    </div>

</div>