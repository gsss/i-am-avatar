<?php
/** @var \yii\web\View $this */

$this->title = 'Кооператив';

$UserEnter = \avatar\models\UserEnter::findOne(['user_id' => Yii::$app->user->id]);

$status = 0;
?>


<div class="container">
    <div class="col-lg-12">
        <h1 class="page-header"><?= \yii\helpers\Html::encode($this->title) ?></h1>
    </div>

    <div class="col-lg-12">

        <?php if (is_null($UserEnter)) { ?>
            <?php $status = 1; ?>
        <?php } else { ?>
            <?php if (\cs\Application::isEmpty($UserEnter->signature1)) { ?>
                <?php $status = 3; ?>
            <?php } else { ?>
                <?php if (\cs\Application::isEmpty($UserEnter->signature2)) { ?>
                    <?php $status = 4; ?>
                <?php } else { ?>
                    <?php $status = 5; ?>
                <?php } ?>
            <?php } ?>
        <?php } ?>
        <?php
        $statusArray = [
            1 => false,
            2 => false,
            3 => false,
            4 => false,
            5 => false,
        ];
        $statusArray[$status] = true;
        ?>
        <?= \yii\bootstrap\Tabs::widget([
                'options' => ['style' => 'margin-bottom: 30px;'],
                'items' => [
                    [
                        'label'  => 'Заявление/регистрация',
                        'active' => $statusArray[1],
                    ],
                    [
                        'label'  => 'Проверка и подпись 1',
                        'active' => $statusArray[3],
                    ],
                    [
                        'label'  => 'Подпись 2',
                        'active' => $statusArray[4],
                    ],
                    [
                        'label'  => 'Внести взнос',
                        'active' => $statusArray[5],
                    ],
                ],
        ]) ?>

        <?php if ($status == 1) { ?>
            <p><a href="registration" class="btn btn-success">Заявление/регистрация</a></p>
        <?php } ?>

        <?php if ($status == 2) { ?>
            <?php if (\cs\Application::isEmpty($UserEnter->file)) { ?>
                <p><a href="video" class="btn btn-success">Указать файл</a></p>
            <?php } else {  ?>
                <p><a href="video" class="btn btn-success">Указать файл</a></p>
                <p>Файл указан ожидайте загрузки файла со стороны кооператива</p>
            <?php }  ?>
        <?php } ?>

        <?php if ($status == 3) { ?>
            <p>Ожидайте подтверждения заявки</p>
        <?php } ?>

        <?php if ($status == 4) { ?>
            <p><a href="sign" class="btn btn-success">Подпись</a></p>
        <?php } ?>

        <?php if ($status == 5) { ?>
            <p><a href="pay" class="btn btn-success">Внести взнос</a></p>
        <?php } ?>
    </div>
</div>

<div class="modal fade" id="modalInfo" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="myModalLabel">Информация</h4>
            </div>
            <div class="modal-body">
                Успешно!
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
            </div>
        </div>
    </div>
</div>