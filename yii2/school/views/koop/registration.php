<?php
/** @var \school\models\validate\UserEnter $model */

$this->title = 'Регистрация в кооперативе';

?>


<div class="container">
    <div class="col-lg-12">
        <h1 class="page-header"><?= \yii\helpers\Html::encode($this->title) ?></h1>
    </div>

    <div class="col-lg-12">

        <?php $form = \iAvatar777\services\FormAjax\ActiveForm::begin([
            'model'      => $model,
            'success' => <<<JS
function (ret) {
    $('#modalInfo').on('hidden.bs.modal', function() {
        if (ret.is_phone == 0) {
            window.location = '/koop/phone';
        } else {
            if (ret.is_sign == 0) {
                window.location = '/koop/sign';
            } else {
                window.location = '/koop/pay';
            }
        }
    }).modal();
}
JS
            ,
        ]); ?>

        <div class="row">
            <div class="col-lg-6">
                <h3 class="page-header">Человек</h3>
                <?= $form->field($model, 'name_first') ?>
                <?= $form->field($model, 'name_middle') ?>
                <?= $form->field($model, 'name_last') ?>
                <?= $form->field($model, 'address_pochta') ?>
                <?= $form->field($model, 'inn') ?>
                <?= $form->field($model, 'snils') ?>
                <?= $form->field($model, 'grajd') ?>
                <?= $form->field($model, 'accept_rules') ?>
            </div>
            <div class="col-lg-6">
                <h3 class="page-header">Паспорт РФ</h3>
                <?= $form->field($model, 'passport_scan1') ?>
                <?= $form->field($model, 'passport_scan2') ?>
                <?= $form->field($model, 'passport_ser') ?>
                <?= $form->field($model, 'passport_number') ?>
                <?= $form->field($model, 'passport_vidan_kem') ?>
                <?= $form->field($model, 'passport_vidan_date') ?>
                <?= $form->field($model, 'passport_vidan_num') ?>
                <?= $form->field($model, 'passport_born_date') ?>
                <?= $form->field($model, 'passport_born_place') ?>
                <?= $form->field($model, 'passport_reg_address') ?>
            </div>
        </div>
        <hr>

        <?php  \iAvatar777\services\FormAjax\ActiveForm::end(); ?>

    </div>
</div>

<div class="modal fade" id="modalInfo1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="myModalLabel">Информация</h4>
            </div>
            <div class="modal-body">
                <p>В Совет потребительского кооператива «Наименование»</p>
                <p>заявление</p>

                <p>Прошу принять меня в члены потребительского кооператива «Наименование». Обязуюсь соблюдать требования Устава кооператива и исполнять все решения органов управления, своевременно вносить взносы, предусмотренные Уставом и внутренними документами кооператива. С Уставом, Положением о взносах и фондах Кооператива ознакомлен(-а).</p>

                <p>В соответствии с требованиями Закона №152-ФЗ от 27.07.2006г. "О персональных данных", выражаю своё согласие на хранение и обработку Кооперативом моих персональных данных, указанных в настоящем заявлении и других документах, переданных в Кооператив, в т.ч. в автоматизированных системах (базах данных) Кооператива, на срок членства в Кооперативе и в течение 3-х лет с момента прекращения членства в нём.</p>
                <p>Достоверность представляемых мною данных подтверждаю, обязуюсь своевременно сообщать об изменении указанных данных. Выражаю свое согласие на получение от Кооператива sms-сообщений на указанный контактный мобильный телефон, а так же сообщений на указанный адрес электронной почты. Выражаю свое согласие на то, что посланное от имени органов управления Кооператива sms-сообщение или сообщение по эл.почте признается надлежащим уведомлением.</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modalInfo" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Информация</h4>
            </div>
            <div class="modal-body">
                Благодарим вас за заполнение подробной анкеты!
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
            </div>
        </div>
    </div>
</div>
