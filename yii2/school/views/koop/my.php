<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;
use app\models\UnionCategory;

/* @var $this yii\web\View */
/* @var $item \common\models\UserEnter */
/* @var $school \common\models\school\School */

$this->title = 'Моя заявка в кооператив';
?>

<div class="container">
    <div class="col-lg-12">
        <h1 class="page-header"><?= Html::encode($this->title) ?></h1>
    </div>
    <div class="col-lg-6">
        <h2 class="page-header">Человек</h2>
        <?= \yii\widgets\DetailView::widget([
            'model'      => $item,
            'attributes' => [
                [
                    'label'          => 'ID',
                    'value'          => $item->id,
                    'captionOptions' => ['style' => 'width: 40%'],
                ],
                [
                    'label' => 'Имя',
                    'value' => $item->name_first,
                ],
                [
                    'label' => 'Фамилия',
                    'value' => $item->name_last,
                ],
                [
                    'label' => 'Отчество',
                    'value' => $item->name_middle,
                ],
                [
                    'label' => 'Телефон',
                    'value' => $item->phone,
                ],
                [
                    'label' => 'Почтовый адрес',
                    'value' => $item->address_pochta,
                ],
            ],
        ]) ?>
        <div class="row">
            <?php if (!\cs\Application::isEmpty($item->signature1)) { ?>
                <div class="col-lg-2">
                    <?php $user = \common\models\UserAvatar::findOne($item->user1_id) ?>
                    <img src="<?= $user->getAvatar() ?>" class="img-circle" width="100%" data-toggle="tooltip" title="<?= $user->getName2() ?>"><br>
                    <img src="https://cloud1.cloud999.ru/upload/cloud/15898/42366_co3C4jVul6.jpg" class="img-circle" width="50" data-toggle="tooltip" title="Подписано ЭЦП" style="margin-top: 30px;">
                </div>
            <?php } ?>
            <?php if (!\cs\Application::isEmpty($item->signature2)) { ?>
                <div class="col-lg-2">
                    <?php $user = \common\models\UserAvatar::findOne($item->user2_id) ?>
                    <img src="<?= $user->getAvatar() ?>" class="img-circle" width="100%" data-toggle="tooltip" title="<?= $user->getName2() ?>"><br>
                    <img src="https://cloud1.cloud999.ru/upload/cloud/15898/42366_co3C4jVul6.jpg" class="img-circle" width="50" data-toggle="tooltip" title="Подписано ЭЦП" style="margin-top: 30px;">
                </div>
            <?php } ?>
        </div>
    </div>
    <div class="col-lg-6">
        <h2 class="page-header">Паспорт</h2>
        <?= \yii\widgets\DetailView::widget([
            'model'      => $item,
            'attributes' => [
                [
                    'label'          => 'Серия',
                    'value'          => $item->passport_ser,
                    'captionOptions' => ['style' => 'width: 40%'],
                ],
                [
                    'label' => 'Номер',
                    'value' => $item->passport_number,
                ],
                [
                    'label' => 'Выдан кем',
                    'value' => $item->passport_vidan_kem,
                ],
                [
                    'label'  => 'Выдан когда',
                    'format' => 'date',
                    'value'  => $item->passport_vidan_date,
                ],
                [
                    'label' => 'Выдан подразделение',
                    'value' => $item->passport_vidan_num,
                ],
                [
                    'label'  => 'Родился когда',
                    'format' => 'date',
                    'value'  => $item->passport_born_date,
                ],
                [
                    'label' => 'Родился где',
                    'value' => $item->passport_born_place,
                ],
            ],
        ]) ?>
        <div class="row">
            <div class="col-lg-3">
                <a href="<?= $item->passport_scan1 ?>" target="_blank">
                    <img src="<?= \iAvatar777\widgets\FileUpload7\FileUpload::getFile($item->passport_scan1, 'crop') ?>"
                         style="width: 100%;">
                </a>
            </div>
            <div class="col-lg-3">
                <a href="<?= $item->passport_scan2 ?>" target="_blank">
                    <img src="<?= \iAvatar777\widgets\FileUpload7\FileUpload::getFile($item->passport_scan2, 'crop') ?>"
                         style="width: 100%;">
                </a>
            </div>
        </div>
    </div>
</div>