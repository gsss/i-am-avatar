<?php
/** @var \yii\web\View $this */
/** @var \school\models\validate\KoopPayAddAjax $model */

$this->title = 'Внести пай';

?>


<div class="container">
    <div class="col-lg-12">
        <h1 class="page-header"><?= \yii\helpers\Html::encode($this->title) ?></h1>
    </div>

    <div class="col-lg-12">

        <?php $form = \iAvatar777\services\FormAjax\ActiveForm::begin([
            'model'      => $model,
            'success' => <<<JS
function (ret) {
    $('#modalInfo').on('hidden.bs.modal', function() {
        
    }).modal();
}
JS
            ,
        ]); ?>

        <div class="row">
            <div class="col-lg-6">
                <?= $form->field($model, 'price') ?>
                <?= $form->field($model, 'ps_config_id')->label('Платежная система') ?>
                <hr>
                <?php  \iAvatar777\services\FormAjax\ActiveForm::end(['label' => 'Внести']); ?>
            </div>
        </div>



    </div>
</div>

<div class="modal fade" id="modalInfo" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="myModalLabel">Информация</h4>
            </div>
            <div class="modal-body">
                Успешно!
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
            </div>
        </div>
    </div>
</div>