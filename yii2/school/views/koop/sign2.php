<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;

/* @var $this \yii\web\View */
/* @var $model \avatar\models\forms\MailTest */

$this->title = 'Числовая (цифровая) подпись';

$this->registerJs(<<<JS
var wif;

$('.buttonGet').click(function(e) {
    ajaxJson({
        url: '/cabinet-digital-sign/get',
        data: $('#formSign').serializeArray(),
        success: function(ret) {
            wif = ret.Wif;
            $('#buttonsAdd').collapse('show');
            $('#form1').collapse('hide');
        }
    });
});
$('.buttonJson').click(function(e) {
    ajaxJson({
        url: '/cabinet-digital-sign/download-json',
        data: {wif:wif},
        success: function(ret) {
            
        }
    });
});
JS
);
$row = \common\models\UserDigitalSign::findOne(['user_id' => Yii::$app->user->id]);
if (is_null($row)) {
    $class1 = 'collapse in';
    $class2 = 'collapse';
} else {
    $class1 = 'collapse';
    $class2 = 'collapse in';
}
?>

<div class="container" style="padding-bottom: 70px;">
    <div class="col-lg-12">
        <h1 class="page-header"><?= $this->title ?></h1>
    </div>
    <div class="col-lg-8">

        <div class="<?= $class1 ?>" id="form1">
            <p>Чтобы получить цифровую подпись нажмите кнопку ниже:</p>
            <?php
            $model = new \avatar\models\validate\CabinetDigitalSignGet();
            $form = ActiveForm::begin([
                'id'      => 'formSign',
                'options' => ['enctype' => 'multipart/form-data']
            ]);
            ?>
            <?= $form->field($model, 'random')
                ->label('Случайное набор')
                ->hint('Это строка произвольной длинны (до 100 мимволов) используется для формирования случайного ключа')
            ?>
            <?= $form->field($model, 'is_save')
                ->label('Сохранять в системе')
                ->dropDownList([
                    1 => 'Сохранять',
                    0 => 'Не сохранять',
                ])
            ?>
            <hr>

            <?php ActiveForm::end(); ?>

            <div class="form-group">
                <?= Html::button('Получить подпись', [
                    'class' => 'btn btn-default buttonGet',
                    'name'  => 'contact-button',
                    'style' => 'width:100%',
                ]) ?>
            </div>
        </div>

        <div class="<?= $class2 ?>"  id="buttonsAdd">
            <p class="alert alert-warning">Сохраните обязательно файл JSON. Он вам пригодится для восстановления.</p>
            <div class="form-group" >
                <a href="/cabinet-digital-sign/download-json" class="btn btn-default">Скачать JSON</a>
            </div>
            <p><a href="/koop/pay" class="btn btn-default">Продолжить</a></p>
        </div>

    </div>

</div>