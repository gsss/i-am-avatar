<?php
/** @var \yii\web\View $this */


$this->title = 'Внести взнос';

$UserEnter = \avatar\models\UserEnter::findOne(['user_id' => Yii::$app->user->id]);
$id = $UserEnter->id;
$s = \common\models\school\School::findOne($UserEnter->school_id);

$model = new \school\models\validate\KoopPayAjax([
    'id'     => $UserEnter->id,
    'school' => $s,
]);

?>


<div class="container">
    <div class="col-lg-12">
        <h1 class="page-header"><?= \yii\helpers\Html::encode($this->title) ?></h1>
    </div>

    <div class="col-lg-6">

        <?php
        $koop = \common\models\SchoolKoop::findOne(['school_id' => $s->id]);
        ?>
        <p>Вам нужно внести взнос <?= Yii::$app->formatter->asDecimal($koop->input_amount) ?> руб.</p>

        <?php $form = \iAvatar777\services\FormAjax\ActiveForm::begin([
            'model'   => $model,
            'success' => <<<JS
function (ret) {
    window.location = '/billing/pay?id=' + ret.billing.id
}

JS
            ,
            'formUrl' => '/koop/pay-ajax'

        ]) ?>

        <?= \yii\helpers\Html::activeHiddenInput($model, 'id') ?>
        <?= $form->field($model, 'ps_config_id')->label('Платежная система') ?>

        <?php \iAvatar777\services\FormAjax\ActiveForm::end(['label' => 'Внести']) ?>
        <hr>
        <p>«Настоящим выражаю свое согласие и мне известно что паевые взносы являются возвратными, возврат паевого взноса осуществляется на основании Устава, заключенных с кооперативом договоров, а также положений о целевых программах, решениях Совета кооператива. Вступительные, членские, членские целевые взносы возврату не подлежат.»</p>



    </div>
</div>

<div class="modal fade" id="modalInfo" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Информация</h4>
            </div>
            <div class="modal-body">
                Успешно!
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
            </div>
        </div>
    </div>
</div>