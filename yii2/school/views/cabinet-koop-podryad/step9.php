<?php

/** @var $this      \yii\web\View */
/** @var $school    \common\models\school\School */
/** @var $item      \common\models\KoopPodryad */

use common\models\KoopPodryadCommandUser;
use yii\helpers\Url;
use app\services\GsssHtml;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use app\services\GetArticle\Collection;

$this->title = 'Шаг 9. Подпись Протокола проведения собрания членов ВТК';

$file = pathinfo($item['protokol']);
$html = Html::img('/images/controller/cabinet/documents/file.png', ['width' => 50]) . '.' . strtoupper($file['extension']);


$UserDigitalSign = \common\models\UserDigitalSign::findOne(['user_id' => Yii::$app->user->id]);
$KoopPodryadCommandUser = KoopPodryadCommandUser::findOne(['podryad_id' => $item->id, 'user_id' => Yii::$app->user->id]);

\avatar\assets\Notify::register($this);
\avatar\assets\Clipboard::register($this);

$this->registerJs(<<<JS

var clipboard = new Clipboard('.buttonCopy');
clipboard.on('success', function(e) {

    new Noty({
        timeout: 1000,
        theme: 'sunset',
        type: 'success',
        layout: 'bottomLeft',
        text: 'Скопировано'
    }).show();

});

JS
);
?>

<div class="container">
    <div class="col-lg-12">
        <h1 class="page-header"><?= Html::encode($this->title) ?></h1>
    </div>

    <div class="col-lg-12">
        <p><a href="<?= $item['tz'] ?>" target="_blank"><?= $html ?></a>
        <hr>
        <?php if (\cs\Application::isEmpty($KoopPodryadCommandUser->protokol_sign)) { ?>
            <?php
            $this->registerJs(<<<JS
$('.buttonSign').click(function(e) {
    if ({$UserDigitalSign->type_id} == 2) {
        $('#modalInfo2').modal();
    }
    if ({$UserDigitalSign->type_id} == 1) {
        if (confirm('Вы уверены?')) {
            ajaxJson({
                url: '/cabinet-koop-podryad/protokol-sign',
                data: {
                    task_id: {$item->id}
                },
                success: function(ret) {
                    // console.log(ret);
                    window.location.reload();
                }
            });
        }
    }
    
});
JS
            )
            ?>
            <p><button class="btn btn-default buttonSign" data-id="<?= $item->id ?>">Подписать</button></p>
        <?php } else { ?>
            <p>Хеш: <code><?= $item->protokol_hash ?></code></p>
            <p>Адрес: <code><?= $KoopPodryadCommandUser->protokol_address ?></code></p>
            <p>Подпись: <code><?= $KoopPodryadCommandUser->protokol_sign ?></code></p>
            <p>Время: <code><?= Yii::$app->formatter->asDatetime($KoopPodryadCommandUser->protokol_created_at) ?></code></p>
        <?php }  ?>


    </div>

</div>

<div class="modal fade" id="modalInfo" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="myModalLabel">Информация</h4>
            </div>
            <div class="modal-body">
                Успешно!
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
            </div>
        </div>
    </div>
</div>

<?php if (!is_null($UserDigitalSign)) { ?>

    <div class="modal fade" id="modalInfo2" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title" id="myModalLabel">Подпись Протокола проведения собрания членов ВТК</h4>
                </div>
                <div class="modal-body">

                    <p>hash: <code ><?= $item->protokol_hash ?></code> <button role="button" class="btn btn-default btn-xs buttonCopy" data-clipboard-text="<?= $item->protokol_hash ?>">Скопировать</button> </p>
                    <p>address: <code><?= $UserDigitalSign->address ?></code></p>
                    <?php

                    $model = new \school\models\validate\CabinetKoopPodryadStep4ProtokolSign2([
                        'hash'    => $item->protokol_hash,
                        'task_id' => $item->id,
                        'address' => $UserDigitalSign->address,
                    ]);
                    $form = \iAvatar777\services\FormAjax\ActiveForm::begin([
                        'model'      => $model,
                        'formUrl' => '/cabinet-koop-podryad/protokol-sign2',
                        'success' => <<<JS
function (ret) {
    $('#modalInfo2').on('hidden.bs.modal', function() {
        $('#modalInfo').on('hidden.bs.modal', function() {
            window.location = '/cabinet-koop-podryad/view?id=' + {$item->id};
        }).modal();
    }).modal('hide');
}
JS
                        ,
                    ]); ?>
                    <?= Html::activeHiddenInput($model, 'task_id') ?>
                    <?= Html::activeHiddenInput($model, 'hash') ?>
                    <?= Html::activeHiddenInput($model, 'address') ?>
                    <?= $form->field($model, 'sign') ?>
                    <hr>

                    <?php  \iAvatar777\services\FormAjax\ActiveForm::end(['label' => 'Проверить и сохранить']); ?>
                </div>
            </div>
        </div>
    </div>
<?php } ?>

