<?php

/** @var $this      \yii\web\View */
/** @var $school    \common\models\school\School */
/** @var $item      \common\models\KoopPodryad */

use common\models\KoopPodryadCommandUser;
use yii\helpers\Url;
use app\services\GsssHtml;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use app\services\GetArticle\Collection;

$this->title = 'Шаг 4. Подпись технического задания';

$file = pathinfo($item['tz']);
$html = Html::img('/images/controller/cabinet/documents/file.png', ['width' => 50]) . '.' . strtoupper($file['extension']);


$UserDigitalSign = \common\models\UserDigitalSign::findOne(['user_id' => Yii::$app->user->id]);
$KoopPodryadCommandUser = KoopPodryadCommandUser::findOne(['podryad_id' => $item->id, 'user_id' => Yii::$app->user->id]);
?>

<div class="container">
    <div class="col-lg-12">
        <h1 class="page-header"><?= Html::encode($this->title) ?></h1>
    </div>

    <div class="col-lg-12">
        <p><a href="<?= $item['tz'] ?>" target="_blank"><?= $html ?></a>
        <hr>
        <?php if (\cs\Application::isEmpty($KoopPodryadCommandUser->tz_sign)) { ?>
            <?php
            $this->registerJs(<<<JS
$('.buttonSign').click(function(e) {
    if ({$UserDigitalSign->type_id} == 2) {
        $('#modalInfo2').modal();
    }
    if ({$UserDigitalSign->type_id} == 1) {
        if (confirm('Вы уверены?')) {
            ajaxJson({
                url: '/cabinet-koop-podryad/tz-sign',
                data: {
                    task_id: {$item->id}
                },
                success: function(ret) {
                    // console.log(ret);
                    window.location.reload();
                }
            });
        }
    }
    
});
JS
            )
            ?>
            <p><button class="btn btn-default buttonSign" data-id="<?= $item->id ?>">Подписать</button></p>
        <?php } else { ?>
            <p>Хеш: <code><?= $item->tz_hash ?></code></p>
            <p>Адрес: <code><?= $KoopPodryadCommandUser->tz_address ?></code></p>
            <p>Подпись: <code><?= $KoopPodryadCommandUser->tz_sign ?></code></p>
            <p>Время: <code><?= Yii::$app->formatter->asDatetime($KoopPodryadCommandUser->tz_created_at) ?></code></p>
        <?php }  ?>


    </div>

</div>

<div class="modal fade" id="modalInfo" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="myModalLabel">Информация</h4>
            </div>
            <div class="modal-body">
                Успешно!
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
            </div>
        </div>
    </div>
</div>

<?php if (!is_null($UserDigitalSign)) { ?>

    <div class="modal fade" id="modalInfo2" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title" id="myModalLabel">Подпись технического задания</h4>
                </div>
                <div class="modal-body">
                    <?php

                    $model = new \school\models\validate\CabinetKoopPodryadStep4TzSign2([
                        'hash'    => $item->tz_hash,
                        'task_id' => $item->id,
                        'address' => $UserDigitalSign->address,
                    ]);
                    $form = \iAvatar777\services\FormAjax\ActiveForm::begin([
                        'model'      => $model,
                        'formUrl' => '/cabinet-koop-podryad/tz-sign2',
                        'success' => <<<JS
function (ret) {
    $('#modalInfo2').on('hidden.bs.modal', function() {
        $('#modalInfo').on('hidden.bs.modal', function() {
            window.location = '/cabinet-koop-podryad/view?id=' + {$item->id};
        }).modal();
    }).modal('hide');
    
}
JS
                        ,
                    ]); ?>
                    <p>hash: <code><?= $item->tz_hash ?></code></p>
                    <p>address: <code><?= $UserDigitalSign->address ?></code></p>
                    <?= Html::activeHiddenInput($model, 'task_id') ?>
                    <?= Html::activeHiddenInput($model, 'hash') ?>
                    <?= Html::activeHiddenInput($model, 'address') ?>
                    <?= $form->field($model, 'sign') ?>
                    <hr>

                    <?php  \iAvatar777\services\FormAjax\ActiveForm::end(['label' => 'Проверить и сохранить']); ?>
                </div>
            </div>
        </div>
    </div>
<?php } ?>

