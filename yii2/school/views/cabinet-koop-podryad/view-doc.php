<?php

/** @var $this \yii\web\View */
/** @var $name string */
/** @var $link string */
/** @var $hash string */

?>

<p><?= $name ?>:</p>
<?php if (!\cs\Application::isEmpty($link)) { ?>
    <?php
    $file = pathinfo($link);
    $html = \yii\helpers\Html::img('/images/controller/cabinet/documents/file.png', ['width' => 50]) . '.' . strtoupper($file['extension']);
    ?>
    <p><a href="<?= $link ?>" target="_blank"><?= $html ?></a></p>
    <?php if (isset($hash)) { ?>
        <?php if (!\cs\Application::isEmpty($hash)) { ?>
            <?php
            $short = substr($hash, 0, 10) . '...';

            $h = \yii\helpers\Html::tag('code', $short, [
                'class' => "buttonCopy",
                'title' => 'Нажми чтобы скопировать',
                'data'  => [
                    'toggle'         => 'tooltip',
                    'clipboard-text' => $hash,
                ],
            ]);
            ?>
            <p>Хеш: <?= $h ?></p>
        <?php } ?>
    <?php } ?>
<?php } ?>
