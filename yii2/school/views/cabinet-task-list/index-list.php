<?php

/** @var $this \yii\web\View */
/** @var $school \common\models\school\School */


use yii\helpers\Url;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use kartik\grid\GridView;

$statusList = \common\models\task\Status::find()
    ->where(['school_id' => $school->id])
    ->orderBy(['sort_index' => SORT_ASC])
    ->select('id')
    ->column();

Yii::$app->session->set('$statusList', $statusList);


?>

<?php
$sort = new \yii\data\Sort([
    'attributes'   => [
        'id'                => ['label' => 'ID'],
        'status'            => [
            'label' => 'Прогресс',
            'asc'   => ['status' => SORT_ASC, 'category_id' => SORT_ASC],
            'desc'  => ['status' => SORT_DESC, 'category_id' => SORT_DESC],
        ],
        'price'             => [
            'label'   => 'Награда',
            'default' => SORT_DESC,
        ],
        'created_at'        => ['label' => 'Созд', 'default' => SORT_DESC],
        'name'              => ['label' => 'Наименование'],
        'last_comment_time' => ['label' => 'Обн', 'default' => SORT_DESC],
        'category_id'       => ['label' => 'category_id'],
    ],
    'defaultOrder' => [
        'category_id' => SORT_ASC,
        'created_at'  => SORT_DESC,
    ],
]);
$model = new \avatar\models\search\Task();

$where = [
    'school_task.school_id' => $school->id,
    //    'parent_id' => $model->id,
    'is_hide'   => 0,
];
$init = [];

// если пользователю можно показывать только свои задачи, то добавляю фильтр
$link = \common\models\school\CommandLink::findOne(['user_id' => Yii::$app->user->id, 'school_id' => $school->id]);
if (is_null($link)) throw new \cs\web\Exception('Вы не в команде');
if ($link->is_only_my) {
    $init['Task']['executer_id'] = \avatar\models\search\Task::EXECUTER_ID_MY;
}

$provider = $model->search($sort, ArrayHelper::merge(Yii::$app->request->get(), $init), $statusList, $where);
?>
<?php
$this->registerJs(<<<JS
$('[data-toggle="tooltip"]').tooltip();
$('.rowTable').click(function(e) {
    window.location = '/cabinet-task-list/view' + '?' + 'id' + '=' + $(this).data('id');
});
JS
);

$columns = [
    [
        'attribute'           => 'category_id',
        'value'               => function ($model, $key, $index, $widget) {
            $c = \common\models\task\Category::findOne($model['category_id']);
            if (is_null($c)) {
                return '- Без категории -';
            } else {
                return $c->name;
            }
        },
        'filterType'          => GridView::FILTER_SELECT2,
        'filter'              => ArrayHelper::map(\common\models\task\Category::find()->asArray()->all(), 'id', 'name'),
        'filterWidgetOptions' => [
            'pluginOptions' => ['allowClear' => true],
        ],
        'filterInputOptions'  => ['placeholder' => 'Any supplier'],
        'group'               => true,  // enable grouping,
        'groupedRow'          => true,                    // move grouped column to a single grouped row
        'groupOddCssClass'    => 'kv-grouped-row',  // configure odd group cell css class
        'groupEvenCssClass'   => 'kv-grouped-row', // configure even group cell css class
    ],
    [
        'attribute' => 'id',
    ],
    [
        'header'    => 'Автор',
        'attribute' => 'user_id',
        'filter'    => \avatar\models\search\Task::$listUserID,
        'content'   => function ($item) {
            $user_id = ArrayHelper::getValue($item, 'user_id', '');
            if ($user_id == '') {
                return '';
            }
            $user = \common\models\UserAvatar::findOne($user_id);

            return Html::img($user->getAvatar(), [
                'class'  => "img-circle",
                'width'  => 30,
                'height' => 30,
                'style'  => 'margin-bottom: 0px;',
                'data'   => ['toggle' => 'tooltip'],
                'title'  => \yii\helpers\Html::encode($user->getName2()),
            ]);
        },
    ],

];
if ($link->is_only_my == 0) {
    $columns[] = [
        'header'    => 'Исполнитель',
        'attribute' => 'executer_id',
        'filter'    => \avatar\models\search\Task::$list,
        'content'   => function ($item) {
            $executer_id = ArrayHelper::getValue($item, 'executer_id', '');
            if ($executer_id == '') {
                return '';
            }
            $user = \common\models\UserAvatar::findOne($executer_id);

            return Html::img($user->getAvatar(), [
                'class'  => "img-circle",
                'width'  => 30,
                'height' => 30,
                'style'  => 'margin-bottom: 0px;',
                'data'   => ['toggle' => 'tooltip'],
                'title'  => \yii\helpers\Html::encode($user->getName2()),
            ]);
        },
    ];
}
$columns2 = [
    [
        'header'             => $sort->link('name'),
        'attribute'          => 'name',
        'pageSummary'        => 'Page Summary',
        'pageSummaryOptions' => ['class' => 'text-right'],
    ],
    [
        'header'    => $sort->link('status'),
        'attribute' => 'status',
        'width'     => '100px',
        'filter'    => [
            1 => 'Показать Выполненные',
            2 => 'Скрыть Выполненные',
        ],
        'content'   => function ($item) {
            if (empty($item['status'])) $item['status'] = 0;
            $statusList = Yii::$app->session->get('$statusList');
            $j = 0;
            if (!(is_null($item['status']) or $item['status'] == 0)) {
                for ($i = 0; $i < count($statusList); $i++) {
                    $j++;
                    if ($item['status'] == $statusList[$i]) break;
                }
            }

            $percent = 0;
            if ($j == 1) $percent = 33;
            if ($j == 2) $percent = 66;
            if ($j == 3) $percent = 100;

            return Html::tag(
                'div',
                Html::tag(
                    'div',
                    Html::tag(
                        'div',
                        $percent . '% Complete'
                        ,
                        ['class' => "sr-only"]
                    )
                    ,
                    [
                        'class'         => "progress-bar",
                        'role'          => "progressbar",
                        'aria-valuenow' => $percent,
                        'aria-valuemin' => "0",
                        'aria-valuemax' => "100",
                        'style'         => "width: $percent%;",
                    ]
                )
                ,
                [
                    'class' => "progress",
                    'style' => "margin-bottom: 0px;",
                ]
            );
        },
    ],
    [
        'header'          => $sort->link('price'),
        'attribute'       => 'price',
        'width'           => '100px',
        'hAlign'          => 'right',
        'format'          => ['decimal', 2],
        'value' => function ($model) {
            return $model['price'] / 100;
        },
        'pageSummary'     => true,
        'pageSummaryFunc' => GridView::F_SUM
    ],
    [
        'header'          => 'Валюта',
        'attribute'       => 'currency_id',
        'width'           => '70px',
        'content'         => function ($model, $key, $index, $widget) {
            $cur = \common\models\piramida\Currency::findOne($model['currency_id']);
            if (is_null($cur)) return '';

            return Html::tag('span', $cur->code, ['class' => 'label label-info'] );
        },
    ],
    [
        'header'    => $sort->link('created_at'),
        'attribute' => 'created_at',
        'content'   => function ($item) {
            $v = \yii\helpers\ArrayHelper::getValue($item, 'created_at', 0);
            if ($v == 0) return '';

            return Html::tag('abbr', \cs\services\DatePeriod::back($v, ['isShort' => true]), ['data' => ['toggle' => 'tooltip'], 'title' => Yii::$app->formatter->asDatetime($v)]);
        },
    ],
    [
        'header'    => $sort->link('last_comment_time'),
        'attribute' => 'last_comment_time',
        'content'   => function ($item) {
            $v = \yii\helpers\ArrayHelper::getValue($item, 'last_comment_time', 0);
            if ($v == 0) return '';

            return Html::tag('abbr', \cs\services\DatePeriod::back($v, ['isShort' => true]), ['data' => ['toggle' => 'tooltip'], 'title' => Yii::$app->formatter->asDatetime($v)]);
        },
    ],
];
$columns = ArrayHelper::merge($columns, $columns2);

?>
<p><a href="<?= Url::to(['cabinet-task-list/add', 'id' => $school->id]) ?>" class="btn btn-default">Добавить задачу</a></p>

<?=
GridView::widget([
    'dataProvider'        => $provider,
    'filterModel'         => $model,
    'showPageSummary'     => true,
    'pjax'                => true,
    'toolbar'             => [
        '{toggleData}',
    ],
    'pjaxSettings'        => [
        'beforeGrid' => <<< HTML
<script>
$('[data-toggle="tooltip"]').tooltip();
$('.rowTable').click(function(e) {
    window.location = '/cabinet-task-list/view' + '?' + 'id' + '=' + $(this).data('id');
});
</script>
HTML
        ,
    ],
    'striped'             => true,
    'hover'               => true,
    'panel'               => ['type' => 'default', 'heading' => 'Задачи'],
    'toggleDataContainer' => ['class' => 'btn-group mr-2'],
    'rowOptions'          => function ($item) {
        return [
            'data'  => ['id' => $item['id']],
            'role'  => 'button',
            'class' => 'rowTable',
        ];
    },
    'columns'             => $columns,
]);
?>

<div class="modal fade" id="modalInfo" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="myModalLabel">Информация</h4>
            </div>
            <div class="modal-body">
                Успешно!
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
            </div>
        </div>
    </div>
</div>