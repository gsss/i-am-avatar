<?php
use yii\helpers\Url;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use kartik\grid\GridView;

$link = \common\models\school\CommandLink::findOne(['user_id' => Yii::$app->user->id, 'school_id' => $school->id]);

$columns = [
    [
        'attribute' => 'category_id',
        'value'     => function ($model, $key, $index, $widget) {
            $c = \common\models\task\Category::findOne($model['category_id']);
            if (is_null($c)) {
                return '- Без категории -';
            } else {
                return $c->name;
            }
        },
        'filter'    => ArrayHelper::map(\common\models\task\Category::find()->asArray()->all(), 'id', 'name'),
    ],
    [
        'attribute' => 'id',
    ],
    [
        'header'    => 'Автор',
        'attribute' => 'user_id',
        'filter'    => \avatar\models\search\Task::$listUserID,
        'content'   => function ($item) {
            $user_id = ArrayHelper::getValue($item, 'user_id', '');
            if ($user_id == '') {
                return '';
            }
            $user = \common\models\UserAvatar::findOne($user_id);

            return Html::img($user->getAvatar(), [
                'class'  => "img-circle",
                'width'  => 30,
                'height' => 30,
                'style'  => 'margin-bottom: 0px;',
                'data'   => ['toggle' => 'tooltip'],
                'title'  => \yii\helpers\Html::encode($user->getName2()),
            ]);
        },
    ],
];

if ($link->is_only_my == 0) {
    $columns[] = [
        'header'    => 'Исполнитель',
        'attribute' => 'executer_id',
        'filter'    => \avatar\models\search\Task::$list,
        'content'   => function ($item) {
            $executer_id = ArrayHelper::getValue($item, 'executer_id', '');
            if ($executer_id == '') {
                return '';
            }
            $user = \common\models\UserAvatar::findOne($executer_id);

            return Html::img($user->getAvatar(), [
                'class'  => "img-circle",
                'width'  => 30,
                'height' => 30,
                'style'  => 'margin-bottom: 0px;',
                'data'   => ['toggle' => 'tooltip'],
                'title'  => \yii\helpers\Html::encode($user->getName2()),
            ]);
        },
    ];
}

$columns2 = [
    [
        'header'    => $sort->link('name'),
        'attribute' => 'name',
        'content'   => function ($item) {
            return Html::a($item['name'], [
                'cabinet-task-list/view',
                'id' => $item['id'],
            ], ['data' => ['pjax' => 0]]);
        },
    ],
    [
        'header'    => $sort->link('status'),
        'attribute' => 'status',
        'filter'    => [
            1 => 'Показать Выполненные',
            2 => 'Скрыть Выполненные',
        ],
        'content'   => function ($item) {
            if (empty($item['status'])) $item['status'] = 0;
            $statusList = Yii::$app->session->get('$statusList');
            $j = 0;
            if (!(is_null($item['status']) or $item['status'] == 0)) {
                for ($i = 0; $i < count($statusList); $i++) {
                    $j++;
                    if ($item['status'] == $statusList[$i]) break;
                }
            }

            $percent = 0;
            if ($j == 1) $percent = 33;
            if ($j == 2) $percent = 66;
            if ($j == 3) $percent = 100;

            $html = Html::tag(
                'div',
                Html::tag(
                    'div',
                    Html::tag(
                        'div',
                        $percent . '% Complete'
                        ,
                        ['class' => "sr-only"]
                    )
                    ,
                    [
                        'class'         => "progress-bar",
                        'role'          => "progressbar",
                        'aria-valuenow' => $percent,
                        'aria-valuemin' => "0",
                        'aria-valuemax' => "100",
                        'style'         => "width: $percent%;",
                    ]
                )
                ,
                [
                    'class' => "progress",
                    'style' => "margin-bottom: 0px;",
                ]
            );
            $b = '';
            if ($j == 2) {
                $b = '<button class="btn btn-success btn-xs buttonDone" data-id="' . $item['id'] . '" role="button" title="Принять" data-toggle="tooltip"><i
                            class="glyphicon glyphicon-thumbs-up"></i></button>';
            }
            if ($j == 1) {
                $b = '<button class="btn btn-info btn-xs buttonFinish" data-id="' . $item['id'] . '" role="button" title="Я сделал(а)" data-toggle="tooltip"><i
                            class="glyphicon glyphicon-ok"></i></button>';
            }
            if ($j == 0) {
                $b = '<button class="btn btn-default btn-xs buttonStart" data-id="' . $item['id'] . '" role="button" title="Я сделаю" data-toggle="tooltip"><i
                            class="glyphicon glyphicon-user"></i></button>';
            }

            return Html::tag(
                'div',
                $html .
                Html::tag('div', $b, ['class' => 'input-group-btn', 'style' => 'padding-left: 10px;']),
                ['class' => 'input-group']);
        },
    ],
    [
        'header'         => $sort->link('price'),
        'attribute'      => 'price',
        'headerOptions'  => ['class' => 'text-right'],
        'contentOptions' => ['class' => 'text-right'],
        'format'         => ['decimal', 2],
        'value'          => function ($model) {
            if (\cs\Application::isEmpty($model['price'])) return '';
            if (\cs\Application::isEmpty($model['currency_id'])) return '';
            try {
                $v = \common\models\piramida\Currency::getValueFromAtom($model['price'], $model['currency_id']);
            } catch (Exception $e) {
                Yii::error([$e->getMessage(), $model, $e->getTraceAsString()], 'avatar\views\cabinet-school-task-list\index2-list-columns.php');
                return '';
            }

            return $v;
        },
    ],
    [
        'header'    => 'Валюта',
        'attribute' => 'currency_id',
        'content'   => function ($model, $key, $index, $widget) {
            $cur = \common\models\piramida\Currency::findOne($model['currency_id']);
            if (is_null($cur)) return '';

            return Html::tag('span', $cur->code, ['class' => 'label label-info']);
        },
    ],
    [
        'header'    => $sort->link('created_at'),
        'attribute' => 'created_at',
        'content'   => function ($item) {
            $v = \yii\helpers\ArrayHelper::getValue($item, 'created_at', 0);
            if ($v == 0) return '';

            return Html::tag('abbr', \cs\services\DatePeriod::back($v, ['isShort' => true]), ['data' => ['toggle' => 'tooltip'], 'title' => Yii::$app->formatter->asDatetime($v)]);
        },
    ],
    [
        'header'    => $sort->link('last_comment_time'),
        'attribute' => 'last_comment_time',
        'content'   => function ($item) {
            $v = \yii\helpers\ArrayHelper::getValue($item, 'last_comment_time', 0);
            if ($v == 0) return '';

            return Html::tag('abbr', \cs\services\DatePeriod::back($v, ['isShort' => true]), ['data' => ['toggle' => 'tooltip'], 'title' => Yii::$app->formatter->asDatetime($v)]);
        },
    ],
];

return ArrayHelper::merge($columns, $columns2);