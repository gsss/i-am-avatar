<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;
use app\models\UnionCategory;
use yii\db\Query;
use yii\helpers\ArrayHelper;

/** @var $this yii\web\View */
/** @var $form yii\bootstrap\ActiveForm */
/** @var $model \avatar\models\forms\Task */
/** @var $school \common\models\school\School */

$this->title = 'Добавить задачу';

$currencyList = [];
$SchoolCoin = \common\models\SchoolCoin::find()
    ->where(['school_id' => $school->id])
    ->all();
$rows = [];
foreach ($SchoolCoin as $r) {
    $c = \common\models\piramida\Currency::findOne($r['currency_id']);
    $rows[$r['currency_id']] = $c->code;
}
$currencyList = ArrayHelper::map($SchoolCoin, 'id', 'code');

$this->registerJs(<<<JS
$('[name="Task[wallet_type]"]').change(function(e) {
    if ($(this).val() == 3) {
        $('.js-wallet_type3_user_id').collapse('show');
    } else {
        $('.js-wallet_type3_user_id').collapse('hide');
    }
})
JS
);
?>


<div class="container">

    <h1 class="page-header"><?= Html::encode($this->title) ?></h1>


    <?php if (Yii::$app->session->hasFlash('form')) : ?>
        <div class="alert alert-success">
            Успешно добавлено.
        </div>
        <p>
            <a href="<?= \yii\helpers\Url::to(['cabinet-school-task-list/index', 'id' => $school->id]) ?>" class="btn btn-success">Все задачи</a>
            <a href="<?= \yii\helpers\Url::to(['cabinet-school-task-list/view', 'id' => Yii::$app->session->getFlash('form')]) ?>" class="btn btn-success">Эта задача</a>
        </p>
    <?php else: ?>
        <?php
        $form = \iAvatar777\services\FormAjax\ActiveForm::begin([
            'model'   => $model,
            'success' => <<<JS
function (ret) {
    window.location = '/cabinet-task-list/index?id=' + {$school->id};
}
JS
            ,
        ]); ?>

        <?= $form->field($model, 'name')->label('Наименование') ?>
        <?= $form->field($model, 'price')->hint('Кол-во монет')->label('Награда') ?>
        <?= $form->field($model, 'currency_id')->label('Валюта')->dropDownList($rows, ['prompt' => '- ничего не выбрано-']) ?>
        <?= $form->field($model, 'wallet_type')->label('Бюджет')->radioList([
            1 => 'Кошелек сообщества',
            2 => 'Кошелек постановщика',
            3 => 'Указать кошелек пользователя',
        ]) ?>
        <div class="collapse js-wallet_type3_user_id">
            <?= $form->field($model, 'wallet_type3_user_id')->label('Кошелек пользователя для списания бюжета')->widget('\common\widgets\Autocomplete\Autocomplete', ['clientOptions' => [
                'minLength' => 1,
                'source'    => new \yii\web\JsExpression(<<<JS
function(qry, callback1) {
    ajaxJson({
        url: '/cabinet-task-list/budjet-search-ajax',
        data: {
            term: qry.term,
            company_id: {$school->id}
        },
        success: function(ret) {
            callback1(ret);
        }
    });
}
JS
                )
            ]]) ?>
        </div>

        <?= $form->field($model, 'category_id')->label('Категория')->dropDownList(
            ArrayHelper::merge(
                ['- Не выбрано -'],
                ArrayHelper::map(
                    \common\models\task\Category::find()->orderBy(['sort_index' => SORT_ASC])->where(['school_id' => $school->id])->all(),
                    'id',
                    'name'
                )
            )
        ) ?>
        <?= $form->field($model, 'content')->textarea(['rows' => 10])->label('Содержание задачи и критерии исполнения') ?>

        <hr>

        <?php  \iAvatar777\services\FormAjax\ActiveForm::end(['label' => 'Поставить']); ?>

    <?php endif; ?>
</div>
