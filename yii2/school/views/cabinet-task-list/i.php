<?php
/**
 * Created by PhpStorm.
 * User: Ra-m-ha
 * Date: 31.12.2018
 * Time: 0:44
 */

?>
<?php /** @var \common\models\task\Task $item */ ?>

<li class="ui-state-default buttonOpen" data-id="<?= $item->id ?>">
    <span class="label label-info" data-id="<?= $item->id ?>" role="button">#<?= $item->id ?></span>
    <?= $item->name ?>
    <br>
    <?php $author = \common\models\UserAvatar::findOne($item->user_id) ?>
    <img src="<?= $author->getAvatar() ?>" width="30" class="img-circle img-user" title="<?= \yii\helpers\Html::encode($author->getName2()) ?>" data-toggle="tooltip"/>
    <?php if ($item->executer_id) { ?>
    <?php $user = \common\models\UserAvatar::findOne($item->executer_id) ?>

        <img src="<?= $user->getAvatar() ?>" width="30" class="img-circle img-user" title="<?= \yii\helpers\Html::encode($user->getName2()) ?>" data-toggle="tooltip"/>
    <?php } ?>
</li>