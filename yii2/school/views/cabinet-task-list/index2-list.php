<?php

/** @var $this \yii\web\View */
/** @var $school \common\models\school\School */
/** @var $isp  bool  is PM */

/** @var $isa  bool  is Admin */

use yii\helpers\Url;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use kartik\grid\GridView;

$statusList = \common\models\task\Status::find()
    ->where(['school_id' => $school->id])
    ->orderBy(['sort_index' => SORT_ASC])
    ->select('id')
    ->column();

Yii::$app->session->set('$statusList', $statusList);

?>

<?php
$sort = new \yii\data\Sort([
    'attributes'   => [
        'id'                => ['label' => 'ID'],
        'status'            => [
            'label' => 'Прогресс',
            'asc'   => ['status' => SORT_ASC, 'category_id' => SORT_ASC],
            'desc'  => ['status' => SORT_DESC, 'category_id' => SORT_DESC],
        ],
        'price'             => [
            'label'   => 'Награда',
            'default' => SORT_DESC,
        ],
        'created_at'        => ['label' => 'Созд', 'default' => SORT_DESC],
        'name'              => ['label' => 'Наименование'],
        'last_comment_time' => ['label' => 'Обн', 'default' => SORT_DESC],
        'category_id'       => ['label' => 'category_id'],
        'currency_id '      => ['label' => 'Валюта'],
    ],
    'defaultOrder' => [
        'last_comment_time' => SORT_DESC,
        'created_at'        => SORT_DESC,
    ],
]);


$where1 = [
    'school_task.school_id' => $school->id,
    'is_hide'               => 0,
    'school_task.parent_id' => null,
];

// если пользователю можно показывать только свои задачи, то добавляю фильтр
$link = \common\models\school\CommandLink::findOne(['user_id' => Yii::$app->user->id, 'school_id' => $school->id]);
if (is_null($link)) throw new \cs\web\Exception('Вы не в команде');
if ($link->is_only_my) {
    $init['Task']['executer_id'] = \avatar\models\search\Task::EXECUTER_ID_MY;
    $where1['executer_id'] = Yii::$app->user->id;
    $where1 = [
        'and',
        ['not', ['executer_id' => null]],
        $where1,
    ];
}


$model = new \avatar\models\search\Task();
$provider = $model->search($sort, Yii::$app->request->get(), $statusList, $where1);


$sum = '';
if (!\cs\Application::isEmpty($school->wallet_id)) {
    $provider2 = $model->search($sort, Yii::$app->request->get(), $statusList, [
        'school_task.school_id' => $school->id,
        'is_hide'               => 0,
    ]);

    /** @var \yii\db\Query $q */
    $q = $provider2->query;
    $n = $q->select(['sum(price)'])->scalar();
    $w = \common\models\piramida\Wallet::findOne($school->wallet_id);
    $c = \common\models\piramida\Currency::findOne($w->currency_id);
    $sum = Yii::$app->formatter->asDecimal($n / pow(10, $c->decimals), $c->decimals) . ' ' . $c->code;

}
?>
    <p>Итого: <?= $sum ?></p>
<?php
$this->registerJs(<<<JS
$('[data-toggle="tooltip"]').tooltip();
$('.rowTable').click(function(e) {
});

JS
);
$columns = require('index2-list-columns.php');

if ($isp || $isa) {
    $this->registerJs(<<<JS
$('.buttonDone').click( function(e) {
    var button = $(this);
    ajaxJson({
        url: '/cabinet-task-list/view-done',
        data: {
            id: $(this).data('id')
        },
        success: function(ret) {
            button.parent().parent().parent().parent().remove();
        }
    })
});
$('.buttonFinish').on('click', function(e) {
    ajaxJson({
        url: '/cabinet-task-list/view-finish',
        data: {
            id: $(this).data('id')
        },
        success: function(ret) {
            window.location.reload();
        }
    })
});
$('.buttonStart').click(function(e) {
    ajaxJson({
        url: '/cabinet-task-list/view-start',
        data: {
            id: $(this).data('id')
        },
        success: function(ret) {
            window.location.reload();
        }
    })
});
JS
    );

}
$isFa = true;
$pdfHeader = '';
$pdfFooter = '';
$title = '$title';
// $isFa below determines if export['fontAwesome'] property is set to true.


if (Yii::$app->deviceDetect->isMobile()) {
    $options = [
        'margin-top'    => '0px',
        'margin-bottom' => '200px',
        'margin-left'   => '10px',
        'margin-right'  => '10px',
    ];
} else {
    $options = [
        'margin-top'    => '0px',
        'margin-bottom' => '200px',
        'margin-left'   => '100px',
        'margin-right'  => '100px',
    ];
}
?>
<?=
\yii\grid\GridView::widget([
    'dataProvider'        => $provider,
    'filterModel'         => $model,
    'options'             => [
        'style' => Html::cssStyleFromArray($options),
    ],
    'tableOptions'          => [
        'class' => 'table table-striped table-hover'
    ],
    'rowOptions'          => function ($item) {
        return [
            'data'  => ['id' => $item['id']],
            'class' => 'rowTable',
        ];
    },
    'columns'             => $columns,
]);
?>