<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

/** @var $this yii\web\View */
/** @var $covCert \common\models\CovCert */

$this->title = 'Портал государственных услуг Российской Федерации';



?>
<html lang="ru">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <link href="https://cloud1.cloud999.ru/upload/cloud/16398/24499_026RSI9sdh.ico" rel="shortcut icon">

    <meta content="IE=edge;chrome=1" http-equiv="X-UA-Compatible">
    <meta content="width=device-width,height=device-height,initial-scale=1,maximum-scale=1,user-scalable=no"
          name="viewport">
    <title>Портал государственных услуг Российской Федерации</title>
    <meta content="Портал государственных услуг Российской Федерации" name="description">
    <meta content="SKYPE_TOOLBAR_PARSER_COMPATIBLE" name="SKYPE_TOOLBAR">
    <meta content="Госуслуги" name="apple-mobile-web-app-title">
    <link href="https://cloud1.cloud999.ru/upload/cloud/16398/24576_6vwQSFzN9q.css"
          rel="stylesheet">
</head>
<body>
<div id="error" class="hide">
    <div class="error-container mt-24">
        <div class="frame"><h4 class="title-h4 mt-24 error-message">Сервис недоступен</h4>
            <div class="small-text gray mt-16 error-hint">Попробуйте снова или зайдите позже. Если ничего не изменится —
                напишите в <a href="https://www.gosuslugi.ru/help">службу поддержки</a>.
            </div>
        </div>
    </div>
</div>
<div id="loader" class="hide">
    <div class="container-app-loader" id="start-app-loader">
        <div class="container-pulse pulse animated"></div>
        <div class="container-app-logo">
            <div class="app-logo"></div>
        </div>
    </div>
</div>
<div id="result" class="vaccine-result">
    <div class="flex-container ml-6 mr-6 justify-between align-items-center mt-52 mb-32">
        <div class="ml-24"><a href="https://www.gosuslugi.ru/" class="logo"></a></div>
        <div id="translate-button" class="translate-button flex-container mt-6 mr-24 align-items-center">
            <div class="mr-8">
                <div class="lang-image">
                    <div class="lang-en hide">
                        <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <mask id="mask20" mask-type="alpha" maskUnits="userSpaceOnUse" x="0" y="0" width="24"
                                  height="24">
                                <circle cx="12" cy="12" r="11.6667" fill="#C4C4C4" stroke="#E1E1E1"
                                        stroke-width="0.666667"></circle>
                            </mask>
                            <g mask="url(#mask20)">
                                <path d="M0 -0.0185547H36V11.9994H0V-0.0185547Z" fill="#FAFCFF"></path>
                                <path d="M0 12H36V24.0179H0V12Z" fill="#D52B1E"></path>
                                <path d="M0 8.09485H36V16.207H0V8.09485Z" fill="#0039A6"></path>
                                <path d="M-11.04 0.47998V23.52H35.04V0.47998H-11.04Z" fill="#00247D"></path>
                                <path d="M-11.04 0.47998L35.04 23.52L-11.04 0.47998ZM35.04 0.47998L-11.04 23.52L35.04 0.47998Z"
                                      fill="black"></path>
                                <path d="M35.04 0.47998L-11.04 23.52M-11.04 0.47998L35.04 23.52L-11.04 0.47998Z"
                                      stroke="white" stroke-width="4.608"></path>
                                <path d="M-11.04 0.47998L35.04 23.52L-11.04 0.47998ZM35.04 0.47998L-11.04 23.52L35.04 0.47998Z"
                                      fill="black"></path>
                                <path d="M35.04 0.47998L-11.04 23.52M-11.04 0.47998L35.04 23.52L-11.04 0.47998Z"
                                      stroke="#CF142B" stroke-width="3.072"></path>
                                <path d="M12 0.47998V23.52V0.47998ZM-11.04 12H35.04H-11.04Z" fill="black"></path>
                                <path d="M-11.04 12H35.04M12 0.47998V23.52V0.47998Z" stroke="white"
                                      stroke-width="7.68"></path>
                                <path d="M12 0.47998V23.52V0.47998ZM-11.04 12H35.04H-11.04Z" fill="black"></path>
                                <path d="M-11.04 12H35.04M12 0.47998V23.52V0.47998Z" stroke="#CF142B"
                                      stroke-width="4.608"></path>
                            </g>
                        </svg>
                    </div>
                    <div class="lang-ru">
                        <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <mask id="mask0" mask-type="alpha" maskUnits="userSpaceOnUse" x="0" y="0" width="24"
                                  height="24">
                                <circle cx="11.7499" cy="11.7499" r="11.4236" fill="#C4C4C4" stroke="#E1E1E1"
                                        stroke-width="0.652774"></circle>
                            </mask>
                            <g mask="url(#mask0)">
                                <path d="M0 -0.0184326H37.5V11.9995H0V-0.0184326Z" fill="white"></path>
                                <path d="M0 12H37.5V24.0179H0V12Z" fill="#D52B1E"></path>
                                <path d="M0 8.09485H37.5V16.207H0V8.09485Z" fill="#0039A6"></path>
                                <circle cx="11.7499" cy="11.7499" r="11.2799" stroke="#E1E1E1"
                                        stroke-width="0.939995"></circle>
                            </g>
                        </svg>
                    </div>
                </div>
            </div>
            <div id="lang-label" class="lang">RUS</div>
        </div>
    </div>
    <div>
        <p style="text-align: left; color: #888;">Медицинский сертификат о профилактических прививках против новой коронавирусной
            инфекции (COVID-19) или медицинских противопоказаниях к вакцинации и (или)
            перенесенном заболевании, вызванном новой коронавирусной инфекцией (COVID-19)</p>
        <p style="text-align: left; color: #888;">Производитель: ФБГУ НИЦЭМ ИМ. Н.Ф. ГАМАЛЕИ МИНЗДРАВА РОССИИ</p>
        <p style="text-align: left; color: #888;">Серия: II-140421</p>
        <p style="text-align: left;">QR код сертификата вакцинации</p>
<!--        <p style="text-align: left;"><a href="https://cloud1.cloud999.ru/upload/cloud/16402/49342_TRMliuKIr0.pdf" style="text-decoration: none;">Скачать сертитификат в PDF</a></p>-->

        <div id="dates" class="small-text gray mr-4"><p>
                <?php
                $wallet_address = 'https://www.gosuslugi.ru.qualitylive.su/covid-cert/status/' . $covCert->uid;

                $qr = new \Endroid\QrCode\QrCode();
                $qr->setText($wallet_address);
                $qr->setSize(180);
                $qr->setMargin(20);
                $qr->setErrorCorrectionLevel(\Endroid\QrCode\ErrorCorrectionLevel::HIGH());
                $qr->setForegroundColor(['r' => 0, 'g' => 0, 'b' => 0, 'a' => 0]);
                $qr->setBackgroundColor(['r' => 255, 'g' => 255, 'b' => 255, 'a' => 0]);
                $qr->setLabelFontSize(16);
                $content = $qr->writeString();
                $src = 'data:image/png;base64,' . base64_encode($content);
                ?>
                <img src="<?= $src ?>">
            </p></div>
        <p style="text-align: left;">Плановое заполнение дневника самонаблюдений</p>
        <p style="text-align: left; color: #888;">1 день после вакцинации 14.10.2021</p>
        <p style="text-align: left; color: #888;">2 день после вакцинации 15.10.2021</p>
        <p style="text-align: left; color: #888;">3 день после вакцинации 16.10.2021</p>
        <p style="text-align: left; color: #888;">7 день после вакцинации 21.10.2021</p>
        <p style="text-align: left; color: #888;">14 день после вакцинации 28.10.2021</p>





        <div class="mt-24"><a id="close" href="https://www.gosuslugi.ru/" class="button">Закрыть</a></div>
    </div>
    <div class="hide" id="elements">
        <div class="mb-4 person-data-wrap attr-wrap">
            <div class="small-text mb-4 mr-4 attr-title"></div>
            <div class="attr-value small-text gray"></div>
        </div>
    </div>
</div>
</body>
</html>
