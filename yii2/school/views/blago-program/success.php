<?php
/** @var \yii\web\View $this */
/** @var \avatar\models\forms\BlagoProgram $program */
/** @var \common\models\school\School $school */

$this->title = 'Благодарность';

?>


<div class="container">
    <div class="col-lg-12">
        <h1 class="page-header text-center"><?= \yii\helpers\Html::encode($this->title) ?></h1>
    </div>

    <div class="col-lg-12">


        <div class="row">
            <div class="col-lg-6 col-lg-offset-3">
                <p class="alert alert-success">Благодарность</p>
            </div>
        </div>


    </div>
</div>
