<?php
/** @var \yii\web\View $this */
/** @var \avatar\models\forms\BlagoProgram $program */
/** @var \common\models\school\School $school */

$this->title = $program->name;

?>


<div class="container">
    <div class="col-lg-12">
        <h1 class="page-header text-center"><?= \yii\helpers\Html::encode($this->title) ?></h1>
    </div>

    <div class="col-lg-12">


        <div class="row">
            <div class="col-lg-6 col-lg-offset-3">
                <?php
                $model = new \school\models\validate\BlagoProgramIndexAjax();
                $form = \iAvatar777\services\FormAjax\ActiveForm::begin([
                    'model'      => $model,
                    'success' => <<<JS
function (ret) {
    $('#modalInfo').on('hidden.bs.modal', function() {
        window.location = '/billing/pay?id' + ret.id
    }).modal();
}
JS
                    ,
                ]); ?>
                <?= $form->field($model, 'price') ?>
                <?= $form->field($model, 'ps_config_id')->label('Платежная система') ?>
                <hr>
                <?php  \iAvatar777\services\FormAjax\ActiveForm::end(['label' => 'Внести']); ?>
            </div>
        </div>


    </div>
</div>

<div class="modal fade" id="modalInfo" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="myModalLabel">Информация</h4>
            </div>
            <div class="modal-body">
                Успешно!
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
            </div>
        </div>
    </div>
</div>