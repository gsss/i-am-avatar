<?php

use yii\helpers\Html;


/** @var \yii\web\View $this */
/** @var \avatar\models\forms\BlagoProgram $program */
/** @var \common\models\school\School $school */
/** @var \school\models\validate\BlagoProgramIndexAjax $model */


$project = \common\models\SchoolKoopProject::findOne($program->project_id);

$this->title = $project->name;

?>


<div class="container">


    <div class="row">
        <div class="col-lg-6 col-lg-offset-3">
            <p><img src="<?= $school->image ?>" class="img-circle" width="100"></p>
            <p><?= $school->name ?></p>
            <hr>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-6 col-lg-offset-3">
            <h1 class="page-header text-center"><?= \yii\helpers\Html::encode($this->title) ?></h1>

        </div>
    </div>

    <div class="collapse in" id="step1">
        <p>Вы не авторизовались, поэтому или войдите в форме ниже и слева или зарегистрируйтесь в форме ниже и справа.</p>

        <!-- Авторизация           -->
        <div class="col-lg-6">
            <h2 class="page-header">Войти</h2>

            <?php $model1 = new \school\models\forms\ShopLogin(); ?>
            <?php $form = \yii\bootstrap\ActiveForm::begin([
                'id'                 => 'login-form',
                'enableClientScript' => false,
            ]); ?>

            <?= Html::hiddenInput(Html::getInputName($model1, 'school_id'), $school->id) ?>
            <?= $form->field($model1, 'login')->label('Логин/почта') ?>
            <?= $form->field($model1, 'password')->passwordInput()->label('Пароль') ?>
            <hr>
            <?php \yii\bootstrap\ActiveForm::end();?>
            <button class="btn btn-success buttonLogin" style="width: 100%;">Логин</button>
        </div>

        <div class="col-lg-6">
            <h2 class="page-header">Зарегистрироваться</h2>

            <?php $model2 = new \school\models\forms\ShopRegistration(); ?>
            <?php $form = \yii\bootstrap\ActiveForm::begin([
                'id'                 => 'registration-form',
                'enableClientScript' => false,
            ]); ?>
            <?= Html::hiddenInput(Html::getInputName($model2, 'school_id'), $school->id) ?>
            <?= $form->field($model2, 'login')->label('Логин/почта') ?>
            <?= $form->field($model2, 'password')->passwordInput()->label('Пароль') ?>
            <hr>
            <?php \yii\bootstrap\ActiveForm::end();?>
            <button class="btn btn-success buttonRegistration" style="width: 100%;">Регистрация</button>
        </div>
        <?php
        $this->registerJs(<<<JS
var functionLogin = function(e) {
    var b = $(this);
    b.off('click');
    var title = b.html();
    b.html($('<i>', {class: 'fa fa-spinner fa-spin fa-fw'}));
    b.attr('disabled', 'disabled');
    
    ajaxJson({
        url: '/blago-program/login',
        data: $('#login-form').serializeArray(),
        success: function(ret) {
            b.on('click', functionLogin);
            b.html(title);
            b.removeAttr('disabled');
            
            window.location.reload();
            // $('#step1').collapse('hide');
            // $('#step2').collapse('show');
        },
        errorScript: function(ret) {
            b.on('click', functionLogin);
            b.html(title);
            b.removeAttr('disabled');
            switch (ret.id) {
                case 102:
                    var f = $('#login-form');
                    for (var key in ret.data.errors) {
                        if (ret.data.errors.hasOwnProperty(key)) {
                            var name = key;
                            var value = ret.data.errors[key];
                            var t = f.find('.field-shoplogin-' + name);
                            t.addClass('has-error');
                            t.find('p.help-block-error').html(value.join('<br>')).show();
                        }
                    }
                    break;
            }
        }
    });
};
        
$('.buttonLogin').click(functionLogin);

var functionRegistration = function(e) {
    var b = $(this);
    b.off('click');
    var title = b.html();
    b.html($('<i>', {class: 'fa fa-spinner fa-spin fa-fw'}));
    b.attr('disabled', 'disabled');
    
    ajaxJson({
        url: '/blago-program/registration',
        data: $('#registration-form').serializeArray(),
        success: function(ret) {
            b.on('click', functionRegistration);
            b.html(title);
            b.removeAttr('disabled');
            
            $('#step1').collapse('hide');
            $('#step2').collapse('show');
        },
        errorScript: function(ret) {
            b.on('click', functionRegistration);
            b.html(title);
            b.removeAttr('disabled');
            switch (ret.id) {
                case 102:
                    var f = $('#registration-form');
                    for (var key in ret.data) {
                        if (ret.data.hasOwnProperty(key)) {
                            var name = key;
                            var value = ret.data[key];
                            var t = f.find('.field-shopregistration-' + name);
                            t.addClass('has-error');
                            t.find('p.help-block-error').html(value.join('<br>')).show();
                        }
                    }
                    break;
            }
        }
    });
};
$('.buttonRegistration').click(functionRegistration);

$('#login-form .form-control').on('focus', function() {
    var o = $(this);
    var p = o.parent();
    if (p.hasClass('input-group')) {
        p = p.parent();
    }
    p.removeClass('has-error');
    p.find('p.help-block-error').hide();
});

$('#registration-form .form-control').on('focus', function() {
    var o = $(this);
    var p = o.parent();
    if (p.hasClass('input-group')) {
        p = p.parent();
    }
    p.removeClass('has-error');
    p.find('p.help-block-error').hide();
});
JS
        )
        ?>



        <?php if (Yii::$app->user->isGuest) { ?>
        <?php } else { ?>
            <?php
            $this->registerJs(<<<JS
$('#step1').collapse('hide');
$('#step3').collapse('show');
JS
            )
            ?>
        <?php } ?>
    </div>

    <div class="collapse" id="step2">
        <p>Введите код из почты</p>

        <!-- Авторизация           -->
        <div class="col-lg-6">

        </div>

        <div class="col-lg-6">
            <h2 class="page-header">Код из почты</h2>

            <?php $model3 = new \school\models\forms\ShopRegistrationCode(); ?>
            <?php $form = \yii\bootstrap\ActiveForm::begin([
                'id'                 => 'code-form',
                'enableClientScript' => false,
            ]); ?>
            <?= Html::hiddenInput(Html::getInputName($model3, 'school_id'), $school->id) ?>
            <?= $form->field($model3, 'code')->label('Код') ?>
            <hr>
            <?php \yii\bootstrap\ActiveForm::end();?>
            <button class="btn btn-success buttonRegistrationCode" style="width: 100%;">Проверить и зарегистрироваться</button>
        </div>

        <?php
        $this->registerJs(<<<JS
var functionLoginCode = function(e) {
    var b = $(this);
    b.off('click');
    var title = b.html();
    b.html($('<i>', {class: 'fa fa-spinner fa-spin fa-fw'}));
    b.attr('disabled', 'disabled');
    
    ajaxJson({
        url: '/blago-program/registration-code',
        data: $('#code-form').serializeArray(),
        success: function(ret) {
            b.on('click', functionLoginCode);
            b.html(title);
            b.removeAttr('disabled');
            
            $('#step2').collapse('hide');
            $('#step3').collapse('show');
        },
        errorScript: function(ret) {
            b.on('click', functionLoginCode);
            b.html(title);
            b.removeAttr('disabled');
            switch (ret.id) {
                case 102:
                    var f = $('#login-form');
                    for (var key in ret.data.errors) {
                        if (ret.data.errors.hasOwnProperty(key)) {
                            var name = key;
                            var value = ret.data.errors[key];
                            var t = f.find('.field-shoplogin-' + name);
                            t.addClass('has-error');
                            t.find('p.help-block-error').html(value.join('<br>')).show();
                        }
                    }
                    break;
            }
        }
    });
};
        
$('.buttonRegistrationCode').click(functionLoginCode);

$('#code-form .form-control').on('focus', function() {
    var o = $(this);
    var p = o.parent();
    if (p.hasClass('input-group')) {
        p = p.parent();
    }
    p.removeClass('has-error');
    p.find('p.help-block-error').hide();
});
JS
        )
        ?>




    </div>


    <div class="collapse" id="step3">
        <div class="row">
            <div class="col-lg-6 col-lg-offset-3">
                <?php
                $form = \iAvatar777\services\FormAjax\ActiveForm::begin([
                    'model'   => $model,
                    'success' => <<<JS
function (ret) {
    window.location = '/billing/pay?id=' + ret.billing.id;
    // $('#modalInfo').on('hidden.bs.modal', function() {
    //     window.location = '/billing/pay?id=' + ret.billing.id
    // }).modal();
}
JS
                    ,
                ]); ?>

                <?= $form->field($model, 'price') ?>
                <?= $form->field($model, 'ps_config_id')->label('Платежная система') ?>
                <hr>

                <?php  \iAvatar777\services\FormAjax\ActiveForm::end(['label' => 'Внести']); ?>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-6 col-lg-offset-3">
            <hr>
            <?php $koop = \common\models\SchoolKoop::findOne(['school_id' => $school->id])  ?>
            <p>ИНН: <?= $koop->inn ?></p>
            <p>Адрес исп.органа: <?= $koop->address_isp ?></p>
            <p>email: <?= $koop->email ?></p>
            <p>Телефон: <?= $koop->phone ?></p>
        </div>
    </div>
</div>

<div class="modal fade" id="modalInfo" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="myModalLabel">Информация</h4>
            </div>
            <div class="modal-body">
                Успешно!
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
            </div>
        </div>
    </div>
</div>