<?php
use yii\helpers\Url;
use yii\helpers\Html;
/** @var $school \common\models\school\School */


$menu = null;
$design = $school->getSchoolDesign();
if (!is_null($design)) {
    if (!\cs\Application::isEmpty($design->guest_frontend)) {
        $data = \yii\helpers\Json::decode($design->guest_frontend);
        $menu = $data;
    }
}

if (is_null($menu)) {
    $menu[] = [
        'label' => Yii::t('c.oeVo5Oaket', 'Новости'),
        'route' => 'news/index',
        'urlList' => [
            'controller' => 'news'
        ]
    ];
    $menu[] = [
        'label' => Yii::t('c.oeVo5Oaket', 'Блог'),
        'route' => 'blog/index',
        'urlList' => [
            'controller' => 'blog'
        ]
    ];
    $menu[] = [
        'label' => Yii::t('c.oeVo5Oaket', 'О нас'),
        'route' => 'site/about',
    ];

    $menu[] = [
        'label' => Yii::t('c.oeVo5Oaket', 'Контакты'),
        'route' => 'site/contact',
    ];
}


?>

<?php
$rows = [];
foreach($menu as $item) {
    $class = ['dropdown'];
    if (\common\helpers\Pages::hasRoute1123123($item, Yii::$app->requestedRoute)) {
        $class[] = 'active';
    }
    if (\yii\helpers\ArrayHelper::keyExists('items', $item)) {
        $html = Html::tag(
            'li',
            Html::a(
                $item['label']
                .
                ' '
                .
                Html::tag('span', null, ['class' => 'caret'])
                ,
                '#'
                ,
                [
                    'class'         => "dropdown-toggle",
                    'data-toggle'   => "dropdown",
                    'role'          => "button",
                    'aria-expanded' => "false",
                ]
            )
            .
            Html::tag('ul',
                \common\helpers\Pages::liMenu1123122(\yii\helpers\ArrayHelper::map($item['items'], 'route', 'label'))
                , [
                    'class' => "dropdown-menu",
                    'role'  => "menu",
                ])
            ,
            ['class' => join(' ', $class)]);
    } else {
        $html = Html::tag(
            'li',
            Html::a($item['label'], [$item['route']]),
            ['class' => join(' ', $class)]
        );
    }

    $rows[] = $html;
}

?>
<?= join("\n", $rows) ?>
