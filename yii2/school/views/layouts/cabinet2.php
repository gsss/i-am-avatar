<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;

/* @var $this \yii\web\View */
/* @var $content string */

//\common\assets\GantelaLayout\FastClick::register($this);
//\common\assets\GantelaLayout\FontAwesome::register($this);
//\common\assets\GantelaLayout\NProgress::register($this);
//\common\assets\GantelaLayout\iCheck::register($this);
//\common\assets\GantelaLayout\CustomTheme::register($this);
\common\assets\GantelaLayout\GantelaLayout::register($this);
\avatar\assets\AjaxJson\Asset::register($this);

$school = \common\models\school\School::get();
$logo = '/images/logo144.png';
if ($school->image) {
    $logo = \iAvatar777\widgets\FileUpload7\FileUpload::getFile($school->image, 'crop');
}
$favicon = '/images/logo144.png';
if ($school->favicon) {
    $favicon = $school->favicon;
}

$this->registerJs(<<<JS
$('.js-buttonChat').click(function(e) {
    window.location  = '/chat/index';
})
JS
);

?>

<?php $this->beginPage(); ?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" href="<?= $favicon ?>">

    <?= Html::csrfMetaTags() ?>
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->

    <title><?= $this->title ?></title>
    <?php $this->head() ?>
</head>

<body class="nav-md">
<?php $this->beginBody(); ?>

<div class="container body">
    <div class="main_container">
        <?php $school = \common\models\school\School::get(); ?>
        <?php if (!is_null($school)) { ?>
            <div class="col-md-3 left_col">
                <div class="left_col scroll-view">


                    <div class="clearfix"></div>

                    <!-- menu profile quick info -->
                    <div class="profile clearfix">
                        <div class="profile_pic">
                            <a href="/">
                                <?php if (\cs\Application::isEmpty($school->image)) { ?>
                                    <img src="/images/school.jpg" alt="..." class="img-circle profile_img">
                                <?php } else { ?>
                                    <img src="<?= \iAvatar777\widgets\FileUpload7\FileUpload::getFile($school->image, 'crop') ?>" alt="..." class="img-circle profile_img">
                                <?php } ?>
                            </a>
                        </div>
                        <div class="profile_info">
                            <span>Сообщество</span>
                            <h2><?= $school->name ?></h2>
                        </div>
                    </div>
                    <!-- /menu profile quick info -->

                    <br />

                    <!-- sidebar menu -->
                    <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
                        <div class="menu_section">
                            <?= \common\widgets\MenuCompany\MenuCompany::widget([
                                'items' => [
                                    [
                                        'items' => [
                                            [
                                                'title' => 'Регистрация',
                                                'icon'  => 'fa fa-user',
                                                'items' => [
                                                    [
                                                        'title'   => 'Получение подписи',
                                                        'url'     => ['cabinet-digital-sign/index'],
                                                        'urlList' => [
                                                            ['route', 'cabinet-digital-sign/index'],
                                                        ],
                                                    ],
                                                    [
                                                        'title'   => 'Указание телефона',
                                                        'url'     => ['cabinet-profile-phone/index'],
                                                        'urlList' => [
                                                            ['route', 'cabinet-profile-phone/index'],
                                                        ],
                                                    ],
                                                    [
                                                        'title'   => 'Регистрация пайщика',
                                                        'url'     => ['koop/registration'],
                                                        'urlList' => [
                                                            ['controller', 'koop'],
                                                        ],
                                                    ],
                                                    [
                                                        'title'   => 'Внесение вступительного взноса',
                                                        'url'     => ['koop/pay1'],
                                                        'urlList' => [
                                                            ['route', 'koop/pay1'],
                                                        ],
                                                    ],
                                                    [
                                                        'title'   => 'Внесение паевого взноса',
                                                        'url'     => ['koop/pay2'],
                                                        'urlList' => [
                                                            ['route', 'koop/pay2'],
                                                        ],
                                                    ],
                                                ],
                                            ],
                                            [
                                                'title' => 'Задачи',
                                                'icon'  => 'fa fa-tasks',
                                                'items' => [
                                                    [
                                                        'title'   => 'Задачи',
                                                        'url'     => ['cabinet-task-list/index'],
                                                        'urlList' => [
                                                            ['controller', 'cabinet-task-list'],
                                                        ],
                                                    ],
                                                ],
                                            ],
                                            [
                                                'title' => 'Голосования',
                                                'icon'  => 'fa fa-hand-paper-o',
                                                'items' => [
                                                    [
                                                        'title'   => 'Участие',
                                                        'url'     => ['cabinet-vote-action/index', 'id' => $school->id],
                                                        'urlList' => [['controller', 'cabinet-vote-action']],
                                                    ],
                                                ],
                                            ],
                                            [
                                                'title' => 'Пр. кооператив',
                                                'icon'  => 'fa fa-cubes',
                                                'items' => [
                                                    [
                                                        'title'   => 'Подряды',
                                                        'url'     => ['cabinet-koop-podryad/index', 'id' => $school->id],
                                                        'urlList' => [
                                                            ['controller', 'cabinet-koop-podryad'],
                                                        ],
                                                    ],
                                                ],
                                            ],
                                            [
                                                'title' => 'Мои инвестиции',
                                                'icon'  => 'fa fa-money',
                                                'items' => [
                                                    [
                                                        'title'   => 'Список',
                                                        'url'     => ['cabinet-invest-list/index'],
                                                        'urlList' => [
                                                            ['controller', 'cabinet-invest-list'],
                                                        ],
                                                    ],
                                                ],
                                            ],
                                            [
                                                'title' => 'Документы',
                                                'icon'  => 'fa fa-file-text',
                                                'items' => [
                                                    [
                                                        'title'   => 'Список',
                                                        'url'     => ['cabinet-school-documents/index'],
                                                        'urlList' => [
                                                            ['controller', 'cabinet-school-documents'],
                                                        ],
                                                    ],
                                                ],
                                            ],
                                        ],
                                    ],
                                ],
                            ]) ?>
                        </div>
                    </div>
                    <!-- /sidebar menu -->

                    <!-- /menu footer buttons -->
                    <div class="sidebar-footer hidden-small">
                        <a data-toggle="tooltip" data-placement="top" title="Settings">
                            <span class="glyphicon glyphicon-cog" aria-hidden="true"></span>
                        </a>
                        <a data-toggle="tooltip" data-placement="top" title="FullScreen">
                            <span class="glyphicon glyphicon-fullscreen" aria-hidden="true"></span>
                        </a>
                        <a data-toggle="tooltip" data-placement="top" title="Lock">
                            <span class="glyphicon glyphicon-eye-close" aria-hidden="true"></span>
                        </a>
                        <a data-toggle="tooltip" data-placement="top" title="Logout" href="login.html">
                            <span class="glyphicon glyphicon-off" aria-hidden="true"></span>
                        </a>
                    </div>
                    <!-- /menu footer buttons -->
                </div>
            </div>
        <?php } ?>


        <!-- top navigation -->
        <div class="top_nav">
            <div class="nav_menu">
                <nav>
                    <div class="nav toggle">
                        <a id="menu_toggle"><i class="fa fa-bars"></i></a>
                    </div>

                    <ul class="nav navbar-nav navbar-right">

                        <li class="">
                            <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                <?php
                                /** @var \common\models\UserAvatar $user */
                                $user = Yii::$app->user->identity;
                                ?>
                                <img src="<?= $user->getAvatar() ?>" alt=""><?= $user->name_first ?>
                                <span class=" fa fa-angle-down"></span>
                            </a>
                            <?= $this->render('user-menu2', ['school' => $school]) ?>
                        </li>
                        <li class="" data-toggle="tooltip" title="Кошелек" data-placement="bottom">
                            <a href="/cabinet-wallet/index">
                                <img src="/images/controller/cabinet-bills/index/wallet.png" width="30">
                            </a>
                        </li>
                    </ul>
                </nav>
            </div>
        </div>
        <!-- /top navigation -->

        <!-- page content -->
        <div class="right_col" role="main">
            <div class="">



                <?= $content ?>



            </div>
        </div>
        <!-- /page content -->

        <!-- footer content -->
        <footer>
            <div class="pull-right">
                <?php
                $path = Yii::getAlias('@avatar/../composer.json');
                $content = file_get_contents($path);
                $data = \yii\helpers\Json::decode($content);

                ?>
                <p>(с) Люди для Людей <?= $data['version'] ?></p>
            </div>
            <div class="clearfix"></div>
        </footer>
        <!-- /footer content -->
    </div>
</div>


<?php $this->endBody() ?>

</body>
</html>
<?php $this->endPage() ?>
