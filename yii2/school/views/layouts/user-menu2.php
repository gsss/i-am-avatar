<?php
/**
 * Created by PhpStorm.
 * User: Бог-Творец
 * Date: 18.05.2016
 * Time: 7:45
 *
 */

use yii\helpers\Url;
use yii\helpers\Html;

/** @var $this \yii\web\View */
/** @var $school \common\models\school\School */


?>
<ul class="dropdown-menu" role="menu">
    <li><a href="<?= Url::to(['cabinet/index']) ?>"><?= \Yii::t('c.qvYMPl9ABq', 'Мой кабинет') ?></a></li>

    <li class="divider"></li>
    <li><a href="<?= Url::to(['cabinet/profile']) ?>"><i class="glyphicon glyphicon-cog" style="padding-right: 5px;"></i><?= \Yii::t('c.qvYMPl9ABq', 'Мой профиль') ?></a></li>
    <li><a href="<?= Url::to(['cabinet-cards/index']) ?>"><i class="glyphicon glyphicon-credit-card" style="padding-right: 5px;"></i>Карты</a></li>

    <?php if (\common\models\school\CommandLink::find()->where(['user_id' => Yii::$app->user->id, 'school_id' => $school->id])->exists()) { ?>
        <li><a href="<?= Url::to(['cabinet-task-list/index']) ?>"><i class="glyphicon glyphicon-check" style="padding-right: 5px;"></i>Задачи</a></li>
    <?php }?>
    <li class="divider"></li>

    <li><a href="<?= Url::to(['auth/logout']) ?>" data-method="post"><i class="glyphicon glyphicon-off" style="padding-right: 5px;"></i><?= \Yii::t('c.qvYMPl9ABq', 'Выйти') ?></a></li>
</ul>