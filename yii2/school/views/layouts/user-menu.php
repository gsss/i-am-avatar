<?php
/**
 * Created by PhpStorm.
 * User: Бог-Творец
 * Date: 18.05.2016
 * Time: 7:45
 *
 */

use yii\helpers\Url;
use yii\helpers\Html;

/** @var $this \yii\web\View */
/** @var $school \common\models\school\School */

?>
<li>
    <p style="margin-top: 13px; margin-right: 5px; margin-left: 5px;">
        <a href="/cabinet-wallet/index">
            <img src="https://platform.qualitylive.su/images/controller/cabinet-bills/index/wallet.png" width="30" data-toggle="tooltip" title="Кошелек" data-placement="bottom">
        </a>
    </p>
</li>
<li class="dropdown" id="userBlockLi">
    <a
            href="#"
            class="dropdown-toggle"
            data-toggle="dropdown"
            aria-expanded="false"
            role="button"
            style="padding: 5px 10px 5px 10px;"
    >
        <?= Html::img(Yii::$app->user->identity->getAvatar(), [
            'height' => '40px',
            'class'  => 'img-circle'
        ]) ?>
        <span class="caret"></span>
    </a>
    <ul class="dropdown-menu" role="menu">
        <li><a href="<?= Url::to(['cabinet/index']) ?>"><?= \Yii::t('c.qvYMPl9ABq', 'Мой кабинет') ?></a></li>

        <li class="divider"></li>
        <li><a href="<?= Url::to(['cabinet/profile']) ?>"><i class="glyphicon glyphicon-cog" style="padding-right: 5px;"></i><?= \Yii::t('c.qvYMPl9ABq', 'Мой профиль') ?></a></li>
        <li><a href="<?= Url::to(['cabinet-cards/index']) ?>"><i class="glyphicon glyphicon-credit-card" style="padding-right: 5px;"></i>Карты</a></li>

        <?php if (\common\models\school\CommandLink::find()->where(['user_id' => Yii::$app->user->id, 'school_id' => $school->id])->exists()) { ?>
            <li><a href="<?= Url::to(['cabinet-task-list/index']) ?>"><i class="glyphicon glyphicon-check" style="padding-right: 5px;"></i>Задачи</a></li>
        <?php }?>
        <li class="divider"></li>

        <li><a href="<?= Url::to(['auth/logout']) ?>" data-method="post"><i class="glyphicon glyphicon-off" style="padding-right: 5px;"></i><?= \Yii::t('c.qvYMPl9ABq', 'Выйти') ?></a></li>
    </ul>
</li>

