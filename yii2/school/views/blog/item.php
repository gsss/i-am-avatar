<?php
use common\widgets\FileUpload3\FileUpload;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $item \common\models\blog\Article */
/* @var $school \common\models\school\School */

$this->title = $item->name;

$header_id = $school->blog_header_id;
$footer_id = $school->blog_footer_id;

if (!empty($header_id)) {
    $header = \common\models\school\Page::findOne($header_id);
    foreach ($header->getBlocks() as $block) {
        echo $block->render();
    }
}

$this->registerMetaTag(['name' => 'robots', 'content' => 'index, follow']);

?>
<style>
    .contentBlock p, .contentBlock li {
        font-size: 130%;
    }
</style>
<div class="container" style="padding-bottom: 70px;">
    <div class="col-lg-12">
        <h1 class="page-header text-center" style="margin-top: 100px;"><?= $this->title ?></h1>
        <p class="text-center" style="color: #888;"><?= Yii::$app->formatter->asDate($item->created_at) ?></p>
    </div>
    <div class="col-lg-8 col-lg-offset-2 contentBlock">
        <p>
            <img src="<?= $item->image ?>" width="100%" class="thumbnail"/>
        </p>

        <?= $item->content ?>
        <hr>
        <?php if ($item->is_signed) { ?>
        <?php $document = \common\models\UserDocument::findOne($item->document_id); ?>
            <p><a href="https://etherscan.io/tx/<?= $document->txid ?>" target="_blank"><img
                    src="/images/office-document-pencil-512.png" width="60"
                    data-toggle="tooltip"
                    data-placement="right"
                    title="Этот документ подписан цифровой подписью, нажмите чтобы посмотреть подробности"
                    ></a></p>
            <p><a href="https://etherscan.io/tx/<?= $document->txid ?>" target="_blank"><code><?= $document->txid ?></code></a></p>
        <?php } ?>

        <p class="text-center">
            <?php if (isset($item->link)) { ?>
                <?php if ($item->link != '') { ?>
                    <?= Html::a('Ссылка на источник »', $item->link, [
                        'class'  => 'btn btn-primary',
                        'target' => '_blank',
                    ]) ?>
                <?php } ?>
            <?php } ?><a href="<?= Url::to(['blog/index']) ?>" class="btn btn-default">Назад к блогу</a></p>
        <hr>
        <?= \avatar\modules\Comment\Module::getComments2(8, $item->id); ?>
        <hr>

        <center>
            <?php
            $description = $item->description;
            if (is_null($description)) {
                $description = \cs\services\Str::sub(strip_tags($item->content), 0, 100);
            } else {
                if ($description == '') {
                    $description = \cs\services\Str::sub(strip_tags($item->content), 0, 100);
                }
            }
            ?>
            <?= $this->render('../blocks/share', [
                'image'       => $item->image,
                'title'       => $this->title,
                'url'         => $school->getUrl(Yii::$app->request->getUrl()),
                'description' => $description,
            ]) ?>
        </center>

    </div>
</div>


<?php

if (!empty($footer_id)) {
    $footer = \common\models\school\Page::findOne($footer_id);
    foreach ($footer->getBlocks() as $block) {
        echo  $block->render();
    }
}

?>