<?php
use common\widgets\FileUpload3\FileUpload;
use yii\helpers\Html;
use yii\helpers\Url;

/** @var $this yii\web\View */
/** @var $kurs \common\models\school\Kurs */
/** @var $potok \common\models\school\Potok */
/** @var $event \common\models\school\Event */
/** @var $data array */

\avatar\assets\Paralax::register($this);
?>

<div class="parallax-window" data-parallax="scroll" data-image-src="https://cloud1.cloud999.ru/upload/cloud/15717/77126_a4zs5njNer.jpg"
     style="min-height: 650px; background: transparent;" id="1">


    <div class="container" style="padding-top: 150px;">
        <div class="col-lg-6">
            <h1><?= $data[1]['name'] ?></h1>
        </div>
        <div class="col-lg-6">
            <h3><i class="fa fa-location-arrow"></i> <?= $data[3]['place'] ?></h3>
            <h3><i class="fa fa-calendar-check-o"></i> <?= $data[2]['date_start'] ?></h3>
            <h3><i class="fa fa-clock-o"></i> 12:21</h3>
        </div>

    </div>

</div>
<div class="container">
    <div class="col-lg-12">
        <h1 class="page-header text-center">Кто ведет</h1>

    </div>
    <?php
    $ids = explode(',', $data[4]['masters']);
    $users = [];
    foreach ($ids as $id) {
        try {
            $users[] = \common\models\UserAvatar::findOne($id);
        } catch (Exception $e) {

        }
    }
    ?>
    <?php foreach ($users as $user) { ?>
        <div class="col-lg-4">
            <h3><?= $user->getName2() ?></h3>
            <p class="text-center">
                <img src="<?= $user->getAvatar() ?>" width="50%" class="img-circle">
            </p>
        </div>
    <?php } ?>
</div>
<div class="container">
    <pre><?= \yii\helpers\VarDumper::dumpAsString($data) ?></pre>

</div>

<div class="container">
    <div class="col-lg-12">
        <h2 class="page-header text-center">Место проведения</h2>
    </div>
</div>

<div id="map" style="width: 100%; height: 450px;"></div>

<?php
\common\assets\YandexMaps::register($this);

$lat = $data[2]['place-lat'];
$lng = $data[2]['place-lng'];
$place = $data[2]['place'];
$place_name = $data[2]['place_name'];

$this->registerJs(<<<JS

ymaps.ready(init);

function init() {
    var myMap = new ymaps.Map("map", {
            center: [{$lng}, {$lat}],
            zoom: 16
        }, {
            searchControlProvider: 'yandex#search'
        });
    var rows = [{"point":"{$lng}, {$lat}","title":"{$place_name}","description":"{$place}"}];
    var i;
    var item;
    var point;
    var html;
    
    for(i = 0; i < rows.length; i++) {
        item = rows[i];
        point = item.point.split(', ');
        html = $('<b>').html(item.title)[0].outerHTML;
        html += '<br>';
        html += item.description;
        
        myMap.geoObjects
        .add(new ymaps.Placemark([point[0], point[1]], {
            balloonContent: html
        }, {
            preset: 'islands#icon',
            iconColor: '#0095b6'
        }))
        ;
    }
}
JS
);
?>
