<?php

use common\widgets\FileUpload3\FileUpload;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $kurs \common\models\school\Kurs */
/* @var $potok \common\models\school\Potok */
/* @var $event \common\models\school\Event */

$this->title = $potok->name;


$data = \yii\helpers\Json::decode($event->config);
?>
<?php if ($data[3]['is_online']) { ?>
    <?= $this->render('online', [
        'data'  => $data,
        'potok' => $potok,
        'kurs'  => $kurs,
        'event' => $event,
    ]) ?>
<?php } else { ?>
    <?= $this->render('offline', [
        'data'  => $data,
        'potok' => $potok,
        'kurs'  => $kurs,
        'event' => $event,
    ]) ?>
<?php } ?>