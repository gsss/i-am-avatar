<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;
use yii\db\Query;
use yii\helpers\ArrayHelper;

/** @var $this yii\web\View */
/** @var $request \common\models\shop\Request */

$this->title = 'Оплата заявки #' . $request->id;

$billing = \common\models\BillingMain::findOne($request->billing_id);

/** @var \common\models\PaySystemConfig $config */
$config = \common\models\PaySystemConfig::findOne($billing->config_id);

$class = $config->getClass();
$ps = $config->getPaySystem();
$price = $request->sum;

// Получаю идентификато класса
$classObject = \common\models\BillingMainClass::findOne(['name' => '\\' . $request::className()]);
if (is_null($classObject)) {
    throw new Exception('Нет \common\models\BillingMainClass');
}

$s = \common\models\school\School::findOne($request->school_id);



?>
    <div class="container">
        <div class="col-lg-12">
            <h1 class="page-header"><?= Html::encode($this->title) ?></h1>

            <?= $class->getForm(
                $billing,
                'Оплата заявки #' . $request->id,
                'Получатель: ' . $s->name,
                [
                    'action'     => 'shopRequest.' . $request->id,
                    'successURL' => '/cabinet/shop-requests-item?id=' . $request->id,
                ]
            ); ?>
        </div>
    </div>

<?php

// подтверждение платежа

$successObject = \common\models\PaySystemSuccess::findOne(['paysystem_id' => $ps->id]);

$view = '';
if (!is_null($successObject)) {
    $view = $successObject->view;

    $this->registerJs(<<<JS
$('#buttonSuccess').click(function(e) {
    $('#modalInfo').modal();
});
$('#modalInfo .buttonConfirm').click(function(e) {
    ajaxJson({
        url: '/cabinet-billing/success?id=' + {$billing->id},
        data: $('#formSuccess').serializeArray(),
        success: function(ret) {
            window.location = '/cabinet/shop-requests-item?id=' + {$request->id};
        }
    })
});

JS
    );
} else {
    $this->registerJs(<<<JS
$('#buttonSuccess').click(function(e) {
    ajaxJson({
        url: '/cabinet-billing/success?id=' + {$billing->id},
        success: function(ret) {
            window.location = '/cabinet/shop-requests-item?id=' + {$request->id};
        }
    })
});
JS
    );

}
?>

<?php if (!is_null($successObject)) { ?>
    <div class="modal fade" id="modalInfo" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Информация</h4>
                </div>
                <div class="modal-body">
                    <?php $form = ActiveForm::begin([
                        'id'        => 'formSuccess',
                        'options'   => ['enctype' => 'multipart/form-data']
                    ]);
                    $model = $successObject->model;
                    $modelObject = new $model(['billing' => $billing]);
                    ?>

                    <?= $this->render($view, [
                        'form'  => $form,
                        'model' => $modelObject,
                    ]); ?>

                    <?php ActiveForm::end(); ?>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-success buttonConfirm">Отправить</button>
                    <button type="button" class="btn btn-default " data-dismiss="modal">Закрыть</button>
                </div>
            </div>
        </div>
    </div>
<?php } ?>