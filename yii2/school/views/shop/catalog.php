<?php

use common\models\avatar\Currency;
use common\models\avatar\UserBill;
use yii\helpers\Html;
use yii\helpers\Url;

/** @var $this yii\web\View */
/** @var $school \common\models\school\School */
/** @var $catalog \common\models\shop\CatalogItem */

$this->title = 'Каталог. '. $catalog->name;


?>

<div class="container" style="padding-bottom: 70px;">
    <div class="col-lg-12">
        <h1 class="page-header text-center">
            <?= $this->title ?>
        </h1>
    </div>

    <div class="col-lg-4">
        <h2 class="page-header">
            Категории
        </h2>
        <ul>
            <?php foreach (\common\models\shop\CatalogItem::find()->where(['school_id' => $school->id])->all() as $c) { ?>
                <li><a href="<?= Url::to(['shop/catalog', 'id' => $c->id]) ?>"><?= $c->name ?></a></li>
            <?php } ?>
        </ul>

    </div>
    <div class="col-lg-8">
        <h2 class="page-header">
            Все товары
        </h2>
        <div class="row">
            <?php /** @var \common\models\shop\Product $product */  ?>
            <?php foreach (\common\models\shop\Product::find()->where(['school_id' => $school->id, 'tree_node_id' => $catalog->id])->all() as $product) {  ?>
                <div class="col-lg-4">
                    <p>
                        <a href="<?= Url::to(['shop/item','id' => $product->id]) ?>">
                            <img class="thumbnail" src="<?= \common\widgets\FileUpload7\FileUpload::getFile($product->image, 'crop') ?>" width="100%">
                        </a>
                    </p>
                    <p class="text-center"><?= $product->name ?></p>
                    <p><a href="<?= Url::to(['shop/order','id' => $product->id]) ?>" class="btn btn-success" style="width: 100%">Купить</a></p>
                </div>
            <?php } ?>
        </div>

    </div>
</div>


