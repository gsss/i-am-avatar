<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \avatar\models\forms\DeviceConfirm */

$this->title = 'Подтверждение устройства';
?>
<div class="container">

    <div class="col-lg-12">
        <h1 class="page-header text-center"><?= Html::encode($this->title) ?></h1>
    </div>
    <div class="col-lg-4 col-lg-offset-4">
        <p>Вы вошли с устройсва которое незарегистрировано у вас в списке доверительных, поэтому вам нужно подтвердить
            его введя код, который был отправлен вам на почту которую вы указывали при регистрации.</p>
        <?php $form = ActiveForm::begin(); ?>
        <?= $form->field($model, 'code')->label('Код подтверждения') ?>
        <hr>
        <div class="form-group">
            <?= Html::submitButton('Подтвердить', [
                'class' => 'btn btn-success',
                'name'  => 'login-button',
                'style' => 'width:100%',
            ]) ?>
        </div>
        <?php ActiveForm::end(); ?>
    </div>

</div>