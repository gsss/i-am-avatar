<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \avatar\models\forms\DeviceConfirm */


$this->title = Yii::t('c.HS07bGa1P0', 'Двухфакторная авторизация');


$this->registerJs(<<<JS
$('#googleconfirm-code').focus();
JS
);
?>
<div class="container">

    <div class="col-lg-12">
        <h1 class="page-header text-center"><?= Html::encode($this->title) ?></h1>
    </div>
    <div class="col-lg-4 col-lg-offset-4">
        <p class="text-center"><img src="/images/controller/auth/google-confirm/ga2.png" width="150"></p>
        <p><?= \Yii::t('c.HS07bGa1P0', 'У вас включена двухфакторная авторизация') ?></p>
        <?php $form = ActiveForm::begin(); ?>
        <?= $form->field($model, 'code', ['inputOptions' => ['placeholder' => '000000']])->label(Yii::t('c.HS07bGa1P0', 'Код подтверждения')) ?>
        <hr>
        <div class="form-group">
            <?= Html::submitButton(Yii::t('c.HS07bGa1P0', 'Подтвердить'), [
                'class' => 'btn btn-success',
                'name'  => 'login-button',
                'style' => 'width:100%',
            ]) ?>
        </div>
        <?php ActiveForm::end(); ?>
    </div>

</div>