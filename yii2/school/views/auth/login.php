<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \avatar\models\forms\Login */
/* @var $school \common\models\school\School */

$this->title = Yii::t('c.Q2vFnrwWB1', 'Вход в личный кабинет');


if ($school->image) {
    $school_subscribe_image = $school->image;
} else {
    $school_subscribe_image = null;
}
if (\yii\helpers\StringHelper::startsWith($school_subscribe_image, '/')) $school_subscribe_image = \yii\helpers\Url::to($school_subscribe_image, true);


$header_id = $school->header_id;
$footer_id = $school->footer_id;

$schoolDesign = \common\models\school\SchoolDesign::findOne(['school_id' => $school->id]);
if (!is_null($schoolDesign)) {
    if (!\cs\Application::isEmpty($schoolDesign->login_header_id)) {
        $header_id = $schoolDesign->login_header_id;
    }
    if (!\cs\Application::isEmpty($schoolDesign->login_footer_id)) {
        $footer_id = $schoolDesign->login_footer_id;
    }
}

if (!empty($header_id)) {
    $header = \common\models\school\Page::findOne($header_id);
    foreach ($header->getBlocks() as $block) {
        echo $block->render();
    }
}

?>
<div class="container">

    <div class="col-lg-12">
        <h1 class="page-header text-center"><?= Html::encode($this->title) ?></h1>
    </div>
    <div class="col-lg-4 col-lg-offset-4">

        <?php if ($school_subscribe_image) { ?>
            <p style="text-align: center">
                <img src="<?= $school_subscribe_image ?>" width="200">
            </p>
            <p style="text-align: center">
                <?= $school->name ?>
            </p>
        <?php } ?>

        <p><span style="color: red"><?= \Yii::t('c.Q2vFnrwWB1', 'Важно') ?>!</span>: <?= \Yii::t('c.Q2vFnrwWB1', 'Убедитесь, что вы находитесь на сайте') ?> <?= $school->getUrl() ?></p>

        <p><?= \Yii::t('c.Q2vFnrwWB1', 'Пожалуйста заполните следующие поля для входа') ?>:</p>

        <?php $form = ActiveForm::begin(); ?>

        <?= $form->field($model, 'username', ['inputOptions' => ['placeholder' => 'Email']])->label('Email', ['class' => 'hide']) ?>
        <?= $form
            ->field(
                $model,
                'password',
                ['inputOptions' => ['placeholder' => Yii::t('c.0uMdJb0e0n', 'Пароль')]]
            )
            ->passwordInput()
            ->label(Yii::t('c.0uMdJb0e0n', 'Пароль'), ['class' => 'hide']) ?>

        <a href="/auth/password-recover"><?= \Yii::t('c.Q2vFnrwWB1', 'Восстановить пароль') ?></a>
        <hr>
        <div class="form-group">
            <?= Html::submitButton(Yii::t('c.Q2vFnrwWB1', 'Вход'), [
                'class' => 'btn btn-success',
                'name'  => 'login-button',
                'style' => 'width:100%',
            ]) ?>
        </div>
        <p><a href="/auth/registration" class="btn btn-default" style="width: 100%;"><?= \Yii::t('c.Q2vFnrwWB1', 'Регистрация') ?></a></p>
        <?php ActiveForm::end(); ?>
    </div>

</div>


<?php

if (!empty($footer_id)) {
    $footer = \common\models\school\Page::findOne($footer_id);
    foreach ($footer->getBlocks() as $block) {
        echo  $block->render();
    }
}

?>