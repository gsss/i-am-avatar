<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \avatar\models\forms\Registration */
/* @var $school \common\models\school\School */

if ($school->image) {
    $school_subscribe_image = $school->image;
} else {
    $school_subscribe_image = null;
}
if (\yii\helpers\StringHelper::startsWith($school_subscribe_image, '/')) $school_subscribe_image = \yii\helpers\Url::to($school_subscribe_image, true);


$this->title = 'Регистрация';

$header_id = $school->header_id;
$footer_id = $school->footer_id;

$schoolDesign = \common\models\school\SchoolDesign::findOne(['school_id' => $school->id]);
if (!is_null($schoolDesign)) {
    if (!\cs\Application::isEmpty($schoolDesign->registration_header_id)) {
        $header_id = $schoolDesign->registration_header_id;
    }
    if (!\cs\Application::isEmpty($schoolDesign->registration_footer_id)) {
        $footer_id = $schoolDesign->registration_footer_id;
    }
}

if (!empty($header_id)) {
    $header = \common\models\school\Page::findOne($header_id);
    foreach ($header->getBlocks() as $block) {
        echo $block->render();
    }
}
?>

<div class="container">
    <div class="col-lg-4 col-lg-offset-4">


        <?php if ($school_subscribe_image) { ?>
            <p style="text-align: center">
                <img src="<?= \common\widgets\FileUpload7\FileUpload::getFile($school_subscribe_image, 'crop') ?>" width="200" class="img-circle">
            </p>
            <p style="text-align: center">
                <?= $school->name ?>
            </p>
        <?php } ?>

        <h1 class="page-header text-center"><?= Html::encode($this->title) ?></h1>
        <?php if (Yii::$app->session->hasFlash('contactFormSubmitted')): ?>
            <div class="alert alert-success">
                <?= \Yii::t('c.0uMdJb0e0n', 'Благодрим вас за регистрацию') ?>
            </div>
        <?php else: ?>
            <?php $form = ActiveForm::begin([
                'id'                   => 'contact-form',
                'enableAjaxValidation' => true,
            ]); ?>
            <?php $field = $form->field($model, 'email', ['inputOptions' => ['placeholder' => 'email']])->label(Yii::t('c.0uMdJb0e0n', 'Почта'), ['class' => 'hide']);
            $field->validateOnBlur = true;
            $field->validateOnChange = true;
            echo $field;
            ?>
            <?= $form->field($model, 'password1', ['inputOptions' => ['placeholder' => Yii::t('c.0uMdJb0e0n', 'Пароль')]])->passwordInput()->label(Yii::t('c.0uMdJb0e0n', 'Пароль'), ['class' => 'hide']) ?>
            <?= $form->field($model, 'password2', ['inputOptions' => ['placeholder' => Yii::t('c.0uMdJb0e0n', 'Повторите пароль')]])->passwordInput()->label(Yii::t('c.0uMdJb0e0n', 'Повторите пароль'), ['class' => 'hide']) ?>

            <?= $form->field($model, 'verificationCode')->widget('\yii\captcha\Captcha') ?>
            <hr>
            <div class="form-group">
                <?= Html::submitButton(Yii::t('c.0uMdJb0e0n', 'Зарегистрироваться'), [
                    'class' => 'btn btn-primary',
                    'name'  => 'contact-button',
                    'style' => 'width: 100%',
                ]) ?>
            </div>
            <?php ActiveForm::end(); ?>
        <?php endif; ?>
    </div>
</div>

<?php

if (!empty($footer_id)) {
    $footer = \common\models\school\Page::findOne($footer_id);
    foreach ($footer->getBlocks() as $block) {
        echo  $block->render();
    }
}

?>