<?php
/**
 * Created by PhpStorm.
 * User: Metatron
 * Date: 22.12.2018
 * Time: 5:44
 */

/** @var \yii\web\View $this */
/** @var \common\models\school\Page $page */

$school = $page->getSchool();

if ($page->facebook_image) {
    $this->registerMetaTag(['name' => 'og:image', 'content' => '//platform.qualitylive.su/' . $page->facebook_image]);
    $this->registerMetaTag(['name' => 'og:url', 'content' => $page->getUrl()]);
    $this->registerMetaTag(['name' => 'og:title', 'content' => $page->facebook_title]);
    $this->registerMetaTag(['name' => 'og:description', 'content' => $page->facebook_description]);
    $this->registerMetaTag(['name' => 'og:shortcut icontype', 'content' => 'website']);
}
if ($page->title) {
    $this->title = $page->title;
}
if ($page->favicon) {
    $this->registerCssFile($page->favicon, ['rel' => 'shortcut icon']);
} else {
    if ($school->favicon) {
        $this->registerCssFile($page->favicon, ['rel' => 'shortcut icon']);
    }
}

if ($page->description) {
    $this->registerMetaTag(['name' => 'description', 'content' => $page->description]);
}
if ($page->keywords) {
    $this->registerMetaTag(['name' => 'keywords', 'content' => $page->keywords]);
}


$header_id = $page->header_id;
if ($header_id == -2) {
    $header_id = null;
} else {
    if (empty($header_id)) {
        $header_id = $page->getSchool()->header_id;
    }
}


$footer_id = $page->footer_id;
if (empty($footer_id)) {
    $footer_id = $page->getSchool()->footer_id;
}

if (!empty($header_id)) {
    $header = \common\models\school\Page::findOne($header_id);
    foreach ($header->getBlocks() as $block) {
        echo $block->render();
    }
}


/** @var \common\models\school\PageBlockContent $block */
foreach ($page->getBlocks() as $block) {
    echo \yii\helpers\Html::tag('a', null, ['name' => 'block' . $block->id]);
    echo $block->render();
}

if (!empty($footer_id)) {
    $footer = \common\models\school\Page::findOne($footer_id);
    foreach ($footer->getBlocks() as $block) {
        echo $block->render();
    }
}

if ($page->body) {
    echo $page->body;
}

?>

