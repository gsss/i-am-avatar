<?php

/** @var $this \yii\web\View */
/** @var $item \common\models\SchoolInvestEvent */


use yii\helpers\Url;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use kartik\grid\GridView;

$this->title = 'Инвестиция #'.$item->id . ' от '.Yii::$app->formatter->asDate($item->created_at);

/** @var $school \common\models\school\School */
$school = \common\models\school\School::get();



?>

<div class="container">
    <div class="col-lg-12">
        <h1 class="page-header"><?= Html::encode($this->title) ?></h1>
    </div>

    <div class="col-lg-12">

        <?= \yii\widgets\DetailView::widget(['model' => $item]) ?>

    </div>
</div>

