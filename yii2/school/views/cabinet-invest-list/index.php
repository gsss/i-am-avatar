<?php

/** @var $this \yii\web\View */


use yii\helpers\Url;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use kartik\grid\GridView;

$this->title = 'Мои инвестиции';

/** @var $school \common\models\school\School */
$school = \common\models\school\School::get();



?>

<div class="container">
    <div class="col-lg-12">
        <h1 class="page-header"><?= Html::encode($this->title) ?></h1>
    </div>

    <div class="col-lg-12">

        <?php \yii\widgets\Pjax::begin(); ?>
        <?php
        $this->registerJS(<<<JS


$('.rowTable').click(function() {
    window.location = '/cabinet-invest-list/view?id=' + $(this).data('id');
});
JS
        );
        $sort = new \yii\data\Sort(
            [
                'attributes'   => [
                    'id'         => ['label' => "ID"],
                    'created_at',
                ],
                'defaultOrder' => [
                    'created_at' => SORT_DESC,
                ],
            ]
        );
        ?>
        <?= \yii\grid\GridView::widget([
            'dataProvider' => new \yii\data\ActiveDataProvider([
                    'query' => \common\models\SchoolInvestEvent::find()
                        ->where(['user_id' => Yii::$app->user->id])
                        ->select([
                            'school_invest_event.*',
                            'billing.is_paid',
                            'school_invest_program.project_id',
                        ])
                        ->innerJoin('billing', 'billing.id = school_invest_event.billing_id')
                        ->innerJoin('school_invest_program', 'school_invest_program.id = school_invest_event.program_id')
                        ->asArray(),
                    'sort' => $sort

            ]),
            'tableOptions' => [
                'class' => 'table table-striped table-hover',
            ],
            'rowOptions'   => function ($item) {
                return [
                    'data'  => ['id' => $item['id']],
                    'role'  => 'button',
                    'class' => 'rowTable',
                ];
            },
            'columns'      => [
                [
                    'attribute'     => 'id',
                    'header'        => $sort->link('id'),
                    'headerOptions' => [
                        'style' => 'width: 10%',
                    ],
                ],

                [
                    'header'  => 'сумма',
                    'contentOptions' => ['class' => 'text-right'],
                    'headerOptions' => ['class' => 'text-right'],
                    'content' => function ($item) {
                        $c = \common\models\piramida\Currency::findOne($item['currency_id']);
                        if (is_null($c)) return '';

                        return Yii::$app->formatter->asDecimal($item['amount'] / pow(10, $c->decimals), $c->decimals);
                    }
                ],
                [
                    'header'  => 'Валюта',
                    'content' => function ($item) {
                        $c = \common\models\piramida\Currency::findOne($item['currency_id']);
                        if (is_null($c)) return '';

                        return Html::tag('span', $c->code, ['class' => 'label label-info']);
                    }
                ],
                [
                    'header'  => 'Создано',
                    'content' => function ($item) {
                        $v = \yii\helpers\ArrayHelper::getValue($item, 'created_at', 0);
                        if ($v == 0) return '';

                        return Html::tag('abbr', \cs\services\DatePeriod::back($v, ['isShort' => true]), ['data' => ['toggle' => 'tooltip'], 'title' => Yii::$app->formatter->asDatetime($v)]);
                    }
                ],
                [
                    'header'  => 'Оплачено',
                    'content' => function ($item) {
                        $v = \yii\helpers\ArrayHelper::getValue($item, 'is_paid', 0);

                        if ($v) {
                            $html = Html::tag('span', 'Да', ['class' => 'label label-success']);
                        } else {
                            $html = Html::tag('span', 'Нет', ['class' => 'label label-default']);
                        }

                        return $html;
                    },
                ],
                [
                    'header'  => 'Проект',
                    'content' => function ($item) {
                        $program = \common\models\SchoolInvestProgram::findOne($item['program_id']);
                        $project = \common\models\SchoolKoopProject::findOne($program['project_id']);

                                                return $project->name;
                    },
                ],

            ],
        ]) ?>
        <?php \yii\widgets\Pjax::end(); ?>

    </div>
</div>

