<?php
/**
 * Created by PhpStorm.
 * User: Metatron
 * Date: 22.12.2018
 * Time: 7:03
 */

/** @var string $data json */
/** @var \common\models\school\PageBlockContent $blockContent */

$object = \yii\helpers\Json::decode($data);

$fileNameArray = explode('/', __FILE__);
$file =$fileNameArray[count($fileNameArray)-1];
$arr = explode('-', $file);
$numStr = $arr[0];
$num = (int)$arr[0];

$this->registerJs(<<<JS


$('.buttonSave{$numStr}').click(function(e) {
    var type = $(this).data('type_id');
    var modal = $('#modalEdit' + type);
    var id = modal.data('id');
    ajaxJson({
        url: '/cabinet-school-pages-constructor/block-save',
        data: {
            id: id,
            form: {
                name: modal.find('input[name="name"]').val(),
                date: modal.find('input[name="date"]').val(),
                time: modal.find('input[name="time"]').val(),
                image: modal.find('div[data-id="picture"] img').attr('src')
            }
        },
        success: function(ret) {
              window.location.reload();
        }
    });
});

$('.buttonSettings[data-type="{$num}"]').click(function(e) {
    // Идентификатор блока контента
    var id = $(this).data('id');
    var type = $(this).data('type');
    var modal = $('#modalEdit' + type);
    modal.data('id', id);
    ajaxJson({
        url: '/cabinet-school-pages-constructor/block-get',
        data: {
            id: id
        },
        success: function(ret) {
            modal.find('div[data-id="picture"]').html($('<img>', {src: ret.image, style:'width:100%;max-width:200px'})); 
            modal.find('input[name="date"]').val(ret.date);
            modal.find('input[name="time"]').val(ret.time);
            modal.find('input[name="name"]').val(ret.name);
            $('#modalEdit' + type).modal();
        }
    });
});


JS
);
?>
<div class="modal fade" id="modalEdit<?= $type_id ?>" tabindex="-1" role="dialog"
     aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="myModalLabel">Информация</h4>
            </div>
            <div class="modal-body">
                <form>
                    <input type="hidden" name="_csrf" value="<?= Yii::$app->request->csrfToken ?>">
                    <input type="hidden" name="block_id" value="">
                    <div class="form-group">
                        <label class="control-label" >Дата</label>
                        <input name="date"
                               class="form-control"/>
                        <p class="help-block help-block-error"></p>
                    </div>
                    <div class="form-group">
                        <label class="control-label" >Время</label>
                        <input name="time"
                               class="form-control"/>
                        <p class="help-block help-block-error"></p>
                    </div>
                    <div class="form-group">
                        <label class="control-label" >Название</label>
                        <input name="name"
                               class="form-control"/>
                        <p class="help-block help-block-error"></p>
                    </div>

                    <div class="form-group required">
                        <label class="control-label">Картинка</label>
                        <?= \common\widgets\FileUpload7\FileUpload::widget([
                            'id'        => 'upload' . $type_id,
                            'name'      => 'upload' . $type_id,
                            'attribute' => 'upload' . $type_id,
                            'model'     => new \avatar\base\Model,
                            'update'    => \avatar\controllers\CabinetSchoolPagesConstructorController::getUpdate(),
                            'settings'  => \avatar\controllers\CabinetSchoolPagesConstructorController::getSettings($school_id, 1),
                        ]); ?>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-success buttonSave<?= $numStr ?>" data-id="" data-type_id="<?= $type_id ?>">
                    Сохранить
                </button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
            </div>
        </div>
    </div>
</div>