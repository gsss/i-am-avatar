<?php
/**
 * Продажа одного продукта из интернет магазина
 *
 */

/** @var \yii\web\View $this */
/** @var string $data json */
/** @var \common\models\school\PageBlockContent $blockContent */

$object = \yii\helpers\Json::decode($data);


$options = [
    'class' => 'container',
    'id'    => \avatar\controllers\CabinetSchoolPagesConstructorController::getBlockId($blockContent),
];
if (isset($object['margin_top'])) {
    if (!\cs\Application::isEmpty($object['margin_top'])) {
        $options['style']['margin-top'] = $object['margin_top'];
    }
}
if (isset($object['margin_bottom'])) {
    if (!\cs\Application::isEmpty($object['margin_bottom'])) {
        $options['style']['margin-bottom'] = $object['margin_bottom'];
    }
}
if (isset($options['style'])) {
    if (is_array($options['style'])) {
        $options['style'] = \yii\helpers\Html::cssStyleFromArray($options['style']);
    }
}

$product = \common\models\shop\Product::findOne($object['product']['id']);
if (is_null($product)) return;
?>
<?= \yii\helpers\Html::beginTag('div', $options) ?>
<div class="col-lg-4 col-lg-offset-4">
    <p>
        <a href="/shop/order?id=<?= $product->id ?>">
            <img src="<?= $product->image ?>" width="100%" class="thumbnail">
        </a>
    </p>
    <p><?= $product->name ?></p>
    <p><?= Yii::$app->formatter->asDecimal($product->price/100, 2) ?> <span class="label label-info"><?= \common\models\avatar\Currency::findOne($product->currency_id)->code ?></span></p>
</div>
<?= \yii\helpers\Html::endTag('div') ?>


