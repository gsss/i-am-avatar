<?php
/**
 *
 */

/** @var \yii\web\View $this */
/** @var string $data json */
/** @var \common\models\school\PageBlockContent $blockContent */

$object = \yii\helpers\Json::decode($data);

$options = [
    'class' => 'container',
    'id'    => \avatar\controllers\CabinetSchoolPagesConstructorController::getBlockId($blockContent),
];
if (isset($object['margin_top'])) {
    if (!\cs\Application::isEmpty($object['margin_top'])) {
        $options['style']['margin-top'] = $object['margin_top'];
    }
}
if (isset($object['margin_bottom'])) {
    if (!\cs\Application::isEmpty($object['margin_bottom'])) {
        $options['style']['margin-bottom'] = $object['margin_bottom'];
    }
}
if (isset($options['style'])) {
    if (is_array($options['style'])) {
        $options['style'] = \yii\helpers\Html::cssStyleFromArray($options['style']);
    }
}


\avatar\assets\Notify::register($this);

$this->registerJs(<<<JS
var n = new Noty({
    text: 'Этот сайт использует Cookie подтвердите свое согласие.',
    theme: 'nest',
    type: 'success',
    layout: 'bottomLeft',
    buttons: [
        Noty.button('Да', 'btn btn-success', function () {
            console.log('button 1 clicked');
        }, {id: 'button1', 'data-status': 'ok'})
    ]
});
n.show();
JS
);
?>


<?= \yii\helpers\Html::beginTag('div', $options) ?>
<div class="col-lg-4 col-lg-offset-4">
    <p class="text-center">
        Cookie
    </p>
</div>
<?= \yii\helpers\Html::endTag('div') ?>
