<?php
/**
 * Счетчик обратного отсчета
 */

/** @var \yii\web\View $this */
/** @var string $data json */
/** @var \common\models\school\PageBlockContent $blockContent */

$object = \yii\helpers\Json::decode($data);


$options = [
    'class' => 'container',
    'id'    => \avatar\controllers\CabinetSchoolPagesConstructorController::getBlockId($blockContent),
];
if (isset($object['margin_top'])) {
    if (!\cs\Application::isEmpty($object['margin_top'])) {
        $options['style']['margin-top'] = $object['margin_top'];
    }
}
if (isset($object['margin_bottom'])) {
    if (!\cs\Application::isEmpty($object['margin_bottom'])) {
        $options['style']['margin-bottom'] = $object['margin_bottom'];
    }
}
if (isset($options['style'])) {
    if (is_array($options['style'])) {
        $options['style'] = \yii\helpers\Html::cssStyleFromArray($options['style']);
    }
}

$selector = '#count_'.$blockContent->id;
\cs\Widget\CountDown::register($selector, $object['start']);


?>
<?= \yii\helpers\Html::beginTag('div', $options) ?>
    <div class="col-lg-8 col-lg-offset-2">
        <center>
            <div id="count_<?= $blockContent->id ?>"></div>
        </center>
    </div>
<?= \yii\helpers\Html::endTag('div') ?>


