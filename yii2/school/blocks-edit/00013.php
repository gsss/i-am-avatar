<?php
/**
 * Created by PhpStorm.
 * User: Metatron
 * Date: 22.12.2018
 * Time: 7:03
 */

/** @var \yii\web\View $this */
/** @var string $data json */
/** @var \common\models\school\PageBlockContent $blockContent */

$object = \yii\helpers\Json::decode($data);

\common\assets\iLightBox\Asset::register($this);

$this->registerJs("$('.ilightbox').iLightBox();");
?>
<div class="container" id="<?= \avatar\controllers\CabinetSchoolPagesConstructorController::getBlockId($blockContent) ?>">
    <div class="col-lg-8 col-lg-offset-2">
        <h1 class="text-center"><a
                    href='http://www.youtube.com/embed/<?= $object['id'] ?>?autohide=1&border=0&egm=0&showinfo=0&showsearch=0'
                    class="ilightbox"
                    data-type="iframe"
                    data-options="
                         thumbnail: '<?= $object['image'] ?>',
                         icon: 'video',
                         width: 1200,
                         height: 600
                       ">
                <img src="<?= $object['image'] ?>" width="100%" style="border-radius: 20px;">
            </a>
        </h1>
    </div>
</div>

