<?php
/**
 * Created by PhpStorm.
 * User: Metatron
 * Date: 22.12.2018
 * Time: 7:03
 */

/** @var string $data json */
/** @var \common\models\school\PageBlockContent $blockContent */

$object = \yii\helpers\Json::decode($data);
\common\assets\PageConstrictor\Asset::register($this);


\common\assets\JqueryUpload::register($this);
$fileNameArray = explode('/', __FILE__);
$file = $fileNameArray[count($fileNameArray)-1];
$arr = explode('-', $file);
$numStr = $arr[0];
$num = (int)$arr[0];


$this->registerJs(<<<JS

blockSettingsInit(
    {$num}, 
    function(modal, type_id, ret) {
        modal.find('#form_' + type_id + ' input[name="id"]').val(ret.id);
    }, 
    function(modal, type_id) {
        return {
            id: $('#form_' + type_id).find('input[name="id"]').val()
        };
    }
);

JS
);
?>
<div class="modal fade" id="modalEdit<?= $type_id ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Видео Блок (#<?= $type_id ?>)</h4>
            </div>
            <div class="modal-body">
                <form id="form_<?= $type_id ?>">
                    <input type="hidden" name="_csrf" value="<?= Yii::$app->request->csrfToken ?>">
                    <input type="hidden" name="block_id" value="">
                    <div class="form-group required field-email">
                        <label class="control-label" >Vimeo ID</label>
                        <input type="text" class="form-control text-input" name="id" aria-required="true" aria-invalid="true">
                        <p class="help-block help-block-error" style="display: none;"></p>
                    </div>

                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-success buttonSave" data-type_id="<?= $type_id ?>">
                    Сохранить
                </button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
            </div>
        </div>
    </div>
</div>