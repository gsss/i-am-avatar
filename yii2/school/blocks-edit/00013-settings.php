<?php
/**
 * Created by PhpStorm.
 * User: Metatron
 * Date: 22.12.2018
 * Time: 7:03
 */

/** @var string $data json */
/** @var \common\models\school\PageBlockContent $blockContent */

$object = \yii\helpers\Json::decode($data);

\common\assets\JqueryUpload::register($this);
\common\assets\PageConstrictor\Asset::register($this);

$this->registerJs(<<<JS

blockSettingsInit(
    13, 
    function(modal, type_id, ret) {
        modal.find('input[name="id"]').val(ret.id);
        modal.find('div[data-id="picture"]').html($('<img>', {src: ret.image, style:'width:100%;max-width:200px'})); 
    }, 
    function(modal, type_id) {
        return {
            id: modal.find('input[name="id"]').val(),
            image: modal.find('div[data-id="picture"] img').attr('src')
        };
    }
);

JS
);
?>
<div class="modal fade" id="modalEdit<?= $type_id ?>" tabindex="-1" role="dialog"
     aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="myModalLabel">Видео Блок (#<?= $type_id ?>)</h4>
            </div>
            <div class="modal-body">
                <form id="form_<?= $type_id ?>">
                    <input type="hidden" name="_csrf" value="<?= Yii::$app->request->csrfToken ?>">
                    <input type="hidden" name="block_id" value="">

                    <div class="form-group field-id">
                        <label class="control-label">ID Youtube</label>
                        <input id="field_<?= $type_id ?>_id" name="id" class="form-control"/>
                        <p class="help-block help-block-error"></p>
                    </div>

                    <div class="form-group field-image">
                        <label class="control-label">Картинка</label>

                        <?= \common\widgets\FileUpload7\FileUpload::widget([
                            'id'        => 'upload' . $type_id,
                            'name'      => 'upload' . $type_id,
                            'attribute' => 'upload' . $type_id,
                            'model'     => new \avatar\base\Model,
                            'update'    => \avatar\controllers\CabinetSchoolPagesConstructorController::getUpdate(),
                            'settings'  => \avatar\controllers\CabinetSchoolPagesConstructorController::getSettings($school_id, 1),
                        ]); ?>

                        <p class="help-block help-block-error"></p>

                    </div>

                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-success buttonSave"
                        data-type_id="<?= $type_id ?>">Сохранить
                </button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
            </div>
        </div>
    </div>
</div>