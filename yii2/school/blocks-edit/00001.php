<?php
/**
 * Created by PhpStorm.
 * User: Metatron
 * Date: 22.12.2018
 * Time: 7:03
 */

/** @var \yii\web\View $this */
/** @var string $data json */
/** @var \common\models\school\PageBlockContent $blockContent  */

$object = \yii\helpers\Json::decode($data);
\avatar\assets\Paralax::register($this);

?>
<div class="parallax-window" data-parallax="scroll" data-image-src="<?= $object['image'] ?>"
     style="min-height: 650px; background: transparent;" id="<?= \avatar\controllers\CabinetSchoolPagesConstructorController::getBlockId($blockContent) ?>">

    <?= $object['content'] ?>

</div>
