<?php
/**
 * Created by PhpStorm.
 * User: Metatron
 * Date: 22.12.2018
 * Time: 7:03
 */

/** @var string $data json */

$object = \yii\helpers\Json::decode($data);

\common\assets\iLightBox\Asset::register($this);

$this->registerJs("$('.ilightbox').iLightBox();");
?>
<div class="container" id="<?= \avatar\controllers\CabinetSchoolPagesConstructorController::getBlockId($blockContent) ?>">
    <div class="col-lg-8 col-lg-offset-2">
        <p class="text-center">
            <a href="<?= $object['video'] ?>"
               class="ilightbox"
               data-options="
                     thumbnail: '<?= $object['image'] ?>',
                     width: 1200,
                     height: 600
                   ">
                <img src="<?= $object['image'] ?>" width="100%" style="border-radius: 20px;">
            </a>
        </p>
    </div>
</div>

