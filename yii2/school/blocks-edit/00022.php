<?php
/**
 * Created by PhpStorm.
 * User: Metatron
 * Date: 22.12.2018
 * Time: 7:03
 */

/** Кто ведет Вариант 4 */

/** @var \yii\web\View $this */
/** @var string $data json */
/** @var \common\models\school\PageBlockContent $blockContent */

$object = \yii\helpers\Json::decode($data);

?>
<div class="container" id="<?= \avatar\controllers\CabinetSchoolPagesConstructorController::getBlockId($blockContent) ?>">
    <div class="col-lg-6">
        <?= $object['text'] ?>
    </div>
    <div class="col-lg-6">
        <p><img src="<?= $object['image'] ?>"  width="100%" style="border-radius: 20px;"></p>
    </div>
</div>


