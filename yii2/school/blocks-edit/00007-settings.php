<?php
/**
 * Created by PhpStorm.
 * User: Metatron
 * Date: 22.12.2018
 * Time: 7:03
 */

/** @var string $data json */
/** @var int $type_id  */
/** @var int $school_id  */

$object = \yii\helpers\Json::decode($data);

\common\assets\PageConstrictor\Asset::register($this);

$this->registerJs(<<<JS

blockSettingsInit(
    7, 
    function(modal, type_id, ret) {
        modal.find('input[name="site"]').val(ret.site);
        modal.find('input[name="mail"]').val(ret.mail);
    }, 
    function(modal, type_id) {
        return {
            site: modal.find('input[name="site"]').val(),
            mail: modal.find('input[name="mail"]').val()
        };
    }
);

JS
);

?>
<div class="modal fade" id="modalEdit<?= $type_id ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Картинка Блок (#<?= $type_id ?>)</h4>
            </div>
            <div class="modal-body">
                <form>
                    <input type="hidden" name="_csrf" value="<?= Yii::$app->request->csrfToken ?>">
                    <input type="hidden" name="block_id" value="">

                    <div class="form-group field-site">
                        <label class="control-label">Сайт</label>
                        <input id="field_<?= $type_id ?>_site" name="site" class="form-control"/>
                        <p class="help-block help-block-error"></p>
                    </div>

                    <div class="form-group field-mail">
                        <label class="control-label">Почта</label>
                        <input id="field_<?= $type_id ?>_mail" name="mail" class="form-control"/>
                        <p class="help-block help-block-error"></p>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-success buttonSave" data-type_id="<?= $type_id ?>">Сохранить</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
            </div>
        </div>
    </div>
</div>