<?php
/**
 * Created by PhpStorm.
 * User: Metatron
 * Date: 22.12.2018
 * Time: 7:03
 */

use iAvatar777\widgets\KoladaDar1\KoladaDar1;
use yii\helpers\Html;
use yii\helpers\Url;
use iAvatar777\services\DateRus\DateRus;

/** @var \yii\web\View $this */
/** @var string $data json */
/** @var \common\models\school\PageBlockContent $blockContent  */

$object = \yii\helpers\Json::decode($data);


$this->registerJs("$('.carousel50').carousel()");
?>

<div class="container" id="<?= \avatar\controllers\CabinetSchoolPagesConstructorController::getBlockId($blockContent) ?>">
    <div id="carousel-example-generic"
         class="carousel carousel50 slide thumbnail"
         data-ride="carousel"
         >
        <!-- Indicators -->

        <!-- Wrapper for slides -->
        <div class="carousel-inner" role="listbox">
            <?php $isFirst = true; ?>
            <?php foreach ($object as $i) { ?>
                <?php $add = ($isFirst)? ' active' : '' ?>
                <div class="item<?= $add ?>">
                    <img src="<?= $i['image'] ?>" alt="...">
                    <?php $isFirst = false; ?>
                </div>
            <?php } ?>
        </div>

        <!-- Controls -->
        <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
            <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
        </a>
        <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
            <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
        </a>
    </div>
</div>
