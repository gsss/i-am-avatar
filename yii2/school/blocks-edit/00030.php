<?php
/**
 * Блог ЯАватар
 */
use yii\helpers\Url;


/** @var \yii\web\View $this */
/** @var string $data json */
/** @var \common\models\school\PageBlockContent $blockContent */
/** @var \common\models\school\School $school */

$object = \yii\helpers\Json::decode($data);

$page = Yii::$app->request->get('page');

$query = \common\models\BlogItem::find();
$recordsCount = $query->count();
$itemsPerPage = 4;

?>
<div class="container"
     id="<?= \avatar\controllers\CabinetSchoolPagesConstructorController::getBlockId($blockContent) ?>">

    <div class="col-lg-12">
        <h1 class="page-header text-center">Блог</h1>
    </div>
    <?php /** @var \common\models\blog\Article $item */ ?>
    <?php foreach ($query->limit($itemsPerPage)->orderBy(['id' => SORT_DESC])->all() as $item) { ?>
        <div class="col-lg-3">
            <p style="height: 100px;"><b><?= $item->name ?></b></p>
            <p>
                <a href="<?= $item->getLink() ?>">
                    <img src="<?= \common\widgets\FileUpload7\FileUpload::getFile($item->image, 'crop') ?>" width="100%" class="thumbnail"/>
                </a>
            </p>
            <p style="color: #ccc;height: 50px;"><?= Yii::$app->formatter->asDate($item->created_at, 'php:d.m.Y') ?></p>
        </div>
    <?php } ?>

    <div class="col-lg-12">
        <a href="/blog/index" class="btn btn-default" style="width: 100%;">Все статьи</a>
    </div>
</div>
