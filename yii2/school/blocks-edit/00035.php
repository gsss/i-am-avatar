<?php
/**
 * Подвал на три блока
 */
use yii\helpers\Url;

/** @var \yii\web\View $this */
/** @var string $data json */
/** @var \common\models\school\PageBlockContent $blockContent  */

$object = \yii\helpers\Json::decode($data);



?>

<div class="container" id="<?= \avatar\controllers\CabinetSchoolPagesConstructorController::getBlockId($blockContent) ?>">
    <div class="col-lg-4">
        <?php foreach ($object['block1'] as $item) { ?>
            <?php if ($item['type'] == 'link') { ?>
                <p>
                    <?php
                    $options = [];
                    if (isset($item['target'])) $options['target'] = $item['target'];
                    ?>
                    <?= \yii\helpers\Html::a($item['text'], $item['href'], $options) ?>
                </p>
            <?php } else if ($item['type'] == 'text') { ?>
                <p>
                    <?= $item['text'] ?>
                </p>
            <?php } ?>
        <?php } ?>
    </div>
    <div class="col-lg-4">
        <?php foreach ($object['block2'] as $item) { ?>
            <?php if ($item['type'] == 'link') { ?>
                <p>
                    <?php
                    $options = [];
                    if (isset($item['target'])) $options['target'] = $item['target'];
                    ?>
                    <?= \yii\helpers\Html::a($item['text'], $item['href'], $options) ?>
                </p>
            <?php } else if ($item['type'] == 'text') { ?>
                <p>
                    <?= $item['text'] ?>
                </p>
            <?php } ?>
        <?php } ?>
    </div>
    <div class="col-lg-4">
        <?php foreach ($object['block3'] as $item) { ?>
            <?php if ($item['type'] == 'link') { ?>
                <p>
                    <?php
                    $options = [];
                    if (isset($item['target'])) $options['target'] = $item['target'];
                    ?>
                    <?= \yii\helpers\Html::a($item['text'], $item['href'], $options) ?>
                </p>
            <?php } else if ($item['type'] == 'text') { ?>
                <p>
                    <?= $item['text'] ?>
                </p>
            <?php } ?>
        <?php } ?>
    </div>
</div>
