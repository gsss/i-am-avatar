<?php
/**
 * Created by PhpStorm.
 * User: Metatron
 * Date: 22.12.2018
 * Time: 7:03
 */
use yii\helpers\Url;


/** @var \yii\web\View $this */
/** @var string $data json */
/** @var \common\models\school\PageBlockContent $blockContent */
/** @var \common\models\school\School $school */

$object = \yii\helpers\Json::decode($data);
$logo = '/images/logo830.png';
$serverNameString = 'www.i-am-avatar.com';
$serverNameImage = '/images/domain-name.png';

?>
<div class="container"
     id="<?= \avatar\controllers\CabinetSchoolPagesConstructorController::getBlockId($blockContent) ?>">

    <center>
        <div class="btn-group">
            <button type="button" class="btn btn-success" id="modalLogin"><i
                        class="glyphicon glyphicon-user" style="padding-right: 5px;"></i>Войти
            </button>
            <a href="<?= Url::to(['auth/registration']) ?>" class="btn btn-default" ><i
                        class="glyphicon glyphicon-user" style="padding-right: 5px;"></i>Регистрация
            </a>
            <a href="<?= Url::to(['auth/password-recover']) ?>" class="btn btn-default" ><i
                        class="glyphicon glyphicon-asterisk" style="padding-right: 5px;"></i>Напомнить пароль
            </a>
        </div>

    </center>

</div>


<div class="modal fade" id="loginModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
     aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">&times;</span></button>
                <h4 class="modal-title text-center" id="myModalLabel"><?= \Yii::t('c.DRNtMmSPgd', 'Вход в личный кабинет') ?></h4>
            </div>
            <div class="modal-body">
                <p class="text-center" style="margin-bottom: 30px;"><img src="/images/avatarlogo1.png" width="200"></p>
                <p><span style="color: red"><?= \Yii::t('c.Q2vFnrwWB1', 'Важно') ?>!</span>: <?= \Yii::t('c.Q2vFnrwWB1', 'Убедитесь, что вы находитесь на сайте') ?> https://<?= $serverNameString ?></p>
                <p><img src="<?= $serverNameImage ?>"></p>
                <div class="form-group">
                    <div class="input-group input-group-lg">
                        <span class="input-group-addon" id="basic-addon1">
                            <i class="fa fa-envelope-o fa-fw"></i>
                        </span>
                        <input type="text" class="form-control" placeholder="Email"
                               aria-describedby="basic-addon1" id="field-email">
                    </div>
                </div>
                <div class="form-group">
                    <div class="input-group input-group-lg">
                        <span class="input-group-addon" id="basic-addon1">
                            <i class="fa fa-key fa-fw" aria-hidden="true"></i>
                        </span>
                        <input type="password" class="form-control" placeholder="<?= \Yii::t('c.DRNtMmSPgd', 'Пароль') ?>" id="field-password">
                        <span class="input-group-btn">
                                <a class="btn btn-default gsssTooltip" href="/auth/password-recover" title="<?= \Yii::t('c.DRNtMmSPgd', 'Забыли пароль? Нажмите чтобы воссановить') ?>">
                                    <i class="glyphicon glyphicon-question-sign"></i>
                                </a>
                            </span>
                    </div>
                    <div class="hide" id="loginFormLoading">
                        <img style="padding-left: 10px;padding-right: 10px;"
                             src="<?= \Yii::$app->assetManager->getBundle(\avatar\assets\App\Asset::className())->baseUrl ?>/images/ajax-loader.gif"
                             id="">
                    </div>
                    <p class="text-danger" style="margin-top: 10px;display: none;" id="loginFormError">Здесь выводятся ошибки</p>
                </div>
                <div class="form-group">
                    <button class="btn btn-success btn-lg" id="buttonLogin" style="width: 100%;">
                        <?= \Yii::t('c.DRNtMmSPgd', 'Войти') ?>
                    </button>
                </div>
            </div>
            <div class="modal-footer">
                <a href="<?= Url::to(['auth/registration']) ?>" class="btn btn-default" style="width: 100%;"><?= \Yii::t('c.DRNtMmSPgd', 'Регистрация') ?></a>
            </div>
        </div>
    </div>
</div>

