<?php
/**
 * Created by PhpStorm.
 * User: Metatron
 * Date: 22.12.2018
 * Time: 7:03
 */

/** @var string $data json */

$object = \yii\helpers\Json::decode($data);

?>
<div class="container" id="<?= \avatar\controllers\CabinetSchoolPagesConstructorController::getBlockId($blockContent) ?>">
    <h1 class="page-header text-center"><?= $object['header'] ?></h1>
    <div class="col-lg-4">
        <p class="text-center">
            <img src="<?= $object['items'][0]['image'] ?>" class="img-center" width="80">
        </p>
        <p class="text-center lead">
            <?= $object['items'][0]['header'] ?>
        </p>
        <p class="text-center">
            <?= $object['items'][0]['text'] ?>
        </p>
    </div>
    <div class="col-lg-4">
        <p class="text-center">
            <img src="<?= $object['items'][1]['image'] ?>" class="img-center" width="80">
        </p>
        <p class="text-center lead">
            <?= $object['items'][1]['header'] ?>
        </p>
        <p class="text-center">
            <?= $object['items'][1]['text'] ?>
        </p>
    </div>
    <div class="col-lg-4">
        <p class="text-center">
            <img src="<?= $object['items'][2]['image'] ?>" class="img-center" width="80">
        </p>
        <p class="text-center lead">
            <?= $object['items'][2]['header'] ?>
        </p>
        <p class="text-center">
            <?= $object['items'][2]['text'] ?>
        </p>
    </div>
</div>

