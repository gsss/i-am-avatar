<?php
/**
 * Счетчик обратного отсчета
 */

/** @var \yii\web\View $this */
/** @var string $data json */
/** @var \common\models\school\PageBlockContent $blockContent */

$object = \yii\helpers\Json::decode($data);


$options = [
    'class' => 'container',
    'id'    => \avatar\controllers\CabinetSchoolPagesConstructorController::getBlockId($blockContent),
];
if (isset($object['margin_top'])) {
    if (!\cs\Application::isEmpty($object['margin_top'])) {
        $options['style']['margin-top'] = $object['margin_top'];
    }
}
if (isset($object['margin_bottom'])) {
    if (!\cs\Application::isEmpty($object['margin_bottom'])) {
        $options['style']['margin-bottom'] = $object['margin_bottom'];
    }
}
if (isset($options['style'])) {
    if (is_array($options['style'])) {
        $options['style'] = \yii\helpers\Html::cssStyleFromArray($options['style']);
    }
}

$selector = '#count_' . $blockContent->id;
$image = $object['image']['url'];
$text = $object['text'];
$button_text = $object['button']['text'];
$id = $object['id'];
$Sale = \common\models\school\SchoolSale::findOne($id);
?>
<?= \yii\helpers\Html::beginTag('div', $options) ?>
    <div class="col-lg-4 col-lg-offset-4">
        <p class="text-center">
            <a href="<?= \yii\helpers\Url::to(['kurs/order', 'id' => $Sale->id]) ?>">
                <img src="<?= $image ?>" class="img-circle" width="200">
            </a>
        </p>
        <p class="text-center lead">
            <?= $text ?>
        </p>
        <p class="text-center">
            <a href="<?= \yii\helpers\Url::to(['kurs/order', 'id' => $Sale->id]) ?>" class="btn btn-success">
                <?= $button_text ?>
            </a>
        </p>
    </div>
<?= \yii\helpers\Html::endTag('div') ?>


