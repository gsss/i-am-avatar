<?php
/**
 * Created by PhpStorm.
 * User: Metatron
 * Date: 22.12.2018
 * Time: 7:03
 */

/** @var string $data json */
/** @var \common\models\school\PageBlockContent $blockContent */

$object = \yii\helpers\Json::decode($data);
\common\widgets\HtmlContent\Asset::register($this);

$this->registerJs(<<<JS
CKEDITOR.config.filebrowserUploadUrl = '/upload/HtmlContent2_common';
CKEDITOR.config.allowedContent = true;
CKEDITOR.config.disallowedContent = true;
CKEDITOR.replace( 'text_area_{$type_id}_text1' );
CKEDITOR.replace( 'text_area_{$type_id}_text2' );

$('.buttonSave00023').click(function(e) {
    var type = $(this).data('type_id');
    var modal = $('#modalEdit'+type);
    var id = modal.data('id');
    ajaxJson({
        url: '/cabinet-school-pages-constructor/block-save',
        data: {
            id: id,
            form: {
                text1: modal.find('.field_{$type_id}_text1')[0].getElementsByTagName("iframe")[0].contentDocument.getElementsByTagName("body")[0].innerHTML,
                text2: modal.find('.field_{$type_id}_text2')[0].getElementsByTagName("iframe")[0].contentDocument.getElementsByTagName("body")[0].innerHTML
            }
        },
        success: function(ret) {
              window.location.reload();
        }
    });
});

$('.buttonSettings[data-type="23"]').click(function(e) {
    // Идентификатор блока контента
    var id = $(this).data('id');
    var type = $(this).data('type');
    var modal = $('#modalEdit'+type);
    modal.data('id', id);
    ajaxJson({
        url: '/cabinet-school-pages-constructor/block-get',
        data: {
            id: id,
        },
        success: function(ret) {
            modal.find('.field_{$type_id}_text1')[0].getElementsByTagName("iframe")[0].contentDocument.getElementsByTagName("body")[0].innerHTML = ret.text1; 
            modal.find('.field_{$type_id}_text2')[0].getElementsByTagName("iframe")[0].contentDocument.getElementsByTagName("body")[0].innerHTML = ret.text2; 
            $('#modalEdit' + type).modal();
        }
    });
});


JS
);
?>
<div class="modal fade" id="modalEdit<?= $type_id ?>" tabindex="-1" role="dialog"
     aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="myModalLabel">Информация</h4>
            </div>
            <div class="modal-body">
                <form id="form_25">
                    <input type="hidden" name="_csrf" value="<?= Yii::$app->request->csrfToken ?>">
                    <input type="hidden" name="block_id" value="">
                    <div class="form-group required field_<?= $type_id ?>_text1">
                        <label class="control-label">Содержимое</label>
                        <textarea id="text_area_<?= $type_id ?>_text1" name="content">21212</textarea>
                        <p class="help-block help-block-error"></p>
                    </div>
                    <div class="form-group required field_<?= $type_id ?>_text2">
                        <label class="control-label">Содержимое</label>
                        <textarea id="text_area_<?= $type_id ?>_text2" name="content">21212</textarea>
                        <p class="help-block help-block-error"></p>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-success buttonSave00023" data-id="<?= $type_id ?>" data-type_id="<?= $type_id ?>">
                    Сохранить
                </button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
            </div>
        </div>
    </div>
</div>