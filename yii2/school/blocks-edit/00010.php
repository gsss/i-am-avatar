<?php
/**
 * Created by PhpStorm.
 * User: Metatron
 * Date: 22.12.2018
 * Time: 7:03
 */

/** @var \yii\web\View $this */
/** @var string $data json */
/** @var \common\models\school\PageBlockContent $blockContent */

$object = \yii\helpers\Json::decode($data);
$rows = count($object['items']);


$this->registerJs(<<<JS

JS
);
?>
<div class="container"
     id="<?= \avatar\controllers\CabinetSchoolPagesConstructorController::getBlockId($blockContent) ?>">

    <div class="col-lg-12">
        <h1 class="text-center page-header"><?= $object['header'] ?></h1>
    </div>

    <div class="col-lg-12">
        <div class="row" style="margin-bottom: 50px;">
            <div class="col-lg-6 col-lg-offset-3">

                <div class="collapse in" id="collapse_form_<?= $blockContent->id ?>">
                    <form id="form_<?= $blockContent->id ?>">
                        <input type="hidden" name="_csrf" value="<?= Yii::$app->request->csrfToken ?>">
                        <input type="hidden" name="block_id" value="<?= $blockContent->id ?>"/>
                        <?php for ($i = 0; $i < $rows; $i++) { ?>
                            <div class="form-group required field-<?= $object['items'][$i]['name'] ?>">
                                <label class="control-label"
                                       for="field_<?= $blockContent->id ?>_<?= $object['items'][$i]['name'] ?>"><?= $object['items'][$i]['placeholder'] ?></label>
                                <input type="text"
                                       id="field_<?= $blockContent->id ?>_<?= $object['items'][$i]['name'] ?>"
                                       class="form-control text-input" name="<?= $object['items'][$i]['name'] ?>"
                                       aria-required="true"
                                       aria-invalid="true">
                                <p class="help-block help-block-error"></p>
                            </div>
                        <?php } ?>
                        <hr>
                        <div class="form-group">
                            <button
                                    class="btn btn-success buttonSend"
                                    name="contact-button"
                                    style="width:100%"
                                    data-id="<?= $blockContent->id ?>"
                                    data-form="form_<?= $blockContent->id ?>"
                            ><?= $object['button']['text'] ?></button>
                        </div>
                    </form>
                </div>
                <div class="collapse" id="collapse_alert_<?= $blockContent->id ?>">
                    <p class="alert alert-success">
                        Успешно.
                    </p>
                </div>
            </div>
        </div>
    </div>
</div>

