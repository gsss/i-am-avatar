<?php
/**
 * Created by PhpStorm.
 * User: Metatron
 * Date: 22.12.2018
 * Time: 7:03
 */

/** @var \yii\web\View $this */
/** @var string $data json */
/** @var \common\models\school\PageBlockContent $blockContent */

$object = \yii\helpers\Json::decode($data);

?>
<div class="container" id="<?= \avatar\controllers\CabinetSchoolPagesConstructorController::getBlockId($blockContent) ?>">

    <div class="container">
        <div class="col-lg-8 col-lg-offset-2">
            <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="headingOne">
                        <h4 class="panel-title">
                            <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne"
                               aria-expanded="true" aria-controls="collapseOne">
                                16.02 прилет в Гоа, с 17 по 22 у нас начинается оздоровительно-творческая программа
                            </a>
                        </h4>
                    </div>
                    <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel"
                         aria-labelledby="headingOne">
                        <div class="panel-body">

                            <div>Утро:&nbsp;</div>
                            <div>Крийя йога с Лирой&nbsp;</div>
                            <div><br></div>
                            <div>Полезный вкусный Завтрак</div>
                            <div><br></div>
                            <div>День:&nbsp;</div>
                            <div>-Мастер класс по плетению амулетов с натуральными камнями для себя&nbsp;</div>
                            <div>-Экскурсия в джунгли на озеро на встречу с Бабой, где сидели Битлс</div>
                            <div>-Медитация с поющими чашами</div>
                            <div>-Вводная лекция и знакомство с дизайном человека</div>
                            <div>-Мастер класс по рисованию&nbsp;</div>
                            <div><br></div>
                            <div>Обед</div>
                            <div><br></div>
                            <div>Вечер:&nbsp;</div>
                            <div>Кундалини или Хатха йога на пляже с закатом&nbsp;</div>
                            <div><br></div>
                            <div>Ночь: концерт с&nbsp; ужином&nbsp; в творческом месте ( каждый вечер новое место)</div>


                        </div>
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="headingTwo">
                        <h4 class="panel-title">
                            <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion"
                               href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                23.02  Экскурсия по Гоа
                            </a>
                        </h4>
                    </div>
                    <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
                        <div class="panel-body">
                            Обширная Экскурсия по Гоа на весь день с заездом на чайную плантацию, а так же плантацию специй и эфирных маслел, купание в водопадах и общение со слонами.
                        </div>
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="headingThree">
                        <h4 class="panel-title">
                            <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion"
                               href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                24.02  днём пляж и сборы, в ночь на автобусе в Пуну в приют
                            </a>
                        </h4>
                    </div>
                    <div id="collapseThree" class="panel-collapse collapse" role="tabpanel"
                         aria-labelledby="headingThree">
                        <div class="panel-body">
                            <p>Детский приют  Shikshangram Shelter  <br>
                                В приюте проживает примерно 140 деток разных возрастов от 2,5 лет и старше.<br><br>
                            </p>
                        </div>
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="heading4">
                        <h4 class="panel-title">
                            <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion"
                               href="#collapse4" aria-expanded="false" aria-controls="collapse4">
                                25.02: утром приезд в Пуну;<br> 25/26/27 проживание в в приюте, проводим время с детьми, посещаем древние буддийские пещеры и храмы
                            </a>
                        </h4>
                    </div>
                    <div id="collapse4" class="panel-collapse collapse" role="tabpanel"
                         aria-labelledby="headingThree">
                        <div class="panel-body">
                        </div>
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="heading5">
                        <h4 class="panel-title">
                            <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion"
                               href="#collapse5" aria-expanded="false" aria-controls="collapse5">
                                28.02 ВыЛЕТ ДОМОЙ
                            </a>
                        </h4>
                    </div>
                    <div id="collapse5" class="panel-collapse collapse" role="tabpanel"
                         aria-labelledby="headingThree">
                        <div class="panel-body">
                            Собираем вещи и выезжаем в аэропорт, откуда летим домой)))
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
