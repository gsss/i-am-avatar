<?php
/**
 * Created by PhpStorm.
 * User: Metatron
 * Date: 22.12.2018
 * Time: 7:03
 */
use yii\helpers\Url;

/** @var \yii\web\View $this */
/** @var string $data json */
/** @var \common\models\school\PageBlockContent $blockContent  */

$object = \yii\helpers\Json::decode($data);
\avatar\assets\Paralax::register($this);


?>

<?php if (!\Yii::$app->user->isGuest) : ?>
    <?php \avatar\widgets\ModalLogin\Asset::register($this); ?>
<?php endif; ?>


<div class="parallax-window" data-parallax="scroll" data-image-src="<?= $object['image'] ?>"
     style="min-height: 650px; background: transparent;" id="<?= \avatar\controllers\CabinetSchoolPagesConstructorController::getBlockId($blockContent) ?>">


    <?= $object['content'] ?>

</div>

<center>
    <div class="btn-group">
        <a href="/auth/login" class="btn btn-success"><i class="glyphicon glyphicon-user" style="padding-right: 5px;"></i>Войти
        </a>
        <a href="/auth/registration" class="btn btn-default"><i class="glyphicon glyphicon-user" style="padding-right: 5px;"></i>Регистрация
        </a>
        <a href="/auth/password-recover" class="btn btn-default"><i class="glyphicon glyphicon-asterisk" style="padding-right: 5px;"></i>Напомнить пароль
        </a>
    </div>
</center>