<?php
/**
 * Created by PhpStorm.
 * User: Metatron
 * Date: 22.12.2018
 * Time: 7:03
 */

/** @var string $data json */

$object = \yii\helpers\Json::decode($data);

$this->registerJs("$('.carousel').carousel();");

use yii\helpers\ArrayHelper;

?>

<style>
    .darkened {
        position: relative;
    }

    .darkened::before {
        content: '';
        position: absolute;
        top: 0;
        left: 0;
        right: 0;
        bottom: 0;
        background-color: rgba(0,0,0,0.5);
        z-index: 1;
    }
</style>

<div class="container" id="<?= \avatar\controllers\CabinetSchoolPagesConstructorController::getBlockId($blockContent) ?>">

    <div id="carousel-example-generic" class="carousel slide" data-ride="carousel" style="width: 100%;">
        <!-- Indicators -->
        <ol class="carousel-indicators">
            <?php for ($i = 0; $i < count($object); $i++) { ?>
                <?php $class = ''; ?>
                <?php if ($i == 0) $class = 'active'; ?>
                <li data-target="#carousel-example-generic" data-slide-to="<?= $i ?>" class="<?= $class ?>"></li>
            <?php } ?>
        </ol>

        <!-- Wrapper for slides -->
        <div class="carousel-inner" role="listbox">
            <?php for ($i = 0; $i < count($object); $i++) { ?>
                <?php $class = ''; ?>
                <?php if ($i == 0) $class = 'active'; ?>
                <div class="item <?= $class ?>">
                    <div class="cvc darkened" >
                        <img src="<?= $object[$i]['image'] ?>" alt="...">
                    </div>
                    <div class="carousel-caption">
                        <h1 style="font-size: 48px;text-shadow: 0px 10px 20px rgba(0, 0, 0, 0.5);color:white;"><?= \yii\helpers\ArrayHelper::getValue($object[$i],'header', '') ?></h1>
                        <p class="lead" style="margin-bottom: 50px;text-shadow: 0px 10px 20px rgba(0, 0, 0, 0.5);color:white;"><?= nl2br(\yii\helpers\ArrayHelper::getValue($object[$i],'text', '')) ?></p>
                    </div>
                </div>
            <?php } ?>
        </div>

        <!-- Controls -->
        <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
            <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
        </a>
        <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
            <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
        </a>
    </div>
</div>

