<?php
/**
 * Created by PhpStorm.
 * User: Metatron
 * Date: 22.12.2018
 * Time: 7:03
 */
use yii\helpers\Url;


/** @var \yii\web\View $this */
/** @var string $data json */
/** @var \common\models\school\PageBlockContent $blockContent */
/** @var \common\models\school\School $school */

$object = \yii\helpers\Json::decode($data);

?>
<div class="container"
     id="<?= \avatar\controllers\CabinetSchoolPagesConstructorController::getBlockId($blockContent) ?>">

    <center>
        <div class="btn-group">
            <a href="<?= Url::to(['auth/login']) ?>" class="btn btn-success" ><i
                        class="glyphicon glyphicon-user" style="padding-right: 5px;"></i>Войти
            </a>
            <a href="<?= Url::to(['auth/registration']) ?>" class="btn btn-default" ><i
                        class="glyphicon glyphicon-user" style="padding-right: 5px;"></i>Регистрация
            </a>
            <a href="<?= Url::to(['auth/password-recover']) ?>" class="btn btn-default" ><i
                        class="glyphicon glyphicon-asterisk" style="padding-right: 5px;"></i>Напомнить пароль
            </a>
        </div>

    </center>

</div>
