<?php
/**
 * Created by PhpStorm.
 * User: Metatron
 * Date: 22.12.2018
 * Time: 7:03
 */

/** @var \yii\web\View $this */
/** @var string $data json */
/** @var \common\models\school\PageBlockContent $blockContent */

$object = \yii\helpers\Json::decode($data);

?>
<nav class="navbar navbar-default">
    <div class="container">
        <ul class="nav navbar-nav">
            <?php $isFirst = true; ?>
            <?php foreach ($object['items'] as $item) { ?>
                <?php $options = []; ?>
                <?php if ($isFirst) $options['class'] = 'active'; ?>
                <?= \yii\helpers\Html::tag('li', \yii\helpers\Html::a($item['name'], $item['href'])) ?>
            <?php } ?>
        </ul>
    </div>
</nav>