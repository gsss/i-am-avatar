<?php
/**
 * Created by PhpStorm.
 * User: Metatron
 * Date: 22.12.2018
 * Time: 7:03
 */

/** @var string $data json */
/** @var int $type_id  */
/** @var int $school_id  */

$fileNameArray = explode('/', __FILE__);
$file =$fileNameArray[count($fileNameArray)-1];
$arr = explode('-', $file);
$numStr = $arr[0];
$num = (int)$arr[0];

$object = \yii\helpers\Json::decode($data);
\common\widgets\HtmlContent\Asset::register($this);

$this->registerJs(<<<JS
CKEDITOR.config.filebrowserUploadUrl = '/upload/HtmlContent2_common';
CKEDITOR.config.allowedContent = true;
CKEDITOR.config.disallowedContent = true;
CKEDITOR.replace( 'text_area_{$type_id}_content' );

$('#modalEdit' + {$num} + ' .buttonSave').click(function(e) {
    var type = $(this).data('type_id');
    var modal = $('#modalEdit' + type);
    var id = modal.data('id');
    ajaxJson({
        url: '/cabinet-school-pages-constructor/block00027-save',
        data: {
            id: id,
            form: {
                content: modal[0].getElementsByTagName("iframe")[0].contentDocument.getElementsByTagName("body")[0].innerHTML
            }
        },
        success: function(ret) {
              window.location.reload();
        }
    });
});

$('.buttonSettings[data-type="{$num}"]').click(function(e) {
    // Идентификатор блока контента
    var id = $(this).data('id');
    var type = $(this).data('type');
    var modal = $('#modalEdit'+type);
    modal.data('id', id);
    ajaxJson({
        url: '/cabinet-school-pages-constructor/block-get',
        data: {
            id: id
        },
        success: function(ret) {
            modal[0].getElementsByTagName("iframe")[0].contentDocument.getElementsByTagName("body")[0].innerHTML = ret.content; 
            $('#modalEdit' + type).modal();
        }
    });
});


JS
);
?>
<div class="modal fade" id="modalEdit<?= $type_id ?>" tabindex="-1" role="dialog"
     aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="myModalLabel">Информация</h4>
            </div>
            <div class="modal-body">
                <form id="form_<?= $type_id ?>">
                    <input type="hidden" name="_csrf" value="<?= Yii::$app->request->csrfToken ?>">
                    <input type="hidden" name="block_id" value="">

                    <div class="form-group">
                        <label class="control-label">Содержимое</label>
                        <textarea id="text_area_<?= $type_id ?>_content" name="content">12</textarea>
                        <p class="help-block help-block-error"></p>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-success buttonSave" data-type_id="<?= $type_id ?>">
                    Сохранить
                </button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
            </div>
        </div>
    </div>
</div>