<?php
/**
 * Created by PhpStorm.
 * User: Metatron
 * Date: 22.12.2018
 * Time: 7:03
 */

/** @var \yii\web\View $this */
/** @var string $data json */
/** @var \common\models\school\PageBlockContent $blockContent  */

$object = \yii\helpers\Json::decode($data);
\avatar\assets\Paralax::register($this);
$rows = count($object['form']['items']);

?>
<div class="parallax-window" data-parallax="scroll" data-image-src="<?= $object['image'] ?>"
     style="min-height: 650px; background: transparent;" id="<?= \avatar\controllers\CabinetSchoolPagesConstructorController::getBlockId($blockContent) ?>">

    <div class="container">
        <div class="col-lg-7">
            <?= $object['content'] ?>
        </div>
        <div class="col-lg-5" style="margin-top: 150px;">
            <div class="collapse in" id="collapse_form_<?= $blockContent->id ?>">
                <form id="form_<?= $blockContent->id ?>" style="background-color: rgba(255, 255, 255, 0.5); padding: 30px; border-radius: 20px; ">
                    <input type="hidden" name="_csrf" value="<?= Yii::$app->request->csrfToken ?>">
                    <input type="hidden" name="block_id" value="<?= $blockContent->id ?>"/>
                    <?php for ($i = 0; $i < $rows; $i++) { ?>
                        <div class="form-group required field-<?= $object['form']['items'][$i]['name'] ?>">
                            <input type="text"
                                   id="field_<?= $blockContent->id ?>_<?= $object['form']['items'][$i]['name'] ?>"
                                   class="form-control text-input"
                                   placeholder="<?= $object['form']['items'][$i]['placeholder'] ?>"
                                   name="<?= $object['form']['items'][$i]['name'] ?>"
                                   aria-required="true"
                                   aria-invalid="true"
                            >
                            <p class="help-block help-block-error"></p>
                        </div>
                    <?php } ?>
                    <hr>
                    <div class="form-group">
                        <button
                                class="btn btn-success buttonSend"
                                name="contact-button"
                                style="width:100%"
                                data-id="<?= $blockContent->id ?>"
                                data-form="form_<?= $blockContent->id ?>"
                        ><?= $object['form']['button']['text'] ?></button>
                    </div>
                </form>
            </div>
            <div class="collapse" id="collapse_alert_<?= $blockContent->id ?>">
                <p class="alert alert-success">
                    Успешно.
                </p>
            </div>
        </div>
    </div>
</div>
