<?php
/**
 * Created by PhpStorm.
 * User: Metatron
 * Date: 22.12.2018
 * Time: 7:03
 */

use iAvatar777\widgets\KoladaDar1\KoladaDar1;
use yii\helpers\Html;
use yii\helpers\Url;
use iAvatar777\services\DateRus\DateRus;

/** @var \yii\web\View $this */
/** @var string $data json */
/** @var \common\models\school\PageBlockContent $blockContent  */

$object = \yii\helpers\Json::decode($data);

\iAvatar777\widgets\KalendarSlavi1\Asset::register($this);

$this->registerJs(<<<JS

var d1 = KrugoLet(new Date());
$('#day_' + d1.Mes + '_' + d1.Chislo).css('background-color', 'red');
console.log(d1);
JS
);
?>

<div class="container" id="<?= \avatar\controllers\CabinetSchoolPagesConstructorController::getBlockId($blockContent) ?>">
    <?= KoladaDar1::widget([
        'dayStart'         => 9,
        'DateGrigorFirst'  => '2020-09-21',
        'optionsWeek'      => [
            1 => ['style' => 'background-color: #000000; color: #ffffff;'],
            2 => ['style' => 'background-color: #ff9395;'],
            3 => ['style' => 'background-color: #ffd092;'],
            4 => ['style' => 'background-color: #fffb92;'],
            5 => ['style' => 'background-color: #ace790;'],
            6 => ['style' => 'background-color: #a1e5fe;'],
            7 => ['style' => 'background-color: #909ffa;'],
            8 => ['style' => 'background-color: #b5a4e5;'],
            9 => ['style' => 'background-color: #ffffff;'],
        ],
        'optionsColumn'    => [
            1 => ['style' => 'width: 90px;'],
            2 => ['style' => 'width: 90px;'],
            3 => ['style' => 'width: 90px;'],
            4 => ['style' => 'width: 90px;'],
            5 => ['style' => 'width: 90px;'],
            6 => ['style' => 'width: 90px;'],
        ],
        'isDrawIds'        => true,
        'isDrawDateGrigor' => true,
        'DateGrigorClass'  => 'kal1',
        'cellFormat'       => function (DateTime $d, $options) {
            $day = $options['day'];

            return $day . Html::tag('span', ' / ' . DateRus::format('j K', $d->format('U')), ['style' => 'color:#ccc; font-size:70%;']);
        }
    ]);
    ?>
</div>
