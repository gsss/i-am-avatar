<?php
/**
 * Created by PhpStorm.
 * User: Metatron
 * Date: 22.12.2018
 * Time: 7:03
 */

/** @var string $data json */

$object = \yii\helpers\Json::decode($data);

?>
<div class="container" id="<?= \avatar\controllers\CabinetSchoolPagesConstructorController::getBlockId($blockContent) ?>">
    <div class="row" style="margin-top: 20px;">
        <div class="col-lg-4">
            <p style="margin-bottom: 30px;">
                <a href="<?= $object['site'] ?>" target="_blank"><?= $object['site'] ?></a>
            </p>
        </div>
        <div class="col-lg-4">

        </div>
        <div class="col-lg-4">
            <p style="margin-bottom: 30px;">
                <?= $object['mail'] ?>
            </p>
        </div>
    </div>
</div>

