<?php
/**
 * Created by PhpStorm.
 * User: Metatron
 * Date: 22.12.2018
 * Time: 7:03
 */

/** @var string $data json */
/** @var \common\models\school\PageBlockContent $blockContent */

$object = \yii\helpers\Json::decode($data);

$fileNameArray = explode('/', __FILE__);
$file = $fileNameArray[count($fileNameArray)-1];
$arr = explode('-', $file);
$numStr = $arr[0];
$num = (int)$arr[0];

$this->registerJs(<<<JS

$('.buttonSave{$numStr}').click(function(e) {
    var type = $(this).data('type_id');
    var modal = $('#modalEdit' + type);
    var id = modal.data('id');
    ajaxJson({
        url: '/cabinet-school-pages-constructor/block-save',
        data: {
            id: id,
            form: {
                "image": {
                    "url": modal.find('div[data-id="picture"] img').attr('src')
                },
                "text": modal.find('textarea[name="text"]').val(),
                "button":  {
                    "text": modal.find('input[name="button_text"]').val()
                },
                "id": modal.find('select[name="id"]').val()
            }
        },
        success: function(ret) {
              window.location.reload();
        }
    });
});

$('.buttonSettings[data-type="{$num}"]').click(function(e) {
    // Идентификатор блока контента
    var id = $(this).data('id');
    var type = $(this).data('type');
    var modal = $('#modalEdit' + type);
    modal.data('id', id);
    ajaxJson({
        url: '/cabinet-school-pages-constructor/block-get',
        data: {
            id: id
        },
        success: function(ret) {
            modal.find('div[data-id="picture"]').html($('<img>', {src: ret.image.url, style:'width:100%;max-width:200px'})); 
            modal.find('input[name="button_text"]').val(ret.button.text); 
            modal.find('textarea[name="text"]').val(ret.text);
            modal.find('select[name="id"]').val(ret.id); 
            $('#modalEdit' + type).modal();
        }
    });
});


JS
);

$rows = \common\models\school\SchoolSale::find()->where(['potok_id' => 32])->all();

?>
<div class="modal fade" id="modalEdit<?= $type_id ?>" tabindex="-1" role="dialog"
     aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="myModalLabel">Информация</h4>
            </div>
            <div class="modal-body">
                <form>

                    <div class="form-group field-start">
                        <label class="control-label" >IMG</label>
                        <?= \common\widgets\FileUpload7\FileUpload::widget([
                            'id'        => 'upload' . $type_id,
                            'name'      => 'upload' . $type_id,
                            'attribute' => 'upload' . $type_id,
                            'model'     => new \avatar\base\Model,
                            'update'    => \avatar\controllers\CabinetSchoolPagesConstructorController::getUpdate(),
                            'settings'  => \avatar\controllers\CabinetSchoolPagesConstructorController::getSettings($school_id, 1),
                        ]); ?>
                        <p class="help-block help-block-error" style="display: none;"></p>
                    </div>
                    <div class="form-group field-start">
                        <label class="control-label" >button_text</label>
                        <input type="text" class="form-control text-input" name="button_text" aria-required="true" aria-invalid="true">
                        <p class="help-block help-block-error" style="display: none;"></p>
                    </div>
                    <div class="form-group field-start">
                        <label class="control-label" >text</label>
                        <textarea name="text" class="form-control" rows="5"></textarea>
                        <p class="help-block help-block-error" style="display: none;"></p>
                    </div>
                    <div class="form-group field-start">
                        <label class="control-label" >sale</label>
                        <select name="id" class="form-control">
                            <option>- Ничего не выбрано -</option>
                            <?php /** @var $item \common\models\school\SchoolSale */ ?>
                            <?php foreach ($rows as $item) { ?>
                                <option value="<?= $item->id ?>"><?= $item->name ?></option>
                            <?php } ?>
                        </select>

                        <p class="help-block help-block-error" style="display: none;"></p>
                    </div>

                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-success buttonSave<?= $numStr ?>" data-id="" data-type_id="<?= $type_id ?>">
                    Сохранить
                </button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
            </div>
        </div>
    </div>
</div>