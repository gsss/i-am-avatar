<?php
/**
 * Created by PhpStorm.
 * User: Metatron
 * Date: 22.12.2018
 * Time: 7:03
 */

/** @var \yii\web\View $this */
/** @var string $data json */
/** @var \common\models\school\PageBlockContent $blockContent  */

$object = \yii\helpers\Json::decode($data);
\avatar\assets\Paralax::register($this);

?>
<div class="parallax-window" data-parallax="scroll" data-image-src="<?= $object['image'] ?>"
     style="min-height: 650px; background: transparent;" id="<?= \avatar\controllers\CabinetSchoolPagesConstructorController::getBlockId($blockContent) ?>">

    <div class="container"
         id="<?= \avatar\controllers\CabinetSchoolPagesConstructorController::getBlockId($blockContent) ?>">
        <div class="col-lg-6" style="margin-top: 100px;">
            <h1><?= $object['name'] ?></h1>
        </div>
        <div class="col-lg-6" style="margin-top: 100px;">
            <p class="lead"><i class="fa fa-calendar"></i> <?= $object['date'] ?></p>
            <p class="lead"><i class="fa fa-clock-o"></i> <?= $object['time'] ?></p>
        </div>
    </div>

</div>
