<?php
/**
 * Как вы с нами можете сотрудничать
 */

use yii\helpers\Url;

/** @var \yii\web\View $this */
/** @var string $data json */
/** @var \common\models\school\PageBlockContent $blockContent */

$object = \yii\helpers\Json::decode($data);


?>

<div class="container"
     id="<?= \avatar\controllers\CabinetSchoolPagesConstructorController::getBlockId($blockContent) ?>">

    <div class="col-lg-12">
        <h1 class="page-header text-center">Как вы с нами можете сотрудничать</h1>
    </div>

    <div class="row">
        <div class="col-lg-12">
            <div class="col-lg-4">
                <div class="panel panel-success">
                    <div class="panel-heading">
                        <h3 class="panel-title">Подключить школу</h3>
                    </div>
                    <div class="panel-body">
                        <p><a href="/anketa/new-school" class="btn btn-default" style="width: 100%;">Заполнить
                                анкету</a></p>
                        <p><a href="http://root.i-am-avatar.com/kurs-i-avatar" target="_blank" class="btn btn-default"
                              style="width: 100%;">Как пользоваться интерфейсом
                                ЯАватар</a></p>
                    </div>
                </div>


            </div>
            <div class="col-lg-4">
                <div class="panel panel-success">
                    <div class="panel-heading">
                        <h3 class="panel-title">Подписаться на рассылку</h3>
                    </div>
                    <div class="panel-body">
                        <form id="formSubscribe">
                            <div class="form-group recipient-field-name">
                                <input type="text" class="form-control" placeholder="Имя" name="name">
                                <p class="help-block help-block-error hide">Это поле должно быть заполнено
                                    обязательно</p>
                            </div>
                            <div class="form-group recipient-field-email">
                                <input type="email" class="form-control" placeholder="Email" name="email">
                                <p class="help-block help-block-error hide">Это поле должно быть заполнено
                                    обязательно</p>
                            </div>
                            <?php
                            $this->registerJs(<<<JS
var functionSubscribeButtonClick = function(e) {
    e.preventDefault();
    e.stopPropagation();
    var b = $(this);
    var bText = b.html();
    b.off('click');
    b.html($('<i>', {class: 'fa fa-spinner fa-spin fa-fw'}));
    b.attr('disabled', 'disabled');
    
    ajaxJson({
        url: '/pages/form',
        data: {
            block_id: 64,
            name: $('#formSubscribe input[name="name"]').val(),
            email: $('#formSubscribe input[name="email"]').val()
        },
        success: function (ret) {
            b.on('click', functionSubscribeButtonClick);
            b.html(bText);
            b.removeAttr('disabled');
            
            $('#modalInfo34').modal();
        },
        errorScript: function(ret) {
            if (ret.id == 102) {
                 var f = $('#formSubscribe');
                 $.each(ret.data,function(i,v) {
                    var name = v.name;
                    var value = v.value;
                    var t = f.find('.recipient-field-' + name);
                    t.addClass('has-error');
                    t.find('p.help-block-error').removeClass('hide').html(value.join('<br>')).show();
                });
             }
            b.on('click', functionSubscribeButtonClick);
            b.html(bText);
            b.removeAttr('disabled');
        }
    });
};

$('#formSubscribeSubmit').click(functionSubscribeButtonClick);

// снимает ошибочные признаки поля при фокусе
$('#formSubscribe .form-control').on('focus', function() {
    var o = $(this);
    var p = o.parent();
    if (p.hasClass('input-group')) {
        p = p.parent();
    }
    p.removeClass('has-error');
    p.find('p.help-block-error').hide();
});

JS
                            );
                            ?>
                            <button type="button" class="btn btn-default" style="width: 100%;" id="formSubscribeSubmit">
                                Подписаться
                            </button>
                        </form>
                    </div>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="panel panel-success">
                    <div class="panel-heading">
                        <h3 class="panel-title">Подключиться в команду</h3>
                    </div>
                    <div class="panel-body">
                        <p><a href="/anketa/new" class="btn btn-default" style="width: 100%;">Заполнить анкету</a></p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <div class="col-lg-4">
                <div class="panel panel-success">
                    <div class="panel-heading">
                        <h3 class="panel-title">Заработать на нашем проекте</h3>
                    </div>
                    <div class="panel-body">
                        <p><a href="https://www.youtube.com/watch?v=7gVXareaBME" target="_blank" class="btn btn-default"
                              style="width: 100%;">Посмотреть видео</a>
                        </p>
                        <p><a href="http://galaxysss.ru/shop/category/20" target="_blank" class="btn btn-default"
                              style="width: 100%;">Получить карту с Эликсирами</a></p>
                    </div>
                </div>


            </div>
            <div class="col-lg-4">
                <div class="panel panel-success">
                    <div class="panel-heading">
                        <h3 class="panel-title">Зарегистрироваться на платформе</h3>
                    </div>
                    <div class="panel-body">
                        <p><a href="/auth/registration" class="btn btn-default" style="width: 100%;">Регистрация</a></p>
                    </div>
                </div>

            </div>
            <div class="col-lg-4">
                <div class="panel panel-success">
                    <div class="panel-heading">
                        <h3 class="panel-title">Узнать о нас подробнее</h3>
                    </div>
                    <div class="panel-body">
                        <p><a href="/site/about" class="btn btn-default" style="width: 100%;">О нашем проекте</a></p>
                        <p><a href="/news/item?id=2" class="btn btn-default" style="width: 100%;">Презентация</a></p>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>
<div class="modal fade" id="modalInfo34" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="myModalLabel">Информация</h4>
            </div>
            <div class="modal-body">
                Успешно! Перейдите на почту и подтвердите почту.
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
            </div>
        </div>
    </div>
</div>