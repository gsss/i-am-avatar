<?php
/**
 * Блог ЯАватар
 */
use yii\helpers\Url;


/** @var \yii\web\View $this */
/** @var string $data json */
/** @var \common\models\school\PageBlockContent $blockContent */
/** @var \common\models\school\School $school */

$object = \yii\helpers\Json::decode($data);


$center = $object['center'];
$zoom = $object['zoom'];
$height = $object['height'];
$jsonArray = \yii\helpers\Json::encode($object['markers']);

\common\assets\YandexMaps::register($this);
$this->registerJs(<<<JS
ymaps.ready(init);

function init() {
    var myMap = new ymaps.Map("map", {
            center: [{$center}],
            zoom: {$zoom}
        }, {
            searchControlProvider: 'yandex#search'
        });
    var rows = {$jsonArray};
    var i;
    var item;
    var point;
    var html;
    
    for(i = 0; i < rows.length; i++) {
        item = rows[i];
        point = item.point.split(', ');
        html = $('<b>').html(item.title)[0].outerHTML;
        html += '<br>';
        html += item.description;
        
        myMap.geoObjects
        .add(new ymaps.Placemark([point[0], point[1]], {
            balloonContent: html
        }, {
            preset: 'islands#icon',
            iconColor: '#0095b6'
        }))
        ;
    }
}

JS
);

?>
<div id="map" style="width: 100%; height: <?= $height ?>px;" id="<?= \avatar\controllers\CabinetSchoolPagesConstructorController::getBlockId($blockContent) ?>"></div>

