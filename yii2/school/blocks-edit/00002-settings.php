<?php
/**
 * Created by PhpStorm.
 * User: Metatron
 * Date: 22.12.2018
 * Time: 7:03
 */

/** @var string $data json */
/** @var int $type_id  */
/** @var int $school_id  */

$object = \yii\helpers\Json::decode($data);

\avatar\assets\Notify::register($this);

$fileNameArray = explode('/', __FILE__);
$file = $fileNameArray[count($fileNameArray) - 1];
$arr = explode('-', $file);
$numStr = $arr[0];
$num = (int)$arr[0];

$this->registerJs(<<<JS

$('.buttonSave{$numStr}').click(function(e) {
    // Идентификатор блока контента
    var type = $(this).data('type_id');
    var modal = $('#modalEdit' + type);
    var id = modal.data('id');
    ajaxJson({
        url: '/cabinet-school-pages-constructor/b2-save',
        data: {
            id: id,
            form: {
                id: modal.find('input[name="id"]').val()
            }
        },
        success: function(ret) {
              window.location.reload();
        },
        errorScript: function(ret) {
            new Noty({
                timeout: 1000,
                theme: 'sunset',
                type: 'error',
                layout: 'bottomLeft',
                text: ret.data
            }).show();
        }
    });
});

$('.buttonSettings[data-type="{$num}"]').click(function(e) {
    // Идентификатор блока контента
    var id = $(this).data('id');
    var type = $(this).data('type');
    var modal = $('#modalEdit' + type);
    modal.data('id', id);
    ajaxJson({
        url: '/cabinet-school-pages-constructor/block-get',
        data: {
            id: id
        },
        success: function(ret) {
            modal.find('input[name="id"]').val(ret.id); 
            $('#modalEdit' + type).modal();
        }
    });
});


JS
);
?>
<div class="modal fade" id="modalEdit<?= $type_id ?>" tabindex="-1" role="dialog"
     aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="myModalLabel">Видео Блок (#<?= $type_id ?>)</h4>
            </div>
            <div class="modal-body">
                <form>
                    <input type="hidden" name="_csrf" value="<?= Yii::$app->request->csrfToken ?>">
                    <input type="hidden" name="block_id" value="">
                    <div class="form-group required field-email">
                        <label class="control-label">YouTube ID</label>
                        <input type="text" class="form-control text-input" name="id" aria-required="true"
                               aria-invalid="true">
                        <p class="help-block help-block-error" style="display: none;"></p>
                    </div>

                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-success buttonSave<?= $numStr ?>" data-id=""
                        data-type_id="<?= $type_id ?>">Сохранить
                </button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
            </div>
        </div>
    </div>
</div>