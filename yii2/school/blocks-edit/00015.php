<?php
/**
 * Created by PhpStorm.
 * User: Metatron
 * Date: 22.12.2018
 * Time: 7:03
 */

/** @var string $data json */

$object = \yii\helpers\Json::decode($data);
\common\assets\Covervid::register($this);

$this->registerJs("$('.covervid-wrapper').coverVid(1920, 1080);");
?>
<style>
    .covervid-wrapper {
        position: absolute;
        top: 0;
        left: 0;
        width: 100%;
        height: 80%;
    }
</style>

<section class="masthead" id="<?= \avatar\controllers\CabinetSchoolPagesConstructorController::getBlockId($blockContent) ?>">
    <div class="covervid-wrapper">
        <video class="covervid-video" autoplay loop poster="<?= $object['image'] ?>">
            <source src="<?= $object['mp4'] ?>" type="video/mp4">
        </video>
    </div>
    <div class="masthead-overlay"></div>
    <div class="masthead-arrow"></div>
    <h1>covervid<span>Background Video Cover</span></h1>
    <a class="masthead-video-credit" href="http://vimeo.com/82495711" target="_blank">
        <span>Video: Dreamscapes</span>
        <span>by Jonathan Besler</span>
    </a>
</section>



