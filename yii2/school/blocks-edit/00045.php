<?php
/**
 * Created by PhpStorm.
 * User: Metatron
 * Date: 22.12.2018
 * Time: 7:03
 */

/** @var \yii\web\View $this */
/** @var string $data json */
/** @var \common\models\school\PageBlockContent $blockContent */


/** Интернет магазин */

$object = \yii\helpers\Json::decode($data);
$school = $blockContent->getPage()->getSchool();

?>
<div class="container" id="<?= \avatar\controllers\CabinetSchoolPagesConstructorController::getBlockId($blockContent) ?>">
    <?php /** @var \common\models\shop\Product $product */ ?>
    <?php foreach (\common\models\shop\Product::find()->where(['school_id' => $school->id])->all() as $product)  { ?>
        <div class="col-lg-4">
            <p><a href="<?= \yii\helpers\Url::to(['shop/item', 'id' => $product->id ]) ?>"><img src="<?= \common\widgets\FileUpload7\FileUpload::getFile($product->image, 'crop') ?>" width="100%" class="thumbnail"></a></p>
            <p class="text-center"><?= $product->name ?></p>
            <p class="text-center">
                <?php $currency = \common\models\avatar\Currency::findOne($product->currency_id); ?>
                <?php if (!is_null($currency)) { ?>
                    <?php $price = $product->price / pow(10, $currency->decimals); ?>
                    <?= Yii::$app->formatter->asDecimal($price, $currency->decimals) ?> <span class="label label-info"><?= $currency->code ?></span>
                <?php } ?>
            </p>
            <p><a href="<?= \yii\helpers\Url::to(['shop/order', 'id' => $product->id]) ?>" class="btn btn-success" style="width: 100%">Купить</a></p>
        </div>
    <?php } ?>
</div>