<?php
/**
 *
 */

/** @var \yii\web\View $this */
/** @var string $data json */
/** @var \common\models\school\PageBlockContent $blockContent */

$object = \yii\helpers\Json::decode($data);

$school = $blockContent->getPage()->getSchool();
$image = $school->image;
if (\cs\Application::isEmpty($image)) {
    $image = 'https://www.i-am-avatar.com/images/avatarlogo1.png';
}
?>


<nav class="navbar navbar-default">
    <div class="container">
        <a class="navbar-brand" href="/" style="padding: 5px 10px 5px 10px;">
            <img src="<?= $image ?>" width="40" data-toggle="tooltip" data-placement="bottom" title=""
                 data-original-title="Главная">
        </a>
    </div>
</nav>