<?php
/**
 * Created by PhpStorm.
 * User: Metatron
 * Date: 22.12.2018
 * Time: 7:03
 */

/** @var string $data json */
/** @var int $type_id  */
/** @var int $school_id  */

$object = \yii\helpers\Json::decode($data);

\common\assets\JqueryUpload::register($this);

$this->registerJs(<<<JS

$('.buttonSave00005').click(function(e) {
    var type = $(this).data('type_id');
    var modal = $('#modalEdit'+type);
    var id = modal.data('id');
    ajaxJson({
        url: '/cabinet-school-pages-constructor/block00005-save',
        data: {
            id: id,
            form: {
                image: modal.find('div[data-id="picture"] img').attr('src'),
            }
        },
        success: function(ret) {
            $('#modalEdit'+type).modal('hide');
        }
    });
});

$('.buttonSettings[data-type="5"]').click(function(e) {
    // Идентификатор блока контента
    var id = $(this).data('id');
    var type = $(this).data('type');
    var modal = $('#modalEdit'+type);
    modal.data('id', id);
    ajaxJson({
        url: '/cabinet-school-pages-constructor/block-get',
        data: {
            id: id,
        },
        success: function(ret) {
            modal.find('div[data-id="picture"]').html($('<img>', {src: ret.image, style:'width:100%;max-width:200px'})); 
            $('#modalEdit' + type).modal();
        }
    });
});



JS
);
?>
<div class="modal fade" id="modalEdit<?= $type_id ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Картинка Блок (#<?= $type_id ?>)</h4>
            </div>
            <div class="modal-body">
                <form >
                    <input type="hidden" name="_csrf" value="<?= Yii::$app->request->csrfToken ?>">
                    <input type="hidden" name="block_id" value="">
                    <div class="form-group required field-email">
                        <label class="control-label" for="field_1_email">Картинка</label>

                        <?= \common\widgets\FileUpload7\FileUpload::widget([
                            'id'        => 'upload' . $type_id,
                            'name'      => 'upload' . $type_id,
                            'attribute' => 'upload' . $type_id,
                            'model'     => new \avatar\base\Model,
                            'update'    => \avatar\controllers\CabinetSchoolPagesConstructorController::getUpdate(),
                            'settings'  => \avatar\controllers\CabinetSchoolPagesConstructorController::getSettings($school_id, 1),
                        ]); ?>
                    </div>

                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-success buttonSave00005" data-id="" data-type_id="<?= $type_id ?>">Сохранить</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
            </div>
        </div>
    </div>
</div>

