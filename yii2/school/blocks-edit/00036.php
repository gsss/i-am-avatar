<?php
/**
 * Видео Vimeo
 */

/** @var string $data json */

$object = \yii\helpers\Json::decode($data);

?>
<div class="container" id="<?= \avatar\controllers\CabinetSchoolPagesConstructorController::getBlockId($blockContent) ?>">
    <div class="col-lg-8 col-lg-offset-2">
        <iframe src="https://player.vimeo.com/video/<?= $object['id'] ?>" width="100%" height="360" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe>
    </div>
</div>

