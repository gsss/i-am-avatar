<?php
/**
 * Created by PhpStorm.
 * User: Metatron
 * Date: 22.12.2018
 * Time: 7:03
 */
use yii\helpers\Url;


/** @var \yii\web\View $this */
/** @var string $data json */
/** @var \common\models\school\PageBlockContent $blockContent */
/** @var \common\models\school\School $school */

$object = \yii\helpers\Json::decode($data);

$page = $blockContent->getPage();
$isMeta = false;

foreach ($this->metaTags as $tag) {
    if (strpos($tag, 'name="og:title"')) {
        $isMeta = true;
        break;
    }
}

$options = [
    'title'       => (!empty($page->facebook_title)) ? $page->facebook_title : $page->name,
    'description' => (!empty($page->facebook_description)) ? $page->facebook_description : $page->name,
    'image'       => (!empty($page->facebook_image)) ? 'https://www.i-am-avatar.com' . $page->facebook_image : '',
    'url'         => $page->getUrl(),
];

?>
<div class="container"
     id="<?= \avatar\controllers\CabinetSchoolPagesConstructorController::getBlockId($blockContent) ?>">

    <center>
        <?php if ($isMeta) { ?>
            <?= $this->render('@school/views/blocks/shareButton', $options) ?>
        <?php } else { ?>
            <?= $this->render('@school/views/blocks/share', $options) ?>
        <?php } ?>
    </center>

</div>
