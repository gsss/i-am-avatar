<?php
/**
 * Кнопки соц сетей
 */

/** @var \yii\web\View $this */
/** @var string $data json */
/** @var \common\models\school\PageBlockContent $blockContent */

$object = \yii\helpers\Json::decode($data);


$rows = [
    [
        'type'  => 'telegram',
        'name'  => 'Телеграм',
        'image' => '//www.people4people.su/images/mail/subscribe/social-net/Telegram.png',
    ],
    [
        'type'  => 'vk',
        'name'  => 'VK',
        'image' => '//www.people4people.su/images/mail/subscribe/social-net/vk.png',
    ],
    [
        'type'  => 'fb',
        'name'  => 'Facebook',
        'image' => '//www.people4people.su/images/mail/subscribe/social-net/fb.png',
    ],
    [
        'type'  => 'youtube',
        'name'  => 'YouTube',
        'image' => '//www.people4people.su/images/mail/subscribe/social-net/media.png',
    ],
    [
        'type'  => 'instagram',
        'name'  => 'Instagram',
        'image' => '//www.people4people.su/images/mail/subscribe/social-net/ins.png',
    ],
];

$options = [
    'class' => 'container',
    'id' => \avatar\controllers\CabinetSchoolPagesConstructorController::getBlockId($blockContent),
];
if (isset($object['margin_top'])) {
    if (!\cs\Application::isEmpty($object['margin_top'])) {
        $options['style']['margin-top'] =  $object['margin_top'];
    }
}
if (isset($object['margin_bottom'])) {
    if (!\cs\Application::isEmpty($object['margin_bottom'])) {
        $options['style']['margin-bottom'] =  $object['margin_bottom'];
    }
}
if (isset($options['style'])) {
    if (is_array($options['style'])) {
        $options['style'] = \yii\helpers\Html::cssStyleFromArray($options['style']);
    }
}
?>
<?= \yii\helpers\Html::beginTag('div', $options) ?>
    <div class="col-lg-8 col-lg-offset-2">
        <center>
            <?php
            foreach ($object['items'] as $item) {
                foreach ($rows as $r) {
                    if ($r['type'] == $item['type']) {
                        $options = [
                            'width' => 43,
                            'style' => 'margin-right: 10px;',
                        ];
                        if (isset($item['title'])) {
                            $options['data']['toggle'] = 'tooltip';
                            $options['title'] = $item['title'];
                        }
                        echo \yii\helpers\Html::a(
                            \yii\helpers\Html::img($r['image'], $options),
                            $item['href'],
                            ['target' => '_blank']
                        );
                    }
                }
            }
            ?>
        </center>
    </div>
</div>

