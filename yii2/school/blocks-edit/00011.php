<?php
/**
 * Created by PhpStorm.
 * User: Metatron
 * Date: 22.12.2018
 * Time: 7:03
 */

/** @var \yii\web\View $this */
/** @var string $data json */
/** @var \common\models\school\PageBlockContent $blockContent  */

$object = \yii\helpers\Json::decode($data);


\common\assets\InlineEdit\Asset::register($this);

$this->registerJs(<<<JS
InlineEdit.init();
JS
);
?>
<div class="container" id="<?= \avatar\controllers\CabinetSchoolPagesConstructorController::getBlockId($blockContent) ?>">
    <div class="col-lg-12">
        <h1 class="text-center page-header js-editable" data-id="<?= $blockContent->id ?>"><?= $object['text'] ?></h1>
    </div>
</div>

