<?php
/**
 * Created by PhpStorm.
 * User: Ra-m-ha
 * Date: 24.10.2019
 * Time: 12:41
 */

return [
    'lesnayaopushka' => ['lesnaya-opushka.ru', 'www.lesnaya-opushka.ru', 'lesnayaopushka.i-am-avatar.com', 'lesnayaopushka.i-am-avatar.local'],
    'zdravo'         => ['zdravo.i-am-avatar.com', 'zdravo.i-am-avatar.local', 'zdravo.itsphere.space'],
];