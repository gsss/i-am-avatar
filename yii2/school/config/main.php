<?php

$params = \yii\helpers\ArrayHelper::merge(
    require(__DIR__ . '/../../common/config/params.php'),
    require(__DIR__ . '/params.php')
);

$schools = require('school.php');
$module = 'index';
foreach ($schools as $k => $urlList) {
    if (in_array($_SERVER['HTTP_HOST'], $urlList)) $module = $k;
}
$bootstrap = array_merge(['log'], [$module]);

$config = [
    'id'                                              => 'avatar-school',
    'basePath'                                        => dirname(__DIR__),
    'bootstrap'                                       => $bootstrap,
    'controllerNamespace'                             => 'school\controllers',
    'vendorPath'                                      => dirname(dirname(__DIR__)) . '/vendor',
    'language'                                        => 'ru',
    'sourceLanguage'                                  => 'ru',
    'timeZone'                                        => 'Etc/GMT-3',
    'aliases'                                         => [
        '@csRoot'  => __DIR__ . '/../../common/app',
        '@cs'      => __DIR__ . '/../../common/app',
        '@webroot' => __DIR__ . '/../../public_html',
    ],
    'components'                                      => [
        'session'         => [
            'class'   => 'yii\web\CacheSession',
            'timeout' => 600,
            'cache'   => 'cacheSession',
        ],
        'mailer'          => [
            'viewPath' => '@school/mail',
        ],
        'urlManager'      => [
            'rules' => [
                'invest-program' => 'invest-program/index',
                'blago-program'  => 'blago-program/index',
                'blago-program2' => 'blago-program/index2',

                'site/captcha/<refresh:\d+>' => 'site/captcha',
                'site/captcha/<v:\w+>'       => 'site/captcha',
                [
                    'class' => '\school\services\CompanyUrlRule2',
                    'url'   => 'covid-cert/status',
                ],
                [
                    'class' => '\school\services\CompanyUrlRule2',
                    'url'   => 'covid-cert/qr-code',
                ],
                [
                    'class' => '\school\services\CompanyUrlRule2',
                    'url'   => 'covid-cert/qr-code2',
                ],

                '<controller>/<action>'           => '<controller>/<action>',
                '<controller>/<action>/<id:\\w+>' => '<controller>/<action>',
            ],
        ],
        'languageManager' => [
            'class'    => 'common\components\LanguageManager',
            'strategy' => [
                'ru' => ['ru', 'be', 'uk'],
                'en' => ['en', 'fr', 'de'],
            ],
            'default'  => 'ru',
        ],
        'authManager'     => [
            'class'          => 'yii\rbac\PhpManager',
            'itemFile'       => '@avatar/rbac/items.php',
            'assignmentFile' => '@avatar/rbac/assignments.php',
            'ruleFile'       => '@avatar/rbac/rules.php',
        ],
        'assetManager'    => [
            'appendTimestamp' => true,
            'basePath'        => '@school/web/assets',
            'bundles' => [
                'yii\web\JqueryAsset' => [
                    'sourcePath' => null,
                    'js'         => ['/admin/vendors/jquery/dist/jquery.min.js'],
                ],
                '\yii\web\JqueryAsset' => [
                    'sourcePath' => null,
                    'js'         => ['/admin/vendors/jquery/dist/jquery.min.js'],
                ],
            ],
        ],
        'piramida'        => [
            'class' => 'app\common\components\Piramida',
        ],
        'request'         => [
            // !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
            'cookieValidationKey'    => '',
            'enableCookieValidation' => false,
        ],
        'deviceDetect'    => [
            'class'     => 'avatar\services\DeviceDetect',
            'setParams' => 'false',
        ],
        'user'            => [
            'identityClass'   => 'common\models\UserAvatar',
            'enableAutoLogin' => true,
            'loginUrl'        => ['auth/login'],
        ],
        'errorHandler'    => [
            'errorAction' => 'site/error',
            'errorView'   => '@school/views/site/error.php',
        ],
        'formatter'       => [
            'class'             => '\iAvatar777\components\FormatterRus\FormatterRus',
            'dateFormat'        => 'php:d.m.b/Y',
            'timeFormat'        => 'php:H:i:s',
            'datetimeFormat'    => 'php:d.m.Y/b H:i',
            'decimalSeparator'  => '.',
            'thousandSeparator' => ',',
            'currencyCode'      => 'RUB',
            'locale'            => 'ru',
            'nullDisplay'       => '',
        ],

        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets'    => [
                [
                    'class'  => 'common\services\DbTarget',
                    'levels' => [
                        'warning',
                        'error',
                    ],
                    'except' => [
                        'yii\web\HttpException:404',
                        'yii\web\HttpException:403',
                        'cs\web\Exception',
                    ],

                    'db'       => 'dbStatistic',
                    'logTable' => 'log2_school',
                ],
                [
                    'class'      => 'common\services\DbTarget',
                    'categories' => ['avatar\\*'],
                    'levels'     => [
                        'info',
                        'trace',
                    ],

                    'db'       => 'dbStatistic',
                    'logTable' => 'log2_school',
                ],
            ],
        ],
    ],
    'modules'                                         => [
        'gridview' => [
            'class' => '\kartik\grid\Module'
            // enter optional module parameters below - only if you need to
            // use your own export download action or custom translation
            // message source
            // 'downloadAction' => 'gridview/export/download',
            // 'i18n' => []
        ],
        'index'          => ['class' => '\school\modules\index\Module'],
        'avr3'           => ['class' => '\school\modules\avr3\Module'],
        'zdravo'         => ['class' => '\school\modules\zdravo\Module'],
        'lesnayaopushka' => ['class' => '\school\modules\lesnayaopushka\Module'],
    ],
    'params'                                          => $params,
    'controllerMap'                                   => [
        'upload'  => '\common\widgets\HtmlContent\Controller',
        'upload3' => '\common\widgets\FileUploadMany2\UploadController',
    ],
    'on beforeRequest'                                => function ($event) {
        Yii::$app->languageManager->onBeforeRequest();
        Yii::$app->onlineManager->onBeforeRequest();
        Yii::$app->monitoring->onBeforeRequest();
        if (!YII_ENV_DEV) \common\models\statistic\ServerRequest::add();

        // для почтовика, так как исполняясь в default настройках nginx в  $_SERVER['SERVER_NAME'] = '_'. А почтовик из-за этого падает и выдает Expected response code 250 but got code "501", with message "501 Syntactically invalid HELO argument(s)
        if (isset($_SERVER['HTTP_HOST'])) {
            $_SERVER['SERVER_NAME'] = $_SERVER['HTTP_HOST'];
        } else {
            Yii::warning(['isset($_SERVER[\'HTTP_HOST\']) == false', $_SERVER], 'avatar\config\school');
        }

        // сохраняю параметр parent_id для партнерки
        \avatar\modules\Shop\Shop::onBeforeRequest();

        Yii::$app->urlManager->hostInfo = \common\models\school\School::get()->getUrl();
    },
    'on ' . \yii\web\Application::EVENT_AFTER_REQUEST => function ($event) {
        Yii::$app->monitoring->onAfterRequest();
    },
];

if (env('MEMCACHE_ENABLED', false)) {
    $config['components']['cacheSession'] = [
        'keyPrefix'    => 'session',
        'class'        => 'yii\caching\MemCache',
        'useMemcached' => true,
        'servers'      => [
            [
                'host' => env('MEMCACHE_HOST', 'localhost'),
                'port' => env('MEMCACHE_PORT', 11211),
            ],
        ],
    ];
} else {
    $config['components']['cacheSession'] = [
        'keyPrefix'    => 'session',
        'class'        => 'yii\caching\FileCache',
    ];
}

if (YII_DEBUG) {
    // configuration adjustments for 'dev' environment
    $config['bootstrap'][] = 'debug';
    $config['modules']['debug'] = [
        'class' => 'yii\debug\Module',
        'allowedIPs' => ['*'],
    ];
    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = [
        'class' => 'yii\gii\Module',
    ];
}

return $config;
