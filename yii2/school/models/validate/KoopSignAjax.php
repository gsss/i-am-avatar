<?php

namespace school\models\validate;

use avatar\models\forms\school\Article;
use avatar\models\UserEnter;
use BitcoinPHP\BitcoinECDSA\BitcoinECDSA;
use common\components\providers\ETH;
use common\models\avatar\Currency;
use common\models\avatar\UserBill;
use common\models\avatar\UserBillOperation;
use common\models\BlogItem;
use common\models\PassportLink;
use common\models\RequestTokenCreate;
use common\models\school\AdminLink;
use common\models\school\Kurs;
use common\models\school\Potok;
use common\models\school\PotokUser3Link;
use common\models\school\School;
use common\models\school\SubscribeItem;
use common\models\shop\Request;
use common\models\UserAvatar;
use common\models\UserDigitalSign;
use common\models\UserDocument;
use common\models\UserRoot;
use common\models\UserRootExt;
use common\services\Security;
use cs\Application;
use cs\services\File;
use cs\services\Str;
use cs\services\UploadFolderDispatcher;
use cs\services\Url;
use cs\services\VarDumper;
use Imagine\Image\Box;
use Yii;
use yii\base\Model;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;
use yii\helpers\StringHelper;
use yii\imagine\Image;

/**
 */
class KoopSignAjax extends Model
{
    /** @var int идентификатор UserEnter */
    public $id;

    /** @var  School */
    public $school;

    /** @var UserEnter */
    public $item;

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            ['id', 'required'],
            ['id', 'integer'],
            ['id', 'validateUserEnter'],
        ];
    }

    public function validateUserEnter($attr, $params)
    {
        if (!$this->hasErrors()) {
            $item = UserEnter::findOne($this->id);
            if (is_null($item)) {
                $this->addError($attr, 'UserEnter не найдена');
                return;
            }
            $s = School::findOne($item->school_id);

            $this->school = $s;
            $this->item = $item;
        }
    }

    /**
     *
     */
    public function action()
    {
        $UserEnter = $this->item;

        $pk2 = UserDigitalSign::getPK($UserEnter->user_id);

        $pk2Object = new BitcoinECDSA();
        $pk2Object->setPrivateKey($pk2);

        $hash = $UserEnter->hash;
        $signedMessage2= $pk2Object->signMessage($hash, true);

        $UserEnter->public_key2 = $pk2Object->getAddress();
        $UserEnter->signature2 = $signedMessage2;
        $UserEnter->save();

        return ['sign' => $UserEnter->signature2];
    }

}
