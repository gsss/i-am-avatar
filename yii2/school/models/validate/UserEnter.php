<?php

namespace school\models\validate;

use avatar\models\forms\BlogItem;
use avatar\models\UserRegistration;
use avatar\modules\UniSender\UniSender;
use common\components\providers\BTC;
use common\models\avatar\Currency;
use common\models\avatar\UserBill;
use common\models\avatar\UserBillDefault;
use common\models\avatar\UserBinance;
use common\models\school\File;
use common\models\school\School;
use common\models\UserAvatar;
use common\models\UserDigitalSign;
use common\services\Subscribe;
use common\widgets\FileUpload7\FileUpload;
use cs\Application;
use cs\services\Security;
use cs\services\SitePath;
use cs\services\VarDumper;
use iAvatar777\services\FormAjax\Model;
use Imagine\Image\Box;
use yii\base\Exception;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;
use yii\helpers\FileHelper;
use yii\helpers\Url;
use cs\services\UploadFolderDispatcher;
use yii\imagine\Image;
use Imagine\Image\ManipulatorInterface;
use yii\web\IdentityInterface;


/**
 * @property integer id
 * @property integer created_at
 * @property integer status
 * @property integer user_id
 * @property string  email
 * @property string  iof
 * @property string  passport
 * @property string  place
 * @property string  address
 * @property string  phone
 * @property string  whatsapp
 * @property string  passport_scan1
 * @property string  passport_scan2
 */
class UserEnter extends Model
{
    public $_school_id;

    public $address_pochta;
    public $name_first;
    public $name_last;
    public $name_middle;
    public $inn;
    public $snils;
    public $grajd;
    public $passport_ser;
    public $passport_number;
    public $passport_vidan_kem;

    /** @var  \DateTime */
    public $passport_vidan_date;
    public $passport_vidan_num;

    /** @var  \DateTime */
    public $passport_born_date;
    public $passport_scan1;
    public $passport_scan2;
    public $passport_born_place;
    public $passport_reg_address;

    public $accept_rules;

    public function attributeWidgets()
    {
        return [
            'passport_scan1'      => [
                'class'    => '\iAvatar777\widgets\FileUpload7\FileUpload',
                'update'   => \avatar\controllers\CabinetSchoolPagesConstructorController::getUpdate(),
                'settings' => \avatar\controllers\CabinetSchoolPagesConstructorController::getSettingsLibrary($this->_school_id, 21),
                'events'   => [
                ],
            ],
            'passport_scan2'      => [
                'class'    => '\iAvatar777\widgets\FileUpload7\FileUpload',
                'update'   => \avatar\controllers\CabinetSchoolPagesConstructorController::getUpdate(),
                'settings' => \avatar\controllers\CabinetSchoolPagesConstructorController::getSettingsLibrary($this->_school_id, 22),
                'events'   => [
                ],
            ],
            'accept_rules'        => '\common\widgets\CheckBox3\CheckBox',
            'passport_born_date'  => ['class' => '\iAvatar777\widgets\DateTime\DateTime', 'dateFormat' => 'php:d.m.Y',],
            'passport_vidan_date' => ['class' => '\iAvatar777\widgets\DateTime\DateTime', 'dateFormat' => 'php:d.m.Y',],
        ];
    }

    public function rules()
    {
        return [
            ['passport_reg_address', 'required'],
            ['passport_reg_address', 'string'],

            ['grajd', 'required'],
            ['grajd', 'string'],

            ['inn', 'required'],
            ['inn', 'string'],

            ['snils', 'required'],
            ['snils', 'string'],

            ['passport_scan1', 'required'],
            ['passport_scan1', 'string'],

            ['passport_scan2', 'required'],
            ['passport_scan2', 'string'],

            ['accept_rules',   'integer', ],
            ['accept_rules',   'compare', 'compareValue' => 1, 'operator' => '==', 'message' => 'Нужно ознакомиться с правилами и принять их'],

            ['name_first', 'required'],
            ['name_first', 'string'],

            ['name_last', 'required'],
            ['name_last', 'string'],

            ['name_middle', 'required'],
            ['name_middle', 'string'],

            ['passport_ser', 'required'],
            ['passport_ser', 'string', 'length' => 4],

            ['passport_number', 'required'],
            ['passport_number', 'string', 'length' => 6],

            ['passport_vidan_kem', 'required'],
            ['passport_vidan_kem', 'string'],

            ['passport_vidan_date', 'required'],
            ['passport_vidan_date', '\iAvatar777\widgets\DateTime\Validator'],

            ['passport_vidan_num', 'required'],
            ['passport_vidan_num', 'string', 'length' => 7],

            ['passport_born_date', 'required'],
            ['passport_born_date', '\iAvatar777\widgets\DateTime\Validator'],

            ['passport_born_place', 'required'],
            ['passport_born_place', 'string'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'address_pochta'       => 'Адрес для бумажной корреспонденции',
            'inn'                  => 'ИНН',
            'snils'                => 'СНИЛС',
            'grajd'                => 'Гражданство',
            'name_first'           => 'Имя',
            'name_last'            => 'Фамилия',
            'name_middle'          => 'Отчество',
            'passport_ser'         => 'Серия',
            'passport_number'      => 'Номер',
            'passport_vidan_kem'   => 'Кем выдан',
            'passport_vidan_date'  => 'Когда выдан',
            'passport_vidan_num'   => 'Код подразделения',
            'passport_born_date'   => 'Дата рождения',
            'passport_scan1'       => 'Скан паспорта 1',
            'passport_scan2'       => 'Скан паспорта 2',
            'passport_born_place'  => 'Место рождения',
            'passport_reg_address' => 'Адрес регистрации',
            'accept_rules'         => 'Согласие на обработку персональных данных',
        ];
    }



    public function save($runValidation = true, $attributeNames = null)
    {
        $i = new \common\models\UserEnter([
            'passport_scan1'      => $this->passport_scan1,
            'passport_scan2'      => $this->passport_scan2,
            'name_first'          => $this->name_first,
            'name_last'           => $this->name_last,
            'name_middle'         => $this->name_middle,
            'passport_ser'        => $this->passport_ser,
            'passport_number'     => $this->passport_number,
            'passport_vidan_kem'  => $this->passport_vidan_kem,
            'passport_vidan_date' => $this->passport_vidan_date->format('Y-m-d'),
            'passport_vidan_num'  => $this->passport_vidan_num,
            'passport_born_date'  => $this->passport_born_date->format('Y-m-d'),
            'passport_born_place' => $this->passport_born_place,

            'inn'                  => $this->inn,
            'snils'                => $this->snils,
            'grajd'                => $this->grajd,
            'passport_reg_address' => $this->passport_reg_address,
        ]);

        $s = School::get();
        $i->school_id = $s->id;
        $i->status = \common\models\UserEnter::STATUS_AFTER_REGISTRATION;
        $i->user_id = \Yii::$app->user->id;
        $i->user2_id = \Yii::$app->user->id;
        $i->created_at = time();
        $i->save();

        /** @var UserAvatar $user */
        $user = \Yii::$app->user->identity;

        return [
            'model'    => $i,
            'is_phone' => $user->phone_is_confirm,
            'is_sign'  => UserDigitalSign::isPK($user->id)? 1 : 0,
        ];
    }

}
