<?php

namespace school\models\validate;

use avatar\models\forms\school\Article;
use avatar\models\UserEnter;
use BitcoinPHP\BitcoinECDSA\BitcoinECDSA;
use common\components\providers\ETH;
use common\models\avatar\Currency;
use common\models\avatar\UserBill;
use common\models\avatar\UserBillOperation;
use common\models\BlogItem;
use common\models\KoopPodryadCommandUser;
use common\models\PassportLink;
use common\models\RequestTokenCreate;
use common\models\school\AdminLink;
use common\models\school\Kurs;
use common\models\school\Potok;
use common\models\school\PotokUser3Link;
use common\models\school\School;
use common\models\school\SubscribeItem;
use common\models\shop\Request;
use common\models\UserAvatar;
use common\models\UserDigitalSign;
use common\models\UserDocument;
use common\models\UserRoot;
use common\models\UserRootExt;
use common\services\Security;
use cs\Application;
use cs\services\File;
use cs\services\Str;
use cs\services\UploadFolderDispatcher;
use cs\services\Url;
use cs\services\VarDumper;
use Imagine\Image\Box;
use Yii;
use yii\base\Model;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;
use yii\helpers\StringHelper;
use yii\imagine\Image;

/**
 */
class CabinetKoopPodryadStep4TzSign extends Model
{
    /** @var int идентификатор \common\models\KoopPodryad */
    public $task_id;

    /** @var \common\models\KoopPodryad */
    public $item;

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            ['task_id', 'required'],
            ['task_id', 'integer'],
            ['task_id', 'validateUserEnter'],
        ];
    }

    public function validateUserEnter($attr, $params)
    {
        if (!$this->hasErrors()) {
            $item = \common\models\KoopPodryad::findOne($this->task_id);
            if (is_null($item)) {
                $this->addError($attr, '\common\models\KoopPodryad не найдена');
                return;
            }

            $this->item = $item;
        }
    }

    /**
     *
     */
    public function action()
    {
        $UserEnter = $this->item;

        $pk2 = UserDigitalSign::getPK(Yii::$app->user->id);

        $pk2Object = new BitcoinECDSA();
        $pk2Object->setPrivateKey($pk2);

        $hash = $UserEnter->tz_hash;
        $signedMessage2= $pk2Object->signMessage($hash, true);

        /** @var \common\models\KoopPodryadCommandUser $KoopPodryadCommandUser */
        $KoopPodryadCommandUser = KoopPodryadCommandUser::findOne(['podryad_id' => $this->task_id, 'user_id' => Yii::$app->user->id]);
        $KoopPodryadCommandUser->tz_address = $pk2Object->getAddress();
        $KoopPodryadCommandUser->tz_sign = $signedMessage2;
        $KoopPodryadCommandUser->tz_created_at = time();
        $KoopPodryadCommandUser->save();

        return $KoopPodryadCommandUser;
    }

}
