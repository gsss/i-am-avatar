<?php

namespace school\models\validate;

use BitcoinPHP\BitcoinECDSA\BitcoinECDSA;
use common\components\providers\ETH;
use common\models\avatar\Currency;
use common\models\avatar\UserBill;
use common\models\avatar\UserBillOperation;
use common\models\PassportLink;
use common\models\RequestTokenCreate;
use common\models\school\AdminLink;
use common\models\school\Kurs;
use common\models\school\Potok;
use common\models\school\PotokUser3Link;
use common\models\school\School;
use common\models\school\SubscribeItem;
use common\models\school\UserLink;
use common\models\task\Helper;
use common\models\task\Task;
use common\models\UserAvatar;
use common\models\UserDigitalSign;
use common\models\UserEnter;
use common\models\UserRoot;
use common\models\UserRootExt;
use common\services\Subscribe;
use cs\services\File;
use cs\services\Str;
use cs\services\UploadFolderDispatcher;
use cs\services\VarDumper;
use Imagine\Image\Box;
use Yii;
use yii\base\Model;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;
use yii\imagine\Image;

/**
 */
class KoopSign2Ajax extends \iAvatar777\services\FormAjax\Model
{
    /** @var  int идентиифкатор \avatar\models\UserEnter */

    public $id;
    public $secret;
    public $signedMessage2;

    /**
     * @var \BitcoinPHP\BitcoinECDSA\BitcoinECDSA
     */
    public $pk2Object;

    /** @var string */
    public $hash;

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            ['id', 'required'],
            ['id', 'integer'],
            ['id', 'exist', 'targetClass' => '\avatar\models\UserEnter'],

            ['secret', 'required'],
            ['secret', 'string'],
            ['secret', 'validateSecret'],
        ];
    }

    public function validateSecret($attr, $params)
    {
        if (!$this->hasErrors()) {
            $pk2Object = new BitcoinECDSA();

            try {
                $pk2Object->setPrivateKey($this->secret);
            } catch (\Exception $e) {
                $this->addError($attr, $e->getMessage());
                return;
            }
            $uSign = UserDigitalSign::findOne(['user_id' => Yii::$app->user->id]);
            if ($uSign->address != $pk2Object->getAddress()) {
                $this->addError($attr, 'Это не ваш ключ или не верный');
                return;
            }

            $UserEnter = \avatar\models\UserEnter::findOne($this->id);
            $signedMessage2 = $pk2Object->signMessage($UserEnter->hash, true);
            $this->signedMessage2 = $signedMessage2;

            $this->pk2Object = $pk2Object;
        }
    }

    public function save($runValidation = true, $attributeNames = null)
    {
        $UserEnter = UserEnter::findOne($this->id);
        $UserEnter->public_key2 = $this->pk2Object->getAddress();
        $UserEnter->signature2 = $this->signedMessage2;
        $UserEnter->status = UserEnter::STATUS_AFTER_SIGN2;
        $UserEnter->save();

        // Отправить уведомление пользователю user1
        $u = UserAvatar::findOne($UserEnter->user1_id);
        Subscribe::sendArraySchool(School::findOne($UserEnter->school_id), [$u->email], 'Заявка подписана', 'koop/request_enter_sign2', [
            'request' => $UserEnter,
        ]);
    }
}
