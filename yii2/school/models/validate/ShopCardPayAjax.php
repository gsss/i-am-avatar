<?php
namespace school\models\validate;

use common\models\avatar\CurrencyLink;
use common\models\avatar\UserBill;
use common\models\BillingMain;
use common\models\BillingMainClass;
use common\models\Card;
use common\models\investment\IcoRequest;
use common\models\investment\IcoRequestSuccess;
use common\models\PaySystemConfig;
use common\models\piramida\Currency;
use common\models\piramida\Wallet;
use common\models\school\School;
use common\models\shop\CatalogItem;
use common\models\shop\DeliveryItem;
use common\models\shop\Product;
use cs\Application;
use cs\services\VarDumper;
use yii\base\Model;
use yii\helpers\Json;
use Yii;


/**
 */
class ShopCardPayAjax extends Model
{
    /** @var int ico_request.id */
    public $id;
    public $card;
    public $pin;

    /** @var Product */
    public $Product;

    /** @var Card */
    public $cardObject;

    /** @var UserBill */
    public $UserBill;

    /** @var Wallet */
    public $wallet;

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            ['id', 'required'],
            ['id', 'integer'],

            ['card', 'required'],
            ['card', 'string'],

            ['pin', 'required'],
            ['pin', 'string', 'length' => 4],
            ['pin', 'validatePin'],

            ['card', 'validateMoney'],
        ];
    }

    /**
     * Проверяет Заказ
     *
     * @param $attribute
     * @param $params
     */
    public function validatePin($attribute, $params)
    {
        if (!$this->hasErrors()) {
            $card = Card::findOne(['number' => $this->card]);
            $this->cardObject = $card;
            if (!password_verify($this->pin, $card->pin)) {
                $this->addError('pin', 'Не верный код');
                return;
            }
        }
    }

    /**
     * Проверяет Заказ
     *
     * @param $attribute
     * @param $params
     */
    public function validateMoney($attribute, $params)
    {
        if (!$this->hasErrors()) {
            $card = $this->cardObject;
            $product = Product::findOne($this->id);
            $this->Product = $product;
            $link = CurrencyLink::findOne(['currency_ext_id' => $product->currency_id]);

            $bill = UserBill::find()->where([
                'currency'     => $link->currency_ext_id,
                'user_id'      => $card->user_id,
                'mark_deleted' => 0,
                'card_id'      => $card->id,
            ])->one();

            if (is_null($bill)) {
                $c = Currency::findOne($link->currency_int_id);
                $this->addError('card', 'На карте нет счета: '.$c->code);
                return;
            }
            $this->UserBill = $bill;
            $w = Wallet::findOne($bill->address);
            $this->wallet = $w;
            if ($w->amount < $product->price) {
                $this->addError('card', 'Не достаточно денег');
                return;
            }
        }
    }

    /**
     * Создает
     * Счет \common\models\BillingMain
     * Заказ \common\models\shop\Request
     * Добавляет статус \common\models\shop\Request::STATUS_SEND_TO_SHOP
     * Добавляю статус: \common\models\shop\Request::STATUS_ORDER_DOSTAVKA
     * Добавляю товары в заказ
     * списываю деньги
     * закрываю заказ
     *
     * @return array
     *
     * @throws
     */
    public function action()
    {
        $product = $this->Product;
        $school = School::findOne($product->school_id);

        $delivery = DeliveryItem::findOne(['school_id' => $school->id]);

        // Выбираю платежную систему
        $PaySystemConfig = $this->getPaySystem($school, $product);

        $BillingMainClass = BillingMainClass::findOne(['name' => '\common\models\shop\Request']);

        // Создаю счет на оплату
        $billing = BillingMain::add([
            'sum_before'  => $product->price,
            'sum_after'   => $product->price,
            'source_id'   => $PaySystemConfig->paysystem_id,
            'class_id'    => $BillingMainClass->id,
            'currency_id' => $product->currency_id,
            'config_id'   => $PaySystemConfig->id,
        ]);
        $card = $this->cardObject;

        // Создаю заявку
        $requestClass = \common\models\shop\Request::add([
            'user_id'     => $card->user_id,
            'school_id'   => $school->id,
            'address'     => '',
            'comment'     => '',
            'phone'       => '',
            'dostavka_id' => $delivery->id,
            'price'       => $product->price,
            'currency_id' => $product->currency_id,
            'billing_id'  => $billing->id,
            'sum'         => $product->price,
        ]);

        // Добавляю статус: Заказ отправлен в магазин
        $requestClass->addStatusToShop(\common\models\shop\Request::STATUS_SEND_TO_SHOP);

        // Добавляю статус: Выставлен счет на оплату клиенту
        {
            $currency = \common\models\avatar\Currency::findOne($requestClass->currency_id);
            $delivery = DeliveryItem::findOne($requestClass->dostavka_id);
            $requestClass->addStatusToClient([
                'status'  => \common\models\shop\Request::STATUS_ORDER_DOSTAVKA,
                'message' => join("\n", [
                    'Стоимость с учетом доставки: ' . Yii::$app->formatter->asDecimal($requestClass->sum / pow(10, $currency->decimals), $currency->decimals),
                    'Доставка: ' . $delivery->name,
                    'Адрес: ' . $requestClass->address,
                ]),
            ]);
        }

        // Добавляю товары в заказ
        \common\models\shop\RequestProduct::add([
            'request_id' => $requestClass->id,
            'product_id' => $product->id,
            'count'      => 1,
        ]);

        // списываю деньги
        $data = Json::decode($PaySystemConfig->config);
        $address = (int)substr($data['address'], 2);
        $walletDest = Wallet::findOne($address);
        $t = $this->wallet->move($walletDest, $product->price, 'Быстрая оплата за покупку товара ' . $product->name . ' [' . $product->id . ']');

        // закрываю заказ
        $requestClass->successShop();

        return [
            'request'         => $requestClass,
            'billing'         => $billing,
            'PaySystemConfig' => $PaySystemConfig,
            'transaction'     => $t,
        ];
    }

    /**
     * @param \common\models\school\School $school
     * @param Product $product
     *
     * @return \common\models\PaySystemConfig
     */
    private function getPaySystem($school, $product)
    {
        $PaySystemConfig = null;
        if (!Application::isEmpty($product->tree_node_id)) {
            $catalogItem = CatalogItem::findOne($product->tree_node_id);
            if (!Application::isEmpty($catalogItem->pay_config_id)) {
                $PaySystemConfig = PaySystemConfig::findOne($catalogItem->pay_config_id);
            }
        }

        if (is_null($PaySystemConfig)) {
            $PaySystemConfig = PaySystemConfig::findOne(['school_id' => $school->id]);
        }

        if (is_null($PaySystemConfig)) {
            throw new \Exception('не установлена PS');
        }

        return $PaySystemConfig;
    }
}




