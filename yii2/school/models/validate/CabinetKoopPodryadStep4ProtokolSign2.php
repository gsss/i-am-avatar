<?php

namespace school\models\validate;

use avatar\models\forms\school\Article;
use avatar\models\UserEnter;
use BitcoinPHP\BitcoinECDSA\BitcoinECDSA;
use common\components\providers\ETH;
use common\models\avatar\Currency;
use common\models\avatar\UserBill;
use common\models\avatar\UserBillOperation;
use common\models\BlogItem;
use common\models\KoopPodryadCommandUser;
use common\models\PassportLink;
use common\models\RequestTokenCreate;
use common\models\school\AdminLink;
use common\models\school\Kurs;
use common\models\school\Potok;
use common\models\school\PotokUser3Link;
use common\models\school\School;
use common\models\school\SubscribeItem;
use common\models\shop\Request;
use common\models\UserAvatar;
use common\models\UserDigitalSign;
use common\models\UserDocument;
use common\models\UserRoot;
use common\models\UserRootExt;
use common\services\Security;
use cs\Application;
use cs\services\File;
use cs\services\Str;
use cs\services\UploadFolderDispatcher;
use cs\services\Url;
use cs\services\VarDumper;
use Imagine\Image\Box;
use Yii;
use yii\base\Model;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;
use yii\helpers\StringHelper;
use yii\imagine\Image;

/**
 */
class CabinetKoopPodryadStep4ProtokolSign2 extends \iAvatar777\services\FormAjax\Model
{
    public $task_id;
    public $sign;
    public $hash;
    public $address;

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            ['task_id', 'required'],
            ['task_id', 'integer'],

            ['sign', 'required'],
            ['sign', 'string'],
            ['sign', 'validateSign'],

            ['hash', 'required'],
            ['hash', 'string'],

            ['address', 'required'],
            ['address', 'string'],
        ];
    }

    public function validateSign($attribute, $params)
    {
        $bitcoinECDSA = new BitcoinECDSA();

        if (!$bitcoinECDSA->checkSignatureForMessage($this->address, $this->sign, $this->hash)) {
            $this->addError($attribute, 'Не верен адрес или подпись');
            return;
        }
    }

    public function attributeLabels()
    {
        return [
            'sign' => 'Подпись'
        ];
    }

    public function save($runValidation = true, $attributeNames = null)
    {
        /** @var \common\models\KoopPodryadCommandUser $KoopPodryadCommandUser */
        $KoopPodryadCommandUser = KoopPodryadCommandUser::findOne(['podryad_id' => $this->task_id, 'user_id' => Yii::$app->user->id]);
        $KoopPodryadCommandUser->protokol_address = $this->address;
        $KoopPodryadCommandUser->protokol_sign = $this->sign;
        $KoopPodryadCommandUser->protokol_created_at = time();
        $KoopPodryadCommandUser->save();

        return $KoopPodryadCommandUser;
    }

}
