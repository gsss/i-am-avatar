<?php

namespace school\models\validate;


use avatar\models\forms\school\Article;
use avatar\models\UserEnter;
use BitcoinPHP\BitcoinECDSA\BitcoinECDSA;
use common\components\providers\ETH;
use common\models\avatar\Currency;
use common\models\avatar\UserBill;
use common\models\avatar\UserBillOperation;
use common\models\BillingMain;
use common\models\BillingMainClass;
use common\models\BlogItem;
use common\models\PassportLink;
use common\models\PaySystem;
use common\models\PaySystemConfig;
use common\models\RequestTokenCreate;
use common\models\school\AdminLink;
use common\models\school\Kurs;
use common\models\school\Potok;
use common\models\school\PotokUser3Link;
use common\models\school\School;
use common\models\school\SubscribeItem;
use common\models\SchoolKoop;
use common\models\shop\Request;
use common\models\UserAvatar;
use common\models\UserDigitalSign;
use common\models\UserDocument;
use common\models\UserRoot;
use common\models\UserRootExt;
use common\services\Security;
use cs\Application;
use cs\services\File;
use cs\services\Str;
use cs\services\UploadFolderDispatcher;
use cs\services\Url;
use cs\services\VarDumper;
use Imagine\Image\Box;
use Yii;
use yii\base\Model;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;
use yii\helpers\StringHelper;
use yii\imagine\Image;

/**
 */
class KoopPayAddAjax extends \iAvatar777\services\FormAjax\Model
{
    /** @var double */
    public $price;

    /** @var int */
    public $ps_config_id;

    /** @var \common\models\PaySystemConfig */
    public $psconfig;

    public function attributeWidgets()
    {
        return [
            'ps_config_id' => [
                'class'    => '\avatar\widgets\PaySystemList',
                'rows'     => \common\models\PaySystemConfig::find()->where(['school_id' => School::get()->id]),
            ],
        ];
    }

    public function attributeLabels()
    {
        return [
            'price' => 'Сумма',
        ];
    }

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            ['price', 'required'],
            ['price', 'double'],

            ['ps_config_id', 'required'],
            ['ps_config_id', 'integer'],
            ['ps_config_id', 'validatePs'],
        ];
    }


    public function validatePs($attr, $params)
    {
        if (!$this->hasErrors()) {
            $item = PaySystemConfig::findOne($this->ps_config_id);
            if (is_null($item)) {
                $this->addError($attr, 'PaySystemConfig не найдена');
                return;
            }

            $this->psconfig = $item;
        }
    }

    public function save($runValidation = true, $attributeNames = null)
    {


        return [
            'billing' => 1,
        ];
    }
}
