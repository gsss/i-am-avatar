<?php

namespace school\models\validate;


use avatar\models\forms\BlagoProgram;
use avatar\models\forms\school\Article;
use avatar\models\UserEnter;
use BitcoinPHP\BitcoinECDSA\BitcoinECDSA;
use common\components\providers\ETH;
use common\models\avatar\Currency;
use common\models\avatar\CurrencyLink;
use common\models\avatar\UserBill;
use common\models\avatar\UserBillOperation;
use common\models\BillingMain;
use common\models\BillingMainClass;
use common\models\BlogItem;
use common\models\PassportLink;
use common\models\PaySystem;
use common\models\PaySystemConfig;
use common\models\RequestTokenCreate;
use common\models\school\AdminLink;
use common\models\school\Kurs;
use common\models\school\Potok;
use common\models\school\PotokUser3Link;
use common\models\school\School;
use common\models\school\SubscribeItem;
use common\models\SchoolBlagoProgram;
use common\models\SchoolInvestProgram;
use common\models\SchoolKoop;
use common\models\shop\Request;
use common\models\UserAvatar;
use common\models\UserDigitalSign;
use common\models\UserDocument;
use common\models\UserRoot;
use common\models\UserRootExt;
use common\services\Security;
use cs\Application;
use cs\services\File;
use cs\services\Str;
use cs\services\UploadFolderDispatcher;
use cs\services\Url;
use cs\services\VarDumper;
use Imagine\Image\Box;
use Yii;
use yii\base\Model;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;
use yii\helpers\StringHelper;
use yii\imagine\Image;

/**
 */
class InvestProgramIndexAjax extends \iAvatar777\services\FormAjax\Model
{
    /** @var int */
    public $id;

    /** @var double */
    public $price;

    /** @var int */
    public $ps_config_id;

    /** @var \common\models\PaySystemConfig */
    public $psconfig;

    /** @var  \common\models\SchoolBlagoProgram */
    public $program;

    public function attributeWidgets()
    {
        return [
            'ps_config_id' => [
                'class'    => '\avatar\widgets\PaySystemList',
                'rows'     => \common\models\PaySystemConfig::find()->where(['school_id' => School::get()->id]),
            ],
        ];
    }

    public function attributeLabels()
    {
        return [
            'price' => 'Сумма',
        ];
    }

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            ['id', 'required'],
            ['id', 'integer'],
            ['id', 'validateID'],

            ['price', 'required'],
            ['price', 'double'],

            ['ps_config_id', 'required'],
            ['ps_config_id', 'integer'],
            ['ps_config_id', 'validatePs'],
        ];
    }


    public function validateID($attr, $params)
    {
        if (!$this->hasErrors()) {
            $item = SchoolInvestProgram::findOne($this->id);
            if (is_null($item)) {
                $this->addError($attr, 'SchoolBlagoProgram не найдена');
                return;
            }

            $this->program = $item;
        }
    }

    public function validatePs($attr, $params)
    {
        if (!$this->hasErrors()) {
            $item = PaySystemConfig::findOne($this->ps_config_id);
            if (is_null($item)) {
                $this->addError($attr, 'PaySystemConfig не найдена');
                return;
            }

            $this->psconfig = $item;
        }
    }

    public function save($runValidation = true, $attributeNames = null)
    {
        $curr = \common\models\piramida\Currency::findOne(Currency::RUB);
        $link = CurrencyLink::findOne(['currency_int_id' => $curr->id]);
        $currExt = Currency::findOne($link->currency_ext_id);

        $amount = bcmul($this->price, bcpow(10, $curr->decimals));
        $billing = BillingMain::add([
            'class_id'    => BillingMainClass::findOne(['name' => '\common\models\SchoolInvestEvent'])->id,
            'sum_before'  => $amount,
            'sum_after'   => $amount,
            'source_id'   => $this->psconfig->paysystem_id,
            'currency_id' => $currExt->id,
            'config_id'   => $this->ps_config_id,
            'description' => 'Взнос на программу ' . $this->program->name . $this->program->id,
            'successUrl'  => \yii\helpers\Url::to(['invest-program/success', 'id' => $this->program->id], true),
        ]);

        $input = \common\models\SchoolInvestEvent::add([
            'billing_id'  => $billing->id,
            'program_id'  => $this->program->id,
            'amount'      => $amount,
            'currency_id' => $curr->id,
            'user_id'     => Yii::$app->user->id,
        ]);

        return [
            'billing' => $billing,
            'input'   => $input,
        ];
    }
}
