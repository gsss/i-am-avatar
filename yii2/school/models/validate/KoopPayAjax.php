<?php

namespace school\models\validate;

use avatar\models\forms\school\Article;
use avatar\models\UserEnter;
use BitcoinPHP\BitcoinECDSA\BitcoinECDSA;
use common\components\providers\ETH;
use common\models\avatar\Currency;
use common\models\avatar\UserBill;
use common\models\avatar\UserBillOperation;
use common\models\BillingMain;
use common\models\BillingMainClass;
use common\models\BlogItem;
use common\models\PassportLink;
use common\models\PaySystem;
use common\models\PaySystemConfig;
use common\models\RequestTokenCreate;
use common\models\school\AdminLink;
use common\models\school\Kurs;
use common\models\school\Potok;
use common\models\school\PotokUser3Link;
use common\models\school\School;
use common\models\school\SubscribeItem;
use common\models\SchoolKoop;
use common\models\shop\Request;
use common\models\UserAvatar;
use common\models\UserDigitalSign;
use common\models\UserDocument;
use common\models\UserRoot;
use common\models\UserRootExt;
use common\services\Security;
use cs\Application;
use cs\services\File;
use cs\services\Str;
use cs\services\UploadFolderDispatcher;
use cs\services\Url;
use cs\services\VarDumper;
use Imagine\Image\Box;
use Yii;
use yii\base\Model;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;
use yii\helpers\StringHelper;
use yii\imagine\Image;

/**
 */
class KoopPayAjax extends \iAvatar777\services\FormAjax\Model
{
    /** @var int идентификатор UserEnter */
    public $id;

    /** @var int */
    public $ps_config_id;

    /** @var  School */
    public $school;

    /** @var UserEnter */
    public $item;

    /** @var \common\models\PaySystemConfig */
    public $psconfig;

    public function attributeWidgets()
    {
        if (Application::isEmpty($this->id)) {
            $rows = [];
        } else {
            $uEnter = \common\models\UserEnter::findOne($this->id);
            $rows = \common\models\PaySystemConfig::find()->where(['school_id' => $uEnter->school_id]);
        }
        return [
            'ps_config_id' => [
                'class'    => '\avatar\widgets\PaySystemList',
                'rows'     => $rows,
            ],
        ];
    }

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            ['id', 'required'],
            ['id', 'integer'],
            ['id', 'validateUserEnter'],

            ['ps_config_id', 'required'],
            ['ps_config_id', 'integer'],
            ['ps_config_id', 'validatePs'],
        ];
    }

    public function validateUserEnter($attr, $params)
    {
        if (!$this->hasErrors()) {
            $item = UserEnter::findOne($this->id);
            if (is_null($item)) {
                $this->addError($attr, 'UserEnter не найдена');
                return;
            }

            $this->item = $item;
        }
    }

    public function validatePs($attr, $params)
    {
        if (!$this->hasErrors()) {
            $item = PaySystemConfig::findOne($this->ps_config_id);
            if (is_null($item)) {
                $this->addError($attr, 'PaySystemConfig не найдена');
                return;
            }

            $this->psconfig = $item;
        }
    }

    public function save($runValidation = true, $attributeNames = null)
    {
        $s = School::findOne($this->psconfig->school_id);
        $koop = SchoolKoop::findOne(['school_id' => $s->id]);
        if (Application::isEmpty($koop->input_amount)) throw new \Exception('Нужно заполнить значение input_amount');

        // Создаю счет
        $price = $koop->input_amount * 100;

        $BillingMainClass = BillingMainClass::findOne(['name' => '\\avatar\\models\\UserEnter']);

        // Создаю счет на оплату
        $billing = BillingMain::add([
            'sum_before'  => $price,
            'sum_after'   => $price,
            'source_id'   => $this->psconfig->getPaySystem()->id,
            'class_id'    => $BillingMainClass->id,
            'currency_id' => 6,
            'config_id'   => $this->ps_config_id,
            'description' => 'Оплата вступительного взноса по заявке #' . $this->item->id . ' ' .join(' ', [
                $this->item->name_first,
                $this->item->name_middle,
                $this->item->name_last,
            ]),
            'successUrl'  => \yii\helpers\Url::to(['koop/my'], true),
            'action'      => 'koopEnter.' . $this->item->id,
        ]);

        return [
            'billing' => $billing,
        ];
    }
}
