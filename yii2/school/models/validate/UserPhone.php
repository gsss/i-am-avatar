<?php

namespace school\models\validate;

use avatar\models\forms\BlogItem;
use avatar\models\UserRegistration;
use avatar\modules\UniSender\UniSender;
use common\components\providers\BTC;
use common\models\avatar\Currency;
use common\models\avatar\UserBill;
use common\models\avatar\UserBillDefault;
use common\models\avatar\UserBinance;
use common\models\school\File;
use common\models\school\School;
use common\services\Subscribe;
use common\widgets\FileUpload7\FileUpload;
use cs\Application;
use cs\services\Security;
use cs\services\SitePath;
use cs\services\VarDumper;
use iAvatar777\services\FormAjax\Model;
use Imagine\Image\Box;
use yii\base\Exception;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;
use yii\helpers\FileHelper;
use yii\helpers\Url;
use cs\services\UploadFolderDispatcher;
use yii\imagine\Image;
use Imagine\Image\ManipulatorInterface;
use yii\web\IdentityInterface;


/**
 * @property integer id
 * @property integer created_at
 * @property integer status
 * @property integer user_id
 * @property string  email
 * @property string  iof
 * @property string  passport
 * @property string  place
 * @property string  address
 * @property string  phone
 * @property string  whatsapp
 * @property string  passport_scan1
 * @property string  passport_scan2
 */
class UserPhone extends Model
{
    public $_school_id;

    public $phone;



    public function rules()
    {
        return [
            ['passport_scan1', 'required'],
            ['passport_scan1', 'string'],

            ['passport_scan2', 'required'],
            ['passport_scan2', 'string'],

            ['accept_rules',   'integer', ],
            ['accept_rules',   'compare', 'compareValue' => 1, 'operator' => '==', 'message' => 'Нужно ознакомиться с правилами и принять их'],

            ['name_first', 'required'],
            ['name_first',   'string'],

            ['name_last', 'required'],
            ['name_last',   'string'],

            ['name_middle', 'required'],
            ['name_middle',   'string'],

            ['passport_ser', 'required'],
            ['passport_ser',   'string'],

            ['passport_number',   'string'],
            ['passport_vidan_kem',   'string'],
            ['passport_vidan_date',   'string'],
            ['passport_vidan_num',   'string'],
            ['passport_born_date',   'string'],
            ['passport_born_place',   'string'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'address_pochta' => 'Адрес для бумажной корреспонденции',
            'name_first' => 'Имя',
            'name_last' => 'Фамилия',
            'name_middle' => 'Отчество',
            'passport_ser' => 'Серия',
            'passport_number' => 'Номер',
            'passport_vidan_kem' => 'кем выдан',
            'passport_vidan_date' => 'когда выдан',
            'passport_vidan_num' => 'код подразделения',
            'passport_born_date' => 'дата рождения',
            'passport_scan1' => 'Скан паспорта 1',
            'passport_scan2' => 'Скан паспорта 2',
            'passport_born_place' => 'место рождения',
            'accept_rules' => 'Согласие на обработку персональных данных',
        ];
    }



    public function save($runValidation = true, $attributeNames = null)
    {
        $i = new \common\models\UserEnter([
            'passport_scan1'      => $this->passport_scan1,
            'passport_scan2'      => $this->passport_scan2,
            'name_first'          => $this->name_first,
            'name_last'           => $this->name_last,
            'name_middle'         => $this->name_middle,
            'passport_ser'        => $this->passport_ser,
            'passport_number'     => $this->passport_number,
            'passport_vidan_kem'  => $this->passport_vidan_kem,
            'passport_vidan_date' => $this->passport_vidan_date,
            'passport_vidan_num'  => $this->passport_vidan_num,
            'passport_born_date'  => $this->passport_born_date,
            'passport_born_place' => $this->passport_born_place,
        ]);

        $s = School::get();
        $i->school_id = $s->id;
        $i->status = \common\models\UserEnter::STATUS_AFTER_REGISTRATION;
        $i->user_id = \Yii::$app->user->id;
        $i->user2_id = \Yii::$app->user->id;
        $i->created_at = time();
        $i->save();

        return $i;
    }

}
