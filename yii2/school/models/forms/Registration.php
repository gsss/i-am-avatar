<?php

namespace school\models\forms;

use app\models\Piramida\Piramida;
use app\models\Piramida\Wallet;
use app\models\User;
use common\models\school\UserLink;
use common\models\UserAvatar;
use common\models\UserPassword;
use common\models\UserRegistration;
use common\models\UserRoot;
use common\services\Subscribe;
use Yii;
use yii\base\Model;


/**
 * Registration is the model behind the contact form.
 */
class Registration extends Model
{

    public $email;

    /** @var  \common\models\school\School */
    public $school;

    /** @var  UserRoot устанавливается если таковой есть */
    public $userRoot;

    public $password1;

    public $password2;

    public $verificationCode;

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            ['email', 'required', 'message' => Yii::t('c.0uMdJb0e0n', 'Это поле должно быть заполнено обязательно')],
            ['email', 'trim'],
            ['email', 'toLower'],
            ['email', 'email', 'checkDNS' => true, 'message' => Yii::t('c.0uMdJb0e0n', 'Email должен быть с корректным доменным именем')],
            ['email', 'validateEmail'],

            ['password1', 'required', 'message' => Yii::t('c.0uMdJb0e0n', 'Это поле должно быть заполнено обязательно')],

            ['password2', 'required', 'message' => Yii::t('c.0uMdJb0e0n', 'Это поле должно быть заполнено обязательно')],
            ['password2', 'compare', 'compareAttribute' => 'password1', 'operator' => '==', 'message' => Yii::t('c.0uMdJb0e0n', 'Пароли должны совпадать')],

            ['verificationCode', 'captcha'],
        ];
    }

    public function scenarios()
    {
        return [
            'insert' => ['email', 'password1', 'password2', 'verificationCode'],
            'ajax'   => ['email', 'password1', 'password2'],
        ];
    }

    /**
     * @return array customized attribute labels
     */
    public function attributeLabels()
    {
        return [
            'verificationCode' => Yii::t('c.0uMdJb0e0n', 'Проверочный код'),
        ];
    }

    public function validateEmail($attribute, $params)
    {
        if (!$this->hasErrors()) {
            if (UserAvatar::find()->where(['email' => $this->email])->exists()) {
                $this->addError($attribute, 'Такой пользователь уже есть, выберите другой или авторизуйтесь');
            }
        }
    }

    public function toLower($attribute, $params)
    {
        if (!$this->hasErrors()) {
           $this->email = strtolower($this->email);
        }
    }

    /**
     */
    public function register()
    {
        // добавляю в школу
        $userRoot = UserRoot::findOne(['email' => $this->email]);
        if (is_null($userRoot)) {
            $userRoot = UserRoot::add(['email' => $this->email]);
        }
        if (!UserLink::find()->where(['user_root_id' => $userRoot->id, 'school_id' => $this->school->id])->exists()) {
            $link = UserLink::add(['user_root_id' => $userRoot->id, 'school_id' => $this->school->id]);
        }

        \avatar\models\UserRegistration::deleteAll(['parent_id' => $userRoot->id]);
        $userRegistration = \avatar\models\UserRegistration::add($userRoot->id);

        UserPassword::deleteAll(['user_root_id' => $userRoot->id]);
        $userPassword = UserPassword::add([
            'password_hash' => UserAvatar::hashPassword($this->password1),
            'date_finish'   => $userRegistration->date_finish,
            'user_root_id'  => $userRoot->id,
        ]);

        Subscribe::sendArraySchool($this->school, [$this->email], 'Подтверждение регистрации', 'registration', [
            'url'      => $this->school->getUrl([
                'auth/registration-activate',
                'code' => $userRegistration->code
            ]),
            'user'     => $userRoot,
            'datetime' => \Yii::$app->formatter->asDatetime($userRegistration->date_finish)
        ], 'layouts/html');
    }
}
