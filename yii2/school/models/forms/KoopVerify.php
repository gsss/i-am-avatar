<?php

namespace school\models\forms;

use avatar\models\UserEnter;
use BitcoinPHP\BitcoinECDSA\BitcoinECDSA;
use common\models\SendLetter;
use common\models\UserDigitalSign;
use common\models\UserSeed;
use common\services\Security;
use cs\Application;
use cs\services\Str;
use Yii;
use yii\base\Model;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;
use yii\helpers\VarDumper;
use yii\web\UploadedFile;

/**
 */
class KoopVerify extends Model
{
    public $id;

    /** @var  string */
    public $file;

    /** @var int */
    public $type;

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            ['id', 'required'],
            ['id', 'integer'],

            ['file', 'required'],
            ['file', 'string'],

            ['type', 'required'],
            ['type', 'integer'],
        ];
    }

    /**
     * @return \avatar\models\UserEnter
     * @throws \Exception
     */
    public function action()
    {
        $UserEnter = UserEnter::findOne($this->id);

        $UserEnter->file = Json::encode([
            'type' => $this->type,
            'data' => $this->file,
        ]);

        $UserEnter->save();

        return $UserEnter;
    }

}
