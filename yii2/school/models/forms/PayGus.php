<?php

namespace school\models\forms;

use common\models\avatar\Currency;
use common\models\avatar\UserBill;
use common\models\BillingMain;
use common\models\PaySystemConfig;
use common\models\piramida\Wallet;
use common\models\shop\Request;
use school\modules\sroutes\models\Deal;
use school\modules\sroutes\models\DealStatus;
use school\modules\sroutes\models\Offer;
use cs\base\FormActiveRecord;
use cs\services\VarDumper;
use Yii;
use yii\base\Model;
use yii\base\NotSupportedException;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\helpers\Json;
use yii\web\IdentityInterface;

/**
 *
 */
class PayGus extends Model
{
    public $school;
    public $billing_id;
    public $wallet_id;

    /** @var \common\models\BillingMain */
    private $_billing;

    /** @var \common\models\Piramida\Transaction */
    private $_transaction;

    public function rules()
    {
        return [
            ['billing_id', 'required'],
            ['billing_id', 'integer'],

            ['wallet_id', 'required'],
            ['wallet_id', 'integer'],
            ['wallet_id', 'tryToSend'],
        ];
    }

    public function tryToSend($a, $p)
    {
        if (!$this->hasErrors()) {
            $walletSource = Wallet::findOne($this->wallet_id);

            $billing = BillingMain::findOne($this->billing_id);
            $config_id  = $billing->config_id;
            $config = PaySystemConfig::findOne($config_id);
            $data = Json::decode($config->config);
            $walletAddress = $data['address'];
            $wallet_id = (int) substr($walletAddress, 2);
            $walletDest = Wallet::findOne($wallet_id);
            $request = Request::findOne(['billing_id' => $billing->id]);

            try {
                $t = $walletSource->move($walletDest, $billing->sum_after, 'Оплата счета #' . $billing->id.' для заказа #'. $request->id);
            } catch (\Exception $e) {
                $this->addError($a, $e->getMessage());
                return;
            }

            $this->_billing = $billing;
            $this->_transaction = $t;
        }
    }

    /**
     * @return \common\models\Piramida\Transaction
     */
    public function action()
    {
        $this->_billing->successClient();
        $this->_billing->successShop();

        return $this->_transaction;
    }

}
