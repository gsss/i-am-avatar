<?php

namespace school\models\forms;

use common\models\school\School;
use common\models\school\UserLink;
use common\models\SendLetter;
use common\models\UserAvatar;
use common\models\UserRegisterCode;
use common\models\UserRoot;
use common\models\UserSeed;
use common\services\Security;
use cs\Application;
use cs\services\Str;
use Yii;
use yii\base\Model;
use yii\helpers\ArrayHelper;
use yii\helpers\VarDumper;
use yii\web\UploadedFile;
use \avatar\models\UserRegistration;
use \common\models\UserPassword;
use \common\services\Subscribe;
use \yii\helpers\Url;


/**
 */
class ShopRegistrationCode extends \iAvatar777\services\FormAjax\Model
{
    public $code;

    /** @var  array
     * [
     *  'code'
     *  'UserRegisterCodeId'
     * ]
     */
    public $data;

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            ['code', 'required'],
            ['code', 'string'],
            ['code', 'validateCode'],
        ];
    }

    /**
     */
    public function validateCode($attribute, $params)
    {
        if (!$this->hasErrors()) {
            $codeData = Yii::$app->session->get('\school\models\forms\ShopRegistration');
            if (is_null($codeData)) {
                $this->addError($attribute, 'Код не найден');
                return;
            }
            if ($codeData['code'] != $this->code) {
                $this->addError($attribute, 'Код не верный');
                return;
            }
            $this->data = $codeData;
        }
    }

    public function save($runValidation = true, $attributeNames = null)
    {
        $UserRegisterCode = UserRegisterCode::findOne($this->data['UserRegisterCodeId']);
        $userRoot = UserRoot::findOne($UserRegisterCode->parent_id);

        // Создаю пользователя
        $user = \common\models\UserAvatar::registrationCreateUser($userRoot->email, $UserRegisterCode->password_hash, [
            'user_root_id'     => $UserRegisterCode->parent_id,
            'email_is_confirm' => 0,
        ]);
        $user->password = $UserRegisterCode->password_hash;
        $user->save();

        // Логиню пользователя
        Yii::$app->user->login($user);

        return 1;
    }
}
