<?php

namespace school\models\forms;

use app\models\Piramida\Piramida;
use app\models\Piramida\Wallet;
use app\models\User;
use common\models\school\UserLink;
use common\models\UserAvatar;
use common\models\UserPassword;
use common\models\UserRegistration;
use common\models\UserRoot;
use common\services\Subscribe;
use Yii;
use yii\base\Model;


/**
 */
class RoomLogin extends Model
{

    public $name;


    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            ['name', 'required'],
        ];
    }

    /**
     */
    public function action()
    {

    }
}
