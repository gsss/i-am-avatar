<?php

namespace school\models\forms;

use common\models\school\School;
use common\models\school\UserLink;
use common\models\SendLetter;
use common\models\UserAvatar;
use common\models\UserRoot;
use common\models\UserSeed;
use common\services\Security;
use cs\Application;
use cs\services\Str;
use Yii;
use yii\base\Model;
use yii\helpers\ArrayHelper;
use yii\helpers\VarDumper;
use yii\web\UploadedFile;
use \avatar\models\UserRegistration;
use \common\models\UserPassword;
use \common\services\Subscribe;
use \yii\helpers\Url;


/**
 */
class ShopRegistration extends \iAvatar777\services\FormAjax\Model
{
    public $login;
    public $password;

    /** @var  int */
    public $school_id;

    public function init()
    {
        $this->school_id = School::get()->id;
    }

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            ['school_id', 'required'],
            ['school_id', 'integer'],

            ['login', 'required'],
            ['login', 'string'],
            ['login', 'trim'],
            ['login', 'toLower'],
            ['login', 'email'],
            ['login', 'validateUser'],
            ['login', 'validateUserRoot'],

            ['password', 'required'],
            ['password', 'string'],

        ];
    }

    /**
     */
    public function validateUser($attribute, $params)
    {
        if (!$this->hasErrors()) {
            try {
                $user = \common\models\UserAvatar::findOne(['email' => $this->login]);
                $this->addError($attribute, 'Такой пользователь уже есть, придумайте другой или если это ваш логин, то войдите');
                return;
            } catch (\Exception $e) {

            }
        }
    }

    /**
     */
    public function toLower($attribute, $params)
    {
        if (!$this->hasErrors()) {
            $this->login = strtolower($this->login);
        }
    }

    /**
     */
    public function validateUserRoot($attribute, $params)
    {
        if (!$this->hasErrors()) {
            $userRoot = UserRoot::findOne(['email' => $this->login]);
            if (is_null($userRoot)) {
                $userRoot = UserRoot::add(['email' => $this->login]);
                $link = UserLink::add(['user_root_id' => $userRoot->id, 'school_id' => $this->school_id]);
            } else {
                $link = UserLink::findOne(['user_root_id' => $userRoot->id, 'school_id' => $this->school_id]);
                if (is_null($link)) {
                    $link = UserLink::add(['user_root_id' => $userRoot->id, 'school_id' => $this->school_id]);
                }
            }
        }
    }

    public function save($runValidation = true, $attributeNames = null)
    {
        $email = $this->login;

        $userRoot = UserRoot::findOne(['email' => $email]);
        if (is_null($userRoot)) {
            $userRoot = UserRoot::add(['email' => $email]);
        }

        \common\models\UserRegisterCode::deleteAll(['parent_id' => $userRoot->id]);
        $code = Security::generateRandomString(6, Security::ALGORITM_NUMS);

        $userRegistration = \common\models\UserRegisterCode::add([
            'parent_id'     => $userRoot->id,
            'code'          => $code,
            'password_hash' => UserAvatar::hashPassword($this->password),
            'created_at'    => time(),
            'date_finish'   => time() + 60 * 60 * 24,
        ]);
        Yii::$app->session->set('\school\models\forms\ShopRegistration', [
            'code' => $code,
            'UserRegisterCodeId' => $userRegistration->id,
        ]);

        Subscribe::sendArray([$email], 'Подтверждение регистрации', 'registration_code', [
            'code'     => $code,
            'user'     => $userRoot,
        ]);

        return 1;
    }
}
