<?php

namespace school\services;

use common\models\school\Page;
use common\models\school\School;
use cs\services\VarDumper;
use yii\helpers\Inflector;
use yii\web\Request;
use yii\web\UrlManager;
use yii\web\UrlRule;
use yii\web\UrlRuleInterface;
use yii\base\BaseObject;

class CompanyUrlRule2 extends BaseObject implements UrlRuleInterface
{
    public $url;

    /**
     * Creates a URL according to the given route and parameters.
     * @param UrlManager $manager the URL manager
     * @param string $route the route. It should not have slashes at the beginning or the end.
     * @param array $params the parameters
     * @return string|bool the created URL, or false if this rule cannot be used for creating this URL.
     */
    public function createUrl($manager, $route, $params)
    {
        if (count($params) > 0) {
            return $route . '?' . http_build_query($params);
        } else {
            return $route;
        }
    }

    /**
     * Parses the given request and returns the corresponding route and parameters.
     * @param UrlManager $manager the URL manager
     * @param Request $request the request component
     * @return array|bool the parsing result. The route and the parameters are returned as an array.
     * If false, it means this rule cannot be used to parse this path info.
     */
    public function parseRequest($manager, $request)
    {

        $pathInfo = $request->getPathInfo();

        if (substr($pathInfo, 0, strlen($this->url .'/')) == $this->url .'/') {
            return [$this->url, ['uid' => substr($pathInfo, strlen($this->url .'/'))]];
        }

        return false;  // данное правило не применимо
    }
}