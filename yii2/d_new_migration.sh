# Создаю миграцию
docker exec -it i-am-avatar-php-fpm php yii migrate/create $1 --interactive=0

# Получить имя файла $path
Path2=$(pwd)
Path=$(docker exec -it i-am-avatar-php-fpm php yii migrations/last "${Path2}" --interactive=0)
echo ${Path}

# Меняю владельца
chown ramha:ramha ${Path2}/console/migrations/${Path}