<?php
/**
 * Created by PhpStorm.
 * User: Ra-m-ha
 * Date: 02.08.2019
 * Time: 3:01
 */

namespace apiprocessing\models\validate;

use common\models\avatar\UserBill;
use common\models\piramida\Currency;
use common\models\piramida\Wallet;
use common\models\school\School;
use common\models\UserAccessToken;
use common\models\UserAvatar;
use common\services\Security\AES;
use cs\services\Security;
use cs\services\VarDumper;

class WalletControllerNew extends \yii\base\Model
{
    public $password;
    public $currency_id;

    public $currency;

    public function rules()
    {
        return [
            ['currency_id', 'required'],
            ['currency_id', 'integer'],
            ['currency_id', 'validateCurrency'],

            ['password', 'required'],
            ['password', 'string', 'min' => 6],
        ];
    }

    public function validateCurrency($attr, $params)
    {
        if (!$this->hasErrors()) {
            $c = Currency::findOne($this->currency_id);
            if (is_null($c)) {
                $this->addError($attr, 'Не найдена валюта');
                return;
            }
            $this->currency = $c;
        }
    }

    public function action()
    {
        $masterKey = Security::generateRandomString(100);
        $wallet = Wallet::addNew([
            'currency_id' => $this->currency_id,
            'password'    => password_hash($this->password, PASSWORD_BCRYPT),
            'master_key'  => AES::encrypt256CBC($masterKey, UserBill::passwordToKey32(\Yii::$app->params['unlockKey'])),
        ]);

        return [
            'id'          => $wallet->id,
            'address'     => $wallet->getAddress(),
            'currency_id' => $this->currency_id,
            'master_key'  => $masterKey,
        ];
    }
}