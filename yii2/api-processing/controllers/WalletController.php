<?php

namespace apiprocessing\controllers;

use app\models\Article;
use app\models\SiteUpdate;
use app\services\Subscribe;
use avatar\models\forms\Contact;
use avatar\services\LogReader;
use common\components\providers\ETH;
use common\components\sms\IqSms;
use common\models\avatar\Currency;
use common\models\avatar\UserBill;
use common\models\CompanyCustomizeItem;
use common\models\PaymentBitCoin;
use common\models\piramida\Login;
use common\models\piramida\Wallet;
use common\models\UserAvatar;
use common\models\UserRegistration;
use common\payment\BitCoinBlockTrailPayment;
use cs\Application;
use cs\base\BaseController;
use cs\services\Security;
use cs\services\UploadFolderDispatcher;
use cs\services\VarDumper;
use cs\web\Exception;
use Yii;
use yii\base\UserException;
use yii\db\Connection;
use yii\db\Query;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use yii\helpers\FileHelper;
use yii\helpers\Json;
use yii\helpers\Url;
use yii\web\BadRequestHttpException;
use yii\web\ForbiddenHttpException;
use yii\web\HttpException;
use yii\web\Response;

class WalletController extends \avatar\base\BaseController
{
    public $enableCsrfValidation = false;

    // https://learn.javascript.ru/xhr-crossdomain
    public function behaviors()
    {
        return [
            'corsFilter' => [
                'class' => \yii\filters\Cors::className(),
                'cors'  => [
                    'Origin'                           => ['*'],
                    'Access-Control-Allow-Origin'      => ['*'],
                    'Access-Control-Request-Method'    => ['POST', 'OPTIONS'],
                    'Access-Control-Request-Headers'   => ['*'],
                    'Access-Control-Allow-Credentials' => true,
                    'Access-Control-Max-Age'           => 86400,
                    'Access-Control-Expose-Headers'    => [],
                ],
            ],
//            'access' => [
//                'class' => '\yii\filters\AccessControl',
//                'rules' => [
//                    [
//                        'allow'   => true,
//                        'roles'   => ['role_admin_command'],
//                    ],
//                ],
//            ],
        ];
    }

    public function init()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        if (!Yii::$app->request->headers->has('X-API-KEY')) throw new ForbiddenHttpException('Нет параметра X-API-KEY', 1);
        if (!Yii::$app->request->headers->has('X-API-SECRET')) throw new ForbiddenHttpException('Нет параметра X-API-SECRET', 2);
        $key = $headers = Yii::$app->request->headers->get('X-API-KEY');
        $secret = $headers = Yii::$app->request->headers->get('X-API-SECRET');
        $login = Login::findOne(['login' => $key]);
        if (is_null($login)) throw new ForbiddenHttpException('Не найден логин', 3);
        $password = $login->secret;
        $hash = hash('sha256', ($password . ((int)(time()/100))));

        if ($hash != $secret) throw new ForbiddenHttpException('Не верный secret', 4);
    }

    /**
     * PARAMS
     * - password
     * - currency_id
     *
     */
    public function actionNew()
    {
        $model = new \apiprocessing\models\validate\WalletControllerNew();

        if (!$model->load(ArrayHelper::merge(Yii::$app->request->post(), Yii::$app->request->get()), '')) {
            throw new BadRequestHttpException('Не загружены данные', 4);
        }
        if (!$model->validate()) {
            throw new HttpException(422, Json::encode($model->errors));
        }

        return $model->action();
    }
    /**
     */
    public function actionInfo()
    {
        return [
            'wallet' => Wallet::findOne(Yii::$app->request->get('id'))
        ];
    }
}
