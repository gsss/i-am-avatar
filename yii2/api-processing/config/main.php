<?php

$params = \yii\helpers\ArrayHelper::merge(
    require(__DIR__ . '/../../common/config/params.php'),
    require(__DIR__ . '/../../common/config/params-local.php'),
    require(__DIR__ . '/params.php'),
    require(__DIR__ . '/params-local.php')
);

$config = [
    'id'                  => 'avatar-api-processing',
    'basePath'            => dirname(__DIR__),
    'bootstrap'           => ['log'],
    'controllerNamespace' => 'apiprocessing\controllers',
    'vendorPath'          => dirname(dirname(__DIR__)) . '/vendor',
    'language'            => 'ru',
    'sourceLanguage'      => 'ru',
    'timeZone'            => 'Etc/GMT-3',
    'aliases'             => [
        '@csRoot' => __DIR__ . '/../../common/app',
        '@cs'     => __DIR__ . '/../../common/app',
    ],
    'components'          => [
        'session'         => [
            'timeout' => 600,
            'class'   => 'yii\web\CacheSession',
            'cache'   => 'cacheSession',
        ],
        'cacheSession'    => [
            'class'        => 'yii\caching\MemCache',
            'useMemcached' => true,
            'keyPrefix'    => 'session',
            'servers'      => [
                [
                    'host' => 'localhost',
                    'port' => 11211,
                ],
            ],
        ],
        'urlManager'      => [
            'rules' => [
                '/'                               => 'site/index',
                '<controller>/<action>'           => '<controller>/<action>',
                '<controller>/<action>/<id:\\w+>' => '<controller>/<action>',
            ],

        ],
        'languageManager' => [
            'class'    => 'common\components\LanguageManager',
            'strategy' => [
                'ru' => ['ru', 'be', 'uk'],
                'en' => ['en', 'fr', 'de'],
            ],
        ],
        'response' => [
            'class' => 'yii\web\Response',
            'on beforeSend' => function ($event) {
                $response = $event->sender;
                if ($response->format == \yii\web\Response::FORMAT_JSON) {
                    $response->data = [
                        'success' => $response->isSuccessful,
                        'data' => $response->data,
                    ];
                }
            },
        ],
        'authManager'     => [
            'class' => 'yii\rbac\PhpManager',
        ],
        'assetManager'    => [
            'appendTimestamp' => true,
        ],
        'piramida'        => [
            'class' => 'app\common\components\Piramida',
        ],
        'request'         => [
            // !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
            'cookieValidationKey'    => '',
            'enableCookieValidation' => false,
        ],
        'deviceDetect'    => [
            'class'     => 'avatar\services\DeviceDetect',
            'setParams' => 'false',
        ],
        'user'            => [
            'identityClass'   => 'common\models\UserAvatar',
            'authTimeout'     => 60 * 60 * 24 * 30,
            'enableAutoLogin' => true,
            'loginUrl'        => ['auth/login'],
        ],
        'errorHandler'    => [
            'errorAction' => 'site/error',
            'errorView'   => '@avatar/views/site/error.php',
        ],
        'formatter'       => [
            'dateFormat'        => 'dd.MM.yyyy',
            'timeFormat'        => 'php:H:i:s',
            'datetimeFormat'    => 'php:d.m.Y H:i',
            'decimalSeparator'  => '.',
            'thousandSeparator' => ',',
            'currencyCode'      => 'RUB',
            'locale'            => 'ru',
            'nullDisplay'       => '',
        ],
        'log'              => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets'    => [
                [
                    'class'       => 'common\services\DbTarget',
                    'levels'      => [
                        'warning',
                        'error',
                    ],
                    'except'      => [
                        'yii\web\HttpException:404',
                        'yii\web\HttpException:403',
                        'cs\web\Exception',
                    ],

                    'db'         => 'dbStatistic',
                    'logTable'   => 'log2_api',
                ],
                [
                    'class'      => 'common\services\DbTarget',
                    'categories' => ['avatar\\*'],
                    'levels'     => [
                        'info',
                        'trace',
                    ],

                    'db'         => 'dbStatistic',
                    'logTable'   => 'log2_api',
                ],
            ],
        ],
    ],
    'modules'             => [
    ],
    'params'              => $params,
];

return $config;
