# Установить docker
apt install curl
curl -fsSL https://get.docker.com -o get-docker.sh
sh get-docker.sh

# Установить docker-compose
curl -L "https://github.com/docker/compose/releases/download/1.25.5/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
chmod +x /usr/local/bin/docker-compose
ln -s /usr/local/bin/docker-compose /usr/bin/docker-compose
usermod -a -G docker $USER
systemctl enable docker # Auto-start on boot
systemctl start docker # Start right now
chown $USER:docker /var/run/docker.sock

# Создать файл настроек окружения
cp .env.test .env

# Собрать образы
docker-compose build --build-arg GITHUB_TOKEN=9caf02a919d5411bde83c84bfde200a9e002c231

# Запустить докер
docker-compose up -d

# Права устанавливаю
chmod 777 yii2/avatar-bank/runtime
chmod 777 yii2/public_html/assets
chmod 777 yii2/public_html/upload

chmod 777 yii2/school/web/assets
chmod 777 yii2/school/runtime

# Создаю стартовые БД
# docker exec -it i-am-avatar-php-fpm1 php db/install

# Скачиваю пустую БД
sudo curl -k -o i_am_avatar_prod_main.sql.gz    -d "file=i_am_avatar_prod_main&key=$1" -X POST  https://platform.qualitylive.su/db/get?file
sudo curl -k -o i_am_avatar_prod_stat.sql.gz    -d "file=i_am_avatar_prod_stat&key=$1" -X POST  https://platform.qualitylive.su/db/get?file
sudo curl -k -o i_am_avatar_prod_wallet.sql.gz  -d "file=i_am_avatar_prod_wallet&key=$1" -X POST  https://platform.qualitylive.su/db/get?file

# Распаковываю БД
gunzip -d i_am_avatar_prod_main.sql.gz
gunzip -d i_am_avatar_prod_stat.sql.gz
gunzip -d i_am_avatar_prod_wallet.sql.gz


# Залить базу данных (`password` - это пароль локальный)
docker exec -it i-am-avatar-mysql ./load_db.sh password

# Заливаю первые данные
# docker exec -it i-am-avatar-php-fpm1 php db/set-start-data

# обновить composer
docker exec -it i-am-avatar-php-fpm1 composer update

# Настроить крон