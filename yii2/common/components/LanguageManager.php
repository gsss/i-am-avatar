<?php
/**
 * Created by PhpStorm.
 * User: s.arhangelskiy
 * Date: 14.04.2016
 * Time: 11:12
 */

namespace common\components;


use common\services\Debugger;
use cs\services\VarDumper;
use Yii;
use yii\base\Component;
use yii\base\Exception;
use yii\helpers\ArrayHelper;

/**
 * Сервис помогающий пользоваться языками.
 * Имеет одну ункцию: определяет лучший язык который подходит пользователю в зависимости от его предпочтений
 *
 * Class LanguageService
 * @package common\components
 */
class LanguageManager extends Component
{
    /**
     * @var array стратегия перевода языка пользователя в язык интерфейса
     */
    public $strategy;

    public $default;

    /**
     * Возвращает язык пользователя в зависимости от настроек пользователя
     * Производит поиск по стратегии (\Yii::$app->params['language']['strategy']), и если не находит сопоставления то возвращает `$default`
     * Если `$default` не задан то используется \Yii::$app->language
     * 
     * @param string $default 
     * 
     * @return string "Код Языка" в стандарте ISO-639-1
     */
    public function getBestLanguage($default = null)
    {
        /**
         * @var array $user предпочтения пользователя
         * например [
         *    'ru', 'en'
         * ]
         */
        $user = self::getUserPreference();
        $strategy = $this->strategy;
        foreach ($user as $userLanguage) {
            foreach($strategy as $key => $userLanguages) {
                if (in_array($userLanguage, $userLanguages)) {
                    return $key;
                }
            }
        }
        
        return $default;
    }

    /**
     * Выдает предпочтения пользователя основываясь на значении $_SERVER['HTTP_ACCEPT_LANGUAGE']
     *
     * Возвращает [
     *     [ru-ru] => 1
     *     [ru] => 0.8
     *     [en-us] => 0.6
     *     [en] => 0.4
     * ]
     */
    public static function _getUserPreference()
    {
        $language = [];
        if (!isset($_SERVER['HTTP_ACCEPT_LANGUAGE'])) {
            return [];
        }
        if (($list = strtolower($_SERVER['HTTP_ACCEPT_LANGUAGE']))) {
            if (preg_match_all('/([a-z]{1,8}(?:-[a-z]{1,8})?)(?:;q=([0-9.]+))?/', $list, $list)) {
                $language = array_combine($list[1], $list[2]);
                foreach ($language as $n => $v)
                    $language[$n] = $v ? $v : 1;
                arsort($language, SORT_NUMERIC);
            }
        }

        return $language;
    }

    /**
     * Выдает предпочтения пользователя основываясь на значении $_SERVER['HTTP_ACCEPT_LANGUAGE']
     * Вызывает _getUserPreference
     * выбирает все индексы
     * обрезает все что после и опускает регистр символов
     * группирует и возвращает в очередности предпочтения языка, где первыми являются более предпочтительными
     *
     * Возвращает [
     *    'ru', 'en'
     * ]
     */
    public static function getUserPreference()
    {
        $language = self::_getUserPreference();
        if (count($language) == 0) return [];
        $new = [];
        foreach (array_keys($language) as $key) {
            if (strlen($key) > 2) {
                $key = substr($key,0, 2);
            }
            $new[] = strtolower($key);
        }
        $new = array_unique($new);

        return $new;
    }

    public static function isDefault()
    {
        return \Yii::$app->language == \Yii::$app->sourceLanguage;
    }

    /**
     * Готовый обработчкик события \yii\base\Application::EVENT_BEFORE_REQUEST
     *
     *
     */
    public function onBeforeRequest()
    {
        $language = null;
        $cookieLanguage = Yii::$app->request->cookies->get('language', null);
        if (is_null($cookieLanguage)) {
            $language = $this->getBestLanguage($this->default);
        } else {
            $language = $cookieLanguage->value;
        }
        Yii::$app->language = $language;
    }

}