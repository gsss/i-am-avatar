<?php
/**
 * Created by PhpStorm.
 * User: god
 * Date: 13.02.2017
 * Time: 12:06
 */

namespace common\components;


use common\models\avatar\UserBill;
use cs\services\Str;
use yii\base\BaseObject;

class Card extends BaseObject
{
    /**
     * Генерирует номер карты, без пробелов с бином, валютой и контрольной цифрой от 16 до 20 символов(цифр)
     *
     * @param string $bin
     * @param string $currency  Валюта
     *
     * @return string
     */
    public static function generateNumber($bin = '8888', $currency = '01')
    {
        do {
            $number = $bin . $currency . substr(str_shuffle('012345678901234567890123456789012345678901234567890123456789'), 0, 9);
            $array = Str::getChars($number);
            $sum = 0;
            for ($i = 0; $i < count($array); $i++) {
                if ($i % 2 == 0) {
                    $value = $array[$i];
                    $value = $value * 2;
                    if ($value >= 10) {
                        $value = $value - 9;
                    }
                    $sum += $value;
                } else {
                    $value = $array[$i];
                    $sum += $value;
                }
            }
            $ostatok = $sum % 10;
            if ($ostatok == 0) {
                $last = 0;
            } else {
                $last = 10 - $ostatok;
            }

            $number = $number . $last;
        } while (\common\models\Card::find()->where(['number' => $number])->exists());

        return $number;
    }


    /**
     * Проверяет контрольную сумму номера карты
     *
     * @param string $number как входное значение должно прийти уже валидированное значение, не имеющее символов и пробелов и длиной 16 символов
     *
     * @return bool
     */
    public static function checkSum($number)
    {
        $array = Str::getChars($number);
        $lastChar = $array[15];
        unset($array[15]);
        $sum = 0;
        for ($i = 0; $i < count($array); $i++) {
            if ($i % 2 == 0) {
                $value = $array[$i];
                $value = $value * 2;
                if ($value >= 10) {
                    $value = $value - 9;
                }
                $sum += $value;
            } else {
                $value = $array[$i];
                $sum += $value;
            }
        }
        $ostatok = $sum % 10;
        if ($ostatok == 0) {
            $last = 0;
        } else {
            $last = 10 - $ostatok;
        }

        return $lastChar == $last;
    }
}