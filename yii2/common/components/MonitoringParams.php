<?php

namespace common\components;

use Yii;
use yii\helpers\StringHelper;

/**
 * Компонент подсчитывающий счетчики
 *
 * Created by PhpStorm.
 * User: s.arhangelskiy
 * Date: 01.06.2016
 * Time: 14:41
 */
class MonitoringParams extends \yii\base\Component
{
    /**
     * @var string под этим ключом хранится массив значений счетчиков
     * [
     *     'time'               => time() - время начало отсчета счетчика в сек
     *     'request_per_second' => <value>,
     *     // ...
     * ]
     */
    public $cacheKey = 'monitoringParams';

    /**
     * @var array функции для подсчета
     * [
     *      'mail' => <value>,
     * ]
     */
    public $counters;

    /**
     * [
     *      'mail',
     *      'ethServer',
     *      'etcServer',
     * ]
     */
    public function init()
    {
        $this->counters = [
            'mail',
        ];
    }

    public function inc($name)
    {
        $data = $this->get();
        $data[$name]++;
        $this->set($data);
    }

    /**
     * Выдает массив расчитанных счетчиков
     * производится расчет <Значение счетчика> / time() - <момент начала отсчета счетчика>
     *
     * @return array
     * [
     *     'request_per_second' => <value>,
     *     // ...
     * ]
     */
    public function calculate()
    {
        $data = $this->get();
        $report = [];
        $delta = time() - $data['time'];
        foreach ($this->counters as $name) {
            $report[$name] = ($data[$name] / $delta);
        }

        return $report;
    }

    public function reset()
    {
        $new = [
            'time'    => time(),
        ];
        foreach ($this->counters as $name) {
            $new[$name] = 0;
        }
        $this->set($new);
    }

    /**
     * Сохраняет данные в кеш
     *
     * @param $users
     */
    protected function set($users)
    {
        Yii::$app->cache->set($this->cacheKey, $users);
    }

    /**
     * Выдает пользователей онлайн в виде массива
     *
     * @return array
     */
    public function get()
    {
        $value = Yii::$app->cache->get($this->cacheKey);
        if ($value === false) {
            $value = [
                'time'    => time(),
            ];
            foreach ($this->counters as $name) {
                $value[$name] = 0;
            }
        };

        return $value;
    }
}