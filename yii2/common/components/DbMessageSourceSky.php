<?php
/**
 * Created by PhpStorm.
 * User: s.arhangelskiy
 * Date: 12.10.2016
 * Time: 15:52
 */

namespace common\components;

use common\models\Config;
use common\models\language\SourceMessage;
use common\services\Debugger;
use yii\helpers\ArrayHelper;
use yii\helpers\VarDumper;
use yii\i18n\DbMessageSource;
use yii\i18n\MissingTranslationEvent;

/**
 * Класс для разрешения ситуации когда не найден перевод для строки в указанном языке
 *
 * Class TranslationEventHandler
 * @package common\components
 */
class DbMessageSourceSky extends DbMessageSource
{
    /**
     * Если не найден перевод на нужный язык, то выдается ключ для перевода
     */
    const STRATEGY_TRANSLATE_KEY = 1;

    /**
     * Если не найден перевод на нужный язык, то ищется перевод на русский
     * Если перевода на русский нет то возвращается `'*** ОШИБКА (' . $category . '/' . $message . ') ***'`
     * $message - ключ для перевода
     */
    const STRATEGY_TRANSLATE_RUS = 2;

    /**
     * Если не найден перевод на нужный язык, то пишется ошибка `'*** ОШИБКА (' . $category . '/' . $message . ') ***'`
     * $message - ключ для перевода
     * $category - категория для перевода
     */
    const STRATEGY_TRANSLATE_ERROR = 3;

    /**
     * Если не найден перевод на нужный язык, то пишется `'+++'`
     */
    const STRATEGY_TRANSLATE_ERROR_PLUS = 4;

    /**
     * Если не найден перевод на нужный язык, то ищется перевод на русский
     * Если перевода на русский нет то возвращается ключ для перевода
     * $message - ключ для перевода
     */
    const STRATEGY_TRANSLATE_RUS_KEY = 5;

    /**
     * Если не найден перевод на нужный язык, то ищется перевод на английский
     * Если перевода на английский нет то возвращается ключ для перевода
     * $message - ключ для перевода
     */
    const STRATEGY_TRANSLATE_ENG_KEY = 6;

    /**
     * Название ключа по которому можно найти переменную стратегию обработки ненайденных строк для перевода в конфиге
     * \common\models\Config
     */
    const CONFIG_KEY = 'translation.missingEvent.strategy';

    /**
     * Обрабатывает событие `\yii\i18n\MessageSource::EVENT_MISSING_TRANSLATION` (Переменная для перевода не найдена)
     *
     * @param MissingTranslationEvent $event
     */
    public static function handleMissingTranslation(MissingTranslationEvent $event)
    {
        if ($event->language == 'zz') {
            $event->translatedMessage = self::getError($event);
        } else {
            $strategy = Config::get('translation.missingEvent.strategy', self::STRATEGY_TRANSLATE_RUS);

            switch ($strategy) {
                case self::STRATEGY_TRANSLATE_KEY:
                    $event->translatedMessage = $event->message;
                    break;

                case self::STRATEGY_TRANSLATE_RUS:
                    $event->translatedMessage = self::getRussian($event);
                    break;

                case self::STRATEGY_TRANSLATE_ERROR:
                    $event->translatedMessage = self::getError($event);
                    break;

                case self::STRATEGY_TRANSLATE_ERROR_PLUS:
                    $event->translatedMessage = self::getErrorPlus($event);
                    break;

                case self::STRATEGY_TRANSLATE_RUS_KEY:
                    $event->translatedMessage = self::getRusKey($event);
                    break;

                case self::STRATEGY_TRANSLATE_ENG_KEY:
                    $event->translatedMessage = self::getEngKey($event);
                    break;
            }
        }
    }

    /**
     * Обрабатывает стратегию self::STRATEGY_TRANSLATE_RUS
     * Если не найден перевод на нужный язык, то ищется перевод на русский
     * Если перевода на русский нет то возвращается `'*** ОШИБКА (' . $message . ') ***'`
     *
     * @param MissingTranslationEvent $event
     *
     * @return string
     */
    private static function getRussian(MissingTranslationEvent $event)
    {
        $category = $event->category;
        $message = $event->message;
        $messageSourceService = \Yii::$app->i18n->getMessageSource($category);
        $messages = $messageSourceService->loadMessages($category, 'ru');
        if (isset($messages[$message])) {
            return $messages[$message];
        }
        $messageSource = SourceMessage::findOne([
            'message'  => $event->message,
            'category' => $event->category,
        ]);
        if (is_null($messageSource)) {
            return '*** ОШИБКА (' . $category . '/' . $message . ') ***';
        } else {
            return '*** ОШИБКА (' . $category . '/ ' . '[' . $messageSource->id .  '] ' . $message . ') ***';
        }
    }

    /**
     * Если не найден перевод на нужный язык, то ищется перевод на русский
     * Если перевода на русский нет то возвращается ключ для перевода
     * $message - ключ для перевода
     *
     * @param MissingTranslationEvent $event
     *
     * @return string
     */
    private static function getRusKey(MissingTranslationEvent $event)
    {
        $category = $event->category;
        $message = $event->message;
        $messageSource = \Yii::$app->i18n->getMessageSource($category);
        $messages = $messageSource->loadMessages($category, 'ru');
        if (isset($messages[$message])) {
            return $messages[$message];
        }

        return $event->message;
    }

    /**
     * Если не найден перевод на нужный язык, то ищется перевод на английский
     * Если перевода на английский нет то возвращается ключ для перевода
     * $message - ключ для перевода
     *
     * @param MissingTranslationEvent $event
     *
     * @return string
     */
    private static function getEngKey(MissingTranslationEvent $event)
    {
        $category = $event->category;
        $message = $event->message;
        $messageSource = \Yii::$app->i18n->getMessageSource($category);
        $messages = $messageSource->loadMessages($category, 'en');
        if (isset($messages[$message])) {
            return $messages[$message];
        }

        return $event->message;
    }

    /**
     * Обрабатывает стратегию self::STRATEGY_TRANSLATE_ERROR
     * Если не найден перевод на нужный язык, то пишется ошибка `'*** ОШИБКА (' . $category . '/' . $message . ') ***'`
     * $message - ключ для перевода
     * $category - категория для перевода
     *
     * @param MissingTranslationEvent $event
     *
     * @return string
     */
    private static function getError(MissingTranslationEvent $event)
    {
        $category = $event->category;
        $message = $event->message;

        $messageSource = SourceMessage::findOne([
            'message'  => $event->message,
            'category' => $event->category,
        ]);
        if (is_null($messageSource)) {
            return '*** ОШИБКА (' . $category . '/' . $message . ') ***';
        } else {
            return '*** ОШИБКА (' . $category . '/ ' . '[' . $messageSource->id .  '] ' . $message . ') ***';
        }
    }

    /**
     * Обрабатывает стратегию self::STRATEGY_TRANSLATE_ERROR_PLUS
     * Если не найден перевод на нужный язык, то пишется `'+++'`
     *
     * @param MissingTranslationEvent $event
     *
     * @return string
     */
    private static function getErrorPlus(MissingTranslationEvent $event)
    {
        return '+++';
    }
}