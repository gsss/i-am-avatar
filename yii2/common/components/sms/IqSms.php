<?php
/**
 * Created by PhpStorm.
 * User: Бог-Творец
 * Date: 12.11.2016
 * Time: 5:26
 */
namespace common\components\sms;

use yii\httpclient\Client;


class IqSms extends \yii\base\BaseObject
{
    public $url = 'https://iqsms.ru/api/api_json/';

    /** @var  string DNS name */
    public $gate;

    /** @var  string логин 'z*' */
    public $login;

    /** @var  string пароль */
    public $password;

    public function send($phone, $text, $sender = false, $wapurl = false)
    {
        if (!YII_ENV_PROD) return true;

        return $this->_send($this->gate, 80, $this->login, $this->password, $phone, $text, 'Avatar', $wapurl);
    }

    /**
     * Отправляет SMS
     *
     * @param string         $host      // "gate.iqsms.ru"
     * @param int            $port      // 80
     * @param string         $login     // "api_login"
     * @param string         $password  // "api_password"
     * @param string         $phone     // "71234567890"
     * @param string         $text      // "text here"
     * @param bool | string  $sender    // "iqsms"
     * @param bool | string  $wapurl    // "wap.yousite.ru"
     *
     * Функция передачи сообщения
     */
    private function _send($host, $port, $login, $password, $phone, $text, $sender = false, $wapurl = false )
    {
        $data = (new Client(['baseUrl' => 'https://api.iqsms.ru/messages/v2']))->get('send', [
            'login'    => $login,
            'password' => $password,
            'phone'    => $phone,
            'text'     => $text,
            'sender'   => $sender,
        ])->send();

        return $data->content;
    }

    /*
    * функция проверки состояния отправленного сообщения
    */
    function status($host, $port, $login, $password, $sms_id)
    {
        $fp = fsockopen($host, $port, $errno, $errstr);
        if (!$fp) {
            return "errno: $errno \nerrstr: $errstr\n";
        }
        fwrite($fp, "GET /status/" .
            "?id=" . $sms_id .
            "  HTTP/1.0\n");
        fwrite($fp, "Host: " . $host . "\r\n");
        if ($login != "") {
            fwrite($fp, "Authorization: Basic " .
                base64_encode($login. ":" . $password) . "\n");
        }
        fwrite($fp, "\n");
        $response = "";
        while(!feof($fp)) {
            $response .= fread($fp, 1);
        }
        fclose($fp);
        list($other, $responseBody) = explode("\r\n\r\n", $response, 2);
        return $responseBody;
    }
} 