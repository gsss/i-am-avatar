<?php

namespace common\components\providers;

use avatar\models\Wallet;
use avatar\modules\ETH\ServiceEtherScan;
use common\models\avatar\Currency;
use common\models\avatar\UserBill;
use common\models\avatar\UserBillAddress;
use common\payment\BitCoinBlockTrailPayment;
use common\services\Security;
use cs\services\VarDumper;
use Yii;
use yii\base\Exception;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;
use yii\helpers\StringHelper;
use yii\httpclient\Client;

/**
 * Компонент осуществляющий достук к API ETH
 */
class ETH extends \yii\base\Component
{
    public $apiUrl;

    public $apiPublicKey;
    public $apiSecretKey;

    public function init()
    {
        if (ArrayHelper::getValue(Yii::$app->params, 'eth.apiUrl', '')) {
            $this->apiUrl = Yii::$app->params['eth']['apiUrl'];
        }
        if (ArrayHelper::getValue(Yii::$app->params, 'eth.apiPublicKey', '')) {
            $this->apiPublicKey = Yii::$app->params['eth']['apiPublicKey'];
        }
        if (ArrayHelper::getValue(Yii::$app->params, 'eth.apiSecretKey', '')) {
            $this->apiSecretKey = Yii::$app->params['eth']['apiSecretKey'];
        }
    }

    /** @var  \yii\httpclient\Client */
    private $provider;

    /**
     * @return \yii\httpclient\Client
     */
    public function getProvider()
    {
        if (is_null($this->provider)) {
            $this->provider = $this->_getProvider();
        }

        return $this->provider;
    }

    /**
     * @return \yii\httpclient\Client
     */
    public function _getProvider()
    {
        return new Client(['baseUrl' => $this->apiUrl]);
    }

    /**
     * Создает кошелек
     *
     * Создает счет \common\models\avatar\UserBill
     * Создает адрес новый \common\models\avatar\UserBillAddress
     *
     * @param string        $name
     * @param string        $password
     * @param int | null    $userId
     * @param int           $passwordType если = 2 то $password пароль от кабинета
     *
     * @return null | \avatar\models\Wallet
     *
     * @throws
     */
    public function create($name, $password, $userId = null, $passwordType = \common\models\avatar\UserBill::PASSWORD_TYPE_OPEN)
    {
        $passwordWallet = Security::generateRandomString();
        if ($passwordType != \common\models\avatar\UserBill::PASSWORD_TYPE_OPEN) {
            $passwordHash = UserBill::passwordEnСript($passwordWallet, $password);
        } else {
            $passwordHash = $passwordWallet;
        }
        $ret = $this->_callPost('new', [
            'password' => $passwordWallet,
        ]);
        try {
            $data = Json::decode($ret->content);
        } catch (\Exception $e) {
            Yii::error($e->getMessage() . ' ' . \yii\helpers\VarDumper::dumpAsString($ret), 'avatar\common\components\providers\ETH::create');
            throw $e;
        }
        Yii::info(\yii\helpers\VarDumper::dumpAsString($data), 'avatar\common\components\providers\ETH::create');
        if (isset($data['error'])) throw new Exception($data['error']);

        $t = Yii::$app->db->beginTransaction();
        try {
            $address = $data['address'];

            // Сохраняю счет
            $billing = \common\models\avatar\UserBill::add([
                'user_id'       => $userId,
                'address'       => $address,
                'name'          => $name,
                'password'      => $passwordHash,
                'password_type' => $passwordType,
                'currency'      => Currency::ETH,
            ]);

            $walletAvatar = new Wallet([
                'billing' => $billing,
            ]);

            $t->commit();

            return $walletAvatar;

        } catch (\Exception $e) {
            Yii::warning($e->getMessage(), 'avatar\\common\\components\\providers\\ETH::create');
            $t->rollBack();

            return null;
        }
    }

    /**
     * Отправляет деньги на кошелек
     *
     * @param string $from     адрес отправителя начинающийся с 0x
     * @param string $password пароль отпрателя для адреса from
     * @param string $to       адрес получателя начинающийся с 0x
     * @param float  $amount   кол-во эфира для отправки, десятичная точка
     * @param array  $options  дополнительные опциональные параметры
     * - gasLimit - int - лимит газа, по умолчанию 21000
     * - gasPrice - int - цена газа, по умолчанию 21000000000
     * - data - string - строка для данных (! пока не внедрено)
     *
     * @return array
     * [
     *      'transaction'
     * ]
     *
     * @throws
     * 401 Не верная авторизация API на сервере
     * 502 Сервер не отвечает
     * 402 Не верный формат JSON от сервера
     * 400 Другая ошибка от API
     */
    public function send($from, $password, $to, $amount, $options = [])
    {
        $params = [
            'user'     => $from,
            'password' => $password,
            'address'  => $to,
            'amount'   => $amount,
        ];
        if (isset($options['gasLimit'])) $params['gasLimit'] = $options['gasLimit'];
        if (isset($options['gasPrice'])) $params['gasPrice'] = $options['gasPrice'];

        Yii::info(\yii\helpers\VarDumper::dumpAsString(Security::maskPasswordInArray($params)), 'avatar\models\WalletETH::pay1');
        $response = $this->_callPost('send', $params);
        Yii::info(\yii\helpers\VarDumper::dumpAsString($response), 'avatar\models\WalletETH::pay1');
        if ($response->headers['http-code'] == 401) {
            throw new \Exception('Не верная авторизация API на сервере', 401);
        }
        if ($response->headers['http-code'] != 200) {
            throw new \Exception('Провайдер не отвечает code=' . $response->headers['http-code'], 502);
        }

        try {
            $data = Json::decode($response->content);
        } catch (\Exception $e) {
            throw new \Exception('Не верный JSON', 402);
        }

        if (isset($data['error'])) {
            throw new \Exception(Json::encode($data), 400);
        }
        Yii::trace(\yii\helpers\VarDumper::dumpAsString($response), 'avatar\\common\\components\\providers\\ETH::send');

        return ['transaction' => $data['transaction']['address']];
    }

    /**
     * Вызывает исполнение контракта
     *
     * @param string    $user
     * @param string    $password
     * @param string    $contract адрес
     * @param string    $abi
     * @param string    $functionName
     * @param array     $params
     * @param bool      $write
     * @param array     $options
     * - gasPrice - int - цена газа, wei, по умолчанию 21000000
     * - value - int - кол-во wei отправляемое в контракт для функции payable (! еще не реализовано)
     *
     * @return string идентификатор транзакции в положительном исходе, иначе вызывается исключение
     * error
     * 'Invalid number of arguments to Solidity function'
     * 'Array.prototype.slice called on null or undefined'
     * 'parameters is not defined'
     *
     * @throws \Exception
     * 502 Сервер не доступен
     * 400 Ошибка на стороне API ($e->getMessage() - описание ошибки)
     * 401 Не верная авторизация API на сервере
     * 403 Ошибка при отправке запроса _callPost
     */
    public function contract($user, $password, $contract, $abi, $functionName, $params = [], $write = true, $options = [])
    {
        if ($write) {
            $write = 'true';
        } else {
            $write = 'false';
        }

        $params = [
            'user'     => $user,
            'password' => $password,
            'contract' => $contract,
            'abi'      => $abi,
            'func'     => $functionName,
            'params'   => Json::encode($params),
            'write'    => $write,
        ];

        if (isset($options['gasPrice'])) $params['gasPrice'] = $options['gasPrice'];

        try {
            $response = $this->_callPost('contract', $params);
        } catch (\Exception $e) {
            throw new \Exception($e->getMessage(), 403);
        }

        if ($response->headers['http-code'] == 401) {
            throw new \Exception('Не верная авторизация API на сервере', 401);
        }
        if ($response->headers['http-code'] != 200) {
            throw new \Exception('Провайдер не отвечает', 502);
        }

        try {
            $response->content = Json::decode($response->content);
        } catch (\Exception $e) {
            throw new \Exception($response->content, 402);
        }

        Yii::info(\yii\helpers\VarDumper::dumpAsString($response), 'avatar\common\components\providers\ETH::contract()');
        if (isset($response->content['error'])) {
            throw new \Exception($response->content['error'], 400);
        }
        if (isset($response->content['transaction'])) {
            return $response->content['transaction']['hash'];
        }

        throw new \Exception(\yii\helpers\VarDumper::dumpAsString($response->content), 400);
    }

    /**
     * Вызывает исполнение контракта
     *
     * @param string    $user       адрес кошелька начиная с 0x
     * @param string    $password
     *
     * @return bool true
     * error
     * 'Invalid number of arguments to Solidity function'
     * 'Array.prototype.slice called on null or undefined'
     * 'parameters is not defined'
     *
     * @throws \Exception
     * 502 Сервер не доступен
     * 400 Ошибка на стороне API ($e->getMessage() - описание ошибки)
     * 401 Не верная авторизация API на сервере
     * 403 Ошибка при отправке запроса _callPost
     */
    public function testPassword($user, $password)
    {
        $user = strtolower($user);
        $params = [
            'user'     => $user,
            'password' => $password,
        ];

        try {
            $response = $this->_callPost('testPassword', $params);
        } catch (\Exception $e) {
            throw new \Exception($e->getMessage(), 403);
        }

        if ($response->headers['http-code'] == 401) {
            throw new \Exception('Не верная авторизация API на сервере', 401);
        }
        if ($response->headers['http-code'] != 200) {
            throw new \Exception('Провайдер не отвечает', 502);
        }

        try {
            $response->content = Json::decode($response->content);
        } catch (\Exception $e) {
            throw new \Exception($response->content, 402);
        }

        Yii::info(\yii\helpers\VarDumper::dumpAsString($response), 'avatar\common\components\providers\ETH::testPassword()');
        if (isset($response->content['error'])) {
            throw new \Exception($response->content['error'], 400);
        }
        if (isset($response->content['success'])) {
            return true;
        }

        throw new \Exception(\yii\helpers\VarDumper::dumpAsString($response->content), 400);
    }

    /**
     * Расчитывает
     * Вызывает исполнение контракта
     *
     * @param string $user
     * @param string $password
     * @param string $contract адрес
     * @param string $abi
     * @param string $functionName
     * @param array  $params
     * @param bool   $write
     *
     * @return array
     * [
     *      'cost'
     *      'gasPrice'
     * ]
     *
     * error
     * 'Invalid number of arguments to Solidity function'
     * 'Array.prototype.slice called on null or undefined'
     * 'parameters is not defined'
     *
     * @throws \Exception
     * 401 Не авторизовано API
     * 502 Сервер не отвечает
     * 100 Сервер выдает ошибку
     * 101 API выдает указанную ошибку
     * 402 От сервера невозможно рабобрать формат данных
     */
    public function contractCalculate($user, $password, $contract, $abi, $functionName, $params = [], $write = true)
    {
        if ($write) {
            $write = 'true';
        } else {
            $write = 'false';
        }

        $response = $this->_callPost('calculateCostCall', [
            'user'     => $user,
            'password' => $password,
            'contract' => $contract,
            'abi'      => $abi,
            'func'     => $functionName,
            'params'   => Json::encode($params),
            'write'    => $write,
        ]);
        Yii::info(\yii\helpers\VarDumper::dumpAsString($response), 'avatar\common\components\providers\ETH::contractCalculate()');
        if ($response->headers['http-code'] != 200) {
            switch ($response->headers['http-code']) {
                case 401:
                    throw new \Exception('Не авторизовано API', 401);
                case 502:
                    throw new \Exception('Сервер не отвечает', 502);
                default:
                    throw new \Exception('Код ошибки сервера =' . $response->headers['http-code'], 100);
            }
        }

        try {
            $response->content = Json::decode($response->content);
        } catch (\Exception $e) {
            // ничего не делать скорее всего это транзакция
            if (!StringHelper::startsWith($response->content, '0x')) {
                throw new \Exception($response->content, 402);
            }
        }

        Yii::info(\yii\helpers\VarDumper::dumpAsString($response), 'avatar\common\components\providers\ETH::contractCalculate()');
        if (isset($response->content['error'])) {
            throw new \Exception($response->content['error'], 101);
        }

        return $response->content;
    }

    /**
     * Регистрирует контракт
     *
     * @param string        $user
     * @param string        $password
     * @param string        $contract адрес
     * @param string        $contractName
     * @param array         $params
     *
     * @return array
     *
     * @throws \Exception
     */
    public function registerContract($user, $password, $contract, $contractName, $params = [])
    {
        $params = [
            'user'         => $user,
            'password'     => $password,
            'contract'     => $contract,
            'contractName' => $contractName,
            'params'       => Json::encode($params),
        ];
        $response = $this->_callPost('registerContract', $params);

        try {
            $data = Json::decode($response->content);
        } catch (\Exception $e) {
            throw new \Exception('Unknown JSON');
        }

        Yii::info(\yii\helpers\VarDumper::dumpAsString($data), 'avatar\common\components\providers\ETH::registerContract()');
        if (isset($data['error'])) {
            throw new \Exception($data['error']);
        }

        return $data;
    }

    /**
     * Регистрирует контракт
     *
     * @param string        $user
     * @param string        $password           пароль от атлантиды
     * @param string        $contract адрес
     * @param string        $contractName
     * @param array         $params
     *
     * @return array
     *
     * @throws \Exception
     */
    public function calculateRegisterContract($user, $password, $contract, $contractName, $params = [])
    {
        $params = [
            'user'         => $user,
            'password'     => $password,
            'contract'     => $contract,
            'contractName' => $contractName,
            'params'       => Json::encode($params),
        ];
        $response = $this->_callPost('calculateCostRegister', $params);

        try {
            $data = Json::decode($response->content);
        } catch (\Exception $e) {
            throw new \Exception('Unknown JSON');
        }

        Yii::info(\yii\helpers\VarDumper::dumpAsString($data), 'avatar\common\components\providers\ETH::registerContract()');
        if (isset($data['error'])) {
            throw new \Exception($data['error']);
        }

        return $data;
    }

    /**
     * Отправляет метод в API и возвращает ответ
     *
     * @param string       $path путь на API
     * @param array | null $data параметры вызова метода
     *
     * @return \yii\httpclient\Response
     */
    public function _callPost($path, $data = [])
    {
        return $this->_call('post', $path, $data);
    }

    /**
     * Отправляет метод в API и возвращает ответ, и обрабатывает ошибки
     *
     * @param string       $path путь на API
     * @param array | null $data параметры вызова метода
     *
     * @return \yii\httpclient\Response
     * @throws \Exception
     * 1401 Не верная авторизация API на сервере
     * 1502 Сервер не отвечает
     * 1402 Не верный формат JSON от сервера
     * 1400 Другая ошибка от API, в $e->message() выдается JSON от сервера
     */
    public function callPost($path, $data = [])
    {
        $response = $this->_call('post', $path, $data);
        if ($response->headers['http-code'] == 401) {
            throw new \Exception('Не верная авторизация API на сервере', 1401);
        }
        if ($response->headers['http-code'] != 200) {
            throw new \Exception('Провайдер не отвечает code=' . $response->headers['http-code'], 1502);
        }

        try {
            $data = Json::decode($response->content);
        } catch (\Exception $e) {
            throw new \Exception('Не верный JSON', 1402);
        }

        if (isset($data['error'])) {
            throw new \Exception(Json::encode($data), 1400);
        }

        return $response;
    }

    /**
     * Отправляет метод в API и возвращает ответ
     *
     * @param string       $path путь на API
     * @param array | null $data параметры вызова метода
     *
     * @return \yii\httpclient\Response
     */
    public function _callGet($path, $data = [])
    {
        return $this->_call('get', $path, $data);
    }

    /**
     * Отправляет метод в API и возвращает ответ
     *
     * @param string       $method метод 'post'/'get'
     * @param string       $path путь на API
     * @param array | null $data параметры вызова метода
     *
     * @return \yii\httpclient\Response
     */
    protected function _call($method, $path, $data = [])
    {
        $provider = $this->getProvider();

        Yii::info(\yii\helpers\VarDumper::dumpAsString([
            $path,
            Security::maskPasswordInArray($data),
        ]), 'avatar\common\components\providers\ETH::_call');

        $credentials = [
            $this->apiPublicKey,
            $this->apiSecretKey,
        ];
        Yii::info(\yii\helpers\VarDumper::dumpAsString([
            $this->apiPublicKey,
            Security::maskPassword($this->apiSecretKey),
        ]), 'avatar\common\components\providers\ETH::_call2');
        $headerAuthorization = [
            'Authorization',
            'Basic' . ' ' . base64_encode(join(':', $credentials)),
        ];
        Yii::info(\yii\helpers\VarDumper::dumpAsString([
            'Authorization',
            'Basic' . ' ' . Security::maskPassword(base64_encode(join(':', $credentials))),
        ]), 'avatar\common\components\providers\ETH::_call2');

        if ($method == 'get') {
            $data = $provider->get($path, $data, [$headerAuthorization[0] => $headerAuthorization[1]])->send();
            Yii::info(\yii\helpers\VarDumper::dumpAsString($data), 'avatar\common\components\providers\ETH::_call');

            return $data;
        }
        if ($method == 'post') {
            $data = $provider->post($path, $data, [$headerAuthorization[0] => $headerAuthorization[1]])->send();
            Yii::info(\yii\helpers\VarDumper::dumpAsString($data), 'avatar\common\components\providers\ETH::_call');

            return $data;
        }
    }


    /**
     * @param $addressIn
     * @param $page
     * @return array
     * [
     * [
     *      'txid' => string
     *      'block' => int
     * ]
     * ]
     */
    public static function getTransactionList($addressIn, $page)
    {
        return ServiceEtherScan::getTransactionList($addressIn, $page);
    }

    /**
     * Импортирует кошелек
     *
     * @param string $contentJson       содержимое файла JSON
     * @param string $password          пароль от JSON файла
     *
     * @return string адрес кошелька начинающийся с 0x
     */
    public function importJsonFile($contentJson, $password)
    {
        $response = $this->_callPost('importWalletJson', [
            'jsonFile' => $contentJson,
            'password' => $password,
        ]);
        \Yii::info(\yii\helpers\VarDumper::dumpAsString($response), '\common\components\providers\ETH::importJsonFile');

        $data = Json::decode($response->content);

        return $data['address'];
    }

    /**
     * Импортирует приватный ключ
     *
     * @param string $key       приватный ключ
     * @param string $password  пароль по которому будет осуществляться доступ к кошельку в дальнейшем
     *
     * @return string адрес кошелька начинающийся с 0x
     */
    public function importPrivateKey($key, $password)
    {
        $response = $this->_callPost('', [
            'key'       => $key,
            'password'  => $password,
        ]);
        $data = Json::decode($response->content);

        return $data['address'];
    }
}