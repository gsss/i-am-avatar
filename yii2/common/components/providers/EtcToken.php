<?php

namespace common\components\providers;

use avatar\models\Wallet;
use avatar\modules\ETH\ServiceEtherScan;
use common\models\avatar\Currency;
use common\models\avatar\UserBill;
use common\models\avatar\UserBillAddress;
use common\payment\BitCoinBlockTrailPayment;
use common\services\Security;
use cs\services\VarDumper;
use Yii;
use yii\base\Exception;
use yii\helpers\Json;
use yii\helpers\StringHelper;
use yii\httpclient\Client;

/**
 * Компонент осуществляющий достук к API ETH
 */
class EtcToken extends ETC
{
    /** @var int идентификатор валюты, таблица: `currency.id` */
    public $currency;

    /** @var  string адрес контракта */
    public $contract;

    /** @var  string интерфейс контракта */
    public $abi;

    /**
     * Отправляет деньги на кошелек
     *
     * @param string $from     адрес отправителя начинающийся с 0x
     * @param string $password пароль отпрателя для адреса from atlantida.io
     * @param string $to       адрес получателя начинающийся с 0x
     * @param float  $amount   кол-во мелких единиц токена
     *
     * @return array
     * [
     *      'transaction'
     * ]
     *
     * Ошибки
     * [
     *      'error' => 'Error: could not decrypt key with given passphrase ...'
     * ]
     *
     * @throws
     */
    public function send($from, $password, $to, $amount)
    {
        $response = $this->contract(
            $from,
            $password,
            $this->contract,
            $this->abi,
            'transfer',
            [
                $to,
                $amount,
            ],
            false
        );

        Yii::info(\yii\helpers\VarDumper::dumpAsString($response), 'avatar\common\components\providers\Token::send()');

        return [
            'transaction' => $response,
        ];
    }

    /**
     * Расчитывает
     * Отправляет деньги на кошелек
     *
     * @param string $from     адрес отправителя начинающийся с 0x
     * @param string $password пароль отпрателя для адреса from atlantida.io
     * @param string $to       адрес получателя начинающийся с 0x
     * @param float  $amount   кол-во мелких единиц токена
     *
     * @return array
     * [
     *      'transaction'
     * ]
     *
     * Ошибки
     * [
     *      'error' => 'Error: could not decrypt key with given passphrase ...'
     * ]
     *
     * @throws
     */
    public function sendCalculate($from, $password, $to, $amount)
    {
        $response = $this->contractCalculate(
            $from,
            $password,
            $this->contract,
            $this->abi,
            'transfer',
            [
                $to,
                $amount,
            ]
        );

        Yii::info(\yii\helpers\VarDumper::dumpAsString($response), 'avatar\common\components\providers\Token::send()');

        return [
            'transaction' => $response,
        ];
    }

    /**
     * Проверяет баланс на кошельке
     *
     * @param string $address адрес кошелька
     *
     * @return int малые единицы
     * @throws
     */
    public function getBalance($address)
    {
        if (false) {
            $client = new Client(['baseUrl' => 'https://api.tokenbalance.com']);
            $response = $client->post('balance/'.$this->contract.'/'.$address)->send();
            $data = Json::decode($response->content);
            $data['balance'];
            $data['decimals'];
            $pow = bcpow(10, $data['decimals']);
            $val = bcmul($data['balance'], $pow);

            return $val;
        } else {
            $provider = new ServiceEtherScan();
            $data = $provider->tokenGetBalance($this->contract, $address);

            return $data;
        }
    }

    /**
     * Выдает список транзакций по 20 шт
     *
     * @param string $address адрес кошелька
     * @param string $page    страница
     *
     * @return array
     * [
         * {
             * "timestamp": 1507394270,
             * "transactionHash": "0xa593a273b3c6fdc151849fb8864d656172f79e8d52014d2ab4a536387670ed60",
             * "tokenInfo": {
                 * "address": "0x86fa049857e0209aa7d9e616f7eb3b3b78ecfdb0",
                 * "name": "EOS",
                 * "decimals": 18,
                 * "symbol": "EOS",
                 * "totalSupply": "1000000000000000000000000000",
                 * "owner": "0xd0a6e6c54dbc68db5db3a091b171a77407ff7ccf",
                 * "txsCount": 624071,
                 * "transfersCount": 733467,
                 * "lastUpdated": 1510883995,
                 * "totalIn": 2.5964967036019e+27,
                 * "totalOut": 2.5964967036019e+27,
                 * "issuancesCount": 0,
                 * "holdersCount": 108009,
                 * "description": "https://eos.io/",
                 * "price": {
                     * "rate": "1.65861",
                     * "diff": 3.14,
                     * "diff7d": 43.49,
                     * "ts": "1510884263",
                     * "marketCapUsd": "797326417.0",
                     * "availableSupply": "480719649.0",
                     * "volume24h": "67217100.0",
                     * "currency": "USD"
                 * }
             * },
             * "type": "transfer",
             * "value": "1500000000000000000",
             * "from": "0x324f0b05cb1e2c586e07fa7da14312f0fced9f7e",
             * "to": "0x2ed7ea0efc43f19cce01903d43f85e04fa92d855"
         * }, ...
     * ]
     *
     * @throws
     */
    public function getTransactions($address, $page)
    {
        $provider = new \avatar\modules\ETH\ServiceEthPlorer();
        $data = $provider->getAddressHistory($address, $this->contract, 'transfer', 20, $page);

        return $data;
    }

    /**
     * Вызывает контракт
     *
     * @param string $from
     * @param string $password
     * @param string $function
     * @param array $params
     *
     * @return array
     * @throws
     */
    public function call($from, $password, $function, $params = [])
    {
        $txid = $this->contract(
            $from,
            $password,
            $this->contract,
            $this->abi,
            $function,
            $params
        );

        return [
            'transaction' => $txid,
        ];
    }

}