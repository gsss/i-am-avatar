<?php

namespace common\components\providers;

use avatar\models\Wallet;
use common\models\avatar\Currency;
use common\models\avatar\UserBill;
use common\models\avatar\UserBillAddress;
use common\payment\BitCoinBlockTrailPayment;
use common\services\Security;
use cs\services\VarDumper;
use Yii;
use yii\helpers\Json;
use yii\helpers\StringHelper;

/**
 * Компонент подсчитывающий колво обращений на сервер в сек
 *
 * Created by PhpStorm.
 * User: s.arhangelskiy
 * Date: 01.06.2016
 * Time: 14:41
 */
class BTC extends \yii\base\Component
{
    /** @var  BitCoinBlockTrailPayment */
    private $provider;

    /**
     * @return BitCoinBlockTrailPayment
     */
    public function getProvider()
    {
        if (is_null($this->provider)) {
            $this->provider = $this->_getProvider();
        }

        return $this->provider;
    }

    /**
     * @return BitCoinBlockTrailPayment
     */
    public function _getProvider()
    {
        return new BitCoinBlockTrailPayment();
    }

    /**
     * @param string $identy
     *
     * @return array
     * [
     *  $confirmedBalance, // satoshi
     *  $unconfirmedBalance // satoshi
     * ]
     */
    public function getBalance($identy)
    {
        $client = $this->getProvider()->getClient();
        return $client->getWalletBalance($identy);
    }

}