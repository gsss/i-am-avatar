<?php

namespace common\components;

use common\services\Debugger;
use Yii;
use yii\helpers\VarDumper;

/**
 * менеджер пользователей Онлайн
 *
 * Это сервис, который позволяет ограничивать кол-во пользователей находящихся на сайте (frontend) одновременно.
 * Принимаю за определение что пользователь который не совершил никаких действий в течение 10 минут выходит из системы.
 *
 * Идентификация пользователя происходит по его id если он авторизован и по ip если не авторизован.
 *
 * Для подсчета использую массив пользователей которые находятся онлайн на сайте. Этот массив хранится в Кеше
 * ```php
 * Yii::$app->cache->get('usersOnline');
 * ```
 * В дальнейшем можно сделать чтобы вместо IP сохранял SESSION_ID чтобы быть точнее для гостей.
 *
 *
 * Формат массива
 * ```php
 * [
 *      ['ip', '56.45.65.4', 1423545684],
 *      ['ip', '56.45.65.2', 1423545684],
 *      ['id', 210,          1423545684],
 *      ['id', 211,          1423545684],
 *      ['id', 212,          1423545684],
 * ]
 * ```
 *
 * Кол-во элементов массива определяет кол-во пользователей онлайн.
 *
 * Элемент массива следующий:
 * - тип объекта. Может быть 'ip' и 'id'. Что означает что пользователь гость или авторизованный пользователь соответственно.
 * - если 'ip' то второй элемент массива является строкой ip
 * если 'id' то второй элемент массива является целым числом идентификатором пользователя
 * - временная метка последнего действия в системе
 *
 * @property int $maxUsersOnline кол-во пользователей которое одновременно может сидеть онлайн, шт
 * @property int $sessionTime длина периуда сессии пользователя в сек
 * @property int $maxTimeToUpdate время в сек после которого происходит update в кеш. Если null то не учитывается
 *
 * Created by PhpStorm.
 * User: s.arhangelskiy
 * Date: 01.06.2016
 * Time: 14:41
 */
class OnlineManager extends \yii\base\Component
{
    const POS_TYPE   = 0;
    const POS_IDENTY = 1;
    const POS_TIME   = 2;

    public $maxUsersOnline;
    public $sessionTime = 600;
    public $maxTimeToUpdate;
    public $cacheKey = 'usersOnline';

    /** @var double время последнего очищения кеша от старых пользователей */
    private $lastClearTime;

    /** @var int длина периода (сек) после которого можно запустить очищение кеша от старых пользователей, если null то не учитывается и очистка производится перед каждым REQUEST */
    public $clearTime;

    /**
     * Выдает ответ возможно ли пользователю зайти на сайт и обновляет данные для текущего пользователя и для нового
     */
    public function isPossible()
    {
        $users = $this->get();
        $users = $this->clear($users);

        if (Yii::$app->user->isGuest) {
            $id = Yii::$app->request->userIP;
            $type = 'ip';
        } else {
            $id = Yii::$app->user->id;
            $type = 'id';
        }

        // Обрабатываю если найдено
        {
            $isFind = false;
            $isUpdate = false;
            $new = [];
            foreach ($users as $user) {

                if ($user[self::POS_IDENTY] == $id) {
                    $isFind = true;
                    if ((time() - $user[self::POS_TIME]) > $this->maxTimeToUpdate) {
                        // обновить
                        $new[] = [$type, $id, time()];
                        $isUpdate = true;
                    }
                } else {
                    $new[] = $user;
                }
            }
            if ($isUpdate) {
                $this->set($new);
            }
        }
        if (!$isFind) {
            // нужно добавить
            if ($this->maxUsersOnline) {
                if ($this->maxUsersOnline < $users) {
                    $users[] = [$type, $id, time()];
                    $this->set($users);
                } else {
                    return false;
                }
            } else {
                $users[] = [$type, $id, time()];
                $this->set($users);
            }
        }

        return true;
    }

    /**
     * Готовая функция для события Application::EVENT_BEFORE_REQUEST
     * обеспечивающая функциональность.
     *
     * @return bool
     * @throws \yii\base\Exception
     */
    public function onBeforeRequest()
    {
        if ($this->isPossible()) {
            return true;
        } else {
            throw  new \yii\base\Exception('Извините сервер перегружен. Зайдите позже');
        }
    }

    /**
     * Выдает пользователя по идентификатору
     *
     * @param array        $users
     * @param string | int $id
     *
     * @return bool | array
     */
    public function getUserById($users, $id)
    {
        foreach ($users as $user) {
            if ($user[self::POS_IDENTY] == $id) {
                return $user;
            }
        }

        return false;
    }

    /**
     * Производит чистку массива используя функцию `$this->_clear()`
     * Вызывая ее если надо с сзадержкой `$this->clearTime`
     *
     * @param $users
     * @return array
     */
    public function clear($users)
    {
        if ($this->clearTime) {
            if ($this->lastClearTime) {
                if ((microtime(true) - $this->lastClearTime) > $this->clearTime) {
                    $users = $this->_clear($users);
                    $this->lastClearTime = microtime(true);
                }
            } else {
                $users = $this->_clear($users);
                $this->lastClearTime = microtime(true);
            }
        } else {
            $users = $this->_clear($users);
        }

        return $users;
    }

    /**
     * Производит чистку массива от старых значений
     * Если нужно удалить то обновляет в кеше
     *
     * @return array Новые пользователи
     */
    protected function _clear($users)
    {
        $new = [];
        $isChange = false;
        foreach ($users as $user) {
            if ((time() - $user[self::POS_TIME]) > $this->sessionTime) {
                // удаляю из массива
                $isChange = true;
            } else {
                $new[] = $user;
            }
        }
        if ($isChange) {
            $this->set($new);
            return $new;
        } else {
            return $users;
        }
    }

    /**
     * Сохраняет пользователей в кеш
     *
     * @param $users
     */
    protected function set($users)
    {
        Yii::$app->cache->set($this->cacheKey, $users);
    }

    /**
     * Выдает пользователей онлайн в виде массива
     *
     * @return array
     */
    public function get()
    {
        $value = Yii::$app->cache->get($this->cacheKey);
        if ($value === false) $value = [];

        return $value;
    }
    
    public function count()
    {
        return count($this->get());
    }
}