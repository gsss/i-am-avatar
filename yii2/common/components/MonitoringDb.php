<?php

namespace common\components;

use common\services\Debugger;
use Yii;

/**
 * Компонент подсчитывающий кол-во обращений на базу данных в сек
 *
 * Created by PhpStorm.
 * User: s.arhangelskiy
 * Date: 01.06.2016
 * Time: 14:41
 */
class MonitoringDb extends \yii\base\Component
{
    /**
     * @var string под этим ключом хранится массив значений счетчиков
     * [
     *     'time'               => time() - время начало отсчета счетчика в сек
     *     'request_per_second' => <value>,
     *     // ...
     * ]
     */
    public $cacheKey = 'monitoringDb';

    /**
     * @var array счетчики
     * [
     *      'time' => 142... // время неачала отсчета, sec
     *      'master' => [
     *                      'select' => 0,
     *                      'insert' => 0,
     *                      'update' => 0,
     *                      'delete' => 0,
     *      ],
     *      'statistic' => [
     *                      'select' => 0,
     *                      'insert' => 0,
     *                      'update' => 0,
     *                      'delete' => 0,
     *      ],
     *      'slaveArray' => [
     *            [
     *                      'select' => 0,
     *                      'insert' => 0,
     *                      'update' => 0,
     *                      'delete' => 0,
     *           ], ...
     *      ],
     * ]
     */
    protected $counters;


    /**
     * Прибавляет счетчик для SELECT на 1
     *
     * @param string | int $name
     * string - 'master'
     * int - подразумевается что это индекс slave
     * @param string $type тип счетчика
     */
    protected function inc($name, $type)
    {
        $counters = $this->get();

        $isChange = false;
        if (is_integer($name) || preg_match('/[0-9.-]/', $name)) {
            $index = (int)$name;
            if (count($counters['slaveArray']) < $index) {
                $counters['slaveArray'][$index][$type]++;
                $isChange = true;
            }
        } else {
            switch ($name) {
                case 'master':
                    $counters['master'][$type]++;
                    $isChange = true;
                    break;
                case 'slave':
                    $counters['slaveArray'][0][$type]++;
                    $isChange = true;
                    break;
                case 'statistic':
                    $counters['statistic'][$type]++;
                    $isChange = true;
                    break;
            }
        }
        if ($isChange) {
            $this->set($counters);
        }
    }

    /**
     * Прибавляет счетчик для SELECT на 1
     *
     * @param string | int $name
     * string - 'master' | 'slave'
     * int - подразумевается что это индекс slave
     */
    public function onSelect($name)
    {
        $this->inc($name, 'select');
    }

    public function onInsert($name)
    {
        $this->inc($name, 'insert');
    }

    public function onUpdate($name)
    {
        $this->inc($name, 'update');
    }

    public function onDelete($name)
    {
        $this->inc($name, 'delete');
    }

    /**
     * Выдает массив расчитанных счетчиков
     * Иначе производится расчет <Значение счетчика> / time() - <момент начала отсчета счетчика>
     *
     * @return array
     * [
     *     'master' => [
     *         'select' => 2825
     *         'insert' => 115
     *         'update' => 0
     *         'delete' => 0
     *     ]
     *     'statistic' => [
     *         'select' => 644
     *         'insert' => 106
     *         'update' => 0
     *         'delete' => 0
     *     ]
     *     'slaveArray' => [
     *         0 => [
     *             'select' => 0
     *             'insert' => 0
     *             'update' => 0
     *             'delete' => 0
     *         ]
     *     ]
     * ]
     */
    public function calculate()
    {
        $value = $this->get();
        $report = [];
        $delta = time() - $value['time'];
        foreach ($value['slaveArray'] as $i) {
            $report['slaveArray'][] = [
                'select' => (int)(($i['select'] / $delta)*60),
                'insert' => (int)(($i['insert'] / $delta)*60),
                'update' => (int)(($i['update'] / $delta)*60),
                'delete' => (int)(($i['delete'] / $delta)*60),
            ];
        }
        $i = $value['master'];
        $report['master'] = [
            'select' => (int)(($i['select'] / $delta)*60),
            'insert' => (int)(($i['insert'] / $delta)*60),
            'update' => (int)(($i['update'] / $delta)*60),
            'delete' => (int)(($i['delete'] / $delta)*60),
        ];
        $i = $value['statistic'];
        $report['statistic'] = [
            'select' => (int)(($i['select'] / $delta)*60),
            'insert' => (int)(($i['insert'] / $delta)*60),
            'update' => (int)(($i['update'] / $delta)*60),
            'delete' => (int)(($i['delete'] / $delta)*60),
        ];

        return $report;
    }

    public function reset()
    {
        $new = $this->getNullCounters();
        $this->set($new);

        return $new;
    }

    /**
     * Сохраняет пользователей в кеш
     *
     * @param array $counterList
     */
    protected function set($counterList)
    {
        Yii::$app->cache->set($this->cacheKey, $counterList);
    }

    /**
     * Выдает пользователей онлайн в виде массива
     *
     * @return array
     */
    public function get()
    {
        $value = Yii::$app->cache->get($this->cacheKey);
        if ($value === false) {
            $value = $this->getNullCounters();
            $this->set($value);
        };

        return $value;
    }

    /**
     * @return array
     */
    private function getNullCounters()
    {
        $now = time();
        $counters = [
            'time'       => $now,
            'master'     => [
                'select' => 0,
                'insert' => 0,
                'update' => 0,
                'delete' => 0,
            ],
            'statistic'  => [
                'select' => 0,
                'insert' => 0,
                'update' => 0,
                'delete' => 0,
            ],
            'slaveArray' => [],
        ];

        foreach (Yii::$app->db->slaves as $slave) {
            $counters['slaveArray'][] = [
                'select' => 0,
                'insert' => 0,
                'update' => 0,
                'delete' => 0,
            ];
        }

        return $counters;
    }
}