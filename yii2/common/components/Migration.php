<?php
/**
 * Created by PhpStorm.
 * User: s.arhangelskiy
 * Date: 06.10.2016
 * Time: 16:16
 */

namespace common\components;

use common\models\language\Category;
use common\models\language\Message;
use common\models\language\SourceMessage;
use common\services\Debugger;
use Stripe\Source;
use Yii;
use yii\helpers\ArrayHelper;
use yii\helpers\FileHelper;
use yii\helpers\VarDumper;

class Migration extends \yii\db\Migration
{
    public $isLog = true;

    public $isReplacePath = false;

    /**
     * @param array $strings
     * [
     *   'messages' => [
     *              'id' => 12,
     *              // ...
     *          ],
     *          // ...
     * ]
     *
     * @return null | true
     */
    public function updateStrings($strings)
    {
        if (isset($strings['categoryTree'])) {
            $this->restoreTree($strings['categoryTree']);
        }
        $ids = $this->checkDouble($strings);
        if (count($ids) > 0) {
            if (!$this->isLog) {
                Yii::$app->cacheFile->set('\common\components\Migration::updateStrings', $strings, 60 * 10);
                Yii::$app->cacheFile->set('\common\components\Migration::updateStringsIds', $ids, 60 * 10);

                return null;
            }
            do {
                self::log('Double exist.');
                self::log(VarDumper::dumpAsString($ids));
                echo "\n  all/new/cancel";
                $answer = trim(fgets(STDIN));
                $answer = strtolower($answer);
            } while (in_array($answer, ['a', 'n', 'c']));

            if ($answer == 'a' || $answer == 'all') {
                // стираю старые для перезаписи и добавляю все
                $this->deleteMessages($ids);
                $this->insertMessages($strings['messages']);

            } else if ($answer == 'n' || $answer == 'new') {
                // добавляю только новые
                // собираю новые
                {
                    $new = [];
                    foreach ($strings as $string) {
                        if (!in_array($string['id'], $ids)) {
                            $new[] = $string;
                        }
                    }
                }
                $this->insertMessages($new);

            } else if ($answer == 'c' || $answer == 'cancel') {
                self::log('Canceled.');
                return true;
            }
        } else {
            $this->insertMessages($strings['messages']);
        }

        Yii::$app->cacheLanguages->flush();

        return true;
    }

    /**
     * @param array $strings
     * [
     *   'messages' => [
     *              'id' => 12,
     *              // ...
     *          ],
     *          // ...
     * ]
     *
     * @return null | true
     */
    public function replaceStrings($strings)
    {
        $ids = $this->checkDouble($strings);
        $this->deleteMessages($ids);
        $this->insertMessages($strings['messages']);

        return true;
    }

    /**
     * @param array $strings
     * [
     *   'messages' => [
     *              'id' => 12,
     *              // ...
     *          ],
     *          // ...
     * ]
     *
     * @return array | bool
     */
    public function newStrings($strings)
    {
        $ids = $this->checkDouble($strings);
        Debugger::dump($ids);
        // добавляю только новые
        // собираю новые
        {
            $new = [];
            foreach ($strings['messages'] as $string) {
                if (!in_array($string['id'], $ids)) {
                    $new[] = $string;
                }
            }
        }
        $this->insertMessages($new);

        return ArrayHelper::getColumn($new, 'id');
        return true;
    }

    private function intersect($arr1, $arr2)
    {
        $ret = [];
        foreach ($arr1 as $id1) {
            if (in_array($id1, $arr2)) $ret[] = $id1;
        }

        return $ret;
    }

    /**
     * Удаление строк
     *
     * @param array $ids
     */
    private function deleteMessages($ids)
    {
        foreach ($ids as $id) {
            $sourceMessage = SourceMessage::findOne($id);
            if (is_null($sourceMessage)) {
                echo 'Not found id=' . $id;
                continue;
            }
            if ($sourceMessage['type'] == Category::TYPE_IMAGE) {
                $this->deleteMessagesDeleteFiles($sourceMessage);
            }
            Message::deleteAll(['id' => $id]);
            $sourceMessage->delete();
            $this->log('delete id=' . $id);
        }
    }

    /**
     * Удаляет файлы для строки
     *
     * @param \common\models\language\SourceMessage $sourceMessage
     *
     * @return bool
     */
    private function deleteMessagesDeleteFiles($sourceMessage)
    {
        $rows = ArrayHelper::map(
            Message::find()->where(['id' => $sourceMessage->id])->select(['id','language','translation'])->asArray()->all(),
            'language',
            'translation'
        );
        foreach ($rows as $language => $path) {
            $pathFull_b = Yii::getAlias(\backend\models\form\LanguageImageSave::$pathSave1 . $path);
            $info = pathinfo($pathFull_b);

            $pathFull_t = substr($pathFull_b, 0, strlen($pathFull_b) - strlen($info['extension']) - 2) . 't' . '.' . $info['extension'];
            if (file_exists($pathFull_b)) {
                unlink($pathFull_b);
            }
            if (file_exists($pathFull_t)) {
                unlink($pathFull_t);
            }
        }

        return true;
    }

    /**
     * Вставляются строки $sourceMessage, при этом считается, что таких строк нет больше
     *
     * @param $strings
     * [
     *          [
     *              'id' => 12,
     *              // ...
     *          ],
     *          // ...
     * ]
     *
     * @throws \Exception
     */
    private function insertMessages($strings)
    {
        foreach ($strings as $sourceMessage) {
            if ($sourceMessage['type'] == Category::TYPE_IMAGE) {
                $this->deleteFiles($sourceMessage);
                $this->importFiles($sourceMessage['files']);
            }
            if ($sourceMessage['type'] == Category::TYPE_FILE) {
                $this->deleteFilesFiles($sourceMessage);
                $this->importFilesFiles($sourceMessage['files']);
            }
            $translations = $sourceMessage['translations'];
            unset($sourceMessage['translations']);
            unset($sourceMessage['files']);

            $dbMessage = new \common\models\language\SourceMessage($sourceMessage);
            $dbMessage->save();
            $rows = [];
            foreach ($translations as $language => $translation) {
                $rows[] = [
                    $dbMessage->id,
                    $language,
                    $translation,
                ];
            }
            if (count($rows) > 0) {
                (new \yii\db\Query())->createCommand()->batchInsert('message', ['id', 'language', 'translation'], $rows)->execute();
            }

            $this->log('update id=' . $dbMessage->id);
        }
    }

    /**
     * Удаляет файлы для строки с типом IMAGE
     *
     * @param $sourceMessage
     *
     * @return bool
     */
    private function deleteFiles($sourceMessage)
    {
        foreach ($sourceMessage['translations'] as $language => $path) {
            $pathFull_b = Yii::getAlias(\backend\models\form\LanguageImageSave::$pathSave1 . $path);
            $info = pathinfo($pathFull_b);

            $pathFull_t = substr($pathFull_b, 0, strlen($pathFull_b) - strlen($info['extension']) - 2) . 't' . '.' . $info['extension'];
            if (file_exists($pathFull_b)) {
                unlink($pathFull_b);
            }
            if (file_exists($pathFull_t)) {
                unlink($pathFull_t);
            }
        }

        return true;
    }

    /**
     * Удаляет файлы для строки с типом FILE
     *
     * @param $sourceMessage
     *
     * @return bool
     */
    private function deleteFilesFiles($sourceMessage)
    {
        foreach ($sourceMessage['translations'] as $language => $path) {
            $pathFull_b = Yii::getAlias(\backend\models\form\LanguageImageSave::$pathSave1 . $path);

            if (file_exists($pathFull_b)) {
                unlink($pathFull_b);
            }
        }

        return true;
    }

    /**
     * Выдает список ID's из $strings которые есть уже в DB
     *
     * @param array $strings
     * [
     *   'messages' => [
     *              'id' => 12,
     *              // ...
     *          ],
     *          // ...
     * ]
     *
     * @return array
     */
    private function checkDouble($strings)
    {
        $ids = ArrayHelper::getColumn($strings['messages'], 'id');
        $idsDb = SourceMessage::find()->where(['in', 'id', $ids])->select('id')->column();

        return $idsDb;
    }

    /**
     * обновляет файлы типа IMAGE
     *
     * @param array $files
     *
     */
    private function importFiles($files)
    {
        if (isset($files)) {
            foreach ($files as $language => $filesList) {
                $this->saveFile($filesList['b'], $language);
                $this->saveFile($filesList['t'], $language);
            }
        }
    }

    /**
     * обновляет файлы типа FILE
     *
     * @param array $files
     *
     */
    private function importFilesFiles($files)
    {
        if (isset($files)) {
            foreach ($files as $language => $file) {
                $this->saveFile($file, $language);
            }
        }
    }

    /**
     * Записывает файл
     * Если файл есть то переписывает его
     * Если файл есть и не удалось его удалить то возвращает false
     *
     * @param string $language
     * @param array $file
     * [
     *      'name'      => string название файла
     *      'content'   => array
     * ]
     *
     * @return bool
     */
    private function saveFile($file, $language)
    {
        $data = $this->getFile($file, $language);
        if (file_exists($data['path'])) {
            if (!unlink($data['path'])) {
                return false;
            }
        }

        return file_put_contents($data['path'], $data['content']) === false ? false : true;
    }

    /**
     * Преобразовывает структуру данных файла
     *
     * @param string $language
     * @param array $file
     * [
     *      'name' => string название файла
     *      'content' => array
     * ]
     *
     * @return array
     * [
     *      'path' => string - полный путь
     *      'content' => string - контент
     * ]
     */
    private function getFile($file, $language)
    {
        $filePath = Yii::getAlias(\backend\models\form\LanguageImageSave::$pathSave1 . '/images/translation/' . $language);
        FileHelper::createDirectory($filePath);
        $filePath .=  '/' . $file['name'];
        $content = '';
        foreach ($file['content'] as $i) {
            $content .= $i;
        }

        return [
            'path'    => $filePath,
            'content' => base64_decode($content),
        ];
    }

    /**
     * Восстанавливает дерево
     *
     * @param $categoryTree
     * [
     *   'id' => '22',
     *   'name' => '2',
     *   'code' => 'c22',
     *   'nodes' => [
     *       [
     *           'id' => '23',
     *           'name' => '3',
     *           'code' => 'c23',
     *       ],
     *   ],
     *   'path' => [
     *       [
     *           'id' => 16,
     *           'name' => 'root',
     *           'sort_index' => null,
     *           'parent_id' => null,
     *           'code' => 'root',
     *       ],
     *       [
     *           'id' => 17,
     *           'name' => 'app',
     *           'sort_index' => null,
     *           'parent_id' => 16,
     *           'code' => 'app',
     *       ],
     *       [
     *           'id' => 21,
     *           'name' => '1',
     *           'sort_index' => null,
     *           'parent_id' => 17,
     *           'code' => 'c21',
     *       ],
     *   ],
     * ]
     *
     */
    public function restoreTree($categoryTree)
    {
        /** Удаляю категории которые ранее в БД были */
        if ($this->isReplacePath) {
            $categoryList = ArrayHelper::getColumn($categoryTree['path'], 'code');
            $new = [];
            foreach ($categoryList as $c) {
                if ($c != 'root') $new[] = $c;
            }
            $new[] = $categoryTree['code'];
            $this->log('deleting...');
            Category::deleteAll([
                'in', 'code', $new
            ]);
            $this->log(VarDumper::dumpAsString($new));
        }
        // добавляю путь
        $parent_id = Category::findOne(['code' => $categoryTree['path'][0]['code']])->id;
        for ($i = 0; $i < count($categoryTree['path']); $i++) {
            $category = $categoryTree['path'][$i];
            $categoryObject = \common\models\language\Category::findOne(['code' => $category['code']]);
            if (is_null($categoryObject)) {
                $fields = [
                    'name' => $category['name'],
                    'code' => $category['code'],
                ];
                if ($i > 0) {
                    $fields['parent_id'] = $parent_id;
                }
                $c = new \common\models\language\Category($fields);
                $c->save();
                $parent_id = $c::getDb()->lastInsertID;
                $this->log('category added. code = ' . $fields['code']);
            } else {
                $parent_id = $categoryObject->id;
            }
        }

        // корневую категорию добавляю если ее нет
        $category = $categoryTree;
        if (!\common\models\language\Category::find()->where(['code' => $category['code']])->exists()) {
            $fields = [
                'name' => $category['name'],
                'code' => $category['code'],
            ];
            if ($i > 0) {
                $fields['parent_id'] = $parent_id;
            }
            $c = new \common\models\language\Category($fields);
            $ret = $c->save();
            $this->log('category added. code = ' . $fields['code']);
        }

    }

    private function addNode($node, $parent_id)
    {
        $category = \common\models\language\Category::findOne(['code' => $node['code']]);
        if (is_null($category)) {
            $fields = [
                'name'      => $node['name'],
                'code'      => $node['code'],
                'parent_id' => $parent_id,
            ];
            (new \common\models\language\Category($fields))->save();
            $id = \common\models\language\Category::getDb()->lastInsertID;
        } else {
            $category->name = $node['name'];
            $category->save();
            $id = $category->id;
        }
        if (isset($node['nodes'])) {
            if (!empty($node['nodes'])) {
                foreach ($node['nodes'] as $n) {
                    $this->addNode($n, $id);
                }
            }
        }
    }

    private function log($string)
    {
        if ($this->isLog) {
            echo($string . "\n");
        }
    }
} 