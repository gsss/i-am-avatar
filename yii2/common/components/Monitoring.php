<?php

namespace common\components;

use Yii;
use yii\helpers\StringHelper;

/**
 * Компонент подсчитывающий колво обращений на сервер в сек
 *
 * Created by PhpStorm.
 * User: s.arhangelskiy
 * Date: 01.06.2016
 * Time: 14:41
 */
class Monitoring extends \yii\base\Component
{
    /**
     * @var string под этим ключом хранится массив значений счетчиков
     * [
     *     'time'               => time() - время начало отсчета счетчика в сек
     *     'request_per_second' => <value>,
     *     // ...
     * ]
     */
    public $cacheKey = 'monitoring';

    /**
     * @var array функции для подсчета
     * [
     *      'request_per_second' => <value>,
     * ]
     */
    public $counters;

    /**
     * [
     *      'request_per_second' => [
     *           // Возрвращает значение для события onCalculate которое попадает в БД
     *           // Возрвращает массив ['value' => <value>, 'isChange' => true]
     *           'onCalculate' => function($value) {}
     *           // Возрвращает массив ['value' => <value>, 'isChange' => true]
     *           'onBeforeRequest' => function($value) {}
     *           // Возрвращает массив ['value' => <value>, 'isChange' => true]
     *           'onAfterRequest' => function($value) {}
     *      ]
     *      // считается что это функция для события onBeforeRequest
     *      'request_per_second' => function($value) {}
     * ]
     */
    public function init()
    {
        $this->counters = [
            'request_per_second' => function($value)
            {
                $isAdmin = StringHelper::startsWith(Yii::$app->requestedRoute, 'admin');
                if ($isAdmin) {
                    return [
                        'value'    => $value,
                        'isChange' => false,
                    ];
                } else {
                    return [
                        'value'    => $value + 1,
                        'isChange' => true,
                    ];
                }
            }
        ];
    }

    public function onBeforeRequest()
    {
        $value = $this->get();
        $isChange = false;
        foreach ($this->counters as $name => $function) {
            if (!is_array($function)) {
                /**
                 * [
                 *     'value'    => int,
                 *     'isChange' => true,
                 * ]
                 */
                $ret = $function($value[$name]);
                $value[$name] = $ret['value'];
                if ($ret['isChange']) $isChange = true;
            } else {
                if (key_exists('onBeforeRequest', $function)) {
                    $function = $function['onBeforeRequest'];
                    /**
                     * [
                     *     'value'    => int,
                     *     'isChange' => true,
                     * ]
                     */
                    $ret = $function($value[$name]);
                    $value[$name] = $ret['value'];
                    if ($ret['isChange']) $isChange = true;
                }
            }
        }
        if ($isChange) $this->set($value);
    }

    public function onAfterRequest()
    {
        $value = $this->get();
        $isChange = false;
        foreach ($this->counters as $name => $function) {
            if (is_array($function)) {
                if (key_exists('onAfterRequest', $function)) {
                    $function = $function['onAfterRequest'];
                    /**
                     * [
                     *     'value'    => int,
                     *     'isChange' => true,
                     * ]
                     */
                    $ret = $function($value[$name]);
                    $value[$name] = $ret['value'];
                    if ($ret['isChange']) $isChange = true;
                }
            }
        }
        if ($isChange) $this->set($value);
    }

    /**
     * Выдает массив расчитанных счетчиков
     * Если для счетчика задана функция 'onCalculate' то она запускается для расчета
     * Иначе производится расчет <Значение счетчика> / time() - <момент начала отсчета счетчика>
     *
     * @return array
     * [
     *     'request_per_second' => <value>,
     *     // ...
     * ]
     */
    public function calculate()
    {
        $value = $this->get();
        $report = [];
        $delta = time() - $value['time'];
        foreach ($this->counters as $name => $function) {
            if (is_array($function) && \yii\helpers\ArrayHelper::keyExists('onCalculate', $function)) {
                $function = $function['onCalculate'];
                $ret = $function($value[$name]);
                $report[$name] = $ret['value'];
            } else {
                if ($delta == 0) $delta = 1;
                $report[$name] = ($value[$name] / $delta);
            }
        }

        return $report;
    }

    public function reset()
    {
        $new = [
            'time'    => time(),
        ];
        foreach ($this->counters as $name => $function) {
            $new[$name] = 0;
        }
        $this->set($new);
    }

    /**
     * Сохраняет пользователей в кеш
     *
     * @param $users
     */
    protected function set($users)
    {
        Yii::$app->cache->set($this->cacheKey, $users);
    }

    /**
     * Выдает пользователей онлайн в виде массива
     *
     * @return array
     */
    public function get()
    {
        $value = Yii::$app->cache->get($this->cacheKey);
        if ($value === false) {
            $value = [
                'time'    => time(),
            ];
            foreach ($this->counters as $name => $function) {
                $value[$name] = 0;
            }
        };

        return $value;
    }
}