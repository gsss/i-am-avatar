<?php

namespace common\widgets\FileUpload8;

use cs\Application;
use cs\services\File;
use cs\services\SitePath;

use Imagine\Image\Box;
use Yii;
use yii\base\InvalidConfigException;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\helpers\Html;
use yii\helpers\Json;
use yii\helpers\VarDumper;
use yii\widgets\InputWidget;
use yii\web\UploadedFile;
use yii\imagine\Image;
use Imagine\Image\ManipulatorInterface;
use cs\base\BaseForm;
use cs\services\UploadFolderDispatcher;

/**
 * Используется для загрузки файлов в облако, обрезки если это картинка
 */
class FileUpload extends InputWidget
{

    /** @var  array
     * - maxSize - int - кол-во КБ
     * - allowedExtensions - Array - массив рисширений которые можно грузить, по умолчанию ['jpg', 'jpeg', 'png']
     * - data - Array - массив для отправки постом при загрузке
     */
    public $settings;

    /** @var  array массив на обработку изображения */
    public $update;

    /** @var  array массив событий */
    public $events;

    /**
     * Renders the widget.
     */
    public function run()
    {
        $this->registerClientScript();
        $html = [];
        $html[] = Html::input('button', null, 'Выбери файл', [
            "class" => "btn btn-primary btn-large clearfix upload-btn buttonDragAndDrop",
        ]);
        $maxSize = ArrayHelper::getValue($this->settings, 'maxSize', 1000);
        $allowedExtensions = ArrayHelper::getValue($this->settings, 'allowedExtensions', []);
        $s = join(',', $allowedExtensions) . ' ' . '(' . $maxSize . ' Кб макс)';
        $html[] = Html::tag('span', Html::tag('i', $s), [
            "style" => "padding-left:5px;vertical-align:middle;",
        ]);
        $html[] = Html::tag('div', null, [
            "class" => "clearfix redtext errormsg",
            "style" => "padding-top: 10px;",
        ]);
        $html[] = Html::hiddenInput(Html::getInputName($this->model, $this->attribute), $this->value, ['class' => 'inputValue']);

        $html[] = Html::tag('div', null, [
            "class" => "progress-wrap pic-progress-wrap",
            "style" => "margin-top:10px;margin-bottom:10px;",
        ]);
        $v = '';
        if (!Application::isEmpty($this->value)) $v = \common\helpers\Html::tag('code', $this->value);
        $html[] = Html::tag('div', $v, [
            "data-id" => "picture",
            "class"   => "clear picbox",
            "style"   => "padding-top:0px;padding-bottom:10px;width:100%;max-width:200px",
        ]);
        $html[] = Html::tag('div', null, [
            "class" => "clear-line",
            "style" => "margin-top:10px;",
        ]);

        return Html::tag('div', join('', $html), ['class' => 'content-box', 'id' => $this->id]);
    }

    /**
     * Registers the needed JavaScript.
     */
    public function registerClientScript()
    {
        $options = [
            'selector'  => '#' . $this->id,
            'server'    => $this->getServer(),
        ];
        if ($this->update) {
            $options['data']['update'] = Json::encode($this->update);
        }
        $options = ArrayHelper::merge($options, $this->settings);
        $jsonOptions = Json::encode($options);
        $this->getView()->registerJs("FileUpload8.init({$jsonOptions});");
        Asset::register($this->getView());
    }

    /**
     * Returns the options for the captcha JS widget.
     *
     * @return array the options
     */
    protected function getClientOptions()
    {
        return [];
    }

    /**
     * Выдает сервер для загрузки файла
     *
     * @return string например https://cloud1.i-am-avatar.com
     */
    public function getServer()
    {
        /** @var \common\services\AvatarCloud $cloud */
        $cloud = Yii::$app->AvatarCloud;

        return $cloud->getServer();
    }

    /**
     * Выдает имя файла в зависимости от инлекса
     * Например если имя файла в параметре value = https://cloud1.i-am-avatar.com/15523/12918_dsffesasd.jpg
     * а параметр $index = 'crop'
     * то выдано будет  https://cloud1.i-am-avatar.com/15523/12918_dsffesasd_crop.jpg
     * То есть по сути идет лишь добавление к конец файла суфикса со знаком подчеркивания
     *
     * @param string $index
     */
    public function getFile($file, $index)
    {
        $info = pathinfo($file);
        $ext = $info['extension'];

        return $info['dirname'] . '/' . $info['filename'] . '_' . $index . '.' . $ext;
    }

    /**
     * @param array $field
     *
     * @return bool
     */
    public function onUpdate($field)
    {
        return true;
    }

    /**
     * @param array $field
     *
     * @return bool
     */
    public function onInsert($field)
    {
        return true;
    }


}
