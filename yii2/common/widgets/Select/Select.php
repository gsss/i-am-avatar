<?php

namespace common\widgets\Select;

use common\services\Security;
use Yii;
use yii\base\InvalidConfigException;
use yii\helpers\ArrayHelper;
use yii\helpers\StringHelper;
use yii\helpers\Url;
use yii\helpers\Html;
use yii\helpers\Json;
use yii\helpers\VarDumper;
use yii\widgets\InputWidget;
use yii\web\UploadedFile;
use yii\web\JsExpression;
use yii\imagine\Image;
use Imagine\Image\ManipulatorInterface;
use cs\base\BaseForm;

/**
 *
 * Использование:
 *
 * ```php
 * $form->input($model, 'name')->widget('\common\widgets\Select\Select', $options)
 * ```
 */
class Select extends InputWidget
{
    private $fieldId;
    private $fieldName;

    private $fieldIdMap;
    private $fieldNameMap;

    private $fieldIdLat;
    private $fieldNameLat;

    private $fieldIdLng;
    private $fieldNameLng;

    public $style = [
        'input'  => ['class' => 'form-control'],
        'divMap' => [],
    ];

    /** @var array Элементы id => name */
    public $items = [];

    public $options;

    public $inputOptions;

    /**
     * Initializes the widget.
     */
    public function init()
    {
        parent::init();

        $formName = $this->model->formName();
        $this->getId();

        $this->fieldId = strtolower($formName . '-' . $this->attribute);
        $this->fieldName = $formName . '[' . $this->attribute . ']';

        $this->fieldIdMap = strtolower($formName . '-' . $this->attribute . '-map');
        $this->fieldNameMap = $formName . '[' . $this->attribute . '-map' . ']';

        $this->fieldIdLng = strtolower($formName . '-' . $this->attribute . '-lng');
        $this->fieldNameLng = $formName . '[' . $this->attribute . '-lng' . ']';

        $this->fieldIdLat = strtolower($formName . '-' . $this->attribute . '-lat');
        $this->fieldNameLat = $formName . '[' . $this->attribute . '-lat' . ']';
    }

    /**
     * Renders the widget.
     */
    public function run()
    {
        $this->registerClientScript();

        if ($this->hasModel()) {
            $items = [];
            foreach ($this->items as $value => $title) {
                $items[] = Html::tag('option', $title, ['value' => $value]);
            }
            $html = Html::tag('select', join("\n", $items), ['class' => 'selectpicker']);
        } else {

        }

        return  $html;
    }

    /**
     * Registers the needed JavaScript.
     */
    public function registerClientScript()
    {
        Asset::register($this->getView());

        $id = Html::getInputId($this->model, $this->attribute);
        $optionsJson = Json::encode($this->options);

        $this->getView()->registerJs(<<<JS
$('#{$id}').selectpicker({$optionsJson});
JS
);
    }

    /**
     * Returns the options for the captcha JS widget.
     *
     * @return array the options
     */
    protected function getClientOptions()
    {
        return [];
    }

    /**
     *
     * @param array $field
     * @param \yii\base\Model $model
     *
     * @return array
     */
    public function onUpdate($field)
    {
        $fieldName = $field[ BaseForm::POS_DB_NAME ];

        $lng = self::getParam($fieldName . '-lng', $model);
        $lat = self::getParam($fieldName . '-lat', $model);
        $address = self::getParam($fieldName, $model);
        if ($lng == '') $lng = null;
        if ($lat == '') $lat = null;
        if ($address == '') $address = null;

        return [
            $fieldName . '_lat'     => $lat,
            $fieldName . '_lng'     => $lng,
            $fieldName . '_address' => $address,
        ];
    }

    /**
     *
     * @param array $field
     *
     * @return array
     */
    public function onLoad($field)
    {
        $fieldName = $field[ BaseForm::POS_DB_NAME ];

        $lng = self::getParam($fieldName . '-lng', $model);
        $lat = self::getParam($fieldName . '-lat', $model);
        $address = self::getParam($fieldName, $model);

        $model->$fieldName = [
            'lat'     => $lat,
            'lng'     => $lng,
            'address' => $address,
        ];
    }

    /**
     *
     *
     * @param array $field
     * @param \cs\base\BaseForm $model
     *
     */
    public function onLoadDb($field)
    {
        $fieldName = $field[ BaseForm::POS_DB_NAME ];
        $fields = $model->getRow();

        $lat = ArrayHelper::getValue($fields, $fieldName . '_lat', null);
        $lng = ArrayHelper::getValue($fields, $fieldName . '_lng', null);
        $address = ArrayHelper::getValue($fields, $fieldName . '_address', null);
        $model->$fieldName = [
            'lat'     => $lat,
            'lng'     => $lng,
            'address' => $address,
        ];
    }

    /**
     *
     * @param array $field
     * @param \yii\base\Model $model
     */
    public function onInsert($field)
    {
        $fieldName = $this->attribute;
        $attribute = $this->attribute;
        $model = $this->model;

        $lng = self::getParam($fieldName . '-lng', $model);
        $lat = self::getParam($fieldName . '-lat', $model);
        $address = self::getParam($fieldName, $model);
        if ($lng == '') $lng = null;
        if ($lat == '') $lat = null;
        if ($address == '') $address = null;

        $this->model->setAttributes([
            $fieldName . '_lat'     => $lat,
            $fieldName . '_lng'     => $lng,
        ]);
    }


    /**
     * Возвращает значение поля формы из поста
     *
     * @param string $fieldName
     * @param \yii\base\Model $model
     *
     * @return string
     */
    public static function getParam($fieldName, $model)
    {
        $formName = $model->formName();
        $query = $formName . '.' . $fieldName;

        return ArrayHelper::getValue(\Yii::$app->request->post(), $query, '');
    }
}
