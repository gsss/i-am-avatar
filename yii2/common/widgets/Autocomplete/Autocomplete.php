<?php

namespace common\widgets\Autocomplete;

use Yii;
use yii\base\InvalidConfigException;
use yii\base\Model;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\helpers\Html;
use yii\helpers\Json;
use yii\helpers\VarDumper;
use yii\widgets\InputWidget;
use yii\web\UploadedFile;
use yii\imagine\Image;
use Imagine\Image\ManipulatorInterface;
use cs\base\BaseForm;

/**
 */
class Autocomplete extends InputWidget
{
    public $options;
    public $clientOptions = [];

    private $attrId;
    private $attrName;
    private $attrIdText;
    private $attrNameText;

    /**
     * Initializes the widget.
     */
    public function init()
    {
        parent::init();

        unset($this->options['id']);
        $this->attrId = strtolower($this->model->formName() . '-' . $this->attribute);
        $this->attrName = $this->model->formName() . '[' . $this->attribute . ']';
        $this->attrIdText = strtolower($this->model->formName() . '-' . $this->attribute . '_text');
        $this->attrNameText = $this->model->formName() . '[' . $this->attribute . '_text' . ']';
    }

    /**
     * Renders the widget.
     */
    public function run()
    {
        $html = [];

        $html[] = Html::hiddenInput($this->attrName, null, ['id' => $this->attrId]);
        $options = [
            'class'       => 'form-control',
        ];
        $options = ArrayHelper::merge($options, $this->options);
        $clientOptions = ArrayHelper::merge($this->getClientOptions(), $this->clientOptions);
        $html[] = \yii\jui\AutoComplete::widget([
            'id'            => $this->attrIdText,
            'clientOptions' => $clientOptions,
            'options'       => $options,
        ]);

        return Html::tag(
            'div',
            join('', $html),
            ['style' => 'display:block;']
        );
    }

    /**
     * Registers the needed JavaScript.
     */
    public function registerClientScript()
    {
    }

    /**
     * @return array the options
     */
    protected function getClientOptions()
    {
        return [
            'select' => new \yii\web\JsExpression(<<<JS
function(event, ui) {
    $('#{$this->attrId}').val(ui.item.id);
    $('#{$this->attrIdText}').val(ui.item.label);
    
    return false;
}
JS
            )
        ];
    }

    /**
     * Здесь до выполнения функции находится значение из БД
     * а после выполнения то, которое используется в виджете
     * Берет значения из POST и возвраает знаяения для добавления в БД
     *
     * @param array $field
     *
     * @return array
     */
    public function onLoad($field)
    {

    }

    /**
     * Здесь до выполнения функции находится значение из БД
     * а после выполнения то, которое используется в виджете
     *
     * @param array $field
     *
     * @return array поля для обновления в БД
     */
    public function onLoadDb($field)
    {

    }

    /**
     * Берет значения из данных виджета и возвраает значения для добавления в БД
     *
     * @param array $field
     */
    public function onUpdate($field)
    {

    }
}
