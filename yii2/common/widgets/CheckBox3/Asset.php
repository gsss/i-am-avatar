<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace common\widgets\CheckBox3;

use yii\web\AssetBundle;

/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class Asset extends AssetBundle
{
    public $css = [
        '/admin/vendors/switchery/dist/switchery.min.css'
    ];

    public $js = [
        '/admin/vendors/switchery/dist/switchery.min.js',
    ];

    public $depends = [
        'yii\web\JqueryAsset',
    ];
}
