<?php

namespace common\widgets\CheckBox3;

use Yii;
use yii\base\InvalidConfigException;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\helpers\Html;
use yii\helpers\Json;
use yii\helpers\VarDumper;
use yii\widgets\InputWidget;
use yii\web\UploadedFile;
use yii\imagine\Image;
use Imagine\Image\ManipulatorInterface;
use cs\base\BaseForm;

/**
 * http://www.bootstraptoggle.com
 *
 * Виджет представляющий значене bool хранящийся в БД
 * Я имею дело с двумя значениями
 * 1. В ActionRecord       значение в модели
 * 2. В widget поле value  значение в виджете
 *
 * Какие есть состояния и события
 * 1. Загрузили из БД
 * 2. onLoadDb
 * 3. onUpdate
 *
 * 1. Загрузили из POST
 * 2. onLoad
 * 3. onUpdate
 *
 * Значение после load
 * - true - да
 * - false - нет
 *
 * Значение которое записывается в БД
 * - 1 - да
 * - 0 - нет
 *
 */
class CheckBox extends InputWidget
{
    /**
     * @var array the HTML attributes for the input tag.
     * @see \yii\helpers\Html::renderTagAttributes() for details on how attributes are being rendered.
     */
    public $options = ['class' => 'js-switch'];

    private $attrId;
    private $attrName;

    /**
     * Initializes the widget.
     */
    public function init()
    {
        parent::init();

        $this->attrId = strtolower($this->model->formName() . '-' . $this->attribute);
        $this->attrName = $this->model->formName() . '[' . $this->attribute . ']';
    }

    /**
     * Renders the widget.
     */
    public function run()
    {
        $this->registerClientScript();
        $options = $this->options;
        if (isset($options['class'])) {
            if (strlen($options['class']) > 0) {
                $options['class'] = $options['class'] . ' ' . 'js-switch';
            } else {
                $options['class'] = 'js-switch';
            }
        } else {
            $options['class'] = 'js-switch';
        }
        $attribute = $this->attribute;
        if ($this->model->$attribute) $options['checked'] = 'checked';

        return Html::tag('div',
            Html::tag(
                'div',
                Html::input(
                    'checkbox',
                    $this->attrName,
                    1,
                    $options),
                ['class' => 'col-lg-12']
            ),
            ['class' => 'row']
        );

    }

    /**
     * Registers the needed JavaScript.
     */
    public function registerClientScript()
    {
        \common\widgets\CheckBox3\Asset::register(Yii::$app->view);
    }

    /**
     * @return array the options
     */
    protected function getClientOptions()
    {
        return [];
    }

}
