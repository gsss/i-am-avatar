<?php
/**
 * @link      http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license   http://www.yiiframework.com/license/
 */

namespace common\widgets\MenuCompany;

use cs\base\BaseForm;
use cs\services\VarDumper;
use Yii;
use yii\base\InvalidParamException;
use yii\base\Widget;
use yii\helpers\ArrayHelper;
use yii\helpers\FormatConverter;
use yii\helpers\Html;
use yii\helpers\Json;
use yii\helpers\Url;
use yii\jui\JuiAsset;

/**
 */
class MenuCompany extends Widget
{
    /**
     * @var array пункты меню
     */
    public $items;


    /**
     * Renders the widget.
     */
    public function run()
    {
        return $this->view->renderFile('@common/widgets/MenuCompany/view.php', ['items' => $this->items]);
    }

    public static function isActive($item1)
    {
        foreach ($item1['items'] as $item) {
            $route = Yii::$app->requestedRoute;
            $uriCurrent = Url::current();
            if (\yii\helpers\ArrayHelper::keyExists('urlList', $item)) {
                foreach ($item['urlList'] as $i) {
                    switch ($i[0]) {
                        case 'route':
                            if ($route == $i[1]) return true;
                            break;
                        case 'startsWith':
                            if (\yii\helpers\StringHelper::startsWith($uriCurrent, $i[1])) return true;
                            break;
                        case 'controller':
                            $arr = explode('/', $route);
                            if ($arr[0] == $i[1]) return true;
                            break;
                    }
                }
            }
        }

        return false;
    }

    public static function isActive2($item)
    {
        $route = Yii::$app->requestedRoute;
        $uriCurrent = Url::current();
        if (\yii\helpers\ArrayHelper::keyExists('urlList', $item)) {
            foreach ($item['urlList'] as $i) {
                switch ($i[0]) {
                    case 'route':
                        if ($route == $i[1]) return true;
                        break;
                    case 'startsWith':
                        if (\yii\helpers\StringHelper::startsWith($uriCurrent, $i[1])) return true;
                        break;
                    case 'controller':
                        $arr = explode('/', $route);
                        if ($arr[0] == $i[1]) return true;
                        break;
                }
            }
        }

        return false;
    }

}
