<?php
/**
 * Created by PhpStorm.
 * User: ramha
 * Date: 18.12.2020
 * Time: 10:22
 */

/** @var $items array */
use yii\helpers\Url;
?>
<?php foreach ($items as $i2) { ?>
<div class="menu_section">
    <ul class="nav side-menu">
        <?php foreach ($i2['items'] as $i) { ?>
            <?php
            if (\common\widgets\MenuCompany\MenuCompany::isActive($i)) {
                $suffix = ' class="active"';
                $suffix2 = ' style="display:block"';
            } else {
                $suffix = '';
                $suffix2 = ' style="display:none"';
            }
            ?>
            <li<?= $suffix ?>><a><i class="<?= $i['icon'] ?>"></i> <?= $i['title'] ?> <span class="fa fa-chevron-down"></span></a>
                <ul class="nav child_menu"<?= $suffix2 ?>>
                    <?php foreach ($i['items'] as $i1) { ?>
                        <?php
                        if (\common\widgets\MenuCompany\MenuCompany::isActive2($i1)) {
                            $suffix3 = ' class="active"';
                        } else {
                            $suffix3 = '';
                        }
                        if (isset($i1['url'])) {
                            $url = Url::to($i1['url']);
                        } else {
                            $url = '#';
                        }
                        ?>
                        <li<?= $suffix3 ?>><a href="<?= $url ?>"><?= $i1['title'] ?></a></li>
                    <?php } ?>
                </ul>
            </li>
        <?php } ?>
    </ul>
</div>
<?php } ?>
