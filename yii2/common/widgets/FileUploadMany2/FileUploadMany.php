<?php

namespace common\widgets\FileUploadMany2;

use Yii;
use yii\db\Query;
use yii\helpers\ArrayHelper;
use yii\helpers\StringHelper;
use yii\helpers\Url;
use yii\helpers\Html;
use yii\helpers\Json;
use Imagine\Image\ManipulatorInterface;
use cs\base\BaseForm;
use cs\services\SitePath;
use yii\jui\InputWidget;

/**
 * Класс FileUploadMany
 *
 * Виджет который загружает файлы по несколько штук
 * Если загружаются картинки то они показываются в предпросмотре
 * и обрезаются по размеру указанному в параметре small
 *
 * Максимальный размер загружаемого файла по умолчанию устанавливается равный тому который указан в параметре ini.php upload_max_filesize
 *
 *
 *
 * $field->widget('common\widgets\FileUploadMany2\FileUploadMany', [
 *
 * ]);
 *
 * $model->$fieldName = [
 *      ['file_path', 'file_name'],
 * ];
 */
class FileUploadMany extends InputWidget
{
    const MODE_THUMBNAIL_INSET    = ManipulatorInterface::THUMBNAIL_INSET;
    const MODE_THUMBNAIL_OUTBOUND = ManipulatorInterface::THUMBNAIL_OUTBOUND;

    /** @var string путь загрузки файлов, может быть только в пределах @webroot и начинается строка с '/' */
    public static $uploadDirectory = 'FileUploadMany2';
    public static $tableNameFiles = 'widget_uploader_many';
    public static $tableNameFields = 'widget_uploader_many_fields';

    public $uploadUrl = '/upload2/upload';

    /**
     * @var string the template for arranging the CAPTCHA image tag and the text input tag.
     * In this template, the token `{image}` will be replaced with the actual image tag,
     * while `{input}` will be replaced with the text input tag.
     */
    public $template = "<div class='upload'>\n{image}\n{checkbox}\n{input}\n</div>";

    /**
     * @var array the HTML attributes for the input tag.
     * @see \yii\helpers\Html::renderTagAttributes() for details on how attributes are being rendered.
     */
    public $options = [];

    public $small;

    private $fieldId;
    private $fieldName;
    private $hiddenId;
    private $hiddenName;

    /**
     * Initializes the widget.
     */
    public function init()
    {
        parent::init();

        $this->fieldId = strtolower($this->model->formName() . '-' . $this->attribute);
        $this->fieldName = $this->model->formName() . '[' . $this->attribute . ']';
        $this->hiddenId = strtolower($this->model->formName() . '-' . $this->attribute . '-files');
        $this->hiddenName = $this->model->formName() . '[' . $this->attribute . '-files' . ']';
    }

    /**
     * рисует виджет
     */
    public function run()
    {
        $this->registerClientScript();

        if ($this->hasModel()) {
            $attribute = $this->attribute;

            $files = $this->model->$attribute;
            if (is_null($files)) $files = [];
            $this->clientOptions['files'] = $files;
            $c1 = Html::hiddenInput($this->hiddenName, json_encode($this->clientOptions['files'], JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE), ['id' => $this->hiddenId]);
            $c2 = Html::fileInput($this->fieldName, null, ['id' => $this->fieldId]);

            return Html::tag('div', $c1 . $c2, ['class' => 'multifile_upload']);
        }
    }

    /**
     * Registers the needed JavaScript.
     */
    public function registerClientScript()
    {
        Asset::register($this->view);
        $this->clientOptions = ArrayHelper::merge($this->getClientOptions(), $this->clientOptions);
        $options = Json::encode($this->clientOptions);
        $js = <<<JSSSS
FileUploadMany2.init('#{$this->fieldId}', {$options});
JSSSS;
        $this->getView()->registerJs($js);
    }

    /**
     * Возвращает опции для виджета
     *
     * @return array the options
     */
    protected function getClientOptions()
    {
        return [
            'url'         => $this->uploadUrl,
            'maxFileSize' => self::getUploadMaxFileSize(),
            'formData' => [
                'table'       => $this->model->tableName(),
                'field'       => $this->attribute,
            ],
        ];
    }

    /**
     * Возвращает максимально возможно загружаемый файл который установлен в настройках PHP
     *
     * @return int в байтах
     */
    private static function getUploadMaxFileSize()
    {
        $maxFileSize = ini_get('upload_max_filesize');

        return (int)substr($maxFileSize, 0, strlen($maxFileSize) - 1) * 1024 * 1024;
    }

    /**
     * @param array $field
     * @param \yii\base\Model $model
     *
     * @return array поля для обновления в БД
     */
    public static function onLoad($field, $model)
    {
        $fieldName = $field[BaseForm::POS_DB_NAME];
        $post = Yii::$app->request->post();
        $query = $model->formName() . '.' . $fieldName . '-files';
        $filesString = ArrayHelper::getValue($post, $query, '');
        $model->$fieldName = json_decode($filesString);
    }

    /**
     *
     *
     * @param array $field
     * @param \cs\base\BaseForm $model
     *
     * @return array поля для обновления в БД
     */
    public static function onLoadDb($field, $model)
    {
        $fieldName = $field[BaseForm::POS_DB_NAME];
        $files = (new Query())
            ->select([
                'widget_uploader_many.file_path',
                'widget_uploader_many.file_name',
            ])
            ->from('widget_uploader_many')
            ->innerJoin('widget_uploader_many_fields', 'widget_uploader_many.field_id = widget_uploader_many_fields.id')
            ->where([
                'widget_uploader_many.row_id'            => $model->id,
                'widget_uploader_many_fields.table_name' => $model->getTableName(),
                'widget_uploader_many_fields.field_name' => $fieldName
            ])
            ->all();
        $rows = [];
        foreach ($files as $file) {
            $rows[] = [$file['file_path'], $file['file_name']];
        }

        $model->$fieldName = $rows;
    }

    /**
     * Обновляет
     *
     * @param array $field
     *
     * @return array поля для обновления в БД
     */
    public function onUpdate($field)
    {
        /** @var \yii\db\ActiveRecord $model */
        $model = $this->model;
        $fieldName = $this->attribute;
        $files = $model->$fieldName;
        $tableName = $model->tableName();

        $fieldDb = new ModelFields([
            'table_name' => $tableName,
            'field_name' => $fieldName,
        ]);
        $fieldDb->save();

        ModelFiles::deleteAll([
            'row_id' => $fieldDb->id,
        ]);
        $folder = \cs\services\UploadFolderDispatcher::createFolder('FileUploadMany2', $fieldDb->id, $model->id);
        $allFiles = [];
        foreach ($files as $file) {
            $allFiles[] = $file[0];
        }
        $currentFiles = ModelFiles::find()
            ->select('widget_uploader_many.file_path, widget_uploader_many.id')
            ->innerJoin('widget_uploader_many_fields', 'widget_uploader_many.field_id = widget_uploader_many_fields.id')
            ->where([
                'widget_uploader_many.row_id'            => $model->id,
                'widget_uploader_many_fields.table_name' => $model->tableName(),
                'widget_uploader_many_fields.field_name' => $fieldName
            ])
            ->all();
        foreach ($currentFiles as $currentFile) {
            if (!in_array($currentFile['file_path'], $allFiles)) {
                (new SitePath($currentFile['file_path']))->deleteFile();
                $id = (int)$currentFile['id'];
                ModelFiles::deleteAll(['id' => $id]);
            }
        }
        foreach ($files as $file) {
            if (StringHelper::startsWith($file[0], '/')) {
                continue;
            }
            $sourcePathFull = Yii::getAlias(self::$uploadDirectory . '/' . $file[0]);
            $destinationFile = $folder->cloneObject($file[0]);
            copy($sourcePathFull, $destinationFile->getPathFull());
            (new ModelFiles([
                'file_path' => $destinationFile->getPath(),
                'file_name' => $file[1],
                'row_id'    => $model->id,
                'field_id'  => $fieldDb->id,
                'datetime'  => gmdate('YmdHis'),
            ]))->save();
        }

        $this->model->$fieldName = count($files);
    }

    /**
     * Возвращает ссылку для скачивания файла
     *
     * @return string
     */
    public static function getDownloadLink($id)
    {
        return 'upload2/download/' . $id;
    }

    /**
     * Удаляет
     *
     * @param array $field
     *
     * @return string
     */
    public function onDelete($field)
    {
        $fieldName = $this->attribute;
        $fieldId = self::getFieldId($this->model->tableName(), $fieldName);
        $files = (new Query())->select('file_path, id')->from(self::$tableNameFiles)->where([
            'row_id'   => $this->model->id,
            'field_id' => $fieldId,
        ])->all();

        foreach ($files as $file) {
            (new SitePath($file))->deleteFile();
        }
    }

    /**
     * Выдает список файлов в поле
     *
     * @param string $tableName название таблицы
     * @param string $fieldName название поля
     * @param int $rowId идентификатор строки
     * @param string $serverName имя сервера для ссылок файлов, если задано то оно будет прибавлено к именам в формате "//{$serverName}/ss/..."
     *
     * @return array
     * [[
     *    'id'        => int
     *    'file_path' => str
     *    'file_name' => str
     *    'datetime'  => str
     * ], ...]
     */
    public static function getFiles($tableName, $fieldName, $rowId, $serverName = null)
    {
        $fieldId = self::getFieldId($tableName, $fieldName);
        if ($fieldId === false) return [];

        $select = [
            'id',
            'file_name',
            'UNIX_TIMESTAMP(datetime) as datetime'
        ];
        if (is_null($serverName)) {
            $select[] = 'file_path';
        } else {
            $select[] = "concat('//{$serverName}',`file_path`) as file_path";
        }

        return (new Query())->select($select)->from(self::$tableNameFiles)->where([
            'row_id'   => $rowId,
            'field_id' => $fieldId,
        ])->orderBy('datetime DESC')->all();
    }

    /**
     * Возвращает FieldId
     *
     * @param $tableName
     * @param $fieldName
     *
     * @return false|int
     */
    public static function getFieldId($tableName, $fieldName)
    {
        return (new Query())->select('id')->from(self::$tableNameFields)->where([
            'table_name' => $tableName,
            'field_name' => $fieldName,
        ])->scalar();
    }
}
