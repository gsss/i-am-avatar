<?php

/**
 * table
 * widget_uploader_many_fields
 * id
 * table_name
 * field_name
 */

namespace common\widgets\FileUploadMany2;

use yii\db\ActiveRecord;

class ModelFields extends ActiveRecord
{
    public static function tableName()
    {
        return 'widget_uploader_many_fields';
    }
}