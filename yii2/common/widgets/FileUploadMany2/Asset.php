<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace common\widgets\FileUploadMany2;

use yii\web\AssetBundle;

/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class Asset extends AssetBundle
{
    public $sourcePath = '@common/widgets/FileUploadMany2/assets';
	public $css = [
        'uploadfile.css',
    ];
    public $js = [
        'jquery.uploadfile.js',
        'handlers.js',
    ];
    public $depends = [
        'yii\web\JqueryAsset',
        'cs\assets\JqueryForm\Asset',
    ];
}
