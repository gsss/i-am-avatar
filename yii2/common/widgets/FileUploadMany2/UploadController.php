<?php

namespace common\widgets\FileUploadMany2;

use cs\services\UploadFolderDispatcher;
use Imagine\Image\Box;
use Yii;
use yii\db\Query;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;
use yii\helpers\VarDumper;
use yii\helpers\Url;
use yii\helpers\FileHelper;
use yii\imagine\Image;
use yii\web\Controller;
use yii\widgets\ActiveForm;
use yii\web\Request;
use yii\web\Response;
use cs\base\BaseController;
use cs\services\Security;

/*
'upload/upload'                         => 'upload/upload',
*/

class UploadController extends BaseController
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => [
                            '@',
                            '?'
                        ],
                    ],
                ],
            ],
        ];
    }

    /**
     * Закачивает файл `$_FILES['file']`
     *
     * @return string JSON Response
     *                [[
     *                    $tempUploadedFileName,
     *                    $fileName
     *                ]]
     */
    public function actionUpload()
    {
        $post = Yii::$app->request->post();
        $table = ArrayHelper::getValue($post, 'table', '');
        $field = ArrayHelper::getValue($post, 'field', '');

        $path = UploadFolderDispatcher::createFolder(FileUploadMany::$uploadDirectory);
        if ($table) $path->addAndCreate($table);
        if ($field) $path->addAndCreate($field);
        $pathOriginal = $path->cloneObject();

        if (isset($_FILES['file'])) {
            $file = $_FILES['file'];
            $ret = [];

            $error = $file['error'];
            //You need to handle  both cases
            //If Any browser does not support serializing of multiple files using FormData()
            if (!is_array($file['name'])) //single file
            {
                // сохраняю оригинал
                $fileNameOriginal = UploadFolderDispatcher::generateFileName($file['name']);
                $pathOriginal->add($fileNameOriginal);
                copy($file['tmp_name'], $pathOriginal->getPathFull());

                $fileName = UploadFolderDispatcher::generateFileName($file['name']);
                $path->add($fileName);
                $image = Image::getImagine()->open($file['tmp_name']);
                $image->thumbnail(new Box(100, 100), \Imagine\Image\ManipulatorInterface::THUMBNAIL_OUTBOUND)->save($path->getPathFull(), []);
                if (file_exists($file['tmp_name'])) unlink($file['tmp_name']);

                $ret[] = [
                    $path->getPath(),
                    $file['name'],
                    $pathOriginal->getPath(),
                ];
            }
            else  //Multiple files, file[]
            {
            }

            return self::jsonSuccess($ret);
        }
    }
}