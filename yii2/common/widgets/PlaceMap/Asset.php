<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace common\widgets\PlaceMap;

use yii\web\AssetBundle;

/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class Asset extends AssetBundle
{
    public $sourcePath = '@common/widgets/PlaceMap/assets';
    public $css = [
        'default.css',
    ];
    public $js = [
        'handlers.js',
    ];
    public $depends = [
        '\yii\web\JqueryAsset',
        '\common\assets\GoogleMaps',
    ];
}
