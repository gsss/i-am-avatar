<?php

namespace common\widgets\CheckBox2;

use Yii;
use yii\base\InvalidConfigException;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\helpers\Html;
use yii\helpers\Json;
use yii\helpers\VarDumper;
use yii\widgets\InputWidget;
use yii\web\UploadedFile;
use yii\imagine\Image;
use Imagine\Image\ManipulatorInterface;
use cs\base\BaseForm;

/**
 * http://www.bootstraptoggle.com
 *
 * Виджет представляющий значене bool хранящийся в БД
 * Я имею дело с двумя значениями
 * 1. В ActionRecord       значение в модели
 * 2. В widget поле value  значение в виджете
 *
 * Какие есть состояния и события
 * 1. Загрузили из БД
 * 2. onLoadDb
 * 3. onUpdate
 *
 * 1. Загрузили из POST
 * 2. onLoad
 * 3. onUpdate
 *
 * Значение после load
 * - true - да
 * - false - нет
 *
 * Значение которое записывается в БД
 * - 1 - да
 * - 0 - нет
 *
 */
class CheckBox extends InputWidget
{
    /**
     * @var array the HTML attributes for the input tag.
     * @see \yii\helpers\Html::renderTagAttributes() for details on how attributes are being rendered.
     */
    public $options = [];

    private $attrId;
    private $attrName;

    public $textOn;
    public $textOff;

    /**
     * Initializes the widget.
     */
    public function init()
    {
        parent::init();

        $this->textOn = \Yii::t('c.hnZFA1VWWN', 'Да');
        $this->textOff = \Yii::t('c.hnZFA1VWWN', 'Нет');
        $this->attrId = strtolower($this->model->formName() . '-' . $this->attribute);
        $this->attrName = $this->model->formName() . '[' . $this->attribute . ']';
    }

    /**
     * Renders the widget.
     */
    public function run()
    {
        $this->registerClientScript();
        $options = ArrayHelper::merge($this->options, [
            'data-toggle' => 'toggle',
        ]);
        $attribute = $this->attribute;
        if ($this->model->$attribute) $options['checked'] = 'checked';
        if ($this->textOn) $options['data-on'] = $this->textOn;
        if ($this->textOff) $options['data-off'] = $this->textOff;

        return Html::tag('div',
            Html::hiddenInput($this->attrName, 0) .
            Html::input('checkbox', $this->attrName, 1, $options)
            , ['style' => 'display:block;'] );
    }

    /**
     * Registers the needed JavaScript.
     */
    public function registerClientScript()
    {
        \cs\assets\CheckBox\Asset::register(Yii::$app->view);
    }

    /**
     * @return array the options
     */
    protected function getClientOptions()
    {
        return [];
    }

    /**
     * Здесь до выполнения функции находится значение из БД
     * а после выполнения то, которое используется в виджете
     * Берет значения из POST и возвраает знаяения для добавления в БД
     *
     * @param array           $field
     *
     * @return array
     */
    public function onLoad($field)
    {
        $attribute = $this->attribute;
        $formName = $this->model->formName();
        $formValues = Yii::$app->request->post($formName);
        if (isset($formValues[$attribute])) {
            $value = ($formValues[$attribute] == 1);
        } else {
            $value = false;
        }
        $this->model->setAttributes([
            $attribute => $value
        ]);
    }

    /**
     * Здесь до выполнения функции находится значение из БД
     * а после выполнения то, которое используется в виджете
     *
     * @param array             $field
     *
     * @return array поля для обновления в БД
     */
    public function onLoadDb($field)
    {
        if (!is_null($this->value)) {
            if ($this->value == 1) $this->value = true;
            else $this->value = false;
        } else {
            $this->value = false;
        }
        $attribute = $this->attribute;
        $this->model->setAttributes([$attribute => $this->value]);
    }

    /**
     * Берет значения из данных виджета и возвраает значения для добавления в БД
     *
     * @param array           $field
     */
    public function onUpdate($field)
    {
        $attribute = $this->attribute;
        if ($this->value) $v = 1;
        else $v = 0;
        $this->model->setAttributes([$attribute => $v]);
    }
}
