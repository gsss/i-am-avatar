<?php

namespace common\widgets\DocumentUpload;

use cs\services\File;
use cs\services\SitePath;

use Imagine\Image\Box;
use Yii;
use yii\base\InvalidConfigException;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\helpers\Html;
use yii\helpers\Json;
use yii\helpers\VarDumper;
use yii\widgets\InputWidget;
use yii\web\UploadedFile;
use yii\imagine\Image;
use Imagine\Image\ManipulatorInterface;
use cs\base\BaseForm;
use cs\services\UploadFolderDispatcher;

/**
 * Виджет который загружает файл по указанному с компьютера или указзаному по ссылке
 * Если указаны оба то приоритет отдается указанному с компьютера.
 *
 * По умолчанию в поле хранится массив
 * [
 *     'file' => null // предназначен для загрузки онлайн
 *     'url'  => '' // предназначено для загрузки онлайн
 *     'db'  => '' // предназначено для загрузки из бд
 * ]
 * Если есть значение
 * [
 *     'file' => \yii\web\UploadedFile
 *     'url'  => 'http://ic.pics.livejournal.com/digitall_angell/38000872/940280/940280_original.jpg'
 * ]
 *
 * ```php
 * $this->options = [
 * ]
 * ```
 *
 * @property \cs\base\FormActiveRecord $model
 *
 */
class DocumentUpload extends InputWidget
{

    public $template = '@common/widgets/DocumentUpload/template';

    /**
     * @var array опции виджета
     */
    public $options;

    /**
     * @var bool
     * если true то для оригинальной картинки будет использоваться дополнительное поле {attribute}_original,
     * то есть функция onUpdate будет возвращать два поля
     */
    public $isUseDbFieldForOriginal;

    private $fieldIdName;
    private $actionIdName;
    private $actionNameAttribute;
    private $isDeleteNameAttribute;
    private $isDeleteIdName;
    private $valueNameAttribute;
    private $valueIdName;

    private $attrId;
    private $attrName;

    public $widgetOptions;
    public $isExpandSmall = true;
    public $value = [
        'file' => null,
        'url'  => '',
        'db'   => '',
    ];

    /**
     * Initializes the widget.
     */
    public function init()
    {
        parent::init();

        $this->attrName = $this->model->formName() . '[' . $this->attribute . ']';
        $this->attrId = strtolower($this->model->formName() . '-' . $this->attribute);

        $this->valueNameAttribute = $this->model->formName() . '[' . $this->attribute . '-value' . ']';
        $this->valueIdName = strtolower($this->model->formName() . '-' . $this->attribute) . '-value';

        $this->actionNameAttribute = $this->model->formName() . '[' . $this->attribute . '-action' . ']';
        $this->actionIdName = strtolower($this->model->formName() . '-' . $this->attribute) . '-action';

        $this->fieldIdName = strtolower($this->model->formName() . '-' . $this->attribute);

        $this->isDeleteNameAttribute = $this->model->formName() . '[' . $this->attribute . '-is-delete' . ']';
        $this->isDeleteIdName = strtolower($this->model->formName() . '-' . $this->attribute) . '-is-delete';

    }

    /**
     * Renders the widget.
     */
    public function run()
    {
        $this->registerClientScript();
        if ($this->hasModel()) {
            return $this->render($this->template, [
                'value'         => $this->value,
                'attribute'     => $this->attribute,
                'formName'      => $this->model->formName(),
                'model'         => $this->model,
                'attrId'        => $this->attrId,
                'attrName'      => $this->attrName,
                'widgetOptions' => $this->widgetOptions,
            ]);
        }
    }

    /**
     * Registers the needed JavaScript.
     */
    public function registerClientScript()
    {
        $this->getView()->registerJs("DocumentUpload.init('#{$this->attrId}');");
        Asset::register($this->getView());
    }

    /**
     * Returns the options for the captcha JS widget.
     *
     * @return array the options
     */
    protected function getClientOptions()
    {
        return [];
    }

    /**
     * @param array           $field
     *
     * @return bool
     */
    public function onDelete($field)
    {
        $fieldName = $field[BaseForm::POS_DB_NAME];
        $value = $this->model->$fieldName;
        $value = $value['value'];
        if (!is_null($value)) {
            if ($value != '') {
                (new SitePath($value))->deleteFile();
            }
        }

        return true;
    }

    /**
     * @param array           $field
     *
     * @return bool
     */
    public function onLoad($field)
    {
        $attribute = $this->attribute;
        $fieldName = $this->attribute;
        $modelName = $this->model->formName();
        $valueUrl = ArrayHelper::getValue(Yii::$app->request->post(), $modelName . '.'  . $attribute . '-url', '');
        $value = ArrayHelper::getValue(Yii::$app->request->post(), $modelName . '.'  . $attribute . '-value', '');
        $this->value = [
            'url'   => $valueUrl,
            'file'  => UploadedFile::getInstance($this->model, $fieldName),
            'value' => $value,
        ];
        $this->model->$fieldName = $this->value;

        return true;
    }

    /**
     * @param array             $field
     *
     * @return bool
     */
    public function onLoadDb($field)
    {
        $attribute = $this->attribute;
        $value = [
            'url'    => '',
            'file'   => null,
            'value'  => $this->value,
        ];
        $this->value = $value;
        $this->model->$attribute = $value;

        return true;
    }

    /**
     * @param array             $field
     *
     * @return bool
     */
    public function onUpdate($field)
    {
        $attribute = $this->attribute;
        $v = $this->value;
        if (is_null($v)) {
            return true;
        }
        /** @var \cs\base\FormActiveRecord $model */
        $model = $this->model;

        $isDelete = false;
        $hasFile = false;
        $hasUrl = false;
        if (ArrayHelper::getValue(Yii::$app->request->post(), $model->formName() . '.' . $attribute . '-is_delete', 0) == 1) {
            $isDelete = true;
        }
        /** @var \yii\web\UploadedFile $fileModel */
        $fileModel = $v['file'];
        if (!is_null($fileModel)) {
            if ($fileModel->error == 0) $hasFile = true;
        }
        if ($v['url'] != '') {
            $hasUrl = true;
        }

        $choose = '';
        if ($isDelete == false && $hasUrl == false && $hasFile == true) {
            $choose = 'file';
        }
        if ($isDelete == false && $hasUrl == true && $hasFile == false) {
            $choose = 'url';
        }
        if ($isDelete == false && $hasUrl == true && $hasFile == true) {
            $choose = 'file';
        }
        if ($isDelete == true && $hasUrl == false && $hasFile == false) {
            $choose = 'delete';
        }
        if ($isDelete == true && $hasUrl == false && $hasFile == true) {
            $choose = 'file';
        }
        if ($isDelete == true && $hasUrl == true && $hasFile == false) {
            $choose = 'url';
        }
        if ($isDelete == true && $hasUrl == true && $hasFile == true) {
            $choose = 'file';
        }
        if ($isDelete == false && $hasUrl == false && $hasFile == true) {
            $choose = 'file';
        }
        if ($isDelete == false && $hasUrl == true && $hasFile == false) {
            $choose = 'url';
        }
        if ($isDelete == false && $hasUrl == true && $hasFile == true) {
            $choose = 'file';
        }

        if ($choose == 'delete') {
            $dbValue = $model->getOldAttribute($attribute);
            if ($dbValue != '') {
                // удалить старые файлы
                $f = new SitePath($dbValue);
                $f->deleteFile();
            }
            $this->model->setAttributes([
                $attribute => '',
            ]);

            return true;
        }
        if ($choose == 'url') {
            try {
                $data = file_get_contents($v['url']);
            } catch(\Exception $e) {
                $data = '';
            }
            if ($data == '') {
                $this->model->setAttributes([
                    $attribute => '',
                ]);
                return true;
            }
            $file = File::content($data);
            $url = new \cs\services\Url($v['url']);
            $extension = $url->getExtension();

            return $this->save($file, $extension, $field);
        }
        if ($choose == 'file') {
            $file = File::path($fileModel->tempName);
            $extension = $fileModel->extension;

            return $this->save($file, $extension, $field);
        }
        if ($choose == '') {
            $dbValue = $model->getOldAttribute($attribute);
            $this->model->setAttributes([
                $attribute => $dbValue,
            ]);

            return true;
        }

        return false;
    }

    /**
     * Сохраняет файл
     *
     * @param \cs\services\File $file
     * @param string            $extension
     * @param array             $field
     *
     * @return bool
     */
    public function save($file, $extension, $field)
    {
        /** @var \cs\base\FormActiveRecord $model */
        $model = $this->model;
        $attribute = $this->attribute;
        $fieldName = $attribute;
        $path = $this->getFolderPath($field);
        $path->add($this->attribute . '.' . $extension);
        $file->save($path->getPathFull());
        $model->setAttributes([
            $fieldName => $path->getPath()
        ]);
    }

    /**
     * Создает папку для загрузки
     *
     * @param array             $field
     *
     * @return \cs\services\SitePath
     */
    protected function getFolderPath($field)
    {
        $model = $this->model;

        return UploadFolderDispatcher::createFolder('DocumentUpload', $model->tableName(), $model->id);
    }

}
