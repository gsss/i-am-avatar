<?php

namespace common\widgets\FileUpload6;

use cs\services\File;
use cs\services\SitePath;

use Imagine\Image\Box;
use Yii;
use yii\base\InvalidConfigException;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\helpers\Html;
use yii\helpers\Json;
use yii\helpers\VarDumper;
use yii\widgets\InputWidget;
use yii\web\UploadedFile;
use yii\imagine\Image;
use Imagine\Image\ManipulatorInterface;
use cs\base\BaseForm;
use cs\services\UploadFolderDispatcher;

/**
 *
 */
class FileUpload extends InputWidget
{
    public $school_id;
    /**
     * Renders the widget.
     */
    public function run()
    {
        $this->registerClientScript();
        $html = [];
        $html[] = Html::input('button', null,'Choose file', [
            "class" => "btn btn-primary btn-large clearfix upload-btn",
        ]);
        $html[] = Html::tag('span', Html::tag('i', 'PNG или JPG (2Мб макс)'), [
            "style" => "padding-left:5px;vertical-align:middle;",
        ]);
        $html[] = Html::tag('div', null, [
            "class" => "clearfix redtext errormsg",
            "style" => "padding-top: 10px;",
        ]);
        $html[] = Html::tag('div', null, [
            "class" => "progress-wrap pic-progress-wrap",
            "style" => "margin-top:10px;margin-bottom:10px;",
        ]);
        $html[] = Html::tag('div', null, [
            "data-id" => "picture",
            "class" => "clear picbox",
            "style" => "padding-top:0px;padding-bottom:10px;width:100%;max-width:200px",
        ]);
        $html[] = Html::tag('div', null, [
            "class" => "clear-line",
            "style" => "margin-top:10px;",
        ]);

        return Html::tag('div', join('', $html), ['class' => 'content-box', 'id' => $this->id]);
    }

    /**
     * Registers the needed JavaScript.
     */
    public function registerClientScript()
    {
        $this->getView()->registerJs("FileUpload6.init('#{$this->id}', $this->school_id, 2000);");
        Asset::register($this->getView());
    }

    /**
     * Returns the options for the captcha JS widget.
     *
     * @return array the options
     */
    protected function getClientOptions()
    {
        return [];
    }

}
