<?php

namespace common\widgets\FileUpload4;

use cs\services\UploadFolderDispatcher;
use Imagine\Image\Box;
use Yii;
use yii\db\Query;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;
use yii\helpers\VarDumper;
use yii\helpers\Url;
use yii\helpers\FileHelper;
use yii\imagine\Image;
use yii\web\Controller;
use yii\widgets\ActiveForm;
use yii\web\Request;
use yii\web\Response;
use cs\base\BaseController;
use cs\services\Security;

/*
'upload/upload'                         => 'upload/upload',
'upload/download/<id:\\d+>'             => 'upload/download',
*/

class UploadController extends BaseController
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => [
                            '@',
                            '?'
                        ],
                    ],
                ],
            ],
        ];
    }

    /**
     * Закачивает файл `$_FILES['file']`
     *
     * @return string JSON Response
     *                [[
     *                    $tempUploadedFileName,
     *                    $fileName
     *                ]]
     */
    public function actionUpload()
    {
        $post = Yii::$app->request->post();
        $table = ArrayHelper::getValue($post, 'table', '');
        $field = ArrayHelper::getValue($post, 'field', '');

        $path = UploadFolderDispatcher::createFolder(FileUploadMany::$uploadDirectory);
        if ($table) $path->addAndCreate($table);
        if ($field) $path->addAndCreate($field);
        $info = parse_url($_SERVER['HTTP_REFERER']);
        if ($info['host'] != $_SERVER['SERVER_NAME']) return self::jsonError('Не тот сайт');

        if (isset($_FILES['file'])) {
            $file = $_FILES['file'];
            $ret = [];

            $error = $file['error'];
            //You need to handle  both cases
            //If Any browser does not support serializing of multiple files using FormData()
            if (!is_array($file['name'])) //single file
            {
                $fileName = UploadFolderDispatcher::generateFileName($file['name']);
                $fileNameFull = $this->getFileNameFull($fileName);
                $pathFull = $path->cloneObject()->add($fileNameFull);
                $path->add($fileName);
                copy($file['tmp_name'], $pathFull->getPathFull());
                $image = Image::getImagine()->open($file['tmp_name']);
                $image->thumbnail(new Box(100, 100), \Imagine\Image\ManipulatorInterface::THUMBNAIL_OUTBOUND)->save($path->getPathFull(), []);
                if (file_exists($file['tmp_name'])) unlink($file['tmp_name']);

                $ret[] = [
                    $path->getPath(),
                    $file['name'],
                    $pathFull->getPath(),
                ];
            }
            else  //Multiple files, file[]
            {
            }

            return self::jsonSuccess($ret);
        }
    }

    public function getFileNameFull($file)
    {
        $info = pathinfo($file);

        return $info['filename'] . '_original.' . strtolower($info['extension']);
    }
}