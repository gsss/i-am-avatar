<?php

/**
 * params:
 * [
 *      'rows'     => [
 *                       [
 *                            'id'                 => '524'
 *                            'name'               => '123456'
 *                            ...
 *                            'selected'           => bool
 *                        ], ...
 *                    ]
 * ]
 */

/** @var $rows array */
/** @var $options string */
/** @var $name string */
/** @var $id string */


?>


<?php foreach ($rows as $item) { ?>
    <p style="margin: 0px;">
        <input
            style="margin-right: 5px;"
            name="<?= $name ?>"
            type="radio"
            value="<?= $item['id'] ?>"
            id="<?= $id ?>_<?= $item['id'] ?>"
            >

        <label for="<?= $id ?>_<?= $item['id'] ?>">
            <?= $item['name'] ?>
        </label>
    </p>
    <?php if (isset($item['nodes'])) { ?>
        <div style="margin-left: 20px;">
            <?= $this->render('_tree', [
                'rows'    => $item['nodes'],
                'options' => $options,
                'name'    => $name,
                'id'      => $id,
            ]); ?>
        </div>
    <?php } ?>
<?php } ?>



