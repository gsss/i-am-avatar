<?php
/**
 * Created by PhpStorm.
 * User: s.arhangelskiy
 * Date: 27.09.2016
 * Time: 17:24
 */
namespace common\widgets\Tree;

use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;

class Tree extends \yii\widgets\InputWidget
{
    /** @var  string $className */
    public $className;

    public function run()
    {
        $rows = $this->getRows();
        $html = \Yii::$app->view->renderFile(\Yii::getAlias('@common/widgets/Tree/_tree.php'), [
            'rows' => $rows,
            'name' => Html::getInputName(new $this->className, $this->attribute),
            'id'   => Html::getInputId(new $this->className, $this->attribute),
            'options' => [],
        ]);

        return $html;
    }

    /**
     * Возвращает элементы списка в виде дерева
     *
     * @param array $options
     * - selectFields - string|array - поля для выборки, по умолчанию id,name
     * - parentId - int - идентификатор ветки-родителя для которой и производится выборка дерева
     * - order - string|array - параметр сортировки который передается в ->orderBy()
     *
     * @return array
     * [[
     *  'id' =>
     *  'name' =>
     *  'nodes' => array
     * ], ... ]
     */
    public function getRows($options = [])
    {
        $selectFields = ArrayHelper::getValue($options, 'selectFields', 'id,name');
        $parentId = ArrayHelper::getValue($options, 'parentId', null);
        $order = ArrayHelper::getValue($options, 'order', null);

        /** @var \yii\db\ActiveRecord $class */
        $class = new $this->className;
        $query = $class::find()
            ->select($selectFields)
            ->where(['parent_id' => $parentId]);
        if ($order) $query->orderBy($order);

        $rows = $query->asArray()->all();
        for ($i = 0; $i < count($rows); $i++) {
            $item = &$rows[$i];
            $options['parentId'] = $item['id'];
            $rows2 = $this->getRows($options);
            if (count($rows2) > 0) {
                $item['nodes'] = $rows2;
            }
        }

        return $rows;
    }

} 