var FileUpload9 = {
    init: function(selector, school_id, maxSize1){

        var btn = $(selector).find('.upload-btn')[0];
        var wrap = $(selector).find('.pic-progress-wrap')[0];
        var picBox = $(selector).find('.picbox')[0];
        var errBox = $(selector).find('.errormsg')[0];

        var uploader = new ss.SimpleUpload({
            button: btn,
            url: '/uploadFile.php?id=' + school_id,
            sessionProgressUrl: '/sessionProgress.php?id=' + school_id,
            name: 'imgfile',
            multiple: true,
            multipart: true,
            maxUploads: 2,
            maxSize: maxSize1,
            queue: false,
            allowedExtensions: ['jpg','pem','crt','ca-bundle', 'key'],
            accept: '*/*',
            debug: true,
            hoverClass: 'btn-hover',
            focusClass: 'active',
            disabledClass: 'disabled',
            responseType: 'json',
            onSubmit: function(filename, ext) {
                var prog = document.createElement('div'),
                    outer = document.createElement('div'),
                    bar = document.createElement('div'),
                    size = document.createElement('div'),
                    self = this;

                prog.className = 'prog';
                size.className = 'size';
                outer.className = 'progress progress-striped';
                bar.className = 'progress-bar progress-bar-success';

                outer.appendChild(bar);
                prog.appendChild(size);
                prog.appendChild(outer);
                wrap.appendChild(prog); // 'wrap' is an element on the page

                self.setProgressBar(bar);
                self.setProgressContainer(prog);
                self.setFileSizeBox(size);

                errBox.innerHTML = '';
                btn.value = 'Choose another file';
            },
            onSizeError: function() {
                errBox.innerHTML = 'Files may not exceed 1024K.';
            },
            onExtError: function() {
                errBox.innerHTML = 'Invalid file type. Please select a PNG, JPG, GIF image.';
            },
            onComplete: function(file, response, btn) {
                if (!response) {
                    errBox.innerHTML = 'Unable to upload file';
                }
                if (response.success === true) {
                    picBox.innerHTML = '<code id="fileUploadedUrl">https://www.i-am-avatar.com/upload/cloud/' + response.file + '</code>' + '<br>'+ '<code id="fileUploadedPath">/home/god/i-am-avatar/www/public_html/upload/cloud/' + response.file + '</code>';
                } else {
                    if (response.msg)  {
                        errBox.innerHTML = response.msg;
                    } else {
                        errBox.innerHTML = 'Unable to upload file';
                    }
                }

            }
        });
    }
};


