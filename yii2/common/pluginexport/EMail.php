<?php

namespace common\pluginexport;

use app\commands\MoneyRateController;
use app\models\Orders;
use app\models\Transaction;
use Blocktrail\SDK\BlocktrailSDK;
use Blocktrail\SDK\Wallet;
use common\components\marketing\Marketing;
use common\models\avatar\UserBill;
use common\models\Billing;
use common\models\Config;
use common\models\Countries;
use common\models\Currency;
use common\models\InstalmentPlan;
use common\models\PaymentBitCoin;
use common\services\Debugger;
use common\services\Security;
use common\services\Subscribe;
use phpDocumentor\Reflection\DocBlock\Tag\VarTag;
use Yii;
use common\components\payments\classes\BasePayment;
use common\models\PaymentSystem;
use yii\base\BaseObject;
use yii\base\Exception;

use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Json;
use yii\helpers\Url;
use Endroid\QrCode\QrCode;

use pjhl\multilanguage\helpers\Languages;
use yii\helpers\VarDumper;
use yii\httpclient\Client;

/**
 */
class EMail extends BaseObject
{
    public $to;

    /**
     * @param array $lid массив пар название значение, где Название это тип поля из формы, а значение это значение из формы
     *
     */
    public function export($lid)
    {
        Subscribe::sendArray([$this->to], 'Новый лид', 'pluginexport/email', ['lid' => $lid]);
    }

}