<?php
/**
 * Created by PhpStorm.
 * User: god
 * Date: 28.02.2017
 * Time: 23:49
 */

namespace common\models;

use common\models\avatar\UserBill;
use common\models\piramida\Billing;
use common\models\statistic\StatisticDb;
use yii\db\ActiveRecord;
use yii\helpers\VarDumper;

/**
 *
 * @property integer id
 * @property integer time
 * @property integer currency_id
 * @property float rub
 * @property float usd
 * @property float btc
 *
 * @package common\models
 */
class ChartPoint extends ActiveRecord
{
    public static function tableName()
    {
        return 'charts';
    }

    public static function add($fields)
    {
        if (!isset($fields['time'])) {
            $fields['time'] = time();
        }
        $i = new self($fields);
        $ret = $i->save();
        if (!$ret) throw new \Exception('Cant save '.VarDumper::dumpAsString($fields));
        $i->id = self::getDb()->lastInsertID;

        return $i;
    }
}