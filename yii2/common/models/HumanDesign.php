<?php


namespace common\models;


use yii\base\BaseObject;
use yii\helpers\Json;
use yii\helpers\Url;

class HumanDesign extends BaseObject
{
    public $image;
    public $type;
    public $profile;
    public $definition;
    public $inner;
    public $strategy;
    public $theme;
    public $cross;
    public $signature;
    public $extended;

    /**
     * Возвращает путь к картинке
     *
     * @param bool $isSchema
     *
     * @return string
     *
     */
    public function getImage($isSchema = false)
    {
        $url = $this->image;
        if (is_null($url)) {
            $url = '';
        }
        if ($url == '') {
            return '';
        }

        return Url::to($url, $isSchema);
    }

    public function getJson()
    {
        return Json::encode($this);
    }
}