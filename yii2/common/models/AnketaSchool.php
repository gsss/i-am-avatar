<?php

namespace common\models;

use app\services\Subscribe;
use cs\services\BitMask;
use yii\db\ActiveRecord;
use yii\db\Query;

/**
 * @property int id
 * @property int created_at
 * @property string name
 * @property string description
 * @property string email
 * @property string phone
 */
class AnketaSchool extends ActiveRecord
{
    public static function tableName()
    {
        return 'anketa_school';
    }

    public function rules()
    {
        return [
            ['created_at','integer'],
            ['name','string'],
            ['description','string'],
            ['email','string'],
            ['phone','string'],
        ];
    }

}