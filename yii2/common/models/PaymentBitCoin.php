<?php

namespace common\models;

use common\models\piramida\WalletSource\BitCoin;
use cs\services\VarDumper;
use Yii;
use yii\base\Exception;
use yii\db\ActiveRecord;
use yii\helpers\Json;

/**
 * @property integer id
 * @property string  action
 * @property integer billing_id
 * @property string  address
 * @property double  sum_before
 * @property double  sum_after
 * @property string  transaction
 * @property integer time_add
 * @property integer time_confirm
 * @property string  web_hook
 * @property integer is_web_hook_deleted
 * @property integer confirmations          кол-во подтверждений
 * @property string  transaction_hash       хеш транзакции
 */
class PaymentBitCoin extends ActiveRecord implements PaymentI
{
    public $confirmations = 6;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'payments_bit_coin';
    }

    public function setTransaction($txid)
    {
        $this->transaction_hash = $txid;
    }

    public function saveTransaction($txid)
    {
        $this->transaction_hash = $txid;
        if (!$this->save()) {
            throw new \Exception('Не удалось сохранить');
        }
    }

    public function getTransaction()
    {
        return $this->transaction_hash;
    }

    public function getAddress()
    {
        return $this->address;
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['address', 'billing_id'], 'required'],
            [['action'], 'string', 'max' => 255],
            [['transaction_hash'], 'string', 'max' => 80],
            [['address'], 'string', 'max' => 40],
            [['transaction'], 'string', 'max' => 65537],
            [['billing_id', 'time_add', 'time_confirm'], 'integer', 'min' => 1],
            [['is_web_hook_deleted'], 'integer', 'min' => 0, 'max' => 1],
            [['sum_before', 'sum_after'], 'double'],
            [['confirmations'], 'integer', 'min' => 0],
        ];
    }

    /**
     * @param mixed $condition
     * @return \common\models\PaymentBitCoin
     * @throws Exception
     */
    public static function findOne($condition)
    {
        $item = parent::findOne($condition);
        if (is_null($item)) {
            throw new Exception('Не найден платеж');
        }

        return $item;
    }


    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'           => 'ID',
            'action'       => 'Действие',
            'address'      => 'Адрес',
            'transaction'  => 'Трназакция',
            'billing_id'   => 'ID счета',
            'sum_before'   => 'сумма до',
            'sum_after'    => 'сумма после',
            'time_add'     => 'Время добавления',
            'time_confirm' => 'Время подтверждения платежа',
        ];
    }


    /**
     * Проверяет транзакцию на оплаченность
     *
     * @return array
     * [
     *  bool - true - валидная, false - не валидная
     *  int  - номер ошибки
     *          1 - недостаточно подтверждений
     *          2 - неверный адрес получателя
     *          3 - недостаточно монет
     *          4 - не найдена транзакция в блокчейне
     *          6 - не верный тип транзакции
     *          7 - такая транзакция уже есть
     * ]
     */
    public function isTransactionPaidBilling()
    {
        if (YII_ENV_PROD) {
            return $this->isTransactionPaidBillingProd();
        } else {
            return $this->isTransactionPaidBillingTest();
        }
    }

    /**
     * Проверяет транзакцию на оплаченность
     *
     * @return array
     * [
     *  bool - true - валидная, false - не валидная
     *  int  - номер ошибки
     *          1 - недостаточно подтверждений
     *          2 - неверный адрес получателя
     *          3 - недостаточно монет
     *          4 - не найдена транзакция в блокчейне
     *          6 - не верный тип транзакции
     *          7 - такая транзакция уже есть
     * ]
     */
    public function isTransactionPaidBillingProd()
    {
        $info = $this->getTxInfo($this->transaction);

        // проверка на кол-во подтверждений
        if ($info['confirmations'] < $this->confirmations) return [false, 1];

        // проверка на верный адрес и кол-во монет
        $result = false;
        foreach ($info['outputs'] as $output) {
            if (in_array($this->address, $output['addresses'])) {
                if (($output['value'] / 100000000) >= $this->sum_after) {
                    $result = true;
                } else {
                    return [false, 3];
                }
            }
        }
        if ($result == false) return [false, 2];

        // проверка на дубликаты
        $exist = self::find()
            ->where(['transaction' => $this->transaction])
            ->andWhere(['<', 'id', $this->id ])
            ->exists();
        if ($exist) return [false, 7];

        return [true];
    }

    /**
     * Проверяет транзакцию на оплаченность
     *
     * @return array
     * [
     *  bool - true - валидная, false - не валидная
     *  int  - номер ошибки
     *          1 - недостаточно подтверждений
     *          2 - неверный адрес получателя
     *          3 - недостаточно монет
     *          4 - не найдена транзакция в блокчейне
     *          6 - не верный тип транзакции
     *          7 - такая транзакция уже есть
     * ]
     */
    public function isTransactionPaidBillingTest()
    {
        $info = self::getTxInfoTest($this->transaction);

        // проверка на кол-во подтверждений
        if ($info['confirmations'] < $this->confirmations) return [false, 1];

        // проверка на верный адрес и кол-во монет
        $result = false;
        foreach ($info['outputs'] as $output) {
            if ($this->address == $output['address']) {
                if (($output['value'] / 100000000) >= $this->sum_after) {
                    $result = true;
                } else {
                    return [false, 3];
                }
            }
        }
        if ($result == false) return [false, 2];

        // проверка на дубликаты
        $exist = self::find()
            ->where(['transaction' => $this->transaction])
            ->andWhere(['<', 'id', $this->id ])
            ->exists();
        if ($exist) return [false, 7];

        return [true];
    }


    /**
     * https://api.blockcypher.com/v1/btc/main/txs/81437e9660c2bafbc4b691e44fb5bae0218ae3a71f2e158485a9783a82806236?limit=50&includeHex=true
     *
     * @param string $txid хеш транзакции
     *
     * @return array
     * https://api.blockcypher.com/v1/btc/main/txs/81437e9660c2bafbc4b691e44fb5bae0218ae3a71f2e158485a9783a82806236?limit=50&includeHex=true
     *
     * @throws
     */
    public static function getTxInfo($txid)
    {
        $url = 'https://api.blockcypher.com';

        $client = new \yii\httpclient\Client(['baseUrl' => $url]);
        $content = $client->get('v1/btc/main/txs/'.$txid, ['limit' => '50', 'includeHex' => 'true'])->send()->content;
        $data = Json::decode($content);
        return $data;
    }

    /**
     * https://api.blockcypher.com/v1/btc/main/txs/81437e9660c2bafbc4b691e44fb5bae0218ae3a71f2e158485a9783a82806236?limit=50&includeHex=true
     *
     * @param string $txid хеш транзакции
     *
     * @return array
     * https://api.blockcypher.com/v1/btc/main/txs/81437e9660c2bafbc4b691e44fb5bae0218ae3a71f2e158485a9783a82806236?limit=50&includeHex=true
     *
     * @throws
     */
    public static function getTxInfoTest($txid)
    {
        $provider = new \common\payment\BitCoinBlockTrailPayment();

        return $provider->getClient()->transaction($txid);
    }

}
