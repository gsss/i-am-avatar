<?php

namespace common\models\comment;

use common\models\PaySystem;
use cs\Application;
use cs\services\BitMask;
use cs\web\Exception;
use yii\db\ActiveRecord;
use yii\db\Query;
use yii\helpers\Json;
use yii\helpers\VarDumper;

/**
 *
 * @property int    id              - идентификатор записи
 * @property string name
 * @property string after_insert
 *
 */
class Type extends ActiveRecord
{
    public static function tableName()
    {
        return 'comments_list_type';
    }


    /**
     * @param array $fields
     * @return static
     */
    public static function add($fields)
    {
        $item = new static($fields);
        $item->save();
        $item->id = self::getDb()->lastInsertID;

        return $item;
    }

}