<?php

namespace common\models\comment;

use common\models\PaySystem;
use cs\Application;
use cs\services\BitMask;
use cs\web\Exception;
use yii\db\ActiveRecord;
use yii\db\Query;
use yii\helpers\Json;
use yii\helpers\VarDumper;

/**
 *
 * @property int    id              - идентификатор записи
 * @property int    type_id
 * @property int    object_id
 *
 */
class CommentList extends ActiveRecord
{
    public static function tableName()
    {
        return 'comments_list';
    }

    /**
     * @param $type_id
     * @param $object_id
     * @return CommentList
     */
    public static function get($type_id, $object_id)
    {
        $d = self::findOne(['type_id' => $type_id, 'object_id' => $object_id]);
        if (is_null($d)) {
            return self::add(['type_id' => $type_id, 'object_id' => $object_id]);
        }
        return $d;
    }

    /**
     * @param array $fields
     * @return static
     */
    public static function add($fields)
    {
        $item = new static($fields);
        $item->save();
        $item->id = self::getDb()->lastInsertID;

        return $item;
    }

}