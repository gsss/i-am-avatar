<?php

namespace common\models\comment;

use common\models\task\Task;
use common\models\UserAvatar;
use common\models\UserIdea;
use common\services\Subscribe;
use yii\helpers\VarDumper;

/**
 * Логика:
 * Все кто участвовали в беседе и автору идеи отправляется уведомление о новом коментарии
 */
class AfterInsertIdea implements iCommentAfterInsert
{
    /**
     * @param Comment $comment
     *
     * @return bool
     * @throws
     */
    function afterInsert($comment)
    {
        // Формирую список идентификаторов пользователей кому будет рассылка
        $list = Comment::find()
            ->where(['list_id' => $comment->list_id])
            ->select('user_id')
            ->column()
        ;
        $idea = UserIdea::findOne($comment->getList()->object_id);
        $list[] = $idea->user_id;
        $listUniq = array_unique($list);

        // Формирую список почтовых адресов для отправки
        $mailList = [];
        foreach ($listUniq as $user_id) {
            if ($user_id != $comment->user_id) {
                $user = UserAvatar::findOne($user_id);
                $mailList[] = $user->email;
            }
        }
        \Yii::info(VarDumper::dumpAsString($mailList), '\common\models\comment\AfterInsertIdea::afterInsert');

        // Отправляю пакет писем
        Subscribe::sendArray($mailList,'Появился новый комментарий в идее', 'comment/idea', [
            'comment' => $comment,
            'idea'    => $idea,
        ]);

        return true;
    }
}
