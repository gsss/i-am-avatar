<?php

namespace common\models\comment;

use common\models\PaySystem;
use common\models\UserAvatar;
use common\services\Subscribe;
use cs\Application;
use cs\services\BitMask;
use cs\web\Exception;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\db\Query;
use yii\helpers\Html;
use yii\helpers\Json;
use yii\helpers\StringHelper;
use yii\helpers\VarDumper;

/**
 *
 */
class AfterInsert implements iCommentAfterInsert
{
    /**
     * @param Comment $comment
     *
     * @return bool
     * @throws
     */
    function afterInsert($comment)
    {
        $list = Comment::find()->where(['list_id' => $comment->list_id])->select('user_id')->column();
        $listUniq = array_unique($list);

        $mailList = [];
        foreach ($listUniq as $user_id) {
            if ($user_id != $comment->user_id) {
                $user = UserAvatar::findOne($user_id);
                $mailList[] = $user->email;
            }
        }
        Subscribe::sendArray($mailList,'Появился новый комментарий', 'comment/new', [
            'comment' => $comment,
        ]);

        return true;
    }
}
