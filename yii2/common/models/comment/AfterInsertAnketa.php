<?php

namespace common\models\comment;

use common\models\Anketa;
use common\models\task\Task;
use common\models\UserAvatar;
use common\services\Subscribe;

/**
 *
 */
class AfterInsertAnketa implements iCommentAfterInsert
{
    /**
     * @param Comment $comment
     *
     * @return bool
     * @throws
     */
    function afterInsert($comment)
    {
        // Формирую список идентификаторов пользователей кому будет рассылка
        $list = Comment::find()
            ->where(['list_id' => $comment->list_id])
            ->select('user_id')
            ->column()
        ;
        $Anketa = Anketa::findOne($comment->getList()->object_id);
        $listUniq = array_unique($list);

        // Формирую список почтовых адресов для отправки
        $mailList = [];
        foreach ($listUniq as $user_id) {
            if ($user_id != $comment->user_id) {
                $user = UserAvatar::findOne($user_id);
                $mailList[] = $user->email;
            }
        }

        // Отправляю пакет писем
        Subscribe::sendArray($mailList,'Появился новый комментарий в анкете', 'comment/anketa', [
            'comment' => $comment,
            'item'    => $Anketa,
        ]);

        // Обновляю время последнего комантария
        $Anketa->last_message_time = time();
        $Anketa->comments_count++;
        $Anketa->save();

        return true;
    }
}
