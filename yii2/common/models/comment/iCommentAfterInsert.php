<?php
/**
 * Created by PhpStorm.
 * User: Ra-m-ha
 * Date: 10.01.2019
 * Time: 12:43
 */

namespace common\models\comment;


interface iCommentAfterInsert
{
    /**
     * @param Comment $comment
     *
     * @return bool
     */
    function afterInsert($comment);
}