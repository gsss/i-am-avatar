<?php

namespace common\models\comment;

use common\models\PaySystem;
use common\models\UserAvatar;
use cs\Application;
use cs\services\BitMask;
use cs\web\Exception;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\db\Query;
use yii\helpers\Html;
use yii\helpers\Json;
use yii\helpers\StringHelper;
use yii\helpers\VarDumper;

/**
 *
 * @property int    id              - идентификатор записи
 * @property string text
 * @property string file
 * @property int    list_id
 * @property int    user_id
 * @property int    parent_id
 * @property int    created_at
 *
 */
class Comment extends ActiveRecord
{
    public static function tableName()
    {
        return 'comments';
    }

    /**
     *
     * @return UserAvatar
     * @throws \yii\base\Exception
     */
    public function getUser()
    {
        return UserAvatar::findOne($this->user_id);
    }

    public function behaviors()
    {
        return [
            [
                'class' => '\yii\behaviors\TimestampBehavior',
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at'],
                ],
                // если вместо метки времени UNIX используется datetime:
                // 'value' => new Expression('NOW()'),
            ],
        ];
    }


    /**
     * @param array $fields
     * @return static
     */
    public static function add($fields)
    {
        $item = new static($fields);
        $item->save();
        $item->id = self::getDb()->lastInsertID;

        return $item;
    }

    /**
     * Преобразовывает text в HTML формат
     * перенос строки в <br>
     * и ссылки в ссылки
     *
     * @return string
     */
    public function getHtml()
    {
        $text = $this->text;
        $rows = explode("\n", $text);
        $rows2 = [];
        foreach ($rows as $row) {
            $arr = explode(' ', $row);
            $arr2 = [];
            foreach ($arr as $word) {
                $w = trim($word);
                if (StringHelper::startsWith($w, 'https://') or StringHelper::startsWith($w, 'http://') or StringHelper::startsWith($w, 'blob:http')) {
                    $w = Html::a($w, $w, ['target' => '_blank']);
                    $arr2[] = $w;
                } else {
                    $arr2[] = Html::encode($w);
                }
            }
            $rows2[] = join(' ', $arr2);
        }

        return join('<br>', $rows2);
    }

    /**
     * @return CommentList
     */
    public function getList()
    {
        return CommentList::findOne($this->list_id);
    }

}