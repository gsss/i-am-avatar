<?php

namespace common\models\comment;

use common\models\BlogItem;
use common\models\task\Task;
use common\models\UserAvatar;
use common\models\UserIdea;
use common\services\Subscribe;
use yii\helpers\ArrayHelper;
use yii\helpers\VarDumper;

/**
 * Логика:
 * Все кто участвовали в беседе и роли role_admin отправляется уведомление о новом коментарии
 */
class AfterInsertBlog implements iCommentAfterInsert
{
    /**
     * @param Comment $comment
     *
     * @return bool
     * @throws
     */
    function afterInsert($comment)
    {
        // Формирую список идентификаторов пользователей кому будет рассылка
        $list = Comment::find()
            ->where(['list_id' => $comment->list_id])
            ->select('user_id')
            ->column()
        ;
        $adminList = \Yii::$app->authManager->getUserIdsByRole('role_admin');
        $list = ArrayHelper::merge($list, $adminList);
        $listUniq = array_unique($list);

        // вычисляю статью которую коментируют
        $blogId = CommentList::findOne($comment->list_id)->object_id;
        $blog = BlogItem::findOne($blogId);
        $blog->comments_count++;
        $blog->save();

        // Формирую список почтовых адресов для отправки
        $mailList = [];
        foreach ($listUniq as $user_id) {
            if ($user_id != $comment->user_id) {
                $user = UserAvatar::findOne($user_id);
                $mailList[] = $user->email;
            }
        }
        \Yii::info(VarDumper::dumpAsString($mailList), '\common\models\comment\AfterInsertBlog::afterInsert');

        // Отправляю пакет писем
        Subscribe::sendArray($mailList,'Появился новый комментарий в стстье блога', 'comment/blog', [
            'comment' => $comment,
            'blog'    => $blog,
        ]);

        return true;
    }
}
