<?php

namespace common\models\comment;

use common\models\task\Helper;
use common\models\task\Task;
use common\models\UserAvatar;
use common\services\Subscribe;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\web\YiiAsset;

/**
 *
 */
class AfterInsertTask implements iCommentAfterInsert
{
    /**
     * @param Comment $comment
     *
     * @return bool
     * @throws
     */
    function afterInsert($comment)
    {
        // Формирую список идентификаторов пользователей кому будет рассылка
        $list = Comment::find()
            ->where(['list_id' => $comment->list_id])
            ->select('user_id')
            ->column();
        $task = Task::findOne($comment->getList()->object_id);
        $list[] = $task->user_id;
        if ($task->executer_id) {
            $list[] = $task->executer_id;
        }

        // Добавляю помощников
        $helpers = Helper::find()->where(['task_id' => $task->id])->select('user_id')->column();
        $list = ArrayHelper::merge($list, $helpers);

        // Убираю дубликаты
        $listUniq = array_unique($list);

        // Формирую список почтовых адресов для отправки
        // Формирую список Идентификаторов телеграмма кого надо известить о новом коментарии
        $mailList = [];
        $tgList = [];
        foreach ($listUniq as $user_id) {
            if ($user_id != $comment->user_id) {
                $user = UserAvatar::findOne($user_id);
                $mailList[] = $user->email;
                if ($user->telegram_chat_id) {
                    $tgList[] = $user;
                }
            }
        }

        // отправляю уведомления
        if (count($tgList) > 0) {
            /** @var \aki\telegram\Telegram $telegram */
            $telegram = \Yii::$app->telegram;
            /** @var \common\models\UserAvatar $user */
            foreach ($tgList as $user) {
                $author = UserAvatar::findOne($comment->user_id);
                if (YII_ENV_PROD) {
                    $telegram->sendMessage([
                        'chat_id' => $user->telegram_chat_id,
                        'text'    => join("\n", [
                            'В задаче "' . $task->name . ' [' . $task->id . ']' . '" появился новый коментарий:',
                            'Автор' . ': ' . $author->getName2() . '[' . $author->id . ']',
                            $comment->text,
                            'Задача: ' . Url::to(['cabinet-task-list/view', 'id' => $task->id], true),
                        ]),
                    ]);
                }
            }
        }

        // Отправляю пакет писем
        Subscribe::sendArraySchool($task->getSchool(), $mailList, 'Появился новый комментарий в задаче', 'comment/task', [
            'comment' => $comment,
            'task'    => $task,
        ], 'layouts/html/task');

        // Триггерю событие
        $task->trigger(Task::EVENT_TASK_COMMENT);

        // Обновляю время последнего комантария в задачу
        $task->last_comment_time = time();
        $task->save();

        return true;
    }
}
