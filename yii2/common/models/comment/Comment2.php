<?php

namespace common\models\comment;

use common\models\PaySystem;
use common\models\UserAvatar;
use cs\Application;
use cs\services\BitMask;
use cs\web\Exception;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\db\Query;
use yii\helpers\Html;
use yii\helpers\Json;
use yii\helpers\StringHelper;
use yii\helpers\VarDumper;

/**
 *
 * @property int    id              - идентификатор записи
 * @property string text
 * @property int    list_id
 * @property int    user_id
 * @property int    parent_id
 * @property int    created_at
 *
 */
class Comment2 extends ActiveRecord
{
    public static function tableName()
    {
        return 'comments';
    }

    /**
     *
     * @return UserAvatar
     * @throws \yii\base\Exception
     */
    public function getUser()
    {
        return UserAvatar::findOne($this->user_id);
    }




    /**
     * @param array $fields
     * @return static
     */
    public static function add($fields)
    {
        $item = new static($fields);
        $item->save();
        $item->id = self::getDb()->lastInsertID;

        return $item;
    }


}