<?php

namespace common\models;

use app\models\Shop\Payments;
use app\services\Subscribe;
use common\models\PaySystem;
use cs\base\DbRecord;
use cs\services\BitMask;
use yii\db\ActiveRecord;
use yii\db\Query;
use yii\helpers\Url;


/**
 * Конфиг платежной системы для магазина
 *
 * @property integer id
 * @property string  name
 * @property integer created_at
 */
class PaySystemListItem extends ActiveRecord
{
    public static function tableName()
    {
        return 'paysystems_config_list';
    }

    public function rules()
    {
        return [
            [['name'], 'required'],
            [['name'], 'string', 'max' => 255],
            [['created_at'], 'integer'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'name'       => 'Название',
            'created_at' => 'Создано',
        ];
    }
}