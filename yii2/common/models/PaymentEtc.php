<?php

namespace common\models;

use cs\services\VarDumper;
use Yii;
use yii\base\Exception;
use yii\db\ActiveRecord;
use yii\helpers\Json;

/**
 * @property integer    id
 * @property integer    billing_id
 * @property float      value
 * @property string     action
 * @property string     transaction
 * @property integer    transaction_at когда была укзана транзакция как оплаченная
 * @property string     from
 * @property string     address
 */
class PaymentEtc extends ActiveRecord implements PaymentI
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'payments_ethereum_classic';
    }

    public function setTransaction($txid)
    {
        $this->transaction = $txid;
    }

    public function saveTransaction($txid)
    {
        $this->transaction = $txid;
        $this->save();
    }

    public function getTransaction()
    {
        return $this->transaction;
    }

    public function getTransactionAt()
    {
        return $this->transaction_at;
    }

    /**
     * @param mixed $condition
     * @return \common\models\PaymentBitCoin
     * @throws Exception
     */
    public static function findOne($condition)
    {
        $item = parent::findOne($condition);
        if (is_null($item)) {
            throw new Exception('Не найден платеж');
        }

        return $item;
    }


    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'          => 'ID',
            'billing_id'  => 'Счет',
            'value'       => 'Число монет',
            'action'      => 'ссылка на платеж в мерчанте',
            'transaction' => 'Трназакция',
            'from'        => 'от кого',
        ];
    }


    /**
     * Проверяет транзакцию на оплаченность
     *
     * @return bool
     */
    public function isTransactionPaidBilling()
    {
        $info = $this->getTxInfo($this->transaction);

        // проверка на кол-во подтверждений
        if ($info['confirmations'] < $this->confirmations) return false;

        // проверка на верный адрес и кол-во монет
        $result = false;
        foreach ($info['outputs'] as $output) {
            if (in_array($this->address, $output['addresses'])) {
                if (($output['value']/100000000) >= $this->value) $result = true;
            }
        }
        if ($result == false) return false;

        // проверка на дубликаты
        $exist = self::find()
            ->where(['transaction' => $this->transaction])
            ->andWhere(['<', 'id', $this->id ])
            ->exists();
        if ($exist) return false;

        return true;
    }


    /**
     * https://etcchain.com/gethProxy/eth_getTransactionByHash?txHash=0xbaf44cb49905f55559682c581a3f434a6c86ec74e67f969167673360ef542433
     *
     * @param string $txid хеш транзакции
     *
     * @return array
     * https://etcchain.com/gethProxy/eth_getTransactionByHash?txHash=0xbaf44cb49905f55559682c581a3f434a6c86ec74e67f969167673360ef542433
     *
     * @throws
     */
    public static function getTxInfo($txid)
    {
        $url = 'https://etcchain.com';

        $client = new \yii\httpclient\Client(['baseUrl' => $url]);
        $content = $client->get('gethProxy/eth_getTransactionByHash', ['txHash' => $txid])->send()->content;
        $data = Json::decode($content);
        return $data;
    }

    /**
     * https://etcchain.com/tx/0xbaf44cb49905f55559682c581a3f434a6c86ec74e67f969167673360ef542433
     *
     * @param string $txid хеш транзакции
     *
     * @return array
     * [
     *      'from'
     *      'to'
     *      'amount'
     *      'confirmations'
     * ]
     *
     * @throws
     */
    public static function getTxInfo2($txid)
    {
        $url = 'https://etcchain.com';

        $client = new \yii\httpclient\Client(['baseUrl' => $url]);
        $content = $client->get('tx/'.$txid)->send()->content;
        require_once(\Yii::getAlias('@csRoot/services/simplehtmldom_1_5/simple_html_dom.php'));
        $content = str_get_html($content);
        /** @var \simple_html_dom_node $rows */
        $rows = $content->find('div.span6');
        $block1 = $rows[0];
        $block2 = $rows[1];

        $rows = $block1->find('td');
        $from = trim($rows[5]->text());
        $to = trim($rows[7]->text());

        $rows = $block2->find('td');
        $amount = (float)explode(' ', $rows[1]->text())[0];
        $confirmations = (int)trim($rows[9]->text());

        return [
            'from'          => $from,
            'to'            => $to,
            'amount'        => $amount,
            'confirmations' => $confirmations,
        ];
    }

    /**
     * http://gastracker.io/tx/0x06487ca8b54e8cc0d34043ced11187778658fc654ed1a33f5500f505660822c8
     *
     * @param string $txid хеш транзакции
     *
     * @return array
     * [
     *      'from'
     *      'to'
     *      'amount'
     *      'time' => \DateTime
     * ]
     *
     * @throws
     */
    public static function getTxInfo3($txid)
    {
        $url = 'http://gastracker.io';

        $client = new \yii\httpclient\Client(['baseUrl' => $url]);
        $content = $client->get('tx/'.$txid)->send()->content;
        require_once(\Yii::getAlias('@csRoot/services/simplehtmldom_1_5/simple_html_dom.php'));
        $content = str_get_html($content);
        /** @var \simple_html_dom_node $rows */
        $rows = $content->find('dl.dl-horizontal//dd');
        $t = $rows[0]->text();
        $wei = $rows[2]->text();
        $amount = explode(' ', $wei)[0] / pow(10,18);
        $block2 = $rows[1];
        $from = trim($rows[7]->text());
        $to = trim($rows[8]->text());

        return [
            'from'          => $from,
            'to'            => $to,
            'amount'        => $amount,
            'time'          => new \DateTime($t),
        ];
    }

}
