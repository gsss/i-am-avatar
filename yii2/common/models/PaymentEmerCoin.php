<?php

namespace common\models;

use Yii;
use yii\base\Exception;
use yii\db\ActiveRecord;
use yii\helpers\Json;

/**
 * @property integer    id
 * @property integer    billing_id
 * @property float      value
 * @property string     action
 * @property string     transaction
 * @property integer    transaction_at когда была укзана транзакция как оплаченная
 * @property string     from
 * @property string     address
 */
class PaymentEmerCoin extends ActiveRecord implements PaymentI
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'payments_emercoin';
    }

    public function setTransaction($txid)
    {
        $this->transaction = $txid;
    }

    public function saveTransaction($txid)
    {
        $this->transaction = $txid;
        $this->save();
    }

    public function getTransaction()
    {
        return $this->transaction;
    }

    public function getTransactionAt()
    {
        return $this->transaction_at;
    }

    /**
     * @param mixed $condition
     * @return \common\models\PaymentBitCoin
     * @throws Exception
     */
    public static function findOne($condition)
    {
        $item = parent::findOne($condition);
        if (is_null($item)) {
            throw new Exception('Не найден платеж');
        }
        return $item;
    }


    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'          => 'ID',
            'billing_id'  => 'Счет',
            'value'       => 'Число монет',
            'action'      => 'ссылка на платеж в мерчанте',
            'transaction' => 'Трназакция',
            'from'        => 'от кого',
        ];
    }

    /**
     * Проверяет транзакцию на оплаченность
     *
     * @return bool
     */
    public function isTransactionPaidBilling()
    {
        $info = $this->getTxInfo($this->transaction);

        // проверка на кол-во подтверждений
        if ($info['confirmations'] < $this->confirmations) return false;

        // проверка на верный адрес и кол-во монет
        $result = false;
        foreach ($info['outputs'] as $output) {
            if (in_array($this->address, $output['addresses'])) {
                if (($output['value']/100000000) >= $this->value) $result = true;
            }
        }
        if ($result == false) return false;

        // проверка на дубликаты
        $exist = self::find()
            ->where(['transaction' => $this->transaction])
            ->andWhere(['<', 'id', $this->id ])
            ->exists();
        if ($exist) return false;

        return true;
    }


    /**
     * https://emercoin.mintr.org/tx/e5d186432f90e8c8b2a81c7ae1709204e16386224e7ba152b50ad17829f7819e
     *
     * @param string $txid хеш транзакции
     *
     * @return array
     * https://emercoin.mintr.org/tx/e5d186432f90e8c8b2a81c7ae1709204e16386224e7ba152b50ad17829f7819e
     *
     * @throws
     */
    public static function getTxInfo($txid)
    {
        $url = 'https://api.blockcypher.com';

        $client = new \yii\httpclient\Client(['baseUrl' => $url]);
        $content = $client->get('tx/'.$txid)->send()->content;
        require_once(\Yii::getAlias('@csRoot/services/simplehtmldom_1_5/simple_html_dom.php'));
        $content = str_get_html($content);
        /** @var \simple_html_dom_node[] $rows */
        $rows = $content->find('div.container/div.panel-default//tr');
        $output = [];
        for ($i = 1; $i < count($rows); $i++) {
            $item = $rows[$i];
            $v = $item->nodes[2];
            $v = str_replace(',','',$v);
            $address = $item->nodes[3]->text();
            $output[$address] = $v;
        }

        return $data;
    }

}
