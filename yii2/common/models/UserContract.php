<?php
namespace common\models;

use avatar\models\UserRegistration;
use common\components\providers\BTC;
use common\models\avatar\Currency;
use common\models\avatar\UserBill;
use common\models\avatar\UserBillDefault;
use cs\Application;
use cs\services\Security;
use cs\services\SitePath;
use cs\services\VarDumper;
use Imagine\Image\Box;
use yii\base\Exception;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;
use yii\helpers\FileHelper;
use yii\helpers\Url;
use cs\services\UploadFolderDispatcher;
use yii\imagine\Image;
use Imagine\Image\ManipulatorInterface;
use yii\web\IdentityInterface;


/**
 * @property integer   id
 * @property integer   user_id
 * @property integer   contract_id
 * @property string    name
 */
class UserContract extends ActiveRecord
{
    public static function tableName()
    {
        return 'user_contract';
    }

    public function rules()
    {
        return [
            [self::attributes(), 'safe'],
        ];
    }
}
