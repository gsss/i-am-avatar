<?php
/**
 * Created by PhpStorm.
 * User: s.arhangelskiy
 * Date: 12.04.2016
 * Time: 16:13
 */

namespace common\models;


use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\helpers\VarDumper;

/**
 *
 * @property int        id
 * @property int        value
 * @property int        time
 *
 * @package app\services
 */
class CryptoCap extends ActiveRecord
{
    public static function tableName()
    {
        return 'cryptocap';
    }

}