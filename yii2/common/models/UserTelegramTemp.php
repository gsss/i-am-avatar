<?php
/**
 * Created by PhpStorm.
 * User: god
 * Date: 28.02.2017
 * Time: 23:49
 */

namespace common\models;

use yii\db\ActiveRecord;

/**
 * @property integer id
 * @property string  username
 * @property string  email
 * @property integer chat_id
 * @property integer status
 * @property integer last_message_time
 *
 */
class UserTelegramTemp extends ActiveRecord
{
    public static function tableName()
    {
        return 'user_telegram_temp';
    }

    public static function add($fields)
    {
        if (!isset($fields['last_message_time'])) {
            $fields['last_message_time'] = time();
        }

        $item = new static($fields);
        $item->save();
        $item->id = self::getDb()->lastInsertID;

        return $item;

    }
}