<?php


namespace common\models\subscribe;


use yii\db\ActiveRecord;

/**
 *
 * @property int    id
 * @property string    name
 */
class UserSubscribeAction extends ActiveRecord
{
    public static function tableName()
    {
        return 'user_subscribe_action';
    }
    
    public static function add($fields)
    {
        $iam = new self($fields);
        $iam->save();
        $iam->id = \Yii::$app->db->lastInsertID;
        
        return $iam;
    }
}