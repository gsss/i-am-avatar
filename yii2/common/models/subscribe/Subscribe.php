<?php


namespace common\models\subscribe;


use yii\db\ActiveRecord;

/**
 *
 * @property int    id
 * @property int    date_insert
 * @property int    type        \app\services\Subscribe::TYPE_*
 * @property string html
 * @property string text
 * @property string subject
 * @property string image
 * @property string from_name
 * @property string from_email
 */
class Subscribe extends ActiveRecord
{
    public static function tableName()
    {
        return 'gs_subscribe';
    }
    
    public static function add($fields)
    {
        if (!isset($fields['date_insert'])) {
            $fields['date_insert'] = time();
        }
        $iam = new self($fields);
        $iam->save();
        $iam->id = \Yii::$app->db->lastInsertID;
        
        return $iam;
    }
}