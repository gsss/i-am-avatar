<?php
/**
 * Created by PhpStorm.
 * User: Бог-Творец
 * Date: 01.05.2016
 * Time: 12:40
 */

namespace common\models\subscribe;


use yii\db\ActiveRecord;

/**
 *
 * @property int counter - кол-во разосланных писем
 * @property int time - время добавления статистики
 */
class SubscribeStat extends ActiveRecord
{
    public function rules()
    {
        return [
            [['counter', 'time'], 'required'],
            [['counter', 'time'], 'integer'],
        ];
    }

    public static function tableName()
    {
        return 'gs_subscribe_stat';
    }
} 