<?php


namespace common\models\subscribe;


use yii\db\ActiveRecord;

/**
 *
 * @property int    id
 * @property int    user_id
 * @property int    action_id
 * @property int    is_mail
 * @property int    is_telegram
 */
class UserSubscribe extends ActiveRecord
{
    public static function tableName()
    {
        return 'user_subscribe';
    }
    
    public static function add($fields)
    {
        $iam = new self($fields);
        $iam->save();
        $iam->id = \Yii::$app->db->lastInsertID;
        
        return $iam;
    }
}