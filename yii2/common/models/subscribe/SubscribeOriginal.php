<?php


namespace common\models\subscribe;


use yii\db\ActiveRecord;

/**
 * @property int    date_insert
 * @property string html
 * @property string text
 * @property string subject
 */
class SubscribeOriginal extends ActiveRecord
{
    public static function tableName()
    {
        return 'gs_subscribe_original';
    }
    
    public static function add($fields)
    {
        $iam = new self($fields);
        $iam->save();
        $iam->id = \Yii::$app->db->lastInsertID;
        
        return $iam;
    }
}