<?php
/**
 * Created by PhpStorm.
 * User: s.arhangelskiy
 * Date: 12.04.2016
 * Time: 16:13
 */

namespace common\models\language;

use common\services\Debugger;

use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;
use yii\helpers\VarDumper;

/**
 * Содержит информацию о переменной языка ктороая содержит в себе переводы
 *
 * @property int         id
 * @property string(32)  category
 * @property string      message - Строковый идентификатор строки, по которой производится выборка из группы
 * @property int         type - тип 1 - строка, 2 - картинка, 3 - HTML
 * @property int         is_locked - заблокировано админом? 0 - нет, можно редактировать, 1 - да, редактировать нельзя
 * @property int         is_to_delete - Галочка "на удаление". 0 - обчный режим, переменные хранятся в таблице `message`, 1 - строка помечена, то есть строки хранятся в таблице `message_to_delete` и показываются как помеченные "на удаление".
 * @property string(255) comment - коментарий для строки
 *
 * Class SourceMessage
 * @package app\services
 */
class SourceMessage extends ActiveRecord
{
    const TYPE_STRING = 1;
    const TYPE_IMAGE  = 2;
    const TYPE_HTML   = 3;
    const TYPE_FILE   = 4;

    public static function tableName()
    {
        return 'source_message';
    }
}