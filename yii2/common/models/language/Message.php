<?php
/**
 * Created by PhpStorm.
 * User: s.arhangelskiy
 * Date: 12.04.2016
 * Time: 16:13
 */

namespace common\models\language;


use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;
use yii\helpers\VarDumper;

/**
 * Содержит информацию о переменной языка ктороая содержит в себе переводы
 *
 * @property int id
 * @property string(16) language
 * @property string translation - перевод
 *
 * Class SourceMessage
 * @package app\services
 */
class Message extends ActiveRecord
{
    public static function tableName()
    {
        return 'message';
    }
}