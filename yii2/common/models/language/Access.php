<?php
/**
 * Created by PhpStorm.
 * User: s.arhangelskiy
 * Date: 12.04.2016
 * Time: 16:13
 */

namespace common\models\language;

use common\services\Debugger;

use yii\db\ActiveRecord;
use yii\db\Query;
use yii\helpers\ArrayHelper;
use yii\helpers\VarDumper;

/**
 * Содержит информацию о языке
 *
 * @property int id
 * @property string name
 * @property string code
 * @property int parent_id
 *
 *
 * Class Language
 * @package app\services
 */
class Access extends ActiveRecord
{
    public static function tableName()
    {
        return 'languages_access';
    }

    public function rules()
    {
        return [
            [['language_id', 'user_id'], 'required'],
            [['language_id', 'user_id'], 'integer', 'min' => 0],
        ];
    }

}