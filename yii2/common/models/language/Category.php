<?php
/**
 * Created by PhpStorm.
 * User: s.arhangelskiy
 * Date: 12.04.2016
 * Time: 16:13
 */

namespace common\models\language;

use common\services\Debugger;

use yii\db\ActiveRecord;
use yii\db\Query;
use yii\helpers\ArrayHelper;
use yii\helpers\VarDumper;

/**
 * Содержит информацию о языке
 *
 * @property int id
 * @property string name
 * @property string code
 * @property int parent_id
 *
 *
 * Class Language
 * @package app\services
 */
class Category extends ActiveRecord
{
    const TYPE_STRING = 1;
    const TYPE_IMAGE = 2;
    const TYPE_HTML = 3;
    const TYPE_FILE = 4;

    public static function tableName()
    {
        return 'languages_category_tree';
    }

    public function rules()
    {
        return [
            [['name'], 'required'],
            ['name', 'string', 'max' => 255],
            ['code', 'string', 'max' => 32],
            ['parent_id', 'integer', 'min' => 0],
        ];
    }


    /**
     * Выдает список строк этой категории
     * @return Query
     */
    public function getMessages($isNested = false)
    {
        $query = SourceMessage::find();
        $codeArray = [];
        // выбираю массив категорий для `parentId = $this->id`
        if ($isNested) {
            $tree = Category::getRows(['parentId' => $this->id, 'selectFields' => 'id, code']);
            $codeArray = $this->getCodeArray($tree);
        }
        $codeArray[] = $this->code;
        $query->where(['source_message.category' => $codeArray]);

        return $query;
    }

    /**
     * @return array [
     *  [
     *      'id' => 1
     *      'name' => 'root'
     *      'code' => 'root'
     *  ],
     *  [
     *      'id' => 2
     *      'name' => 'cat1'
     *      'code' => 'cat1'
     *  ],
     *  [
     *      'id' => 3
     *      'name' => 'sub_cat1'
     *      'code' => 'sub_cat1'
     *  ],
     * ]
     */
    public function getPath($parent = [])
    {
        if (is_null($this->parent_id)) {
            return [];
        }
        $cat = self::findOne($this->parent_id);
        return ArrayHelper::merge($cat->getPath(), [$cat->getAttributes()]);
    }

    /**
     * Возвращает элементы списка в виде дерева
     *
     * @param array $options
     * - selectFields - string|array - поля для выборки, по умолчанию id,name
     * - parentId - int - идентификатор ветки-родителя для которой и производится выборка дерева
     * - order - string|array - параметр сортировки который передается в ->orderBy()
     *
     * @return array
     * [[
     *  'id' =>
     *  'name' =>
     *  'nodes' => array
     * ], ... ]
     */
    public static function getRows($options = [])
    {
        $selectFields = ArrayHelper::getValue($options, 'selectFields', 'id,name');
        $parentId = ArrayHelper::getValue($options, 'parentId', null);
        $order = ArrayHelper::getValue($options, 'order', null);

        $query = self::find()
            ->select($selectFields)
            ->where(['parent_id' => $parentId])
            ->orderBy(['sort_index' => SORT_ASC]);
        if ($order) $query->orderBy($order);

        $rows = $query->asArray()->all();
        for ($i = 0; $i < count($rows); $i++) {
            $item = &$rows[ $i ];
            $options['parentId'] = $item['id'];
            $rows2 = self::getRows($options);
            if (count($rows2) > 0) {
                $item['nodes'] = $rows2;
            }
        }

        return $rows;
    }



    /**
     * Возвращает все ids подкатегорий для ноды
     *
     * @param array $node
     * [
     *      'id' =>
     *      'code' =>
     *      'nodes' => [
     *
     *                 ]
     * ]
     *
     * @return array
     */
    private function getIds($nodes)
    {
        $ret = [];
        foreach($nodes as $node1) {
            $ret = ArrayHelper::merge($ret, $this->_getIds($node1));
        }

        return $ret;
    }

    /**
     * Возвращает все ids подкатегорий для ноды
     *
     * @param array $node
     * [
     *      'id' =>
     *      'name' =>
     *      'nodes' => [
     *
     *                 ]
     * ]
     *
     * @return array
     */
    private function getCodeArray($nodes)
    {
        $ret = [];
        foreach($nodes as $node1) {
            $ret = ArrayHelper::merge($ret, $this->_getCodeArray($node1));
        }

        return $ret;
    }

    /**
     * Возвращает все ids подкатегорий для ноды
     *
     * @param array $node
     * [
     *      'id' =>
     *      'name' =>
     *      'nodes' => [
     *
     *                 ]
     * ]
     *
     * @return array [1,2,3,...]
     */
    private function _getIds($node)
    {
        $ret = [];
        if (isset($node['nodes'])) {
            foreach($node['nodes'] as $node1) {
                $ret = ArrayHelper::merge($ret, $this->_getIds($node1));
            }
        }
        $ret[] = $node['id'];

        return $ret;
    }

    /**
     * Возвращает все `code` подкатегорий для ноды
     *
     * @param array $node
     * [
     *      'id' =>
     *      'name' =>
     *      'nodes' => [
     *
     *                 ]
     * ]
     *
     * @return array [1,2,3,...]
     */
    private function _getCodeArray($node)
    {
        $ret = [];
        if (isset($node['nodes'])) {
            foreach($node['nodes'] as $node1) {
                $ret = ArrayHelper::merge($ret, $this->_getCodeArray($node1));
            }
        }
        $ret[] = $node['code'];

        return $ret;
    }

    /**
     * Удаляет все строки для категории
     * и саму категорию c подветками
     *
     * @return bool|int|void
     */
    public function delete()
    {
        $tree = Category::getRows(['parentId' => $this->id]);
        $ids = $this->getIds($tree);
        $ids[] = $this->id;
        foreach($ids as $id) {
            Category::findOne($id)->_delete();
        }
    }

    /**
     * Удаляет все строки для категории
     * и саму категорию без подветок
     */
    public function _delete()
    {
        $ids = SourceMessage::find()->where(['category' => $this->code])->select(['id'])->column();
        Message::deleteAll(['in', 'id', $ids]);
        SourceMessage::deleteAll(['in', 'id', $ids]);
        parent::delete();
    }

}