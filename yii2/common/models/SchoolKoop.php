<?php

namespace common\models;

use common\components\Card;
use common\models\avatar\QrCode;
use common\models\avatar\UserBill;
use common\models\avatar\UserBillMerchant;
use common\models\SendLetter;
use cs\Application;
use cs\services\Str;
use iAvatar777\services\FormAjax\ActiveRecord;
use Yii;
use yii\base\Model;
use yii\db\Query;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\VarDumper;

/**
 * @param int $id
 * @param int $input_amount
 */
class SchoolKoop extends \yii\db\ActiveRecord
{
    public static function tableName()
    {
        return 'school_koop';
    }
}
