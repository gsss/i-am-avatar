<?php
namespace common\models;

use avatar\models\UserRegistration;
use avatar\modules\UniSender\UniSender;
use common\components\providers\BTC;
use common\models\avatar\Currency;
use common\models\avatar\UserBill;
use common\models\avatar\UserBillDefault;
use common\models\avatar\UserBinance;
use cs\Application;
use cs\services\Security;
use cs\services\SitePath;
use cs\services\VarDumper;
use Imagine\Image\Box;
use yii\base\Exception;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;
use yii\helpers\FileHelper;
use yii\helpers\Url;
use cs\services\UploadFolderDispatcher;
use yii\imagine\Image;
use Imagine\Image\ManipulatorInterface;
use yii\web\IdentityInterface;


/**
 * User model
 *
 * @property integer   id
 * @property string    email
 * @property string    phone
 * @property string    name
 * @property integer   email_is_confirm
 * @property integer   school_id
 */
class User2 extends ActiveRecord
{
    public static function tableName()
    {
        return 'user2';
    }

    public static function add($fields)
    {
        if (!isset($fields['created_at'])) $fields['created_at'] = time();
        if (!isset($fields['school_id'])) $fields['school_id'] = 1;
        $i = new static($fields);
        $i->save();
        $i->id = self::getDb()->lastInsertID;

        return $i;
    }
}
