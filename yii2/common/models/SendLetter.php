<?php
/**
 * Created by PhpStorm.
 * User: Бог-Творец
 * Date: 02.09.2016
 * Time: 9:31
 */

namespace common\models;

use cs\services\VarDumper;
use yii\base\Exception;
use yii\db\ActiveRecord;

/**
 * @property int $id
 * @property int $time
 * @property string $name
 * @property string $email
 * @property string $text
 */
class SendLetter extends ActiveRecord
{
    public static function tableName()
    {
        return 'gs_send_letters';
    }

    public function rules()
    {
        return [
            [[
                'name',
                'email',
                'text',
                'time',
            ], 'required'],
            [[
                'name',
                'email',
            ], 'string', 'max' => 255],
            [[
                'text',
            ], 'string', 'max' => 4000],
            [[
                'email',
            ], 'email'],
            [[
                'time',
            ], 'integer'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id'    => 'ID',
            'name'  => 'Имя',
            'email' => 'Почта',
            'text'  => 'Текст письма',
            'time'  => 'Создано',
        ];
    }

    public static function add($fields)
    {
        if (!isset($fields['time'])) {
            $fields['time'] = time();
        }
        $new = new self($fields);
        $ret = $new->save();
        if (!$ret) {
            throw new Exception('Не удалось сохранить. '. \yii\helpers\VarDumper::dumpAsString($new->errors));
        }
        $new->id = self::getDb()->lastInsertID;

        return $new;
    }
} 