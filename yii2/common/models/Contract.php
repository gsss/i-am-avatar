<?php

namespace common\models;

use Yii;
use common\extensions\XSSActiveRecord;
use yii\db\ActiveRecord;

/**
 * @property integer    id
 * @property string     address
 * @property string     image
 * @property string     name
 * @property string     abi
 */
class Contract extends ActiveRecord
{

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'contract';
    }

}
