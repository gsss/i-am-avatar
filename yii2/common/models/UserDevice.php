<?php
/**
 * Created by PhpStorm.
 * User: Бог-Творец
 * Date: 06.01.2017
 * Time: 17:14
 */

namespace common\models;



use yii\db\ActiveRecord;

/**
 * @property int    id
 * @property int    created_at
 * @property int    user_id
 * @property string name
 *
 * Class UserDevice
 * @package common\models
 */
class UserDevice extends ActiveRecord
{
    public static function tableName()
    {
        return 'user_device_list';
    }

    /**
     *
     * @param array $fields
     *
     * @return \common\models\UserDevice
     */
    public static function add($fields)
    {
        if (!isset($fields['created_at'])) {
            $fields['created_at'] = time();
        }
        $item = new static($fields);
        $item->save();
        $item->id = static::getDb()->lastInsertID;

        return $item;
    }
}