<?php
/**
 * Created by PhpStorm.
 * User: s.arhangelskiy
 * Date: 12.04.2016
 * Time: 16:13
 */

namespace common\models;


use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;
use yii\helpers\Url;
use yii\helpers\VarDumper;

/**
 *
 * @property int        id
 * @property string     config
 *
 */
class CompanyCustomizeItem extends ActiveRecord
{
    const PREFIX_MEMCACHE = 'CompanyCustomize';

    public static function tableName()
    {
        return 'company_customize';
    }

    /**
     * @param $id
     * @return array
     * @throws
     */
    public static function getItem($id)
    {
        $value = \Yii::$app->cache->get(self::PREFIX_MEMCACHE . $id);
        if ($value === false) {
            $row = self::findOne($id);
            $value = Json::decode($row->config);
        }

        return $value;
    }

    /**
     * Выдает данные из кеша
     *
     * @return array
     * [
     *     'cabinet.new-earth.space' => [
     *          'id' => 1,
     *          'domain-name' => 'cabinet.new-earth.space'
     *          //...
     *      ],
     * ]
     * @throws
     */
    public static function getAll()
    {
        $value = \Yii::$app->cache->get(self::PREFIX_MEMCACHE);
        if ($value === false) {
            $value = self::_getAll();
        }

        return $value;
    }

    /**
     * Выдает из БД данные
     *
     * @return array
     * [
     *     'cabinet.new-earth.space' => [
     *          'id' => 1,
     *          'domain-name' => 'cabinet.new-earth.space'
     *          //...
     *      ],
     * ]
     * @throws
     */
    public static function _getAll()
    {
        $rows = self::find()->all();
        $data = [];
        /** @var \common\models\CompanyCustomizeItem $row */
        foreach ($rows as $row) {
            $item = Json::decode($row->config);
            $item['id'] = $row->id;
            if (isset($item['domain-name'])) {
                $data[$item['domain-name']] = $item;
            }
        }

        return $data;
    }

}