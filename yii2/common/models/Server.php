<?php
/**
 * Created by PhpStorm.
 * User: god
 * Date: 28.02.2017
 * Time: 23:49
 */

namespace common\models;

use common\models\avatar\UserBill;
use common\models\piramida\Billing;
use common\models\statistic\StatisticDb;
use yii\db\ActiveRecord;

/**
 * Class Card
 *
 * @property integer id
 * @property string  name
 * @property string  ip
 * @property string  config
 * @property string  url
 * @property string  api
 * @property string  hosting
 * @property string  is_php
 * @property integer is_mysql
 *
 * @package common\models
 */
class Server extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'servers';
    }


}