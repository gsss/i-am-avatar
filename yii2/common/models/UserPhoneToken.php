<?php
/**
 * Created by PhpStorm.
 * User: Бог-Творец
 * Date: 06.01.2017
 * Time: 17:14
 */

namespace common\models;



use yii\db\ActiveRecord;

/**
 * @property int    id
 * @property int    finish_time
 * @property string token
 * @property string model
 *
 * Class UserDevice
 * @package common\models
 */
class UserPhoneToken extends ActiveRecord
{
    public static function tableName()
    {
        return 'user_phone_token';
    }
}