<?php
namespace common\models;

use avatar\models\UserRegistration;
use avatar\modules\UniSender\UniSender;
use common\components\providers\BTC;
use common\models\avatar\Currency;
use common\models\avatar\UserBill;
use common\models\avatar\UserBillDefault;
use common\models\avatar\UserBinance;
use cs\Application;
use cs\services\Security;
use cs\services\SitePath;
use cs\services\VarDumper;
use Imagine\Image\Box;
use yii\base\Exception;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;
use yii\helpers\FileHelper;
use yii\helpers\Url;
use cs\services\UploadFolderDispatcher;
use yii\imagine\Image;
use Imagine\Image\ManipulatorInterface;
use yii\web\IdentityInterface;


/**
 * @property integer   id
 * @property integer   created_at
 * @property integer   user_root_id
 * @property string    hash
 * @property string    card
 * @property string    password_hash
 */
class UserRegistrationCard extends ActiveRecord
{
    public static function tableName()
    {
        return 'user_registration_card';
    }

    public static function add($fields)
    {
        if (!isset($fields['created_at'])) $fields['created_at'] = time();
        $i = new static($fields);
        $i->save();
        $i->id = self::getDb()->lastInsertID;

        return $i;
    }
}
