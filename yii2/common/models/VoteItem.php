<?php
/**
 * Created by PhpStorm.
 * User: s.arhangelskiy
 * Date: 12.04.2016
 * Time: 16:13
 */

namespace common\models;

use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\helpers\VarDumper;

/**
 *
 * @property int        id
 * @property int        user_id
 * @property int        answer_id
 * @property int        created_at
 * @property string     comment
 * @property string     hash
 * @property string     sign
 *
 */
class VoteItem extends ActiveRecord
{
    public static function tableName()
    {
        return 'vote_item';
    }

    public function hash()
    {
        $data = [
            $this->id,
            $this->user_id,
            $this->answer_id,
            $this->created_at,
            $this->comment,
        ];

        return hash('sha256', join('', $data));
    }
}