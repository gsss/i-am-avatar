<?php


namespace common\models;

use yii\db\ActiveRecord;

/**
 * Class CardSchoolLink
 *
 * @property integer id
 * @property integer card_id
 * @property integer school_id
 *
 * @package common\models
 */
class CardSchoolLink extends ActiveRecord
{
    public static function tableName()
    {
        return 'card_school_link';
    }


    /**
     * @param $fields
     *
     * @return static
     */
    public static function add($fields)
    {
        $i = new static($fields);
        $i->save();
        $i->id = self::getDb()->lastInsertID;

        return $i;
    }
}