<?php
/**
 * Created by PhpStorm.
 * User: Бог-Творец
 * Date: 06.01.2017
 * Time: 17:14
 */

namespace common\models;



use yii\db\ActiveRecord;

/**
 * @property int    id
 * @property int    created_at
 * @property int    owner_user_id
 * @property int    owner_billing_id
 * @property int    partner_billing_id
 * @property int    partner_user_id
 * @property int    finish
 * @property int    status              0 - только создана заявка, 1 - отозваны токены
 * @property string amount
 * @property string hash
 *
 */
class UserZalog extends ActiveRecord
{
    const STATUS_CREATED = 0;
    const STATUS_RECALL = 1;

    public static function tableName()
    {
        return 'user_zalog';
    }

    /**
     *
     * @param array $fields
     *
     * @return \common\models\UserZalog
     */
    public static function add($fields)
    {
        if (!isset($fields['created_at'])) {
            $fields['created_at'] = time();
        }
        $item = new static($fields);
        $item->save();
        $item->id = static::getDb()->lastInsertID;

        return $item;
    }
}