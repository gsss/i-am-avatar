<?php
/**
 * Created by PhpStorm.
 * User: god
 * Date: 28.02.2017
 * Time: 23:49
 */

namespace common\models;

use yii\db\ActiveRecord;

/**
 * @property integer id
 * @property string  uid
 * @property string  passport_num
 * @property string  end_date
 * @property string  born_date
 * @property integer created_at
 * @property integer user_id
 * @property integer company_id
 *
 * @package common\models
 */
class CovCert extends ActiveRecord
{
    public static function tableName()
    {
        return 'cov_cert';
    }
}