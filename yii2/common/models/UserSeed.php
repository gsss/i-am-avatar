<?php
/**
 * Created by PhpStorm.
 * User: Бог-Творец
 * Date: 06.01.2017
 * Time: 17:14
 */

namespace common\models;



use yii\db\ActiveRecord;

/**
 * @property int    id
 * @property string seeds
 *
 * @package common\models
 */
class UserSeed extends ActiveRecord
{
    public static function tableName()
    {
        return 'user_seeds';
    }

    /**
     * Добаляет поля в таблицу, если в массиве не указано поле id то оно берется из Yii::app->user->id
     *
     * @param array | string $fields
     *
     * @return \common\models\UserSeed
     */
    public static function add($fields)
    {
        if (is_array($fields)) {
            if (!isset($fields['id'])) {
                $fields['id'] = \Yii::$app->user->id;
            }
        } else {
            $seeds = $fields;
            $fields = [
                'id'    => \Yii::$app->user->id,
                'seeds' => $seeds,
            ];
        }
        $item = new static($fields);
        $item->save();

        return $item;

    }

    /**
     * Проверяет есть ли у пользователя SEEDS
     * Если $userId == null то подразумевается что пользователь тот что в системе, есди пользователь не авторизован то будет возвращен false
     *
     * @param null | int  $userId
     *
     * @return bool
     */
    public static function has($userId = null)
    {
        if (is_null($userId)) {
            if (\Yii::$app->user->isGuest) {
                return false;
            } else {
                $userId = \Yii::$app->user->id;
            }
        }

        return self::find()->where(['id' => $userId])->exists();
    }
}