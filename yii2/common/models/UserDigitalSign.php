<?php
/**
 * Created by PhpStorm.
 * User: Бог-Творец
 * Date: 06.01.2017
 * Time: 17:14
 */

namespace common\models;


use common\models\avatar\UserBill;
use cs\Application;
use cs\services\VarDumper;
use yii\db\ActiveRecord;

/**
 * @property int        id
 * @property int        user_id         идентификатор хозяина
 * @property int        type_id         Тип хранения подписи: 0 - пользователь хранит у себя, 1 - ключ хранится в системе, 2 - версия 2. По умолчанию 0
 * @property int        created_at      мгновение добавления записи
 * @property string     address
 * @property string     private_key
 * @property string     address_uncompressed
 * @property string     public_key_uncompressed
 *
 */
class UserDigitalSign extends ActiveRecord
{
    const TYPE_SAVE_USER = 0;
    const TYPE_SAVE_SYSTEM = 1;
    const TYPE_VER2 = 2;

    public static function tableName()
    {
        return 'user_digital_sign';
    }

    public function behaviors()
    {
        return [
            [
                'class' => '\yii\behaviors\TimestampBehavior',
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at'],
                ],
                // если вместо метки времени UNIX используется datetime:
                // 'value' => new Expression('NOW()'),
            ],
        ];
    }

    /**
     *
     * @param array $fields
     *
     * @return \common\models\UserDigitalSign
     */
    public static function add($fields)
    {
        $item = new static($fields);
        $ret = $item->save();
        $item->id = static::getDb()->lastInsertID;

        return $item;
    }

    public function hasPK()
    {
        return !Application::isEmpty($this->private_key);
    }

    public static function isPK($user_id)
    {
        $i = self::findOne(['user_id' => $user_id]);
        if (is_null($i)) return false;

        return !Application::isEmpty($i->private_key);
    }

    /**
     * Выдает хозяина документа
     *
     * @return \common\models\UserAvatar
     */
    public function getUser()
    {
        return UserAvatar::findOne($this->user_id);
    }

    /**
     * Выдает приватный ключ
     *
     * @param int $id
     * @return string
     */
    public static function getPK($id)
    {
        $o = self::findOne(['user_id' => $id]);
        $base64 = $o->private_key;
        $unlockKey = \Yii::$app->params['unlockKey'];
        $nonce = '1234567890123456';

        $key = UserBill::passwordToKey32($unlockKey);
        $ciphertext2 = base64_decode($base64);
        $s = openssl_decrypt($ciphertext2, 'AES-256-CTR', $key, true, $nonce); // OpenSSL

        return $s;
    }
}