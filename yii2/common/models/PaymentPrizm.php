<?php

namespace common\models;

use avatar\modules\ETH\ServiceEtherScan;
use cs\services\VarDumper;
use Yii;
use yii\base\Exception;
use yii\db\ActiveRecord;
use yii\helpers\Json;
use yii\helpers\StringHelper;

/**
 * @property integer    id
 * @property integer    billing_id
 * @property float      value
 * @property string     action
 * @property string     transaction
 * @property integer    transaction_at когда была укзана транзакция как оплаченная
 * @property string     from
 * @property string     address
 */
class PaymentPrizm extends ActiveRecord implements PaymentI
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'payments_prizm';
    }

    public function setTransaction($txid)
    {
        $this->transaction = $txid;
    }

    public function saveTransaction($txid)
    {
        $this->transaction = $txid;
        $this->transaction_at = time();
        $this->save();
    }

    public function getTransaction()
    {
        return $this->transaction;
    }

    public function getAddress()
    {
        return $this->address;
    }

    public function getTransactionAt()
    {
        return $this->transaction_at;
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'          => 'ID',
            'billing_id'  => 'Счет',
            'value'       => 'Число монет',
            'action'      => 'ссылка на платеж в мерчанте',
            'transaction' => 'Трназакция',
            'from'        => 'от кого',
        ];
    }

}
