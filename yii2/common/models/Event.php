<?php

namespace common\models;

use app\models\SiteContentInterface;
use app\models\SiteUpdateItem;
use app\models\SubscribeItem;
use cs\services\BitMask;
use yii\db\ActiveRecord;
use yii\db\Query;
use app\services\Subscribe;

/**
 * @property int    id
 * @property string name
 * @property date   start_date
 * @property time   start_time
 * @property date   end_date
 * @property time   end_time
 * @property string content
 * @property string image
 * @property int    user_id
 * @property int    date_insert
 * @property string link
 * @property date   date
 * @property int    is_added_site_update
 * @property int    moderation_status
 * @property int    union_id
 * @property double place_lng
 * @property double place_lat
 * @property double place_address
 *
 * Class Event
 * @package common\models
 */
class Event extends ActiveRecord implements SiteContentInterface
{
    public static function tableName()
    {
        return 'gs_events';
    }

    public function getImage($isScheme = false)
    {
        $image = $this->image;
        if ($image == '') return '';

        return \yii\helpers\Url::to($this->image, $isScheme);
    }

    public function getLink($isScheme = false)
    {
        return \yii\helpers\Url::to(['calendar/events_item', 'id' => $this->id], $isScheme);
    }

    /**
     * @inheritdoc
     */
    public function getMailContent()
    {
        // шаблон
        $view = 'subscribe/event';
        // опции шаблона
        $options = [
            'item' => $this,
            'user' => \Yii::$app->user->identity,
        ];

        /** @var \yii\swiftmailer\Mailer $mailer */
        $mailer = \Yii::$app->mailer;
        $text = $mailer->render('text/' . $view, $options, 'layouts/text/subscribe');
        $html = $mailer->render('html/' . $view, $options, 'layouts/html/subscribe');

        $subscribeItem = new SubscribeItem();
        $subscribeItem->subject = $this->name;
        $subscribeItem->html = $html;
        $subscribeItem->text = $text;
        $subscribeItem->type = Subscribe::TYPE_SITE_UPDATE;

        return $subscribeItem;
    }

    /**
     * @inheritdoc
     */
    public function getSiteUpdateItem($isScheme = false)
    {
        $siteUpdateItem = new SiteUpdateItem();
        $siteUpdateItem->name = $this->name;
        $siteUpdateItem->image = $this->getImage($isScheme);
        $siteUpdateItem->link = $this->getLink($isScheme);
        $siteUpdateItem->type = SiteUpdateItem::TYPE_EVENT;

        return $siteUpdateItem;
    }

}