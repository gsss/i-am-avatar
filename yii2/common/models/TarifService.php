<?php
/**
 * Created by PhpStorm.
 * User: god
 * Date: 28.02.2017
 * Time: 23:49
 */

namespace common\models;

use common\models\avatar\Currency;
use yii\db\ActiveRecord;

/**
 * Class TarifService
 *
 * @property integer id
 * @property integer tarif_id
 * @property integer company_id
 * @property date paid_till
 *
 * @package common\models
 */
class TarifService extends ActiveRecord
{

    public static function add($fields)
    {
        $i = new static($fields);
        $i->save();
        $i->id = self::getDb()->lastInsertID;

        return $i;
    }
}