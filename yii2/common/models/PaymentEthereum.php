<?php

namespace common\models;

use avatar\modules\ETH\ServiceEtherScan;
use cs\services\VarDumper;
use Yii;
use yii\base\Exception;
use yii\db\ActiveRecord;
use yii\helpers\Json;
use yii\helpers\StringHelper;

/**
 * @property integer    id
 * @property integer    billing_id
 * @property integer    value
 * @property string     action
 * @property string     transaction
 * @property integer    transaction_at когда была укзана транзакция как оплаченная
 * @property string     from
 * @property string     address
 */
class PaymentEthereum extends ActiveRecord implements PaymentI
{
    const TYPE_SEND_ETHER = 1;
    const TYPE_CONTRACT_EXECUTE = 2;
    const TYPE_CONTRACT_CREATE = 3;

    public $confirmations = 12;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'payments_ethereum';
    }

    public function setTransaction($txid)
    {
        $this->transaction = $txid;
    }

    public function saveTransaction($txid)
    {
        $this->transaction = $txid;
        $this->transaction_at = time();
        $this->save();
    }

    public function getTransaction()
    {
        return $this->transaction;
    }

    public function getAddress()
    {
        return $this->address;
    }

    public function getTransactionAt()
    {
        return $this->transaction_at;
    }

    /**
     * @param mixed $condition
     * @return \common\models\PaymentBitCoin
     * @throws Exception
     */
    public static function findOne($condition)
    {
        $item = parent::findOne($condition);
        if (is_null($item)) {
            throw new Exception('Не найден платеж');
        }
        return $item;
    }


    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'          => 'ID',
            'billing_id'  => 'Счет',
            'value'       => 'Число монет',
            'action'      => 'ссылка на платеж в мерчанте',
            'transaction' => 'Трназакция',
            'from'        => 'от кого',
        ];
    }

    /**
     * Выдает ссылку на информацию о транзакции
     */
    public static function getTxLink($txid)
    {
        return (new ServiceEtherScan())->getUrl('/tx/' . $txid);
    }

    /**
     * Проверет транзакцию на оплаченность
     *
     * @return array
     * [
     *  bool - true - валидная, false - не валидная
     *  int  - номер ошибки
     *          1 - недостаточно подтверждений
     *          2 - неверный адрес получателя
     *          3 - недостаточно монет
     *          4 - не найдена транзакция в блокчейне
     *          6 - не верный тип транзакции
     *          7 - такая транзакция уже есть
     * ]
     */
    public function isTransactionPaidBilling()
    {
        $info = $this->getTransactionInfo($this->transaction);
        if (is_null($info)) return [false, 4];

        // проверка на перевод эфира
        if ($info['type'] != self::TYPE_SEND_ETHER) return [false, 6];

        // проверка на кол-во подтверждений
        if ($info['confirmations'] < $this->confirmations) return [false, 1];

        // проверка на верный адрес
        if ($info['to'] != $this->address) return [false, 2];

        // проверка на кол-во монет
        if ($info['amount'] < $this->value) return [false, 3];

        // проверка на дубликаты
        $exist = self::find()
            ->where(['transaction' => $this->transaction])
            ->andWhere(['<', 'id', $this->id ])
            ->exists();
        if ($exist) return [false, 7];

        return [true];
    }


    /**
     * @param string $txid хеш транзакции
     *
     * @return array | null - null выдается если транзакция не найдена
     * [
     *      'from'          => $from,
     *      'confirmations' => $confirmations,
     *      'to'            => $to,
     *      'amount'        => $amount,
     *      'type'          => self::TYPE_*,
     * ]
     *
     * @throws
     */
    public function getTransactionInfo($txid)
    {
        $url = (new ServiceEtherScan())->getUrl();
        $client = new \yii\httpclient\Client(['baseUrl' => $url]);
        $content = $client->get('tx/' . $txid)->send()->content;
        require_once(\Yii::getAlias('@common/app/services/simplehtmldom_1_5/simple_html_dom.php'));
        $content = str_get_html($content);
        /** @var \simple_html_dom_node $rows */
        $rows = $content->find('#ContentPlaceHolder1_maintable/div.cbs');
        if (count($rows) == 0) return null;

        // $from
        $from = trim($rows[3]->text());

        // $confirmations
        $c = explode('(', $rows[1]->text());
        $c = explode(' ', $c[1]);
        $confirmations = $c[0];

        // $to
        $to = trim($rows[4]->text());

        // $amount
        $amount = (float)explode(' ', trim($rows[5]->text()))[0];

        // получаю тип
        if (StringHelper::startsWith($to, 'Contract')) {
            $type = self::TYPE_CONTRACT_EXECUTE;
        } else if (StringHelper::startsWith($to, '[Contract')) {
            $type = self::TYPE_CONTRACT_CREATE;
        } else {
            $type = self::TYPE_SEND_ETHER;
        }

        return [
            'from'          => $from,
            'confirmations' => $confirmations,
            'to'            => $to,
            'amount'        => $amount,
            'type'          => $type,
        ];
    }

    /**
     * @param string $txid хеш транзакции
     *
     * @return array | null - null выдается если транзакция не найдена
     * [
     *      'from'          => $from,
     *      'confirmations' => $confirmations,
     *      'to'            => $to,
     *      'amount'        => $amount,
     *      'type'          => self::TYPE_*,
     * ]
     *
     * @throws
     */
    public function getTransactionInfo2($txid)
    {
        $url = 'https://etherchain.org/api';
        $client = new \yii\httpclient\Client(['baseUrl' => $url]);
        $content = $client->get('tx/' . $txid)->send()->content;
        $data = Json::decode($content);
        VarDumper::dump($data);

        require_once(\Yii::getAlias('@common/app/services/simplehtmldom_1_5/simple_html_dom.php'));
        $content = str_get_html($content);
        /** @var \simple_html_dom_node $rows */
        $rows = $content->find('#ContentPlaceHolder1_maintable/div.cbs');
        if (count($rows) == 0) return null;

        // $from
        $from = trim($rows[3]->text());

        // $confirmations
        $c = explode('(', $rows[1]->text());
        $c = explode(' ', $c[1]);
        $confirmations = $c[0];

        // $to
        $to = trim($rows[4]->text());

        // $amount
        $amount = (float)explode(' ', trim($rows[5]->text()))[0];

        // получаю тип
        if (StringHelper::startsWith($to, 'Contract')) {
            $type = self::TYPE_CONTRACT_EXECUTE;
        } else if (StringHelper::startsWith($to, '[Contract')) {
            $type = self::TYPE_CONTRACT_CREATE;
        } else {
            $type = self::TYPE_SEND_ETHER;
        }

        return [
            'from'          => $from,
            'confirmations' => $confirmations,
            'to'            => $to,
            'amount'        => $amount,
            'type'          => $type,
        ];
    }



}
