<?php

namespace common\models;

use common\components\Card;
use common\models\avatar\QrCode;
use common\models\avatar\UserBill;
use common\models\avatar\UserBillMerchant;
use common\models\SendLetter;
use cs\Application;
use cs\services\Str;
use iAvatar777\services\FormAjax\ActiveRecord;
use Yii;
use yii\base\Model;
use yii\db\Query;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\VarDumper;

/**
 * @property int id
 * @property int school_id
 * @property int currency_id          dbWallet.currency.id
 * @property int wallet_id            Кошелек общий
 * @property int budjet_currency_id   валюта для бюджета (что будет списываться) dbWallet.currency.id
 */
class SchoolCoin extends \yii\db\ActiveRecord
{
    public static function tableName()
    {
        return 'school_coin';
    }
}
