<?php
/**
 * Created by PhpStorm.
 * User: s.arhangelskiy
 * Date: 12.04.2016
 * Time: 16:13
 */

namespace common\models;

use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\helpers\VarDumper;

/**
 *
 * @property int        id
 * @property string     name
 * @property string     link
 * @property string     image
 * @property string     description
 * @property string     content
 * @property int        created_at
 * @property int        views_counter
 * @property int        author_id
 * @property int        subscribe_is_send
 * @property int        is_send
 * @property int        company_id          - Идентификатор компании для которой относится статья
 * @property int        comments_count
 *
 * Class BlogItem
 */
class BlogItem extends ActiveRecord
{
    public static function tableName()
    {
        return 'blog';
    }

    public function getLink($isScheme = false)
    {
        return Url::to(['blog/item', 'id' => $this->id], $isScheme);
    }

    public function getImage($isScheme = false)
    {
        return Url::to($this->image, $isScheme);
    }

    public function attributeLabels()
    {
        return [
            'name'              => 'Название',
            'image'             => 'Картинки',
            'description'       => 'Описание',
            'content'           => 'Полный текст',
            'created_at'        => 'Создано',
            'views_counter'     => 'Счетчик просмотров',
            'author_id'         => 'Автор',
            'subscribe_is_send' => 'Послано?',
        ];
    }
}