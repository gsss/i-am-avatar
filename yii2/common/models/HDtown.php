<?php

namespace common\models;

use app\services\Subscribe;
use cs\services\BitMask;
use yii\db\ActiveRecord;
use yii\db\Query;

/**
 * @property int id
 * @property int country_id
 * @property string name
 * @property string title
 *
 * Class HDtown
 * @package common\models
 */
class HDtown extends ActiveRecord
{
    public static function tableName()
    {
        return 'gs_hd_town';
    }
}