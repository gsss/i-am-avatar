<?php
/**
 * Created by PhpStorm.
 * User: s.arhangelskiy
 * Date: 12.04.2016
 * Time: 16:13
 */

namespace common\models;

use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;
use yii\helpers\VarDumper;

/**
 * @property int    id
 * @property string dogovor_podryada
 * @property string akt_pp_sirya
 * @property string akt_pp_prod
 * @property string report
 * @property string protokol
 * @property string protokol_hash
 * @property string tz
 * @property string tz_hash
 * @property int    school_id
 * @property int    created_at
 * @property int    status
 *
 *
 * @package app\services
 */
class KoopPodryad extends ActiveRecord
{
    public static function tableName()
    {
        return 'koop_podryad';
    }

    public function attributeLabels()
    {
        return [
        ];
    }

}