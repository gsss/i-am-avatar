<?php
/**
 * Created by PhpStorm.
 * User: Бог-Творец
 * Date: 18.06.2016
 * Time: 20:41
 */

namespace common\models\piramida;


use yii\db\ActiveRecord;
use yii\db\Query;

class ReferalBonus extends ActiveRecord
{
    public static function tableName()
    {
        return 'nw_referal_bonus';
    }


    public function rules()
    {
        return [
            [[
                'from',
                'to',
                'level',
                'to_wallet',
                'sum',
                'percent',
                'datetime',
                'request_id',
                'transaction_id',
            ], 'required'],
            [[
                'from',
                'to',
                'level',
                'to_wallet',
                'request_id',
                'transaction_id',
            ], 'integer'],
            [[
                'sum',
                'percent',
                'datetime',
            ], 'double'],
        ];
    }

    public static function add($fields)
    {
        $fields['datetime'] = microtime(true);
        $iam = new self($fields);
        $iam->save();
        $iam->id = self::getDb()->lastInsertID;

        return $iam;
    }

    public static function batchInsert($columns, $rows)
    {
        (new Query())->createCommand()->batchInsert(self::tableName(), $columns, $rows)->execute();
    }
} 