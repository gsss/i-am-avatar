<?php

namespace common\models\piramida;

use common\models\avatar\UserBill;
use common\models\PaymentBitCoin;
use common\models\PaySystemConfig;
use common\models\UserAvatar;
use Yii;
use common\models\PaySystem;
use cs\services\BitMask;
use cs\services\VarDumper;
use cs\web\Exception;
use yii\db\ActiveRecord;
use yii\db\Query;
use yii\helpers\ArrayHelper;
use yii\helpers\FileHelper;
use yii\helpers\Json;
use yii\imagine\Image;
use Endroid\QrCode\QrCode;
use yii\rbac\Role;

/**
 * Заявка на инвестицию
 *
 * @property int    id                           - идентификатор
 * @property int    user_id                      - идентификатор пользователя на сайте gs_users.id
 * @property int    billing_id                   - идентификатор счета
 * @property int    datetime                     - время операции
 * @property float  price                        - размер пополнения
 * @property int    is_paid                      - оплачен счет?
 * @property int    status                       - статус
 * @property int    is_answer_from_client        - Есть ответ от клиента?
 * @property int    is_answer_from_shop          - Есть ответ из магазина?
 * @property int    last_message_time            - Время последнего ответа
 *
 * Class InRequest
 * @package app\models\Piramida
 */
class InRequest extends ActiveRecord
{
    /** Заявка послана магазину */
    const STATUS_SEND = 1;

    /** Заявка подтверждена клиентом и статсус отправляется магазину */
    const STATUS_CONFIRM_CLIENT = 2;

    /** Заявка подтверждена магазином и статсус отправляется клиенту */
    const STATUS_CONFIRM_SHOP = 3;

    const EVENT_BEFORE_SUCCESS = 'beforeSuccess';
    const EVENT_AFTER_SUCCESS  = 'afterSuccess';

    private $_paySystem;

    public static function tableName()
    {
        return 'piramida_in_requests';
    }

    public function rules()
    {
        return [
            [['price', 'datetime',], 'required'],
            [[
                'user_id',
                'billing_id',
                'datetime',
                'is_paid',
                'status',
                'is_answer_from_client',
                'is_answer_from_shop',
                'last_message_time',
            ], 'integer'],
            [['price',], 'double'],
        ];
    }

    public static function add($fields)
    {
        $fields['datetime'] = time();
        $fields['is_paid'] = 0;
        $new = new self($fields);
        $new->save();
        $new->id = self::getDb()->lastInsertID;

        return $new;
    }

    /**
     * @param int $condition
     *
     * @return static
     * @throws \cs\web\Exception
     */
    public static function findOne($condition)
    {
        $item = parent::findOne($condition);
        if (is_null($item)) {
            throw new Exception('Нет такого заказа');
        }
        return $item;
    }

    /**
     * @return \common\models\UserAvatar
     */
    public function getUser()
    {
        return UserAvatar::findOne($this->getUserId());
    }

    /**
     * @return \common\models\piramida\Billing
     *
     * @throws \yii\base\Exception
     */
    public function getBilling()
    {
        return \common\models\piramida\Billing::findOne($this->billing_id);
    }

    /**
     * @return \common\models\PaySystem
     */
    public function getPaySystem()
    {
        if (is_null($this->_paySystem)) {
            $this->_paySystem = $this->_getPaySystem();
        }

        return $this->_paySystem;
    }

    /**
     * @return \common\models\PaySystem
     */
    public function _getPaySystem()
    {
        $billing = $this->getBilling();
        $source_id = $billing->source_id;
        return PaySystem::findOne($source_id);
    }

    /**
     * @return int
     */
    public function getOwnerId()
    {
        return $this->user_id;
    }

    /**
     * @return int
     */
    public function getUserId()
    {
        return $this->user_id;
    }

    /**
     * Подтверждает заявку на инвестицию
     * Создает транзакцию и разброс по кошелькам
     * Если уже подтверждена, то не выполнится
     *
     * @param WalletSourceInterface $source транзакция о переводе из внешней системы
     * @param string $owner хозяин
     * @param string $transaction_id транзакция ENR
     *
     * @return array
     * [
     *      'transaction' => \app\models\Piramida\Transaction
     *      'message'     => \app\models\Shop\RequestMessage
     * ]
     *
     * @throws \Exception
     */
    public function success(WalletSourceInterface $source, $owner = null, $transaction_id = null)
    {
        if ($this->is_paid == 0) {
            $this->trigger(self::EVENT_BEFORE_SUCCESS);
            $transaction = \Yii::$app->db->beginTransaction();
            try {
                // Раскидать по пирамиде
                $billing = $this->getBilling();
                Yii::info($billing->getPaySystem()->code, 'avatar\\success');
                if ($billing->getPaySystem()->code == 'bit-coin') {
                    $originalSum = PaymentBitCoin::findOne(['billing_id' => $billing->id])->sum_after;
                    Yii::info('sum='.$originalSum, 'avatar\\success\\sum');
                    $ostatok = $originalSum;
                    // Составить список пользователей кому отчислять
                    $users = $this->getPiramidaUsers();
                    Yii::info(\yii\helpers\VarDumper::dumpAsString($users), 'avatar\\success');

                    $piramida = new \avatar\modules\Piramida\components\Core();
                    $percentList = $piramida->percentList;
                    $c = count($users);
                    for ($i = 0; $i < $c; $i++) {
                        /** @var \common\models\UserAvatar $user */
                        $user = $users[$i];
                        $billing = $user->getPiramidaBilling();
                        $sum = Yii::$app->formatter->asDecimal($originalSum * ($percentList[$i] / 100), 8);
                        Yii::info($sum, 'avatar\\success\\sum');
                        $ostatok -= $sum;
                        /** @var \common\models\PaySystemConfig $payConfig */
                        $payConfig = PaySystemConfig::findOne(['paysystem_id' => $this->getBilling()->getPaySystem()->id, 'parent_id' => 2]);
                        $config = $payConfig->config;
                        $configObject = ArrayHelper::toArray(Json::decode($config));
                        $address = $configObject['external'];
                        /** @var \common\models\avatar\UserBill $billingPiramida */
                        $billingPiramida = UserBill::findOne(['address' => $address]);
                        Yii::info(\yii\helpers\VarDumper::dumpAsString($billingPiramida), 'avatar\\success\\billing');
                        $result = $billingPiramida->getWalletAvatar()->pay([$billing->address => [
                            'amount'  => $sum,
                            'comment' => [
                                'transaction' => 'Перевод из фонда новой Земли для $user=' . $user->id,
                                'from'        => 'Перевод для $user=' . $user->id . ' по инвестиции piramida_in_requests.id=' . $this->id,
                                'to'          => 'Перевод из фонда по инвестиции piramida_in_requests.id=' . $this->id,
                            ]
                        ]]);
                        Yii::info(\yii\helpers\VarDumper::dumpAsString($result), 'avatar\\success\\pay');
                    }

                    $this->is_paid = 1;
//                $this->getBilling()->success($t, $source);
                    $ret = $this->save();
                    Yii::info(\yii\helpers\VarDumper::dumpAsString($ret), 'avatar\\success\\save');
                }


                if (false) {
                    $transaction->rollBack();

                    throw new Exception('Не удалось сохранить запрос на вывод');
                } else {

                    $transaction->commit();
                    $this->trigger(self::EVENT_AFTER_SUCCESS);
                }
            } catch (\Exception $e) {
                $transaction->rollBack();

                throw $e;
            }
        }

        return [];
    }

    /**
     * Возвращает пирамиду пользователей (не более 7)
     *
     * @return \common\models\UserAvatar[]
     * наверху располагаются те кому нужно отчислять больше всего денег
     *
     */
    private function getPiramidaUsers()
    {
        $ret = [];
        Yii::info(1, 'avatar\\getPiramidaUsers');
        $user = $this->getUser();
        Yii::info(\yii\helpers\VarDumper::dumpAsString($user), 'avatar\\getPiramidaUsers');
        $c = 1;
        while (1) {
            $parentID = $user->parent_id;
            if (is_null($parentID)) break;
            $user = UserAvatar::findOne($parentID);
            $ret[] = $user;
            $c++;
            if ($c == 7) break;
        }

        return $ret;
    }

    /**
     * Добавить сообщение или статус
     *
     * @param array $fields поля для сообщения
     * @param int $direction направление сообщения self::DIRECTION_*
     *
     * @return \common\models\piramida\InRequestMessage
     */
    public function addMessageItem($fields, $direction)
    {
        $this->is_answer_from_shop = ($direction == InRequestMessage::DIRECTION_TO_CLIENT) ? 1 : 0;
        $this->is_answer_from_client = ($direction == InRequestMessage::DIRECTION_TO_CLIENT) ? 0 : 1;
        $this->last_message_time = time();

        if (isset($fields['status'])) {
            $this->status = $fields['status'];
        }
        $this->save();

        return InRequestMessage::add(ArrayHelper::merge($fields, [
            'request_id' => $this->id,
            'direction'  => $direction,
        ]));
    }

    /**
     * Добавить сообщение
     *
     * @param string $message сообщение
     * @param int $direction направление сообщения self::DIRECTION_*
     *
     * @return \common\models\piramida\InRequestMessage
     */
    public function addMessage($message, $direction)
    {
        $fields = [
            'message' => $message,
        ];
        $fields['user_id'] = \Yii::$app->user->id;

        return $this->addMessageItem($fields, $direction);
    }

    /**
     * Добавить сообщение для клиента
     *
     * @param string $message сообщение
     *
     * @return \common\models\piramida\InRequestMessage
     */
    public function addMessageToClient($message)
    {
        return $this->addMessage($message, InRequestMessage::DIRECTION_TO_CLIENT);
    }

    /**
     * Добавить сообщение для клиента
     *
     * @param string $message сообщение
     *
     * @return \common\models\piramida\InRequestMessage
     */
    public function addMessageToShop($message)
    {
        return $this->addMessage($message, InRequestMessage::DIRECTION_TO_SHOP);
    }

    /**
     * Добавить статус
     *
     * @param int | array $status статус self::STATUS_* или массив со статусом и сообщением
     * @param int $direction направление сообщения self::DIRECTION_*
     *
     * @return \common\models\piramida\InRequestMessage
     */
    public function addStatus($status, $direction)
    {
        if (!is_array($status)) {
            $fields = [
                'status' => $status,
            ];
        } else {
            $fields = $status;
        }

        return $this->addMessageItem($fields, $direction);
    }

    /**
     * Добавить статус к клиенту
     *
     * @param int | array $status статус self::STATUS_* или массив со статусом и сообщением
     *
     * @return \common\models\piramida\InRequestMessage
     */
    public function addStatusToClient($status)
    {
        if (!is_array($status)) {
            $fields = [
                'status' => $status,
            ];
        } else {
            $fields = $status;
        }

        return $this->addMessageItem($fields, InRequestMessage::DIRECTION_TO_CLIENT);
    }

    /**
     * Добавить статус в магазин
     *
     * @param int | array $status статус self::STATUS_* или массив со статусом и сообщением
     *
     * @return \common\models\piramida\InRequestMessage
     */
    public function addStatusToShop($status)
    {
        if (!is_array($status)) {
            $fields = [
                'status' => $status,
            ];
        } else {
            $fields = $status;
        }
        $fields['user_id'] = \Yii::$app->user->id;

        return $this->addMessageItem($fields, InRequestMessage::DIRECTION_TO_SHOP);
    }

    /**
     * Возвращает заготовку запроса с сообщениями
     *
     * @return \yii\db\Query
     */
    public function getMessages()
    {
        return InRequestMessage::find()
            ->andWhere(['request_id' => $this->id]);
    }

}