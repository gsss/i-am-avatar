<?php

namespace common\models\piramida;

use Yii;
use common\extensions\XSSActiveRecord;
use yii\base\Exception;
use yii\base\Security;
use yii\db\ActiveRecord;

/**
 * @property integer id
 * @property string  config
 * @property string  identity
 * @property string  password
 * @property string  address
 * @property string  pin - хеш pin кода
 */
class BitCoinUser extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'nw_bit_coin_account';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'config'], 'required'],
            [['config'], 'string', 'max' => 15000],
            [['address','identity','password'], 'string', 'max' => 40],
            [['pin'], 'string', 'max' => 60],
            ['id', 'integer']
        ];
    }

    /**
     *
     * @param mixed $condition
     * @return static
     */
    public static function findOne($condition)
    {
        $item = parent::findOne($condition);
        if (is_null($item)) {
            $item = new static([
                'id'     => $condition,
                'config' => ''
            ]);
        }

        return $item;
    }

    public static function getDb()
    {
        return Yii::$app->dbWallet;
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'     => 'ID',
            'config' => 'config',
        ];
    }

    public function setPin($string)
    {
        if (strlen($string) > 4) {
            throw new Exception('Длина pin кода не может быть не равной 4');
        }
        $this->pin = Yii::$app->security->generatePasswordHash($string);
    }

    /**
     * Проверяет PIN на соответствие с переданным
     *
     * @param  string $string
     *
     * @return bool
     */
    public function validatePin($string)
    {
        return Yii::$app->security->validatePassword($string, $this->pin);
    }
}
