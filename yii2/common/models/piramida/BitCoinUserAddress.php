<?php

namespace common\models\piramida;

use Yii;
use common\extensions\XSSActiveRecord;
use yii\base\Exception;
use yii\base\Security;
use yii\db\ActiveRecord;

/**
 * @property integer id
 * @property integer user_id
 * @property string  address
 */
class BitCoinUserAddress extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'nw_bit_coin_account_addresses';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'address'], 'required'],
            [['address'], 'string', 'max' => 40],
            ['user_id', 'integer']
        ];
    }

    /**
     *
     * @param mixed $condition
     * @return static
     */
    public static function findOne($condition)
    {
        $item = parent::findOne($condition);
        if (is_null($item)) {
            throw new Exception('Не могу найти адрес БитКойн');
        }

        return $item;
    }

    public static function getDb()
    {
        return Yii::$app->dbWallet;
    }
}
