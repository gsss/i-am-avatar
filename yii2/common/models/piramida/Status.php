<?php

namespace common\models\piramida;

use app\models\Shop;
use app\models\Union;
use app\models\User;
use app\services\Subscribe;
use cs\Application;
use cs\services\BitMask;
use cs\web\Exception;
use cs\Widget\FileUploadMany\FileUploadMany;

use yii\db\Query;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;


/**
 * Class PiramidaStatus
 * @package app\models\Piramida
 *
 * @property int attribute  кодовое уникальное значение
 * @property int title      название на русском
 * @property int sort_index
 * @property array levels
 * @property int id
 */
class Status extends Object
{
    public $attribute;
    public $title;
    public $sort_index;
    /** @var  array */
    public $levels;
    public $id;

}