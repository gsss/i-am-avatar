<?php
/**
 * Created by PhpStorm.
 * User: Бог-Творец
 * Date: 10.04.2016
 * Time: 2:04
 */

namespace common\models\piramida\WalletSource;

use common\models\avatar\UserBillMerchant;
use common\models\BillingMain;
use common\models\MerchantRequest;
use common\models\PaymentBitCoin;
use common\services\Security;
use Yii;
use app\modules\Graal\models\search\InRequest;
use avatar\models\Wallet;
use common\models\avatar\UserBill;
use common\models\Config;
use common\models\piramida\Billing;
use common\payment\BitCoinBlockTrailPayment;
use cs\Application;
use yii\base\Exception;

use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Json;
use yii\helpers\VarDumper;
use yii\rbac\Role;

class BitCoin extends Base implements \common\models\piramida\WalletSourceInterface
{
    /**
     * @var array
     *
     * [
    'network' => 'BTC'
    'event_type' => 'address-transactions'
    'data' => [
    'raw' => '01000000018095bf47a64f59c90e7423e7b3585f2cde03a17ad0e6480f54db13b0f0bd8687010000006b483045022100d444ac45bd9a910c306f9331c23a8b170b26f231d2f6ffa52e3203b86a66b13d02203a4e58459059b85218e047550bde2589fb5d95f8d40cea83ac8581936c43f5440121038bd68bdc84f4a230cdd29d504569b09e824907ad440530ff9985a824e58ecb49ffffffff02102700000000000017a914fb1aaba4d6e134450f5969985bab382f79c40cde87598da100000000001976a9143a3bf3c5fc1f30854a084c418cb517372cfe780b88ac00000000'
    'hash' => 'c16a08ec8194e5c3af8d2f4a5e8d825aadab7d06a2918b0b9f4f3423a854272b'
    'first_seen_at' => '2017-05-15T14:27:34+0000'
    'last_seen_at' => '2017-05-15T14:27:34+0000'
    'block_height' => null
    'block_time' => null
    'block_hash' => null
    'confirmations' => 0
    'is_coinbase' => false
    'estimated_value' => 10000
    'total_input_value' => 10624601
    'total_output_value' => 10597481
    'total_fee' => 27120
    'estimated_change' => 10587481
    'estimated_change_address' => '16Jv1gVFBGApVJi74LVieZGKigj8wbE6Co'
    'high_priority' => false
    'enough_fee' => true
    'contains_dust' => false
    'inputs' => [
    0 => [
    'index' => 0
    'output_hash' => '8786bdf0b013db540f48e6d07aa103de2c5f58b3e723740ec9594fa647bf9580'
    'output_index' => 1
    'output_confirmed' => false
    'value' => 10624601
    'sequence' => 4294967295
    'address' => '1Gdu6FVChKRzy7kgShftPLroXwKjNeS3Wo'
    'type' => 'pubkeyhash'
    'multisig' => null
    'multisig_addresses' => null
    'script_signature' => '483045022100d444ac45bd9a910c306f9331c23a8b170b26f231d2f6ffa52e3203b86a66b13d02203a4e58459059b85218e047550bde2589fb5d95f8d40cea83ac8581936c43f5440121038bd68bdc84f4a230cdd29d504569b09e824907ad440530ff9985a824e58ecb49'
    ]
    ]
    'outputs' => [
    0 => [
    'index' => 0
    'value' => 10000
    'address' => '3QajUnprbTcs7WhbhfQbTc3wcZk4JTZNdo'
    'type' => 'scripthash'
    'multisig' => null
    'multisig_addresses' => null
    'script' => 'OP_HASH160 fb1aaba4d6e134450f5969985bab382f79c40cde OP_EQUAL'
    'script_hex' => 'a914fb1aaba4d6e134450f5969985bab382f79c40cde87'
    'spent_hash' => null
    'spent_index' => 0
    ]
    1 => [
    'index' => 1
    'value' => 10587481
    'address' => '16Jv1gVFBGApVJi74LVieZGKigj8wbE6Co'
    'type' => 'pubkeyhash'
    'multisig' => null
    'multisig_addresses' => null
    'script' => 'OP_DUP OP_HASH160 3a3bf3c5fc1f30854a084c418cb517372cfe780b OP_EQUALVERIFY OP_CHECKSIG'
    'script_hex' => '76a9143a3bf3c5fc1f30854a084c418cb517372cfe780b88ac'
    'spent_hash' => null
    'spent_index' => 0
    ]
    ]
    'opt_in_rbf' => false
    'unconfirmed_inputs' => true
    'lock_time_timestamp' => null
    'lock_time_block_height' => null
    'size' => 224
    ]
    'retry_count' => '4'
    'addresses' => [
    '3QajUnprbTcs7WhbhfQbTc3wcZk4JTZNdo' => 10000
    ]
    ]
     */
    public $transaction;
    public $source_id;

    /** @var  double */
    public $sumAfter;

    /** @var  double */
    public $sumBefore;

    public $bill_id;

    public function getTransactionInfo()
    {
        return $this->transaction;
    }

    /**
     * Выдает цену с учетом оплаты комиссии
     *
     * @param float $price цена которую хотим получить от клиента через прием денег через эту платежную систему
     *
     * @return float цена для оплаты в платежной соистеме
     */
    public function getPriceWithTax($price)
    {
        return (float)($price + 5);
    }

    public function getTax()
    {
        return 2;
    }

    public function getSumBefore()
    {
        return $this->sumBefore;
    }

    public function getSumAfter()
    {
        return $this->sumAfter;
    }

    /**
     * Формирует форму
     *
     * @param \common\models\BillingMain $billing
     * @param string $description
     * @param string $destinationName - наименование поучателя
     *
     * @param null | array $options
     * - form - array - параметры(аттрибуты) тега формы
     * - values - array - доп параметры для формы как скрытые поля (ключ => значение)
     *
     * @return string
     */
    public function getForm($billing, $description, $destinationName, $destinationAddress,  $options = null)
    {
        Yii::trace('getForm', 'avatar\\payments\bitcoin\\getForm\\start');

        $payment = $this->createPayment($sum, $action, $address);

        return \Yii::$app->view->renderFile('@common/models/piramida/WalletSource/Ethereum.template.php', [
            'billing'            => $billing,
            'destinationAddress' => $destinationAddress,
            'payment'            => $payment,
        ]);
    }


    /**
     * Генерирует форму HTML: картинка и текст с инструкцией
     *
     * @param float $sum сумма в биткойнах
     * @param string $action сумма в биткойнах
     * @param string $address адрес выставленного счета
     *
     * @return \common\models\PaymentBitCoin
     * @throws \Exception
     */
    public function createPayment($sum, $action, $address)
    {
        $model = new PaymentBitCoin([
            'action'     => $action,
            'billing_id' => $this->bill_id,
            'address'    => $address,
            'time_add'   => time(),
            'sum_before' => $amount,
            'sum_after'  => $amount,
            'web_hook'   => $hookName,
        ]);
        $ret = $model->save();
        if (!$ret) {
            Yii::warning('Не удалось сохранить платеж \common\models\PaymentBitCoin' . VarDumper::dumpAsString($model->errors));
        }


        return $model;
    }


    /**
     * Сохраняет время подтверждения в платеже `\common\models\PaymentBitCoin`
     *
     * @param $billing_id
     * @throws Exception
     */
    public function saveConfirmTime($billing_id)
    {
        $payment = \common\models\PaymentBitCoin::findOne(['billing_id' => $billing_id]);
        $payment->time_confirm = time();
        $payment->transaction = VarDumper::dumpAsString($this->transaction);
        $payment->save(false);
    }

    /**
     *
     *
     * @param array $actions массив функций (действий)
     * где индекс - string - действие
     * а значение - функция bool function($id, \app\models\Piramida\WalletSourceInterface $transaction)
     * - $id - int - идентификатор в действии
     * - $transaction - \app\models\Piramida\WalletSourceInterface
     *
     * @return bool
     * @throws Exception
     */
    public function success($actions)
    {
        $rawBody = Yii::$app->request->rawBody;
        $rawBody = str_replace('\\"', '"', $rawBody);
        $rawBody = json_decode($rawBody);
        $rawBody = ArrayHelper::toArray($rawBody);
        $billing_id = Yii::$app->request->get('billing_id');

        $this->transaction = $rawBody;
        $payment = \common\models\PaymentBitCoin::findOne(['billing_id' => $billing_id]);
        $payment->transaction_hash = $rawBody['data']['hash'];
        $payment->confirmations = $rawBody['data']['confirmations'];
        $ret = $payment->save();
        if (!$ret) {
            Yii::warning('$payment->save() cant save ' . VarDumper::dumpAsString($payment->errors), 'avatar\\payments\\bitcoin\\success');
        }

        /** @var \common\models\piramida\Billing $billing */
        $billing = BillingMain::findOne(\Yii::$app->request->get('billing_id'));
        \Yii::info('graalInvest', 'avatar\\payments\\bit-coin::' . $billing->id . '\\success');

        $merchantRequest = MerchantRequest::findOne(['billing_id' => $billing->id]);
        $confirmations = $merchantRequest->getMerchant()->confirmations;

        if ($this->isConfirmations($billing, $confirmations)) {
            $this->successAction($actions, $billing);
            \Yii::info('6 confirmations', 'avatar\\payments\\bit-coin::' . $billing->id . '\\success\\6');
        }

        return true;
    }

    /**
     * Проверяет было ли 6 подтверждений транзакции
     * и проверяет достаточно ли денег прислано было в соответствии с тем что указано в $billing
     *
     *
     * Yii::$app->request->rawBody
     * [
     * "network" => "BTC",
     * "data" => [
     * "hash" => "8fa29a1db30aceed2ec5afb2bcdf6196207975780b4ca5f3f1d6313cab855fe7",
     * "time" => "2014-08-25T09:29:28+0000",
     * "confirmations" => 3948,
     *
     * "...etc..." => "...etc..."
     * ],
     * "addresses" => [
     * "1qMBuZnrmGoAc2MWyTnSgoLuWReDHNYyF" => −2501000
     * ],
     * ]
     * @param \common\models\piramida\Billing $billing
     * @param int                             $count
     *
     * @return bool
     */
    public function isConfirmations($billing, $count)
    {
        \Yii::info($this->transaction, 'avatar\\payments\\bit-coin::' . $billing->id . '\\isConfirmations');
        \Yii::info($this->transaction['data']['confirmations'], 'avatar\\payments\\bit-coin::' . $billing->id . '\\confirmations');

        return $this->transaction['data']['confirmations'] >= $count;
    }

    /**
     * Проверяет было ли 6 подтверждений транзакции
     * и проверяет достаточно ли денег прислано было в соответствии с тем что указано в $billing
     *
     *
     * Yii::$app->request->rawBody
     * [
     * "network" => "BTC",
     * "data" => [
     * "hash" => "8fa29a1db30aceed2ec5afb2bcdf6196207975780b4ca5f3f1d6313cab855fe7",
     * "time" => "2014-08-25T09:29:28+0000",
     * "confirmations" => 3948,
     *
     * "...etc..." => "...etc..."
     * ],
     * "addresses" => [
     * "1qMBuZnrmGoAc2MWyTnSgoLuWReDHNYyF" => −2501000
     * ],
     * ]
     * @param \common\models\piramida\Billing $billing
     *
     * @return bool
     */
    public function isEnouf($billing)
    {
        $amount = Yii::$app->request->get('amount');
        $amountSatoshi = $amount * 100000000;

        foreach ($this->transaction['data']['outputs'] as $o) {
            if ($o['value'] >= $amountSatoshi) {
                return true;
            } else {
                // не достаточно средств
                return false;
            }
        }
    }

    /**
     * Сохраняет \common\models\PaymentBitCoin (время подтверждения и транзакцию)
     * Выполняет actions
     *
     * @param array                           $actions
     * @param \common\models\piramida\Billing $billing
     *
     * @return mixed call_user_func($function, $id, $this);
     */
    public function successAction($actions, $billing)
    {
        \Yii::info(VarDumper::dumpAsString([$actions, $billing]), 'avatar\\payments\\bit-coin::' . $billing->id . '/rawBody1');
        $rawBody = \Yii::$app->request->rawBody;
        \Yii::info(VarDumper::dumpAsString(\Yii::$app->request->rawBody), 'avatar\\payments\\bit-coin::' . $billing->id . '/rawBody');
        $rawBody = str_replace('\\"', '"', $rawBody);
        $rawBody = json_decode($rawBody);
        $rawBody = ArrayHelper::toArray($rawBody);
        $this->transaction = $rawBody;

        // если транзакция уже обработана, то второй раз не запускать на исполнение
        if (ArrayHelper::getValue($billing, 'is_paid', 0) == 1) {
            return true;
        }
        $this->saveConfirmTime($billing->id);

        // определяю действие и идентификатор
        $action = \Yii::$app->request->get('action');
        $action = explode('.', $action);

        $id = $action[1];
        $action = $action[0];

        $function = $actions[$action];

        return call_user_func($function, $id, $this);
    }
}