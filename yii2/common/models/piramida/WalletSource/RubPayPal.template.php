<?php
/**
 * Created by PhpStorm.
 * User: Бог-Творец
 * Date: 10.04.2016
 * Time: 2:04
 */
/** @var \yii\web\View $this */
/** @var \app\models\Piramida\Billing $billing */
/** @var string $destinationAddress */
/** @var \app\modules\Graal\models\InRequest $request */


use yii\helpers\ArrayHelper;
use yii\helpers\Html;

?>
<p><?= \Yii::t('c18', 'Вам нужно перевести {sum} руб. на счет: <b><u>{card}</u></b>', [
        'sum'  => \Yii::$app->formatter->asDecimal($billing->sum_before, 2),
        'card' => $destinationAddress,
    ])
    ?></p>

<p>И нажмите "Подтвердить"</p>
<p>Мы подтвердим оплату и вы получите энергопакет по почте</p>
<?php
$this->registerJs(<<<JS
$('#buttonSuccess').click(function(e) {
    ajaxJson({
        url: '/cabinet-graal/in-requests-messages-success',
        data: {
            id: {$request->id}
        },
        success: function(ret) {
            window.location = '/cabinet-graal/in-requests-messages/{$request->id}'
        }
    });
});
JS
);
?>
<p>
    <button class="btn btn-success btn-lg" id="buttonSuccess">Подтвердить</button>
</p>
