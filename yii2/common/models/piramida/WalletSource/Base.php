<?php
/**
 * Created by PhpStorm.
 * User: Бог-Творец
 * Date: 26.12.2016
 * Time: 3:30
 */

namespace common\models\piramida\WalletSource;


use yii\base\BaseObject;

class Base extends BaseObject
{
    /** @var  \common\models\PaySystem */
    public $paySystem;

    /** @var  string JSON */
    public $config;

    /** @var  int paysystems_config.id */
    public $config_id;

    /** @var  int paysystems.id */
    public $source_id;

    public function getTypeId()
    {
        return $this->paySystem->id;
    }

} 