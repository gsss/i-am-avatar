<?php
/**
 * Created by PhpStorm.
 * User: Бог-Творец
 * Date: 10.04.2016
 * Time: 2:04
 */
/** @var \yii\web\View $this */
/** @var \app\models\Piramida\Billing $billing */
/** @var string $destinationAddress */
/** @var \app\modules\Graal\models\InRequest $request */


use yii\helpers\ArrayHelper;
use yii\helpers\Html;


$config = ArrayHelper::toArray(json_decode($destinationAddress));

?>
<p>
    <?= \Yii::t('c18', 'Вам нужно перевести {sum} руб.', [
        'sum'  => \Yii::$app->formatter->asDecimal($billing->sum_before, 2),
    ])
    ?>
</p>
<p>Нажмите на "кошелек" и там переведите на карту.</p>
<p><a href="<?= 'https://money.yandex.ru/to/' . $config['address'] ?>" class="btn btn-success"
      target="_blank">Кошелек</a>
</p>
<p>После переведа денег на кошелек подтвердите нажав на кнопку "Подтвердить"</p>
<p>Мы подтвердим оплату и вы получите энергопакет по почте</p>



<?php
$this->registerJs(<<<JS
$('#buttonSuccess').click(function(e) {
    ajaxJson({
        url: '/cabinet-graal/in-requests-messages-success',
        data: {
            id: {$request->id}
        },
        success: function(ret) {
            window.location = '/cabinet-graal/in-requests-messages/{$request->id}'
        }
    });
});
JS
);
?>
<p>
    <button class="btn btn-success btn-lg" id="buttonSuccess">Подтвердить</button>
</p>

<p>
    <?= \Yii::t('c18', 'Или переведиите {sum} руб. один из счетов: {card}', [
        'sum'  => \Yii::$app->formatter->asDecimal($billing->sum_before, 2),
        'card' => $config['card'],
    ])
    ?>
</p>