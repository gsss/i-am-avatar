<?php
/**
 * Created by PhpStorm.
 * User: Бог-Творец
 * Date: 10.04.2016
 * Time: 2:04
 */

namespace common\models\piramida\WalletSource;

use common\models\PaymentEthereum;
use common\models\PaymentLightCoin;
use cs\services\VarDumper;
use yii\helpers\Json;

class DogeCoin extends Base implements \common\models\piramida\WalletSourceInterface
{
    public $transaction;

    public $source_id;

    /** @var  double */
    public $sumAfter;

    /** @var  double */
    public $sumBefore;

    public function getTransactionInfo()
    {
        return $this->transaction;
    }

    public function getPriceWithTax($price)
    {
        return $price;
    }

    public function getTax()
    {
        return 0;
    }

    public function getSumBefore()
    {
        return $this->sumBefore;
    }

    public function getSumAfter()
    {
        return $this->sumAfter;
    }

    /**
     * Формирует форму
     *
     * @param \common\models\BillingMain    $billing         счет, в котором валюта должна быть равна (ETH)
     * @param string                        $description
     * @param string                        $destinationName наименование поучателя
     * @param null | array                  $options
     * - form - array - параметры(аттрибуты) тега формы
     * - values - array - доп параметры для формы как скрытые поля (ключ => значение)
     *
     * @return string
     */
    public function getForm($billing, $description, $destinationName, $destinationAddress, $options = null)
    {
        try {
            $payment = \common\models\PaymentDoge::findOne(['billing_id' => $billing->id]);
        } catch (\Exception $e) {
            $payment = new \common\models\PaymentDoge([
                'value'      => $billing->sum_after,
                'action'     => $options['action'],
                'billing_id' => $billing->id,
            ]);
            $payment->save();
        }

        return \Yii::$app->view->renderFile('@common/models/piramida/WalletSource/DogeCoin.template.php', [
            'billing'            => $billing,
            'destinationAddress' => $destinationAddress,
            'payment'            => $payment,
        ]);
    }


    /**
     * @return bool
     */
    public function success($actions)
    {
        if (\Yii::$app->request->get('type') != 'ethereum') {
            return false;
        }

        return true;
    }

    /**
     * @param string $txid хеш транзакции
     *
     * @return int
     *
     * @throws
     */
    public static function getConfirmationsOfTransaction($txid)
    {
        $url = 'https://live.blockcypher.com/';

        $client = new \yii\httpclient\Client(['baseUrl' => $url]);
        $content = $client->get('doge/tx/'.$txid.'/')->send()->content;
        require_once(\Yii::getAlias('@csRoot/services/simplehtmldom_1_5/simple_html_dom.php'));
        $content = str_get_html($content);
        /** @var \simple_html_dom_node $rows */
        $rows = $content->find('#confs-hover');
        if (count($rows) != 1) throw new \Exception('Не верный формат страницы');
        $s = trim(strip_tags($rows[0]->attr['data-content']));
        $s1 = explode(' ', $s);

        return $s1[0];
    }

    /**
     * https://api.blockcypher.com/v1/ltc/main/txs/944b914324ec68be2f5f5988d85f28348f42a0c515ad427ebf9bae5c4f7e208f?limit=50&includeHex=true
     *
     * @param string $txid хеш транзакции
     *
     * @return array
     *
     * @throws
     */
    public static function getTransaction($txid)
    {
        $url = 'https://api.blockcypher.com';

        $client = new \yii\httpclient\Client(['baseUrl' => $url]);
        $content = $client->get('v1/doge/main/txs/'.$txid, [
            'limit' => 50,
            'includeHex' => 'true',
        ])->send()->content;

        return Json::decode($content);
    }

}