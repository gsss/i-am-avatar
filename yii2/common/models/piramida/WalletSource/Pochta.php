<?php
/**
 * Created by PhpStorm.
 * User: Бог-Творец
 * Date: 10.04.2016
 * Time: 2:04
 */

namespace common\models\piramida\WalletSource;


use yii\helpers\ArrayHelper;
use yii\helpers\Html;

class Pochta extends Object implements \common\models\piramida\WalletSourceInterface
{
    public $transaction;
    public $sumAfter;
    public $sumBefore;

    public $paymentType;

    /** @var string */
    public $config;

    public function getTransactionInfo()
    {
        return $this->transaction;
    }

    public function getTypeId()
    {
        return $this->paymentType;
    }

    /**
     * Выдает цену с учетом оплаты комиссии
     *
     * @param float $price цена которую хотим получить от клиента через прием денег через эту платежную систему
     *
     * @return float цена для оплаты в платежной соистеме
     */
    public function getPriceWithTax($price)
    {
        return $price;
    }

    public function getTax()
    {
        return 0;
    }

    public function getSumBefore()
    {
        return $this->sumBefore;
    }

    public function getSumAfter()
    {
        return $this->sumAfter;
    }

    /**
     * Формирует форму
     * 
     * @param \app\models\Piramida\Billing $billing
     * @param string $description
     * @param string $destinationName - наименование поучателя
     * 
     * @param null | array $options
     * - form - array - параметры(аттрибуты) тега формы
     * - values - array - доп параметры для формы как скрытые поля (ключ => значение)
     * - request - string - строка для значения "За что: "
     *
     * @return string
     */
    public function getForm($billing, $description, $destinationName, $destinationAddress, $options = null)
    {
        $content = [
            'Вам нужно перевести ' . $billing->sum_before . ' руб. на счет solncelub@gmail.com',
            $description,
        ];

        return join('<br>', $content);
    }
}