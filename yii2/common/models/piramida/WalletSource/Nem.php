<?php
/**
 * Created by PhpStorm.
 * User: Бог-Творец
 * Date: 10.04.2016
 * Time: 2:04
 */

namespace common\models\piramida\WalletSource;

use common\models\PaymentEthereum;
use common\models\PaymentNem;

class Nem extends Base implements \common\models\piramida\WalletSourceInterface
{
    public $transaction;

    public $source_id;

    /** @var  double */
    public $sumAfter;

    /** @var  double */
    public $sumBefore;

    public function getTransactionInfo()
    {
        return $this->transaction;
    }

    public function getPriceWithTax($price)
    {
        return $price;
    }

    public function getTax()
    {
        return 0;
    }

    public function getSumBefore()
    {
        return $this->sumBefore;
    }

    public function getSumAfter()
    {
        return $this->sumAfter;
    }

    /**
     * Формирует форму
     *
     * @param \common\models\BillingMain    $billing         счет, в котором валюта должна быть равна (ETH)
     * @param string                        $description
     * @param string                        $destinationName наименование поучателя
     * @param null | array                  $options
     * - form - array - параметры(аттрибуты) тега формы
     * - values - array - доп параметры для формы как скрытые поля (ключ => значение)
     *
     * @return string
     */
    public function getForm($billing, $description, $destinationName, $destinationAddress, $options = null)
    {
        try {
            $payment = PaymentNem::findOne(['billing_id' => $billing->id]);
        } catch (\Exception $e) {
            $payment = new PaymentNem([
                'value'      => $billing->sum_after,
                'action'     => $options['action'],
                'billing_id' => $billing->id,
            ]);
            $payment->save();
        }

        return \Yii::$app->view->renderFile('@common/models/piramida/WalletSource/Nem.template.php', [
            'billing'            => $billing,
            'destinationAddress' => $destinationAddress,
            'payment'            => $payment,
        ]);
    }


    /**
     * @return bool
     */
    public function success($actions)
    {
        if (\Yii::$app->request->get('type') != 'ethereum') {
            return false;
        }

        return true;
    }

}