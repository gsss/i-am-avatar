<?php
/**
 * Created by PhpStorm.
 * User: Бог-Творец
 * Date: 10.04.2016
 * Time: 2:04
 */

namespace common\models\piramida\WalletSource;


use yii\helpers\ArrayHelper;
use yii\helpers\Html;

class AdvCash extends Base implements \common\models\piramida\WalletSourceInterface
{
    public $transaction;
    public $source_id;

    /** @var  double */
    public $sumAfter;

    /** @var  double */
    public $sumBefore;

    public function getTransactionInfo()
    {
        return $this->transaction;
    }

    public function getPriceWithTax($price)
    {
        return $price;
    }

    public function getTax()
    {
        return 2;
    }

    public function getSumBefore()
    {
        return $this->sumBefore;
    }

    public function getSumAfter()
    {
        return $this->sumAfter;
    }

    /**
     * Формирует форму
     * 
     * @param \app\models\Piramida\Billing $billing
     * @param string $description
     * @param string $destinationName - наименование поучателя
     * 
     * @param null | array $options
     * - form - array - параметры(аттрибуты) тега формы
     * - values - array - доп параметры для формы как скрытые поля (ключ => значение)
     * 
     * @return string
     */
    public function getForm($billing, $description, $destinationName, $destinationAddress, $options = null)
    {
        return \Yii::$app->view->renderFile('@common/models/Piramida/WalletSource/AdvCash.template.php', [
            'billing'            => $billing,
            'destinationAddress' => $destinationAddress,
            'request'            => $options['request'],
        ]);
    }


    /**
     * Прием денег из яндекса
     *
     * @return bool
     */
    public function success($actions)
    {
        if (Yii::$app->request->get('type') != 'adv-cash') {
            return false;
        }

        return true;
    }

}