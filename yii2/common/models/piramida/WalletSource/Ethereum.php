<?php
/**
 * Created by PhpStorm.
 * User: Бог-Творец
 * Date: 10.04.2016
 * Time: 2:04
 */

namespace common\models\piramida\WalletSource;

use common\models\BillingMain;
use common\models\PaymentEthereum;
use cs\services\VarDumper;

class Ethereum extends Base implements \common\models\piramida\WalletSourceInterface
{
    public $transaction;

    public $source_id;

    /** @var  double */
    public $sumAfter;

    /** @var  double */
    public $sumBefore;

    public function getTransactionInfo()
    {
        return $this->transaction;
    }

    public function getPriceWithTax($price)
    {
        return $price;
    }

    public function getTax()
    {
        return 0;
    }

    public function getSumBefore()
    {
        return $this->sumBefore;
    }

    public function getSumAfter()
    {
        return $this->sumAfter;
    }

    /**
     * Формирует форму
     *
     * @param \common\models\BillingMain    $billing         счет, в котором валюта должна быть равна (ETH)
     * @param string                        $description
     * @param string                        $destinationName наименование поучателя
     * @param null | array                  $options
     * - form - array - параметры(аттрибуты) тега формы
     * - values - array - доп параметры для формы как скрытые поля (ключ => значение)
     *
     * @return string
     */
    public function getForm($billing, $description, $destinationName, $destinationAddress, $options = null)
    {
        $config = \yii\helpers\Json::decode($destinationAddress);
        try {
            $payment = PaymentEthereum::findOne(['billing_id' => $billing->id]);
        } catch (\Exception $e) {
            $payment = new PaymentEthereum([
                'value'      => $billing->sum_after,
                'action'     => $options['action'],
                'billing_id' => $billing->id,
                'address'    => $config['address'],
            ]);
            $payment->save();
        }

        if (!isset($options['view'])) {
            return $this->form($billing, $options['action'], $config['address']);
        } else {
            return $this->form($billing, $options['action'], $config['address'], $options['view']);
        }
    }

    /**
     * Генерирует форму HTML: картинка и текст с инструкцией
     *
     * @param \common\models\BillingMain    $billing         счет, в котором валюта должна быть равна (ETH)
     * @param string $action
     * @param string $address   адрес выставленного счета
     *
     * @return string HTML картинка и текст с инструкцией
     * @throws \Exception
     */
    private function form($billing, $action, $address, $view = null)
    {
        if (is_null($view)) {
            $view = '@common/models/piramida/WalletSource/Ethereum.template.php';
        }

        return \Yii::$app->view->renderFile($view, [
            'amount'     => $billing->sum_after,
            'action'     => $action,
            'address'    => $address,
            'billing'    => $billing,
        ]);
    }


    /**
     * @return bool
     */
    public function success($actions)
    {
        if (\Yii::$app->request->get('type') != 'ethereum') {
            return false;
        }

        return true;
    }


    /**
     * @param string $txid хеш транзакции
     *
     * @return int
     *
     * @throws
     */
    public static function getConfirmationsOfTransaction($txid)
    {
        $url = 'https://etherscan.io';
        if (!YII_ENV_PROD) {
            $url = 'https://ropsten.etherscan.io';
        }

        $client = new \yii\httpclient\Client(['baseUrl' => $url]);
        $content = $client->get('tx/'.$txid)->send()->content;
        require_once(\Yii::getAlias('@csRoot/services/simplehtmldom_1_5/simple_html_dom.php'));
        $content = str_get_html($content);
        /** @var \simple_html_dom_node $rows */
        $rows = $content->find('#ContentPlaceHolder1_maintable/div.cbs');
        VarDumper::dump($rows[3]->text());
        if (count($rows) != 1) throw new \Exception('Не верный формат страницы');
        $s = trim(strip_tags($rows[0]->attr['data-content']));
        $s1 = explode(' ', $s);

        return $s1[0];
    }

    /**
     * @param string $txid хеш транзакции
     *
     * @return array
     *
     * @throws
     */
    public static function getTransaction($txid)
    {
        $url = 'https://etherscan.io';
        if (!YII_ENV_PROD) {
            $url = 'https://ropsten.etherscan.io';
        }

        $client = new \yii\httpclient\Client(['baseUrl' => $url]);
        $content = $client->get('tx/'.$txid)->send()->content;
        require_once(\Yii::getAlias('@csRoot/services/simplehtmldom_1_5/simple_html_dom.php'));
        $content = str_get_html($content);
        /** @var \simple_html_dom_node $rows */
        $rows = $content->find('#ContentPlaceHolder1_maintable/div.cbs');
        $from = $rows[3]->text();
        $confirmations = $rows[1]->text();
        $to = $rows[4]->text();

        return [
            'from'          => $from,
            'confirmations' => $confirmations,
            'to'            => $to,
        ];
    }

}