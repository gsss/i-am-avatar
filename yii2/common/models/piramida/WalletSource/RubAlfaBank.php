<?php
/**
 * Created by PhpStorm.
 * User: Бог-Творец
 * Date: 10.04.2016
 * Time: 2:04
 */

namespace common\models\piramida\WalletSource;

use common\models\PaymentSber;

use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Json;

class RubAlfaBank extends Base implements \common\models\piramida\WalletSourceInterface
{
    public $transaction;
    public $source_id;

    /** @var  double */
    public $sumAfter;

    /** @var  double */
    public $sumBefore;


    public function getTransactionInfo()
    {
        return $this->transaction;
    }

    public function getPriceWithTax($price)
    {
        return $price;
    }

    public function getTax()
    {
        return 2;
    }

    public function getSumBefore()
    {
        return $this->sumBefore;
    }

    public function getSumAfter()
    {
        return $this->sumAfter;
    }

    /**
     * Формирует форму
     *
     * @param \common\models\piramida\Billing $billing
     * @param string $description
     * @param string $destinationName - наименование поучателя
     *
     * @param null | array $options
     * - form - array - параметры(аттрибуты) тега формы
     * - values - array - доп параметры для формы как скрытые поля (ключ => значение)
     *
     * @return string
     */
    public function getForm($billing, $description, $destinationName, $destinationAddress, $options = null)
    {
        $config = Json::decode($destinationAddress);
        try {
            $payment = PaymentSber::findOne(['billing_id' => $billing->id]);
        } catch (\Exception $e) {
            $payment = new PaymentSber([
                'value'      => $billing->sum_after,
                'action'     => $options['action'],
                'billing_id' => $billing->id,
                'card'       => $config['address'],
            ]);
            $payment->save();
        }

        $html = \Yii::$app->view->renderFile('@common/models/piramida/WalletSource/RubAlfaBank.template.php', [
            'billing'            => $billing,
            'destinationAddress' => $config['address'],
            'payment'            => $payment,
        ]);

        return $html;
    }
}