<?php
/**
 * Created by PhpStorm.
 * User: Бог-Творец
 * Date: 10.04.2016
 * Time: 2:04
 */

namespace common\models\piramida\WalletSource;

use app\models\Piramida\InRequest;
use app\models\Shop\Request;
use app\models\Shop\RequestTransaction;
use app\modules\Shop\services\Basket;
use cs\Application;
use Yii;
use app\models\Shop\Payments;
use cs\web\Exception;

use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\StringHelper;
use yii\rbac\Role;

class RubRubYandex extends RubYandexBase
{
    public $paymentType = 'PC';

    public function getTax()
    {
        return 0.5;
    }
}