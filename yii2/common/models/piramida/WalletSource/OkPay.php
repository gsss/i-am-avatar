<?php
/**
 * Created by PhpStorm.
 * User: Бог-Творец
 * Date: 10.04.2016
 * Time: 2:04
 */

namespace common\models\piramida\WalletSource;


use yii\helpers\ArrayHelper;
use yii\helpers\Html;

class OkPay extends Base implements \common\models\piramida\WalletSourceInterface
{
    public $transaction;
    public $source_id;

    /** @var  double */
    public $sumAfter;

    /** @var  double */
    public $sumBefore;

    public function getTransactionInfo()
    {
        return $this->transaction;
    }

    public function getPriceWithTax($price)
    {
        return $price;
    }

    public function getTax()
    {
        return 2;
    }

    public function getSumBefore()
    {
        return $this->sumBefore;
    }

    public function getSumAfter()
    {
        return $this->sumAfter;
    }

    /**
     * Формирует форму
     * 
     * @param \app\models\Piramida\Billing $billing
     * @param string $description
     * @param string $destinationName - наименование поучателя
     * 
     * @param null | array $options
     * - form - array - параметры(аттрибуты) тега формы
     * - values - array - доп параметры для формы как скрытые поля (ключ => значение)
     * 
     * @return string
     */
    public function getForm($billing, $description, $destinationName, $destinationAddress, $options = null)
    {
        return \Yii::$app->view->renderFile('@common/models/Piramida/WalletSource/OkPay.template.php', [
            'billing'            => $billing,
            'destinationAddress' => $destinationAddress,
            'request'            => $options['request'],
        ]);
    }


    /**
     * Рисует форму и вызывает событие отправки формы
     */
    private function getFormToAPI($billing, $description, $destinationName, $destinationAddress, $options = null)
    {
        $content = [];
        $config = ArrayHelper::toArray(json_decode($destinationAddress));
        $values = [
            'quickpay-form' => 'donate',
            'short-dest'    => $description,
            'targets'       => $description,
            'formcomment'   => $destinationName,
            'comment'       => '',
            'need-fio'      => 'false',
            'need-email'    => 'false',
            'need-phone'    => 'false',
            'need-address'  => 'false',
            'paymentType'   => $this->paymentType,
            'sum'           => $billing->sum_before,
            'receiver'      => $config['address'],
        ];
        if (isset($options['action'])) {
            $values['label'] = $options['action'];
        }
        foreach ($values as $key => $value) {
            $content[] = Html::hiddenInput($key, $value);
        }
        $formOptions = ArrayHelper::getValue($options, 'form', []);
        $formOptions = ArrayHelper::merge([
            'id'     => 'formPay',
            'method' => 'post',
            'action' => 'https://www.okpay.com/process.html',
        ], $formOptions);
        foreach (ArrayHelper::getValue($options, 'values', []) as $key => $value) {
            $content[] = Html::hiddenInput($key, $value);
        }
        \Yii::$app->view->registerJs(<<<JS
    $('#formPay').submit();
JS

        );
        $form = Html::tag('form', join("\n", $content), $formOptions);

        return $form;
    }


    /**
     * Прием денег из яндекса
     *
     * @return bool
     */
    public function success($actions)
    {
        if (Yii::$app->request->get('type') != 'adv-cash') {
            return false;
        }

        return true;
    }
}