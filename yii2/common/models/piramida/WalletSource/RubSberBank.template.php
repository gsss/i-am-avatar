<?php
/**
 * Created by PhpStorm.
 * User: Бог-Творец
 * Date: 10.04.2016
 * Time: 2:04
 */
/** @var \yii\web\View                      $this */
/** @var \common\models\piramida\Billing    $billing */
/** @var string                             $destinationAddress */
/** @var \common\models\PaymentSber         $payment */


use yii\helpers\ArrayHelper;
use yii\helpers\Html;


?>
<div style="margin-bottom: 200px;">
    <p><?= \Yii::t('app', 'Вам нужно перевести <b>{sum} руб.</b> на карту <b>{card}</b>', [
            'sum'  => \Yii::$app->formatter->asDecimal($billing->sum_after, 2),
            'card' => $destinationAddress,
        ])
        ?></p>
    <p>После перевода введите идентификатор отправлнной транзакции и нажмите "Подтвердить"</p>
    <p><b>Идентификатор транзакции:</b></p>
    <p><input type="text" class="form-control" id="txid"/></p>
    <p>Мы подтвердим оплату и вы получите токены на свой кошелек.</p>
    <?php
    $this->registerJs(<<<JS
$('#buttonSuccess').click(function(e) {
    ajaxJson({
        url: '/ico/success-payment-sber',
        data: {
            id: {$billing->id},
            txid: $('#txid').val()
        },
        success: function(ret) {
            window.location = '/ico/payment-success?id={$billing->id}'
        }
    });
});
JS
    );
    ?>
    <hr>
    <p>
        <button class="btn btn-success btn-lg" id="buttonSuccess" style="width: 100%;">Подтвердить</button>
    </p>
</div>


