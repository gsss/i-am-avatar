<?php
/**
 * Created by PhpStorm.
 * User: Бог-Творец
 * Date: 10.04.2016
 * Time: 2:04
 */

/** @var \yii\web\View $this */
/** @var \common\models\BillingMain $billing */
/** @var string $destinationAddress */
/** @var \common\models\PaymentRipple $payment */


use yii\helpers\ArrayHelper;
use yii\helpers\Html;

$config = \yii\helpers\Json::decode($destinationAddress);
$content = (new \Endroid\QrCode\QrCode())
    ->setText('ethereum:' . $config['address'] . '?amount=' . \Yii::$app->formatter->asDecimal($billing->sum_after, 18))
    ->setSize(180)
    ->setPadding(20)
    ->setErrorCorrection('high')
    ->setForegroundColor(['r' => 0, 'g' => 0, 'b' => 0, 'a' => 0])
    ->setBackgroundColor(['r' => 255, 'g' => 255, 'b' => 255, 'a' => 0])
    ->setLabelFontSize(16)
    ->setImageType(\Endroid\QrCode\QrCode::IMAGE_TYPE_PNG)
    ->get('png')
;
$src = 'data:image/png;base64,' . base64_encode($content);
?>
<div style="margin-bottom: 200px;">
    <p class="text-center"><img src="<?= $src ?>"/></p>
    <p><?= \Yii::t('c18', 'Вам нужно перевести <b>{sum} ETH</b>. на счет: <b style="font-family: Courier, \'Courier new\', monospace">{card}</b>', [
            'sum'  => \Yii::$app->formatter->asDecimal($billing->sum_after, 18),
            'card' => $config['address'],
        ])

        ?></p>
    <p>После перевода введите идентификатор отправлнной транзакции и нажмите "Подтвердить"</p>
    <p><b>Идентификатор транзакции:</b></p>
    <p><input type="text" class="form-control" id="txid"/></p>
    <p>Мы подтвердим оплату и вы получите токены на свой кошелек.</p>
    <?php
    $this->registerJs(<<<JS
$('#buttonSuccess').click(function(e) {
    ajaxJson({
        url: '/ico/success-payment-etereum',
        data: {
            id: {$billing->id},
            txid: $('#txid').val()
        },
        success: function(ret) {
            window.location = '/ico/payment-success?id={$billing->id}'
        }
    });
});
JS
    );
    ?>
    <hr>
    <p>
        <button class="btn btn-success btn-lg" id="buttonSuccess" style="width: 100%;">Подтвердить</button>
    </p>
</div>
