<?php
/**
 * Created by PhpStorm.
 * User: Бог-Творец
 * Date: 10.04.2016
 * Time: 2:04
 */

/** @var \yii\web\View $this */
/** @var \common\models\BillingMain $billing */
/** @var string $destinationAddress */
/** @var \common\models\PaymentEthereum $payment */


use yii\helpers\ArrayHelper;
use yii\helpers\Html;

$config = \yii\helpers\Json::decode($destinationAddress);

?>
<div style="margin-bottom: 200px;">
    <p><?= \Yii::t('c18', 'Вам нужно перевести <b>{sum} BTC</b>. на счет: <b style="font-family: Courier, \'Courier new\', monospace">{card}</b>', [
            'sum'  => \Yii::$app->formatter->asDecimal($billing->sum_after, 8),
            'card' => $config['address'],
        ])

        ?></p>
    <p>После перевода введите идентификатор отправлнной транзакции и нажмите "Подтвердить"</p>
    <p><b>Идентификатор транзакции:</b></p>
    <p><input type="text" class="form-control" id="txid"/></p>
    <p>Мы подтвердим оплату и вы получите токены на свой кошелек.</p>
    <?php
    $this->registerJs(<<<JS
$('#buttonSuccess').click(function(e) {
    ajaxJson({
        url: '/cabinet-ico/confirm-billing',
        data: {
            txid: $('#txid').val(),
            id: {$billing->id}
        },
        success: function(ret) {
            alert('Успешно');
        }
    });
});
JS
    );
    ?>
    <hr>
    <p>
        <button class="btn btn-success btn-lg" id="buttonSuccess" style="width: 100%;">Подтвердить</button>
    </p>
</div>
