<?php

namespace common\models\piramida;

use app\services\Subscribe;
use common\models\avatar\CurrencyLink;
use common\models\CurrencyIO;
use common\models\PaySystem;
use cs\Application;
use cs\services\BitMask;
use cs\web\Exception;
use yii\db\ActiveRecord;
use yii\db\Query;
use yii\helpers\Json;
use yii\helpers\VarDumper;

/**
 * @property int id
 * @property string name
 * @property string code
 * @property string address
 * @property int    amount
 * @property int    decimals
 */
class Currency extends \yii\db\ActiveRecord
{
    const ELX = 1;
    const ELXT = 2;
    const ELXGOLD = 1;
    const ELXSKY = 2;
    const RUB = 7;

    public static function tableName()
    {
        return 'currency';
    }

    public function getAddress()
    {
        return 'C_' . str_repeat('0', 10 - strlen($this->id)) . $this->id;
    }

    public function getAddressShort()
    {
        $id = (string)$this->id;
        if (strlen($id) > 4) {
            $last4 = substr($id, strlen($id) - 4);
        } else {
            $last4 = str_repeat('0', 4 - strlen($id)) . $id;
        }

        return 'C_...' . $last4;
    }

    public static function getDb()
    {
        return \Yii::$app->dbWallet;
    }

    /**
     * @param int $value
     * @param int | \common\models\piramida\Currency $id
     *
     * @return float
     */
    public static function getValueFromAtom($value, $id)
    {
        /** @var \common\models\piramida\Currency $c */
        $c = null;
        if (!($id instanceof \common\models\piramida\Currency)) {
            $c = \common\models\piramida\Currency::findOne($id);
        } else {
            $c = $id;
        }
        return bcdiv($value, pow(10, $c->decimals), $c->decimals);
    }

    /**
     * @param int | \common\models\avatar\Currency $id
     *
     * @return self
     * @throws
     */
    public static function initFromCurrencyExt($id)
    {
        $id1 = null;
        if ($id instanceof \common\models\avatar\Currency) {
            $id1 = $id->id;
        } else {
            if (Application::isInteger($id)) {
                $id1 = $id;
            } else {
                throw new \Exception('Не верно задан параметр');
            }
        }

        $cio = CurrencyLink::findOne(['currency_ext_id' => $id1]);

        return self::findOne($cio->currency_int_id);
    }

    /**
     * @param float $value
     * @param int | \common\models\piramida\Currency $id
     *
     * @return int
     */
    public static function getAtomFromValue($value, $id)
    {
        /** @var \common\models\piramida\Currency $c */
        $c = null;
        if ($id instanceof \common\models\piramida\Currency) {
            $c = $id;
        } else {
            if (Application::isInteger($id)) {
                $c = \common\models\piramida\Currency::findOne($id);
            } else {
                throw new \Exception('Не верно задан параметр');
            }
        }

        return bcmul($value, pow(10, $c->decimals));
    }

    public function rules()
    {
        return [
            [[
                'decimals',
                'name',
                'code',
            ], 'required'],
            [[
                'decimals',
                'amount',
            ], 'integer'],
            ['code', 'string', 'max' => 10],
            ['name', 'string', 'max' => 100],
            ['address', 'string', 'max' => 70],
        ];
    }

    public function convert($atom)
    {
        return $atom / (pow(10, $this->decimals));
    }


    /**
     * @param array $fields
     *
     * @return self
     * @throws
     */
    public static function add($fields)
    {
        if (!isset($fields['amount'])) $fields['amount'] = 0;
        $i = new self($fields);
        $i->save();

        return $i;
    }
}