<?php

namespace common\models\piramida;

use Yii;
use common\extensions\XSSActiveRecord;
use yii\base\Exception;
use yii\base\Security;
use yii\db\ActiveRecord;

/**
 * @property integer id
 * @property integer time
 * @property double  rub
 * @property double  usd
 */
class BitCoinKurs extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'nw_bit_coin_kurs';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['time'], 'required'],
            [['time'], 'integer'],
            [['rub', 'usd'], 'double'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'   => 'ID',
            'time' => 'Время',
            'rub'  => 'RUB',
            'usd'  => 'USD',
        ];
    }
}
