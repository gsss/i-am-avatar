<?php

namespace common\models\piramida;

use app\services\Subscribe;
use cs\services\BitMask;
use cs\services\VarDumper;
use cs\web\Exception;
use yii\db\ActiveRecord;
use yii\db\Query;
use app\models\Piramida\Transaction;

/**
 * 1. Что делает?
 * Описывает класс продаваемого пакета
 *
 * 2. какие есть свойства объекта?
 * @property double price_in цена которая будет начисляться внутри системы за покупку пакета
 * @property double price_out цена сколько стоит пакет в рублях
 * @property string name
 * 3. кикими функциями обладает?
 *
 * Class Packet
 *
 * @package app\models\Piramida
 */
class Packet extends ActiveRecord
{
    public static function tableName()
    {
        return 'nw_packets';
    }


    public function rules()
    {
        return [
            [['price_in', 'price_out', 'name', ],'required'],
            [['price_in', 'price_out', ],'double'],
            [['name', ],'string', 'max' => 255],
        ];
    }
}