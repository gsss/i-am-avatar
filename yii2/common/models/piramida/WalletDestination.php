<?php

namespace common\models\piramida;

use app\services\Subscribe;
use cs\services\BitMask;
use cs\web\Exception;
use yii\db\ActiveRecord;
use yii\db\Query;
use app\models\Transaction;

/**
 * Модель которая хранит запись куда были выведены стредства с кошелька
 *
 * Class WalletDestination
 * @package avatar\models
 */
class WalletDestination extends ActiveRecord
{
    public static function tableName()
    {
        return 'nw_wallets_destinations_link';
    }

    public function rules()
    {
        return [
            [['destination_id', 'data'], 'required'],
            [['destination_id'], 'integer'],
            [['data'], 'string', 'max' => 4000],
        ];
    }

}