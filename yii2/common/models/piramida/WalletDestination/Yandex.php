<?php
/**
 * Created by PhpStorm.
 * User: Бог-Творец
 * Date: 10.04.2016
 * Time: 2:24
 */
namespace common\models\piramida\WalletDestination;



class Yandex extends Object implements \common\models\piramida\WalletDestinationInterface
{
    public $transaction;

    public function getTransactionInfo()
    {
        return $this->transaction;
    }

    public function getTypeId()
    {
        return 2;
    }
}