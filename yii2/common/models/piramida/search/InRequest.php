<?php

namespace app\modules\Graal\models\search;

use app\models\Piramida\OutRequest;
use app\models\User;
use cs\Application;
use cs\services\Str;
use cs\services\VarDumper;
use Yii;
use yii\base\Model;
use cs\Widget\FileUpload2\FileUpload;
use yii\data\ActiveDataProvider;
use yii\data\Sort;
use yii\db\ActiveQuery;
use yii\db\Query;
use yii\helpers\Html;

/**
 */
class InRequest extends Model
{
    /** @var  int */
    public $id;

    /** @var  double */
    public $price;

    /** @var  int 0/1 */
    public $is_paid;

    /** @var  int 0/1 */
    public $is_answer_from_client;

    /** @var  int 0/1 */
    public $is_answer_from_shop;

    /** @var  string, может быть указан id пользователя, часть email и часть имени или фамилии */
    public $user;

    public function rules()
    {
        return [
            [['id', 'price', 'is_paid', 'is_answer_from_client', 'is_answer_from_shop'], 'default'],
            [['id', 'is_paid','is_answer_from_client','is_answer_from_shop'], 'integer'],
            [['price', ], 'double'],
            [['user'], 'string'],
        ];
    }

    /**
     * @param array $params
     * @param array $where
     * @param \yii\data\Sort $sort
     *
     * @return \yii\data\ActiveDataProvider
     */
    public function search(array $params, array $where = null, Sort $sort = null)
    {
        /** @var \yii\db\ActiveQuery $query */
        $query = \app\modules\Graal\models\InRequest::find()
        ;
        if (!is_null($where)) $query->andWhere($where);

        $dataProvider = new ActiveDataProvider([
            'query'      => $query,
            'pagination' => [
                'pageSize' => 20,
            ],
        ]);
        if (!is_null($sort)) $dataProvider->sort = $sort;

        // загружаю данные
        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        // добавляю фильтры
        if ($this->id) {
            $query->andWhere(['graal_in_requests.id' => $this->id]);
        }
        if ($this->price) {
            $query->andWhere(['>', 'graal_in_requests.price', $this->price]);
        }
        if ($this->is_paid) {
            $query->andWhere(['graal_in_requests.is_paid' => $this->is_paid]);
        }
        if (Application::isInteger($this->is_answer_from_client)) {
            $query->andWhere(['graal_in_requests.is_answer_from_client' =>$this->is_answer_from_client]);
        }
        if (Application::isInteger($this->is_answer_from_shop)) {
            $query->andWhere(['graal_in_requests.is_answer_from_shop' =>$this->is_answer_from_shop]);
        }
        if ($this->user) {
            $user = $this->user;
            if (Application::isInteger($user)) {
                $query->andWhere(['graal_in_requests.user_id' => $user]);
            } else {
                if (preg_match('/[a-zA-Z-_@.]/', $user)) {
                    $query
                        ->innerJoin('gs_users', 'gs_users.id = graal_in_requests.user_id')
                        ->andWhere([
                            'or',
                            ['like', 'gs_users.email', $user],
                            ['like', 'gs_users.name_last', $user],
                            ['like', 'gs_users.name_first', $user],
                        ]);
                } else {
                    $query
                        ->innerJoin('gs_users', 'gs_users.id = graal_in_requests.user_id')
                        ->andWhere([
                            'or',
                            ['like', 'gs_users.name_last', $user],
                            ['like', 'gs_users.name_first', $user],
                        ]);
                }
            }
        }

        return $dataProvider;
    }
}
