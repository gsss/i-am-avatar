<?php

namespace common\models\piramida;

use app\services\Subscribe;
use cs\services\BitMask;
use cs\web\Exception;
use yii\db\ActiveRecord;
use yii\db\Query;
use app\models\Piramida\Transaction;

/**
 * Сообщение по запросу на вывод средств
 *
 * @property int    $id                            - идентификатор
 * @property int    $request_id                    - идентификатор запроса nw_out_requests.id
 * @property int    $direction                     - направление сообщения 1 - для клиента, 2 - для магазина
 * @property int    $status                        - статус
 * @property int    $datetime                      - время сообщения
 * @property string $message                       - сообщение
 * @property int    $user_id                       - идентификатор пользователя который написал сообщение
 *
 * Class InRequest
 * @package app\models\Piramida
 */
class OutRequestMessage extends ActiveRecord
{
    const DIRECTION_TO_CLIENT = 1;
    const DIRECTION_TO_SHOP = 2;

    public static function tableName()
    {
        return 'nw_out_requests_messages';
    }


    public function rules()
    {
        return [
            [['request_id', 'datetime'], 'required'],
            [[
                'request_id',
                'direction',
                'status',
                'datetime',
            ], 'integer'],
            [['message',], 'string', 'max' => 1000],
        ];
    }

    public static function add($fields)
    {
        $fields['datetime'] = time();
        $new = new self($fields);
        $new->save();
        $new->id = self::getDb()->lastInsertID;

        return $new;
    }


}