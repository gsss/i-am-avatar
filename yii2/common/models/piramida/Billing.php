<?php

namespace common\models\piramida;

use app\models\Penalty;
use app\services\Subscribe;
use common\models\PaySystem;
use cs\Application;
use cs\services\BitMask;
use cs\web\Exception;
use yii\db\ActiveRecord;
use yii\db\Query;
use app\models\Piramida\Transaction;
use yii\helpers\Json;
use yii\helpers\VarDumper;

/**
 * Содержит информацию об внесении денежных средств из внешней системы
 * Предназначен только для рублей и валют которые проходят по транзакциям 'nw_*'
 *
 * @property int    id              - идентификатор записи
 * @property double datetime        - время операции
 * @property double sum_before      - сумма до транзакции, сумма которую пользователь фактически оплатил за транзакцию
 * @property double sum_after       - сумма которая постпила в систему после транзакции
 * @property int    is_paid         - флаг подтвержден ли ввод денег из платежной системы 0 - нет 1 - да
 * @property int    transaction_id  - транзакция к которой принадлежит данная операция
 * @property int    source_id       - идентификатор платежной системы
 * @property string data            - данные о транзакции из платежной системы, JSON
 * @property int    config_id       - paysystems_config.id
 * @property int    currency_id     - db.currency.id
 *
 * Class Billing
 * @package app\models\Piramida
 */
class Billing extends ActiveRecord
{
    const TYPE_IN       = 1;
    const TYPE_PAY      = 2;
    const TYPE_BLAGO    = 3;
    const TYPE_PARTNER    = 3;

    public static function tableName()
    {
        return 'nw_billing';
    }

    /**
     * @param mixed $condition
     * @return static
     * @throws \yii\base\Exception
     */
    public static function findOne($condition)
    {
        $item = parent::findOne($condition);
        if (is_null($item)) throw new \yii\base\Exception('Не найден счет');

        return $item;
    }

    /**
     * Добавляет запись
     *
     * @param $fields
     * @return Billing
     */
    public static function add($fields)
    {
        $fields['datetime'] = microtime(true);
        if (!isset($fields['is_paid'])) {
            $fields['is_paid'] = 0;
        }
        $iam = new self($fields);
        $ret = $iam->save();
        if (!$ret) {
            throw new \yii\base\Exception('Не удалось сохранить счет ' . \yii\helpers\VarDumper::dumpAsString($iam->errors));
        }
        \Yii::info('BillingID=' . self::getDb()->lastInsertID, 'gs\\BillingID');
        $iam->id = self::getDb()->lastInsertID;

        return $iam;
    }

    /**
     * Подтверждает внесение средств
     *
     * @param \common\models\piramida\Transaction $transaction
     * @param \common\models\piramida\WalletSourceInterface $source
     *
     * @throws \yii\base\Exception
     */
    public function success($transaction, $source)
    {
        $this->transaction_id = $transaction->id;
        $this->data = Json::encode($source->getTransactionInfo());
        if (empty($this->sum_after)) {
            $this->sum_after = $source->getSumAfter();
        }
        $this->is_paid = 1;
        $res = $this->save();
        if (!$res) {
            throw new \yii\base\Exception('Не удалось сохранить Счет #' . $this->id . VarDumper::dumpAsString($this->errors));
        }
    }

    /**
     * @return \common\models\PaySystem
     * @throws \yii\base\Exception
     */
    public function getPaySystem()
    {
        return PaySystem::findOne($this->source_id);
    }

    /**
     * @return string
     */
    public function getPaySystemClass()
    {
        $paySystem = $this->getPaySystem();
        return $paySystem->getClassName();
    }

    /**
     * @return \common\models\piramida\WalletSourceInterface
     */
    public function getPaySystemSource()
    {
        $paySystem = $this->getPaySystem();

        return $paySystem->getClass([
            'sumAfter'    => $this->sum_before,
            'sumBefore'   => $this->sum_after,
            'paymentType' => $paySystem->id,
        ]);
    }
}