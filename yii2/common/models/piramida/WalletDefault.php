<?php
/**
 * Created by PhpStorm.
 * User: Бог-Творец
 * Date: 27.05.2016
 * Time: 9:03
 */

namespace common\models\piramida;

use cs\services\VarDumper;
use Exception;
use yii\db\ActiveRecord;

/**
 * @property int id
 * @property int wallet_id
 * @property int currency_id
 */
class WalletDefault extends ActiveRecord
{
    public static function tableName()
    {
        return 'wallet_default';
    }
}