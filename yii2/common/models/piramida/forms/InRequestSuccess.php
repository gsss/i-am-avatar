<?php
/**
 * Created by PhpStorm.
 * User: Бог-Творец
 * Date: 31.07.2016
 * Time: 10:18
 */

namespace app\modules\Graal\models\forms;

use app\models\Piramida\Billing;
use app\models\Piramida\WalletSource\BitCoin;
use app\models\Piramida\WalletSource\PayPal;
use app\models\Piramida\WalletSource\VisaMasterCard;
use app\models\Piramida\WalletSource\Yandex;
use app\modules\Graal\models\InRequest;
use common\models\PaySystem;
use cs\Application;
use cs\services\VarDumper;
use yii\base\Model;

class InRequestSuccess extends Model
{
    public $id;
    public $sumBefore;
    public $sumAfter;
    public $comment;
    public $owner;
    public $transaction_id;

    /** @var  \app\modules\Graal\models\InRequest */
    public $request;

    /** @var  \app\models\Piramida\Billing */
    public $billing;

    public function init()
    {
        if ($this->id) {
            $this->request = InRequest::findOne($this->id);
            $this->billing = $this->request->getBilling();
            $this->sumBefore = $this->billing->sum_before;
            $this->sumAfter = $this->billing->sum_after;
        }
    }

    public function rules()
    {
        return [
            [['comment'], 'required'],
            [['sumBefore', 'sumAfter'], 'double'],
            [['id'], 'integer'],
            [['comment'], 'string', 'max' => 4000],
            [['owner'], 'string', 'max' => 50],
            [['transaction_id'], 'string', 'max' => 70],
        ];
    }

    public function attributeLabels()
    {
        return [
            'sumBefore'      => 'Сумма до внесения',
            'sumAfter'       => 'Сумма после внесения',
            'comment'        => 'Дополнительная информация о транзакции',
            'owner'          => 'Владелец',
            'transaction_id' => 'Идентификатор транзакции',
        ];
    }

    public function success($id)
    {
        $request = InRequest::findOne($id);
        $billing = $request->getBilling();
        $class = PaySystem::findOne($billing->source_id)->getClass([
            'source_id'   => $billing->source_id,
            'transaction' => [
                'comment'   => $this->comment,
                'sumBefore' => $this->sumBefore,
                'sumAfter'  => $this->sumAfter,
            ],
            'sumBefore' => $this->sumBefore,
            'sumAfter'  => $this->sumAfter,
        ]);
        $return = $request->success($class, $this->owner, $this->transaction_id);

        \cs\Application::mail($request->getOwner()->getEmail(), 'Ваша инвестиция подтверждена', 'graal/out-request-to-client-confirm', [
            'request'     => $request,
            'message'     => $return['message'],
            'transaction' => $return['transaction'],
        ]);

        return true;
    }
}