<?php

namespace common\models\piramida;

use app\services\Subscribe;
use cs\services\BitMask;
use cs\web\Exception;
use yii\db\ActiveRecord;
use yii\db\Query;
use app\models\Piramida\Transaction;

/**
 *
 */
class YandexTransaction extends ActiveRecord
{

    public static function tableName()
    {
        return 'yandex_transaction';
    }

    public static function add($fields)
    {
        $fields['created_at'] = time();
        $iAm = new self($fields);
        $ret = $iAm->save();
        $iAm->id = self::getDb()->lastInsertID;

        return $iAm;
    }


}