<?php

namespace common\models\piramida;

use app\services\Subscribe;
use cs\services\BitMask;
use cs\web\Exception;
use yii\db\ActiveRecord;
use yii\db\Query;
use app\models\Piramida\Transaction;

/**
 * Элементарная операция с кошельком
 * Всего их две:
 * - снять деньги
 * - положить деньги
 * За это отвечают поле `type` и константы self::IN и self::OUT
 * содержит информацию о кошельке с которым производится операция
 *
 * @property int    id
 * @property int    wallet_id      - идентификатор кошелька
 * @property int    transaction_id - транзакция к которой принадлежит данная операция
 * @property int    type            - тип лперации
 * @property int    datetime       - время операции
 * @property float  before         - размер кошелька до операции
 * @property float  after          - размер кошелька после операции
 * @property float  amount         - денежный размер операции
 * @property string address         -
 * @property string hash
 * @property string comment        - комментарий для операции, чтобы владельцу кошелька было понятно что это за
 *           операция
 *
 *
 * Class Operation
 * @package app\models\Piramida
 */
class Operation extends ActiveRecord
{
    const TYPE_EMISSION = 2;
    const TYPE_IN = 1;
    const TYPE_OUT = -1;
    const TYPE_BURN = -2;

    public static function tableName()
    {
        return 'operations';
    }

    public function getAddress()
    {
        return 'O_' . str_repeat('0', 20 - strlen($this->id)) . $this->id;
    }

    public function getAddressShort()
    {
        $id = (string)$this->id;
        if (strlen($id) > 4) {
            $last4 = substr($id, strlen($id) - 4);
        } else {
            $last4 = str_repeat('0', 4 - strlen($id)) . $id;
        }

        return 'O_...' . $last4;
    }

    public static function getDb()
    {
        return \Yii::$app->dbWallet;
    }

    public function rules()
    {
        return [
            [['wallet_id', 'before', 'after', 'amount', 'type', 'datetime'], 'required'],
            [['wallet_id', 'transaction_id', 'type', 'before', 'after', 'amount'], 'integer'],
            ['datetime', 'double'],
            ['comment', 'string', 'max' => 255],
            ['address', 'string', 'max' => 70],
            ['hash', 'string', 'max' => 64],
        ];
    }

    public function hash()
    {
        return hash('sha256', $this->comment . $this->before . $this->after . $this->amount . $this->type . $this->id . $this->wallet_id . $this->transaction_id . ((int)$this->datetime));
    }

    public static function add($fields)
    {
        $fields['datetime'] = (int)(microtime(true) * 1000);
        $iAm = new self($fields);
        $ret = $iAm->save();
        if (!$ret) throw new \Exception('\common\models\piramida\Operation::add');
        $iAm->id = self::getDb()->lastInsertID;
        $iAm->hash = $iAm->hash();
        $ret = $iAm->save();
        if (!$ret) throw new \Exception('\common\models\piramida\Operation::add');

        return $iAm;
    }


}