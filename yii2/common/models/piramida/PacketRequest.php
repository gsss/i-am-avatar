<?php

namespace common\models\piramida;

use app\services\Subscribe;
use cs\services\BitMask;
use cs\services\VarDumper;
use cs\web\Exception;
use yii\db\ActiveRecord;
use yii\db\Query;
use app\models\Piramida\Transaction;

/**
 * 1. Что делает?
 * Описывает класс продаваемого пакета
 *
 * 2. какие есть свойства объекта?
 * @property int packet_id идентификатор заказываемого пакета
 * @property int is_paid   Оплачено?
 *                         null, 0 - нет
 *                         1 - да
 * @property int status
 * @property int user_id   идентификатор заказчика
 * @property int datetime  время заказа
 *
 * 3. какими функциями обладает?
 *
 * Class PacketRequest
 *
 * @package app\models\Piramida
 */
class PacketRequest extends ActiveRecord
{
    const STATUS_REQUESTED = 1;
    const STATUS_PAID = 2;

    public static function tableName()
    {
        return 'nw_packets_requests';
    }


    public function rules()
    {
        return [
            [['packet_id', 'user_id', 'datetime', ], 'required'],
            [['packet_id', 'user_id', 'datetime', 'is_paid', 'status',], 'integer'],
        ];
    }
}