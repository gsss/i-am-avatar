<?php

namespace common\models\piramida;

use app\models\User;
use app\models\User2;
use app\services\Subscribe;
use cs\services\BitMask;
use cs\web\Exception;
use yii\db\ActiveRecord;
use yii\db\Query;
use app\models\Piramida\Transaction;
use yii\helpers\ArrayHelper;

/**
 * Заявка на пополнение кошелька
 *
 * @property int   $id                           - идентификатор
 * @property int   $wallet_id                    - идентификатор кошелька
 * @property int   $transaction_id               - транзакция для опереции начисления
 * @property int   $datetime                     - время операции
 * @property float $price                        - размер пополнения
 * @property int   $is_paid                      - размер пополнения
 * @property int   $is_answer_from_client        - Есть ответ от клиента?
 * @property int   $is_answer_from_shop          - Есть ответ из магазина?
 * @property int   $last_message_time            - время последнего ответа
 * @property int   $is_hide                      - 0/1 скрыт?
 *
 * Class InRequest
 * @package app\models\Piramida
 */
class   OutRequest extends ActiveRecord
{
    const STATUS_SEND           = 1;
    const STATUS_CONFIRM_SHOP   = 2;
    const STATUS_CONFIRM_CLIENT = 3;

    public static function tableName()
    {
        return 'nw_out_requests';
    }

    public function rules()
    {
        return [
            [['wallet_id', 'price', 'datetime',], 'required'],
            [[
                'transaction_id',
                'is_paid',
                'datetime',
                'wallet_id', 
                'destination_id',
                'is_answer_from_client',
                'is_answer_from_shop', 
                'last_message_time',
                'is_hide',
            ], 'integer'],
            [['price',], 'double'],
        ];
    }

    public static function add($fields)
    {
        $fields['datetime'] = time();
        $fields['is_paid'] = 0;
        $new = new self($fields);
        $new->save();
        $new->id = self::getDb()->lastInsertID;

        return $new;
    }

    /**
     * @param int $condition
     *
     * @return static
     * @throws \cs\web\Exception
     */
    public static function findOne($condition)
    {
        $item = parent::findOne($condition);
        if (is_null($item)) {
            throw new Exception('Нет такого заказа');
        }
        return $item;
    }

    /**
     * @return \app\models\User
     */
    public function getOwner()
    {
        return User::find($this->getOwnerId());
    }

    public function getOwnerId()
    {
        return $this->getWallet()->user_id;
    }

    public function getWallet()
    {
        return Wallet::findOne($this->wallet_id);
    }

    /**
     * Подтверждает заявку на вывод денег
     * Создает транзакцию
     *
     * @param WalletDestinationInterface $destination транзакция о переводе из внешней системы
     *
     * @return array
     * [
     *      'transaction' => \app\models\Piramida\Transaction
     *      'message'     => \app\models\Shop\RequestMessage
     * ]
     *
     * @throws \cs\web\Exception
     */
    public function success(WalletDestinationInterface $destination)
    {
        $transaction = \Yii::$app->db->beginTransaction();
        try {
            $wallet_id = $this->wallet_id;
            $wallet = Wallet::findOne($wallet_id);
            $t = $wallet->out($this->price);
            $this->transaction_id = $t->id;
            $this->is_paid = 1;
            $this->destination_id =  $destination->getTypeId();
            $this->data = json_encode($destination->getTransactionInfo(), JSON_UNESCAPED_UNICODE);
            $result = $this->save();
            if (!$result) {
                $transaction->rollBack();

                throw new Exception('Не удалось сохранить запрос на вывод');
            } else {
                $message = $this->addStatusToClient(self::STATUS_CONFIRM_SHOP);
                $transaction->commit();
            }
        } catch (\Exception $e) {
            $transaction->rollBack();

            throw $e;
        }

        return [
            'transaction' => $t,
            'message'     => $message,
        ];
    }


    /**
     * Добавить сообщение или статус
     *
     * @param array $fields поля для сообщения
     * @param int $direction направление сообщения self::DIRECTION_*
     *
     * @return \app\models\Shop\RequestMessage
     */
    public function addMessageItem($fields, $direction)
    {
        $this->is_answer_from_shop = ($direction == OutRequestMessage::DIRECTION_TO_CLIENT) ? 1 : 0;
        $this->is_answer_from_client = ($direction == OutRequestMessage::DIRECTION_TO_CLIENT) ? 0 : 1;
        $this->last_message_time = time();

        if (isset($fields['status'])) {
            $this->status = $fields['status'];
        }
        $this->save();

        return OutRequestMessage::add(ArrayHelper::merge($fields, [
            'request_id' => $this->id,
            'direction'  => $direction,
        ]));
    }

    /**
     * Добавить сообщение
     *
     * @param string $message сообщение
     * @param int $direction направление сообщения self::DIRECTION_*
     *
     * @return \app\models\Shop\RequestMessage
     */
    public function addMessage($message, $direction)
    {
        $fields = [
            'message' => $message,
        ];
        $fields['user_id'] = \Yii::$app->user->id;

        return $this->addMessageItem($fields, $direction);
    }

    /**
     * Добавить сообщение для клиента
     *
     * @param string $message сообщение
     *
     * @return \app\models\Shop\RequestMessage
     */
    public function addMessageToClient($message)
    {
        return $this->addMessage($message, OutRequestMessage::DIRECTION_TO_CLIENT);
    }

    /**
     * Добавить сообщение для клиента
     *
     * @param string $message сообщение
     *
     * @return \app\models\Shop\RequestMessage
     */
    public function addMessageToShop($message)
    {
        return $this->addMessage($message, OutRequestMessage::DIRECTION_TO_SHOP);
    }

    /**
     * Добавить статус
     *
     * @param int | array $status статус self::STATUS_* или массив со статусом и сообщением
     * @param int $direction направление сообщения self::DIRECTION_*
     *
     * @return \app\models\Shop\RequestMessage
     */
    public function addStatus($status, $direction)
    {
        if (!is_array($status)) {
            $fields = [
                'status' => $status,
            ];
        } else {
            $fields = $status;
        }

        return $this->addMessageItem($fields, $direction);
    }

    /**
     * Добавить статус к клиенту
     *
     * @param int | array $status статус self::STATUS_* или массив со статусом и сообщением
     *
     * @return \app\models\Shop\RequestMessage
     */
    public function addStatusToClient($status)
    {
        if (!is_array($status)) {
            $fields = [
                'status' => $status,
            ];
        } else {
            $fields = $status;
        }

        return $this->addMessageItem($fields, OutRequestMessage::DIRECTION_TO_CLIENT);
    }

    /**
     * Добавить статус в магазин
     *
     * @param int | array $status статус self::STATUS_* или массив со статусом и сообщением
     *
     * @return \app\models\Shop\RequestMessage
     */
    public function addStatusToShop($status)
    {
        if (!is_array($status)) {
            $fields = [
                'status' => $status,
            ];
        } else {
            $fields = $status;
        }

        return $this->addMessageItem($fields, OutRequestMessage::DIRECTION_TO_SHOP);
    }

    /**
     * Возвращает заготовку запроса с сообщениями
     *
     * @return \yii\db\Query
     */
    public function getMessages()
    {
        return OutRequestMessage::find()
            ->andWhere(['request_id' => $this->id])
            ;
    }

}