<?php

namespace common\models\piramida;

use app\services\Subscribe;
use cs\services\BitMask;
use cs\web\Exception;
use yii\db\ActiveRecord;
use yii\db\Query;
use app\models\Transaction;

interface WalletSourceInterface
{
    /**
     * @return array информация о транзакции в платежной системе
     * @deprecated
     */
    public function getTransactionInfo();

    /**
     * Возвращает идентификатор платежной системы
     *
     * @return int
     * @deprecated
     */
    public function getTypeId();

    /**
     * Возвращает процентную ставку на перевод
     *
     * @return double
     */
    public function getTax();

    /**
     * Выдает цену с учетом оплаты комиссии
     *
     * @param float $price цена которую хотим получить от клиента через прием денег через эту платежную систему
     *
     * @return float цена для оплаты в платежной соистеме
     */
    public function getPriceWithTax($price);

    /**
     * Выдает сумму до перевода,
     * которую пользователь фактически заплатил,
     * до вычета процентов
     *
     * @return double
     */
    public function getSumBefore();

    /**
     * Выдает сумму после перевода,
     * после вычета процентов
     *
     * @return double
     */
    public function getSumAfter();


    /**
     * Формирует форму
     *
     * @param \common\models\BillingMain        $billing
     * @param string                            $description
     * @param string                            $destinationName - наименование поучателя
     *
     * @param null | array $options
     * - form - array - параметры(аттрибуты) тега формы
     * - values - array - доп параметры для формы как скрытые поля (ключ => значение)
     *
     * @return string
     */
    public function getForm($billing, $description, $destinationName, $options = null);


    /**
     * Обрабатывает входящую транзакцию
     *
     * @param array $actions
     *
     * @return bool
     */
    public function success($actions);



}