<?php
/**
 * Created by PhpStorm.
 * User: god
 * Date: 28.02.2017
 * Time: 23:49
 */

namespace common\models;

use yii\db\ActiveRecord;

/**
 * Class MapBankomat
 *
 * @property integer id
 * @property string  name
 * @property string  image
 * @property string  content
 * @property string  description
 * @property integer created_at
 * @property integer user_id
 * @property integer btc
 * @property integer eth
 * @property integer enc
 * @property float   lat
 * @property float   lng
 *
 * @package common\models
 */
class MapBankomat extends ActiveRecord
{
    public static function tableName()
    {
        return 'map_bankomat';
    }
}