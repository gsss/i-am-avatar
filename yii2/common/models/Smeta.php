<?php
/**
 * Created by PhpStorm.
 * User: Бог-Творец
 * Date: 02.09.2016
 * Time: 9:31
 */

namespace common\models;

use cs\services\VarDumper;
use yii\base\Exception;
use yii\db\ActiveRecord;

/**
 * @property int id
 * @property int school_id
 * @property int created_at
 * @property int category_id
 * @property int year
 * @property int month
 * @property int sum
 */
class Smeta extends ActiveRecord
{
    public static function tableName()
    {
        return 'school_smeta';
    }


    public static function add($fields)
    {
        $new = new self($fields);
        $ret = $new->save();
        $new->id = self::getDb()->lastInsertID;

        return $new;
    }
} 