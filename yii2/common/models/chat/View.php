<?php
/**
 * Created by PhpStorm.
 * User: s.arhangelskiy
 * Date: 12.04.2016
 * Time: 16:13
 */

namespace common\models\chat;


use yii\db\ActiveRecord;
use yii\db\Query;
use yii\helpers\ArrayHelper;
use yii\helpers\VarDumper;

/**
 * Содержит информацию о сообщении
 *
 * @property int    id
 * @property int    message_id
 * @property int    owner_id
 * @property int    partner_id
 * @property int    is_view
 *
 * Class View
 */
class View extends ActiveRecord
{
    public static function tableName()
    {
        return 'gs_chat_view';
    }

    public function rules()
    {
        return [
            [['message_id', 'owner_id', 'partner_id'], 'required'],
            [['message_id', 'owner_id', 'partner_id', 'is_view'], 'integer'],
        ];
    }
}