<?php
/**
 * Created by PhpStorm.
 * User: s.arhangelskiy
 * Date: 12.04.2016
 * Time: 16:13
 */

namespace common\models\chat;


use yii\db\ActiveRecord;
use yii\db\Query;
use yii\helpers\ArrayHelper;
use yii\helpers\VarDumper;

/**
 * Содержит информацию о сообщении
 *
 * @property int    id
 * @property string message
 * @property int    time
 *
 *
 * Class Message
 */
class Message extends ActiveRecord
{
    public static function tableName()
    {
        return 'chat_message';
    }

    public function rules()
    {
        return [
            [['message', 'time'], 'required'],
            [['message'], 'string', 'max' => 10000],
            [['time'], 'integer'],
        ];
    }
}