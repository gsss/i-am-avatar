<?php


namespace common\models\chat;


use yii\db\ActiveRecord;

class Room extends ActiveRecord
{
    const TYPE_ID_GROUP = 1;
    const TYPE_ID_TET_A_TET = 2;
}