<?php
/**
 * Created by PhpStorm.
 * User: god
 * Date: 28.02.2017
 * Time: 23:49
 */

namespace common\models\school;

use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;

/**
 * @property integer id
 * @property string  name
 * @property integer is_dns_verified
 * @property integer school_id
 * @property integer created_at
 * @property integer user_id
 */
class DnsRequest extends ActiveRecord
{
    public static function tableName()
    {
        return 'school_dns_request';
    }

    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at'],
                ],
                // если вместо метки времени UNIX используется datetime:
                // 'value' => new Expression('NOW()'),
            ],
        ];
    }

    public function rules()
    {
        return [
            ['name', 'string'],
            ['name', 'validateName'],

            ['is_dns_verified', 'integer'],
            ['school_id', 'integer'],
            ['created_at', 'integer'],
        ];
    }

    public function validateName($attribute, $params)
    {
        if (!$this->hasErrors()) {
            if (strpos($this->name, ' ') !== false) {
                $this->addError($attribute, 'Пробелы не допустимы');
                return;
            }
            if (!preg_match('/^[a-z0-9-_.]+$/', $this->name)) {
                $this->addError($attribute, 'Возможны только малые буквы латинского алфавита, цифры, дефис, подчеркивание и точка для домена второго уровня');
                return;
            }
        }
    }

    /**
     * @return School
     */
    public function getSchool()
    {
        return School::findOne($this->school_id);
    }
}