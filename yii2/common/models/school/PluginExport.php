<?php
/**
 * Created by PhpStorm.
 * User: god
 * Date: 28.02.2017
 * Time: 23:49
 */

namespace common\models\school;

use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\helpers\Json;

/**
 * @property integer id
 * @property string  name
 * @property string  class
 */
class PluginExport extends ActiveRecord
{
    public static function tableName()
    {
        return 'school_out_plugin';
    }

    public function rules()
    {
        return [
            ['name', 'string'],
        ];
    }
}