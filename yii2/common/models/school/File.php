<?php
/**
 * Created by PhpStorm.
 * User: god
 * Date: 28.02.2017
 * Time: 23:49
 */

namespace common\models\school;

use common\models\UserAvatar;
use cs\services\VarDumper;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;

/**
 * @property integer id
 * @property integer school_id      Идентификатор школы
 * @property integer type_id        Тип файла, таблица и поле
 * @property integer is_main        Флаг. Это главный файл? Не обрезок? 0 - это файл оригинал. 1 - Это обрезок. По умолчанию - 0.
 * @property integer size           Размер файла в байтах
 * @property integer created_at
 * @property string  file           Полный путь к файлу
 */
class File extends ActiveRecord
{
    public static function tableName()
    {
        return 'school_file';
    }

    public function behaviors()
    {
        return [
            [
                'class' => '\yii\behaviors\TimestampBehavior',
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at'],
                ],
                // если вместо метки времени UNIX используется datetime:
                // 'value' => new Expression('NOW()'),
            ],
        ];
    }

    public function rules()
    {
        return [
            ['school_id', 'integer'],
            ['type_id', 'integer'],
            ['size', 'integer'],
            ['created_at', 'integer'],
            ['is_main', 'integer'],
            ['file', 'string'],
        ];
    }

    public static function add($fields)
    {
        $i = new static($fields);
        $i->save();
        $i->id = self::getDb()->lastInsertID;

        return $i;
    }

    /**
     * @return \common\models\school\School
     */
    function getSchool()
    {
        return School::findOne($this->school_id);
    }
}