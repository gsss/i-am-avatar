<?php
/**
 * Created by PhpStorm.
 * User: god
 * Date: 28.02.2017
 * Time: 23:49
 */

namespace common\models\school;

use yii\db\ActiveRecord;

/**
 * @property integer id
 * @property integer subscribe_id
 * @property integer created_at
 * @property integer gs_subscribe_id Идентификатор запущенной рассылки gs_subscribe.id
 * @property integer unsubscribe_count
 * @property integer views_count
 * @property integer email_count
 */
class SubscribeItem extends ActiveRecord
{
    public static function tableName()
    {
        return 'school_potok_subscribe_item';
    }

    public static function add($fields)
    {
        $i = new static($fields);
        $i->save();
        $i->id = self::getDb()->lastInsertID;

        return $i;
    }

    public function behaviors()
    {
        return [
            [
                'class' => '\yii\behaviors\TimestampBehavior',
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at'],
                ],
                // если вместо метки времени UNIX используется datetime:
                // 'value' => new Expression('NOW()'),
            ],
        ];
    }
}