<?php
/**
 * Created by PhpStorm.
 * User: god
 * Date: 28.02.2017
 * Time: 23:49
 */

namespace common\models\school;

use yii\db\ActiveRecord;

/**
 * @property integer id
 * @property integer page_id
 * @property integer is_bot
 * @property integer created_at
 */
class PageStat extends ActiveRecord
{
    public static function tableName()
    {
        return 'school_page_stat';
    }

    public function rules()
    {
        return [
            ['page_id', 'integer'],
            ['created_at', 'integer'],
            ['is_bot', 'integer'],
        ];
    }

    public function behaviors()
    {
        return [
            [
                'class' => '\yii\behaviors\TimestampBehavior',
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at'],
                ],
                // если вместо метки времени UNIX используется datetime:
                // 'value' => new Expression('NOW()'),
            ],
        ];
    }

    public static function add($fields)
    {
        $i = new self($fields);
        $i->save();
        $i->id = self::getDb()->lastInsertID;

        return $i;
    }

}