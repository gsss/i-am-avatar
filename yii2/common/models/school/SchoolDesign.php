<?php
/**
 * Created by PhpStorm.
 * User: god
 * Date: 28.02.2017
 * Time: 23:49
 */

namespace common\models\school;

use common\models\UserAvatar;
use cs\services\VarDumper;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;
use yii\helpers\StringHelper;
use yii\web\HttpException;

/**
 * @property integer    id
 * @property integer    school_id
 * @property integer    registration_header_id
 * @property integer    registration_footer_id
 * @property integer    login_header_id
 * @property integer    login_footer_id
 * @property integer    card_design_id
 * @property integer    is_add_user_in_command_after_register
 * @property string     logo_top_menu
 * @property string     video_intro
 * @property string     background_cabinet
 * @property string     footer_site
 * @property string     footer_mail
 * @property string     cabinet_menu
 * @property string     guest_frontend
 * @property string     auth_frontend
 * @property string     user_menu
 */
class SchoolDesign extends ActiveRecord
{
    public static function tableName()
    {
        return 'school_design';
    }

    /**
     * @return \common\models\school\School
     */
    public function getSchool()
    {
        return School::findOne($this->school_id);
    }

    public function rules()
    {
        return [
            [array_keys($this->attributes), 'default'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'auth_frontend'          => 'Меню для лицевого сайта (для авторизованного пользователя)',
            'guest_frontend'         => 'Меню для лицевого сайта (для гостя)',
            'cabinet_menu'           => 'Меню для кабинета верхнее',
            'login_header_id'        => 'Шапка для страницы входа',
            'login_footer_id'        => 'Подвал для страницы входа',
            'registration_header_id' => 'Шапка для страницы регистрации',
            'registration_footer_id' => 'Подвал для страницы регистрации',
            'footer_mail'            => 'Почта в подвале',
            'footer_site'            => 'Сайт в подвале',
            'background_cabinet'     => 'ссылка на задник в кабинете',
            'video_intro'            => 'Ссылка на ютуб на приветственное видео',
            'logo_top_menu'          => 'logo_top_menu',
            'user_menu'              => 'Меню для кабинета внутри справа',
            'is_add_user_in_command_after_register'              => 'Добавлять ли пользователя в команду после регистрации?',
        ];
    }

}