<?php
/**
 * Created by PhpStorm.
 * User: god
 * Date: 28.02.2017
 * Time: 23:49
 */

namespace common\models\school;

use common\widgets\Select\Asset;
use cs\services\VarDumper;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;

/**
 * @property integer id
 * @property string content
 * @property integer page_id
 * @property integer type_id
 * @property integer sort_index
 */
class PageBlockContent extends ActiveRecord
{
    public static function tableName()
    {
        return 'school_page_block_content';
    }

    public function rules()
    {
        return [
            ['content', 'string'],
            ['page_id', 'integer'],
            ['type_id', 'integer'],
            ['sort_index', 'integer'],
        ];
    }

    public static function add($fields)
    {
        $i = new static($fields);
        $i->save();
        $i->id = self::getDb()->lastInsertID;

        return $i;
    }

    /**
     *
     * @return  \common\models\school\Page
     */
    public function getPage()
    {
        return Page::findOne($this->page_id);
    }

    public function getBaseBlock()
    {
        return PageBlock::findOne($this->type_id);
    }

    public function getContentData($path = null)
    {
        $data = Json::decode($this->content);
        if ($path) return ArrayHelper::getValue($data, $path);

        return $data;
    }

    /**
     * Выводит чистовик блока
     *
     * @return string
     * @throws \Throwable
     */
    public function render()
    {
        $baseBlock = $this->getBaseBlock();
        try {
            if ($baseBlock->asset) {
                /** @var \yii\web\AssetBundle $asset */
                $asset = $baseBlock->asset;
                $asset::register(\Yii::$app->view);
            }
        } catch (\Exception $e) {
            VarDumper::dump([$baseBlock, $this]);
        }

        $fileName = str_repeat('0', 5 - strlen($baseBlock->id)) . $baseBlock->id . '.' . 'php';
        $path = '@school/blocks/' . $fileName;
        if (!file_exists(\Yii::getAlias($path))) {
            $path = '@school/blocks-edit/' . $fileName;
        }
        $school = $this->getPage()->getSchool();

        $html = \Yii::$app->view->renderPhpFile(
            \Yii::getAlias($path),
            [
                'data'         => $this->content,
                'blockContent' => $this,
                'school'       => $school,
            ]
        );

        return $html;
    }

    /**
     * Выводит блок для редактирования
     *
     * @return string
     * @throws \Throwable
     */
    public function renderEdit()
    {
        $baseBlock = $this->getBaseBlock();
        try {
            if ($baseBlock->asset) {
                /** @var \yii\web\AssetBundle $asset */
                $asset = $baseBlock->asset;
                $asset::register(\Yii::$app->view);
            }
        } catch (\Exception $e) {
            VarDumper::dump([$baseBlock, $this]);
        }
        $school = $this->getPage()->getSchool();

        // формирую отображение блока
        $name = str_repeat('0', 5 - strlen($baseBlock->id)) . $baseBlock->id . '.' . 'php';
        $html = \Yii::$app->view->renderPhpFile(
            \Yii::getAlias('@school/blocks-edit/' . $name),
            [
                'data'         => $this->content,
                'blockContent' => $this,
                'school'       => $school,
            ]
        );

        return $html;
    }

    /**
     * Выводит блок для редактирования, его настройки
     *
     * @return string
     * @throws \Throwable
     */
    public function renderEditSettings($type_id = null, $school_id = null)
    {
        $baseBlock = $this->getBaseBlock();

        // формирую настройки блока
        $nameSettings = str_repeat('0', 5 - strlen($baseBlock->id)) . $baseBlock->id . '-settings' . '.' . 'php';
        $html = \Yii::$app->view->renderPhpFile(
            \Yii::getAlias('@school/blocks-edit/' . $nameSettings),
            ['data' => $this->content, 'type_id' => $type_id, 'school_id' => $school_id]
        );

        return $html;
    }
}