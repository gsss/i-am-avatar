<?php
/**
 * Created by PhpStorm.
 * User: god
 * Date: 28.02.2017
 * Time: 23:49
 */

namespace common\models\school;

use common\services\TildaApi;
use cs\services\VarDumper;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;

/**
 * @property integer id
 * @property integer school_id
 * @property integer created_at
 * @property string publickey
 * @property string secretkey
 */
class TildaPlugin extends ActiveRecord
{
    public static function tableName()
    {
        return 'school_tilda';
    }

    public function behaviors()
    {
        return [
            [
                'class' => '\yii\behaviors\TimestampBehavior',
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at'],
                ],
                // если вместо метки времени UNIX используется datetime:
                // 'value' => new Expression('NOW()'),
            ],
        ];
    }

    public static function add($fields)
    {
        $i = new static($fields);
        $i->save();
        $i->id = self::getDb()->lastInsertID;

        return $i;
    }

    /**
     * @return TildaApi
     */
    public function getProviderTilda()
    {
        return new TildaApi([
            'publickey' => $this->publickey,
            'secretkey' => $this->secretkey,
        ]);
    }

}