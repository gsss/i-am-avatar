<?php
/**
 * Created by PhpStorm.
 * User: god
 * Date: 28.02.2017
 * Time: 23:49
 */

namespace common\models\school;

use yii\db\ActiveRecord;

/**
 * @property integer id
 * @property integer category_id
 * @property string  name
 * @property string  image
 * @property string  asset
 * @property string  init
 * @property integer sort_index
 * @property integer is_prod        флаг 0 - в разработке, 1 - для всех можно пользоваться
 */
class PageBlock extends ActiveRecord
{
    public static function tableName()
    {
        return 'school_page_block';
    }
    public function rules()
    {
        return [
            ['name', 'string'],
            ['asset', 'string'],
            ['image', 'string'],
            ['init', 'string'],
            ['category_id', 'integer'],
            ['sort_index', 'integer'],
            ['is_prod', 'integer'],
        ];
    }

}