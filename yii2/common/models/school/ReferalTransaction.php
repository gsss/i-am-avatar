<?php
/**
 * Created by PhpStorm.
 * User: god
 * Date: 28.02.2017
 * Time: 23:49
 */

namespace common\models\school;

use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;

/**
 * @property integer id
 * @property integer school_id
 * @property integer request_id
 * @property integer currency_id    dbWallet.currency.id
 * @property integer level
 * @property integer from_uid
 * @property integer to_uid
 * @property integer to_wid
 * @property integer amount
 * @property integer transaction_id
 * @property integer created_at
 */
class ReferalTransaction extends ActiveRecord
{
    public static function tableName()
    {
        return 'school_referal_transaction';
    }

    public function behaviors()
    {
        return [
            [
                'class'      => '\yii\behaviors\TimestampBehavior',
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at'],
                ],
                // если вместо метки времени UNIX используется datetime:
                // 'value' => new Expression('NOW()'),
            ],
        ];
    }

    public static function add($fields)
    {
        $i = new static($fields);
        $i->save();
        $i->id = self::getDb()->lastInsertID;

        return $i;
    }
}