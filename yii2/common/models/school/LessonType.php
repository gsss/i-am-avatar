<?php
/**
 * Created by PhpStorm.
 * User: god
 * Date: 28.02.2017
 * Time: 23:49
 */

namespace common\models\school;

use yii\db\ActiveRecord;

/**
 * @property integer id
 * @property string  name
 * @property integer sort_index
 */
class LessonType extends ActiveRecord
{
    public static function tableName()
    {
        return 'school_kurs_lesson_type';
    }
}