<?php
/**
 * Created by PhpStorm.
 * User: god
 * Date: 28.02.2017
 * Time: 23:49
 */

namespace common\models\school;

use yii\db\ActiveRecord;

/**
 * @property integer id
 * @property string  video_id
 * @property integer lesson_id
 */
class LessonVebinar extends ActiveRecord
{
    public static function tableName()
    {
        return 'school_kurs_lesson_vebinar';
    }

    /**
     * @param array $fields
     *
     * @return self
     */
    public static function add($fields)
    {
        $item = new static($fields);
        $item->save();
        $item->id = self::getDb()->lastInsertID;

        return $item;
    }
}