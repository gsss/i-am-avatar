<?php
/**
 * Created by PhpStorm.
 * User: god
 * Date: 28.02.2017
 * Time: 23:49
 */

namespace common\models\school;

use yii\db\ActiveRecord;

/**
 * @property integer id
 * @property string  name
 * @property string  country
 * @property string  town
 * @property string  point
 * @property integer lesson_id
 */
class LessonPlace extends ActiveRecord
{
    public static function tableName()
    {
        return 'school_kurs_lesson_place';
    }

    public function rules()
    {
        return [
            ['name', 'string'],
            ['country', 'string'],
            ['town', 'string'],
            ['point', 'string'],
            ['lesson_id', 'integer'],
        ];
    }
}