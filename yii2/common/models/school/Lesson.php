<?php
/**
 * Created by PhpStorm.
 * User: god
 * Date: 28.02.2017
 * Time: 23:49
 */

namespace common\models\school;

use avatar\models\UserRegistration;
use cs\services\Security;
use yii\db\ActiveRecord;

/**
 * @property integer id
 * @property string  name
 * @property string  image
 * @property integer kurs_id
 * @property integer type_id
 * @property integer created_at
 */
class Lesson extends ActiveRecord
{
    public static function tableName()
    {
        return 'school_kurs_lesson';
    }

    /**
     * @return Kurs
     */
    public function getKurs()
    {
        return Kurs::findOne($this->kurs_id);
    }


    /**
     * @param array $fields
     *
     * @return self
     */
    public static function add($fields)
    {
        $item = new static($fields);
        $item->save();
        $item->id = self::getDb()->lastInsertID;

        return $item;
    }
}