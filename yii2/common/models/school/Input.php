<?php
/**
 * Created by PhpStorm.
 * User: god
 * Date: 28.02.2017
 * Time: 23:49
 */

namespace common\models\school;

use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;

/**
 * @property integer id
 * @property string comment
 * @property integer school_id
 * @property integer amount
 * @property integer created_at
 * @property integer emission_operation_id
 * @property integer emission2_operation_id
 */
class Input extends ActiveRecord
{
    public static function tableName()
    {
        return 'school_input';
    }

    public function behaviors()
    {
        return [
            [
                'class' => '\yii\behaviors\TimestampBehavior',
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at'],
                ],
                // если вместо метки времени UNIX используется datetime:
                // 'value' => new Expression('NOW()'),
            ],
        ];
    }

    public function rules()
    {
        return [
            ['school_id', 'integer'],
            ['created_at', 'integer'],
            ['emission_operation_id', 'integer'],
            ['emission2_operation_id', 'integer'],
            ['amount', 'integer'],
            ['comment', 'string'],
        ];
    }

    public static function add($fields)
    {
        $i = new static($fields);
        $i->save();
        $i->id = self::getDb()->lastInsertID;

        return $i;
    }
}