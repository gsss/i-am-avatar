<?php
/**
 * Created by PhpStorm.
 * User: god
 * Date: 28.02.2017
 * Time: 23:49
 */

namespace common\models\school;

use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\helpers\Json;

/**
 * @property integer id
 * @property integer plugin_id
 * @property integer school_id
 * @property string  content
 * @property string  name
 */
class PluginExportSettings extends ActiveRecord
{
    public static function tableName()
    {
        return 'school_out_plugin_settings';
    }

    public function rules()
    {
        return [
            ['name', 'string'],
            ['content', 'string'],
            ['plugin_id', 'integer'],
            ['school_id', 'integer'],
        ];
    }

    public function export($fields)
    {
        $content = $this->content;
        $data = Json::decode($content);
        $plugin = $this->getPlugin();
        $class = $plugin->class;
        /** @var \common\models\PluginInterface $object */
        $object = new $class($data);

        return $object->export($fields);
    }

    public function getPlugin()
    {
        return PluginExport::findOne($this->plugin_id);
    }
}