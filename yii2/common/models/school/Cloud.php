<?php
/**
 * Created by PhpStorm.
 * User: god
 * Date: 28.02.2017
 * Time: 23:49
 */

namespace common\models\school;

use common\models\UserAvatar;
use cs\services\VarDumper;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;

/**
 * @property integer id
 * @property integer school_id      Идентификатор школы
 * @property integer is_active
 * @property string  url            Полный путь к облаку
 * @property string  key            ключ
 */
class Cloud extends ActiveRecord
{
    public static function tableName()
    {
        return 'school_cloud';
    }

    public static function add($fields)
    {
        $i = new static($fields);
        $i->save();
        $i->id = self::getDb()->lastInsertID;

        return $i;
    }

    public function call($path, $params, $options = [])
    {
        /** @var \common\services\AvatarCloud $cloud */
        $cloud = \Yii::$app->AvatarCloud;
        $cloud->url = $this->url;
        $cloud->key = $this->key;

        if (!isset($params['signature'])) {
            $params['signature'] = \common\models\UserAvatar::hashPassword($this->key);
        }

        return $cloud->post($cloud->url, $path, $params, $options);
    }

    /**
     * @return \common\models\school\School
     */
    function getSchool()
    {
        return School::findOne($this->school_id);
    }
}