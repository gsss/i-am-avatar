<?php
/**
 * Created by PhpStorm.
 * User: god
 * Date: 28.02.2017
 * Time: 23:49
 */

namespace common\models\school;

use cs\services\VarDumper;
use yii\db\ActiveRecord;

/**
 * @property integer id
 * @property integer user_id
 * @property integer school_id
 * @property integer is_only_my             Настройка для члена команды, 0 - показываются все задачи. 1 - показываются только свои задачи, по умолчанию - 1
 * @property integer hour_price             Цена трудочаса в атомах
 * @property integer hour_price_currency_id Валюта для hour_price, dbWallet.currency.id
 */
class CommandLink extends ActiveRecord
{
    public static function tableName()
    {
        return 'school_command_link';
    }

    public function rules()
    {
        return [
            ['user_id', 'integer'],
            ['school_id', 'integer'],
            ['is_only_my', 'integer'],
            ['hour_price', 'integer'],
            ['hour_price_currency_id', 'integer'],
        ];
    }

    public static function add($fields)
    {
        $i = new static($fields);
        $i->save();
        $i->id = self::getDb()->lastInsertID;

        return $i;
    }

}