<?php
/**
 * Created by PhpStorm.
 * User: god
 * Date: 28.02.2017
 * Time: 23:49
 */

namespace common\models\school;

use cs\services\VarDumper;
use yii\db\ActiveRecord;

/**
 * @property integer id
 * @property integer field_id       Идентификатор поля
 * @property string  name           Наименование элемента списка
 */
class AnketaListItem extends ActiveRecord
{
    public static function tableName()
    {
        return 'school_potok_fields_list';
    }

    public static function add($fields)
    {
        $i = new static($fields);
        $i->save();
        $i->id = self::getDb()->lastInsertID;

        return $i;
    }

}