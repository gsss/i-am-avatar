<?php
/**
 * Created by PhpStorm.
 * User: god
 * Date: 28.02.2017
 * Time: 23:49
 */

namespace common\models\school;

use yii\db\ActiveRecord;

/**
 * @property integer id
 * @property integer lesson_id
 * @property string content
 */
class LessonHomeWork extends ActiveRecord
{
    public static function tableName()
    {
        return 'school_urok_dz_request';
    }

}