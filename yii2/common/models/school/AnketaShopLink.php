<?php
/**
 * Created by PhpStorm.
 * User: god
 * Date: 28.02.2017
 * Time: 23:49
 */

namespace common\models\school;

use cs\services\VarDumper;
use yii\db\ActiveRecord;

/**
 * @property integer id
 * @property integer anketa_id     идентифиатор анкеты school_shop_anketa.id
 * @property integer product_id    идентификатор продукта gs_unions_shop_product.id
 */
class AnketaShopLink extends ActiveRecord
{
    public static function tableName()
    {
        return 'school_shop_anketa_link';
    }

    public static function add($fields)
    {
        $i = new static($fields);
        $i->save();
        $i->id = self::getDb()->lastInsertID;

        return $i;
    }

}