<?php
/**
 * Created by PhpStorm.
 * User: god
 * Date: 28.02.2017
 * Time: 23:49
 */

namespace common\models\school;

use yii\db\ActiveRecord;

/**
 * @property integer id
 * @property integer sort_index
 * @property string  name
 */
class PageBlockCategory extends ActiveRecord
{
    public static function tableName()
    {
        return 'school_page_block_category';
    }

    public function rules()
    {
        return [
            ['name', 'string'],
        ];
    }
}