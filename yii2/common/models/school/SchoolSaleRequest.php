<?php
/**
 * Created by PhpStorm.
 * User: god
 * Date: 28.02.2017
 * Time: 23:49
 */

namespace common\models\school;

use common\models\BillingMain;
use common\models\UserAvatar;
use cs\services\VarDumper;
use yii\db\ActiveRecord;
use yii\helpers\StringHelper;
use yii\web\HttpException;

/**
 * @property integer id
 * @property integer sale_id
 * @property integer lid_id
 * @property integer ps_config_id
 * @property integer created_at
 * @property integer billing_id
 * @property integer is_paid
 */
class SchoolSaleRequest extends ActiveRecord
{
    public static function tableName()
    {
        return 'school_kurs_sale_request';
    }

    public function rules()
    {
        return [
            ['id', 'integer', 'min' => 0],
            ['sale_id', 'integer', 'min' => 0],
            ['lid_id', 'integer', 'min' => 0],
            ['ps_config_id', 'integer', 'min' => 0],
            ['created_at', 'integer', 'min' => 0],
            ['billing_id', 'integer', 'min' => 0],
        ];
    }

    public function behaviors()
    {
        return [
            [
                'class' => '\yii\behaviors\TimestampBehavior',
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at'],
                ],
                // если вместо метки времени UNIX используется datetime:
                // 'value' => new Expression('NOW()'),
            ],
        ];
    }

    public static function add($fields)
    {
        $i = new static($fields);
        $i->save();
        $i->id = self::getDb()->lastInsertID;

        return $i;
    }

    /**
     * @return \common\models\school\SchoolSale
     */
    public function getSale()
    {
        return SchoolSale::findOne($this->sale_id);
    }

    /**
     * @param \common\models\piramida\WalletSourceInterface $source
     *
     */
    public function successShop($source)
    {
        $this->is_paid = 1;
        $this->save();

        $billing = BillingMain::findOne($this->billing_id);
        $billing->successShop();
    }


    /**
     * Клиент подтвердил отправку денег
     */
    public function successClient()
    {
        $this->is_paid_client = 1;
        $this->save();

        $billing = BillingMain::findOne($this->billing_id);
        $billing->successClient();
    }

}