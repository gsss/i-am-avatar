<?php

namespace common\models\school;

use common\models\UserAvatar;
use cs\Application;
use cs\services\VarDumper;
use yii\db\ActiveRecord;
use yii\helpers\StringHelper;
use yii\web\ForbiddenHttpException;
use yii\web\HttpException;

/**
 * @property integer id
 * @property string  name
 * @property string  image
 * @property string  subscribe_image
 * @property string  subscribe_from_name
 * @property string  subscribe_from_email
 * @property string  description
 * @property string  dns
 * @property string  favicon
 * @property string  about
 * @property integer created_at
 * @property integer user_id
 * @property integer currency_id            Идентификатор валюты школы dbWallet.currency.id
 * @property integer wallet_id
 * @property integer paid_till
 * @property integer header_id
 * @property integer footer_id
 * @property integer is_super_company      Флаг. Это суперкомпания?
 * @property integer blog_header_id
 * @property integer blog_footer_id
 * @property integer news_header_id
 * @property integer news_footer_id
 * @property integer pay_wallet_id          идентификатор кошелька. Счетчик на 6%
 * @property integer is_mlm                 Флаг. есть партнерка?
 * @property integer is_hide                Флаг. Скрыта школа? 1 - Да, скрыта в списке "Мои школы" и если стоит флаг что она в каталоге то там ее тоже не показывать. 0 - нет, показывается в "Мои школы". По умолчанию - 0
 * @property integer referal_wallet_id      Кошелек реферальной программы
 * @property integer files_max_size         колво мегабайт которое можно загрузить в облако
 * @property integer is_catalog             Флаг. Школа в каталоге? 1 - Да, школа видна в каталоге. 0 - школа скрыта, нет в каталоге. По умолчанию - 0
 * @property integer fund_rub               Идентификатор счета i_am_avatar_prod_wallet.wallet.id. Рублевый фонд.
 * @property integer fund_rub2              Идентификатор счета i_am_avatar_prod_wallet.wallet.id. Рублевый фонд.
 */
class School extends ActiveRecord
{
    public static $reservedUrl = [
        'url' => [

        ],
        'controller' => [
            'auth',
            'blog',
            'news',
            'cabinet',
        ],
    ];

    public static function tableName()
    {
        return 'school';
    }

    public function rules()
    {
        return [
            ['name', 'string', 'max' => 100],
            ['description', 'string', 'max' => 1000],
            ['image', 'string', 'max' => 100],
            ['dns', 'string', 'max' => 100],
            ['about', 'string', 'max' => 65535],

            ['subscribe_image', 'string', 'max' => 255],
            ['subscribe_from_email', 'string', 'max' => 100],
            ['subscribe_from_name', 'string', 'max' => 100],

            ['created_at', 'integer', 'min' => 0],
            ['user_id', 'integer', 'min' => 0],
            ['currency_id', 'integer', 'min' => 0],
            ['wallet_id', 'integer', 'min' => 0],
            ['paid_till', 'integer', 'min' => 0],

            ['header_id', 'integer', 'min' => 0],
            ['footer_id', 'integer', 'min' => 0],

            ['blog_header_id', 'integer', 'min' => 0],
            ['blog_footer_id', 'integer', 'min' => 0],

            ['news_header_id', 'integer', 'min' => 0],
            ['news_footer_id', 'integer', 'min' => 0],

            ['files_max_size', 'integer', 'min' => 0],

            ['is_catalog', 'integer', 'min' => 0, 'max' => 1],

            ['referal_wallet_id', 'integer', 'min' => 0],

            ['fund_rub', 'integer', 'min' => 0],
            ['fund_rub2', 'integer', 'min' => 0],
        ];
    }

    /**
     * @param string $url
     *
     * @return Page
     */
    public function getPage($url)
    {
        return Page::findOne(['url' => $url, 'school_id' => $this->id]);
    }


    /**
     * @return Cloud
     */
    public function getCloud()
    {
        return Cloud::findOne(['is_active' => 1, 'school_id' => $this->id]);
    }

    /**
     * @return UserAvatar
     * @throws
     */
    public function getUser()
    {
        return UserAvatar::findOne([$this->user_id]);
    }

    /**
     * Проверяет URL на валидность, перед соданием страницы
     * При добавлении нужно проверить чтобы он не совпадал с предыдущими чтобы он был уникальным и чтобы не совпадал с зарезервированными.
     *
     * @param string $url относительная ссылка на страницу, начинается со /
     *
     * @return array
     * [
     *              'code' => int - обязательное
     * 0 - такой страницы нет, можно создавать
     * 1 - такая страница уже есть
     * 2 - эта страница зарезервирована
     * 3 - этот контроллер зарезервирован
     *              'message' - string - не обязательное, указывается если code!=0
     * ]
     */
    public function validateUrl($url)
    {
        if (Page::find()->where(['school_id' => $this->id, 'url' => $url])->exists()) {
            return ['code' => 1, 'message' => 'такая страница уже есть'];
        }
        if (in_array($url, self::$reservedUrl['url'])) {
            return ['code' => 2, 'message' => 'эта страница зарезервирована'];
        }
        $arr = explode('/', $url);
        if (count($arr) == 1) {
            return ['code' => 4, 'message' => 'Нужен слеш'];
        }
        if (in_array($arr[1], self::$reservedUrl['controller'])) {
            return ['code' => 3, 'message' => 'этот контроллер зарезервирован, запрещены url начинающиеся на /'.$arr[1]];
        }

        return ['code' => 0];
    }
    /**
     * @param int $school_id
     *
     * @throws ForbiddenHttpException
     */
    public static function isAccessAdmin($school_id)
    {
        $result = AdminLink::find()->where([
            'school_id' => $school_id,
            'user_id'   => \Yii::$app->user->id,
        ])->exists();

        if (!$result) throw new ForbiddenHttpException('Вам запрещен доступ к чужой школе');
    }

    public function getUrl($url = null)
    {
        if (is_array($url)) {
            $u = '';
            $params = [];
            foreach ($url as $k => $v) {
                if (Application::isInteger($k)) {
                    $u = '/' . $v;
                } else {
                    $params[] =  $k . '=' . urlencode($v);
                }
            }
            if (count($params) > 0) {
                $url = $u . '?' . join('&', $params);
            } else {
                $url = $u;
            }
        }
        if (Application::isEmpty($this->dns)) return $url;

        return $this->dns . $url;
    }

    /**
     * Выдает школу смотря на DNS
     *
     * @return static
     * @throws
     */
    private static function _get()
    {
        $HostName = \Yii::$app->request->getHostName();
        $Port = \Yii::$app->request->getPort();
        $PortStr = (in_array($Port, [80, 443])) ? '' : ':'.$Port;
        $url1 = 'http://' . $HostName . $PortStr;
        $url2 = 'https://' . $HostName . $PortStr;

        $school = School::findOne(['dns' => [$url1, $url2]]);
        if (is_null($school)) {
            throw  new HttpException(404, 'Сообщество не найдено');
        }

        return $school;
    }

    public function save($runValidation = true, $attributeNames = null)
    {
        $key = '__school';
        if (\Yii::$app instanceof \yii\web\Application) \Yii::$app->session->remove($key);

        return parent::save($runValidation, $attributeNames); // TODO: Change the autogenerated stub
    }

    /**
     * Выдает школу смотря на DNS
     *
     * @return static
     * @throws
     */
    public static function get()
    {
        $key = '__school';
        $v = \Yii::$app->session->get($key);
        if (is_null($v)) {
            $v = self::_get();
            if (!is_null($v)) {
                \Yii::$app->session->set($key, $v->attributes);
            }
        }

        return self::_get();
    }

    /**
     * Выдает модуль если есть
     *
     * @return static
     * @throws
     */
    public static function getModuleClassName()
    {
        // определяю школу
        $HostName = strtolower(\Yii::$app->request->getHostName());
        $schoolList = require(\Yii::getAlias('@school/config/school.php'));

        foreach ($schoolList as $k => $hostList) {
            if (in_array($HostName, $hostList)) return '\\school\\modules\\' . $k . '\\Module';
        }

        return null;
    }

    /**
     * Выдает ответ: Нахожусь ли я на Аватаре(админка) или на школе?
     * Не привязана к определению домена, вычисляет на основе какое приложение запущено
     */
    public static function isRoot()
    {
        return \Yii::$app->id == 'avatar-root';
    }

    /**
     * Выдает ответ: может ли пользователь $id управлять школой
     *
     * @param int $id идентификатор пользователя, если не указывается то подставляется пользователь в системе
     *
     * @return bool
     */
    public function isAccess($id = null)
    {
        if (is_null($id)) {
            $id = \Yii::$app->user->id;
        }
        if ($this->user_id == $id) return true;

        return AdminLink::find()->where(['school_id' => $this->id, 'user_id' => $id])->exists();
    }

    /**
     * @return \common\models\school\SchoolDesign | null
     */
    public function getSchoolDesign()
    {
        return SchoolDesign::findOne(['school_id' => $this->id]);
    }
}