<?php
/**
 * Created by PhpStorm.
 * User: god
 * Date: 28.02.2017
 * Time: 23:49
 */

namespace common\models\school;

use yii\db\ActiveRecord;

/**
 * @property integer id
 * @property string  video_id
 * @property integer lesson_id
 */
class LessonVideo extends ActiveRecord
{
    public static function tableName()
    {
        return 'school_kurs_lesson_video';
    }

    public function rules()
    {
        return [
            ['video_id', 'string'],
            ['lesson_id', 'integer'],
        ];
    }
}