<?php
/**
 * Created by PhpStorm.
 * User: god
 * Date: 28.02.2017
 * Time: 23:49
 */

namespace common\models\school;

use cs\services\VarDumper;
use yii\db\ActiveRecord;

/**
 * @property integer id
 * @property integer potok_id       Идентификатор потока school_potok.id
 * @property integer lid_id         Идентификатор ЛИДа school_potok_user_root_link.id
 * @property integer created_at     Момент создания записи
 * @property string  data           Данные JSON
 */
class AnketaPotokData extends ActiveRecord
{
    public static function tableName()
    {
        return 'school_potok_fields_list_data_request';
    }

    public function behaviors()
    {
        return [
            [
                'class' => '\yii\behaviors\TimestampBehavior',
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at'],
                ],
                // если вместо метки времени UNIX используется datetime:
                // 'value' => new Expression('NOW()'),
            ],
        ];
    }

    public static function add($fields)
    {
        $i = new static($fields);
        $i->save();
        $i->id = self::getDb()->lastInsertID;

        return $i;
    }

}