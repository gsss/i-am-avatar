<?php
/**
 * Created by PhpStorm.
 * User: god
 * Date: 28.02.2017
 * Time: 23:49
 */

namespace common\models\school;

use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;

/**
 * @property integer id
 * @property integer user_id
 * @property integer potok_id
 * @property string  name
 */
class PluginInTilda extends ActiveRecord
{
    public static function tableName()
    {
        return 'school_potok_in_tilda';
    }

    public function rules()
    {
        return [
            ['name', 'string'],
            ['user_id', 'integer'],
            ['potok_id', 'integer'],
        ];
    }

}