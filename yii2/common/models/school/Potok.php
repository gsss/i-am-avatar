<?php
/**
 * Created by PhpStorm.
 * User: god
 * Date: 28.02.2017
 * Time: 23:49
 */

namespace common\models\school;

use common\models\UserAvatar;
use cs\services\VarDumper;
use yii\db\ActiveRecord;

/**
 * @property integer id
 * @property integer kurs_id
 * @property integer page_id
 * @property string  name
 * @property string  date_start
 * @property string  date_end
 */
class Potok extends ActiveRecord
{
    public static function tableName()
    {
        return 'school_potok';
    }

    public function rules()
    {
        return [
            ['kurs_id', 'integer'],
            ['page_id', 'integer'],
            ['name', 'string'],
            ['date_start', 'string'],
            ['date_end', 'string'],
        ];
    }

    /**
     * @return Kurs
     */
    public function getKurs()
    {
        return Kurs::findOne($this->kurs_id);
    }

    /**
     * @return School
     */
    public function getSchool()
    {
        return School::findOne($this->getKurs()->school_id);
    }

    /**
     * @return Page
     */
    public function getPage()
    {
        if ($this->page_id) {
            return Page::findOne($this->page_id);
        }

        return null;
    }


    public static function add($fields)
    {
        $i = new static($fields);
        $i->save();
        $i->id = self::getDb()->lastInsertID;

        return $i;
    }
}