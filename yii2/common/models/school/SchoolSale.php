<?php
/**
 * Created by PhpStorm.
 * User: god
 * Date: 28.02.2017
 * Time: 23:49
 */

namespace common\models\school;

use common\models\UserAvatar;
use cs\services\VarDumper;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;
use yii\helpers\StringHelper;
use yii\web\HttpException;

/**
 * @property integer    id
 * @property integer    school_id
 * @property integer    potok_id
 * @property integer    price
 * @property integer    currency_id
 * @property integer    created_at
 * @property string     name
 */
class SchoolSale extends ActiveRecord
{
    public static function tableName()
    {
        return 'school_kurs_sale';
    }

    public function behaviors()
    {
        return [
            [
                'class'      => '\yii\behaviors\TimestampBehavior',
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at'],
                ],
                // если вместо метки времени UNIX используется datetime:
                // 'value' => new Expression('NOW()'),
            ],
        ];
    }

    public function rules()
    {
        return [
            ['id', 'integer', 'min' => 0],
            ['school_id', 'integer', 'min' => 0],
            ['potok_id', 'integer', 'min' => 0],
            ['price', 'integer', 'min' => 0],
            ['currency_id', 'integer', 'min' => 0],
            ['created_at', 'integer', 'min' => 0],
            ['name', 'string', 'max' => 255],
        ];
    }

    public static function add($fields)
    {
        $i = new static($fields);
        $i->save();
        $i->id = self::getDb()->lastInsertID;

        return $i;
    }

    /**
     * @return \common\models\school\School
     */
    public function getSchool()
    {
        return School::findone($this->school_id);
    }

    public function attributeLabels()
    {
        $parent = parent::attributeLabels();
        $t = [
            'price'       => 'Цена',
            'currency_id' => 'Валюта',
            'created_at'  => 'Создано',
            'name'        => 'Наименование',
        ];
        return ArrayHelper::merge($parent, $t);
    }

    public function attributeHints()
    {
        $parent = parent::attributeHints();
        $t = [
            'price'       => 'В атомах',
        ];
        return ArrayHelper::merge($parent, $t);
    }
}