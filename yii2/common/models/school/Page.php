<?php
/**
 * Created by PhpStorm.
 * User: god
 * Date: 28.02.2017
 * Time: 23:49
 */

namespace common\models\school;

use cs\services\VarDumper;
use yii\db\ActiveRecord;

/**
 * @property integer    id
 * @property string     name
 * @property string     facebook_image
 * @property string     facebook_title
 * @property string     facebook_description
 * @property string     url
 * @property string     body
 * @property string     title
 * @property string     description
 * @property string     keywords
 * @property integer    school_id
 * @property integer    header_id
 * @property integer    footer_id
 * @property integer    tilda_page_id
 * @property string     favicon
 */
class Page extends ActiveRecord
{
    public static $rezervUrlList = [
        'auth/login',
        'auth/registration',
    ];

    public static function tableName()
    {
        return 'school_page';
    }

    public static function add($fields)
    {
        $i = new static($fields);
        $i->save();
        $i->id = self::getDb()->lastInsertID;

        return $i;
    }


    public function rules()
    {
        return [
            ['name', 'string'],
            ['title', 'string'],
            ['description', 'string'],
            ['keywords', 'string'],
            ['body', 'string'],
            ['url', 'string'],
            ['school_id', 'integer'],
            ['header_id', 'integer'],
            ['footer_id', 'integer'],
            ['tilda_page_id', 'integer'],
            ['favicon', 'string'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'name'          => 'Название страницы',
            'url'           => 'Путь к странице',
            'tilda_page_id' => 'Идентификатор страницы в тильде',
        ];
    }

    public function attributeHints()
    {
        return [
            'name' => 'Название внутреннее',
            'url'  => 'Начинается со слеша',
        ];
    }

    /**
     * @return \common\models\school\PageBlockContent[]
     */
    public function getBlocks()
    {
        return PageBlockContent::find()
            ->where(['page_id' => $this->id])
            ->orderBy(['sort_index' => SORT_ASC])
            ->all();
    }

    /**
     * @return \common\models\school\School
     */
    public function getSchool()
    {
        return School::findOne($this->school_id);
    }


    public function getUrl()
    {
        return $this->getSchool()->getUrl($this->url);
    }

    /**
     * Вызывается когда пользователь открыл страницу для просмотра, функция записывает запись о просмотре в статистику
     *
     * @return \common\models\school\PageStat
     */
    public function view()
    {
        return PageStat::add([
            'page_id' => $this->id,
            'is_bot'  => self::isBot(\Yii::$app->request->userAgent) ? 1 : 0,
        ]);
    }

    public static function isBot($userAgent)
    {
        $botArray = [
            'YandexBot',
            'TelegramBot',
            'Go-http-client',
            'bingbot',
            'SemrushBot',
            'Pinterestbot',
            'Googlebot',
            'DotBot',
            'AhrefsBot',
            'Twitterbot',
            'seekport.com',
            'facebookexternalhit',
            'Google-Youtube-Links',
            'WhatsApp',
            'vkShare',
            'SkypeUriPreview',
            'masscan',
            'mj12bot',
            'CloudFlare-AlwaysOnline',
            'openstat.ru',
            'svetabot.online',
            'SearchAtlas.com',
        ];
        $agent = $userAgent;
        foreach ($botArray as $botName) {
            if (strpos($agent, $botName) !== false) {
                return true;
            }
        }

        return false;
    }
}