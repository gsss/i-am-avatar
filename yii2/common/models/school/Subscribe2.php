<?php
/**
 * Created by PhpStorm.
 * User: god
 * Date: 28.02.2017
 * Time: 23:49
 */

namespace common\models\school;

use yii\db\ActiveRecord;

/**
 * @property integer id
 * @property integer school_id
 * @property string  subject
 * @property string  content
 * @property integer created_at
 */
class Subscribe2 extends ActiveRecord
{
    public static function tableName()
    {
        return 'school_subscribe';
    }
}