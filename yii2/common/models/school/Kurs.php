<?php
/**
 * Created by PhpStorm.
 * User: god
 * Date: 28.02.2017
 * Time: 23:49
 */

namespace common\models\school;

use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;

/**
 * @property integer id
 * @property string  name
 * @property string  social_net_image
 * @property string  social_net_title
 * @property string  social_net_description
 * @property string  for_who
 * @property string  daet
 * @property integer status         1 - только подтверждение почты, 2 - подтверждение и регистрация
 * @property integer school_id
 * @property integer created_at
 * @property integer is_show
 */
class Kurs extends ActiveRecord
{
    const STATUS_CONFIRM = 1;
    const STATUS_CONFIRM_AND_REGISTRATION = 2;

    public static function tableName()
    {
        return 'school_kurs';
    }

    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
                ],
                // если вместо метки времени UNIX используется datetime:
                // 'value' => new Expression('NOW()'),
            ],
        ];
    }

    public function rules()
    {
        return [
            ['name', 'required'],
            ['name', 'string'],

            ['status', 'integer'],

            ['school_id', 'integer'],

            ['created_at', 'integer'],
        ];
    }

    /**
     * @return School
     */
    public function getSchool()
    {
        return School::findOne($this->school_id);
    }


    public static function add($fields)
    {
        $i = new static($fields);
        $i->save();
        $i->id = self::getDb()->lastInsertID;

        return $i;
    }
}