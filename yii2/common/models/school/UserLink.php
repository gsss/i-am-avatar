<?php
/**
 * Created by PhpStorm.
 * User: god
 * Date: 28.02.2017
 * Time: 23:49
 */

namespace common\models\school;

use cs\services\VarDumper;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;

/**
 * @property integer id
 * @property integer user_root_id
 * @property integer school_id
 */
class UserLink extends ActiveRecord
{
    public static function tableName()
    {
        return 'user_school_link';
    }

    public function rules()
    {
        return [
            ['user_root_id', 'integer'],
            ['school_id', 'integer'],
        ];
    }

    /**
     * @return School
     */
    public function getSchool()
    {
        return School::findOne($this->school_id);
    }

    public static function add($fields)
    {
        if (isset($fields['user_root_id']) && isset($fields['school_id'])) {
            $i = self::findOne([
                'user_root_id'  => $fields['user_root_id'],
                'school_id'     => $fields['school_id'],
            ]);
            if ($i) {
                $rows = [];
                $c = 1;
                foreach (debug_backtrace(2) as $item) {
                    $rows[] = '#' . $c . ' ' . ArrayHelper::getValue($item, 'file', '') . ':' . ArrayHelper::getValue($item, 'line', '') . ' ' . ArrayHelper::getValue($item, 'class', '') . ArrayHelper::getValue($item, 'type', '') . ArrayHelper::getValue($item, 'function', '') . "\n";
                    $c++;
                }
                \Yii::warning(\yii\helpers\VarDumper::dumpAsString($rows), 'avatar\common\models\school\UserLink::add');

                return $i;
            }
        }
        $i = new static($fields);
        $i->save();
        $i->id = self::getDb()->lastInsertID;

        return $i;
    }
}