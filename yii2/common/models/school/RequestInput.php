<?php
/**
 * Created by PhpStorm.
 * User: god
 * Date: 28.02.2017
 * Time: 23:49
 */

namespace common\models\school;

use common\models\avatar\Currency;
use common\models\avatar\UserBill;
use common\models\BillingMain;
use common\models\piramida\Wallet;
use common\models\UserAvatar;
use cs\services\VarDumper;
use yii\db\ActiveRecord;
use yii\helpers\StringHelper;
use yii\web\HttpException;

/**
 * @property integer id
 * @property integer user_id
 * @property integer amount
 * @property integer currency_id db.currency.id
 * @property integer billing_id
 * @property integer created_at
 * @property integer is_paid
 * @property integer is_paid_client
 */
class RequestInput extends ActiveRecord
{
    public static function tableName()
    {
        return 'request_input';
    }



    public function behaviors()
    {
        return [
            [
                'class' => '\yii\behaviors\TimestampBehavior',
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at'],
                ],
                // если вместо метки времени UNIX используется datetime:
                // 'value' => new Expression('NOW()'),
            ],
        ];
    }

    public static function add($fields)
    {
        $i = new static($fields);
        $i->save();
        $i->id = self::getDb()->lastInsertID;

        return $i;
    }


    /**
     * @param \common\models\piramida\WalletSourceInterface $source
     */
    public function successShop($source = null)
    {
        if ($this->is_paid_client == 0) $this->successClient();

        \Yii::info(\yii\helpers\VarDumper::dumpAsString($this->is_paid == 0), 'avatar\common\models\school\RequestInput::successShop1');
        if ($this->is_paid == 0) {
            $this->is_paid = 1;
            $this->save();

            \Yii::info('test', 'avatar\common\models\school\RequestInput::successShop2');

            $user = UserAvatar::findOne($this->user_id);
            $bill = UserBill::getBillByCurrencyAvatarProcessing(Currency::ELXGOLD, $this->user_id);
            $w = Wallet::findOne($bill->address);
            $wELXGOLDAll = Wallet::findOne(11);
            $t = $wELXGOLDAll->move($w, $this->amount, 'Пополнение в связи с заявкой #' . $this->id);

            $billing = BillingMain::findOne($this->billing_id);
            $billing->successShop();
        }
    }


    /**
     * Клиент подтвердил отправку денег
     */
    public function successClient()
    {
        if ($this->is_paid_client == 0) {
            $this->is_paid_client = 1;
            $this->save();


            $billing = BillingMain::findOne($this->billing_id);

            /** @var \aki\telegram\Telegram $telegram */
            $telegram = \Yii::$app->telegram;
            $res = $telegram->sendMessage(['chat_id' => 122605414, 'text' => 'По заявке #'.$this->id.' пришло подтверждение что было пополнение по платежной системе Ethereum']);

            $billing->successClient();
        }
    }

}