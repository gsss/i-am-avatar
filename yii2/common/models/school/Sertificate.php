<?php
/**
 * Created by PhpStorm.
 * User: god
 * Date: 28.02.2017
 * Time: 23:49
 */

namespace common\models\school;

use yii\db\ActiveRecord;

/**
 * @property integer id
 * @property string  name
 * @property string  description
 * @property string  image
 * @property integer school_id
 * @property integer user_id
 */
class Sertificate extends ActiveRecord
{
    public static function tableName()
    {
        return 'school_user_sertificate';
    }

    public function rules()
    {
        return [
            ['name', 'string'],
            ['description', 'string'],
            ['image', 'string'],
            ['school_id', 'integer'],
            ['user_id', 'integer'],
        ];
    }

}