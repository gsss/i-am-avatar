<?php
/**
 * Created by PhpStorm.
 * User: god
 * Date: 28.02.2017
 * Time: 23:49
 */

namespace common\models\school;

use yii\db\ActiveRecord;

/**
 * @property integer id
 * @property integer user_id
 * @property integer potok_id
 * @property integer avatar_id
 */
class PotokUserLink extends ActiveRecord
{
    public static function tableName()
    {
        return 'school_potok_user_link';
    }

    public static function add($fields)
    {
        $i = new static($fields);
        $i->save();
        $i->id = self::getDb()->lastInsertID;

        return $i;
    }
}