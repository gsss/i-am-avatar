<?php
namespace common\models\school;

use Imagine\Image\Box;
use yii\base\Exception;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;
use yii\helpers\FileHelper;
use yii\helpers\Url;
use cs\services\UploadFolderDispatcher;
use yii\imagine\Image;
use Imagine\Image\ManipulatorInterface;
use yii\web\IdentityInterface;
use yii\behaviors\TimestampBehavior;


/**
 * @property integer   id
 * @property integer   user_root_id
 * @property integer   is_avatar
 * @property integer   potok_id
 * @property integer   status
 * @property integer   created_at
 * @property integer   is_unsubscribed
 * @property integer   is_unsubscribed_telegram
 */
class PotokUser3Link extends ActiveRecord
{
    const IS_AVATAR_GUEST = 0;
    const IS_AVATAR_USER = 1;

    public static function tableName()
    {
        return 'school_potok_user_root_link';
    }

    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at'],
                ],
                // если вместо метки времени UNIX используется datetime:
                // 'value' => new Expression('NOW()'),
            ],
        ];
    }

    public static function add($fields)
    {
        $i = new static($fields);
        $i->save();
        $i->id = self::getDb()->lastInsertID;

        return $i;
    }
}
