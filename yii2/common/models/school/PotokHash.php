<?php
/**
 * Created by PhpStorm.
 * User: god
 * Date: 28.02.2017
 * Time: 23:49
 */

namespace common\models\school;

use common\models\UserAvatar;
use cs\services\VarDumper;
use yii\db\ActiveRecord;

/**
 * @property integer id
 * @property integer potok_id
 * @property string  hash
 */
class PotokHash extends ActiveRecord
{
    public static function tableName()
    {
        return 'school_potok_hash';
    }

    public static function add($fields)
    {
        $i = new static($fields);
        $i->save();
        $i->id = self::getDb()->lastInsertID;

        return $i;
    }
}