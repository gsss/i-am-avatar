<?php
/**
 * Created by PhpStorm.
 * User: god
 * Date: 28.02.2017
 * Time: 23:49
 */

namespace common\models\school;

use common\models\UserAvatar;
use cs\services\VarDumper;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\StringHelper;
use yii\web\HttpException;

/**
 * @property integer    id
 * @property integer    school_id
 * @property string     auth_backend
 * @property string     guest_frontend
 * @property string     auth_frontend
 */
class SchoolDesignMain extends ActiveRecord
{
    public static function tableName()
    {
        return 'school_design_main';
    }

    public function rules()
    {
        return [
            [array_keys($this->attributes), 'default'],
        ];
    }

    /**
     * @return \common\models\school\School
     */
    public function getSchool()
    {
        return School::findone($this->school_id);
    }

    public function attributeLabels()
    {
        return [
            'auth_frontend'          => 'Меню для лицевого сайта (для авторизованного пользователя)',
            'guest_frontend'         => 'Меню для лицевого сайта (для гостя)',
            'auth_backend'           => 'Меню для кабинета',
        ];
    }


    public function getErrors102($attribute = null)
    {
        $fields = [];
        foreach ($this->attributes as $k => $v) {
            $fields[$k] = Html::getInputId($this, $k);
        }
        $data =    [
            'errors' => $this->errors,
            'fields' => $fields,
        ];

        return $data;
    }
}