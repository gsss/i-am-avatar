<?php
/**
 * Created by PhpStorm.
 * User: god
 * Date: 28.02.2017
 * Time: 23:49
 */

namespace common\models\school;

use yii\db\ActiveRecord;

/**
 * @property integer id
 * @property integer page_id
 * @property integer lesson_id
 */
class LessonPage extends ActiveRecord
{
    public static function tableName()
    {
        return 'school_kurs_lesson_page';
    }

    public function rules()
    {
        return [
            ['page_id', 'integer'],
            ['lesson_id', 'integer'],
        ];
    }
}