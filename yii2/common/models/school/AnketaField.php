<?php
/**
 * Created by PhpStorm.
 * User: god
 * Date: 28.02.2017
 * Time: 23:49
 */

namespace common\models\school;

use cs\services\VarDumper;
use yii\db\ActiveRecord;

/**
 * @property integer id
 * @property integer created_at     Момент создания
 * @property integer potok_id       Идентификатор потока к которому присоединяются поля school_potok.id
 * @property integer setting_id     Название настройки к которой присоединяются поля school_fields_settings.id
 * @property integer sort_index     Индекс сортировки
 * @property integer type_id        Тип поля
 * @property integer is_required    Флаг, обязательное поле? 0 - нет, 1 - да, по умолчанию - 0
 * @property string  name           Название поля
 * @property string  description    Описание поля
 */
class AnketaField extends ActiveRecord
{
    public static function tableName()
    {
        return 'school_potok_fields';
    }

    public function behaviors()
    {
        return [
            [
                'class' => '\yii\behaviors\TimestampBehavior',
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at'],
                ],
                // если вместо метки времени UNIX используется datetime:
                // 'value' => new Expression('NOW()'),
            ],
        ];
    }

    public static function add($fields)
    {
        $i = new static($fields);
        $i->save();
        $i->id = self::getDb()->lastInsertID;

        return $i;
    }

}