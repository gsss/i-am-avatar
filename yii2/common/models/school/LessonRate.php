<?php
/**
 * Created by PhpStorm.
 * User: god
 * Date: 28.02.2017
 * Time: 23:49
 */

namespace common\models\school;

use yii\db\ActiveRecord;

/**
 * @property integer id
 * @property integer lesson_state_id
 * @property integer user_id
 * @property integer ocenka
 * @property integer created_at
 */
class LessonRate extends ActiveRecord
{
    public static function tableName()
    {
        return 'school_urok_dz_rate';
    }

}