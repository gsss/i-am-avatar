<?php
/**
 * Created by PhpStorm.
 * User: god
 * Date: 28.02.2017
 * Time: 23:49
 */

namespace common\models\school;

use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;

/**
 * @property integer id
 * @property integer input_id
 * @property integer transaction_id
 * @property integer balance_elxsky
 * @property integer balance_rub
 */
class InputTransaction extends ActiveRecord
{
    public static function tableName()
    {
        return 'school_input_transactions';
    }

    public function rules()
    {
        return [
            ['input_id', 'integer'],
            ['transaction_id', 'integer'],
            ['balance_elxsky', 'integer'],
            ['balance_rub', 'integer'],
        ];
    }

    public static function add($fields)
    {
        $i = new static($fields);
        $i->save();
        $i->id = self::getDb()->lastInsertID;

        return $i;
    }
}