<?php
/**
 * Created by PhpStorm.
 * User: god
 * Date: 28.02.2017
 * Time: 23:49
 */

namespace common\models\school;

use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;

/**
 * @property integer id
 * @property integer school_id
 * @property integer user_id
 * @property integer currency_id    db.currency.id
 * @property string  to             Адрес кошелька для вывода
 * @property integer amount         Кол-во атомов для вывода
 * @property integer status         Статус заявки, 0 - создана, 1 - выведено успешно, 2 - отвергнута
 * @property integer transaction_id
 * @property integer created_at
 */
class ReferalRequestOut extends ActiveRecord
{
    public static function tableName()
    {
        return 'school_referal_out_request';
    }

    public function behaviors()
    {
        return [
            [
                'class'      => '\yii\behaviors\TimestampBehavior',
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at'],
                ],
                // если вместо метки времени UNIX используется datetime:
                // 'value' => new Expression('NOW()'),
            ],
        ];
    }

    public static function add($fields)
    {
        $i = new static($fields);
        $i->save();
        $i->id = self::getDb()->lastInsertID;

        return $i;
    }
}