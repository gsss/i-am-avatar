<?php
/**
 * Created by PhpStorm.
 * User: god
 * Date: 28.02.2017
 * Time: 23:49
 */

namespace common\models\school;

use cs\services\VarDumper;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;

/**
 * @property integer id
 * @property integer school_id
 * @property integer user_id
 * @property integer parent_id
 * @property integer created_at
 */
class UserPartner extends ActiveRecord
{
    public static function tableName()
    {
        return 'school_user_partner';
    }

    public function behaviors()
    {
        return [
            [
                'class' => '\yii\behaviors\TimestampBehavior',
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at'],
                ],
                // если вместо метки времени UNIX используется datetime:
                // 'value' => new Expression('NOW()'),
            ],
        ];
    }

    public function rules()
    {
        return [
            ['created_at', 'integer'],
            ['parent_id', 'integer'],
            ['user_id', 'integer'],
            ['school_id', 'integer'],
        ];
    }

    /**
     * @return School
     */
    public function getSchool()
    {
        return School::findOne($this->school_id);
    }

    public static function add($fields)
    {
        $i = new static($fields);
        $i->save();
        $i->id = self::getDb()->lastInsertID;

        return $i;
    }


}