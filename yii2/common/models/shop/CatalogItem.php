<?php

namespace common\models\shop;

use common\models\shop\Product;
use cs\services\VarDumper;
use yii\base\BaseObject;
use yii\db\ActiveRecord;
use yii\db\Query;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;

/**
 * @property int    id
 * @property int    school_id
 * @property int    parent_id
 * @property string name
 * @property string description
 * @property string id_string
 * @property string content
 * @property int    created_at
 * @property int    sort_index
 * @property int    pay_config_id   Идентификатор платежной системы которая будет использоваться в быстрой покупке
 */
class CatalogItem extends ActiveRecord
{
    public static function tableName()
    {
        return 'gs_unions_shop_tree';
    }


    public function behaviors()
    {
        return [
            [
                'class' => '\yii\behaviors\TimestampBehavior',
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at'],
                ],
                // если вместо метки времени UNIX используется datetime:
                // 'value' => new Expression('NOW()'),
            ],
        ];
    }

    /**
     * @param $fields
     *
     * @return CatalogItem
     */
    public static function add($fields)
    {
        $i = new static($fields);
        $i->save();
        $i->id = self::getDb()->lastInsertID;

        return $i;
    }
}