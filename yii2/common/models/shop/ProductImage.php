<?php

namespace common\models\shop;

use app\models\Union;
use app\services\Subscribe;
use common\models\avatar\Currency;
use common\models\school\School;
use common\models\shop\DostavkaItem;
use cs\Application;
use cs\services\BitMask;
use yii\db\ActiveRecord;
use yii\db\Query;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;


/**
 * @property int    id
 * @property int    product_id
 * @property string name
 * @property string image
 * @property int    moderation_status
 * @property int    sort_index
 *
 */
class ProductImage extends ActiveRecord
{
    public static function tableName()
    {
        return 'gs_unions_shop_product_images';
    }

    public static function add($fields)
    {
        $item = new self($fields);
        $ret = $item->save();
        if (!$ret) \cs\services\VarDumper::dump($item->errors);
        $item->id = $item::getDb()->lastInsertID;

        return $item;
    }
}