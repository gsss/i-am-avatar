<?php

namespace common\models\shop;

use app\services\Subscribe;
use cs\base\DbRecord;
use cs\services\BitMask;
use yii\db\ActiveRecord;
use yii\db\Query;
use yii\helpers\Url;


/**
 * @property integer id
 * @property string name
 * @property integer description
 * @property integer content
 * @property integer school_id
 * @property integer price
 * @property integer is_need_address
 * @property integer type
 * @property integer is_pay
 * @property integer currency_id db.currency.id
 */
class DeliveryItem extends ActiveRecord
{
    const TYPE_SAMOVIVOZ = 1;
    const TYPE_POCHTA = 2;
    const TYPE_NALOZHNII_PLATEZH = 3;

    public static function tableName()
    {
        return 'gs_unions_shop_dostavka';
    }

    public function rules()
    {
        return [
            ['name', 'string'],
            ['description', 'string'],
            ['school_id', 'integer'],
            ['price', 'integer'],
            ['is_need_address', 'integer'],
            ['type', 'integer'],
            ['currency_id', 'integer'],
            ['currency_id', 'default', 'value' => 6],
        ];
    }

    public function attributeLabels()
    {
        return [
            'name'        => 'Название',
            'description' => 'Описание',
            'price'       => 'Стоимость',
            'type'        => 'Тип доставки',
            'currency_id' => 'Валюта',
        ];
    }
}