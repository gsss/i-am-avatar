<?php

namespace common\models\shop;

use app\models\Union;
use app\services\Subscribe;
use common\models\shop\DostavkaItem;
use cs\Application;
use cs\services\BitMask;
use yii\db\ActiveRecord;
use yii\db\Query;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;

/**
 * Class Shop
 *
 * @property int id
 * @property int union_id
 * @property string piramida допустим поле в котором через запятую перечисляются проценты по уровням, допустим, "35,10,10" значит 35 на 1-й уровень 10 на второй 10 на третий, если написать "20" то значит только 20 на первый и все
 *
 * @package common\models\shop
 */
class Shop extends ActiveRecord
{
    public static function tableName()
    {
        return 'gs_unions_shop';
    }

    public function getName()
    {
        return $this->getField('name', '');
    }

    public function getAdminEmail()
    {
        return $this->getField('admin_email', '');
    }

    /**
     * @return int 0|1
     */
    public function getModerationMode()
    {
        $v = $this->getField('moderation_mode', 1);

        return (is_null($v))? 1 : $v;
    }

    /**
     * Возвращает массив объектов используемых Платежных Систем (Конфиг) `\common\models\shop\PaymentConfig`
     *
     * @return \common\models\shop\PaymentConfig[]
     */
    public function getPayments()
    {
        return PaymentConfig::find()->where(['union_id' => $this->union_id])->all();
    }


    /**
     * @return \app\models\Union
     */
    public function getUnion()
    {
        $unionId = $this->getField('union_id');

        return Union::find($unionId);
    }

    public function getLink($isScheme = false)
    {
        $union = $this->getUnion();

        return Url::to(['union_shop/index', 'id' => $union->getId(), 'category' => $union->getCategory()->getField('id_string')]);
    }

    /**
     * Возвращает список доставки
     *
     * @return array
     */
    public function getDostavkaRows()
    {
        return \app\models\Shop\DostavkaItem::query(['union_id' => $this->getField('union_id')])->all();
    }

    /**
     * Возвращает список доставки
     *
     * @return \common\models\shop\DostavkaItem
     */
    public function getDostavka()
    {
        return DostavkaItem::find()->where(['union_id' => $this->getField('union_id')])->all();
    }

    /**
     * Выдает подпись для магазина
     */
    public function getMailSignature()
    {
        $v = $this->getField('signature', '');
        return ($v == '')? 'С уважением.' : $v;
    }

    /**
     *
     */
    public function getMailImage()
    {
        return \yii\helpers\Url::to($this->getField('mail_image', '/images/page/mission/ab.jpg'), true);
    }

    /**
     * Отправляет письмо в формате html
     *
     * @param string       $email   куда
     * @param string       $subject тема
     * @param string       $view    шаблон, лежит в /mail/html и /mail/text
     * @param array        $options параметры для шаблона
     * @param array        $attaches файлы для прикрепления
     * [[
     *      'path' => 'file_name',
     * ]]
     *
     * @return boolean
     */
    public function mail($email, $subject, $view, $options = [], $attaches = [])
    {
        if ($this) {
            $options['shop'] = $this;
        }
        return Application::mail($email, $subject, $view, $options, ['html' => 'layouts/html/shop'], $attaches);
    }

    /**
     * Отправляет письмо в формате html
     *
     * @param string       $subject тема
     * @param string       $view    шаблон, лежит в /mail/html и /mail/text
     * @param array        $options параметры для шаблона
     * @param array        $attaches файлы для прикрепления
     * [[
     *      'path' => 'file_name',
     * ]]
     *
     * @return boolean
     */
    public function mailToMe($subject, $view, $options = [], $attaches = [])
    {
        $options['shop'] = $this;

        return Application::mail($this->getAdminEmail(), $subject, $view, $options, ['html' => 'layouts/html/shop'], $attaches);
    }

    public function getField($name, $default = null)
    {
        return (is_null($this->$name)) ? $default : $this->$name;
    }
}