<?php

namespace common\models\shop;

use common\models\avatar\UserBill;
use common\models\piramida\Wallet;
use common\models\piramida\WalletSourceInterface;
use common\models\school\PageBlockContent;
use common\models\school\ReferalLevel;
use common\models\school\ReferalTransaction;
use common\models\school\School;
use cs\Application;
use cs\services\BitMask;
use cs\services\VarDumper;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\db\Query;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;


/**
 * Class Request
 *
 * @property int    id
 * @property int    user_id
 * @property int    school_id
 * @property int    status
 * @property int    created_at
 * @property string comment
 * @property string address
 * @property string phone
 * @property int    is_answer_from_shop
 * @property int    is_answer_from_client
 * @property int    last_message_time
 * @property int    dostavka_id
 * @property int    price
 * @property int    is_canceled
 * @property int    is_paid
 * @property int    is_paid_client
 * @property int    is_hide
 * @property int    parent_id
 * @property int    sum
 * @property int    billing_id
 * @property int    currency_id             db.currency.id
 *
 * @package common\models\shop
 */
class Request extends ActiveRecord
{
    const EVENT_BEFORE_SHOP_SUCCESS = 'beforeShopSuccess';
    const EVENT_AFTER_SHOP_SUCCESS = 'afterShopSuccess';

    const STATUS_USER_NOT_CONFIRMED = 1; // Пользователь заказал с регистрацией пользователя, но не подтвердил свою почту еще

    const STATUS_SEND_TO_SHOP = 2;       // заказ отправлен в магазин
    const STATUS_ORDER_DOSTAVKA = 3;     // клиенту выставлен счет с учетом доставки
    const STATUS_PAID_CLIENT = 5;        // заказ оплачен со стороны клиента
    const STATUS_PAID_SHOP = 6;          // оплата подтверждена магазином
    const STATUS_REJECTED = 17;          // Заказ отменен клиентом

    // статусы для доставки
    const STATUS_DOSTAVKA_ADDRESS_PREPARE = 8;            // заказ формируется для отправки клиенту
    const STATUS_DOSTAVKA_ADDRESS_PREPARE_DONE = 9;       // заказ сформирован для отправки клиенту
    const STATUS_DOSTAVKA_ADDRESS_SEND_TO_USER = 7;       // заказ отправлен клиенту
    const STATUS_DOSTAVKA_ADDRESS_FINISH_CLIENT = 10;     // заказ исполнен, как сообщил клиент
    const STATUS_DOSTAVKA_ADDRESS_FINISH_SHOP = 11;       // заказ исполнен, магазин сам указал этот статус по своим данным

    // статусы для самовывоза
    const STATUS_DOSTAVKA_SAMOVIVOZ_WAIT = 13;            // заказ ожидает клиента
    const STATUS_DOSTAVKA_SAMOVIVOZ_FINISH_CLIENT = 14;   // клиент сообщил что получил товар
    const STATUS_DOSTAVKA_SAMOVIVOZ_FINISH_SHOP = 15;     // заказ исполнен, магазин сам указал этот статус по своим данным

    // статусы для электронного товара
    const STATUS_DOSTAVKA_ELECTRON_DONE = 16;             // заказ принимает этот статус если в заказе все товары электронные, после факта оплаты заказа

    // статусы для наложного платежа
    const STATUS_DOSTAVKA_NALOZH_PREPARE = 21;            // заказ формируется для отправки клиенту
    const STATUS_DOSTAVKA_NALOZH_PREPARE_DONE = 22;       // заказ сформирован для отправки клиенту
    const STATUS_DOSTAVKA_NALOZH_SEND_TO_USER = 23;       // заказ отправлен клиенту
    const STATUS_DOSTAVKA_NALOZH_PIAD = 24;               // заказ исполнен, деньги были получены
    const STATUS_DOSTAVKA_NALOZH_RETURN = 25;             // заказ вернулся, отменен

    const DIRECTION_TO_CLIENT = 1;
    const DIRECTION_TO_SHOP = 2;

    public static $statusList = [
        self::STATUS_DOSTAVKA_NALOZH_PREPARE               => [
            'client'   => 'заказ формируется для отправки клиенту',
            'shop'     => 'заказ формируется для отправки клиенту',
            'timeLine' => [
                'icon'  => 'glyphicon-plane',
                'color' => 'default',
            ],
        ],
        self::STATUS_DOSTAVKA_NALOZH_PREPARE_DONE               => [
            'client'   => 'заказ сформирован для отправки клиенту',
            'shop'     => 'заказ сформирован для отправки клиенту',
            'timeLine' => [
                'icon'  => 'glyphicon-plane',
                'color' => 'warning',
            ],
        ],
        self::STATUS_DOSTAVKA_NALOZH_SEND_TO_USER               => [
            'client'   => 'заказ отправлен клиенту',
            'shop'     => 'заказ отправлен клиенту',
            'timeLine' => [
                'icon'  => 'glyphicon-plane',
                'color' => 'success',
            ],
        ],
        self::STATUS_DOSTAVKA_NALOZH_PIAD               => [
            'client'   => 'заказ исполнен, деньги были получены',
            'shop'     => 'заказ исполнен, деньги были получены',
            'timeLine' => [
                'icon'  => 'glyphicon-thumbs-up',
                'color' => 'success',
            ],
        ],
        self::STATUS_DOSTAVKA_NALOZH_RETURN               => [
            'client'   => 'заказ вернулся, отменен',
            'shop'     => 'заказ вернулся, отменен',
            'timeLine' => [
                'icon'  => 'glyphicon-remove',
                'color' => 'danger',
            ],
        ],


        self::STATUS_REJECTED               => [
            'client'   => 'Заказ отменен',
            'shop'     => 'Пользователь отменил заказ',
            'timeLine' => [
                'icon'  => 'glyphicon-remove',
                'color' => 'danger',
            ],
        ],
        self::STATUS_USER_NOT_CONFIRMED               => [
            'client'   => 'Пользователь не подтвердил почту',
            'shop'     => 'Пользователь не подтвердил почту',
            'timeLine' => [
                'icon'  => 'glyphicon-minus',
                'color' => 'default',
            ],
        ],
        self::STATUS_SEND_TO_SHOP                     => [
            'client'   => 'Отпрален в магазин',
            'shop'     => 'Пользователь отправил заказ',
            'timeLine' => [
                'icon'  => 'glyphicon-ok',
                'color' => 'default',
            ],
        ],
        self::STATUS_ORDER_DOSTAVKA                   => [
            'client'   => 'Выставлен счет с учетом доставки',
            'shop'     => 'Выставлен счет с учетом доставки',
            'timeLine' => [
                'icon'  => 'glyphicon-credit-card',
                'color' => 'default',
            ],
        ],
        self::STATUS_PAID_CLIENT                      => [
            'client'   => 'Оплата сделана',
            'shop'     => 'Оплата сделана',
            'timeLine' => [
                'icon'  => 'glyphicon-credit-card',
                'color' => 'warning',
            ],
        ],
        self::STATUS_PAID_SHOP                        => [
            'client'   => 'Оплата подтверждена',
            'shop'     => 'Оплата подтверждена',
            'timeLine' => [
                'icon'  => 'glyphicon-credit-card',
                'color' => 'success',
            ],
        ],
        self::STATUS_DOSTAVKA_ADDRESS_PREPARE         => [
            'client'   => 'Заказ формируется для отправки клиенту',
            'shop'     => 'Заказ формируется для отправки клиенту',
            'timeLine' => [
                'icon'  => 'glyphicon-plane',
                'color' => 'default',
            ],
        ],


        self::STATUS_DOSTAVKA_ADDRESS_PREPARE_DONE    => [
            'client'   => 'Заказ сформирован для отправки клиенту',
            'shop'     => 'Заказ сформирован для отправки клиенту',
            'timeLine' => [
                'icon'  => 'glyphicon-plane',
                'color' => 'warning',
            ],
        ],
        self::STATUS_DOSTAVKA_ADDRESS_SEND_TO_USER    => [
            'client'   => 'Заказ отправлен клиенту',
            'shop'     => 'Заказ отправлен клиенту',
            'timeLine' => [
                'icon'  => 'glyphicon-plane',
                'color' => 'success',
            ],
        ],
        self::STATUS_DOSTAVKA_ADDRESS_FINISH_CLIENT   => [
            'client'   => 'Заказ исполнен, как сообщил клиент',
            'shop'     => 'Заказ исполнен, как сообщил клиент',
            'timeLine' => [
                'icon'  => 'glyphicon-thumbs-up',
                'color' => 'success',
            ],
        ],
        self::STATUS_DOSTAVKA_ADDRESS_FINISH_SHOP     => [
            'client'   => 'Заказ выполнен',
            'shop'     => 'Заказ выполнен',
            'timeLine' => [
                'icon'  => 'glyphicon-thumbs-up',
                'color' => 'success',
            ],
        ],
        self::STATUS_DOSTAVKA_SAMOVIVOZ_WAIT          => [
            'client'   => 'Заказ ожидает клиента',
            'shop'     => 'Заказ ожидает клиента',
            'timeLine' => [
                'icon'  => 'glyphicon-plane',
                'color' => 'warning',
            ],
        ],
        self::STATUS_DOSTAVKA_SAMOVIVOZ_FINISH_CLIENT => [
            'client'   => 'Клиент сообщил что получил товар',
            'shop'     => 'Клиент сообщил что получил товар',
            'timeLine' => [
                'icon'  => 'glyphicon-thumbs-up',
                'color' => 'success',
            ],
        ],
        self::STATUS_DOSTAVKA_SAMOVIVOZ_FINISH_SHOP   => [
            'client'   => 'Заказ исполнен',
            'shop'     => 'Заказ исполнен',
            'timeLine' => [
                'icon'  => 'glyphicon-thumbs-up',
                'color' => 'success',
            ],
        ],
        self::STATUS_DOSTAVKA_ELECTRON_DONE           => [
            'client'   => 'Заказ отправлен по почте',
            'shop'     => 'Заказ отправлен по почте',
            'timeLine' => [
                'icon'  => 'glyphicon-thumbs-up',
                'color' => 'success',
            ],
        ],
    ];

    public static function tableName()
    {
        return 'gs_users_shop_requests';
    }

    public function behaviors()
    {
        return [
            [
                'class'      => '\yii\behaviors\TimestampBehavior',
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at'],
                ],
                // если вместо метки времени UNIX используется datetime:
                // 'value' => new Expression('NOW()'),
            ],
        ];
    }

    /**
     * @param $fields
     *
     * @return static
     */
    public static function add($fields)
    {
        $i = new static($fields);
        $i->save();
        $i->id = self::getDb()->lastInsertID;

        return $i;
    }

    /**
     * Клиент подтвердил отправку денег
     */
    public function successClient($message = null)
    {
        $this->is_paid_client = 1;
        $this->save();

        if (!is_null($message)) {
            $this->addStatusToShop([
                'status'  => self::STATUS_PAID_CLIENT,
                'message' => $message,
            ]);
        } else {
            $this->addStatusToShop(self::STATUS_PAID_CLIENT);
        }
    }

    /**
     * @return \yii\db\Query
     */
    public function getMessages()
    {
        return RequestMessage::find()->where(['request_id' => $this->id]);
    }

    /**
     * @return \common\models\school\School
     */
    public function getSchool()
    {
        return School::findOne($this->school_id);
    }

    /**
     * Магазин подтвердил приход денег
     */
    public function successShop($data = null)
    {
        $this->trigger(self::EVENT_BEFORE_SHOP_SUCCESS);

        // Ставлю статус оплачено
        $this->is_paid = 1;
        $this->save();

        $this->addStatusToClient(self::STATUS_PAID_SHOP);

        $s = $this->getSchool();
        if ($s->is_mlm) {

            // Если пользователь уже встроен в пирамиду, то ссылка не актуальна.
            $partner = \common\models\school\UserPartner::findOne(['school_id' => $s->id, 'user_id' => $this->user_id]);
            if (is_null($partner)) {
                if ($this->parent_id) {
                    $partner = \common\models\school\UserPartner::add([
                        'school_id' => $s->id,
                        'user_id'   => $this->user_id,
                        'parent_id' => $this->parent_id,
                    ]);
                }
            }

            if ($partner) {
                // распределение реферальных начислений
                $levels = ReferalLevel::find()->where(['school_id' => $this->school_id])->all();
                if (count($levels) > 0) {
                    ArrayHelper::multisort($levels, 'level');
                    $referal_wallet_id = $s->referal_wallet_id;
                    $referal_wallet = Wallet::findOne($referal_wallet_id);
                    $currency_id = $referal_wallet->currency_id;
                    $price = $this->price;
                    for ($level = 1; $level <= count($levels); $level++) {
                        $parent_id = $partner->parent_id;

                        /** @var \common\models\school\ReferalLevel $referalLevel */
                        $referalLevel = $levels[$level - 1];
                        // множитель для цены
                        $m = $referalLevel->percent / 10000;
                        // сумма отчислений
                        $amount = (int)($price * $m);
                        $bill = UserBill::getBillByCurrencyAvatarProcessing($currency_id, $parent_id);
                        $wallet = Wallet::findOne($bill->address);
                        $transaction = $referal_wallet->move($wallet, $amount, 'Реферальные начисления за заказ = ' . $this->id . ' от uid=' . $this->user_id . ' к uid=' . $parent_id);

                        // Записываю реферальные начисления
                        $ReferalTransaction = ReferalTransaction::add([
                            'school_id'      => $this->school_id,
                            'request_id'     => $this->id,
                            'level'          => $level,
                            'from_uid'       => $this->user_id,
                            'to_uid'         => $parent_id,
                            'to_wid'         => $wallet->id,
                            'amount'         => $amount,
                            'transaction_id' => $transaction->id,
                            'currency_id'    => $wallet->currency_id,
                        ]);

                        // выбираю следующего из уровня
                        $partner = \common\models\school\UserPartner::findOne(['school_id' => $s->id, 'user_id' => $partner->parent_id]);

                        // если дерево кончилось то выхожу
                        if (is_null($partner)) break;
                    }
                }
            }
        }
        $this->trigger(self::EVENT_AFTER_SHOP_SUCCESS);
    }


    /**
     * Добавить статус
     *
     * @param int | array $status статус self::STATUS_* или массив со статусом и сообщением
     * @param int $direction направление сообщения self::DIRECTION_*
     *
     * @return \common\models\shop\RequestMessage
     */
    public function addStatus($status, $direction)
    {
        if (!is_array($status)) {
            $fields = [
                'status' => $status,
            ];
        } else {
            $fields = $status;
        }

        return $this->addMessageItem($fields, $direction);
    }

    /**
     * Добавить статус к клиенту
     *
     * @param int | array $status статус self::STATUS_* или массив со статусом и сообщением
     *
     * @return \common\models\shop\RequestMessage
     */
    public function addStatusToClient($status)
    {
        if (!is_array($status)) {
            $fields = [
                'status' => $status,
            ];
        } else {
            $fields = $status;
        }

        return $this->addMessageItem($fields, self::DIRECTION_TO_CLIENT);
    }

    /**
     * Добавить статус в магазин
     *
     * @param int | array $status статус self::STATUS_* или массив со статусом и сообщением
     *
     * @return \common\models\shop\RequestMessage
     */
    public function addStatusToShop($status)
    {
        if (!is_array($status)) {
            $fields = [
                'status' => $status,
            ];
        } else {
            $fields = $status;
        }

        return $this->addMessageItem($fields, self::DIRECTION_TO_SHOP);
    }

    /**
     * Добавить сообщение
     *
     * @param string $message сообщение
     * @param int $direction направление сообщения self::DIRECTION_*
     *
     * @return \common\models\shop\RequestMessage
     */
    public function addMessage($message, $direction)
    {
        return $this->addMessageItem([
            'message' => $message,
        ], $direction);
    }

    /**
     * Добавить сообщение для клиента
     *
     * @param string $message сообщение
     *
     * @return \common\models\shop\RequestMessage
     */
    public function addMessageToClient($message)
    {
        return $this->addMessage($message, self::DIRECTION_TO_CLIENT);
    }

    /**
     * Добавить сообщение для клиента
     *
     * @param string $message сообщение
     *
     * @return \common\models\shop\RequestMessage
     */
    public function addMessageToShop($message)
    {
        return $this->addMessage($message, self::DIRECTION_TO_SHOP);
    }

    /**
     * Добавить сообщение или статус
     *
     * @param array $fields поля для сообщения
     * @param int $direction направление сообщения self::DIRECTION_*
     *
     * @return \common\models\shop\RequestMessage
     */
    public function addMessageItem($fields, $direction)
    {
        $fieldsRequest = [
            'is_answer_from_shop'   => ($direction == self::DIRECTION_TO_CLIENT) ? 1 : 0,
            'is_answer_from_client' => ($direction == self::DIRECTION_TO_CLIENT) ? 0 : 1,
            'last_message_time'     => time(),
        ];
        if (isset($fields['status'])) {
            $fieldsRequest['status'] = $fields['status'];
        }
        if ($direction == self::DIRECTION_TO_SHOP) {
        }
        foreach ($fieldsRequest as $k => $v) {
            $this->$k = $v;
        }

        return RequestMessage::add(ArrayHelper::merge($fields, [
            'request_id' => $this->id,
            'direction'  => $direction,
        ]));
    }

}