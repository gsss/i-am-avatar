<?php

namespace common\models\shop;

use common\models\shop\Product;
use cs\services\VarDumper;
use yii\base\BaseObject;
use yii\db\ActiveRecord;
use yii\db\Query;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;

/**
 * @property int id
 * @property int user_id
 * @property int lat
 * @property int lng
 * @property int type_id
 */
class UserMap extends ActiveRecord
{
    public static function tableName()
    {
        return 'user_map';
    }

    /**
     * @param $fields
     *
     * @return CatalogItem
     */
    public static function add($fields)
    {
        $i = new static($fields);
        $i->save();
        $i->id = self::getDb()->lastInsertID;

        return $i;
    }
}