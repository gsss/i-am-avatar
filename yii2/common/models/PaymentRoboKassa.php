<?php

namespace common\models;

use Yii;
use yii\base\Exception;
use yii\db\ActiveRecord;

/**
 * @property integer    id
 * @property integer    billing_id
 * @property integer    currency_id
 * @property integer    amount
 * @property string     transaction
 * @property integer    created_at
 */
class PaymentRoboKassa extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'payments_robo_kassa';
    }

    public function behaviors()
    {
        return [
            [
                'class' => '\yii\behaviors\TimestampBehavior',
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at'],
                ],
                // если вместо метки времени UNIX используется datetime:
                // 'value' => new Expression('NOW()'),
            ],
        ];
    }

    public static function add($fields)
    {
        $i = new static($fields);
        $i->save();
        $i->id = self::getDb()->lastInsertID;

        return $i;
    }
}
