<?php
/**
 * Created by PhpStorm.
 * User: Бог-Творец
 * Date: 06.01.2017
 * Time: 17:14
 */

namespace common\models;



use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;

/**
 * @property int    id
 * @property int    user_id
 * @property int    is_hide
 * @property string name
 * @property string content
 *
 * @package common\models
 */
class UserIdea extends ActiveRecord
{
    public static function tableName()
    {
        return 'user_idea';
    }

    public function rules()
    {
        return [
            ['content','string'],
            ['name','string'],
            ['user_id','integer'],
            ['is_hide','integer'],
            ['id','integer'],
        ];
    }

    /**
     * @return \common\models\UserAvatar
     * @throws
     */
    public function getUser()
    {
        return UserAvatar::findOne($this->user_id);
    }

    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at'],
                ],
                // если вместо метки времени UNIX используется datetime:
                // 'value' => new Expression('NOW()'),
            ],
        ];
    }

}