<?php

namespace common\models;

use app\services\Subscribe;
use cs\services\BitMask;
use yii\db\ActiveRecord;
use yii\db\Query;

/**
 * Class Anketa
 *
 * @package app\models\rai
 *
 * @property int id
 * @property int created_at
 * @property int user_id
 * @property string name_first
 * @property string name_last
 * @property string name_middle
 * @property string contact_email
 * @property string contact_phone
 * @property string contact_vk
 * @property string contact_fb
 * @property string contact_telegram
 * @property string contact_whatsapp
 * @property string contact_skype
 * @property string age
 * @property string place
 * @property string vozmojnosti
 * @property string opit
 * @property int comments_count
 * @property int last_message_time
 */
class Anketa extends ActiveRecord
{
    public static function tableName()
    {
        return 'anketa';
    }

    public function rules()
    {
        return [
            ['created_at', 'integer'],
            ['last_message_time', 'integer'],
            ['comments_count', 'integer'],
            ['name_first', 'string'],
            ['name_last', 'string'],
            ['name_middle', 'string'],
            ['contact_email', 'string'],
            ['contact_phone', 'string'],
            ['contact_vk', 'string'],
            ['contact_fb', 'string'],
            ['contact_telegram', 'string'],
            ['contact_whatsapp', 'string'],
            ['contact_skype', 'string'],
            ['age', 'string'],
            ['place', 'string'],
            ['vozmojnosti', 'string'],
            ['opit', 'string'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'name_first'       => 'Имя',
            'name_last'        => 'Фамилия',
            'name_middle'      => 'Отчество',
            'contact_email'    => 'Email',
            'contact_phone'    => 'Телефон',
            'contact_vk'       => 'Страница Вконтакте (ссылка)',
            'contact_fb'       => 'Страница FaceBook (ссылка)',
            'contact_telegram' => 'Аккаунт в Телеграм или на который зарегистрирован аккаунт',
            'contact_whatsapp' => 'Whatsapp, Телефон на который зарегистрирован аккаунт',
            'contact_skype'    => 'Аккаунт скайп',
            'age'              => 'Полных лет',
            'place'            => 'Место проживания',
            'vozmojnosti'      => 'Навыки которыми обладаете, вверху самые наработанные и по убывающей',
            'opit'             => 'Какой опыт был, в каких сферах и в каких ролях',
        ];
    }

}