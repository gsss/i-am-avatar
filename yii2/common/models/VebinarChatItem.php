<?php
/**
 * Created by PhpStorm.
 * User: Бог-Творец
 * Date: 06.01.2017
 * Time: 17:14
 */

namespace common\models;



use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;

/**
 * @property int    id
 * @property int    vebinar_id
 * @property int    created_at
 * @property string session_id
 * @property string text
 *
 * @package common\models
 */
class VebinarChatItem extends ActiveRecord
{
    public static function tableName()
    {
        return 'vebinar_chat';
    }

    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at'],
                ],
                // если вместо метки времени UNIX используется datetime:
                // 'value' => new Expression('NOW()'),
            ],
        ];
    }

}