<?php
namespace common\models;

use avatar\models\UserRegistration;
use avatar\modules\UniSender\UniSender;
use common\components\providers\BTC;
use common\models\avatar\Currency;
use common\models\avatar\UserBill;
use common\models\avatar\UserBillDefault;
use common\models\avatar\UserBinance;
use cs\Application;
use cs\services\Security;
use cs\services\SitePath;
use cs\services\VarDumper;
use Imagine\Image\Box;
use yii\base\Exception;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;
use yii\helpers\FileHelper;
use yii\helpers\Url;
use cs\services\UploadFolderDispatcher;
use yii\imagine\Image;
use Imagine\Image\ManipulatorInterface;
use yii\web\IdentityInterface;


/**
 * UserRoot model
 *
 * @property integer   id
 * @property integer   avatar_status 0 - не подтвержден, 1 - подтверждена почта, 2 - подтверждена почта и есть запись в user
 * @property string    email
 */
class UserRoot extends ActiveRecord
{
    const STATUS_UNCONFIRMED = 0;
    const STATUS_CONFIRMED = 1;
    const STATUS_REGISTERED = 2;

    public static function tableName()
    {
        return 'user_root';
    }

    public static function add($fields)
    {
        $i = new static($fields);
        $i->save();
        $i->id = self::getDb()->lastInsertID;

        return $i;
    }
}
