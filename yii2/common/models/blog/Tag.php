<?php
/**
 * Created by PhpStorm.
 * User: s.arhangelskiy
 * Date: 12.04.2016
 * Time: 16:13
 */

namespace common\models\blog;


use yii\db\ActiveRecord;
use yii\db\Query;
use yii\helpers\ArrayHelper;
use yii\helpers\VarDumper;

/**
 * @property int    id
 * @property int    school_id
 * @property int    sort_index
 * @property string name
 *
 *
 */
class Tag extends ActiveRecord
{
    public static function tableName()
    {
        return 'school_blog_tag';
    }
}