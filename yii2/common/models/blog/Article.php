<?php
/**
 * Created by PhpStorm.
 * User: s.arhangelskiy
 * Date: 12.04.2016
 * Time: 16:13
 */

namespace common\models\blog;

use yii\db\ActiveRecord;
use yii\db\Query;
use yii\helpers\ArrayHelper;
use yii\helpers\VarDumper;

/**
 * @property int    id
 * @property int    school_id
 * @property int    document_id
 * @property int    created_at
 * @property int    views_counter
 * @property int    is_signed
 * @property int    is_subscribe
 * @property string date
 * @property string id_string
 * @property string content
 * @property string name
 * @property string image
 * @property string link
 *
 *
 * Class Message
 */
class Article extends ActiveRecord
{
    public static function tableName()
    {
        return 'school_blog_article';
    }

    /**
     * Возвращает ссылку на статью
     *
     * @param bool $isScheme надо ли добавлять полный путь
     *
     * @return string
     */
    public function getLink($isScheme = false)
    {
        $date = $this->date;
        $year = substr($date, 0, 4);
        $month = substr($date, 5, 2);
        $day = substr($date, 8, 2);
        $id = $this->id_string;
        $url = "/blog/{$year}/{$month}/{$day}/{$id}";

        return \yii\helpers\Url::to($url, $isScheme);
    }

}

