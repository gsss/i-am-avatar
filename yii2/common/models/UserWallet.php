<?php
namespace common\models;

use avatar\models\UserRegistration;
use common\components\providers\BTC;
use common\models\avatar\Currency;
use common\models\avatar\UserBill;
use common\models\avatar\UserBillDefault;
use common\models\piramida\Wallet;
use cs\Application;
use cs\services\Security;
use cs\services\SitePath;
use cs\services\VarDumper;
use Imagine\Image\Box;
use yii\base\Exception;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;
use yii\helpers\FileHelper;
use yii\helpers\Url;
use cs\services\UploadFolderDispatcher;
use yii\imagine\Image;
use Imagine\Image\ManipulatorInterface;
use yii\web\IdentityInterface;


/**
 * @property integer   id
 * @property integer   user_id
 * @property integer   wallet_id
 * @property integer   currency_id
 */
class UserWallet extends ActiveRecord
{
    public static function tableName()
    {
        return 'user_wallet';
    }

    public function rules()
    {
        return [
            [self::attributes(), 'safe'],
        ];
    }

    /**
     * @param $id
     *
     * @return UserWallet
     */
    public static function getWalletByCurrency($id , $user_id = null)
    {
        if (is_null($user_id)) $user_id = \Yii::$app->user->id;

        $i = self::findOne(['user_id' => $user_id, 'currency_id' => $id]);
        if (is_null($i)) {
            $wallet = Wallet::addNew(['currency_id' => $id]);
            return self::add([
                'user_id'     => $user_id,
                'wallet_id'   => $wallet->id,
                'currency_id' => $id,
            ]);
        }
        return $i;
    }
    /**
     * @param array $fields
     *
     * @return UserWallet
     */
    public static function add($fields)
    {
        $i = new self($fields);
        $i->save();
        $i->id = self::getDb()->lastInsertID;

        return $i;
    }

    public function getWallet()
    {
        return Wallet::findOne($this->wallet_id);
    }

    /**
     * @return UserAvatar
     * @throws Exception
     */
    public function getUser()
    {
        return UserAvatar::findOne($this->user_id);
    }
}
