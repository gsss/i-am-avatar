<?php
/**
 * Created by PhpStorm.
 * User: god
 * Date: 28.02.2017
 * Time: 23:49
 */

namespace common\models;

use common\models\avatar\UserBill;
use common\models\piramida\Billing;
use common\models\statistic\StatisticDb;
use yii\db\ActiveRecord;

/**
 * @property int created_at
 */
class CardId extends ActiveRecord
{
    public static function tableName()
    {
        return 'card_id';
    }

}