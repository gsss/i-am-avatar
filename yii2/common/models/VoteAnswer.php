<?php
/**
 * Created by PhpStorm.
 * User: s.arhangelskiy
 * Date: 12.04.2016
 * Time: 16:13
 */

namespace common\models;

use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\helpers\VarDumper;

/**
 *
 * @property int        id
 * @property string     name
 * @property int        list_id
 */
class VoteAnswer extends ActiveRecord
{
    public static function tableName()
    {
        return 'vote_answer';
    }

    public function rules()
    {
        return [
            ['name', 'string'],
            ['name', 'required'],
        ];
    }

}