<?php
/**
 * Created by PhpStorm.
 * User: Бог-Творец
 * Date: 06.01.2017
 * Time: 17:14
 */

namespace common\models;



use yii\db\ActiveRecord;

/**
 * @property int    id      - идентификатор пользователя
 * @property int    register_count
 * @property int    upload_count
 *
 * @package common\models
 */
class UserFilesUpload extends ActiveRecord
{
    public static function tableName()
    {
        return 'user_files_upload';
    }
}