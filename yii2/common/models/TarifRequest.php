<?php
/**
 * Created by PhpStorm.
 * User: god
 * Date: 28.02.2017
 * Time: 23:49
 */

namespace common\models;

use common\models\avatar\Currency;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;

/**
 * Class TarifRequest
 *
 * @property integer id
 * @property integer tarif_id
 * @property integer company_id
 * @property integer count
 * @property integer price
 * @property integer currency_id
 * @property integer billing_id
 * @property integer created_at
 *
 * @package common\models
 */
class TarifRequest extends ActiveRecord
{

    public static function add($fields)
    {
        $i = new static($fields);
        $i->save();
        $i->id = self::getDb()->lastInsertID;

        return $i;
    }

    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at'],
                ],
                // если вместо метки времени UNIX используется datetime:
                // 'value' => new Expression('NOW()'),
            ],
        ];
    }
}