<?php

namespace common\models;

use common\models\avatar\Currency;
use Yii;
use yii\base\Exception;
use yii\base\NotSupportedException;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\web\IdentityInterface;

/**
 * PaySystem model
 *
 * @property integer    id
 * @property string     code
 * @property string     title
 * @property string     class_name
 * @property string     image
 * @property string(8)  currency
 */
class PaySystem extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'paysystems';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['code', 'string', 'max' => 255],
            ['image', 'string', 'max' => 255],
            ['class_name', 'string', 'max' => 20],
            ['code', 'string', 'max' => 20],
            ['title', 'string', 'max' => 60],
            ['currency', 'string', 'max' => 8],
            ['id', 'integer'],
            ['comission_percent', 'integer'],
        ];
    }

    /**
     * @param mixed $condition
     *
     * @return static
     * @throws \yii\base\Exception
     */
    public static function findOne($condition)
    {
        $i = parent::findOne($condition);
        if (is_null($i)) {
            throw new Exception('Не найдена платежная система');
        }
        return $i;
    }

    /**
     * @return string
     */
    public function getClassName()
    {
        return '\\avatar\\modules\\PaySystems\\items\\' . $this->class_name . '\\in\\Class1';
    }

    /**
     * @param array $fields инициализирующий массив
     *
     * @return \common\models\piramida\WalletSourceInterface
     */
    public function getClass($fields = null)
    {
        if (is_null($fields)) {
            $fields = [];
            $fields['paySystem'] = $this;
            $fields['source_id'] = $this->id;
        } else {
            if (!isset($fields['paySystem'])) {
                $fields['paySystem'] = $this;
            }
            if (!isset($fields['source_id'])) {
                $fields['source_id'] = $this->id;
            }
        }
        $s = $this->getClassName();

        $object = new $s($fields);

        return $object;
    }

    /**
     * @return \common\models\avatar\Currency
     * @throws Exception
     */
    public function getCurrencyObject()
    {
        $currency = $this->currency;
        $c = Currency::findOne(['code' => $currency]);
        if (is_null($c)) {
            throw new Exception('Не найлена валюта ' . $currency);
        }

        return $c;
    }

    /**
     * Функция которая возвращает класс совместимый с интерфейсом \common\models\PaymentI для тог чтобы можно было
     * указать транзакцию
     *
     * @param int $billingId идентификатор заказа
     *
     * @return \common\models\PaymentI
     * @throws
     */
    function getPayment($billingId)
    {
        $classList = [

        ];
        $className = $this->class_name;
        $fullClassName = '\common\models\Payment' . $className;

        $object = $fullClassName::findOne(['billing_id' => $billingId]);
        if (is_null($object)) throw new Exception('Не найден платеж $billingId=' . $billingId);

        return $object;
    }

}
