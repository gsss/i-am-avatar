<?php
/**
 * Created by PhpStorm.
 * User: Бог-Творец
 * Date: 06.01.2017
 * Time: 17:14
 */

namespace common\models;


use cs\services\VarDumper;

use yii\db\ActiveRecord;

/**
 * Подпись документа
 *
 * @property int         id
 * @property int         user_id     идентификатор хозяина документа по базе данных АватарБанка (Класс UserAvatar)
 * @property int         document_id идентификатор документа по контракту подписи документов
 * @property int         created_at  мгновение добавления записи в БД АватарБанка
 * @property string      name
 * @property string(70)  txid
 * @property string      file
 * @property string(70)  hash
 * @property int         type_id
 * @property int         school_id
 * @property string(160) signature
 * @property string      data
 * @property string      link
 *
 * Class UserDevice
 * @package common\models
 */
class UserDocument extends ActiveRecord
{
    const TYPE_ETHEREUM = 1;
    const TYPE_AVATAR_CHAIN = 2;

    public static function tableName()
    {
        return 'user_documents';
    }

    /**
     *
     * @param array $fields
     *
     * @return \common\models\UserDocument
     */
    public static function add($fields)
    {
        if (!isset($fields['created_at'])) {
            $fields['created_at'] = time();
        }
        $item = new static($fields);
        $ret = $item->save();
        $item->id = static::getDb()->lastInsertID;

        return $item;
    }

    /**
     * Выдает хозяина документа
     *
     * @return \common\models\UserAvatar
     */
    public function getOwner()
    {
        return UserAvatar::findOne($this->user_id);
    }
}