<?php
/**
 * Created by PhpStorm.
 * User: god
 * Date: 28.02.2017
 * Time: 23:49
 */

namespace common\models;

use common\models\avatar\Currency;
use yii\db\ActiveRecord;

/**
 * Class Tarif
 *
 * @property integer id
 * @property string  name
 * @property string  description
 * @property integer price
 * @property integer currency_id db.currency.id
 *
 * @package common\models
 */
class Tarif extends ActiveRecord
{

    public static function add($fields)
    {
        $i = new static($fields);
        $i->save();
        $i->id = self::getDb()->lastInsertID;

        return $i;
    }
}