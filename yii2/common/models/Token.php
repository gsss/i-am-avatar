<?php
/**
 * Created by PhpStorm.
 * User: god
 * Date: 28.02.2017
 * Time: 23:49
 */

namespace common\models;

use common\models\avatar\Currency;
use yii\db\ActiveRecord;

/**
 * Class Token
 *
 * @property integer id
 * @property string  address
 * @property string  abi
 * @property integer currency_id
 *
 * @package common\models
 */
class Token extends ActiveRecord
{

    public static function tableName()
    {
        return 'token';
    }

    /**
     * @return \common\components\providers\Token
     */
    public function getProvider()
    {
        $provider = new \common\components\providers\Token([
            'contract' => $this->address,
            'abi'      => $this->abi,
            'currency' => $this->currency_id,
        ]);

        return $provider;
    }

    /**
     * @return \common\models\avatar\Currency
     */
    public function getCurrency()
    {
        return Currency::findOne($this->currency_id);
    }
}