<?php
/**
 * Created by PhpStorm.
 * User: god
 * Date: 28.02.2017
 * Time: 23:49
 */

namespace common\models;

use common\models\avatar\UserBill;
use common\models\piramida\Billing;
use common\models\statistic\StatisticDb;
use yii\db\ActiveRecord;

/**
 * Class Card
 *
 * @property integer id
 * @property integer number             номер карты
 * @property string  myfire_id          Идентификатор mufire string(8), Шестнадцатеричное число большими буквами
 * @property integer design_id          Идентификатор дизайна
 * @property string  pin                хеш ри пинкода
 * @property string  finish             формат 'MM/YYYY'
 * @property string  address            адрес указанный на карте полностью с префиками и суфиксами как указано на карте
 * @property integer secret_num         секретный номер на обратной стороне
 * @property integer is_new             Это карта на перевыпуск? Я их нашел и понял что их надо перевыпустить
 * @property integer is_address_work    рабочий ли адрес на карте? 0 - нет, этот адрес только для идентификации, 1 - да адрес рабочий кошелек
 * @property integer user_id            Идентификатор пользователя владельца карты
 * @property integer is_personal
 *
 * @package common\models
 */
class Card extends ActiveRecord
{

    public function apiFields()
    {
        return [
            'id',
            'number',
            'design_id',
            'finish',
        ];
    }

    public function getApiFields()
    {
        $data = [];
        foreach ($this->apiFields() as $field) {
            $data[$field] = $this->$field;
        }
        return $data;
    }


    /**
     * @return \common\models\CardDesign
     */
    public function getDesign()
    {
        return CardDesign::findOne($this->design_id);
    }

    /**
     * Конвертирует сплошной номер карты в номер с пробелами
     *
     * @param string | int $cardNumber
     *
     * @return string
     */
    public static function convertNumberToSpace($cardNumber)
    {
        return substr($cardNumber, 0, 4) . ' ' . substr($cardNumber, 4, 4) . ' ' . substr($cardNumber, 8, 4) . ' ' . substr($cardNumber, 12, 4);
    }

    /**
     * Конвертирует сплошной номер карты в номер с пробелами и возвращает его
     *
     * @return string
     */
    public function getNumbberWithSpace()
    {
        return self::convertNumberToSpace($this->number);
    }

    /**
     * @return \yii\db\Query
     */
    public function getBillingListObject()
    {
        return UserBill::find()->where(['card_id' => $this->id]);
    }

    /**
     * Выбирает 5-6 символы из номера карты (начиная с 1)
     * @return string
     * '01' - BTC
     * '02' - ETH
     */
    public function getCurrencyFromNum()
    {
        return substr($this->number, 4, 2);
    }

    /**
     * @param $fields
     *
     * @return static
     */
    public static function add($fields)
    {
        $i = new static($fields);
        $i->save();
        $i->id = self::getDb()->lastInsertID;

        return $i;
    }
}