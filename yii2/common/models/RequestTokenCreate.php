<?php
/**
 * Created by PhpStorm.
 * User: s.arhangelskiy
 * Date: 12.04.2016
 * Time: 16:13
 */

namespace common\models;


use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\helpers\VarDumper;

/**
 *
 * @property int          id
 * @property int          is_paid_avatars
 * @property string(80)   txid
 * @property string       abi
 * @property string(50)   address
 * @property string(1000) init
 * @property string       contract
 * @property string(50)   owner
 * @property string(50)   name
 * @property string(10)   code
 * @property string(200)  image                 картинка токена
 * @property int          created_at            время добавления заявки
 * @property int          decimals              кол-во знаков после запятой
 * @property int          coin_emission         кол-во эмитируемых токенов
 * @property int          user_id
 * @property int          is_register_in_system зарегистрирован в системе? 0 - нет, 1 - да
 * @property int          step                  шаг заявки до которого дошел клиент
 *
 */
class RequestTokenCreate extends ActiveRecord
{
    public static function tableName()
    {
        return 'requests_token_create';
    }

    public static function add($fields)
    {
        if (!isset($fields['created_at'])) {
            $fields['created_at'] = time();
        }
        $i = new static($fields);
        $i->save();
        $i->id = self::getDb()->lastInsertID;

        return $i;
    }
}