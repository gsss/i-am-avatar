<?php
/**
 * Created by PhpStorm.
 * User: s.arhangelskiy
 * Date: 12.04.2016
 * Time: 16:13
 */

namespace common\models;

use common\interfaceList\BillingInterface;
use common\models\avatar\UserBill;
use common\models\school\AdminLink;
use common\models\school\School;
use common\models\subscribe\Subscribe;
use cs\Application;
use iAvatar777\services\Processing\Currency;
use iAvatar777\services\Processing\Operation;
use iAvatar777\services\Processing\Wallet;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\helpers\VarDumper;

/**
 *
 * @property int        id
 * @property int        program_id
 * @property int        amount
 * @property int        user_id
 * @property int        created_at
 * @property int        billing_id
 * @property int        currency_id dbWallet.currency.id
 *
 */
class SchoolBlagoInput extends ActiveRecord implements BillingInterface
{

    public function behaviors()
    {
        return [
            [
                'class'      => '\yii\behaviors\TimestampBehavior',
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at'],
                ],
                // если вместо метки времени UNIX используется datetime:
                // 'value' => new Expression('NOW()'),
            ],
        ];
    }


    /**
     *
     * @param array $fields
     *
     * @return \common\models\SchoolBlagoInput
     */
    public static function add($fields)
    {
        $item = new static($fields);
        $ret = $item->save();
        $item->id = static::getDb()->lastInsertID;

        return $item;
    }

    function getCompanyId()
    {
        $program = SchoolBlagoProgram::findOne($this->program_id);

        return $program->school_id;
    }

    function successShop()
    {
        if (!Application::isEmpty($this->user_id)) {
            $programm = SchoolBlagoProgram::findOne($this->program_id);
            $wFrom = Wallet::findOne($programm->currency_wallet_id);
            $bill = UserBill::getBillByCurrencyAvatarProcessing($programm->currency_id, $this->user_id);
            $w = Wallet::findOne($bill->address);

            // Перевожу средства
//            $t = $wFrom->move2(
//                $bill->address,
//                Currency::convertAtom($this->amount, $this->currency_id, $programm->currency_id),
//                'Вознаграждение за благодарность #'.$this->id
//            );

            // Эмиттирую стредства
            $o = $w->in(
                Currency::convertAtom($this->amount, $this->currency_id, $programm->currency_id),
                'Вознаграждение за благодарность #' . $this->id
            );

            // Делаю запись
            $t = \common\models\CompanyBlagoInputTransaction::add([
                'proram_id' => $programm->id,
                'oid'       => $o->id,
                'input_id'  => $this->id,
            ]);

            $this->notifyTelegram($t);

//            $u = UserAvatar::findOne($this->user_id);
//
//            \common\services\Subscribe::sendArray(
//                [$u->email],
//                'Вознаграждение за благодарность #'.$this->id,
//                'blago-return',
//                [
//                    'user'          => $u,
//                    'transaction'   => $t,
//                ]
//            );
        }
    }

    /**
     * @param \common\models\CompanyBlagoInputTransaction $t
     */
    public function notifyTelegram($t)
    {
        /** @var \aki\telegram\Telegram $telegram */
        $telegram = \Yii::$app->telegram;

        $o = Operation::findOne($t->oid);
        $w = Wallet::findOne($o->wallet_id);
        $c = Currency::findOne($w->currency_id);
        $text = 'Пришло новое блоготворительное пожертвование. Отправлены токены: ' . Currency::getValueFromAtom($o->amount, $c->id) . ' ' . $c->code;

        $proramm = SchoolBlagoProgram::findOne($t->proram_id);
        $school = School::findOne($proramm->school_id);
        $adminList = AdminLink::find()->where(['school_id' => $school->id])->select(['user_id'])->column();

        foreach ($adminList as $uid) {
            $u = UserAvatar::findOne($uid);
            if (!Application::isEmpty($u->telegram_chat_id)) {
                /** @var \common\models\UserAvatar $uCurrent */
                $telegram->sendMessage([
                    'chat_id' => $u->telegram_chat_id,
                    'text' => $text,
                ]);
            }
        }
    }
}