<?php

namespace common\models;

use app\services\Subscribe;
use cs\services\BitMask;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\db\Query;

/**
 * @property int id
 * @property int created_at
 * @property string name
 * @property string image
 * @property string file
 * @property string content
 */
class Reklama extends ActiveRecord
{
    public static function tableName()
    {
        return 'reklama';
    }

    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at'],
                ],
                // если вместо метки времени UNIX используется datetime:
                // 'value' => new Expression('NOW()'),
            ],
        ];
    }

}