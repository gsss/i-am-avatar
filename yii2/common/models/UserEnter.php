<?php

namespace common\models;

use common\models\piramida\WalletSourceInterface;
use cs\services\Security;
use Yii;
use yii\db\ActiveRecord;

/**
 * @property integer $id
 * @property integer status
 * @property integer school_id
 * @property integer user_id
 * @property integer user1_id
 * @property integer user2_id
 * @property integer created_at
 * @property string  pay_id
 * @property string  is_paid
 * @property string  public_key1
 * @property string  public_key2
 * @property string  signature1
 * @property string  signature2
 * @property string  hash
 */
class UserEnter extends ActiveRecord
{
    const EVENT_BEFORE_PAYD = 'beforePayd';
    const EVENT_AFTER_PAYD = 'afterPayd';

    const STATUS_AFTER_REGISTRATION = 1;
    const STATUS_AFTER_SIGN1 = 2;
    const STATUS_AFTER_SIGN2 = 3;
    const STATUS_AFTER_PAYD = 4;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'user_enter';
    }

    public function successShop(WalletSourceInterface $transaction)
    {
        $this->trigger(self::EVENT_BEFORE_PAYD);

        $this->is_paid = 1;
        $this->save();

        $this->trigger(self::EVENT_AFTER_PAYD);
    }

    public function successClient($message = null)
    {
        $this->is_paid_client = 1;
        $this->save();
    }
}
