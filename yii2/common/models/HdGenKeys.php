<?php

namespace common\models;

use app\services\Subscribe;
use cs\base\DbRecord;
use cs\services\BitMask;
use yii\db\ActiveRecord;
use yii\db\Query;

/**
 * @property int num
 * @property string image_codon
 * @property string image_graph
 * @property string siddhi
 * @property string dar
 * @property string ten
 * @property string name
 *
 * Class HdGenKeys
 * @package common\models
 */
class HdGenKeys extends ActiveRecord
{
    public static function tableName()
    {
        return 'gs_hd_gen_keys';
    }
    
    public function getImage($isScheme = false)
    {
        $image = $this->image;
        if ($image == '') return '';

        return \yii\helpers\Url::to($this->image, $isScheme);
    }
}