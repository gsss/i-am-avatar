<?php

namespace common\models\statistic;

use cs\services\BitMask;
use yii\db\ActiveRecord;
use yii\db\Query;
use app\services\Subscribe;
use yii\helpers\VarDumper;

/**
 * Class ServerRequest
 * @package app\models
 *
 * @property $id int
 * @property $get string
 * @property $post string
 * @property $server string
 * @property $session string
 * @property $headers string
 * @property $log_time double
 * @property $ip string
 * @property $ip_int int
 * @property $user_id int
 * @property $mode int
 * @property $session_id string
 * @property  string uri
 * @property  string row_text
 */
class ServerRequest extends ActiveRecord
{
    const MODE_GET      = 1;
    const MODE_POST     = 2;
    const MODE_OPTIONS  = 3;
    const MODE_HEAD     = 4;

    public static function tableName()
    {
        return 'server_request';
    }

    public static function getDb()
    {
        return \Yii::$app->dbStatistic;
    }

    public static function add()
    {
        $i = new self();
        $i->get = serialize(\Yii::$app->request->get());
        $i->post = serialize(\Yii::$app->request->post());
        $i->server = serialize($_SERVER);
        if (isset($_SESSION)) {
            $i->session = serialize($_SESSION);
        }
        $i->headers = serialize(\Yii::$app->request->headers);
        $i->log_time = microtime(true);
        $i->ip = \Yii::$app->request->userIP;
        $i->ip_int = self::ipToInt(\Yii::$app->request->userIP);
        $i->row_text = serialize(\Yii::$app->request->rawBody);
        $i->uri = $_SERVER['REQUEST_URI'];
        if (\Yii::$app->session->hasSessionId) {
            $i->session_id = \Yii::$app->session->id;
        }
        if (\Yii::$app->request->isPost) {
            $i->mode = self::MODE_POST;
        }
        if (\Yii::$app->request->isGet) {
            $i->mode = self::MODE_GET;
        }
        if (\Yii::$app->request->isHead) {
            $i->mode = self::MODE_HEAD;
        }
        if (\Yii::$app->request->isOptions) {
            $i->mode = self::MODE_OPTIONS;
        }
        if (!\Yii::$app->user->isGuest) {
            $i->user_id = \Yii::$app->user->id;
        }
        $i->save();
        $i->id = self::getDb()->lastInsertID;

        return $i;
    }

    /**
     * Преобразовывает IP в INT
     *
     * @param $ip
     *
     * @return int
     */
    private static function ipToInt($ip)
    {
        if ($ip == '') return null;
        if ($ip == '-') return null;
        $list = explode('.', $ip);
        $sum = 256*256*256 * $list[0] + 256*256 * $list[0] + 256 * $list[0] + $list[3];

        return $sum;
    }

}