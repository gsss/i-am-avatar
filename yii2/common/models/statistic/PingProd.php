<?php

namespace common\models\statistic;

use Yii;

/**
 */
class PingProd extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'statistic_ping_server_prod';
    }

    public function rules()
    {
        return [
            [['time', 'status'], 'required'],
            [['time', 'status'], 'integer'],
        ];
    }

    public static function add($status)
    {
        $item = new self([
            'time' => time(),
            'status' => $status,
        ]);
        $item->save();
        $item->id = $item::getDb()->lastInsertID;

        return $item;
    }

}