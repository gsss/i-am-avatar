<?php

namespace common\models\statistic;

use app\models\Transaction;
use common\models\User;
use common\extensions\XSSActiveRecord;
use cs\services\VarDumper;
use Yii;
use app\models\CrmLog;
use yii\db\ActiveRecord;

/**
 * @property integer $time  момент времени
 * @property integer $timer  время исполнения страницы
 * @property integer $controller_id  контроллер
 * @property integer $action_id  действие
 */
class StatisticRoute extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'timer';
    }
    
    public function rules()
    {
        return [
            [['time', 'controller_id', 'action_id', 'timer', ], 'required'],
            [['time', 'timer', ], 'double'],
            [['controller_id', 'action_id', ], 'string', 'max' => 64],
        ];
    }

    /**
     * @param array $fields
     * @return \common\models\statistic\StatisticRoute
     */
    public static function add($fields)
    {
        $i = new static($fields);
        $i->save();

        return $i;
    }

    public static function getDb()
    {
        return Yii::$app->dbStatistic;
    }
}
