<?php

namespace common\models\statistic;

use Yii;

/**
 * This is the model class for table "statistic_db".
 *
 * @property integer $id
 */
class StatisticDb extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'statistic_db';
    }

    public static function getDb()
    {
        return Yii::$app->dbStatistic;
    }

    public function rules()
    {
        return [
            [['time'], 'required'],
            [
                [
                    'time',
                    'main_master_select',
                    'main_master_insert',
                    'main_master_update',
                    'main_master_delete',
                    'main_slave_select',
                    'statistic_select',
                    'statistic_insert',
                    'statistic_update',
                    'statistic_delete',
                ],
                'integer'
            ],
        ];
    }

}