<?php

namespace common\models\statistic;

use app\models\Transaction;
use common\models\User;
use common\extensions\XSSActiveRecord;
use Yii;
use app\models\CrmLog;
use yii\db\ActiveRecord;

/**
 * @property integer $time  момент времени 
 * @property integer $value кол-во пользователей онлайн
 * @property integer $requests_per_second кол-во запросов в сек
 */
class StatisticOnline extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'statistic_online';
    }
    
    public function rules()
    {
        return [
            [['time'], 'required'],
            [['online', 'time'], 'integer'],
            [['request_per_second'], 'double'],
        ];
    }

    public static function getDb()
    {
        return Yii::$app->dbStatistic;
    }

}
