<?php

namespace common\models\statistic;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * @property int id
 * @property int status
 * @property int server_id
 * @property int time
 * @property double disk
 * @property double memory
 * @property double processor
 */
class PingStatus extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'statistic_ping';
    }

    public static function getDb()
    {
        return Yii::$app->dbStatistic;
    }

    /**
     * @param int   $server
     * @param int   $status
     * @param array $fields
     * - disk - double
     * - memory - double
     * - processor - double
     *
     * @return PingStatus
     */
    public static function add($server, $status, $fields = [])
    {
        $fields = ArrayHelper::merge($fields, [
            'time'      => time(),
            'status'    => $status,
            'server_id' => $server,
        ]);
        $item = new self($fields);
        $item->save();
        $item->id = $item::getDb()->lastInsertID;

        return $item;
    }

}