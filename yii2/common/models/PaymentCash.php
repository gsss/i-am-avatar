<?php

namespace common\models;

use Yii;
use yii\base\Exception;
use yii\db\ActiveRecord;

/**
 * @property integer    id
 * @property integer    billing_id
 * @property integer    currency_id
 * @property integer    amount
 * @property integer    created_at
 * @property string     data
 */
class PaymentCash extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'payments_cash';
    }

    /**
     * @param mixed $condition
     * @return \common\models\PaymentCash
     * @throws Exception
     */
    public static function findOne($condition)
    {
        $item = parent::findOne($condition);
        if (is_null($item)) {
            throw new Exception('Не найден платеж');
        }
        return $item;
    }
}
