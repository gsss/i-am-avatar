<?php

namespace common\models;

use common\components\Card;
use common\models\avatar\QrCode;
use common\models\avatar\UserBill;
use common\models\avatar\UserBillMerchant;
use common\models\SendLetter;
use cs\Application;
use cs\services\Str;
use iAvatar777\services\FormAjax\ActiveRecord;
use Yii;
use yii\base\Model;
use yii\db\Query;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\VarDumper;

/**
 * @param int       $id
 * @param int       $koop_id school_koop.id
 * @param string    $name
 */
class SchoolKoopAim extends \yii\db\ActiveRecord
{

    public function attributeLabels()
    {
        return [
            'name' => 'Наименование',
        ];
    }


    public static function add($fields)
    {
        $i = new static($fields);
        $i->save();
        $i->id = self::getDb()->lastInsertID;

        return $i;
    }
}
