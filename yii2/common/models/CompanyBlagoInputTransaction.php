<?php
/**
 * Created by PhpStorm.
 * User: god
 * Date: 28.02.2017
 * Time: 23:49
 */

namespace common\models;

use common\models\avatar\UserBill;
use common\models\piramida\Billing;
use common\models\statistic\StatisticDb;
use yii\db\ActiveRecord;
use yii\helpers\VarDumper;

/**
 *
 * @property integer id
 * @property integer proram_id  school_blago_program.id
 * @property integer created_at
 * @property integer oid        operation.id
 * @property integer input_id   school_blago_input.id
 *
 * @package common\models
 */
class CompanyBlagoInputTransaction extends ActiveRecord
{
    public function behaviors()
    {
        return [
            [
                'class'      => '\yii\behaviors\TimestampBehavior',
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at'],
                ],
                // если вместо метки времени UNIX используется datetime:
                // 'value' => new Expression('NOW()'),
            ],
        ];
    }

    public static function add($fields)
    {
        $i = new self($fields);
        $ret = $i->save();
        if (!$ret) throw new \Exception('Cant save '.VarDumper::dumpAsString($fields));
        $i->id = self::getDb()->lastInsertID;

        return $i;
    }
}