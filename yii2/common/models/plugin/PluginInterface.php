<?php
/**
 * Created by PhpStorm.
 * User: god
 * Date: 28.02.2017
 * Time: 23:49
 */

namespace common\models;

use yii\base\Model;
use yii\db\ActiveRecord;

/**
 */
interface PluginInterface
{
    /**
     * @param array $fields
     *
     * @return bool
     */
    public function export($fields);
}