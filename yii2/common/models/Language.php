<?php
/**
 * Created by PhpStorm.
 * User: s.arhangelskiy
 * Date: 12.04.2016
 * Time: 16:13
 */

namespace common\models;

use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;
use yii\helpers\VarDumper;

/**
 * Содержит информацию о языке
 *
 * @property int    id
 * @property string name
 * @property string code
 * @property int    status
 *
 *
 * Class Language
 * @package app\services
 */
class Language extends ActiveRecord
{
    const STATUS_EDIT = 1; // на редакции
    const STATUS_DONE = 2; // в Prodaction

    public static function tableName()
    {
        return 'languages';
    }

    public function rules()
    {
        return [
            [['name', 'code', 'status'], 'required'],
            [['name'], 'string', 'max' => 255],
            [['code'], 'string', 'max' => 16],
            [['status'], 'integer'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'name'   => 'Название',
            'code'   => 'Код',
            'status' => 'Режим отображения',
        ];
    }

    public function attributeHints()
    {
        return [
            'code' => 'Двухсимвольный код на английском по стандарту ISO 639-1',
        ];
    }
}