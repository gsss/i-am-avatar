<?php

namespace common\models;

use app\services\Subscribe;
use cs\services\BitMask;
use yii\db\ActiveRecord;
use yii\db\Query;


/**
 * Страны для Дизайна Человека
 *
 * @property int id
 * @property string name
 * @property string title
 *
 * Class HD
 * @package common\models
 */
class HD extends ActiveRecord
{
    public static function tableName()
    {
        return 'gs_hd';
    }
}