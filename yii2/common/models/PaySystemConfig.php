<?php

namespace common\models;

use common\models\PaySystem;
use cs\base\DbRecord;
use cs\services\BitMask;
use cs\services\VarDumper;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\db\Query;
use yii\helpers\Url;


/**
 * Конфиг платежной системы для магазина
 *
 * @property integer id
 * @property integer school_id
 * @property integer created_at
 * @property integer paysystem_id
 * @property string  config
 * @property string  name
 */
class PaySystemConfig extends ActiveRecord
{
    /** @var  \common\models\PaySystem */
    private $_paySystem;

    public static function tableName()
    {
        return 'paysystems_config';
    }

    public function behaviors()
    {
        return [
            [
                'class' => '\yii\behaviors\TimestampBehavior',
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at'],
                ],
                // если вместо метки времени UNIX используется datetime:
                // 'value' => new Expression('NOW()'),
            ],
        ];
    }

    public function rules()
    {
        return [
            ['school_id', 'integer'],
            ['paysystem_id', 'integer'],
            ['created_at', 'integer'],
            ['name', 'string'],
            ['config', 'string'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'config' => 'Параметры платежной системы',
        ];
    }

    /**
     * @return \common\models\PaySystem
     */
    public function getPaySystem()
    {
        if (is_null($this->_paySystem)) {
            $this->_paySystem = $this->_getPayment();
        }

        return $this->_paySystem;
    }

    /**
     * @return \common\models\PaySystem
     */
    public function _getPayment()
    {
        return PaySystem::findOne($this->paysystem_id);
    }


    /**
     * @return string
     */
    public function getClassName()
    {
        return $this->getPaySystem()->getClassName();
    }

    /**
     * @return \common\models\piramida\WalletSourceInterface
     */
    public function getClass()
    {
        return $this->getPaySystem()->getClass([
            'config_id'   => $this->id,
            'config'      => $this->config,
            'source_id'   => $this->paysystem_id,
            'paySystem'   => $this->getPaySystem(),
        ]);
    }

}