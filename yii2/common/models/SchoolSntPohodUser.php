<?php
/**
 * Created by PhpStorm.
 * User: s.arhangelskiy
 * Date: 12.04.2016
 * Time: 16:13
 */

namespace common\models;

use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\helpers\VarDumper;

/**
 *
 * @property int            id
 * @property int            school_id
 * @property int            pohod_id
 * @property int            created_at
 * @property int            user_id
 * @property int            start
 * @property int            end
 * @property int            tarif
 *
 */
class SchoolSntPohodUser extends ActiveRecord
{

    public function behaviors()
    {
        return [
            [
                'class'      => '\yii\behaviors\TimestampBehavior',
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at'],
                ],
                // если вместо метки времени UNIX используется datetime:
                // 'value' => new Expression('NOW()'),
            ],
        ];
    }


    /**
     *
     * @param array $fields
     *
     * @return \common\models\SchoolMoneyIncome
     */
    public static function add($fields)
    {
        $item = new static($fields);
        $ret = $item->save();
        $item->id = static::getDb()->lastInsertID;

        return $item;
    }

}