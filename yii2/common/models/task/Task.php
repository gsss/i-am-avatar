<?php
/**
 * Created by PhpStorm.
 * User: god
 * Date: 28.02.2017
 * Time: 23:49
 */

namespace common\models\task;

use common\models\avatar\UserBill;
use common\models\piramida\Billing;
use common\models\piramida\Currency;
use common\models\school\School;
use common\models\statistic\StatisticDb;
use common\models\UserAvatar;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;

/**
 * @property int    id
 * @property int    school_id
 * @property int    category_id
 * @property int    parent_id
 * @property int    user_id
 * @property int    executer_id
 * @property int    created_at
 * @property int    updated_at
 * @property int    status
 * @property int    price
 * @property int    currency_id             dbWallet.currency.id
 * @property int    is_hide
 * @property int    sort_index
 * @property int    operation_sub
 * @property int    operation_add
 * @property int    wallet_type             1 - Брать из кошелька школы, 2 - Брать с кошелька постановщика, 3 - в поле `wallet_type3_user_id` указан пользователь откуда будет списываться бюджет
 * @property int    wallet_type3_user_id    Идентификатор пользователя откуда будет происходить списание бюджета, если `wallet_type` = 3
 * @property int    last_comment_time       Мгновение когда был добавлен новый коментарий
 * @property string name
 * @property string content
 */
class Task extends ActiveRecord
{
    // Новая задача
    const EVENT_TASK_NEW = 'taskNew';

    // Пользователь взял задачу
    const EVENT_TASK_START = 'taskStart';

    // Исполнитель выполнил задачу
    const EVENT_TASK_FINISH = 'taskFinish';

    // PM принял задачу
    const EVENT_TASK_ACCEPT = 'taskAccept';

    // PM отклонил задачу
    const EVENT_TASK_REJECT = 'taskReject';

    // Исполнитель снял с себя ответственность за выполнение задачи
    const EVENT_TASK_UNLINK = 'taskUnlink';

    // Задачу стала скрытой
    const EVENT_TASK_HIDE = 'taskHide';

    // Изменилась награда за задачу
    const EVENT_TASK_PRICE_CHANGE = 'taskPriceChange';

    // Добавлен новый коментарий в задачу
    const EVENT_TASK_COMMENT = 'taskComment';

    const WALLET_TYPE_SCHOOL = 1;
    const WALLET_TYPE_AUTHOR = 2;
    const WALLET_TYPE_USER = 3;

    /** @var Currency */
    private $currency;

    /** @var  School */
    private $school;

    /** @var UserAvatar */
    private $user;

    /** @var UserAvatar */
    private $executer;

    /** @var \common\models\task\Category */
    private $category;

    public static function tableName()
    {
        return 'school_task';
    }


    /**
     * @return \common\models\UserAvatar
     * @throws
     */
    public function getUser()
    {
        if (empty($this->user)) {
            $this->user = UserAvatar::findOne($this->user_id);
        }

        return $this->user;
    }

    /**
     * @return \common\models\school\School
     * @throws
     */
    public function getSchool()
    {
        if (empty($this->school)) {
            $this->school = School::findOne($this->school_id);
        }

        return $this->school;
    }

    /**
     * @return \common\models\UserAvatar
     * @throws
     */
    public function getExecuter()
    {
        if (empty($this->executer)) {
            $this->executer = UserAvatar::findOne($this->executer_id);
        }

        return $this->executer;
    }

    public function rules()
    {
        return [
            ['school_id', 'integer'],
            ['is_hide', 'integer'],
            ['user_id', 'integer'],
            ['executer_id', 'integer'],
            ['created_at', 'integer'],
            ['updated_at', 'integer'],
            ['status', 'integer'],
            ['price', 'integer'],
            ['currency_id', 'integer'],
            ['category_id', 'integer'],
            ['last_comment_time', 'integer'],
            ['name', 'string'],
            ['content', 'string'],
        ];
    }

    public function behaviors()
    {
        return [
            [
                'class' => '\yii\behaviors\TimestampBehavior',
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at', 'last_comment_time'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
                ],
                // если вместо метки времени UNIX используется datetime:
                // 'value' => new Expression('NOW()'),
            ],
        ];
    }

    /**
     * @return Currency
     */
    public function getCurrency()
    {
        if (empty($this->currency)) {
            $this->currency = Currency::findOne($this->currency_id);
        }

        return $this->currency;
    }

    public function getCategory()
    {
        if (empty($this->category)) {
            $this->category = Category::findOne($this->category_id);
        }

        return $this->category;
    }

    public function getPriceFormatted()
    {
        return \Yii::$app->formatter->asDecimal(
            $this->getCurrency()->convert($this->price)
        );
    }

    /**
     * @param $fields
     *
     * @return static
     */
    public static function add($fields)
    {
        $i = new static($fields);
        $i->save();
        $i->id = self::getDb()->lastInsertID;

        return $i;
    }
}