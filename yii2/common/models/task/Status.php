<?php
/**
 * Created by PhpStorm.
 * User: god
 * Date: 28.02.2017
 * Time: 23:49
 */

namespace common\models\task;

use common\models\avatar\UserBill;
use common\models\piramida\Billing;
use common\models\statistic\StatisticDb;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;

/**
 * @property int id
 * @property int school_id
 * @property int sort_index
 * @property int type_id
 * @property string name
 */
class Status extends ActiveRecord
{
    const STATUS_CREATE = null;
    const STATUS_ATTACH = 1;
    const STATUS_DONE = 2;
    const STATUS_ACCEPT = 3;

    public static function tableName()
    {
        return 'school_task_status';
    }


    /**
     * @param $fields
     *
     * @return static
     */
    public static function add($fields)
    {
        $i = new static($fields);
        $i->save();
        $i->id = self::getDb()->lastInsertID;

        return $i;
    }
}