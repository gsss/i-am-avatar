<?php
/**
 * Created by PhpStorm.
 * User: god
 * Date: 28.02.2017
 * Time: 23:49
 */

namespace common\models\task;

use common\models\avatar\UserBill;
use common\models\piramida\Billing;
use common\models\piramida\Currency;
use common\models\school\School;
use common\models\statistic\StatisticDb;
use common\models\UserAvatar;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;

/**
 * @property int    id
 * @property int    task_id
 * @property int    start
 * @property int    finish
 * @property int    is_finish

 */
class Session extends ActiveRecord
{


    public static function tableName()
    {
        return 'school_task_session';
    }

    /**
     * @param $fields
     *
     * @return static
     */
    public static function add($fields)
    {
        $i = new static($fields);
        $i->save();
        $i->id = self::getDb()->lastInsertID;

        return $i;
    }
}