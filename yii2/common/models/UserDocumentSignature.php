<?php
/**
 * Created by PhpStorm.
 * User: Бог-Творец
 * Date: 06.01.2017
 * Time: 17:14
 */

namespace common\models;


use yii\base\Exception;

use yii\db\ActiveRecord;
use yii\helpers\VarDumper;

/**
 * Документ пользователя
 *
 * @property int        id
 * @property int        document_id
 * @property int        created_at
 * @property int        user_id
 * @property string(70) member
 * @property string(70) txid
 * @property string(70) signature
 *
 * Class UserDocumentSignature
 * @package common\models
 */
class UserDocumentSignature extends ActiveRecord
{
    public static function tableName()
    {
        return 'user_documents_signatures';
    }

    /**
     *
     * @param array $fields
     *
     * @return \common\models\UserDocumentSignature
     */
    public static function add($fields)
    {
        if (!isset($fields['created_at'])) {
            $fields['created_at'] = time();
        }
        $item = new static($fields);
        $ret = $item->save();
        if (!$ret) {
            throw new Exception('Не удалось сохранить \common\models\UserDocumentSignature::add ' . VarDumper::dumpAsString($item->errors));
        }
        $item->id = static::getDb()->lastInsertID;

        return $item;
    }

    /**
     * Выдает документ
     *
     * @return \common\models\UserDocument
     */
    public function getDocument()
    {
        return UserDocument::findOne($this->document_id);
    }
}