<?php

namespace common\models;

use Yii;
use common\extensions\XSSActiveRecord;
use yii\base\Exception;
use yii\db\ActiveRecord;

/**
 * @property integer    id
 * @property integer    billing_id
 * @property float      value
 * @property string     card
 * @property string     action
 * @property string     transaction
 * @property string     from
 */
class PaymentSber extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'payments_sber';
    }

    /**
     * @param mixed $condition
     * @return \common\models\PaymentBitCoin
     * @throws Exception
     */
    public static function findOne($condition)
    {
        $item = parent::findOne($condition);
        if (is_null($item)) {
            throw new Exception('Не найден платеж');
        }
        return $item;
    }


    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'          => 'ID',
            'billing_id'  => 'Счет',
            'value'       => 'Число монет',
            'action'      => 'ссылка на платеж в мерчанте',
            'transaction' => 'Трназакция',
            'from'        => 'от кого',
        ];
    }
}
