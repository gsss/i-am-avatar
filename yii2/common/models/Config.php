<?php

namespace common\models;

use Yii;
use common\extensions\XSSActiveRecord;
use yii\db\ActiveRecord;

/**
 * @property integer $id
 * @property string $name
 * @property string $value
 */
class Config extends ActiveRecord
{
    public static $cachePrefix = 'config_';

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'config';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['name'], 'string', 'max' => 100],
            [['name', 'value'], 'safe']
        ];
    }

    /**
     *
     * @param mixed $condition
     * @return static
     */
    public static function findOne($condition)
    {
        $item = parent::findOne($condition);
        if (is_null($item)) {
            $item = new static([
                'name'  => $condition['name'],
                'value' => ''
            ]);
        }

        return $item;
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'    => 'ID',
            'name'  => 'Name',
            'value' => 'Value',
        ];
    }

    /**
     * Выдает закешированное значение
     *
     * @param $name
     * @return null|string
     */
    public static function get($name)
    {
        $value = Yii::$app->cache->get(self::$cachePrefix . $name);
        if ($value === false || is_null($value)) {
            $value = Config::find()->where(['name' => $name])->select('value')->scalar();
            if ($value === false) {
                Yii::warning('Нет значения ' . $name, 'avatar\\cache');
            }
            Yii::$app->cache->set(self::$cachePrefix . $name, $value);
        }

        return $value;
    }

    public static function set($name, $value)
    {
        $property = Config::findOne(['name' => $name]);
        $property->value = $value;
        $property->save();
        Yii::$app->cache->set(self::$cachePrefix . $name, $value);
    }

    public static function _get($name)
    {
        $value = Config::find()->where(['name' => self::$cachePrefix . $name])->select('value')->scalar();
        if ($value === false) {
            $value = null;
            Yii::$app->cache->set(self::$cachePrefix . $name, $value);
        }

        return $value;
    }
}
