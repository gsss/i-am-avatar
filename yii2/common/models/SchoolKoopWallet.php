<?php

namespace common\models;

use common\components\Card;
use common\models\avatar\QrCode;
use common\models\avatar\UserBill;
use common\models\avatar\UserBillMerchant;
use common\models\SendLetter;
use cs\Application;
use cs\services\Str;
use iAvatar777\services\FormAjax\ActiveRecord;
use Yii;
use yii\base\Model;
use yii\db\Query;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\VarDumper;

/**
 * @param int       $id
 * @param int       $school_id school.id
 * @param int       $currency_id dbWallet.currency.id
 * @param int       $wallet_id dbWallet.wallet.id
 * @param int       $type_id
 * @param string    $name
 */
class SchoolKoopWallet extends \yii\db\ActiveRecord
{
    const TYPE_PF = 1;
    const TYPE_ROHD = 2;
    const TYPE_SF = 3;
    const TYPE_RF = 4;

    public function attributeLabels()
    {
        return [
            'name' => 'Наименование',
        ];
    }


    public static function add($fields)
    {
        $i = new static($fields);
        $i->save();
        $i->id = self::getDb()->lastInsertID;

        return $i;
    }
}
