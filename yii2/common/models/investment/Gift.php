<?php

namespace common\models\investment;

use cs\services\VarDumper;
use yii\base\Exception;
use yii\base\InvalidConfigException;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;

/**
 * @property int    id               Идентификатор
 * @property int    project_id       Идентификатор проекта
 * @property string name             Наименование подарка
 * @property string content          Текст описания подпрка
 * @property string image            Картинка для проекта. 300*300 обрезается
 * @property int    currency         Идентификатор валюты по таблице currency
 * @property double sum              Сумма вложений
 *
 */
class Gift extends ActiveRecord
{
    public static function tableName()
    {
        return 'projects_gifts';
    }

    public function getProject()
    {
        return Project::findOne($this->project_id);
    }
}