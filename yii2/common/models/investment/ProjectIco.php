<?php

namespace common\models\investment;

use common\models\avatar\Currency;
use common\models\avatar\UserBill;
use yii\db\ActiveRecord;

/**
 * @property int    id                  Идентификатор projects.id
 * @property int    billing_id          Идентификатор счета на котором лежат все монеты
 * @property int    currency_id         Идентификатор валюты по таблице currency
 * @property float  remained_tokens     Сколько осталось токенов
 * @property float  all_tokens          Сколько токенов выставленное на продажу изначально
 * @property float  kurs                Сколько токенов выставленное на продажу
 * @property int    kurs_currency_id    Сколько токенов выставленное на продажу
 *
 */
class ProjectIco extends ActiveRecord
{
    /** @var  \common\models\investment\Project объект проекта */
    public $_project;

    public static function tableName()
    {
        return 'projects_ico';
    }

    public function rules()
    {
        return [
            [
                [
                    'id',
                    'billing_id',
                    'currency_id',
                    'remained_tokens',
                    'all_tokens',
                    'kurs',
                    'kurs_currency_id',
                ],
                'safe'
            ],
        ];
    }

    /**
     * @return \common\models\investment\Project
     */
    public function getProject()
    {
        if (is_null($this->_project)) {
            $this->_project = $this->_getProject();
        }

        return $this->_project;
    }

    /**
     * @return \common\models\investment\Project
     */
    public function _getProject()
    {
        return Project::findOne($this->id);
    }

    /**
     * Выдает кошелек токена где лежат монеты на раздачу
     *
     * @return UserBill
     */
    public function getBilling()
    {
        return UserBill::findOne($this->billing_id);
    }

    /**
     * @return \common\models\avatar\Currency
     */
    public function getCurrency()
    {
        return Currency::findOne($this->currency_id);
    }

    /**
     * Возвращает все подарки
     *
     * @param null | array | string $sort сортировка для запроса
     *
     * @return \common\models\investment\Gift[]
     */
    public function getGifts($sort = null)
    {
        $query = $this->getGiftsQuery();
        if (is_null($sort)) {
            $query->orderBy($sort);
        }

        return $query->all();
    }

    /**
     * @return \yii\db\Query
     */
    public function getGiftsQuery()
    {
        return Gift::find()->where(['project_id' => $this->id]);
    }

}