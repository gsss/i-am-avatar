<?php
/**
 * Created by PhpStorm.
 * User: s.arhangelskiy
 * Date: 12.04.2016
 * Time: 16:13
 */

namespace common\models\investment;

use common\models\BillingMain;
use common\models\UserAvatar;

use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;
use yii\helpers\VarDumper;

/**
 * Содержит информацию о заявке на покупку токенов
 *
 * @property int        id                      идентификатор заявки
 * @property int        ico_id                  идентификатор ICO
 * @property int        paysystem_config_id     идентификатор настройки платежной систему через которую человек выбрал платить
 * @property int        billing_id              идентификатор счета который прикреплен к заказу
 * @property string(70) address                 адрес эфира для доставки токенов
 * @property string(70) email                   адрес почты клиента если он не авторизован
 * @property int        created_at              время создания заявки
 * @property int        is_paid                 флаг оплаты заказа, 0 - не оплачено, 1 - оплачено. 0 - по умолчанию
 * @property int        user_id                 идентификатор пользователя если он авторизован в системе
 * @property int        is_hide                 скрыт заказ? 0 - нет, 1 - скрыт, не актуальный
 * @property float      tokens                  кол-во покупаемых токенов
 * @property int        is_answer_from_shop
 * @property int        is_answer_from_client
 * @property int        last_message_time
 * @property int        status
 * @property int        is_verify
 * @property int        is_verify_result
 * @property int        parent_id               если покупал человек по партнерской ссылке то здесь указывается от кого.
 *
 *
 * Class IcoRequest
 * @package app\services
 */
class IcoRequest extends ActiveRecord
{

    const DIRECTION_TO_CLIENT = 1;
    const DIRECTION_TO_SHOP = 2;

    const STATUS_CREATED = 1; // Заказ создан
    const STATUS_USER_CONFIRM = 2; // Клиент выставил транзакцию
    const STATUS_SHOP_CONFIRM = 3; // Магазин подтвердил
    const STATUS_SHOP_SEND = 4; // Магазин выслал токены
    const STATUS_COMPLITED = 5; // Заказ Выполнен
    const STATUS_REJECTED = -1; // Заказ отменен

    public static $statusList = [
        self::STATUS_CREATED               => [
            'client'   => 'заказ сформирован',
            'shop'     => 'заказ сформирован',
            'timeLine' => [
                'icon'  => 'glyphicon-ok',
                'color' => 'default',
            ],
        ],
        self::STATUS_USER_CONFIRM               => [
            'client'   => 'клиент указал транзакцию',
            'shop'     => 'клиент указал транзакцию',
            'timeLine' => [
                'icon'  => 'glyphicon-plane',
                'color' => 'warning',
            ],
        ],
        self::STATUS_SHOP_CONFIRM               => [
            'client'   => 'деньги подтверждены магазином',
            'shop'     => 'деньги подтверждены магазином',
            'timeLine' => [
                'icon'  => 'glyphicon-plane',
                'color' => 'success',
            ],
        ],
        self::STATUS_REJECTED               => [
            'client'   => 'заказ отменен',
            'shop'     => 'заказ отменен',
            'timeLine' => [
                'icon'  => 'glyphicon-close',
                'color' => 'danger',
            ],
        ],
        self::STATUS_SHOP_SEND               => [
            'client'   => 'токены были высланы',
            'shop'     => 'токены были высланы',
            'timeLine' => [
                'icon'  => 'glyphicon-plane',
                'color' => 'warning',
            ],
        ],
        self::STATUS_COMPLITED               => [
            'client'   => 'заказ исполнен',
            'shop'     => 'заказ исполнен',
            'timeLine' => [
                'icon'  => 'glyphicon-thumbs-up',
                'color' => 'success',
            ],
        ],
    ];


    /** @var  \common\models\investment\ProjectIco */
    private $ico;

    public static function tableName()
    {
        return 'ico_request';
    }

    /**
     * @param array $fields
     *
     * @return \common\models\investment\IcoRequest
     */
    public static function add($fields)
    {
        if (!isset($fields['created_at'])) {
            $fields['created_at'] = time();
        }
        if (!isset($fields['is_paid'])) {
            $fields['is_paid'] = 0;
        }
        $i = new static($fields);
        $i->save();
        $i->id = $i::getDb()->lastInsertID;

        return $i;
    }

    /**
     * Отправляет токены по адресу с кошелька где лежат все токены
     *
     * @param string $address  адрес куда послать токены
     * @param string $password пароль от кошелька который содержит токены
     *
     * @return \common\models\investment\IcoRequestSuccess
     */
    public function sendTokens($address, $password)
    {
        $ico = $this->getIco();
        $txid = $ico->getBilling()->getWalletToken()->pay([$address => $this->tokens], $password);
        return $this->addSuccessObject(['txid' => $txid]);
    }

    /**
     * @param array $fields
     * @return \common\models\investment\IcoRequestSuccess
     */
    private function addSuccessObject($fields)
    {
        $fields['id'] = $this->id;
        return IcoRequestSuccess::add($fields);
    }

    /**
     * Возвращает свойства проекта ICO
     *
     * @return \common\models\investment\ProjectIco
     * @throws \yii\base\Exception
     */
    public function getIco()
    {
        if (is_null($this->ico)) {
            $this->ico = $this->_getIco();
        }

        return $this->ico;
    }

    /**
     * Возвращает свойства проекта ICO
     *
     * @return \common\models\investment\ProjectIco
     */
    private function _getIco()
    {
        return ProjectIco::findOne($this->ico_id);
    }

    /**
     * Ставит флаг что клиент оплатил заказ
     *
     */
    public function success()
    {
        // подтверждаю `billing`
        $this->getBilling()->success();

        // ставлю флаг
        $this->is_paid = 1;
        $this->save();
    }

    /**
     * @return \common\models\UserAvatar
     */
    public function getUser()
    {
        return UserAvatar::findOne($this->user_id);
    }

    /**
     * @return \common\models\BillingMain
     */
    public function getBilling()
    {
        return BillingMain::findOne($this->billing_id);
    }

    /**
     * Функция которая возвращает класс совместимый с интерфейсом \common\models\PaymentI для тог чтобы можно было указать транзакцию
     * Это класс для таблицы payment{paymentName}
     *
     * @param int $billingId идентификатор заказа
     *
     * @return \common\models\PaymentI
     *
     * @throws
     */
    function getPayment()
    {
        $billing = $this->getBilling();
        $paySystem = $billing->getPaySystem();
        $className = $paySystem->class_name;
        $fullClassName = '\common\models\Payment' . $className;

        $object = $fullClassName::findOne(['billing_id' => $this->id]);
        if (is_null($object)) throw new Exception('Не найден платеж $billingId='.$this->id);

        return $object;
    }
    /**
     * @return \common\models\investment\Project
     */
    public function getProject()
    {
        return Project::findOne($this->ico_id);
    }

    /**
     * Высылает информацию о транзакции отправки токенов
     *
     * @return \common\models\investment\IcoRequestSuccess
     */
    public function getTransactionInfo()
    {
        return IcoRequestSuccess::findOne($this->id);
    }

    /**
     * ! Не использовать, для класса Billing это не типичная функция а скорее исключительная, поэтому и неуместна
     *
     * Функция которая возвращает класс совместимый с интерфейсом \common\models\PaymentI для тог чтобы можно было указать транзакцию
     * Это класс для таблицы payment{paymentName}
     *
     * @return \common\models\PaymentI
     *
     * @throws
     */
    function getPaymentI()
    {
        $billing = $this->getBilling();
        $paySystem = $billing->getPaySystem();
        $className = $paySystem->class_name;
        $fullClassName = '\common\models\Payment' . $className;

        $object = $fullClassName::findOne(['billing_id' => $billing->id]);
        if (is_null($object)) throw new \Exception('Не найден платеж $billingId=' . $billing->id . ' класса ' . '\common\models\Payment' . $className);

        return $object;
    }

    /**
     * Возвращает строку таблицы ico_request_success
     *
     * @return \common\models\investment\IcoRequestSuccess
     *
     * @throws
     */
    function getPaymentSuccess()
    {
        $successObject = IcoRequestSuccess::findOne($this->id);
        if (is_null($successObject)) {
            throw new \Exception('Не найден $successObject $requestId=' . $this->id);
        }

        return $successObject;
    }

    /**
     * @param string $txid
     * @return IcoRequestSuccess
     */
    public function setSuccessTx($txid)
    {
        return IcoRequestSuccess::add(['txid' => $txid, 'id' => $this->id]);
    }
    /**
     * Добавить статус к клиенту
     *
     * @param int | array $status статус self::STATUS_* или массив со статусом и сообщением
     *
     * @return \common\models\investment\IcoRequestMessage
     */
    public function addStatusToClient($status)
    {
        if (!is_array($status)) {
            $fields = [
                'status' => $status,
            ];
        } else {
            $fields = $status;
        }

        return $this->addMessageItem($fields, self::DIRECTION_TO_CLIENT);
    }

    /**
     * Добавить статус в магазин
     *
     * @param int | array $status статус self::STATUS_* или массив со статусом и сообщением
     *
     * @return \common\models\investment\IcoRequestMessage
     */
    public function addStatusToShop($status)
    {
        if (!is_array($status)) {
            $fields = [
                'status' => $status,
            ];
        } else {
            $fields = $status;
        }

        return $this->addMessageItem($fields, self::DIRECTION_TO_SHOP);
    }

    /**
     * Добавить сообщение
     *
     * @param string $message сообщение
     * @param int $direction направление сообщения self::DIRECTION_*
     *
     * @return \common\models\investment\IcoRequestMessage
     */
    public function addMessage($message, $direction)
    {
        return $this->addMessageItem([
            'message' => $message,
        ], $direction);
    }

    /**
     * Добавить сообщение для клиента
     *
     * @param string $message сообщение
     *
     * @return \common\models\investment\IcoRequestMessage
     */
    public function addMessageToClient($message)
    {
        return $this->addMessage($message, self::DIRECTION_TO_CLIENT);
    }

    /**
     * Добавить сообщение для клиента
     *
     * @param string $message сообщение
     *
     * @return \common\models\investment\IcoRequestMessage
     */
    public function addMessageToShop($message)
    {
        return $this->addMessage($message, self::DIRECTION_TO_SHOP);
    }

    /**
     * Добавить сообщение или статус
     *
     * @param array $fields поля для сообщения
     * @param int $direction направление сообщения self::DIRECTION_*
     *
     * @return \common\models\investment\IcoRequestMessage
     */
    public function addMessageItem($fields, $direction)
    {
        $fieldsRequest = [
            'is_answer_from_shop'   => ($direction == self::DIRECTION_TO_CLIENT) ? 1 : 0,
            'is_answer_from_client' => ($direction == self::DIRECTION_TO_CLIENT) ? 0 : 1,
            'last_message_time'     => time(),
        ];
        if (isset($fields['status'])) {
            $fieldsRequest['status'] = $fields['status'];
        }
        foreach ($fieldsRequest as $k => $v) {
            $this->$k = $v;
        }
        $this->save();

        return IcoRequestMessage::add(ArrayHelper::merge($fields, [
            'request_id' => $this->id,
            'direction'  => $direction,
            'datetime'   => time(),
        ]));
    }


}