<?php
/**
 * Created by PhpStorm.
 * User: s.arhangelskiy
 * Date: 12.04.2016
 * Time: 16:13
 */

namespace common\models\investment;

use common\models\BillingMain;

use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;
use yii\helpers\VarDumper;

/**
 *
 * @property int        id                      идентификатор заказа
 * @property string     address
 *
 *
 */
class IcoRequestAddress extends ActiveRecord
{

    public static function tableName()
    {
        return 'ico_request_address';
    }

}