<?php
/**
 * Created by PhpStorm.
 * User: s.arhangelskiy
 * Date: 12.04.2016
 * Time: 16:13
 */

namespace common\models\investment;


use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;
use yii\helpers\VarDumper;

/**
 * Содержит информацию о транзакции перевода токена по его заявке
 *
 * @property int        id                      идентификатор ico_request.id
 * @property int        created_at              время добавления транзакции
 * @property string(80) txid                    адрес транзакции
 *
 *
 * Class IcoRequestSuccess
 * @package app\services
 */
class IcoRequestSuccess extends ActiveRecord
{
    public static function tableName()
    {
        return 'ico_request_success';
    }

    /**
     * @param array $fields id,txid
     *
     * @return \common\models\investment\IcoRequestSuccess
     * @throws
     */
    public static function add($fields)
    {
        if (!isset($fields['created_at'])) {
            $fields['created_at'] = time();
        }
        $i = new static($fields);
        if (!$i->save()) {
            throw new \Exception('Не удалось сохранить \common\models\investment\IcoRequestSuccess');
        }

        return $i;
    }
}