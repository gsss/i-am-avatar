<?php

namespace common\models\investment;

use common\models\UserAvatar;
use cs\services\VarDumper;
use yii\base\Exception;
use yii\base\InvalidConfigException;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;

/**
 * @property int    id               Идентификатор
 * @property string name             Наименование проекта
 * @property string key              Идентификатор проекта, случайная строка из 10 символов
 * @property string content          Текст описания проекта
 * @property string image            Картинка брендированная для проекта
 * @property int    user_id          Идентификатор пользователя кому принадлежит проект
 * @property int    status           Флаг. Прошел врификацию?
 *                                      0 - не прошел верификацию и не отправил на верификацию (черновик)
 *                                      2 - отправил на верификацию
 *                                      3 - верифицировано
 * @property int    time_start       Флаг. Время начала краудфандинга
 * @property int    time_end         Время окончания краудфандинга
 * @property int    currency         Идентификатор валюты по таблице currency
 * @property int    created_at       Время добавления проекта
 * @property int    type             Тип проекта 0 - краудфандинг, 1 - ico
 * @property int    payments_list_id Идентификатор листа приема денег paysystems_config_list.id
 * @property int    is_bonus         Если ли бонусная программа? 0 - нет, 1 - есть
 * @property int    is_hide_client   Флаг, скрыть проект с точки зрения клиента
 * @property int    is_hide_platform Флаг, скрыть проект с точки зрения платформы
 * @property int    mark_deleted     Флаг, 0 - проект действующий, 1 - проект удален
 *
 */
class Project extends ActiveRecord
{
    const TYPE_KRAUDFUNDING = 0;
    const TYPE_ICO = 1;

    const STATUS_DRAFT = 0;
    const STATUS_WAIT = 1;
    const STATUS_ASSEPTED = 2;
    const STATUS_REJECTED = 3;

    /** @var  \common\models\investment\ProjectIco */
    private $ico;

    public static function tableName()
    {
        return 'projects';
    }

    public function rules()
    {
        $attributes = array_keys($this->attributes);
        return [
            [$attributes, 'safe'],
        ];
    }

    /**
     * Возвращает свойства проекта ICO
     *
     * @return \common\models\investment\ProjectIco
     * @throws \yii\base\Exception
     */
    public function getIco()
    {
        if (is_null($this->ico)) {
            if ($this->type != self::TYPE_ICO) throw new Exception('Это не ICO');
            $this->ico = $this->_getIco();
        }

        return $this->ico;
    }

    /**
     * @return \common\models\UserAvatar
     */
    public function getUser()
    {
        return UserAvatar::findOne($this->user_id);
    }

    /**
     * Возвращает свойства проекта ICO
     *
     * @return \common\models\investment\ProjectIco
     */
    private function _getIco()
    {
        $ico = ProjectIco::findOne($this->id);
        if (!is_null($ico)) {
            $ico->_project = $this;
        }

        return $ico;
    }

    /**
     * Возвращает все подарки
     *
     * @param null | array | string $sort сортировка для запроса
     *
     * @return \common\models\investment\Gift[]
     */
    public function getGifts($sort = null)
    {
        $query = $this->getGiftsQuery();
        if (is_null($sort)) {
            $query->orderBy($sort);
        }

        return $query->all();
    }

    /**
     * @return \yii\db\Query
     */
    public function getGiftsQuery()
    {
        return Gift::find()->where(['project_id' => $this->id]);
    }
}