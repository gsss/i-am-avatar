<?php
/**
 * Created by PhpStorm.
 * User: s.arhangelskiy
 * Date: 12.04.2016
 * Time: 16:13
 */

namespace common\models\investment;

use common\models\BillingMain;

use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;
use yii\helpers\VarDumper;

/**
 *
 * @property int        id                      идентификатор сообщения
 * @property int        request_id              идентификатор заявки
 * @property int        direction
 * @property int        status
 * @property int        datetime
 * @property string     message
 *
 *
 * Class IcoRequestMessage
 * @package app\services
 */
class IcoRequestMessage extends ActiveRecord
{

    public static function tableName()
    {
        return 'ico_request_messages';
    }

    /**
     * @param array $fields
     *
     * @return \common\models\investment\IcoRequestMessage
     */
    public static function add($fields)
    {
        if (!isset($fields['created_at'])) {
            $fields['datetime'] = time();
        }
        $i = new static($fields);
        $i->save();
        $i->id = $i::getDb()->lastInsertID;

        return $i;
    }
}