<?php
/**
 * Created by PhpStorm.
 * User: s.arhangelskiy
 * Date: 12.04.2016
 * Time: 16:13
 */

namespace common\models;

use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\helpers\VarDumper;

/**
 *
 * @property int        id
 * @property string     name
 * @property string     content
 * @property int        created_at
 * @property int        user_id
 * @property int        school_id
 * @property string     price
 */
class VoteList extends ActiveRecord
{
    public static function tableName()
    {
        return 'vote_list';
    }

    public function rules()
    {
        return [
            ['name', 'string'],
            ['name', 'required'],

            ['content', 'string'],

            ['created_at', 'integer'],

            ['user_id', 'integer'],

            ['school_id', 'integer'],

            ['price', 'string'],
        ];
    }
}