<?php

namespace common\models;

use common\components\Card;
use common\models\avatar\QrCode;
use common\models\avatar\UserBill;
use common\models\avatar\UserBillMerchant;
use common\models\SendLetter;
use cs\Application;
use cs\services\Str;
use iAvatar777\services\FormAjax\ActiveRecord;
use Yii;
use yii\base\Model;
use yii\db\Query;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\VarDumper;

/**
 * @property  int       $id
 * @property  int       $school_id
 * @property  int       $program_id
 * @property  int       $author_id
 * @property  int       $created_at
 * @property  int       $type_id
 * @property  string    $name
 * @property  string    $document
 * @property  string    $link
 */
class SchoolKoopProject extends \yii\db\ActiveRecord
{
    const TYPE_BLAGO_PROGRAM = 1;
    const TYPE_INVEST_PROGRAM = 2;

    public function attributeLabels()
    {
        return [
            'name' => 'Наименование',
        ];
    }

    public function behaviors()
    {
        return [
            [
                'class' => '\yii\behaviors\TimestampBehavior',
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at'],
                ],
                // если вместо метки времени UNIX используется datetime:
                // 'value' => new Expression('NOW()'),
            ],
        ];
    }


    public static function add($fields)
    {
        $i = new static($fields);
        $i->save();
        $i->id = self::getDb()->lastInsertID;

        return $i;
    }
}
