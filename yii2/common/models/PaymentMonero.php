<?php

namespace common\models;

use Yii;
use yii\base\Exception;
use yii\db\ActiveRecord;

/**
 * @property integer    id
 * @property integer    billing_id
 * @property float      value
 * @property string     action
 * @property string     transaction
 * @property string     from
 */
class PaymentMonero extends ActiveRecord implements PaymentI
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'payments_monero';
    }

    public function setTransaction($txid)
    {
        $this->transaction = $txid;
    }

    public function saveTransaction($txid)
    {
        $this->transaction = $txid;
        $this->save();
    }

    public function getTransaction()
    {
        return $this->transaction;
    }

    public function getTransactionAt()
    {
        return $this->transaction_at;
    }

    /**
     * @param mixed $condition
     * @return \common\models\PaymentBitCoin
     * @throws Exception
     */
    public static function findOne($condition)
    {
        $item = parent::findOne($condition);
        if (is_null($item)) {
            throw new Exception('Не найден платеж');
        }
        return $item;
    }


    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'          => 'ID',
            'billing_id'  => 'Счет',
            'value'       => 'Число монет',
            'action'      => 'ссылка на платеж в мерчанте',
            'transaction' => 'Трназакция',
            'from'        => 'от кого',
        ];
    }
}
