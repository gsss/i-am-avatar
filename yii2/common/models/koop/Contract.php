<?php
/**
 * Created by PhpStorm.
 * User: god
 * Date: 28.02.2017
 * Time: 23:49
 */

namespace common\models\koop;

use yii\db\ActiveRecord;

/**
 *
 * @property integer id
 * @property double  price
 * @property integer currency
 * @property integer kassir_id
 * @property integer company_id
 * @property integer created_at
 *
 * @package common\models
 */
class Contract extends ActiveRecord
{
    public static function tableName()
    {
        return 'koop_buy';
    }
}