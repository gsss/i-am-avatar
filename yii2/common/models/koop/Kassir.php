<?php
/**
 * Created by PhpStorm.
 * User: god
 * Date: 28.02.2017
 * Time: 23:49
 */

namespace common\models\koop;

use common\models\UserAvatar;
use yii\db\ActiveRecord;

/**
 *
 * @property integer        id
 * @property integer        user_id
 * @property integer        company_id
 *
 * @package common\models
 */
class Kassir extends ActiveRecord
{
    public static function tableName()
    {
        return 'koop_kassir';
    }

    public function getUser()
    {
        return UserAvatar::findOne($this->user_id);
    }
}