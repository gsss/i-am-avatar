<?php
/**
 * Created by PhpStorm.
 * User: god
 * Date: 28.02.2017
 * Time: 23:49
 */

namespace common\models\koop;

use yii\db\ActiveRecord;

/**
 *
 * @property integer id
 * @property integer product_id
 * @property integer created_at
 * @property integer koop_id
 * @property integer is_done     выпущены токены? 0 - нет, 1 - да, по умолчанию - 0
 *
 * @package common\models
 */
class RequestPayIn extends ActiveRecord
{
    public static function tableName()
    {
        return 'koop_request_pay_in';
    }

    public function rules()
    {
        return [[array_keys($this->attributes), 'safe']];
    }

    /**
     * @return \common\models\koop\Product
     */
    public function getProduct()
    {
        return Product::findOne($this->product_id);
    }
}