<?php
/**
 * Created by PhpStorm.
 * User: god
 * Date: 28.02.2017
 * Time: 23:49
 */

namespace common\models\koop;

use yii\db\ActiveRecord;

/**
 *
 * @property integer id
 * @property string  document
 * @property integer buy_id         идентификатор сделки, контркта koop_buy.id
 * @property integer type
 *
 * @package common\models
 */
class Document extends ActiveRecord
{
    public static function tableName()
    {
        return 'koop_buy_documents';
    }
}