<?php
/**
 * Created by PhpStorm.
 * User: god
 * Date: 28.02.2017
 * Time: 23:49
 */

namespace common\models\koop;

use yii\db\ActiveRecord;

/**
 *
 * @property integer id
 * @property integer koop_id
 * @property integer company_id
 *
 * @package common\models
 */
class LinkCompanyKooperative extends ActiveRecord
{
    public static function tableName()
    {
        return 'koop_company_link';
    }

    /**
     * @return \common\models\koop\Kooperative
     */
    public function getKoop()
    {
        return Kooperative::findOne($this->koop_id);
    }
}