<?php
namespace common\models\koop;

use avatar\models\UserRegistration;
use avatar\modules\UniSender\UniSender;
use common\components\providers\BTC;
use common\models\avatar\Currency;
use common\models\avatar\UserBill;
use common\models\avatar\UserBillDefault;
use common\models\avatar\UserBinance;
use cs\Application;
use cs\services\Security;
use cs\services\SitePath;
use cs\services\VarDumper;
use Imagine\Image\Box;
use yii\base\Exception;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;
use yii\helpers\FileHelper;
use yii\helpers\Url;
use cs\services\UploadFolderDispatcher;
use yii\imagine\Image;
use Imagine\Image\ManipulatorInterface;
use yii\web\IdentityInterface;


/**
 *
 * @property integer   id
 * @property integer   user_enter_id
 * @property integer   school_id
 * @property integer   created_at
 * @property integer   user1_id
 * @property integer   user2_id
 * @property string    public_key1
 * @property string    public_key2
 * @property string    hash
 * @property string    signature1
 * @property string    signature2
 * @property string    file
 */
class UserVerification extends ActiveRecord
{
    public static function tableName()
    {
        return 'koop_user_verification';
    }

    public static function add($fields)
    {
        $i = new static($fields);
        $i->save();
        $i->id = self::getDb()->lastInsertID;

        return $i;
    }
}
