<?php

namespace common\models;


use yii\db\ActiveRecord;


/**
 * Class UserRegisterCode
 * @package common\models
 *
 * @property $id
 * @property $code
 * @property $password_hash
 * @property $parent_id
 * @property $created_at
 * @property $date_finish
 */
class UserRegisterCode extends ActiveRecord
{


    /**
     *
     * @param array $fields
     *
     * @return UserRegisterCode
     */
    public static function add($fields)
    {
        if (!isset($fields['created_at'])) {
            $fields['created_at'] = time();
        }
        $item = new static($fields);
        $ret = $item->save();
        $item->id = static::getDb()->lastInsertID;

        return $item;
    }
}