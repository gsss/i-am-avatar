<?php

namespace common\models\log;

use cs\services\BitMask;
use yii\db\ActiveRecord;
use yii\db\Query;

class LogAvatar extends ActiveRecord
{
    public static  function tableName()
    {
        return 'log2_avatar';
    }

    public static function getDb()
    {
        return \Yii::$app->dbStatistic;
    }

}