<?php

namespace common\models\log;

use cs\services\BitMask;
use yii\db\ActiveRecord;
use yii\db\Query;

class LogApi extends ActiveRecord
{
    public static  function tableName()
    {
        return 'log2_api';
    }

    public static function getDb()
    {
        return \Yii::$app->dbStatistic;
    }

}