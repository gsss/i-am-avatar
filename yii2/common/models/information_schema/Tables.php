<?php
/**
 * Created by PhpStorm.
 * User: s.arhangelskiy
 * Date: 12.04.2016
 * Time: 16:13
 */

namespace common\models\information_schema;


use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\helpers\VarDumper;

/**
 *
 */
class Tables extends ActiveRecord
{
    public static function tableName()
    {
        return 'TABLES';
    }

    public static function getDb()
    {
        return \Yii::$app->dbInfo;
    }
}