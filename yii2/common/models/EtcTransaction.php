<?php
/**
 * Created by PhpStorm.
 * User: s.arhangelskiy
 * Date: 12.04.2016
 * Time: 16:13
 */

namespace common\models;


use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\helpers\VarDumper;

/**
 *
 * @property int        id
 * @property int        time
 * @property int        block
 * @property float      comission
 * @property string     hash
 * @property string     from
 * @property string     to
 * @property float      amount
 * @property int        type
 * @property string     data
 *
 * Class Language
 * @package app\services
 */
class EtcTransaction extends ActiveRecord
{
    const TYPE_ETH = 1;
    const TYPE_CONTRACT_EXECUTE = 2;
    const TYPE_CONTRACT_CREATE = 3;

    public static function tableName()
    {
        return 'etc_transactions';
    }
}