<?php

namespace common\models;

use common\models\PaySystem;
use cs\Application;
use cs\services\BitMask;
use cs\web\Exception;
use yii\db\ActiveRecord;
use yii\db\Query;
use yii\helpers\Json;
use yii\helpers\VarDumper;

/**
 * @property int    id              - идентификатор записи
 * @property string name            - Имя класса, начинается со слеша
 *
 */
class BillingMainClass extends ActiveRecord
{
    public static function tableName()
    {
        return 'billing_class';
    }


    public static function add($fields)
    {
        $i = new static($fields);
        $i->save();
        $i->id = self::getDb()->lastInsertID;

        return $i;
    }

    /**
     * @param string $name Название класса
     *
     * @return static
     */
    public static function getByName($name)
    {
        $i = self::findOne(['name' => $name]);
        if (is_null($i)) {
            $i = new self(['name' => $name]);
            $i->save();
            $i->id = self::getDb()->lastInsertID;
        }

        return $i;
    }
}