<?php
/**
 * Created by PhpStorm.
 * User: s.arhangelskiy
 * Date: 12.04.2016
 * Time: 16:13
 */

namespace common\models;

use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;
use yii\helpers\VarDumper;

/**
 *
 * @property int        id
 * @property int        podryad_id
 * @property int        user_id
 * @property string     tz_address
 * @property string     tz_sign
 * @property int        tz_created_at
 * @property string     protokol_address
 * @property string     protokol_sign
 * @property int        protokol_created_at
 *
 *
 * @package app\services
 */
class KoopPodryadCommandUser extends ActiveRecord
{
    public static function tableName()
    {
        return 'koop_podryad_command_user';
    }

    public function attributeLabels()
    {
        return [];
    }


    public static function add($fields)
    {
        $i = new self($fields);
        $ret = $i->save();
        if (!$ret) throw new \Exception('Cant save ' . VarDumper::dumpAsString($fields));
        $i->id = self::getDb()->lastInsertID;

        return $i;
    }
}