<?php

namespace common\models;

use Yii;
use yii\base\Exception;
use yii\db\ActiveRecord;
use yii\helpers\Json;

/**
 * @property integer    id
 * @property integer    billing_id
 * @property float      value
 * @property string     action
 * @property string     transaction
 * @property integer    transaction_at когда была укзана транзакция как оплаченная
 * @property string     from
 * @property string     address
 */
class PaymentLightCoin extends ActiveRecord implements PaymentI
{
    public $confirmations = 6;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'payments_lightcoin';
    }

    public function setTransaction($txid)
    {
        $this->transaction = $txid;
    }

    public function saveTransaction($txid)
    {
        $this->transaction = $txid;
        $this->save();
    }

    public function getTransaction()
    {
        return $this->transaction;
    }

    public function getTransactionAt()
    {
        return $this->transaction_at;
    }


    /**
     * @param mixed $condition
     * @return \common\models\PaymentBitCoin
     * @throws Exception
     */
    public static function findOne($condition)
    {
        $item = parent::findOne($condition);
        if (is_null($item)) {
            throw new Exception('Не найден платеж');
        }
        return $item;
    }


    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'          => 'ID',
            'billing_id'  => 'Счет',
            'value'       => 'Число монет',
            'action'      => 'ссылка на платеж в мерчанте',
            'transaction' => 'Трназакция',
            'from'        => 'от кого',
        ];
    }


    /**
     * Проверяет транзакцию на оплаченность
     *
     * @return bool
     */
    public function isTransactionPaidBilling()
    {
        $info = $this->getTxInfo($this->transaction);

        // проверка на кол-во подтверждений
        if ($info['confirmations'] < $this->confirmations) return false;

        // проверка на верный адрес и кол-во монет
        $result = false;
        foreach ($info['outputs'] as $output) {
            if (in_array($this->address, $output['addresses'])) {
                if (($output['value']/100000000) >= $this->value) $result = true;
            }
        }
        if ($result == false) return false;

        // проверка на дубликаты
        $exist = self::find()
            ->where(['transaction' => $this->transaction])
            ->andWhere(['<', 'id', $this->id ])
            ->exists();
        if ($exist) return false;

        return true;
    }


    /**
     * https://api.blockcypher.com/v1/ltc/main/txs/26eedef360667b2358f32724fd861cdb9cabe58fe273322e2d6eb2eac35673e5?limit=50&includeHex=true
     *
     * @param string $txid хеш транзакции
     *
     * @return array
     * https://api.blockcypher.com/v1/ltc/main/txs/26eedef360667b2358f32724fd861cdb9cabe58fe273322e2d6eb2eac35673e5?limit=50&includeHex=true
     *
     * @throws
     */
    public static function getTxInfo($txid)
    {
        $url = 'https://api.blockcypher.com';

        $client = new \yii\httpclient\Client(['baseUrl' => $url]);
        $content = $client->get('v1/ltc/main/txs/'.$txid, ['limit' => '50', 'includeHex' => 'true'])->send()->content;
        $data = Json::decode($content);
        return $data;
    }

}
