<?php
/**
 * Created by PhpStorm.
 * User: Бог-Творец
 * Date: 06.01.2017
 * Time: 17:14
 */

namespace common\models;


use cs\services\VarDumper;
use yii\db\ActiveRecord;

/**
 * @property int        id
 * @property int        user_id         идентификатор хозяина
 * @property int        created_at      мгновение добавления записи
 * @property string     address
 *
 */
class UserDigitalSignList extends ActiveRecord
{
    public static function tableName()
    {
        return 'user_digital_sign_list';
    }

    public function behaviors()
    {
        return [
            [
                'class' => '\yii\behaviors\TimestampBehavior',
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at'],
                ],
                // если вместо метки времени UNIX используется datetime:
                // 'value' => new Expression('NOW()'),
            ],
        ];
    }

    /**
     *
     * @param array $fields
     *
     * @return \common\models\UserDigitalSignList
     */
    public static function add($fields)
    {
        $item = new static($fields);
        $ret = $item->save();
        $item->id = static::getDb()->lastInsertID;

        return $item;
    }

    /**
     * Выдает хозяина документа
     *
     * @return \common\models\UserAvatar
     */
    public function getUser()
    {
        return UserAvatar::findOne($this->user_id);
    }
}