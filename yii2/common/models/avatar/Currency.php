<?php

namespace common\models\avatar;

use common\models\Token;
use cs\services\VarDumper;
use yii\base\Exception;
use yii\base\InvalidConfigException;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;

/**
 *
 *
 * @property int    id                  Идентификатор
 * @property string code                Трехбуквенный код на англ, большими буквами
 * @property string title               Название
 * @property string image               Картинка
 * @property int    decimals            Кол-во десятичных знаков
 * @property double kurs                Курс к рублю
 * @property int    is_merchant         Показывать?
 * @property int    is_view             Показывать на мерчанте?
 * @property int    is_crypto           Это криптовалюта?
 * @property int    code_coincap        Код валюты на coincap
 * @property int    code_coinmarketcap  Код валюты на coinmarketcap
 * @property int    id_coinmarketcap    Идентификатор валюты на coinmarketcap
 * @property int    rank
 * @property double chanche_1h
 * @property double chanche_1d
 * @property double chanche_1w
 * @property double day_volume_usd
 * @property double market_cap_usd
 * @property double available_supply
 * @property double total_supply
 * @property double max_supply
 * @property double price_usd
 * @property double price_rub
 * @property double price_btc
 *
 */
class Currency extends ActiveRecord
{
    const CACHE_KEY1 = '\common\models\avatar\Currency::moneyRate';
    const CACHE_KEY2 = '\common\models\avatar\Currency::currencyList';
    const CACHE_KEY3 = '\common\models\avatar\Currency::getListKurs';
    const CACHE_KEY4 = '\common\models\avatar\Currency::getCurrencyList';
    const CACHE_KEY5 = '\common\models\avatar\Currency::matrix';
    const CACHE_KEY6 = '\common\models\avatar\Currency::getBaseRateMatrix';

    const BTC = 3;
    const ETH = 4;
    const ETC = 14;
    const VVB = 9;
    const SB = 10;
    const ELXGOLD = 1;
    const RUB = 7;

    public static $currencyList;

    public static function tableName()
    {
        return 'currency';
    }

    public function rules()
    {
        return [
            [[
                'code',
                'title',
            ], 'required'],
            ['kurs', 'double'],
            ['code', 'string', 'max' => 6],
            ['title', 'string', 'max' => 255],
            ['image', 'string', 'max' => 255],
            [[
                'code',
                'title',
                'decimals',
                'kurs',
                'is_view',
                'image',
                'is_merchant',
                'is_avr_wallet',
                'code_coincap',
                'code_coinmarketcap',
                'id_coinmarketcap',
                'is_crypto',
                'chanche_1h',
                'chanche_1d',
                'chanche_1w',
                'day_volume_usd',
                'market_cap_usd',
                'available_supply',
                'total_supply',
                'max_supply',
                'rank',
                'price_rub',
                'price_usd',
                'price_btc',
            ], 'safe']
        ];
    }

    /**
     * @return array массив валют, первая всегда рубль
     * [
     * 'RUB',
     * 'BTC',
     * 'ETH',
     * 'USD',
     * 'EUR',
     * 'LTC',
     * ]
     */
    private static function _getCurrencyList()
    {
        $list = self::_getList();
        $currencyList = ['RUB'];
        /** @var self $item */
        foreach ($list as $item) {
            if ($item['code'] != 'RUB') {
                $currencyList[] = $item['code'];
            }
        }

        return $currencyList;
    }

    /**
     * @return array массив валют, первая всегда рубль
     * [
     * 'RUB',
     * 'BTC',
     * 'ETH',
     * 'USD',
     * 'EUR',
     * 'LTC',
     * ]
     */
    public static function getCurrencyList()
    {
        $key = self::CACHE_KEY4;
        $matrix = \Yii::$app->cache->get($key);
        if ($matrix === false) {
            $matrix = self::_getCurrencyList();
            \Yii::$app->cache->set($key, $matrix);
        }

        return $matrix;
    }

    /**
     * Проверяет является ли эта валюта токеном?
     * @return bool
     */
    public function isToken()
    {
        return Token::find()->where(['currency_id' => $this->id])->exists();
    }

    /**
     * Выдает класс токена
     * @return null | \common\models\Token
     */
    public function getToken()
    {
        return Token::findOne(['currency_id' => $this->id]);
    }

    /**
     * Выдает курс из кеша
     *
     * @param string(3) $code
     *
     * @return double
     *
     * @throws
     */
    public static function getRate($code)
    {
        $key = self::CACHE_KEY1;
        $list = \Yii::$app->cache->get($key);
        if ($list === false) {
            $list = self::_getRateList();
            \Yii::$app->cache->set($key, $list);
        }
        if (!isset($list[$code])) {
            throw new Exception('Нет такого курса');
        }

        return $list[$code];
    }

    /**
     * Возвращает из базы данных значения курсов валют
     *
     * @return array
     * [
     *      'BTC' =>  <double>,
     *      'ETH' =>  <double>,
     *      'ETC' =>  <double>,
     * ]
     */
    private static function _getRateList()
    {
        return ArrayHelper::map(
            self::find()->select(['code', 'kurs'])->all(),
            'code',
            'kurs'
        );
    }

    /**
     * Конвертирут деньги из одной валюты в другую
     * Приведение к кол-ву знаков после запятых по валюте $to функция не выполняет
     *
     * @param double $amount    кол-во денег для перевода
     * @param string $from      трехбуквенный код валюты
     * @param string $to        трехбуквенный код валюты
     *
     * @return double
     *
     * @throws \yii\base\InvalidConfigException
     * Если валюта $from или $to не будет найдена среди заданных в self::$currencyList
     */
    public static function convert($amount, $from, $to)
    {
        $currencyList = self::getCurrencyList();
        $moneyRateMatrix = self::getRateMatrix();
        if (!in_array($from, $currencyList)) {
            throw new InvalidConfigException('Не верный тип валюты передан в параметре $from');
        }
        if (!in_array($to, $currencyList)) {
            throw new InvalidConfigException('Не верный тип валюты передан в параметре $from');
        }
        if ($from == $to) {
            return $amount;
        }
        // ищу позицию FROM
        $x = 0;
        foreach ($currencyList as $k) {
            if ($k == $from) break;
            $x++;
        }
        // ищу позицию TO
        $y = 0;
        foreach ($currencyList as $k) {
            if ($k == $to) break;
            $y++;
        }

        return $moneyRateMatrix[$y][$x] * $amount;
    }

    /**
     * Конвертирует значение в то которое указано в настройках пользователя, но не форматирует
     *
     * @param float $amount
     * @param string(3) $from код валюты `$amount`
     * @param bool $isCutNumbers обрезать?
     *
     * @return string
     * @throws \yii\base\InvalidConfigException
     */
    public static function convertBySettings($amount, $from, $isCutNumbers = false)
    {
        /** @var \common\models\UserAvatar $user */
        $user = \Yii::$app->user->identity;
        $currencyView = $user->currency_view;
        if (is_null($currencyView)) return $amount;
        if ($currencyView == 0) return $amount;

        $list = ArrayHelper::map(self::getList(), 'id', 'code');
        if (!isset($list[$currencyView])) {
            throw new InvalidConfigException('Не задана валюта с индексом ' . $currencyView);
        }
        $value = self::convert($amount, $from, $list[$currencyView]);

//        if ($isCutNumbers) {
//            $value = \Yii::$app->formatter->asDecimal($value, self::getDecimals($list[$currencyView]));
//        }

        return $value;
    }

    /**
     * Конвертирует значение в то которое указано, но не форматирует
     *
     * @param float $amount
     * @param string(3) $from код валюты `$amount`
     *
     * @return string
     * @throws \yii\base\InvalidConfigException
     */
    public static function convertBySettingsFormat($amount, $from)
    {
        /** @var \common\models\UserAvatar $user */
        $user = \Yii::$app->user->identity;
        $currencyView = $user->currency_view;

        $list = ArrayHelper::map(self::getList(), 'id', 'decimals');
        if ($currencyView == 0) {
            $currencyObject = Currency::findOne(['code' => $from]);
            return \Yii::$app->formatter->asDecimal($amount, $currencyObject->decimals);
        }
        if (!isset($list[$currencyView])) {
            throw new InvalidConfigException('Не задана валюта с индексом ' . $currencyView);
        }

        return \Yii::$app->formatter->asDecimal(self::convertBySettings($amount, $from), $list[$currencyView]);
    }

    public static function getRateMatrix()
    {
        $key = self::CACHE_KEY5;
        $matrix = \Yii::$app->cache->get($key);
        if ($matrix === false) {
            $matrix = self::_getRateMatrix();
            \Yii::$app->cache->set($key, $matrix);
        }

        return $matrix;
    }

    /**
     *
     * @return array возвращает масив с курсами валют, первая обязательно RUB и равна 1 то есть самой себе равна
     * [
     * 'RUB' => 1,
     * 'BTC' => 75000,
     * 'ETH' => 60 * 11,
     * 'ETC' => 60 * 1.36,
     * 'USD' => 58,
     * 'EUR' => 63,
     * ];
     */
    private static function _getBaseRateMatrix()
    {
        $list = self::getList();
        $ret = ['RUB' => 1];
        /** @var self $item */
        foreach ($list as $item) {
            if ($item['code'] != 'RUB') {
                $ret[$item['code']] = (float)$item['kurs'];
            }
        }

        return $ret;
    }

    /**
     *
     * @return array возвращает масив с курсами валют, первая обязательно RUB и равна 1 то есть самой себе равна
     * [
     * 'RUB' => 1,
     * 'BTC' => 75000,
     * 'ETH' => 60 * 11,
     * 'ETC' => 60 * 1.36,
     * 'USD' => 58,
     * 'EUR' => 63,
     * ];
     */
    public static function getBaseRateMatrix()
    {
        $key = self::CACHE_KEY6;
        $matrix = \Yii::$app->cache->get($key);
        if ($matrix === false) {
            $matrix = self::_getBaseRateMatrix();
            \Yii::$app->cache->set($key, $matrix);
        }

        return $matrix;
    }

    /**
     * Возвращает матрицу двумерную где первой идет валюта RUB
     * Первый индекс столбцы
     * @return array
     */
    public static function _getRateMatrix()
    {
        $rub = self::getBaseRateMatrix();
        $moneyRateMatrix = [];
        $c = 0;
        $moneyRateMatrix[$c] = array_values($rub);

        // заполняю нижнюю левую часть
        for ($x = 1; $x < count($rub); $x++) {
            $val01 = $moneyRateMatrix[0][$x];
            $moneyRateMatrix[$x][$x] = 1;
            for ($y = $x + 1; $y < count($rub); $y++) {
                try {
                    $current = $moneyRateMatrix[0][$y] / $val01;
                } catch (\Exception $e) {
                    $current = 1;
                }

                $moneyRateMatrix[$x][$y] = $current;
            }
        }

        // заполняю верхнюю правую часть
        for ($x = 1; $x < count($rub); $x++) {
            for ($y = 0; $y < $x; $y++) {
                try {
                    $moneyRateMatrix[$x][$y] = 1 / $moneyRateMatrix[$y][$x];
                } catch (\Exception $e) {
                    $moneyRateMatrix[$x][$y] = 1;
                }
            }
        }

        return $moneyRateMatrix;
    }

    /**
     *
     * @return array
     * [
     *      [
     *          'id' => 1,
     *          'name' => 1,
     *          'code' => 1,
     *          'kurs' => 1,
     *      ],
     * ]
     */
    public static function getList()
    {
        $key = self::CACHE_KEY2;
        $matrix = \Yii::$app->cache->get($key);
        if ($matrix === false) {
            $matrix = self::_getList();
            \Yii::$app->cache->set($key, $matrix);
        }

        return $matrix;
    }

    /**
     * Очищает весь кеш для валют
     */
    public static function clearCache()
    {
        \Yii::$app->cache->delete(self::CACHE_KEY1);
        \Yii::$app->cache->delete(self::CACHE_KEY2);
        \Yii::$app->cache->delete(self::CACHE_KEY3);
        \Yii::$app->cache->delete(self::CACHE_KEY4);
        \Yii::$app->cache->delete(self::CACHE_KEY5);
        \Yii::$app->cache->delete(self::CACHE_KEY6);
    }

    /**
     * @return array
     * [
     *      [
     *          'id' => 1,
     *          'name' => 1,
     *          'code' => 1,
     *          'kurs' => 1,
     *      ],
     * ]
     */
    public static function getListDecimals()
    {
        $key = self::CACHE_KEY3;
        $matrix = \Yii::$app->cache->get($key);
        if ($matrix === false) {
            $matrix = self::_getListDecimals();
            \Yii::$app->cache->set($key, $matrix);
        }

        return $matrix;
    }

    /**
     * @return array
     * [
     *      [
     *          'id' => 1,
     *          'name' => 1,
     *          'code' => 1,
     *          'kurs' => 1,
     *      ],
     * ]
     *
     * @throws
     */
    public static function getDecimals($code)
    {
        $matrix = self::getListDecimals();
        if (!isset($matrix[$code])) {
            throw new Exception('Нет такого кода '.$code);
        }

        return $matrix[$code];
    }

    /**
     *
     * @return array возвращает масив с курсами валют, первая обязательно RUB и равна 1 то есть самой себе равна
     * [
     * 'RUB' => 1,
     * 'BTC' => 75000,
     * 'ETH' => 60 * 11,
     * 'ETC' => 60 * 1.36,
     * 'USD' => 58,
     * 'EUR' => 63,
     * ];
     */
    private static function _getListDecimals()
    {
        $list = self::getList();
        return ArrayHelper::map($list, 'code', 'decimals');
    }

    /**
     * Возвращает список цент валют по отношению к рублю, RUB не содержит, список берется из таблицы `currency`
     *
     * @return array
     * [
     *      [
     *          'id' => 1,
     *          'name' => 1,
     *          'code' => 1,
     *          'kurs' => 1,
     *      ],
     * ]
     */
    private static function _getList()
    {
        return self::find()
            ->where(['>', 'kurs', 0])
            ->asArray()
            ->all();
    }

    public static function add($fields)
    {
        $i = new static($fields);
        $i->save();
        $i->id = self::getDb()->lastInsertID;

        return $i;
    }
}