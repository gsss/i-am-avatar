<?php

namespace common\models\avatar;

use avatar\models\Wallet;
use avatar\models\WalletETC;
use avatar\models\WalletETH;
use avatar\models\WalletToken;
use Blocktrail\SDK\BlocktrailSDK;
use common\components\providers\ETH;
use common\components\providers\Token;
use common\models\Card;
use common\models\UserAvatar;
use common\payment\BitCoinBlockTrailPayment;
use common\services\Security;
use common\services\Security\AES;
use cs\services\BitMask;
use cs\services\VarDumper;
use Yii;
use yii\base\Exception;
use yii\db\ActiveRecord;
use yii\db\Query;

/**
 * @property int        id
 * @property string     address
 * @property string     identity
 * @property string     password
 * @property int        password_type
 * @property int        is_default
 * @property string     pin
 * @property string     name
 * @property int        user_id
 * @property \common\models\UserAvatar user
 * @property int        is_merchant
 * @property int        created_at
 * @property int        mark_deleted
 * @property string     card_number
 * @property int        currency    i_am_avatar_prod_main.currency_link_avatar_processing.currency_ext_id =
 *           i_am_avatar_prod_main.currency.id
 * @property int card_id     идентификатор привязанной карты
 *
 */
class UserBill extends ActiveRecord
{
    const PASSWORD_TYPE_OPEN = 1;
    const PASSWORD_TYPE_OPEN_CRYPT = 4;
    const PASSWORD_TYPE_HIDE_CABINET = 2;
    const PASSWORD_TYPE_HIDE_PRIVATE = 3;

    public static function tableName()
    {
        return 'user_bill';
    }

    public function rules()
    {
        return [
            [['address'], 'required'],
            [['name'], 'string', 'max' => 100],
            [['address'], 'string', 'max' => 46],
            [['identity'], 'string', 'max' => 40],
            [['password'], 'string', 'max' => 128],
            [['pin'], 'string', 'max' => 60],
            [
                [
                    'user_id',
                    'is_merchant',
                    'created_at',
                    'mark_deleted',
                    'card_id',
                    'password_type',
                    'currency',
                ],
                'integer',
            ],
        ];
    }

    public function setPin($value)
    {
        $this->pin = \Yii::$app->security->generatePasswordHash($value);
        $this->save();
    }

    /**
     * Проверяет пароль на валидность
     *
     * @param string $password password to validate
     *
     * @return boolean if password provided is valid for current user
     */
    public function validatePassword($password)
    {
        switch ($this->password_type) {
            case UserBill::PASSWORD_TYPE_OPEN:
                return $this->getUser()->validatePassword($password);
            case UserBill::PASSWORD_TYPE_HIDE_CABINET:
            case UserBill::PASSWORD_TYPE_HIDE_PRIVATE:
                $key32 = UserBill::passwordToKey32($password);
                $encrypted = $this->password;
                $output = AES::decrypt256CBC($encrypted, $key32);
                return !($output == '');
            default:
                return $this->getUser()->validatePassword($password);
        }
    }

    /**
     * @return \common\models\Card
     */
    public function getCard()
    {
        return Card::findOne($this->card_id);
    }

    public function validatePin($value)
    {
        return \Yii::$app->security->validatePassword($value, $this->pin);
    }

    /**
     * @return \common\models\avatar\UserBill
     *
     * @throws \yii\base\Exception
     */
    public static function findOne($condition)
    {
        $item = parent::findOne($condition);
        if (is_null($item)) {
            throw new Exception('Не найден счет');
        }

        return $item;
    }

    public static function add($fields)
    {
        if (!isset($fields['created_at'])) $fields['created_at'] = time();
        if (!isset($fields['mark_deleted'])) $fields['mark_deleted'] = 0;

        $item = new self($fields);
        $ret = $item->save();

        if (!$ret) throw new Exception('Не удалось сохранить счет ' . \yii\helpers\VarDumper::dumpAsString($item));
        $item->id = $item::getDb()->lastInsertID;

        return $item;
    }

    /**
     * Добавляет счет, если он первый то добавляется как счет по умолчанию
     *
     * @param array $fields
     * + user_id
     * + currency
     * - address
     * - identity
     * - password
     * - name
     * - mark_deleted
     * - is_default
     * - card_id
     * - password_type
     *
     * @return UserBill
     * @throws Exception
     */
    public static function addWithDefault($fields)
    {
        if (!UserBill::find()->where(['user_id' => $fields['user_id'], 'currency' => $fields['currency']])->exists()) {
            $fields['is_default'] = 1;
        }
        $item = self::add($fields);

        return $item;
    }

    /**
     * @return \common\models\avatar\Currency
     * @throws
     */
    public function getCurrencyObject()
    {
        $o = Currency::findOne($this->currency);
        if (is_null($o)) {
            throw new \Exception('Не найдена валюта ' . $this->currency);
        }

        return $o;
    }

    /**
     * Возвращает кошелек Эфира
     *
     * @return \avatar\models\WalletETH
     * @throws \Exception
     */
    public function getWalletETH()
    {
        if ($this->currency != Currency::ETH) {
            throw new Exception('Не верный формат кошелька');
        }

        return new WalletETH([
            'billing'  => $this,
            'provider' => \Yii::$app->eth,
        ]);
    }

    /**
     * Возвращает кошелек Ethereum Classic
     *
     * @return \avatar\models\WalletETC
     *
     * @throws \Exception
     */
    public function getWalletETC()
    {
        if ($this->currency != Currency::ETC) {
            throw new Exception('Не верный формат кошелька');
        }

        return new WalletETC([
            'billing'  => $this,
            'provider' => \Yii::$app->etc,
        ]);
    }

    /**
     * Возвращает кошелек токена
     *
     * @return \avatar\models\WalletToken
     * @throws \Exception
     */
    public function getWalletToken()
    {
        $currency = $this->getCurrencyObject();
        $token = $currency->getToken();
        $provider = $token->getProvider();

        return new WalletToken([
            'billing'  => $this,
            'provider' => $provider,
            'token'    => $token,
        ]);
    }

    /**
     * Для счетов с типом паролей 2
     * Заменяет пароли c $from на $to в счетах пользователя $userId
     *
     * @param string $from
     * @param string $to
     * @param int $userId
     *
     * @throws
     */
    public static function changePassword($from, $to, $userId)
    {
        $billingList = UserBill::find()
            ->where([
                'user_id'       => $userId,
                'password_type' => UserBill::PASSWORD_TYPE_HIDE_CABINET,
                'mark_deleted'  => 0,
            ])
            ->all();
        $t = \Yii::$app->db->beginTransaction();

        try {
            /** @var \common\models\avatar\UserBill $billing */
            foreach ($billingList as $billing) {
                $passwordHash = $billing->password;
                $keyFrom = UserBill::passwordToKey32($from);
                $keyTo = UserBill::passwordToKey32($to);
                $walletPassword = AES::decrypt256CBC($passwordHash, $keyFrom);
                \Yii::info(\yii\helpers\VarDumper::dumpAsString([$walletPassword, $billing->address]), 'avatar\common\models\avatar\UserBill::changePassword:1');
                if ($walletPassword == '') {
                    \Yii::warning('Не верный пароль к кошельку ' . $billing->id, 'avatar\common\models\avatar\UserBill::changePassword');
                    throw new Exception('Не верный пароль к кошельку ' . $billing->id);
                }
                $passwordHashNew = AES::encrypt256CBC($walletPassword, $keyTo);
                \Yii::info(\yii\helpers\VarDumper::dumpAsString([$passwordHashNew, $billing->address]), 'avatar\common\models\avatar\UserBill::changePassword:2');
                $billing->password = $passwordHashNew;
                $ret = $billing->save();
                \Yii::trace(\yii\helpers\VarDumper::dumpAsString($ret), 'avatar\common\models\avatar\UserBill::changePassword');
            }
            $t->commit();
        } catch (\Exception $e) {
            $t->rollBack();
            throw $e;
        }
    }

    /**
     * Конвертирует пароль в ключ для шифрования
     *
     * @param $password
     *
     * @return string
     */
    public static function passwordToKey32($password)
    {
        if (strlen($password) < 32) {
            $key = $password . str_repeat('0', 32 - strlen($password));
        } else if (strlen($password) > 32) {
            $key = substr($password, 0, 32);
        } else {
            $key = $password;
        }
        \Yii::trace($key, 'avatar\common\models\avatar\UserBill::passwordToKey32');

        return $key;
    }

    /**
     * Расшифровывает пароль
     *
     * @param string $passwordUser пароль для рассшифровки
     *
     * @return string
     */
    public function passwordDeСript($passwordUser)
    {
        $key = self::passwordToKey32($passwordUser);
        return AES::decrypt256CBC($this->password, $key);
    }

    /**
     * Шифрует пароль
     *
     * @param string $passwordWallet пароль от кошелька
     * @param string $passwordUser   пароль от кабинета
     *
     * @return string
     */
    public static function passwordEnСript($passwordWallet, $passwordUser)
    {
        $key = self::passwordToKey32($passwordUser);
        return \common\services\Security\AES::encrypt256CBC($passwordWallet, $key);
    }

    /**
     *
     *
     * @param string $userPassword пользовательский пароль, если null то предполагается что будет стандартный пароль
     *
     * @return \avatar\models\Wallet
     * @throws \Exception
     */
    public function getWalletBTC($userPassword = null)
    {
        Yii::info(\yii\helpers\VarDumper::dumpAsString($userPassword), 'avatar\common\models\avatar\UserBill::getWalletBTC:1');
        $passwordWallet = $this->getPassword($userPassword);
        Yii::info(\yii\helpers\VarDumper::dumpAsString($passwordWallet), 'avatar\common\models\avatar\UserBill::getWalletBTC:2');

        $provider = new BitCoinBlockTrailPayment();
        $client = $provider->getClient();
        $wallet = $client->initWallet([
            "identifier" => $this->identity,
            "password"   => $passwordWallet,
        ]);
        Yii::info(\yii\helpers\VarDumper::dumpAsString($wallet), 'avatar\common\models\avatar\UserBill::getWalletBTC:3');

        return new Wallet([
            'blockTrailWallet' => $wallet,
            'billing'          => $this,
        ]);
    }

    /**
     * @return \common\models\UserAvatar
     */
    public function getUser()
    {
        return UserAvatar::findOne($this->user_id);
    }

    /**
     * Выдает пароль от кошелька на сервере
     *
     * @param $password
     *
     * @return string
     */
    public function getPassword($password)
    {
        $passwordHash = $this->password;
        $passwordType = $this->password_type;
        switch ($passwordType) {
            case self::PASSWORD_TYPE_HIDE_CABINET:
            case self::PASSWORD_TYPE_HIDE_PRIVATE:
                $unlockKey = $password;
                $key = self::passwordToKey32($unlockKey);
                $passwordWallet = \common\services\Security\AES::decrypt256CBC($passwordHash, $key);
                break;
            case self::PASSWORD_TYPE_OPEN:
                $passwordWallet = $passwordHash;
                break;
            case self::PASSWORD_TYPE_OPEN_CRYPT:
            default:
                $unlockKey = Yii::$app->params['unlockKey'];
                $key = self::passwordToKey32($unlockKey);
                $passwordWallet = \common\services\Security\AES::decrypt256CBC($passwordHash, $key);
                break;
        }

        return $passwordWallet;
    }

    /**
     * Устанавливает пароль от кошелька на сервере в счет в типе UserBill::PASSWORD_TYPE_OPEN_CRYPT
     *
     * @param string $passwordWallet
     *
     * @return boolean
     */
    public function setPasswordOpenCrypt($passwordWallet)
    {
        $this->password_type = UserBill::PASSWORD_TYPE_OPEN_CRYPT;
        $unlockKey = Yii::$app->params['unlockKey'];
        $this->password = UserBill::passwordEnСript($passwordWallet, $unlockKey);
        Yii::info(\yii\helpers\VarDumper::dumpAsString([$this->address, $passwordWallet, $this->password, $unlockKey]), 'avatar\common\models\avatar\UserBill::setPasswordOpenCrypt');
        Yii::info(\yii\helpers\VarDumper::dumpAsString([$this->address, $passwordWallet, $this->password, $unlockKey]), 'info\common\models\avatar\UserBill::setPasswordOpenCrypt');

        return $this->save();
    }

    /**
     * Устанавливает пароль от кошелька на сервере в счет в типе UserBill::PASSWORD_TYPE_HIDE_CABINET
     *
     * @param string $passwordCabinet
     *
     * @return boolean
     * @throws
     */
    public function setPasswordHideCabinet($passwordCabinet)
    {
        $passwordWallet = $this->getPassword($passwordCabinet);
        if ($passwordWallet == '') {
            \Yii::warning('Не верный пароль к кошельку ' . $this->id, 'avatar\common\models\avatar\UserBill::changePassword');
            throw new \Exception('Не верный пароль к кошельку ' . $this->id);
        }
        $this->password = UserBill::passwordEnСript($passwordWallet, $passwordCabinet);
        $this->password_type = UserBill::PASSWORD_TYPE_HIDE_CABINET;
        Yii::info(\yii\helpers\VarDumper::dumpAsString([$this->address, $passwordWallet, $passwordCabinet, $this->password]), 'avatar\common\models\avatar\UserBill::setPasswordHideCabinet');
        Yii::info(\yii\helpers\VarDumper::dumpAsString([$this->address, $passwordWallet, $passwordCabinet, $this->password]), 'info\common\models\avatar\UserBill::setPasswordHideCabinet');

        return $this->save();
    }

    /**
     * Выдает пароль от кошелька на сервере
     *
     * @param string $password пароль от кабинета
     * @param string $passwordType
     *
     * @return array
     * [
     *      'passwordHash'
     *      'passwordWallet'
     * ]
     */
    public static function createPassword($password, $passwordType)
    {
        $passwordWallet = Security::generateRandomString();
        switch ($passwordType) {
            case self::PASSWORD_TYPE_HIDE_CABINET:
            case self::PASSWORD_TYPE_HIDE_PRIVATE:
                $unlockKey = $password;
                $key = self::passwordToKey32($unlockKey);
                $passwordHash = \common\services\Security\AES::encrypt256CBC($passwordWallet, $key);
                break;
            case self::PASSWORD_TYPE_OPEN:
                $passwordHash = $passwordWallet;
                break;
            case self::PASSWORD_TYPE_OPEN_CRYPT:
            default:
                $unlockKey = Yii::$app->params['unlockKey'];
                $key = self::passwordToKey32($unlockKey);
                $passwordHash = \common\services\Security\AES::encrypt256CBC($passwordWallet, $key);
                break;
        }

        return [
            'passwordHash'   => $passwordHash,
            'passwordWallet' => $passwordWallet,
        ];
    }

    /**
     * Ищет UserBill соответствующий, если нет то создает счет user_bill и кошелек i_am_avatar_prod_wallet.wallet.
     * Используется только для процессинга Аватара. Если не будет найден $currency_id в
     * \common\models\avatar\CurrencyLink то произойдент исключение \Exception('Не найден
     * \common\models\avatar\CurrencyLink');
     *
     * @param int $currency_id dbWallet.currency.id
     * @param int $user_id     user.id
     *
     * @return self
     * @throws
     */
    public static function getBillByCurrencyAvatarProcessing($currency_id, $user_id = null)
    {
        if (is_null($user_id)) $user_id = Yii::$app->user->id;
        $link = \common\models\avatar\CurrencyLink::findOne(['currency_int_id' => $currency_id]);
        if (is_null($link)) throw new \Exception('Не найден \common\models\avatar\CurrencyLink');

        try {
            $billing = UserBill::findOne([
                'user_id'      => $user_id,
                'currency'     => $link->currency_ext_id,
                'is_default'   => 1,
                'mark_deleted' => 0,
            ]);
        } catch (\Exception $e) {
            $wallet = \common\models\piramida\Wallet::addNew([
                'currency_id' => $currency_id,
            ]);
            $currency = Currency::findOne($link->currency_ext_id);
            $billing = UserBill::add([
                'user_id'    => $user_id,
                'currency'   => $link->currency_ext_id,
                'address'    => (string)$wallet->id,
                'name'       => $currency->title,
                'is_default' => 1,
            ]);
        }

        return $billing;
    }

    /**
     * Присоединяет адрес к счету не создавая его
     *
     * @param string $address
     */
    public function addAddress($address)
    {
        $model = new UserBillAddress([
            'bill_id'    => $this->id,
            'address'    => $address,
            'created_at' => time(),
        ]);
        if (!$model->save()) \Yii::warning(\yii\helpers\VarDumper::dumpAsString($model->errors), 'avatar\\addAddress');
    }

    /**
     * Ищет счет по адресу `$address`
     *
     * @param string $address
     *
     * @return \common\models\avatar\UserBill
     * @throws
     */
    public static function findByAddress($address)
    {
        return UserBillAddress::findOne(['address' => $address])->getBilling();
    }

}