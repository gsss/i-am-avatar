<?php

namespace common\models\avatar;

use avatar\models\Wallet;
use avatar\models\WalletETC;
use avatar\models\WalletETH;
use avatar\models\WalletToken;
use Blocktrail\SDK\BlocktrailSDK;
use common\components\providers\ETH;
use common\components\providers\Token;
use common\models\Card;
use common\models\UserAvatar;
use common\payment\BitCoinBlockTrailPayment;
use common\services\Security;
use common\services\Security\AES;
use cs\services\BitMask;
use cs\services\VarDumper;
use yii\base\Exception;
use yii\db\ActiveRecord;
use yii\db\Query;

/**
 * @property int                        id
 * @property string                     api_key
 * @property string                     api_secret
 *
 */
class UserBinance extends ActiveRecord
{
    public static function tableName()
    {
        return 'user_binance';
    }

    public function rules()
    {
        return [
            [['id','api_key','api_secret',], 'required'],
            [['id'], 'integer'],
            [['api_key','api_secret',], 'string', 'length' => 64],
        ];
    }
}