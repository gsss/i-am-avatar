<?php

namespace common\models\avatar;

use app\models\SiteContentInterface;
use app\models\SiteUpdateItem;
use app\models\SubscribeItem;
use common\payment\BitCoinBlockTrailPayment;
use cs\services\BitMask;
use yii\base\Exception;
use yii\db\ActiveRecord;
use yii\db\Query;
use app\services\Subscribe;
use yii\helpers\VarDumper;

/**
 * @property int    id
 * @property string transaction
 * @property string message
 * @property string data        данные о противоположном партнере (партнерах сделки), формат JSON
 * @property int    bill_id
 * @property int    type         направление перевода
 * @property int    created_at
 *
 */
class UserBillOperation extends ActiveRecord
{
    const TYPE_IN = 1;
    const TYPE_OUT = -1;

    public static function tableName()
    {
        return 'user_bill_operation';
    }

    public function rules()
    {
        return [
            [[
                'bill_id',
                'type',
                'transaction',
                'message',
                'created_at',
            ], 'required'],
            [['transaction'], 'string', 'max' => 70],
            [['message'],'string', 'max' => 1000],
            [['bill_id', 'type', 'created_at', 'user_id'], 'integer'],
        ];
    }

    /**
     * @return static
     * @throws \yii\base\Exception
     */
    public static function findOne($condition)
    {
        $item = parent::findOne($condition);
        if (is_null($item)) {
            throw new Exception('Не найдена операция');
        }
        return $item;
    }

    /**
     * Добавляет операцию
     * Добавляет created_at если нет
     * Добавляет type если нет = -1
     *
     * @param $fields
     * @return static
     * @throws Exception
     */
    public static function add($fields)
    {
        if (!isset($fields['created_at'])) {
            $fields['created_at'] = time();
        }
        if (!isset($fields['type'])) {
            $fields['type'] = -1;
        }
        $item = new static($fields);
        $item->save();
        if ($item->hasErrors()) throw new Exception('Не удалось сохранить \common\models\avatar\UserBillOperation::add() ' . VarDumper::dumpAsString($item->errors));
        $item->id = $item::getDb()->lastInsertID;

        return $item;
    }
}