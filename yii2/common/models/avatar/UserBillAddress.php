<?php

namespace common\models\avatar;

use app\models\SiteContentInterface;
use app\models\SiteUpdateItem;
use app\models\SubscribeItem;
use common\payment\BitCoinBlockTrailPayment;
use cs\services\BitMask;
use yii\base\Exception;
use yii\db\ActiveRecord;
use yii\db\Query;
use app\services\Subscribe;
use yii\helpers\VarDumper;

/**
 * @property int    id
 * @property string address
 * @property int    bill_id
 * @property int    created_at
 *
 */
class UserBillAddress extends ActiveRecord
{
    public static function tableName()
    {
        return 'user_bill_address';
    }

    public function rules()
    {
        return [
            [[
                'bill_id',
                'address',
                'created_at',
            ], 'required'],
            [['address'],'string', 'max' => 40],
            [['created_at'], 'integer'],
        ];
    }

    /**
     * @return static
     * @throws \yii\base\Exception
     */
    public static function findOne($condition)
    {
        $item = parent::findOne($condition);
        if (is_null($item)) {
            throw new Exception('Не найден счет');
        }
        return $item;
    }

    /**
     * @return \common\models\avatar\UserBill
     *
     */
    public function getBilling()
    {
        return UserBill::findOne($this->bill_id);
    }
}