<?php

namespace common\models\avatar;

use app\models\SiteContentInterface;
use app\models\SiteUpdateItem;
use app\models\SubscribeItem;
use common\payment\BitCoinBlockTrailPayment;
use cs\services\BitMask;
use yii\base\Exception;
use yii\db\ActiveRecord;
use yii\db\Query;
use app\services\Subscribe;
use yii\helpers\VarDumper;

/**
 * @property int    id
 * @property string config
 *
 */
class UserBillConfig extends ActiveRecord
{
    /** @var  \common\models\avatar\UserBill */
    public $billing;

    /**
     * @return UserBill
     */
    public function getBilling()
    {
        return $this->billing;
    }

    public static function tableName()
    {
        return 'user_bill_config';
    }

    public function rules()
    {
        return [
            [
                [
                    'primary_mnemonic',
                    'backup_mnemonic',
                    'primaryPrivateKey_BIP32Key',
                    'primaryPrivateKey_BIP32Key_path',
                    'primaryPublicKeys_BIP32Key_path',
                    'backupPublicKey_BIP32Key_key',
                    'backupPublicKey_BIP32Key_path',
                    'backupPublicKey_BIP32Key_derivations',
                    'blocktrailPublicKeys_BIP32Key_key',
                    'blocktrailPublicKeys_BIP32Key_path',
                    'blocktrailPublicKeys_BIP32Key_derivations',
                    'pubKeys',
                    'derivations',
                    'derivationsByAddress',
                    'checksum',
                    'config',
                ],
                'safe'
            ],
        ];
    }

    /**
     * @return static
     * @throws \yii\base\Exception
     */
    public static function findOne($condition)
    {
        $item = parent::findOne($condition);
        if (is_null($item)) {
            throw new Exception('Не найден конфиг');
        }
        return $item;
    }
}