<?php

namespace common\models\avatar;

use app\models\SiteContentInterface;
use app\models\SiteUpdateItem;
use app\models\SubscribeItem;
use avatar\models\Wallet;
use common\payment\BitCoinBlockTrailPayment;
use cs\services\BitMask;
use yii\base\Exception;
use yii\db\ActiveRecord;
use yii\db\Query;
use app\services\Subscribe;
use yii\helpers\VarDumper;

/**
 * @property int    id
 * @property string code
 * @property int    created_at
 * @property int    is_used
 * @property int    card_id
 *
 */
class QrCode extends ActiveRecord
{
    public static function tableName()
    {
        return 'bill_codes';
    }

    public function rules()
    {
        return [
            ['code',       'required'],
            ['created_at', 'required'],

            ['code', 'string', 'max' => 10],
            [['created_at', 'is_used', 'card_id'], 'integer'],
        ];
    }

    /**
     * @return static
     * @throws \yii\base\Exception
     */
    public static function findOne($condition)
    {
        $item = parent::findOne($condition);
        if (is_null($item)) {
            throw new Exception('Не найден счет');
        }
        return $item;
    }

    /**
     * @param $fields
     *
     * @return \common\models\avatar\QrCode
     * @throws Exception
     */
    public static function add($fields)
    {
        if (!isset($fields['created_at'])) {
            $fields['created_at'] = time();
        }
        $item = new self($fields);
        $ret = $item->save();
        if (!$ret) throw new Exception('no save ' . self::tableName() . ' errors='.VarDumper::dumpAsString($item->errors));
        $item->id = $item::getDb()->lastInsertID;

        return $item;
    }
}