<?php

namespace common\models\avatar;

use common\models\Token;
use cs\services\VarDumper;
use yii\base\Exception;
use yii\base\InvalidConfigException;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;

/**
 *
 *
 * @property int    id                  Идентификатор
 * @property int    currency_int_id     внутренняя i_am_avatar_prod_wallet.currency.id
 * @property int    currency_ext_id     внешняя i_am_avatar_prod_main.currency.id
 *
 */
class CurrencyLink extends ActiveRecord
{
    public static function tableName()
    {
        return 'currency_link_avatar_processing';
    }

    public static function add($fields)
    {
        $i = new static($fields);
        $i->save();
        $i->id = self::getDb()->lastInsertID;

        return $i;
    }
}