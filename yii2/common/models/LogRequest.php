<?php
/**
 * Created by PhpStorm.
 * User: s.arhangelskiy
 * Date: 12.04.2016
 * Time: 16:13
 */

namespace common\models;


use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;
use yii\helpers\VarDumper;

/**
 * Данные о запросах на сервер
 *
 * @property int    user_id
 * @property int    mode
 * @property float  time
 * @property string session_id
 * @property string ipv4
 * @property string url
 * @property string session
 * @property string cookie
 * @property string server
 * @property string get
 * @property string post
 * @property string headers
 *
 * Class Language
 * @package app\services
 */
class LogRequest extends ActiveRecord
{
    const MODE_GET      = 1;
    const MODE_POST     = 2;
    const MODE_OPTIONS  = 3;
    const MODE_HEAD     = 4;

    public static function tableName()
    {
        return 'log_requests';
    }

    public static function add()
    {
        \Yii::$app->session->open();
        $fields = [
            'time'    => microtime(true),
            'session' => substr(VarDumper::dumpAsString($_SESSION),0, 4000),
            'cookie'  => substr( VarDumper::dumpAsString($_COOKIE),0, 2000),
            'server'  => substr(VarDumper::dumpAsString($_SERVER),0, 4000),
            'get'     => substr(VarDumper::dumpAsString($_GET),0, 2000),
            'post'    => substr(VarDumper::dumpAsString($_POST),0, 2000),
            'url'     => $_SERVER['REQUEST_URI'],
            'ipv4'    => $_SERVER['REMOTE_ADDR'],
            'headers' => substr(VarDumper::dumpAsString(\Yii::$app->request->headers),0, 2000),
        ];
        if (\Yii::$app->session->hasSessionId) {
            $fields['session_id'] =\Yii::$app->session->id;
        }
        if ($_SERVER['REQUEST_METHOD'] == 'GET') {
            $fields['mode'] = self::MODE_GET;
        } else if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            $fields['mode'] = self::MODE_POST;
        }
        if (!\Yii::$app->user->isGuest) {
            $fields['user_id'] = \Yii::$app->user->id;
        }
        $item = new static($fields);
        $item->save();

        return $item;
    }

    public static function getDb()
    {
        return \Yii::$app->dbStatistic;
    }

}