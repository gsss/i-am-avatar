<?php
/**
 * Created by PhpStorm.
 * User: god
 * Date: 28.02.2017
 * Time: 23:49
 */

namespace common\models;

use yii\db\ActiveRecord;

/**
 * Class AvatarHash
 *
 * @property integer id
 * @property string  image
 * @property string  hash
 * @property string  data
 * @property integer created_at
 *
 * @package common\models
 */
class AvatarHash extends ActiveRecord
{
    public static function tableName()
    {
        return 'avatar_hash';
    }
}