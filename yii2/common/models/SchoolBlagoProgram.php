<?php
/**
 * Created by PhpStorm.
 * User: s.arhangelskiy
 * Date: 12.04.2016
 * Time: 16:13
 */

namespace common\models;

use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\helpers\VarDumper;

/**
 *
 * @property int        id
 * @property string     name
 * @property string     success_url
 * @property string     error_url
 * @property int        school_id
 * @property int        project_id
 * @property int        currency_id         - идентификатор валюты который будет начисляться токен вознаграждения
 * @property int        currency_wallet_id  - идентификатор кошелька с которого будет начисляться вознаграждение `currency_id`
 *
 * @package app\services
 */
class SchoolBlagoProgram extends ActiveRecord
{


}