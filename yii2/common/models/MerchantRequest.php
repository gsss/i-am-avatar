<?php
/**
 * Created by PhpStorm.
 * User: god
 * Date: 16.02.2017
 * Time: 10:52
 */

namespace common\models;

use avatar\base\Application;
use common\models\avatar\UserBill;
use common\models\avatar\UserBillMerchant;
use common\models\avatar\UserBillOperation;
use Yii;
use common\models\piramida\Billing;
use yii\base\Exception;
use yii\db\ActiveRecord;
use yii\helpers\Json;
use yii\helpers\StringHelper;
use yii\helpers\VarDumper;
use yii\httpclient\Client;

/**
 * Заявка на оплату
 *
 * @property int    id
 * @property int    merchant_id
 * @property int    paysystem_id
 * @property int    paysystem_config_id
 * @property int    billing_id
 * @property int    created_at
 * @property string title
 * @property string label
 * @property string data
 * @property int    price
 * @property int    user_id
 * @property int    is_paid
 * @property int    is_mail_send отправено ли письмо уведомление о приходе денег 0 - нет, 1 - да, по умолчанию - 0
 * @property string currency
 *
 * Class MerchantRequest
 * @package common\models
 */
class MerchantRequest extends ActiveRecord
{
    const EVENT_BEFORE_SUCCESS = 'beforeSuccess';
    const EVENT_AFTER_SUCCESS = 'afterSuccess';

    public static function tableName()
    {
        return 'merchant_request';
    }

    public function rules()
    {
        return [
            [[
                'id',
                'merchant_id',
                'paysystem_id',
                'paysystem_config_id',
                'billing_id',
                'created_at',
                'title',
                'label',
                'data',
                'price',
                'is_paid',
                'user_id',
                'is_mail_send',
            ], 'safe'],
        ];
    }

    /**
     * @param array $fields
     * @return \common\models\MerchantRequest
     */
    public static function add($fields)
    {
        if (!isset($fields['created_at'])) {
            $fields['created_at'] = time();
        }
        if (!isset($fields['is_paid'])) {
            $fields['is_paid'] = 0;
        }
        $item = new static($fields);
        $item->save();
        $item->id = self::getDb()->lastInsertID;

        return $item;
    }

    /**
     * @return \common\models\BillingMain
     */
    public function getBilling()
    {
        return BillingMain::findOne($this->billing_id);
    }

    /**
     * @return \common\models\avatar\UserBillMerchant
     */
    public function getMerchant()
    {
        return UserBillMerchant::findOne($this->merchant_id);
    }

    /**
     * Заменяет операцию по счету на котором стоим мерчант на свою 'Оплата по заказу #' . $this->id . ', ' . $this->title
     * Ставить флаг что заказ оплачен
     * Отправляет уведомление об успешной оплате по почте
     * Отправляет уведомление об успешной оплате по HTTP
     *
     * @param \common\models\piramida\WalletSourceInterface $source
     * @return mixed
     *
     * @throws
     */
    public function success($source)
    {
        if ($this->is_paid == 0) {
            $this->trigger(self::EVENT_BEFORE_SUCCESS);
            $transaction = \Yii::$app->db->beginTransaction();
            try {
                $billing = $this->getBilling();
                Yii::info($billing->getPaySystem()->code, 'avatar\\success');
                if ($billing->getPaySystem()->code == 'bit-coin') {
                    // если есть старая операция то убираю
                    try {
                        $operation = UserBillOperation::findOne([
                            'transaction' => $source->transaction['data']['hash'],
                            'bill_id'     => $this->merchant_id,
                        ]);
                        if (!is_null($operation)) {
                            $operation->message = 'Оплата по заказу #' . $this->id . ', ' . $this->title;
                            $operation->save();
                        }
                    } catch (\Exception $e) {
                        $operation = UserBillOperation::add([
                            'transaction' => $source->transaction['data']['hash'],
                            'bill_id'     => $this->merchant_id,
                            'message'     => 'Оплата по заказу #' . $this->id . ', ' . $this->title,
                            'type'        => UserBillOperation::TYPE_IN,
                            'created_at'  => (int)(new \DateTime($source->transaction['data']['first_seen_at']))->format('U'),
                        ]);
                        $operation->save();
                    }
                }
                $this->is_paid = 1;
                if (!$this->save()) {
                    Yii::warning(self::className() . ' error save ' . __METHOD__ . ' ' . VarDumper::dumpAsString($this->errors), 'avatar\\save');
                }

                // отправляю уведомление
                $merchant = UserBillMerchant::findOne($this->merchant_id);
                if ($merchant->is_email) {
                    if ($this->is_mail_send == 0) {
                        $email = $merchant->is_email_email;
                        if (!is_null($email)) {
                            if ($email != '') {
                                $billingTo = UserBill::findOne($this->merchant_id);
                                $ret = Application::mail(
                                    $email,
                                    'Поступили деньги на счет по мерчанту #' . $this->merchant_id,
                                    'merchant_in',
                                    [
                                        'merchant'   => $merchant,
                                        'billingTo'  => $billingTo,
                                        'request'    => $this,
                                    ]
                                );
                                if ($ret) {
                                    $this->is_mail_send = 1;
                                    if (!$this->save()) {
                                        Yii::warning(self::className() . ' cant save ' . __METHOD__ . ':'.__LINE__ . ' ' . VarDumper::dumpAsString($this->errors), 'avatar\\save');
                                    }
                                }
                            }
                        }
                    }
                }
                if ($merchant->is_http) {
                    $address = $merchant->is_http_address;
                    $secret = $merchant->secret;
                    if (!is_null($address)) {
                        if ($address != '') {
                            $client = new Client();
                            try {
                                $result = $client
                                    ->post(
                                        $address,
                                        [
                                            'id'           => $this->id,
                                            'created_at'   => $this->created_at,
                                            'paid_at'      => time(),
                                            'data'         => $this->data,
                                            'label'        => $this->label,
                                            'price'        => $this->price,
                                            'secret'       => $secret,
                                            'currency'     => $this->currency,
                                            'paysystem_id' => $this->paysystem_id,
                                            'transaction'  => Json::encode(['txid' => $source->transaction['data']['hash']]),
                                        ]
                                    )
                                    ->send()
                                ;
                            } catch (\Exception $e) {
                                Yii::warning(VarDumper::dumpAsString($address), 'avatar\\common\\models\\MerchantRequest::success::httpSend::error');
                            }
                        }
                    }
                }

                if (false) {
                    $transaction->rollBack();
                    throw new Exception('Не удалось сохранить запрос на вывод');
                } else {
                    $transaction->commit();
                    $this->trigger(self::EVENT_AFTER_SUCCESS);
                }
            } catch (\Exception $e) {
                $transaction->rollBack();

                throw $e;
            }
        }
        return true;
    }

    public function getPaymentBitCoin()
    {
        return PaymentBitCoin::findOne(['billing_id' => $this->billing_id]);
    }
}