<?php
/**
 * http://dimsemenov.com/plugins/magnific-popup/documentation.html
 */

namespace common\assets;

use yii\helpers\VarDumper;
use yii\web\AssetBundle;
use yii\web\View;

/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since  2.0
 */
class HighLightJs extends AssetBundle
{
    public $sourcePath = '@vendor/highlightjs/highlight.js/src';
    public $css      = [
        'default.css',
    ];
    public $js       = [
        'highlight.js',
    ];
    public $jsOptions = [
        'position' => View::POS_HEAD,
    ];
    public $depends  = [
        'yii\web\JqueryAsset',
    ];

    public function init()
    {
        return \Yii::$app->view->registerJs('hljs.initHighlightingOnLoad();', View::POS_HEAD);
    }
}
