<?php
namespace common\assets\InlineEdit;

use yii\web\AssetBundle;

class Asset extends AssetBundle
{
    public $sourcePath = '@common/assets/InlineEdit/source';

    public $js = [
        'jquery.editable.min.js',
        'my.js',
    ];

    public $css = [
    ];

    public $depends = [
        '\yii\web\JqueryAsset',
    ];
}