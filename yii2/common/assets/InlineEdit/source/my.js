var InlineEdit = {
    init: function () {
        $('.js-editable').editable({
            callback : function( data ) {
                if ( data.content ) {
                    var id;
                    for (var key in data) {
                        // этот код будет вызван для каждого свойства объекта
                        // ..и выведет имя свойства и его значение
                        if (key == "$el") {
                            var d = data[key];
                            id = d.data('id');
                        }
                    }
                    ajaxJson({
                        url: '/cabinet-school-pages-constructor/b11-save',
                        data: {
                            content: data.content,
                            id: id
                        },
                        success: function(ret) {

                        }
                    })
                }
                if ( data.fontSize ) {
                    // the font size has changed
                }
            }
        });
    }
};
