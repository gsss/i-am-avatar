<?php
namespace common\assets;

use yii\web\AssetBundle;

class Covervid extends AssetBundle
{
    public $sourcePath = '@vendor/stefanerickson/covervid';

    public $js = [
        'covervid.min.js',
    ];

    public $css = [
    ];

    public $depends = [
        'yii\web\JqueryAsset',
    ];
}