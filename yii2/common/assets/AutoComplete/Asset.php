<?php


namespace common\assets\AutoComplete;

use yii\web\AssetBundle;
use Yii;

/**
 */
class Asset extends AssetBundle
{
    public $sourcePath = '@vendor/xcash/bootstrap-autocomplete/dist/latest';
    public $css      = [
    ];
    public $js       = [
        'bootstrap-autocomplete.min.js'
    ];
    public $depends  = [
        'yii\web\JqueryAsset',
        'yii\bootstrap\BootstrapPluginAsset',
    ];
}
