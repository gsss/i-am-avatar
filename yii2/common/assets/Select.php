<?php
namespace common\assets;

use yii\web\AssetBundle;

class Select extends AssetBundle
{
    public $sourcePath = '@vendor/snapappointments/bootstrap-select/dist';

    public $js = [
        'js/bootstrap-select.min.js',
        'js/i18n/defaults-ru_RU.min.js',
    ];

    public $css = [
        'css/bootstrap-select.min.css'
    ];

    public $depends = [
    ];
}