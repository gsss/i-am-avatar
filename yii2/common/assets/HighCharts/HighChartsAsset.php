<?php

namespace common\assets\HighCharts;

use yii\helpers\ArrayHelper;
use yii\web\AssetBundle;
use yii\web\View;

class HighChartsAsset extends AssetBundle
{
    public $sourcePath = '@common/assets/HighCharts/web';
    public $depends = [
        'yii\web\JqueryAsset',
    ];

    public $js = [
        'js/highcharts.js',
    ];
    
    /**
     * Registers additional JavaScript files required by the widget.
     *
     * @param array $scripts list of additional JavaScript files to register.
     * @return $this
     */
    public function withScripts($scripts = null)
    {
        // use unminified files when in debug mode
        $ext = YII_DEBUG ? 'src.js' : 'js';

        // add files
        if ($scripts) {
            foreach ($scripts as $script) {
                $this->js[] = "js/$script.$ext";
            }
        }

        return $this;
    }
}