<?php

namespace common\assets\HighCharts;

use yii\helpers\ArrayHelper;
use yii\web\AssetBundle;
use yii\web\View;

class Asset extends AssetBundle
{
    public $sourcePath = '@common/assets/HighCharts/web';
    public $js = [
        'js/highcharts.js',
    ];
}