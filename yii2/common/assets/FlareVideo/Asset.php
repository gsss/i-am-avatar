<?php

namespace common\assets\FlareVideo;

use yii\helpers\ArrayHelper;
use yii\web\AssetBundle;
use yii\web\View;

class Asset extends AssetBundle
{
    public $sourcePath = '@common/assets/FlareVideo/maccman-flarevideo-ee31f65';
    public $js = [
        'js/highcharts.js',
    ];
}