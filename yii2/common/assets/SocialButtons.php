<?php

namespace common\assets;

use yii\web\AssetBundle;

class SocialButtons extends AssetBundle
{

    public $sourcePath = '@vendor/kni-labs/rrssb';
    public $js = [
        'js/rrssb.min.js',
    ];
    public $css = [
        'css/rrssb.css'
    ];
    public $depends = [
        'yii\web\JqueryAsset',
    ];
}