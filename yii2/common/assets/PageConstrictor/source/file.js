

/**
 *
 * @param type_id int идентификатор типа блока
 * @param functionSetFields function функция расстановка полей после чтения в форму
 * @param functionGetFields function функция для сбора полей для сохранения из формы
 */
function blockSettingsInit(type_id, functionSetFields, functionGetFields) {

    var selector = '#modalEdit' + type_id + ' .buttonSave';

    $(selector).click(function(e) {
        var type = $(this).data('type_id');
        var modal = $('#modalEdit' + type);
        var id = modal.data('id');
        ajaxJson({
            url: '/cabinet-school-pages-constructor/block-save',
            data: {
                id: id,
                form: functionGetFields(modal, type_id)
            },
            success: function(ret) {
                window.location.reload();
            }
        });
    });

    selector = '.buttonSettings[data-type="' + type_id + '"]';
    $(selector).click(function(e) {
        // Идентификатор блока контента
        var id = $(this).data('id');
        var type = $(this).data('type');
        var modal = $('#modalEdit' + type);
        modal.data('id', id);
        ajaxJson({
            url: '/cabinet-school-pages-constructor/block-get',
            data: {
                id: id
            },
            success: function(ret) {
                functionSetFields(modal, type, ret);
                $('#modalEdit' + type).modal();
            }
        });
    });

}

