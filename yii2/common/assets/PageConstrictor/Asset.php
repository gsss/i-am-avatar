<?php
namespace common\assets\PageConstrictor;

use yii\web\AssetBundle;

class Asset extends AssetBundle
{
    public $sourcePath = '@common/assets/PageConstrictor/source';

    public $js = [
        'file.js',
    ];

    public $css = [
    ];

    public $depends = [
    ];
}