<?php

namespace common\assets\GantelaLayout;

use yii\helpers\ArrayHelper;
use yii\web\AssetBundle;
use yii\web\View;

class NProgress extends AssetBundle
{

    public $js = [
        '/admin/vendors/nprogress/nprogress.js',
    ];
    public $css = [
        '/admin/vendors/nprogress/nprogress.css',
    ];
    public $depends = [
        '\yii\web\JqueryAsset',
        '\common\assets\GantelaLayout\BootstrapAsset',
    ];
}