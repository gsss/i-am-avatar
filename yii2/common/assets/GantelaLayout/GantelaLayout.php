<?php

namespace common\assets\GantelaLayout;

use yii\helpers\ArrayHelper;
use yii\web\AssetBundle;
use yii\web\View;

class GantelaLayout extends AssetBundle
{

    public $css = [
        '/admin/vendors/bootstrap/dist/css/bootstrap.min.css',
        '/admin/vendors/font-awesome/css/font-awesome.min.css',
        '/admin/vendors/nprogress/nprogress.css',
        '/admin/vendors/iCheck/skins/flat/green.css',
        '/admin/build/css/custom.css',
    ];

    public $js = [
        '/admin/vendors/bootstrap/dist/js/bootstrap.min.js',
        '/admin/vendors/fastclick/lib/fastclick.js',
        '/admin/vendors/nprogress/nprogress.js',
        '/admin/vendors/iCheck/icheck.min.js',
        '/admin/build/js/custom.js',
    ];
    public $depends = [
        '\yii\web\JqueryAsset',
        '\yii\web\YiiAsset',
    ];
}