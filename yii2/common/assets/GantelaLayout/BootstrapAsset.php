<?php

namespace common\assets\GantelaLayout;

use yii\helpers\ArrayHelper;
use yii\web\AssetBundle;
use yii\web\JqueryAsset;
use yii\web\View;

class BootstrapAsset extends AssetBundle
{

    public $css = [
        '/admin/vendors/bootstrap/dist/css/bootstrap.min.css',
    ];

    public $js = [
        '/admin/vendors/bootstrap/dist/js/bootstrap.min.js',
    ];

    public $depends = [
        '\yii\web\JqueryAsset',
    ];
}