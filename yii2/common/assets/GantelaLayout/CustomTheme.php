<?php

namespace common\assets\GantelaLayout;

use yii\helpers\ArrayHelper;
use yii\web\AssetBundle;
use yii\web\View;

class CustomTheme extends AssetBundle
{

    public $js = [
        '/admin/build/js/custom.min.js',
    ];
    public $css = [
        '/admin/build/css/custom.min.css',
    ];

    public $depends = [
        '\yii\web\JqueryAsset',
        '\common\assets\GantelaLayout\BootstrapAsset',
    ];
}