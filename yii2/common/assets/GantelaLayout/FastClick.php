<?php

namespace common\assets\GantelaLayout;

use yii\helpers\ArrayHelper;
use yii\web\AssetBundle;
use yii\web\View;

class FastClick extends AssetBundle
{

    public $js = [
        '/admin/vendors/fastclick/lib/fastclick.js',
    ];
    public $depends = [
        '\yii\web\JqueryAsset',
        '\common\assets\GantelaLayout\BootstrapAsset',
    ];
}