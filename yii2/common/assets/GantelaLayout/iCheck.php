<?php

namespace common\assets\GantelaLayout;

use yii\helpers\ArrayHelper;
use yii\web\AssetBundle;
use yii\web\View;

class iCheck extends AssetBundle
{

    public $js = [
        '/admin/vendors/iCheck/icheck.min.js',
    ];
    public $css = [
        '/admin/vendors/iCheck/skins/flat/green.css',
    ];
    public $depends = [
        '\yii\web\JqueryAsset',
        '\common\assets\GantelaLayout\BootstrapAsset',
    ];
}