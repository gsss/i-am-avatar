<?php

namespace common\assets\GantelaLayout;

use yii\helpers\ArrayHelper;
use yii\web\AssetBundle;
use yii\web\View;

class FontAwesome extends AssetBundle
{
    public $css = [
        '/admin/vendors/font-awesome/css/font-awesome.min.css',
    ];

    public $depends = [
        '\yii\web\JqueryAsset',
        '\common\assets\GantelaLayout\BootstrapAsset',
    ];
}