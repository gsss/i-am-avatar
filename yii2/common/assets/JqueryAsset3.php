<?php

namespace common\assets;

use yii\web\AssetBundle;

class JqueryAsset3 extends AssetBundle
{

    public function init()
    {
        \Yii::$app->view->registerJsFile('https://code.jquery.com/jquery-3.3.1.slim.min.js', [
            'integrity'   => 'sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo',
            'crossorigin' => 'anonymous',
        ]);
    }


}