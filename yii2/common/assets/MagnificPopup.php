<?php
/**
 * http://dimsemenov.com/plugins/magnific-popup/documentation.html
 */

namespace common\assets;

use yii\helpers\VarDumper;
use yii\web\AssetBundle;

/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since  2.0
 */
class MagnificPopup extends AssetBundle
{
    public $sourcePath = '@vendor/dimsemenov/magnific-popup/dist';
    public $css      = [
        'magnific-popup.css',
    ];
    public $js       = [
        'jquery.magnific-popup.js',
    ];
    public $depends  = [
        'yii\web\JqueryAsset',
    ];
}
