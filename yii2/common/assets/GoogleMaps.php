<?php
namespace common\assets;

use yii\web\AssetBundle;

class GoogleMaps extends AssetBundle
{
    public $js = [
        '//maps.googleapis.com/maps/api/js?v=3.exp&signed_in=true&libraries=places&key=AIzaSyBWCm-y1qnF2mPQEbm0vOvGcZ39MFWt4vo',
    ];
    public $css = [
    ];
    public $depends = [
    ];
}