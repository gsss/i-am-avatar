<?php
namespace common\assets\SocketIO;

use yii\web\AssetBundle;


/**

 */
class Asset extends AssetBundle
{
    public static function getHost()
    {
        $serverName = 'http://chat.qualitylive.su/';
        if (YII_ENV_TEST) $serverName = 'http://stage.neiro-n.com:3000';
        if (YII_ENV_PROD && YII_DEBUG) $serverName = 'http://stage.neiro-n.com:3000';
        if (YII_ENV_DEV) $serverName = 'http://localhost:3000';

        return $serverName;
    }

    public function init()
    {
        $serverName = self::getHost();

        \Yii::$app->view->registerJsFile($serverName . '/socket.io/socket.io.js', ['depends' => ['yii\web\JqueryAsset']]);
    }

}