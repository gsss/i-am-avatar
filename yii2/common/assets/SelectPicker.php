<?php
namespace common\assets;

use yii\web\AssetBundle;

class SelectPicker extends AssetBundle
{
    public $sourcePath = '@vendor/biggora/bootstrap-select/js';

    public $js = [
        'bootstrap-select.min.js',
    ];

    public $css = [
    ];

    public $depends = [
    ];
}