<?php


namespace common\assets\AutoComplete2;

use yii\web\AssetBundle;
use Yii;

/**
 */
class Asset extends AssetBundle
{
    public $sourcePath = '@common/assets/AutoComplete2/src/dist/latest';
    public $css      = [
    ];
    public $js       = [
        'bootstrap-autocomplete.min.js'
    ];
    public $depends  = [
        'yii\web\JqueryAsset',
        'yii\bootstrap\BootstrapPluginAsset',
    ];
}
