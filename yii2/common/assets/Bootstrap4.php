<?php

namespace common\assets;

use yii\web\AssetBundle;

class Bootstrap4 extends AssetBundle
{

    public function init()
    {
        \Yii::$app->view->registerCssFile('https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css', [
            'integrity'   => 'sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO',
            'crossorigin' => 'anonymous',
        ]);
        \Yii::$app->view->registerJsFile('https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js', [
            'integrity'   => 'sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49',
            'crossorigin' => 'anonymous',
            'depends' => [
                '\common\assets\JqueryAsset3'
            ],
        ]);
        \Yii::$app->view->registerJsFile('https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js', [
            'integrity'   => 'sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy',
            'crossorigin' => 'anonymous',
            'depends' => [
                '\common\assets\JqueryAsset3'
            ],
        ]);
    }


}