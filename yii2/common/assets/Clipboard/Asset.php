<?php
/**
 * @link      http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license   http://www.yiiframework.com/license/
 */

namespace common\assets\Clipboard;

use yii\web\AssetBundle;

/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since  2.0
 */
class Asset extends AssetBundle
{
    public $sourcePath = '@common/assets/Clipboard/src/dist';
    public $css = [
    ];
    public $js = [
        'clipboard.min.js'
    ];
    public $depends = [
        '\yii\web\JqueryAsset',
    ];
}
