<?php
/**
 * Created by PhpStorm.
 * User: Бог-Творец
 * Date: 09.09.2016
 * Time: 0:26
 */

namespace common\assets\SlideShow;



use yii\bootstrap\Html;
use yii\helpers\ArrayHelper;

class Core extends Object
{
    public static function item($small, $big, $options = [])
    {
        self::registerAsset();
        $aOptions = ArrayHelper::getValue($options, 'a', []);
        $imgOptions = ArrayHelper::getValue($options, 'img', []);
        $aOptions['rel'] = 'lightbox[example]';
        $aOptions['onclick'] = 'return hs.expand(this)';
        if (!isset($aOptions['class'])) {
            $aOptions['class'] = 'highslide';
        } else {
            $aOptions['class'] .= ' highslide';
        }

        return Html::a(Html::img($small, $imgOptions), $big, $aOptions);
    }

    public static function registerAsset()
    {
        \common\assets\SlideShow\Asset::register(\Yii::$app->view);
    }
} 