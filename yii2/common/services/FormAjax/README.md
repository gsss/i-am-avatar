# FormAjax

Сервис для yii2 для для валидации и исполнения формы по AJAX.
Делает вместро submit с обновлением страницы - ajax запрос и вызов `success`.

## Концепция

Чтобы форма передавалась по AJAX.

После возвращения выводились ошибки.

Исключение составляет в том что нельзя по AJAX передать файл, или сложно, поэтому применяется виджет для онлайн загрузки где по AJAX передается только файл.

\common\services\FormAjax\ActiveRecord::attributeWidgets - здесь указываются виджеты и их настроки для вывода в форме

Код для сохранения:
```php
if (Yii::$app->request->isPost) {
    if ($model->load(Yii::$app->request->post()) && $model->validate()) {
        $s = $model->save();

        return self::jsonSuccess();
    } else {
        $fields = [];
        foreach ($model->attributes as $k => $v) {
            $fields[$k] = Html::getInputId($model, $k);
        }

        return self::jsonErrorId(102, [
            'errors' => $model->errors,
            'fields' => $fields,
        ]);
    }
}
```

## Пример использования

## Использование если на странице форма добавления

В контроллере прописываю в функции `actions()`

```php
class CabinetBlogController extends CabinetBaseController
{
    public function actions()
    {
        return [
            'add' => [
                'class'    => '\common\services\FormAjax\DefaultFormAdd',
                'model'    => '\avatar\models\forms\BlogItem',
                'view'     => '@avatar/views/blog/add',
            ],
        ];
    }
}
```

Параметр `view` не обязателен, если не указан то используется идентификатор действия (action).



## Событийная модель для ActiveRecord

Widget:onBeforeLoad выполняется до   $model->load()
Widget:onAfterLoad выполняется после $model->load()

Widget:onAfterLoadDb выполняется после $model->findOne()

Widget:onBeforeInsert выполняется до   $model->save()
Widget:onAfterInsert выполняется после $model->save()

Widget:onBeforeUpdate выполняется до   $model->save()
Widget:onAfterUpdate выполняется после $model->save()

Widget:onBeforeValidate выполняется до   $model->validate()
Widget:onAfterValidate выполняется после $model->validate()

Widget:onBeforeDelete выполняется до   $model->delete()
Widget:onAfterDelete выполняется после $model->delete()

Всего два сценария показа формы
1
$model->findOne()

2
$model->findOne()
$model->load()
$model->validate()
$model->save() - если прошла валидация




# Виджеты

Виджеты предназначены для того чтобы можно было в них вызывать события для обработки постуивших данных.
Например в виджете с датой указывается дата в формате dd.mm.yyyy для валидации она должна быть в формате dd.mm.yyyy, 
при сохранении она должна быть в виде yyyy-mm-dd