<?php
/**
 * Created by PhpStorm.
 * User: Бог-Творец
 * Date: 04.02.2017
 * Time: 0:33
 */

namespace common\services;


use common\models\UserDevice;
use cs\services\VarDumper;


class Mobile extends Object
{
    /**
     * Выдает модель телефона основываясь на переменной `$_SERVER['HTTP_USER_AGENT']`
     *
     * @return string Если не найдена модель то выдает ''
     */
    public static function getModel()
    {
        $model = \Yii::$app->request->getUserAgent();
        $model = explode('(', $model);
        $isModel = false;
        if (count($model) > 1) {
            $model = explode(')', $model[1]);
            if (count($model) > 1) {
                $model = explode(')', $model[0]);
                $model = $model[0];
                $isModel = true;
            }
        }
        if (!$isModel) {
            return '';
        }

        return $model;
    }

}