<?php
/**
 * Created by PhpStorm.
 * User: Бог-Творец
 * Date: 04.02.2017
 * Time: 0:33
 */

namespace common\services;


use common\models\UserDevice;
use cs\services\File;
use cs\services\VarDumper;
use yii\base\Component;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;
use yii\httpclient\Client;

class OneC extends Component
{

    /** @var  string ключ по которому формируется подпись */
    public $key;
    public $url;


    /**
     * Отправляет команду $path на сервер $server с параметрами $params
     *
     * @param null $server
     * @param $path
     * @param array $params
     * @param array $options
     * - files - array \cs\services\File[] - файлы для отправки
     *
     * @return \yii\httpclient\Response
     */
    public function _post($server = null, $path, $params = [], $options = [])
    {
        if (is_null($server)) $server = $this->getServer();

        if (!isset($params['signature'])) {
            $params['signature'] = \common\models\UserAvatar::hashPassword($this->key);
        }

        $provider = new Client(['baseUrl' => $server]);
        $request = $provider->post($path, $params);
        if (isset($options['files'])) {
            $c = 0;
            /** @var \cs\services\File $file */
            foreach ($options['files'] as $file) {
                if ($file->isPath()) {
                    $request->addFile('file' . $c, $file->path);
                }
                if ($file->isContent()) {
                    $request->addFileContent('file' . $c, $file->content);
                }
                $c++;
            }
        }
        $response = $request->send();

        return $response;
    }

    /**
     * Отправляет команду $path на сервер $server с параметрами $params
     *
     * @param null $server
     * @param $path
     * @param array $params
     * @param array $options
     * - files - array \cs\services\File[] - файлы для отправки
     *
     * @return \yii\httpclient\Response
     * @throws
     */
    public function post($server = null, $path, $params = [], $options = [])
    {
        $response = $this->_post($server, $path, $params, $options);
        \Yii::info(\yii\helpers\VarDumper::dumpAsString($response), 'avatar\\post');
        $data = Json::decode($response->content);
        if ($response->statusCode != 200) {
            throw new \Exception('Status code = '.$response->statusCode);
        }

        return $data;
    }

}