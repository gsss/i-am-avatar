<?php
/**
 * Created by PhpStorm.
 * User: Ra-m-ha
 * Date: 15.08.2019
 * Time: 12:28
 */

namespace common\services;

class TildaApi extends \yii\base\Component
{
    public $apiUrl = 'http://api.tildacdn.info';
    public $publickey;
    public $secretkey;


    /**
     * Вызывает API и навешивает исключения в случае неудачи. Возвращает только положительный результат
     * Если код отличный от 200 то вызывается исключение
     * Возвращает значение $data['result']
     *
     * @param $method
     * @param $path
     * @param $params
     *
     * @return mixed
     * @throws
     */
    public function call($method, $path, $params)
    {
        $response = $this->_call($method, $path, $params);
        if ($response->statusCode != 200) {
            throw new Exception(\yii\helpers\Json::encode(['status' => $response->statusCode, 'message' => 'Сервер вернул не 200']));
        }
        try {
            $data = \yii\helpers\Json::decode($response->content);
        } catch (Exception $e) {
            throw new Exception(\yii\helpers\Json::encode(['message' => 'Json error']));
        }
        if (!isset($data['status'])) {
            throw new Exception(\yii\helpers\Json::encode(['message' => 'Не верный формат ответа в JSON от сервера']));
        }
        if ($data['status'] != 'FOUND') {
            throw new Exception(\yii\helpers\Json::encode(['message' => 'Сервер вернул ответ status='.$data['status']]));
        }

        return $data['result'];
    }

    /**
     * Вызывает API с подстановкой ключей
     *
     * @param $method
     * @param $path
     * @param $params
     *
     * @return \yii\httpclient\Response
     * @throws \yii\base\InvalidConfigException
     */
    private function _call($method, $path, $params)
    {
        $client = new \yii\httpclient\Client(['baseUrl' => $this->apiUrl]);
        $options = [
            'publickey' => $this->publickey,
            'secretkey' => $this->secretkey,
        ];
        $params = \yii\helpers\ArrayHelper::merge($params, $options);
        if (!in_array($method, ['get', 'post'])) throw new \yii\base\InvalidConfigException('$method может быть только [\'get\', \'post\']');
        if ($method == 'get') {
            $request = $client->get($path, $params);
        }
        if ($method == 'post') {
            $request = $client->post($path, $params);
        }
        $response = $request->send();

        return $response;
    }
}