<?php


namespace common\services;


use yii\base\Component;
use yii\helpers\Json;
use yii\httpclient\Client;

/**
 * Class Fns
 *
 * @see https://api-fns.ru/api_help
 *
 * @package common\services
 */
class Fns extends Component
{
    public $url = 'https://api-fns.ru/api';
    public $key;
    public $format = 'json';

    /**
     * @param $path
     * @param array $params
     *
     * @return \yii\httpclient\Response
     */
    public function _get($path, $params = [])
    {
        $client = $this->getProvider();
        $params['key'] = $this->key;

        $response = $client->get($path, $params)->send();

        return $response;
    }

    /**
     * Обрабатывает ответ
     *
     * @param string $path
     * @param array $params
     *
     * @return array
     *
     * @throws \Exception
     */
    public function get($path, $params = [])
    {
        $response = $this->_get($path, $params);
        if ($response->headers['http-code'] != 200) {
            throw new \Exception();
        }

        try {
            $data = Json::decode($response->content);
        } catch (\Exception $e) {
            throw $e;
        }

        return $data;
    }

    public function getProvider()
    {
        $client = new Client(['baseUrl' => $this->url]);

        return $client;
    }
}