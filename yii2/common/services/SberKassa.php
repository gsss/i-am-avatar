<?php


namespace common\services;


use yii\base\Component;
use yii\helpers\ArrayHelper;
use yii\httpclient\Client;

/**
 * @url https://cloud1.cloud999.ru/upload/cloud/16085/40867_R7nciAFeBL.pdf
 * @url https://cloud1.cloud999.ru/upload/cloud/16085/41100_oTl6CM2kEL.pdf
 * @url https://cloud1.cloud999.ru/upload/cloud/16085/41150_IehzmdUQCR.pdf
 */
class SberKassa extends Component
{
    public $url = 'https://securepayments.sberbank.ru/payment/rest';
//    public $url = 'https://3dsec.sberbank.ru/payment/rest';
    public $operator;
    public $api;
    public $password;
    public $secret;

    /**
     *
     * @return array
     * [
     *      'orderId' => 'd4dc594d-bdc3-7d0b-b2b0-fa4804b09de5'
     *      'formUrl' => 'https://3dsec.sberbank.ru/payment/merchants/sbersafe/payment_ru.html?mdOrder=d4dc594d-bdc3-7d0b-b2b0-fa4804b09de5'
     * ]
     */
    public function createPayment($billing, $description, $destinationName, $options)
    {
        $d = 'https://3dsec.sberbank.ru/payment/rest/register.do?
        amount=100&
        orderNumber=87654321&
        password=password&
        returnUrl=https://3dsec.sberbank.ru/payment/finish.html&
        userName=userName&
        jsonParams={"orderNumber":1234567890}&
        pageView=DESKTOP&
        expirationDate=2014-09-08T14:14:14&
        merchantLogin=merch_child&
        features=AUTO_PAYMENT';


        $client = new \yii\httpclient\Client([
            'baseUrl'        => $this->url,
            'responseConfig' => [
                'format' => \yii\httpclient\Client::FORMAT_JSON,
            ],
        ]);

        $params['amount']       = $billing->sum_after;
        $params['orderNumber']  = $billing->id;
        $params['password']     = $this->password;
        $params['userName']     = $this->api;
        $params['returnUrl']    = ArrayHelper::getValue($options, 'successUrl', '');

        $payment = $client
            ->post(
                'register.do',
                $params
            )
            ->send();

        if ($payment->headers['http-code'] != 200) {
            throw new \Exception('Код от SberAPI'.$payment->headers['http-code']);
        }

        return $payment->data;
    }

    /**
     * Сравнивает контрольную сумму $sum с контрольной суммой параметров $params используя симетричное шифрование
     * @param array  $params параметры Ключ значение
     * @param string $sum контрольная сумма от источника получения уведомления. От "Банка"
     * @return bool
     */
    public function isCheckSum($params, $sum)
    {
        ksort($params);
        $rows = [];
        foreach ($params as $k => $v) {
            if ($k != 'type') {
                $rows[] = $k . ';' . $v . ';';
            }
        }
        $data = join('', $rows);

        $key = $this->secret;
        $hmac = hash_hmac ( 'sha256' , $data , $key);

        return strtoupper($hmac) == $sum;
    }

}