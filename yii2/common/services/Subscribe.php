<?php


namespace common\services;

use common\models\school\School;
use common\models\subscribe\SubscribeMail;
use cs\Application;
use cs\services\VarDumper;
use yii\db\Query;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;

class Subscribe
{

    /**
     * Генерирует hash для ссылки отписки
     *
     * @param string $email почта клиента
     * @param integer $type тип рассылки \app\services\Subscribe::TYPE_*
     *
     * @return string
     *
     */
    public static function hashGenerate($email, $type)
    {
        return md5($email . '_' . $type);
    }

    /**
     * Проверяет hash для ссылки отписки
     *
     * @param string $email почта клиента
     * @param integer $type имп рассылки \app\services\Subscribe::TYPE_*
     * @param integer $hash
     *
     * @return boolean
     * true - верный
     * false - не верный
     */
    public static function hashValidate($email, $type, $hash)
    {
        return md5($email . '_' . $type) == $hash;
    }

    /**
     * Добавляет в очередь пакет писем
     *
     * @param array | string $emailList пакет писем (массив) или роль (строка)
     * @param string $subject           тема письма
     * @param string $view              представление котореое лежит относительно '@avatar/mail/html'
     * @param array $options            настройки для представления
     * @param array $layout             default 'layouts/html'
     *                                  <p><code>$layout</code> если это строка то то путь к обложке (layout) письма от
     *                                  пути '@avatar/mail' в конце по умолчанию добавляется '.php'. В итоге если
     *                                  <code>$layout</code> = <code>layouts/html</code> то это значит файлом обложки
     *                                  является <code>@avatar/mail/layouts/html.php</code>.</p>
     *                                  <p>если это массив то следующие параметры:</p>
     *
     * <p><code>layout</code> то же что и этот параметр в виде строки.</p>
     * <p><code>from_name</code> от кого имя</p>
     * <p><code>from_email</code> от кого почта</p>
     *
     * @return \common\models\subscribe\Subscribe
     */
    public static function sendArray($emailList, $subject, $view, $options = [], $layout = null)
    {
        // установка значений для функции
        {
            $from = \Yii::$app->params['mailer']['from'];
            $from_name = '';
            $from_email = '';
            foreach ($from as $m => $n) {
                $from_name = $n;
                $from_email = $m;
            }
            if (is_null($layout)) {
                $layout = [
                    'layout'     => 'layouts/html',
                    'from_name'  => $from_name,
                    'from_email' => $from_email,
                ];
            } else {
                if (!is_array($layout)) {
                    $layout = [
                        'layout'     => $layout,
                        'from_name'  => $from_name,
                        'from_email' => $from_email,
                    ];
                } else {
                    if (!isset($layout['layout'])) $layout['layout'] = 'layouts/html';
                    if (!isset($layout['from_name'])) $layout['from_name'] = $from_name;
                    if (!isset($layout['from_email'])) $layout['from_email'] = $from_email;
                }
            }
        }

        if (!is_array($emailList)) {
            $role = $emailList;
            $emailList = [];
            $ids = \Yii::$app->authManager->getUserIdsByRole($role);
            foreach ($ids as $id) {
                try {
                    $user = \common\models\UserAvatar::findOne($id);
                    $emailList[] = $user->email;
                } catch (\Exception $e) {
                    \Yii::warning('user id not found ' . $id, 'avatar\common\services\Subscribe::sendArray');
                }
            }
        }

        /** @var \yii\swiftmailer\Mailer $mailer */
        $mailer = \Yii::$app->mailer;
        $html = $mailer->render('html/' . $view, $options, $layout['layout']);

        $subscribe = \common\models\subscribe\Subscribe::add([
            'html'       => $html,
            'subject'    => $subject,
            'from_name'  => $layout['from_name'],
            'from_email' => $layout['from_email'],
        ]);

        foreach ($emailList as $mail) {
            SubscribeMail::add([
                'subscribe_id' => $subscribe->id,
                'mail'         => $mail,
            ]);
        }

        return $subscribe;
    }

    /**
     * Добавляет в очередь пакет писем с подстановками параметров школы
     *
     * @param School $school            объект школы
     * @param array | string $emailList пакет писем (массив) или роль (строка)
     * @param string $subject           тема письма
     * @param string $view              представление котореое лежит относительно '@avatar/mail/html'
     * @param array $options            настройки для представления
     *                                  - сюда подставляется значение 'school_subscribe_image' => school.subscribe_image если оно есть
     *                                    если оно задано явно, то не подставляется
     *                                  - mailer - не обязательное, если указано то указывает какой mailer использовать
     *
     * @param string $layout            default 'layouts/html'
     *                                  <p><code>$layout</code> если это строка то то путь к обложке (layout) письма от
     *                                  пути '@avatar/mail' в конце по умолчанию добавляется '.php'. В итоге если
     *                                  <code>$layout</code> = <code>layouts/html</code> то это значит файлом обложки
     *                                  является <code>@avatar/mail/layouts/html.php</code>.</p>
     *
     * @return \common\models\subscribe\Subscribe
     */
    public static function sendArraySchool($school, $emailList, $subject, $view, $options = [], $layout = null)
    {
        // установка значений для функции
        {
            $from = \Yii::$app->params['mailer']['from'];
            $from_name = '';
            $from_email = '';
            foreach ($from as $m => $n) {
                $from_name = $n;
                $from_email = $m;
            }
            if ($school->subscribe_from_name) $from_name = $school->subscribe_from_name;
            if (is_null($layout)) {
                $layout = 'layouts/html';
            }
        }

        if (!is_array($emailList)) {
            $role = $emailList;
            $emailList = [];
            $ids = \Yii::$app->authManager->getUserIdsByRole($role);
            foreach ($ids as $id) {
                try {
                    $user = \common\models\UserAvatar::findOne($id);
                    $emailList[] = $user->email;
                } catch (\Exception $e) {
                    \Yii::warning('user id not found ' . $id, 'avatar\common\services\Subscribe::sendArray');
                }
            }
        }

        if (!isset($options['school_subscribe_image'])) {
            if ($school->subscribe_image) {
                $options['school_subscribe_image'] = $school->subscribe_image;
            }
        }

        /** @var \yii\swiftmailer\Mailer $mailer */
        if (isset($options['mailer'])) {
            $mailer = $options['mailer'];
        } else {
            $mailer = \Yii::$app->mailer;
        }
        $html = $mailer->render('html/' . $view, $options, $layout);

        $subscribe = \common\models\subscribe\Subscribe::add([
            'html'       => $html,
            'subject'    => $subject,
            'from_name'  => $from_name,
            'from_email' => $from_email,
        ]);

        foreach ($emailList as $mail) {
            SubscribeMail::add([
                'subscribe_id' => $subscribe->id,
                'mail'         => $mail,
            ]);
        }

        return $subscribe;
    }
}