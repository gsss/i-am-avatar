<?php


namespace common\services;


use yii\base\Component;
use yii\httpclient\Client;

/**
 * @url https://kassa.yandex.ru/developers/using-api/testing
 */
class YandexKassa extends Component
{
    public $url = 'https://payment.yandex.net/api/v3';
    public $shopID;
    public $secret;

    /**
     * @param array $data
     * [
     * 'amount'       => [
     * 'value'    => 100.0,
     * 'currency' => 'RUB',
     * ],
     * 'confirmation' => [
     * 'type'       => 'redirect',
     * 'return_url' => 'http://zdravo.i-am-avatar.local/koop/return_url',
     * ],
     * 'capture'      => true,
     * 'description'  => 'Заказ №1',
     * ]
     *
     * @return array
     * [
     * 'id' => '26413069-000f-5000-9000-10f8079fbc84'
     * 'status' => 'pending'
     * 'paid' => false
     * 'amount' => [
     * 'value' => '100.00'
     * 'currency' => 'RUB'
     * ]
     * 'confirmation' => [
     * 'type' => 'redirect'
     * 'confirmation_url' => 'https://money.yandex.ru/api-pages/v2/payment-confirm/epl?orderId=26413069-000f-5000-9000-10f8079fbc84'
     * ]
     * 'created_at' => '2020-05-03T19:34:01.276Z'
     * 'description' => 'Заказ №1'
     * 'metadata' => []
     * 'recipient' => [
     * 'account_id' => '701775'
     * 'gateway_id' => '1712759'
     * ]
     * 'refundable' => false
     * 'test' => true
     * ]
     * @throws
     */
    public function createPatment($data)
    {
        $client = new \yii\httpclient\Client([
            'baseUrl'       => $this->url,
            'requestConfig' => [
                'format' => \yii\httpclient\Client::FORMAT_JSON
            ],
            'responseConfig' => [
                'format' => \yii\httpclient\Client::FORMAT_JSON
            ],
        ]);

        $payment = $client
            ->post(
                'payments',
                $data,
                [
                    'Authorization'   => 'Basic ' . base64_encode($this->shopID . ':' . $this->secret),
                    'Idempotence-Key' => \YandexCheckout\Helpers\UUID::v4(),
                ]
            )
            ->send();

        if ($payment->headers['http-code'] != 200) {
            throw new \Exception('Код от Yandex'.$payment->headers['http-code']);
        }

        return $payment->data;
    }

}