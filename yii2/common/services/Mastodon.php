<?php


namespace common\services;


use yii\base\Component;
use yii\httpclient\Client;

/**
 * @package common\services
 */
class Mastodon extends Component
{
    public $url;

    public $client_id;
    public $client_secret;
    public $vapid_key;
    public $access_token;


    /**
     * @param string $path
     * @param array $params
     * @return string
     * @throws
     */
    public function getAuth($path, $params)
    {
        $client = new Client(['baseUrl' => $this->url]);

        $response = $client
            ->get($path, $params)
            ->addHeaders([
                'Authorization' => 'Bearer ' . $this->access_token
            ])
            ->send();

        if ($response->statusCode != 200) {
            throw new \Exception('status = '.$response->statusCode);
        }

        return $response->content;
    }

    /**
     * @param string $path
     * @param array $params
     * @return string
     * @throws
     */
    public function postAuth($path, $params)
    {
        $client = new Client(['baseUrl' => $this->url]);

        $response = $client
            ->post($path, $params)
            ->addHeaders([
                'Authorization' => 'Bearer ' . $this->access_token
            ])
            ->send();

        if ($response->statusCode != 200) {
            throw new \Exception('status = '.$response->statusCode);
        }

        return $response->content;
    }

}