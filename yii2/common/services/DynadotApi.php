<?php


namespace common\services;


use yii\base\Component;
use yii\httpclient\Client;

/**
 * Class DynadotApi
 *
 * @package common\services
 * @url https://www.dynadot.com/ru/domain/api2.html
 */
class DynadotApi extends Component
{
    public $url = 'https://api.dynadot.com/api2.html';
    public $key = '647m83S6p9G808yr6O7b6D6v6m7G6M6n9R9F6YRa7E7X';

    /**
     * @param string $command 'search'| 'register'| 'delete'| 'renew'| 'get_ns'| 'set_ns'| 'set_renew_option'| 'set_folder'| 'is_processing'
     * @param array $params
     * @return string
     * @throws
     */
    public function command($command, $params)
    {
        $client = new Client(['baseUrl' => $this->url]);
        $params['key'] = $this->key;
        $params['command'] = $command;

        $response = $client->get('', $params)->send();
        if ($response->statusCode != 200) {
            throw new \Exception('status = '.$response->statusCode);
        }

        return $response->content;
    }

}