<?php
/**
 * Created by PhpStorm.
 * User: Бог-Творец
 * Date: 04.02.2017
 * Time: 0:33
 */

namespace common\services;


use common\models\UserDevice;
use cs\services\VarDumper;
use yii\base\Component;

use yii\helpers\Json;
use yii\httpclient\Client;

class CloudFlare extends Component
{
    public $apiUrl = 'https://api.cloudflare.com/client/v4';
    public $authEmail;
    public $authKey;

    /**
     * @param  string $zone    example `023e105f4ecef8ad9ca31a8372d0c353`
     * @param  string $name    max length: 255
     * @param  string $type    valid values: A, AAAA, CNAME, TXT, SRV, LOC, MX, NS, SPF, CERT, DNSKEY, DS, NAPTR,
     *                         SMIMEA, SSHFP, TLSA, URI
     * @param  string $content `127.0.0.1`
     * @param  array  $options
     *                         - ttl min value:120    max value:2147483647
     *                         - priority min value:0    max value:65535
     *                         - proxied valid values: (true,false)
     *
     * @return
     * {
     * "success": true,
     * "errors": [],
     * "messages": [],
     * "result": {
     * "id": "372e67954025e0ba6aaa6d586b9e0b59",
     * "type": "A",
     * "name": "example.com",
     * "content": "198.51.100.4",
     * "proxiable": true,
     * "proxied": false,
     * "ttl": 120,
     * "locked": false,
     * "zone_id": "023e105f4ecef8ad9ca31a8372d0c353",
     * "zone_name": "example.com",
     * "created_on": "2014-01-01T05:20:00.12345Z",
     * "modified_on": "2014-01-01T05:20:00.12345Z",
     * "data": {}
     * }
     * }
     */
    public function createDns($zone, $name, $type, $content, $options = [])
    {
        $options['name'] = $name;
        $options['type'] = $type;
        $options['content'] = $content;

        return $this->_post('zones/' . $zone . '/dns_records', $options);
    }

    public function _post($path, $params = [])
    {
        $headers = [
            'X-Auth-Email' => $this->authEmail,
            'X-Auth-Key'   => $this->authKey,
            'Content-Type' => 'application/json',
        ];

        $provider = new Client(['baseUrl' => $this->apiUrl]);
        $response = $provider->post($path, Json::encode($params), $headers)->send();


        return $response;
    }

    public function _delete($path, $params = [])
    {
        $headers = [
            'X-Auth-Email' => $this->authEmail,
            'X-Auth-Key'   => $this->authKey,
            'Content-Type' => 'application/json',
        ];

        $provider = new Client(['baseUrl' => $this->apiUrl]);
        $response = $provider->delete($path, Json::encode($params), $headers)->send();


        return $response;
    }

    public function _get($path, $params = [])
    {
        $headers = [
            'X-Auth-Email' => $this->authEmail,
            'X-Auth-Key'   => $this->authKey,
            'Content-Type' => 'application/json',
        ];

        $provider = new Client(['baseUrl' => $this->apiUrl]);
        $response = $provider->get($path, $params, $headers)->send();

        return $response;
    }


}