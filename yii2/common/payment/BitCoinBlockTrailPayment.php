<?php

namespace common\payment;

use app\commands\MoneyRateController;
use app\models\Orders;
use app\models\Transaction;
use Blocktrail\SDK\BlocktrailSDK;
use Blocktrail\SDK\Wallet;
use common\components\marketing\Marketing;
use common\models\avatar\UserBill;
use common\models\Billing;
use common\models\Config;
use common\models\Countries;
use common\models\Currency;
use common\models\InstalmentPlan;
use common\models\PaymentBitCoin;
use common\services\Debugger;
use common\services\Security;
use phpDocumentor\Reflection\DocBlock\Tag\VarTag;
use Yii;
use common\components\payments\classes\BasePayment;
use common\models\PaymentSystem;
use yii\base\Exception;

use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Json;
use yii\helpers\Url;
use Endroid\QrCode\QrCode;

use pjhl\multilanguage\helpers\Languages;
use yii\helpers\VarDumper;
use yii\httpclient\Client;

/**
 * Класс который совмещает в себе две функции
 * TODO: Нужно разбить класс
 * 1. предоставляет интерфесс общения с сервисом BlockTrail
 * 2. генерирует форму и осуществляет подтверждение по Billing
 *
 * Class BitCoinBlockTrailPayment
 * @package common\payment
 */
class BitCoinBlockTrailPayment extends Object
{
    public $url = 'https://api.blocktrail.com/v3/BTC';

    public $apiKey       = 'd7215444e4c5cf68c6b652005605a404dbf87ef3';
    public $apiKeySecret = '367b4123860182271b7ba52bcaef8d8e9314555d';

    public $destinationAddressArray;
    public $callback                = 'https://avatarnetwork.io/shop-order/success';
    public $isTest                  = false;

    public $wallets = [
        'main' => [
            'identifier' => 'main',
            'password'   => 'dram10081',
        ],
        'test' => [
            'identifier' => 'test1',
            'password'   => 'dram10081',
        ],
    ];

    public $bill_id;

    /** @var  \Blocktrail\SDK\BlocktrailSDK */
    public $client;

    /**
     * Initializes the widget.
     */
    public function init()
    {
        $params = ArrayHelper::getValue(Yii::$app->params, 'pay-system.BitCoin');
        if (!is_null($params)) {
            foreach ($params as $k => $v) {
                if ($k != 'class') {
                    $this->$k = $v;
                }
            }
        }

        parent::init();
    }

    /**
     * Генерирует форму HTML: картинка и текст с инструкцией
     *
     * @param float $sum сумма в биткойнах
     * @param string $action сумма в биткойнах
     * @param string $address адрес выставленного счета
     *
     * @return string HTML картинка и текст с инструкцией
     * @throws \Exception
     */
    public function form($sum, $action, $address)
    {
        $sum = Yii::$app->formatter->asDecimal($sum, 8);
        $amount = $sum;

        if (!YII_ENV_DEV) {
            $content = (new \Endroid\QrCode\QrCode())
                ->setText('bitcoin:' . $address . '?amount=' . $amount)
                ->setSize(180)
                ->setPadding(20)
                ->setErrorCorrection('high')
                ->setForegroundColor(['r' => 0, 'g' => 0, 'b' => 0, 'a' => 0])
                ->setBackgroundColor(['r' => 255, 'g' => 255, 'b' => 255, 'a' => 0])
                ->setLabelFontSize(16)
                ->setImageType(\Endroid\QrCode\QrCode::IMAGE_TYPE_PNG)
                ->get('png');
        } else {
            $content = file_get_contents(Yii::getAlias('@webroot' . '/images/qrTest.png'));
        }

        $src = 'data:image/png;base64,' . base64_encode($content);
        $rows = [];
        $rows[] = Html::tag('p', Html::img($src), ['class' => 'text-center']);
        $rows[] = Html::tag('p', Yii::t('app', 'Переведите {amount} BTC на кошелек {address}. Биткойны будут зачислены в течение 10 минут.', [
            'amount'  => Html::tag('code', $sum),
            'address' => Html::tag('code', $address),
        ]), ['class' => 'text-center']);

        $params = [
            'billing_id' => $this->bill_id,
            'address'    => $address,
            'amount'     => $amount,
            'action'     => $action,
            'type'       => 'bitcoin',
        ];
        $url = new \cs\services\Url($this->callback, $params);

        // создаю подписчика
        $hookName = 'bill_' . Security::generateRandomString(10);
        if (!YII_ENV_DEV) {
            // создаю подписчика
            $newWebhook = $this->getClient()->setupWebhook($url->__toString(), $hookName);
            // подписываюсь на приход денег в кошелек
            $this->getClient()->subscribeAddressTransactions($hookName, $address, 6);
        }
        $model = new PaymentBitCoin([
            'action'     => $action,
            'billing_id' => $this->bill_id,
            'address'    => $address,
            'time_add'   => time(),
            'sum_before' => $amount,
            'sum_after'  => $amount,
            'web_hook'   => $hookName,
        ]);
        $ret = $model->save();
        if (!$ret) {
            Yii::warning('Не удалось сохранить платеж \common\models\PaymentBitCoin' . VarDumper::dumpAsString($model->errors));
        }

        return join("\n", $rows);
    }

    /**
     * Возвращает закешированный экземляр класса
     *
     *
     * @return BlocktrailSDK
     */
    public function getClient()
    {
        if (empty($this->client)) {
            $this->client = $this->_getClient();
        }

        return $this->client;
    }

    /**
     * Создает экземляр класса клиента
     *
     * @return BlocktrailSDK
     */
    public function _getClient()
    {
        return new BlocktrailSDK($this->apiKey, $this->apiKeySecret, 'BTC', $this->isTest);
    }

    public function initWallet($identifier = null, $password = null)
    {
        if (!empty($identifier) && !empty($password)) {
            $config = [
                'identifier' => $identifier,
                'password'   => $password,
            ];
        } else {
            throw new \Exception('Не указан кошелек');
        }

        return $this->getClient()->initWallet($config['identifier'], $config['password']);
    }

    /**
     * Проверяет было ли 6 подтверждений транзакции
     * и проверяет достаточно ли денег прислано было в соответствии с тем что указано в $billing
     *
     *
     * Yii::$app->request->rawBody
     * [
     * "network" => "BTC",
     * "data" => [
     * "hash" => "8fa29a1db30aceed2ec5afb2bcdf6196207975780b4ca5f3f1d6313cab855fe7",
     * "time" => "2014-08-25T09:29:28+0000",
     * "confirmations" => 3948,
     *
     * "...etc..." => "...etc..."
     * ],
     * "addresses" => [
     * "1qMBuZnrmGoAc2MWyTnSgoLuWReDHNYyF" => −2501000
     * ],
     * ]
     * @param \common\models\piramida\Billing $billing
     *
     * @return bool
     */
    public function confirm($billing)
    {
        $address = Yii::$app->request->get('address');
        $amount = Yii::$app->request->get('amount');
        $amountSatoshi = $amount * 100000000;

        $rawBody = Yii::$app->request->rawBody;
        $rawBody = str_replace('\\"', '"', $rawBody);
        $rawBody = json_decode($rawBody);
        $rawBody = ArrayHelper::toArray($rawBody);
        Yii::info($rawBody['data']['confirmations'], 'avatar\\payments\\bitcoin\\confirmations');
        if ($rawBody['data']['confirmations'] < 6) return false;
        $isSuccess = false;
        foreach ($rawBody['data']['outputs'] as $o) {
            if ($o['address'] == $address) {
                if ($o['value'] >= $amountSatoshi) {
                    return true;
                } else {
                    // не достаточно средств
                    return false;
                }
            }
        }
    }

    /**
     * Вызывается сразу по приходу POST для определения billing_id
     *
     * @return integer
     */
    public function getBillingId()
    {
        return Yii::$app->request->get('billing_id');
    }

    private function getDestinationAddress()
    {
        if ($this->isTest) {
            $address = $this->destinationAddressArray['test'];
        } else {
            $address = $this->destinationAddressArray['main'];
        }

        return $address;
    }
}