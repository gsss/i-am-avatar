<?php
return [
    'adminEmail'   => 'admin@example.com',

    // кошелек для оплаты за выпуск токена
    'createTokenWalletPayment'      => '0xfa54ee6f5e332012a75acc500011bdc7ac9ad511',

    // Ip где находятся все школы, применяется для CloudFlare
    'schoolIp'                      => '89.40.117.156',

    'isSeeds'                       => 1,
    'supportEmail'                  => 'support@example.com',
    'isExternalSubscribeReady'      => 1,

    'mailer' => [
        'from' => ['no-reply@qualitylive.su' => 'Платформа Качество Жизни'],
    ],

    // время жизни токена для сброса пароля
    'user.passwordResetTokenExpire' => 3600,


    'mobile' => [
        'phoneTokenSavePeriod' => 60 * 60 * 24 * 100, // срок действия phone-token для быстрого логина, сек
    ],
    'google-authorisation-code' => [
        'server' => 'i-am-avatar.com'
    ],

    'unlockKey'                  => env('UNLOCK_KEY'),
    'subscribe_unsubscribe_pass' => env('SUBSCRIBE_UNSUBSCRIBE_PASS'),
    'root_url'                   => env('ROOT_URL'),

    'google' => [
        'plugins' => [
            'reCaptcha' => [
                'secretKey' => env('GOOGLE_SECRET_KEY'),
                'key'       => env('GOOGLE_KEY'),
            ],
        ],
    ],
];
