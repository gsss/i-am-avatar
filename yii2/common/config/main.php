<?php

$config = [
    'vendorPath' => dirname(dirname(__DIR__)) . '/vendor',
    'aliases' => [
        '@bower' => env('BOWER_PATH'),
        '@npm'   => '@vendor/npm-asset',
    ],
    'components' => [
        'DynadotApi'       => [
            'class' => '\common\services\DynadotApi',
        ],
        'TildaApi'         => [
            'class' => '\common\services\TildaApi',
        ],
        'AvatarCloud' => [
            'class' => '\common\services\AvatarCloud',
            'url'   => env('AVATAR_CLOUD_URL'),
            'key'   => env('AVATAR_CLOUD_KEY'),
        ],
        'sms'              => [
            'class'    => '\common\components\sms\IqSms',
            'gate'     => env('SMS_GATE'),
            'login'    => env('SMS_LOGIN'),
            'password' => env('SMS_PASSWORD'),
        ],
        'Fns' => [
            'class' => '\common\services\Fns',
            'key'   => env('FNS_KEY'),
        ],
        'YandexKassa'      => [
            'class' => '\common\services\YandexKassa',
        ],
        'CloudFlare'       => [
            'class'     => '\common\services\CloudFlare',
            'authKey'   => env('CLOUD_FLARE_AUTH_KEY'),
            'authEmail' => env('CLOUD_FLARE_AUTH_EMAIL'),
        ],
        'CustomizeService' => [
            'class' => '\avatar\services\CustomizeService',
        ],
        'onlineManager'    => [
            'class'           => 'common\components\OnlineManager',
            'maxTimeToUpdate' => 2,
            'sessionTime'     => 3600,
            'clearTime'       => 10,
        ],
        'telegram'         => [
            'class'    => '\aki\telegram\Telegram',
            'botToken' => env('TELEGRAM_BOT_TOKEN'),
        ],
        'cacheFile'        => [
            'class' => 'yii\caching\FileCache',
        ],
        'monitoring'       => [
            'class' => 'common\components\Monitoring',
        ],
        'monitoringDb'     => [
            'class' => 'common\components\MonitoringDb',
        ],
        'monitoringParams' => [
            'class' => 'common\components\MonitoringParams',
        ],
        'urlManager'       => [
            'enablePrettyUrl'     => true,
            'showScriptName'      => false,
            'enableStrictParsing' => true,
            'suffix'              => '',
            'hostInfo'            => env('URL_MANAGER_HOST_INFO', 'https://www.i-am-avatar.com'),
        ],
        'db'               => [
            'class'               => 'yii\db\Connection',
            'dsn'                 => env('MYSQL_DSN_MAIN'),
            'username'            => env('MYSQL_USER'),
            'password'            => env('MYSQL_PASSWORD'),
            'charset'             => 'utf8',
            // Enable cache schema
            'enableSchemaCache'   => false,
            // Duration of schema cache.
            'schemaCacheDuration' => 3600,
            // Name of the cache component used to store schema information
            'schemaCache'         => 'cache',
        ],
        'dbStatistic'      => [
            'class'               => 'yii\db\Connection',
            'charset'             => 'utf8',
            'dsn'                 => env('MYSQL_DSN_STAT'),
            'username'            => env('MYSQL_USER'),
            'password'            => env('MYSQL_PASSWORD'),
            // Enable cache schema
            'enableSchemaCache'   => false,
            // Duration of schema cache.
            'schemaCacheDuration' => 3600,
            // Name of the cache component used to store schema information
            'schemaCache'         => 'cache',
        ],
        'dbWallet'         => [
            'class'               => 'yii\db\Connection',
            'charset'             => 'utf8',
            'dsn'                 => env('MYSQL_DSN_WALLET'),
            'username'            => env('MYSQL_USER'),
            'password'            => env('MYSQL_PASSWORD'),
            // Enable cache schema
            'enableSchemaCache'   => false,
            // Duration of schema cache.
            'schemaCacheDuration' => 3600,
            // Name of the cache component used to store schema information
            'schemaCache'         => 'cache',
        ],

        'dbInfo'           => [
            'class'               => 'yii\db\Connection',
            'charset'             => 'utf8',
            'dsn'                 => env('MYSQL_DSN_INFO'),
            'username'            => env('MYSQL_USER'),
            'password'            => env('MYSQL_PASSWORD'),
            // Enable cache schema
            'enableSchemaCache'   => false,
            // Duration of schema cache.
            'schemaCacheDuration' => 3600,
            // Name of the cache component used to store schema information
            'schemaCache'         => 'cache',
        ],
        'authManager' => [
            'class'          => 'yii\rbac\PhpManager',
            'assignmentFile' => '@frontend/rbac/assignments.php',
            'ruleFile'       => '@frontend/rbac/rules.php',
            'itemFile'       => '@frontend/rbac/items.php',
        ],

        // Языки
        'i18n'             => [
            'translations' => [
                'app*' => [
                    'class'                 => '\common\components\DbMessageSourceSky',
                    'cache'                 => 'cacheLanguages',
                    'forceTranslation'      => true,
                    'enableCaching'         => true,
                    'sourceLanguage'        => 'ru',
                    'on missingTranslation' => ['\common\components\DbMessageSourceSky', 'handleMissingTranslation'],
                ],
                '*'    => [
                    'class'                 => '\common\components\DbMessageSourceSky',
                    'cache'                 => 'cacheLanguages',
                    'forceTranslation'      => true,
                    'enableCaching'         => true,
                    'sourceLanguage'        => 'ru',
                    'on missingTranslation' => ['\common\components\DbMessageSourceSky', 'handleMissingTranslation'],
                ],
            ],
        ],
        'log'              => [
            'targets' => [
                [
                    'class'  => '\yii\log\FileTarget',
                    'levels' => [
                        'warning',
                        'error',
                    ],
                    'except' => [
                        'yii\web\HttpException:404',
                        'yii\web\HttpException:403',
                        'cs\web\Exception',
                    ],
                ],
            ],
        ],
        'mailer'           => [
            'class'            => 'yii\swiftmailer\Mailer',
            'useFileTransport' => false,
            'transport'        => [
                'class'    => 'Swift_SmtpTransport',
                'host'     => env('MAILER_TRANSPORT_HOST'),
                'port'     => env('MAILER_TRANSPORT_PORT'),
                'username' => env('MAILER_TRANSPORT_USERNAME'),
                'password' => env('MAILER_TRANSPORT_PASSWORD'),
            ],
            'viewPath'         => '@avatar/mail',
        ],
        'deviceDetect'     => [
            'class' => '\common\components\DeviceDetect',
        ],
        'BTC'              => [
            'class' => '\common\components\providers\BTC',
        ],
        'eth'              => [
            'class'        => '\common\components\providers\ETH',
            'apiUrl'       => 'https://atlantida.io',
            'apiPublicKey' => 'fb60449a-c3b2-4493-a267-0aa1165277df',
        ],
    ],
];

if (env('MEMCACHE_ENABLED', false)) {
    $config['components']['cache'] = [
        'keyPrefix'    => 'i-am-avatar',
        'class'        => 'yii\caching\MemCache',
        'useMemcached' => true,
        'servers'      => [
            [
                'host' => env('MEMCACHE_ENABLED', 'localhost'),
                'port' => env('MEMCACHE_PORT', 11211),
            ],
        ],
    ];
    $config['components']['cacheLanguages'] = [
        'keyPrefix'    => 'i-am-avatar-languages',
        'class'        => 'yii\caching\MemCache',
        'useMemcached' => true,
        'servers'      => [
            [
                'host' => env('MEMCACHE_ENABLED', 'localhost'),
                'port' => env('MEMCACHE_PORT', 11211),
            ],
        ],
    ];
} else {
    $config['components']['cache'] = [
        'keyPrefix'    => 'i-am-avatar',
        'class'        => 'yii\caching\FileCache',
    ];
    $config['components']['cacheLanguages'] = [
        'keyPrefix'    => 'i-am-avatar-languages',
        'class'        => 'yii\caching\FileCache',
    ];
}

return $config;