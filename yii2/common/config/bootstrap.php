<?php
Yii::setAlias('@api', dirname(dirname(__DIR__)) . '/api');
Yii::setAlias('@apiprocessing', dirname(dirname(__DIR__)) . '/api-processing');
Yii::setAlias('@backend', dirname(dirname(__DIR__)) . '/backend');
Yii::setAlias('@console', dirname(dirname(__DIR__)) . '/console');
Yii::setAlias('@common', dirname(dirname(__DIR__)) . '/common');
Yii::setAlias('@avatar', dirname(dirname(__DIR__)) . '/avatar-bank');
Yii::setAlias('@frontend', dirname(dirname(__DIR__)) . '/avatar-bank');
Yii::setAlias('@csRoot', dirname(dirname(__DIR__)) . '/common/app');
Yii::setAlias('@cs', dirname(dirname(__DIR__)) . '/common/app');
Yii::setAlias('@school', dirname(dirname(__DIR__)) . '/school');
Yii::setAlias('@web', dirname(dirname(__DIR__)) . '/public_html');
Yii::setAlias('@webroot', dirname(dirname(__DIR__)) . '/public_html');
Yii::setAlias('@upload', dirname(dirname(__DIR__)) . '/public_html/upload');
