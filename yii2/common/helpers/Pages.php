<?php

namespace common\helpers;


class Pages
{

    public static function field($id, $name)
    {

    }

    public static function hasRoute1123123($item, $route)
    {
        if (isset($item['route'])) {
            if ($item['route'] == $route) return true;
        }

        if (isset($item['items'])) {
            foreach($item['items'] as $i) {
                if ($i['route'] == $route) {
                    return true;
                }
            }
        }

        if (\yii\helpers\ArrayHelper::keyExists('urlList', $item)) {
            foreach($item['urlList'] as $i => $v) {
                switch($i) {
                    case 'startsWith':
                        if (\yii\helpers\StringHelper::startsWith($route, $v)) return true;
                        break;
                    case 'controller':
                        $arr = explode('/', $route);
                        if ($arr[0] == $v) return true;
                        break;
                }
            }
        }

        return false;
    }

    public static function liMenu1123122($route, $name = null, $options = [])
    {
        if (is_array($route)) {
            $arr = [];
            foreach ($route as $route1 => $name1) {
                $arr[] = self::liMenu1123122($route1, $name1);
            }
            return join("\n", $arr);
        }
        if (\Yii::$app->requestedRoute == $route) {
            $options['class'] = 'active';
        }

        return \yii\helpers\Html::tag('li', \common\helpers\Html::a($name, [$route]), $options);
    }
} 