<?php

namespace common\interfaceList;

interface BillingInterface
{
    /**
     * Возвращает идентификатор компании к которому относится счет
     * @return int
     */
    function getCompanyId();
}