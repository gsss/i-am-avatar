<?php

namespace cs;

use common\components\providers\ETH;
use common\models\Config;
use common\models\school\PotokUser3Link;
use cs\services\SitePath;
use cs\services\VarDumper;
use Yii;
use yii\helpers\ArrayHelper;
use yii\helpers\FileHelper;
use yii\helpers\Json;

class Application extends \yii\web\Application
{
    protected $timeZone2;
    protected $timeZone;

    /**
     * @param $value
     *
     * @return bool
     */
    public static function isInteger($value)
    {
        if (is_integer($value)) return true;
        if (is_array($value)) return false;
        if (preg_match('/[0-9-]/', $value)) return true;

        return false;
    }

    /**
     * Если null => true
     * Если is_string => Если длина == 0 ? true : false
     * Если is_object => false
     * Если is_array => Если длина == 0 ? true : false
     *
     * @param $val
     *
     * @return bool
     */
    public static function isEmpty($val)
    {
        if (is_null($val)) return true;
        if (is_string($val)) {
            if (strlen($val) == 0) return true;
            return false;
        }
        if (is_object($val)) return false;
        if (is_array($val)) {
            if (count($val) == 0) return true;
            return false;
        }

        return false;
    }

    /**
     * @param int|double|string $value
     *
     * @return bool
     */
    public static function isDouble($value)
    {
        if (is_integer($value)) return true;
        if (self::isInteger($value)) return true;
        if (preg_match('/[0-9-.]/', $value)) return true;

        return false;
    }

    /**
     * Отправляет письмо в формате html и text
     *
     * @param string $email        куда
     * @param string $subject      тема
     * @param string $view         шаблон, лежит в /mail/html и /mail/text
     * @param array $options       параметры для шаблона
     * @param array $attaches      файлы для прикрепления, должны быть класса \Swift_Attachment
     * @param array | string $from от кого отправляется письмо, если не указано то указывается
     *                \Yii::$app->params['mailer']['from']
     *
     * @return boolean
     */
    public static function mail($email, $subject, $view, $options = [], $layout = null, $attaches = [], $from = null)
    {
        $e = Yii::$app->request->cookies->get('mailTest');
        if (!is_null($e)) {
            if ($e->value != '') {
                $email = $e->value;
            }
        }

        if (is_null($from)) {
            $from = \Yii::$app->params['mailer']['from'];
        }
        if (is_null($layout)) {
            $layout = ['text' => 'layouts/text', 'html' => 'layouts/html'];
        }
        $text = '';

        /** @var \yii\swiftmailer\Mailer $mailer */
        $mailer = Yii::$app->mailer;
        $html = $mailer->render('html/' . $view, $options, $layout['html']);

        $o = \Yii::$app->mailer
            ->compose()
            ->setFrom($from)
            ->setTo($email)
            ->setHtmlBody($html)
            ->setSubject($subject);


        if ($text) $o->setTextBody($text);
        if ($attaches) {
            foreach ($attaches as $path => $name) {
                $c = file_get_contents($path);
                $o->attachContent($c, ['fileName' => $name]);
            }
        }
        Yii::$app->monitoringParams->inc('mail');

        $result = $o->send();
        if (!$result) Yii::warning('message not send email=' . $email . ' html=' . $html, 'avatar/mail');

        return $result;
    }

    /**
     * Кеширует даные при помощи $functionGet
     *
     * @param string $key           ключ для кеша
     * @param \Closure $functionGet функция для получения данных кеша
     * @param mixed $options        данные которыебудут переданы в функцию $functionGet
     * @param bool $isUseCache      использовать кеш?
     *
     * @return mixed
     */
    public static function cache($key, $functionGet, $options = null, $isUseCache = true)
    {
        if ($isUseCache) {
            $cache = Yii::$app->cache->get($key);
            if ($cache === false) {
                $cache = $functionGet($options);
                Yii::$app->cache->set($key, $cache);
            }
            return $cache;
        } else {
            return $functionGet($options);
        }
    }

    /**
     * Вызывает контракт на чтение
     * Аккаунт для чтения берет из конфига параметра 'eth.main.wallet.contract.read'
     * Если параметр не задан то будет вызвано исключение
     *
     * @param string $address      адрес контракта
     * @param string $abi          интерфейс контракта
     * @param string $functionName функция контракта
     * @param array $params        параметры функции
     *
     * @return string
     * @throws
     */
    public static function readContract($address, $abi, $functionName, $params = [])
    {
        $key = 'eth.main.wallet.contract.read';
        $json = Config::get($key);
        if (is_null($json)) {
            throw new \Exception('Не задан параметр eth.main.wallet.contract.read');
        }
        $data = Json::decode($json);
        $user = $data[0];
        $password = $data[1];
        $provider = new ETH();

        return $provider->contract($user, $password, $address, $abi, $functionName, $params, 'false');
    }

    public static function subscribeSite($id, $isAvatar = true)
    {
        if (!PotokUser3Link::find()->where([
            'user_root_id' => $id,
            'potok_id'     => 2,
        ])->exists()) {
            PotokUser3Link::add([
                'user_root_id' => $id,
                'is_avatar'    => $isAvatar ? 1 : 0,
                'potok_id'     => 2,
            ]);
        }
    }
}