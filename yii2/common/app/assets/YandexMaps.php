<?php
namespace cs\assets;

use yii\web\AssetBundle;

class YandexMaps extends AssetBundle
{
    public $js = [
        'https://api-maps.yandex.ru/2.1/?lang=ru_RU',
    ];
    public $css = [
    ];
    public $depends = [
    ];
}