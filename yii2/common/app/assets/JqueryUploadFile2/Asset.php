<?php


namespace cs\assets\JqueryUploadFile2;

use yii\web\AssetBundle;
use Yii;

/**
 */
class Asset extends AssetBundle
{
    public $sourcePath = '@csRoot/assets/JqueryUploadFile2/source';
    public $css      = [
    ];
    public $js       = [
        'jquery.uploadfile.js'
    ];
    public $depends  = [
        'cs\assets\JqueryForm\Asset',
    ];
}
