<?php


namespace cs\assets\JqueryUploadFile;

use yii\web\AssetBundle;
use Yii;

/**
 */
class Asset extends AssetBundle
{
    public $sourcePath = '@csRoot/assets/JqueryUploadFile/source';
    public $css      = [
    ];
    public $js       = [
        'jquery.uploadfile.js'
    ];
    public $depends  = [
        'cs\assets\JqueryForm\Asset',
    ];
}
