<?php
/**
 * Created by PhpStorm.
 * User: Дмитрий
 * Date: 14.09.2015
 * Time: 16:19
 */

namespace cs\Widget\BreadCrumbs;


use cs\helpers\Html;

use yii\base\BaseObject;
use yii\helpers\Url;

class BreadCrumbs extends BaseObject
{
    /** @var  array
     * [
     *   <[
     *      'label' =>
     *      'url' => array|string
     *   ] | string> , ...
     * ]
     */
    public $items;

    public $home = [
        'name' => '<i class="glyphicon glyphicon-home"></i>',
        'url'  => '/',
    ];

    public function run()
    {
        $this->registerAssets();

        $items = [
            Html::a($this->home['name'], $this->home['url'], ['class' => 'btn btn-default']),
            '<div class="btn btn-default">...</div>',
        ];
        foreach ($this->items as $item) {
            if (is_array($item)) {
                $url = $item['url'];
                $name = $item['label'];
            } else {
                $url = 'javascript:void();';
                $name = $item;
            }
            $items[] = Html::a(Html::tag('div', $name), Url::to($url), ['class' => 'btn btn-default']);
        }
        $items = join('', $items);

        return Html::tag('div', $items, ['class' => 'btn-group btn-breadcrumb']);
    }

    private function registerAssets()
    {
        \cs\assets\BreadCrumbs\Asset::register(\Yii::$app->view);
    }

    public static function widget($config = [])
    {
        $class = new self($config);

        return $class->run();
    }
} 