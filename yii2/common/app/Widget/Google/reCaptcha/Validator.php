<?php

namespace cs\Widget\Google\reCaptcha;

/**
 * https://developers.google.com/recaptcha/docs/verify#api-request
 */

use cs\services\VarDumper;
use yii\authclient\InvalidResponseException;
use yii\helpers\ArrayHelper;

class Validator extends \yii\validators\Validator
{
    public function validateAttribute($model, $attribute)
    {
        if (!YII_ENV_PROD) return;
        $url = 'https://www.google.com/recaptcha/api/siteverify';

        $params = [
            'secret'   => ArrayHelper::getValue(\Yii::$app->params, 'google.plugins.reCaptcha.secretKey'),
            'response' => \Yii::$app->request->post('g-recaptcha-response'),
            'remoteip' => $_SERVER['REMOTE_ADDR'],
        ];
        $curlOptions = [
            CURLOPT_HTTPHEADER     => [],
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_URL            => $url,
        ];
        $curlOptions[CURLOPT_POST] = true;
        $curlOptions[CURLOPT_HTTPHEADER] = ['Content-type: application/x-www-form-urlencoded'];
        $curlOptions[CURLOPT_POSTFIELDS] = http_build_query($params, '', '&', PHP_QUERY_RFC3986);
        $curlResource = curl_init();
        foreach ($curlOptions as $option => $value) {
            curl_setopt($curlResource, $option, $value);
        }
        $response = curl_exec($curlResource);
        $responseHeaders = curl_getinfo($curlResource);
        // check cURL error
        $errorNumber = curl_errno($curlResource);
        $errorMessage = curl_error($curlResource);

        curl_close($curlResource);

        if ($errorNumber > 0) {
            $this->addError($model, $attribute, 'Curl error requesting "' .  $url . '": #' . $errorNumber . ' - ' . $errorMessage);
            return;
        }
        if (strncmp($responseHeaders['http_code'], '20', 2) !== 0) {
            $this->addError($model, $attribute, 'Код отличный от 200');
            return;
        }
        $data = json_decode($response);
        if ($data->success != true) {
            $this->addError($model, $attribute, 'Вы не прошли проверку');
            return;
        }
    }

    public function clientValidateAttribute($model, $attribute, $view)
    {
    }

}