<?php

namespace cs\Widget\Google\reCaptcha;

use Yii;
use yii\base\Exception;
use yii\base\InvalidConfigException;
use yii\base\Model;
use yii\base\Widget;
use yii\db\Query;
use yii\helpers\ArrayHelper;
use yii\helpers\StringHelper;
use yii\helpers\Url;
use yii\helpers\Html;
use yii\helpers\Json;
use yii\helpers\VarDumper;
use yii\web\UploadedFile;
use yii\imagine\Image;
use yii\web\JsExpression;
use Imagine\Image\ManipulatorInterface;
use cs\base\BaseForm;
use cs\Widget\FileUploadMany\ModelFiles;
use cs\services\UploadFolderDispatcher;
use cs\services\SitePath;
use yii\jui\InputWidget;

/**
 * Настройки
 * google.plugins.reCaptcha.secretKey
 *
 * $field->widget('cs\Widget\FileUploadMany\FileUploadMany', [
 *
 * ]);
 *
 * $options = [
 * 'serverName'
 * ];
 *
 * $model->$fieldName = [
 * ['file_path', 'file_name'],
 * ];
 */
class reCaptcha extends \yii\widgets\InputWidget
{
    public $key;
    public $secretKey;

    /**
     * Initializes the widget.
     */
    public function init()
    {
        $params = ArrayHelper::getValue(Yii::$app->params, 'google.plugins.reCaptcha');
        if (!is_null($params)) {
            foreach ($params as $k => $v) {
                if ($k != 'class') {
                    $this->$k = $v;
                }
            }
        }

        parent::init();
    }

    /**
     * рисует виджет
     */
    public function run()
    {
        $this->registerClientScript();

        return Html::tag('div', null, [
            'class'        => 'g-recaptcha',
            'data-sitekey' => $this->key,
        ]);
    }

    /**
     * Registers the needed JavaScript.
     */
    public function registerClientScript()
    {
        Asset::register($this->view);
    }

    /**
     * Возвращает опции для виджета
     *
     * @return array the options
     */
    protected function getClientOptions()
    {
        return [
        ];
    }

}
