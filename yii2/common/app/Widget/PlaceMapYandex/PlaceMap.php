<?php

namespace cs\Widget\PlaceMapYandex;

use Yii;
use yii\base\InvalidConfigException;
use yii\helpers\ArrayHelper;
use yii\helpers\StringHelper;
use yii\helpers\Url;
use yii\helpers\Html;
use yii\helpers\Json;
use yii\helpers\VarDumper;
use yii\widgets\InputWidget;
use yii\web\UploadedFile;
use yii\web\JsExpression;
use yii\imagine\Image;
use Imagine\Image\ManipulatorInterface;
use cs\base\BaseForm;
use CreditSystem\App as CApplication;

/**
 * class PlaceMap
 *
 * Опции виджета:
 * ```php
 * $options = [
 *     'style' => [
 *                'input' => ''
 *                'divMap' => ''
 *                ]
 * ]);
 * ```
 *
 * Использование:
 *
 * ```php
 * $form->input($model, 'name')->widget(PlaceMap::className(), $options)
 * ```
 */
class PlaceMap extends InputWidget
{
    private $fieldId;
    private $fieldName;

    private $fieldIdMap;
    private $fieldNameMap;

    private $fieldIdLat;
    private $fieldNameLat;

    private $fieldIdLng;
    private $fieldNameLng;

    public $style = [
        'input'  => ['class' => 'form-control'],
        'divMap' => [],
    ];

    /**
     * Initializes the widget.
     */
    public function init()
    {
        parent::init();

        $formName = $this->model->formName();
        $this->getId();

        $this->fieldId = strtolower($formName . '-' . $this->attribute);
        $this->fieldName = $formName . '[' . $this->attribute . ']';

        $this->fieldIdMap = strtolower($formName . '-' . $this->attribute . '-map');
        $this->fieldNameMap = $formName . '[' . $this->attribute . '-map' . ']';

        $this->fieldIdLng = strtolower($formName . '-' . $this->attribute . '-lng');
        $this->fieldNameLng = $formName . '[' . $this->attribute . '-lng' . ']';

        $this->fieldIdLat = strtolower($formName . '-' . $this->attribute . '-lat');
        $this->fieldNameLat = $formName . '[' . $this->attribute . '-lat' . ']';
    }

    /**
     * Renders the widget.
     */
    public function run()
    {
        $this->registerClientScript();
        $html = [];

        if ($this->hasModel()) {
            $lng = null;
            $lat = null;
            $attribute = $this->attribute;
            if ($this->model->$attribute) {
                $lat = ArrayHelper::getValue($this->model->$attribute, 'lat');
                $lng = ArrayHelper::getValue($this->model->$attribute, 'lng');
                $address = ArrayHelper::getValue($this->model->$attribute, 'address');
            } else { // для обеспечения совместимости со старыми версиями
                $fieldNameLng = $this->attribute . '_lng';
                $fieldNameLat = $this->attribute . '_lat';

                if (isset($this->model->$fieldNameLng)) {
                    $lng = $this->model->$fieldNameLng;
                }
                if (isset($this->model->$fieldNameLat)) {
                    $lat = $this->model->$fieldNameLat;
                }
            }


            // hidden
            $html[] = Html::input('hidden', $this->fieldNameLng, $lng, ['id' => $this->fieldIdLng]);
            $html[] = Html::input('hidden', $this->fieldNameLat, $lat, ['id' => $this->fieldIdLat]);

            // input
            $inputAttributes = ArrayHelper::getValue($this->style, 'input', []);
            $inputAttributes = ArrayHelper::merge($inputAttributes, ['id' => $this->fieldId]);
            $html[] = Html::input('text', $this->fieldName, null, $inputAttributes);

            // divMap
            $divMapAttributes = ArrayHelper::getValue($this->style, 'divMap', []);
            $divMapAttributes = ArrayHelper::merge([
                'id'    => 'map',
                'style' => 'width: 800px; height: 300px;'
            ], $divMapAttributes);
            $html[] = Html::tag('div', null, [
                'id'    => 'map',
                'style' => 'width: 100%; height: 300px; border-radius:5px;margin-top: 10px;'
            ]);
        } else {

        }

        return join("\r\n", $html);
    }

    /**
     * Registers the needed JavaScript.
     */
    public function registerClientScript()
    {
        Asset::register($this->getView());
        $options = ["'{$this->fieldId}'"];

        $lng = null;
        $lat = null;
        $attribute = $this->attribute;
        if ($this->model->$attribute) {
            $lat = ArrayHelper::getValue($this->model->$attribute, 'lat');
            $lng = ArrayHelper::getValue($this->model->$attribute, 'lng');
            $address = ArrayHelper::getValue($this->model->$attribute, 'address');
        } else { // для обеспечения совместимости со старыми версиями
            $fieldNameLng = $this->attribute . '_lng';
            $fieldNameLat = $this->attribute . '_lat';
            if (isset($this->model->$fieldNameLng)) {
                $lng = $this->model->$fieldNameLng;
            }
            if (isset($this->model->$fieldNameLat)) {
                $lat = $this->model->$fieldNameLat;
            }
        }

        if (!is_null($lng) && !is_null($lat)) {
            $optionsExt = [
                'lat' => $lat,
                'lng' => $lng,
            ];
            $options[] = json_encode($optionsExt);
        }
        $optionsString = join(',', $options);

//        $this->getView()->registerJs("PlaceMap.init({$optionsString});");
        $this->getView()->registerJs(<<<JS
// Как только будет загружен API и готов DOM, выполняем инициализацию
ymaps.ready(init);

function init () {

    // При возникновении событий, изменяющих состояние карты,
    // ее параметры передаются в адресную строку браузера (после символа #).
    // При загрузке страницы карта устанавливается в состояние,
    // соответствующее переданным параметрам.
    // http://.../savemap.html#type=hybrid&center=93.3218,60.0428&zoom=12
    var myMap = new ymaps.Map("map", {
            center: [48.707787, 44.515933], // Волгоград
            zoom: 8
        }),
        myPlacemark1 = new ymaps.Placemark([48.82399, 44.768619], {
            balloonContent: 'Первый',
            myId: 'first'
        }),
        myPlacemark2 = new ymaps.Placemark([48.489177, 44.661502], {
            balloonContent: 'Второй',
            myId: 'second'
        }),
        myPlacemarkCollection = new ymaps.GeoObjectCollection(),
        lastOpenedBalloon = false;

    myPlacemarkCollection
        .add(myPlacemark1)
        .add(myPlacemark2);

    myMap.geoObjects.add(myPlacemarkCollection);

    myMap.controls
        .add('typeSelector')
        // .add('smallZoomControl')
        ;

    // Обработка событий карты:
    // - boundschange - изменение границ области показа;
    // - type - изменение типа карты;
    // - balloonclose - закрытие балуна.
    myMap.events.add(['boundschange', 'typechange', 'balloonclose'], setLocationHash);

    // Обработка событий открытия балуна для любого элемента
    // коллекции.
    // В данном случае на карте находятся только метки одной коллекции.
    // Чтобы обработать события любых геообъектов карты можно использовать
    // myMap.geoObjects.events.add(['balloonopen'],function (e) { ...
    myPlacemarkCollection.events.add(['balloonopen'], function (e) {
        lastOpenedBalloon = e.get('target').properties.get('myId');
        setLocationHash();
    });

    setMapStateByHash();

    // Получение значение параметра name из адресной строки
    // браузера.
    function getParam (name, location) {
        location = location || window.location.hash;
        var res = location.match(new RegExp('[#&]' + name + '=([^&]*)', 'i'));
        return (res && res[1] ? res[1] : false);
    }

    // Передача параметров, описывающих состояние карты,
    // в адресную строку браузера.
    function setLocationHash () {
        var params = [
            'type=' + myMap.getType().split('#')[1],
            'center=' + myMap.getCenter(),
            'zoom=' + myMap.getZoom()
        ];
        if (myMap.balloon.isOpen()) {
            params.push('open=' + lastOpenedBalloon);
        }
        window.location.hash = params.join('&');
    }

    // Установка состояния карты в соответствии с переданными в адресной строке
    // браузера параметрами.
    function setMapStateByHash () {
        var hashType = getParam('type'),
            hashCenter = getParam('center'),
            hashZoom = getParam('zoom'),
            open = getParam('open');
        if (hashType) {
            myMap.setType('yandex#' + hashType);
        }
        if (hashCenter) {
            myMap.setCenter(hashCenter.split(','));
        }
        if (hashZoom) {
            myMap.setZoom(hashZoom);
        }
        if (open) {
            myPlacemarkCollection.each(function (geoObj) {
                var id = geoObj.properties.get('myId');
                if (id == open) {
                    geoObj.balloon.open();

                }
            });
        }
    }
    
    myMap.events.add('click', function (e) {
        // Получение координат щелчка
        var coords = e.get('coords');
        $('#profilehumandesigncalc-point-lat').val(coords[0]);
        $('#profilehumandesigncalc-point-lng').val(coords[1]);
        $('#profilehumandesigncalc-point').val(coords.join(', '));
    });
}
JS
);
    }

    /**
     * Returns the options for the captcha JS widget.
     *
     * @return array the options
     */
    protected function getClientOptions()
    {
        return [];
    }

    /**
     *
     * @param array $field
     * @param \yii\base\Model $model
     *
     * @return array
     */
    public static function onUpdate($field, $model)
    {
        $fieldName = $field[ BaseForm::POS_DB_NAME ];

        $lng = self::getParam($fieldName . '-lng', $model);
        $lat = self::getParam($fieldName . '-lat', $model);
        $address = self::getParam($fieldName, $model);
        if ($lng == '') $lng = null;
        if ($lat == '') $lat = null;
        if ($address == '') $address = null;

        return [
            $fieldName . '_lat'     => $lat,
            $fieldName . '_lng'     => $lng,
            $fieldName . '_address' => $address,
        ];
    }

    /**
     *
     * @param array $field
     * @param \yii\base\Model $model
     *
     * @return array
     */
    public static function onLoad($field, $model)
    {
        $fieldName = $field[ BaseForm::POS_DB_NAME ];

        $lng = self::getParam($fieldName . '-lng', $model);
        $lat = self::getParam($fieldName . '-lat', $model);
        $address = self::getParam($fieldName, $model);

        $model->$fieldName = [
            'lat'     => $lat,
            'lng'     => $lng,
            'address' => $address,
        ];
    }

    /**
     *
     *
     * @param array $field
     * @param \cs\base\BaseForm $model
     *
     * @return array поля для обновления в БД
     */
    public static function onLoadDb($field, $model)
    {
        $fieldName = $field[ BaseForm::POS_DB_NAME ];
        $fields = $model->getRow();

        $lat = ArrayHelper::getValue($fields, $fieldName . '_lat', null);
        $lng = ArrayHelper::getValue($fields, $fieldName . '_lng', null);
        $address = ArrayHelper::getValue($fields, $fieldName . '_address', null);
        $model->$fieldName = [
            'lat'     => $lat,
            'lng'     => $lng,
            'address' => $address,
        ];
    }

    /**
     *
     * @param array $field
     * @param \yii\base\Model $model
     *
     * @return array
     */
    public static function onInsert($field, $model)
    {
        $fieldName = $field[ BaseForm::POS_DB_NAME ];

        $lng = self::getParam($fieldName . '-lng', $model);
        $lat = self::getParam($fieldName . '-lat', $model);
        $address = self::getParam($fieldName, $model);
        if ($lng == '') $lng = null;
        if ($lat == '') $lat = null;
        if ($address == '') $address = null;

        return [
            $fieldName . '_lat'     => $lat,
            $fieldName . '_lng'     => $lng,
            $fieldName . '_address' => $address,
        ];
    }


    /**
     * Возвращает значение поля формы из поста
     *
     * @param string $fieldName
     * @param \yii\base\Model $model
     *
     * @return string
     */
    public static function getParam($fieldName, $model)
    {
        $formName = $model->formName();
        $query = $formName . '.' . $fieldName;

        return ArrayHelper::getValue(\Yii::$app->request->post(), $query, '');
    }
}
