<?php

namespace cs\Widget\HighCharts;

use yii\helpers\ArrayHelper;
use yii\helpers\Html;

/**
 * Created by PhpStorm.
 * User: s.arhangelskiy
 * Date: 07.06.2016
 * Time: 11:27
 */
class HighCharts extends \yii\base\Widget
{
    /**
     * @var array массив для тега DIV
     */
    public $options = [];
    
    /**
     * @var array массив для настройки графика
     */
    public $chartOptions;

    /** 
     * @var array доп модули которые подключаются при инициализации (без js), например ['highcharts-3d', 'modules/heatmap']
     */
    public $modules;
    
    public function run()
    {
        $this->registerAssets();
        if (!ArrayHelper::keyExists('id', $this->options)) {
            $this->options['id'] = $this->getId();
        }
        $this->registerOptions();

        return Html::tag('div', null, $this->options);
    }
    
    public function registerAssets()
    {
        $asset = \common\assets\HighCharts\HighChartsAsset::register($this->getView());
        if ($this->modules) {
            $asset->withScripts($this->modules);
        }
    }
    public function registerOptions()
    {
        $optionsJson = \yii\helpers\Json::encode($this->chartOptions);
        $id = $this->options['id'];
        $this->getView()->registerJs("window.chart = $('#{$id}').highcharts({$optionsJson});");
    }
}