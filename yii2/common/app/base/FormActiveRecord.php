<?php
/**
 * Created by PhpStorm.
 * User: s.arhangelskiy
 * Date: 13.04.2016
 * Time: 11:54
 */

namespace cs\base;

use cs\services\VarDumper;
use cs\web\Exception;
use yii\base\Event;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;
use yii\helpers\StringHelper;


/**
 * Расширяет функциональность класса \yii\db\ActiveRecord
 *
 * Class FormActiveRecord
 * @package cs\base
 */
class FormActiveRecord extends ActiveRecord
{
    use FormActiveRecordTrait;

    const POS_DB_NAME     = 0;
    const POS_RUS_NAME    = 1;
    const POS_IS_REQUIRED = 2;
    const POS_RULE        = 3;
    const POS_PARAMS      = 4;
    const POS_HINT        = 5;

    public function formAttributes()
    {
        return [
            [
                'name',
                'Навание',
                1,
                'string',
                [],
                'hint',
                'widget' => [
                    'cs\ww', [
                        'rules' => 1,
                    ],
                ],
            ],
        ];
    }

    public function widgets()
    {
        return [
            'name' => ['app\upload'],
        ];
    }

    /**
     * @param mixed $condition
     * @return null|static
     */
    public static function findOne($condition)
    {
        $model = parent::findOne($condition);
        if (is_null($model)) return null;
        $model->executeMethod('onLoadDb');

        return $model;
    }

    /**
     * Ищет строку с условием $condition и инициализирует объект параметрами $params
     *
     * @param mixed $condition
     * @param array $params
     *
     * @return null|static
     */
    public static function findOneWithInit($condition, $params = [])
    {
        $model = parent::findOne($condition);
        foreach ($params as $k => $v) {
            $model->$k = $v;
        }
        if (is_null($model)) return null;
        $model->executeMethod('onLoadDb');

        return $model;
    }

    /**
     * Ищет поле среди полей формы
     *
     * @param $name
     *
     * @return mixed
     *
     * @throws Exception
     */
    public function findField($name)
    {
        foreach($this->formAttributes() as $field) {
            if ($field[self::POS_DB_NAME] == $name) {
                return $field;
            }
        }
        throw new Exception('Не найдено поле '. $name);
    }

    /**
     * @param \yii\bootstrap\ActiveForm $form
     * @param string                    $name
     *
     * @return \yii\bootstrap\ActiveField
     */
    public function field($form, $name)
    {
        $fieldObject = $form->field($this, $name);
        $fieldArray = $this->findField($name);
        $label = ArrayHelper::getValue($fieldArray, self::POS_RUS_NAME, '');
        if ($label != '') {
            $fieldObject->label($label);
        }
        $hint = ArrayHelper::getValue($fieldArray, self::POS_HINT, '');
        if ($hint != '') {
            $fieldObject->hint($hint);
        }
        if (isset($fieldArray['widget'])) {
            $widget = $fieldArray['widget'];
            $config = ['value' => $this->$name];
            if (is_array($fieldArray['widget'])) {
                $options = ArrayHelper::getValue($widget, 1, []);
                $fieldObject->widget($widget[0], ArrayHelper::merge($options, $config));
            } else {
                $fieldObject->widget($widget, $config);
            }
        }

        return $fieldObject;
    }

    public function attributeLabels()
    {
        $return = [];
        foreach ($this->formAttributes() as $field) {
            $return[$field[self::POS_DB_NAME]] = $field[self::POS_RUS_NAME];
        }

        return $return;
    }

    public function attributeHints()
    {
        $return = [];
        foreach ($this->formAttributes() as $field) {
            if (isset($field[self::POS_HINT])) {
                $return[$field[self::POS_DB_NAME]] = $field[self::POS_HINT];
            }
        }

        return $return;
    }

    public function rules()
    {
        $return = [];
        $fields = $this->formAttributes();
        foreach ($fields as $field) {
            $row = [];
            $row[] = $field[self::POS_DB_NAME];
            $row[] = $field[self::POS_RULE];
            if (isset($field[self::POS_PARAMS])) {
                foreach ($field[self::POS_PARAMS] as $key => $value) {
                    if (is_string($key)) {
                        $row[$key] = $value;
                    } else {
                        $row[] = $value;
                    }
                }
            }
            $return[] = $row;
        }
        $requiredFields = [];
        // Добавляю обязательные поля
        foreach ($fields as $field) {
            if ($field[self::POS_IS_REQUIRED] == 1) {
                $requiredFields[] = $field[self::POS_DB_NAME];
            }
        }
        if ($requiredFields) {
            $return[] = [
                $requiredFields, 'required',
            ];
        }

        return $return;
    }

    /**
     * @param $methodName
     * @return array
     */
    private function executeMethod($methodName)
    {
        $fields = $this->formAttributes();
        $ret = [];
        foreach ($fields as $field) {
            if (isset($field['widget'])) {
                $widget = $field['widget'];
                $options = [];
                if (is_array($widget)) {
                    $class = $widget[0];
                    if (isset($widget[1])) {
                        $options = $widget[1];
                    }
                } else {
                    $class = $widget;
                }
                $fieldName = $field[self::POS_DB_NAME];
                $options = ArrayHelper::merge($options, [
                    'model'     => $this,
                    'attribute' => $fieldName,
                    'value'     => $this->$fieldName,
                ]);
                $object = new $class($options);
                if (method_exists($object, $methodName)) {
                    \Yii::info(\yii\helpers\VarDumper::dumpAsString($field), 'gs\\cs\\base\\FormActiveRecord::executeMethod');
                    $ret[] = $fieldName;
                    $object->$methodName($field);
                }
            } else {
                $ret[] = $field[self::POS_DB_NAME];
            }
        }

        return $ret;
    }

    public function load($data, $formName = null)
    {
        $res = parent::load($data, $formName);
        if ($res) {
            $this->executeMethod('onLoad');
            return true;
        } else {
            return false;
        }
    }

    public function beforeInsert()
    {
        return [];
    }

    public function insert($runValidation = true, $attributeNames = null)
    {
        if ($runValidation) $this->validate($attributeNames);
        if ($this->hasErrors()) return false;

        $ret = $this->executeMethod('onInsert');
        $beforeInsert = $this->beforeInsert();
        foreach($beforeInsert as $name => $function) {
            $ret[] = $name;
            $this->$name = $function($this);
        }
        parent::insert(true, $ret);
        $this->id = self::getDb()->lastInsertID;

        $fields = $this->formAttributes();
        $rr = [];
        $isChanged = false;
        foreach ($fields as $field) {
            if (isset($field['widget'])) {
                $widget = $field['widget'];
                $options = [];
                if (is_array($widget)) {
                    $class = $widget[0];
                    if (isset($widget[1])) {
                        $options = $widget[1];
                    }
                } else {
                    $class = $widget;
                }
                $fieldName = $field[self::POS_DB_NAME];
                $options = ArrayHelper::merge($options, [
                    'model'     => $this,
                    'attribute' => $fieldName,
                    'value'     => $this->$fieldName,
                ]);
                $object = new $class($options);
                $methodName = 'onInsert';
                if (!method_exists($object, $methodName)) {
                    if (method_exists($object, 'onUpdate')) {
                        $rr[] = $fieldName;
                        $object->onUpdate($field);
                        $isChanged = true;
                    }
                }

            }
        }
        if ($isChanged) parent::update(false, $rr);

        return true;
    }

    public function getFields()
    {
        return $this->getAttributes();
    }

    /**
     * Возвращает атрибуты в виде массива
     * @return array
     */
    public function getRow()
    {
        return $this->getAttributes();
    }

    public function update($runValidation = true, $attributeNames = null)
    {
        if ($runValidation) $this->validate($attributeNames);
        if ($this->hasErrors()) return false;

        $fields = $this->executeMethod('onUpdate');
        parent::update(false, $fields);

        return true;
    }

    public function delete()
    {
        $this->executeMethod('onDelete');
        parent::delete();

        return true;
    }
}