<?php
/**
 * Created by PhpStorm.
 * User: Бог-Творец
 * Date: 25.05.2016
 * Time: 19:54
 */

namespace cs\base;
use yii\db\Query;

/**
 * Применяется для класса `cs\base\FormActiveRecord` для того чтобы обеспечить обратную совместимость с `cs\base\DbRecord`
 * когда идет переход
 *
 * Class FormActiveRecordTrait
 * @package cs\base
 *
 * @property int $id
 *
 * @method          getAttribute
 * @method          getAttributes
 *
 */
trait FormActiveRecordTrait
{
    /**
     * Возвращает поле так как оно есть
     *
     * @param $name
     * @return mixed
     */
    public function getField($name, $default = null)
    {
        $value = $this->getAttribute($name);
        return (is_null($value)) ? $default : $value;
    }

    /**
     * Ищет строку по идентификатору
     *
     * @param integer $id идентификатор пользователя
     *
     * @return array|null
     */
    protected static function _find($id)
    {
        $row = static::find()->select('*')->from(static::tableName())->where(['id' => $id])->asArray()->one();
        if ($row === false) return null;

        return $row;
    }

    /**
     * Возвращает значение поля
     * Если оно не найдено среди полей то будет возвращено $default
     * Если значение поля равно null то будет возвращено $default
     *
     * @param $name
     * @param $default
     *
     * @return int|string
     */
    public function get($name, $default = null)
    {
        return $this->getField($name, $default);
    }

    public static function deleteByCondition($condition = null, $params = [])
    {
        \Yii::info((new Query())->createCommand()->delete(static::tableName(), $condition, $params)->getRawSql(), 'gs\\row');
        return (new Query())->createCommand()->delete(static::tableName(), $condition, $params)->execute();
    }

    /**
     * Возвращает заготовку Query для создания запросов
     *
     * @param $condition
     * @param $params
     *
     * @return Query
     */
    public static function query($condition = null, $params = null)
    {
        $query = self::find()->asArray();
        if (!is_null($condition)) {
            $query->where($condition, $params);
        }

        return $query;
    }

    public function getFields()
    {
        return $this->getAttributes();
    }

    public function getId()
    {
        return $this->id;
    }

    public static function tableName()
    {
        return static::TABLE;
    }
}