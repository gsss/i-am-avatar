<?php

namespace cs\services;


class Security
{
    public static function generateRandomString($length = 10)
    {
        return substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, $length);
    }

    public static function getSeeds()
    {
        $file = \Yii::getAlias('@common/data/ENRUS3.TXT');
        $data = file_get_contents($file);
        $words = explode("\n", $data);
        $return = [];
        for ($i = 0; $i < 20; $i++) {
            $r = rand(0, count($words));
            $return[] = $words[$r];
        }

        return join(' ', $return);
    }
} 