<?php

namespace backend\models\form;

use app\models\ActionsRange;
use common\components\EmailVerifier;
use common\models\Config;
use common\models\Language;
use common\models\language\Access;
use common\services\Debugger;
use GuzzleHttp\Query;
use yii\base\Model;
use Yii;
use yii\helpers\ArrayHelper;
use yii\helpers\VarDumper;

class LanguageAccess extends Model
{
    public $name;

    public function rules()
    {
        return [
            [['name'], 'default'],
        ];
    }

    public function do1()
    {
        \common\models\language\Access::deleteAll();
        $languagesCodes = ArrayHelper::map(Language::find()->all(), 'code', 'id');
        foreach ($this->name as $user_id => $languages) {
            foreach ($languages as $language => $value) {
                $language_id = ArrayHelper::getValue($languagesCodes, $language, '');
                if ($language_id) {
                    (new Access([
                        'user_id'     => $user_id,
                        'language_id' => $language_id,
                    ]))->save();
                }
            }
        }
    }
}