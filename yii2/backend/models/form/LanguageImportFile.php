<?php

namespace backend\models\form;

use app\models\ActionsRange;
use common\components\EmailVerifier;
use common\models\Config;
use common\models\Language;
use common\models\language\Access;
use common\models\language\Message;
use common\services\Debugger;
use GuzzleHttp\Query;
use Imagine\Image\Box;
use yii\base\Model;
use Yii;
use yii\helpers\ArrayHelper;
use yii\helpers\FileHelper;
use yii\helpers\Json;
use yii\helpers\VarDumper;
use yii\imagine\Image;
use yii\web\UploadedFile;

class LanguageImportFile extends Model
{
    public $file;

    public function rules()
    {
        return [
            [['file'], 'file', 'extensions' => ['json']],
        ];
    }

    /**
     * Сохраняет файл
     *
     * @return bool
     */
    public function action()
    {
        $file = UploadedFile::getInstanceByName('file');
        $content = file_get_contents($file->tempName);
        $dataArray = ArrayHelper::toArray(Json::decode($content));
        $migration = new \common\components\Migration(['isLog' => false]);
        $migration->updateStrings($dataArray);

        return true;
    }

    /**
     * Сохраняет картинку по формату
     *
     * @param string $file          полный путь к файлу источника
     * @param string $destination   полный путь к файлу назначения
     * @param array | false $format => [
     *           3000,
     *           3000,
     *           FileUpload::MODE_THUMBNAIL_OUTBOUND
     *          'isExpandSmall' => true,
     *      ] ,
     *
     * @return string полный путь к файлу назначения
     */
    private function saveImage($file, $destination, $format)
    {
        $widthFormat = 1;
        $heightFormat = 1;
        if (is_numeric($format)) {
            // Обрезать квадрат
            $widthFormat = $format;
            $heightFormat = $format;
        }
        else if (is_array($format)) {
            $widthFormat = $format[0];
            $heightFormat = $format[1];
        }

        // generate a thumbnail image
        $mode = ArrayHelper::getValue($format, 2, \Imagine\Image\ManipulatorInterface::THUMBNAIL_OUTBOUND);
        $image = Image::getImagine()->open($file);

        $image = self::expandImage($image, $widthFormat, $heightFormat, $mode);
        $quality = 100;
        $options = ['quality' => $quality];
        $image->thumbnail(new Box($widthFormat, $heightFormat), $mode)->save($destination, $options);

        return $destination;
    }

    /**
     * Расширяет маленькую картинку по заданной стратегии
     *
     * @param \Imagine\Image\ImageInterface $image
     * @param int $widthFormat
     * @param int $heightFormat
     * @param int $mode
     *
     * @return \Imagine\Image\ImageInterface
     */
    protected static function expandImage($image, $widthFormat, $heightFormat, $mode)
    {
        $size = $image->getSize();
        $width = $size->getWidth();
        $height = $size->getHeight();
        if ($width < $widthFormat || $height < $heightFormat) {
            // расширяю картинку
            if ($mode == \Imagine\Image\ManipulatorInterface::THUMBNAIL_INSET) {
                if ($width < $widthFormat && $height >= $heightFormat) {
                    $size = $size->widen($widthFormat);
                } else if ($width >= $widthFormat && $height < $heightFormat) {
                    $size = $size->heighten($heightFormat);
                } else if ($width < $widthFormat && $height < $heightFormat) {
                    // определяю как расширять по ширине или по высоте
                    if ($width / $widthFormat < $height / $heightFormat) {
                        $size = $size->widen($widthFormat);
                    }
                    else {
                        $size = $size->heighten($heightFormat);
                    }
                }
                $image->resize($size);
            } else {
                if ($width < $widthFormat && $height >= $heightFormat) {
                    $size = $size->heighten($heightFormat);
                } else if ($width >= $widthFormat && $height < $heightFormat) {
                    $size = $size->widen($widthFormat);
                } else if ($width < $widthFormat && $height < $heightFormat) {
                    // определяю как расширять по ширине или по высоте
                    if ($width / $widthFormat < $height / $heightFormat) {
                        $size = $size->heighten($heightFormat);
                    }
                    else {
                        $size = $size->widen($widthFormat);
                    }
                }
                $image->resize($size);
            }
        }

        return $image;
    }

}