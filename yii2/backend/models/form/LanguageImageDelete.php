<?php

namespace backend\models\form;

use app\models\ActionsRange;
use common\components\EmailVerifier;
use common\models\Config;
use common\models\Language;
use common\models\language\Access;
use common\models\language\Message;
use common\models\language\SourceMessage;
use common\services\Debugger;
use GuzzleHttp\Query;
use Imagine\Image\Box;
use yii\base\Model;
use Yii;
use yii\helpers\ArrayHelper;
use yii\helpers\FileHelper;
use yii\helpers\VarDumper;
use yii\imagine\Image;
use yii\web\UploadedFile;

class LanguageImageDelete extends Model
{
    public $file;

    /** @var  int идентификатор строки source_message.id */
    public $id;

    /** @var  string код языка */
    public $language;

    public function rules()
    {
        return [
            [['id', 'language'], 'required'],
            [['id'], 'integer'],
            [['language'], 'string', 'max' => 2],
        ];
    }

    /**
     * Сохраняет файл
     *
     * @return bool
     */
    public function action()
    {
        $file = UploadedFile::getInstanceByName('file');
        $info = pathinfo($file->name);
        $ext = strtolower($info['extension']);

        $messageObject = Message::findOne(['id' => $this->id, 'language' => $this->language]);
        $translation = $messageObject->translation;
        $path_b = $translation;
        $info = pathinfo($translation);
        $ext = strtolower($info['extension']);
        $path_t = substr($messageObject->translation,0,55) . '_t' . '.' . $ext;
        $fullPath_b = Yii::getAlias(\backend\models\form\LanguageImageSave::$pathSave1 . $path_b);
        $fullPath_t = Yii::getAlias(\backend\models\form\LanguageImageSave::$pathSave1 . $path_t);

        // удаляю старые файлы
        if (file_exists($fullPath_b)) {
            unlink($fullPath_b);
        }
        if (file_exists($fullPath_t)) {
            unlink($fullPath_t);
        }

        $messageObject->delete();

        // сбрасываю кеш
        {
            $sourceMessage = SourceMessage::findOne($this->id);
            $key = [
                'yii\\i18n\\DbMessageSource',
                $sourceMessage->category,
                $this->language,
            ];
            Yii::$app->cacheLanguages->flush();
        }

        return true;
    }
}