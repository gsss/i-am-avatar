<?php

namespace backend\models\form;

use app\models\ActionsRange;
use common\components\EmailVerifier;
use common\models\Config;
use common\models\Language;
use common\models\language\Access;
use common\models\language\Message;
use common\models\language\SourceMessage;
use common\services\Debugger;
use GuzzleHttp\Query;
use Imagine\Image\Box;
use yii\base\Model;
use Yii;
use yii\helpers\ArrayHelper;
use yii\helpers\FileHelper;
use yii\helpers\VarDumper;
use yii\imagine\Image;
use yii\web\UploadedFile;

class LanguageImageSave extends Model
{
    public $file;

    public static $pathSave1 = '@avatar/../public_html';
    public static $pathSave = '@avatar/../public_html/images/translation';


    /** @var  int идентификатор строки source_message.id */
    public $id;

    /** @var  string код языка */
    public $language;

    public function rules()
    {
        return [
            [['id', 'language'], 'required'],
            [['id'], 'integer'],
            [['language'], 'string', 'max' => 2],
        ];
    }

    /**
     * Сохраняет файл
     *
     * @return bool
     */
    public function action()
    {
        $file = UploadedFile::getInstanceByName('file');
        $info = pathinfo($file->name);
        $ext = strtolower($info['extension']);

        // формирую имя файла
        $hash = Yii::$app->security->generateRandomString();
        $b = $hash . '_b' . '.' . $ext;
        $t = $hash . '_t' . '.' . $ext;
        $folder = Yii::getAlias(self::$pathSave . '/' . $this->language);
        if (!file_exists($folder)) FileHelper::createDirectory($folder);
        $path_b = '/images/translation/' . $this->language . '/' . $b;
        $path_t = '/images/translation/' . $this->language . '/' . $t;
        $fullPath_b = Yii::getAlias(self::$pathSave1 . $path_b);
        $fullPath_t = Yii::getAlias(self::$pathSave1 . $path_t);

        // удаляю старые файлы
        if (file_exists($fullPath_b)) {
            unlink($fullPath_b);
        }
        if (file_exists($fullPath_t)) {
            unlink($fullPath_t);
        }

        // сохраняю файл
        $file->saveAs($fullPath_b);

        // сохраняю предпросмотр файл
        $this->saveImage($fullPath_b, $fullPath_t, [50,50]);

        // сохраняю в БД
        $messageObject = Message::findOne([
            'id'       => $this->id,
            'language' => $this->language,
        ]);
        if (is_null($messageObject)) {
            $messageObject = new Message([
                'id'       => $this->id,
                'language' => $this->language,
            ]);
        }
        $messageObject->translation = $path_b;
        $messageObject->save();

        // сбрасываю кеш
        {
            $sourceMessage = SourceMessage::findOne($this->id);
            $key = [
                'yii\\i18n\\DbMessageSource',
                $sourceMessage->category,
                $this->language,
            ];
            Yii::$app->cacheLanguages->flush();
        }

        return true;
    }

    /**
     * Сохраняет картинку по формату
     *
     * @param string $file          полный путь к файлу источника
     * @param string $destination   полный путь к файлу назначения
     * @param array | false $format => [
     *           3000,
     *           3000,
     *           FileUpload::MODE_THUMBNAIL_OUTBOUND
     *          'isExpandSmall' => true,
     *      ] ,
     *
     * @return string полный путь к файлу назначения
     */
    private function saveImage($file, $destination, $format)
    {
        $widthFormat = 1;
        $heightFormat = 1;
        if (is_numeric($format)) {
            // Обрезать квадрат
            $widthFormat = $format;
            $heightFormat = $format;
        }
        else if (is_array($format)) {
            $widthFormat = $format[0];
            $heightFormat = $format[1];
        }

        // generate a thumbnail image
        $mode = ArrayHelper::getValue($format, 2, \Imagine\Image\ManipulatorInterface::THUMBNAIL_OUTBOUND);
        $image = Image::getImagine()->open($file);

        $image = self::expandImage($image, $widthFormat, $heightFormat, $mode);
        $quality = 100;
        $options = ['quality' => $quality];
        $image->thumbnail(new Box($widthFormat, $heightFormat), $mode)->save($destination, $options);

        return $destination;
    }

    /**
     * Расширяет маленькую картинку по заданной стратегии
     *
     * @param \Imagine\Image\ImageInterface $image
     * @param int $widthFormat
     * @param int $heightFormat
     * @param int $mode
     *
     * @return \Imagine\Image\ImageInterface
     */
    protected static function expandImage($image, $widthFormat, $heightFormat, $mode)
    {
        $size = $image->getSize();
        $width = $size->getWidth();
        $height = $size->getHeight();
        if ($width < $widthFormat || $height < $heightFormat) {
            // расширяю картинку
            if ($mode == \Imagine\Image\ManipulatorInterface::THUMBNAIL_INSET) {
                if ($width < $widthFormat && $height >= $heightFormat) {
                    $size = $size->widen($widthFormat);
                } else if ($width >= $widthFormat && $height < $heightFormat) {
                    $size = $size->heighten($heightFormat);
                } else if ($width < $widthFormat && $height < $heightFormat) {
                    // определяю как расширять по ширине или по высоте
                    if ($width / $widthFormat < $height / $heightFormat) {
                        $size = $size->widen($widthFormat);
                    }
                    else {
                        $size = $size->heighten($heightFormat);
                    }
                }
                $image->resize($size);
            } else {
                if ($width < $widthFormat && $height >= $heightFormat) {
                    $size = $size->heighten($heightFormat);
                } else if ($width >= $widthFormat && $height < $heightFormat) {
                    $size = $size->widen($widthFormat);
                } else if ($width < $widthFormat && $height < $heightFormat) {
                    // определяю как расширять по ширине или по высоте
                    if ($width / $widthFormat < $height / $heightFormat) {
                        $size = $size->heighten($heightFormat);
                    }
                    else {
                        $size = $size->widen($widthFormat);
                    }
                }
                $image->resize($size);
            }
        }

        return $image;
    }

}