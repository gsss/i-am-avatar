<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;
use app\models\UnionCategory;
use yii\db\Query;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \avatar\models\forms\school\PageBlock */
/* @var $category \common\models\school\PageBlockCategory */

$this->title = 'Добавить блок';

?>
<div class="container">
    <div class="col-lg-12">
        <h1 class="page-header"><?= Html::encode($this->title) ?></h1>

        <?php if (!is_null($id = Yii::$app->session->getFlash('form'))) : ?>

            <div class="alert alert-success">
                Успешно добавлено.
            </div>
            <p>
                <a href="<?= \yii\helpers\Url::to(['admin-school-page-block/index', 'id' => $category->id]) ?>" class="btn btn-success">Все блоки</a>
                <a href="<?= \yii\helpers\Url::to(['admin-school-page-block/edit', 'id' => Yii::$app->session->getFlash('form')]) ?>" class="btn btn-success">Редактировать</a>
            </p>
        <?php else: ?>


            <div class="row">
                <div class="col-lg-5">
                    <?php $form = ActiveForm::begin([
                        'options' => ['enctype' => 'multipart/form-data']
                    ]); ?>
                    <?= $model->field($form, 'name') ?>
                    <?= $model->field($form, 'asset') ?>
                    <?= $model->field($form, 'image') ?>
                    <?= $model->field($form, 'init')->textarea(['rows' => 10]) ?>


                    <hr>
                    <div class="form-group">
                        <?= Html::submitButton('Добавить', [
                            'class' => 'btn btn-default',
                            'name'  => 'contact-button',
                            'style' => 'width:100%',
                        ]) ?>
                    </div>
                    <?php ActiveForm::end(); ?>
                </div>
            </div>

        <?php endif; ?>
    </div>
</div>
