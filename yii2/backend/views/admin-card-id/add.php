<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;
use app\models\UnionCategory;
use yii\db\Query;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \avatar\models\forms\CardId */

$this->title = 'Добавить карту';

?>
<div class="container">
    <div class="col-lg-12">
        <h1 class="page-header"><?= Html::encode($this->title) ?></h1>

        <?php if (Yii::$app->session->hasFlash('form')) { ?>

            <div class="alert alert-success">
                Успешно добавлено.
            </div>

            <?php $id = Yii::$app->session->getFlash('form'); ?>
            <p>
                <a href="<?= \yii\helpers\Url::to(['admin-card-id/index']) ?>" class="btn btn-default">Все карты</a>
            </p>

        <?php  } else { ?>


            <div class="row">
                <div class="col-lg-5">
                    <?php $form = ActiveForm::begin([
                        'options' => ['enctype' => 'multipart/form-data']
                    ]); ?>
                    <?= $model->field($form, 'name_first') ?>
                    <?= $model->field($form, 'name_patronymic') ?>
                    <?= $model->field($form, 'name_last') ?>
                    <?= $model->field($form, 'date_born') ?>
                    <?= $model->field($form, 'date_get') ?>
                    <?= $model->field($form, 'place_born') ?>
                    <?= $model->field($form, 'image_top') ?>
                    <?= $model->field($form, 'card_num') ?>


                    <hr>
                    <div class="form-group">
                        <?= Html::submitButton('Добавить', [
                            'class' => 'btn btn-default',
                            'name'  => 'contact-button',
                            'style' => 'width:100%',
                        ]) ?>
                    </div>
                    <?php ActiveForm::end(); ?>
                </div>
            </div>

        <?php } ?>
    </div>
</div>
