<?php

/** @var $this \yii\web\View */
/** @var $wallet \common\models\piramida\Wallet */

use common\models\avatar\UserBill;
use yii\helpers\Url;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;

$this->title = 'Test';

\avatar\assets\Notify::register($this);
\avatar\assets\Clipboard::register($this);




?>
<?php
$this->registerJs(<<<JS

var clipboard = new Clipboard('.buttonCopy');
clipboard.on('success', function(e) {

    new Noty({
        timeout: 1000,
        theme: 'sunset',
        type: 'success',
        layout: 'bottomLeft',
        text: 'Скопировано'
    }).show();

});

JS
);
?>

<div class="container">
    <div class="col-lg-12">
        <h1 class="page-header text-center"><?= Html::encode($this->title) ?></h1>
    </div>

    <div class="col-lg-12">


        <?php \yii\widgets\Pjax::begin(); ?>

        <?php
        $this->registerJs(<<<JS

$('[data-toggle="tooltip"]').tooltip();

JS
        );
        ?>
        <?= \yii\grid\GridView::widget([
            'dataProvider' => new \yii\data\ActiveDataProvider([
                'query'      => \common\models\piramida\Operation::find()
                    ->orderBy(['datetime' => SORT_DESC])
                ,
                'pagination' => [
                    'pageSize' => 1000,
                ],
            ]),
            'tableOptions' => [
                'class' => 'table table-striped table-hover',
            ],
            'rowOptions'   => function ($item) {
                $data = [
                    'data'  => ['id' => $item['id']],
                    'role'  => 'button',
                    'class' => 'rowTable',
                ];
                return $data;
            },
            'columns'      => [
                [
                    'header'    => 'sum',
                    'content'   => function ($item) {
                        $v = \yii\helpers\ArrayHelper::getValue($item, 'type');
                        if ($v < 0) $i = -1;
                        else $i = 1;
                        $sum = $item['after'] - $item['before'] - ($i * $item['amount']);

                        return $sum;
                    },
                ],
                'after',
                'before',
                'amount',
                'id',
            ],
        ]) ?>
        <?php \yii\widgets\Pjax::end(); ?>


        <?= \yii\grid\GridView::widget([
            'dataProvider' => new \yii\data\ArrayDataProvider([
                'allModels' => (new \yii\db\Query())->createCommand(Yii::$app->dbWallet)->setSql('select t1.am1,t1.currency_id,currency.amount, currency.amount - t1.am1 as balance FROM (
select sum(wallet.amount) as am1,currency_id from wallet GROUP BY currency_id
) as t1
INNER JOIN currency on (currency.id = t1.currency_id)')->queryAll()
            ]),
            'tableOptions' => [
                'class' => 'table table-striped table-hover',
            ],
        ]) ?>
    </div>
</div>
