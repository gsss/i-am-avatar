<?php
/**
 * Created by PhpStorm.
 * User: Бог-Творец
 * Date: 29.09.2016
 * Time: 19:49
 */
use yii\helpers\Html;

/** @var array $transactions */
/** @var int $page номер страницы */
/** @var int $id идентификатор счета */
/** @var \common\models\avatar\UserBill $billing счет */

/**
 * $transactions - транзакции = itewmsPerPage + 1, чтобы последнюю не показывать и понять что есть еще чего запрашивать, поэтому вывожу кнопку Далее!
 * $start - int - старт от которой транзакции сейчас если первая то 1
 *
 *
 */



$columns = [
    [
        'header'         => 'ID',
        'contentOptions' => [
            'class' => 'code',
        ],
        'content'        => function ($item) {
            return
                Html::tag(
                    'abbr',
                    substr($item['hash'], 0, 8) . '...',
                    [
                        'class' => 'js-buttonTransactionInfo gsssTooltip',
                        'role'  => 'button',
                        'title' => $item['hash'],
                        'data'  => [
                            'toggle'    => 'tooltip',
                            'html'      => $item,
                            'placement' => 'bottom',
                        ],
                    ]
                );
        }
    ],
    [
        'header'         => 'Подтверждений',
        'headerOptions'  => [
            'style' => 'text-align:right',
        ],
        'contentOptions' => [
            'style' => 'text-align:right',
        ],
        'content'        => function ($item) {
            $confirmations = $item['confirmations'];
            if ($confirmations == 0) {
                $class = 'label label-danger';
            } else
                if ($confirmations < 6) {
                    $class = 'label label-warning';
                } else {
                    $class = 'label label-success';
                }
            return
                Html::tag(
                    'span',
                    Yii::$app->formatter->asDecimal($confirmations),
                    ['class' => $class]
                );
        }
    ],

    [
        'header'         => 'mBTC',
        'headerOptions'  => [
            'style' => 'text-align:right;',
        ],
        'contentOptions' => function ($item) {
            $style = ['text-align:right'];
            if ($item['wallet_value_change'] < 0) {
                $style[] = 'color: #F45F5F';
            } else {
                $style[] = 'color: #6ED098';
            }
            return [
                'style' => join(';', $style),
                'class' => 'code',
            ];
        },
        'content'        => function ($item) {
            $v = $item['wallet_value_change'];
            if ($v < 0) {
                $v += $item['total_fee'];
            }
            return Yii::$app->formatter->asDecimal($v / 100000, 5);
        }
    ],
    [
        'header'         => 'Комиссия mBTC',
        'headerOptions'  => [
            'style' => 'text-align:right',
        ],
        'contentOptions' => [
            'style' => 'color: #F45F5F;text-align:right;',
            'class' => 'code',
        ],
        'content'        => function ($item) {
            if ($item['wallet_value_change'] > 0) {
                return '';
            }

            return '-' . Yii::$app->formatter->asDecimal($item['total_fee'] / 100000, 5);
        }
    ],
    [
        'header'    => 'Время',
        'content'   => function ($item) {
            $t = $item['time'];
            $t = str_replace('+00:00', '', $t);
            $t = str_replace('T', ' ', $t);

            return Html::tag('abbr', \cs\services\DatePeriod::back($t, ['isShort' => true]), [
                'title' => Yii::$app->formatter->asDatetime($t),
                'class' => 'gsssTooltip',
            ]);
        },
        'attribute' => 'time'
    ],
    [
        'header'  => 'Коментарий',
        'content' => function ($item) {
            return $item['comment'];
        },
    ],
];

if (Yii::$app->deviceDetect->isMobile()) {
    $columns = [
        $columns[4],
        $columns[2],
        $columns[3],
    ];
}
?>
<style>
    .table .code {
        font-family: 'Courier New', Monospace;
    }

</style>

<?= \yii\grid\GridView::widget([
    'dataProvider' => new \yii\data\ArrayDataProvider([
        'allModels' => $transactions['data'],
    ]),
    'tableOptions' => [
        'class' => 'table table-hover table-striped',
        'id'    => 'tableTransaction'
    ],
    'columns'      => $columns,
    'summary'      => ''

]);
?>

<ul class="pagination">
    <?php if ($page == 1) { ?>
        <li class="prev disabled"><span>«</span></li>
    <?php } else { ?>
        <li class="prev"><a href="<?= \yii\helpers\Url::to([
                'cabinet-bills/transactions-list',
                'id'      => $id,
                'page'    => $page - 1,
            ]) ?>" data-page="0" class="paginationItem">«</a></li>
    <?php } ?>
    <?php for ($i = 1; $i <= $page; $i++) { ?>
        <li><a href="<?= \yii\helpers\Url::to([
                'cabinet-bills/transactions-list',
                'id'      => $id,
                'page'    => $i,
            ]) ?>"
               data-page="1" class="paginationItem"><?= $i ?></a></li>
    <?php } ?>
    <?php if (count($transactions['data']) == 20) { ?>
        <li class="next"><a
                href="<?= \yii\helpers\Url::to([
                    'cabinet-bills/transactions-list',
                    'id'      => $id,
                    'page'    => $page + 1,
                ]) ?>"
                data-page="<?= $page ?>" class="paginationItem">»</a></li>
    <?php } ?>
</ul>
