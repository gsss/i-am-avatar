<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;
use app\models\UnionCategory;
use yii\db\Query;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model \avatar\models\forms\UserAvatar */


$this->title = $model->name_first . '. Роли';

$userRoles = Yii::$app->authManager->getRolesByUser($model->id);
Yii::$app->session->set(__FILE__, array_values(ArrayHelper::getColumn($userRoles, 'name')));
$roleList = Yii::$app->authManager->getRoles();

foreach ($roleList as $role) {
    $name = $role->name;
    $nameMd5 = md5($role->name);
    $this->registerJs(<<<JS
$('#m1-{$nameMd5}').change(function(e) {
    ajaxJson({
        url: '/admin-users/permissions-set',
        data: {
            id: {$model->id},
            name: '{$name}',
            value: $(this).is(':checked') 
        },
        success: function(ret) {
            
        }
    });
});
JS
    );
}


$this->registerJs(<<<JS
$('.buttonUsers').click(function(e) {
    e.preventDefault();
    e.stopPropagation();
    window.location = '/admin-users/role-users' + '?' + 'name' + '=' + $(this).data('name');
});
JS
);

?>
<div class="container">
    <div class="col-lg-12">
        <h1 class="page-header text-center"><?= Html::encode($this->title) ?></h1>

        <?= \yii\grid\GridView::widget([
            'dataProvider' => new \yii\data\ArrayDataProvider([
                'allModels' => $roleList
            ]),
            'tableOptions' => [
                'class' => 'table table-striped table-hover',
            ],
            'rowOptions'   => function ($item) {
                $data = [
                    'data'  => ['id' => $item->name],
                    'role'  => 'button',
                    'class' => 'rowTable'
                ];
                return $data;
            },
            'columns'      => [
                'name',
                'description',
                [
                    'header'  => 'Включено?',
                    'content' => function ($item) {
                        return \common\widgets\CheckBox2\CheckBox::widget([
                            'model'     => new \avatar\models\M1(['user_roles' => Yii::$app->session->get(__FILE__)]),
                            'attribute' => md5($item->name),
                        ]);
                    }
                ],
                [
                    'header'  => 'Пользователи',
                    'content' => function (\yii\rbac\Role $item) {
                        return Html::button('Пользователи', ['class' => 'btn btn-primary btn-xs buttonUsers', 'data' => ['name' => $item->name]]);
                    }
                ],
            ]
        ]) ?>
    </div>
</div>
