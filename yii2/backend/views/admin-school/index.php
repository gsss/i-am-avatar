<?php

/** $this \yii\web\View  */

use yii\helpers\Url;
use app\services\GsssHtml;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use app\services\GetArticle\Collection;

$this->title = 'Все школы';

\avatar\assets\Notify::register($this);

?>

<div class="container">
    <div class="col-lg-12">
        <h1 class="page-header"><?= Html::encode($this->title) ?></h1>
    </div>

    <div class="col-lg-12">

        <?php \yii\widgets\Pjax::begin(); ?>
        <?php
        $this->registerJS(<<<JS
$('[data-toggle="tooltip"]').tooltip();

$('.rowTable').click(function() {

});
JS
        );
        $sort = new \yii\data\Sort([
            'defaultOrder' => [
                'created_at' => SORT_DESC,
                'id'         => SORT_DESC,
            ],
        ]);
        ?>
        <?= \yii\grid\GridView::widget([
            'dataProvider' => new \yii\data\ActiveDataProvider([
                'query'      => \common\models\school\School::find()
                ,
                'pagination' => [
                    'pageSize' => 100,
                ],
                'sort'       => $sort,
            ]),
            'tableOptions' => [
                'class' => 'table table-striped table-hover',
            ],
            'rowOptions'   => function ($item) {
                $data = [
                    'data'  => ['id' => $item['id']],
                    'role'  => 'button',
                    'class' => 'rowTable',
                ];
                return $data;
            },
            'columns'      => [
                'id',
                [
                    'header'  => 'Картинка',
                    'content' => function ($item) {
                        $i = ArrayHelper::getValue($item, 'image', '');
                        if ($i == '') return Html::img('/images/1x1.png', [
                                'class'  => "img-circle",
                                'width'  => 30,
                                'height' => 30,
                                'style'  => 'margin-bottom: 0px;',
                            ]
                        );

                        return Html::img($i, [
                            'class'  => "img-circle",
                            'width'  => 30,
                            'height' => 30,
                            'style'  => 'margin-bottom: 0px;',
                        ]);
                    },
                ],
                'name:text:Название',
                'dns',
                [
                    'header'        => 'Автор',
                    'headerOptions' => [
                        'style' => 'width: 15px;',
                    ],
                    'content'       => function ($item) {
                        $user_id = $item['user_id'];
                        $user = \common\models\UserAvatar::findOne($user_id);

                        $i = $user->getAvatar();
                        $params = [
                            'class'  => "img-circle",
                            'width'  => 30,
                            'height' => 30,
                            'style'  => ['margin-bottom' => '0px'],
                            'title'  => \yii\helpers\Html::encode($user->getName2()),
                            'data'   => ['toggle' => 'tooltip'],
                        ];

                        if (isset($params['style'])) {
                            if (is_array($params['style'])) {
                                $params['style'] = Html::cssStyleFromArray($params['style']);
                            }
                        }

                        return Html::img($i, $params);
                    },
                ],
                [
                    'header'  => 'Создано',
                    'content' => function ($item) {
                        $v = \yii\helpers\ArrayHelper::getValue($item, 'created_at', 0);
                        if ($v == 0) return '';

                        return Html::tag('abbr', \cs\services\DatePeriod::back($v, ['isShort' => true]), ['data' => ['toggle' => 'tooltip'], 'title' => Yii::$app->formatter->asDatetime($v)]);
                    },
                ],
                [
                    'header'  => 'Оплачено до',
                    'content' => function ($item) {
                        $v = \yii\helpers\ArrayHelper::getValue($item, 'paid_till', 0);
                        if ($v == 0) return '';

                        return Html::tag('abbr', \cs\services\DatePeriod::forward($v, ['isShort' => true]), ['data' => ['toggle' => 'tooltip'], 'title' => Yii::$app->formatter->asDatetime($v)]);
                    },
                ],
            ],
        ]) ?>
        <?php \yii\widgets\Pjax::end(); ?>

    </div>
</div>

<div class="modal fade" id="modalInfo" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="myModalLabel">Информация</h4>
            </div>
            <div class="modal-body">
                Успешно!
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
            </div>
        </div>
    </div>
</div>