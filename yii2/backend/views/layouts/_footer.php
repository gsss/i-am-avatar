<?php
/**
 * Created by PhpStorm.
 * User: Бог-Творец
 * Date: 21.01.2017
 * Time: 18:16
 */

/** @var \common\models\school\School $school */

$site = 'https://www.i-am-avatar.com';
$mail = 'info@i-am-avatar.com';

?>

<footer class="footer" style="opacity: 1;">
    <div class="container">
        <div class="row" style="margin-top: 20px;">
            <div class="col-lg-4">
                <p style="margin-bottom: 30px;">
                    <?= $site ?>
                </p>
            </div>
            <div class="col-lg-4">

            </div>
            <div class="col-lg-4">
                <p style="margin-bottom: 30px;">
                    <?= $mail ?>
                </p>
            </div>
        </div>
    </div>
</footer>

