<?php
/**
 * Created by PhpStorm.
 * User: Бог-Творец
 * Date: 11.11.2016
 * Time: 1:44
 */

/** @var $this \yii\web\View */

use yii\helpers\Html;
use yii\helpers\Url;

$image = 'https://cloud1.iampau.pro/upload/cloud/15846/29196_KY9yFSPvQU_crop.png';


?>


<!-- Fixed navbar -->
<nav class="navbar navbar-default navbar-fixed-top">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar"
                    aria-expanded="false" aria-controls="navbar">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a
                class="navbar-brand"
                href="/"
                style="padding: 5px 10px 5px 10px;"
                >
                <img
                    src="<?= $image ?>"
                    width="40"
                    data-toggle="tooltip"
                    data-placement="bottom"
                    title="Главная"
                    class="img-circle"
                    >
            </a>
        </div>
        <div id="navbar" class="collapse navbar-collapse">
            <ul class="nav navbar-nav">
                <?= $this->render('../blocks/topMenuCabinet') ?>
            </ul>
            <ul class="nav navbar-nav navbar-right">
                <?php if (\Yii::$app->user->isGuest) { ?>
                    <li id="userBlockLi">
                        <!-- Split button -->
                        <div class="btn-group" style="margin-top: 9px; opacity: 0.5;" id="loginBarButton">
                            <button type="button" class="btn btn-default" id="modalLogin"><i
                                    class="glyphicon glyphicon-user" style="padding-right: 5px;"></i>Войти
                            </button>
                            <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown"
                                    aria-expanded="false">
                                <span class="caret"></span>
                                <span class="sr-only">Toggle Dropdown</span>
                            </button>
                            <ul class="dropdown-menu" role="menu">
                                <li><a href="<?= Url::to(['auth/password_recover']) ?>">Напомнить пароль</a></li>
                                <li><a href="<?= Url::to(['auth/registration']) ?>">Регистрация</a></li>
                            </ul>
                        </div>
                    </li>
                <?php } else { ?>
                    <?= $this->render('user-menu') ?>
                <?php } ?>
            </ul>
        </div>

    </div>
</nav>
