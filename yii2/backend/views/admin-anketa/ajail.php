<?php

/** @var $this \yii\web\View */

use yii\helpers\Url;
use app\services\GsssHtml;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use app\services\GetArticle\Collection;

$this->title = 'Все анкеты';

$statusList = [
    1 => 'Не актуальные',
    2 => 'На рассмотрении',
    3 => 'Выпал',
    4 => 'Команда (очень тормоза)',
    5 => 'Команда',
];

$w1 = 130;
$w2 = $w1 + 10;
$w3 = $w2 + 10;
$w4 = $w3 + 10;
?>

<div class="container">
    <div class="col-lg-12">
        <h1 class="page-header"><?= Html::encode($this->title) ?></h1>

        <style>
            .container123 {
                margin: 0 auto;
            }

            .connectedSortable {
                border: 1px solid #eee;
                width: <?= $w3 ?>px;
                min-height: 20px;
                list-style-type: none;
                margin: 0;
                padding: 5px;
                float: left;
                margin-right: 10px;
            }

            .connectedSortable li {
                margin: 0 5px 5px 5px;
                padding: 5px;
                width: <?= $w1 ?>px;
                border: 1px solid #eee;
                border-radius: 10px;
            }

            .img-user {
                margin-top: 10px;
            }

            /** .ui-state-highlight { height: 1.2em; line-height: 1.2em; } */
        </style>
        <?php $this->registerJsFile('https://code.jquery.com/ui/1.12.1/jquery-ui.js', ['depends' => ['\yii\web\JqueryAsset']]);
        $this->registerJs(<<<JS
$(".connectedSortable").on("sortreceive", function(event, ui) {
    var item_id = $(ui.item[0]).data('id');
    var list_id = $(event.currentTarget).data('id');
    ajaxJson({
        url:"/admin-anketa/set",
        data: {
            item_id:item_id,
            list_id:list_id
        },
        success: function(ret) {
            
        }
    });
});
JS
        );
        $orderBy = [
            'last_message_time' => SORT_DESC,
            'created_at'        => SORT_DESC,
        ];
        ?>
        <table border="0" style="height: 50px;">
            <tr>
                <td style="width: <?= $w4 ?>px;font-weight: bold;">Не распределено</td>
                <td style="width: <?= $w4 ?>px;font-weight: bold;"><?= $statusList[1] ?></td>
                <td style="width: <?= $w4 ?>px;font-weight: bold;"><?= $statusList[2] ?></td>
                <td style="width: <?= $w4 ?>px;font-weight: bold;"><?= $statusList[4] ?></td>
                <td style="width: <?= $w4 ?>px;font-weight: bold;"><?= $statusList[5] ?></td>
            </tr>
        </table>
        <div class="row">
            <div class="container123">
                <?php
                $rows0 = \common\models\Anketa::find()->where(['status' => 0])->orderBy($orderBy)->all();
                $rows1 = \common\models\Anketa::find()->where(['status' => 1])->orderBy($orderBy)->all();
                $rows2 = \common\models\Anketa::find()->where(['status' => 2])->orderBy($orderBy)->all();
                $rows4 = \common\models\Anketa::find()->where(['status' => 4])->orderBy($orderBy)->all();
                $rows5 = \common\models\Anketa::find()->where(['status' => 5])->orderBy($orderBy)->all();
                ?>
                <?php
                $this->registerJs(<<<JS
$( ".sortable0, .sortable1, .sortable2, .sortable3, .sortable4, .sortable5" ).sortable({
    placeholder: "ui-state-highlight",
    connectWith: ".connectedSortable"
});
JS
                );
                ?>
                <ul class="connectedSortable sortable0" data-id="0">
                    <?php /** @var \common\models\Anketa $item */ ?>
                    <?php foreach ($rows0 as $item) { ?>
                        <?= $this->render('i', ['item' => $item]) ?>
                    <?php } ?>
                </ul>
                <ul class="connectedSortable sortable1" data-id="1">
                    <?php /** @var \common\models\Anketa $item */ ?>
                    <?php foreach ($rows1 as $item) { ?>
                        <?= $this->render('i', ['item' => $item]) ?>
                    <?php } ?>
                </ul>
                <ul class="connectedSortable sortable2" data-id="2">
                    <?php /** @var \common\models\Anketa $item */ ?>
                    <?php foreach ($rows2 as $item) { ?>
                        <?= $this->render('i', ['item' => $item]) ?>
                    <?php } ?>
                </ul>
                <ul class="connectedSortable sortable4" data-id="4">
                    <?php /** @var \common\models\Anketa $item */ ?>
                    <?php foreach ($rows4 as $item) { ?>
                        <?= $this->render('i', ['item' => $item]) ?>
                    <?php } ?>
                </ul>
                <ul class="connectedSortable sortable5" data-id="5">
                    <?php /** @var \common\models\Anketa $item */ ?>
                    <?php foreach ($rows5 as $item) { ?>
                        <?= $this->render('i', ['item' => $item]) ?>
                    <?php } ?>
                </ul>
            </div>
        </div>
    </div>
</div>
