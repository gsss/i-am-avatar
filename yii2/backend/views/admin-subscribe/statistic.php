<?php

/**
 * @var $this \yii\web\View
 */

use yii\widgets\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;
use cs\Widget\HighCharts\HighCharts;

$this->title = 'Мониторинг системы';
?>

<style>
    a.asc:after {
        content: ' ↓';
        display: inline;
    }

    a.desc:after {
        content: ' ↑';
        display: inline;
    }
    .tableStat td {
        font-family: "Courier New", Courier, monospace;
    }
</style>

<div class="container">
    <div class="col-lg-12">
        <h1 class="page-header">Мониторинг рассылки</h1>



        <?php
        $period = 24 * 2;
        $rows = \common\models\subscribe\SubscribeStat::find()
            ->select([
                'time',
                'delay',
            ])
            ->where(['between', 'time', time() - 60 * 60 * $period, time()])
            ->all()
        ;
        $rows2 = \common\models\subscribe\SubscribeStat::find()
            ->select([
                'time',
                '`counter`',
            ])
            ->where(['between', 'time', time() - 60 * 60 * $period, time()])
            ->all()
        ;

        $rowsJson = \yii\helpers\Json::encode($rows);
        $rows2Json = \yii\helpers\Json::encode($rows2);
        $this->registerJs(<<<JS
        Highcharts.setOptions({
            global: {
                timezoneOffset: -3 * 60
            }
        });
        var rows = {$rowsJson};
        var newRows = [];
        for(i = 0; i < rows.length; i++)
        {
            var item = rows[i];
            newRows.push({
                x: new Date(item.time * 1000),
                y: item.delay
            });
        }
        
        var rows2 = {$rows2Json};
        var newRows2 = [];
        for(i = 0; i < rows2.length; i++)
        {
            var item2 = rows2[i];
            newRows2.push({
                x: new Date(item2.time * 1000),
                y: item2.counter
            });
        }
        console.log(newRows2);

JS
        );
        ?>
        <?= HighCharts::widget([
            'chartOptions' => [
                'chart' => [
                    'zoomType' => 'x',
                    'type'     => 'spline',
                ],
                'title' => [
                    'text' => 'График',
                ],
                'subtitle' => [
                    'text' => 'Выделите область для изменения масштаба',
                ],
                'xAxis' => [
                    'type' => 'datetime',
                ],
                'yAxis' => [
                    [
                        'title' => [
                            'text' => 'Количество',
                        ],
                    ]
                ],
                'legend' => [
                    'enabled' => true
                ],
                'tooltip' => [
                    'crosshairs' => true,
                    'shared' => true,
                ],
                'plotOptions' => [
                    'series' => [
                        'turboThreshold' => 0,
                    ],
                ],
                'series' => [
                    [
                        'type' => 'spline',
                        'name' => 'Сек',
                        'data' => new \yii\web\JsExpression('newRows'),
                    ],
                    [
                        'type' => 'spline',
                        'name' => 'Писем в мин',
                        'data' => new \yii\web\JsExpression('newRows2'),
                    ],
                ],
            ],
        ]);

        ?>

    </div>

</div>