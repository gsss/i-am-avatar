<?php

/** $this \yii\web\View  */

use yii\helpers\Url;
use app\services\GsssHtml;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use app\services\GetArticle\Collection;

$this->title = 'Все Анкеты школ';


?>

<div class="container">
    <div class="col-lg-12">
        <h1 class="page-header"><?= Html::encode($this->title) ?></h1>

        <?php \yii\widgets\Pjax::begin(); ?>
        <?php
        $this->registerJS(<<<JS
$('.buttonDelete').click(function (e) {
    e.preventDefault();
    e.stopPropagation();
    if (confirm('Подтвердите удаление')) {
        var button = $(this);
        var id = $(this).data('id');
        ajaxJson({
            url: '/admin-anketa-school/delete?id=' + id,
            success: function (ret) {
                infoWindow('Успешно', function() {
                    button.parent().parent().remove();
                });
            }
        });
    }
});

$('.gsssTooltip').tooltip();

$('.rowTable').click(function() {
    window.location = '/admin-anketa-school/view?id=' + $(this).data('id');
});
JS
        );
        $sort = new \yii\data\Sort(
            [
                'attributes'   => [
                    'id'         => ['label' => "ID"],
                    'name' => ['label' => "Имя"],
                    'description',
                    'email',
                    'phone',
                    'created_at' => ['label' => "Создано"],
                ],
                'defaultOrder' => [
                    'created_at' => SORT_DESC,
                ],
            ]
        );
        $model = new \avatar\models\search\Anketa();
        $provider = $model->search(Yii::$app->request->get(), null, $sort);
        ?>
        <?= \yii\grid\GridView::widget([
            'dataProvider' => new \yii\data\ActiveDataProvider([
                'query' => \common\models\AnketaSchool::find()
            ]),
            'tableOptions' => [
                'class' => 'table table-striped table-hover',
            ],
            'rowOptions'   => function ($item) {
                return [
                    'data'  => ['id' => $item['id']],
                    'role'  => 'button',
                    'class' => 'rowTable',
                ];
            },
            'columns'      => [
                [
                    'attribute'     => 'id',
                    'header'        => $sort->link('id'),
                    'headerOptions' => [
                        'style' => 'width: 10%',
                    ],
                ],
                [
                    'attribute'     => 'name',
                    'header'        => $sort->link('name'),
                    'headerOptions' => [
                        'style' => 'width: 10%',
                    ],
                ],
                [
                    'attribute'     => 'description',
                    'header'        => $sort->link('description'),
                    'headerOptions' => [
                        'style' => 'width: 10%',
                    ],
                ],
                [
                    'attribute'     => 'email',
                    'header'        => $sort->link('email'),
                    'headerOptions' => [
                        'style' => 'width: 10%',
                    ],
                ],
                [
                    'attribute'     => 'phone',
                    'header'        => $sort->link('phone'),
                    'headerOptions' => [
                        'style' => 'width: 10%',
                    ],
                ],
                [
                    'header'        => 'Создано',
                    'attribute'     => 'created_at',
                    'headerOptions' => [
                        'style' => 'width: 10%',
                    ],
                    'content'       => function ($item) {
                        $v = \yii\helpers\ArrayHelper::getValue($item, 'created_at', 0);
                        if ($v == 0) return '';

                        return Html::tag('abbr', \cs\services\DatePeriod::back($v, ['isShort' => true]), ['class' => 'gsssTooltip', 'title' => Yii::$app->formatter->asDatetime($v)]);
                    },
                ],

                [
                    'header'        => 'Удалить',
                    'headerOptions' => [
                        'style' => 'width: 10%',
                    ],
                    'content'       => function ($item) {
                        return Html::button('Удалить', [
                            'class' => 'btn btn-danger btn-xs buttonDelete',
                            'data'  => [
                                'id' => $item['id'],
                            ],
                        ]);
                    },
                ],
            ],
        ]) ?>
        <?php \yii\widgets\Pjax::end(); ?>
    </div>
</div>
