<?php

/**
 * Created by PhpStorm.
 * User: Ramha
 * Date: 04.12.2018
 * Time: 1:32
 */

use yii\helpers\Html;

/** @var $model \common\models\AnketaSchool */
/** $this \yii\web\View  */

$this->title = $model->name;

?>

<div class="container">
    <div class="col-lg-12">
        <h1 class="page-header"><?= Html::encode($this->title) ?></h1>

        <?= \yii\widgets\DetailView::widget([
            'model'      => $model,
            'attributes' => [
                [
                    'label'          => 'Имя',
                    'value'          => $model->name,
                    'captionOptions' => [
                        'style' => 'width:30%',
                    ],
                ],
                'description',
                'email',
                'phone',
                'created_at:datetime', // creation date formatted as datetime
            ],
        ]) ?>
        <h2 class="page-header">Коментарии</h2>
        <?= \avatar\modules\Comment\Module::getComments2(4, $model->id); ?>
    </div>
</div>
