<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;
use app\models\UnionCategory;
use yii\db\Query;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \avatar\models\forms\PaySystem */

$this->title = $model->title;
?>
<div class="container">
    <div class="page-header">
        <h1><?= Html::encode($this->title) ?></h1>
    </div>

    <?php if (Yii::$app->session->hasFlash('form')) : ?>

        <div class="alert alert-success">
            Успешно добавлено.
        </div>
        <?php $id = Yii::$app->session->getFlash('form'); ?>
        <p>
            <a href="<?= \yii\helpers\Url::to(['admin-paysystem/index']) ?>" class="btn btn-success">Все ПС</a>
            <a href="<?= \yii\helpers\Url::to(['admin-paysystem/edit', 'id' => $id]) ?>" class="btn btn-success">Редактировать</a>
        </p>
    <?php else: ?>


        <div class="row">
            <div class="col-lg-5">
                <?php $form = ActiveForm::begin([
                    'id'      => 'contact-form',
                    'options' => ['enctype' => 'multipart/form-data']
                ]); ?>
                <?= $model->field($form, 'code') ?>
                <?= $model->field($form, 'title') ?>
                <?= $model->field($form, 'class_name') ?>
                <?= $model->field($form, 'currency') ?>
                <?= $model->field($form, 'image') ?>


                <hr>
                <div class="form-group">
                    <?= Html::submitButton('Обновить', [
                        'class' => 'btn btn-default',
                        'name'  => 'contact-button',
                        'style' => 'width:100%',
                    ]) ?>
                </div>
                <?php ActiveForm::end(); ?>
            </div>
        </div>

    <?php endif; ?>
</div>
