<?php

/** @var $this \yii\web\View */
/** @var $school \common\models\school\School */

use yii\helpers\Url;
use app\services\GsssHtml;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use app\services\GetArticle\Collection;

$this->title = 'Все задачи';

?>

<div class="container">
    <div class="col-lg-12">
        <h1 class="page-header"><?= Html::encode($this->title) ?></h1>

    <p><a href="<?= Url::to(['admin-task-list/add', 'id' => $school->id]) ?>" class="btn btn-default">Добавить</a>
        <a href="<?= Url::to(['admin-task-list/ajail', 'id' => $school->id]) ?>" class="btn btn-default">Статусы</a></p>

    <?php \yii\widgets\Pjax::begin(); ?>
    <?php
    $this->registerJS(<<<JS

$('.rowTable').click(function(e) {
    window.location = '/admin-task-list/edit' + '?' + 'id' + '=' + $(this).data('id');
});

$('.buttonDelete').click(function (e) {
    e.preventDefault();
    e.stopPropagation();
    if (confirm('Подтвердите удаление')) {
        var button = $(this);
        var id = $(this).data('id');
        ajaxJson({
            url: '/admin-task-list/delete' + '?' + 'id' + '=' + id,
            success: function (ret) {
                $('#modalInfo').on('hidden.bs.modal', function() {
                    button.parent().parent().remove();
                }).modal();
            }
        });
    }
});

JS
    );
    ?>
    <?= \yii\grid\GridView::widget([
        'dataProvider' => new \yii\data\ActiveDataProvider([
            'query'      => \common\models\task\Task::find()
                ->where(['school_id' => $school->id])
            ->orderBy(['created_at' => SORT_DESC])
            ,
            'pagination' => [
                'pageSize' => 100,
            ],
        ]),
        'tableOptions' => [
            'class' => 'table table-striped table-hover',
        ],
        'rowOptions'   => function ($item) {
            $data = [
                'data'  => ['id' => $item['id']],
                'role'  => 'button',
                'class' => 'rowTable',
            ];
            return $data;
        },
        'columns'      => [
            'id',
            'name',
            [
                'header'  => 'Стоимость',
                'headerOptions' => [
                    'style' => Html::cssStyleFromArray([
                        'text-align' => 'right',
                    ])
                ],
                'contentOptions' => [
                    'style' => Html::cssStyleFromArray([
                        'text-align' => 'right',
                    ])
                ],
                'content' => function (\common\models\task\Task $item) {
                    if (empty($item->price)) {
                        return '';
                    }
                    return
                        $item->getPriceFormatted() . ' ' .
                        Html::tag('span', $item->getCurrency()->code, [
                            'class' => 'label label-info',
                        ]);
                },
            ],
            [
                'header'  => 'Удалить',
                'content' => function ($item) {
                    return Html::button('Удалить', [
                        'class' => 'btn btn-danger btn-xs buttonDelete',
                        'data'  => [
                            'id' => $item['id'],
                        ],
                    ]);
                },
            ],
        ],
    ]) ?>
    <?php \yii\widgets\Pjax::end(); ?>
    </div>

</div>

<div class="modal fade" id="modalInfo" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="myModalLabel">Информация</h4>
            </div>
            <div class="modal-body">
                Успешно!
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
            </div>
        </div>
    </div>
</div>