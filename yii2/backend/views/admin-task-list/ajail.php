<?php

/** @var $this \yii\web\View */

/** @var $school \common\models\school\School */

use yii\helpers\Url;
use app\services\GsssHtml;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use app\services\GetArticle\Collection;

$this->title = 'Все задачи';
?>

<div class="container">
    <div class="col-lg-12">
        <h1 class="page-header"><?= Html::encode($this->title) ?></h1>

        <p><a href="<?= Url::to(['admin-task-list/add', 'id' => $school->id]) ?>" class="btn btn-default">Добавить</a>
        </p>

        <?php
        $this->registerJS(<<<JS


JS
        );
        ?>
        <style>
            .container123 {
                margin: 0 auto;
            }

            .connectedSortable {
                border: 1px solid #eee;
                width: 220px;
                min-height: 20px;
                list-style-type: none;
                margin: 0;
                padding: 5px;
                float: left;
                margin-right: 10px;
            }

            .connectedSortable li {
                margin: 0 5px 5px 5px;
                padding: 5px;
                width: 200px;
                border: 1px solid #eee;
            }

            /*.ui-state-highlight { height: 1.2em; line-height: 1.2em; }*/
        </style>
        <?php $this->registerJsFile('https://code.jquery.com/ui/1.12.1/jquery-ui.js', ['depends' => ['\yii\web\JqueryAsset']]);
        $this->registerJs(<<<JS


$(".connectedSortable").on("sortreceive", function(event, ui) {
    console.log([event, ui,$(this)]);
    var item_id = $(ui.item[0]).data('id');
    var list_id = $(event.currentTarget).data('id');
    console.log([item_id,list_id]);
    ajaxJson({
        url:"/admin-task-list/set",
        data: {
            item_id:item_id,
            list_id:list_id,
        },
        success: function(ret) {
            
        }
    });
});
JS
        );

        ?>


        <h2 class="page-header">Без категории</h2>

        <table border="0" style="height: 50px;">
            <tr>
                <td style="width: 230px;font-weight: bold;">Не распределено</td>
                <td style="width: 230px;font-weight: bold;">Исполняется</td>
                <td style="width: 230px;font-weight: bold;">Проверяется</td>
                <td style="width: 230px;font-weight: bold;">Выполнено</td>
            </tr>
        </table>

        <div class="row">
            <div class="container123">

                <?php
                $rows1 = \common\models\task\Task::find()->where(['school_id' => $school->id])->andWhere(['or', ['status' => null], ['status' => 0]])->all();
                $rows2 = \common\models\task\Task::find()->where(['school_id' => $school->id, 'status' => 1])->all();
                $rows3 = \common\models\task\Task::find()->where(['school_id' => $school->id, 'status' => 2])->all();
                $rows4 = \common\models\task\Task::find()->where(['school_id' => $school->id, 'status' => 3])->all();
                ?>
                <?php
                $this->registerJs(<<<JS
$( ".sortable1, .sortable2, .sortable3, .sortable4" ).sortable({
    placeholder: "ui-state-highlight",
    connectWith: ".connectedSortable"
});
JS
                );
                ?>
                <ul class="connectedSortable sortable1" data-id="0">
                    <?php /** @var \common\models\task\Task $item */ ?>
                    <?php foreach ($rows1 as $item) { ?>
                        <?= $this->render('i', ['item' => $item]) ?>
                    <?php } ?>
                </ul>

                <ul class="connectedSortable sortable2" data-id="1">
                    <?php /** @var \common\models\task\Task $item */ ?>
                    <?php foreach ($rows2 as $item) { ?>
                        <?= $this->render('i', ['item' => $item]) ?>
                    <?php } ?>
                </ul>

                <ul class="connectedSortable sortable3" data-id="2">
                    <?php /** @var \common\models\task\Task $item */ ?>
                    <?php foreach ($rows3 as $item) { ?>
                        <?= $this->render('i', ['item' => $item]) ?>
                    <?php } ?>
                </ul>

                <ul class="connectedSortable sortable4" data-id="3">
                    <?php /** @var \common\models\task\Task $item */ ?>
                    <?php foreach ($rows4 as $item) { ?>
                        <?= $this->render('i', ['item' => $item]) ?>
                    <?php } ?>
                </ul>

            </div>
        </div>



    </div>
</div>

</div>
