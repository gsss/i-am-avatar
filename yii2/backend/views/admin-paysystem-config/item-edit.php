<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;
use app\models\UnionCategory;
use yii\db\Query;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model \yii\base\Model */
/* @var $class string */
/* @var $title string */

$this->title = $title;

$path = '@avatar/modules/Piramida/PaySystems/' . $class . '/template';
$pathPhp = '@avatar/modules/Piramida/PaySystems/' . $class . '/template.php';
$file = Yii::getAlias($pathPhp);
if (!file_exists($file)) {
    $path = '@avatar/modules/Piramida/PaySystems/Base/template';
}
?>
<div class="container">
    <h1 class="page-header"><?= Html::encode($this->title) ?></h1>


    <?php if (Yii::$app->session->hasFlash('contactFormSubmitted')): ?>

        <div class="alert alert-success">
            Успешно обновлено.
        </div>

    <?php else: ?>


        <div class="row">
            <div class="col-lg-5">
                <?= $this->render($path, ['model' => $model]); ?>
            </div>
        </div>
    <?php endif; ?>
</div>
