<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;
use app\models\UnionCategory;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model cs\base\BaseForm */

$this->title = 'Завершение регистрации';
?>
<div class="container">
    <div class="col-lg-12">
        <h1 class="page-header text-center"><?= Html::encode($this->title) ?></h1>


    </div>
    <div class="col-lg-4 col-lg-offset-4">
        <?php if (Yii::$app->session->hasFlash('form')): ?>

            <div class="alert alert-success text-center">
                Успешно обновлено.
            </div>
            <p>
                <a href="/cabinet/registration-activate-get-seeds" class="btn btn-primary" style="width:100%;"><i class="glyphicon glyphicon-download-alt"></i> Скачать SEEDs в PDF</a>
            </p>

            <hr>

            <p>
                <a href="/cabinet-bills/index" class="btn btn-default" style="width:100%;">Перейти в кабинет</a>
            </p>

        <?php else: ?>

            <?php $form = ActiveForm::begin(); ?>

            <?= $form->field($model, 'password1')->passwordInput() ?>
            <?= $form->field($model, 'password2')->passwordInput() ?>

            <hr class="featurette-divider">
            <?= Html::submitButton('Обновить', [
                'class' => 'btn btn-default',
                'name'  => 'contact-button',
                'style' => 'width:100%',
            ]) ?>
            <?php ActiveForm::end(); ?>
        <?php endif; ?>
    </div>
</div>