<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;
use app\models\UnionCategory;
use yii\db\Query;
use yii\helpers\Url;

/* @var $this yii\web\View */
$requestedRoute = Yii::$app->requestedRoute;

$isSchool = !\common\models\school\School::isRoot();

$menu = [];

$menu[] = [
        'route' => 'cabinet/profile',
        'label' => Yii::t('c.kTtfiHPqec', 'Профиль'),
];

$menu[] = [
        'route' => 'cabinet/shop-requests',
        'label' => 'Заявки из магазина',
        'urlList' => [
            ['route', 'cabinet/shop-requests'],
            ['route', 'cabinet/shop-requests-item'],
        ],
];
$menu[] = [
        'route' => 'cabinet-documents/index',
        'label' => 'Документы',
        'urlList' => [
            ['controller', 'cabinet-documents'],
        ],
];

if (!$isSchool) {
    $menu[] =     [
        'route'   => 'cabinet-certificate/index',
        'label'   => 'Сертификаты',
        'urlList' => [
            ['controller', 'cabinet-certificate'],
        ],
    ];
    $menu[] =     [
        'route'   => 'cabinet-idea/index',
        'label'   => 'Идеи и предложения',
        'urlList' => [
            ['controller', 'cabinet-idea'],
        ],
    ];
}
$menu[] =     [
    'route'   => 'cabinet/human-design',
    'label'   => 'Дизайн аватара',
    'urlList' => [
        ['startsWith', '/cabinet/human-design'],
        ['controller', 'human-design'],
    ],
];
$menu[] =     [
    'route'   => 'cabinet/subscribe',
    'label'   => 'Рассылка',
    'urlList' => [
        ['startsWith', '/cabinet/subscribe'],
    ],
];
$menu[] =     [
    'route'   => 'cabinet/card-action',
    'label'   => 'Модель поведения после сканирования QR кода вашей карты',
    'urlList' => [
        ['route', 'cabinet/card-action'],
    ],
];
$menu[] =     [
    'route'   => 'cabinet-digital-sign/index',
    'label'   => 'Цифровая подпись',
    'urlList' => [
        ['controller', 'cabinet-digital-sign'],
    ],
];
if (!$isSchool) {
    $menu[] =     [
        'route'   => 'cabinet/telegram',
        'label'   => 'Telegram бот',
        'urlList' => [
            ['startsWith', '/cabinet/telegram'],
        ],
    ];
}

$menu[] =     [
    'route'   => 'cabinet/referal-link',
    'label'   => 'Реферальная ссылка',
    'urlList' => [
        ['startsWith', '/cabinet/referal-link'],
    ],
];
$menu[] =     [
    'route'   => 'cabinet/referal-transactions',
    'label'   => 'Реферальные начисления',
    'urlList' => [
        ['startsWith', '/cabinet/referal-transactions'],
    ],
];
$menu[] =     [
    'route'   => 'cabinet/referal-structure',
    'label'   => 'Реферальная структура',
    'urlList' => [
        ['startsWith', '/cabinet/referal-structure'],
    ],
];


if ($isSchool) {
    $menu[] =     [
        'route'   => 'cabinet-referal-request/index',
        'label'   => 'Заявки на вывод реферальных начислений',
        'urlList' => [
            ['controller', 'cabinet-referal-request'],
        ],
    ];
}
$menu[] =     [
    'route'   => 'cabinet/modal-master',
    'label'   => 'Мастер помощник',
    'urlList' => [
        ['startsWith', '/cabinet/modal-master'],
    ],
];

if (!$isSchool) {
    $menu[] =     [
        'route'   => 'cabinet/catalog-master',
        'label'   => 'Участие в каталоге мастеров',
        'urlList' => [
            ['startsWith', '/cabinet/catalog-master'],
        ],
    ];
    $menu[] =     [
        'route'   => 'cabinet-map/index',
        'label'   => 'Участие на карте',
        'urlList' => [
            ['controller', 'cabinet-map'],
        ],
    ];
}


function hasRoute12($item, $route)
{
    if (isset($item['items'])) {
        foreach ($item['items'] as $i) {
            if ($i['route'] == $route) {
                return true;
            }
        }
    }
    if (isset($item['route'])) {
        if ($item['route'] == $route) return true;
    }
    if (\yii\helpers\ArrayHelper::keyExists('urlList', $item)) {
        foreach ($item['urlList'] as $i) {
            switch ($i[0]) {
                case 'route':
                    if ($route == $i[1]) return true;
                    break;
                case 'startsWith':
                    if (\yii\helpers\StringHelper::startsWith($route, $i[1])) return true;
                    break;
                case 'controller':
                    $arr = explode('/', $route);
                    if ($arr[0] == $i[1]) return true;
                    break;
            }
        }
    }

    return false;
}

?>

<div class="list-group">
    <?php
    foreach ($menu as $item) {
        $options = ['class' => ['list-group-item']];
        if (hasRoute12($item, Yii::$app->requestedRoute)) {
            $options['class'][] = 'active';
        }
        if (\yii\helpers\ArrayHelper::keyExists('data', $item)) {
            $options['data'] = $item['data'];
        }
        $options['class'] = join(' ', $options['class']);
        echo Html::a($item['label'], [$item['route']], $options);
    }
    ?>
</div>
