<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;
use app\models\UnionCategory;

/* @var $this yii\web\View */

$this->title = 'Реферальные начисления';

\avatar\assets\Notify::register($this);
\avatar\assets\Clipboard::register($this);

$this->registerJs(<<<JS

var clipboard = new Clipboard('.buttonCopy');
clipboard.on('success', function(e) {

    new Noty({
        timeout: 1000,
        theme: 'sunset',
        type: 'success',
        layout: 'bottomLeft',
        text: 'Скопировано'
    }).show();

});

JS
);

$query = \common\models\school\ReferalTransaction::find()
    ->where(['to_uid' => Yii::$app->user->id]);


// Если это страницу открывает человек в школе то ему показываются отчисления только по этой школе
if (!\common\models\school\School::isRoot()) {
    $school = \common\models\school\School::get();
    $wallet_id = $school->referal_wallet_id;
    $wallet = \common\models\piramida\Wallet::findOne($wallet_id);
    $query->andWhere(['currency_id' => $wallet->currency_id]);
}

?>
<div class="container">
    <div class="col-lg-12">
        <h1 class="page-header"><?= Html::encode($this->title) ?></h1>
    </div>
    <div class="col-lg-8">

        <?php \yii\widgets\Pjax::begin(); ?>
        <?php
        $this->registerJS(<<<JS

$('.rowTable').click(function() {
   // window.location = '/admin-anketa/view?id=' + $(this).data('id');
});
JS
        );
        $sort = new \yii\data\Sort([
            'attributes'   => [
                'id'         => ['label' => "ID"],
                'level'      => ['label' => "Уровень"],
                'request_id',
                'to_uid',
                'to_wid',
                'from_uid'     => ['label' => "От кого"],
                'amount',
                'currency_id',
                'transaction_id',
                'contact_email',
                'created_at' => ['label' => "Создано"],
            ],
            'defaultOrder' => [
                'created_at' => SORT_DESC,
            ],
        ]);
//        $model = new \avatar\models\search\Anketa();
        //$provider = $model->search(Yii::$app->request->get(), null, $sort);
        ?>
        <?= \yii\grid\GridView::widget([
            'dataProvider' => new \yii\data\ActiveDataProvider([
                'query' => $query
                ,
                'sort'  => $sort,
            ]),
//            'filterModel'  => $model,
            'tableOptions' => [
                'class' => 'table table-striped table-hover',
            ],
            'rowOptions'   => function ($item) {
                return [
                    'data'  => ['id' => $item['id']],
                    'role'  => 'button',
                    'class' => 'rowTable',
                ];
            },
            'columns'      => [
                [
                    'attribute'     => 'id',
                    'header'        => $sort->link('id'),
                    'headerOptions' => [
                        'style' => 'width: 10%',
                    ],
                ],
                [
                    'attribute'     => 'level',
                    'header'        => $sort->link('level'),
                    'headerOptions' => [
                        'style' => 'width: 10%',
                    ],
                ],
                [
                    'attribute'     => 'request_id',
                    'header'        => $sort->link('request_id'),
                    'headerOptions' => [
                        'style' => 'width: 10%',
                    ],
                ],
                [
                    'attribute'     => 'from_uid',
                    'header'        => $sort->link('from_uid'),
                    'headerOptions' => [
                        'style' => 'width: 10%',
                    ],
                    'content'       => function ($model) {
                        $item = \common\models\UserAvatar::findOne($model['from_uid']);
                        $i = $item->getAvatar();
                        $size = 40;
                        $params = [
                            'class'  => "img-circle",
                            'width'  => $size,
                            'height' => $size,
                            'style'  => ['margin-bottom' => '0px'],
                        ];
                        if (\yii\helpers\StringHelper::endsWith($i, '/images/iam.png')) {
                            $params['style']['opacity'] = '0.3';
                            $params['style']['border'] = '1px solid #888';
                        }
                        if (isset($params['style'])) {
                            if (is_array($params['style'])) {
                                $params['style'] = Html::cssStyleFromArray($params['style']);
                            }
                        }

                        return Html::img($i, $params);
                    },
                ],
                [
                    'attribute'     => 'amount',
                    'header'        => $sort->link('amount'),
                    'headerOptions' => [
                        'style' => 'width: 10%',
                    ],
                    'content'       => function ($model) {
                        $c = \common\models\piramida\Currency::findOne($model['currency_id']);
                        $v = $model['amount'] / pow(10, $c->decimals);

                        return Yii::$app->formatter->asDecimal($v, $c->decimals);
                    },
                ],
                [
                    'attribute'     => 'currency_id',
                    'header'        => $sort->link('currency_id'),
                    'headerOptions' => [
                        'style' => 'width: 10%',
                    ],
                    'content'       => function ($model) {
                        $link = \common\models\avatar\CurrencyLink::findOne(['currency_int_id' => $model['currency_id']]);
                        if (is_null($link)) return '';
                        $c = \common\models\avatar\Currency::findOne($link->currency_ext_id);
                        $i = $c->image;
                        $size = 40;
                        $params = [
                            'class'  => "img-circle",
                            'width'  => $size,
                            'height' => $size,
                            'style'  => ['margin-bottom' => '0px'],
                        ];
                        if (isset($params['style'])) {
                            if (is_array($params['style'])) {
                                $params['style'] = Html::cssStyleFromArray($params['style']);
                            }
                        }

                        return Html::img($i, $params);
                    },
                ],

                [
                    'attribute'     => 'transaction_id',
                    'header'        => $sort->link('transaction_id'),
                    'headerOptions' => [
                        'style' => 'width: 10%',
                    ],
                    'content'   => function ($model) {

                        $item = \common\models\piramida\Transaction::findOne($model['transaction_id']);
                        $address = $item->getAddress();
                        $options = [
                            'data'  => [
                                'toggle'         => 'tooltip',
                                'clipboard-text' => $address,
                            ],
                            'class' => 'buttonCopy',
                            'title' => 'Нажми чтобы скопировать',
                        ];

                        return Html::tag('code', $item->getAddressShort(), $options);
                    },
                ],
                [
                    'header'        => 'Создано',
                    'attribute'     => 'created_at',
                    'headerOptions' => [
                        'style' => 'width: 10%',
                    ],
                    'content'       => function ($item) {
                        $v = \yii\helpers\ArrayHelper::getValue($item, 'created_at', 0);
                        if ($v == 0) return '';

                        return Html::tag('abbr', \cs\services\DatePeriod::back($v, ['isShort' => true]), ['class' => 'gsssTooltip', 'title' => Yii::$app->formatter->asDatetime($v)]);
                    },
                ],

            ],
        ]) ?>
        <?php \yii\widgets\Pjax::end(); ?>

    </div>
    <div class="col-lg-4">
        <?= $this->render('_menu') ?>
    </div>
</div>



