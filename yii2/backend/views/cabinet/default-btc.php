<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;
use app\models\UnionCategory;

/* @var $this yii\web\View */

$this->title = Yii::t('c.uE83KTCQzP', 'Счет по умолчанию');

$ids = \common\models\avatar\UserBillDefault::find()->where([
    'user_id' => Yii::$app->user->id,
])->column();

$default = null;
try {
    $default = \common\models\avatar\UserBill::findOne([
        'id'       => $ids,
        'currency' => \common\models\avatar\Currency::BTC,
    ])->id;
} catch (Exception $e) {

}
Yii::$app->session->set('default', $default);
?>
<div class="container">
    <div class="col-lg-12">
        <h1 class="page-header"><?= Html::encode($this->title) ?></h1>
    </div>
    <div class="col-lg-8">

        <?php if (Yii::$app->session->hasFlash('form')) { ?>
            <p class="alert alert-success"><?= \Yii::t('c.uE83KTCQzP', 'Успешно') ?></p>

            <a href="/cabinet/default" class="btn btn-default"><?= \Yii::t('c.uE83KTCQzP', 'Назад') ?></a>
            <a href="/cabinet-bills/index" class="btn btn-default"><?= \Yii::t('c.uE83KTCQzP', 'Счета') ?></a>
        <?php } else { ?>
            <?php $form = ActiveForm::begin() ?>
            <?= \yii\grid\GridView::widget([
                'dataProvider' => new \yii\data\ActiveDataProvider([
                    'query' => \common\models\avatar\UserBill::find()
                        ->where([
                            'mark_deleted' => 0,
                            'user_id'      => Yii::$app->user->id,
                            'currency'     => \common\models\avatar\Currency::BTC,
                        ])
                ]),
                'tableOptions' => [
                    'class' => 'table table-hover table-striped',
                    'id'    => 'tableTransaction',
                ],
                'columns'      => [
                    'id',
                    'name',
                    [
                        'header'  => 'address',
                        'content' => function ($item) {
                            return Html::tag('code', $item['address']);
                        },
                    ],
                    [
                        'header'  => Yii::t('c.uE83KTCQzP', 'По умолчниаю'),
                        'content' => function ($item) {
                            $default = Yii::$app->session->get('default');
                            if (is_null($default)) {
                                return Html::submitButton(Yii::t('c.uE83KTCQzP', 'Включить'), ['name' => 'billing', 'value' => $item['id'], 'class' => 'btn btn-default btn-xs buttonSet', 'data' => ['id' => $item['id']]]);
                            } else {
                                if ($item->id == $default) {
                                    return Html::button(Yii::t('c.uE83KTCQzP', 'Активен'), ['class' => 'btn btn-info btn-xs']);
                                } else {
                                    return Html::submitButton(Yii::t('c.uE83KTCQzP', 'Включить'), ['name' => 'billing', 'value' => $item['id'], 'class' => 'btn btn-default btn-xs buttonSet', 'data' => ['id' => $item['id']]]);
                                }
                            }
                        },
                    ],
                ]
            ]) ?>
            <?php ActiveForm::end() ?>
        <?php } ?>

    </div>
    <div class="col-lg-4">
        <?= $this->render('_menu') ?>
    </div>
</div>



