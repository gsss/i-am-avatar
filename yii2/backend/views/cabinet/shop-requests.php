<?php
use common\models\school\PotokUser3Link;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \avatar\models\forms\Subscribe */

$this->title = 'Заявки из магазина';

?>
<div class="container">
    <div class="col-lg-12">
        <h1 class="page-header"><?= Html::encode($this->title) ?></h1>
    </div>
    <div class="col-lg-8">
        <p>В этом разделе вы можете управлять своими заявками из разных магазинов.</p>


        <?php
        $this->registerJS(<<<JS

$('.rowTable').click(function() {
    window.location = '/cabinet/shop-requests-item' + '?' + 'id' + '=' + $(this).data('id');
});
JS
        );
        ?>
        <?= \yii\grid\GridView::widget([
            'dataProvider' => new \yii\data\ActiveDataProvider([
                'query' => \common\models\shop\Request::find()
                    ->where(['user_id' => Yii::$app->user->id]),

                'sort' => ['defaultOrder' => ['created_at' => SORT_DESC]],
            ]),

            'tableOptions' => [
                'class' => 'table table-striped table-hover',
            ],
            'rowOptions'   => function ($item) {
                $data = [
                    'data'  => ['id' => $item['id']],
                    'role'  => 'button',
                    'class' => 'rowTable',
                ];
                return $data;
            },
            'summary'      => '',
            'columns'      => [
                'id',
                [
                    'header'  => 'Школа',
                    'content' => function ($item) {
                        $school_id = $item['school_id'];
                        $school = \common\models\school\School::findOne($school_id);

                        $i = ArrayHelper::getValue($school, 'image', '');
                        if ($i == '') return '';

                        return Html::img(
                            \common\widgets\FileUpload7\FileUpload::getFile($i, 'crop'),
                            [
                                'class'  => "img-circle",
                                'width'  => 60,
                                'height' => 60,
                                'style'  => 'margin-bottom: 0px;',
                                'title'  => $school->name,
                                'data' => ['toggle' => 'tooltip']
                            ]);
                    }
                ],
                [
                    'header'  => 'Создано',
                    'content' => function ($item) {
                        $v = \yii\helpers\ArrayHelper::getValue($item, 'created_at', 0);
                        if ($v == 0) return '';

                        return Html::tag('abbr', \cs\services\DatePeriod::back($v, ['isShort' => true]), ['data' => ['toggle' => 'tooltip'], 'title' => Yii::$app->formatter->asDatetime($v)]);
                    }
                ],
                [
                    'header'  => 'Последнее сообщение',
                    'content' => function ($item) {
                        $v = \yii\helpers\ArrayHelper::getValue($item, 'last_message_time', 0);
                        if ($v == 0) return '';

                        return Html::tag('abbr', \cs\services\DatePeriod::back($v, ['isShort' => true]), ['data' => ['toggle' => 'tooltip'], 'title' => Yii::$app->formatter->asDatetime($v)]);
                    },
                ],
                [
                    'header'  => 'Оплачено?',
                    'content' => function ($item) {
                        $v = \yii\helpers\ArrayHelper::getValue($item, 'is_paid', 0);

                        if ($v) {
                            $html = Html::tag('span', 'Да', ['class' => 'label label-success']);
                        } else {
                            $html = Html::tag('span', 'Нет', ['class' => 'label label-default']);
                        }

                        return $html;
                    },
                ],
                [
                    'header'         => 'Стоимость',
                    'headerOptions'  => ['class' => 'text-right'],
                    'contentOptions' => ['class' => 'text-right'],
                    'content'        => function ($item) {
                        $v = \yii\helpers\ArrayHelper::getValue($item, 'price', 0);
                        if ($v == 0) return '';

                        return Yii::$app->formatter->asDecimal($v / 100, 2);
                    },
                ],
            ],
        ]) ?>


    </div>
    <div class="col-lg-4">
        <?= $this->render('_menu') ?>
    </div>


</div>