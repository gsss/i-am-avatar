<?php

use yii\helpers\Html;
use yii\helpers\Url;


/** @var \common\models\UserAvatar $user */
$user = Yii::$app->user->identity;

/** @var \yii\web\View $this */
$this->title = \yii\helpers\Html::encode($user->getName2());

$certList = \common\models\school\Sertificate::find()->where(['user_id' => $user->id])->all();

$rows = (count($certList) + 2) / 3;
$rows = (int)$rows;
\common\assets\iLightBox\Asset::register($this);
$this->registerJs(<<<JS
$('.ilightbox').iLightBox();
JS
);
\avatar\assets\Paralax::register($this);

$background = '//www.i-am-avatar.com/upload/cloud/1546392582_SLqjARoGrO.jpg';

$school = null;
try {
    $school = \common\models\school\School::get();
    $design = \common\models\school\SchoolDesign::findOne(['school_id' => $school->id]);
    if (!is_null($design)) {
        if ($design->background_cabinet) {
            $background = $design->background_cabinet;
        }
    }
} catch (Exception $e) {

}
$video = 'XNY3YBZstLU';

$is_show = true;
if (is_null($school)) {
    // Я нахожусь на аватаре
    if ($user->is_hide_master_enter_window == 0) {
        $is_closed_master_enter_window = Yii::$app->request->cookies->get('is_closed_master_enter_window');
        if (is_null($is_closed_master_enter_window)) {
            // показывать
        } else {
            if ($is_closed_master_enter_window->value == 1) {
                $is_show = false;
            }
        }
    } else {
        // показывать
        $is_show = false;
    }
} else {
    $is_show = false;
    if (isset($design)) {
        if (!is_null($design)) {
            if ($design->video_intro) {
                $is_show = true;
                $video = $design->video_intro;
            }
        }
    }
}
if ($is_show) {
    $this->registerJs(<<<JS
$('#modalInfo').on('hide.bs.modal', function(e) {
    setCookie2('is_closed_master_enter_window', 1);
}).modal();

function setCookie2 (name, value) {
    var date = new Date();
    var now = new Date();
    var time = now.getTime();
    var expireTime = time + (1000 * 60 * 60 * 24 * 365);
    now.setTime(expireTime);
    document.cookie = name + "=" + value +
    "; expires=" + now.toGMTString() +
    "; path=/";
}

JS
    );

    $name = 'is_show';
    $nameMd5 = md5($name);
    $this->registerJs(<<<JS
$('#m2-is_show').change(function(e) {
    ajaxJson({
        url: '/cabinet/modal-master-set-value',
        data: {value: 1},
        success: function(ret) {
            
        }
    });
});
JS
    );
}

?>

<div class="parallax-window"
     data-parallax="scroll"
     data-image-src="<?= $background ?>"

     style="min-height: 450px; background: transparent;" >

    <div class="container">
        <div class="col-sm-4 col-sm-offset-4">
            <p style="padding-top: 70px; margin-bottom: 0px;"><img
                        src="<?= $user->getAvatar() ?>"
                        class="img-circle" width="100%"
                        style="text-shadow: 0px 10px 20px rgba(0, 0, 0, 0.5);color:white;"></p>
        </div>
    </div>

</div>
<div class="container" style="padding-bottom: 70px;">
    <div class="col-lg-12">
        <h1 class="page-header text-center">
            <?= \yii\helpers\Html::encode($user->getName2()) ?>
        </h1>
    </div>
    <?php if ($user->hasHumanDesign()) { ?>
        <div class="col-lg-12">
            <p class="text-center">
                <img src="<?= $user->getHumanDesign()->image ?>" style="width: 100%; max-width: 600px;">
            </p>
        </div>
        <div class="col-lg-12">
            <table align="center">
                <tr>
                    <td title="дизайн луна"></td>
                    <td title="дизайн солнце"></td>
                    <td title="личность солнце"></td>
                    <td title="личность луна"></td>
                </tr>
                <tr>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                </tr>
            </table>
        </div>
    <?php } ?>

    <div class="col-lg-12">
        <h1 class="page-header text-center">
            Сертификаты
        </h1>
        <div class="popup-gallery">
            <?php for ($count = 0; $count < count($certList); $count++) { ?>
                <?php /** @var \common\models\school\Sertificate $c */ ?>
                <?php $c = $certList[$count]; ?>
                <div class="col-lg-4">
                    <a href="<?= $c->image ?>" class="ilightbox">
                        <img src="<?= \common\widgets\FileUpload7\FileUpload::getFile($c->image, 'crop') ?>" width="100%" class="thumbnail">
                    </a>
                    <p class="lead text-center">
                        <?= $c->name ?>
                    </p>
                </div>
            <?php } ?>
        </div>
    </div>

</div>


<div class="modal fade" id="modalInfo" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Мастер помощник</h4>
            </div>
            <div class="modal-body">
                <p class="alert alert-info">Это видео расскажет вам как устроен наш кабинет</p>
                <iframe width="100%" height="315" src="https://www.youtube.com/embed/<?= $video ?>" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

                <div class="row" style="margin-top: 50px;">
                    <div class="col-lg-2">
                        <?= \common\widgets\CheckBox2\CheckBox::widget([
                            'model'     => new \avatar\models\M2(['is_show' => true]),
                            'attribute' => 'is_show',
                        ]); ?>
                    </div>
                    <div class="col-lg-10">
                        Показывать всегда при авторизации?
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
            </div>
        </div>
    </div>
</div>
