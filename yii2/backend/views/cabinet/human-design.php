<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;
use yii\helpers\ArrayHelper;

/* @var $this  \yii\web\View */
/* @var $model \avatar\models\forms\ProfileHumanDesignCalc */


$this->title = 'Дизайн Аватара';

/** @var \common\models\UserAvatar $user */
$user = Yii::$app->user->identity;
?>
<div class="container">
    <div class="col-lg-12">
        <h1 class="page-header"><?= Html::encode($this->title) ?></h1>


    </div>
    <div class="col-lg-8">
        <?php if ($user->hasHumanDesign()): ?>
            <?php $humanDesign = $user->getHumanDesign();

            $this->registerJs(<<<JS
$('.buttonDelete').click(function(){
    ajaxJson({
            url: '/cabinet/human-design-delete',
            success: function(){
                location.reload();
            }
        })
    }
);
JS
            );
            ?>

            <div class="panel panel-default">
                <div class="panel-heading">Дизайн Аватара

                    <div class="btn-group pull-right">
                        <button type="button" class="btn btn-default btn-xs buttonDelete">
                            Удалить и пересчитать
                        </button>
                    </div>
                </div>
                <div class="panel-body">
                    <img src="https://www.i-am-avatar.com<?= $humanDesign->getImage() ?>" style="width: 100%;">
                    <table class="table table-striped table-hover" style="width: auto;" align="center">
                        <tr>
                            <td>Тип</td>
                            <td>
                                <?= $humanDesign->type->text ?>
                            </td>
                        </tr>
                        <tr>
                            <td>Профиль</td>
                            <td>
                                <?= $humanDesign->profile->text ?>
                            </td>
                        </tr>
                        <tr>
                            <td>Определение</td>
                            <td><?= $humanDesign->definition->text ?></td>
                        </tr>
                        <tr>
                            <td>Внутренний Авторитет</td>
                            <td><?= $humanDesign->inner->text ?></td>
                        </tr>
                        <tr>
                            <td>Стратегия</td>
                            <td><?= $humanDesign->strategy->text ?></td>
                        </tr>
                        <tr>
                            <td>Тема ложного Я</td>
                            <td><?= $humanDesign->theme->text ?></td>
                        </tr>
                        <tr>
                            <td>Инкарнационный крест</td>
                            <?php
                            $text = $humanDesign->cross->text;
                            $arr = explode('(', $text);
                            $text = trim($arr[0]);
                            $isError = false;
                            if (count($arr) == 1) {
//                                \cs\Application::mail('admin@galaxysss.ru', 'Ошибка', 'admin', [
//                                    'text' => '/galaxysss/views/site/user.php:187 ' . \yii\helpers\VarDumper::dumpAsString([$arr, $humanDesign])
//                                ]);
                                $isError = true;
                            } else {
                                $arr = explode(')', $arr[1]);
                                $arr = explode('|', $arr[0]);
                                $new = [];
                                foreach ($arr as $i) {
                                    $a = explode('/', $i);
                                    $new[] = trim($a[0]);
                                    $new[] = trim($a[1]);
                                }
                            }
                            ?>
                            <td><?= $text ?>
                                <?php if (!$isError) { ?>
                                    (<a
                                            href="<?= Url::to(['human-design/gen-keys-item', 'id' => $new[0]]) ?>"><?= $new[0] ?></a>/
                                    <a
                                            href="<?= Url::to(['human-design/gen-keys-item', 'id' => $new[1]]) ?>"><?= $new[1] ?></a>
                                    | <a
                                            href="<?= Url::to(['human-design/gen-keys-item', 'id' => $new[2]]) ?>"><?= $new[2] ?></a>/
                                    <a
                                            href="<?= Url::to(['human-design/gen-keys-item', 'id' => $new[3]]) ?>"><?= $new[3] ?></a>)
                                <?php } ?>
                            </td>
                        </tr>
                    </table>
                    <h3>Генные ключи</h3>
                    <?php $rows = []; ?>
                    <?php foreach ($new as $i) { ?>
                        <?php $rows[] = \common\models\HdGenKeys::findOne(['num' => $i]); ?>
                    <?php } ?>
                    <?php $this->registerJs(<<<JS
$('.rowTable').click(function() {
    window.location = '/human-design/gen-keys-item' + '?' + 'id' + '=' + $(this).data('id');
});
JS
); ?>
                    <?= \yii\grid\GridView::widget([
                        'dataProvider' => new \yii\data\ArrayDataProvider([
                            'allModels'  => $rows,
                        ]),
                        'tableOptions' => [
                            'class' => 'table table-hover table-striped',
                        ],
                        'rowOptions'   => function ($item) {
                            $data = [
                                'data'  => ['id' => $item['id']],
                                'role'  => 'button',
                                'class' => 'rowTable'
                            ];
                            return $data;
                        },
                        'summary' => '',
                        'columns' => [
                            [
                                'header' => '#',
                                'content' => function($item) {
                                    return Html::a($item['num'], ['human-design/gen-keys-item', 'id' => $item['num']]);
                                }
                            ],
                            'name:text:Название',
                            'ten:text:Тень',
                            'dar:text:Дар',
                            'siddhi:text:Сиддхи',
                        ],
                    ]) ?>
                </div>
            </div>

        <?php else: ?>
            <p>Для того чтобы расчитать Дизайн Человека вам необходимо заполнить следующие данные:</p>
            <div class="hide" id="loginFormLoading">
                <img style="padding-left: 10px; padding-right: 10px;"
                     src="/images/ajax-loader.gif"
                >
            </div>
            <!--                Форма подключения дизайна человека -->
            <div class="col-lg-8 row">
                <?php
                $this->registerJs(<<<JS
    

    $('#buttonNext').click(function(){
        ajaxJson({
            url: '/cabinet/human-design-ajax',
            data: {
                date: $('#profilehumandesigncalc-date').val(),
                time: $('#profilehumandesigncalc-time').val(),
                country: $('#profilehumandesigncalc-country').val(),
                town: $('#profilehumandesigncalc-town').val()
            },
            success: function(ret3) {
                window.location.reload();
            }
        });
    });
JS
                );
                $this->registerJs(<<<JS
    

    $('#profilehumandesigncalc-country').on('change', function() {
        var o = $(this);
        var v = o.val();
        
        if (v != '-') {
            ajaxJson({
                url: '/cabinet/human-design-town',
                data: {
                    id: v
                },
                success: function(ret3) {
                    /**
                    * ret3
                    * [
                    *   {
                    *       id:
                    *       name: англ
                    *       title: рус
                    *   }
                    * ]
                    */
                    var tObject = $('#profilehumandesigncalc-town');
                    tObject.html('');
                    tObject.append($('<option>', {value:0}).html('- Ничего не выбрано -'));
                    $.each(ret3, function(i, v2) {
                        tObject.append($('<option>', {value:v2.id}).html(v2.title));
                    });
                    if (ret3.length == 1) {
                        $('.field-profilehumandesigncalc-town').hide();
                    } else {
                        $('.field-profilehumandesigncalc-town').show();
                    }
                }
            });
        } else {
            $('.field-profilehumandesigncalc-town').hide();
        }
    });
JS
                );
                $form = ActiveForm::begin([
                    'id'                 => 'contact-form',
                    'enableClientScript' => true,
                ]); ?>

                <?= $model->field($form, 'date') ?>
                <?= $model->field($form, 'time') ?>
                <?= $model->field($form, 'country')->dropDownList(
                    ArrayHelper::merge(
                        [0 => '- Ничего не выбрано -'],
                        ArrayHelper::map(
                            \common\models\HD::find()
                                ->orderBy(['title' => SORT_ASC])->all(),
                            'id',
                            'title'
                        )
                    )
                ) ?>
                <?= $model->field($form, 'town')->dropDownList(['0' => '- Ничего не выбрано -']) ?>

                <div class="form-group">
                    <hr>
                    <?= Html::button('Далее', [
                        'class' => 'btn btn-primary',
                        'name'  => 'contact-button',
                        'style' => 'width:100%',
                        'id'    => 'buttonNext'
                    ]) ?>
                </div>
                <?php ActiveForm::end(); ?>
            </div>
            <!-- /Форма подключения дизайна человека -->

        <?php endif; ?>
    </div>
    <div class="col-lg-4">
        <?= $this->render('_menu') ?>
    </div>


</div>
