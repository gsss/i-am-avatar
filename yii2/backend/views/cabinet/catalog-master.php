<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;
use app\models\UnionCategory;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \avatar\models\forms\UserMaster */

$this->title = 'Участие в каталоге мастеров';

/** @var \common\models\UserAvatar $user */
$user = Yii::$app->user->identity;

$this->registerJs(<<<JS
$('#m2-is_show').change(function(e) {
    var v = ($(this).is(':checked'))? 1 : 0;
    ajaxJson({
        url: '/cabinet/catalog-master-set-value',
        data: {value: v},
        success: function(ret) {
            
        }
    });
});
JS
);

?>
<div class="container">
    <div class="col-lg-12">
        <h1 class="page-header"><?= Html::encode($this->title) ?></h1>

    </div>
    <div class="col-lg-8">

        <div class="row"                style="margin-bottom: 50px;">
            <div class="col-lg-2">
                <?= \common\widgets\CheckBox2\CheckBox::widget([
                    'model'     => new \avatar\models\M2(['is_show' => ($user->is_master == 1)]),
                    'attribute' => 'is_show',
                ]); ?>
            </div>
            <div class="col-lg-10">
                Участвовать в каталоге учителей
            </div>
        </div>

        <?php if (Yii::$app->session->hasFlash('form')): ?>

            <div class="alert alert-success">
                <?= \Yii::t('c.FMg0mLqFR9', 'Успешно обновлено') ?>.
            </div>

            <p>
                <a href="<?= \yii\helpers\Url::to(['cabinet/catalog-master']) ?>" class="btn btn-primary">Назад</a>
            </p>

        <?php else: ?>

            <?php $form = ActiveForm::begin([]); ?>
            <?= $model->field($form, 'contact_email') ?>
            <?= $model->field($form, 'contact_vk') ?>
            <?= $model->field($form, 'contact_fb') ?>
            <?= $model->field($form, 'contact_youtube') ?>
            <?= $model->field($form, 'contact_tweeter') ?>
            <?= $model->field($form, 'contact_instagram') ?>
            <?= $model->field($form, 'contact_telegram') ?>
            <?= $model->field($form, 'contact_whatsapp') ?>
            <?= $model->field($form, 'contact_phone') ?>
            <?= $model->field($form, 'contact_viber') ?>
            <?= $model->field($form, 'bio') ?>
            <?= $model->field($form, 'vozm') ?>
            <?= $model->field($form, 'is_sert') ?>
            <?= $model->field($form, 'is_design') ?>
            <h2 class="page-header">Информация для репоста в соцсетях</h2>
            <p>Если здесь информация не будет задана, то ини будут следующими:<br>
                заголовок - имя и фамилия, при отсутствии идентификатор пользователя<br>
                описание - профиль из дизайна Аватара, при отсутствии ничего не будет указано<br>
                картинка - фото профиля, при отсутствии заглушка для профиля</p>

            <?= $model->field($form, 'share_title') ?>
            <?= $model->field($form, 'share_description')->textarea(['rows' => 3]) ?>
            <?= $model->field($form, 'share_image') ?>
            <hr>
            <p><b>Примечание</b></p>
            <p>Соц сети кешируют информацию если вы хоть один раз сделали репост в сети. Если вы хотите обновить информацию у себя в профиле, которая находится в разделе "Информация для репоста в соцсетях", вам нужно сбросить информацию храняющуюся в соц сети. Для этого используйте следующие ссылки:<br>
                Facebook: <a href="https://developers.facebook.com/tools/debug/" target="_blank">https://developers.facebook.com/tools/debug/</a><br>
                Bkontakte: <a href="https://vk.com/dev/pages.clearCache" target="_blank">https://vk.com/dev/pages.clearCache</a>
            </p>


            <hr class="featurette-divider">
            <div class="form-group">
                <?= Html::submitButton(Yii::t('c.FMg0mLqFR9', 'Обновить'), [
                    'class' => 'btn btn-primary',
                    'name'  => 'contact-button',
                    'style' => 'width: 100%;',
                ]) ?>
            </div>
            <?php ActiveForm::end(); ?>

        <?php endif; ?>



    </div>
    <div class="col-lg-4">
        <?= $this->render('_menu') ?>
    </div>
</div>



