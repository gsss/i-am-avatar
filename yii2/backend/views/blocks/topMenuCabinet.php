<?php

/** @var $school \common\models\school\School */
/** @var $this \yii\web\View */

use yii\helpers\Url;
use yii\helpers\Html;

function liMenu($route, $name = null, $options = [])
{
    if (is_array($route)) {
        $arr = [];
        foreach ($route as $route1 => $name1) {
            $arr[] = liMenu($route1, $name1);
        }
        return join("\n", $arr);
    }
    if (Yii::$app->requestedRoute == $route) {
        $options['class'] = 'active';
    }

    return \yii\helpers\Html::tag('li', \cs\helpers\Html::a($name, [$route]), $options);
}

$menu = [
    [
        'label' => Yii::t('c.oeVo5Oaket', 'Новости'),
        'route' => 'news/index',
        'urlList' => [
            'controller' => 'news'
        ]
    ],
    [
        'label' => Yii::t('c.oeVo5Oaket', 'Блог'),
        'route' => 'blog/index',
        'urlList' => [
            'controller' => 'blog'
        ]
    ],
    [
        'label' => Yii::t('c.oeVo5Oaket', 'О нас'),
        'route' => 'site/about',
    ],
    [
        'label' => Yii::t('c.oeVo5Oaket', 'Контакты'),
        'route' => 'site/contact',
    ],

];


function isSelected($item, $route)
{
    if (isset($item['route'])) {
        if ($item['route'] == $route) return true;
    }
    if (isset($item['url'])) {
        if (\yii\helpers\StringHelper::startsWith($_SERVER['REQUEST_URI'], $item['url']) ) return true;
    }

    if (isset($item['items'])) {
        foreach($item['items'] as $i) {
            if (isSelected($i, $route)) {
                return true;
            }
        }
    }

    if (\yii\helpers\ArrayHelper::keyExists('urlList', $item)) {
        foreach($item['urlList'] as $i => $v) {
            switch($i) {
                case 'startsWith':
                    if (\yii\helpers\StringHelper::startsWith($route, $v)) return true;
                    break;
                case 'controller':
                    $arr = explode('/', $route);
                    if ($arr[0] == $v) return true;
                    break;
            }
        }
    }

    return false;
}

?>

<?php
$rows = [];
foreach($menu as $item) {
    $class = ['dropdown'];
    if (isSelected($item, Yii::$app->requestedRoute)) {
        $class[] = 'active';
    }
    if (\yii\helpers\ArrayHelper::keyExists('items', $item)) {
        $html = Html::tag(
            'li',
            Html::a(
                $item['label']
                .
                ' '
                .
                Html::tag('span', null, ['class' => 'caret'])
                ,
                '#'
                ,
                [
                    'class'         => "dropdown-toggle",
                    'data-toggle'   => "dropdown",
                    'role'          => "button",
                    'aria-expanded' => "false",
                ]
            )
            .
            Html::tag('ul',
                liMenu(\yii\helpers\ArrayHelper::map($item['items'], 'route', 'label'))
                , [
                    'class' => "dropdown-menu",
                    'role'  => "menu",
                ])
            ,
            ['class' => join(' ', $class)]);
    } else {
        $url = '/';
        if (isset($item['url'])) {
            $url = $item['url'];
        }
        if (isset($item['route'])) {
            $url = [$item['route']];
        }
        $html = Html::tag(
            'li',
            Html::a($item['label'], $url),
            ['class' => join(' ', $class)]
        );
    }

    $rows[] = $html;
}


?>
<?= join("\n", $rows) ?>
