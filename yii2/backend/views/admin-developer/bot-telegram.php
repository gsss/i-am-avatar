<?php

/** $this \yii\web\View  */

use yii\helpers\Url;
use app\services\GsssHtml;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use app\services\GetArticle\Collection;

$this->title = 'Телеграм Бот';



?>

<div class="container">
    <div class="col-lg-12">
        <h1 class="page-header"><?= Html::encode($this->title) ?></h1>

        <p><a href="https://github.com/akbarjoudi/yii2-bot-telegram" target="_blank">https://github.com/akbarjoudi/yii2-bot-telegram</a>
        </p>
        <p><a href="https://tlgrm.ru/docs/bots/api" target="_blank">https://tlgrm.ru/docs/bots/api</a></p>
        <p>Есть такая функция у телеграм API <code>getUpdates</code>, она возвращает активные чаты, из нее можно достать
            параметр <code>chat_id</code> который нужен для функции <code>sendMessage</code>.</p>
        <p>Чтобы получать уведомления о поступивших сообщениях надо в API указать свой WebHook куда он будет сообщать
            что клиент написал. Это делается при помощи функции <code>setWebhook</code>.</p>
        <p>В приложении iAvatar уже подключен этот компонент и в переменной <code>Yii::$app</code> его можно найти под
            именем <code>telegram</code>.</p>
        <p style="font-style: italic;">Пример использования:</p>
        <pre>/** @var \aki\telegram\Telegram $telegram */
$telegram = Yii::$app->telegram;
$telegram->sendMessage(['chat_id' => 122605414, 'text' => 'Привет Я твой Звездный Аватар! Я покажу тебе Путь! Ты готов к Игре?']);</pre>
        <p>Наш бот называется <code>iAvatar_bot</code>. Найти его соответственно можно в телегремме по сокращению <code>@iAvatar_bot</code>.
        </p>
        <p>Клиент в телеграмме идентифицируется по полю <code>username</code> это поле записывается в поле таблицы
            <code>user.telegram</code>.</p>


        <h2 class="page-header">Список функций</h2>
        <pre>getMe
sendMessage
forwardMessage
sendPhoto
sendAudio
sendDocument
sendSticker
sendVideo
sendLocation
sendChatAction
getUserProfilePhotos
getUpdates
setWebhook
getChat
getChatAdministrators
getChatMembersCount
getChatMember
answerCallbackQuery
editMessageText
editMessageCaption
sendGame
Game
Animation
CallbackGame
getGameHighScores
GameHighScore
answerInlineQuery
setChatStickerSet
deleteChatStickerSet
leaveChat
pinChatMessage
unpinChatMessage
setChatDescription
setChatTitle
deleteChatPhoto
exportChatInviteLink
promoteChatMember
restrictChatMember</pre>
        <h2 class="page-header">getUpdates</h2>
        <p>Возвращает:</p>
        <pre>stdClass#1
(
    [ok] => true
    [result] => [
        0 => stdClass#2
        (
            [update_id] => 327254148
            [message] => stdClass#3
            (
                [message_id] => 2
                [from] => stdClass#4
                (
                    [id] => 122605414
                    [is_bot] => false
                    [first_name] => '⭐️Святослав⭐️'
                    [last_name] => 'Архангельский'
                    [username] => 'ArhangelSky999'
                    [language_code] => 'ru'
                )
                [chat] => stdClass#5
                (
                    [id] => 122605414
                    [first_name] => '⭐️Святослав⭐️'
                    [last_name] => 'Архангельский'
                    [username] => 'ArhangelSky999'
                    [type] => 'private'
                )
                [date] => 1546228624
                [text] => '/start'
                [entities] => [
                    0 => stdClass#6
                    (
                        [offset] => 0
                        [length] => 6
                        [type] => 'bot_command'
                    )
                ]
            )
        )
    ]
)</pre>
        <h2 class="page-header">callback</h2>
        <p>Установка производится через функцию <code>setWebhook</code>.</p>
        <?php
        $rows = [
            'https://www.i-am-avatar.com/telegram/call-back',
            'https://www.i-am-avatar.com/telegram/get-webhook-info',
            'https://www.i-am-avatar.com/telegram/set-call-back',
            'https://www.i-am-avatar.com/telegram/remove-call-back',
        ];
        ?>
        <?php foreach ($rows as $a) { ?>
            <p><a href="<?= $a ?>" target="_blank"><?= $a ?></a></p>
        <?php } ?>

        <h2 class="page-header">callback</h2>
        <p>Телеграм выдает такие сообщения в теле rawBody.</p>
        <pre>Json::decode(Yii::$app->request->rawBody)</pre>
        <pre>[
    'update_id' => 327254191
    'message' => [
        'message_id' => 54
        'from' => [
            'id' => 122605414
            'is_bot' => false
            'first_name' => '⭐Святослав⭐'
            'last_name' => 'Архангельский'
            'username' => 'ArhangelSky999'
            'language_code' => 'ru'
        ]
        'chat' => [
            'id' => 122605414
            'first_name' => '⭐Святослав⭐'
            'last_name' => 'Архангельский'
            'username' => 'ArhangelSky999'
            'type' => 'private'
        ]
        'date' => 1549558279
        'text' => 'Тест'
    ]
]</pre>

        <p>
            <a href="https://www.draw.io/#G1HaLjDRZDjLehjnaUb3_-oySRqVm-eL18" target="_blank">
                <img src="/images/controller/admin-developer/bot-telegram/bot.png">
            </a>
        </p>

        <p>Функция в контроллере на который приходят CallBack от Telegram: <code>\avatar\controllers\TelegramController::actionCallBack()</code>
        </p>

        <p>Для гостя алгоритм</p>
        <p>В ввиду того что надо где то сохранить временный чат с пользователем и его статут то сделаю это в отдельной
            таблице. Например <code>user_telegram_temp</code></p>
        <?= \common\services\documentation\DbTable::widget([
            'name'        => 'user_telegram_temp',
            'description' => 'Таблица для хранения логинов и чатов гостей',
            'model'       => '\common\models\UserTelegramTemp',
            'columns'     => [
                [
                    'name'        => 'id',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Идентификатор строки',
                ],
                [
                    'name'        => 'username',
                    'type'        => 'varchar(60)',
                    'isRequired'  => false,
                    'description' => 'username',
                ],
                [
                    'name'        => 'chat_id',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Хеш пароля',
                ],
                [
                    'name'        => 'status',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Статус общения по <code>https://www.draw.io/#G1HaLjDRZDjLehjnaUb3_-oySRqVm-eL18</code>',
                ],
                [
                    'name'        => 'last_message_time',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Время последнего сообщения от пользователя',
                ],
                [
                    'name'        => 'email',
                    'type'        => 'varchar(100)',
                    'isRequired'  => true,
                    'description' => 'Почта',
                ],
            ],
        ]) ?>

        <p>Ввожу поле статус (<code>status</code>) для каждого пользователя чтобы понимать на какой стадии находится
            общение. То есть если нет гостя в таблице значит общение только начато, если есть то я понимаю на чем бот
            закончил и что отвечать боту.</p>
        <p>Непонятно если телеграм отдает username и chat_id то значит chat_id может меняться?</p>
        <p>Да еще было бы неплохо сделать помощь чтобы бот подсказывал какие команды есть. Например "помощь" - выводит список команд, "выход" переводит бота в начальную позицию в статусах.</p>

        <p>
            <a href="https://www.draw.io/#G1HaLjDRZDjLehjnaUb3_-oySRqVm-eL18" target="_blank">
                <img src="/images/controller/admin-developer/bot-telegram/bot2.png" >
            </a>
        </p>
        <p>На 3-5 статусе нужно запомнить соответствие email-login-chat_id. Вопрос как? Можно в БД. Какая таблица? Нет добавляю поле в <code>user_telegram_temp.email</code>.</p>
        <p>Все такие лучше отдельную таблицу, во первых чтобы не мешать, а во вторых чтобы защита надежнее.</p>
        <?= \common\services\documentation\DbTable::widget([
            'name'        => 'user_telegram_connect',
            'description' => 'Таблица для хранения данных для присоединения пользователя',
            'model'       => '\common\models\UserTelegramConnect',
            'columns'     => [
                [
                    'name'        => 'id',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Идентификатор строки',
                ],
                [
                    'name'        => 'username',
                    'type'        => 'varchar(60)',
                    'isRequired'  => false,
                    'description' => 'username Telegram',
                ],
                [
                    'name'        => 'hash',
                    'type'        => 'varchar(32)',
                    'isRequired'  => true,
                    'description' => 'Случайная строка, передается в почте',
                ],
                [
                    'name'        => 'email',
                    'type'        => 'varchar(100)',
                    'isRequired'  => false,
                    'description' => 'email, используется для регистрации',
                ],
                [
                    'name'        => 'chat_id',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Идентификатор чата',
                ],
                [
                    'name'        => 'user_id',
                    'type'        => 'int',
                    'isRequired'  => false,
                    'description' => 'Идентификатор пользователя, (если нет значит это регистрация полная)',
                ],
                [
                    'name'        => 'created_at',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Момент записи',
                ],
            ],
        ]) ?>
        <p><code>/auth/confirm-telegram</code> - активация по почте.</p>
        <p>Кнопочка "присоединить" в кабинете появится только когда пользователь в телеграмме укажет свой email.</p>

        <h2 class="page-header">Задача</h2>
        <p>https://www.i-am-avatar.com/cabinet-school-task-list/view?id=165 подключения Telegram бота к профилю</p>
        <h2 class="page-header">Ошибки</h2>
        <p>Если в процессе обработки запроса от телеграмма возникнет ошибка то телеграм опять пошел этот же запрос, видимо думает что запрос не отработан</p>
        <p>А сервер должен отправить <code>'ok'</code></p>

        <h2 class="page-header">Ситуация</h2>
        <p>Оказалось что в параметрах которые передает telegram может и не быть username.</p>

        <h2 class="page-header">Установка сертификата</h2>
        <p><code>/telegram/set-call-back</code> Установка сертификата и call-back</p>
        <p><code>/telegram/get-webhook-info</code> Проверка статуса сертификата</p>
        <p><code>/telegram/remove-call-back</code> Отключение сертификата</p>

        <h2 class="page-header">Работа с кошельком</h2>
        <p>Исследование</p>
        <p>Для начала чтобы отправить монету, надо выбрать кошелек. Значит например первая команда <code>/wallets</code> и бот ответит какие есть кошельки, и далее "С каким кошельком ты хочешь поработать?". Далее бот ожидает команды внутри этой ветки общения.</p>
        <p>Далее человек выбрал кошелек, бот ему предлагает 1. Баланс, 2. Перевести, 3. Принять, 4. Настройки, 5. Удалить.</p>
        <p>Но у бота нет статуса состояния в какой позиции находится бот. Поэтому нужно это как то решить. Причем нужно статус с доп информацией, потому что своя череда выборов зависит еще и от других данных, такие как например выбранный кошелек.</p>
        <p>Попробую пердставить как может выглядеть статус</p>
        <pre>[{"step": 1, "command":"/wallets"}, {"step":2, "command": "2"}, {"step":3,"command": "Перевести"}, {"step":4,"command": "500"}, {"step":5,"command": "god@galaxysss.ru"}]</pre>
        <p>Так достаточно адекватно на мой взгляд, надо проверить реализацию.</p>
    </div>
</div>

