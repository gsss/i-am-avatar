<?php
/**
 * Created by PhpStorm.
 * User: Ra-m-ha
 * Date: 12.05.2019
 * Time: 12:36
 */

?>
<?= \common\services\documentation\DbTable::widget([
    'name'    => 'blog',
    'columns' => [
        [
            'name'        => 'id',
            'type'        => 'int',
            'isRequired'  => true,
            'description' => 'Идентификатор школы',
        ],
        [
            'name'        => 'name',
            'type'        => 'varchar(100)',
            'isRequired'  => true,
            'description' => 'Название страницы',
        ],
        [
            'name'        => 'image',
            'type'        => 'varchar(100)',
            'isRequired'  => true,
            'description' => 'Картинка',
        ],
        [
            'name'        => 'description',
            'type'        => 'varchar(2000)',
            'isRequired'  => true,
            'description' => 'Описание',
        ],
        [
            'name'        => 'content',
            'type'        => 'longtext',
            'isRequired'  => true,
            'description' => 'HTML',
        ],
        [
            'name'        => 'created_at',
            'type'        => 'int',
            'isRequired'  => true,
            'description' => 'Момент создания',
        ],
        [
            'name'        => 'views_counter',
            'type'        => 'int',
            'isRequired'  => true,
            'description' => 'Кол-во просмотров',
        ],
        [
            'name'        => 'is_send',
            'type'        => 'int',
            'isRequired'  => true,
            'description' => 'Флаг, отправлена рассылка?',
        ],
        [
            'name'        => 'link',
            'type'        => 'varchar(255)',
            'isRequired'  => true,
            'description' => 'Ссылка на источник',
        ],
        [
            'name'        => 'comments_count',
            'type'        => 'int',
            'isRequired'  => true,
            'description' => 'Кол-во комментариев, по умолчанию 0',
        ],
    ],
]) ?>