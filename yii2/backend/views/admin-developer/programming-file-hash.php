<?php

/** $this \yii\web\View  */

use yii\helpers\Url;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;

$this->title = 'Hash от большого файла';


?>

<div class="container">
    <div class="col-lg-12">
        <h1 class="page-header"><?= Html::encode($this->title) ?></h1>

        <pre>&lt;?php
$fileName = isset($argv[1]) ? $argv[1] : die("Usage : php sha256.php &lt;&lt;filename&gt;&gt; &lt;&lt;hash&gt;&gt;" . PHP_EOL);
if(!file_exists($fileName))
    die("FATAL : The file specified does not exists!");

$hash = isset($argv[2]) ? $argv[2] : die("Usage : php sha256.php &lt;&lt;filename&gt;&gt; &lt;&lt;hash&gt;&gt;" . PHP_EOL);

$ctx = hash_init('sha256');

echo("INFO : Reading file $fileName ...");
$file = fopen($fileName, 'r');
while(!feof($file)){
    $buffer = fgets($file, 1024);
    hash_update($ctx, $buffer);
}
echo(" DONE!" . PHP_EOL);

echo("INFO : Calculating SHA256 hash of $fileName ...");
$hashRaw = hash_final($ctx, false); // Here, set the $raw_output to false
echo(" DONE!" . PHP_EOL);

echo ($hash . " INPUT " . PHP_EOL);
echo ($hashRaw . " OUTPUT " . PHP_EOL);

if($hash == $hashRaw)
    echo("INFO : Hash Comparison: OK!" . PHP_EOL);
else
    echo("WARN : Hash Comparison: MISMATCH!" . PHP_EOL);
echo("END" . PHP_EOL);</pre>
    </div>
</div>

