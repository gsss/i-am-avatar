<?php

/** $this \yii\web\View  */

use yii\helpers\Url;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;

$this->title = 'Домашнее задание к уроку';


?>

<div class="container">
    <div class="col-lg-12">
        <h1 class="page-header"><?= Html::encode($this->title) ?></h1>
        <h3 class="page-header">Домашнее задание к уроку</h3>

        <p>Урок может содержать домашнее задание.
            В нем есть флаг is_stop. Если он выставлен то значит ученик не сможет перети к следующим урокам не выполнив
            домашнего задания.
        </p>

        <?= \common\services\documentation\DbTable::widget([
            'name'        => 'school_urok_dz_request',
            'description' => 'Домашнее задание к уроку',
            'model'       => '\common\models\school\LessonHomeWork',
            'columns'     => [
                [
                    'name'        => 'id',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Идентификатор домашнего задания',
                ],
                [
                    'name'        => 'lesson_id',
                    'type'        => 'integer',
                    'isRequired'  => true,
                    'description' => 'Идентификатор урока',
                ],
                [
                    'name'        => 'content',
                    'type'        => 'text',
                    'isRequired'  => true,
                    'description' => 'Содержание домашнего задания',
                ],
            ],
        ]) ?>

        <p>Домашнее задание считается выполненным если ученик получает оценку более чем 2.</p>
        <p>Если ученик не выполняет домашнего задания и урок является стоповым, то ученик делает ДЗ до тех пор пока не
            выполнит его.</p>

        <p><a href="https://www.i-am-avatar.com/cabinet-school-task-list/view?id=1431">Задача</a></p>
        <pre>Коментарий к уроку и чат для домашнего задания разные?
или для домашнего задания не может быть чата а только окно для приема задания?
думаю только окно для приема ответа с файлом
хорошо
а если он не сделает задание то будет ли ему еще один шанс сделать задание и каким статусом обозначать эту ситуацию что мы в таком то статусе?
статус домашного задания нужен
нужна диаграма состояний домашеного задания.</pre>
        <p>
            <a href="https://www.draw.io/#G18afpFy3xJ005yJL4WTTdX4GlmxdKx2IG" target="_blank">
                <img src="/images/controller/admin-developer/school-uroki/urok_diag.png">
            </a>
        </p>

        <h3 class="page-header">Ответ ученика на домашнее задание</h3>
        <p>Если ученик несколько раз может присылать ответ а учитель проверять, значит нужен лог этого взаимодействия.
            Нет последовательно будет все определяться и + статус урока. Статус будет храниться в отдельной таблице,
            чтобы на каждый поток отдельно формировался.</p>
        <?= \common\services\documentation\DbTable::widget([
            'name'        => 'school_urok_dz_response',
            'description' => 'Домашнее задание к уроку',
            'model'       => '\common\models\school\LessonResponse',
            'columns'     => [
                [
                    'name'        => 'id',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Идентификатор',
                ],
                [
                    'name'        => 'lesson_state_id',
                    'type'        => 'integer',
                    'isRequired'  => true,
                    'description' => 'Идентификатор состояния урока',
                ],
                [
                    'name'        => 'user_id',
                    'type'        => 'integer',
                    'isRequired'  => true,
                    'description' => 'Идентификатор пользователя',
                ],
                [
                    'name'        => 'content',
                    'type'        => 'text',
                    'isRequired'  => true,
                    'description' => 'Содержание ответа',
                ],
                [
                    'name'        => 'created_at',
                    'type'        => 'int',
                    'isRequired'  => false,
                    'description' => 'Момент создания действия',
                ],
            ],
        ]) ?>
        <?= \common\services\documentation\DbTable::widget([
            'name'        => 'school_urok_potok_state',
            'description' => 'Состояние урока для определенного потока',
            'model'       => '\common\models\school\LessonPotokState',
            'columns'     => [
                [
                    'name'        => 'id',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Идентификатор',
                ],
                [
                    'name'        => 'lesson_id',
                    'type'        => 'integer',
                    'isRequired'  => true,
                    'description' => 'Идентификатор урока',
                ],
                [
                    'name'        => 'potok_id',
                    'type'        => 'integer',
                    'isRequired'  => true,
                    'description' => 'Идентификатор потока',
                ],
                [
                    'name'        => 'status',
                    'type'        => 'integer',
                    'isRequired'  => true,
                    'description' => 'Статус урока',
                ],
                [
                    'name'        => 'is_hide',
                    'type'        => 'int',
                    'isRequired'  => false,
                    'description' => 'Флаг показа. 0 - показывать, 1 - скрыть, не показывать.',
                ],
            ],
        ]) ?>

        <p>В итоге ученик отправляет ДЗ а учитель проверяет. А нужна тогда лента? Может лучше две таблицы "дз" и
            "оценка" ? да. Значит так и сделаю</p>

        <h3 class="page-header">Ответ учителя на домашнее задание</h3>

        <?= \common\services\documentation\DbTable::widget([
            'name'        => 'school_urok_dz_rate',
            'description' => 'Состояние урока для определенного потока',
            'model'       => '\common\models\school\LessonRate',
            'columns'     => [
                [
                    'name'        => 'id',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Идентификатор ответа',
                ],
                [
                    'name'        => 'lesson_state_id',
                    'type'        => 'integer',
                    'isRequired'  => true,
                    'description' => 'Идентификатор ДЗ',
                ],
                [
                    'name'        => 'user_id',
                    'type'        => 'integer',
                    'isRequired'  => true,
                    'description' => 'Пользователь учитель',
                ],
                [
                    'name'        => 'ocenka',
                    'type'        => 'integer',
                    'isRequired'  => true,
                    'description' => 'Оценка за домашнее задание',
                ],
                [
                    'name'        => 'created_at',
                    'type'        => 'int',
                    'isRequired'  => false,
                    'description' => 'Момент создания действия',
                ],
            ],
        ]) ?>


        <h3 class="page-header">Схема таблиц для ДЗ</h3>
        <p>
            <a href="https://www.draw.io/#G1cVuhiCc3AlpKpJvbgr9mE2UuwVTWHJgJ" target="_blank">
                <img src="/images/controller/admin-developer/school-dz/db.png">
            </a>
        </p>
        <h3 class="page-header">Схема отображения уроков в потоке</h3>
        <p>Например если все уроки показываются то не нужно даже в состояние ничего заносить, хотя нет, нужно же статус заносить. Значит после создания потока создается и карта состояний. И после создания урока в каждом потоке добавляется по пункту состояния для каждого потока.</p>

        <p>
            <a href="https://www.draw.io/#G1iIspfUX-67Giwg2iMr7rYvrSJCSjCRZ_" target="_blank">
                <img src="/images/controller/admin-developer/school-dz/scheme_show.png">
            </a>
        </p>

        <h3 class="page-header">Домашнее задание в интерфейсе</h3>
        <p>Здесь можно добавить табуляцию "Комментарии" и "домашнее задание"</p>
        <p>
            <img src="/images/controller/admin-developer/school-dz/ui1.png" width="500" class="thumbnail">
        </p>
        <p>Если у урока есть ДЗ то табуляция отображается, если нет то не отображается.</p>
        <pre>Да заморочка с этими ДЗ
не зря я их откладывал
там же в отображении показывается ID домашеного задания а надо ID состояния ДЗ которое привязано к потоку
а чтобы это сделать надо сначала для всех имеющихся сделать состояния, + сделать чтобы при создании урока тоже создавалось состояние для каждого потока
и после этого можно менять страницу урока</pre>

        <h3 class="page-header">Добавление состояние после добавления урока</h3>
        <p>/cabinet-school-lesson/add?id=6</p>
        <p>Сделал создание статусов при добавлении урока и удаление при удалении урока</p>
        <p>Если у урока нет домашнего задания то вообщемто незачем создаваь состояние, но лучше сделать.</p>


    </div>
</div>