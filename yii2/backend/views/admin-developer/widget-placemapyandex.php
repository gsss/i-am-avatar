<?php

/** $this \yii\web\View  */

use yii\helpers\Url;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;

$this->title = '\common\widgets\PlaceMapYandex\PlaceMap';


?>

<div class="container">
    <div class="col-lg-12">
        <h1 class="page-header"><?= Html::encode($this->title) ?></h1>

        <p>Указание точки на карте</p>
        <p>API Геокодера <a href="https://tech.yandex.ru/maps/geocoder/doc/desc/concepts/input_params-docpage/" target="_blank">https://tech.yandex.ru/maps/geocoder/doc/desc/concepts/input_params-docpage/</a></p>



    </div>
</div>

