<?php

/** $this \yii\web\View  */

use yii\helpers\Url;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;

$this->title = 'Активация карты ЯАватар';


?>

<div class="container">
    <div class="col-lg-12">
        <h1 class="page-header"><?= Html::encode($this->title) ?></h1>

        <!-- Nav tabs -->
        <ul class="nav nav-tabs" role="tablist">
            <li role="presentation"><a href="#clear" aria-controls="clear" role="tab" data-toggle="tab">Чистовик</a></li>
            <li role="presentation" class="active"><a href="#potok" aria-controls="potok" role="tab" data-toggle="tab">Поток</a></li>
        </ul>

        <!-- Tab panes -->
        <div class="tab-content">
            <div role="tabpanel" class="tab-pane fade" id="clear">
            </div>
            <div role="tabpanel" class="tab-pane in active" id="potok">

                <h4 class="page-header">Исследование</h4>
                <p>Активация карты проходит на странице <a href="https://www.i-am-avatar.com/qr/enter" target="_blank">https://www.i-am-avatar.com/qr/enter</a></p>
                <p><img src="/images/controller/admin-developer/school-user-models-card-activate/2019-03-27_02-28-43.png"></p>
                <p>Путь регистрации похож на регистрацию в Аватаре, но после активации мне надо сделать присоединение карты и если я это сделаю на другом пути, то может возникнуть ситуация что человеку не придет письмо и ему надо будет восстановить пароль, а где это делать? Если посылать его на другие страницы то много путаницы, лучше конечно если это будет единая страница для восстановления пароля, это может быть, но сложнее уже. Вот здесь и надо принять решение.</p>
                <p>Как лучше?</p>
                <p>1. На единый путь восстановления.</p>
                <p>2. На отдельный путь восстановления и тогда активация по ссылке в одну точку будет вести.</p>
                <p>Лучше мне кажется второй. Если пользователь растерялся то пользователь на этой же странице вводит данные и ему опять высылаются данные.</p>


                <p>/auth/registration-activate-card</p>
                <p>hash = ################ - Контрольная сумма которая будет находиться в таблице <code>user_registration_card</code></p>
                <?= \common\services\documentation\DbTable::widget([
                    'name'        => 'user_registration_card',
                    'description' => 'Дополнительная информация при активации карты',
                    'model'       => '\common\models\UserRegistrationCard',
                    'columns'     => [
                        [
                            'name'        => 'id',
                            'type'        => 'int',
                            'isRequired'  => true,
                            'description' => 'Идентификатор элемента',
                        ],
                        [
                            'name'        => 'created_at',
                            'type'        => 'int',
                            'isRequired'  => true,
                            'description' => 'Момент создания записи',
                        ],
                        [
                            'name'        => 'user_root_id',
                            'type'        => 'int',
                            'isRequired'  => true,
                            'description' => 'Идентификатор пользователя ROOT, на которого делается активация',
                        ],
                        [
                            'name'        => 'hash',
                            'type'        => 'varchar(32)',
                            'isRequired'  => true,
                            'description' => 'Случайный хеш который передается в письме',
                        ],
                        [
                            'name'        => 'card',
                            'type'        => 'varchar(16)',
                            'isRequired'  => true,
                            'description' => 'Номер активируемой карты',
                        ],
                        [
                            'name'        => 'password_hash',
                            'type'        => 'varchar(100)',
                            'isRequired'  => true,
                            'description' => 'Хеш пароля для пользователя, который создавался при регистрации',
                        ],
                    ],
                ]) ?>

                <p>
                    <a href="https://www.draw.io/#G1xVwXXhlxEM1PrIu2gphkYrCq05qwjanu" target="_blank">
                        <img src="/images/controller/admin-developer/school-user-models-card-activate/schema.png" width="100%" class="thumbnail">
                    </a>
                </p>

                <p><img src="/images/controller/admin-developer/school-user-models-card-activate/2019-03-28_20-08-48.png"></p>

                <p>Задача: <a href="https://www.i-am-avatar.com/cabinet-task-list/view?id=275" target="_blank">https://www.i-am-avatar.com/cabinet-task-list/view?id=275</a></p>



            </div>
        </div>


    </div>
</div>

