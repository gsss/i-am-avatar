<?php

/** $this \yii\web\View  */

use yii\helpers\Url;
use app\services\GsssHtml;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use app\services\GetArticle\Collection;

$this->title = 'Шина пользователей. AvatarNetwork';


?>

<div class="container">
    <div class="col-lg-12">
        <h1 class="page-header"><?= Html::encode($this->title) ?></h1>

        <p>Это сервис который обеспечивает сохранение персональных данных и упраление ими, авторизацию, идентификацию.</p>
        <p>Имеется свое API.</p>
        <p>Этим сервисом может пользоваться любой сервис.</p>
        <p>Логотип сервиса AvatarID</p>

        <h2 class="page-header">Пароли</h2>
        <p>Рабочий пароль.</p>
        <p>Мастер пароль - тот при помощи которого можно сменить Рабочий пароль.</p>

        <h2 class="page-header">Таблицы</h2>
        <p>login</p>
        <p>user</p>
        <p>master_password</p>

        <h2 class="page-header">Проблематика</h2>
        <p>Основной проблематикой в идентификации является то что пользователи создают несколько одинаковых аккаунтов. Что затрудняет их учет. Поэтому нужен отдельный сервис который автоматически объединяет повторяющиеся аккаунты.</p>
        <p>Этот сервис может называться UniqueAvatarID - уникальный Аватар ID.</p>
        <p>На данный момент уникальным идентификаторм является биометрический паспорт.</p>


        <h3 class="page-header">API</h3>
        <p>Адрес для доступа: <code>https://id.avatar-network.org</code></p>
        <p>Все ответы выдаются в формете JSON</p>
        <p>Стандартный положительный ответ: <code>{success:true}</code></p>
        <p>Стандартный положительный ответ с данными: <code>{success:true, data: ...}</code></p>

        <h2 class="page-header">Авторизация</h2>
        <p>Для доступа к API испольуется key. Он указывается в заголовке REQUEST под названием <code>X-API-KEY</code></p>
        <p>В качестве пароля используется подпись. Подпись как результат функции <code>hash('sha256', ($secret . ((int)(time()/100))))</code>. Таким образом пароль не передается в открытом виде, текущая подпись действует только 100 секунд. <code>X-API-SECRET</code></p>

        <h3 class="page-header">Ошибки</h3>
        <p>Если не передан параметр <code>X-API-KEY</code>, то возвращается ошибка 403. и текст</p>
        <pre>{"name":"Forbidden","message":"Нет параметра X-API-KEY","code":1,"status":403,"type":"yii\\web\\ForbiddenHttpException"}</pre>
        <p>Если не передан параметр <code>X-API-SECRET</code>, то возвращается ошибка 403. и текст</p>
        <pre>{"name":"Forbidden","message":"Нет параметра X-API-SECRET","code":2,"status":403,"type":"yii\\web\\ForbiddenHttpException"}</pre>
        <p>Если не найден параметр <code>X-API-KEY</code>, то возвращается ошибка 403. и текст</p>
        <pre>{"name":"Forbidden","message":"Не найден логин","code":3,"status":403,"type":"yii\\web\\ForbiddenHttpException"}</pre>

        <p>Если не верный <code>X-API-SECRET</code>, то возвращается ошибка 403. и текст</p>
        <pre>{"name":"Forbidden","message":"Не верный secret","code":4,"status":403,"type":"yii\\web\\ForbiddenHttpException"}</pre>

        <p>Если превышено кол-во запросов, то возвращается ошибка 429</p>

        <p>Если запрашивается метод которого не существует, то выдается ошибка 404</p>

    </div>
</div>

