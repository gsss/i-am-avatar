<?php

/** $this \yii\web\View  */

use yii\helpers\Url;
use app\services\GsssHtml;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use app\services\GetArticle\Collection;
use BitcoinPHP\BitcoinECDSA\BitcoinECDSA;
use iAvatar777\services\EllipticCurve\PrivateKey;
use iAvatar777\services\EllipticCurve\Ecdsa;


$this->title = 'Цифровые подписи';



$bitcoinECDSA = new BitcoinECDSA();
$bitcoinECDSA->generateRandomPrivateKey(); //generate new random private key
$bitcoinECDSA->checkSignatureForMessage();

$message = "Test messageTest messageTest messageTest messageTest messageTest messageTest messageTest messageTest messageTest messageTest messageTest messageTest messageTest messageTest messageTest messageTest messageTest messageTest messageTest messageTest messageTest messageTest messageTest messageTest messageTest messageTest messageTest message";
$signedMessage = $bitcoinECDSA->signMessage($message);



/**
 * Will print something like this:
-----BEGIN BITCOIN SIGNED MESSAGE-----
Test message
-----BEGIN SIGNATURE-----
1L56ndSQ1LfrAB2xyo3ZN7egiW4nSs8KWS
HxTqM+b3xj2Qkjhhl+EoUpYsDUz+uTdz6RCY7Z4mV62yOXJ3XCAfkiHV+HGzox7Ba/OC6bC0y6zBX0GhB7UdEM0=
-----END BITCOIN SIGNED MESSAGE-----
 */


// If you only want the signature you can do this
$signature = $bitcoinECDSA->signMessage($message, true);




$privateKey = new PrivateKey;
$publicKey = $privateKey->publicKey();

$message = "My test message";

# Generate Signature
$signature1 = Ecdsa::sign($message, $privateKey);
?>

<div class="container">
    <div class="col-lg-12">
        <h1 class="page-header"><?= Html::encode($this->title) ?></h1>

        <p><a href="https://github.com/BitcoinPHP/BitcoinECDSA.php" target="_blank">BitcoinPHP/BitcoinECDSA.php</a></p>
        <p>getAddress = <code><?= $bitcoinECDSA->getAddress() ?></code></p>
        <p>getPrivateKey = <code><?= $bitcoinECDSA->getPrivateKey() ?></code></p>
        <p>getUncompressedAddress = <code><?= $bitcoinECDSA->getUncompressedAddress() ?></code></p>
        <p>getUncompressedPubKey = <code><?= $bitcoinECDSA->getUncompressedPubKey() ?></code></p>
        <p>$signedMessage = </p>
        <pre><?= $signedMessage ?></pre>
        <p>$signature = <code><?= $signature ?></code></p>
        <p>$wif = <code><?=  $bitcoinECDSA->getWif() ?></code></p>
        <p><a href="https://github.com/i-avatar777/service-ecdsa" target="_blank">i-avatar777/service-ecdsa</a></p>
        <p>$publicKey = </p>
        <pre><?=  \yii\helpers\VarDumper::dumpAsString($publicKey) ?></pre>
        <p>$privateKey->toString() = <code><?=  $privateKey->toString() ?></code></p>
        <p>$privateKey->toPem() = </p>
        <pre><?=  $privateKey->toPem() ?></pre>
        <p>$signature1 = <code><?=  $signature1->toBase64() ?></code></p>

        <h2 class="page-header">Хранение ключа</h2>
        <p>Аватар самостоятельно выбирает режим хранения ключа, либо у нас либо у себя. Настройка отвечающая за этот параметр располагается в настройкаъ цифровой подписи. Таблица для хранения данных о цифровой подписи <code>user_digital_sign</code></p>
        <?= \common\services\documentation\DbTable::widget([
            'name'        => 'user_digital_sign',
            'model'       => '\common\models\UserDigitalSign',
            'description' => 'Цифровая подпись пользователя',
            'columns'     => [
                [
                    'name'        => 'id',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Идентификатор строки',
                ],
                [
                    'name'        => 'user_id',
                    'type'        => 'integer',
                    'isRequired'  => true,
                    'description' => 'Хозяин подписи',
                ],
                [
                    'name'        => 'created_at',
                    'type'        => 'integer',
                    'isRequired'  => true,
                    'description' => 'Момент создания',
                ],
                [
                    'name'        => 'address',
                    'type'        => 'string',
                    'isRequired'  => true,
                    'description' => 'Адрес',
                ],
                [
                    'name'        => 'private_key',
                    'type'        => 'string',
                    'isRequired'  => false,
                    'description' => 'Дополнительные данные к файлу',
                ],
                [
                    'name'        => 'address_uncompressed',
                    'type'        => 'string',
                    'isRequired'  => true,
                    'description' => 'Адрес',
                ],
                [
                    'name'        => 'public_key_uncompressed',
                    'type'        => 'string',
                    'isRequired'  => true,
                    'description' => 'Публичный адрес',
                ],
                [
                    'name'        => 'type_id',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Тип хранения подписи: 0 - пользователь хранит у себя, 1 - ключ хранится в системе. По умолчанию 0',
                ],
            ],
        ]) ?>

        <p><code>user_digital_sign_list</code> предназначена для хранения истории всех цифровых подписей, чтобы можно определить кто когда что подписывал</p>
        <?= \common\services\documentation\DbTable::widget([
            'name'        => 'user_digital_sign_list',
            'model'       => '\common\models\UserDigitalSignList',
            'description' => 'История создания цифровых подписей',
            'columns'     => [
                [
                    'name'        => 'id',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Идентификатор строки',
                ],
                [
                    'name'        => 'user_id',
                    'type'        => 'integer',
                    'isRequired'  => true,
                    'description' => 'Хозяин подписи',
                ],
                [
                    'name'        => 'created_at',
                    'type'        => 'integer',
                    'isRequired'  => true,
                    'description' => 'Момент создания',
                ],
                [
                    'name'        => 'address',
                    'type'        => 'string',
                    'isRequired'  => true,
                    'description' => 'Адрес',
                ],
            ],
        ]) ?>

        <h2 class="page-header">TON</h2>
        <p>https://github.com/tonlabs/ton-client-js/blob/049f3108861c8d65b9eaa7366854c2ca253685dd/__tests__/crypto.js#L144</p>

        <h2 class="page-header">Пользовательский интерфейс</h2>
        <p>Страница на которой можно получить цифровую подпись: <code>/cabinet-digital-sign/index</code></p>
        <p>
            <a href="https://www.draw.io/#G1z_6Rvm9wX1laTpw-mO50lkOOJP4Pa41o" target="_blank">
                <img src="/images/controller/admin-developer/digital-sign/shceme.png">
            </a>
        </p>

    </div>
</div>


