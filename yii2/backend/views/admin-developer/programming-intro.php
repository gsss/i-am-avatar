<?php

/** $this \yii\web\View  */

use yii\helpers\Url;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;

$this->title = 'Введение';


?>

<div class="container">
    <div class="col-lg-12">
        <h1 class="page-header"><?= Html::encode($this->title) ?></h1>

        <p><a href="https://habr.com/ru/company/mobileup/blog/335382/" target="_blank">Заблуждения Clean Architecture</a></p>

        <h2 class="page-header">Терминолоия</h2>
        <p>Сненд - копия платформы для рабочего использования PROD, для тестирования TEST и для разработки DEV.</p>
        <p>Приложение, точка входа - точка куда настроен веб сервер</p>
        <p>Платформа - вся совокупность системы iAvatar и все сервисы.</p>

        <h2 class="page-header">Приложения</h2>
        <?php
        $rows = [
            [
                'webfolder'   => 'api/web',
                'folder'      => 'api',
                'alias'       => 'api',
                'namespace'   => 'api',
                'description' => 'api ЯАватар',
            ],
            [
                'webfolder'   => 'api-processing/web',
                'folder'      => 'api-processing',
                'alias'       => 'apiprocessing',
                'namespace'   => 'api-processing',
                'description' => 'api процессинга ЯАватар',
            ],
            [
                'webfolder'   => 'public_html',
                'folder'      => 'avatar-bank',
                'alias'       => 'avatar',
                'namespace'   => 'avatar',
                'description' => 'ЯАватар',
            ],
            [
                'webfolder'   => './',
                'folder'      => 'console',
                'alias'       => 'console',
                'namespace'   => 'console',
                'description' => 'консольные команды ЯАватар',
            ],
            [
                'webfolder'   => 'school/web',
                'folder'      => 'school',
                'alias'       => 'school',
                'namespace'   => 'school',
                'description' => 'Школы ЯАватар',
            ],
        ];
        ?>
        <?= \yii\grid\GridView::widget([
            'dataProvider' => new \yii\data\ArrayDataProvider([
                'allModels' => $rows,
            ]),
            'tableOptions' => [
                'class' => 'table table-striped table-hover',
            ],
        ]) ?>
        <h2 class="page-header">Базы Данных</h2>
        <?php
        $rows = [
            [
                'alias'       => 'db',
                'name_prod'        => 'i_am_avatar_prod_main',
            ],
            [
                'alias'       => 'dbStatistic',
                'name_prod'        => 'i_am_avatar_prod_stat',
            ],
            [
                'alias'       => 'dbWallet',
                'name_prod'        => 'i_am_avatar_prod_wallet',
            ],
            [
                'alias'       => 'dbInfo',
                'name_prod'        => 'information_schema',
            ],

        ];
        ?>
        <?= \yii\grid\GridView::widget([
            'dataProvider' => new \yii\data\ArrayDataProvider([
                'allModels' => $rows,
            ]),
            'tableOptions' => [
                'class' => 'table table-striped table-hover',
            ],
        ]) ?>


    </div>
</div>

