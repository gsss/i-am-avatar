<?php

/** $this \yii\web\View  */

use yii\helpers\Url;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;

$this->title = 'Купить Эликсир';


?>

<div class="container">
    <div class="col-lg-12">

        <h1 class="page-header"><?= Html::encode($this->title) ?></h1>

        <h2 class="page-header">Предисловие</h2>
        <p>Этот документ описывает путь пользователя для того чтобы он смог пополнить свой кошелек. То есть пользователь
            отдает одни монеты, рубли, крипту и тд и получает Золотой Эликсир.</p>
        <p>Для того чтобы пополнить счет надо сначала создать заявку.</p>

        <?= \common\services\documentation\DbTable::widget([
            'name'        => 'request_input',
            'model'       => '\common\models\school\RequestInput',
            'description' => 'Заявка на пополнение Золотого Эликсира',
            'columns'     => [
                [
                    'name'        => 'id',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Идентификатор заявки',
                ],
                [
                    'name'        => 'user_id',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Идентификатор пользователя',
                ],
                [
                    'name'        => 'amount',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Количество монет которые аватар хочет купить',
                ],
                [
                    'name'        => 'billing_id',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Счет на оплату',
                ],
                [
                    'name'        => 'is_paid',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Флаг. Оплачено? 0 - не оплачено, 1 - оплачено, по умолчанию 0',
                ],
                [
                    'name'        => 'is_paid_client',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Флаг. Оплачено? 0 - не оплачено, 1 - оплачено, по умолчанию 0',
                ],
                [
                    'name'        => 'created_at',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Момент создания заявки',
                ],
            ],
        ]) ?>

        <h2 class="page-header">Где будет эта кнопка пополнения</h2>
        <p>
            <a href="https://www.draw.io/#G1h-XEQ6JAhaHvIIbLzeMfMSvW8wUaHqPV" target="_blank">
                <img src="/images/controller/admin-developer/user-models-buy-elixir/input.png">
            </a>
        </p>

        <p>Если платежная система эфир то сделаю уведомление в телеграм бот.</p>
    </div>
</div>

