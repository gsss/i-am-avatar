<?php

/** $this \yii\web\View  */

use yii\helpers\Url;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;

$this->title = 'Анкетирование. Дополнительные поля при заявке';


?>

<div class="container">
    <div class="col-lg-12">
        <h1 class="page-header"><?= Html::encode($this->title) ?></h1>

        <p>Анкетирование предназначено для сбора дополнительных данных о пользователе в момент заказа. Заказ может быть как заказ курса и заказ товара.</p>


        <?= \common\services\documentation\DbTable::widget([
            'name'        => 'school_potok_fields',
            'description' => 'дополнительные поля для заявки',
            'model'       => '\common\models\school\AnketaField',
            'columns'     => [
                [
                    'name'        => 'id',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Идентификатор записи',
                ],
                [
                    'name'        => 'potok_id',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Идентификатор потока к которому присоединяются поля',
                ],
                [
                    'name'        => 'created_at',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Момент создания записи',
                ],
                [
                    'name'        => 'name',
                    'type'        => 'varchar(100)',
                    'isRequired'  => true,
                    'description' => 'Название поля',
                ],
                [
                    'name'        => 'description',
                    'type'        => 'varchar(1000)',
                    'isRequired'  => true,
                    'description' => 'Описание поля',
                ],
                [
                    'name'        => 'type_id',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Тип поля',
                ],
                [
                    'name'        => 'is_required',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Флаг, обязательное поле? 0 - нет, 1 - да, по умолчанию - 0',
                ],
            ],
        ]) ?>

        <h2 class="page-header">Типы полей</h2>
        <p><b>Строка (1)</b></p>
        <p><b>Текстовое поле (2)</b></p>
        <p><b>Целое (3)</b></p>
        <p><b>Дата (4)</b></p>
        <p><b>Картинка (5)</b></p>
        <p><b>Файл (6)</b></p>
        <p><b>Список И (7)</b></p>
        <p><b>Список ИЛИ (8)</b></p>

        <?= \common\services\documentation\DbTable::widget([
            'name'        => 'school_potok_fields_list',
            'description' => 'Поля для списков И/ИЛИ',
            'model'       => '\common\models\school\AnketaListItem',
            'columns'     => [
                [
                    'name'        => 'id',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Идентификатор записи',
                ],
                [
                    'name'        => 'field_id',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Идентификатор поля',
                ],
                [
                    'name'        => 'name',
                    'type'        => 'varchar(100)',
                    'isRequired'  => true,
                    'description' => 'Наименование элемента списка',
                ],
            ],
        ]) ?>

        <?= \common\services\documentation\DbTable::widget([
            'name'        => 'school_shop_anketa',
            'description' => 'Анета школы для магазина',
            'model'       => '\common\models\school\AnketaShop',
            'columns'     => [
                [
                    'name'        => 'id',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Идентификатор записи',
                ],
                [
                    'name'        => 'school_id',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Идентификатор школы',
                ],
                [
                    'name'        => 'name',
                    'type'        => 'varchar(100)',
                    'isRequired'  => true,
                    'description' => 'Наименование анкеты',
                ],
                [
                    'name'        => 'created_at',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Момент создания записи',
                ],
            ],
        ]) ?>

        <?= \common\services\documentation\DbTable::widget([
            'name'        => 'school_potok_fields_list_data_request',
            'description' => 'данные анкеты для потоков (связь на заказ)',
            'model'       => '\common\models\school\AnketaShopData',
            'columns'     => [
                [
                    'name'        => 'id',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Идентификатор записи',
                ],
                [
                    'name'        => 'request_id',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Идентификатор заявки',
                ],
                [
                    'name'        => 'anketa_id',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Идентификатор анкеты',
                ],
                [
                    'name'        => 'data',
                    'type'        => 'text',
                    'isRequired'  => true,
                    'description' => 'Данные JSON',
                ],
                [
                    'name'        => 'created_at',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Момент создания записи',
                ],
            ],
        ]) ?>

        <?= \common\services\documentation\DbTable::widget([
            'name'        => 'school_shop_anketa_link',
            'description' => 'ссылка анкеты школы и продукта для которого собираются данные',
            'model'       => '\common\models\school\AnketaShopLink',
            'columns'     => [
                [
                    'name'        => 'id',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Идентификатор записи',
                ],
                [
                    'name'        => 'anketa_id',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'идентифиатор анкеты school_shop_anketa.id',
                ],
                [
                    'name'        => 'product_id',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'идентификатор продукта gs_unions_shop_product.id',
                ],
            ],
        ]) ?>


        <p>
            <a href="https://www.draw.io/#G18E38EmVSpsb04o88X5_47lbVpkr1Y5Qy" target="_blank">
                <img src="/images/controller/admin-developer/user-anketa/p.png">
            </a>
        </p>
        <p><code>school_potok_fields</code> - Список полей потока (school_potok) или для анкеты (school_shop_anketa)</p>
        <p><code>school_potok</code> - поток куда записываются люди на событие</p>
        <p><code>school_shop_anketa</code> - анета школы для магазина</p>
        <p><code>school_potok_fields_list_data_lid</code> - данные анкеты для потоков (связь на лид)</p>
        <p><code>school_potok_fields_list_data_request</code> - данные анкеты для магазина (связь на заказ)</p>
        <p><code>school_shop_anketa_link</code> - ссылка анкеты школы и продукта для которого собираются данные</p>

        <h2 class="page-header">Как работать с анкетами</h2>

        <h3 class="page-header">Анкеты для заявки на курс</h3>
        <h4 class="page-header">Исследование</h4>
        <p>Как сопоставить ссылку АнкетаШколы - Событие/поток? Пока нет связи. Надо создать. А нужна ли она? можно же ведь и выбрать поля по потоку. нет тогда непонятно какая анкета. </p>
        <p>Стоит ли использовать таблицу school_shop_anketa как сущность анкеты или лучше сделать другую? В принципе можно но потом они могут расходиться. тогда сделаю пока общую.</p>

        <h4 class="page-header">Решение</h4>
        <p>Я в событии/потоке.</p>
        <p>Выбираю связь <code>school_potok_anketa_link</code> и определяю анкету</p>
        <p>Выбираю анкету <code>school_shop_anketa</code></p>
        <p>Выбираю список полей <code>school_potok_fields</code> и вывожу его</p>
        <p>Собранные данные записываю в <code>school_potok_fields_list_data_lid</code> для <code>potok_id</code> и <code>lid_id</code></p>
        <p>
            <a href="https://www.draw.io/#G1Y87yPDwegQHcb0RJ4q2ptcirniZYu2Db" target="_blank">
                <img src="/images/controller/admin-developer/user-anketa/potok.png">
            </a>
        </p>
        <h3 class="page-header">Анкеты для покупки товара</h3>
    </div>
</div>

