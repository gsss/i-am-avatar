<?php

/** $this \yii\web\View  */

use yii\helpers\Url;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;

$this->title = 'Школа. Уроки';


?>

<div class="container">
    <div class="col-lg-12">
        <h1 class="page-header"><?= Html::encode($this->title) ?></h1>

        <h2 class="page-header">Описание</h2>
        <p>Это нужно для того чтобы учитель мог создавать свои уроки и выкладывать / предоставлять доступ для других.
            Платно или на другой основе.</p>
        <p>Какие уроки могут быть?</p>
        <p>1. Видео в YouTube</p>
        <p>2. ТекстДомашнее задание к уроку HTML</p>
        <p>3. Документ DOCX, PDF</p>
        <p>Нужно к объединению добавить сущность Школа со своими параметрами</p>
        <p>Как предоставлять доступ к урокам?</p>
        <p>1. Лично для каждого клиента</p>
        <p>2. Для группы людей. Тогда нужно сделать сущность группа/класс внутри школы.</p>
        <p>3. Для Участников такого то события на который они записались/оплатили.</p>

        <p>
            <iframe width="100%" height="315" src="https://www.youtube.com/embed/8VnEJE1T3yI" frameborder="0"
                    allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
                    allowfullscreen></iframe>
        </p>

        <p>
            <a href="https://www.draw.io/#G1E-sIB5Vuc15RkUqVIE6lj9m8LldzVe-b" target="_blank">
                <img src="/images/controller/admin-developer/school-uroki/struct.png" class="thumbnail">
            </a>
        </p>




        <h2 class="page-header">Права доступа</h2>

        <p>Например курсы состоят из уроков. Урок состоит из нескольких материатов. Для урока может быть выдано домашнее
            задание.</p>
        <p>Для урока может быть задано от одного до нескольких лекторов и ведущих.</p>
        <p>Для курса задаются уже права доступа для группы или для конкретного пользователя.</p>

        <h2 class="page-header">Сущности</h2>

        <h3 class="page-header">Школа</h3>

        <?= \common\services\documentation\DbTable::widget([
            'name'        => 'school',
            'description' => 'Школа',
            'model'       => '\common\models\school\School',
            'columns'     => [
                [
                    'name'        => 'id',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Идентификатор школы',
                ],
                [
                    'name'        => 'user_id',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Идентификатор держателя школы',
                ],
                [
                    'name'        => 'currency_id',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Идентификатор валюты школы dbWallet.currency.id',
                ],
                [
                    'name'        => 'wallet_id',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Идентификатор кошелька школы dbWallet.wallet.id',
                ],
                [
                    'name'        => 'paid_till',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Момент до которого оплачена школа',
                ],
                [
                    'name'        => 'name',
                    'type'        => 'varchar(100)',
                    'isRequired'  => true,
                    'description' => 'Название школы',
                ],
                [
                    'name'        => 'image',
                    'type'        => 'varchar(100)',
                    'isRequired'  => true,
                    'description' => 'Картнка школы',
                ],
                [
                    'name'        => 'dns',
                    'type'        => 'varchar(100)',
                    'isRequired'  => false,
                    'description' => 'Имя dns третьего уровня. Если второй уровень, то не должно быть "www"',
                ],
                [
                    'name'        => 'description',
                    'type'        => 'varchar(1000)',
                    'isRequired'  => false,
                    'description' => 'Описание',
                ],
                [
                    'name'        => 'subscribe_from_name',
                    'type'        => 'varchar(100)',
                    'isRequired'  => false,
                    'description' => 'Обратный адрес в рассылке, имя',
                ],
                [
                    'name'        => 'subscribe_from_email',
                    'type'        => 'varchar(100)',
                    'isRequired'  => false,
                    'description' => 'Обратный адрес в рассылке, почта',
                ],
                [
                    'name'        => 'subscribe_image',
                    'type'        => 'varchar(255)',
                    'isRequired'  => false,
                    'description' => 'Картинка для письма в рассылке',
                ],
                [
                    'name'        => 'favicon',
                    'type'        => 'varchar(100)',
                    'isRequired'  => false,
                    'description' => 'Иконка для сайта',
                ],
                [
                    'name'        => 'header_id',
                    'type'        => 'int',
                    'isRequired'  => false,
                    'description' => 'Идентификатор страницы которая выражается шапкой',
                ],
                [
                    'name'        => 'footer_id',
                    'type'        => 'int',
                    'isRequired'  => false,
                    'description' => 'Идентификатор страницы которая выражается подвалом',
                ],
                [
                    'name'        => 'files_max_size',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'колво мегабайт которое можно загрузить в облако. 1mb=1024*1024 байт. По умолчанию 50 Mb.',
                ],
                [
                    'name'        => 'fund_rub',
                    'type'        => 'int',
                    'isRequired'  => false,
                    'description' => 'Рублевый фонд на развитие',
                ],
                [
                    'name'        => 'is_catalog',
                    'type'        => 'tinyint',
                    'isRequired'  => true,
                    'description' => 'Флаг. Школа в каталоге? 1 - Да, школа видна в каталоге. 0 - школа скрыта, нет в каталоге. По умолчанию - 0',
                ],
                [
                    'name'        => 'is_mlm',
                    'type'        => 'tinyint',
                    'isRequired'  => true,
                    'description' => 'Флаг. Есть партнерская программа? 1 - Да. 0 - нет. По умолчанию - 0',
                ],
                [
                    'name'        => 'is_hide',
                    'type'        => 'tinyint',
                    'isRequired'  => true,
                    'description' => 'Флаг. Скрыта школа? 1 - Да, скрыта в списке "Мои школы" и если стоит флаг что она в каталоге то там ее тоже не показывать. 0 - нет, показывается в "Мои школы". По умолчанию - 0',
                ],
                [
                    'name'        => 'referal_wallet_id',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Идентификатор кошелька. Кошелек откуда берутся реферальные начисления',
                ],
            ],
        ]) ?>
        <h3 class="page-header">Сертификат</h3>

        <?= \common\services\documentation\DbTable::widget([
            'name'    => 'school_user_sertificate',
            'columns' => [
                [
                    'name'        => 'id',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Идентификатор школы',
                ],
                [
                    'name'        => 'user_id',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Идентификатор держателя сертификата',
                ],
                [
                    'name'        => 'school_id',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Идентификатор школы',
                ],
                [
                    'name'        => 'name',
                    'type'        => 'varchar(100)',
                    'isRequired'  => true,
                    'description' => 'Название сертификата',
                ],
                [
                    'name'        => 'description',
                    'type'        => 'text',
                    'isRequired'  => true,
                    'description' => 'Название школы',
                ],
                [
                    'name'        => 'image',
                    'type'        => 'varchar(100)',
                    'isRequired'  => true,
                    'description' => 'Картинка',
                ],
            ],
        ]) ?>

        <h3 class="page-header">Курс</h3>
        <p>Основные свойства курса:</p>
        <p>- Название</p>
        <p>- Принадлежность Школе</p>

        <?= \common\services\documentation\DbTable::widget([
            'name'    => 'school_kurs',
            'columns' => [
                [
                    'name'        => 'id',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Идентификатор курса',
                ],
                [
                    'name'        => 'name',
                    'type'        => 'varchar(100)',
                    'isRequired'  => true,
                    'description' => 'Название курса',
                ],
                [
                    'name'        => 'school_id',
                    'type'        => 'integer',
                    'isRequired'  => true,
                    'description' => 'Идентификатор школы',
                ],
                [
                    'name'        => 'status',
                    'type'        => 'integer',
                    'isRequired'  => true,
                    'description' => '1 - только подтверждение почты, 2 - подтверждение и регистрация',
                ],
                [
                    'name'        => 'is_show',
                    'type'        => 'tinyint',
                    'isRequired'  => true,
                    'description' => 'флаг. Показывать в каталоге? 0 - Не показывать, скрыта, 1 - показывать. По умолчанию - 0',
                ],
                [
                    'name'        => 'for_who',
                    'type'        => 'text',
                    'isRequired'  => false,
                    'description' => 'Для кого. Текст с переносом строки',
                ],
                [
                    'name'        => 'daet',
                    'type'        => 'text',
                    'isRequired'  => false,
                    'description' => 'Что дает. Текст с переносом строки',
                ],
                [
                    'name'        => 'social_net_image',
                    'type'        => 'varchar(255)',
                    'isRequired'  => false,
                    'description' => 'Репост для соц сети. Картинка',
                ],
                [
                    'name'        => 'social_net_title',
                    'type'        => 'varchar(255)',
                    'isRequired'  => false,
                    'description' => 'Репост для соц сети. Заголовок',
                ],
                [
                    'name'        => 'social_net_description',
                    'type'        => 'varchar(2000)',
                    'isRequired'  => false,
                    'description' => 'Репост для соц сети. Описание',
                ],
            ],
        ]) ?>

        <h3 class="page-header">Урок</h3>

        <p>Курсы содержат уроки.
            Уроки могут быть доступны как сразу все так и последовательно.
            Если доступны все уроки сразу то ученик может проходить их последовательно или в разнобой не соблюдая
            очередность.
            Если доступ производится последовательно то значит что ученик не может перейти к прохождению следующего
            урока не выплнив домашнее задание и не получив положительной оценки.
        </p>
        <p>Основные свойства урока:</p>
        <p>- Название</p>
        <p>- Принадлежность курсу</p>

        <?= \common\services\documentation\DbTable::widget([
            'name'        => 'school_kurs_lesson',
            'model'       => '\common\models\school\Lesson',
            'description' => 'Урок',
            'columns'     => [
                [
                    'name'        => 'id',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Идентификатор урока',
                ],
                [
                    'name'        => 'name',
                    'type'        => 'varchar(100)',
                    'isRequired'  => true,
                    'description' => 'Название урока',
                ],
                [
                    'name'        => 'image',
                    'type'        => 'varchar(255)',
                    'isRequired'  => true,
                    'description' => 'Изображение урока',
                ],
                [
                    'name'        => 'kurs_id',
                    'type'        => 'integer',
                    'isRequired'  => true,
                    'description' => 'Идентификатор курса',
                ],
                [
                    'name'        => 'type_id',
                    'type'        => 'integer',
                    'isRequired'  => true,
                    'description' => 'Тип урока',
                ],
                [
                    'name'        => 'description',
                    'type'        => 'varchar(4000)',
                    'isRequired'  => false,
                    'description' => 'Описание урока, Html запрещен',
                ],
                [
                    'name'        => 'is_stop',
                    'type'        => 'tinyint',
                    'isRequired'  => false,
                    'description' => 'Флаг. Это стоповый урок? 0 - нет, после этого урока остальные видны, 1 - да, следующий урок будет виден после того как он выполнить домашнее задание',
                ],
            ],
        ]) ?>


        <p>Если урок является стоповым а дз не указано?</p>
        <p>Если урок является то обязательно должно быть указано домашнее задание.</p>
        <p>А если дз указано то обязательно ли это считается то урок является стоповым? нет.</p>
        <p>Значит флаг is_stop я перенешу в домашнее задание.</p>


        <p>Урок может быть задан в нескольких типах</p>
        <p>- Вебинар</p>
        <p>- Видео (2)</p>
        <p>- Аудио</p>
        <p>- Файл (pdf, pptx, docx)</p>
        <p>- Страница Аватар (5) school_kurs_lesson_page</p>

        <p>Где указывается список уроков?</p>

        <?= \common\services\documentation\DbTable::widget([
            'name'    => 'school_kurs_lesson_type',
            'model'   => '\common\models\school\LessonType',
            'columns' => [
                [
                    'name'        => 'id',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Идентификатор типа',
                ],
                [
                    'name'        => 'name',
                    'type'        => 'varchar(100)',
                    'isRequired'  => true,
                    'description' => 'Наименование типа',
                ],
                [
                    'name'        => 'sort_index',
                    'type'        => 'integer',
                    'isRequired'  => true,
                    'description' => 'Индекс сортировки',
                ],
            ],
        ]) ?>


        <?= \common\services\documentation\DbTable::widget([
            'name'    => 'school_kurs_lesson_video',
            'model'   => '\common\models\school\LessonVideo',
            'columns' => [
                [
                    'name'        => 'id',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Идентификатор строки',
                ],
                [
                    'name'        => 'lesson_id',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Идентификатор урока',
                ],
                [
                    'name'        => 'video_id',
                    'type'        => 'varchar(20)',
                    'isRequired'  => true,
                    'description' => 'Идентификатор видео youtube',
                ],
            ],
        ]) ?>

        <?= \common\services\documentation\DbTable::widget([
            'name'    => 'school_kurs_lesson_page',
            'model'   => '\common\models\school\LessonPage',
            'columns' => [
                [
                    'name'        => 'id',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Идентификатор строки',
                ],
                [
                    'name'        => 'lesson_id',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Идентификатор урока',
                ],
                [
                    'name'        => 'page_id',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Идентификатор страницы',
                ],
            ],
        ]) ?>


        <?= \common\services\documentation\DbTable::widget([
            'name'    => 'school_kurs_lesson_vebinar',
            'columns' => [
                [
                    'name'        => 'id',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Идентификатор строки',
                ],
                [
                    'name'        => 'lesson_id',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Идентификатор урока',
                ],
                [
                    'name'        => 'video_id',
                    'type'        => 'varchar(20)',
                    'isRequired'  => true,
                    'description' => 'Идентификатор видео youtube',
                ],
            ],
        ]) ?>


        <p>Предупреждение. Оценка привязывается к домашнему заданию, а домашнее задание одно на все прохождения, то
            может случиться ситуация что если человек проходит курс дважды, то будет виден и предыдущий ответ а это уже
            некрасиво, должны быть только ответы на текущее прехождение. Поэтому чтобы избежать такой ситуации то нужно
            ввести такую сущность как прохождение курса группой.</p>

        <h3 class="page-header">Прохождение курса группой - поток</h3>

        <?= \common\services\documentation\DbTable::widget([
            'name'    => 'school_potok',
            'columns' => [
                [
                    'name'        => 'id',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Идентификатор потока',
                ],
                [
                    'name'        => 'kurs_id',
                    'type'        => 'integer',
                    'isRequired'  => true,
                    'description' => 'Идентификатор курса',
                ],
                [
                    'name'        => 'date_start',
                    'type'        => 'date',
                    'isRequired'  => false,
                    'description' => 'Дата старта потока (планируемая)',
                ],
                [
                    'name'        => 'date_end',
                    'type'        => 'date',
                    'isRequired'  => false,
                    'description' => 'Дата окончания потока (планируемая)',
                ],
            ],
        ]) ?>

        <?= \common\services\documentation\DbTable::widget([
            'name'    => 'user_school_link',
            'columns' => [
                [
                    'name'        => 'id',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Идентификатор записи',
                ],
                [
                    'name'        => 'user_id',
                    'type'        => 'integer',
                    'isRequired'  => true,
                    'description' => 'Идентификатор пользователя',
                ],
                [
                    'name'        => 'school_id',
                    'type'        => 'integer',
                    'isRequired'  => true,
                    'description' => 'Идентификатор школы',
                ],
            ],
        ]) ?>


        <p>Обязательно указываются курс к которому привязан поток и группа которая го будет проходить.</p>
        <p>Можно указать дата начала и окончания потока.</p>

        <h2 class="page-header">Продажа курса</h2>
        <p>Продать можно только курс. Если у вас один урок или вебинар то делайте курс с одним уроком.</p>
        <p>Организовать продажу курса можно через событие.</p>
        <p>Продажа курса сопровождается обязательным формированием потока и группы на него.</p>
        <p>Как назвать продажу курса? Продажа курса? Событие? Анонс? Анонс - это просто объявление о событии. А что я
            хочу?
            Я хочу как то обозначить именно событие связанное с продажей курса.
            Ну скорее это продажа курса.
            Выгладит в виде анонса.
            Рабочее название - событие</p>
        <p>По сути конечно это страничка - минилендинг с продажей с отдельной ссылкой.</p>

        <?= \common\services\documentation\DbTable::widget([
            'name'    => 'school_sale_kurs_event',
            'columns' => [
                [
                    'name'        => 'id',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Идентификатор потока',
                ],
                [
                    'name'        => 'potok_id',
                    'type'        => 'integer',
                    'isRequired'  => true,
                    'description' => 'Идентификатор потока для курса',
                ],
                [
                    'name'        => 'price',
                    'type'        => 'double',
                    'isRequired'  => false,
                    'description' => 'Стоимость курса',
                ],
                [
                    'name'        => 'date_end',
                    'type'        => 'date',
                    'isRequired'  => false,
                    'description' => 'Дата окончания продаж',
                ],
                [
                    'name'        => 'is_offline',
                    'type'        => 'date',
                    'isRequired'  => false,
                    'description' => 'Флаг показывающий что это уже оффлайн курс. 0 - это онлайн курс, 1 - это офлайн курс. По умолчанию 0.',
                ],
            ],
        ]) ?>
        <p>Еще я так подумал что лучше конечно же сделать и возможность физического обучения. Да это очень
            актуально.</p>

        <h2 class="page-header">Продажа курса. Местоположение</h2>

        <p>Описывает местоположение проведения курса.</p>
        <?= \common\services\documentation\DbTable::widget([
            'name'    => 'school_sale_kurs_place',
            'columns' => [
                [
                    'name'        => 'id',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Идентификатор местоположения',
                ],
                [
                    'name'        => 'sale_id',
                    'type'        => 'integer',
                    'isRequired'  => true,
                    'description' => 'Идентификатор продажи',
                ],
                [
                    'name'        => 'place',
                    'type'        => 'varchar(100)',
                    'isRequired'  => false,
                    'description' => 'Мосто положение в стандарте Google, (Страна, город)',
                ],
                [
                    'name'        => 'lat',
                    'type'        => 'double',
                    'isRequired'  => false,
                    'description' => 'Мосто положение, широта',
                ],
                [
                    'name'        => 'lng',
                    'type'        => 'double',
                    'isRequired'  => false,
                    'description' => 'Мосто положение, долгота',
                ],
                [
                    'name'        => 'id_string',
                    'type'        => 'varchar(10)',
                    'isRequired'  => false,
                    'description' => 'уникальный идентификатор',
                ],
            ],
        ]) ?>
        <p>Для продажи можно продумать некое подобие конструктора лендингов как в тильде.</p>
        <p>Ну банально можно сделать заголовок, преимущества, описание, форма заказа, контакты.</p>
        <p>Кстати как формируется алгоритм заказа?</p>
        <p>1. Клиент оставляет свои контакты, Имя телефон и почту</p>
        <p>2. Он регистрируется как клиент в системе</p>
        <p>3. Он Оплачивает курс</p>
        <p>4. Ему открывается доступ</p>
        <p>5. Он получает письмо в котором все расписывается. Возможности и тд. Если это физическое мероприятие то
            получает QR код для прохода.</p>

        <h2 class="page-header">Продажа курса. Местоположение</h2>

        <p>Рассылку можно любую сделать</p>
        <p><a href="/admin-developer/school-subscribe">Рассылки</a></p>

        <p>Самые главные вопросы у меня сейчас это нужно ли разделсть поток и продажу?</p>
        <p>Чтобы отвестить надо проанализировать а может буть ли по другому?</p>
        <p>Рассмомтрим.</p>
        <p>Например может быть одна продажа и два потока? проходит в одно и тоже время и по тойже цене и с тойже
            страницы. И идет набор двух групп, потоков. Возможно но странно. Если бы не было такой возможности то как
            нужно было бы поступить? Сделать две продажи и две страницы.</p>
        <p>Например один поток и две продажи. Это невозможно, так как поток набирается с продажи.</p>
        <p>Я думаю лучше упростить, одна продажа один поток, так проще и мне и пользователям.</p>

        <p>А вот учителей точно лучше разделить. И еще момент что учитель это пользователь платформы.</p>

        <?= \common\services\documentation\DbTable::widget([
            'name'    => 'school_teacher_link',
            'columns' => [
                [
                    'name'        => 'id',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Идентификатор ссылки',
                ],
                [
                    'name'        => 'user_id',
                    'type'        => 'integer',
                    'isRequired'  => true,
                    'description' => 'Идентификатор пользователя-учителя',
                ],
                [
                    'name'        => 'school_id',
                    'type'        => 'integer',
                    'isRequired'  => true,
                    'description' => 'Идентификатор школы',
                ],
            ],
        ]) ?>

        <h2 class="page-header">Рассылка нового урока</h2>

        <p>Рассылка проходит по всем кто записан на курс. Есть небольшая проблема. На курс может быть несколько потоков
            и в том числе те которые уже прошли. Вот с ними что делать?</p>
        <p>Файл письма <code>/avatar-bank/mail/html/subscribe/new-lesson.php</code></p>

        <h2 class="page-header">Удаление курса</h2>
        <p>Чтобы удалить курс нужно до этого удалить все вложенные элементы:</p>
        <ul>
            <li>уроки (school_kurs_lesson) с настройками</li>
            <li>потоки (school_potok)</li>
            <li>продажи (school_kurs_sale)</li>
            <li>заявки на продажи (school_kurs_sale_request)</li>
            <li>Рассылки (school_potok_subscribe)</li>
            <li>Отчеты по рассылкам (school_potok_subscribe_item)</li>
            <li>ЛИД (school_potok_user_root_link)</li>
        </ul>

        <p>
            <a href="https://www.draw.io/#G1Z0Z0Vu3pKi4v30kUslyojBdvryQUgEuA" target="_blank">
                <img src="/images/controller/admin-developer/school-uroki/urok.png">
            </a>
        </p>

        <p>А что если с потоком еще и сопоставлены страницы? Значит надо спросить их удалять или как?</p>

    </div>
</div>

