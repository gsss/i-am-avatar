<?php

/** $this \yii\web\View  */

use yii\helpers\Url;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;

$this->title = 'Школа. Домен';


?>

<div class="container">
    <div class="col-lg-12">
        <h1 class="page-header"><?= Html::encode($this->title) ?></h1>

        <p>
            <a href="https://www.draw.io/#G1xgf-iq0cegC_uLMwexXZGKomaaQGqpY_" target="_blank">
                <img src="/images/controller/admin-developer/school-domain/1.png" class="thumbnail">
            </a>
        </p>
        <p>Все заявки в админке показываются по роутеру <code>admin-school-dns/index</code>.</p>
        <p>Отклонить вызывает AJAX: <code>/admin-school-dns/reject</code> и передается причина отклонения.</p>
        <p>Одобрить вызывает AJAX: <code>/admin-school-dns/accept</code></p>
        <h2 class="pahe-header">Конфигурация</h2>
        <p>В параметре <code>schoolIp</code> находится IP сервера, где находятся все школы, применяется для CloudFlare для создания домена третьего уровня.</p>
        <pre>Yii::$app->params['schoolIp']</pre>

        <h2 class="pahe-header">Как формируется запись третьего уровня?</h2>

        <p>DNS третьего уровня создается через <a
                    href="https://api.cloudflare.com/#dns-records-for-a-zone-create-dns-record" target="_blank">AIP
                Cloudflare</a>.</p>
        <p>WebServer настроен на *.i-am-avatar.com на папку <code>school</code>.</p>
        <p>Настройка построена таким образом: есть настройка на главный домен и www все остальные запросы на сервер обрабатываются в файле default.</p>
        <pre>ServerName _;</pre>
        <p>Но при такой настройке в переменной <code>$_SERVER['SERVER_NAME']</code> находится строка <code>_</code>. За счет чего возникает ошибка при отправке почту. Поэтому перед каждым запросом на сервер я подставляю <code>$_SERVER['SERVER_NAME']</code> = <code>$_SERVER['HOST_NAME']</code></p>


        <p>ZoneId: <code>061a8247a405dcf405ebbbf7544e359c</code> - зона i-am-avatar.com</p>

        <p>При подаче заявке она каждый раз создается заново. То есть если после отказа будет подана заявка то она будет новая.</p>

        <?= \common\services\documentation\DbTable::widget([
            'name'        => 'school_dns_request',
            'model'       => '\common\models\school\DnsRequest',
            'description' => 'Заявка на подключение домена',
            'columns'     => [
                [
                    'name'        => 'id',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Идентификатор записи',
                ],
                [
                    'name'        => 'school_id',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Идентификатор школы, какой принадлежит заявка',
                ],
                [
                    'name'        => 'is_dns_verified',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Утвержден ли домен? 0 - в ожидании, 1 - утвержден, -1 - отклонен',
                ],
                [
                    'name'        => 'created_at',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'момент создания заявки',
                ],
                [
                    'name'        => 'user_id',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Пользователь создавший заявку, это может быть не только автор школы, но и администратор, поэтому надо понимать кто именно, чтобы если что связаться с ним',
                ],
                [
                    'name'        => 'name',
                    'type'        => 'varchar(100)',
                    'isRequired'  => true,
                    'description' => 'Название домена, третий уровень - только имя, второй уровень - только имя, без www',
                ],
            ],
        ]) ?>


        <h2 class="pahe-header">Домен второго уровня</h2>
        <p>Создается пока вручную.</p>
        <p>Конфигурация nginx</p>
        <pre>server {
    listen 80;

    # на какое доменное имя будет откликаться веб сервер
    server_name alfa16.ru www.alfa16.ru;

    # лог для этого сайта
    access_log /home/god/i-am-avatar/log-nginx/access.log;
    error_log  /home/god/i-am-avatar/log-nginx/errors.log;


    # Домашняя папка
    root /home/god/i-am-avatar/www/school/web;

    # Запрет на открытие сайта в чужом фрейме
    add_header X-Frame-Options "SAMEORIGIN";

    # Установка индексного файла, файл по умолчанию
    index index.php;

    # Установка максимального объема POST запроса
    client_max_body_size 200M;

    location / {
        if (!-e $request_filename) {
            rewrite ^(.*)$ /index.php;
        }

        try_files $uri $uri/ =404;
    }

    # Установка обработчика для файлов PHP
    location ~ \.php$ {
        include snippets/fastcgi-php.conf;
        fastcgi_pass unix:/var/run/php/php7.0-fpm.sock;
    }

    location ~ /\.ht {
        deny all;
    }
}</pre>

        <h2 class="page-header">Покупка доменов через провайдера услуг</h2>
        <p><a href="https://www.dynadot.com/ru/domain/api2.html" target="_blank">https://www.dynadot.com/ru/domain/api2.html</a></p>
        <p>Запросы все делаются через GET запрос</p>
        <p>В интерфейсе это отдельная страница <code>/cabinet-school-dns/search</code></p>
        <p>Чтобы найти и зарегистрировать домен надо: найти свободный домен, нажать кнопку зарегистрирвать, уходит заявка на регистрацию, мы его покупаем если есть деньги. и запускаем.</p>
        <p>Создан API: <code>\common\services\DynadotApi</code></p>
        <pre>/** @var \common\services\DynadotApi $DynadotApi */
$DynadotApi = Yii::$app->DynadotApi;
$DynadotApi->command('search', ['domain0' => 'name']);</pre>

        <h2 class="page-header">Утверждение домена</h2>
        <p>Создается страница</p>
        <h2 class="page-header">Сброс домена домена</h2>
        <p><code>/cabinet-school-dns/reject</code></p>
        <p>Если домен третьего уровня то надо удалять его в Cloudflare.</p>

        <h2 class="page-header">Поиск домена</h2>
        <p><code>/cabinet-school-dns/search</code></p>
        <p>Модель сценария:</p>
        <p>Страница поиска</p>
        <p>Нажимаю поиск AJAX</p>
        <pre>ok,

domain0,ddd1.com,,no,</pre>
        <p>Если возможно то появляется кнопка купить.</p>

        <p>
            <a href="https://www.draw.io/#G1uKPd9H3qRVzL2NuSZcQloG6uYITLIK6R" target="_blank">
                <img src="/images/controller/admin-developer/school-domain/search.png">
            </a>
        </p>
    </div>
</div>