<?php

/** $this \yii\web\View  */

use yii\helpers\Url;
use app\services\GsssHtml;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use app\services\GetArticle\Collection;

$this->title = 'Резервное копирование';


?>

<div class="container">
    <div class="col-lg-12">
        <h1 class="page-header"><?= Html::encode($this->title) ?></h1>

        <p>Резервное копирование делается локально на сервере.</p>

        <pre>0 * * * * /home/god/i-am-avatar/backup/hour.sh
0 0 * * 0 /home/god/i-am-avatar/backup/day1.sh
0 0 * * 1 /home/god/i-am-avatar/backup/day2.sh
0 0 * * 2 /home/god/i-am-avatar/backup/day3.sh
0 0 * * 3 /home/god/i-am-avatar/backup/day4.sh
0 0 * * 4 /home/god/i-am-avatar/backup/day5.sh
0 0 * * 5 /home/god/i-am-avatar/backup/day6.sh
0 0 * * 6 /home/god/i-am-avatar/backup/day7.sh </pre>

<pre>sudo mysqldump -uroot -p*** i_am_avatar_prod_main > /home/god/i-am-avatar/backup/hour/i_am_avatar_prod_main.sql
sudo mysqldump -uroot -p*** i_am_avatar_prod_wallet > /home/god/i-am-avatar/backup/hour/i_am_avatar_prod_wallet.sql</pre>

        <p>Путь сохранения бекапов: <code>~/i-am-avatar/backup</code></p>

        <p>База данных статистики (<code>i_am_avatar_test_stat</code>) нет смысла бекапить.</p>
        <p>Итого получилось раз в час и каждый день в неделю в отдельную базу далее цикл повторяется </p>

        <h2 class="page-header">Как заливать дамп в тестовую базу</h2>

        <pre>mysql -uroot -p***
use i_am_avatar_test_main
source /home/god/i-am-avatar/backup/hour/i_am_avatar_prod_main.sql
use i_am_avatar_test_wallet
source /home/god/i-am-avatar/backup/hour/i_am_avatar_prod_wallet.sql</pre>

        <p>Восстановить пароль от MYSQL <a href="https://sysadmin.ru/articles/kak-izmenit-root-parol-v-mysql" target="_blank">https://sysadmin.ru/articles/kak-izmenit-root-parol-v-mysql</a></p>

    </div>
</div>

