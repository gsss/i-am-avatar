<?php

/** $this \yii\web\View  */

use yii\helpers\Url;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;

$this->title = 'Карта Аватаров';


?>

<div class="container">
    <div class="col-lg-12">
        <h1 class="page-header"><?= Html::encode($this->title) ?></h1>

        <p>Этот документ описывает данные которые необходимы для фцнкционирования карты Аватаров.</p>

        <?= \common\services\documentation\DbTable::widget([
            'name'        => 'user_map',
            'description' => 'Поля для пользователя, для карты',
            'model'       => '\common\models\school\UserMap',
            'columns'     => [
                [
                    'name'        => 'id',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Идентификатор записи',
                ],
                [
                    'name'        => 'user_id',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Идентификатор пользоватля',
                ],
                [
                    'name'        => 'lat',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'LAT на карте * 1000000000',
                ],
                [
                    'name'        => 'lng',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'LNG на карте * 1000000000',
                ],
                [
                    'name'        => 'type_id',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Идентификатор какой точности координаты. 1 - Город, 2 - Район, 3 - Дом',
                ],
            ],
        ]) ?>

        <p><code>/map/index</code> - распололожение карты.</p>
        <p><code>/cabinet-map/index</code> - Редактирование положения на карте.</p>
    </div>
</div>

