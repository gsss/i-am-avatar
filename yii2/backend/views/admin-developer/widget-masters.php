<?php

/** $this \yii\web\View  */

use yii\helpers\Url;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;

$this->title = 'widget-masters';


?>

<div class="container">
    <div class="col-lg-12">
        <h1 class="page-header"><?= Html::encode($this->title) ?></h1>

        <p>Этот мастер предназначен для выбора мастеров из списка имеющихся. Мастера это пользователи UserAvatar.</p>
        <p>Выбираю из возможных, кликаю добавить и собирается список ведущих.</p>
        <p>Поле INPUT текстовое скытое, в нем через запятую указывается список выбрпннных мастеров.</p>
        <p>Эскиз <a href="https://cloud1.cloud999.ru/upload/cloud/15693/13663_azHI8qN3dT.jpg" target="_blank">https://cloud1.cloud999.ru/upload/cloud/15693/13663_azHI8qN3dT.jpg</a></p>
        <p>Класс <code>\avatar\widgets\Masters\Widget</code></p>
        <p>Шаблон <code>avatar-bank/widgets/Masters/view.php</code></p>
        <p>Если возникает ошибка то как то ее снять, допустим после выбора хотябы одного</p>
        <p class="alert alert-danger">Не доделана ситуация, если таких виджетов будет более одного в форме.</p>



    </div>
</div>

