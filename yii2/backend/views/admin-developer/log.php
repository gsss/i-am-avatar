<?php

/** $this \yii\web\View  */

use yii\helpers\Url;
use app\services\GsssHtml;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use app\services\GetArticle\Collection;

$this->title = 'Логирование';


?>

<div class="container">
    <div class="col-lg-12">
        <h1 class="page-header"><?= Html::encode($this->title) ?></h1>


        <p>Здесь описываются куда пишутся логи на разных снендах и точках входа.</p>
        <p>Yii: <a href="https://www.yiiframework.com/doc/guide/2.0/ru/runtime-logging" target="_blank">https://www.yiiframework.com/doc/guide/2.0/ru/runtime-logging</a>
        </p>

        <h2 class="page-header">Код</h2>
        <p>В коде можно записать четыре уровни (levels) логировани: info, trace, warning, error</p>

        <h2 class="page-header">Исследование</h2>
        <p>Мне нужно логирование в avatar отдельно в console отдельно и в school отдельно. и в api отдельно.</p>
        <p>Какого типа сообщения надо записывать? ошибки, предупреждения и инфо категории avatar.</p>
        <p>Значит мне кажется все можно писать в БД. По крайней мере попробовать.</p>
        <p>Самый лучший вариант все сохранять в БД.</p>

        <h3 class="page-header">PROD</h3>
        <?php
        $rows = [
            [
                'name'    => 'avatar',
                'info'    => 'dbStatistic.log2_avatar',
                'trace'   => '',
                'warning' => 'dbStatistic.log2_avatar',
                'error'   => 'dbStatistic.log2_avatar',

                'except'      => [
                    'yii\web\HttpException:404',
                    'yii\web\HttpException:403',
                    'cs\web\Exception',
                ],
            ],
            [
                'name'    => 'console',
                'info'    => 'dbStatistic.log2_console',
                'trace'   => '',
                'warning' => 'dbStatistic.log2_console',
                'error'   => 'dbStatistic.log2_console',

                'except'      => [
                    'yii\web\HttpException:404',
                    'yii\web\HttpException:403',
                    'cs\web\Exception',
                ],
            ],
            [
                'name'    => 'school',
                'info'    => 'dbStatistic.log2_school',
                'trace'   => '',
                'warning' => 'dbStatistic.log2_school',
                'error'   => 'dbStatistic.log2_school',

                'except'      => [
                    'yii\web\HttpException:404',
                    'yii\web\HttpException:403',
                    'cs\web\Exception',
                ],
            ],
            [
                'name'    => 'api',
                'info'    => 'dbStatistic.log2_api',
                'trace'   => '',
                'warning' => 'dbStatistic.log2_api',
                'error'   => 'dbStatistic.log2_api',

                'except'      => [
                    'yii\web\HttpException:404',
                    'yii\web\HttpException:403',
                    'cs\web\Exception',
                ],
            ],
        ];
        ?>
        <?= \yii\grid\GridView::widget([
            'dataProvider' => new \yii\data\ArrayDataProvider([
                'allModels' => $rows,
            ]),
            'tableOptions' => [
                'class' => 'table table-striped table-hover',
                'style' => 'width: auto',
            ],
            'summary'      => '',
            'columns'      => [
                [
                    'header'    => 'WebEnter',
                    'attribute' => 'name',
                ],
                [
                    'attribute' => 'info',
                    'headerOptions' => [
                        'style' => 'width: 200px;',
                    ],
                    'content'   => function ($item) {
                        return Html::tag('code', $item['info']);
                    }
                ],
                [
                    'attribute' => 'trace',
                    'headerOptions' => [
                        'style' => 'width: 200px;',
                    ],
                    'content'   => function ($item) {
                        return Html::tag('code', $item['trace']);
                    }
                ],
                [
                    'attribute' => 'warning',
                    'headerOptions' => [
                        'style' => 'width: 200px;',
                    ],
                    'content'   => function ($item) {
                        return Html::tag('code', $item['warning']);
                    }
                ],
                [
                    'attribute' => 'error',
                    'headerOptions' => [
                        'style' => 'width: 200px;',
                    ],
                    'content'   => function ($item) {
                        return Html::tag('code', $item['error']);
                    }
                ],
            ],
        ]) ?>
        <p class="alert alert-info">Примечание: тип trace будет писаться если включить YII_DEBUG = true, но по умолчанию он отключен, поэтому в таблице и написано что этот тип сообщений trace нигде не пишется.</p>

        <h3 class="page-header">TEST</h3>
        <?php
        $rows = [
            [
                'name'    => 'avatar',
                'info'    => 'dbStatistic.log2_avatar',
                'trace'   => 'dbStatistic.log2_avatar',
                'warning' => 'dbStatistic.log2_avatar',
                'error'   => 'dbStatistic.log2_avatar',

                'except'      => [
                    'yii\web\HttpException:404',
                    'yii\web\HttpException:403',
                    'cs\web\Exception',
                ],
            ],
            [
                'name'    => 'console',
                'info'    => 'dbStatistic.log2_console',
                'trace'   => 'dbStatistic.log2_console',
                'warning' => 'dbStatistic.log2_console',
                'error'   => 'dbStatistic.log2_console',

                'except'      => [
                    'yii\web\HttpException:404',
                    'yii\web\HttpException:403',
                    'cs\web\Exception',
                ],
            ],
            [
                'name'    => 'school',
                'info'    => 'dbStatistic.log2_school',
                'trace'   => 'dbStatistic.log2_school',
                'warning' => 'dbStatistic.log2_school',
                'error'   => 'dbStatistic.log2_school',

                'except'      => [
                    'yii\web\HttpException:404',
                    'yii\web\HttpException:403',
                    'cs\web\Exception',
                ],
            ],
            [
                'name'    => 'api',
                'info'    => 'dbStatistic.log2_api',
                'trace'   => 'dbStatistic.log2_api',
                'warning' => 'dbStatistic.log2_api',
                'error'   => 'dbStatistic.log2_api',

                'except'      => [
                    'yii\web\HttpException:404',
                    'yii\web\HttpException:403',
                    'cs\web\Exception',
                ],
            ],
        ];
        ?>
        <?= \yii\grid\GridView::widget([
            'dataProvider' => new \yii\data\ArrayDataProvider([
                'allModels' => $rows,
            ]),
            'tableOptions' => [
                'class' => 'table table-striped table-hover',
                'style' => 'width: auto',
            ],
            'summary'      => '',
            'columns'      => [
                [
                    'header'    => 'WebEnter',
                    'attribute' => 'name',
                ],
                [
                    'attribute' => 'info',
                    'headerOptions' => [
                        'style' => 'width: 200px;',
                    ],
                    'content'   => function ($item) {
                        return Html::tag('code', $item['info']);
                    }
                ],
                [
                    'attribute' => 'trace',
                    'headerOptions' => [
                        'style' => 'width: 200px;',
                    ],
                    'content'   => function ($item) {
                        return Html::tag('code', $item['trace']);
                    }
                ],
                [
                    'attribute' => 'warning',
                    'headerOptions' => [
                        'style' => 'width: 200px;',
                    ],
                    'content'   => function ($item) {
                        return Html::tag('code', $item['warning']);
                    }
                ],
                [
                    'attribute' => 'error',
                    'headerOptions' => [
                        'style' => 'width: 200px;',
                    ],
                    'content'   => function ($item) {
                        return Html::tag('code', $item['error']);
                    }
                ],
            ],
        ]) ?>
        <p>Причем <code>info</code> и <code>trace</code> пишу только категории <code>avatar</code></p>
        <p>Причем <code>warning</code> и <code>error</code> пишу все кроме</p>
        <pre>'except'      => [
    'yii\web\HttpException:404',
    'yii\web\HttpException:403',
    'cs\web\Exception',
],</pre>
        <p>Итого вся конфигурация выглядит следующим образом. Причем она одна для обоих стендов потому что отличается лишь trace а он на стенде PROD по умолчанию не пишется</p>
        <pre>'log'              => [
    'traceLevel' => YII_DEBUG ? 3 : 0,
    'targets'    => [
        [
            'class'       => 'common\services\DbTarget',
            'levels'      => [
                'warning',
                'error',
            ],
            'except'      => [
                'yii\web\HttpException:404',
                'yii\web\HttpException:403',
                'cs\web\Exception',
            ],

            'db'         => 'dbStatistic',
            'logTable'   => 'log2_avatar',
        ],
        [
            'class'      => 'common\services\DbTarget',
            'categories' => ['avatar\\*'],
            'levels'     => [
                'info',
                'trace',
            ],

            'db'         => 'dbStatistic',
            'logTable'   => 'log2_avatar',
        ],
    ],
],</pre>


        <h3 class="page-header">Запись в файл</h3>
        <p>Все <code>error</code> и <code>warning</code> для всех приложений еще дополнительно пишутся в файл по умолчанию. Вот файл <code>common/config/main.php</code></p>
        <pre>[
    'class'       => '\yii\log\FileTarget',
    'levels'      => [
        'warning',
        'error',
    ],
    'except'      => [
        'yii\web\HttpException:404',
        'yii\web\HttpException:403',
        'cs\web\Exception',
    ],
],</pre>

    </div>
</div>

