<?php

/** $this \yii\web\View  */

use yii\helpers\Url;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;

$this->title = 'widget-anketa';


?>

<div class="container">
    <div class="col-lg-12">
        <h1 class="page-header"><?= Html::encode($this->title) ?></h1>

        <p>Этот виджет предназначен для собирания набора названий полей для анкеты</p>
        <p>Класс <code>\avatar\widgets\Anketa\Widget</code></p>
        <p>Шаблон <code>avatar-bank/widgets/Anketa/view.php</code></p>



    </div>
</div>

