<?php

/** $this \yii\web\View  */

use yii\helpers\Url;
use app\services\GsssHtml;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use app\services\GetArticle\Collection;

$this->title = 'Модуль коментариев';


?>

<div class="container">
    <div class="col-lg-12">
        <h1 class="page-header"><?= Html::encode($this->title) ?></h1>

        <?= \common\services\documentation\DbTable::widget([
            'name'        => 'comments',
            'description' => 'Комментарии',
            'columns'     => [
                [
                    'name'        => 'id',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Идентификатор строки',
                ],
                [
                    'name'        => 'list_id',
                    'type'        => 'integer',
                    'isRequired'  => true,
                    'description' => 'Идентификатор списка комментария',
                ],
                [
                    'name'        => 'user_id',
                    'type'        => 'integer',
                    'isRequired'  => true,
                    'description' => 'Автор коментария',
                ],
                [
                    'name'        => 'parent_id',
                    'type'        => 'integer',
                    'isRequired'  => true,
                    'description' => 'Идентификатор родителя коментария',
                ],
                [
                    'name'        => 'created_at',
                    'type'        => 'integer',
                    'isRequired'  => true,
                    'description' => 'Момент создания',
                ],
                [
                    'name'        => 'text',
                    'type'        => 'string',
                    'isRequired'  => true,
                    'description' => 'Содержимое коментариея',
                ],
            ],
        ]) ?>
        <?= \common\services\documentation\DbTable::widget([
            'name'        => 'comments_list',
            'description' => 'Массив объектов "лист коментариев"',
            'columns'     => [
                [
                    'name'        => 'id',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Идентификатор строки',
                ],
                [
                    'name'        => 'type_id',
                    'type'        => 'integer',
                    'isRequired'  => true,
                    'description' => 'Тип объекта',
                ],
                [
                    'name'        => 'object_id',
                    'type'        => 'integer',
                    'isRequired'  => true,
                    'description' => 'Идентификатор объекта',
                ],
            ],
        ]) ?>
        <?= \common\services\documentation\DbTable::widget([
            'name'        => 'comments_list_type',
            'description' => 'Тип объекта',
            'columns'     => [
                [
                    'name'        => 'id',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Идентификатор строки',
                ],
                [
                    'name'        => 'name',
                    'type'        => 'string',
                    'isRequired'  => true,
                    'description' => 'Наименование объекта',
                ],
                [
                    'name'        => 'after_insert',
                    'type'        => 'string',
                    'isRequired'  => true,
                    'description' => 'Название класса. Обработчки события после добавления коментария (AFTER_INSERT)',
                ],
            ],
        ]) ?>

        <p>В поле <code>after_insert</code> указывается класс который будет обрабатывать событие <code>AFTER_INSERT</code>.</p>
        <p>Класс должен имплементировать интерфейс <code>\common\models\comment\iCommentAfterInsert</code></p>

        <p>Например: тип - страница, блог</p>
        <p>Есть стандартный класс <code>AfterInsert</code> который делает рассылку всем кто осталял коментарии в этой ветке. Рассылка происходит по почте.</p>
        <p><code>Задачи</code> если кто то написал коментарий, то уведомление отсылается автору задачи и всем кто коментировал. Класс <code>AfterInsertTask</code></p>



    </div>
</div>

