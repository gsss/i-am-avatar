<?php

/** $this \yii\web\View  */

use yii\helpers\Url;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;

$this->title = 'Школа. Конструктор сайтов';


?>

<div class="container">
    <div class="col-lg-12">
        <h1 class="page-header"><?= Html::encode($this->title) ?></h1>

        <!-- Nav tabs -->
        <ul class="nav nav-tabs" role="tablist">
            <li role="presentation" class="active"><a href="#clear" aria-controls="clear" role="tab" data-toggle="tab">Чистовик</a>
            </li>
            <li role="presentation"><a href="#potok" aria-controls="potok" role="tab" data-toggle="tab">Поток</a></li>
        </ul>

        <!-- Tab panes -->
        <div class="tab-content">
            <div role="tabpanel" class="tab-pane in active" id="clear">
                <h3 class="page-header">Страница</h3>

                <?= \common\services\documentation\DbTable::widget([
                    'name'    => 'school_page',
                    'columns' => [
                        [
                            'name'        => 'id',
                            'type'        => 'int',
                            'isRequired'  => true,
                            'description' => 'Идентификатор школы',
                        ],
                        [
                            'name'        => 'school_id',
                            'type'        => 'int',
                            'isRequired'  => true,
                            'description' => 'Идентификатор школы',
                        ],
                        [
                            'name'        => 'name',
                            'type'        => 'varchar(100)',
                            'isRequired'  => true,
                            'description' => 'Название страницы',
                        ],
                        [
                            'name'        => 'url',
                            'type'        => 'varchar(100)',
                            'isRequired'  => true,
                            'description' => 'url страницы',
                        ],
                        [
                            'name'        => 'facebook_image',
                            'type'        => 'varchar(255)',
                            'isRequired'  => false,
                            'description' => 'картинка для соц сетей',
                        ],
                        [
                            'name'        => 'facebook_title',
                            'type'        => 'varchar(255)',
                            'isRequired'  => false,
                            'description' => 'Заголовок для соц сетей',
                        ],
                        [
                            'name'        => 'facebook_description',
                            'type'        => 'varchar(2000)',
                            'isRequired'  => false,
                            'description' => 'Описание для соц сетей',
                        ],
                        [
                            'name'        => 'body',
                            'type'        => 'text',
                            'isRequired'  => false,
                            'description' => 'HTML код для страницы',
                        ],
                        [
                            'name'        => 'title',
                            'type'        => 'varchar(255)',
                            'isRequired'  => false,
                            'description' => 'Заголовок для страницы',
                        ],
                        [
                            'name'        => 'description',
                            'type'        => 'varchar(2000)',
                            'isRequired'  => false,
                            'description' => 'Описание для страницы',
                        ],
                        [
                            'name'        => 'keywords',
                            'type'        => 'varchar(2000)',
                            'isRequired'  => false,
                            'description' => 'Ключевые слова для страницы',
                        ],
                        [
                            'name'        => 'favicon',
                            'type'        => 'varchar(100)',
                            'isRequired'  => false,
                            'description' => 'Иконка для страницы PNG квадратная',
                        ],
                        [
                            'name'        => 'header_id',
                            'type'        => 'int',
                            'isRequired'  => false,
                            'description' => 'Идентификатор страницы которая будет шапкой',
                        ],
                        [
                            'name'        => 'footer_id',
                            'type'        => 'int',
                            'isRequired'  => false,
                            'description' => 'Идентификатор страницы которая будет подвалом',
                        ],
                        [
                            'name'        => 'tilda_page_id',
                            'type'        => 'int',
                            'isRequired'  => false,
                            'description' => 'Идентификатор страницы тильды',
                        ],
                    ],
                ]) ?>

                <p>Если <code>school_page.header_id</code> не указан то используется тот что у школы <code>school.header_id</code>.
                </p>
                <p>Если <code>school_page.footer_id</code> не указан то используется тот что у школы <code>school.footer_id</code>.
                </p>

                <p>Шапка и подвал рендерятся только в "чистовой странице", в конструкторе ее не видно.</p>

                <h3 class="page-header">Блок внутри страницы</h3>

                <?= \common\services\documentation\DbTable::widget([
                    'name'    => 'school_page_block_content',
                    'columns' => [
                        [
                            'name'        => 'id',
                            'type'        => 'int',
                            'isRequired'  => true,
                            'description' => 'Идентификатор курса',
                        ],
                        [
                            'name'        => 'page_id',
                            'type'        => 'integer',
                            'isRequired'  => true,
                            'description' => 'Идентификатор страницы',
                        ],
                        [
                            'name'        => 'type_id',
                            'type'        => 'integer',
                            'isRequired'  => true,
                            'description' => 'Тип блока',
                        ],
                        [
                            'name'        => 'content',
                            'type'        => 'string',
                            'isRequired'  => true,
                            'description' => 'Данные для наполнения блока',
                        ],
                        [
                            'name'        => 'sort_index',
                            'type'        => 'integer',
                            'isRequired'  => true,
                            'description' => 'Порядок сортировки, чем ниже значение тем выше на странице',
                        ],
                    ],
                ]) ?>

                <h3 class="page-header">Блок страницы образец</h3>
                <p>Библиотека блоков</p>

                <?= \common\services\documentation\DbTable::widget([
                    'name'    => 'school_page_block',
                    'columns' => [
                        [
                            'name'        => 'id',
                            'type'        => 'int',
                            'isRequired'  => true,
                            'description' => 'Идентификатор курса',
                        ],
                        [
                            'name'        => 'name',
                            'type'        => 'string',
                            'isRequired'  => true,
                            'description' => 'Название блока',
                        ],
                        [
                            'name'        => 'asset',
                            'type'        => 'string',
                            'isRequired'  => true,
                            'description' => 'css',
                        ],


                    ],
                ]) ?>


                <h3 class="page-header">Статистика</h3>

                <?= \common\services\documentation\DbTable::widget([
                    'name'        => 'school_page_stat',
                    'description' => 'Статистика заходов на страницу',
                    'model'       => '\common\models\school\PageStat',
                    'columns'     => [
                        [
                            'name'        => 'id',
                            'type'        => 'int',
                            'isRequired'  => true,
                            'description' => 'Идентификатор школы',
                        ],
                        [
                            'name'        => 'page_id',
                            'type'        => 'int',
                            'isRequired'  => true,
                            'description' => 'Идентификатор страницы',
                        ],
                        [
                            'name'        => 'created_at',
                            'type'        => 'int',
                            'isRequired'  => true,
                            'description' => 'Момент захода',
                        ],
                        [
                            'name'        => 'browser',
                            'type'        => 'varchar(100)',
                            'isRequired'  => true,
                            'description' => 'Браузер',
                        ],
                        [
                            'name'        => 'ip',
                            'type'        => 'varchar(15)',
                            'isRequired'  => true,
                            'description' => 'IP клиента',
                        ],
                        [
                            'name'        => 'country',
                            'type'        => 'varchar(2)',
                            'isRequired'  => true,
                            'description' => 'Страна клиента',
                        ],
                    ],
                ]) ?>
            </div>
            <div role="tabpanel" class="tab-pane fade" id="potok">
                <h2 class="page-header">Описание</h2>
                <p>Делаем на подобии тильды</p>
                <p>Каждая страница состоит из блоков</p>
                <p>Блок наполняется контентом. При выводе страницы она компилируется и выдается.</p>

                <h3 class="page-header">Страница</h3>


                <h3 class="page-header">Блок внутри страницы</h3>

                <?= \common\services\documentation\DbTable::widget([
                    'name'    => 'school_page_block_content',
                    'columns' => [
                        [
                            'name'        => 'id',
                            'type'        => 'int',
                            'isRequired'  => true,
                            'description' => 'Идентификатор курса',
                        ],
                        [
                            'name'        => 'page_id',
                            'type'        => 'integer',
                            'isRequired'  => true,
                            'description' => 'Идентификатор страницы',
                        ],
                        [
                            'name'        => 'type_id',
                            'type'        => 'integer',
                            'isRequired'  => true,
                            'description' => 'Тип блока',
                        ],
                        [
                            'name'        => 'content',
                            'type'        => 'string',
                            'isRequired'  => true,
                            'description' => 'Данные для наполнения блока',
                        ],
                        [
                            'name'        => 'sort_index',
                            'type'        => 'integer',
                            'isRequired'  => true,
                            'description' => 'Порядок сортировки, чем ниже значение тем выше на странице',
                        ],
                    ],
                ]) ?>

                <h3 class="page-header">Блок страницы образец</h3>
                <p>Библиотека блоков</p>

                <?= \common\services\documentation\DbTable::widget([
                    'name'    => 'school_page_block',
                    'columns' => [
                        [
                            'name'        => 'id',
                            'type'        => 'int',
                            'isRequired'  => true,
                            'description' => 'Идентификатор курса',
                        ],
                        [
                            'name'        => 'name',
                            'type'        => 'string',
                            'isRequired'  => true,
                            'description' => 'Название блока',
                        ],
                        [
                            'name'        => 'asset',
                            'type'        => 'string',
                            'isRequired'  => true,
                            'description' => 'css',
                        ],


                    ],
                ]) ?>
                <h2 class="page-header">Хранение шаблона и данных и их компиляция</h2>

                <p>Самый главный вопрос: Как контент записывать в массиве</p>
                <p>Например:</p>
                <pre>{'name': 'test','blocks': [{'image':'https://www.youtube.com/watch?v=T6sOKjzV-u4'},{'image':'https://www.youtube.com/watch?v=T6sOKjzV-u2'}]}</pre>

                <p>HTML код нужно тогда ложить в файл php а в него передавать <code>content</code>. </p>
                <p><code>content</code> Был в формате JSON.</p>


                <h2 class="page-header">Обработка формы на странице</h2>
                <p>Все формы работают по AJAX. Отправка происходит на роутер <code>pages/form</code> естественно в поле
                    имен @school. </p>

                <h2 class="page-header">Обработка действий формы</h2>
                <p>То есть что делать с данными которые пришли с формы.</p>
                <p>Эти данные можно пославть в один из плагинов который связывает с внешними сервисами или сохранить в
                    своем списке</p>
                <p>За указание обработки внешними плагинами отвечает поле <code>action</code> в корне натроек формы. В
                    этом поле в массиве записаны идентификаторы настроек <code>school_out_plugin_settings.id</code></p>
                <p>Если ни одно action не указано то данные добавляются в поток по умолчанию.</p>

                <?= \common\services\documentation\DbTable::widget([
                    'name'    => 'school_out_plugin',
                    'columns' => [
                        [
                            'name'        => 'id',
                            'type'        => 'int',
                            'isRequired'  => true,
                            'description' => 'Идентификатор плагина',
                        ],
                        [
                            'name'        => 'name',
                            'type'        => 'string',
                            'isRequired'  => true,
                            'description' => 'Название внешнего сервиса',
                        ],
                    ],
                ]) ?>

                <?= \common\services\documentation\DbTable::widget([
                    'name'    => 'school_out_plugin_settings',
                    'columns' => [
                        [
                            'name'        => 'id',
                            'type'        => 'int',
                            'isRequired'  => true,
                            'description' => 'Идентификатор настройки',
                        ],
                        [
                            'name'        => 'plugin_id',
                            'type'        => 'int',
                            'isRequired'  => true,
                            'description' => 'Идентификатор плагина',
                        ],
                        [
                            'name'        => 'school_id',
                            'type'        => 'int',
                            'isRequired'  => true,
                            'description' => 'Идентификатор пользователя',
                        ],
                        [
                            'name'        => 'name',
                            'type'        => 'string',
                            'isRequired'  => true,
                            'description' => 'Наименование настройки',
                        ],
                        [
                            'name'        => 'content',
                            'type'        => 'string',
                            'isRequired'  => true,
                            'description' => 'Сохраненные настройки в JSON',
                        ],
                    ],
                ]) ?>

                <h2 class="page-header">Плагины</h2>
                <p><a href="https://highlightjs.org/usage/" target="_blank">MarkUp</a></p>
                <h2 class="page-header">Документация</h2>
                <p><a href="https://tilda.cc/ru/lp/konstructor-saitov/" target="_blank">Тильда</a></p>
                <h2 class="page-header">Конструктор</h2>
                <p>Сами шаблоны для вывода блока содержатся в пути <code>@school/blocks</code> c названиями файлов
                    <code>#####.php</code></p>
                <p>Так как рано или поздно я приду к тому что в режиме редактирования блок надо по другому отображать то
                    и надо задать набор блоков для режима редактирования.</p>
                <p>Сами шаблоны для блока в режиме редактирования содержатся в пути <code>@school/blocks-edit</code> c
                    названиями файлов <code>#####.php</code></p>
                <p>Для редактирования страницы есть Контроллер <code>\avatar\controllers\CabinetSchoolPagesConstructorController</code>.
                    В нем есть функции которые предназначены для:<br>
                    <code>actionBlockAddExecute</code> - добавление блока<br>
                    <code>actionBlockDelete</code> - удаление блока<br>
                    <code>actionBlockAdd</code> - получить список блоков в категории в библиотеке блоков<br>
                    <code>actionBlockSave</code> - Сохраняет данные из формы настройки в поле <code>content</code>.
                </p>

                <p>На кнопке для редактирования должна быть указан атрибут data-id и туда поместить тип объекта.</p>


                <h2 class="page-header">Редактирование блока</h2>
                <pre>$('#modalEdit{$blockContent->type_id} .buttonSave').click(function(e) {
    var type = $(this).data('type_id');
    var modal = $('#modalEdit'+type);
    var id = modal.data('id');
    ajaxJson({
        url: '/cabinet-school-pages-constructor/block-save',
        data: {
            id: id,
            form: $('#form_{$blockContent->type_id}').serializeArray()
        },
        success: function(ret) {
            window.location.reload();
        }
    });
});

$('.buttonSettings[data-type="24"]').click(function(e) {
    // Идентификатор блока контента
    var id = $(this).data('id');
    var type = $(this).data('type');
    var modal = $('#modalEdit'+type);
    modal.data('id', id);
    ajaxJson({
        url: '/cabinet-school-pages-constructor/block-get',
        data: {
            id: id
        },
        success: function(ret) {
            modal.find('.field_{$blockContent->type_id}_rows').val(ret.rows);
            $('#modalEdit' + type).modal();
        }
    });
});
</pre>

                <p>Это классический код редактирования блока. Видно что можно оптимизировать и оставить только
                    расстановку полей в форму после чтения.</p>
                <p>Хотя еще и получение полей для сохранения, еще разнится. Тогда две функции: 1. сбор полей для
                    сохранения. 2. Расстановка полей после чтения.</p>
                <p>В итоге вышла такая функция</p>
                <pre>/**
*
* @param type_id int идентификатор типа блока
* @param functionSetFields function функция расстановка полей после чтения в форму
* @param functionGetFields function функция для сбора полей для сохранения из формы
*/
function blockSettingsInit(type_id, functionSetFields, functionGetFields) {

    var selector = '#modalEdit' + type_id + ' .buttonSave';

    $(selector).click(function(e) {
        var type = $(this).data('type_id');
        var modal = $('#modalEdit' + type);
        var id = modal.data('id');
        ajaxJson({
            url: '/cabinet-school-pages-constructor/block-save',
            data: {
                id: id,
                form: functionGetFields(type_id)
            },
            success: function(ret) {
                window.location.reload();
            }
        });
    });

    selector = '.buttonSettings[data-type="' + type_id + '"]';
    $(selector).click(function(e) {
        // Идентификатор блока контента
        var id = $(this).data('id');
        var type = $(this).data('type');
        var modal = $('#modalEdit' + type);
        modal.data('id', id);
        ajaxJson({
            url: '/cabinet-school-pages-constructor/block-get',
            data: {
                id: id
            },
            success: function(ret) {
                functionSetFields(modal, type, ret);
                $('#modalEdit' + type).modal();
            }
        });
    });

}</pre>
                <p>Пример вызова для инициации</p>
                <pre>blockSettingsInit(
    24,
    function(modal, type_id, ret) {
        modal.find('#field_{$blockContent->type_id}_rows').val(ret.rows);
    },
    function(modal, type_id) {
        return {
            rows: $('#form_'+type_id).find('input[name="rows"]').val()
        };
    }
);</pre>
                <p>При этом нужно подключить Asset <code>\common\assets\PageConstrictor\Asset::register($this);</code>
                </p>


                <h2 class="page-header">Редактирование текста в странице</h2>

                <p><a href="https://www.jqueryscript.net/text/Edit-Element-In-Place-jQuery-Editable.html"
                      target="_blank">https://www.jqueryscript.net/text/Edit-Element-In-Place-jQuery-Editable.html</a>
                </p>
                <p>
                    <a href="https://www.jqueryscript.net/text/Medium-Style-jQuery-Html5-WYSIWYG-Inline-Editor-Penplate.html"
                       target="_blank">https://www.jqueryscript.net/text/Medium-Style-jQuery-Html5-WYSIWYG-Inline-Editor-Penplate.html</a>
                </p>
                <p>
                    <a href="https://www.jqueryscript.net/text/Full-featured-jQuery-WYSIWYG-Editor-Plugin-wysiwyg-js.html"
                       target="_blank">https://www.jqueryscript.net/text/Full-featured-jQuery-WYSIWYG-Editor-Plugin-wysiwyg-js.html</a>
                </p>

                <h2 class="page-header">Блок 6</h2>
                <p>Возможности</p>
                <p>Редактировать текст в блоках можно прямо на странице. Удалить блок и добавить можно допустим в
                    настройках. Этого достаточно для работы с этим блоком? да. Делаю.</p>

                <h2 class="page-header">Исследование</h2>
                <p>При сохранении картинок и выводе ее на другом сайте надо запоминать путь. в UploadFile3 предусмотрено
                    сохранение только от корня, значит нужен другой тип файла.</p>

                <h2 class="page-header">Буфер</h2>
                <p><code>/cabinet-school-pages-constructor/block-copy</code><br><code>/cabinet-school-pages-constructor/block-paste</code></p>

                <p>Копирование производится в переменную сессии <code>clipboard</code> самого блока контента <code>content</code> и его типа <code>type_id</code></p>


                <h3 class="page-header">Статистика</h3>

                <?= \common\services\documentation\DbTable::widget([
                    'name'        => 'school_page_stat',
                    'description' => 'Статистика заходов на страницу',
                    'model'       => '\common\models\school\PageStat',
                    'columns'     => [
                        [
                            'name'        => 'id',
                            'type'        => 'int',
                            'isRequired'  => true,
                            'description' => 'Идентификатор школы',
                        ],
                        [
                            'name'        => 'page_id',
                            'type'        => 'int',
                            'isRequired'  => true,
                            'description' => 'Идентификатор страницы',
                        ],
                        [
                            'name'        => 'is_bot',
                            'type'        => 'int',
                            'isRequired'  => true,
                            'description' => 'Флаг. 0 - не бот, человек, 1 - бот, определяется здесь \common\models\school\Page::isBot()',
                        ],
                        [
                            'name'        => 'created_at',
                            'type'        => 'int',
                            'isRequired'  => true,
                            'description' => 'Момент захода',
                        ],
                        [
                            'name'        => 'browser',
                            'type'        => 'varchar(100)',
                            'isRequired'  => false,
                            'description' => 'Браузер',
                        ],
                        [
                            'name'        => 'ip',
                            'type'        => 'varchar(15)',
                            'isRequired'  => false,
                            'description' => 'IP клиента',
                        ],
                        [
                            'name'        => 'country',
                            'type'        => 'varchar(2)',
                            'isRequired'  => false,
                            'description' => 'Страна клиента',
                        ],
                    ],
                ]) ?>

                <p>Бот определяется по значению <code>\Yii::$app->request->userAgent</code>. Список совпадений находится в <code>\common\models\school\Page::isBot()</code></p>


                <h2 class="page-header">Валидацию URL</h2>
                <p>Все url страниц в БД задаются с начального слеша /.</p>
                <p>При добавлении нужно проверить чтобы он не совпадал с предыдущими чтобы он был уникальным и чтобы не совпадал с зарезервированными. Это делается функцией <code>validateUrl</code> в экземлляре класса <code>\common\models\school\School</code></p>
                <p>Зарезервированные url хранятся в переменной <code>\common\models\school\School::$reservedUrl</code></p>

            </div>
        </div>


    </div>
</div>

