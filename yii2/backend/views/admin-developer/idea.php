<?php

/** $this \yii\web\View  */

use yii\helpers\Url;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;

$this->title = 'Идеи';


?>

<div class="container">
    <div class="col-lg-12">
        <h1 class="page-header"><?= Html::encode($this->title) ?></h1>

        <p>Нужно чтобы записывать идеи от пользователей системы.</p>

        <?= \common\services\documentation\DbTable::widget([
            'name'        => 'user_idea',
            'description' => 'Идея',
            'columns'     => [
                [
                    'name'        => 'id',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Идентификатор записи',
                ],
                [
                    'name'        => 'user_id',
                    'type'        => 'integer',
                    'isRequired'  => true,
                    'description' => 'Кому принадлежит настройка',
                ],
                [
                    'name'        => 'created_at',
                    'type'        => 'integer',
                    'isRequired'  => true,
                    'description' => 'Кому принадлежит настройка',
                ],
                [
                    'name'        => 'name',
                    'type'        => 'varchar(255)',
                    'isRequired'  => true,
                    'description' => 'Заголовок',
                ],
                [
                    'name'        => 'content',
                    'type'        => 'text',
                    'isRequired'  => true,
                    'description' => 'Содержимое',
                ],
            ],
        ]) ?>

        <p>Появились левые, спамерские идеи которые надо скрыть. Значит надо ввести поле "скрыть идею" <code>is_hide</code>.</p>



    </div>
</div>

