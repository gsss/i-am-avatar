<?php

/** $this \yii\web\View  */

use yii\helpers\Url;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;

$this->title = 'КИИС Прометей';


?>

<div class="container">
    <div class="col-lg-12">
        <h1 class="page-header"><?= Html::encode($this->title) ?></h1>

        <p><a href="https://docs.google.com/document/d/1N3Ul4E989f-wjzSADzSnFS1AIvqspwQH-A4PZNEGuMQ/edit"
              target="_blank">Первый документ</a></p>
        <p><a href="https://yadi.sk/i/Zynt5iu2LomL5Q" target="_blank">https://yadi.sk/i/Zynt5iu2LomL5Q</a></p>

        <p><img class="thumbnail" src="/images/controller/admin-developer/prometey/in.png" width="100%"></p>
        <h2 class="page-header">Процедура вступления в потребительский кооператив «ПРОМЕТЕЙ»</h2>
        <p>Процедура вступления в потребительский кооператив «ПРОМЕТЕЙ»: <a
                    href="https://cloud1.iampau.pro/upload/cloud/15846/59353_WiRXudD2kr.pdf" target="_blank">https://cloud1.iampau.pro/upload/cloud/15846/59353_WiRXudD2kr.pdf</a>
        <p><img class="thumbnail" src="/images/controller/admin-developer/prometey/2020-03-20_02-12-48.png"
                width="100%"></p>


        <?= \common\services\documentation\DbTable::widget([
            'name'        => 'user_enter',
            'description' => 'Файл',
            'model'       => '\common\models\UserEnter',
            'columns'     => [
                [
                    'name'        => 'id',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Идентификатор записи',
                ],
                [
                    'name'        => 'email',
                    'type'        => 'string',
                    'isRequired'  => true,
                    'description' => 'email',
                ],
                [
                    'name'        => 'iof',
                    'type'        => 'string',
                    'isRequired'  => true,
                    'description' => 'ИОФ',
                ],
                [
                    'name'        => 'passport',
                    'type'        => 'string',
                    'isRequired'  => true,
                    'description' => 'Паспорт',
                ],
                [
                    'name'        => 'place',
                    'type'        => 'string',
                    'isRequired'  => true,
                    'description' => 'Проживающего(ей)',
                ],
                [
                    'name'        => 'address',
                    'type'        => 'string',
                    'isRequired'  => true,
                    'description' => 'Адрес для корреспонденции',
                ],
                [
                    'name'        => 'phone',
                    'type'        => 'string',
                    'isRequired'  => true,
                    'description' => 'Телефон GSM',
                ],
                [
                    'name'        => 'whatsapp',
                    'type'        => 'string',
                    'isRequired'  => true,
                    'description' => 'Телефон для whatsapp',
                ],
                [
                    'name'        => 'code',
                    'type'        => 'string',
                    'isRequired'  => true,
                    'description' => 'Код для ссылке в почте',
                ],
                [
                    'name'        => 'status',
                    'type'        => 'integer',
                    'isRequired'  => false,
                    'description' => 'Статус 0 - создана, 1 - утверждена, 2 - отвергнута',
                ],
                [
                    'name'        => 'created_at',
                    'type'        => 'integer',
                    'isRequired'  => false,
                    'description' => 'Время добавления записи',
                ],

            ],
        ]) ?>

        <?= \common\services\documentation\DbTable::widget([
            'name'        => 'user_enter_sign',
            'description' => 'Подписанный документ',
            'model'       => '\common\models\UserEnterSign',
            'columns'     => [
                [
                    'name'        => 'id',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Идентификатор записи',
                ],
                [
                    'name'        => 'user_id',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Идентификатор пользователя',
                ],
                [
                    'name'        => 'url',
                    'type'        => 'string',
                    'isRequired'  => true,
                    'description' => 'url',
                ],
                [
                    'name'        => 'status',
                    'type'        => 'integer',
                    'isRequired'  => true,
                    'description' => 'Статус: 0 - создана, 1 - утверждено, 2 - отвергнуто',
                ],
                [
                    'name'        => 'created_at',
                    'type'        => 'integer',
                    'isRequired'  => false,
                    'description' => 'Время добавления записи',
                ],

            ],
        ]) ?>

        <p>Заявление: <a
                href="https://cloud1.iampau.pro/upload/cloud/15847/05710_nec1CYFgl3.docx" target="_blank">https://cloud1.iampau.pro/upload/cloud/15847/05710_nec1CYFgl3.docx</a>
        </p>

        <p>Роль <code>role_pravoved</code> обозначает что правовед имеет список заявок. После утверждения заявки пользователь становится <code>role_kooperate</code>.</p>


    </div>
</div>


