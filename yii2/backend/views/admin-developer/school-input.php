<?php

/** $this \yii\web\View  */

use yii\helpers\Url;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;

$this->title = 'Школа. Плагины для входа данных';


?>

<div class="container">
    <div class="col-lg-12">
        <h1 class="page-header"><?= Html::encode($this->title) ?></h1>



        <h3 class="page-header">Тильда</h3>


        <?= \common\services\documentation\DbTable::widget([
            'name'        => 'school_potok_in_tilda',
            'description' => 'Валюта',
            'columns'     => [
                [
                    'name'        => 'id',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Идентификатор записи',
                ],
                [
                    'name'        => 'user_id',
                    'type'        => 'integer',
                    'isRequired'  => true,
                    'description' => 'Кому принадлежит настройка',
                ],
                [
                    'name'        => 'potok_id',
                    'type'        => 'integer',
                    'isRequired'  => true,
                    'description' => 'Идентификатор потока',
                ],
                [
                    'name'        => 'name',
                    'type'        => 'varchar(20)',
                    'isRequired'  => true,
                    'description' => 'Кодовое имя формы',
                ],
            ],
        ]) ?>

    </div>
</div>

