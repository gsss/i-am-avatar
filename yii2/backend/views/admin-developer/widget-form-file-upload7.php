<?php

/** $this \yii\web\View  */

use yii\helpers\Url;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;

$this->title = 'FileUpload7 - загружает картинку';


?>

<div class="container">
    <div class="col-lg-12">
        <h1 class="page-header"><?= Html::encode($this->title) ?></h1>


        <h2 class="page-header">Виджеты для загрузки FileUpload7</h2>
        <p>Строится на основе FileUpload6. Предназначен для загрузки файлов картинок.</p>
        <p>Есть возможность обрезать до нужного размера и наложить водяной знак.</p>
        <p>При установке водяного знака используются следующие параметры:</p>
        <?= \avatar\services\Params::widget([
            'params' => [
                [
                    'name'        => 'file',
                    'isRequired'  => true,
                    'description' => 'Путь к файлу на сервере относительный или полный.',
                ],
                [
                    'name'        => 'index',
                    'description' => 'Латинская буквица - суфикс для имени файла, чтобы различать разные типы файлов',
                ],
                [
                    'name'        => 'update',
                    'description' => 'Массив Обработка оригинала файла',
                ],
            ],
        ]) ?>

        <p>Объект массива <code>update</code></p>

        <?= \avatar\services\Params::widget([
            'params' => [
                [
                    'name'        => 'function',
                    'description' => 'Функция которую надо применить к файлу',
                ],
                [
                    'name'        => 'options',
                    'description' => 'options',
                ],
            ],
        ]) ?>

        <p>Функция <code>\common\widgets\FileUpload7\FileUpload::getFile($index)</code> Выдает имя файла в зависимости
            от инлекса</p>
        <p>Например если имя файла в параметре value = https://cloud1.i-am-avatar.com/15523/12918_dsffesasd.jpg,
            а параметр $index = 'crop',
            то выдано будет https://cloud1.i-am-avatar.com/15523/12918_dsffesasd_crop.jpg</p>
        <p>То есть по сути идет лишь добавление к конец файла суфикса со знаком подчеркивания</p>


        <h3 class="page-header">Исследование</h3>
        <p>Какие функции могут быть? 1. Обрезка, 2. Маркировка. Все? да. Хорошо. Что далее? Надо определить к чему они
            будут применяться.</p>
        <p>Например мне нужно промаркировать оригинал и обрезать оригинал</p>
        <p>Например мне нужно промаркировать оригинал и обрезать промаркированный файл</p>
        <p>Например мне нужно обрезать оригинал</p>
        <p>Например мне нужно обрезать оригинал и промаркировать обрезок.</p>
        <p>Вот для каждого из четырех типов как будут задаваться параметры? А нужны ли эти варианты вообще. Рассмотрю. 1
            точно нужен. 2 не особо, 3. нужен, 4 возможно.</p>
        <p>Хорошо вобщем можно считать что все нужны. Как для таких типов задавать параметры?</p>
        <p><img src="/images/controller/admin-developer/school-cloud/IMAG0116.jpg" class="thumbnail"></p>
        <p>Какие типы функций возможны? copy - копирование, crop - обрезка, mark - нанесение маркировки.</p>
        <p>Для первого вида такой вид задания параметров:</p>
        <pre>\common\widgets\FileUpload7\FileUpload::widget([
    'options' => [
        'update' => [
            [
                'function' => 'copy',
                'index'    => 'c',
            ],
            [
                'function' => 'mark',
                'index'    => 'mark',
                'options'  => [
                    'file'       => '@avatar/file.png',
                    'position'   => 'bottom_right',
                    'horizontal' => '10%',
                    'vertical'   => '10%',
                    'width'      => '20%',
                    'height'     => '15%',
                ],
            ],
            [
                'function' => 'crop',
                'index'    => 'crop',
                'options'  => [
                    'width'  => '200',
                    'height' => '200',
                ],
            ],
        ],
    ],
]);</pre>

        <p>Для второго вида, следующий вид:</p>
        <pre>\common\widgets\FileUpload7\FileUpload::widget([
    'options' => [
        'update' => [
            [
                'function' => 'copy',
                'index'    => 'c',
                'update'   => [
                    [
                        'function' => 'mark',
                        'index'    => 'mark',
                        'options'  => [
                            'file'       => '@avatar/file.png',
                            'position'   => 'bottom_right',
                            'horizontal' => '10%',
                            'vertical'   => '10%',
                            'width'      => '20%',
                            'height'     => '15%',
                        ],
                        'update'   => [
                            [
                                'function' => 'crop',
                                'index'    => 'crop',
                                'options'  => [
                                    'width'  => '200',
                                    'height' => '200',
                                ],
                            ],
                        ],
                    ],
                ],
            ],
        ],
    ],
]);</pre>

        <h2 class="page-header">Виджеты для загрузки</h2>
        <p>Первый сервер-облако: <code>https://cloud1.i-am-avatar.com</code></p>

        <h3 class="page-header">Сервис для загрузки файлов и его ПО</h3>
        <p>GIT: <code>https://Ra-m-ha@bitbucket.org/Ra-m-ha/avatar-cloud.git</code></p>
        <p>Нужен конечно же интерфейс на облако, который будет обслуживать все это.</p>
        <p>Функции:<br>
            - Проверка кол-ва свободного места<br>
            - Удаление файла<br>
            - Загрузка файла FileUpload7<br>
            - Загрузка файла FileUpload8<br>
            - Загрузка файла HtmlContent
        </p>
        <p>Для того чтобы загружать файлы нужно передать ключ доступа, он передается методом POST под именем key. Если
            ключ
            не будет передан то будет вызвана ошибка 400. На каждый сервер облако свои ключи доступа.</p>
        <p>Контроллер <code>stat</code> взят с <code>sirius-aton-php</code>.</p>

        <h3 class="page-header">Функция: Запрос остатка места</h3>
        <p><code>/service/get-free-space</code></p>
        <p>Без параметров.</p>

        <p>Ответ: 200 JSON <code>{"result":true,"data":{"free":int}}</code></p>
        <p>data/free - int - кол-во оставшихся байтов для загрузки</p>


        <h3 class="page-header">Виджеты для загрузки</h3>
        <p>Как будет определяться на какое облако закачивать и когда будет проверка оставшегося места?</p>
        <p>Не понятно, ну например можно раз в день проверять и если ниже лимита то использовать другой.</p>
        <p>Второй вариант - стратегия: использовать рандомный сервер на подключение.</p>
        <p>Нужен мониторинг остатка места на облаках.</p>
        <p>Внутри есть функция FileUpload7 которая выдает сервер для загрузки, допустим <code>getServer()</code></p>

        <p>На каком уровне будет происходить обработка файлов? Обрезка и наложение? Я думаю на стороне облака. Так как я
            не смогу обработанный файл загрузить. Хотя нет смогу но Загружать я изначально буду на облако и там значит
            надо дальнейшую обработку производить.</p>
        <p>Вот и ответ готовый. Хорошо, а что далее?</p>
        <p>Далее надо запрограммировать на сервере обрезку и маркировку. И еще конечно же переброску параметров
            обработки.</p>
        <p>А еще хочется сделать указание типа файла в таблице <code>school_file</code></p>
        <p>Остаток места на сервере сейчас не актуален</p>




        <h3 class="page-header">SIZE</h3>
        <p>Если передаются индексы то не считается файлы обрезков. Только размер главного файла</p>


        <h3 class="page-header">Возврат данных</h3>
        <pre>{"success":true,"file":"12876_FWhToZpKN7.jpg","url":"https:\/\/cloud1.i-am-avatar.com\/upload\/cloud\/15575\/12876_FWhToZpKN7.jpg","size":554731,"update":{"crop":{"url":"https:\/\/cloud1.i-am-avatar.com\/upload\/cloud\/15575\/12876_FWhToZpKN7_crop.jpg","size":12458}}}</pre>

        <h3 class="page-header">AJAX</h3>
        <p>AJAX вызывается после каждой загрузки и записывает какой файл был загружен в таблицу <code>school_file</code>.</p>
        <p><code>/cabinet/file-upload7-save</code></p>
        <?= \avatar\services\Params::widget([
            'params' => [
                [
                    'name'        => 'file',
                    'type'        => 'string',
                    'isRequired'  => true,
                    'description' => 'полный путь к файлу с доменом и без протокола',
                ],
                [
                    'name'        => 'school_id',
                    'type'        => 'integer',
                    'isRequired'  => true,
                    'description' => 'Идентификатор школы',
                ],
                [
                    'name'        => 'type_id',
                    'type'        => 'integer',
                    'isRequired'  => true,
                    'description' => 'Идентификатор типа файла',
                ],
                [
                    'name'        => 'size',
                    'type'        => 'integer',
                    'isRequired'  => true,
                    'description' => 'Размер файла в байтах',
                ],
            ],
        ]) ?>

        <p><a href="https://www.i-am-avatar.com/cabinet-task-list/view?id=362" target="_blank">Задача</a></p>

        <h3 class="page-header">Удаление</h3>
        <p>POST запрос на облако <code>/upload/file-upload7-delete</code></p>
        <?= \avatar\services\Params::widget([
            'params' => [
                [
                    'name'        => 'file',
                    'type'        => 'string',
                    'isRequired'  => true,
                    'description' => 'полный путь к файлу с доменом и с протоколом',
                ],
                [
                    'name'        => 'signature',
                    'type'        => 'string',
                    'isRequired'  => true,
                    'description' => 'Подпись котоая вычисляется от ' . Html::tag('code', 'Yii::$app->params["file-upload7-key"]'),
                ],
                [
                    'name'        => 'indexList',
                    'type'        => 'array',
                    'isRequired'  => false,
                    'description' => 'массив индексов которые так же надо удалить',
                ],
            ],


        ]) ?>

        <pre>добавлю передачу параметра signature
которая вычисляется из \common\models\UserAvatar::hashPassword($key)
$key - это ключ для облака
и чтобы в открытую его не передавать можно его зашифровывать
а проверять через функцию password_verify($key, $signature) </pre>

        <h3 class="page-header">Виды формирования предпросмотра</h3>
        <p><code>MODE_THUMBNAIL_CUT</code> - обрезать</p>
        <p><code>MODE_THUMBNAIL_FIELDS</code> - вписать</p>
        <p><code>MODE_THUMBNAIL_WHITE</code> - вписать на фоне</p>
        <p>
            <a href="https://www.draw.io/#G14Yu4i8qXk8zb_mTtRFfcwqo0bg-FMg1K" target="_blank">
                <img src="/images/controller/admin-developer/widget-form-file-upload7/s.png">
            </a>
        </p>
    </div>
</div>

