<?php

/** $this \yii\web\View  */

use yii\helpers\Url;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;

$this->title = 'События';


?>

<div class="container">
    <div class="col-lg-12">
        <h1 class="page-header"><?= Html::encode($this->title) ?></h1>

        <h2 class="page-header">Таблица</h2>
        <p>Надо проанализировать какие варианты возможны</p>
        <?php
        $events = [
            [
                'title'    => 'Вебинар Безоплатно',
                'isPaid'   => 0, // Безоплатно
                'isDate'   => true, // Точная дата?
                'isOnline' => true, // Online?
                'count'    => 1, // Кол-во уроков
                'enter'    => 1, // Кол-во уроков
                'comment'  => 'Вебинар Безоплатно',
            ],
            [
                'title'    => 'Вебинар безоплатно + платно',
                'isPaid'   => 1, // безоплатно + платно
                'isDate'   => true, // Точная дата?
                'isOnline' => true, // Online?
                'count'    => 1, // Кол-во уроков
                'comment'  => 'Вебинар безоплатно + платно',
            ],
            [
                'title'    => 'Вебинар только платно',
                'isPaid'   => 1, // только платно
                'isDate'   => true, // Точная дата?
                'isOnline' => true, // Online?
                'count'    => 1, // Кол-во уроков
            ],
            [
                'title'    => 'Вебинар',
                'isPaid'   => 1, // только платно
                'isDate'   => true, // Точная дата?
                'count'    => 3, // Кол-во уроков
                'isOnline' => true, // Online?
                'comment'  => 'Как в таком случае задается дата каждого урока? Опитывается на странице и делаются рассылки',
            ],
            [
                'title'    => 'Офлайн урок',
                'isPaid'   => 0, // Безоплатно
                'isDate'   => true, // Точная дата?
                'isOnline' => false, // Online?
                'count'    => 1, // Кол-во уроков
            ],
            [
                'title'    => 'Офлайн урок',
                'isPaid'   => 0, // Безоплатно
                'isDate'   => true, // Точная дата?
                'isOnline' => false, // Online?
                'count'    => 1, // Кол-во уроков
                'comment'  => 'Офлайн уроки только с фиксированной датой',
            ],
            [
                'title'    => 'Курс',
                'isPaid'   => 1,
                'isDate'   => false, // Точная дата?
                'count'    => 3, // Кол-во уроков
                'isOnline' => true, // Online?
                'comment'  => 'Для курсов без даты могут проходить их самостоятельно, по одному',
            ],

        ];

        ?>


        <?= \yii\grid\GridView::widget([
            'dataProvider' => new \yii\data\ArrayDataProvider([
                'allModels' => $events,
            ]),
        ]) ?>

        <p><img src="/images/controller/admin-developer/school-events/2019-10-30_22-32-47.png"></p>

        <p>Вот актуальный момент. Как понять что группа набрана и начинается курс? Для курса без даты. Этот курс будет
            открываться после оплаты или записи на него.</p>
        <p>Шаги заполнения</p>

        <ul>
            <li>Контент</li>
            <ul>
                <li>Название</li>
                <li>Описание + краткое описание</li>
                <li>Картинка</li>
            </ul>
            <li>Дата проведения</li>
            <li>Место проведения</li>
            <ul>
                <li>онлайн</li>
                <li>оффлайн</li>
            </ul>
            <li>Кол-во уроков</li>
            <li>Кто ведет</li>
            <ul>
                <li>Задать не зареганного</li>
                <li>Указать из своих</li>
            </ul>
            <li>Информация об организаторе</li>
            <li>Анкета регистрации</li>
            <li>Билеты, Оплата</li>
            <li>Страница</li>
            <ul>
                <li>Оформление</li>
                <li>Ссылка</li>
                <li>Данные для соц сети</li>
            </ul>
            <li>Публикация</li>
        </ul>


        <h2 class="page-header">USE CASE (опыт применения/проверить)</h2>

        <p>Платформа ЯАватар может быстро вам помочь сделать анонс в интернете о вашем событии.</p>

        <p>Самое главное описать:</p>
        <ul>
            <li>Название мероприятия</li>
            <li>Место проведения</li>
            <li>Описание мероприятия<br>
                - программа<br>
                - что будет на событии
            </li>
            <li>Для кого предназначено</li>
        </ul>


        <h2 class="page-header">Разработка</h2>

        <p>События выглядят в виде Курса/Потока/Страницы.</p>
        <p>Уже подготовлен шаблон мероприятия.</p>
        <p>Ссылка на мероприятие <code>&lt;domain&gt;/events/&lt;page_id&gt;</code></p>
        <p>Возможно организовать продажу билетов.</p>
        <p>Как организована продажа билетов?</p>
        <p>По сути это продажа курса, поэтому в потоке будет кнопка "Заявки", так ну лиды уже есть значит надо продумать
            варианты события</p>
        <p>Например: 1. Безоплатно и 2. платно. Рассмотрим по отдельности</p>

        <h3 class="page-header">Безоплатно</h3>
        <p>Нужны ли в таком случае билеты? Приглашение. Можно по опции сделать.</p>

        <h3 class="page-header">Платно</h3>
        <p>Здесь будут продажа курса в нескольких вариантах.</p>
        <p>Автор заполняет платежные реквизиты если он их еще не ввел.</p>
        <p>Вводит сколько платных вариантов будет. Они записываются в "продажу курса"</p>
        <p>"продажа курса" это какая таблица?</p>
        <p>Вот пока нет или непонятно. Значит надо сделать. Например: <code>school_kurs_sale</code></p>
        <p>Продажа курса идет в потоке, значит должен быть параметр потока.</p>
        <p>Курс может продаваться в нескольких модификациях.</p>

        <?= \common\services\documentation\DbTable::widget([
            'name'        => 'school_kurs_sale',
            'model'       => '\common\models\school\SchoolSale',
            'description' => 'Продажа курса',
            'columns'     => [
                [
                    'name'        => 'id',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Идентификатор записи',
                ],
                [
                    'name'        => 'school_id',
                    'type'        => 'integer',
                    'isRequired'  => true,
                    'description' => 'Идентификатор школы',
                ],
                [
                    'name'        => 'potok_id',
                    'type'        => 'integer',
                    'isRequired'  => true,
                    'description' => 'Идентификатор потока',
                ],
                [
                    'name'        => 'price',
                    'type'        => 'biginteger',
                    'isRequired'  => true,
                    'description' => 'Цена курса в атомах',
                ],
                [
                    'name'        => 'currency_id',
                    'type'        => 'integer',
                    'isRequired'  => true,
                    'description' => 'Валюта внешняя db.currency.id',
                ],
                [
                    'name'        => 'name',
                    'type'        => 'varchar(255)',
                    'isRequired'  => true,
                    'description' => 'Название модификации продажи курса',
                ],
            ],
        ]) ?>

        <pre>CREATE TABLE `school_kurs_sale` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `school_id` int(11) DEFAULT NULL,
  `potok_id` int(11) DEFAULT NULL,
  `currency_id` int(11) DEFAULT NULL,
  `price` BIGINT DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;</pre>

        <p>А где в интерфейсе указывать продажи? В потоке</p>

        <h2 class="page-header">Заявки на покупку курса</h2>
        <p>Форму заказа можно сделать очень просто, потому что не нужно ничего доставлять</p>
        <p>Какие параметры нужны для оформления заявки?</p>
        <p>Классика: имя телефон и почта и сразу оплата.</p>
        <p>Хорошо по какой ссылке будет покупка курса? Например <code>/kurs/order?id=1</code>. id - это
            school_kurs_sale.id</p>
        <p>А если я хочу выставить не все платежные системы? то значит нужно сделать настройку. Но пока по умолчанию все
            выводятся.</p>
        <?= \common\services\documentation\DbTable::widget([
            'name'        => 'school_kurs_sale_request',
            'model'       => '\common\models\school\SchoolSaleRequest',
            'description' => 'Заявка на продажу курса',
            'columns'     => [
                [
                    'name'        => 'id',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Идентификатор записи',
                ],
                [
                    'name'        => 'school_id',
                    'type'        => 'integer',
                    'isRequired'  => true,
                    'description' => 'Идентификатор школы school.id',
                ],
                [
                    'name'        => 'sale_id',
                    'type'        => 'integer',
                    'isRequired'  => true,
                    'description' => 'Идентификатор продажи school_kurs_sale.id',
                ],
                [
                    'name'        => 'lid_id',
                    'type'        => 'integer',
                    'isRequired'  => true,
                    'description' => 'Идентификатор school_potok_user_root_link.id',
                ],
                [
                    'name'        => 'ps_config_id',
                    'type'        => 'integer',
                    'isRequired'  => true,
                    'description' => 'Идентификатор paysystems_config.id',
                ],
                [
                    'name'        => 'billing_id',
                    'type'        => 'integer',
                    'isRequired'  => true,
                    'description' => 'Идентификатор счета billing.id',
                ],
                [
                    'name'        => 'is_paid',
                    'type'        => 'integer',
                    'isRequired'  => true,
                    'description' => 'Флаг. Оплачено по подтверждению магазина 0 - нет, 1 - оплачено',
                ],
                [
                    'name'        => 'is_paid_client',
                    'type'        => 'integer',
                    'isRequired'  => true,
                    'description' => 'Флаг. Оплачено по подтверждению клиента 0 - нет, 1 - оплачено',
                ],
                [
                    'name'        => 'created_at',
                    'type'        => 'integer',
                    'isRequired'  => true,
                    'description' => 'Время создания заявки',
                ],
            ],
        ]) ?>

        <pre>CREATE TABLE `school_kurs_sale_request` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `sale_id` int(11) DEFAULT NULL,
  `lid_id` int(11) DEFAULT NULL,
  `ps_config_id` int(11) DEFAULT NULL,
  `created_at` int(11) DEFAULT NULL,
  `school_id` int(11) DEFAULT NULL,
  `is_paid` tinyint DEFAULT 0,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;</pre>

        <p>
            <a href="https://www.draw.io/#G1g5WSzUlq2Y2DpzRxSfY5YUVS7jsk6TkQ" target="_blank">
                <img src="/images/controller/admin-developer/school-events/sale_request.png" class="thumbnail">
            </a>
        </p>

        <h2 class="page-header">Интерфейс</h2>
        <p>Где отображать заявка на покупку курса? В потоке. По сути если курс платный то каждый лид это будет
            заявка.</p>
        <p>А может ли быть ситуация что есть безоплатное участие, а есть платное? Да конечно. И как в таком случае
            поступать?</p>
        <p>Будет обычное оформление лида и платные.</p>

        <h2 class="page-header">Платежные системы</h2>
        <p>Какие платежные системы выдавать при продаже курса? Те которые соответствуют <code>$sale->currency_id =>
                paysystems.currency == $currency->code</code></p>

        <h2 class="page-header">Оплата</h2>
        <p>После выбора платежной системы клиент переходит к оплате на страницу <code>/kurs/pay?id=1</code> где id - это
            идентификатор заявки school_kurs_sale_request.id</p>

        <h2 class="page-header">Модель поведения автора</h2>
        <p>Заходит в школу</p>
        <p>Нажимает кнопку "создать событие"</p>
        <p>Заполняет бриф</p>
        <p>Нажимает "создать"</p>
        <p>Получает ссылку на страницу.</p>

        <h2 class="page-header">Варианты событий</h2>
        <p>
            <a href="https://docs.google.com/spreadsheets/d/1kCpp1FwVvX2_QuS9wAVHFLS7R7sMt_4dH_zZFn40cxM/edit?usp=sharing"
               target="_blank">
                Варианты событий
            </a>
        </p>


        <h2 class="page-header">Билеты</h2>
        <p>Ссылка отсылаемая пользователю: <code>site/event-room?id=12121</code>.</p>


        <h2 class="page-header">Билеты</h2>
        <p><a href="/images/controller/admin-developer/school-events/ticket_2488447913091495.pdf" target="_blank">ticket_2488447913091495.pdf</a>
        </p>
        <p><a href="https://ticketforevent.com/ru/biletnyiy-servis-dlya-organizatorov-meropriyatiy/#pricing"
              target="_blank">ticketforevent.com</a></p>

        <h2 class="page-header">Цикл движения клиента в событии</h2>
        <p>
            <a href="https://www.draw.io/#G1jEXy3OnelM_5oN48tV8UJzQ5vjJ5vwcZ">
                <img src="/images/controller/admin-developer/school-events/cicle.png">
            </a>
        </p>

        <h2 class="page-header">Универсальная страница анонса события для клиентов</h2>
        <p><code>/event?id=123</code> где <code>id</code> - идентификатор потока.</p>
        <p>Контроллер: <code>\school\controllers\EventController</code></p>
        <p>Роутер: <code>event/item</code></p>
        <p>Данные с события записывается в таблицу <code>school_event</code></p>
        <?= \common\services\documentation\DbTable::widget([
            'name'        => 'school_event',
            'model'       => '\common\models\school\Event',
            'description' => 'Конфиг после создания события',
            'columns'     => [
                [
                    'name'        => 'id',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Идентификатор записи',
                ],
                [
                    'name'        => 'school_id',
                    'type'        => 'integer',
                    'isRequired'  => true,
                    'description' => 'Идентификатор школы school.id',
                ],
                [
                    'name'        => 'potok_id',
                    'type'        => 'integer',
                    'isRequired'  => true,
                    'description' => 'Идентификатор потока school_potok.id',
                ],
                [
                    'name'        => 'config',
                    'type'        => 'string',
                    'isRequired'  => true,
                    'description' => 'Конфиг после создания события, JSON',
                ],
                [
                    'name'        => 'created_at',
                    'type'        => 'integer',
                    'isRequired'  => true,
                    'description' => 'Время создания заявки',
                ],
            ],
        ]) ?>
        <p>Примерное содержание поля <code>config</code>:</p>
        <pre>{"1":{"name":"12","content_short":"","content":"","image":""},"2":{"date_start":"01.11.2019","date_end":""},"3":{"is_online":"0","place":"123","online_url":""},"4":{"masters":"9"},"5":{"fields":"[]"},"6":{"vebinar_is_password":"0","vebinar_password":"","vebinar_max":"","vebinar_both_tickets":"[]","vebinar_buy_tickets":"[]","vebinar_tab":"1","offline_max":"","offline_both_tickets":"[]","offline_buy_tickets":"[]","offline_tab":"1"},"7":{"url":"ddd"}}</pre>




        <h2 class="page-header">Страница входа в вебинарную комнату (платно)</h2>
        <p><code>/event-room?id=11</code> ссылка вебинарную комнату. Если клиент не авторизован, то клиента переправляют на авторизацию.</p>
        <p>а стоит ли авторизовывать пользователя? Давай рассмотрим вариант не авторизовывать, тогда что? Если клиент вывалился то как он будет заходить?</p>
        <p>Так сейчас какой варинат я рассматриваю?</p>
        <p>Оплаченный вариант. Вход по ссылке. <code>/event-room?id=11&hash=234211321234234243</code>. Где сохранять хеши? Например <code>school_potok_hash</code></p>

        <?= \common\services\documentation\DbTable::widget([
            'name'        => 'school_potok_hash',
            'model'       => '\common\models\school\PotokHash',
            'description' => 'Хеши для потока платного',
            'columns'     => [
                [
                    'name'        => 'id',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Идентификатор записи',
                ],
                [
                    'name'        => 'potok_id',
                    'type'        => 'integer',
                    'isRequired'  => true,
                    'description' => 'Идентификатор потока school_potok.id',
                ],
                [
                    'name'        => 'hash',
                    'type'        => 'string',
                    'isRequired'  => true,
                    'description' => 'Хеш',
                ],
            ],
        ]) ?>
        <p>Страница поделена на два варианта. 1. Вход. 2. После авторизация редирект на <code>/event-room/room</code></p>
        <p>Сделал я страницу и модель <code>\school\models\forms\RoomLogin</code> что далее? Под каким пользователем его авторизовывать? Нужен пользователь все равно. То есть любой пользователь который купил курс, ему создается пользователь со своим паролем.</p>

        <h2 class="page-header">Типы страниц</h2>
        <p>
            <a href="https://www.draw.io/#G1jNi4oE-gGjaB9bfMIveMfg4evdCTW3F7" target="_blank">
                <img src="/images/controller/admin-developer/school-events/types.png" class="thumbnail">
            </a>
        </p>
        <p>Рассылки: сейчас (15-01-7528/2020) нет автоматизированных рассылок. Что это значит? это значит что у нее задано время для рассылки. И я предполагаю чтобы эти рассылки сами генерировались а люди могли бы их отредактировать.</p>

        <p><b>Путь пользователя для онлайн вебинара</b></p>
        <p>
            <a href="https://www.draw.io/#G1TQl9odyaMcBN_M6djEacejCOO7ujDBFm" target="_blank">
                <img src="/images/controller/admin-developer/school-events/path1.png" class="thumbnail">
            </a>
        </p>

        <p><b>Путь пользователя для платного онлайн вебинара</b></p>
        <p>
            <a href="https://www.draw.io/#G10qamGuAiUHLtQ7l8h8B8zskHjA5hKU8E" target="_blank">
                <img src="/images/controller/admin-developer/school-events/path2.png" class="thumbnail">
            </a>
        </p>
        <p>Для платного вебинара рассылка будет вестись по тем кто оплатил. и именно они получат ссылку на трансляцию.</p>

    </div>
</div>

