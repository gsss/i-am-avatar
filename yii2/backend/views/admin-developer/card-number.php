<?php
use common\services\UsersInCache;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;

/* @var $this yii\web\View */

$this->title = 'Номер карты ЯАватар';

?>
<div class="container">
    <div class="col-lg-12">
        <h1 class="page-header text-center"><?= Html::encode($this->title) ?></h1>
        <p>Биткойн кошелек для карты уже задан жестко на карте, а другие кошельки к ней можно привязать самостоятельно</p>
        <p>Префикс номер или БИН кодом является всегда 8888.</p>
        <p>Префикс платежной системы является 8 и обозначает платежную
            систему AvatarPay (Платежная система для свободных людей - Аватаров).</p>
        <p>Последний номер контрольный и вычисляется так:</p>
        <p>Здесь алгоритм вычисления последнего кода.</p>
        <p>Остальные номера указывают на номер счета привязанный к карте.</p>
        <p>16-4-1 = 11</p>

        <h2 class="page-header">Активация карты</h2>
        <p>Ее можно активировать в кабинете нажав на кнопку "Активировать карту".</p>
        <p>Откроется окно "Активировать карту"</p>
        <p>В окне нужно ввести "название счета" и "Номер карты"</p>
        <p>Нажать кнопку "Активировать"</p>



        <h2 class="page-header">Представление в БД</h2>
        <p>Модель: <code>\common\models\Card</code></p>
        <?= \common\services\documentation\DbTable::widget([
            'name'   => 'card',
            'params' => [
                [
                    'name'        => 'id',
                    'isRequired'  => true,
                    'description' => 'Идентификатор карты',
                    'type'        => 'int',
                ],
                [
                    'name'        => 'number',
                    'isRequired'  => true,
                    'description' => 'Номер карты без пробелов',
                    'type'        => 'int',
                ],
                [
                    'name'        => 'design_id',
                    'description' => 'Идентификатор дизайна',
                    'type'        => 'int',
                ],
                [
                    'name'        => 'secret_num',
                    'description' => 'Секретный номер на обратной стороне',
                    'type'        => 'int',
                ],
                [
                    'name'        => 'finish',
                    'description' => 'Дата окончания карты, формат \'MM/YYYY\'',
                    'type'        => 'varchar(10)',
                ],
                [
                    'name'        => 'user_id',
                    'description' => 'Идентификатор пользователя владельца карты',
                    'type'        => 'int',
                ],
                [
                    'name'        => 'myfire_id',
                    'description' => 'Идентификатор Myfire карты',
                    'type'        => 'varchar(8)',
                ],
                [
                    'name'        => 'is_address_work',
                    'description' => '?',
                    'type'        => 'int',
                ],
                [
                    'name'        => 'is_new',
                    'description' => '?',
                    'type'        => 'int',
                ],
                [
                    'name'        => 'is_personal',
                    'description' => 'Флаг Это карта с персональными данными? 0 - нет, это анонимная карта, 1 - да это карта с персональными данными',
                    'type'        => 'int',
                ],
            ]
        ]) ?>

        <p>Модель: <code>\common\models\CardDesign</code></p>
        <?= \common\services\documentation\DbTable::widget([
            'name'   => 'card_design',
            'params' => [
                [
                    'name'        => 'id',
                    'isRequired'  => true,
                    'description' => 'Идентификатор дизайна',
                    'type'        => 'int',
                ],
                [
                    'name'        => 'image',
                    'description' => 'Ссылка на файл дизайна',
                    'type'        => 'varchar(255)',
                ],
            ]
        ]) ?>
        <h3 class="page-header">Модель данных</h3>
        <p>
            <a href="https://drive.google.com/file/d/0BzHYNoEyPNTXOENJN2V3elB2YVk/view?usp=sharing" target="_blank">
                <img
                    src="/images/controller/development-avatar-bank/card-number/card.png"
                    class="thumbnail"
                />
            </a>
        </p>
        <p>
            <a href="https://drive.google.com/file/d/0B9lrcmdS-YbTZEk0YzlLaFVhV00/view?usp=sharing" target="_blank">
                <img
                    src="/images/controller/development-avatar-bank/card-number/schema.png"
                    class="thumbnail"
                />
            </a>
        </p>


        <h3 class="page-header">Интерфейс управления</h3>
        <p class="alert alert-danger">В разработке</p>
        <p>Так как Биткойн кошелек для карты уже задан жестко на карте, то его менять нельзя, а вот другие кошельки к ней можно привязать самостоятельно. Это делается на странице карты.</p>
        <p>Для этого необходимо зайти в карту. Вопрос Как? Вариант один - там где биткйон счет будет переход в карту + там где привязанный счет то тоже можно перейти на карту.</p>
        <p>Там выбрать привязу счета к карте.</p>
        <p>Там где карта будет ее дизайн и привязанные счета.</p>
        <p>Для показа картинки карты используется оригинальное изображение. (см <code>\common\widgets\FileUpload3\FileUpload</code>)</p>
        <p>Так выглядит страница карты:</p>
        <p>
            <img src="/images/controller/development-avatar-bank/card-number/des.png" class="thumbnail" width="50%">
        </p>
        <h3 class="page-header">Функция добавить счет к какрте</h3>
        <p>Здесь нужно вывести счета, которые не прикреплены ни к отдной карте и за исключением биткойн счетов и тех валют которые на карте уже есть.</p>

        <h3 class="page-header">Шифрование поролей начальных</h3>
        <p>Это необходимо для того чтобы в БД не было открытых паролей.</p>
        <p>Все нераспределенные счета карт зашифрованы по паролю который находится у "управляющего картами" (<code>role_card-admin</code>), он же и активирует карту.</p>
        <p>Получается что для того чтобы по новому алгоритму активировать карту, то сначала заявка на активацию отправляется "управляющему картами". Ему приходит письмо и он вводит пароль для разблокировки и пароль расшифровывается.</p>
        <p>Есть два решения:</p>
        <p>1. Выдавать всем ключи для расшифровки</p>
        <p>2. Разблокировать, перешифровать и выслать на почту временный пароль.</p>

        <h3 class="page-header">NFC</h3>
        <p>Для начала работы надо</p>
        <ul>
            <li>Установить <a href="https://yadi.sk/d/oPKV6jbpWlJ0xw">драйвер</a></li>
            <li>Инициализировать порт COM3</li>
        </ul>

        <p>Пример главного окна</p>
        <p><img src="/images/controller/development-avatar-bank/card-number/nfc.png" class="thumbnail"></p>
        <p>Запись производится в параметрами key=ffffffffffff</p>

        <h3 class="page-header">Ситуация "Пользователь потерял карту"</h3>
        <p>В этом случае пользователь должен зайти в кабинет и нажать заблокировать карту. При этом счета не закрываются и если клиент захочет при помощи ключей получить доступ к карте то у него не получится.</p>

        <h3 class="page-header">Генерация номера карты</h3>
        <p><code>\common\components\Card::generateNumber()</code> Эта функция выдает случайный номер карты, уникальный (сама проверяет чтобы не было повтора в таблице <code>card</code>)</p>
        <p>Номера</p>
        <p>0 позиция, 1 цифра - Всегда 8 - AvatarPay</p>
        <p>1 позиция, 3 цифры - Банк/организация</p>
        <p>4 позиция, 2 цифры - валюта сзади карты</p>
        <p>6 позиция, 9 цифр - Номер счета</p>
        <p>15 позиция, 1 цифра - контрольная, так же как у банков. Проверить контрольную сумму можно в функции <code>\common\components\Card::checkSum()</code></p>





    </div>
</div>



