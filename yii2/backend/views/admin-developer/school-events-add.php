<?php

/** $this \yii\web\View  */

use yii\helpers\Url;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;

$this->title = 'События. Страница добавления события';


?>

<div class="container">
    <div class="col-lg-12">
        <h1 class="page-header"><?= Html::encode($this->title) ?></h1>

        <p>Шаги заполнения</p>

        <ul>
            <li>Контент <code>content</code></li>
            <ul>
                <li>Название <code>name</code></li>
                <li>Описание</li>
                <li>Краткое описание</li>
                <li>Картинка <code>image</code></li>
            </ul>
            <li>Дата проведения</li>
            <li>Место проведения</li>
            <ul>
                <li>онлайн</li>
                <li>оффлайн</li>
            </ul>
            <li>Кол-во уроков</li>
            <li>Кто ведет</li>
            <ul>
                <li>Задать не зареганного</li>
                <li>Указать из своих</li>
            </ul>
            <li>Информация об организаторе</li>
            <li>Анкета регистрации</li>
            <li>Билеты, Оплата</li>
            <li>Страница</li>
            <ul>
                <li>Оформление</li>
                <li>Ссылка</li>
                <li>Данные для соц сети</li>
            </ul>
            <li>Публикация</li>
        </ul>

        <p>Сохраняет в сессии <code>event</code></p>
        <p>Страница добавления: <code>/cabinet-school-events/add?id=2</code></p>

        <p>По завершению вызывается скрипт <code>/cabinet-school-events-ajax/finish</code> <code>\avatar\controllers\CabinetSchoolEventsAjaxController::actionFinish()</code></p>

        <h3 class="page-header">Варианты входа в вебинарную комнату</h3>
        <p>Безоплатно без регистрации, для входа необходимо ввети почту и Имя</p>
        <p>Безоплатно только после регистрации, для входа необходимо ввети почту и Имя</p>

        <p>Безоплатные веьинары могут быть с ограниченным входом. 1. Ограничение на лимит пользователей, как это может быть реализовано?.</p>
        <p>Если платно то как входить? Каждому выдается пароль или ссылка для входа. Он входит без представления, так как он зарегистрировался при покупке, или дать возможность сменить имя отображаемое.</p>

        <h2 class="page-header">Стандарт блока config</h2>
        <pre>[
    1 => [
        'name' => '12'
        'content_short' => ''
        'content' => ''
        'image' => ''
    ]
    2 => [
        'is_online'  => '0'
        'place-lng'  => '48.74739339923016'
        'place-lat'  => '44.510933362396244'
        'place'      => 'Волгоград, Россия; улица имени Ивана Морозова, 3'
        'place_name' => 'Йога клуб Эра Водолея'
        'online_url' => ''
    ]
    3 => [
        'date_start' => '05.11.2019'
        'date_end' => ''
    ]
    4 => [
        'masters' => '9'
    ]
    5 => [
        'fields' => '[]'
    ]
    6 => [
        'vebinar_is_password' => '0'
        'vebinar_password' => ''
        'vebinar_max' => ''
        'vebinar_both_tickets' => '[]'
        'vebinar_buy_tickets' => '[]'
        'vebinar_tab' => '1'
        'offline_max' => ''
        'offline_both_tickets' => '[]'
        'offline_buy_tickets' => '[]'
        'offline_tab' => '1'
    ]
    7 => [
        'url' => 'ddd'
    ]
]</pre>
        <h3 class="page-header">Валидация</h3>
        <p>Дата окончания должны быть больше даты начала или равна</p>

        <h3>Блок 1</h3>
        <p>Общие данные</p>
        <?= \avatar\services\Params::widget([
            'params' => [
                [
                    'name'        => 'name',
                    'type'        => 'string',
                    'isRequired'  => true,
                    'description' => 'Название события',
                ],
                [
                    'name'        => 'content_short',
                    'type'        => 'string html нельзя',
                    'isRequired'  => false,
                    'description' => 'Описание события короткое',
                ],
                [
                    'name'        => 'content',
                    'type'        => 'string html нельзя',
                    'isRequired'  => false,
                    'description' => 'Описание события длинное',
                ],
                [
                    'name'        => 'image',
                    'type'        => 'url полный',
                    'isRequired'  => false,
                    'description' => 'Картинка события',
                ],
            ],
        ]) ?>
        <h3>Блок 2</h3>
        <p>Время проведения</p>
        <?= \avatar\services\Params::widget([
            'params' => [
                [
                    'name'        => 'date_start',
                    'type'        => 'date dd.mm.yyyy',
                    'isRequired'  => false,
                    'description' => 'Дата начала события',
                ],
                [
                    'name'        => 'date_end',
                    'type'        => 'date dd.mm.yyyy',
                    'isRequired'  => false,
                    'description' => 'Дата окончания события',
                ],
                [
                    'name'        => 'time_start',
                    'type'        => 'time hh:mm',
                    'isRequired'  => false,
                    'description' => 'Время начала события',
                ],
                [
                    'name'        => 'time_end',
                    'type'        => 'time hh:mm',
                    'isRequired'  => false,
                    'description' => 'Время окончания события',
                ],
                [
                    'name'        => 'time_zone',
                    'type'        => 'string',
                    'isRequired'  => false,
                    'description' => 'Временная зона если указывается время. https://www.php.net/manual/ru/timezones.php',
                ],
            ],
        ]) ?>
        <p>Дата начала может быть и не указана, это означает что это событие-курс не имеет времени начала и может проходиться по одному самостоятльно или с ведущим/руководителем.</p>
        <p>Если указывается время то должно быть указано и временная зона.</p>

        <h3>Блок 3</h3>
        <p>Место проведения</p>
        <?= \avatar\services\Params::widget([
            'params' => [
                [
                    'name'        => 'is_online',
                    'type'        => 'boolean 0/1',
                    'isRequired'  => true,
                    'description' => 'Флаг. Это событие онлайн? 0 - офлайн событие, 1 - онлайн событие',
                ],
                [
                    'name'        => 'online_url',
                    'type'        => 'url',
                    'isRequired'  => false,
                    'description' => 'Ссылка на онлайн событие',
                ],
                [
                    'name'        => 'place',
                    'type'        => 'string',
                    'isRequired'  => false,
                    'description' => 'Офлайн событие. Адрес места проведения',
                ],
                [
                    'name'        => 'place_name',
                    'type'        => 'string',
                    'isRequired'  => false,
                    'description' => 'Офлайн событие. Название места проведения',
                ],
                [
                    'name'        => 'place_point_lat',
                    'type'        => 'double',
                    'isRequired'  => false,
                    'description' => 'Офлайн событие. Точка места проведения. LAT - широта. десятичный разделитель - точка.',
                ],
                [
                    'name'        => 'place_point_lng',
                    'type'        => 'double',
                    'isRequired'  => false,
                    'description' => 'Офлайн событие. Точка места проведения. LNG - долгота. десятичный разделитель - точка.',
                ],
            ],
        ]) ?>
        <p><code>online_url</code>Ссылка на онлайн событие будет обязательной если <code>is_online=1</code>.</p>
        <p>Если <code>is_online=0</code> то поле <code>place</code> является обязательным. А поля <code>place_point_lat</code>, <code>place_point_lng</code> дополнительные.</p>
        <p><code>online_url</code> ссылка на анонс события. Ссылка на youtube вебинар указывается в уроке этого курса как первый урок</p>

        <h2 class="page-header">Онлайн событие</h2>
        <p><code>online_url</code> ссылка на анонс события. Ссылка на youtube вебинар указывается в уроке этого курса как первый урок</p>
        <p>После прохождения мастера создания события формируется урок в котором сохраняется ссылка на ютуб.</p>
        <p>Формируется урок <code>school_kurs_lesson_type.id = 6</code> c указанием в таблице <code>school_kurs_lesson_vebinar</code> ссылки на видео.</p>


        <h3 class="page-header">Шаг 6</h3>
        <p>Поле табуляции <code>vebinar_tab</code> 1 - безоплатно, 2 - смешанно, 3 - платно</p>

        <p>Поле табуляции <code>offline_tab</code> 1 - безоплатно, 2 - смешанно, 3 - платно</p>
        <p>Чем отличается смешанный тип от платного? Пока не понятно. Типа что в первом можно создать безоплатные билеты? тогда это частный случай платного. Надо в смешанном сделать отдельное поле для безоплатного участия, (кол-во человек, лимит)</p>
    </div>
</div>

