<?php

/** $this \yii\web\View  */

use yii\helpers\Url;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;

$this->title = 'front-end. Введение';


?>

<div class="container">
    <div class="col-lg-12">
        <h1 class="page-header"><?= Html::encode($this->title) ?></h1>

        <h2 class="page-header">Введение</h2>

        <p>Сам фреймворк Yii2 (<a href="https://www.yiiframework.com/" target="_blank">https://www.yiiframework.com/</a>).
            Поэтому структура построения Front-End построена на нем.</p>
        <p>Основное описание про представления <a href="https://www.yiiframework.com/doc/guide/2.0/ru/structure-views"
                                                  target="_blank">здесь</a></p>
        <p>Напомню несколько тезисов из его возможностей:</p>
        <ul>
            <li>В качестве шаблонизатора используется нативный PHP c расширением через классы.</li>
            <li>Можно подключить и другие шаблонизаторы Smarty, twig</li>
            <li>Поддерживает <a href="https://www.yiiframework.com/doc/guide/2.0/ru/output-theming" target="_blank">темизацию</a>
                — это способ заменить один набор представлений другим без переписывания кода, что замечательно подходит
                для изменения внешнего вида приложения.
            </li>
            <li>Есть встроенная поддержка <a href="https://www.yiiframework.com/doc/api/2.0/yii-widgets-pjax"
                                             target="_blank">PJAX</a>.
            </li>
            <li>Интегрирован вместе с <a
                        href="https://github.com/yiisoft/yii2-bootstrap/blob/master/docs/guide-ru/README.md"
                        target="_blank">Bootstrap</a>.
            </li>
            <li>Есть встроенные виджеты <a href="https://www.yiiframework.com/doc/api/2.0/yii-grid-detailview"
                                           target="_blank">DetailView</a>, <a
                        href="https://www.yiiframework.com/doc/api/2.0/yii-grid-listview" target="_blank">ListView</a>,
                <a href="https://www.yiiframework.com/doc/api/2.0/yii-grid-gridview" target="_blank">GridView</a>,
                BootstrapForm.
            </li>
            <li>Библиотеки подключаются через <a href="https://www.yiiframework.com/doc/guide/2.0/ru/structure-assets"
                                                 target="_blank">ресурсы (Assets)</a>.
            </li>
            <li>Законченные модули упаковываются в <a
                        href="https://www.yiiframework.com/doc/guide/2.0/ru/structure-widgets"
                        target="_blank">Виджеты</a>.
            </li>
            <li>Есть встроенное <a href="https://www.yiiframework.com/doc/guide/2.0/ru/caching-page" target="_blank">Кэширование
                    страниц</a> и <a href="https://www.yiiframework.com/doc/guide/2.0/ru/caching-fragment"
                                     target="_blank">Кэширование фрагментов</a>.
            </li>
        </ul>
        <p>Отображение данных:</p>
        <ul>
            <li><a href="https://www.yiiframework.com/doc/guide/2.0/ru/output-formatting">Форматирование данных</a></li>
            <li><a href="https://www.yiiframework.com/doc/guide/2.0/ru/output-pagination">Постраничное разделение
                    данных</a></li>
            <li><a href="https://www.yiiframework.com/doc/guide/2.0/ru/output-sorting">Сортировка</a></li>
            <li><a href="https://www.yiiframework.com/doc/guide/2.0/ru/output-data-providers">Провайдеры данных</a></li>
            <li><a href="https://www.yiiframework.com/doc/guide/2.0/ru/output-theming">Темизация</a></li>
        </ul>


        <h2 class="page-header">Приложение iAvatar</h2>
        <p>Мы придерживаемся классического построения представлений. Шаблонизатор встроенный нативный PHP.</p>
        <p>Наше приложение построено на Bootstrap v3.3</p>
        <p>Представления располагаются в папке <code>views</code>, обложки в папке <code>views/layouts</code></p>
        <p>Файлы картинок грузятся в отдельное облако.</p>
        <p>Есть самописный конструктор страниц из блоков по типу Тильды.</p>

        <h2 class="page-header">Разработка режов приложения</h2>
        <p><a href="https://habr.com/ru/post/455898/">О чем следует помнить при разработке тёмного режима приложения или
                сайта</a></p>
        <p><a href="https://material.io/design/introduction/#principles">UI</a></p>

        <h2 class="page-header">Форма логина</h2>
        <p>Подключается как ресурс <code>\avatar\widgets\ModalLogin\Asset::register($this);</code></p>
        <p>Этот ресурс добавляет событие <code>View::EVENT_END_BODY</code> где рисует форму.</p>
        <p>Код формы содержится в <code>@avatar/widgets/ModalLogin/view</code>.</p>
        <p>Форма работает и на главный сайт и на школы. В школах идет подстановка картинки лого школы.</p>
        <p>Авторизация идет через скрипт <code>/auth/login</code>.</p>

        <h2 class="page-header">Школа</h2>
        <p>Для оформления школы есть отдельная таблица: school_design.</p>
        <?= \common\services\documentation\DbTable::widget([
            'name'        => 'school_design',
            'description' => 'Параметры для оформления школы',
            'model'       => '\common\models\school\SchoolDesign',
            'columns'     => [
                [
                    'name'        => 'id',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Идентификатор строки',
                ],
                [
                    'name'        => 'school_id',
                    'type'        => 'integer',
                    'isRequired'  => true,
                    'description' => 'Идентификатор школы',
                ],
                [
                    'name'        => 'background_cabinet',
                    'type'        => 'string',
                    'isRequired'  => false,
                    'description' => 'фоновая картинка в личном кабинете, если нет то берется стандартная',
                ],
                [
                    'name'        => 'video_intro',
                    'type'        => 'string',
                    'isRequired'  => false,
                    'description' => 'Идентификатор видео для ютуба который будет показываться как интро при входе, если не задано то ничего не показывается',
                ],
                [
                    'name'        => 'logo_top_menu',
                    'type'        => 'string',
                    'isRequired'  => false,
                    'description' => 'Картинка для меню кабинета, если не используется то используется картинка школы',
                ],
                [
                    'name'        => 'footer_site',
                    'type'        => 'string',
                    'isRequired'  => false,
                    'description' => 'Ссылка на сайт в футере',
                ],
                [
                    'name'        => 'footer_mail',
                    'type'        => 'string',
                    'isRequired'  => false,
                    'description' => 'Почта в футере',
                ],
                [
                    'name'        => 'registration_header_id',
                    'type'        => 'string',
                    'isRequired'  => false,
                    'description' => 'Идентификатор шапки для страницы регистрации',
                ],
                [
                    'name'        => 'registration_footer_id',
                    'type'        => 'string',
                    'isRequired'  => false,
                    'description' => 'Идентификатор подвала для страницы регистрации',
                ],
                [
                    'name'        => 'cabinet_menu',
                    'type'        => 'string',
                    'isRequired'  => false,
                    'description' => 'Меню в кабинете',
                ],
            ],
        ]) ?>

        <h2 class="page-header">Шаблоны</h2>
        <p>main</p>
        <p>
            <a href="https://www.draw.io/#G1Dem2ZtGAY5jQH87kli2-T1cjIOpd_ICU" target="_blank">
                <img src="/images/controller/admin-developer/front-end/main.png" width="100%">
            </a>
        </p>
        <p>cabinet</p>
        <p>
            <a href="https://www.draw.io/#G1VIZ8zrSUguAeniWE5bY6CWCSZBPwFxRq" target="_blank">
                <img src="/images/controller/admin-developer/front-end/cabinet.png" width="100%">
            </a>
        </p>

        <h2 class="page-header">Шаблоны</h2>
        <p><a href="https://demos.creative-tim.com/now-ui-kit/index.html" target="_blank">https://demos.creative-tim.com/now-ui-kit/index.html</a></p>


        <h2 class="page-header">Виджеты</h2>
        <p><a href="https://bootsnipp.com/snippets/92e5X" target="_blank">https://bootsnipp.com/snippets/92e5X</a></p>
        <p><a href="https://bootsnipp.com/snippets/gN2b5" target="_blank">https://bootsnipp.com/snippets/92e5X</a></p>
        <p><a href="https://demos.creative-tim.com/fullcalendar/fullcalendar.html" target="_blank">https://demos.creative-tim.com/fullcalendar/fullcalendar.html</a> Календарь</p>
        <p><a href="https://bootsnipp.com/snippets/rNNng" target="_blank">https://bootsnipp.com/snippets/rNNng</a> Профиль</p>
        <p><a href="https://bootsnipp.com/snippets/eNNE6" target="_blank">https://bootsnipp.com/snippets/eNNE6</a> Профиль</p>
        <p><a href="https://bootsnipp.com/snippets/3x8BB" target="_blank">https://bootsnipp.com/snippets/3x8BB</a> Профиль</p>
        <p><a href="https://bootsnipp.com/snippets/K0ZmK" target="_blank">https://bootsnipp.com/snippets/K0ZmK</a> Профиль</p>
        <p><a href="https://demos.creative-tim.com/rotating-css-card/" target="_blank">https://demos.creative-tim.com/rotating-css-card/</a> Профиль</p>
        <p><a href="https://bootsnipp.com/snippets/vl4R7" target="_blank">https://bootsnipp.com/snippets/vl4R7</a> Логин</p>
    </div>
</div>

