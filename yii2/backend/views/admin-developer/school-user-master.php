<?php

/** $this \yii\web\View  */

use yii\helpers\Url;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;

$this->title = 'Учителя мастера';


?>

<div class="container">
    <div class="col-lg-12">
        <h1 class="page-header"><?= Html::encode($this->title) ?></h1>

        <h4 class="page-header">Параметры профиля мастера</h4>
        <?= \common\services\documentation\DbTable::widget([
            'name'    => 'user_master',
            'description'    => 'Параметры профиля мастера',
            'columns' => [
                [
                    'name'        => 'id',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Идентификатор строки',
                ],
                [
                    'name'        => 'user_id',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Идентификатор пользователя',
                ],
                [
                    'name'        => 'contact_vk',
                    'type'        => 'varchar(100)',
                    'isRequired'  => false,
                    'description' => 'Ссылка на страницу VK',
                ],
                [
                    'name'        => 'contact_fb',
                    'type'        => 'varchar(100)',
                    'isRequired'  => false,
                    'description' => 'Ссылка на страницу FB',
                ],
                [
                    'name'        => 'contact_youtube',
                    'type'        => 'varchar(100)',
                    'isRequired'  => false,
                    'description' => 'Ссылка на страницу youtube',
                ],
                [
                    'name'        => 'contact_instagram',
                    'type'        => 'varchar(100)',
                    'isRequired'  => false,
                    'description' => 'Ссылка на страницу instagram',
                ],
                [
                    'name'        => 'contact_tweeter',
                    'type'        => 'varchar(100)',
                    'isRequired'  => false,
                    'description' => 'Ссылка на страницу tweeter',
                ],
                [
                    'name'        => 'contact_telegram',
                    'type'        => 'varchar(100)',
                    'isRequired'  => false,
                    'description' => 'Аккаунт telegram или номер телефона',
                ],
                [
                    'name'        => 'contact_whatsapp',
                    'type'        => 'varchar(100)',
                    'isRequired'  => false,
                    'description' => 'Аккаунт-телефон whatsapp',
                ],
                [
                    'name'        => 'contact_email',
                    'type'        => 'varchar(100)',
                    'isRequired'  => false,
                    'description' => 'Почта для связи',
                ],
                [
                    'name'        => 'contact_phone',
                    'type'        => 'varchar(100)',
                    'isRequired'  => false,
                    'description' => 'Телефон для связи',
                ],
                [
                    'name'        => 'contact_viber',
                    'type'        => 'varchar(100)',
                    'isRequired'  => false,
                    'description' => 'Телефон viber',
                ],
                [
                    'name'        => 'bio',
                    'type'        => 'text',
                    'isRequired'  => false,
                    'description' => 'Биография',
                ],
                [
                    'name'        => 'vozm',
                    'type'        => 'text',
                    'isRequired'  => false,
                    'description' => 'Возможности',
                ],
                [
                    'name'        => 'is_sert',
                    'type'        => 'text',
                    'isRequired'  => true,
                    'description' => 'Флаг. Отображать сертификаты? 0 - не показывать, 1 - показывать, 0 - по умолчанию',
                ],
                [
                    'name'        => 'is_design',
                    'type'        => 'text',
                    'isRequired'  => true,
                    'description' => 'Флаг. Отображать дизайн аватара? 0 - не показывать, 1 - показывать, 0 - по умолчанию',
                ],
            ],
        ]) ?>
        <pre>CREATE TABLE `user_master` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `contact_vk` VARCHAR(100) DEFAULT NULL,
  `contact_fb` VARCHAR(100) DEFAULT NULL,
  `contact_youtube` VARCHAR(100) DEFAULT NULL,
  `contact_instagram` VARCHAR(100) DEFAULT NULL,
  `contact_tweeter` VARCHAR(100) DEFAULT NULL,
  `contact_telegram` VARCHAR(100) DEFAULT NULL,
  `contact_whatsapp` VARCHAR(100) DEFAULT NULL,
  `contact_email` VARCHAR(100) DEFAULT NULL,
  `contact_phone` VARCHAR(100) DEFAULT NULL,
  `contact_viber` VARCHAR(100) DEFAULT NULL,
  `bio` text DEFAULT NULL,
  `vozm` text DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8</pre>

        <p>В таблице <code>user</code> есть параметр <code>is_master</code>, который показывает участвует ли человек в каталоге.</p>

    </div>
</div>

