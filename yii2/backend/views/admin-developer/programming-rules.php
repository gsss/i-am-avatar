<?php

/** $this \yii\web\View  */

use yii\helpers\Url;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;

$this->title = 'Правила для программистов';


?>

<div class="container">
    <div class="col-lg-12">
        <h1 class="page-header"><?= Html::encode($this->title) ?></h1>

        <h2 class="page-header">Обработка ошибок</h2>
        <p>Если внутри кода явное нарушение внутренних правил то вызывать исключение.</p>
        <p>Оборачивать все связанные SQL запросы в транзакцию.</p>

        <h2 class="page-header">Комментарии в коде</h2>
        <p>Для стандартизации в функции я пишу отвечая на вопрос "Что делаю", в описании функции пишу отвечая на вопрос "Что делает"</p>

        <h2 class="page-header">Миграции</h2>
        <p>При изменении БД делать миграции, так как используется несколько стендов</p>
        <p>Миграции лежат в <code>/console/migrations</code>. Запуск производится следующей командой:</p>
        <pre>php yii migrate</pre>
        <p>Дополнительно о миграциях можно почитать здесь: <a href="https://www.yiiframework.com/doc/guide/2.0/ru/db-migrations" target="_blank">https://www.yiiframework.com/doc/guide/2.0/ru/db-migrations</a></p>



    </div>
</div>

