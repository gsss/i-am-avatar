<?php

/** $this \yii\web\View  */

use yii\helpers\Url;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;

$this->title = 'FileUpload7 - загружает картинку';


?>

<div class="container">
    <div class="col-lg-12">
        <h1 class="page-header"><?= Html::encode($this->title) ?></h1>

        <p>AJAX вызывается после каждой загрузки и записывает какой файл был загружен в таблицу <code>school_file</code>.</p>
    </div>
</div>

