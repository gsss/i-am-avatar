<?php

/** $this \yii\web\View  */

use yii\helpers\Url;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;

$this->title = 'Яндекс.Кассы';


?>

<div class="container">
    <div class="col-lg-12">
        <h1 class="page-header"><?= Html::encode($this->title) ?></h1>

        <pre>https://kassa.yandex.ru/developers</pre>
        <pre>https://for-people.life/shop-order/success?type=yandexkassa</pre>
        <pre>{
  "id": "wh-e44e8088-bd73-43b1-959a-954f3a7d0c54",
  "event": "payment.succeeded",
  "url": "https://www.merchant-website.com/notification_url"
}</pre>
        <?= \common\services\documentation\DbTable::widget([
            'name'        => 'payments_yandex_kassa',
            'model'       => '\common\models\PaymentYandexKassa',
            'description' => 'данные платежа Яндекс кассы',
            'columns'     => [
                [
                    'name'        => 'id',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Идентификатор строки',
                ],
                [
                    'name'        => 'billing_id',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Идентификатор счета',
                ],
                [
                    'name'        => 'payment_id',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Идентификатор платежа Яндекс',
                ],
                [
                    'name'        => 'currency_id',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Идентификатор валюты db.currency.id',
                ],
                [
                    'name'        => 'amount',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Кол-во атомов на платеж',
                ],
                [
                    'name'        => 'created_at',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Момент создания платежа',
                ],
            ],
        ]) ?>

    </div>
</div>

