<?php

/** $this \yii\web\View  */

use yii\helpers\Url;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;

$this->title = 'Рекламные матриалы';


?>

<div class="container">
    <div class="col-lg-12">
        <h1 class="page-header"><?= Html::encode($this->title) ?></h1>

        <?= \common\services\documentation\DbTable::widget([
            'name'    => 'reklama',
            'model'    => '\common\models\Reklama',
            'columns' => [
                [
                    'name'        => 'id',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Идентификатор шага',
                ],
                [
                    'name'        => 'name',
                    'type'        => 'varchar(255)',
                    'isRequired'  => true,
                    'description' => 'Наименование объекта',
                ],
                [
                    'name'        => 'image',
                    'type'        => 'varchar(255)',
                    'isRequired'  => true,
                    'description' => 'Превью',
                ],
                [
                    'name'        => 'file',
                    'type'        => 'varchar(255)',
                    'isRequired'  => true,
                    'description' => 'Файл для скачивания',
                ],
                [
                    'name'        => 'created_at',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Момент действия',
                ],
                [
                    'name'        => 'content',
                    'type'        => 'text',
                    'isRequired'  => true,
                    'description' => 'Описание обхекта',
                ],
            ],
        ]) ?>


    </div>
</div>

