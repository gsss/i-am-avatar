<?php

/** $this \yii\web\View  */

use yii\helpers\Url;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;

$this->title = 'Служба поддержки';


?>

<div class="container">
    <div class="col-lg-12">
        <h1 class="page-header"><?= Html::encode($this->title) ?></h1>

        <div>

            <!-- Nav tabs -->
            <ul class="nav nav-tabs" role="tablist">
                <li role="presentation" class="active"><a href="#clear" aria-controls="clear" role="tab" data-toggle="tab">Чистовик</a></li>
                <li role="presentation"><a href="#potok" aria-controls="potok" role="tab" data-toggle="tab">Поток</a></li>
            </ul>

            <!-- Tab panes -->
            <div class="tab-content">
                <div role="tabpanel" class="tab-pane in active" id="clear">

                </div>
                <div role="tabpanel" class="tab-pane fade" id="potok">
                    <p>Это сервис который будет помогать школам обрабатывать заявки в службу поддержки</p>
                </div>
            </div>

        </div>
    </div>
</div>