<?php

/** $this \yii\web\View  */

use yii\helpers\Url;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;

$this->title = 'Безопасность';


?>

<div class="container">
    <div class="col-lg-12">
        <h1 class="page-header"><?= Html::encode($this->title) ?></h1>

        <!-- Nav tabs -->
        <ul class="nav nav-tabs" role="tablist">
            <li role="presentation" class="active"><a href="#clear" aria-controls="clear" role="tab" data-toggle="tab">Чистовик</a></li>
            <li role="presentation"><a href="#potok" aria-controls="potok" role="tab" data-toggle="tab">Поток</a></li>
        </ul>

        <!-- Tab panes -->
        <div class="tab-content">
            <div role="tabpanel" class="tab-pane in active" id="clear">
            </div>
            <div role="tabpanel" class="tab-pane fade" id="potok">
                <p>Доступ в кабинет защищается двухфакторной авторизацией.</p>
                <p>БД делается резерв каждый час</p>
                <p>БД делается копия резерва каждый день</p>
                <p>Все материалы заверяются авторским правом</p>




            </div>
        </div>


    </div>
</div>

