<?php

/** $this \yii\web\View  */

use yii\helpers\Url;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;

$this->title = 'Школа. Настройки';


?>

<div class="container">
    <div class="col-lg-12">
        <h1 class="page-header"><?= Html::encode($this->title) ?></h1>

        <?= \common\services\documentation\DbTable::widget([
            'name'        => 'school_settings',
            'description' => 'Настройки школы',
            'model'       => '\common\models\subscribe\UserSubscribeAction',
            'columns'     => [
                [
                    'name'        => 'id',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Идентификатор процесса',
                ],
                [
                    'name'        => 'shop_image_cut_id',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Конфигурация вида обрезки изображений товаров. 0 - обрезать 1 - вписывать, 2 - вписывать с фоном. По умолчанию - 0',
                ],
            ],
        ]) ?>

        <h2 class="page-header">Настройка магазина</h2>
        <p>Ну вот проблема, а что если аватар поменяет настройку когда у него уже залиты некоторые фотки?
            нужно их конечно же обновить все. Это уже проблема. Пока оставлю так, а потом доделаю. Можно сделать кнопку - привести все старые фотки к одному формату и по этой кнопке будет произведено обновление. Это уже <a href="https://www.i-am-avatar.com/cabinet-task-list/view?id=1140" data-toggle="tooltip" title="Задача">нужно сделать функцию обновления фоток на облаке</a>.</p>

    </div>
</div>

