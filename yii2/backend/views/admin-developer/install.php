<?php

/** $this \yii\web\View  */

use yii\helpers\Url;
use app\services\GsssHtml;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use app\services\GetArticle\Collection;

$this->title = 'Инсталяция сайта';


?>

<div class="container">
    <div class="col-lg-12">
        <h1 class="page-header"><?= Html::encode($this->title) ?></h1>

        <h2 class="page-header">Установка memcache</h2>
        <p>Установка memcache выполняется в несколько действий.</p>
        <p>Для начала используйте apt-get, чтобы установить memcached.</p>
        <pre>sudo apt-get install memcached</pre>
        <p>Затем нужно установить php-pear – репозиторий, в котором хранится memcache.</p>
        <pre>sudo apt-get install php-pear</pre>
        <p>Если на вашем сервере нет компилятора, скачайте инструменты build-essential:</p>
        <pre>sudo apt-get install php7.0-dev</pre>
        <pre>sudo apt-get install build-essential</pre>
        <p>В завершение используйте PECL (PHP Extension Community Library) для установки memcache:</p>
        <pre>sudo pecl install memcache</pre>
        <p>Если не захочет</p>
        <pre>sudo apt-get install zlib1g-dev</pre>
        <h3 class="page-header">Просмотр статистики</h3>
        <p>Проверить, установился ли Memcache, можно с помощью функции поиска:</p>
        <pre>ps aux | grep memcache</pre>

        <h2 class="page-header">Возможные ошибки</h2>
        <p>Если в Mysql возникают ошибка</p>
        <p class="alert alert-danger">SQLSTATE[42000]: Syntax error or access violation: 1055 Expression #3 of SELECT list is not in GROUP BY clause and contains nonaggregated</p>
        <p>То устанавливаю:</p>
        <pre>sudo mysql -u root -p</pre>
        <pre>SET GLOBAL sql_mode=(SELECT REPLACE(@@sql_mode,'ONLY_FULL_GROUP_BY',''));</pre>


        <p class="header2">Создать нового пользователя</p>
        <button style="margin: 0px 0px 20px 0px;" class="btn btn-primary btn-xs" type="button" data-toggle="collapse" data-target="#step1" aria-expanded="false" aria-controls="step1">
            Показать
        </button>
        <div class="collapse" id="step1">
            <pre>adduser god && adduser god sudo</pre>
        </div>


        <p class="header2">Генерация ssh ключей</p>
        <button style="margin: 0px 0px 20px 0px;" class="btn btn-primary btn-xs" type="button" data-toggle="collapse"
                data-target="#step5" aria-expanded="false" aria-controls="step5">
            Показать
        </button>
        <div class="collapse" id="step5">
            <pre>ssh-keygen
cat /home/ethereum/.ssh/id_rsa.pub</pre>
            <p>добавить в <a href="https://bitbucket.org/" target="_blank">https://bitbucket.org/</a></p>
        </div>


        <p class="header2">Отключить Root пользователя</p>
        <button style="margin: 0px 0px 20px 0px;" class="btn btn-primary btn-xs" type="button" data-toggle="collapse"
                data-target="#step2" aria-expanded="false" aria-controls="step2">
            Показать
        </button>
        <div class="collapse" id="step2">
            <p>Открыть файл конфигурации</p>
            <pre>sudo nano /etc/ssh/sshd_config</pre>
            <p>Параметр <code>PermitRootLogin</code> поставить в положение <code>no</code></p>
            <p>Сохранить файл</p>
            <p>Перезагрузить сервер</p>
            <pre>sudo reboot</pre>
            <p>Зайти под новым пользователем <code>god</code></p>
        </div>


        <p class="header2">Настройка nginx</p>
        <button style="margin: 0px 0px 20px 0px;" class="btn btn-primary btn-xs" type="button" data-toggle="collapse"
                data-target="#step7" aria-expanded="false" aria-controls="step7">
            Показать
        </button>
        <div class="collapse" id="step7">
            <pre>sudo apt-get install nginx
sudo nano /etc/nginx/sites-enabled/default</pre>

            <p>Все стереть, записать:</p>
            <p><b>SSL</b></p>
            <pre>server {
    listen 80;
    return 301 https://$host$request_uri;
}

server {
    listen 443 ssl;
    server_name atlantida.io atlantida;
    access_log /var/log/nginx/atlantida.log;

    ssl_certificate /home/ethereum/atlantida_io.crt;
    ssl_certificate_key /home/ethereum/1.key;

    location / {
        proxy_pass http://localhost:3000;
        proxy_http_version 1.1;
        proxy_set_header Upgrade $http_upgrade;
        proxy_set_header Connection 'upgrade';
        proxy_set_header Host $host;
        proxy_cache_bypass $http_upgrade;
    }
}</pre>

            <p><b>Без SSL</b></p>
            <pre>server {
    listen 80;
    server_name atlantida.io atlantida;
    access_log /var/log/nginx/atlantida.log;

    location / {
        proxy_pass http://localhost:3000;
        proxy_http_version 1.1;
        proxy_set_header Upgrade $http_upgrade;
        proxy_set_header Connection 'upgrade';
        proxy_set_header Host $host;
        proxy_cache_bypass $http_upgrade;
    }
}</pre>

            <pre>sudo reboot</pre>
        </div>

        <p class="header2">Поставить php</p>
        <button style="margin: 0px 0px 20px 0px;" class="btn btn-primary btn-xs" type="button" data-toggle="collapse"
                data-target="#step9" aria-expanded="false" aria-controls="step9">
            Показать
        </button>
        <div class="collapse" id="step9">
            <pre>sudo apt-get install php7.0</pre>
        </div>

        <p class="header2">Поставить mysql</p>
        <button style="margin: 0px 0px 20px 0px;" class="btn btn-primary btn-xs" type="button" data-toggle="collapse"
                data-target="#step10" aria-expanded="false" aria-controls="step10">
            Показать
        </button>
        <div class="collapse" id="step10">
            <pre>sudo apt-get install mysql-server</pre>
            <p>Сменить пароль</p>
            <p><a href="http://ikode.ru/articles/kak-sbrosit-parol-root-mysql-na-ubuntu-1804" target="_blank">http://ikode.ru/articles/kak-sbrosit-parol-root-mysql-na-ubuntu-1804</a> </p>
        </div>


        <p class="header2">Поставить ssl</p>
        <button style="margin: 0px 0px 20px 0px;" class="btn btn-primary btn-xs" type="button" data-toggle="collapse"
                data-target="#step8" aria-expanded="false" aria-controls="step8">
            Показать
        </button>
        <div class="collapse" id="step8">
            <pre>cd ~
sudo apt install socat
mkdir acme
cd acme
git clone https://github.com/Neilpang/acme.sh.git
cd acme.sh
./acme.sh --install
sudo service nginx stop
sudo ./acme.sh --force --issue --standalone -d  cloud1.iampau.pro
</pre>
            <p>Скопировать имя файла сертификата и прописать его в файле <code>/etc/nginx/sites-enabled/default</code></p>
            <pre>sudo nano /etc/nginx/sites-enabled/default</pre>
            <pre>sudo crontab -e</pre>
            <p>Прописать</p>
            <pre>6 0 * * * "/home/ethereum/.acme.sh"/acme.sh --cron --home "/home/ethereum/.acme.sh" > /dev/null</pre>
            <pre>sudo service nginx start</pre>
            <p>Перезагрузить сервер</p>
            <pre>sudo reboot</pre>
            <hr>
            <p><a href="http://zoic.ru/ssl-сертификаты/" target="_blank">Получить сертификат бесплатно</a></p>
        </div>


        <p class="header2">Настроить PHP сайт</p>
        <button style="margin: 0px 0px 20px 0px;" class="btn btn-primary btn-xs" type="button" data-toggle="collapse"
                data-target="#step11" aria-expanded="false" aria-controls="step11">
            Показать
        </button>
        <div class="collapse" id="step11">
            <p>Зайти</p>
            <pre>cd /etc/nginx/sites-available</pre>
            <p>Здесь располагаются настройки каждого сайта, и нужно создать новый файл</p>
            <pre>sudo nano {siteName}</pre>
            <p>Вписать в файл указанный конфиг</p>
            <pre>server {
    listen 80;
    server_name p.i-am-avatar.com;
    return 301 https://$host$request_uri;
}

server {
    # прослушивание 443 защищенного порта
    listen 443 ssl;

    # на какое доменное имя будет откликаться веб сервер
    server_name p.i-am-avatar.com;

    # лог для этого сайта
    access_log /root/prometey/log-nginx/access.log;
    error_log  /root/prometey/log-nginx/errors.log;

    # Сертификаты
    ssl_certificate /root/.acme.sh/p.i-am-avatar.com/p.i-am-avatar.com.cer;
    ssl_certificate_key /root/.acme.sh/p.i-am-avatar.com/p.i-am-avatar.com.key;

    # Домашняя папка
    root /root/prometey/www/public_html;

    # Запрет на открытие сайта в чужом фрейме
    add_header X-Frame-Options "SAMEORIGIN";

    # Установка индексного файла, файл по умолчанию
    index index.php;

    # Установка максимального объема POST запроса
    client_max_body_size 200M;

    location / {
        if (!-e $request_filename) {
            rewrite ^(.*)$ /index.php;
        }

        try_files $uri $uri/ =404;
    }

    # Установка обработчика для файлов PHP
    location ~ \.php$ {
        include snippets/fastcgi-php.conf;
        fastcgi_pass unix:/var/run/php/php7.0-fpm.sock;
    }

    location ~ /\.ht {
        deny all;
    }
}</pre>
            <p><code>{domainName}</code> - доменное имя сайта, оно должно быть прописано в DNS сервере.</p>
            <p><code>{file.cer}</code> - полный путь к файлу сертификата полученный на пункте <b>Поставить ssl</b></p>
            <p><code>{file.key}</code> - полный путь к файлу ключа полученный на пункте <b>Поставить ssl</b></p>


            <p>Сделать Symlink из cd <code>/etc/nginx/sites-available</code> на <code>/etc/nginx/sites-enabled</code></p>


            <p>Открыть</p>
            <p>Создать сертификат</p>

            <pre>cd ~
cd acme/acme.sh
sudo ./acme.sh --issue --standalone -d i-am-avatar.com</pre>

            <p>Прописать в файле настроек сайта</p>
            <pre>cd /etc/nginx/sites-available
sudo nano php</pre>

            <pre>cd ~</pre>
            <pre>git clone git@bitbucket.org:Ra-m-ha/sirius-aton-php.git</pre>

            <pre>sudo apt-get install php7.0-mbstring
sudo apt-get install php7.0-bcmath
sudo apt-get install php7.0-mysql
sudo apt-get install php7.0-curl
sudo apt-get install php7.0-dom
sudo apt-get install php7.0-mcrypt
sudo apt-get install php7.0-gd
sudo apt-get install php7.0-gmp
sudo apt-get install php7.0-memcache
sudo apt-get install php7.0-intl</pre>
            <pre>sudo apt-get install php7.0-mbstring php7.0-bcmath php7.0-mysql php7.0-curl php7.0-dom php7.0-mcrypt php7.0-gd php7.0-gmp php7.0-memcache php7.0-intl</pre>

            <p>Если не захочет то:</p>
            <pre>sudo apt-add-repository ppa:ondrej/php
sudo apt-get update</pre>

            <pre>sudo php composer.phar global require "fxp/composer-asset-plugin:^1.2.0"</pre>
            <pre>sudo php composer.phar update</pre>
            <p>Может возникнуть вопрос о получении токена. Для этого надо перейти по указанной ссылке и сохранить токен и указать его в строке ввода.</p>
            <p><img src="/images/controller/development-avatar-bank/install-eth/2018-02-10_22-33-16.png" class="thumbnail" width="100%"></p>
            <pre>sudo php init</pre>
            <p>Выбрать пункт <code>prod</code></p>
            <hr>
            <h4>Инсталяция Memcache</h4>
            <p><a href="http://www.servermom.org/install-use-memcached-nginx-php-7-ubuntu-16-04/3670/" target="_blank">INSTALL AND USE MEMCACHED WITH NGINX, PHP 7 ON UBUNTU 16.04</a></p>
            <pre>sudo apt-get update -y
sudo apt install memcached
sudo apt install php-memcached</pre>
        </div>


        <p class="header2">Поставить FireWall</p>
        <button style="margin: 0px 0px 20px 0px;"
                class="btn btn-primary btn-xs"
                type="button"
                data-toggle="collapse"
                data-target="#step13"
                aria-expanded="false"
                aria-controls="step13">
            Показать
        </button>
        <div class="collapse" id="step13">
            <p>Установить файервол ufw</p>

            <pre>sudo apt-get install ufw</pre>

            <p>Настроить файервол</p>
            <pre>sudo ufw default allow outgoing
sudo ufw default deny incoming
sudo ufw allow ssh/tcp
sudo ufw limit ssh/tcp
sudo ufw allow http/tcp
sudo ufw allow https/tcp
sudo ufw logging on</pre>

            <p>Включить все настройки и подтвердить включение</p>
            <pre>sudo ufw enable</pre>

            <p>В любое время вы можете проверить состояние UFW с помощью команды:</p>
            <pre>sudo ufw status verbose</pre>

            <p><a href="https://community.vscale.io/hc/ru/community/posts/208348529-Настройка-фаервола-в-Ubuntu-с-помощью-утилиты-UFW" target="_blank">Статья</a></p>
            <p><a href="https://habrahabr.ru/company/ua-hosting/blog/230201/" target="_blank">Статья</a></p>

        </div>

        /home/god/i-am-avatar/www/2/YOURPUBLIC.pem
        /home/god/i-am-avatar/www/2/YOURPRIVATE.key


        <p class="header2">Поставить ssl</p>
        <button style="margin: 0px 0px 20px 0px;" class="btn btn-primary btn-xs" type="button" data-toggle="collapse"
                data-target="#step8" aria-expanded="false" aria-controls="step8">
            Показать
        </button>
        <div class="collapse" id="step8">
            <pre>cd ~
sudo apt install socat
mkdir acme
cd acme
git clone https://github.com/Neilpang/acme.sh.git
cd acme.sh
./acme.sh --install
sudo service nginx stop
sudo ./acme.sh --issue --standalone -d m.imperiacityhotel.com
</pre>
            <p>Скопировать имя файла сертификата и прописать его в файле <code>/etc/nginx/sites-enabled/default</code></p>
            <pre>sudo nano /etc/nginx/sites-enabled/default</pre>
            <pre>sudo crontab -e</pre>
            <p>Прописать</p>
            <pre>6 0 * * * "/home/ethereum/.acme.sh"/acme.sh --cron --home "/home/ethereum/.acme.sh" > /dev/null</pre>
            <pre>sudo service nginx start</pre>
            <p>Перезагрузить сервер</p>
            <pre>sudo reboot</pre>
            <hr>
            <p><a href="http://zoic.ru/ssl-сертификаты/" target="_blank">Получить сертификат бесплатно</a></p>
        </div>

        <p class="header2">Поставить права на папку upload</p>
        <button style="margin: 0px 0px 20px 0px;" class="btn btn-primary btn-xs" type="button" data-toggle="collapse"
                data-target="#step20190523" aria-expanded="false" aria-controls="step20190523">
            Показать
        </button>
        <div class="collapse" id="step20190523">
            <pre>sudo chmod ugo+rwx -R upload</pre>
        </div>


    </div>
</div>

apt-get install mc
ключи
create database prometey_prod_wallet;
create database prometey_prod_main;
create database prometey_prod_stat;

use prometey_prod_main
source /var/www/prometey/www/vendor/i_am_avatar_prod_main.sql
use prometey_prod_stat
source /var/www/prometey/www/vendor/i_am_avatar_prod_stat.sql
use prometey_prod_wallet
source /var/www/prometey/www/vendor/i_am_avatar_prod_wallet.sql