<?php

/** $this \yii\web\View  */

use yii\helpers\Url;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;

$this->title = 'Конструктор сайтов. Блок 37. Соц сети';




?>

<div class="container">
    <div class="col-lg-12">
        <h1 class="page-header"><?= Html::encode($this->title) ?></h1>

        <p>Всего есть несколько типов соц сетей которые поддерживаются</p>
        <ul>
            <li>Телеграм</li>
            <li>VK</li>
            <li>Facebook</li>
            <li>YouTube</li>
            <li>Instagram</li>
        </ul>


        <p>Высота картинок 43 PX</p>

        <p>Сохранение настроек идет в JSON</p>
        <h2 class="page-header">Настройки в БД</h2>
        <pre>{"items":[{"type":"telegram","href":"https://www.i-am-avatar.com/telegram/group","title":"Группа в Телеграм"}],"margin-top":15,"margin-bottom":15,"background-color:"#ffffff"}</pre>
        <p>Объект массива <code>items</code></p>
        <?= \avatar\services\Params::widget([
            'params' => [
                [
                    'name'        => 'type',
                    'isRequired'  => true,
                    'description' => 'Тип значка',
                ],
                [
                    'name'        => 'href',
                    'isRequired'  => true,
                    'description' => 'Ссылка на соц сеть',
                ],
                [
                    'name'        => 'title',
                    'isRequired'  => false,
                    'description' => 'Подсказка',
                ],
            ],
        ]) ?>

        <h2 class="page-header">Картинки</h2>
        <?php
        $rows = [
            [
                'type'  => 'telegram',
                'name'  => 'Телеграм',
                'image' => '//www.i-am-avatar.com/images/mail/subscribe/social-net/Telegram.png',
            ],
            [
                'type'  => 'vk',
                'name'  => 'VK',
                'image' => '//www.i-am-avatar.com/images/mail/subscribe/social-net/vk.png',
            ],
            [
                'type'  => 'vk',
                'name'  => 'Facebook',
                'image' => '//www.i-am-avatar.com/images/mail/subscribe/social-net/fb.png',
            ],
            [
                'type'  => 'youtube',
                'name'  => 'YouTube',
                'image' => '//www.i-am-avatar.com/images/mail/subscribe/social-net/media.png',
            ],
            [
                'type'  => 'instagram',
                'name'  => 'Instagram',
                'image' => '//www.i-am-avatar.com/images/mail/subscribe/social-net/ins.png',
            ],
        ];
        ?>

        <?= \yii\grid\GridView::widget([
            'dataProvider' => new \yii\data\ArrayDataProvider([
                'allModels'  => $rows,
                'pagination' => [
                    'pageSize' => 20,
                ],
            ]),
            'summary' => '',
            'tableOptions' => [
                'class' => 'table table-striped table-hover',
                'style' => 'width: auto;'
            ],
            'rowOptions'   => function ($item) {
                $data = [
                    'role'  => 'button',
                    'class' => 'rowTable',
                ];
                return $data;
            },
            'columns'      => [
                'type',
                'name',
                'image',
                [
                    'header'  => 'Картинка',
                    'content' => function ($item) {
                        $i = ArrayHelper::getValue($item, 'image', '');
                        if ($i == '') return '';

                        return Html::img($i, [
                            'width'  => 43,
                            'height' => 43,
                            'style'  => 'margin-bottom: 0px;',
                        ]);
                    },
                ],
            ],

        ]) ?>

        <h2 class="page-header">Исследование</h2>
        <p>Здесь хочу понять как сделать 1 обработку ошибок 2 редактирования массива элементов. Поделю.</p>

        <h3 class="page-header">Редактирования массива элементов</h3>
        <p>Здесь надо сделать: добавление, удаление, вывод ошибок.</p>

        <h4 class="page-header">Добавление</h4>
        <p>Тут надо понять как это все изобразить.</p>

    </div>
</div>

