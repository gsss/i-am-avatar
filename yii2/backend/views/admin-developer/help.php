<?php

/** $this \yii\web\View  */

use yii\helpers\Url;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;

$this->title = 'Помощь';


?>

<div class="container">
    <div class="col-lg-12">
        <h1 class="page-header"><?= Html::encode($this->title) ?></h1>

        <p>Здесь описывается структура каталога помощи</p>

        <?= \common\services\documentation\DbTable::widget([
            'name'        => 'htlp_tree',
            'model'       => '\common\models\htlp\TreeItem',
            'description' => 'Дерево помощи',
            'columns'     => [
                [
                    'name'        => 'id',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Идентификатор записи',
                ],
                [
                    'name'        => 'parent_id',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Идентификатор родительского элемента',
                ],
                [
                    'name'        => 'name',
                    'type'        => 'varchar(100)',
                    'isRequired'  => true,
                    'description' => 'Название элемента дерева',
                ],
                [
                    'name'        => 'created_at',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'момент создания заявки',
                ],
            ],
        ]) ?>

        <?= \common\services\documentation\DbTable::widget([
            'name'        => 'htlp_article',
            'model'       => '\common\models\help\Article',
            'description' => 'Статья помощи',
            'columns'     => [
                [
                    'name'        => 'id',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Идентификатор статьи',
                ],
                [
                    'name'        => 'user_id',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Автор статьи',
                ],
                [
                    'name'        => 'name',
                    'type'        => 'varchar(100)',
                    'isRequired'  => true,
                    'description' => 'Название Статьи',
                ],
                [
                    'name'        => 'content',
                    'type'        => 'text',
                    'isRequired'  => true,
                    'description' => 'Содержимое стаьи HTML',
                ],
                [
                    'name'        => 'created_at',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'момент создания статьи',
                ],
            ],
        ]) ?>

        <h2 class="pahe-header">Fronend</h2>
        <p>Помощь располагается по ссылке <code>/help/index</code> а статья <code>/help/view?id=1</code></p>

    </div>
</div>