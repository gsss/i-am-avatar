<?php

/** $this \yii\web\View  */

use yii\helpers\Url;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;

$this->title = 'Школа. Автоворонки';


?>

<div class="container">
    <div class="col-lg-12">
        <h1 class="page-header"><?= Html::encode($this->title) ?></h1>

        <div>

            <!-- Nav tabs -->
            <ul class="nav nav-tabs" role="tablist">
                <li role="presentation" class="active"><a href="#clear" aria-controls="clear" role="tab" data-toggle="tab">Чистовик</a></li>
                <li role="presentation"><a href="#potok" aria-controls="potok" role="tab" data-toggle="tab">Поток</a></li>
            </ul>

            <!-- Tab panes -->
            <div class="tab-content">
                <div role="tabpanel" class="tab-pane in active" id="clear">
                    <h2 class="page-header">Описание</h2>
                    <p><iframe class="thumbnail" width="560" height="315" src="https://www.youtube.com/embed/dBmrmnunuAw" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe></p>
                    <p>Автоворонки это организация схемы движения клиента от захвата внимания до факта покупки посредством писем, чат ботов в автоматизированном виде.</p>
                    <p>Органзовывается через систему событий. Допустим первое событие это регистрация клиента или покупка курса.</p>

                    <h2 class="page-header">Термины</h2>
                    <p>Событие - которое происходит с пользователем</p>
                    <p>Маршрут автоворонки - путь по которому можно будет пройти клиенту, определяется классом AutoVoronkaGraf, свойства принадлежность школе,</p>
                    <p>Шаг маршрута - единица маршрута, узел графа пути в маршруте следования, определяется классом AutoVoronkaStep</p>
                    <p>Пользователь - Польователь который произвел шаг в графе, в основном производится только над пользователем User2</p>
                    <p>Рекация на событие - действие которое совершает платформа на действие пользователя.</p>

                    <h2 class="page-header">Типы событий</h2>
                    <p>Пользователь регился в тильде и пришел лид на платформу</p>
                    <p>Пользователь заказал курс</p>
                    <p>Пользователь заказал купил курс</p>
                    <p>Пользователь зарегистрировался</p>
                    <p>Пользователь залогинился</p>
                </div>
                <div role="tabpanel" class="tab-pane fade" id="potok">
                    <h2 class="page-header">Описание</h2>
                    <p><iframe class="thumbnail" width="560" height="315" src="https://www.youtube.com/embed/dBmrmnunuAw" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe></p>
                    <p>Автоворонки это организация схемы движения клиента от захвата внимания до факта покупки посредством писем, чат ботов в автоматизированном виде.</p>
                    <p>Органзовывается через систему событий. Допустим первое событие это регистрация клиента или покупка курса.</p>

                    <h2 class="page-header">Термины</h2>
                    <p>Событие - которое происходит с пользователем</p>
                    <p>Маршрут автоворонки - путь по которому можно будет пройти клиенту, определяется классом AutoVoronkaGraf, свойства принадлежность школе,</p>
                    <p>Шаг маршрута - единица маршрута, узел графа пути в маршруте следования, определяется классом AutoVoronkaStep</p>
                    <p>Пользователь - Польователь который произвел шаг в графе, в основном производится только над пользователем User2</p>
                    <p>Рекация на событие - действие которое совершает платформа на действие пользователя.</p>

                    <h2 class="page-header">Типы событий</h2>
                    <p>Пользователь регился в тильде и пришел лид на платформу</p>
                    <p>Пользователь заказал курс</p>
                    <p>Пользователь заказал купил курс</p>
                    <p>Пользователь зарегистрировался</p>
                    <p>Пользователь залогинился</p>

                    <h2 class="page-header">Типы рекаций</h2>
                    <p>Выслать письмо</p>
                    <p>Ответ в мессенджер</p>

                    <h2 class="page-header">Сущности</h2>
                    <p>Событие</p>
                    <p>Реакция-действие-ответ - может быть как отсылка письма или сообщения через месенджер (VK, FB, TeleGram).</p>
                    <p>Событие - регистрация из внешней системы - <code>registerLidTilda</code></p>
                    <p>Как вешается событие?</p>
                    <p>Как оно вызывается? Из класса User2 вызывается функция eventRegisterLidTilda() которая вызывает триггер registerLidTilda</p>
                    <p>В событии registerLidTilda определяющим свойством является из какой формы тильды (form_id) пришли данные и они являются свойством шага маршрута</p>
                    <p>Как только он зарегистрировался надо совершить действие. Какое? Это определяется из Маршрута Автоворонки. Как определяется маршрут автоворонки?</p>
                    <p>Какое действие я могу запустить после этого? Выслать письмо клиенту.</p>

                    <p>Какой еще вопрос продумать? Как подсчитывать переход по ссылке?</p>
                    <p>В письме ставить ссылку на i-am-avatar, там инкрементируется счетчик и проводится редирект</p>


                    <p>Как сделать ожидание действия?</p>
                    <p>Сделать таблицу ожидающих действий, туда заносить с временем должного трабатывания, и прогоняться каждую минуту и выполнять действие.</p>

                    <p>Как проверить открытие письма?</p>
                    <p>Вставить пиксель который будет обращаться на аватар и там фиксировать это событие</p>


                    <p style="font-weight: bold;">Обработка событий</p>
                    <p>Пользователь пришел как лид с тильды - Событие - добавляется в user2</p>
                    <p>Пользователь регистрируется на платформе (в школе) - Событие - Проверка есть ли такой юзер в user, да - делается ссылка на школу к юзеру, нет - регистрируется и отсылается письмо.</p>
                    <p>Пользователь логинится в системе - Событие - Пользователь ищется в user, есть - проверка логина, нет - вы не зарегистрированы.</p>
                    <p>В школе надо вывести список пользователей. User2 link User_School</p>

                    <h2 class="page-header">Сущности</h2>
                    <p>Как организовать автоворонку?</p>
                    <p>После события если есть автоворонка клиент попадает в таблицу</p>
                    <?= \common\services\documentation\DbTable::widget([
                        'name'    => 'tunnel_step',
                        'columns' => [
                            [
                                'name'        => 'id',
                                'type'        => 'int',
                                'isRequired'  => true,
                                'description' => 'Идентификатор шага',
                            ],
                            [
                                'name'        => 'tunnel_id',
                                'type'        => 'int',
                                'isRequired'  => true,
                                'description' => 'Идентификатор тунеля',
                            ],
                            [
                                'name'        => 'user_root_id',
                                'type'        => 'int',
                                'isRequired'  => true,
                                'description' => 'Идентификатор пользователя',
                            ],
                            [
                                'name'        => 'object_id',
                                'type'        => 'int',
                                'isRequired'  => true,
                                'description' => 'Идентификатор объекта который пройден',
                            ],
                            [
                                'name'        => 'is_done',
                                'type'        => 'int',
                                'isRequired'  => true,
                                'description' => 'Выполнено действие?',
                            ],
                            [
                                'name'        => 'created_at',
                                'type'        => 'int',
                                'isRequired'  => true,
                                'description' => 'Момент действия',
                            ],
                            [
                                'name'        => 'is_client_or_system',
                                'type'        => 'int',
                                'isRequired'  => true,
                                'description' => '1 - клиент, 2 - система',
                            ],
                        ],
                    ]) ?>
                    <p>Таблица таймеров - сохраняет какие действия надо выполнить в будущем. своего рода будильник</p>
                    <?= \common\services\documentation\DbTable::widget([
                        'name'    => 'tunnel_actions',
                        'columns' => [
                            [
                                'name'        => 'id',
                                'type'        => 'int',
                                'isRequired'  => true,
                                'description' => 'Идентификатор действия',
                            ],
                            [
                                'name'        => 'tunnel_id',
                                'type'        => 'int',
                                'isRequired'  => true,
                                'description' => 'Идентификатор тунеля',
                            ],
                            [
                                'name'        => 'start_at',
                                'type'        => 'int',
                                'isRequired'  => true,
                                'description' => 'Момент запуска',
                            ],
                        ],
                    ]) ?>
                    <p>Реализация - событие произошло - высылаем письмо или ставим таймер на высылку письмо - добавляю в таблицу шагов - ставлю таймер на проверку прочтения - ставлю таймер на отсылку письма</p>
                    <pre>php yii tunnel/do</pre>
                    <p>Находит событие, высылает и добавляет шаг.</p>
                    <p>Как происходит проверка открытия?</p>
                    <p>Ставлю пиксель</p>
                    <p>https://www.i-am-avatar.com/tunnel/letter-open?id=object_id&hash=######</p>
                    <p>Как осуществляется проверка  на открытие  по ссылке в письме?</p>
                    <p>https://www.i-am-avatar.com/tunnel/letter-click?id=object_id&hash=######</p>
                    <p>hash нужен для того чтобы никто не мог от балды ставить статусы, своего рода контрольная строка, которая сохраняется в таблице.</p>
                    <?= \common\services\documentation\DbTable::widget([
                        'name'    => 'tunnel_link',
                        'columns' => [
                            [
                                'name'        => 'id',
                                'type'        => 'int',
                                'isRequired'  => true,
                                'description' => 'Идентификатор действия',
                            ],
                            [
                                'name'        => 'object_id',
                                'type'        => 'int',
                                'isRequired'  => true,
                                'description' => 'Идентификатор объекта',
                            ],
                            [
                                'name'        => 'hash',
                                'type'        => 'varchar(6)',
                                'isRequired'  => true,
                                'description' => 'hash',
                            ],
                            [
                                'name'        => 'link',
                                'type'        => 'varchar(255)',
                                'isRequired'  => true,
                                'description' => 'ссылка',
                            ],
                        ],
                    ]) ?>
                    <p>Значит нужен конструктор писем, можно простой сделать для начала</p>
                    <p>
                        Картинка<br>
                        Текст<br>
                        Кнопки<br>
                        Подвал
                    </p>
                    <p>А зачем link?</p>
                    <p>Зачем нужны кнопки и какие могут быть кнопки?</p>
                    <p>1 купить курс - действие - перенаправление на оплату</p>
                    <p>2 В письме задается вопрос, например "что вы хотите узнать?"<br>
                        a - выслать письмо<br>
                        b - выслать письмо
                    </p>
                    <p>По сути туннель это интерактивное взаимодействие с пользователем. То есть это чат бот.</p>
                    <p>Как сделать визуально? в виде дерева.</p>


                    <p>В этой таблице хранится само дерево тунеля</p>
                    <?= \common\services\documentation\DbTable::widget([
                        'name'    => 'tunnel_tree_item',
                        'columns' => [
                            [
                                'name'        => 'id',
                                'type'        => 'int',
                                'isRequired'  => true,
                                'description' => 'Идентификатор действия',
                            ],
                            [
                                'name'        => 'tunnel_id',
                                'type'        => 'int',
                                'isRequired'  => true,
                                'description' => 'Идентификатор тунеля',
                            ],
                            [
                                'name'        => 'parent_id',
                                'type'        => 'int',
                                'isRequired'  => true,
                                'description' => 'Идентификатор родителя',
                            ],
                            [
                                'name'        => 'type_id',
                                'type'        => 'int',
                                'isRequired'  => true,
                                'description' => 'Тип объекта',
                            ],
                        ],
                    ]) ?>
                    <p>Типы:<br>
                        1 - событие активации тонеля (регистрация на событие, покапка курса)<br>
                        2 - письмо<br>
                        3 - переход по ссылке<br>
                        4 - клик по ссылке<br>
                        5 - отсрочка
                    </p>
                    <?= \common\services\documentation\DbTable::widget([
                        'name'    => 'tunnel_tree',
                        'columns' => [
                            [
                                'name'        => 'id',
                                'type'        => 'int',
                                'isRequired'  => true,
                                'description' => 'Идентификатор тунеля',
                            ],
                            [
                                'name'        => 'school_id',
                                'type'        => 'int',
                                'isRequired'  => true,
                                'description' => 'Идентификатор школы',
                            ],
                            [
                                'name'        => 'name',
                                'type'        => 'varchar(100)',
                                'isRequired'  => true,
                                'description' => 'Наименование тунеля',
                            ],
                        ],
                    ]) ?>
                </div>
            </div>

        </div>




    </div>
</div>