<?php

/** $this \yii\web\View  */

use yii\helpers\Url;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;

$this->title = 'Процессинг ЯАватар';


?>



<div class="col-lg-6">
<h1 class="text-center" style="margin-top:0px;padding-top:300px;font-size:400%;color:#000;text-shadow: -1px 0 1px white, 0 -1px 1px white, 0 1px 1px white, 1px 0 1px white, 0 0 8px white, 0 0 8px white, 0 0 8px white, 2px 2px 3px black;">Проект ЯАватар</h1>

<p class="text-center lead" style="font-size:200%;color:#000;text-shadow: -1px 0 1px white, 0 -1px 1px white, 0 1px 1px white, 1px 0 1px white, 0 0 8px white, 0 0 8px white, 0 0 8px white, 2px 2px 3px black;">Платформа для ускоренного обучения Человечества методам перехода на Золотой уровень Сознания.</p>
</div>
<div class="col-lg-6" style="padding-top:300px;">
    <iframe width="100%" height="315" src="https://www.youtube.com/embed/7gVXareaBME" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</div>



<div class="container">
    <div class="col-lg-12">
        <h1 class="page-header"><?= Html::encode($this->title) ?></h1>


        <p>Все операции совершаются в атомах и целых числах. Атом – самая малая величина для монеты, то есть это копейка для монеты. Максимальная размерность для суммы 18000000000000000000.</p>
        <p>Процессинг позволяет создавать свои монеты.</p>
        <p>Процессинг поддерживает карты.</p>
        <p>На одной карте может быть привязано любое кол-во монет. Но строго по одной одного типа.</p>


        <h4 class="page-header">Термины</h4>
        <p><b>Сущности</b></p>
        <p>Атом - копейка монеты</p>
        <p>Валюта - монета, свойства: название, кол-во атомов в монете, код-валюты</p>
        <p>Кошелек - сущность которая хранит монеты, свойства: валюта, чей, кол-во атомв монеты</p>
        <p>Операция - операция с кошельком</p>
        <p>Транзакция - перевод монеты из одного кошелька в другой, всегда сопровождается двумя операциями.</p>
        <p><b>Операции</b></p>
        <p>Эмиссия - создание монет в кошельке, не содержит поле transaction_id</p>
        <p>Сжигание - уничтожение монет в кошельке, не содержит поле transaction_id</p>
        <p>Приход - опрерация прибавления монет в кошельке. Всегда сопровождается операцией Расход и содержит поле transaction_id</p>
        <p>Расход - опрерация убавления монет в кошельке. Всегда сопровождается операцией Приход и содержит поле transaction_id</p>
        <p><img src="/images/controller/admin-developer/school-money/trans.png"></p>

        <h3 class="page-header">Стандарт адресов</h3>
        <p>Всего четыре сущности:</p>
        <p>1. Валюта-монета</p>
        <p>2. Операция</p>
        <p>3. Транзакция</p>
        <p>4. Кошелек</p>
        <p>Иднетификатор для операции, транзакции и кошелька является типом UNSIGNED BIGINT, а это значит максимальное значение равно 18446744073709551615. Это 20 символов, поэтому для обозначения адреса используется 20 символов с ведущими нулями.</p>
        <p>Иднетификатор для валюты является типом UNSIGNED INT, а это значит максимальное значение равно 4294967295. Это 10 символов, поэтому для обозначения адреса используется 10 символов с ведущими нулями.</p>
        <p>Вначале адреса для валюты добавлется префикс <code>C_</code></p>
        <p>Вначале адреса для операции добавлется префикс <code>O_</code></p>
        <p>Вначале адреса для транзакции добавлется префикс <code>T_</code></p>
        <p>Вначале адреса для кошелька добавлется префикс <code>W_</code></p>
        <p>Сокращенная запись для отображения в основном испольуется в стандарте A_...BBBB. Где А - буква для именования сущности, BBBB - последние цифры адреса.</p>
        <p>Например для валюты испольуется адрес <code>C_0000000003</code>, сокращенный <code>C_...0003</code>.</p>
        <p>Например для операции испольуется адрес <code>O_00000000000000000007</code>, сокращенный <code>O_...0007</code>.</p>
        <p>Например для транзакции испольуется адрес <code>T_00000000000000000007</code>, сокращенный <code>T_...0007</code>.</p>
        <p>Например для кошелька испольуется адрес <code>W_00000000000000000007</code>, сокращенный <code>W_...0007</code>.</p>

        <h3 class="page-header">API</h3>
        <p>Адрес для доступа: <code>https://api.processing.i-am-avatar.com</code></p>
        <p>Все ответы выдаются в формете JSON</p>
        <p>Стандартный положительный ответ: <code>{success:true}</code></p>
        <p>Стандартный положительный ответ с данными: <code>{success:true, data: ...}</code></p>

        <h2 class="page-header">Авторизация</h2>
        <p>Для доступа к API испольуется key. Он указывается в заголовке REQUEST под названием <code>X-API-KEY</code></p>
        <p>В качестве пароля используется подпись. Подпись как результат функции <code>hash('sha256', ($secret . ((int)(time()/100))))</code>. Таким образом пароль не передается в открытом виде, текущая подпись действует только 100 секунд. <code>X-API-SECRET</code></p>
        <h3 class="page-header">Ошибки</h3>
        <p>Если не передан параметр <code>X-API-KEY</code>, то возвращается ошибка 403. и текст</p>
        <pre>{"name":"Forbidden","message":"Нет параметра X-API-KEY","code":1,"status":403,"type":"yii\\web\\ForbiddenHttpException"}</pre>
        <p>Если не передан параметр <code>X-API-SECRET</code>, то возвращается ошибка 403. и текст</p>
        <pre>{"name":"Forbidden","message":"Нет параметра X-API-SECRET","code":2,"status":403,"type":"yii\\web\\ForbiddenHttpException"}</pre>
        <p>Если не найден параметр <code>X-API-KEY</code>, то возвращается ошибка 403. и текст</p>
        <pre>{"name":"Forbidden","message":"Не найден логин","code":3,"status":403,"type":"yii\\web\\ForbiddenHttpException"}</pre>
        <p>Если не верный <code>X-API-SECRET</code>, то возвращается ошибка 403. и текст</p>
        <pre>{"name":"Forbidden","message":"Не верный secret","code":4,"status":403,"type":"yii\\web\\ForbiddenHttpException"}</pre>

        <h2 class="page-header">Функции</h2>
        <p class="alert alert-danger">В разработке</p>

        <h3 class="page-header">wallet/new</h3>
        <h3 class="page-header">wallet/info</h3>
        <h3 class="page-header">wallet/burn</h3>
        <h3 class="page-header">wallet/emission</h3>
        <h3 class="page-header">wallet/send</h3>
        <h3 class="page-header">wallet/transaction-list</h3>
        <h3 class="page-header">wallet/set-call-back</h3>
        <h3 class="page-header">wallet/get-call-back-list</h3>
        <h3 class="page-header">wallet/clear-all-call-backs</h3>

        <h3 class="page-header">remove-call-back</h3>

        <h3 class="page-header">transaction/info</h3>
        <h3 class="page-header">transaction/operations</h3>

        <h3 class="page-header">operation/info</h3>

        <h3 class="page-header">currency/new</h3>
        <h3 class="page-header">currency/info</h3>
        <h3 class="page-header">currency/operations</h3>
        <h3 class="page-header">currency/transactions</h3>


    </div>
</div>

