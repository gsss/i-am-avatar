<?php

/** $this \yii\web\View  */

use yii\helpers\Url;
use app\services\GsssHtml;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use app\services\GetArticle\Collection;

$this->title = 'Facebook Бот';


?>

<div class="container">
    <div class="col-lg-12">
        <h1 class="page-header"><?= Html::encode($this->title) ?></h1>
        <p><a href="https://habr.com/post/281559/" target="_blank">Разработка чат-бота для Facebook Messenger</a></p>
        <p><a href="https://github.com/alexrvs/yii2-facebook" target="_blank">Yii 2 Facebook API</a></p>
    </div>
</div>

