<?php
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model \avatar\models\forms\AdminQrFileDb */

$this->title = 'Карты';

?>
<div class="container" style="padding-bottom: 70px;">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header text-center"><?= $this->title ?></h1>

            <?php if (Yii::$app->session->hasFlash('form')) { ?>

            <?php } else { ?>

                <?php $form = \yii\bootstrap\ActiveForm::begin([
                    'options' => ['enctype' => 'multipart/form-data']
                ]); ?>
                <?= $form->field($model, 'file')->fileInput() ?>

                <hr>
                <div class="form-group">
                    <?= Html::submitButton('Добавить', [
                        'class' => 'btn btn-default',
                        'name'  => 'contact-button',
                        'style' => 'width:100%',
                    ]) ?>
                </div>
                <?php \yii\bootstrap\ActiveForm::end(); ?>
            <?php } ?>
        </div>
    </div>
</div>