<?php
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model \avatar\models\forms\AdminQrFileDb2 */

$this->title = 'Карты';

?>
<div class="container" style="padding-bottom: 70px;">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header text-center"><?= $this->title ?></h1>

            <?php if (Yii::$app->session->hasFlash('form')) { ?>
                <?php

                $db = Yii::$app->session->getFlash('form');
                $this->registerJs(<<<JS

var count = 2;
var p = 1;
var db = '{$db}';

var functionPage = function() {
    ajaxJson({
        url: '/admin-qr-code/page',
        data: {
            page: p,
            db: db
        },
        success: function(ret) {
            console.log(ret);
            ajaxJson({
                url: '/admin-qr-code/page2',
                data: {
                    page: p,
                    file: ret.file,
                    db: db
                },
                success: function(ret) {
                    $('#result').append(
                        $('<p>').html(ret.file)
                    );
                    p++;
                    if (p <= count) {
                        functionPage();
                    }
                }
            });
        },
        error: function(ret) {
            console.log(ret);
            if (ret.status == 200) {
                ajaxJson({
                    url: '/admin-qr-code/page2',
                    data: {
                        page: p,
                        file: ret.file,
                        db: db
                    },
                    success: function(ret) {
                        $('#result').append(
                            $('<p>').html(ret.file)
                        );
                        p++;
                        if (p <= count) {
                            functionPage();
                        }
                    }
                });
            }
        }
    });
    
};


functionPage();


JS
                );
                ?>
                <pre id="result"></pre>


            <?php } else { ?>

                <?php $form = \yii\bootstrap\ActiveForm::begin([
                    'options' => ['enctype' => 'multipart/form-data']
                ]); ?>
                <?= $form->field($model, 'file')->fileInput() ?>

                <hr>
                <div class="form-group">
                    <?= Html::submitButton('Добавить', [
                        'class' => 'btn btn-default',
                        'name'  => 'contact-button',
                        'style' => 'width:100%',
                    ]) ?>
                </div>
                <?php \yii\bootstrap\ActiveForm::end(); ?>
            <?php } ?>
        </div>
    </div>
</div>