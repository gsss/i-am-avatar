<?php
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $billing \common\models\avatar\UserBill */

$this->title = 'Карты';

$this->registerJs(<<<JS

var count = 100;
var p = 1;

var functionPage = function() {
    ajaxJson({
        url: '/admin-qr-code/page',
        data: {
            page: p
        },
        success: function(ret) {
            ajaxJson({
                url: '/admin-qr-code/page2',
                data: {
                    page: p,
                    file: ret.file
                },
                success: function(ret) {
                    $('#result').append(
                        $('<p>').html(ret.file)
                    );
                    p++;
                    if (p <= count) {
                        functionPage();
                    }
                }
            });
        }
    });
    
};


functionPage();


JS
);

?>
<div class="container" style="padding-bottom: 70px;">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header text-center"><?= $this->title ?></h1>
            <pre id="result"></pre>
        </div>
    </div>
</div>