<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;
use yii\db\Query;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $project \common\models\investment\Project */

$this->title = $project->name;


$this->registerJs(<<<JS
$('.rowTable').click(function() {
    window.location = '/cabinet-projects/request?' +
     'id' + '=' + $(this).data('id')
     ;
});

JS
);

?>

<style>
    .textDecorated {
        text-decoration-line: underline;
        text-decoration-style: dotted;
    }
</style>

<div class="container">
    <div class="col-lg-12">
        <h1 class="page-header"><?= Html::encode($this->title) ?></h1>
    </div>
    <div class="col-lg-8">

        <?php
        $this->registerJs(<<<JS
                
$('.buttonSend').click(function(e) {
    e.preventDefault();
    e.stopPropagation();
    var bid = $(this).data('id');
    $('#modalTransaction').attr('data-id', bid);
    $('#modalTransaction').modal();
});

JS
        );

        ?>
        <?= \yii\grid\GridView::widget([
            'dataProvider' => new \yii\data\ActiveDataProvider([
                'query' => \common\models\investment\IcoRequest::find()
                    ->where(['ico_request.ico_id' => $project->id])
                    ->innerJoin('paysystems_config', 'paysystems_config.id = ico_request.paysystem_config_id')
                    ->innerJoin('paysystems', 'paysystems.id = paysystems_config.paysystem_id')
                    ->leftJoin('ico_request_success', 'ico_request.id = ico_request_success.id')
                    ->select([
                        'ico_request.*',
                        'paysystems_config.paysystem_id',
                        'paysystems.currency',
                        'paysystems.title',
                        'paysystems.image',
                        'ico_request_success.txid',
                        'ico_request_success.created_at as txid_created_at',
                    ])
                    ->orderBy(['ico_request.created_at' => SORT_DESC])
                    ->asArray()
            ]),
            'tableOptions' => [
                'class' => 'table table-hover table-striped',
                'style' => 'width: auto;',
                'id'    => 'tableTransaction',
            ],
            'rowOptions'   => function ($item) {
                $params = [
                    'data'  => [
                        'id' => $item['id'],
                    ],
                    'role'  => 'button',
                    'class' => 'rowTable'
                ];

                return $params;
            },
            'columns' => [
                'id',
                [
                    'header'  => 'Платежная система',
                    'content' => function ($item) {
                        $v = \yii\helpers\ArrayHelper::getValue($item, 'image', '');

                        return Html::img($v, [
                            'width' => 50,
                            'class' => 'img-circle',
                            'title' => \yii\helpers\ArrayHelper::getValue($item, 'title', ''),
                            'data'  => [
                                'toggle'    => 'tooltip',
                                'placement' => 'bottom',
                            ],
                        ]);
                    },
                ],
                [
                    'header'    => 'Создано',
                    'attribute' => 'created_at',
                    'content'   => function ($item) {
                        $v = \yii\helpers\ArrayHelper::getValue($item, 'created_at', 0);
                        if ($v == 0) return '';

                        return Html::tag('span', \cs\services\DatePeriod::back($v, ['isShort' => true]), [
                            'data'  => [
                                'toggle'    => 'tooltip',
                                'placement' => 'bottom',
                            ],
                            'title' => Yii::$app->formatter->asDatetime($v),
                            'class' => 'textDecorated'
                        ]);
                    },
                ],
                [
                    'header'         => 'Токенов',
                    'format'         => ['decimal', 2],
                    'attribute'      => 'tokens',
                    'headerOptions'  => [
                        'style' => 'text-align:right',
                    ],
                    'contentOptions' => [
                        'style' => 'text-align:right',
                    ],

                ],
                [
                    'header'  => 'Пользователь',
                    'content' => function ($item) {
                        $user_id = \yii\helpers\ArrayHelper::getValue($item, 'user_id', '');
                        $email = \yii\helpers\ArrayHelper::getValue($item, 'email', '');
                        if (is_null($user_id)) {
                            return $email;
                        }
                        $user = \common\models\UserAvatar::findOne($user_id);
                        $v = $user->getAvatar();

                        return
                            Html::a(Html::img($v, [
                                'width' => 50,
                                'class' => 'img-circle',
                                'title' => $user->email,
                                'data'  => [
                                    'toggle'    => 'tooltip',
                                    'placement' => 'bottom',
                                ],
                            ]), ['user/index', 'id' => $user_id])
                            ;
                    },
                ],
                [
                    'header'  => 'Транзакция',
                    'content' => function ($item) {
                        $request = \common\models\investment\IcoRequest::findOne($item['id']);
                        try {
                            $payment = $request->getBilling()->getPayment();
                        } catch (Exception $e) {
                            return '';
                        }
                        $txid = $payment->getTransaction();
                        if (is_null($txid)) {
                            $txid = '';
                        }
                        if ($txid == '') return '';

                        return Html::tag('code', substr($txid,0,8).'...');
                    },
                ],
                [
                    'header'  => 'Оплачено?',
                    'content' => function ($item) {
                        $request = \common\models\investment\IcoRequest::findOne($item['id']);
                        try {
                            $payment = $request->getBilling()->getPayment();
                        } catch (Exception $e) {
                            return '';
                        }
                        $txid = $payment->getTransaction();
                        if (is_null($txid)) {
                            $txid = '';
                        }
                        if ($txid == '') return Html::tag('span', 'Нет', ['class' => 'label label-default']);
                        return Html::tag('span', 'Да', ['class' => 'label label-success']);
                    },
                ],
                [
                    'header'    => 'Отправлено',
                    'content'   => function ($item) {
                        $v = \yii\helpers\ArrayHelper::getValue($item, 'txid', '');
                        if ($v == '') return '';
                        $provider = new \avatar\modules\ETH\ServiceEtherScan();

                        return Html::a(Html::tag('code', substr($v, 0, 8) . '...'), $provider->getUrl('/tx/'.$v), ['target' => '_blank']);
                    },
                ],
                [
                    'header'         => '',
                    'contentOptions' => [
                        'nowrap' => 'nowrap',
                    ],
                    'content'        => function ($item) {
                        $v = \yii\helpers\ArrayHelper::getValue($item, 'txid_created_at', 0);
                        if ($v == 0) return '';

                        return Html::tag('span', \cs\services\DatePeriod::back($v, ['isShort' => true]), [
                            'data'  => [
                                'toggle'    => 'tooltip',
                                'placement' => 'bottom',
                            ],
                            'title' => Yii::$app->formatter->asDatetime($v),
                            'class' => 'textDecorated'
                        ]);
                    },
                ],
            ]

        ]) ?>

    </div>
    <div class="col-lg-4">
        <?= $this->render('../cabinet/_menu') ?>
    </div>
</div>

<div class="modal fade" tabindex="-1" role="dialog" id="modalTransaction">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header text-center">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Отправить токены</h4>
            </div>
            <div class="modal-body">

                <p>Пароль:</p>
                <p><input id="field-password" class="form-control" type="password"></p>

            </div>
            <div class="modal-footer">
                <?php
                $str4 = Yii::t('c.TsqnzVaJuC', 'Транзакция успешно отправлена');

                $this->registerJs(<<<JS
                
$('.buttonModalSet').click(function(e) {
    var bid = $('#modalTransaction').data('id');
    
    var b = $(this);
    b.off('click');
    b.html($('<i>', {class: 'fa fa-spinner fa-spin fa-fw'}));
    b.attr('disabled', 'disabled');
    ajaxJson({
        url: '/cabinet-projects/send',
        data: {
            password: $('#field-password').val(),
            id: bid
        },
        success: function(ret) {
            $('#modalTransaction').on('hidden.bs.modal', function(e) {
                $('#modalInfo .modal-body')
                .append(
                    $('<p>').html('{$str4}')
                )
                .append(
                    $('<p>').append(
                        $('<code>').html(ret.txid)
                    )
                )
                ;
                $('#modalInfo').on('hidden.bs.modal', function(e) {
                    // window.location.reload();
                });
                $('#modalInfo').modal();
            }).modal('hide');
        }
    });
});

JS
                );

                ?>

                <button type="button" class="btn btn-primary buttonModalSet" style="width: 100%;">Подтвердить
                </button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div><!-- /.modal -->



<div class="modal fade" id="modalInfo" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">&times;</span></button>
                <h4 class="modal-title text-center" id="myModalLabel"><?= \Yii::t('c.TsqnzVaJuC', 'Информация') ?></h4>
            </div>
            <div class="modal-body text-center">

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" style="width: 100%;" data-dismiss="modal">
                    <?= \Yii::t('c.TsqnzVaJuC', 'Закрыть') ?>
                </button>
            </div>
        </div>
    </div>
</div>