<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \avatar\models\forms\Login */

$this->title = 'Вход в кабинет РАйСовета';

?>
<div class="container">

    <div class="col-lg-12">
        <h1 class="page-header text-center"><?= Html::encode($this->title) ?></h1>
    </div>
    <div class="col-lg-4 col-lg-offset-4">



        <p class="text-center"><img src="https://cloud1.iampau.pro/upload/cloud/15846/29196_KY9yFSPvQU_crop.png" width="300" class="img-circle"></p>
        <p><span style="color: red">Важно!</span>: Убедитесь, что вы находитесь на сайте: <code>https://admin.iampau.pro</code></p>

        <p><?= \Yii::t('c.Q2vFnrwWB1', 'Пожалуйста заполните следующие поля для входа') ?>:</p>

        <?php $form = ActiveForm::begin(); ?>

        <?= $form->field($model, 'username', ['inputOptions' => ['placeholder' => 'Email']])->label('Email', ['class' => 'hide']) ?>
        <?= $form
            ->field(
                $model,
                'password',
                ['inputOptions' => ['placeholder' => Yii::t('c.0uMdJb0e0n', 'Пароль')]]
            )
            ->passwordInput()
            ->label(Yii::t('c.0uMdJb0e0n', 'Пароль'), ['class' => 'hide']) ?>

        <a href="/auth/password-recover"><?= \Yii::t('c.Q2vFnrwWB1', 'Восстановить пароль') ?></a>
        <hr>
        <div class="form-group">
            <?= Html::submitButton(Yii::t('c.Q2vFnrwWB1', 'Вход'), [
                'class' => 'btn btn-success',
                'name'  => 'login-button',
                'style' => 'width:100%',
            ]) ?>
        </div>
        <p><a href="/auth/registration" class="btn btn-default" style="width: 100%;"><?= \Yii::t('c.Q2vFnrwWB1', 'Регистрация') ?></a></p>
        <?php ActiveForm::end(); ?>
    </div>

</div>

