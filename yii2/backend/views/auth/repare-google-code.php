<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;

/* @var $this yii\web\View */
/* @var $model \avatar\models\forms\RepareGoogleCode */

$this->title = 'Восстановить код Google Authentification';
?>

<div class="container">
    <div class="col-lg-4 col-lg-offset-4">
        <h1 class="page-header text-center"><?= Html::encode($this->title) ?></h1>
        <?php if (Yii::$app->session->hasFlash('form')): ?>
            <?php
            $content = (new \Endroid\QrCode\QrCode())
                ->setText($model->url)
                ->setSize(180)
                ->setPadding(20)
                ->setErrorCorrection('high')
                ->setForegroundColor(['r' => 0, 'g' => 0, 'b' => 0, 'a' => 0])
                ->setBackgroundColor(['r' => 255, 'g' => 255, 'b' => 255, 'a' => 0])
                ->setLabelFontSize(16)
                ->setImageType(\Endroid\QrCode\QrCode::IMAGE_TYPE_PNG)
                ->get('png');
            $src = 'data:image/png;base64,' . base64_encode($content);

            ?>
            <p><img src="<?= $src ?>"></p>
            <p class="alert alert-success">Сохраните код отсканировав код.</p>
        <?php else: ?>
            <?php $form = ActiveForm::begin(); ?>

            <?= $form->field($model, 'email', ['inputOptions' => ['placeholder' => 'Email']])->label('Email', ['class' => 'hide']) ?>
            <?= $form->field($model, 'code', ['inputOptions' => ['placeholder' => 'Код GoogleAuth']])->label('Код GoogleAuth', ['class' => 'hide']) ?>
            <?= $form->field($model, 'verificationCode')->widget('\yii\captcha\Captcha') ?>

            <hr>
            <div class="form-group">
                <?= Html::submitButton('Восстановить', [
                    'class' => 'btn btn-primary',
                    'name'  => 'contact-button',
                    'style' => 'width: 100%',
                ]) ?>
            </div>
            <?php ActiveForm::end(); ?>
        <?php endif; ?>
    </div>
</div>