<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \avatar\models\forms\Registration */



$this->title = 'Регистрация';


?>

<div class="container">
    <div class="col-lg-4 col-lg-offset-4">

        <h1 class="page-header text-center"><?= Html::encode($this->title) ?></h1>
        <?php if (Yii::$app->session->hasFlash('contactFormSubmitted')): ?>
            <div class="alert alert-success">
                <?= \Yii::t('c.0uMdJb0e0n', 'Благодрим вас за регистрацию') ?>
            </div>
        <?php else: ?>
            <?php $form = ActiveForm::begin([
                'id'                   => 'contact-form',
                'enableAjaxValidation' => true,
            ]); ?>
            <?php $field = $form->field($model, 'email', ['inputOptions' => ['placeholder' => 'email']])->label(Yii::t('c.0uMdJb0e0n', 'Почта'), ['class' => 'hide']);
            $field->validateOnBlur = true;
            $field->validateOnChange = true;
            echo $field;
            ?>
            <?= $form->field($model, 'password1', ['inputOptions' => ['placeholder' => Yii::t('c.0uMdJb0e0n', 'Пароль')]])->passwordInput()->label(Yii::t('c.0uMdJb0e0n', 'Пароль'), ['class' => 'hide']) ?>
            <?= $form->field($model, 'password2', ['inputOptions' => ['placeholder' => Yii::t('c.0uMdJb0e0n', 'Повторите пароль')]])->passwordInput()->label(Yii::t('c.0uMdJb0e0n', 'Повторите пароль'), ['class' => 'hide']) ?>

            <?= $form->field($model, 'verificationCode')->widget('\yii\captcha\Captcha') ?>
            <hr>
            <div class="form-group">
                <?= Html::submitButton(Yii::t('c.0uMdJb0e0n', 'Зарегистрироваться'), [
                    'class' => 'btn btn-primary',
                    'name'  => 'contact-button',
                    'style' => 'width: 100%',
                ]) ?>
            </div>
            <?php ActiveForm::end(); ?>
        <?php endif; ?>
    </div>
</div>
