<?php
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this \yii\web\View */
/* @var $model \avatar\models\forms\MailTest */

$this->title = 'Временная почта';

$this->registerJs(<<<JS
$('.buttonSet').click(function(e) {
    function setCookie2 (name, value) {
        var date = new Date();
        var now = new Date();
        var time = now.getTime();
        var expireTime = time + (1000 * 60 * 60 * 24 * 365);
        now.setTime(expireTime);
        document.cookie = name + "=" + value +
        "; expires=" + now.toGMTString() +
        "; path=/";
    }
    setCookie2('mailTest', $('#mailtest-mail').val());
})
JS
);

?>

<div class="container" style="padding-bottom: 70px;">
    <div class="col-lg-12">
        <h1 class="page-header"><?= $this->title ?></h1>

        <?php if (Yii::$app->session->hasFlash('form')) { ?>
            <p class="alert alert-success">Успешно</p>
        <?php } else { ?>
            <?php $form = \yii\bootstrap\ActiveForm::begin() ?>
            <?= $form->field($model, 'mail') ?>
            <hr>
            <?= Html::button('Обновить', ['class' => 'btn btn-success buttonSet'])?>
            <?php \yii\bootstrap\ActiveForm::end() ?>
        <?php } ?>

    </div>
</div>


<div class="col-lg-12">
    <h1 class="page-header text-center"><?= $this->title ?></h1>
</div>

<div class="col-lg-4">
    <p><a href="/anketa/new-school">Подключить школу</a></p>
    <p><a href="http://root.i-am-avatar.com/kurs-i-avatar" target="_blank">Как пользоваться интерфейсом ЯАватар</a></p>
</div>
<div class="col-lg-4">
    <div class="panel panel-default">
        <div class="panel-heading">
            <h3 class="panel-title">Подписаться на рассылку</h3>
        </div>
        <div class="panel-body">
            <form id="formSubscribe">
                <div class="form-group">
                    <input type="text" class="form-control"  placeholder="Имя" name="name">
                    <p class="help-block help-block-error hide">Это поле должно быть заполнено обязательно</p>
                </div>
                <div class="form-group">
                    <input type="email" class="form-control"  placeholder="Email" name="email">
                    <p class="help-block help-block-error hide">Это поле должно быть заполнено обязательно</p>
                </div>
                <button type="button" class="btn btn-default" style="width: 100%;" id="formSubscribeSubmit">Подписаться</button>
            </form>
        </div>
    </div>
</div>
<div class="col-lg-4">
    <p><a href="/anketa/new">Подключиться в команду, заполнить анкету</a></p>
</div>
<div class="col-lg-4">
    <p><a href="https://www.youtube.com/watch?v=7gVXareaBME" target="_blank">Заработать на нашем проекте</a></p>
    <p><a href="http://galaxysss.ru/shop/category/20" target="_blank">Получить карту с Эликсирами</a></p>
</div>
<div class="col-lg-4">
    <p><a href="/auth/registration">Регистрация</a></p>
</div>
<div class="col-lg-4">
    <p><a href="/site/about">О нашем проекте</a></p>
    <p><a href="/news/item?id=2">Презентация</a></p>
</div>
