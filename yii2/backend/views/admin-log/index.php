<?php

/** $this \yii\web\View  */

use yii\helpers\Url;
use app\services\GsssHtml;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use app\services\GetArticle\Collection;

$this->title = 'server_request';


?>

<div class="container">
    <div class="col-lg-12">
        <h1 class="page-header"><?= Html::encode($this->title) ?></h1>


        <?php \yii\widgets\Pjax::begin(); ?>
        <?php
        $this->registerJS(<<<JS
$('[data-toggle="tooltip"]').tooltip();

JS
        );
        ?>
        <?= \yii\grid\GridView::widget([
            'dataProvider' => new \yii\data\ActiveDataProvider([
                'query'      => \common\models\statistic\ServerRequest::find()
                ,
                'pagination' => [
                    'pageSize' => 20,
                ],
                'sort' => ['defaultOrder' => ['id' => SORT_DESC]],
            ]),
            'tableOptions' => [
                'class' => 'table table-striped table-hover',
            ],
            'rowOptions'   => function ($item) {
                $data = [
                    'data'  => ['id' => $item['id']],
                    'role'  => 'button',
                    'class' => 'rowTable'
                ];
                return $data;
            },
            'columns'      => [
                'id',
                [
                    'header'  => 'mode',
                    'content' => function (\common\models\statistic\ServerRequest $item) {
                        if ($item->mode == \common\models\statistic\ServerRequest::MODE_GET) return Html::tag('span', 'GET', ['class' => 'label label-warning']);
                        if ($item->mode == \common\models\statistic\ServerRequest::MODE_POST) return Html::tag('span', 'POST', ['class' => 'label label-info']);
                        if ($item->mode == \common\models\statistic\ServerRequest::MODE_HEAD) return Html::tag('span', 'HEAD', ['class' => 'label label-danger']);
                        if ($item->mode == \common\models\statistic\ServerRequest::MODE_OPTIONS) return Html::tag('span', 'OPTIONS', ['class' => 'label label-danger']);
                        return '';
                    }
                ],
                [
                    'header'  => 'uri',
                    'content' => function (\common\models\statistic\ServerRequest $item) {
                        $u = parse_url($item->uri);
                        return Html::a($u['path'], Url::to($u['path'], true));
                    }
                ],
                [
                    'header'  => 'get',
                    'content' => function (\common\models\statistic\ServerRequest $item) {
                        if ($item->id >= 256237) return \yii\helpers\VarDumper::dumpAsString(unserialize($item->get), 10, true);
                        return \yii\helpers\VarDumper::dumpAsString($item->get, 10, true);
                    }
                ],

                [
                    'header'  => 'post',
                    'content' => function (\common\models\statistic\ServerRequest $item) {
                        if ($item->id >= 256237) return \yii\helpers\VarDumper::dumpAsString(unserialize($item->post), 10, true);
                        return \yii\helpers\VarDumper::dumpAsString($item->post, 10, true);
                    }
                ],
                [
                    'header'  => 'server',
                    'content' => function (\common\models\statistic\ServerRequest $item) {
                        if ($item->id >= 256237) {
                            return Html::button('Посмотреть', ['class' => 'btn btn-default btn-xs buttonServer', 'data' => ['id' => $item->id]]);
//                            return \yii\widgets\DetailView::widget([
//                                'model' => unserialize($item->server)
//                            ]);
                        }
                        return \yii\helpers\VarDumper::dumpAsString($item->server, 10, true);
                    }
                ],
                [
                    'header'  => 'is_bot',
                    'content' => function (\common\models\statistic\ServerRequest $item) {
                        if ($item->id >= 256237) {
                            $server = unserialize($item->server);
                            if (!isset($server['HTTP_USER_AGENT'])) return 'Alarm';
                            $u = $server['HTTP_USER_AGENT'];
                            $result = \common\models\school\Page::isBot($u);

                            if ($result) return Html::tag('span', 'Да', ['class' => 'label label-success']);
                            return Html::tag('span', 'Нет', ['class' => 'label label-default']);
                        }
                        return '';
                    }
                ],
                [
                    'header'  => 'session_id',
                    'content' => function (\common\models\statistic\ServerRequest $item) {
                        return Html::tag('code', $item->session_id);
                    }
                ],
                [
                    'header'  => 'session',
                    'content' => function (\common\models\statistic\ServerRequest $item) {
                        if ($item->id >= 256237) return \yii\helpers\VarDumper::dumpAsString(unserialize($item->session), 10, true);
                        return \yii\helpers\VarDumper::dumpAsString($item->session, 10, true);
                    }
                ],
                [
                    'header'  => 'log_time',
                    'content' => function (\common\models\statistic\ServerRequest $item) {
                        $t = (int)$item->log_time;

                        return Html::tag('abbr', \cs\services\DatePeriod::back($t, ['isShort' => true]), ['class' => 'gsssTooltip', 'title' => Yii::$app->formatter->asDatetime($t)]);
                    }
                ],
                'ip',
                'user_id',
                [
                    'header'  => 'row_text',
                    'content' => function (\common\models\statistic\ServerRequest $item) {
                        if (\cs\Application::isEmpty($item->row_text)) return '';
                        return Html::button('Посмотреть', ['class' => 'btn btn-default btn-xs buttonHeaders', 'data' => ['id' => $item->id]]);
                    }
                ],
                [
                    'header'  => 'headers',
                    'content' => function (\common\models\statistic\ServerRequest $item) {
                        if ($item->id >= 256237) {
                            return Html::button('Посмотреть', ['class' => 'btn btn-default btn-xs buttonHeaders', 'data' => ['id' => $item->id]]);
//                            return \yii\widgets\DetailView::widget([
//                                'model' => unserialize($item->server)
//                            ]);
                        }
                        return \yii\helpers\VarDumper::dumpAsString($item->headers, 10, true);
                    }
                ],

            ],
        ]) ?>
        <?php \yii\widgets\Pjax::end(); ?>
    </div>
</div>