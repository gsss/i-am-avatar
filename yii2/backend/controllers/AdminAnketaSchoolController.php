<?php

namespace backend\controllers;

use common\models\Anketa;
use common\models\AnketaSchool;
use common\models\UserAvatar;
use Yii;
use yii\web\Response;

class AdminAnketaSchoolController extends AdminBaseController
{
    public function actionIndex()
    {
        return $this->render([]);
    }

    public function actionView($id)
    {
        $model = AnketaSchool::findOne($id);


        return $this->render([
            'model' => $model,
        ]);
    }


    public function actionDelete($id)
    {
        $model = Anketa::findOne($id);
        $model->delete();

        return self::jsonSuccess();
    }
}
