<?php

namespace backend\controllers;

use avatar\controllers\AdminBaseController;
use avatar\models\forms\BlogItem;
use avatar\models\forms\UserAvatar;
use avatar\modules\UniSender\UniSender;
use common\models\piramida\Login;
use common\models\school\SubscribeItem;
use common\models\UserRoot;
use cs\models\Tables\Client;
use cs\services\Security;
use cs\services\VarDumper;
use cs\web\Exception;
use Yii;
use yii\base\UserException;
use yii\filters\AccessControl;
use yii\helpers\Json;
use yii\helpers\Url;
use yii\httpclient\Client as ClientHttp;
use yii\web\Controller;
use yii\filters\VerbFilter;
use cs\base\BaseController;
use yii\web\Response;
use yii\widgets\ActiveForm;

class AdminApiLoginController extends AdminBaseController
{
    public function actionIndex()
    {
        return $this->render();
    }

    public function actionAdd()
    {
        $model = new Login();

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $model->login = Security::generateRandomString(64);
            $model->secret = Security::generateRandomString(64);
            $model->created_at = time();
            $model->save();
            Yii::$app->session->setFlash('form', 1);
        }

        return $this->render([
            'model' => $model,
        ]);
    }


    public function actionEdit($id)
    {
        $model = Login::findOne($id);

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $model->save();
            Yii::$app->session->setFlash('form', $id);
        }

        return $this->render([
            'model' => $model,
        ]);
    }


    public function actionDelete($id)
    {
        Login::findOne($id)->delete();

        return self::jsonSuccess();
    }
}
