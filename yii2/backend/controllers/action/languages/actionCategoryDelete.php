<?php

namespace backend\controllers\action\languages;

use common\models\language\Category;
use common\models\language\Message;
use common\models\language\SourceMessage;
use common\services\Debugger;
use yii\base\Action;

/**
 * Created by PhpStorm.
 * User: s.arhangelskiy
 * Date: 18.01.2017
 * Time: 13:13
 */
class actionCategoryDelete extends \avatar\controllers\action\BaseAction
{
    public function run()
    {
        $id = self::getParam('id');
        $category = Category::findOne($id);
        if (is_null($category)) {
            return self::jsonErrorId(101, 'Не найдена категория');
        }

        $ids = SourceMessage::find()
            ->where([
                'category' => $category->code,
                'type'     => 2,
            ])
            ->select(['id'])
            ->column();
        $rows = Message::find()->where(['in', 'id', $ids])->all();
        $path = \backend\models\form\LanguageImageSave::$pathSave1;
        /** @var \common\models\language\Message $messageObject */
        foreach ($rows as $messageObject) {
            $pathFull = \Yii::getAlias($path . $messageObject->translation);
            $info = pathinfo($messageObject->translation);
            $t_path = $info['dirname'] . '/' . substr($info['basename'], 0, 33) . 't' . '.' . $info['extension'];
            $t_pathFull = \Yii::getAlias($path . $t_path);
            if (file_exists($pathFull)) {
                unlink($pathFull);
            }
            if (file_exists($t_pathFull)) {
                unlink($t_pathFull);
            }
        }

        $ids = SourceMessage::find()
            ->where([
                'category' => $category->code,
            ])
            ->select(['id'])
            ->column()
        ;

        SourceMessage::deleteAll(['in', 'id', $ids]);
        Message::deleteAll(['in', 'id', $ids]);
        $category->delete();

        return self::jsonSuccess();
    }
}