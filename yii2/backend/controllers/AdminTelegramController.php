<?php

namespace backend\controllers;

use common\models\Anketa;
use common\models\task\Task;
use cs\services\VarDumper;
use Yii;
use yii\filters\AccessControl;
use yii\helpers\Json;
use yii\httpclient\Client;
use yii\web\Response;

class AdminTelegramController extends AdminBaseController
{
    public function actionIndex()
    {
        return $this->render([]);
    }

    public function actionSubscribe()
    {
        $model = new \avatar\models\forms\school\AdminTelegram();

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $model->action();
            Yii::$app->session->setFlash('form', 1);
        }

        return $this->render(['model' => $model]);
    }

    /**
     */
    public function actionSetCallBack()
    {
        $path = self::getParam('path');

        $client = new Client();
        /** @var \aki\telegram\Telegram $telegram */
        $telegram = Yii::$app->telegram;

        $url = "https://api.telegram.org/bot" . $telegram->botToken . "/setWebhook";

        $response = $client->createRequest()
            ->setMethod('POST')
            ->setUrl($url)
            ->addData(['url' => 'https://www.i-am-avatar.com/telegram/call-back'])
            ->addFile('certificate', $path)
            ->send();

        return self::jsonSuccess(Json::decode($response->content));
    }

    /**
     */
    public function actionGetWebhookInfo()
    {
        $client = new Client();
        /** @var \aki\telegram\Telegram $telegram */
        $telegram = Yii::$app->telegram;

        $url = "https://api.telegram.org/bot" . $telegram->botToken . "/getWebhookInfo";

        $response = $client->createRequest()
            ->setMethod('POST')
            ->setUrl($url)
            ->send();

        return self::jsonSuccess(Json::decode($response->content));
    }

    /**
     */
    public function actionRemoveCallBack()
    {
        $client = new Client();
        /** @var \aki\telegram\Telegram $telegram */
        $telegram = Yii::$app->telegram;

        $url = "https://api.telegram.org/bot" . $telegram->botToken . "/setWebhook";

        $response = $client->createRequest()
            ->setMethod('POST')
            ->setUrl($url)
            ->send();

        return self::jsonSuccess(Json::decode($response->content));
    }
}
