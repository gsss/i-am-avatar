<?php

namespace backend\controllers;

use app\services\Subscribe;
use avatar\controllers\AdminBaseController;
use avatar\models\forms\BlogItem;
use avatar\models\forms\UserAvatar;
use avatar\modules\UniSender\UniSender;
use common\models\school\SubscribeItem;
use common\models\UserRoot;
use cs\models\Tables\Client;
use cs\services\VarDumper;
use cs\web\Exception;
use Yii;
use yii\base\UserException;
use yii\filters\AccessControl;
use yii\helpers\Json;
use yii\helpers\Url;
use yii\httpclient\Client as ClientHttp;
use yii\web\Controller;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use app\models\NewsItem;
use app\models\Chenneling;
use cs\base\BaseController;
use yii\web\Response;
use yii\widgets\ActiveForm;
use app\models\SiteUpdate;

class AdminHdController extends AdminBaseController
{


    public function actionIndex()
    {
        return $this->render();
    }

    public function actionAdd()
    {
        $model = new \avatar\models\forms\HdGen();

        if ($model->load(Yii::$app->request->post()) && $model->insert()) {
            Yii::$app->session->setFlash('form', $model->id);
        }

        return $this->render([
            'model' => $model,
        ]);
    }


    public function actionEdit($id)
    {
        $model = BlogItem::findOne($id);

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $model->update();
            Yii::$app->session->setFlash('form', $id);
        }

        return $this->render([
            'model' => $model,
        ]);
    }

    /**
     * AJAX
     * Формирует данные для рассылки
     *
     * @param integer $id - идентификатор послания
     *
     * @return string
     */
    public function actionSubscribe($id)
    {
        $item = \common\models\BlogItem::findOne($id);
        if (is_null($item)) {
            return self::jsonErrorId(101, 'Не найдена статья');
        }

        $emailList = UserRoot::find()
            ->innerJoin('school_potok_user_root_link', 'school_potok_user_root_link.user_root_id = user_root.id and school_potok_user_root_link.potok_id = 2')
            ->andWhere(['school_potok_user_root_link.is_unsubscribed' => 0])
            ->select(['user_root.email'])
            ->asArray()
            ->column();

        $subscribeProcess = \common\services\Subscribe::sendArray(
            $emailList,
            $item->name,
            'subscribe/blog-item',
            ['newsItem' => $item],
            'layouts/html/subscribe'
        );

        $item->is_send = 1;
        $item->save();

        return self::jsonSuccess();
    }

    /**
     * AJAX
     * Формирует данные для рассылки
     *
     * @param integer $id - идентификатор послания
     *
     * @return string
     */
    public function actionSubscribeOne($id)
    {
        $item = \common\models\BlogItem::findOne($id);
        if (is_null($item)) {
            return self::jsonErrorId(101, 'Не найдена статья');
        }

        $emailList = ['dram1008@yandex.ru'];

        $subscribeProcess = \common\services\Subscribe::sendArray(
            $emailList,
            $item->name,
            'subscribe/blog-item',
            ['newsItem' => $item],
            'layouts/html/subscribe'
        );

        return self::jsonSuccess();
    }

    public function actionDelete($id)
    {
        BlogItem::findOne($id)->delete();

        return self::jsonSuccess();
    }
}
