<?php

namespace backend\controllers;

use avatar\base\Application;
use avatar\models\search\UserAvatar;
use common\models\avatar\UserBill;
use common\models\avatar\UserBillSystem;
use console\controllers\MoneyRateController;
use cs\services\VarDumper;
use cs\web\Exception;
use Yii;
use yii\filters\AccessControl;
use yii\helpers\FileHelper;
use yii\helpers\Html;
use yii\rbac\Role;
use yii\web\Response;

class AdminUsersController extends \avatar\controllers\AdminBaseController
{
    public static $isStatistic = false;

    /**
     */
    public function actionIndex()
    {
        return $this->render();
    }

    /**
     */
    public function actionPermissions($id)
    {
        return $this->render(['model' => \common\models\UserAvatar::findOne($id)]);
    }

    /**
     * REQUEST:
     * - name
     * - value
     */
    public function actionPermissionsSet()
    {
        $id = self::getParam('id');
        $name = self::getParam('name');
        $value = self::getParam('value');
        if ($value == 'true') {
            Yii::$app->authManager->assign(new Role(['name' => $name]), $id);
        } else {
            Yii::$app->authManager->revoke(new Role(['name' => $name]), $id);
        }

        return self::jsonSuccess();
    }

    /**
     * REQUEST:
     * - name
     * - value
     */
    public function actionRoleUsers()
    {
        $name = self::getParam('name');

        return $this->render(['name' => $name]);
    }

    /**
     * @param int $id идентификатор пользователя
     * @return string
     * @throws
     */
    public function actionWalletList($id)
    {
        if (!Application::isInteger($id)) {
            throw new Exception('id должен быть INT');
        }
        try {
            $user = \common\models\UserAvatar::findOne($id);
        } catch (\Exception $e) {
            throw new Exception('Не найден пользователь');
        }

        return $this->render(['user' => $user]);
    }

    /**
     */
    public function actionBinanceView($id)
    {
        return $this->render(['user' => \common\models\UserAvatar::findOne($id)]);
    }

    /**
     */
    public function actionEdit($id)
    {
        $model = \avatar\models\forms\UserAvatar::findOne($id);

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            Yii::$app->session->setFlash('form',1 );
            $model->update();

            return $this->refresh();
        } else {
            return $this->render([
                'model' => $model
            ]);
        }
    }
}
