<?php

namespace backend\controllers;

use Yii;

class AdminCardDesignController extends AdminBaseController
{

    public function actionIndex()
    {
        return $this->render();
    }

    public function actionAdd()
    {
        $model = new \avatar\models\forms\CardDesign();

        if ($model->load(Yii::$app->request->post()) && $model->insert()) {
            Yii::$app->session->setFlash('form', $model->id);
            $i = \common\models\BlogItem::findOne($model->id);
            $i->created_at = time();
            $i->save();
        }

        return $this->render([
            'model' => $model,
        ]);
    }

    public function actionEdit($id)
    {
        $model = \avatar\models\forms\CardDesign::findOne($id);
        if ($model->load(Yii::$app->request->post()) && $model->update()) {
            Yii::$app->session->setFlash('form', 1);
        }

        return $this->render([
            'model' => $model,
        ]);
    }

    public function actionDelete($id)
    {
        \avatar\models\forms\CardDesign::findOne($id)->delete();

        return self::jsonSuccess();
    }
}
