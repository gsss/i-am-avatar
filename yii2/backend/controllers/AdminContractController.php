<?php

namespace backend\controllers;

use app\services\Subscribe;
use avatar\controllers\AdminBaseController;
use avatar\models\forms\BlogItem;
use avatar\models\forms\UserAvatar;
use cs\models\Tables\Client;
use cs\services\VarDumper;
use cs\web\Exception;
use Yii;
use yii\base\UserException;
use yii\filters\AccessControl;
use yii\helpers\Json;
use yii\helpers\Url;
use yii\httpclient\Client as ClientHttp;
use yii\web\Controller;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use app\models\NewsItem;
use app\models\Chenneling;
use cs\base\BaseController;
use yii\web\Response;
use yii\widgets\ActiveForm;
use app\models\SiteUpdate;

class AdminContractController extends AdminBaseController
{
    public function actionRegister()
    {
        $model = new \avatar\models\forms\ContractRegistration();

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $txid = $model->action();
            Yii::$app->session->setFlash('form', $txid);
        }

        return $this->render([
            'model' => $model,
        ]);
    }

    public function actionCode()
    {
        $model = new \avatar\models\forms\ContractCode();

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $data = $model->action();
            Yii::$app->session->setFlash('form', $data);
        }

        return $this->render([
            'model' => $model,
        ]);
    }


    public function actionIndex()
    {
        return $this->render();
    }

    public function actionAdd()
    {
        $model = new \avatar\models\forms\Contract();

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $model->insert();
            Yii::$app->session->setFlash('form', $model->id);
        }

        return $this->render([
            'model' => $model,
        ]);
    }

    public function actionEdit($id)
    {
        $model = \avatar\models\forms\Contract::findOne($id);

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $model->update();
            Yii::$app->session->setFlash('form', $id);
        }

        return $this->render([
            'model' => $model,
        ]);
    }

    public function actionDelete($id)
    {
        \avatar\models\forms\Contract::findOne($id)->delete();

        return self::jsonSuccess();
    }
}
