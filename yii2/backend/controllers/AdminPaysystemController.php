<?php

namespace backend\controllers;

use app\models\Article;
use app\models\SiteUpdate;
use app\services\Subscribe;
use common\models\avatar\UserBill;
use cs\base\BaseController;
use cs\services\UploadFolderDispatcher;
use cs\services\VarDumper;
use cs\web\Exception;
use Yii;
use yii\base\UserException;
use yii\bootstrap\ActiveForm;
use yii\db\Connection;
use yii\db\Query;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use yii\helpers\FileHelper;
use yii\web\Response;
use avatar\models\forms\PaySystem;

class AdminPaysystemController extends AdminBaseController
{
    public $type_id = 14;
    /**
     */
    public function actionIndex()
    {
        return $this->render();
    }

    public function actionAdd()
    {
        $model = new PaySystem(['_school_id' => 2, '_type_id' => $this->type_id]);
        if ($model->load(Yii::$app->request->post()) && $model->insert()) {
            Yii::$app->session->setFlash('form', $model->id);
        }

        return $this->render([
            'model' => $model,
        ]);
    }

    public function actionEdit()
    {
        $id = self::getParam('id');

        /** @var \avatar\models\forms\PaySystem $model */
        $model = PaySystem::findOne($id);
        $model->_school_id = 2;
        $model->_type_id = $this->type_id;

        if ($model->load(Yii::$app->request->post()) && $model->update()) {
            Yii::$app->session->setFlash('form', $model->id);
        }

        return $this->render([
            'model' => $model,
        ]);
    }

    public function actionDelete()
    {
        $id = self::getParam('id');
        PaySystem::findOne($id)->delete();

        return self::jsonSuccess();
    }
}
