<?php

namespace backend\controllers;

use app\services\Subscribe;
use avatar\controllers\AdminBaseController;
use avatar\models\forms\BlogItem;
use avatar\models\forms\UserAvatar;
use common\models\piramida\Currency;
use cs\models\Tables\Client;
use cs\services\VarDumper;
use cs\web\Exception;
use Yii;
use yii\base\UserException;
use yii\filters\AccessControl;
use yii\helpers\Json;
use yii\helpers\Url;
use yii\httpclient\Client as ClientHttp;
use yii\web\Controller;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use app\models\NewsItem;
use app\models\Chenneling;
use cs\base\BaseController;
use yii\web\Response;
use yii\widgets\ActiveForm;
use app\models\SiteUpdate;

class AdminCurrencyIntController extends AdminBaseController
{
    public function actionIndex()
    {
        return $this->render();
    }

    public function actionAdd()
    {
        $model = new Currency();

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $model->amount = 0;
            $model->save();
            $model->id = $model::getDb()->lastInsertID;
            Yii::$app->session->setFlash('form', $model->id);
        }


        return $this->render([
            'model' => $model,
        ]);
    }

    public function actionEdit($id)
    {
        $model = Currency::findOne($id);
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            Yii::$app->session->setFlash('form', 1);
            $model->save();

        }

        return $this->render([
            'model' => $model,
        ]);
    }

    public function actionDelete($id)
    {
        Currency::findOne($id)->delete();

        return self::jsonSuccess();
    }
}
