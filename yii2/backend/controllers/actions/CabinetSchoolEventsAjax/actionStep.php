<?php

namespace backend\controllers\actions\CabinetSchoolEventsAjax;

/**
 * Created by PhpStorm.
 * User: s.arhangelskiy
 * Date: 08.08.2017
 * Time: 17:58
 */

use avatar\models\WalletETH;
use avatar\models\WalletToken;
use common\models\avatar\Currency;
use common\models\avatar\UserBill;
use common\models\BillingMain;
use common\models\comment\Comment;
use common\models\comment\CommentList;
use common\models\investment\IcoRequest;
use common\models\PaySystemConfig;
use common\models\piramida\Wallet;
use common\models\school\School;
use common\models\task\Task;
use common\models\Token;
use common\models\UserAvatar;
use common\models\UserWallet;
use common\services\Subscribe;
use cs\services\File;
use Yii;
use yii\base\Exception;
use yii\base\Model;
use yii\bootstrap\ActiveForm;
use yii\helpers\FileHelper;
use yii\imagine\Image;
use yii\web\Response;

/**
 * REQUEST
 * - id
 */
class actionStep extends \avatar\base\BaseAction
{
    public $num;

    /** @var string */
    public $model;

    public $school_id;

    public function run()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $data = Yii::$app->session->get('event', []);

        $name = $this->model;
        /** @var \avatar\models\forms\shop\EventAdd\StepBase $model */
        $model = new $name();
        $model->school_id = $this->school_id;
        if ($model->load(Yii::$app->request->post())) {
            $errors = ActiveForm::validate($model);
            if (count($errors) == 0) {

                $data[$model->formName()] = Yii::$app->request->post($model->formName());
                Yii::$app->session->set('event', $data);

                return self::jsonSuccess();

            } else {
                return self::jsonErrorId(102, $errors);
            }
        }

        return self::jsonSuccess();
    }
}