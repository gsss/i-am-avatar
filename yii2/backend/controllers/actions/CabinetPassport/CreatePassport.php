<?php

namespace backend\controllers\actions\CabinetPassport;

/**
 * Created by PhpStorm.
 * User: s.arhangelskiy
 * Date: 08.08.2017
 * Time: 17:58
 */

use avatar\models\WalletETH;
use avatar\models\WalletToken;
use common\models\avatar\Currency;
use common\models\BillingMain;
use common\models\investment\IcoRequest;
use common\models\PaySystemConfig;
use common\models\Token;
use common\models\UserAvatar;
use cs\services\File;
use Yii;
use yii\base\Exception;
use yii\helpers\FileHelper;
use yii\imagine\Image;

/**
 * AJAX
 * REQUEST:
 * - txid - string
 *
 * Class CreatePassport
 *
 */
class CreatePassport extends \avatar\base\BaseAction
{
    public $txid;

    public function init()
    {
        $this->txid = self::getParam('txid');
    }

    public function run()
    {
        // уменьшение
        $original_path = "/upload/FileUpload3/user/00000009/small/avatar.jpg";
        $original_path = Yii::getAlias('@common/../public_html/' . $original_path);
        $small = Image::thumbnail($original_path,120,120);
        $file = File::content($small->get('PNG'));

        // обрезка круглой
        $dest_path = "/upload/FileUpload3/user/00000009/small/avatar22.png";
        $dest_path = Yii::getAlias('@common/../public_html/' . $dest_path);
        $d = $this->getCircleImage($file, $dest_path, [120,120]);

        $background = Yii::getAlias('@avatar/modules/PassportGenerate/images/AvatarPassport.jpg');

        $new = $this->watermark($background, [[
            'file'  => $dest_path,
            'start' => [220,88],
        ]]);

        return Yii::$app->response->sendFile($new['path']);
    }


    /**
     * @param string $file полный путь к файлу
     * @param array  $watermark
     *                     [[
     *                     'file' => string - полный путь | array [
     *                          'format'  => string - расширение возможно jpg png
     *                          'content' => string - содержимое
     *                     ]
     *                          'start' => array [x, y] - точка от верхнего левого угла оригинальной картинки
     *                     ],...]
     *
     * @return array
     * [
     *      'img'  => \Imagine\Image\ImageInterface
     *      'path' => string - полный путь к файлу png
     * ]
     *
     * @throws \yii\base\Exception
     */
    private function watermark($file, $watermark)
    {
        if (count($watermark) == 0) {
            throw new Exception('Не задан массив $watermark');
        }
        $tempFilePath = \Yii::getAlias('@runtime/watermark');
        FileHelper::createDirectory($tempFilePath);
        $background = $file;
        $image = null;
        foreach ($watermark as $watermarkConfig) {
            if (is_array($watermarkConfig['file'])) {
                $temp = $tempFilePath . DIRECTORY_SEPARATOR . time() . '_' . Yii::$app->security->generateRandomString(10) . '.' . $watermarkConfig['file']['format'];
                file_put_contents($temp, $watermarkConfig['file']['content']);
            } else {
                $temp = $watermarkConfig['file'];
            }
            $image = Image::watermark($background, $temp, $watermarkConfig['start']);
            $background = $tempFilePath . DIRECTORY_SEPARATOR . time() . '_' . Yii::$app->security->generateRandomString(10) . '.' . 'png';
            file_put_contents($background, $image->get('png'));
        }

        return [
            'img'  => $image,
            'path' => $background,
        ];
    }

    /**
     * Делает картинку круглой с прозрачным фоном в PNG
     *
     * @param string | \cs\services\File $source
     * @param array $box
     */
    private function getCircleImage($source, $dest, $box)
    {
        $w = $box[0];
        $h = $box[1]; // original size
        if ($source instanceof File) {
            /** @var \cs\services\File $file */
            $file = $source;
            $content = $file->content;
        } else {
            $content = file_get_contents($source);
        }
        $src = imagecreatefromstring($content);
        $newpic = imagecreatetruecolor($w, $h);
        imagealphablending($newpic, false);
        $transparent = imagecolorallocatealpha($newpic, 0, 0, 0, 127);
        $r = $w / 2;
        for ($x = 0; $x < $w; $x++) {
            for ($y = 0; $y < $h; $y++) {
                $c = imagecolorat($src, $x, $y);
                $_x = $x - $w / 2;
                $_y = $y - $h / 2;
                if ((($_x * $_x) + ($_y * $_y)) < ($r * $r)) {
                    imagesetpixel($newpic, $x, $y, $c);
                } else {
                    imagesetpixel($newpic, $x, $y, $transparent);
                }
            }
        }
        imagesavealpha($newpic, true);
        imagepng($newpic, $dest);
        imagedestroy($newpic);
        imagedestroy($src);
    }

}