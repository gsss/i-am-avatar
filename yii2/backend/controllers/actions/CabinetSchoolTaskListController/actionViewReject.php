<?php

namespace backend\controllers\actions\CabinetSchoolTaskListController;

/**
 * Created by PhpStorm.
 * User: s.arhangelskiy
 * Date: 08.08.2017
 * Time: 17:58
 */

use avatar\models\WalletETH;
use avatar\models\WalletToken;
use common\models\avatar\Currency;
use common\models\avatar\UserBill;
use common\models\BillingMain;
use common\models\comment\Comment;
use common\models\comment\CommentList;
use common\models\investment\IcoRequest;
use common\models\PaySystemConfig;
use common\models\piramida\Wallet;
use common\models\school\School;
use common\models\task\Task;
use common\models\Token;
use common\models\UserAvatar;
use common\models\UserWallet;
use common\services\Subscribe;
use cs\services\File;
use Yii;
use yii\base\Exception;
use yii\helpers\FileHelper;
use yii\imagine\Image;

/**
 * REQUEST
 * - id
 */
class actionViewReject extends \avatar\base\BaseAction
{

    public function run()
    {
        $task = Task::findOne(self::getParam('id'));
        $statusList = \common\models\task\Status::find()
            ->where(['school_id' => $task->school_id])
            ->orderBy(['sort_index' => SORT_ASC])
            ->select('id')
            ->column();

        $task->status = $statusList[0];
        $task->save();
        $list = CommentList::get(1, $task->id);
        $reason = self::getParam('text');

        $comment = Comment::add([
            'text'    => 'Отклонено. Причина:' . $reason,
            'list_id' => $list->id,
            'user_id' => Yii::$app->user->id,
        ]);

        // Сообщить исполнителю что все готово
        Subscribe::sendArraySchool($task->getSchool(), [\common\models\UserAvatar::findOne($task->executer_id)->email], 'Задача отклонена', 'task/reject', [
            'task'   => $task,
            'reason' => $reason,
        ], 'layouts/html/task');

        return self::jsonSuccess($comment);
    }
}