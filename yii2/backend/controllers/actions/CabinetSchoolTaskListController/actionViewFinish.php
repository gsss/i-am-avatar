<?php

namespace backend\controllers\actions\CabinetSchoolTaskListController;

/**
 * Created by PhpStorm.
 * User: s.arhangelskiy
 * Date: 08.08.2017
 * Time: 17:58
 */

use avatar\models\WalletETH;
use avatar\models\WalletToken;
use common\models\avatar\Currency;
use common\models\avatar\UserBill;
use common\models\BillingMain;
use common\models\comment\Comment;
use common\models\comment\CommentList;
use common\models\investment\IcoRequest;
use common\models\PaySystemConfig;
use common\models\piramida\Wallet;
use common\models\school\ProjectManagerLink;
use common\models\school\School;
use common\models\task\Session;
use common\models\task\Task;
use common\models\Token;
use common\models\UserAvatar;
use common\models\UserWallet;
use common\services\Subscribe;
use cs\services\File;
use Yii;
use yii\base\Exception;
use yii\helpers\FileHelper;
use yii\imagine\Image;

/**
 * REQUEST
 * - id
 */
class actionViewFinish extends \avatar\base\BaseAction
{
    public function run()
    {
        $task = Task::findOne(self::getParam('id'));
        $statusList = \common\models\task\Status::find()
            ->where(['school_id' => $task->school_id])
            ->orderBy(['sort_index' => SORT_ASC])
            ->select('id')
            ->column();

        $task->status = $statusList[1];
        $task->save();

        $comment = Comment::add([
            'text'    => 'Я сделал(а)!',
            'list_id' => CommentList::get(1, $task->id)->id,
            'user_id' => Yii::$app->user->id,
        ]);

        // Закрываю сессию если она открыта
        {
            /** @var \common\models\task\Session $session */
            $session = Session::find()->where(['task_id' => $task->id, 'is_finish' => 0])->one();
            if (!is_null($session)) {
                $session->finish = time();
                $session->is_finish = 1;
                $session->save();
            }
        }

        // Сообщаю PM что все готово
        $pmList = ProjectManagerLink::find()->where(['school_id' => $task->school_id])->select('user_id')->column();
        $pmListString = \common\models\UserAvatar::find()->where(['in', 'id', $pmList])->select('email')->column();
        Subscribe::sendArraySchool($task->getSchool(), $pmListString, 'Исполнитель выполнил задачу', 'task/finish', ['task' => $task], 'layouts/html/task');

        return self::jsonSuccess($comment);
    }
}