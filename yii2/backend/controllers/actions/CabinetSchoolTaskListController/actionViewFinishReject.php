<?php

namespace backend\controllers\actions\CabinetSchoolTaskListController;

/**
 * Created by PhpStorm.
 * User: s.arhangelskiy
 * Date: 08.08.2017
 * Time: 17:58
 */

use avatar\models\WalletETH;
use avatar\models\WalletToken;
use common\models\avatar\Currency;
use common\models\avatar\UserBill;
use common\models\BillingMain;
use common\models\comment\Comment;
use common\models\comment\CommentList;
use common\models\investment\IcoRequest;
use common\models\PaySystemConfig;
use common\models\piramida\Wallet;
use common\models\school\AdminLink;
use common\models\school\ProjectManagerLink;
use common\models\school\School;
use common\models\task\Session;
use common\models\task\Task;
use common\models\Token;
use common\models\UserAvatar;
use common\models\UserWallet;
use common\services\Subscribe;
use cs\services\File;
use Yii;
use yii\base\Exception;
use yii\helpers\FileHelper;
use yii\imagine\Image;

/**
 * REQUEST
 * - id
 */
class actionViewFinishReject extends \avatar\base\BaseAction
{
    public function run()
    {
        $task = Task::findOne(self::getParam('id'));
        $pmList = ProjectManagerLink::find()->where(['school_id' => $task->school_id])->select('user_id')->column();

        $task->status = null;
        $executer_id = $task->executer_id;
        $task->executer_id = null;
        $task->save();

        // Если исполнитель открепляет себя то уведомляется PM
        if ($executer_id == Yii::$app->user->id) {
            $pmListString = \common\models\UserAvatar::find()->where(['in', 'id', $pmList])->select('email')->column();
            Subscribe::sendArraySchool($task->getSchool(), $pmListString, 'Исполнитель открепил себя', 'task/finish-reject-pm', ['task' => $task], 'layouts/html/task');
            $comment = Comment::add([
                'text'    => 'Я снимаю с себя ответственность за задачу',
                'list_id' => CommentList::get(1, $task->id)->id,
                'user_id' => Yii::$app->user->id,
            ]);
        } else {
            Subscribe::sendArraySchool($task->getSchool(), [\common\models\UserAvatar::findOne($executer_id)->email], 'PM открепил тебя с задачи', 'task/finish-reject-executer', ['task' => $task], 'layouts/html/task');
            $comment = Comment::add([
                'text'    => 'Я снимаю с исполнителя задачу',
                'list_id' => CommentList::get(1, $task->id)->id,
                'user_id' => Yii::$app->user->id,
            ]);
        }

        return self::jsonSuccess($comment);
    }
}