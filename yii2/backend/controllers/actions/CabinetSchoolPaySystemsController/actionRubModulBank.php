<?php

namespace backend\controllers\actions\CabinetSchoolPaySystemsController;

/**
 * Created by PhpStorm.
 * User: s.arhangelskiy
 * Date: 08.08.2017
 * Time: 17:58
 */

use avatar\models\WalletETH;
use avatar\models\WalletToken;
use common\models\avatar\Currency;
use common\models\avatar\UserBill;
use common\models\BillingMain;
use common\models\comment\Comment;
use common\models\comment\CommentList;
use common\models\investment\IcoRequest;
use common\models\PaySystemConfig;
use common\models\piramida\Wallet;
use common\models\school\School;
use common\models\task\Task;
use common\models\Token;
use common\models\UserAvatar;
use common\models\UserWallet;
use common\services\Subscribe;
use cs\services\File;
use Yii;
use yii\base\Exception;
use yii\helpers\FileHelper;
use yii\imagine\Image;

/**
 * REQUEST
 * - id
 */
class actionRubModulBank extends \avatar\base\BaseAction
{

    public function run()
    {
        $config = PaySystemConfig::findOne(self::getParam('id'));
        $paySystem = $config->_getPayment();
        $school = School::findOne($config->school_id);

        $class = '\\avatar\\modules\\PaySystems\\models\\' . $paySystem->class_name;
        /** @var \yii\base\Model $model */
        $model = new $class();
        $model->setData($config->config);

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            Yii::$app->session->setFlash('form', 1);
            $config->config = $model->save();
            $config->save();
        }

        return $this->controller->render('@avatar/views/cabinet-school-pay-systems/settings-edit', [
            'view'      => '@avatar/modules/PaySystems/views/' . $paySystem->code,
            'config'    => $config,
            'paySystem' => $paySystem,
            'school'    => $school,
            'model'     => $model,
        ]);
    }
}