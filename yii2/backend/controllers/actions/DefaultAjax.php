<?php
/**
 * Created by PhpStorm.
 * User: s.arhangelskiy
 * Date: 18.01.2017
 * Time: 13:17
 */

namespace backend\controllers\actions;

use cs\services\VarDumper;
use Yii;
use yii\base\Action;
use yii\base\Model;
use yii\web\Response;

class DefaultAjax extends BaseAction
{
    public $model;
    public $formName;

    public function run()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        $class = $this->model;
        /** @var Model $model */
        $model = new $class();
        if (!$model->load(Yii::$app->request->post(), $this->formName)) {
            return self::jsonErrorId(400, 'Не загружены данные');
        }
        if (!$model->validate()) {
            return self::jsonErrorId(102, $model->errors);
        }

        return self::jsonSuccess($model->action());
    }
}