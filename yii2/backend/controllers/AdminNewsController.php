<?php

namespace backend\controllers;

use avatar\controllers\AdminBaseController;
use avatar\models\forms\NewsItem;
use avatar\models\forms\UserAvatar;
use avatar\modules\UniSender\UniSender;
use common\models\UserRoot;
use cs\Application;
use cs\services\VarDumper;
use cs\web\Exception;
use Yii;
use yii\base\UserException;
use yii\filters\AccessControl;
use yii\helpers\Json;
use yii\helpers\Url;
use yii\httpclient\Client;
use yii\web\Controller;
use yii\filters\VerbFilter;
use cs\base\BaseController;
use yii\web\Response;
use yii\widgets\ActiveForm;

class AdminNewsController extends AdminBaseController
{
    public $type_id = 13;

    public function behaviors()
    {

        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['role_content'],
                    ],
                ],
            ],
        ];
    }

    public function actionIndex()
    {
        return $this->render();
    }

    public function actionAdd()
    {
        $model = new NewsItem(['_school_id' => 2, '_type_id' => $this->type_id]);

        if ($model->load(Yii::$app->request->post()) && $model->insert()) {
            Yii::$app->session->setFlash('form', $model->id);
            $i = \common\models\NewsItem::findOne($model->id);
            $i->created_at = time();
            $i->save();

            return $this->refresh();
        } else {
            return $this->render([
                'model' => $model,
            ]);
        }
    }


    public function actionEdit($id)
    {
        $model = NewsItem::findOne($id);
        $model->_school_id = 2;
        $model->_type_id = $this->type_id;

        if ($model->load(Yii::$app->request->post()) && $model->update()) {
            Yii::$app->session->setFlash('form', $id);

            return $this->refresh();
        } else {
            return $this->render([
                'model' => $model,
            ]);
        }
    }

    /**
     * AJAX
     * Формирует данные для рассылки
     *
     * @param integer $id - идентификатор послания
     *
     * @return string
     */
    public function actionSubscribe($id)
    {
        $model = new \avatar\models\validate\AdminNewsControllerSubscribe();

        if (!$model->load(Yii::$app->request->get(), '')) {
            return self::jsonErrorId(400, 'Не загружены данные');
        }
        if (!$model->validate()) {
            return self::jsonErrorId(102, $model->errors);
        }
        $data = $model->action();

        return self::jsonSuccess(['item' => $data]);
    }

    /**
     * AJAX
     * Формирует данные для рассылки
     *
     * @param integer $id - идентификатор послания
     *
     * @return string
     */
    public function actionSubscribeOne($id)
    {
        $item = \common\models\NewsItem::findOne($id);
        if (is_null($item)) {
            return self::jsonErrorId(101, 'Не найдена статья');
        }

        $emailList = ['dram1008@yandex.ru'];

        $subscribeProcess = \common\services\Subscribe::sendArray(
            $emailList,
            $item->name,
            'subscribe/news-item',
            ['newsItem' => $item],
            'layouts/html/subscribe'
        );

        return self::jsonSuccess();
    }

    public function actionDelete($id)
    {
        NewsItem::findOne($id)->delete();

        return self::jsonSuccess();
    }
}
