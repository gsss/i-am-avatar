<?php

namespace backend\controllers;

use app\models\Article;
use app\models\SiteUpdate;
use app\services\Subscribe;
use common\models\avatar\UserBill;
use common\models\PaySystemConfig;
use common\models\PaySystemListItem;
use cs\base\BaseController;
use cs\services\UploadFolderDispatcher;
use cs\services\VarDumper;
use cs\web\Exception;
use Yii;
use yii\base\UserException;
use yii\bootstrap\ActiveForm;
use yii\db\Connection;
use yii\db\Query;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use yii\helpers\FileHelper;
use yii\web\Response;
use avatar\models\forms\PaySystem;

class AdminPaysystemConfigController extends AdminBaseController
{
    /**
     */
    public function actionIndex()
    {
        return $this->render();
    }

    public function actionAdd()
    {
        $model = new PaySystemListItem();
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        } else {
            return $this->render([
                'model' => $model,
            ]);
        }
    }

    public function actionEdit()
    {
        $id = self::getParam('id');
        /** @var \avatar\models\forms\PaySystem $model */
        $model = PaySystemListItem::findOne($id);
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        } else {
            return $this->render([
                'model' => $model,
            ]);
        }
    }

    public function actionDelete()
    {
        $id = self::getParam('id');
        PaySystemListItem::findOne($id)->delete();

        return self::jsonSuccess();
    }


    /**
     * Выводит список доставки
     *
     * @param int $id идентификатор объединения
     * @return string
     *
     * @throws \cs\web\Exception
     */
    public function actionItem()
    {
        $id = self::getParam('id');
        $listItem = PaySystemListItem::findOne($id);
        if (is_null($listItem)) {
            throw new Exception('Не найдено такое объединение');
        }

        return $this->render([
            'listItem' => $listItem,
        ]);
    }

    /**
     * Добавление доставки
     *
     * REQUEST:
     * - list_id - int - идентификатор объединения gs_unions.id
     * - paysystem_id - int - идентификатор платежной системы gs_unions.id
     *
     * @return string|\yii\web\Response
     * @throws \cs\web\Exception
     */
    public function actionItemEdit()
    {
        $list_id = self::getParam('list_id');
        $paysystem_id = self::getParam('paysystem_id');
        $fields = [
            'parent_id'    => $list_id,
            'paysystem_id' => $paysystem_id,
        ];
        $listItem = PaySystemListItem::findOne($list_id);
        $paymentConfig = PaySystemConfig::findOne($fields);
        if (is_null($paymentConfig)) {
            $paymentConfig = new PaySystemConfig($fields);
        }
        /** @var \common\models\PaySystem $paySystem */
        $paySystem = $paymentConfig->getPaySystem();
        $className = 'avatar\\modules\\Piramida\\PaySystems\\' . $paySystem->class_name . '\\Model';
        if (!class_exists($className)) {
            $className = 'avatar\\modules\\Piramida\\PaySystems\\Base\\Model';
        }
        /** @var \yii\base\Model $model */
        $model = new $className;
        if ($paymentConfig->config) {
            $model->setConfig($paymentConfig->config);
        }
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $paymentConfig->config = $model->getConfig();
            $ret = $paymentConfig->save();
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        } else {
            return $this->render('item-edit',[
                'model'    => $model,
                'listItem' => $listItem,
                'title'    => $paySystem->title,
                'class'    => $paySystem->class_name,
            ]);
        }
    }
}
