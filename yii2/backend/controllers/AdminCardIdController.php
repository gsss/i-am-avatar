<?php

namespace backend\controllers;

use avatar\controllers\AdminBaseController;
use avatar\models\forms\BlogItem;
use avatar\models\forms\UserAvatar;
use avatar\modules\UniSender\UniSender;
use common\models\CardId;
use common\models\school\SubscribeItem;
use common\models\UserRoot;
use cs\models\Tables\Client;
use cs\services\VarDumper;
use cs\web\Exception;
use Yii;
use yii\base\UserException;
use yii\filters\AccessControl;
use yii\helpers\Json;
use yii\helpers\Url;
use yii\httpclient\Client as ClientHttp;
use yii\web\Controller;
use yii\filters\VerbFilter;
use cs\base\BaseController;
use yii\web\Response;
use yii\widgets\ActiveForm;

class AdminCardIdController extends AdminBaseController
{
    public function actionIndex()
    {
        return $this->render();
    }

    public function actionAdd()
    {
        $model = new \avatar\models\forms\CardId(['_school_id' => 2]);

        if ($model->load(Yii::$app->request->post()) && $model->insert()) {
            Yii::$app->session->setFlash('form', $model->id);

            $i = CardId::findOne($model->id);
            $i->created_at = time();
            $i->save();
        }

        return $this->render([
            'model' => $model,
        ]);
    }

    public function actionEdit($id)
    {
        $model = \avatar\models\forms\CardId::findOne($id);
        $model->_school_id = 2;

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $model->update();
            Yii::$app->session->setFlash('form', $id);
        }

        return $this->render([
            'model' => $model,
        ]);
    }

    public function actionDelete($id)
    {
        \avatar\models\forms\CardId::findOne($id)->delete();

        return self::jsonSuccess();
    }
}
