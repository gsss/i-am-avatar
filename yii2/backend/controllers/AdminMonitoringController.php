<?php

namespace backend\controllers;

use common\models\statistic\ServerRequest;
use cs\services\VarDumper;
use cs\web\Exception;
use Yii;
use yii\base\UserException;
use yii\filters\AccessControl;
use yii\helpers\Url;
use yii\web\Controller;
use yii\filters\VerbFilter;
use cs\base\BaseController;
use yii\web\Response;
use yii\widgets\ActiveForm;
use yii\widgets\DetailView;

class AdminMonitoringController extends \avatar\controllers\AdminBaseController
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@', 'monitoring'],
                    ],
                ],
            ],
        ];
    }
    
    public function actionIndex()
    {
        return $this->render([]);
    }

    /**
     * POST:
     * - min
     * - max
     */
    public function actionIndexAjax()
    {
        $min = self::getParam('min');
        $max = self::getParam('max');
        $dateMin = new \DateTime($min);
        $dateMax = new \DateTime($max);

        Yii::$app->response->format = Response::FORMAT_JSON;

        return self::jsonSuccess(['html' => $this->renderAjax('index-ajax', ['min' => $dateMin, 'max' => $dateMax])]);
    }

    /**
     */
    public function actionIndexServer($id)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $row = ServerRequest::findOne($id);
        if (is_null($row)) {
            return self::jsonErrorId(404, 'Запись не найдена');
        }

        return self::jsonSuccess(['html' => DetailView::widget([
            'model' => unserialize($row->server)
        ])]);
    }

    /**
     */
    public function actionIndexRowText($id)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $row = ServerRequest::findOne($id);
        if (is_null($row)) {
            return self::jsonErrorId(404, 'Запись не найдена');
        }

        return self::jsonSuccess(['html' => \yii\helpers\VarDumper::dumpAsString(unserialize($row->row_text), 10, true)]);
    }

    /**
     */
    public function actionIndexPost($id)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $row = ServerRequest::findOne($id);
        if (is_null($row)) {
            return self::jsonErrorId(404, 'Запись не найдена');
        }

        return self::jsonSuccess(['html' => \yii\helpers\VarDumper::dumpAsString(unserialize($row->post), 10, true)]);
    }

    /**
     */
    public function actionIndexGet($id)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $row = ServerRequest::findOne($id);
        if (is_null($row)) {
            return self::jsonErrorId(404, 'Запись не найдена');
        }


        return self::jsonSuccess(['html' => DetailView::widget([
            'model' => unserialize($row->get)
        ])]);
    }

    /**
     */
    public function actionIndexHeaders($id)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $row = ServerRequest::findOne($id);
        if (is_null($row)) {
            return self::jsonErrorId(404, 'Запись не найдена');
        }
        /** @var \yii\web\HeaderCollection $data */
        $data = unserialize($row->headers);
        $rows = [];
        foreach ($data->getIterator() as $k => $v) {
            $rows[$k] = $v[0];
        }

        return self::jsonSuccess(['html' => DetailView::widget([
            'model' => $rows
        ])]);
    }

    /**
     */
    public function actionIndexSession($id)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $row = ServerRequest::findOne($id);
        if (is_null($row)) {
            return self::jsonErrorId(404, 'Запись не найдена');
        }

        return self::jsonSuccess(['html' => \yii\helpers\VarDumper::dumpAsString(unserialize($row->session), 10, true)]);
    }

    public function actionDb($table_schema)
    {
        return $this->render(['table_schema' => $table_schema]);
    }

    public function actionTimer()
    {
        return $this->render([]);
    }


    public function actionErrors()
    {
        return $this->render([]);
    }

    public function actionError()
    {
        $category = self::getParam('category');
        if (is_null($category)) {
            throw new Exception('Нет параметра error');
        }
        return $this->render([
            'category' => $category
        ]);
    }

    /**
     * @return array
     */
    public function actionAjax()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        return [
            'online' => Yii::$app->onlineManager->count(),
            'rpm'    => (float)Yii::$app->formatter->asDecimal(((Yii::$app->monitoring->calculate()['request_per_second']) * 60), 1),
        ];
    }

    /**
     * Обноляет лимит пользователей онлайн
     *
     * REQUEST:
     * - limit - int
     *
     * @return string
     */
    public function actionIndexUpdateLimit()
    {
        $limit = Yii::$app->request->post('limit', '');
        if ($limit != '') {
            Yii::$app->onlineManager->setLimit($limit);
        } else {
            Yii::$app->onlineManager->setLimit(null);
        }

        return self::jsonSuccess();
    }

    /**
     *
     */
    public function actionReset()
    {
        Yii::$app->monitoring->reset();
        VarDumper::dump('Счетчик мониторинга сброшен');
    }
}
