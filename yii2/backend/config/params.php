<?php

return [
    'adminEmail'         => 'admin@galaxysss.ru',
    'mailList'           => [
        'contact' => 'god@avatar-bank.com',
    ],
    'mailer' => [
        // адрес который будет указан как адрес отправителя для писем и рассылок
        'from' => [
            'info@iampau.pro' => 'Я-Человек',
        ],
    ],

    // Ip где находятся все школы, применяется для CloudFlare
    'schoolIp'      => '116.203.36.44',

    'google'             => [
        'plugins' => [
            'reCaptcha' => [
                'key'       => '6LcIwgsUAAAAAFkCKLCyNL-UYSFRCwyHFYJOcnhv',
                'secretKey' => '6LcIwgsUAAAAAGDzjh7kYkv4wOw8PoPwlZLnhJwv',
            ],
        ],
    ],
];
