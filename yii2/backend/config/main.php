<?php

$params = \yii\helpers\ArrayHelper::merge(
    require(__DIR__ . '/../../common/config/params.php'),
    require(__DIR__ . '/../../common/config/params-local.php'),
    require(__DIR__ . '/params.php'),
    require(__DIR__ . '/params-local.php')
);

$config = [
    'id'                                              => 'prometey-backend',
    'basePath'                                        => dirname(__DIR__),
    'bootstrap'                                       => ['log'],
    'controllerNamespace'                             => 'backend\controllers',
    'vendorPath'                                      => dirname(dirname(__DIR__)) . '/vendor',
    'language'                                        => 'ru',
    'sourceLanguage'                                  => 'ru',
    'timeZone'                                        => 'Etc/GMT-3',
    'aliases'                                         => [
        '@csRoot'  => __DIR__ . '/../../common/app',
        '@cs'      => __DIR__ . '/../../common/app',
        '@webroot' => __DIR__ . '/../../public_html',
    ],
    'components'                                      => [
        'session'         => [
            'timeout' => 600,
            'class'   => 'yii\web\CacheSession',
            'cache'   => 'cacheSession',
        ],
        'cacheSession' => [
            'keyPrefix' => 'session',
        ],
        'urlManager' => [
            'rules' => [
                '/'                                                        => 'site/index',
                'telegram'                                                 => 'telegram/index',
                'partner'                                                  => 'partner/index',
                'ru'                                                       => 'ru/index',
                'en'                                                       => 'en/index',
                'documents'                                                => 'documents/index',

                // блог
                'blog'                                                     => 'blog/index',
                'blog/<year:\\d{4}>/<month:\\d{2}>/<day:\\d{2}>/<id:\\w+>' => 'blog/item',
                'blog/<action>'                                            => 'blog/<action>',

                // карточка пользователей
                'user/<id:\\d+>'                                           => 'user/item',

                // карточка школы
                'school/<id:\\d+>'                                         => 'school/item',
                'kurs/<id:\\d+>'                                           => 'kurs/item',

                'upload/HtmlContent2_common'       => 'upload/upload',
                'upload3/upload'                   => 'upload3/upload',
                'partner/registration/<code:\\d+>' => 'partner/registration',

                'page/<controller>'              => 'page/controller',
                'page/<controller>/<action>'     => 'page/controller-action',
                'gridview/<controller>/<action>' => 'gridview/<controller>/<action>',


                'cabinet/<action>'                => 'cabinet/<action>',
                '<controller>/<action>'           => '<controller>/<action>',
                '<controller>/<action>/<id:\\w+>' => '<controller>/<action>',
            ],

        ],
        'languageManager' => [
            'class'    => 'common\components\LanguageManager',
            'strategy' => [
                'ru' => ['ru', 'be', 'uk'],
                'en' => ['en', 'fr', 'de'],
            ],
            'default' => 'ru',
        ],
        'authManager'     => [
            'class' => 'yii\rbac\PhpManager',
        ],
        'assetManager'    => [
            'appendTimestamp' => true,
        ],
        'piramida'        => [
            'class' => 'app\common\components\Piramida',
        ],
        'request'         => [
            // !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
            'cookieValidationKey'    => '',
            'enableCookieValidation' => false,
        ],
        'deviceDetect'    => [
            'class'     => 'avatar\services\DeviceDetect',
            'setParams' => 'false',
        ],
        'user'            => [
            'identityClass'   => 'common\models\UserAvatar',
            'authTimeout'     => 60*60*24*30,
            'enableAutoLogin' => true,
            'loginUrl'        => ['auth/login'],
        ],
        'errorHandler'    => [
            'errorAction' => 'site/error',
            'errorView'   => '@avatar/views/site/error.php',
        ],
        'formatter'       => [
            'class'             => '\iAvatar777\components\FormatterRus\FormatterRus',
            'dateFormat'        => 'php:d.m.b/Y',
            'timeFormat'        => 'php:H:i:s',
            'datetimeFormat'    => 'php:d.m.b/Y H:i',
            'decimalSeparator'  => '.',
            'thousandSeparator' => ',',
            'currencyCode'      => 'RUB',
            'locale'            => 'ru',
            'nullDisplay'       => '',
        ],
        'log'              => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets'    => [
                [
                    'class'       => 'common\services\DbTarget',
                    'levels'      => [
                        'warning',
                        'error',
                    ],
                    'except'      => [
                        'yii\web\HttpException:404',
                        'yii\web\HttpException:403',
                        'cs\web\Exception',
                    ],

                    'db'         => 'dbStatistic',
                    'logTable'   => 'log2_avatar',
                ],

                [
                    'class'      => 'common\services\DbTarget',
                    'categories' => ['avatar\\*'],
                    'levels'     => [
                        'info',
                        'trace',
                    ],

                    'db'         => 'dbStatistic',
                    'logTable'   => 'log2_avatar',
                ],
            ],
        ],
    ],
    'modules' => [
        'gridview' =>  [
            'class' => '\kartik\grid\Module'
            // enter optional module parameters below - only if you need to
            // use your own export download action or custom translation
            // message source
            // 'downloadAction' => 'gridview/export/download',
            // 'i18n' => []
        ]
    ],
    'params'                                          => $params,
    'controllerMap'                                   => [
        'upload'  => '\common\widgets\HtmlContent\Controller',
        'upload3' => '\common\widgets\FileUploadMany2\UploadController',
        'upload4' => '\iAvatar777\assets\JqueryUpload1\Upload2Controller',
    ],
];

return $config;
