<?php
/**
 * @var \common\models\comment\Comment $comment
 * @var \common\models\UserIdea $idea
 */

$url = \yii\helpers\Url::to(['cabinet-idea/view', 'id' => $idea->id], true);
?>

<p>Добавлен новый коментарий к идее</p>
<p>Идентификатор задачи: <?= $comment->getList()->object_id ?></p>
<p>Название задачи: <?= $idea->name ?></p>
<p>Автор: <?= \yii\helpers\Html::encode($comment->getUser()->getName2()) ?></p>
<p>Время коментария: <?= Yii::$app->formatter->asDatetime($comment->created_at) ?></p>
<p>Ссылка на задачу: <a href="<?= $url ?>" target="_blank"><?= $url ?></a></p>

<hr>
<p><?= $comment->getHtml() ?></p>
