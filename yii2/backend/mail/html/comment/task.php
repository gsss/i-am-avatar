<?php
/**
 * @var \common\models\comment\Comment $comment
 * @var \common\models\task\Task $task
 */
/** @var $school_subscribe_image    string                              картинка с относительным путем или полным */

if (!isset($school_subscribe_image)) $school_subscribe_image = '/images/mail/new-lesson/logo_v2.png';
if (\yii\helpers\StringHelper::startsWith($school_subscribe_image, '/')) $school_subscribe_image = \yii\helpers\Url::to($school_subscribe_image, true);

$url = \yii\helpers\Url::to(['cabinet-task-list/view', 'id' => $task->id], true);
?>

<p style="text-align: center">
    <img src="<?= $school_subscribe_image ?>" width="200">
</p>

<p>Добавлен новый коментарий к задаче</p>
<p>Идентификатор задачи: <?= $comment->getList()->object_id ?></p>
<p>Название задачи: <?= $task->name ?></p>
<p>Автор: <?= \yii\helpers\Html::encode($comment->getUser()->getName2()) ?></p>
<p>Время коментария: <?= Yii::$app->formatter->asDatetime($comment->created_at) ?></p>
<p>Ссылка на задачу: <a href="<?= $url ?>" target="_blank"><?= $url ?></a></p>

<hr>
<p><?= $comment->getHtml() ?></p>
