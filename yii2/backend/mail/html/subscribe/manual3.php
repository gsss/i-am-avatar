<?php
/**
 * Created by PhpStorm.
 * User: s.arhangelskiy
 * Date: 13.04.2017
 * Time: 11:34
 */

/** @var $item  \common\models\school\Subscribe */
/** @var $image  string */

?>


<p style="text-align: center"><img src="<?= \yii\helpers\Url::to($image, true) ?>"
                                   width="200" style="border-radius: 20px;"></p>

<h1 style="text-align: center;font-weight: normal;"><?= $item->subject ?></h1>

<?= $item->content ?>
