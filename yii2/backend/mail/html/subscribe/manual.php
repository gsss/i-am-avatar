<?php
/**
 * Created by PhpStorm.
 * User: s.arhangelskiy
 * Date: 13.04.2017
 * Time: 11:34
 */

/** @var $newsItem  \common\models\subscribe\SubscribeOriginal */
?>

<h1 style="text-align: center;font-weight: normal;"><?= $newsItem->subject ?></h1>

<?= $newsItem->html ?>

<p style="margin-top: 40px;">Присоединяйтесь к нашим каналам:</p>
<p>
    <a target="_blank" href="https://www.youtube.com/channel/UC5_n6x3PFK_pA0w2So6lciQ"><img src="https://avatarnetwork.io/images/controller/landing/index/media.png" alt="YouTube" width="43" data-toggle="tooltip" title="" data-placement="top" data-original-title="YouTube"></a>
    <a target="_blank" href="https://www.facebook.com/AvatarNetwork.io/"><img src="https://avatarnetwork.io/images/controller/landing/index/fb.png" alt="FaceBook" data-toggle="tooltip" title="" data-placement="top" data-original-title="FaceBook"></a>
    <a target="_blank" href="https://t.me/AvatarNetwork"><img src="https://avatarnetwork.io/network/assets/images/Telegram.png" alt="Telegram" data-toggle="tooltip" title="" data-placement="top" data-original-title="Telegram"></a>
</p>
