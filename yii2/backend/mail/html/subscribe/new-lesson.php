<?php
/**
 * Created by PhpStorm.
 * User: s.arhangelskiy
 * Date: 13.04.2017
 * Time: 11:34
 */

/** @var $lesson \common\models\school\Lesson */
$url = \yii\helpers\Url::to(['cabinet-kurs-list/lesson-view', 'id' => $lesson->id], true);

?>

<p>В курс в котором вы обучаетесь "<?= $lesson->getKurs()->name ?>" добавлен урок "<?= $lesson->name ?>".</p>
<p><img src="<?= \common\widgets\FileUpload7\FileUpload::getFile($lesson->image, 'crop') ?>" width="200" style="border-radius: 20px;"></p>
<p>Ознакомиться с ним вы можете по ссылке: <a href="<?= $url ?>" target="_blank"><?= $url ?></a></p>

