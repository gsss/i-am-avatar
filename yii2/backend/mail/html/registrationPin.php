<?php
/**
 * @var $pin        string
 * @var $user       \common\models\UserAvatar
 */
?>

<p>Вы успешно прошли регистрацию в AvatarNetwork.</p>
<p>Добро пожаловать в новый мир!</p>
<p>Pin code: <?= $pin ?></p>