<?php
/**
 * Created by PhpStorm.
 * User: Ra-m-ha
 * Date: 06.01.2019
 * Time: 19:08
 */

/** @var string $url */
/** @var string $email */
/** @var string $username логин в телеграмме */
/** @var string $hash хеш */

?>


<p>Чтобы подтвердить присоединение своего аккаунта Telegram <b><?= $username ?></b>, нажми кнопку ниже.</p>

<p><a href="<?= $url ?>" style="
text-decoration: none;
color: #fff;
background-color: #337ab7;
border-color: #2e6da4;
display: inline-block;
padding: 6px 12px;
margin-bottom: 0;
font-size: 14px;
font-weight: normal;
line-height: 1.42857143;
text-align: center;
white-space: nowrap;
vertical-align: middle;
-ms-touch-action: manipulation;
touch-action: manipulation;
cursor: pointer;
-webkit-user-select: none;
-moz-user-select: none;
-ms-user-select: none;
user-select: none;
background-image: none;
border: 1px solid transparent;
border-radius: 4px;
">Ссылка</a></p>

