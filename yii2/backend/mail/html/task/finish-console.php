<?php
/**
 * Created by PhpStorm.
 * User: Ra-m-ha
 * Date: 06.01.2019
 * Time: 19:08
 */

/** @var \common\models\task\Task $task */


$url = \yii\helpers\Url::to(['cabinet-task-list/view', 'id' => $task->id], true);
?>

<p>Задача остановлена в связи с тем что она уже более 24 часов открыта.</p>
<p>ID: <?= $task->id ?>.</p>
<p>Задача: "<?= $task->name ?>".</p>
<p>Задачу можно посмотреть по ссылке <a href="<?= $url ?>"><?= $url ?></a></p>
