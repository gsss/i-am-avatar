<?php
/**
 * Created by PhpStorm.
 * User: Ra-m-ha
 * Date: 06.01.2019
 * Time: 19:08
 */

/** @var \common\models\school\DnsRequest $request */
/** @var string $reason */

?>


<p>Поздравляем. Ваш домен отклонен.</p>
<p>Заявка id=<?= $request->id ?></p>
<p>Причина: <?= \yii\helpers\Html::encode($reason) ?></p>

