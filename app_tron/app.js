var express = require('express');
var path = require('path');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var basicAuth = require('express-basic-auth');
var fs = require('fs');

var index = require('./routes/index');
var createAccount = require('./routes/createAccount');
var getAccount = require('./routes/getAccount');
var sendToken = require('./routes/sendToken');
var sendTrx = require('./routes/sendTrx');
var sendToken2 = require('./routes/sendToken2');
var toHex = require('./routes/toHex');

var app = express();

// Сообщение в случае неудачной авторизации
function getUnauthorizedResponse(req) {
  return req.auth ?
      ({ error: 'Credentials ' + req.auth.user + ':' + req.auth.password + ' rejected'}) :
        { error: 'No credentials provided' };
}

// Проверка авторизации
function checkCredentials(username, password) {
    try {
        var db = JSON.parse(fs.readFileSync('./api.json', 'utf8'));
        var valid = false;
        for(var i = 0; i < db.length; i++) {
            if(db[i].id == username) {
                if (db[i].secret == password) {
                    valid = true;
                }
            }
        }
        return valid;
    } catch (e) {
        return false;
    }
}

// настройка view
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/', index);
//app.use('/api_key', api_key);

// Авторизация
app.use(basicAuth({
    unauthorizedResponse: getUnauthorizedResponse,
    authorizer: checkCredentials,
    challenge: true
}));

app.use('/createAccount', createAccount);
app.use('/getAccount', getAccount);
app.use('/sendToken', sendToken);
app.use('/sendToken2', sendToken2);
app.use('/sendTrx', sendTrx);
app.use('/toHex', toHex);

// ловит ошибку 404 и перенаправляет на обработчик ошибок
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// обработчик ошибок
app.use(function(err, req, res, next) {
  // показывает ошибки только в режиме разработки
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // создание страницы ошибки
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
