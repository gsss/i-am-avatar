https://tron.blockchain.social/

git clone git@bitbucket.org:gsss/armmoney-tronweb.git

TRONWEB API - предназначено для создания кошельков трона и получение всех их параметров.

Файлы кошельков все размещаются в папке `./account-list`

Формат названия файла: `[base58address].json`

# Авторизация

Для авторизации используется авторизация `Basic Authentification`

https://efim360.ru/rfc-2617-http-autentifikatsiya-bazovaya-i-daydzhest-autentifikatsiya/

```php
$credentials = "user:password";
header("Authorization: Basic " . base64_encode($credentials));
```

Ключи лежат в файле `./api.json`

# API key

тут вам надо зарегистрироваться https://www.trongrid.io/
там надо получить TRON_API_KEY чтобы ваш был

# Запуск на сервере

перейти в папку проекта и выполнить команду:

```php
npm start
```

Проект стартуется автоматически со стартом сервера. работает внутри на 3001 порту и проксируется на сайт `tron.blockchain.social`.

# Обновление сертификата

Сертификат действует 3 месяца со дня выпуска. На сервере стоит крон (автозадача) которая 1 числа месяца перевыпускает сертификат. 

# фунцции

## фунцция createAccount

Метод: **POST**

входящие параметры 
- password

### Возвращаемые параметры:

Успешно
```json
{
    "success": true,
    "data": {
        "publicKey": "04AF036B0E0219C973FDBFD91D2F7E3BFEED9AB5CE796E488716B47A614A5D03E30DD2E90132E983DF762723B70E8AE5C256F50E49274617114405203A02335AC2",
        "address": {
            "base58": "TMWSf8iPFrcJ5kD2hm1nniEFtMkMU6CuJe",
            "hex": "417E91ADA399AAA8A7CBB30D703C1AF194E1A4B2F0"
        }
    }
}
```

### Ошибки:

```json
{
    "success": false,
    "data": {
        "status": 400,
        "message": "Password mast be not empty"
    }
}
```

Пример:

```php
$post_data = array(
    "password" => "bar",
);
$id = '';
$secret = '';
$credentials = "$id:$secret";

$url = 'https://tron.blockchain.social/createAccount'; // url, на который отправляется запрос
$post_data = [ // поля нашего запроса
       "password" => "bar",
];

$headers = []; // заголовки запроса
$headers[] = "Authorization: Basic " . base64_encode($credentials);

$post_data = http_build_query($post_data);

$curl = curl_init();
curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
curl_setopt($curl, CURLOPT_VERBOSE, 1);
curl_setopt($curl, CURLOPT_POSTFIELDS, $post_data);
curl_setopt($curl, CURLOPT_URL, $url);
curl_setopt($curl, CURLOPT_POST, true); // true - означает, что отправляется POST запрос
curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);

$result = curl_exec($curl);
```


## фунцция getAccount

Метод: **POST**

входящие параметры 
- address_base58
- password

### Возвращаемые параметры:

Успешно
```json
{
    "success": true,
    "data": {
        "privateKey": "*********************************************",
        "publicKey": "04AF036B0E0219C973FDBFD91D2F7E3BFEED9AB5CE796E488716B47A614A5D03E30DD2E90132E983DF762723B70E8AE5C256F50E49274617114405203A02335AC2",
        "address": {
            "base58": "TMWSf8iPFrcJ5kD2hm1nniEFtMkMU6CuJe",
            "hex": "417E91ADA399AAA8A7CBB30D703C1AF194E1A4B2F0"
        }
    }
}
```

### Ошибки:

```json
{
    "success": false,
    "data": {
        "status": 404,
        "message": "File not found"
    }
}
```

```json
{
    "success": false,
    "data": {
        "status": 400,
        "message": "Password not equal"
    }
}
```

```json
{
    "success": false,
    "data": {
        "status": 410,
        "message": "Password mast be not empty"
    }
}
```

Пример:

```php
$post_data = array(
    "address_base58" => "TFHQzGf2rvDNGYGCiKMzyJxSqQuU8Zk3sH",
    "password"       => "bar",
);
$id = '';
$secret = '';
$credentials = "$id:$secret";

$url = 'https://tron.blockchain.social/getAccount'; // url, на который отправляется запрос


$headers = []; // заголовки запроса
$headers[] = "Authorization: Basic " . base64_encode($credentials);

$post_data = http_build_query($post_data);

$curl = curl_init();
curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
curl_setopt($curl, CURLOPT_VERBOSE, 1);
curl_setopt($curl, CURLOPT_POSTFIELDS, $post_data);
curl_setopt($curl, CURLOPT_URL, $url);
curl_setopt($curl, CURLOPT_POST, true); // true - означает, что отправляется POST запрос
curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);

$result = curl_exec($curl);
$e = curl_error($curl);
return $result;
```

## функция sendToken2

Метод: **POST**

входящие параметры: 
- from - string - адрес откуда отправляются base58
- password - string - пароль от кошелька отправителя
- to - string - адрес для отправки base58
- amount - int - кол-во отправляемых atom (копеек, самых малых частиц) токена
- tokenID - string - адрес токена base58 (адрес контракта)

### Возвращаемые параметры:

Успешно
```json
{
    "success": true,
    "data": {
        "txid": "*********************************************"
    }
}
```

### Ошибки:

```json
{
    "success": false,
    "data": {
        "status": 404,
        "message": "File not found"
    }
}
```

```json
{
    "success": false,
    "data": {
        "status": 400,
        "message": "Password not equal"
    }
}
```

```json
{
    "success": false,
    "data": {
        "status": 410,
        "message": "Password mast be not empty"
    }
}
```

## функция sendTrx

Метод: **POST**

входящие параметры: 
- from - string - адрес откуда отправляются base58
- password - string - пароль от кошелька отправителя
- to - string - адрес для отправки base58
- amount - int - кол-во отправляемых atom (копеек, самых малых частиц 10e-6)

### Возвращаемые параметры:

Успешно
```json
{
    "success": true,
    "data": {
        "txid": "*********************************************"
    }
}
```

### Ошибки:

```json
{
    "success": false,
    "data": {
        "status": 404,
        "message": "File not found"
    }
}
```

```json
{
    "success": false,
    "data": {
        "status": 400,
        "message": "Password not equal"
    }
}
```

```json
{
    "success": false,
    "data": {
        "status": 410,
        "message": "Password mast be not empty"
    }
}
```

Пример:

```php
$post_data = array(
    'from'     => 'TV1Whxi9YVz456TiHP6Qbjb3h6BLARix6U',
    'password' => "*******************************",
    'to'       => 'TLUzbZ8VzXoC3eMBhKJ6uonT1h1SDnfXAk',
    'amount'   => '1',
);
$id = '***********************';
$secret = '***********************';
$credentials = "$id:$secret";

$url = 'https://tron.blockchain.social/sendTrx'; // url, на который отправляется запрос


$headers = []; // заголовки запроса
$headers[] = "Authorization: Basic " . base64_encode($credentials);

$post_data = http_build_query($post_data);

$curl = curl_init();
curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
curl_setopt($curl, CURLOPT_VERBOSE, 1);
curl_setopt($curl, CURLOPT_POSTFIELDS, $post_data);
curl_setopt($curl, CURLOPT_URL, $url);
curl_setopt($curl, CURLOPT_POST, true); // true - означает, что отправляется POST запрос
curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);

$result = curl_exec($curl);
$e = curl_error($curl);
```