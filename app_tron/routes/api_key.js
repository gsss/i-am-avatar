var express = require('express');
var router = express.Router();
var uuid = require('node-uuid');
var fs = require('fs');

var API_PATH = './api.json';

/* POST запрос на генерацию API key-pair. */
router.post('/', function(req, res, next) {
    if (!fs.existsSync(API_PATH)) {
        fs.writeFileSync(API_PATH, JSON.stringify([]));
    }
    var db = JSON.parse(fs.readFileSync(API_PATH, 'utf8'));
    if (db.length == 0) {
        db = [];
    }
    var keyPair = {
        id: uuid.v4(),
        secret: uuid.v4()
    };
    db.push(keyPair);
    fs.writeFileSync(API_PATH, JSON.stringify(db));
    res.send(JSON.stringify(keyPair));
});

/* GET запрос на получение списка API key-pair. */
router.get('/', function(req, res, next) {
    if (!fs.existsSync(API_PATH)) {
        fs.writeFileSync(API_PATH, JSON.stringify([]));
    }
    var db = JSON.parse(fs.readFileSync(API_PATH, 'utf8'));
    if (db.length == 0) {
        db = [];
    }
    res.send(JSON.stringify(db));
});


module.exports = router;
