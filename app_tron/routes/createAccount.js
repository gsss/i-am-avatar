var express = require('express');
var router = express.Router();
var fs = require('fs');
var path = require('path');

const TronWeb = require('tronweb');
const tronWeb = new TronWeb({
    fullHost: 'https://api.trongrid.io',
    headers: { "TRON-PRO-API-KEY": 'a0a16ec2-660e-4584-bd7e-4da4ca54a148' },
    privateKey: '85c4a1b0c7c7a9a390c039001972df38f07a423b40a702bb72d18d875d6594ab'
});

/* POST запрос. */
router.post('/', function(req, res, next) {

    var password = req.query.password || req.body.password;

    if(typeof password == "undefined") {

        var oRet = {
            success: false,
            data: {
                status: 400,
                message: 'Password mast be not empty'
            }
        };

        res.send(oRet);

        return;
    }

    tronWeb.createAccount().then(function (e) {
        var oFile = {
            privateKey: e.privateKey,
            publicKey: e.publicKey,
            address: e.address,
            password: password
        };
        var oRet = {
            success: true,
            data: {
                publicKey: e.publicKey,
                address: e.address
            }
        };

        var content = JSON.stringify(oFile);
        fs.writeFileSync("./account-list/" + e.address.base58 + '.json', content);

        res.send(oRet);
    });

});

module.exports = router;
