var express = require('express');
var router = express.Router();

var fs = require('fs');

/* POST запрос на выдачу файла JSON */
router.post('/', function(req, res, next) {

    var address_base58 = req.query.address_base58 || req.body.address_base58;
    var password = req.query.password || req.body.password;

    if (typeof password == "undefined") {

        var oRet = {
            "success": false,
            "data": {
                "status": 410,
                "message": "Password mast be not empty"
            }
        };

        res.send(oRet);

        return;
    }

    var resp;
    try {

        // Функция ищет список файлов
        var getFiles = function (dir, files_)
        {

            files_ = files_ || [];
            var files = fs.readdirSync(dir);
            for (var i in files){
                var name = dir + '/' + files[i];
                if (fs.statSync(name).isDirectory()){
                    getFiles(name, files_);
                } else {
                    files_.push({
                        path: name,
                        name: files[i],
                        split: files[i].split('.')
                    });
                }
            }
            return files_;
        };

        var f2;

        // Получаю список файлов
        /** @var array [{path:'',name:'',split:['', 'json']},]  */
        f2 = getFiles('./account-list', null);

        var i1;
        var i2;
        var isFind = false;
        var id1 = address_base58;

        for(i1=0; i1 < f2.length; i1++) {
            if (f2[i1].split[0] == id1) {
                isFind = true;
                i2 = f2[i1];
            }
        }

        if (isFind) {
            var text = fs.readFileSync(i2.path, 'utf8');
            var o = JSON.parse(text);

            if (o.password != password) {
                resp = {
                    "success": false,
                    "data": {
                        "status": 400,
                        "message": "Password not equal"
                    }
                };
                res.send(resp);
                return;
            }

            resp = {
                "success": true,
                "data": {
                    "privateKey": o.privateKey,
                    "publicKey": o.publicKey,
                    "address": o.address
                }
            };
            res.send(resp);

        } else {
            resp = {
                "success": false,
                "data": {
                    "status": 404,
                    "message": "File not found"
                }
            };
            res.send(resp);
        }

    } catch (e) {
        resp = {
            "success": false,
            "data": {
                "status": 500,
                "message": e.message
            }
        };
        res.send(resp);
    }
});

module.exports = router;
