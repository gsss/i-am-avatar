var express = require('express');
var router = express.Router();

var testnet = false;

const TESTNET_NODE = 'https://api.shasta.trongrid.io';
const MAINNET_NODE = 'https://api.trongrid.io';

const TronWeb = require('tronweb');
const tronWeb = new TronWeb({
    fullHost: testnet ? TESTNET_NODE : MAINNET_NODE,
    headers: { "TRON-PRO-API-KEY": 'a0a16ec2-660e-4584-bd7e-4da4ca54a148' },
    privateKey: '85c4a1b0c7c7a9a390c039001972df38f07a423b40a702bb72d18d875d6594ab'
});

// https://developers.tron.network/reference#sendtoken

/* POST запрос. */
router.post('/', function(req, res, next) {

    var address = req.query.address || req.body.address;

    if (typeof address == "undefined") {

        var oRet = {
            "success": false,
            "data": {
                "status": 400,
                "message": "address mast be not empty"
            }
        };

        res.send(oRet);

        return;
    }

    var h = tronWeb.address.toHex(address);
    res.send({hex: h});


});

module.exports = router;
