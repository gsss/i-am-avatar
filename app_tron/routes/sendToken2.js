var express = require('express');
var router = express.Router();
var fs = require('fs');
var path = require('path');

var testnet = false;

const TESTNET_NODE = 'https://api.shasta.trongrid.io';
const MAINNET_NODE = 'https://api.trongrid.io';

const TronWeb = require('tronweb');
const tronWeb = new TronWeb({
    fullHost: testnet ? TESTNET_NODE : MAINNET_NODE,
    headers: { "TRON-PRO-API-KEY": 'a0a16ec2-660e-4584-bd7e-4da4ca54a148' },
    privateKey: '85c4a1b0c7c7a9a390c039001972df38f07a423b40a702bb72d18d875d6594ab'
});

// https://developers.tron.network/reference#tronweb-triggersmartcontract

async function send(from, from_pk, to, tokenID, amount)
{
    const options = {};

    const functionSelector = 'transfer(address,uint256)';

    var contract_address = tronWeb.address.toHex(tokenID);
    var parameter = [{type:'address',value: to}, {type:'uint256',value: amount}];

    let transactionObject = await tronWeb.transactionBuilder.triggerSmartContract (
        contract_address,
        functionSelector,
        options,
        parameter,
        tronWeb.address.toHex(from)
    );

    if (!transactionObject.result || !transactionObject.result.result)
        return console.error('Unknown error: ' + txJson, null, 2);

    // Signing the transaction
    const signedTransaction = await tronWeb.trx.sign(transactionObject.transaction, from_pk);

    if (!signedTransaction.signature) {
        return console.error('Transaction was not signed properly');
    }

    // Broadcasting the transaction
    const broadcast = await tronWeb.trx.sendRawTransaction(signedTransaction);

    return broadcast.txid;
}

/* POST запрос. */
router.post('/', function(req, res, next) {

    var from = req.query.from || req.body.from;
    var to = req.query.from || req.body.to;
    var password = req.query.password || req.body.password;
    var amount = req.query.amount || req.body.amount;
    var tokenID = req.query.tokenID || req.body.tokenID;

    if (typeof password == "undefined") {

        var oRet = {
            "success": false,
            "data": {
                "status": 410,
                "message": "Password mast be not empty"
            }
        };

        res.send(oRet);

        return;
    }

    var resp;
    try {

        // Функция ищет список файлов
        var getFiles = function (dir, files_)
        {

            files_ = files_ || [];
            var files = fs.readdirSync(dir);
            for (var i in files){
                var name = dir + '/' + files[i];
                if (fs.statSync(name).isDirectory()){
                    getFiles(name, files_);
                } else {
                    files_.push({
                        path: name,
                        name: files[i],
                        split: files[i].split('.')
                    });
                }
            }
            return files_;
        };

        var f2;

        // Получаю список файлов
        /** @var array [{path:'',name:'',split:['', 'json']},]  */
        f2 = getFiles('./account-list', null);

        var i1;
        var i2;
        var isFind = false;
        var id1 = from;

        for(i1=0; i1 < f2.length; i1++) {
            if (f2[i1].split[0] == id1) {
                isFind = true;
                i2 = f2[i1];
            }
        }

        if (isFind) {
            var text = fs.readFileSync(i2.path, 'utf8');
            var o = JSON.parse(text);

            if (o.password != password) {
                resp = {
                    "success": false,
                    "data": {
                        "status": 400,
                        "message": "Password not equal"
                    }
                };
                res.send(resp);
                return;
            }

            // тут отправляю
            try {

                send(from, o.privateKey, to, tokenID, amount).then(function (ret) {
                    resp = {
                        "success": true,
                        "data": {
                            "txid": ret
                        }
                    };
                    res.send(resp);
                });

            } catch (e) {
                return console.error(e);
            }


        } else {
            resp = {
                "success": false,
                "data": {
                    "status": 404,
                    "message": "File not found"
                }
            };
            res.send(resp);
        }

    } catch (e) {
        resp = {
            "success": false,
            "data": {
                "status": 500,
                "message": e.message
            }
        };
        res.send(resp);
    }

});

module.exports = router;
