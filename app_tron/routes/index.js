var express = require('express');
var router = express.Router();

/* GET запрос для главной страницы, создаётся страница из шаблона views/index */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'TronWeb API' });
});

module.exports = router;
