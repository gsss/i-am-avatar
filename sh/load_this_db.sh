#!/bin/bash

# $1 db root password

rm /root/i-am-avatar/i_am_avatar_prod_main.sql
rm /root/i-am-avatar/i_am_avatar_prod_wallet.sql

cp /root/i-am-avatar/db/dumps/* /root/i-am-avatar/
gunzip -d /root/i-am-avatar/i_am_avatar_prod_main.sql.gz
gunzip -d /root/i-am-avatar/i_am_avatar_prod_wallet.sql.gz
cd /root/i-am-avatar

mysql -u root -p$1 i_am_avatar_prod_main < i_am_avatar_prod_main.sql
mysql -u root -p$1 i_am_avatar_prod_wallet < i_am_avatar_prod_wallet.sql

rm /root/i-am-avatar/i_am_avatar_prod_main.sql
rm /root/i-am-avatar/i_am_avatar_prod_wallet.sql