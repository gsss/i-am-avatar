#!/bin/bash

# $1 db root password

## Экспортирую БД
docker exec i-am-avatar-mysql mysqldump -u root -p$1 i_am_avatar_prod_main   | gzip > /root/i-am-avatar/db/dumps/i_am_avatar_prod_main.sql.gz
docker exec i-am-avatar-mysql mysqldump -u root -p$1 i_am_avatar_prod_wallet | gzip > /root/i-am-avatar/db/dumps/i_am_avatar_prod_wallet.sql.gz

## Скопировать БД в STAGE
scp -r /root/i-am-avatar/db/dumps avatar@217.61.110.105:/home/avatar/i-am-avatar/db

## Скопировать папку `upload` в STAGE
ssh avatar@217.61.110.105 'rm -R /root/backups_db_prod/upload'
scp -r /root/i-am-avatar/public_html/upload avatar@217.61.110.105:/home/avatar/i-am-avatar/db