#!/bin/bash

rm /root/i-am-avatar/i_am_avatar_prod_main.sql
rm /root/i-am-avatar/i_am_avatar_prod_wallet.sql

cp /root/i-am-avatar/db/dumps/i_am_avatar_prod_main.sql.gz   /root/i-am-avatar/i_am_avatar_prod_main.sql.gz
cp /root/i-am-avatar/db/dumps/i_am_avatar_prod_wallet.sql.gz /root/i-am-avatar/i_am_avatar_prod_wallet.sql.gz
gunzip -d /root/i-am-avatar/s_routes_prod_main.sql.gz
gunzip -d /root/i-am-avatar/s_routes_prod_wallet.sql.gz
cd /root/i-am-avatar

docker exec i-am-avatar-mysql mysql -u root -pAinHMxMAfTPRtznkaFjm i_am_avatar_prod_main < i_am_avatar_prod_main.sql
docker exec i-am-avatar-mysql mysql -u root -pAinHMxMAfTPRtznkaFjm i_am_avatar_prod_wallet < i_am_avatar_prod_wallet.sql

rm /root/i-am-avatar/i_am_avatar_prod_main.sql
rm /root/i-am-avatar/i_am_avatar_prod_wallet.sql