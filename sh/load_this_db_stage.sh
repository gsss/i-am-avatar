#!/bin/bash

# $1 db root password

rm /home/avatar/i-am-avatar/i_am_avatar_prod_main.sql
rm /home/avatar/i-am-avatar/i_am_avatar_prod_wallet.sql

cp /home/avatar/i-am-avatar/db/dumps/i_am_avatar_prod_main.sql.gz /home/avatar/i-am-avatar/i_am_avatar_prod_main.sql.gz
cp /home/avatar/i-am-avatar/db/dumps/i_am_avatar_prod_wallet.sql.gz /home/avatar/i-am-avatar/i_am_avatar_prod_wallet.sql.gz
gunzip -d /home/avatar/i-am-avatar/i_am_avatar_prod_main.sql.gz
gunzip -d /home/avatar/i-am-avatar/i_am_avatar_prod_wallet.sql.gz
cd /home/avatar/i-am-avatar

mysql -u root -p$1 i_am_avatar_prod_main2 < i_am_avatar_prod_main.sql
mysql -u root -p$1 i_am_avatar_prod_wallet2 < i_am_avatar_prod_wallet.sql

rm /home/avatar/i-am-avatar/i_am_avatar_prod_main.sql
rm /home/avatar/i-am-avatar/i_am_avatar_prod_wallet.sql