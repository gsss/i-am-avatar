# Социальная нейросеть

- `install.sh` - установка на голую VM 

```
install.sh {GITHUB_TOKEN} {PROD_DB_TOKEN} {ENV_FILE}
```


- `update.sh` - закачать изменения с репозитория и применить миграции 
- `ssl.sh` - выпустить сертификат с параметром домена
- `load_db.sh` - залить БД (внутри контейнера)
- `apply_env.sh` - залить новые настройки по параметру

```
apply_env.sh .env.prod 
```

- `composer_start.sh` - установить композер
- `composer_update.sh` - обновить композер
- `test.sh` - запустить тест

# Запуск

- localhost:3081 - главный сайт
- localhost:3000 - чат
- localhost:3082 - сообщество Здраво
